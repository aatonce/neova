/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/NvDpyObject[NGC].h>
#include <NvUserShader.h>




interface NvUserShaderBase : public NvDpyObject_NGC
{
	NvUserShader				itf;
	NvUserShader::UserCB*		ucb;


	virtual	~	NvUserShaderBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)


	void	Init	( NvUserShader::UserCB*		inUCB	)
	{
		InitDpyObject();
		ucb = inUCB;
		ucb->OnRef( &itf );
	}


	void	Shut	(		)
	{
		ucb->OnRelease( &itf );
		ucb = NULL;
	}


	void	AddRef	(		)
	{
		refCpt++;
	}


	uint	GetRefCpt	(		)
	{
		return refCpt;
	}


	void	Release	(		)
	{
		if( ShutDpyObject() )
		{
			Shut();
			NvEngineDelete( this );
		}
	}


	bool	Draw	(			)
	{
		if( ucb )
		{
			ucb->OnDraw( &itf, dpyctxt.drawFrameNo, dpyctxt.chainHead, dpyctxt.chainCpt );
		}
		else
		{
			//
		}

		return TRUE;
	}
};



NvUserShader*
NvUserShader::Create	(	UserCB*		inUCB	)
{
	if( inUCB )
	{
		NvUserShaderBase * inst = NvEngineNew( NvUserShaderBase );
		if( inst )
		{
			inst->Init( inUCB );
			return &inst->itf;
		}
	}

	return NULL;
}




//
// INTERFACES


#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( UserShader,	NvInterface*,	GetBase				)
NVITF_CAL0( UserShader,	void,			AddRef				)
NVITF_MTH0( UserShader,	uint,			GetRefCpt			)
NVITF_CAL0( UserShader,	void,			Release				)



