/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/NGC/NvDpyObject[NGC].h>
#include <Kernel/NGC/NvkMesh[NGC].h>
#include <NvShadowingShader.h>
using namespace nv;



//
// BASE


struct NvShadowingShaderBase : public NvDpyObject_NGC
{
	NvShadowingShader itf;

	virtual	~	NvShadowingShaderBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

	void
	_Init		(	NvkMesh*	inKMesh	)
	{
		InitDpyObject();
	}

	void
	Release			(		)
	{
		if( ShutDpyObject() ) {
			NvEngineDelete( this );
		}
	}

	void
	Enable		(	uint32		inEnableFlags	)
	{
		//
	}

	void
	Disable		(	uint32		inEnableFlags	)
	{
		//
	}

	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
		//
	}

	uint32
	GetEnabled		(		)
	{
		return 0;
	}

	NvResource*
	GetResource		(		)
	{
		return NULL;
	}

	bool
	Setup			(	NvShadowingShader::Type		inType,
						const Vec3&					inSource,
						float						inOffsetLength,
						float						inExtrudeLength		)
	{
		return FALSE;
	}

	void
	SetColor		(	uint32		inRGBA	)
	{
		//
	}

	void
	SetFlags		(	uint		inFlags	)
	{
		//
	}

	uint
	GetFlags		(				)
	{
		return 0;
	}


	bool
	Draw			(	)
	{
		return FALSE;
	}
};





NvShadowingShader*
NvShadowingShader::Create	(	NvResource*		inRsc		)
{
	return NULL;
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( ShadowingShader,	NvInterface*,	GetBase												)
NVITF_CAL0( ShadowingShader,	void,			AddRef												)
NVITF_MTH0( ShadowingShader,	uint,			GetRefCpt											)
NVITF_CAL0( ShadowingShader,	void,			Release												)
NVITF_MTH0( ShadowingShader,	NvResource*,	GetResource											)
NVITF_CAL1( ShadowingShader,	void,			Enable,				uint32							)
NVITF_CAL1( ShadowingShader,	void,			Disable,			uint32							)
NVITF_CAL1( ShadowingShader,	void,			SetEnabled,			uint32							)
NVITF_MTH0( ShadowingShader,	uint32,			GetEnabled											)
NVITF_MTH4( ShadowingShader,	bool,			Setup,				Type, const Vec3&, float, float	)
NVITF_MTH1( ShadowingShader,	void,			SetColor,			uint32							)
NVITF_CAL1( ShadowingShader,	void,			SetFlags,			uint							)
NVITF_MTH0( ShadowingShader,	uint,			GetFlags											)



