/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
#include "NvLightmapShader_Base[NGC].h"
#include <Kernel/NGC/NvkBitmap[NGC].h>
#include <Kernel/NGC/NvkMesh[NGC].h>
#include <Kernel/NGC/NvDpyState[NGC].h>
using namespace nv;

//#define	TEST_CLIPPING_MODE
//#define	DISABLE_TEXTURING
#define INVALID_SURF 0xFFFFFFFF

namespace
{		
	interface Shader : public NvLightmapShaderBase
	{
		struct Surface {
			DpyState_NGC		dpystate;
			Vec4				ambient	;			// in [0,1]^4
			Vec4				diffuse	;			// in [0,1]^4
			bool				hidden	;
		};

		uint32					enflags;			// enable flags (EN_...)
		NvkMesh*				mesh;
		uint					surfCpt;
		Surface*				surf;
		NvkMesh::Vtx* 			mvtx;

		NvkMesh::Bunch* 		mbunch;
		NvkMesh::Bunch*	 		multiTexbunch;
		uint16	*			 	surfDiff ;
		uint16	*			 	surfLight;
		uint 					new_surfCpt;
	
		virtual	~	Shader	(	) {}				// needed by nv::DestructInPlace<T>(ptr)

		uint GetNbVtxcpt(NvkMesh::Bunch* b)
		{
			if (!b) return 0;
			
			return *((uint16*)(((uint8* )b->dlistAddr) +1));
		}
		
		void 	buildDL()
		{
		
			uint indexCompBStride = 0;
			uint texBStride = 0;
			if (mvtx->vtxFmt & vtxfmt::CO_LOC_MASK) indexCompBStride += 2;
			if (mvtx->vtxFmt & vtxfmt::CO_NRM_MASK) indexCompBStride += 2;
			if (mvtx->vtxFmt & vtxfmt::CO_COL_MASK) indexCompBStride += 2;
			texBStride = indexCompBStride;
			if (mvtx->vtxFmt & vtxfmt::CO_TEX_MASK) indexCompBStride += 2;

			uint8 vtxType = *((uint8* )mbunch[0].dlistAddr);
			vtxType = vtxType & 0xF8;

				
			for (uint dli = 0 ; dli < new_surfCpt ; ++dli) {
			
				uint surfDiffId = surfDiff[dli];
				uint surfLightId= surfLight[dli];
				
				// Init de multi tex bunch
				multiTexbunch[dli] = mbunch[surfDiffId];

				uint16 vtxCpt0 = GetNbVtxcpt ( mbunch + surfDiffId );
				uint16 vtxCpt1 = GetNbVtxcpt ( mbunch + surfLightId );
				
				multiTexbunch[dli].dlistBSize 	= Round32(1 + 2 + (vtxCpt0 * 3 * sizeof(uint16)));
				multiTexbunch[dli].dlistAddr	= NvMalloc(multiTexbunch[dli].dlistBSize);
				
				NV_ASSERT(vtxCpt0 == vtxCpt1);
				
				Memset(multiTexbunch[dli].dlistAddr,0,multiTexbunch[dli].dlistBSize);

				*((uint8 *)multiTexbunch[dli].dlistAddr) 				= (vtxType) | GX_VTXFMT0 ;
				*((uint16*)(((uint8 *)multiTexbunch[dli].dlistAddr)+1)) 	= vtxCpt0 ;
				
				uint16 * ptrDst  	= (uint16*)(((uint8*)multiTexbunch[dli].dlistAddr) 	+ 3 );
				uint16 * ptrSrc1Loc = (uint16*)(((uint8*)mbunch[surfDiffId].dlistAddr) 	+ 3 );
				uint16 * ptrSrc1Tex = (uint16*)(((uint8*)mbunch[surfDiffId].dlistAddr) 	+ 3 + texBStride );
				uint16 * ptrSrc2Loc = (uint16*)(((uint8*)mbunch[surfLightId].dlistAddr) + 3 );
				uint16 * ptrSrc2Tex = (uint16*)(((uint8*)mbunch[surfLightId].dlistAddr) + 3 + texBStride );

				uint	stride16 = indexCompBStride / 2;
				
				for (uint i = 0 ; i < vtxCpt0 ; ++i){
					ptrDst[i*3+0] = *ptrSrc1Loc;
					ptrDst[i*3+1] = *ptrSrc1Tex;
					ptrDst[i*3+2] = *ptrSrc2Tex;

					ptrSrc1Loc += stride16;
					ptrSrc1Tex += stride16;
					ptrSrc2Tex += stride16;
				}
				SyncDCache( multiTexbunch[dli].dlistAddr, multiTexbunch[dli].dlistBSize );	
				
			}
		}
		
		bool
		_Init		(	NvMesh*		inMesh		)
		{
			InitDpyObject();
			
			enflags    = NvShader::EN_DEFAULT;
			#ifdef DISABLE_TEXTURING
			enflags   ^= NvShader::EN_TEXTURING;
			#endif

			mesh = inMesh->GetKInterface();
			mesh->AddRef();

			mvtx   = mesh->GetVtx();
			mbunch = mesh->GetBunchA();
			

			// Init capabilities
			uint dpycaps = 0;
			if( mvtx->vtxFmt & vtxfmt::CO_TEX_MASK )	dpycaps |= DpyState_NGC::CF_HAS_TEX0;
			if( mvtx->vtxFmt & vtxfmt::CO_COL_MASK )	dpycaps |= DpyState_NGC::CF_HAS_RGBA;

			// Setup internal surfaces
			NvkMesh::Shading* shadingA = mesh->GetSurfaceShadingA();
			for( uint i = 0 ; i < surfCpt ; i++ ) {
				// hidden
				surf[i].hidden = FALSE;

				// Init dpystate
				ConstructInPlace( &surf[i].dpystate );
				surf[i].dpystate.Init();
				surf[i].dpystate.SetCapabilities	( dpycaps );
				surf[i].dpystate.SetEnabled			( shadingA[i].stateEnFlags & enflags );
				surf[i].dpystate.SetMode			( shadingA[i].stateMdFlags );
				surf[i].dpystate.SetAlphaPass		( shadingA[i].stateAlphaPass );
				surf[i].dpystate.SetBlendSrcCte		( shadingA[i].stateBlendCte );
				surf[i].dpystate.SetTexturing		( shadingA[i].bitmapUID, 0, 0 );
				surf[i].dpystate.SetDefaultColor	( ConvertPSM(PSM_RGBA32, PSM_ABGR32,  shadingA[i].diffuse ) );
			}
			
		
			new_surfCpt = surfCpt/2;
			multiTexbunch = (NvkMesh::Bunch*)NvMalloc(sizeof(NvkMesh::Bunch) * new_surfCpt );
			
			uint diffCpt 	= 0;
			uint lightCpt 	= 0;
			surfDiff 	= (uint16*)NvMalloc(sizeof(uint16) * surfCpt );
			surfLight	= (uint16*)NvMalloc(sizeof(uint16) * surfCpt );
			
			for (uint i = 0 ; i < surfCpt ; ++i){
				if ( (surf[i].dpystate.GetMode() & DpyState::BM_MASK) == DpyState::BM_MUL ){
					surfLight[lightCpt] = i;
					lightCpt ++;
				}
				else {
					surfDiff[diffCpt] = i;
					diffCpt ++;
				}
			}

	/*		for (uint i = 0 ; i < diffCpt ; ++i)
			{
				uint vtxCptDiff = GetNbVtxcpt(mbunch + surfDiff[i]);
				//INVALID_SURF
				
				for (uint j = i ; j < lightCpt ; ++j)
				{	
					uint vtxCptLight = GetNbVtxcpt(mbunch + surfLight[j]);				
					if (vtxCptDiff == vtxCptLight){
						if (i != j){
							Swap(surfLight[i],surfLight[j]);
						}
						found = TRUE;
						break;
					}
				}
				if (! found ) {
					new_surfCpt = 0;
					return FALSE;	
				}
			}*/
			
			
			if (lightCpt != diffCpt || new_surfCpt != diffCpt ) 
			{	
				new_surfCpt = 0;
				return FALSE;
			}
			
			for (uint i = 0 ; i < new_surfCpt ; ++i)
			{
				uint vtxCpt0 = GetNbVtxcpt(mbunch + surfDiff[i]);
				uint vtxCpt1 = GetNbVtxcpt(mbunch + surfLight[i]);				

				if (vtxCpt0 != vtxCpt1)
				{
					new_surfCpt = 0;
					return FALSE;
				}
			}
		
			buildDL();	

			return TRUE;
		}


		void
		Release	(		)
		{
		
			if( ShutDpyObject() ) {			
				for( uint i = 0 ; i < surfCpt ; i++ ) {
					surf[i].dpystate.Shut();
				}
				SafeRelease( mesh );
				NvFree(surfDiff);
				NvFree(surfLight);
				for (uint i = 0 ; i < new_surfCpt ; ++i)
					NvFree(multiTexbunch[i].dlistAddr);
				NvFree(multiTexbunch);
				NvEngineDelete( this );
			}
		}


		NvResource*
		GetResource		(					)
		{
			return mesh->GetInterface();			
		}


		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}


		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}


		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				for( uint i = 0 ; i < surfCpt ; i++ ) {			
					surf[i].dpystate.SetEnabled( inEnableFlags );
				}
			}
		}


		uint32
		GetEnabled		(		)
		{
			return enflags;
		}


		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfCpt )
				return FALSE;
			surf[inIndex].hidden = TRUE;
			return TRUE;
		}


		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfCpt )
				return FALSE;
			surf[inIndex].hidden = FALSE;
			return TRUE;
		}


		bool
		HideAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfCpt ; i++ )
				HideSurface( i );
			return TRUE;
		}


		bool
		ShowAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfCpt ; i++ )
				ShowSurface( i );
			return TRUE;
		}

		bool
		LightmapSurface		(	uint	inSurfaceIndex		,
								bool	inEnable			)
		{
			if( inSurfaceIndex >= surfCpt )
				return FALSE;
			
			if (inEnable) 
				surf[inSurfaceIndex].dpystate.SetMode	( DpyState::BM_MUL | DpyState::BS_FRAGMENT ) ;	
			else 
				surf[inSurfaceIndex].dpystate.SetMode	( DpyState::BM_NONE | DpyState::BS_FRAGMENT ) ;	
			
			return TRUE;
		
		}
															

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return ( inIndex<surfCpt ) ? &surf[inIndex].dpystate : NULL;
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );
			
			mesh->UpdateBeforeDraw();
			
			// 2D ?
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;

			// Clipping
			GXClipMode clipping = !mode2D && (enflags&NvShader::EN_CLIPPING) ? GX_CLIP_ENABLE : GX_CLIP_DISABLE ;
			#ifdef TEST_CLIPPING_MODE
			clipping = GX_CLIP_ENABLE;
			#endif
			GXSetClipMode( clipping );

			GXSetChanCtrl( GX_COLOR0,GX_FALSE,GX_SRC_VTX,GX_SRC_VTX,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE );
			GXSetChanCtrl( GX_ALPHA0,GX_FALSE,GX_SRC_VTX,GX_SRC_VTX,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE );
			
			int chainIdx     = dpyctxt.chainHead;
			NV_ASSERT( chainIdx >= 0 );
			
			while( chainIdx >= 0 )
			{
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
				NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()		   + dctxt->lightIdx;
				Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;
				bool 						mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0 ;
				
				DpyManager::SetupDrawing (	DpyRaster(dctxt->raster),dview,dwtr, mode2D );
				
				GXClearVtxDesc();

				GXSetVtxDesc(GX_VA_POS , GX_INDEX16);
				GXSetVtxDesc(GX_VA_TEX0, GX_INDEX16);
				GXSetVtxDesc(GX_VA_TEX1, GX_INDEX16);
				
				GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX3x4, GX_TG_TEX0, GX_IDENTITY);
				GXSetTexCoordGen	(GX_TEXCOORD1, GX_TG_MTX3x4, GX_TG_TEX1, GX_IDENTITY);
				
				int fracLoc = 0;
				if ( mvtx->locFrac >= 0 )
					fracLoc = mvtx->locFrac;
				
		        GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS , GX_POS_XYZ,vtxfmt::GetType(vtxfmt::Component(mvtx->vtxFmt & vtxfmt::CO_LOC_MASK)) , fracLoc );
		        
		        GXCompType 	texType = vtxfmt::GetType(vtxfmt::Component(mvtx->vtxFmt & vtxfmt::CO_TEX_MASK));
		        uint8 		frac 	= (texType==GX_F32)? uint8(0) : uint8(10) ;
			    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, texType , frac);
			    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX1, GX_TEX_ST, texType , frac);
			    
			    GXSetArray( GX_VA_POS,	mvtx->compAddr[0], mvtx->compBStride[0] );
				GXSetArray( GX_VA_TEX0, mvtx->compAddr[1], mvtx->compBStride[1] );
				GXSetArray( GX_VA_TEX1, mvtx->compAddr[1], mvtx->compBStride[1] );
				
				GXInvalidateVtxCache();
			
				for (uint i = 0 ; i < new_surfCpt ; ++i)
				{
					uint displaySurf 	= surfDiff[i];
					uint otherSurf 		= surfLight[i];
					if( surf[displaySurf].hidden )
						continue;

					surf[displaySurf].dpystate.ActivateLightmap(surf[otherSurf].dpystate.src,surf[otherSurf].dpystate.mode);
				
					// Setup drawing
					GXCallDisplayList( multiTexbunch[i].dlistAddr, multiTexbunch[i].dlistBSize );
				}
				chainIdx = dchain->next;
				
			}
			
			vtxfmt::Reset();
			return TRUE;
		}
		
		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			//return FALSE;
			outTriCpt	= 0;
			outLocCpt	= 0;
			outVtxCpt	= 0;
			outPrimCpt	= 0;
			for( uint i = 0 ; i < surfCpt ; i++ ) {
				outTriCpt  += mbunch[i].triCpt * dpyctxt.chainCpt;
				outLocCpt  += mbunch[i].locCpt  * dpyctxt.chainCpt;
				outVtxCpt  += mbunch[i].vtxCpt  * dpyctxt.chainCpt;
				outPrimCpt += mbunch[i].primCpt * dpyctxt.chainCpt;
			}
			return TRUE;
		}
		
	};
};


NvLightmapShader*
nv_lightmapshader_mesh_Create	(	NvMesh*		inMesh		)
{
	if( !inMesh )
		return NULL;

	uint surfCpt = inMesh->GetSurfaceCpt();
	
	if (surfCpt & 0x1)
		return NULL;
	
	NvkMesh * kmesh = inMesh->GetKInterface();
	if ( ! (kmesh->GetVtx()->vtxFmt & vtxfmt::CO_TEX_MASK ) )
		return NULL;
	
	NvkMesh::Bunch*	mbunch = kmesh->GetBunchA();
	
	uint supply  = 32 + sizeof(Shader::Surface)  * surfCpt; // + 32 + dlistBSize;
	Shader* shader = NvEngineNewS( Shader, supply );
	if( !shader )
		return NULL;

	shader->surfCpt = surfCpt;
	shader->surf    =(Shader::Surface*) Round32(shader+1);
		
	if ( ! shader->_Init( inMesh ) )
	{
		shader->Release();
		return NULL;
	}
	
	return shader->GetInterface();
}




