/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef	_NvPoseShaderBase_NGC_H_
#define	_NvPoseShaderBase_NGC_H_


#include <NvPoseShader.h>
#include <Kernel/NGC/NvDpyObject[NGC].h>



//
// BASE implementation


struct NvPoseShaderBase : public NvDpyObject_NGC
{
private:
	friend struct NvPoseShader;
	NvPoseShader	itf;
public:
			NvPoseShader*			GetInterface		(											) { return &itf;  }
	virtual 					~	NvPoseShaderBase	(											) {}
	virtual	void					Release				(											) = 0;
	virtual NvResource*				GetResource			(											) = 0;
	virtual	void					Enable				(	uint32					inEnableFlags	) = 0;
	virtual	void					Disable				(	uint32					inEnableFlags	) = 0;
	virtual	void					SetEnabled			(	uint32					inEnableFlags	) = 0;
	virtual	uint32					GetEnabled			(											) = 0;

	virtual bool					HideSurface			(	uint					inSurfaceIndex	) = 0;
	virtual bool					ShowSurface			(	uint					inSurfaceIndex	) = 0;
	virtual bool					HideAllSurfaces		(											) = 0;
	virtual bool					ShowAllSurfaces		(											) = 0;
	virtual DpyState*				GetDisplayState		(	uint					inIndex			) = 0;
	virtual	uint					GetDrawStride		(											) { return 0;		}
	virtual	bool					SetDrawStart		(	uint					inOffset		) { return FALSE;	}
	virtual	bool					SetDrawSize			(	uint					inSize			) { return FALSE;	}

	virtual	void					SetFading			(	float					inInnerD,
															float					inOuterD		) = 0;
	virtual	void					SetMode				(	NvPoseShader::Mode		inMode			) = 0;
	virtual	uint					GetPoseComponents	(											) = 0;
	virtual	uint					GetListSize			(											) = 0;
	virtual	void					SetListDrawSize		(	uint					inSize			) = 0;
	virtual	bool					CreateAccess		(											) = 0;
	virtual	pvoid					GetAccess			(	NvPoseShader::Component	inComponent,
															uint&					outStride,
															Psm*					outPSM			) = 0;
	virtual	void					ValidateAccess		(	uint					inComponents,
															uint					inSizeFrom0		) = 0;
	virtual	void					ReleaseAccess		(											) = 0;
};



#endif // _NvPoseShaderBase_NGC_H_



