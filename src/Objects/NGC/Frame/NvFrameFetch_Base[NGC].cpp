/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/NvDpyObject[NGC].h>
#include <Kernel/NGC/NvkImage[NGC].h>
#include <Kernel/Common/NvkCore_Mem.h>

#include <NvFrameFetch.h>
using namespace nv;


//
// BASE
namespace 
{

	void* FrameAlloc ( uint inBSize )
	{
	#ifdef _RVL
		void * ret = nv::mem::NativeMalloc( inBSize, 32 , nv::mem::MEM_NGC_MEM2 );
		return ret;
	#else
		return NvMallocA( inBSize , 32);
	#endif
	}

	void FrameFree ( void* inPtr )
	{
	#ifdef _RVL
		return nv::mem::NativeFree( inPtr, nv::mem::MEM_NGC_MEM2 );
	#else
		return NvFree( inPtr );
	#endif
	}

}

interface NvFrameFetchBase : public NvDpyObject_NGC
{
	NvFrameFetch		itf;
	
	virtual	~NvFrameFetchBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)

	void 		* 	frameData;
	NvkImage 	* 	kImg;
	uint 		  	width;
	uint			height;
	
	NvFrameFetch*
	Init	(	uint inW ,uint inH	)
	{
		InitDpyObject();
		
		uint 	allocSize 	= GXGetTexBufferSize(inW , inH, GX_TF_RGBA8 ,GX_FALSE,0);		
		frameData	  		= FrameAlloc( allocSize );
		kImg 				= NvkImage::Create(GX_TF_RGBA8,inW,inH,frameData);
		if (!kImg){
			FrameFree(frameData);
			ShutDpyObject();
			NvEngineDelete( this );
			return NULL;
		}
		kImg->SetAutoFreePixels(FALSE);
		width = inW;
		height= inH;
		
		return &itf;
	}


	void
	Release	(		)
	{
		if ( kImg->GetRefCpt() > 1 || kImg->IsDrawing() )
		{
			DpyManager::AddToGarbager( this );
		}
		else if( ShutDpyObject() )
		{
			SafeRelease(kImg);
			FrameFree(frameData);
			frameData = NULL;
			NvEngineDelete( this );
		}
	}



	NvBitmap  *	
	GetFrameData	( )
	{
		return kImg->GetBitmap();
	}

	bool
	Draw	(		)
	{
		GXPixModeSync();
		GXFlush();
		GXDrawDone();

		GXSetTexCopySrc	( 0, 0, width , height 					);
		GXSetTexCopyDst	( width , height, GX_TF_RGBA8, 	FALSE 	);
		GXCopyTex		( frameData  , FALSE 					);

		GXPixModeSync();
		GXFlush();
		return TRUE;
	}
};





NvFrameFetch *
NvFrameFetch::Create	( uint inW, uint inH)
{
	NvFrameFetchBase * inst = NvEngineNew( NvFrameFetchBase );
	return inst ? inst->Init(inW,inH) : NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameFetch,	NvInterface*,	GetBase						)
NVITF_CAL0( FrameFetch,	void,			AddRef						)
NVITF_MTH0( FrameFetch,	uint,			GetRefCpt					)
NVITF_CAL0( FrameFetch,	void,			Release						)
NVITF_MTH0( FrameFetch,	NvBitmap  *,	GetFrameData				)

