/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/NvDpyObject[NGC].h>
#include <Kernel/NGC/NvkImage[NGC].h>
#include <Kernel/NGC/NvDpyState[NGC].h>

#include <NvAAShader.h>
using namespace nv;


//
// BASE
static const GXTexMapID TexmapTranslation[] =
{
	GX_TEXMAP0,
    GX_TEXMAP1,
    GX_TEXMAP2,
    GX_TEXMAP3,
};	
	
interface NvAAShaderBase : public NvDpyObject_NGC
{
	NvAAShader			itf;
	NvBitmap *			frames[4];
	GXTexObj 			texObj[4];
	DpyState_NGC		dpystate;
	uint32				enflags;				// enable flags (EN_...)
	
	
	
	virtual	~NvAAShaderBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)
	
	NvAAShader*
	Init	(		)
	{
		InitDpyObject();		
		Memset(frames,0,4*sizeof(NvBitmap*));
		
		enflags    = NvShader::EN_DEFAULT;
		
		// rebuild dpystate capabilities
		uint dpycaps = DpyState_NGC::CF_HAS_TEX0;

		// init dpystate
		dpystate.Init();
		dpystate.SetEnabled( enflags );
		dpystate.SetCapabilities( dpycaps );
		
		return &itf;
	}

	void
	Release	(		)
	{
		if( ShutDpyObject() )
		{
			for (uint i = 0; i < 4 ; ++i)
			{
				SafeRelease (frames[i]);
			}
			NvEngineDelete( this );
		}
	}

	void
	Enable		(	uint32		inEnableFlags	)
	{			
		SetEnabled( GetEnabled() | inEnableFlags );
	}

	void
	Disable		(	uint32		inEnableFlags	)
	{
		SetEnabled( GetEnabled()&(~inEnableFlags) );
	}

	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
		enflags = inEnableFlags;
		dpystate.SetEnabled( inEnableFlags );
	}

	uint32
	GetEnabled		(		)
	{
		return enflags;
	}

	// Basic properties
	DpyState*
	GetDisplayState	(	uint			inIndex		)
	{
		return (inIndex==0) ? &dpystate : NULL;
	}

	bool
	AddFrame (	uint			inId		,
				NvBitmap *		inFrame		)
	{
		if (inId >=4)
			return false;
		
		SafeGetRef(frames[inId],inFrame);
		
		NvkBitmap * kBmp = frames[inId]->GetKInterface();
		
		GXInitTexObj(	&texObj[inId], 
						kBmp->GetTexel(), 
						kBmp->GetWidth(), 
						kBmp->GetHeight(), 
						kBmp->GetPixelFormat(),
             			GX_CLAMP, GX_CLAMP, FALSE );
		GXInitTexObjLOD	(&texObj[inId],GX_NEAR,GX_NEAR,0.0,0.0,0.0,GX_DISABLE,GX_DISABLE,GX_ANISO_1);
			
		return true;
	}
	
	uint GetNbValidFrame()
	{
		uint cpt = 0;
		for (uint i = 0; i < 4 ; ++i)
		{
			if (frames[i])
				cpt ++;
		}
		return cpt;
	}
	
	bool
	Draw	(		)
	{
		NV_ASSERT( refCpt );
		NV_ASSERT( dpyctxt.chainHead >= 0 );
		uint validF = GetNbValidFrame();
		
		if (validF <3)
			return FALSE;

		NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data()	+ dpyctxt.chainHead;
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()		+ dchain->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()			+ dctxt->viewIdx;
		
		GXInvalidateTexAll();

		Matrix wtr;
		MatrixIdentity(&wtr);
		DpyManager::SetupRaster	 (	DpyRaster(dctxt->raster));
		DpyManager::SetupDrawing (	DpyRaster(dctxt->raster),dview,&wtr, TRUE );

		Matrix proj;
		MatrixIdentity( &proj );
	    GXSetProjection( proj.m, GX_ORTHOGRAPHIC );

		Mtx id;
		MTXIdentity( id );
		GXLoadPosMtxImm( id, GX_PNMTX0);
		GXSetCurrentMtx(GX_PNMTX0);

		Vec4& vp = dview->viewport;
		float sw = float( gx::GetWidth()  );
		float sh = float( gx::GetHeight() );
		GXSetViewport( 0, 0, sw, sh, 0, 1 );
	    GXSetScissor ( vp.x, vp.y, vp.z, vp.w );

	   	GXSetColorUpdate	( GX_TRUE );
		GXSetAlphaUpdate	( GX_TRUE );
	    GXSetZMode			( GX_TRUE, GX_ALWAYS, GX_TRUE );
	    GXSetCullMode		( GX_CULL_NONE );
		GXSetBlendMode		( GX_BM_NONE, GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR );
		GXSetClipMode		( GX_CLIP_DISABLE );
		GXSetAlphaCompare	( GX_ALWAYS, 0, GX_AOP_AND, GX_ALWAYS, 0 );
		
		GXSetNumChans		( 0 );
		GXSetNumTexGens		( 1 );
		GXSetNumTevStages 	( validF + 1 );
		
		GXSetTevOrder		( GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0,  GX_COLOR_NULL					);			
		GXSetTevColorOp		( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	);
		GXSetTevColorIn		( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_TEXC				);
		GXSetTevAlphaOp		( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	);
	    GXSetTevAlphaIn		( GX_TEVSTAGE0, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ONE 				);
	    
	    GXSetTevOrder		( GX_TEVSTAGE1, GX_TEXCOORD0, GX_TEXMAP1,  GX_COLOR_NULL					);			
		GXSetTevColorOp		( GX_TEVSTAGE1, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVREG0	);
		GXSetTevColorIn		( GX_TEVSTAGE1, GX_CC_CPREV, GX_CC_TEXC, GX_CC_HALF, GX_CC_ZERO				);
		GXSetTevAlphaOp		( GX_TEVSTAGE1, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVREG0	);
	    GXSetTevAlphaIn		( GX_TEVSTAGE1, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ONE 				);
	    
	    GXSetTevOrder		( GX_TEVSTAGE2, GX_TEXCOORD0, GX_TEXMAP2,  GX_COLOR_NULL					);			
		GXSetTevColorOp		( GX_TEVSTAGE2, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	);
		GXSetTevColorIn		( GX_TEVSTAGE2, GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_TEXC				);
		GXSetTevAlphaOp		( GX_TEVSTAGE2, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	);
	    GXSetTevAlphaIn		( GX_TEVSTAGE2, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ONE 				);

		if (validF == 4) 
		{
		    GXSetTevOrder		( GX_TEVSTAGE3, GX_TEXCOORD0, GX_TEXMAP3,  GX_COLOR_NULL					);			
			GXSetTevColorOp		( GX_TEVSTAGE3, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVREG1	);
			GXSetTevColorIn		( GX_TEVSTAGE3, GX_CC_CPREV, GX_CC_TEXC, GX_CC_HALF, GX_CC_ZERO				);
			GXSetTevAlphaOp		( GX_TEVSTAGE3, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVREG1	);
		    GXSetTevAlphaIn		( GX_TEVSTAGE3, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ONE 				);
		    
		    GXSetTevOrder		( GX_TEVSTAGE4, GX_TEXCOORD_NULL, GX_TEX_DISABLE,  GX_COLOR_ZERO			);			
			GXSetTevColorOp		( GX_TEVSTAGE4, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	);
			GXSetTevColorIn		( GX_TEVSTAGE4, GX_CC_C0, GX_CC_C1, GX_CC_HALF, GX_CC_ZERO					);
			GXSetTevAlphaOp		( GX_TEVSTAGE4, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	);
		    GXSetTevAlphaIn		( GX_TEVSTAGE4, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ONE 				);
		}
		else // == 3
		{
			GXColor colReg1;
			colReg1.r = colReg1.g = colReg1.b = colReg1.a = 85;
		 	GXSetTevColor		( GX_TEVREG1 , colReg1 );
			GXSetTevOrder		( GX_TEVSTAGE3, GX_TEXCOORD_NULL, GX_TEX_DISABLE,  GX_COLOR_ZERO			);			
			GXSetTevColorOp		( GX_TEVSTAGE3, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	);
			GXSetTevColorIn		( GX_TEVSTAGE3, GX_CC_C0, GX_CC_CPREV, GX_CC_C1, GX_CC_ZERO					);
			GXSetTevAlphaOp		( GX_TEVSTAGE3, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	);
		    GXSetTevAlphaIn		( GX_TEVSTAGE3, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ONE 				);
		}
		
		//texObj
		for (uint i = 0; i < validF ; ++i)
		{
			NvkBitmap * 	kBmp = frames[i]->GetKInterface();
			void * tt = kBmp->GetTexel();
			DCFlushRange	(kBmp->GetTexel(), 	kBmp->GetWidth()*kBmp->GetHeight()*4 );
			GXLoadTexObj	(&texObj[i], TexmapTranslation[i]);			
		}
		
		GXClearVtxDesc();
		GXSetVtxDesc	( GX_VA_POS,  GX_DIRECT );
		GXSetVtxDesc	( GX_VA_TEX0, GX_DIRECT);
		GXSetVtxAttrFmt	( GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0 );
		GXSetVtxAttrFmt	( GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_F32, 0);

		// normalized clipping space is <[-1,+1],[-1,+1],[-1,+0]>
		float xy = 1.0f;				// fullscreen area
		GXBegin( GX_QUADS, GX_VTXFMT0, 4 );
			GXPosition3f32( -xy, -xy, -0.5f );
			GXTexCoord2f32(0.0f, 1.0f);
			
			GXPosition3f32( -xy, xy	, -0.5f );
			GXTexCoord2f32(0.0f, 0.0f);
			
			GXPosition3f32( xy, xy	, -0.5f );
			GXTexCoord2f32(1.0f, 0.0f);
			
			GXPosition3f32( xy, -xy	, -0.5f );
			GXTexCoord2f32(1.0f, 1.0f);
		GXEnd();

		GXSetClipMode( GX_CLIP_ENABLE );
		vtxfmt::Reset();

		return TRUE;
	}
};





NvAAShader *
NvAAShader::Create	( )
{
	NvAAShaderBase * inst = NvEngineNew( NvAAShaderBase );
	return inst ? inst->Init() : NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( AAShader,	NvInterface*,	GetBase						)
NVITF_CAL0( AAShader,	void,			AddRef						)
NVITF_MTH0( AAShader,	uint,			GetRefCpt					)
NVITF_CAL0( AAShader,	void,			Release						)
NVITF_MTH1( AAShader,	DpyState*,		GetDisplayState,	uint	)
NVITF_CAL1( AAShader,	void,			Enable,				uint32	)
NVITF_CAL1( AAShader,	void,			Disable,			uint32	)
NVITF_CAL1( AAShader,	void,			SetEnabled,			uint32	)
NVITF_MTH0( AAShader,	uint32,			GetEnabled					)
NVITF_MTH2( AAShader,	bool,			AddFrame,			uint ,	NvBitmap *)