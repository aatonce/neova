/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/NvDpyObject[NGC].h>
#include <NvFrameClear.h>
using namespace nv;


//
// BASE

interface NvFrameClearBase : public NvDpyObject_NGC
{
	NvFrameClear		itf;
	GXBool				enDepth;
	GXBool				enColor;
	uint32				clrColor;


	virtual	~NvFrameClearBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)


	NvFrameClear*
	Init	(		)
	{
		InitDpyObject();
		clrColor = 0;
		enDepth  = GX_ENABLE;
		enColor  = GX_ENABLE;
		return &itf;
	}


	void
	Release	(		)
	{
		if( ShutDpyObject() )
			NvEngineDelete( this );
	}


	void
	EnableDepth	(	bool	inOnOff		)
	{
		enDepth = inOnOff ? GX_ENABLE : GX_DISABLE;
	}


	void
	EnableColor	(	bool	inOnOff		)
	{
		enColor = inOnOff ? GX_ENABLE : GX_DISABLE;
	}


	void
	SetColor	(	uint32		inRGBA		)
	{
		clrColor = inRGBA;
	}


	bool
	Draw	(		)
	{
		if( !enDepth && !enColor )
			return TRUE;

		NV_ASSERT( refCpt );
		NV_ASSERT( dpyctxt.chainHead >= 0 );
		NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data()	+ dpyctxt.chainHead;
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()		+ dchain->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()			+ dctxt->viewIdx;

		Vec4& vp = dview->viewport;
		float sw = float( gx::GetWidth()  );
		float sh = float( gx::GetHeight() );

		// valid region ?
		#ifdef _NVCOMP_ENABLE_DBG
		NV_ASSERTC( vp.x >= 0.f, "Invalid viewport !\n" );
		NV_ASSERTC( vp.x <  sw,  "Invalid viewport !\n" );
		NV_ASSERTC( vp.y >= 0.f, "Invalid viewport !\n" );
		NV_ASSERTC( vp.y <  sh,  "Invalid viewport !\n" );
		NV_ASSERTC( vp.z >= 0.f, "Invalid viewport !\n" );
		NV_ASSERTC( (vp.x+vp.z) <= sw, "Invalid viewport !\n" );
		NV_ASSERTC( vp.w >= 0.f, "Invalid viewport !\n" );
		NV_ASSERTC( (vp.y+vp.w) <= sh, "Invalid viewport !\n" );
		#endif

		DpyManager::SetupRaster(DpyRaster(dctxt->raster));

		Matrix proj;
		MatrixIdentity( &proj );
	    GXSetProjection( proj.m, GX_ORTHOGRAPHIC );

		Mtx id;
		MTXIdentity( id );
		GXLoadPosMtxImm( id, GX_PNMTX0);
		GXSetCurrentMtx(GX_PNMTX0);

		GXSetViewport( 0, 0, sw, sh, 0, 1 );
	    GXSetScissor ( vp.x, vp.y, vp.z, vp.w );
	
	   	GXSetColorUpdate( enColor );
		GXSetAlphaUpdate( enColor );
	    GXSetZMode		( enDepth, GX_ALWAYS, GX_TRUE );
    
	    gx::ClearBack(clrColor) ;

		return TRUE;
	}
};





NvFrameClear *
NvFrameClear::Create	(		)
{
	NvFrameClearBase * inst = NvEngineNew( NvFrameClearBase );
	return inst ? inst->Init() : NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameClear,	NvInterface*,	GetBase								)
NVITF_CAL0( FrameClear,	void,			AddRef								)
NVITF_MTH0( FrameClear,	uint,			GetRefCpt							)
NVITF_CAL0( FrameClear,	void,			Release								)
NVITF_CAL1( FrameClear,	void,			EnableColor,		bool			)
NVITF_CAL1( FrameClear,	void,			EnableDepth,		bool			)
NVITF_CAL1( FrameClear,	void,			SetColor,			uint32			)



