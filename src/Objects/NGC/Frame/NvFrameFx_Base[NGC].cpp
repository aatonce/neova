/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvFrameFx.h>
#include <Kernel/NGC/NvDpyObject[NGC].h>
#include <Kernel/NGC/GEK/GEK_gx.h>
#include <Kernel/Common/NvkCore_Mem.h>

using namespace nv;


#define FILTERING1 		GX_LINEAR
#define FILTERING2 		GX_LINEAR//GX_NEAR
#define TEX_FORMAT 		GX_TF_RGBA8
#define TEX_FORMAT_COPY gx::CF_RGBA8

// Can't be 0 !!!
#define ZShift 2

#define SetTevRegColor(reg,R,G,B,A) {	GXColor		_regColor; 			\
        								_regColor.r = R;				\
        								_regColor.g = G;				\
        								_regColor.b = B;				\
        								_regColor.a = A;				\
        								GXSetTevColor(reg, _regColor );	\
        							}
#define SetTevRegColor32(reg,RGBA) 	{	GXColor		_regColor; 					\
        								_regColor.r = (RGBA&0xFF000000) >> 24 ;	\
        								_regColor.g = (RGBA&0x00FF0000) >> 16 ;	\
        								_regColor.b = (RGBA&0x0000FF00) >> 8 ;	\
        								_regColor.a = (RGBA&0x000000FF) >> 0 ;	\
        								GXSetTevColor(reg, _regColor );	\
        							}
namespace {

	struct MipmapGC {
		float	weight;		// in [0,1]
		int		blend;		// in [0,128]
		bool	quality;	// TRUE if best quality is required !
	};

	struct Kernel33 {					// 3x3 convolution kernel
		union {
			struct {
				float	k00, k01, k02;	// (-1,-1) ( 0,-1) ( 1,-1)
				float	k10, k11, k12;	// (-1, 0) ( 0, 0) ( 1, 0)
				float	k20, k21, k22;	// (-1, 1) ( 0, 1) ( 1, 1)
			};
			float		k[3][3];		// [y][x]
		};
		void	Reset						(									);
		void	Normalize					(									);
		void	RotateCW					(									);
		void	RotateCCW					(									);
	};

	void	
	Kernel33::Reset	()
	{
		k00 = k01 = k02 = 0;
		k10 = k11 = k12 = 0;
		k20 = k21 = k22 = 0;	
	}

	void	
	Kernel33::Normalize	()
	{
		float s  = k00 + k01 + k02;
			  s += k10 + k11 + k12;
			  s += k20 + k21 + k22;
		if( s > 0.0f ) {
			float oos = 1.0f / s;
			k00 *= oos;		k01 *= oos;		k02 *= oos;
			k10 *= oos;		k11 *= oos;		k12 *= oos;
			k20 *= oos;		k21 *= oos;		k22 *= oos;
		}
	}

	void	
	Kernel33::RotateCW	()
	{
		float _k;
		_k = k00; k00 = k20; k20 = k22; k22 = k02; k02 = _k;
		_k = k01; k01 = k10; k10 = k21; k21 = k12; k12 = _k;
	}

	void	
	Kernel33::RotateCCW	()
	{
		float _k;
		_k = k00; k00 = k02; k02 = k22; k22 = k20; k20 = _k;
		_k = k01; k01 = k12; k12 = k21; k21 = k10; k10 = _k;
	}
	
	int Compute_MipmapedGaussConvol	(	MipmapGC*	outMipGC/*[16]*/,
										int			inSizeX,
										int			inSizeY,
										float		inRadius,
										float		inQuality		)
	{
		//
		// Computes Gaussian normalized sequence

		float gWeights[128];
		float gSigma = GaussianDeviation( inRadius );
		int   gWidth = GaussianWidth( gSigma );
		if( gWidth < 2 || gWidth > 128 )
			return 0;
		GaussianNormSequence( gWeights, gSigma, TRUE );

		//
		// N mipmaps, from (inSizeX,inSizeY) -> (8,8)

		int mip_cpt = 0;
		int minSize = Min( inSizeX, inSizeY );
		while( (minSize>>(mip_cpt+1)) >= 8 )
			mip_cpt++;
		if( mip_cpt == 0 || mip_cpt >= 16 )
			return 0;

		//
		// Computes mipmaps weight

		float sum_mipw[16];
			  sum_mipw[ mip_cpt+1 ] = 0.0f;

		float cur_avg = 0.0f;
		for( int mip = mip_cpt ; mip >= 0 ; mip-- ) {
			MipmapGC* gc = & outMipGC[mip];

			// average mipmap weights
			int   w1  = Min( 1 << mip,    gWidth );
			int   w0  = Min( (1<<mip)>>1, gWidth );
			float s   = 0.0f;
			for( int wi = w0 ; wi < w1 ; wi++ )
				s += gWeights[wi];
			float avg  = (w1>w0) ? s / float( w1-w0 ) : 0.0f;

			// mipmap factor to reach the average from the current average
			float w			= 1.0f / float( 1<<mip );
			float mipw		= (avg - cur_avg) / w;
			NV_ASSERT( mipw >= 0.f );
			NV_ASSERT( mipw <= 1.f );
			mipw			= Clamp( mipw, 0.f, 1.f );
			cur_avg			= avg;
			sum_mipw[mip]	= sum_mipw[ mip+1 ] + mipw;
			gc->weight      = mipw;
			// Smooth mipmap for better quality, based on mipmap ratio 1/2^N
			gc->quality    = ( inQuality >= w );

		//	Printf( "mip%d [%d,%d[ avg=%f  w=%f quality=%c\n", mip, w0, w1, avg, mipw, gc->quality?'Y':'N' );
		}

		//		
		// Computes recursive blending alpha factors (a,b,c,...z),
		// such that :
		// mip(i-1) = mip(i  ) * a + mip(i-1) * (1-a)
		// mip(i-2) = mip(i-1) * b + mip(i-2) * (1-b)
		// ...
		// mip(0)   = mip(1)   * z + mip(0)   * (1-z)
		// fromBAddr += mip(0)
		//
		// a = wa      / (wa+wb)
		// b = (wa+wb) / (wa+wb+wc)
		// ...

		outMipGC[0].blend = int( sum_mipw[0] * 128.0f );	// ~1.0
		for( int mip = mip_cpt ; mip > 0 ; mip-- ) {
			float sw0   = sum_mipw[ mip   ];
			float sw1   = sum_mipw[ mip-1 ];
			float blend = ( sw0 && sw1 ) ? sw0 / sw1 : 0.0f;
			int alpha   = int( blend * 128.0f );
			if( alpha == 0 )	mip_cpt = mip - 1;
			else				outMipGC[mip].blend = alpha;
		//	Printf( "mip%d mip_blend=%d\n", mip, mip_blend[mip] );
		}
		//	Printf( "mip%d mip_blend=%d\n", 0, mip_blend[0] );
		NV_ASSERT( mip_cpt > 0 );

		return mip_cpt;
	}
};

interface NvFrameFxBase : public NvDpyObject_NGC
{
	struct MipTex{
		GXTexObj texObj;
		uint8 *  ptr;
	}; 
	
	NvFrameFx			itf;

	uint8 * 			zPict;
	uint8 * 			zPict8;
	uint8 * 			backPict;	
	uint8 * 			backPictMip;
	uint8 * 			tlutDataH;		// Highest bits
	uint8 * 			tlutDataM;		// Middle  bits
	uint8 * 			tlutDofCustom;		
	uint8 * 			tlutFogCustom;
	
	GXTexObj 			zTexH;
	GXTexObj 			zTexM;
	GXTexObj 			zTex8;
	GXTexObj 			backTex;
	GXTlutObj			tlutH;
	GXTlutObj			tlutM;
	GXTlutObj			tlutDofC;
	GXTlutObj			tlutFogC;
	MipTex 				backTexMip [16];
			
	uint				enableFx;
	float 				blurRadius;
	uint32				bloomRGBA;
	float				bloomRadius;
	uint32				fogRGBA;
	nv::mem::MemId		memoryBank;
	
	NvFrameFxBase				(	)	{}
	virtual ~NvFrameFxBase		(	)	{}
	
	void * AllocImage(uint bsize)
	{
		return nv::mem::NativeMalloc( bsize, 32, memoryBank );
	}
	
	void	FreeImage(void * ptr)
	{
		nv::mem::NativeFree(ptr,memoryBank);
	}
	
	void
	_Init		(		)
	{
		InitDpyObject();
		zPict 		= NULL;
		zPict8 		= NULL;
		backPict	= NULL;
		backPictMip = NULL;
		tlutDataH	= NULL;
		tlutDataM	= NULL;
		tlutDofCustom= NULL;
		tlutFogCustom= NULL;
		enableFx 	= 0;
		blurRadius	= 10.0f;
		bloomRGBA	= 0x90909090;
		fogRGBA		= 0xC0C0C0C0;
		bloomRadius	= 8.0f;
		memoryBank = nv::mem::MEM_MAIN;
#ifdef _RVL
		bool	useMem2 = TRUE;
		nv::core::GetParameter(core::PR_NGC_FX_MEM2,&useMem2);
		if (useMem2)
			memoryBank = nv::mem::MEM_NGC_MEM2;
		else 
			memoryBank = nv::mem::MEM_MAIN;
#endif 
	}

	void FreeBuffers()
	{
		if (zPict){
			FreeImage(zPict);
			zPict = NULL;
		}
		if (zPict8){
			FreeImage(zPict8);
			zPict8 = NULL;
		}
		if (backPict){
			FreeImage(backPict);
			backPict = NULL;
		}
		if (backPictMip){
			FreeImage(backPictMip);
			backPictMip = NULL;
		}
		//SafeFree(zPict);
		//SafeFree(zPict8);
		//SafeFree(backPict);
		//SafeFree(backPictMip);
		if (tlutDataH) {
			//NvFree(tlutDataH);
			FreeImage(tlutDataH);
			tlutDataH 	= NULL;
			tlutDataM 	= NULL;
			tlutDofCustom 	= NULL;
			tlutFogCustom 	= NULL;
		}
		enableFx 	= 0;
	}
	
	void
	Release	(		)
	{
		if( ShutDpyObject() ) {
			FreeBuffers();
			NvEngineDelete( this );
		}
	}

	NvkFrameFx*
	GetKInterface	(			)
	{
		return NULL;
	}

	NvFrameFx*
	GetInterface		(		)
	{
		return &itf;
	}
	
	bool
	SetDFogDepth		(	uint8			inDepthIdx,
							uint8			inDepth		)
	{
		tlutFogCustom[inDepthIdx*2] 	= inDepth;
		tlutFogCustom[inDepthIdx*2+1] 	= 0xFF;
		SyncDCache(&tlutFogCustom[inDepthIdx*2],32);
		return TRUE;
	}

	bool					
	SetDFogColor		(	uint32			inRGBA		)
	{
		fogRGBA = inRGBA;
		return TRUE;
	}
											
	bool
	SetDOFSharpness		(	uint8			inDepthIdx	,
							uint8			inSharpness	)
	{
		if (!tlutDofCustom) 
			return FALSE;
		
		tlutDofCustom[inDepthIdx*2] 	= 255;
		tlutDofCustom[inDepthIdx*2+1] 	= inSharpness;
		
		SyncDCache(&tlutDofCustom[inDepthIdx*2],32);
		return TRUE;
	}
							
	bool
	SetBlurRadius		(	float			inRadius	)
	{
		if (inRadius > 0.0f) {
			blurRadius = inRadius;
			return TRUE;
		}
		return FALSE;
	}

	bool
	SetBloom			(	uint32			inRGBA,
							float			inRadius	)
	{
		if (inRadius <= 0.2f)
			return FALSE;
			
		bloomRGBA 	= inRGBA;
		bloomRadius = inRadius;
		
		return TRUE;
	}

	void 
	MakeDefaultClut()
	{
		for (int i = 0 ; i < 256 ; ++i){
			uint8 partH =  uint8(i) >> ZShift;
			uint8 partM = (uint8(i) << (8-ZShift)) ;
			
			for (int k = 0 ; k <= GetBitRangeMask(0,ZShift-1) ; ++k ) {
				int indH = partH | (k << (8-ZShift) ); 
				tlutDataH[indH*2]   = 255; // A
				tlutDataH[indH*2+1] = partH << ZShift;
			}
			
			for (int k = 0 ; k <= GetBitRangeMask(0,7-ZShift) ; ++k ) {
				int indM = partM | k; 
				tlutDataM[indM*2]   = 255; // A
				tlutDataM[indM*2+1] = partM >> (8-ZShift);
			}
			
			tlutDofCustom[i*2]   = 255	; // A
			tlutDofCustom[i*2+1] = 255-i;
			tlutFogCustom[i*2] 	 = uint8(Max(int((i-160)*1.5f),0));
			tlutFogCustom[i*2+1] = 255	;
		}
		SyncDCache(tlutDataH,256*2*3 + 256*4);
	}

	bool	GenerateFx	(	uint	inEN	)
	{
		// sync current FrameFX's gx cmds before change buffers & co
		gx::Sync();

		bool doFog		= inEN & (1<<NvFrameFx::FX_DFOG);
		bool doDof		= inEN & (1<<NvFrameFx::FX_DOF);
		bool doBlur		= inEN & (1<<NvFrameFx::FX_BLUR);
		bool doBloom	= inEN & (1<<NvFrameFx::FX_BLOOM);
		bool doOutline	= inEN & (1<<NvFrameFx::FX_OUTLINE);
		
		uint efbW = gx::GetWidth	();
		uint efbH = gx::GetHeight	();

		if (inEN && !backPict) {
			uint size = GXGetTexBufferSize(efbW, efbH, GX_TF_RGBA8, GX_FALSE, 0);
			//backPict = (uint8*)NvMalloc(size);
			backPict = (uint8*)AllocImage(size);
			if (!backPict){
				DebugPrintf("Warning : An allocation error leads to FrameFx::GenerateFx error\n");
				FreeBuffers();
				return FALSE;
			}
		}
		
		if ( doFog || doDof ) {
			if ( !zPict ) {
    			uint size = GXGetTexBufferSize(efbW, efbH, GX_TF_Z8, GX_FALSE, 0) * 2 + 32;
				//zPict = (uint8*)NvMalloc(size);
				zPict = (uint8*)AllocImage(size);
			}
			if ( !zPict8 ) {
    			uint size = GXGetTexBufferSize(efbW, efbH, GX_CTF_R8, GX_FALSE, 0) * 2 + 32;
				//zPict8 = (uint8*)NvMalloc(size);
				zPict8 = (uint8*)AllocImage(size);
			}
			if (!tlutDataH) {
				//tlutDataH = (uint8*)NvMalloc(256 * 2 * 4);
				tlutDataH = (uint8*)AllocImage(256 * 2 * 4);
				tlutDataM = tlutDataH + (256*2);
				tlutDofCustom= tlutDataM + (256*2);
				tlutFogCustom= tlutDofCustom + (256*2);
				
				if (tlutDataH) {
					MakeDefaultClut();
					GXInitTlutObj(&tlutH,tlutDataH ,GX_TL_IA8,256);
					GXInitTlutObj(&tlutM,tlutDataM ,GX_TL_IA8,256);
					GXInitTlutObj(&tlutDofC,tlutDofCustom,GX_TL_IA8,256);
					GXInitTlutObj(&tlutFogC,tlutFogCustom,GX_TL_IA8,256);
				}
			}
			
			if ( !zPict || !zPict8 || !tlutDataH ) {
				DebugPrintf("Warning : An allocation error leads to FrameFx::GenerateFx error\n");
				FreeBuffers();
				return FALSE;
			}
		}
		else {
			if ( zPict ) {
				//NvFree(zPict);
				FreeImage(zPict);
				zPict = NULL;
			}
			if ( zPict8 ) {
				//NvFree(zPict8);
				FreeImage(zPict8);
				zPict8 = NULL;
			}
			if (tlutDataH) {
				//NvFree(tlutDataH);
				FreeImage(tlutDataH);
				tlutDataH = NULL;
				tlutDataM = NULL;
				tlutDofCustom= NULL;
				tlutFogCustom= NULL;
			}
		}
		
		if (doBloom || doDof || doBlur) {
			if (!backPictMip) {
				uint size = GXGetTexBufferSize(efbW, efbH, GX_TF_RGBA8, GX_FALSE, 0) * 2.2f; //*1.2 for security
				//backPictMip = (uint8*)NvMalloc(size);
				backPictMip = (uint8*)AllocImage(size);
			}
			if ( !backPictMip ) {
				DebugPrintf("Warning : An allocation error leads to FrameFx::GenerateFx error\n");
				FreeBuffers();
				return FALSE;
			}
		}
		else {
			if (backPictMip) {
				//NvFree(backPictMip);
				FreeImage(backPictMip);
				backPictMip = NULL;
			}
		}
		
		enableFx = inEN;
		
		return TRUE;
	}

	
	void DrawTexQuad(int inSizeX, int inSizeY, int inZ = 0)
	{
		GXSetNumChans		( 0 );
	    if (!inZ)
	    	GXSetZMode		(GX_FALSE,GX_ALWAYS ,GX_FALSE );
		GXSetAlphaCompare	(GX_ALWAYS,0U,GX_AOP_AND,GX_ALWAYS,0U);
	    
	    // set vertex descriptor
	    GXClearVtxDesc();
	    GXSetVtxDesc(GX_VA_POS,  GX_DIRECT);
	    GXSetVtxDesc(GX_VA_TEX0, GX_DIRECT);
		GXSetVtxAttrFmt( GX_VTXFMT0, GX_VA_POS ,GX_POS_XYZ, GX_S16,0);
		GXSetVtxAttrFmt( GX_VTXFMT0, GX_VA_TEX0,	GX_TEX_ST, GX_S8,0);	
	
	    GXBegin(GX_QUADS, GX_VTXFMT0, 4);
	    	uint16 z = uint16(inZ);
	        GXPosition3s16( 0, inSizeY	, z );
	        GXTexCoord2s8 ( 0, 1 );
	        GXPosition3s16( inSizeX  , inSizeY	, z );
	        GXTexCoord2s8 ( 1, 1);
	        GXPosition3s16( inSizeX  , 0, z );
	        GXTexCoord2s8 ( 1, 0 );
	        GXPosition3s16( 0, 0, z );
	        GXTexCoord2s8 ( 0, 0 );
	    GXEnd();
	}
	
	
	void DrawColQuad(int sizeX, int sizeY, uint8 inR , uint8 inG , uint8 inB, int inZ = 0)
	{
		GXSetNumChans		( 1 );
		GXSetChanCtrl		( GX_COLOR0, GX_DISABLE, GX_SRC_VTX, GX_SRC_VTX, GX_LIGHT0, GX_DF_NONE , GX_AF_NONE );
	    
	    if (!inZ)
	    	GXSetZMode		(GX_FALSE,GX_ALWAYS ,GX_FALSE );
		GXSetAlphaCompare	(GX_ALWAYS,0U,GX_AOP_AND,GX_ALWAYS,0U);
	    
	    // set vertex descriptor
	    GXClearVtxDesc();
	    GXSetVtxDesc(GX_VA_POS,  GX_DIRECT);
	    GXSetVtxDesc(GX_VA_CLR0, GX_DIRECT);
		GXSetVtxAttrFmt( GX_VTXFMT0, GX_VA_POS ,GX_POS_XYZ, GX_S16 ,0);
		GXSetVtxAttrFmt( GX_VTXFMT0, GX_VA_CLR0,GX_CLR_RGB, GX_RGB8,0);	
	
	    GXBegin(GX_QUADS, GX_VTXFMT0, 4);
	        GXPosition3s16	( 0, sizeY, inZ );
	        GXColor3u8 		( inR, inG, inB );
	        GXPosition3s16	(  sizeX,  sizeY, inZ );
	        GXColor3u8 		( inR, inG, inB );
	        GXPosition3s16	(  sizeX,  0, inZ );
	        GXColor3u8 		( inR, inG, inB );
	        GXPosition3s16	( 0, 0, inZ );
	        GXColor3u8 		( inR, inG, inB );
	    GXEnd();
	}
	
	void FixTevTex() {
	
		GXSetBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
	
		GXSetNumTexGens		(1);
    	GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    	
		GXSetNumTevStages	(1);
    	GXSetTevOp			(GX_TEVSTAGE0, GX_REPLACE);
    	GXSetTevOrder		(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL);
	}
	
	// linear 		Blend : Texmap1 * Factor + Texmap0 * ( 1 - Factor )
	// !(linear)	Blend : Texmap1 * Factor + Texmap0
	void FixTev2TexBlending (bool	linear = TRUE) { 
		GXSetBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
	
		GXSetNumTexGens		(2);
    	GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    	GXSetTexCoordGen	(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    	
    	GXSetNumTevStages(2);
		
    	GXSetTevOrder		(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL);
    	GXSetTevColorIn		(GX_TEVSTAGE0,GX_CC_ZERO,GX_CC_ZERO,GX_CC_ZERO,GX_CC_TEXC );
    	GXSetTevColorOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
    	GXSetTevAlphaIn		(GX_TEVSTAGE0,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_ONE);
        GXSetTevAlphaOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
        
        GXSetTevOrder		(GX_TEVSTAGE1, GX_TEXCOORD1, GX_TEXMAP1, GX_COLOR_NULL);
        
        if (linear)
    		GXSetTevColorIn	(GX_TEVSTAGE1,GX_CC_CPREV,GX_CC_TEXC,GX_CC_A0,GX_CC_ZERO );
    	else 
    		GXSetTevColorIn	(GX_TEVSTAGE1,GX_CC_ZERO ,GX_CC_TEXC,GX_CC_A0,GX_CC_CPREV);

    	GXSetTevColorOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
    	GXSetTevAlphaIn		(GX_TEVSTAGE1,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_ONE);
        GXSetTevAlphaOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
	}
	
	void FixTevFog () { 
		GXSetBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
	
		GXSetNumTexGens		(2);
    	GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    	GXSetTexCoordGen	(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    	
    	GXSetNumTevStages(2);
		
    	GXSetTevOrder		(GX_TEVSTAGE0,GX_TEXCOORD1, GX_TEXMAP1, GX_COLOR_NULL);
    	GXSetTevColorIn		(GX_TEVSTAGE0,GX_CC_ZERO,GX_CC_TEXC,GX_CC_C0,GX_CC_ZERO  );
    	GXSetTevColorOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
    	GXSetTevAlphaIn		(GX_TEVSTAGE0,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_TEXA );
        GXSetTevAlphaOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
        
        GXSetTevOrder		(GX_TEVSTAGE1,GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL);
    	GXSetTevColorIn		(GX_TEVSTAGE1,GX_CC_TEXC,GX_CC_CPREV,GX_CC_APREV,GX_CC_ZERO );
    	GXSetTevColorOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
    	GXSetTevAlphaIn		(GX_TEVSTAGE1,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_TEXA);
        GXSetTevAlphaOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
	}

	void SetBlendFactor (int factor ){
		SetTevRegColor(GX_TEVREG0,0,0,0,factor);
	}
	
	// Blend : Texmap1C * texmap2A + Texmap2C * (1-texmap2A) || //Texmap0(blur) Texmap1(back) Texmap2(ZH) Texmap3(ZM)
	void FixTevDof () {
		GXSetBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
		
		GXSetNumTexGens		(1);
    	GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    	
    	GXSetNumTevStages	(3);
        
		// Z tex 2
        GXSetTevOrder		(GX_TEVSTAGE0,GX_TEXCOORD0, GX_TEXMAP2, GX_COLOR_NULL    );
    	GXSetTevColorIn		(GX_TEVSTAGE0,GX_CC_ZERO,GX_CC_ONE,GX_CC_TEXC,GX_CC_ZERO);
    	GXSetTevColorOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVREG0);
    	GXSetTevAlphaIn		(GX_TEVSTAGE0,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_TEXA);
        GXSetTevAlphaOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVREG0);        
        
        // Back Tex 
        GXSetTevOrder		(GX_TEVSTAGE1,GX_TEXCOORD0, GX_TEXMAP1, GX_COLOR_NULL);
    	GXSetTevColorIn		(GX_TEVSTAGE1,GX_CC_ZERO,GX_CC_ZERO,GX_CC_ZERO,GX_CC_TEXC);
    	GXSetTevColorOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
    	GXSetTevAlphaIn		(GX_TEVSTAGE1,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_TEXA);
        GXSetTevAlphaOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
        
        // Blur Tex and blending
        GXSetTevOrder		(GX_TEVSTAGE2,GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL);
    	GXSetTevColorIn		(GX_TEVSTAGE2,GX_CC_TEXC,GX_CC_CPREV,GX_CC_C0,GX_CC_ZERO);
    	GXSetTevColorOp		(GX_TEVSTAGE2,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
    	GXSetTevAlphaIn		(GX_TEVSTAGE2,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_APREV);
        GXSetTevAlphaOp		(GX_TEVSTAGE2,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
	}
	
	void FixTevSeeZ() {
		GXSetBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
		    
		GXSetNumTexGens		(1);
    	GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    	
    	GXSetNumTevStages	(2);
        
        // Z tex 1
        GXSetTevOrder		(GX_TEVSTAGE0,GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL    );
    	GXSetTevColorIn		(GX_TEVSTAGE0,GX_CC_ZERO,GX_CC_ZERO,GX_CC_ZERO,GX_CC_TEXC );
    	GXSetTevColorOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV);
    	GXSetTevAlphaIn		(GX_TEVSTAGE0,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_TEXA);
        GXSetTevAlphaOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV);
		// Z tex 2
        GXSetTevOrder		(GX_TEVSTAGE1,GX_TEXCOORD0, GX_TEXMAP1, GX_COLOR_NULL    );
    	GXSetTevColorIn		(GX_TEVSTAGE1,GX_CC_ZERO,GX_CC_ONE,GX_CC_TEXC,GX_CC_CPREV);
    	GXSetTevColorOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVREG0);
    	GXSetTevAlphaIn		(GX_TEVSTAGE1,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_TEXA);
        GXSetTevAlphaOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVREG0);   
	}
	
	void FixTevSubstract() 
	{ 	
		GXSetBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
	
		GXSetNumTexGens		(1);
    	GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
		GXSetNumTevStages	(1);
    	 	
	    GXSetTevOrder		(GX_TEVSTAGE0,GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL     );
	    
		GXSetTevColorIn		(GX_TEVSTAGE0,GX_CC_ZERO,GX_CC_C0,GX_CC_ONE,GX_CC_TEXC );
		GXSetTevColorOp		(GX_TEVSTAGE0,GX_TEV_SUB,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV);
		GXSetTevAlphaIn		(GX_TEVSTAGE0,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_TEXA);
	    GXSetTevAlphaOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV);
	}


	void DrawMip(MipTex * inMips , uint inCpt) {
	
		inMips ++; // pass first mip (it's the original back picture)
		
		uint offsetx 	= 0;
		uint efbW 		= gx::GetWidth	();
		uint efbH 		= gx::GetHeight	();	
		for (uint i = 0 ; i <= inCpt ; ++i ) {
			Matrix wtr;
			MatrixTranslation	(&wtr,offsetx,0.0f,0.0f);
			MatrixTranspose		(&wtr,&wtr);
			GXLoadPosMtxImm		(wtr.m, GX_PNMTX0);
			GXSetCurrentMtx		( GX_PNMTX0 );
			
			uint sizex = efbW >> (i + 1);
			uint sizey = efbH >> (i + 1);
			offsetx += sizex;
			 
			GXLoadTexObj	( &inMips->texObj, GX_TEXMAP0 );
			FixTevTex();
			DrawTexQuad(sizex,sizey);
		}
	}
	
	void DoBlurMap(float inRadius) {
	
		uint efbW = gx::GetWidth	();
		uint efbH = gx::GetHeight	();		

		NV_ASSERT(backPictMip);

		MipmapGC mipGC[16];
		int mipCpt = Compute_MipmapedGaussConvol(mipGC,efbW,efbH,inRadius,1.0);
		GXTexObj  * curTexObj;

		uint offset = 0;
		uint bsize = GXGetTexBufferSize(efbW, efbH, GX_TF_RGBA8, GX_FALSE, 0) ;
		
		backTexMip[0].ptr   = backPictMip;		
		//gx::CopyTex(backTexMip[0].ptr,TEX_FORMAT_COPY);
		offset += bsize;
    	offset = Round32(offset);
    	
    	GXSetPixelFmt( GX_PF_RGB8_Z24, GX_ZC_LINEAR );
    	
    	// Init mip 0 (with Back texture copy)
		GXInitTexObj	(&backTexMip[0].texObj, backTexMip[0].ptr , efbW , efbH , TEX_FORMAT, GX_CLAMP, GX_CLAMP, GX_FALSE );
		GXInitTexObjLOD	(&backTexMip[0].texObj, FILTERING1, FILTERING1, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
		GXInitTexObjLOD	(&backTex, FILTERING1, FILTERING1, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );

		FixTevTex();
		for( int mip = 1 ; mip <= mipCpt ; mip++ ) {
			if (mip == 1)
				GXLoadTexObj	( &backTex, GX_TEXMAP0 );
			else
				GXLoadTexObj	( &backTexMip[mip-1].texObj, GX_TEXMAP0 );
			
			uint sizeX = efbW >> mip;
			uint sizeY = efbH >> mip;
			
			DrawTexQuad		( sizeX , sizeY);
			
			// Copy this mip to memory .
			
			backTexMip[mip].ptr = backPictMip + offset;
			
			gx::CopyTex		(backTexMip[mip].ptr ,TEX_FORMAT_COPY,sizeX,sizeY);
			
			GXInitTexObj	( &backTexMip[mip].texObj, backTexMip[mip].ptr , sizeX , sizeY , TEX_FORMAT, GX_CLAMP, GX_CLAMP, GX_FALSE );
    		GXInitTexObjLOD	( &backTexMip[mip].texObj, FILTERING1, FILTERING1, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );

			offset += GXGetTexBufferSize(sizeX, sizeY, GX_TF_RGBA8, GX_FALSE, 0) ;
    		offset = Round32(offset);
		}
		
	//	DrawMip(backTexMip,mipCpt);

		FixTev2TexBlending();
		for( int mip = mipCpt-1 ; mip >= 0 ; mip-- ) {
			int coeff = mipGC[mip].blend ;

			uint sizeX = efbW >> mip;
			uint sizeY = efbH >> mip;
			
			if (mip == 0){
				GXSetPixelFmt( GX_PF_RGBA6_Z24, GX_ZC_LINEAR );			
			}
		
			// copy mip to efb
			GXInvalidateTexAll();
			
			GXInitTexObjLOD	( &backTexMip[mip].texObj, FILTERING2, FILTERING2, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
			GXInitTexObjLOD	( &backTexMip[mip+1].texObj, FILTERING1, FILTERING1, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
			
			GXLoadTexObj	( &backTexMip[mip].texObj	, GX_TEXMAP0 );
			GXLoadTexObj	( &backTexMip[mip+1].texObj	, GX_TEXMAP1 );
			
			SetBlendFactor(Min( int(coeff *  (255.0f/128.0f)) , int(255)));
			DrawTexQuad		( sizeX , sizeY );

			GXTexModeSync();
			GXFlush();
			gx::CopyTex		(backTexMip[mip].ptr ,TEX_FORMAT_COPY ,sizeX,sizeY);
			
			GXTexModeSync();
			GXPixModeSync();
			GXInvalidateTexAll();
			GXFlush();
		}
	}
	
	void DoGlowMap(float inRadius) {
	
		Kernel33 diffK;
		diffK.k00 = diffK.k02 = diffK.k20 = diffK.k22 = 0.7f;
		diffK.k01 = diffK.k21 = diffK.k10 = diffK.k12 = 1.0f;
		diffK.k11 = 0.0f;
		diffK.Normalize();
	
		uint efbW = gx::GetWidth	();
		uint efbH = gx::GetHeight	();		

		NV_ASSERT(backPictMip);

		MipmapGC mipGC[16];
		int mipCpt = Compute_MipmapedGaussConvol(mipGC,efbW,efbH,inRadius,1.0);
		GXTexObj  * curTexObj;

		uint offset = 0;
		uint bsize = GXGetTexBufferSize(efbW, efbH, GX_TF_RGBA8, GX_FALSE, 0) ;
		
		// Init mip 0 (with Back texture copy)
		backTexMip[0].ptr   = backPictMip;		
		//gx::CopyTex(backTexMip[0].ptr,TEX_FORMAT_COPY);
		GXInitTexObj	(&backTexMip[0].texObj, backTexMip[0].ptr , efbW , efbH , TEX_FORMAT, GX_CLAMP, GX_CLAMP, GX_FALSE );
		GXInitTexObjLOD	(&backTexMip[0].texObj, FILTERING1, FILTERING1, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
		GXInitTexObjLOD	(&backTex, FILTERING1, FILTERING1, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
		
		offset += bsize;
    	offset = Round32(offset);
    	
    	GXSetPixelFmt( GX_PF_RGB8_Z24, GX_ZC_LINEAR );

		for( int mip = 1 ; mip <= mipCpt ; mip++ ) {
			GXLoadTexObj	( &backTexMip[mip-1].texObj, GX_TEXMAP0 );
			
			uint sizeX = efbW >> mip;
			uint sizeY = efbH >> mip;
			FixTevTex();
			DrawTexQuad		( sizeX , sizeY);
			
			// Copy this mip to memory .
			backTexMip[mip].ptr = backPictMip + offset;
			
			gx::CopyTex		(backTexMip[mip].ptr ,TEX_FORMAT_COPY,sizeX,sizeY);
    		
    		// Do expansion

    		int   kidxA[9]   = { 0*4+0, 0*4+2, 2*4+0, 2*4+2, 1*4+0,	1*4+2,	0*4+1,	2*4+1, 1*4+1 };
			const float d256 = 1.0f / 256.0f;
			float round_err = 0.0f;							// Cumul round alpha error to preserve the global brightness
			for( int kidx = 0 ; kidx < 8 ; kidx++ ) {
				int   ki = kidxA[ kidx ] & 3;
				int   kj = kidxA[ kidx ] >> 2;
				float k  = diffK.k[ kj ][ ki ];
				
				float fa = ( k + round_err ) * 256.0f;
				int   ia = int( fa );
				round_err = ( fa - ia ) * d256;
				if( ia > 0 ) {					
			
		    		GXTexObj 		tmpTex1;
					GXInitTexObj	( &tmpTex1, backTexMip[mip].ptr , sizeX , sizeY , TEX_FORMAT, GX_CLAMP, GX_CLAMP, GX_FALSE );
		    		GXInitTexObjLOD	( &tmpTex1, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
		    		GXLoadTexObj	( &tmpTex1, GX_TEXMAP0 );
		    		
		    		GXSetBlendMode	( GX_BM_BLEND,GX_BL_SRCALPHA, GX_BL_ONE, GX_LO_CLEAR);
		    		
					GXSetNumTexGens		(2);
					GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
					GXSetTexCoordGen	(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX0, GX_TEXMTX0);
					
					Matrix msTr;
					MatrixTranslation	(&msTr,(1.0f-ki)/float(sizeX),(1.0f-kj)/float(sizeY),0.0f);
					MatrixTranspose		(&msTr,&msTr);
					GXLoadTexMtxImm		(msTr.m, GX_TEXMTX0 , GX_MTX3x4 );
					
					SetTevRegColor		(GX_TEVREG0,0,0,0,ia);
					
					GXSetNumTevStages	(2);
				    GXSetTevOrder		(GX_TEVSTAGE0,GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL    );
					GXSetTevColorIn		(GX_TEVSTAGE0,GX_CC_ZERO,GX_CC_ZERO,GX_CC_ZERO,GX_CC_TEXC);
					GXSetTevColorOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV);
					GXSetTevAlphaIn		(GX_TEVSTAGE0,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_ONE);
				    GXSetTevAlphaOp		(GX_TEVSTAGE0,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV);
				    
				    GXSetTevOrder		(GX_TEVSTAGE1,GX_TEXCOORD1	,GX_TEXMAP0, GX_COLOR_NULL);
					GXSetTevColorIn		(GX_TEVSTAGE1,GX_CC_ZERO	,GX_CC_CPREV,GX_CC_ONE, GX_CC_TEXC);
					GXSetTevColorOp		(GX_TEVSTAGE1,GX_TEV_SUB	,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
					GXSetTevAlphaIn		(GX_TEVSTAGE1,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO, GX_CA_A0);
				    GXSetTevAlphaOp		(GX_TEVSTAGE1,GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV );
				    
				   	DrawTexQuad		( sizeX , sizeY);
				}
			}
	
			gx::CopyTex		(backTexMip[mip].ptr ,TEX_FORMAT_COPY,sizeX,sizeY);

			GXInitTexObj	( &backTexMip[mip].texObj, backTexMip[mip].ptr , sizeX , sizeY , TEX_FORMAT, GX_CLAMP, GX_CLAMP, GX_FALSE );
    		GXInitTexObjLOD	( &backTexMip[mip].texObj, FILTERING1, FILTERING1, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
    		offset += GXGetTexBufferSize(sizeX, sizeY, GX_TF_RGBA8, GX_FALSE, 0) ;
    		offset = Round32(offset);
		}
		
//		DrawMip(backTexMip,mipCpt-1);

		FixTev2TexBlending();
		for( int mip = mipCpt-1 ; mip >= 0 ; mip-- ) {
			int coeff = mipGC[mip].blend ;

			uint sizeX = efbW >> mip;
			uint sizeY = efbH >> mip;
			
			if (mip == 0){
				GXSetPixelFmt( GX_PF_RGBA6_Z24, GX_ZC_LINEAR );			
			}
		
			// copy mip to efb
			GXInvalidateTexAll();
			
			GXInitTexObjLOD	( &backTexMip[mip].texObj, FILTERING2, FILTERING2, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
			GXInitTexObjLOD	( &backTexMip[mip+1].texObj, FILTERING1, FILTERING1, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
			
			GXLoadTexObj	( &backTexMip[mip].texObj	, GX_TEXMAP0 );
			GXLoadTexObj	( &backTexMip[mip+1].texObj	, GX_TEXMAP1 );
			
			SetBlendFactor(Min( int(coeff *  (255.0f/128.0f)) , int(255)));
			DrawTexQuad		( sizeX , sizeY );

			GXTexModeSync();
			GXFlush();
			gx::CopyTex		(backTexMip[mip].ptr ,TEX_FORMAT_COPY ,sizeX,sizeY);
			
			GXTexModeSync();
			GXPixModeSync();
			GXInvalidateTexAll();
			GXFlush();
		}
	}
	
	void SetZProjMatrix(float w, float h) {
		Matrix proj2D;
		MatrixZero(&proj2D);
		// write transposed 2D projection Matrix
		proj2D.m11 =	2.0f  / w;
		proj2D.m22 =	-2.0f / h ;
		proj2D.m14 =	-1.0f ;
		proj2D.m24 =	1.0f  ;
		proj2D.m33 =	1.0f / 65535.0f ;
		proj2D.m34 =	0.0;
		proj2D.m44 =	1.0f  ;
		GXSetProjection(proj2D.m, GX_ORTHOGRAPHIC);	
	}


	bool
	Draw	(		)
	{
		bool doFog		= enableFx & (1<<NvFrameFx::FX_DFOG);
		bool doDof		= enableFx & (1<<NvFrameFx::FX_DOF);
		bool doBlur		= enableFx & (1<<NvFrameFx::FX_BLUR);
		bool doBloom	= enableFx & (1<<NvFrameFx::FX_BLOOM);
		bool doOutline	= enableFx & (1<<NvFrameFx::FX_OUTLINE);

		uint efbW = gx::GetWidth	();
		uint efbH = gx::GetHeight	();

		if (!doFog && !doDof && !doBlur && !doBloom ) return TRUE;
////////////////////////////////////////////////////////////////////////////////////////////////////////
//						Get Z		
////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (doDof || doFog) {
			NV_ASSERT(zPict);
			uint zbsize = GXGetTexBufferSize(efbW, efbH, GX_TF_Z8, GX_FALSE, 0) ;
			uint zoffset = Round32(zbsize);
			
			gx::CopyTex(zPict,gx::CF_Z8);
			GXInitTexObjCI	( &zTexH, zPict , efbW , efbH , GX_TF_C8, GX_CLAMP, GX_CLAMP, GX_FALSE, GX_TLUT0 );
	    	GXInitTexObjLOD	( &zTexH, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
	    	
			gx::CopyTex(zPict + zoffset,gx::CF_Z8M);
			GXInitTexObjCI	( &zTexM, zPict + zoffset , efbW , efbH , GX_TF_C8, GX_CLAMP, GX_CLAMP, GX_FALSE, GX_TLUT1 );
	    	GXInitTexObjLOD	( &zTexM, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////
//						Get Back
////////////////////////////////////////////////////////////////////////////////////////////////////////
		NV_ASSERT(backPict);
		gx::CopyTex(backPict,TEX_FORMAT_COPY);
		GXInitTexObj	( &backTex, backPict , efbW , efbH , GX_TF_RGBA8, GX_CLAMP, GX_CLAMP, GX_FALSE );
    	GXInitTexObjLOD	( &backTex, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );

////////////////////////////////////////////////////////////////////////////////////////////////////////
//						Fix Drawing matrix/parameters
////////////////////////////////////////////////////////////////////////////////////////////////////////
		int chainIdx     = dpyctxt.chainHead;
		if (!chainIdx) return FALSE;
		NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
		Matrix						dwtr   ;
		MatrixIdentity (&dwtr);
				
		DpyManager::SetupDrawing ( DpyRaster(dctxt->raster), dview, &dwtr, TRUE);
			
////////////////////////////////////////////////////////////////////////////////////////////////////////
//						Do dof
////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (doFog || doDof) {	// Transform Z24 to Z8 (with Z shift)
			NV_ASSERT(zPict && zPict8 && tlutDataH && tlutDataM);
			int quadZ = GetBitRangeMask(16-ZShift,15);
	
			SetZProjMatrix(dview->viewport.z, dview->viewport.w);
			
			GXSetBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
		    
			GXSetNumTexGens	 (0  ); // no texture
			GXSetNumTevStages( 1 );
			
    		GXSetTevOrder	(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR0A0);
    		GXSetTevOp		(GX_TEVSTAGE0, GX_PASSCLR);
    		GXSetZMode		(GX_TRUE,GX_GREATER,GX_FALSE );
    		DrawColQuad		(efbW,efbH,0,0,0,quadZ); // Color 0 => Custom clut idx 0
    		
			GXLoadTlut(&tlutH,GX_TLUT0);
			GXLoadTlut(&tlutM,GX_TLUT1);
			GXLoadTexObj(&zTexH, GX_TEXMAP0);
			GXLoadTexObj(&zTexM, GX_TEXMAP1);
			GXSetPixelFmt( GX_PF_RGB8_Z24, GX_ZC_LINEAR );
			FixTevSeeZ();
			GXSetZMode		(GX_TRUE,GX_LEQUAL,GX_FALSE );
			DrawTexQuad(efbW,efbH,quadZ);

			DpyManager::SetupDrawing ( DpyRaster(dctxt->raster), dview, &dwtr, TRUE);		
			gx::CopyTex(zPict8,gx::CF_R8);
			GXInitTexObjCI	( &zTex8, zPict8 , efbW , efbH , GX_TF_C8, GX_CLAMP, GX_CLAMP, GX_FALSE, GX_TLUT0 );
	    	GXInitTexObjLOD	( &zTex8, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
	  	 	GXSetPixelFmt( GX_PF_RGBA6_Z24, GX_ZC_LINEAR );
		}
		
		if (doFog) {
			GXLoadTlut		(&tlutFogC	, GX_TLUT0);
			GXLoadTexObj	(&backTex	, GX_TEXMAP0);
			GXLoadTexObj	(&zTex8		, GX_TEXMAP1);
			SetTevRegColor32(GX_TEVREG0 , fogRGBA );
			FixTevFog ();
			DrawTexQuad(efbW,efbH);
		}
		
		if (doDof) {
			if (doFog) {
				gx::CopyTex		(backPict,TEX_FORMAT_COPY);
				GXInitTexObj	( &backTex, backPict , efbW , efbH , GX_TF_RGBA8, GX_CLAMP, GX_CLAMP, GX_FALSE );
		    	GXInitTexObjLOD	( &backTex, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
			}
			NV_ASSERT(backPictMip);
			
			DoBlurMap(5.0f);

			//Texmap0(blur) Texmap1(back) texmap2(ZH) texmap3(ZM)    		
			GXLoadTlut	(&tlutDofC, GX_TLUT0);
			GXLoadTexObj(&backTexMip[0].texObj, GX_TEXMAP0);
			GXLoadTexObj(&backTex, GX_TEXMAP1);
			GXLoadTexObj(&zTex8, GX_TEXMAP2);
			FixTevDof();
			DrawTexQuad(efbW,efbH);
		}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//						Do Bloom
////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if (doBloom) {
			if (doFog || doDof) {
				//Get Back
				gx::CopyTex(backPict,TEX_FORMAT_COPY);
				GXInitTexObj	( &backTex, backPict , efbW , efbH , GX_TF_RGBA8, GX_CLAMP, GX_CLAMP, GX_FALSE );
		    	GXInitTexObjLOD	( &backTex, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
			}
			
   			GXLoadTexObj		(&backTex, GX_TEXMAP0);
   			FixTevSubstract		(	);
			SetTevRegColor32	(GX_TEVREG0,bloomRGBA);		    										
			DrawTexQuad			(efbW,efbH);
			gx::CopyTex			(backPictMip,TEX_FORMAT_COPY);
   			DoGlowMap			(bloomRadius);
   			GXInitTexObjLOD		( &backTex, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
   			GXInitTexObjLOD		( &backTexMip[0].texObj, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
	   		GXLoadTexObj		( &backTex				, GX_TEXMAP0);
   			GXLoadTexObj		(&backTexMip[0].texObj	, GX_TEXMAP1);

			FixTev2TexBlending 	(FALSE);
			SetBlendFactor 		(255);
			DrawTexQuad			(efbW,efbH);
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////
//						Do Blur		
////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (doBlur) {
			if (doFog || doDof || doBlur) {
				//Get Back
				gx::CopyTex(backPict,TEX_FORMAT_COPY);
				GXInitTexObj	( &backTex, backPict , efbW , efbH , GX_TF_RGBA8, GX_CLAMP, GX_CLAMP, GX_FALSE );
		    	GXInitTexObjLOD	( &backTex, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
			}
			
			NV_ASSERT(backPictMip);
			DoBlurMap(blurRadius);
			GXLoadTexObj(&backTexMip[0].texObj, GX_TEXMAP0);
			FixTevTex();
			DrawTexQuad(efbW,efbH);
		}
		
		vtxfmt::Reset();

		return TRUE;
	}
};


NvFrameFx *
NvFrameFx::Create	(		)
{
	NvFrameFxBase * inst = NvEngineNew( NvFrameFxBase );
	if( !inst )
		return NULL;
	inst->_Init();
	return &inst->itf;
}

//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameFx,	NvInterface*,	GetBase													)
NVITF_CAL0( FrameFx,	void,			AddRef													)
NVITF_MTH0( FrameFx,	uint,			GetRefCpt												)
NVITF_CAL0( FrameFx,	void,			Release													)
NVITF_MTH0( FrameFx,	NvkFrameFx*,	GetKInterface											)
NVITF_MTH1( FrameFx,	bool,			GenerateFx,			uint								)
NVITF_MTH1( FrameFx,	bool, 			SetDFogColor,		uint32								)
NVITF_MTH2( FrameFx,	bool, 			SetDFogDepth,		uint8,	uint8						)
NVITF_MTH2( FrameFx,	bool, 			SetDOFSharpness,	uint8,	uint8						)
NVITF_MTH1( FrameFx,	bool, 			SetBlurRadius,		float								)
NVITF_MTH2( FrameFx,	bool, 			SetBloom,			uint32,	float						)


