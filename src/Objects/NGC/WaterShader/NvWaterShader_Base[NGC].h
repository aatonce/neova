/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef	_NvWaterShaderBase_NGC_H_
#define	_NvWaterShaderBase_NGC_H_


#include <NvWaterShader.h>
#include <Kernel/NGC/NvDpyObject[NGC].h>



//
// BASE implementation


struct NvWaterShaderBase : public NvDpyObject_NGC
{
private:
	friend struct NvWaterShader;
	NvWaterShader	itf;
public:
			NvWaterShader*			GetInterface		(											) { return &itf;  }
	virtual 					~	NvWaterShaderBase	(											) {}
	virtual	void					Release				(											) = 0;
	virtual NvResource*				GetResource			(											) = 0;
	virtual	void					Enable				(	uint32					inEnableFlags	) = 0;
	virtual	void					Disable				(	uint32					inEnableFlags	) = 0;
	virtual	void					SetEnabled			(	uint32					inEnableFlags	) = 0;
	virtual	uint32					GetEnabled			(											) = 0;

	virtual DpyState*				GetDisplayState		(	uint					inIndex			) = 0;

	virtual bool					SetMap				(	NvWaterShader::MapID	inId			,
															NvBitmap * 				inMap			) = 0;
												
	virtual bool					SetMatrix			(	NvWaterShader::MapID	inId			,
															Matrix * 				inMatrix		) = 0;
												
	virtual void					SetSize				(	float					inWidth			,
															float					inHeight		) = 0;
															
	virtual void					SetTextureArea		(	float					inWidth			,
															float					inHeight		) = 0;

};



#endif // _NvWaterShaderBase_NGC_H_



