/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
#include "NvWaterShader_Base[NGC].h"
#include <Kernel/NGC/NvkBitmap[NGC].h>
#include <Kernel/NGC/NvkMesh[NGC].h>
#include <Kernel/NGC/NvDpyState[NGC].h>
using namespace nv;


//#define	TEST_CLIPPING_MODE
//#define	DISABLE_TEXTURING
//#define	DISABLE_LIGHTING

namespace
{

	static f32 identityMtx[2][3] = {{1.0f, 0.0f, 0.0f, },  // Indirect "identity" mtx
                         	   	{ 0.0f, 1.0f, 0.0f, }}; // (not used)
                         	   
	interface Shader : public NvWaterShaderBase
	{

		struct Surface {
			DpyState_NGC		dpystate;
		};
		
		uint32					enflags;			// enable flags (EN_...)
		Surface					surf;
		
		float					width;
		float					height;
		float					textureMappingSizeX;
		float					textureMappingSizeY;
		float					warpAngle;
		
		Matrix					texDiffMatrix;
		Matrix					texBump1Matrix;
		Matrix					texBump2Matrix;
		Matrix					texReflMatrix;
		
		NvkBitmap	* 			kBump1Map;
		NvkBitmap	* 			kBump2Map;
		NvkBitmap	* 			kReflectionMap;
		NvkBitmap	* 			kDiffMap;
		GXTexObj 				reflectionMapObj;
		GXTexObj 				bump1MapObj;
		GXTexObj 				bump2MapObj;
		GXTexObj 				diffMapObj;
		
		virtual	~	Shader	(	) {}				// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init		(		)
		{
			InitDpyObject();
			
			enflags    = NvShader::EN_DEFAULT;
			#ifdef DISABLE_TEXTURING
			enflags   ^= NvShader::EN_TEXTURING;
			#endif

			// Init capabilities
			uint dpycaps = 0;
			dpycaps |= DpyState_NGC::CF_HAS_NORMAL;
			dpycaps |= DpyState_NGC::CF_HAS_TEX0;
			dpycaps |= DpyState_NGC::CF_HAS_RGBA;

			// Init dpystate
			ConstructInPlace( &surf.dpystate );
			surf.dpystate.Init();
			surf.dpystate.SetCapabilities	( dpycaps );
			surf.dpystate.SetEnabled			( enflags );
			surf.dpystate.SetMode			( DpyState::TF_DECAL 			|
											  DpyState::TM_NEAREST			|
											  DpyState::TW_REPEAT_REPEAT 	|
											  DpyState::CM_TWOSIDED		 	|
											  DpyState::BM_NONE 			);
											  
			surf.dpystate.SetAlphaPass		( 0.0f );
			surf.dpystate.SetBlendSrcCte	( 1.0f );
			surf.dpystate.SetDefaultColor	( 0xFFFFFFFF );
			
			width 				= 1.0f;
			height 				= 1.0f;
			textureMappingSizeX = 1.0F;
			textureMappingSizeY = 1.0F;
			warpAngle			= 0.0f;
			
			kBump1Map 			= NULL;
			kBump2Map 			= NULL;
			kDiffMap 			= NULL;
			kReflectionMap 		= NULL;
			
			MatrixIdentity(&texDiffMatrix);
			MatrixIdentity(&texBump1Matrix);
			MatrixIdentity(&texBump2Matrix);
			MatrixIdentity(&texReflMatrix);
		}


		void
		Release	(		)
		{
			if( ShutDpyObject() ) {			
				surf.dpystate.Shut();
				SafeRelease( kBump1Map  );
				SafeRelease( kBump2Map  );
				SafeRelease( kReflectionMap );
				SafeRelease( kDiffMap );
				NvEngineDelete( this );
			}
		}


		NvResource*
		GetResource		(					)
		{
			return NULL;		
		}


		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}


		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}


		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				surf.dpystate.SetEnabled( inEnableFlags );
			}
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}
		

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return ( inIndex<1 ) ? &surf.dpystate : NULL;
		}


		bool
		SetMap	(	NvWaterShader::MapID	inId,  NvBitmap *	inMap	)
		{
			if (! inMap ) return FALSE;
			
			switch (inId)
			{
				case NvWaterShader::MI_BUMP1 :
					SafeGetRef(kBump1Map,inMap->GetKInterface());
					GXInitTexObj(	&bump1MapObj, 
							kBump1Map->GetTexel(), 
							kBump1Map->GetWidth(), 
							kBump1Map->GetHeight(), 
							kBump1Map->GetPixelFormat(),
                 			GX_REPEAT, GX_REPEAT, FALSE );	
					GXInitTexObjLOD(&bump1MapObj,GX_LINEAR,GX_LINEAR,0.0,0.0,0.0,GX_DISABLE,GX_DISABLE,GX_ANISO_1);
					break;
					
				case NvWaterShader::MI_BUMP2 : 
					SafeGetRef(kBump2Map,inMap->GetKInterface());
					GXInitTexObj(	&bump2MapObj, 
							kBump2Map->GetTexel(), 
							kBump2Map->GetWidth(), 
							kBump2Map->GetHeight(), 
							kBump2Map->GetPixelFormat(),
                 			GX_REPEAT, GX_REPEAT, FALSE );
					GXInitTexObjLOD(&bump2MapObj,GX_LINEAR,GX_LINEAR,0.0,0.0,0.0,GX_DISABLE,GX_DISABLE,GX_ANISO_1);
					break;
					
				case NvWaterShader::MI_DIFFUSE : 
					SafeGetRef(kDiffMap,inMap->GetKInterface());
					GXInitTexObj(	&diffMapObj, 
							kDiffMap->GetTexel(), 
							kDiffMap->GetWidth(), 
							kDiffMap->GetHeight(), 
							kDiffMap->GetPixelFormat(),
                 			GX_REPEAT, GX_REPEAT, FALSE );
					GXInitTexObjLOD(&diffMapObj,GX_LINEAR,GX_LINEAR,0.0,0.0,0.0,GX_DISABLE,GX_DISABLE,GX_ANISO_1);
					break;
					
				case NvWaterShader::MI_REFLECTION :
					SafeGetRef(kReflectionMap,inMap->GetKInterface());
					GXInitTexObj(	&reflectionMapObj, 
						kReflectionMap->GetTexel(), 
						kReflectionMap->GetWidth(), 
						kReflectionMap->GetHeight(), 
						kReflectionMap->GetPixelFormat(),
             			GX_CLAMP, GX_CLAMP, FALSE );
					GXInitTexObjLOD(&reflectionMapObj,GX_LINEAR,GX_LINEAR,0.0,0.0,0.0,GX_DISABLE,GX_DISABLE,GX_ANISO_1);
					break;
					
				default:
					return FALSE;
			}
			return TRUE;
		}
												
		bool
		SetMatrix	(	NvWaterShader::MapID	inId , Matrix *	inMatrix	)
		{
			if (! inMatrix ) return FALSE;
			
			switch (inId)
			{
				case NvWaterShader::MI_BUMP1 :
					MatrixCopy(&texBump1Matrix,inMatrix);
					break;
				case NvWaterShader::MI_BUMP2 : 
					MatrixCopy(&texBump2Matrix,inMatrix);
					break;
				case NvWaterShader::MI_DIFFUSE : 
					MatrixCopy(&texDiffMatrix,inMatrix);
					break;
				case NvWaterShader::MI_REFLECTION :
					MatrixIdentity(&texReflMatrix);
					texReflMatrix.m11 = inMatrix->m11;
					texReflMatrix.m12 = inMatrix->m12;
					texReflMatrix.m21 = inMatrix->m21;
					texReflMatrix.m22 = inMatrix->m22;
				default:
					return FALSE;
			}
			return TRUE;
		}
												
		void
		SetSize		(	float	inWidth	,float	inHeight	)
		{
			if (inWidth <=0)	inWidth  = 1.0f;
			if (inHeight <=0)	inHeight = 1.0f;
			
			width 		= inWidth;
			height 		= inHeight;
		}
		
		void
		SetTextureArea		(	float	inWidth	,float	inHeight	)
		{
			if (inWidth <=0)	inWidth  = 1.0f;
			if (inHeight <=0)	inHeight = 1.0f;
			
			textureMappingSizeX = inWidth;
			textureMappingSizeY = inHeight;
		}
		
		void TexturingSetup()
		{
            // Load object into hw. Diffuse Color map will be TEXMAP0.
    		GXLoadTexObj(&diffMapObj, GX_TEXMAP0);
            // Load object into hw. Lightmap will be TEXMAP1.
    		GXLoadTexObj(&reflectionMapObj, GX_TEXMAP1);
            // Load object into hw. Bumpmap1 will be TEXMAP2.
    		GXLoadTexObj(&bump1MapObj, GX_TEXMAP2);
            // Load object into hw. Bumpmap2 will be TEXMAP3.
    		GXLoadTexObj(&bump2MapObj, GX_TEXMAP3);
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );
			
			if (!kReflectionMap || ! kBump1Map || !kBump2Map || !kDiffMap)
				return FALSE;
				
			// 2D ?
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;

			// Clipping
			GXClipMode clipping = !mode2D && (enflags&NvShader::EN_CLIPPING) ? GX_CLIP_ENABLE : GX_CLIP_DISABLE ;
			#ifdef TEST_CLIPPING_MODE
			clipping = GX_CLIP_ENABLE;
			#endif
			GXSetClipMode( clipping );

			int chainIdx     = dpyctxt.chainHead;
			int lastLightIdx = -1;
			NV_ASSERT( chainIdx >= 0 );
			
			// Set vertex format			
			GXClearVtxDesc();

			GXSetVtxDesc(GX_VA_POS, GX_DIRECT);
    		GXSetVtxDesc(GX_VA_NBT, GX_DIRECT);
    		GXSetVtxDesc(GX_VA_TEX0, GX_DIRECT);
    
	        GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
    		GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_NBT, GX_NRM_NBT, GX_F32, 0);
		    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_F32, 0);

			while( chainIdx >= 0 )
			{
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
				NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()		   + dctxt->lightIdx;
				Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;
				
				DpyManager::SetupDrawing (	DpyRaster(dctxt->raster),dview,dwtr, mode2D );
		
				chainIdx = dchain->next;
		
				surf.dpystate.Activate();
				
//	********************************************************************************
// 	Initialize states
//
				TexturingSetup();

				GXSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0		, GX_TEXMTX0	);
				GXSetTexCoordGen(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX0		, GX_TEXMTX1	);
				GXSetTexCoordGen(GX_TEXCOORD2, GX_TG_MTX2x4, GX_TG_BINRM	, GX_TEXMTX2	);
				GXSetTexCoordGen(GX_TEXCOORD3, GX_TG_MTX2x4, GX_TG_TANGENT	, GX_TEXMTX2	);
				GXSetTexCoordGen(GX_TEXCOORD4, GX_TG_MTX2x4, GX_TG_BINRM	, GX_TEXMTX2	);
				GXSetTexCoordGen(GX_TEXCOORD5, GX_TG_MTX2x4, GX_TG_TANGENT	, GX_TEXMTX2	);
				GXSetTexCoordGen(GX_TEXCOORD6, GX_TG_MTX2x4, GX_TG_NRM		, GX_TEXMTX3	);
				GXSetTexCoordGen(GX_TEXCOORD7, GX_TG_MTX2x4, GX_TG_TEX0		, GX_TEXMTX4	);// For Diffuse texture
				
				GXSetNumTexGens		(8);
    			GXSetNumChans		(0);
    			GXSetNumTevStages	(6);
    			GXSetNumIndStages	(2);
    			
				// Indirect Stage 0 -- Sample normal perturbation map
			    GXSetIndTexOrder(GX_INDTEXSTAGE0, GX_TEXCOORD0, GX_TEXMAP2);
    			GXSetIndTexCoordScale(GX_INDTEXSTAGE0, GX_ITS_1, GX_ITS_1);
    			
    			// Indirect Stage 1 -- Sample normal perturbation map
			    GXSetIndTexOrder(GX_INDTEXSTAGE1, GX_TEXCOORD1, GX_TEXMAP3);
    			GXSetIndTexCoordScale(GX_INDTEXSTAGE1, GX_ITS_1, GX_ITS_1);

				// Stage 0 -- Save material texture
    			//
    			// TEVPREV = TEXC/TEXA
    			//
    			GXSetTevDirect	(GX_TEVSTAGE0	);
    			GXSetTevOrder	(GX_TEVSTAGE0, GX_TEXCOORD7, GX_TEXMAP0, GX_COLOR_NULL	);
    			GXSetTevOp		(GX_TEVSTAGE0, GX_REPLACE	);

    			// Stage 1,2,3,4,5 Indirect.
    			//

				GXSetTevIndirect(	GX_TEVSTAGE1,	GX_INDTEXSTAGE0,	GX_ITF_8,     	
									GX_ITB_ST,    	GX_ITM_S0,    		GX_ITW_0,     	
									GX_ITW_0,     	FALSE,       		FALSE,       	
									GX_ITBA_OFF   									);
				
				GXSetTevIndirect(	GX_TEVSTAGE2,	GX_INDTEXSTAGE0,	GX_ITF_8,     
									GX_ITB_ST,    	GX_ITM_T0,    		GX_ITW_0,     
									GX_ITW_0,     	TRUE,         		FALSE,        
									GX_ITBA_OFF   									);
				
				GXSetTevIndirect(	GX_TEVSTAGE3, 	GX_INDTEXSTAGE1,	GX_ITF_8,     
									GX_ITB_ST,    	GX_ITM_S0,    		GX_ITW_0,     
									GX_ITW_0,     	TRUE,         		FALSE,        
									GX_ITBA_OFF   									);
				
				GXSetTevIndirect(	GX_TEVSTAGE4, 	GX_INDTEXSTAGE1,	GX_ITF_8,     
									GX_ITB_ST,    	GX_ITM_T0,    		GX_ITW_0,     
									GX_ITW_0,     	TRUE,         		FALSE,        
									GX_ITBA_OFF   									);
				
				GXSetTevIndirect(	GX_TEVSTAGE5, 	GX_INDTEXSTAGE1, 	GX_ITF_8,     
									GX_ITB_NONE,  	GX_ITM_OFF,   		GX_ITW_OFF,   
									GX_ITW_OFF,   	TRUE,         		FALSE,        
									GX_ITBA_OFF   									);
    			
			    // Stage 1 -- Accumulate binormal perturbation component in Bump
			    //
			    // TEVPREV = PREV
			    //
			    GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD2,
			                  (GXTexMapID)(GX_TEXMAP1|GX_TEX_DISABLE), GX_COLOR_NULL);
			    GXSetTevOp(GX_TEVSTAGE1, GX_PASSCLR);

			    // Stage 2 -- Accumulate tangent perturbation component in Bump
			    //
			    // TEVPREV = PREV
			    //
			    GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD3,
			                  (GXTexMapID)(GX_TEXMAP1|GX_TEX_DISABLE), GX_COLOR_NULL);
			    GXSetTevOp(GX_TEVSTAGE2, GX_PASSCLR);
			  
			  
			    // Stage 3 -- Accumulate binormal perturbation component in Bump
			    //
			    // TEVPREV = PREV
			    //
			    GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD4,
			                  (GXTexMapID)(GX_TEXMAP1|GX_TEX_DISABLE), GX_COLOR_NULL);
			    GXSetTevOp(GX_TEVSTAGE3, GX_PASSCLR);

				// Stage 4 -- Accumulate tangent perturbation component in Bump
			    //
			    // TEVPREV = PREV
			    //
			    GXSetTevOrder(GX_TEVSTAGE4, GX_TEXCOORD5,
			                  (GXTexMapID)(GX_TEXMAP1|GX_TEX_DISABLE), GX_COLOR_NULL);
			    GXSetTevOp(GX_TEVSTAGE4, GX_PASSCLR);


			    // Stage 5 -- Add source normal in Bump. Index lightmap with result of
			    //            perturbation. Apply diffuse and specular components.
			    //
			    // TEVPREVC = PREVC * TEXC 
			    // TEVPREVA = PREVA
			    //
				GXSetTevOrder  (GX_TEVSTAGE5, GX_TEXCOORD6, GX_TEXMAP1, GX_COLOR_NULL);
			    
			    GXSetTevColorIn(GX_TEVSTAGE5,
			                    GX_CC_ZERO, GX_CC_TEXC , GX_CC_CPREV, GX_CC_ZERO);
			                    
			    GXSetTevAlphaIn(GX_TEVSTAGE5,
			                    GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_TEXA);
			                    
			    GXSetTevColorOp(GX_TEVSTAGE5,
			                    GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, TRUE, GX_TEVPREV);
			                    
			    GXSetTevAlphaOp(GX_TEVSTAGE5,
			                    GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, TRUE, GX_TEVPREV);
			                
			                    
//	********************************************************************************
// 	Setup matrix for texGen 
//
				Matrix transM,scaleM,rotWarpM;
				MatrixTranslation(&transM,0.5f,0.5f,0.0f);
				MatrixScaling(&scaleM,0.5f,0.5f,0.5f);
				Vec3 rotAxis(0.0f,0.0f,1.0f);
				MatrixRotation(&rotWarpM,&rotAxis,warpAngle);
                  
				Matrix nrmTexM,binTexM;
				
				// nrmTexM
				nrmTexM = texReflMatrix * scaleM * transM ;
				
				// binTexM
				binTexM = texReflMatrix * scaleM  ;

				MatrixTranspose (&nrmTexM,&nrmTexM);
				MatrixTranspose (&binTexM,&binTexM);
	    		
	    		Matrix scaleTexMatrix;
	    		
	    		float scaleU = width / textureMappingSizeX;
	    		float scaleV = height / textureMappingSizeY;
	    		MatrixScaling(&scaleTexMatrix, scaleU , scaleV , 1.0f);
	    		
	    		Matrix texBump1MatrixScaled,texBump2MatrixScaled,texDiffMatrixScaled;
	    		
	    		texBump1MatrixScaled 	= scaleTexMatrix * texBump1Matrix;
	    		texBump2MatrixScaled 	= scaleTexMatrix * texBump2Matrix;
	    		texDiffMatrixScaled 	= scaleTexMatrix * texDiffMatrix;
	    		
	    		MatrixTranspose(&texBump1MatrixScaled,&texBump1MatrixScaled);
	    		MatrixTranspose(&texBump2MatrixScaled,&texBump2MatrixScaled);
	    		MatrixTranspose(&texDiffMatrixScaled ,&texDiffMatrixScaled);
	    		
				GXLoadTexMtxImm(binTexM.m		, GX_TEXMTX2 	, GX_MTX2x4	);
			    GXLoadTexMtxImm(nrmTexM.m		, GX_TEXMTX3 	, GX_MTX2x4	);
				GXLoadTexMtxImm(texBump1MatrixScaled.m, GX_TEXMTX0	, GX_MTX2x4	);
				GXLoadTexMtxImm(texBump2MatrixScaled.m, GX_TEXMTX1	, GX_MTX2x4	);
				GXLoadTexMtxImm(texDiffMatrixScaled.m	, GX_TEXMTX4	, GX_MTX2x4	);	// For diffuse texture
				
				// Set indirect matrix and scale to be "identity" for 64x64 textures.
	    		GXSetIndTexMtx(GX_ITM_0, identityMtx, 0);
//	********************************************************************************
//	Compute reflexion, tangent, binormal vector
//
				Matrix vtxM,nrmM;
				
				// vtxM
				vtxM = (*dwtr) * dview->viewTR;
				
				nrmM = vtxM;
				nrmM.m41 = 0.0f;
				nrmM.m42 = 0.0f;
				nrmM.m43 = 0.0f;
				nrmM.m44 = 1.0f;
				
				Vec3 n(0.0f,0.0f,1.0f);
				Vec3 t(1.0f, 0.0f, 0.0f);
				Vec3 b(0.0f, 1.0f, 0.0f);
			
				Vec3 p1( 0, height, 0.0f	);
				Vec3 p2( width, height, 0.0f	);
				Vec3 p3( width, 0, 0.0f	);
				Vec3 p4( 0, 0, 0.0f	);
	
	
				Vec3 nc,tc,bc;
				Vec3 p1c,p2c,p3c,p4c;
				
				nc = n * nrmM;
				tc = t * nrmM;
				bc = b * nrmM;
				

				p1c = p1 * vtxM;
				p2c = p2 * vtxM;
				p3c = p3 * vtxM;
				p4c = p4 * vtxM;
				
				Vec3 i1n,i2n,i3n,i4n; 				// normalized incidence vector;
				i1n = -p1c;
				i2n = -p2c;
				i3n = -p3c;
				i4n = -p4c;
				Vec3Normalize (&i1n,&i1n) ;
				Vec3Normalize (&i2n,&i2n) ;
				Vec3Normalize (&i3n,&i3n) ;
				Vec3Normalize (&i4n,&i4n) ;
				
				float alpha1,alpha2,alpha3,alpha4;
				
				alpha1 = Vec3Dot(&i1n,&nc);
				alpha2 = Vec3Dot(&i2n,&nc);
				alpha3 = Vec3Dot(&i3n,&nc);
				alpha4 = Vec3Dot(&i4n,&nc);
				
				Vec3 vp1,vp2,vp3,vp4; 				// vectorial product of Nc and ixn
				Vec3Cross (&vp1,&i1n,&nc);
				Vec3Cross (&vp2,&i2n,&nc);
				Vec3Cross (&vp3,&i3n,&nc);
				Vec3Cross (&vp4,&i4n,&nc);
				
				
				Matrix r1M,r2M,r3M,r4M;				// matrice de rotation
				MatrixRotation(&r1M,&vp1,Acos(alpha1)*0.5f);
				MatrixRotation(&r2M,&vp2,Acos(alpha2)*0.5f);
				MatrixRotation(&r3M,&vp3,Acos(alpha3)*0.5f);
				MatrixRotation(&r4M,&vp4,Acos(alpha4)*0.5f);
				
				Vec3 r1,r2,r3,r4; 		// Reflexion vector
				Vec3 tr1,tr2,tr3,tr4; 	// Tangent for reflexion vector
				Vec3 br1,br2,br3,br4; 	// Binormal for reflexion vector
				
				r1  = i1n * r1M;
				tr1 = tc * r1M;
				
				r2  = i2n * r2M;
				tr2 = tc * r2M;
				
				r3  = i3n * r3M;
				tr3 = tc * r3M;
				
				r4  = i4n * r4M;
				tr4 = tc * r4M;

				Vec3Normalize( &r1, &r1 );
				Vec3Normalize( &r2, &r2 );
				Vec3Normalize( &r3, &r3 );
				Vec3Normalize( &r4, &r4 );

				Vec3Cross( &br1, &tr1, &r1 );
				Vec3Cross( &br2, &tr2, &r2 );
				Vec3Cross( &br3, &tr3, &r3 );
				Vec3Cross( &br4, &tr4, &r4 );

				Vec3Normalize( &br1, &br1 );
				Vec3Normalize( &br2, &br2 );
				Vec3Normalize( &br3, &br3 );
				Vec3Normalize( &br4, &br4 );

//	********************************************************************************
// 	Draw quad
//
				GXBegin( GX_QUADS, GX_VTXFMT0, 4 );
					// position
					GXPosition3f32(p1.x, p1.y, p1.z);
					// normal
					GXNormal3f32(r1.x, r1.y, r1.z);
					// binormal/ Tangent
					GXNormal3f32(tr1.x,tr1.y,tr1.z);
        			GXNormal3f32(br1.x,br1.y,br1.z);
        			// texCoord
        			GXTexCoord2f32(0, 1);
        			
					GXPosition3f32(p2.x, p2.y, p2.z);
					GXNormal3f32(r2.x, r2.y, r2.z);
					GXNormal3f32(tr2.x,tr2.y,tr2.z);
        			GXNormal3f32(br2.x,br2.y,br2.z);
        			GXTexCoord2f32(1, 1);
					
					GXPosition3f32(p3.x, p3.y, p3.z);
					GXNormal3f32(r3.x, r3.y, r3.z);
					GXNormal3f32(tr3.x,tr3.y,tr3.z);
        			GXNormal3f32(br3.x,br3.y,br3.z);
        			GXTexCoord2f32(1, 0);
        			
        			GXPosition3f32(p4.x, p4.y, p4.z);
					GXNormal3f32(r4.x, r4.y, r4.z);
					GXNormal3f32(tr4.x,tr4.y,tr4.z);
        			GXNormal3f32(br4.x,br4.y,br4.z);
        			GXTexCoord2f32(0, 0);
				GXEnd();
			}

//	********************************************************************************
// 	ResetState
//
			// reset vertex format cache.
			vtxfmt::Reset();
			
			// reset texGen matrix 
			GXSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0	, GX_IDENTITY);
			GXSetTexCoordGen(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX1	, GX_IDENTITY);
			GXSetTexCoordGen(GX_TEXCOORD2, GX_TG_MTX2x4, GX_TG_TEX2	, GX_IDENTITY);
			GXSetTexCoordGen(GX_TEXCOORD3, GX_TG_MTX2x4, GX_TG_TEX3	, GX_IDENTITY);
			
			GXSetTevDirect( GX_TEVSTAGE1 );
			GXSetTevDirect( GX_TEVSTAGE2 );
			GXSetTevDirect( GX_TEVSTAGE3 );
			GXSetTevDirect( GX_TEVSTAGE4 );
			GXSetTevDirect( GX_TEVSTAGE5 );
			
			GXSetNumTexGens		(1);
			GXSetNumChans		(0);
			GXSetNumTevStages	(1);
			GXSetNumIndStages	(0);

			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			return FALSE;
		}
		
	};
};


NvWaterShader*
nv_watershader_Create	(			)
{
	Shader* shader = NvEngineNew( Shader );
	if( !shader )
		return NULL;
	
	shader->_Init(  );

	return shader->GetInterface();
}




