/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/NvDpyState[NGC].h>
using namespace nv;

//#define 	DISABLE_TEXTURING

namespace
{

	inline uint	_UpdateMode	(	uint	inMode,
								uint	inMask,
								uint	inFlags		)
	{
		uint f = inFlags & inMask;
		return f ? (inMode&(~inMask))|f : inMode;
	}
	
	const GXCullMode GX_CULL_MODE[]={
		GX_CULL_BACK ,
		GX_CULL_FRONT,			
		GX_CULL_NONE ,
	};
	const GXBlendMode GX_BLEND_MODE [] = {
		GX_BM_NONE,		// BM_NONE
		GX_BM_BLEND,	// BM_FILTER
		GX_BM_BLEND,	// BM_ADD		
		GX_BM_SUBTRACT,	// BM_SUB
		GX_BM_BLEND,	// BM_MODULATE
		GX_BM_BLEND,	// BM_FILTER2
		GX_BM_BLEND,	// BM_ADD2
		GX_BM_SUBTRACT,	// BM_SUB2
		GX_BM_BLEND,	// BM_MODULATE2
		GX_BM_BLEND,	// BM_DUMMY
		GX_BM_BLEND,	// BM_ZERO
		GX_BM_BLEND,	// BM_MUL
	};
	const GXBlendFactor GX_BLEND_SRC_DST [] = {	 //src , dst
		// BS_FRAGMENT
		GX_BL_SRCALPHA , GX_BL_INVSRCALPHA, //BM_NONE		    => Not significant
		GX_BL_SRCALPHA , GX_BL_INVSRCALPHA, //BM_FILTER
		GX_BL_SRCALPHA , GX_BL_ONE, 		//BM_ADD
		GX_BL_SRCALPHA , GX_BL_INVSRCALPHA, //BM_SUB 			=> Not Supported				
		GX_BL_SRCALPHA , GX_BL_ZERO, 		//BM_MODULATE
		GX_BL_INVSRCALPHA , GX_BL_SRCALPHA, //BM_FILTER2
		GX_BL_ONE 		  , GX_BL_SRCALPHA, //BM_ADD2
		GX_BL_INVSRCALPHA , GX_BL_SRCALPHA, //BM_SUB2 			=> Not Supported		
		GX_BL_ZERO 		  , GX_BL_SRCALPHA, //BM_MODULATE2		
		GX_BL_ZERO 		  , GX_BL_ONE, 		//BM_DUMMY		
		GX_BL_ZERO 		  , GX_BL_ZERO, 	//BM_ZERO
		GX_BL_DSTCLR 	  , GX_BL_ZERO, 	//BM_LIGHT
		
		// BS_BUFFER
		GX_BL_DSTALPHA , GX_BL_INVDSTALPHA, //BM_NONE		    => Not significant
		GX_BL_DSTALPHA , GX_BL_INVDSTALPHA, //BM_FILTER
		GX_BL_DSTALPHA , GX_BL_ONE, 		//BM_ADD
		GX_BL_DSTALPHA , GX_BL_INVDSTALPHA, //BM_SUB 			=> Not Supported				
		GX_BL_DSTALPHA , GX_BL_ZERO, 		//BM_MODULATE
		GX_BL_INVDSTALPHA , GX_BL_DSTALPHA, //BM_FILTER2
		GX_BL_ONE 		  , GX_BL_DSTALPHA, //BM_ADD2
		GX_BL_INVDSTALPHA , GX_BL_DSTALPHA, //BM_SUB2 			=> Not Supported		
		GX_BL_ZERO 		  , GX_BL_DSTALPHA, //BM_MODULATE2		
		GX_BL_ZERO 		  , GX_BL_ONE, 		//BM_DUMMY		
		GX_BL_ZERO 		  , GX_BL_ZERO, 	//BM_ZERO
		GX_BL_DSTCLR 	  , GX_BL_ZERO, 	//BM_LIGHT
		
		// BS_CTE
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_NONE		    => Not significant
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_FILTER			=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_ADD			=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_SUB 			=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_MODULATE		=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_FILTER2		=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_ADD2			=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_SUB2 			=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_MODULATE2		=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_DUMMY			=> Not Supported
		GX_BL_ZERO , GX_BL_ZERO, 			//BM_ZERO			=> Not Supported
		GX_BL_DSTCLR , GX_BL_ZERO, 			//BM_LIGHT			=> Not Supported
	};	
	struct GXTevCConf {
		GXTevColorArg		arg[4];
		GXTevOp				op;		
	};
	struct GXTevAConf {
		GXTevAlphaArg		arg[4];
		GXTevOp				op;		
	};	
	
	const GXTevCConf GX_TEV_COLOR_ARG[] = {	// a,b,c,d, | op
		{GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_TEXC	,	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_TEXC, GX_CC_RASC, GX_CC_ZERO	,	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_TEXC, GX_CC_RASC, GX_CC_RASA	,	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_TEXC, GX_CC_C0, 	 GX_CC_CPREV,	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_RASC	,	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_C0  , GX_CC_ONE,  GX_CC_RASC	,	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_TEXC, GX_CC_C0 ,  GX_CC_ZERO	,	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_C0	,	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_CPREV,	GX_TEV_ADD			},		
		{GX_CC_RASC, GX_CC_C0, 	 GX_CC_C2, 	 GX_CC_C1, 		GX_TEV_COMP_R8_GT 	},
		{GX_CC_ZERO, GX_CC_CPREV,GX_CC_TEXC, GX_CC_ZERO , 	GX_TEV_ADD			},
		{GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_ONE 	,	GX_TEV_ADD			},
	};	
	const GXTevAConf GX_TEV_ALPHA_ARG[] = {	// a,b,c,d,	| op
		{GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_TEXA	,	GX_TEV_ADD			},
		{GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_A2	,	GX_TEV_ADD			},
		{GX_CA_ZERO, GX_CA_TEXA, GX_CA_RASA, GX_CA_ZERO	,	GX_TEV_ADD			},
		{GX_CA_ZERO, GX_CA_TEXA, GX_CA_ONE,	 GX_CA_RASA ,	GX_TEV_ADD			},
		{GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_APREV,	GX_TEV_ADD			},
		{GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_RASA	,	GX_TEV_ADD			},
		{GX_CA_ZERO, GX_CA_A0, 	 GX_CA_TEXA, GX_CA_ZERO	,	GX_TEV_ADD			},
		{GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_A0	,	GX_TEV_ADD			},
		{GX_CA_APREV,GX_CA_A1  , GX_CA_A2  , GX_CA_ZERO	,	GX_TEV_COMP_A8_GT	},
		{GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ONE	,	GX_TEV_ADD			},
	};

	enum TEV_COLOR_ARG_CONF {
		TCAC_Tex 			= 0,				// St0 TF_DECAL
		TCAC_TexRas 		= 1,				// St0 TF_MODULATE
		TCAC_TexRasADDRasA	= 2,				// St0 TF_HIGHLIGHT1 	|| St0 TF_HIGHLIGHT2
		TCAC_TexReg0ADDPrev = 3,				// St1 TF_MODULATE
		TCAC_Ras			= 4,				// St0 !Textuting && !ligthing
		TCAC_Reg0ADDRas		= 5,				// St0 !Textuting && ligthing
		TCAC_TexReg0		= 6,				// Color modulation St0 Texturing
		TCAC_Reg0			= 7,				// Color modulation St0 !Texturing
		TCAC_Prev			= 8,				// St1 | St2 Alpha test Tev stage
		TCAC_Cartoon		= 9,				// Cartoon St0
		TCAC_TexPrev		= 10,				// Cartoon St1 (texturing)
		TCAC_White			= 11,				// Dummy
	};
	
	enum TEV_ALPHA_ARG_CONF {
		TAAC_Tex 		= 0,					// St0 TF_DECAL		( !useAlphaConstant ) 	||  St0 TF_MODULATE	( !useAlphaConstant && lighting) || St0 TF_HIGHLIGHT2
		TAAC_Reg2 		= 1,					// St0 TF_DECAL		(  useAlphaConstant )	||  St0 TF_MODULATE	( useAlphaConstant )
		TAAC_TexRas 	= 2,					// St0 TF_MODULATE	( !useAlphaConstant && !lighting)
		TAAC_TexADDRas	= 3,					// St0 TF_HIGHLIGHT
		TAAC_Prev		= 4,					// St1 TF_MODULATE
		TAAC_Ras		= 5,					// St0 !Textuting && !ligthing
		TAAC_TexReg0	= 6,					// Color modulation St0 Texturing
		TAAC_Reg0		= 7,					// Color modulation St0 !Texturing
		TAAC_ATest		= 8,					// St1 | St2 Alpha test Tev stage
		TAAC_One		= 9,					// Dummy  	|| St0 !Textuting && ligthing
	};
	
	

	static 			uint8 inkTexData[4][64] ATTRIBUTE_ALIGN(32);
	// Do not change W and H !
	static const 	uint  inkTexW = 64;
	static const 	uint  inkTexH = 4;	
	
	void 
	InitInkTexture()
	{	
		static bool isInit = FALSE;
		if (isInit) return ;
		
	
		uint8 * ptr = (uint8*)(inkTexData);
		
		for (uint j = 0 ; j < 4	; ++j) {
			for (uint i = 0 ; i < 64 ; ++i ) {
			
				uint8 color = uint8( 0xFF * ( float(i) / 63.0f) );				
				//without tiling
				//data[j][i] =  color;
				ptr[((i / 8)*4*8)  + (j * 8) + (i%8)] = color ;
			}
		}		
		isInit = TRUE;
	}
	
	void 
	FixInkTexture(GXTexMapID inGXTexId)
	{
		NV_COMPILE_TIME_ASSERT(GX_TEXMAP0 == GX_TEXCOORD0 && GX_TEXMAP1 == GX_TEXCOORD1);
	
		GDSetTexLookupMode 	(inGXTexId,GX_CLAMP,GX_CLAMP,GX_LINEAR, GX_LINEAR, 0, 0, 0,GX_FALSE,GX_FALSE, GX_ANISO_1);    
		GDSetTexImgAttr 	(inGXTexId,inkTexW,inkTexH, GX_TF_I8);
		GDSetTexImgPtr  	(inGXTexId,inkTexData);
		GDSetTexCoordScaleAndTOEs (GXTexCoordID(inGXTexId), inkTexW,GX_FALSE, GX_FALSE,
															inkTexH,GX_FALSE,GX_FALSE,
															GX_FALSE, GX_FALSE );
	}


	// One Display list for each cull mode (back front none)	
	GDLObj 				gdOutliningDL[3];
	static uchar		dlOLBuffer[64 * 3] ATTRIBUTE_ALIGN(32);
	
	void
	GenDLForOutlining(uint32 inCullMode)
	{
		uint cm 		= ((inCullMode & DpyState::CM_MASK) >> 11) -1 ;
		#ifdef DISABLE_FACECULLING
			cm =  GX_CULL_NONE ;
		#endif
		
		GDSetBlendModeEtc( GX_BM_BLEND , GX_BL_ZERO , GX_BL_DSTALPHA, GX_LO_CLEAR,							
						   GX_TRUE, GX_FALSE, GX_FALSE 			); 	
		GDSetAlphaCompare( GX_ALWAYS,0,GX_AOP_AND,GX_ALWAYS,0	);
		
		GDSetGenMode2( 0,1,1,uint8(0), GX_CULL_MODE[cm] );

		GDSetTevOrder(	GX_TEVSTAGE0, 	GX_TEXCOORD_NULL, GX_TEXMAP_NULL, 	GX_COLOR_ZERO 	,
	    			  /*GX_TEVSTAGE1*/	GX_TEXCOORD_NULL, GX_TEXMAP_NULL,  	GX_COLOR_ZERO	);

		GDSetTevColorCalc		(	GX_TEVSTAGE0,  	GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO,    GX_CC_C0,
													GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE,  GX_TEVPREV );
					    			
		GDSetTevAlphaCalcAndSwap(	GX_TEVSTAGE0,  	GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO,    GX_CA_A0,
								 				 	GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, GX_TRUE,	 GX_TEVPREV,
									GX_TEV_SWAP0,GX_TEV_SWAP0 );
		GDPadCurr32				( );
		GDFlushCurrToMem		( );						
	}
	
	void 
	InitOutliningDL()
	{	
		static bool isInit = FALSE;
		if (isInit) return;
		
		for (uint i = 0 ; i < 3 ; ++i){
			GDInitGDLObj ( &gdOutliningDL[i], dlOLBuffer + (i * 64) , 64 );
			GDSetCurrent ( &gdOutliningDL[i] );
			GenDLForOutlining( (i + 1) << 11 );
			GDPadCurr32					( );
			GDFlushCurrToMem			( );
		}
		
		
		isInit = TRUE;
	}
	
	void
	SetOutliningDL(uint32 inCullMode)
	{
		uint32 cm = ((inCullMode & DpyState::CM_MASK) >> 11) -1 ;
		NV_ASSERT(cm >= 0 && cm < 3);

		void * 	dlPtr 	= GDGetGDLObjStart	(gdOutliningDL+cm);
		uint 	dlbSize = GDGetGDLObjOffset	(gdOutliningDL+cm);
		GXCallDisplayList( dlPtr, dlbSize );
	}
	
	uint pfmbitsSizeOf(	GXTexFmt 	inPfm) 
	{
		switch (inPfm) 
		{
			case GX_TF_I4:
				return 4;
			case GX_TF_I8:
				return 8;
			case GX_TF_IA4:
				return 8;
			case GX_TF_IA8:
				return 16;
			case GX_TF_RGB565:
				return 16;
			case GX_TF_RGB5A3:
				return 16;				
			case GX_TF_RGBA8:
				return 32;
			case GX_TF_CMPR:
				return 4;
			default :
				return 0;
		}
	}
}



void
DpyState_NGC::Init	(		)
{
	InitInkTexture();
	InitOutliningDL();	
	
	caps				= 0;
	enabled				= EN_DEFAULT;
	mode				= MODE_DEFAULT;
	src					= DpySource();
	Vec2Zero( &srcXY );
	srcBmp				= NULL;
	alphaPass			= 0.02f;
	blendCte			= 0.5f;
	wrColorMask			= 0xFFFFFFFF;
	defColor			= 0xFFFFFFFF;
	changed				= TRUE;
	
	lod_bias 	= 0.0f;
	bias_clamp 	= FALSE;
	do_edge_lod	= FALSE;
	aFilter 	= AFL_A1;
	
	GDInitGDLObj ( &gdDL, dlBuffer, GDDL_Size);
}


void
DpyState_NGC::Copy	(	const DpyState_NGC&		inRef	)
{
	caps			= inRef.caps;
	enabled			= inRef.enabled;
	mode			= inRef.mode;
	SetSource( inRef.src );
	srcXY			= inRef.srcXY;
	alphaPass		= inRef.alphaPass;
	blendCte		= inRef.blendCte;
	wrColorMask		= inRef.wrColorMask;
	changed			= TRUE;
}


void
DpyState_NGC::Shut		(			)
{
	SafeRelease( srcBmp );
}


void
DpyState_NGC::Enable	(	uint		inEnableFlags		)
{
	SetEnabled( enabled | inEnableFlags );
}


void
DpyState_NGC::Disable	(	uint		inEnableFlags		)
{
	SetEnabled( enabled & (~inEnableFlags) );
}


void
DpyState_NGC::SetEnabled	(	uint	inEnableFlags		)
{
	uint32 newEnableFlags = inEnableFlags & EN_ALL_MASK;	
	if( enabled != newEnableFlags ) {
		enabled = newEnableFlags;
		changed = TRUE;
	}
}


uint
DpyState_NGC::GetEnabled	(					)
{
	return enabled;
}


void
DpyState_NGC::SetCapabilities		(	uint		inCapabilities		)
{
	if( caps != inCapabilities ) {
		caps = inCapabilities;
		changed = TRUE;
	}
}


uint
DpyState_NGC::GetCapabilities		(									)
{
	return caps;
}


void
DpyState_NGC::SetMode		(	uint		inModeFlags	)
{
	uint m = _UpdateMode( mode, TF_MASK, inModeFlags );
		 m = _UpdateMode( m,    TM_MASK, inModeFlags );
		 m = _UpdateMode( m,	TR_MASK, inModeFlags );
		 m = _UpdateMode( m,    TW_MASK, inModeFlags );
		 m = _UpdateMode( m,    CM_MASK, inModeFlags );
		 m = _UpdateMode( m,    BM_MASK, inModeFlags );
		 m = _UpdateMode( m,    BS_MASK, inModeFlags );
	if( m != mode ) {
		changed = TRUE;
		mode = m;
	}
}


uint
DpyState_NGC::GetMode		(							)
{
	return mode;
}


bool
DpyState_NGC::SetSource	(	DpySource	inSource		)
{
	if( src == inSource )
		return TRUE;
	src = inSource;

	NvkBitmap* kbmp = NULL;
	if( src.Bitmap )
		kbmp = src.Bitmap->GetKInterface();
	SafeGetRef( srcBmp, kbmp );

	changed = TRUE;
	return TRUE;
}


void
DpyState_NGC::GetSource		(	DpySource&	outSource		)
{
	outSource = src;
}


void
DpyState_NGC::SetSourceOffset	(	const Vec2&	inOffsetXY		)
{
	if( srcXY != inOffsetXY ) {
		srcXY = inOffsetXY;
		changed = TRUE;
	}
}


void
DpyState_NGC::GetSourceOffset	(	Vec2&		outOffsetXY		)
{
	Vec2Copy( &outOffsetXY, &srcXY );
}


bool
DpyState_NGC::SetAlphaPass	(	float		inAlphaPass		)
{
	inAlphaPass = Clamp( inAlphaPass, 0.0f, 1.0f );
	if( inAlphaPass != alphaPass ) {
		alphaPass = inAlphaPass;
		changed = TRUE;
	}
	return TRUE;
}


float
DpyState_NGC::GetAlphaPass	(				)
{
	return alphaPass;
}


bool
DpyState_NGC::SetBlendSrcCte	(	float		inCte	)
{
	inCte = Clamp( inCte, 0.0f, 1.0f );
	if( inCte != blendCte ) {
		blendCte = inCte;
		changed = TRUE;
	}
	return TRUE;
}


float
DpyState_NGC::GetBlendSrcCte	(			)
{
	return blendCte;
}


void
DpyState_NGC::SetWRColorMask	(	uint32		inRGBAMask		)
{
	if( wrColorMask != inRGBAMask ) {
		wrColorMask = inRGBAMask;
		changed = TRUE;
	}
}

void
DpyState_NGC::SetDefaultColor		(	uint32	inRGBAColor	)
{
	if (inRGBAColor != defColor){
		defColor = inRGBAColor;
		changed = TRUE;
	}
}
	

uint32
DpyState_NGC::GetWRColorMask	(		)
{
	return wrColorMask;
}


bool
DpyState_NGC::HasValidSource	(		)
{
	return ( srcBmp || src.Raster!=DPYR_NULL );
}


bool
DpyState_NGC::IsTexturing	(			)
{
	return (enabled&EN_TEXTURING) && (caps&CF_HAS_TEX0) && HasValidSource();
}

NvkBitmap*
DpyState_NGC::SetTexturing	(	uint32		inBitmapUID,
								float		inMappingOffset,
								float		inMappingRatio		)
{
	NvBitmap* bmpP = NvBitmap::Create( inBitmapUID );
	SetSource	( DpySource(bmpP) );
	SafeRelease	( bmpP );
	if( !src.Bitmap )
		return NULL;
	NvkBitmap* kbmp = src.Bitmap->GetKInterface();
	NV_ASSERT( kbmp );
	return kbmp;
}

void
DpyState_NGC::ForceUpdate	(	) 
{
	changed = TRUE;
}

void
DpyState_NGC::Activate	(		)
{
	NvkBitmap * srcBmp = (src.Bitmap) ? src.Bitmap->GetKInterface() : NULL;
	if (srcBmp) {
		// Always update the source bitmap
		srcBmp->UpdateBeforeDraw();	
	}
									
	if (changed) {
	//if (TRUE) {
		changed = FALSE;
		
		GDSetCurrent 	( &gdDL );
		GDSetCurrOffset ( 0 	);
		
		bool lighting 	= (caps&CF_HAS_NORMAL) != 0;
		bool texturing 	= IsTexturing();
		uint cm 		= ((mode & CM_MASK) >> 11) -1 ;	
		
		#ifdef DISABLE_TEXTURING  
			texturing = FALSE;
		#endif		
		#ifdef DISABLE_FACECULLING
			cm =  GX_CULL_NONE ;
		#endif

		bool 	useDefColor			= (lighting || texturing || (caps&(CF_HAS_RGBA|CF_HAS_RGB)))?FALSE:TRUE;
						
		uint 	bs  				= mode & BS_MASK; // BlendMode
		uint	bm  				= mode & BM_MASK; // BlendMode
		
		bool 	textureHaveVA		= (texturing && srcBmp && srcBmp->GetAlphaStatus() == NvBitmap::AS_VARIED);
		bool 	bmf 				= (bm == BM_SELECT)  &&  textureHaveVA;
				bm 					= (bm != BM_SELECT)? bm : (textureHaveVA? BM_FILTER: BM_NONE );	
		
		bool 	useAlphaConstant 	= ((bs == BS_CTE) && (bm != BM_NONE && bm != BM_MUL));
		uint 	aref 				= bmf ? 0 : uint( alphaPass * 255.0f );	
		bool	useTevToDoATest		= useAlphaConstant && (aref != 0);
		
		GXColor	gxAlphaBlendCst		= {0.0f,0.0f,0.0f, uint8(Clamp(blendCte,0.0f,1.0f) * 255.0f)	};
		GXColor	gxAlphaTestCst		= {0.0f,0.0f,0.0f, aref	- 1										};	

		bool 	haveOnlyTex 		= ( !(caps&(CF_HAS_RGBA|CF_HAS_RGB)) && !(caps&CF_HAS_NORMAL) && texturing);
		uint 	tf 					= (mode & TF_MASK);				
		useDefColor				   |= (haveOnlyTex && (tf == TF_MODULATE));
		
		if (texturing && tf == TF_DECAL)
				lighting			= FALSE;
		
		NV_ASSERT					( bm>BM_SELECT && bm <= BM_MUL );
		
		if (useAlphaConstant) 
			bs = BS_FRAGMENT;

		// Set BlendMod and Color Writing
		GXBlendMode 	op 			= GX_BLEND_MODE		[  (bm>>13) - 2 				];
		GXBlendFactor   asrc 		= GX_BLEND_SRC_DST	[ (((bm>>13) - 2) + (((bs>>17) -1) * 22)) *2     ];
		GXBlendFactor   adst 		= GX_BLEND_SRC_DST	[ (((bm>>13) - 2) + (((bs>>17) -1) * 22)) *2 + 1 ];	
		
		bool 			writeColor 	= (!(enabled & EN_WR_COLOR))? FALSE : ( (wrColorMask>>8  ) ? TRUE : FALSE );
		bool 			writeAlpha 	= (!(enabled & EN_WR_COLOR))? FALSE : ( (wrColorMask&0xFF) ? TRUE : FALSE );
			
		GDSetBlendModeEtc( op, asrc, adst, GX_LO_CLEAR,							// Blend, Logical operation
						   GXBool(writeColor),GXBool(writeAlpha),GX_FALSE ); 	// Write Color/Alpha, dither enable
		
		// Alpha pass test
		GDSetAlphaCompare(GX_GEQUAL,useTevToDoATest?1:aref,GX_AOP_AND,GX_ALWAYS,0);

		bool enableZRead  = ( enabled & EN_RD_DEPTH) != 0;
		bool enableZWrite = bmf ? FALSE : ((enabled & EN_WR_DEPTH) != 0);	
		GDSetZMode( GX_ENABLE, enableZRead? GX_LEQUAL:GX_ALWAYS , GXBool(enableZWrite) );
		
		GDSetGenMode2( 	uint8(texturing						? 1 : 0							), 	// TexGen
						uint8(haveOnlyTex					? 0 : 1							),	// Color
						(uint8((lighting	&&	texturing) 	? 2 : 1 ) + (useTevToDoATest?1:0) ),	// Nb TeV
						uint8(0), GX_CULL_MODE[cm] 											);	// 0, CullMode

		NV_ASSERT(texturing || !haveOnlyTex);

		TEV_COLOR_ARG_CONF tcacStg[3] 		= {TCAC_White	,TCAC_White	,TCAC_White	};
		TEV_ALPHA_ARG_CONF taacStg[3] 		= {TAAC_One		,TAAC_One	,TAAC_One	};
		TEV_COLOR_ARG_CONF *tcacStgATest 	= (texturing && lighting)? (tcacStg+2) : (tcacStg+1);
		TEV_ALPHA_ARG_CONF *taacStgATest 	= (texturing && lighting)? (taacStg+2) : (taacStg+1);	
		
		if ( texturing && FixTexture(src, mode, lod_bias , bias_clamp , do_edge_lod , aFilter) ) {
			// stage 0 :
			// Decal 		Tex
			// Modulate		Tex * Color (Lighting Diffuse) or Gouraud)
			// stage 1 (only for lighting): 
			// Modulate		prec + Tex * amb  ( prec = Tex * Diffuse )
			switch (tf) {
				case TF_DECAL :
					tcacStg[0] = TCAC_Tex;
					taacStg[0] = TAAC_Tex;
					break;
					
				case TF_MODULATE :
					if (!useDefColor) {
						tcacStg[0] = TCAC_TexRas ;
						if (lighting) {
							taacStg[0] = TAAC_Tex;
							tcacStg[1] = TCAC_TexReg0ADDPrev ;					
							taacStg[1] = TAAC_Prev;					
						}
						else
							taacStg[0] = TAAC_TexRas;
					}
					else {
						tcacStg[0] = TCAC_TexReg0 ;
						taacStg[0] = TAAC_TexReg0;
					}
					break;
					
				case TF_HIGHLIGHT :
					tcacStg[0] = TCAC_TexRasADDRasA ;
					taacStg[0] = TAAC_TexADDRas;
					if (lighting) {
						// TODO St1
					}
					break;
					
				case TF_HIGHLIGHT2 :
					tcacStg[0] = TCAC_TexRasADDRasA ;
					taacStg[0] = TAAC_Tex;			
					if (lighting) {
						// TODO St1
					}
					break;
					
				default:
					NV_ASSERT(FALSE);
					break;
			}

			GDSetTevOrder(	GX_TEVSTAGE0, 	GX_TEXCOORD0, GX_TEXMAP0,  haveOnlyTex ? GX_COLOR_NULL: GX_COLOR0A0		,
		    			  /*GX_TEVSTAGE1*/	GX_TEXCOORD0, GX_TEXMAP0,  GX_COLOR_NULL								);
			GDSetTevOrder(	GX_TEVSTAGE2, 	GX_TEXCOORD0, GX_TEXMAP0,  GX_COLOR_NULL								,
		    			  /*GX_TEVSTAGE3*/	GX_TEXCOORD0, GX_TEXMAP0,  GX_COLOR_NULL								);
		    			  
		}	
		else {
			if (haveOnlyTex) useDefColor = TRUE;
			
			GDSetTevOrder(	GX_TEVSTAGE0, 	GX_TEXCOORD_NULL,	GX_TEX_DISABLE, GX_COLOR0A0 	,
		    			  /*GX_TEVSTAGE1*/	GX_TEXCOORD_NULL, 	GX_TEX_DISABLE, GX_COLOR0A0		);
		    			  
			if (lighting) {		// Ras = DifColor, Reg0 = AmbColor => Ras+Reg0
				tcacStg[0] = TCAC_Reg0ADDRas;
				taacStg[0] = TAAC_One;		
			}
			else if (useDefColor){
				tcacStg[0] = TCAC_Reg0;
				taacStg[0] = TAAC_Reg0;
			}
			else {				// Color Stream
				tcacStg[0] = TCAC_Ras;
				taacStg[0] = TAAC_Ras;
			}
		}
		if (useDefColor) {
			GXColor defC = {defColor>>24 & 0xFF, defColor>>16 & 0xFF, defColor>>8 & 0xFF, 0xFF};
			GDSetTevColor(GX_TEVREG0,defC);
		}
		if (useAlphaConstant && !useTevToDoATest) {
			taacStg[0] = TAAC_Reg2;
		}
		
		GDSetTevColor(GX_TEVREG2,gxAlphaBlendCst);
		GDSetTevColor(GX_TEVREG1,gxAlphaTestCst);	
		
		if (useTevToDoATest) {
			*tcacStgATest = TCAC_Prev;
			*taacStgATest = TAAC_ATest;
		}
		
		// Fix stage
		// setup stage 0
		// always add Stage1 configuration : only used if (texturing && lighting)
		// always add Stage2 configuration : only used if (texturing && lighting) && if Alpha test is done by tev test
		NV_COMPILE_TIME_ASSERT ( GX_TEVSTAGE0 == 0 &&  GX_TEVSTAGE1 == 1 && GX_TEVSTAGE2 == 2);	
		for (uint i = 0 ; i < 3 ; ++i) {
			GDSetTevColorCalc		(	GXTevStageID(i),GX_TEV_COLOR_ARG[tcacStg[i]].arg[0]	, GX_TEV_COLOR_ARG[tcacStg[i]].arg[1],
														GX_TEV_COLOR_ARG[tcacStg[i]].arg[2]	, GX_TEV_COLOR_ARG[tcacStg[i]].arg[3],
														GX_TEV_COLOR_ARG[tcacStg[i]].op		, GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV );
						    			
			GDSetTevAlphaCalcAndSwap(	GXTevStageID(i),GX_TEV_ALPHA_ARG[taacStg[i]].arg[0]	, GX_TEV_ALPHA_ARG[taacStg[i]].arg[1], 
														GX_TEV_ALPHA_ARG[taacStg[i]].arg[2]	, GX_TEV_ALPHA_ARG[taacStg[i]].arg[3],
									 					GX_TEV_ALPHA_ARG[taacStg[i]].op		, GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV,
										GX_TEV_SWAP0,GX_TEV_SWAP0 );
		}
		GDPadCurr32					( );
		GDFlushCurrToMem			( );
	}
	else {

	}
	
	void * 	dlPtr 	= GDGetGDLObjStart (&gdDL);
	uint 	dlbSize = GDGetGDLObjOffset(&gdDL);
	NV_ASSERT(dlbSize <= GDDL_Size);
	GXCallDisplayList( dlPtr, dlbSize );	
}

void
DpyState_NGC::ActivateCartoonLighting	(  bool inIsInkable )
{
	
	if (changed) {
		GDSetCurrent 	( &gdDL );
		changed = FALSE;
		GDSetCurrOffset(0);
		
		bool texturing 	= IsTexturing();
		uint cm 		= ((mode & CM_MASK) >> 11) -1 ;	
		
		#ifdef DISABLE_TEXTURING  
			texturing = FALSE;
		#endif		
		#ifdef DISABLE_FACECULLING
			cm =  GX_CULL_NONE ;
		#endif

		uint 	bs  				= mode & BS_MASK; // BlendMode
		uint	bm  				= mode & BM_MASK; // BlendMode
		
		bool 	textureHaveVA		= (texturing && srcBmp && srcBmp->GetAlphaStatus() == NvBitmap::AS_VARIED);
		bool 	bmf 				= (bm == BM_SELECT)  &&  textureHaveVA;
				bm 					= (bm != BM_SELECT)? bm : (textureHaveVA? BM_FILTER: BM_NONE );	
				
		uint 	aref 				= bmf ? 0 : uint( alphaPass * 255.0f );	

		
		NV_ASSERT	( caps&CF_HAS_NORMAL );	
		NV_ASSERT	( bm>BM_SELECT && bm <= BM_ZERO );

		if (bs == BS_CTE )					DebugPrintf("<Neova> Warning: NGC does not support constant as blend source with Cartoon shader\n");
		if (bm == BM_SUB || bm == BM_SUB2 )	DebugPrintf("<Neova> Warning: NGC does not support blend factor for substract mode\n");
		
		// Set BlendMod and Color Writing
		GXBlendMode 	op 			= GX_BLEND_MODE		[  (bm>>13) - 2 				];
		GXBlendFactor   asrc 		= GX_BLEND_SRC_DST	[ (((bm>>13) - 2) + (((bs>>17) -1) * 22)) *2     ];
		GXBlendFactor   adst 		= GX_BLEND_SRC_DST	[ (((bm>>13) - 2) + (((bs>>17) -1) * 22)) *2 + 1 ];	
		
		bool 			writeColor 	= (!(enabled & EN_WR_COLOR))? FALSE : ( (wrColorMask>>8  ) ? TRUE : FALSE );
		bool 			writeAlpha 	= (!(enabled & EN_WR_COLOR))? FALSE : ( (wrColorMask&0xFF) ? TRUE : FALSE );
			
		GDSetBlendModeEtc( op, asrc, adst, GX_LO_CLEAR,							// Blend, Logical operation
						   GXBool(writeColor),GXBool(writeAlpha),GX_FALSE ); 	// Write Color/Alpha, dither enable
		
		// Alpha pass test
		GDSetAlphaCompare(GX_GEQUAL,aref,GX_AOP_AND,GX_ALWAYS,0);

		bool enableZRead  = ( enabled & EN_RD_DEPTH) != 0;
		bool enableZWrite = bmf ? FALSE : ((enabled & EN_WR_DEPTH) != 0);	
		GDSetZMode( GX_ENABLE, enableZRead? GX_LEQUAL:GX_ALWAYS , GXBool(enableZWrite) );


		uint numTevStage = 1 + (texturing  ? 1 : 0 ) + (inIsInkable? 1 : 0)	;
		GDSetGenMode2( 	numTevStage - 1					, 	// TexGen
						1								,	// Color
					    numTevStage 					,	// Nb TeV
						uint8(0), GX_CULL_MODE[cm] 		);	// 0, CullMode


		// stage 0 color : TCAC_Cartoon, alpha : TAAC_One.	
		TEV_COLOR_ARG_CONF 	 tcacStg[3] 	= {TCAC_Cartoon	,TCAC_White	,TCAC_White	};
		TEV_ALPHA_ARG_CONF 	 taacStg[3] 	= {TAAC_One		,TAAC_One	,TAAC_One	};
		TEV_COLOR_ARG_CONF * tcacInk 		= tcacStg+1;

		// Cartoon 	Shader :
		// RasterColor = (ShadingCoef, ShadingCoef, ShadingCoef, (N*M).Z)	
		if ( texturing && FixTexture(src, mode , lod_bias , bias_clamp , do_edge_lod , aFilter) ) {
			tcacStg[1] = TCAC_TexPrev;
			taacStg[1] = TAAC_Tex;
			tcacInk++;
		}

		GDSetTevOrder(	GX_TEVSTAGE0, 	GX_TEXCOORD_NULL, GX_TEXMAP_NULL, 	GX_COLOR0A0 	,
	    			  /*GX_TEVSTAGE1*/	GX_TEXCOORD0	, GX_TEXMAP0	,  	GX_COLOR_NULL	);
		GDSetTevOrder(	GX_TEVSTAGE2, 	GX_TEXCOORD1	, GX_TEXMAP1	,  	GX_COLOR_NULL	,
	    			  /*GX_TEVSTAGE3*/	GX_TEXCOORD_NULL, GX_TEXMAP_NULL, 	GX_COLOR_NULL	);

		
		if (inIsInkable) {
			// FixInkTexture
			FixInkTexture(texturing? GX_TEXMAP1 : GX_TEXMAP0);
	 		*tcacInk = TCAC_TexPrev;
	 	}
		// Fix stage
		NV_COMPILE_TIME_ASSERT ( GX_TEVSTAGE0 == 0 &&  GX_TEVSTAGE1 == 1 && GX_TEVSTAGE2 == 2);	
		
		for (uint i = 0 ; i < 3 ; ++i) {
			GDSetTevColorCalc		(	GXTevStageID(i),GX_TEV_COLOR_ARG[tcacStg[i]].arg[0]	, GX_TEV_COLOR_ARG[tcacStg[i]].arg[1],
														GX_TEV_COLOR_ARG[tcacStg[i]].arg[2]	, GX_TEV_COLOR_ARG[tcacStg[i]].arg[3],
														GX_TEV_COLOR_ARG[tcacStg[i]].op		, GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV );
						    			
			GDSetTevAlphaCalcAndSwap(	GXTevStageID(i),GX_TEV_ALPHA_ARG[taacStg[i]].arg[0]	, GX_TEV_ALPHA_ARG[taacStg[i]].arg[1], 
														GX_TEV_ALPHA_ARG[taacStg[i]].arg[2]	, GX_TEV_ALPHA_ARG[taacStg[i]].arg[3],
									 					GX_TEV_ALPHA_ARG[taacStg[i]].op		, GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV,
										GX_TEV_SWAP0,GX_TEV_SWAP0 );
		}
		GDPadCurr32					( );
		GDFlushCurrToMem			( );
	}
	
	void * 	dlPtr 	= GDGetGDLObjStart (&gdDL);
	uint 	dlbSize = GDGetGDLObjOffset(&gdDL);	
	NV_ASSERT(dlbSize <= GDDL_Size);
	GXCallDisplayList( dlPtr, dlbSize );	
}

void
DpyState_NGC::ActivateLightmap(		DpySource & inLightmapSrc				, 
									uint 		inLightmapMode				)
{
	NvkBitmap * srcBmp = (src.Bitmap) ? src.Bitmap->GetKInterface() : NULL;
	if (srcBmp) srcBmp->UpdateBeforeDraw();	// Always update the source bitmap
		
	if (changed) {
		changed = FALSE;
		
		GDSetCurrent 	( &gdDL );
		GDSetCurrOffset ( 0 	);

		uint cm 		= ((mode & CM_MASK) >> 11) -1 ;	

		#ifdef DISABLE_FACECULLING
			cm =  GX_CULL_NONE ;
		#endif
						
		uint 	bs  				= mode & BS_MASK; // BlendMode
		uint	bm  				= mode & BM_MASK; // BlendMode
		
		bool 	textureHaveVA		= (srcBmp && srcBmp->GetAlphaStatus() == NvBitmap::AS_VARIED);
		bool 	bmf 				= (bm == BM_SELECT)  &&  textureHaveVA;
				bm 					= (bm != BM_SELECT)? bm : (textureHaveVA? BM_FILTER: BM_NONE );	
		
		uint 	aref 				= bmf ? 0 : uint( alphaPass * 255.0f );	
		
		GXColor	gxAlphaBlendCst		= {0.0f,0.0f,0.0f, uint8(Clamp(blendCte,0.0f,1.0f) * 255.0f)	};
		GXColor	gxAlphaTestCst		= {0.0f,0.0f,0.0f, aref	- 1										};	

		bool 	haveOnlyTex 		= TRUE;
		uint 	tf 					= (mode & TF_MASK);
		
		NV_ASSERT					( bm>BM_SELECT && bm <= BM_MUL );
		
		
		// Set BlendMod and Color Writing
		GXBlendMode 	op 			= GX_BLEND_MODE		[   (bm>>13) - 2 				];
		GXBlendFactor   asrc 		= GX_BLEND_SRC_DST	[ (((bm>>13) - 2) + (((bs>>17) -1) * 22)) *2     ];
		GXBlendFactor   adst 		= GX_BLEND_SRC_DST	[ (((bm>>13) - 2) + (((bs>>17) -1) * 22)) *2 + 1 ];	
		
		bool 			writeColor 	= (!(enabled & EN_WR_COLOR))? FALSE : ( (wrColorMask>>8  ) ? TRUE : FALSE );
		bool 			writeAlpha 	= (!(enabled & EN_WR_COLOR))? FALSE : ( (wrColorMask&0xFF) ? TRUE : FALSE );
			
		GDSetBlendModeEtc( op, asrc, adst, GX_LO_CLEAR,							// Blend, Logical operation
						   GXBool(writeColor),GXBool(writeAlpha),GX_FALSE ); 	// Write Color/Alpha, dither enable
		
		// Alpha pass test
		GDSetAlphaCompare(GX_GEQUAL,aref,GX_AOP_AND,GX_ALWAYS,0);

		bool enableZRead  = ( enabled & EN_RD_DEPTH) != 0;
		bool enableZWrite = bmf ? FALSE : ((enabled & EN_WR_DEPTH) != 0);
		GDSetZMode( GX_ENABLE, enableZRead? GX_LEQUAL:GX_ALWAYS , GXBool(enableZWrite) );

		GDSetGenMode2( 	uint8(2) , uint8(0), uint8(2), uint8(0), GX_CULL_MODE[cm] );
		
		FixTexture(src, mode , lod_bias , bias_clamp , do_edge_lod , aFilter);
		
		FixTexture(inLightmapSrc, inLightmapMode,lod_bias , bias_clamp , do_edge_lod , aFilter,1);
		
		GDSetTevOrder(	GX_TEVSTAGE0, 	GX_TEXCOORD0, GX_TEXMAP0,  GX_COLOR_NULL,
	    			  /*GX_TEVSTAGE1*/	GX_TEXCOORD1, GX_TEXMAP1,  GX_COLOR_NULL);
		
		GDSetTevColorCalc		(GX_TEVSTAGE0,GX_CC_ZERO,GX_CC_ZERO,GX_CC_ZERO,GX_CC_TEXC,
											  GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV);
											  
		GDSetTevAlphaCalcAndSwap(GX_TEVSTAGE0,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO,GX_CA_TEXA,
											  GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV,
											  GX_TEV_SWAP0,GX_TEV_SWAP0);
											  
		GDSetTevColorCalc		(GX_TEVSTAGE1,GX_CC_ZERO,GX_CC_CPREV,GX_CC_TEXC,GX_CC_ZERO,
											  GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV);
		GDSetTevAlphaCalcAndSwap(GX_TEVSTAGE1,GX_CA_ZERO,GX_CA_ZERO,GX_CA_ZERO,GX_CA_APREV,
											  GX_TEV_ADD,GX_TB_ZERO,GX_CS_SCALE_1,GX_TRUE,GX_TEVPREV,
											  GX_TEV_SWAP0,GX_TEV_SWAP0);
											  
		GDPadCurr32					( );
		GDFlushCurrToMem			( );
	}

	void * 	dlPtr 	= GDGetGDLObjStart (&gdDL);
	uint 	dlbSize = GDGetGDLObjOffset(&gdDL);
	NV_ASSERT(dlbSize <= GDDL_Size);
	GXCallDisplayList( dlPtr, dlbSize );	
}

void
DpyState_NGC::ActivateOutlining	(	uint32		inCullMode	)
{
	SetOutliningDL(inCullMode);
}

void
DpyState_NGC::SetMipmapping		(	float					inBias				,
									bool					inBiasClamp			,	
									bool					onEdgeLoad			,	
									AnisotropicFilterLevel	inAnisoFilterLevel	)
{
	lod_bias 	= inBias;
	bias_clamp 	= inBiasClamp;
	do_edge_lod	= onEdgeLoad;
	aFilter 	= inAnisoFilterLevel;
	changed 	= TRUE;
}
													

bool 
DpyState_NGC::FixTexture(	DpySource & 			inSrc, 
							uint 					inMode ,
							float					inBias				,
							bool					inBiasClamp			,
							bool					onEdgeLoad			,
							AnisotropicFilterLevel	inAnisoFilterLevel	, 
							uint					inTexId )
{
		
	NvkBitmap * srcBmp = (inSrc.Bitmap) ? inSrc.Bitmap->GetKInterface() : NULL;

	GXTexWrapMode	wrapS;
	GXTexWrapMode	wrapT;
	GXTexFilter 	minFilter;
	GXTexFilter 	magFilter;
	
	uint16 			width;
	uint16 			height;
	GXTexFmt 		pfm;
	void * 			idata;
	float			min_lod = 0.0f;
    float			max_lod = 0.0f;
	    
	if (srcBmp) {
		// Always update the source bitmap
		//srcBmp->UpdateBeforeDraw();	

		width	= uint16 (srcBmp->GetWidth() );
		height	= uint16 (srcBmp->GetHeight());
		
		pfm		= srcBmp->GetPixelFormat();
		
		idata 	= srcBmp->GetTexel();
		DCFlushRange(idata, (width * height * pfmbitsSizeOf(pfm)) / 8);
		
		max_lod = srcBmp->GetMipmapCpt() - 1 ;
		
	}
	else if (inSrc.Raster != DPYR_NULL){
		uint	w,h ; 
		DpyManager::GetRasterTex (inSrc.Raster ,pfm,w,h,&idata);											
		width	= w;
		height	= h;
		/*pfm		= GX_TF_RGBA8;
		idata 	= NULL;*/
		// TO DO offsreen as texture
	}
	else 
		return FALSE;
	
	if (idata && width && height) {
		// tex-wrapping-mode
		static const GXTexWrapMode  GX_WRAP_MODE[] = {
			GX_REPEAT, GX_REPEAT,
			GX_REPEAT, GX_CLAMP,
			GX_CLAMP,  GX_REPEAT,
			GX_CLAMP,  GX_CLAMP,			
		};

		uint tw =(((inMode & TW_MASK) >> 7 ) -2); 	// == to (((mode & TW_MASK)>>8)-1)<<1;			
		wrapS = GX_WRAP_MODE[tw	 ];
		wrapT = GX_WRAP_MODE[tw+1];			

#ifdef _DEBUG 
	if (wrapS == GX_REPEAT && !IsPow2(uint32(width)) ) {
		wrapS = GX_CLAMP;
		DebugPrintf("Warning, texture wrap mode pass to Clamp ( no pow of two Size )\n");
	}
		
	if ( wrapT == GX_REPEAT && !IsPow2(uint32(height)) ) {
		wrapT = GX_CLAMP;
		DebugPrintf("Warning, texture wrap mode pass to Clamp ( no pow of two Size )\n");
	}
#endif 
		// tex-filter
		static const GXTexFilter  	GX_FILTER_MODE[] = {
			GX_NEAR			,
			GX_LINEAR		,
			GX_NEAR_MIP_NEAR,
			GX_NEAR_MIP_LIN ,
			GX_LIN_MIP_NEAR ,
			GX_LIN_MIP_LIN	,
		};
		
		static const GXTexFilter  	GX_FILTER_MODE_NO_MIP[] = {
			GX_NEAR			,
			GX_LINEAR		,
			GX_NEAR			,
			GX_NEAR 		,
			GX_LINEAR 		,
			GX_LINEAR		,
		};
		
		// tex-mag-sampler
		uint tm = ((inMode & TM_MASK) >> 3) -1;
		// tex-reduce-sampler
		uint tr = ((inMode & TR_MASK) >> 5) -1;
		if (max_lod != min_lod)	
		{
			magFilter = GX_FILTER_MODE[tm];			
			minFilter = GX_FILTER_MODE[tr];
		}
		else 
		{
			magFilter = GX_FILTER_MODE_NO_MIP[tm];			
			minFilter = GX_FILTER_MODE_NO_MIP[tr];
		}
	    
	    NV_COMPILE_TIME_ASSERT( GX_TEXMAP0 == 0 && GX_TEXMAP1 == 1 && GX_TEXMAP2 == 2 && GX_TEXMAP3 == 3 && GX_TEXMAP4 == 4 );
	    NV_COMPILE_TIME_ASSERT( GX_TEXCOORD0 == 0 && GX_TEXCOORD1 == 1 && GX_TEXCOORD2 == 2 && GX_TEXCOORD3 == 3 && GX_TEXCOORD4 == 4 );
    
    	NV_COMPILE_TIME_ASSERT( AFL_A1 == GX_ANISO_1);
		NV_COMPILE_TIME_ASSERT( AFL_A2 == GX_ANISO_2);
		NV_COMPILE_TIME_ASSERT( AFL_A4 == GX_ANISO_4);
		
	    GDSetTexLookupMode 	(GXTexMapID(GX_TEXMAP0+inTexId),wrapS,wrapT,minFilter, magFilter, min_lod,max_lod, inBias,inBiasClamp,onEdgeLoad,GXAnisotropy(inAnisoFilterLevel));    
	    GDSetTexImgAttr 	(GXTexMapID(GX_TEXMAP0+inTexId),width,height,pfm);
	    GDSetTexImgPtr  	(GXTexMapID(GX_TEXMAP0+inTexId),CachedPointer(idata) );

		GDSetTexCoordScaleAndTOEs (GXTexCoordID(GX_TEXCOORD0+inTexId),width,GX_FALSE, GX_FALSE,
   												height,GX_FALSE,GX_FALSE,
												GX_FALSE, GX_FALSE );
    
	    return TRUE;
	}
	else 
		return FALSE;
			
}												

