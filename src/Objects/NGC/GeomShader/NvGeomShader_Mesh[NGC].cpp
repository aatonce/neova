/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
#include "NvGeomShader_Base[NGC].h"
#include <Kernel/NGC/NvkBitmap[NGC].h>
#include <Kernel/NGC/NvkMesh[NGC].h>
#include <Kernel/NGC/NvDpyState[NGC].h>
using namespace nv;


//#define	TEST_CLIPPING_MODE
//#define	DISABLE_TEXTURING
//#define	DISABLE_LIGHTING


namespace
{		
	interface Shader : public NvGeomShaderBase
	{
		struct Surface {
			DpyState_NGC		dpystate;
			Vec4				ambient	;			// in [0,1]^4
			Vec4				diffuse	;			// in [0,1]^4
			bool				hidden	;
		};

		uint32					enflags;			// enable flags (EN_...)
		NvkMesh*				mesh;
		uint					surfCpt;
		Surface*				surf;
		NvkMesh::Vtx* 			mvtx;
		NvkMesh::Bunch* 		mbunch;
		bool					tolit;
	
		virtual	~	Shader	(	) {}				// needed by nv::DestructInPlace<T>(ptr)

	
		void
		_Init		(	NvMesh*		inMesh		)
		{
			InitDpyObject();
			
			enflags    = NvShader::EN_DEFAULT;
			#ifdef DISABLE_TEXTURING
			enflags   ^= NvShader::EN_TEXTURING;
			#endif

			mesh = inMesh->GetKInterface();
			mesh->AddRef();

			mvtx   = mesh->GetVtx();
			NV_ASSERT( mvtx->vtxFmt <= 255 );
			mbunch = mesh->GetBunchA();
			tolit  = mesh->IsLightable();
			NV_ASSERT_IF( tolit, (mvtx->vtxFmt & vtxfmt::CO_NRM_MASK) );

			// Init capabilities
			uint dpycaps = 0;
			if( tolit )									dpycaps |= DpyState_NGC::CF_HAS_NORMAL;
			if( mvtx->vtxFmt & vtxfmt::CO_TEX_MASK )	dpycaps |= DpyState_NGC::CF_HAS_TEX0;
			if( mvtx->vtxFmt & vtxfmt::CO_COL_MASK )	dpycaps |= DpyState_NGC::CF_HAS_RGBA;

			// Setup internal surfaces
			NvkMesh::Shading* shadingA = mesh->GetSurfaceShadingA();
			for( uint i = 0 ; i < surfCpt ; i++ ) {
				// hidden
				surf[i].hidden = FALSE;

				// Init dpystate
				ConstructInPlace( &surf[i].dpystate );
				surf[i].dpystate.Init();
				surf[i].dpystate.SetCapabilities	( dpycaps );
				surf[i].dpystate.SetEnabled			( shadingA[i].stateEnFlags & enflags );
				surf[i].dpystate.SetMode			( shadingA[i].stateMdFlags );
				surf[i].dpystate.SetAlphaPass		( shadingA[i].stateAlphaPass );
				surf[i].dpystate.SetBlendSrcCte		( shadingA[i].stateBlendCte );
				surf[i].dpystate.SetTexturing		( shadingA[i].bitmapUID, 0, 0 );
				surf[i].dpystate.SetDefaultColor	( ConvertPSM(PSM_RGBA32, PSM_ABGR32,  shadingA[i].diffuse ) );

				ExtractRGBA( surf[i].ambient,  PSM_ABGR32, shadingA[i].ambient  );
				ExtractRGBA( surf[i].diffuse,  PSM_ABGR32, shadingA[i].diffuse  );
				
			}	
		}


		void
		Release	(		)
		{
			if( ShutDpyObject() ) {			
				for( uint i = 0 ; i < surfCpt ; i++ ) {
					surf[i].dpystate.Shut();
				}
				SafeRelease( mesh );
				NvEngineDelete( this );
			}
		}


		NvResource*
		GetResource		(					)
		{
			return mesh->GetInterface();			
		}


		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}


		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}


		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				for( uint i = 0 ; i < surfCpt ; i++ ) {			
					surf[i].dpystate.SetEnabled( inEnableFlags );
				}
			}
		}


		uint32
		GetEnabled		(		)
		{
			return enflags;
		}


		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfCpt )
				return FALSE;
			surf[inIndex].hidden = TRUE;
			return TRUE;
		}


		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfCpt )
				return FALSE;
			surf[inIndex].hidden = FALSE;
			return TRUE;
		}


		bool
		HideAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfCpt ; i++ )
				HideSurface( i );
			return TRUE;
		}


		bool
		ShowAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfCpt ; i++ )
				ShowSurface( i );
			return TRUE;
		}


		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return ( inIndex<surfCpt ) ? &surf[inIndex].dpystate : NULL;
		}


		bool
		SetAmbient		(	uint			inIndex,
							Vec4*			inColor					)
		{
			if( !tolit || inIndex>=surfCpt || !inColor)
				return FALSE;
			Vec4Copy( &surf[inIndex].ambient, inColor );
			return TRUE;			
		}


		bool
		SetDiffuse		(	uint			inIndex,
							Vec4*			inColor					)
		{
			if( inIndex>=surfCpt || !inColor)
				return FALSE;
				
			Vec4Copy( &surf[inIndex].diffuse, inColor );
			
			surf[inIndex].dpystate.SetDefaultColor	(GetPSM(PSM_RGBA32,*inColor));
			
			return TRUE;
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );
			
			mesh->UpdateBeforeDraw();
			
			// 2D ?
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;

			// Clipping
			GXClipMode clipping = !mode2D && (enflags&NvShader::EN_CLIPPING) ? GX_CLIP_ENABLE : GX_CLIP_DISABLE ;
			#ifdef TEST_CLIPPING_MODE
			clipping = GX_CLIP_ENABLE;
			#endif
			GXSetClipMode( clipping );

			// Lighting
			bool dolit = tolit;
			#ifdef DISABLE_LIGHTING
			dolit = FALSE;
			#endif 
			if( dolit ) {
				GXSetChanCtrl(	GX_COLOR0,GX_TRUE ,GX_SRC_REG,GX_SRC_REG,GX_LIGHT0 | GX_LIGHT1, GX_DF_CLAMP,GX_AF_NONE	);		
				GXSetChanCtrl(	GX_ALPHA0,GX_FALSE,GX_SRC_REG,GX_SRC_REG,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE			);				
			}
			else {
				GXSetChanCtrl( GX_COLOR0,GX_FALSE,GX_SRC_VTX,GX_SRC_VTX,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE );
				GXSetChanCtrl( GX_ALPHA0,GX_FALSE,GX_SRC_VTX,GX_SRC_VTX,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE );
			}
			
			int chainIdx     = dpyctxt.chainHead;
			int lastLightIdx = -1;
			NV_ASSERT( chainIdx >= 0 );
			while( chainIdx >= 0 )
			{
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
				NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()		   + dctxt->lightIdx;
				Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;
				bool 						mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;				
			    
				GXLightObj 					gxLight1,
											gxLight2;
														    
				if( dolit && (dctxt->lightIdx!=lastLightIdx) ) {
					// Diffuse/Specular lights
					Vec3	lightPos1;
					Vec3	lightPos2;					
					Vec3ApplyVector	( &lightPos1, (Vec3*)(dlight->direction  ), &dview->viewTR,-1.0f);
					Vec3ApplyVector	( &lightPos2, (Vec3*)(dlight->direction+1), &dview->viewTR,-1.0f);
					
					bool validLight1 = lightPos1 !=Vec3::ZERO;
					bool validLight2 = lightPos2 !=Vec3::ZERO;					

					lightPos1 *= DpyManager::LIGHT_SCALE_VALUE;
					lightPos2 *= DpyManager::LIGHT_SCALE_VALUE;
					
					GXInitLightPos	( &gxLight1,lightPos1.x,lightPos1.y,lightPos1.z );
					GXInitLightPos	( &gxLight2,lightPos2.x,lightPos2.y,lightPos2.z );
					
					GXColor		gxLColor1,gxLColor2;
					GekColor	tmpColor;
					tmpColor.c =  validLight1? GetPSM(PSM_RGBA32,dlight->color[1]): 0x00000000;
					gxLColor1  = tmpColor.gxc;
					tmpColor.c =  validLight2? GetPSM(PSM_RGBA32,dlight->color[2]): 0x00000000;
					gxLColor2  = tmpColor.gxc;
					
					GXInitLightColor( &gxLight1,gxLColor1 );
					GXInitLightColor( &gxLight2,gxLColor2 );
					
					GXLoadLightObjImm(&gxLight1,GX_LIGHT0 );
					GXLoadLightObjImm(&gxLight2,GX_LIGHT1 );

					Matrix 	  toView;
					if (!mode2D)
		   				MatrixMul	( &toView,dwtr,&dview->viewTR );
		   			else 
						MatrixCopy	( &toView,dwtr);
					
					Matrix invToView;
					MatrixFastInverse	( &invToView, 	&toView );
					GXLoadNrmMtxImm(invToView.m,GX_PNMTX0);					
					
					lastLightIdx = dctxt->lightIdx;
				}
				
				DpyManager::SetupDrawing (	DpyRaster(dctxt->raster),dview,dwtr, mode2D );

				vtxfmt::Activate( mvtx->vtxFmt, mvtx->locFrac );

				if( mvtx->compAddr[0] )		GXSetArray( GX_VA_POS,	mvtx->compAddr[0], mvtx->compBStride[0] );
				if( mvtx->compAddr[1] )		GXSetArray( GX_VA_TEX0, mvtx->compAddr[1], mvtx->compBStride[1] );
				if( mvtx->compAddr[2] )		GXSetArray( GX_VA_CLR0, mvtx->compAddr[2], mvtx->compBStride[2] );
				if( mvtx->compAddr[3] )		GXSetArray( GX_VA_NRM,	mvtx->compAddr[3], mvtx->compBStride[3] );
				GXInvalidateVtxCache();										

				for( uint i = 0 ; i < surfCpt ; i++ )
				{
					if( surf[i].hidden )
						continue;

					// Setup material parameters
					if( dolit ) {
						GXColor		gxAmbient;
						Vec4		ambColor;
						Vec4Mul(&ambColor,dlight->color,&surf[i].ambient);
						GekColor	tmpColor;
						tmpColor.c  =  GetPSM(PSM_RGBA32,ambColor);
						gxAmbient 	= tmpColor.gxc;
						
						GXColor	gxMat,gxAmb;
						tmpColor.c =  GetPSM(PSM_RGBA32,surf[i].diffuse);
						gxMat = tmpColor.gxc;
						gxAmb.r = 0x00; gxAmb.g = 0x00; gxAmb.b = 0x00 ; gxAmb.a = 0xFF;
						
						GXSetChanMatColor(GX_COLOR0A0,gxMat);					
						GXSetChanAmbColor(GX_COLOR0A0,gxAmb);

						// Ambiant
						GXSetTevColor(GX_TEVREG0,gxAmbient);
					}
					Vec2 uvOffset;
					surf[i].dpystate.GetSourceOffset(uvOffset);
					if (uvOffset.x!=0 || uvOffset.y!=0)
					{
						GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4,GX_TG_TEX0, GX_TEXMTX0);
						Matrix msTr;
						MatrixIdentity(&msTr);
						msTr.m14 = uvOffset.x;
						msTr.m24 = uvOffset.y;
						GXLoadTexMtxImm		(msTr.m, GX_TEXMTX0 , GX_MTX3x4 );
					}

					surf[i].dpystate.Activate();

					// Setup drawing
					GXCallDisplayList( mbunch[i].dlistAddr, mbunch[i].dlistBSize );
					if (uvOffset.x!=0 || uvOffset.y!=0)
						GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX3x4, GX_TG_TEX0, GX_IDENTITY);		
				}
				chainIdx = dchain->next;
			}
			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			//return FALSE;
			outTriCpt	= 0;
			outLocCpt	= 0;
			outVtxCpt	= 0;
			outPrimCpt	= 0;
			for( uint i = 0 ; i < surfCpt ; i++ ) {
				outTriCpt  += mbunch[i].triCpt * dpyctxt.chainCpt;
				outLocCpt  += mbunch[i].locCpt  * dpyctxt.chainCpt;
				outVtxCpt  += mbunch[i].vtxCpt  * dpyctxt.chainCpt;
				outPrimCpt += mbunch[i].primCpt * dpyctxt.chainCpt;
			}
			return TRUE;
		}
		
	};
};


NvGeomShader*
nv_geomshader_mesh_Create	(	NvMesh*		inMesh		)
{
	if( !inMesh )
		return NULL;

	uint surfCpt = inMesh->GetSurfaceCpt();
	
	uint supply  = 32 + sizeof(Shader::Surface)  * surfCpt;
	Shader* shader = NvEngineNewS( Shader, supply );
	if( !shader )
		return NULL;

	shader->surfCpt = surfCpt;
	shader->surf    =(Shader::Surface*) Round32(shader+1);
	
	shader->_Init( inMesh );
	return shader->GetInterface();
}




