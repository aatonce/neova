/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#include <NvMesh.h>
#include <NvSurface.h>
#include "NvGeomShader_Base[NGC].h"


NvGeomShader*	nv_geomshader_mesh_Create	(	NvMesh*		inMesh	);
NvGeomShader*	nv_geomshader_surf_Create	(	NvSurface*	inRsc	);


NvGeomShader*
NvGeomShader::Create	(	NvResource*		inRsc	)
{
	if( !inRsc )
		return NULL;
	if( inRsc->GetRscType() == NvMesh::TYPE )
		return nv_geomshader_mesh_Create( (NvMesh*)inRsc );
	if( inRsc->GetRscType() == NvSurface::TYPE )
		return nv_geomshader_surf_Create( (NvSurface*)inRsc );
	return NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( GeomShader,	NvInterface*,	GetBase												)
NVITF_CAL0( GeomShader,	void,			AddRef												)
NVITF_MTH0( GeomShader,	uint,			GetRefCpt											)
NVITF_CAL0( GeomShader,	void,			Release												)
NVITF_MTH0( GeomShader,	NvResource*,	GetResource											)
NVITF_CAL1( GeomShader,	void,			Enable,				uint32							)
NVITF_CAL1( GeomShader,	void,			Disable,			uint32							)
NVITF_CAL1( GeomShader,	void,			SetEnabled,			uint32							)
NVITF_MTH0( GeomShader,	uint32,			GetEnabled											)
NVITF_MTH1( GeomShader,	DpyState*,		GetDisplayState,	uint							)
NVITF_MTH1( GeomShader,	bool,			HideSurface,		uint							)
NVITF_MTH1( GeomShader,	bool,			ShowSurface,		uint							)
NVITF_MTH0( GeomShader,	bool,			HideAllSurfaces										)
NVITF_MTH0( GeomShader,	bool,			ShowAllSurfaces										)
NVITF_MTH0( GeomShader,	uint,			GetDrawStride										)
NVITF_MTH1( GeomShader,	bool,			SetDrawStart,		uint							)
NVITF_MTH1( GeomShader,	bool,			SetDrawSize,		uint							)
NVITF_MTH2( GeomShader,	bool,			SetAmbient,			uint,	Vec4*					)
NVITF_MTH2( GeomShader,	bool,			SetDiffuse,			uint,	Vec4*					)




