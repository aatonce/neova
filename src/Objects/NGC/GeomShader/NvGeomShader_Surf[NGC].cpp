/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
#include <Kernel/NGC/NvkBitmap[NGC].h>
#include <Kernel/NGC/NvkSurface[NGC].h>
#include <Kernel/NGC/NvDpyState[NGC].h>
#include "NvGeomShader_Base[NGC].h"

using namespace nv;


//#define	DISABLE_TEXTURING
//#define 	TEST_CLIPPING_MODE
//#define 	DISABLE_LIGHTING

namespace
{	
	static const uint PrimitiveTranslation[] = 
	{
		GX_DRAW_TRIANGLE_STRIP,
		GX_DRAW_LINES,
		GX_DRAW_LINE_STRIP,
		GX_DRAW_POINTS,
	};
	
	interface Shader : public NvGeomShaderBase
	{
	
		DpyState_NGC		dpystate;
		uint32				enflags;				// enable flags (EN_...)
		NvkSurface*			surf;
		uint				drawStart;
		uint				drawSize;
		uint32				currentDLVersion;		// Current display list version (compare with surface)
		uint16 * 			displayList;			// surfaceDisplayList for [drawStart,drawStart+draxSize] vertex.
		uint32				displayListBSize;
		uint32				displayListDrawSize;
		int					vtxFMTid;
		bool				dlMustBeUpdate;			// if drawstart or drawSize change, display list must be update.		
		bool				hidden;
		PrimitiveType		primitiveType;

		void
		_Init		(	NvSurface*		inSurface	)
		{
			InitDpyObject();
			
			enflags    = NvShader::EN_DEFAULT;
			#ifdef DISABLE_TEXTURING
			enflags   ^= NvShader::EN_TEXTURING;
			#endif

			surf = inSurface->GetKInterface();
			surf->AddRef();
			drawStart = 0;
			drawSize  = 0;

			hidden = FALSE;

			// rebuild dpystate capabilities
			uint dpycaps = 0;
			if( surf->HasComponent(NvSurface::CO_NORMAL) )	dpycaps |= DpyState_NGC::CF_HAS_NORMAL;
			if( surf->HasComponent(NvSurface::CO_TEX) )		dpycaps |= DpyState_NGC::CF_HAS_TEX0;
			if( surf->HasComponent(NvSurface::CO_COLOR) )	dpycaps |= DpyState_NGC::CF_HAS_RGBA;

			// init dpystate
			dpystate.Init();
			dpystate.SetEnabled( enflags );
			dpystate.SetCapabilities( dpycaps );
			dlMustBeUpdate 		= TRUE;
			currentDLVersion 	= 0xFFFFFFFF;
			displayListBSize 	= 0;
			displayListDrawSize = 0;
			displayList			= NULL;
			primitiveType		= PT_TriangleStrip;
		}
		
		void
		Release	(		)
		{
			if( ShutDpyObject() ) {
				dpystate.Shut();
				SafeFree(displayList);
				SafeRelease( surf );
				NvEngineDelete( this );
			}
		}
		
		NvResource*
		GetResource		(					)
		{
			return surf->GetInterface();
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{			
			SetEnabled( GetEnabled() | inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( GetEnabled()&(~inEnableFlags) );
		}
		
		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			enflags = inEnableFlags;
			dpystate.SetEnabled( inEnableFlags );
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}

		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex != 0 )
				return FALSE;
			hidden = TRUE;
			return TRUE;
		}

		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex != 0 )
				return FALSE;
			hidden = FALSE;
			return TRUE;
		}

		bool
		HideAllSurfaces		(					)
		{
			hidden = TRUE;
			return TRUE;
		}

		bool
		ShowAllSurfaces		(					)
		{
			hidden = FALSE;
			return TRUE;
		}
		
		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return (inIndex==0) ? &dpystate : NULL;
		}

		uint
		GetDrawStride	(			)
		{
			return 1;
		}

		bool
		SetDrawStart	(	uint			inOffset		)
		{
			drawStart = inOffset;
			dlMustBeUpdate = TRUE;
			return TRUE;
		}

		bool
		SetDrawSize		(	uint			inSize			)
		{
			drawSize = inSize;
			dlMustBeUpdate = TRUE;
			return TRUE;
		}
		
		bool
		ValidDrawRange	(		)
		{
			uint drawLast = drawStart + drawSize;
			uint drawMax  = surf->GetMaxSize();
			return (drawSize>0 && drawLast<=drawMax);
		}
		
		bool
		SetPrimitiveType ( PrimitiveType 	inPt ) { 
			primitiveType = inPt;
			return TRUE;
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );

			if( hidden )
				return FALSE;

			surf->UpdateBeforeDraw();

			if( !ValidDrawRange() )
				return FALSE;

			// 2D ?
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;

			// Clipping
			GXClipMode clipping = !mode2D && (enflags&NvShader::EN_CLIPPING) ? GX_CLIP_ENABLE : GX_CLIP_DISABLE ;
			#ifdef TEST_CLIPPING_MODE
			clipping = GX_CLIP_ENABLE;
			#endif
			GXSetClipMode( clipping );
			
			// Lighting
			bool lighting = surf->HasComponent(NvSurface::CO_NORMAL);
			#ifdef DISABLE_LIGHTING
			lighting = FALSE;
			#endif
			
			// Update Display List!
			dlMustBeUpdate |= !surf->IsUptoDate(&currentDLVersion);			
			if (!displayList || dlMustBeUpdate) {
				displayListDrawSize = surf->GetDisplayListBSize(NvkSurface::BF_BACK,drawStart,drawSize);
				 
				if ( displayListBSize < displayListDrawSize && displayListDrawSize > 0)  {
					SafeFree(displayList);
					displayListBSize = displayListDrawSize;
					displayList	= (uint16*) NvMalloc(displayListBSize);			
				}
				if (!displayListDrawSize) return FALSE;
		
				NV_ASSERT_A256(displayList);
				uint tmpsize = surf->GetBufferDrawing( NvkSurface::BF_BACK, drawStart,drawSize, &displayList , &vtxFMTid );								
				NV_ASSERT(tmpsize == displayListDrawSize);				
				dlMustBeUpdate = FALSE;
			}

			if (!displayList || !displayListDrawSize) return FALSE;			
			

			GXVtxFmt fmt = vtxfmt::Activate	( vtxFMTid);
		
			uint stride ;
			uint8 * ptr ;
			uint psm;
			ptr =surf->GetComponentBase	(NvkSurface::BF_BACK,NvSurface::CO_LOC,&stride,&psm);							
			if (ptr)	GXSetArray( GX_VA_POS , ptr, stride );
			
			ptr =surf->GetComponentBase	(NvkSurface::BF_BACK,NvSurface::CO_TEX,&stride,&psm);							
			if (ptr)	GXSetArray( GX_VA_TEX0, ptr, stride );
			
			ptr =surf->GetComponentBase	(NvkSurface::BF_BACK,NvSurface::CO_COLOR,&stride,&psm);							
			if (ptr)	GXSetArray( GX_VA_CLR0, ptr, stride );
			
			ptr =surf->GetComponentBase	(NvkSurface::BF_BACK,NvSurface::CO_NORMAL,&stride,&psm);							
			if (ptr)	GXSetArray( GX_VA_NRM , ptr, stride );						
			
			((uint8*)displayList)[0] = uint8(PrimitiveTranslation[uint(primitiveType)] | fmt);
			GXInvalidateVtxCache();				
			DCFlushRange( displayList, displayListDrawSize );
			
			Vec2 uvOffset;
			dpystate.GetSourceOffset(uvOffset);
			if (uvOffset.x!=0 || uvOffset.y!=0)
			{
				GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4,GX_TG_TEX0, GX_TEXMTX0);
				Matrix msTr;
				MatrixIdentity(&msTr);
				msTr.m14 = uvOffset.x;
				msTr.m24 = uvOffset.y;
				GXLoadTexMtxImm		(msTr.m, GX_TEXMTX0 , GX_MTX3x4 );
			}
					
			dpystate.Activate	( );
	
			int chainIdx = dpyctxt.chainHead;
			NV_ASSERT( chainIdx >= 0 );
			while( chainIdx >= 0 )
			{
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
				Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;
				NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()   + dctxt->lightIdx;				
				bool 						mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;   				
				
				if( lighting ) {
					Vec3	lightPos1;
					Vec3	lightPos2;					
					Vec3ApplyVector	( &lightPos1, (Vec3*)(dlight->direction  ), &dview->viewTR,-1.0f);
					Vec3ApplyVector	( &lightPos2, (Vec3*)(dlight->direction+1), &dview->viewTR,-1.0f);											
					Vec3Normalize	( &lightPos1, &lightPos1);
					Vec3Normalize	( &lightPos2, &lightPos2);
					lightPos1 *= DpyManager::LIGHT_SCALE_VALUE;
					lightPos2 *= DpyManager::LIGHT_SCALE_VALUE;
					
					GXLightObj 	gxLight1,gxLight2;
					GXColor		gxColor1,gxColor2;
					GXColor		gxAmbient;
					
					Vec4	ambFilter (0.00f,0.00f,0.75f,1.0f);
					Vec4	difFilter (1.0f,1.0f,1.0f,1.0f);					
					Vec4	ambColor ;
					Vec4Mul(&ambColor,dlight->color,&ambFilter);
					GekColor tmpColor;
					tmpColor.c 	= GetPSM(PSM_RGBA32,ambColor);
					gxAmbient 	= tmpColor.gxc;
					tmpColor.c 	= GetPSM(PSM_RGBA32,dlight->color[1]);
					gxColor1 	= tmpColor.gxc;
					tmpColor.c 	= GetPSM(PSM_RGBA32,dlight->color[2]);
					gxColor2 	= tmpColor.gxc;					
										
					GXInitLightColor( &gxLight1,gxColor1 );
					GXInitLightColor( &gxLight2,gxColor2 );
					GXInitLightPos	( &gxLight1,lightPos1.x,lightPos1.y,lightPos1.z );
					GXInitLightPos	( &gxLight2,lightPos2.x,lightPos2.y,lightPos2.z );
					
					GXColor	gxMat,gxAmb;
					tmpColor.c 	= GetPSM(PSM_RGBA32,difFilter);
					gxMat = tmpColor.gxc;
					gxAmb.r = 0x00; gxAmb.g = 0x00; gxAmb.b = 0x00 ; gxAmb.a = 0xFF;										
					
					GXSetChanMatColor(GX_COLOR0A0,gxMat);					
					GXSetChanAmbColor(GX_COLOR0A0,gxAmb);
					GXLoadLightObjImm(&gxLight1,GX_LIGHT0 );
					GXLoadLightObjImm(&gxLight2,GX_LIGHT1 );
					// Light1 + Light2
					GXSetChanCtrl(	GX_COLOR0,GX_TRUE ,GX_SRC_REG,GX_SRC_REG,GX_LIGHT0 | GX_LIGHT1, GX_DF_CLAMP,GX_AF_NONE	);
					GXSetChanCtrl(	GX_ALPHA0,GX_FALSE,GX_SRC_REG,GX_SRC_REG,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE			);
					// Ambiant
					GXSetTevColor(GX_TEVREG0,gxAmbient);

					Matrix 	  toView;
					if (!mode2D)
		   				MatrixMul	( &toView,dwtr,&dview->viewTR );
		   			else 
						MatrixCopy	( &toView,dwtr);
											
					Matrix invToView;					
					MatrixFastInverse	( &invToView, 	&toView );
					GXLoadNrmMtxImm(invToView.m,GX_PNMTX0);
					
				}
				else {
					GXSetChanCtrl( GX_COLOR0,GX_FALSE,GX_SRC_VTX,GX_SRC_VTX,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE );
					GXSetChanCtrl( GX_ALPHA0,GX_FALSE,GX_SRC_VTX,GX_SRC_VTX,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE );
				}
			
				DpyManager::SetupDrawing (	DpyRaster(dctxt->raster),dview,dwtr, mode2D );
				
				// draw 
				GXCallDisplayList(displayList,displayListDrawSize);
				
				chainIdx = dchain->next;
			}
			if (uvOffset.x!=0 || uvOffset.y!=0)
						GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX3x4, GX_TG_TEX0, GX_IDENTITY);		
			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxWeldCpt,
								uint32&		outVtxProcCpt	)
		{
			if( !ValidDrawRange() )		return FALSE;
			return FALSE;
		}
	};
};
	


NvGeomShader*
nv_geomshader_surf_Create	(	NvSurface*		inSurface		)
{
	if( !inSurface )
		return NULL;

	// At least some components are needed !
	if( !inSurface->HasComponent(NvSurface::CO_LOC) )
		return NULL;
	
	Shader* shader = NvEngineNew( Shader );
	if( !shader )
		return NULL;

	shader->_Init( inSurface );
	return shader->GetInterface();
}





