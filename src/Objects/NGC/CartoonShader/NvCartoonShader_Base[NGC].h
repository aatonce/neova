/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifndef	_NvCartoonShaderBase_NGC_H_
#define	_NvCartoonShaderBase_NGC_H_


#include <NvCartoonShader.h>
#include <Kernel/NGC/NvDpyObject[NGC].h>


//
// BASE implementation


struct NvCartoonShaderBase : public NvDpyObject_NGC
{
private:
	friend struct NvCartoonShader;
	NvCartoonShader	itf;
public:
			NvCartoonShader*		GetInterface			(									) { return &itf;  }
	virtual 					~	NvCartoonShaderBase		(									) {}
	virtual	void					Release					(									) = 0;
	virtual NvResource*				GetResource				(									) = 0;
	virtual	void					Enable					(	uint32		inEnableFlags		) = 0;
	virtual	void					Disable					(	uint32		inEnableFlags		) = 0;
	virtual	void					SetEnabled				(	uint32		inEnableFlags		) = 0;
	virtual	uint32					GetEnabled				(									) = 0;

	virtual bool					HideSurface				(	uint			inSurfaceIndex	) = 0;
	virtual bool					ShowSurface				(	uint			inSurfaceIndex	) = 0;
	virtual bool					HideAllSurfaces			(									) = 0;
	virtual bool					ShowAllSurfaces			(									) = 0;
	virtual DpyState*				GetDisplayState			(	uint			inIndex			) = 0;
	virtual	uint					GetDrawStride			(									) { return 0;	  }
	virtual	bool					SetDrawStart			(	uint			inOffset		) { return FALSE; }
	virtual	bool					SetDrawSize				(	uint			inSize			) { return FALSE; }
	virtual	bool					SetAmbient				(	uint			inIndex,
																Vec4*			inColor			) = 0;
	virtual	bool					SetDiffuse				(	uint			inSurfaceIndex,
																Vec4*			inColor			) = 0;
	virtual void					SetShadingLevel			(	float			inLevel			) = 0;
	virtual	void					SetInking				(	bool			inOnOff			) = 0;
	virtual void					SetInkingRamp			(	float			inStartLevel,
																float			inEndLevel		) = 0;
	virtual	void					SetOutlining			(	bool			inOnOff			) = 0;
	virtual	void					SetOutliningThickness	(	float			inThickness		) = 0;
	virtual	void					SetOutliningQuality		(	float			inQuality		) = 0;
};





#endif // _NvCartoonShaderBase_NGC_H_



