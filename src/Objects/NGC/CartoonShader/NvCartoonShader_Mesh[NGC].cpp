/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
#include <Kernel/NGC/NvkBitmap[NGC].h>
#include <Kernel/NGC/NvkMesh[NGC].h>
#include <Kernel/NGC/NvDpyState[NGC].h>
#include <Kernel/NGC/NvkImage[NGC].h>
#include "NvCartoonShader_Base[NGC].h"
using namespace nv;


namespace
{	
	//
	// BASE		
	struct Shader : public NvCartoonShaderBase
	{
		struct Surface {
			DpyState_NGC		dpystate;
			Vec4				ambient;			// in [0,1]^4
			Vec4				diffuse;			// in [0,1]^4
			bool				hidden;
		};

		uint32					enflags;			// enable flags (EN_...)
		NvkMesh*				mesh;
		uint					surfCpt;
		Surface*				surf;
		NvkMesh::Vtx* 			mvtx;
		NvkMesh::Bunch* 		mbunch;
		
		Vec4 *					lightColor;
		
		float					shdlevel;		
		bool					inkstate;
		float					inkstart;
		float					inkstop;

		bool					outlineState;
		float					outlineThickness;
		float					outlineQuality;



		virtual	~	Shader	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init	(	NvMesh*		inMesh	)
		{
			InitDpyObject();
			
			enflags    = NvShader::EN_DEFAULT;
			mesh = inMesh->GetKInterface();
			mesh->AddRef();

			mvtx   = mesh->GetVtx();
			mbunch = mesh->GetBunchA();
			
			NV_ASSERT(mvtx->vtxFmt & vtxfmt::CO_NRM_MASK);
			
			// Init capabilities
			uint dpycaps = 0;
			dpycaps |= DpyState_NGC::CF_HAS_NORMAL;
			if( mvtx->vtxFmt & vtxfmt::CO_TEX_MASK )	dpycaps |= DpyState_NGC::CF_HAS_TEX0;

			// Setup internal surfaces
			NvkMesh::Shading* shadingA = mesh->GetSurfaceShadingA();
			for( uint i = 0 ; i < surfCpt ; i++ ) {
				// hidden
				surf[i].hidden = FALSE;

				// Init dpystate
				ConstructInPlace( &surf[i].dpystate );
				surf[i].dpystate.Init();
				surf[i].dpystate.SetCapabilities	( dpycaps );
				surf[i].dpystate.SetEnabled			( shadingA[i].stateEnFlags & enflags );
				surf[i].dpystate.SetMode			( shadingA[i].stateMdFlags );
				surf[i].dpystate.SetAlphaPass		( shadingA[i].stateAlphaPass );
				surf[i].dpystate.SetBlendSrcCte		( shadingA[i].stateBlendCte );
				surf[i].dpystate.SetTexturing		( shadingA[i].bitmapUID, 0, 0 );

				ExtractRGBA( surf[i].ambient,  PSM_ABGR32, shadingA[i].ambient  );
				ExtractRGBA( surf[i].diffuse,  PSM_ABGR32, shadingA[i].diffuse  );
			}
			
			outlineState = FALSE;
			outlineThickness = NvCartoonShader::DEF_OUTL_THICKNESS;
			outlineQuality	 = NvCartoonShader::DEF_OUTL_QUALITY;			
			
			inkstate	= TRUE;
			shdlevel 	= float(NvCartoonShader::DEF_SHADLEVEL)     * 0.01f;
			inkstart 	= float(NvCartoonShader::DEF_INKRAMP_START) * 0.01f;
			inkstop	 	= float(NvCartoonShader::DEF_INKRAMP_END)   * 0.01f;
		}

		void
		AddRef		(		)
		{
			refCpt++;
		}
	
		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}
	
		void
		Release	(		)
		{			
			if( ShutDpyObject() ) {
				for( uint i = 0 ; i < surfCpt ; i++ )
					surf[i].dpystate.Shut();
				SafeRelease( mesh );			
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return mesh->GetInterface();
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}
	
		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				for( uint i = 0 ; i < surfCpt ; i++ ) {			
					surf[i].dpystate.SetEnabled( inEnableFlags );
				}
			}
		}
	
		uint32
		GetEnabled		(		)
		{
			return enflags;
		}

		bool
		HideSurface			(	uint			inSurfaceIndex	)
		{
			if( inSurfaceIndex >= surfCpt )
				return FALSE;
			surf[inSurfaceIndex].hidden = TRUE;
			return TRUE;
		}

		bool
		ShowSurface			(	uint			inSurfaceIndex	)
		{
			if( inSurfaceIndex >= surfCpt )
				return FALSE;
			surf[inSurfaceIndex].hidden = FALSE;
			return TRUE;
		}

		bool
		HideAllSurfaces		(									)
		{
			for( uint i = 0 ; i < surfCpt ; i++ )
				HideSurface( i );
			return TRUE;
		}

		bool
		ShowAllSurfaces		(									)
		{
			for( uint i = 0 ; i < surfCpt ; i++ )
				ShowSurface( i );
			return TRUE;
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return ( inIndex<surfCpt ) ? &surf[inIndex].dpystate : NULL;
		}

		bool
		SetAmbient		(	uint			inIndex,
							Vec4*			inColor					)
		{
			if( inIndex>=surfCpt || !inColor)
				return FALSE;
			Vec4Copy( &surf[inIndex].ambient, inColor );
			return TRUE;			
		}
		
		bool
		SetDiffuse		(	uint			inIndex					,
							Vec4*			inColor					)
		{
			if( inIndex>=surfCpt || !inColor)
				return FALSE;
			Vec4Copy( &surf[inIndex].diffuse, inColor );
			return TRUE;
		}

		void 
		SetShadingLevel (	float inLevel		)
		{
			shdlevel = Clamp(inLevel,0.0f,1.0f);
		}

		void 
		SetInkingRamp	(	float inStartLevel,
							float inEndLevel	)
		{
			inkstart = inStartLevel;
			inkstop	 = inEndLevel;		   
		}

		void
		SetInking			(	bool	inState			)
		{	
			if (inState != inkstate){		
				inkstate = inState;
				for( uint i = 0 ; i < surfCpt ; i++ )
					surf[i].dpystate.ForceUpdate();
			}
		}

		void
		SetOutlining		(	bool	inState			)
		{
			outlineState	= inState;
		}

		void
		SetOutliningThickness	(	float	inThickness		)
		{
			outlineThickness	= Max(inThickness , 0.0f );
		}

		void
		SetOutliningQuality	(	float			inQuality		)
		{
			outlineQuality = Clamp(inQuality,0.0f,1.0f);
		}

		void DrawOutline 	( NvDpyContext::View *  inDview ) 
		{
			NV_ASSERT(outlineState);
			NV_ASSERT(inDview);
			
			uint prevCullMode = 0;
			
			for( uint i = 0 ; i < surfCpt ; i++ )
			{
				if( surf[i].hidden )
					continue;

				uint cm = surf[i].dpystate.GetMode() & DpyState::CM_MASK;				
				if (prevCullMode != cm) {
					DpyState_NGC::ActivateOutlining(cm);
					prevCullMode = cm;
				}
				
				GXColor		gxOLColor = {0xFF,0xFF,0xFF,0x0};
				GXSetTevColor(GX_TEVREG0,gxOLColor);
			//	GXSetZMode(GX_FALSE,GX_NEVER,GX_FALSE);
				float nb_step   = (1.f-outlineQuality)*4.f + (32.f * outlineQuality);
				float angl_step = TwoPi / nb_step;
				float angl_base = Pi * 0.25f;
				for( float angl = 0.f ; angl < TwoPi ; angl += angl_step ) {
					float s, c;
					Sincosf( angl+angl_base, &s, &c );
					float shiftX =s* outlineThickness;
					float shiftY =c* outlineThickness;
					GXSetViewport(inDview->viewport.x + shiftX , inDview->viewport.y + shiftY, inDview->viewport.z, inDview->viewport.w, 0.0f, 1.0f);
					// Setup drawing
					GXCallDisplayList( mbunch[i].dlistAddr, mbunch[i].dlistBSize );					
				}
			}
		}
		
		bool
		Draw		(				)
		{
			NV_ASSERT( refCpt );
			
			mesh->UpdateBeforeDraw();		
			// 2D ?
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;

			// Clipping
			GXClipMode clipping = !mode2D && (enflags&NvShader::EN_CLIPPING) ? GX_CLIP_ENABLE : GX_CLIP_DISABLE ;
			#ifdef TEST_CLIPPING_MODE
			clipping = GX_CLIP_ENABLE;
			#endif
			GXSetClipMode( clipping );

			if (outlineState) {
				// clear alpha (using like a stencil buffer).
				GXSetColorUpdate(GX_FALSE);
				GXSetAlphaUpdate(GX_TRUE);
			    GXSetZMode		( FALSE, GX_ALWAYS, GX_TRUE );
				gx::ClearBack( 0x00000000);
			}
			float inkA = 0.0f, inkB = 0.0f;
			{
				// Light
				GXSetChanCtrl(	GX_COLOR0,GX_TRUE ,GX_SRC_REG,GX_SRC_REG,GX_LIGHT0 	  , GX_DF_CLAMP,GX_AF_NONE	);
				if (inkstate) {
					// RasterColor.A = (N*M).z * 255 => use light1
					GXSetChanCtrl(	GX_ALPHA0,GX_TRUE,GX_SRC_REG,GX_SRC_REG,GX_LIGHT1 , GX_DF_CLAMP,GX_AF_NONE	);
					
					// Diffuse/Specular lights
					GXLightObj gxLight1;
					GXInitLightPos	( &gxLight1,0.0f,0.0f,1.0f );
					GXColor gxColor = {0xFF,0xFF,0xFF,0xFF};
					GXInitLightColor( &gxLight1,gxColor );
					GXLoadLightObjImm(&gxLight1,GX_LIGHT1 );
					
					float dot_S   = Clamp( inkstart, 0.0f, 1.0f );
					float dot_E   = Clamp( inkstop, 0.0f, 1.0f );
						  dot_E   = Max( dot_S, dot_E );
					float dot_d   = (dot_E - dot_S);
					if( dot_d <= 0.f )
					   dot_d = 0.001f;			
					inkA   = 1.0f/float(dot_d);
					inkB   = -inkA*dot_S;
				}
				else 
					GXSetChanCtrl(	GX_ALPHA0,GX_FALSE,GX_SRC_REG,GX_SRC_REG,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE	);
					
				GXColor	gxMat,gxAmb;
				gxMat.r = 0xFF; gxMat.g = 0xFF; gxMat.b = 0xFF; gxMat.a = 0xFF;
				gxAmb.r = 0x00; gxAmb.g = 0x00; gxAmb.b = 0x00; gxAmb.a = 0x00;
				GXSetChanMatColor(GX_COLOR0A0,gxMat);
				GXSetChanAmbColor(GX_COLOR0A0,gxAmb);
			}
			
			int chainIdx     = dpyctxt.chainHead;
			int lastLightIdx = -1;
			NV_ASSERT( chainIdx >= 0 );

			while( chainIdx >= 0 )
			{
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
				NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()		   + dctxt->lightIdx;
				Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;
				bool 						mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;				
				GXLightObj 					gxLight0;


				if( (dctxt->lightIdx!=lastLightIdx) ) {
					// Diffuse/Specular lights
					Vec3	lightPos0;			
					Vec3ApplyVector	( &lightPos0, (Vec3*)(dlight->direction  ), &dview->viewTR,-1.0f);
					Vec3Normalize	( &lightPos0, &lightPos0);
					lightPos0 *= DpyManager::LIGHT_SCALE_VALUE;

					GXInitLightPos	( &gxLight0,lightPos0.x,lightPos0.y,lightPos0.z );
					GXColor gxColor = {0xFF,0xFF,0xFF,0xFF};
					GXInitLightColor( &gxLight0,gxColor );
					GXLoadLightObjImm(&gxLight0,GX_LIGHT0 );
					
					lastLightIdx = dctxt->lightIdx;
				}



				Matrix invToView;
				if (!mode2D) {
	   				MatrixMul			( &invToView,dwtr,&dview->viewTR );
					MatrixFastInverse	( &invToView, &invToView );	   				
	   			}
	   			else 
					MatrixFastInverse	( &invToView, dwtr );	   				

				GXLoadNrmMtxImm		(invToView.m,GX_PNMTX0);					

				if (inkstate) {
					Matrix genTexCoordMtx;//, invToViewT;
					MatrixZero(&genTexCoordMtx);
					genTexCoordMtx.m11 = invToView.m31 * inkA;
					genTexCoordMtx.m12 = invToView.m32 * inkA;
					genTexCoordMtx.m13 = invToView.m33 * inkA;
					genTexCoordMtx.m14 = invToView.m44 * inkB;					
					GXLoadTexMtxImm(genTexCoordMtx.m, GX_TEXMTX0 , GX_MTX2x4 );
				}

				vtxfmt::Activate( mvtx->vtxFmt, mvtx->locFrac );

				if( mvtx->compAddr[0] )		GXSetArray( GX_VA_POS,	mvtx->compAddr[0], mvtx->compBStride[0] );
				if( mvtx->compAddr[1] )		GXSetArray( GX_VA_TEX0, mvtx->compAddr[1], mvtx->compBStride[1] );
				if( mvtx->compAddr[2] )		GXSetArray( GX_VA_CLR0, mvtx->compAddr[2], mvtx->compBStride[2] );
				if( mvtx->compAddr[3] )		GXSetArray( GX_VA_NRM,	mvtx->compAddr[3], mvtx->compBStride[3] );
				GXInvalidateVtxCache();										

				DpyManager::SetupDrawing (	DpyRaster(dctxt->raster),dview,dwtr, mode2D );
				
				GXSetDstAlpha( GX_TRUE, 0xff );
				
				uint oldInkTextureID = -1;				
				for( uint i = 0 ; i < surfCpt ; i++ )
				{
					if( surf[i].hidden )
						continue;
					{
						GXColor		gxAmbient, gxDiffuse;
						Vec4		ambColor,difColor;
						Vec4Mul		(&ambColor,dlight->color  ,&surf[i].ambient);
						Vec4Mul		(&difColor,dlight->color+1,&surf[i].diffuse);						
						
						GekColor	tmpColor;
						tmpColor.c  =  GetPSM(PSM_RGBA32,ambColor);
						gxAmbient 	= tmpColor.gxc;
						tmpColor.c  =  GetPSM(PSM_RGBA32,difColor);
						gxDiffuse 	= tmpColor.gxc;
								
						GXSetTevColor(GX_TEVREG1,gxAmbient);
						GXSetTevColor(GX_TEVREG2,gxDiffuse);
						
						GXColor		gxShdlevel;
						gxShdlevel.r = uint8(shdlevel * 255);
						GXSetTevColor(GX_TEVREG0,gxShdlevel);
					}
					
					uint inkTextureId = surf[i].dpystate.IsTexturing() ? 1:0;
					if (inkTextureId != oldInkTextureID) {
						GXSetTexCoordGen	( inkTextureId? GX_TEXCOORD1:GX_TEXCOORD0 ,GX_TG_MTX2x4, GX_TG_NRM, GX_TEXMTX0);
						if (inkTextureId == 1) 
							GXSetTexCoordGen(GX_TEXCOORD0,GX_TG_MTX3x4,GX_TG_TEX0,  GX_IDENTITY);
						oldInkTextureID = inkTextureId;							
					}

					surf[i].dpystate.ActivateCartoonLighting ( inkstate ) ;
						
					// drawing
					GXCallDisplayList( mbunch[i].dlistAddr, mbunch[i].dlistBSize );
				}
				GXSetDstAlpha( GX_FALSE, 0xFF );
				
				if (outlineState) {
					// draw outline for this surface, with the current context (wtr, proj, ...) and dpyState (Z, alphatest, ... )
					DrawOutline(dview);
						// clear alpha (using like a stencil buffer).
				}
						
				chainIdx = dchain->next;
			}
			GXSetTexCoordGen(GX_TEXCOORD0,GX_TG_MTX3x4,GX_TG_TEX0,  GX_IDENTITY);
			return TRUE;
		}
	};
}


NvCartoonShader*
nv_cartoonshader_mesh_Create	(	NvMesh*		inMesh	)
{
	if( !inMesh )
		return NULL;
	
	NvkMesh * kmesh = inMesh->GetKInterface();
	
	// CartoonShader need normal 	
	if ( ! (kmesh->GetVtx()->vtxFmt & vtxfmt::CO_NRM_MASK ) )
		return NULL;

	uint surfCpt 	= kmesh->GetSurfaceCpt();
	uint supply 	= 32 + sizeof(Shader::Surface) * surfCpt;
	Shader* shader 	= NvEngineNewS( Shader, supply );
	
	if( !shader )
		return NULL;

	shader->surfCpt = surfCpt;
	shader->surf    = (Shader::Surface*) Round32(shader+1);
	shader->_Init( inMesh );
	return shader->GetInterface();
}


