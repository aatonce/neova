/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
#include "NvIndirectShader_Base[NGC].h"
#include <Kernel/NGC/NvkBitmap[NGC].h>
#include <Kernel/NGC/NvkMesh[NGC].h>
#include <Kernel/NGC/NvDpyState[NGC].h>
using namespace nv;


//#define	TEST_CLIPPING_MODE
//#define	DISABLE_TEXTURING
//#define	DISABLE_LIGHTING

namespace
{
                      	   
	interface Shader : public NvIndirectShaderBase
	{

		struct Surface {
			DpyState_NGC		dpystate;
		};
		
		uint32					enflags;			// enable flags (EN_...)
		Surface					surf;
		
		float					width;
		float					height;
		NvkBitmap	* 			kBump1Map;
		NvkBitmap	* 			kBump2Map;
		NvkBitmap	* 			kReflectionMap;
		GXTexObj 				reflectionMapObj;
		GXTexObj 				bump1MapObj;
		GXTexObj 				bump2MapObj;	
		float 					bumpX,bumpY;
		
		NvIndirectShader::FilterType reflectionFiter;
		
		Vec2					coordsA[3][4];
		
		virtual	~	Shader	(	) {}				// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init		(		)
		{
			InitDpyObject();
			
			enflags    = NvShader::EN_DEFAULT;
			#ifdef DISABLE_TEXTURING
			enflags   ^= NvShader::EN_TEXTURING;
			#endif

			// Init capabilities
			uint dpycaps = 0;
			dpycaps |= DpyState_NGC::CF_HAS_NORMAL;
			dpycaps |= DpyState_NGC::CF_HAS_TEX0;
			dpycaps |= DpyState_NGC::CF_HAS_RGBA;

			// Init dpystate
			ConstructInPlace( &surf.dpystate );
			surf.dpystate.Init();
			surf.dpystate.SetCapabilities	( dpycaps );
			surf.dpystate.SetEnabled			( enflags );
			surf.dpystate.SetMode			( DpyState::TF_DECAL 			|
											  DpyState::TM_NEAREST			|
											  DpyState::TW_REPEAT_REPEAT 	|
											  DpyState::CM_TWOSIDED		 	|
											  DpyState::BM_NONE 			);
											  
			surf.dpystate.SetAlphaPass		( 0.0f );
			surf.dpystate.SetBlendSrcCte	( 1.0f );
			surf.dpystate.SetDefaultColor	( 0xFFFFFFFF );
			
			kBump1Map 			= NULL;
			kBump2Map 			= NULL;
			kReflectionMap 		= NULL;
			reflectionFiter		= NvIndirectShader::FT_Linear;
			
			for (uint j = 0 ; j < 3 ; ++j) {
				for (uint i = 0; i < 4 ; ++i){
					coordsA[j][i] = Vec2(0.0f,0.0f);
				}
			}
			
			width 	= 	height 	= 0.99f;
			bumpX	=	bumpY	= 0.99f;
		}


		void
		Release	(		)
		{
			if( ShutDpyObject() ) {			
				surf.dpystate.Shut();
				SafeRelease( kBump1Map  );
				SafeRelease( kBump2Map  );
				SafeRelease( kReflectionMap );
				NvEngineDelete( this );
			}
		}


		NvResource*
		GetResource		(					)
		{
			return NULL;		
		}


		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}


		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}


		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				surf.dpystate.SetEnabled( inEnableFlags );
			}
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}
		

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return ( inIndex<1 ) ? &surf.dpystate : NULL;
		}

		void
		SetSize		(	float	inWidth	,float	inHeight	)
		{
			if (inWidth <=0)	inWidth  = 1.0f;
			if (inHeight <=0)	inHeight = 1.0f;
			
			width 		= inWidth;
			height 		= inHeight;
		}
		
		bool
		SetBumpScale	(	float	inBumpX	,
							float	inBumpY	)
		{
			if (inBumpX < 0.0f) return false;
			if (inBumpY < 0.0f) return false;
			if (inBumpX >= 1.0f) return false;
			if (inBumpY >= 1.0f) return false;
			
			bumpX = inBumpX;
			bumpY = inBumpY;
			return true;
		}
																
		bool
		SetMap	(	NvIndirectShader::MapId		inId	,
					NvBitmap *	inMap					,
					NvIndirectShader::FilterType inFt	)
		{
			if (! inMap ) return FALSE;
			
			static 
			GXTexFilter convertion[] = {
				GX_NEAR,
				GX_LINEAR,
			};
					
			switch (inId)
			{
				case NvIndirectShader::MI_Bump1 :
					SafeGetRef(kBump1Map,inMap->GetKInterface());
					GXInitTexObj(	&bump1MapObj, 
							kBump1Map->GetTexel(), 
							kBump1Map->GetWidth(), 
							kBump1Map->GetHeight(), 
							kBump1Map->GetPixelFormat(),
                 			GX_REPEAT, GX_REPEAT, FALSE );	
					GXInitTexObjLOD(&bump1MapObj,GX_LINEAR,GX_LINEAR,0.0,0.0,0.0,GX_DISABLE,GX_DISABLE,GX_ANISO_1);
					break;
					
				case NvIndirectShader::MI_Bump2 : 
					SafeGetRef(kBump2Map,inMap->GetKInterface());
					GXInitTexObj(	&bump2MapObj, 
							kBump2Map->GetTexel(), 
							kBump2Map->GetWidth(), 
							kBump2Map->GetHeight(), 
							kBump2Map->GetPixelFormat(),
                 			GX_REPEAT, GX_REPEAT, FALSE );
					GXInitTexObjLOD(&bump2MapObj,GX_LINEAR,GX_LINEAR,0.0,0.0,0.0,GX_DISABLE,GX_DISABLE,GX_ANISO_1);
					break;
					
				case NvIndirectShader::MI_Reflection:
					reflectionFiter = inFt;
					SafeGetRef(kReflectionMap,inMap->GetKInterface());
					GXInitTexObj(	&reflectionMapObj, 
						kReflectionMap->GetTexel(), 
						kReflectionMap->GetWidth(), 
						kReflectionMap->GetHeight(), 
						kReflectionMap->GetPixelFormat(),
             			GX_CLAMP, GX_CLAMP, FALSE );
					GXInitTexObjLOD(&reflectionMapObj,convertion[reflectionFiter],convertion[reflectionFiter],0.0,0.0,0.0,GX_DISABLE,GX_DISABLE,GX_ANISO_1);
					break;
					
				default:
					return FALSE;
			}
			return TRUE;
		}
													
		bool
		SetCoord	(NvIndirectShader::	MapId	inId		,
					 	Vec2	inCoord[4]	)
		{
			if ( inId >= 3) return false;
		
			for (uint i = 0 ; i < 4 ; ++i)
			coordsA[inId][i] = inCoord[i];
			
			return true;
		}
																
		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );
			
			if (!kBump1Map || !kBump2Map || !kReflectionMap) return false;
			
		
			// 2D ?
			bool mode2D = (enflags & NvShader::EN_THROUGHMODE) != 0;

			// Clipping
			GXClipMode clipping = !mode2D && (enflags&NvShader::EN_CLIPPING) ? GX_CLIP_ENABLE : GX_CLIP_DISABLE ;
			#ifdef TEST_CLIPPING_MODE
			clipping = GX_CLIP_ENABLE;
			#endif
			GXSetClipMode( clipping );

			int chainIdx     = dpyctxt.chainHead;
			int lastLightIdx = -1;
			NV_ASSERT( chainIdx >= 0 );
			
			// Set vertex format			
			GXClearVtxDesc();

			GXSetVtxDesc(GX_VA_POS , GX_DIRECT);
    		GXSetVtxDesc(GX_VA_TEX0, GX_DIRECT);
    		GXSetVtxDesc(GX_VA_TEX1, GX_DIRECT);
    		GXSetVtxDesc(GX_VA_TEX2, GX_DIRECT);
    		GXSetVtxDesc(GX_VA_TEX3, GX_DIRECT);
    		
	        GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
		    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_F32, 0);
		    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX1, GX_TEX_ST, GX_F32, 0);
		    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX2, GX_TEX_ST, GX_F32, 0);
		    GXSetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX3, GX_TEX_ST, GX_F32, 0);

			Vec3 p1( 0.0f	, height , 0.0f	);
			Vec3 p2( width	, height , 0.0f	);
			Vec3 p3( width	, 0.0f	 , 0.0f	);
			Vec3 p4( 0.0f	, 0.0f   , 0.0f );
			
			
			while( chainIdx >= 0 )
			{
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
				NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()		   + dctxt->lightIdx;
				Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;
				
				DpyManager::SetupDrawing (	DpyRaster(dctxt->raster),dview,dwtr, mode2D );
				chainIdx = dchain->next;
				surf.dpystate.Activate();
 			
//	********************************************************************************
// 	Load texture
//
	    		GXLoadTexObj(&bump1MapObj, 		GX_TEXMAP0);
    			GXLoadTexObj(&bump2MapObj, 		GX_TEXMAP1);
    			GXLoadTexObj(&reflectionMapObj, GX_TEXMAP2);
    			
//	********************************************************************************
// 	Initialize states
//
				GXSetNumTexGens		(4);
    			GXSetNumChans		(0);
    			GXSetNumTevStages	(5);
    			GXSetNumIndStages	(2);
    			
				GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX3x4, GX_TG_TEX0, GX_IDENTITY);
				GXSetTexCoordGen	(GX_TEXCOORD1, GX_TG_MTX3x4, GX_TG_TEX1, GX_IDENTITY);
				GXSetTexCoordGen	(GX_TEXCOORD2, GX_TG_MTX3x4, GX_TG_TEX2, GX_IDENTITY);
				GXSetTexCoordGen	(GX_TEXCOORD3, GX_TG_MTX3x4, GX_TG_TEX3, GX_IDENTITY);
				
				// Indirect Stage 0 
			    GXSetIndTexOrder		(	GX_INDTEXSTAGE0, GX_TEXCOORD0, GX_TEXMAP0	);
    			GXSetIndTexCoordScale	(	GX_INDTEXSTAGE0, GX_ITS_1, GX_ITS_1	);
    			// Indirect Stage 1 
			    GXSetIndTexOrder		(	GX_INDTEXSTAGE1, GX_TEXCOORD1, GX_TEXMAP1	);
    			GXSetIndTexCoordScale	(	GX_INDTEXSTAGE1, GX_ITS_1, GX_ITS_1	);
    			
    			
    			// Tev 0,1,2,3,4 : indirect.
    			//
				GXSetTevIndirect(	GX_TEVSTAGE0,	GX_INDTEXSTAGE0,	GX_ITF_8,     	
									GX_ITB_ST,    	GX_ITM_0,    		GX_ITW_0,     	
									GX_ITW_0,     	FALSE,       		FALSE,       	
									GX_ITBA_OFF   									);
				
				GXSetTevIndirect(	GX_TEVSTAGE1,	GX_INDTEXSTAGE0,	GX_ITF_8,     
									GX_ITB_ST,    	GX_ITM_0,    		GX_ITW_0,     
									GX_ITW_0,     	TRUE,         		FALSE,        
									GX_ITBA_OFF   									);
				
				GXSetTevIndirect(	GX_TEVSTAGE2, 	GX_INDTEXSTAGE1,	GX_ITF_8,     
									GX_ITB_ST,    	GX_ITM_0,    		GX_ITW_0,     
									GX_ITW_0,     	TRUE,         		FALSE,        
									GX_ITBA_OFF   									);
				
				GXSetTevIndirect(	GX_TEVSTAGE3, 	GX_INDTEXSTAGE1,	GX_ITF_8,     
									GX_ITB_ST,    	GX_ITM_0,    		GX_ITW_0,     
									GX_ITW_0,     	TRUE,         		FALSE,        
									GX_ITBA_OFF   									);
				
				GXSetTevIndirect(	GX_TEVSTAGE4, 	GX_INDTEXSTAGE1, 	GX_ITF_8,     
									GX_ITB_NONE,  	GX_ITM_OFF,   		GX_ITW_OFF,   
									GX_ITW_OFF,   	TRUE,         		FALSE,        
									GX_ITBA_OFF   									);
				
									

				float  mtx[2][3];
				Zero(mtx,sizeof(float) * 2*3);
				mtx[0][0] = bumpX;
				mtx[1][1] = bumpY;
				GXSetIndTexMtx(GX_ITM_0,mtx,1);
    
				// Stage 0 -- Accumulate U1 perturbation component in Bump
			    //
			    // TEVPREV = 0
			    //
			    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD3,
			                  (GXTexMapID)(GX_TEXMAP0|GX_TEX_DISABLE), GX_COLOR_NULL);
			    GXSetTevColorIn(GX_TEVSTAGE0,GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO);
			    GXSetTevAlphaIn(GX_TEVSTAGE0,GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO);
			    GXSetTevColorOp(GX_TEVSTAGE0,GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, TRUE, GX_TEVPREV);
			    GXSetTevAlphaOp(GX_TEVSTAGE0,GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, TRUE, GX_TEVPREV);
			                    

			    // Stage 2 -- Accumulate V2 perturbation component in Bump
			    //
			    // TEVPREV = PREV
			    //
			    GXSetTevOrder(GX_TEVSTAGE1, GX_TEXCOORD3,
			                  (GXTexMapID)(GX_TEXMAP0|GX_TEX_DISABLE), GX_COLOR_NULL);
			    GXSetTevOp(GX_TEVSTAGE1, GX_PASSCLR);
			  
			  
			    // Stage 3 -- Accumulate U2 perturbation component in Bump
			    //
			    // TEVPREV = PREV
			    //
			    GXSetTevOrder(GX_TEVSTAGE2, GX_TEXCOORD3,
			                  (GXTexMapID)(GX_TEXMAP1|GX_TEX_DISABLE), GX_COLOR_NULL);
			    GXSetTevOp(GX_TEVSTAGE2, GX_PASSCLR);

				// Stage 4 -- Accumulate V2 perturbation component in Bump
			    //
			    // TEVPREV = PREV
			    //
			    GXSetTevOrder(GX_TEVSTAGE3, GX_TEXCOORD3,
			                  (GXTexMapID)(GX_TEXMAP1|GX_TEX_DISABLE), GX_COLOR_NULL);
			    GXSetTevOp(GX_TEVSTAGE3, GX_PASSCLR);


			    // Stage 5 -- Add source normal in Bump. Index reflexMap with result of
			    //            perturbation.
			    //
			    // TEVPREVC = TEXC 
			    // TEVPREVA = TEXA
			    //
			    //GXSetTevDirect(	GX_TEVSTAGE4 );
				GXSetTevOrder  (GX_TEVSTAGE4, GX_TEXCOORD2, GX_TEXMAP2, GX_COLOR_NULL);
			    
			    GXSetTevColorIn(GX_TEVSTAGE4,
			                    GX_CC_ZERO, GX_CC_ZERO , GX_CC_ZERO, GX_CC_TEXC);
			                    
			    GXSetTevAlphaIn(GX_TEVSTAGE4,
			                    GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_TEXA);
			                    
			    GXSetTevColorOp(GX_TEVSTAGE4,
			                    GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, TRUE, GX_TEVPREV);
			                    
			    GXSetTevAlphaOp(GX_TEVSTAGE4,
			                    GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1, TRUE, GX_TEVPREV);
			                    
//	********************************************************************************
// 	Draw quad
//

				GXBegin( GX_QUADS, GX_VTXFMT0, 4 );
					// position, texCoord
					GXPosition3f32(p1.x, p1.y, p1.z);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Bump1][0].x		, coordsA[NvIndirectShader::MI_Bump1][0].y);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Bump2][0].x		, coordsA[NvIndirectShader::MI_Bump2][0].y);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Reflection][0].x, coordsA[NvIndirectShader::MI_Reflection][0].y);
        			GXTexCoord2f32(0, 0);
        			
					GXPosition3f32(p2.x, p2.y, p2.z);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Bump1][1].x		, coordsA[NvIndirectShader::MI_Bump1][1].y);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Bump2][1].x		, coordsA[NvIndirectShader::MI_Bump2][1].y);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Reflection][1].x, coordsA[NvIndirectShader::MI_Reflection][1].y);
        			GXTexCoord2f32(0, 0);
					
					GXPosition3f32(p3.x, p3.y, p3.z);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Bump1][2].x		, coordsA[NvIndirectShader::MI_Bump1][2].y);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Bump2][2].x		, coordsA[NvIndirectShader::MI_Bump2][2].y);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Reflection][2].x, coordsA[NvIndirectShader::MI_Reflection][2].y);
        			GXTexCoord2f32(0, 0);
        			
        			GXPosition3f32(p4.x, p4.y, p4.z);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Bump1][3].x		, coordsA[NvIndirectShader::MI_Bump1][3].y);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Bump2][3].x		, coordsA[NvIndirectShader::MI_Bump2][3].y);
        			GXTexCoord2f32(coordsA[NvIndirectShader::MI_Reflection][3].x, coordsA[NvIndirectShader::MI_Reflection][3].y);
        			GXTexCoord2f32(0, 0);
				GXEnd();
			}

//	********************************************************************************
// 	ResetState
//
			// reset vertex format cache.
			vtxfmt::Reset();
			
			// reset texGen matrix 
			GXSetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0	, GX_IDENTITY);
			GXSetTexCoordGen(GX_TEXCOORD1, GX_TG_MTX2x4, GX_TG_TEX1	, GX_IDENTITY);
			GXSetTexCoordGen(GX_TEXCOORD2, GX_TG_MTX2x4, GX_TG_TEX2	, GX_IDENTITY);
			GXSetTexCoordGen(GX_TEXCOORD3, GX_TG_MTX2x4, GX_TG_TEX3	, GX_IDENTITY);

			GXSetTevDirect( GX_TEVSTAGE0 );			
			GXSetTevDirect( GX_TEVSTAGE1 );
			GXSetTevDirect( GX_TEVSTAGE2 );
			GXSetTevDirect( GX_TEVSTAGE3 );
			GXSetTevDirect( GX_TEVSTAGE4 );
			
			GXSetNumTexGens		(1);
			GXSetNumChans		(0);
			GXSetNumTevStages	(1);
			GXSetNumIndStages	(0);

			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			return FALSE;
		}
		
	};
};


NvIndirectShader*
nv_indirectshader_Create	(			)
{
	Shader* shader = NvEngineNew( Shader );
	if( !shader )
		return NULL;
	
	shader->_Init(  );

	return shader->GetInterface();
}




