/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#include <NvMesh.h>
#include <NvSurface.h>
#include "NvIndirectShader_Base[NGC].h"


NvIndirectShader*	nv_indirectshader_Create	(		);

NvIndirectShader*
NvIndirectShader::Create	(		)
{
	return nv_indirectshader_Create(  );
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>


NVITF_MTH0( IndirectShader,	NvInterface*,	GetBase													)
NVITF_CAL0( IndirectShader,	void,			AddRef													)
NVITF_MTH0( IndirectShader,	uint,			GetRefCpt												)
NVITF_CAL0( IndirectShader,	void,			Release													)
NVITF_MTH0( IndirectShader,	NvResource*,	GetResource												)
NVITF_CAL1( IndirectShader,	void,			Enable,				uint32								)
NVITF_CAL1( IndirectShader,	void,			Disable,			uint32								)
NVITF_CAL1( IndirectShader,	void,			SetEnabled,			uint32								)
NVITF_MTH0( IndirectShader,	uint32,			GetEnabled												)
NVITF_MTH1( IndirectShader,	DpyState*,		GetDisplayState,	uint								)
NVITF_CAL2( IndirectShader,	void,			SetSize	,			float,	float						)
NVITF_MTH3( IndirectShader,	bool,			SetMap,				NvIndirectShader::MapId	,	NvBitmap*,NvIndirectShader::FilterType	)
												  
NVITF_MTH2( IndirectShader,	bool,			SetCoord,			NvIndirectShader::MapId	,	Vec2 * 	)
NVITF_MTH2( IndirectShader,	bool,			SetBumpScale,		float,	float						)