/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <NvMesh.h>
#include <NvSurface.h>
#include "NvCartoonShader_Base[PS2].h"



NvCartoonShader*	nv_cartoonshader_mesh_Create	(	NvMesh*		inMesh	);
NvCartoonShader*	nv_cartoonshader_surf_Create	(	NvSurface*	inRsc	);


NvCartoonShader*
NvCartoonShader::Create	(	NvResource*		inRsc	)
{
	if( !inRsc )
		return NULL;
	if( inRsc->GetRscType() == NvMesh::TYPE )
		return nv_cartoonshader_mesh_Create( (NvMesh*)inRsc );
	if( inRsc->GetRscType() == NvSurface::TYPE )
		return nv_cartoonshader_surf_Create( (NvSurface*)inRsc );
	return NULL;
}




//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( CartoonShader,	NvInterface*,	GetBase													)
NVITF_CAL0( CartoonShader,	void,			AddRef													)
NVITF_MTH0( CartoonShader,	uint,			GetRefCpt												)
NVITF_CAL0( CartoonShader,	void,			Release													)
NVITF_MTH0( CartoonShader,	NvResource*,	GetResource												)
NVITF_CAL1( CartoonShader,	void,			Enable,					uint32							)
NVITF_CAL1( CartoonShader,	void,			Disable,				uint32							)
NVITF_CAL1( CartoonShader,	void,			SetEnabled,				uint32							)
NVITF_MTH0( CartoonShader,	uint32,			GetEnabled												)
NVITF_MTH0( CartoonShader,	uint,			GetDrawStride											)
NVITF_MTH1( CartoonShader,	bool,			SetDrawStart,			uint							)
NVITF_MTH1( CartoonShader,	bool,			SetDrawSize,			uint							)
NVITF_MTH1( CartoonShader,	bool,			HideSurface,			uint							)
NVITF_MTH1( CartoonShader,	bool,			ShowSurface,			uint							)
NVITF_MTH0( CartoonShader,	bool,			HideAllSurfaces											)
NVITF_MTH0( CartoonShader,	bool,			ShowAllSurfaces											)
NVITF_MTH1( CartoonShader,	DpyState*,		GetDisplayState,		uint							)
NVITF_MTH2( CartoonShader,	bool,			SetAmbient,				uint,	Vec4*					)
NVITF_MTH2( CartoonShader,	bool,			SetDiffuse,				uint,	Vec4*					)
NVITF_CAL1( CartoonShader,	void,			SetShadingLevel,		float							)
NVITF_CAL1( CartoonShader,	void,			SetInking,				bool							)
NVITF_CAL2( CartoonShader,	void,			SetInkingRamp,			float,	float					)
NVITF_CAL1( CartoonShader,	void,			SetOutlining,			bool							)
NVITF_CAL1( CartoonShader,	void,			SetOutliningThickness,	float							)
NVITF_CAL1( CartoonShader,	void,			SetOutliningQuality,	float							)



