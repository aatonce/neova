/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvCartoonShader_Base[PS2].h"
#include <Kernel/PS2/NvkBitmap[PS2].h>
#include <Kernel/PS2/NvkImage[PS2].h>
#include <Kernel/PS2/NvkMesh[PS2].h>
#include <Kernel/PS2/NvDpyState[PS2].h>
#include <Kernel/PS2/VU/vu1_Def.h>
using namespace nv;


#define		INK_RAMP_W		(64)
#define		INK_RAMP_H		(8)




namespace
{

	struct Shader : public NvCartoonShaderBase
	{
		struct Surface {
			ALIGNED128( DpyState_PS2, dpystate );
			Vec4	ambient;		// in [0,1]^4
			Vec4	diffuse;		// in [0,1]^4
			uint32	dmaAddr;
			uint	dmaQSize;
			uint	dmaNbPkt;
			bool	hidden;
		};

		frame::Packet			pkt;
		uint32					enflags;			// enable flags (EN_...)
		NvkMesh*				mesh;
		uint					surfaceCpt;
		Surface*				surfaces;
		float					xyzUnpack;
		float					shdlevel;
		bool					inkable;
		float					inkstart, inkend;
		bool					outlinable;
		float					outlineThickness;
		float					outlineQuality;
		ALIGNED128( DpyState_PS2, outldpystate );
		ALIGNED128( DpyState_PS2, inkdpystate );
		ALIGNED128( uint32,		  inkrampPxl[INK_RAMP_W*INK_RAMP_H] );


		virtual	~	Shader	(	) {}				// needed by nv::DestructInPlace<T>(ptr)


		void
		_Init		(	NvMesh*		inMesh		)
		{
			NV_COMPILE_TIME_ASSERT( (sizeof(Surface)&15)==0 );

			InitDpyObject();

			enflags = NvShader::EN_DEFAULT;

			NV_ASSERT( inMesh->IsLightable() );
			mesh = inMesh->GetKInterface();
			mesh->AddRef();
			xyzUnpack = vu1::GetXYZUnpackF( mesh->GetPackIPartLen() );

			pkt.Init( this );
			pkt.SetNeedUpdate( TRUE );

			NvkMesh::Shading* shadingA = mesh->GetSurfaceShadingA();
			for( uint i = 0 ; i < surfaceCpt ; i++ ) {
				// Init capabilities
				uint dpycaps = DpyState_PS2::CF_HAS_NORMAL | DpyState_PS2::CF_HAS_RGBA;
				if( shadingA[i].hasUV )
					dpycaps |= DpyState_PS2::CF_HAS_TEX0;

				// hidden
				surfaces[i].hidden = FALSE;

				// Init dpystate
				ConstructInPlace( &surfaces[i].dpystate );
				surfaces[i].dpystate.Init();
				surfaces[i].dpystate.gsCtxt.owner = this;	// need post-update to clear tex0.TCC !
				surfaces[i].dpystate.SetCapabilities( dpycaps );
				surfaces[i].dpystate.SetEnabledMask( enflags );
				surfaces[i].dpystate.SetEnabled( shadingA[i].stateEnFlags );
				surfaces[i].dpystate.SetMode( shadingA[i].stateMdFlags );
				surfaces[i].dpystate.SetAlphaPass( shadingA[i].stateAlphaPass );
				surfaces[i].dpystate.SetBlendSrcCte( shadingA[i].stateBlendCte );
				surfaces[i].dpystate.SetTexturing( shadingA[i].bitmapUID, shadingA[i].mipL, shadingA[i].mipK, shadingA[i].mappingRatio );
				pkt.Append( &surfaces[i].dpystate.texCtxt );

				// Inking -> blend with alpha buffer !
				surfaces[i].dpystate.SetMode( DpyState::BM_MODULATE | DpyState::BS_BUFFER );

				// Color filters
				ExtractRGBA( surfaces[i].ambient, PSM_ABGR32, shadingA[i].ambient );
				ExtractRGBA( surfaces[i].diffuse, PSM_ABGR32, shadingA[i].diffuse );

				// dma data
				NvkMesh::BunchData* bunch = mesh->GetBunchDataA() + mesh->GetBunchBySurfA()[i].startIdx;
				NV_ASSERT( mesh->GetBunchBySurfA()[i].size == 1 );
				surfaces[i].dmaAddr  = bunch->dmaBOffset + mesh->GetBunchDataBase();
				surfaces[i].dmaQSize = bunch->dmaQSize;
				surfaces[i].dmaNbPkt = bunch->packetCpt;
			}

			// Outlining dpystate
			outlinable       = TRUE;
			outlineThickness = NvCartoonShader::DEF_OUTL_THICKNESS;
			outlineQuality	 = NvCartoonShader::DEF_OUTL_QUALITY;
			outldpystate.Init();
			outldpystate.SetCapabilities( DpyState_PS2::CF_HAS_RGBA );
			outldpystate.SetMode( DpyState::BM_NONE );
			outldpystate.SetWRColorMask( 0xFFFFFF00 );						// keep alpha mask
			outldpystate.SetAlphaPass( 0.f );								// no alpha test
			outldpystate.SetDestAlphaTest( DpyState_PS2::DAT_EQUAL_0 );		// pass if alpha >= 128 (outside the alpha mask)
			outldpystate.Activate();

			// Inking dpystate
			inkable	 = TRUE;
			shdlevel = float(NvCartoonShader::DEF_SHADLEVEL)     * 0.01f;
			inkstart = float(NvCartoonShader::DEF_INKRAMP_START) * 0.01f;
			inkend	 = float(NvCartoonShader::DEF_INKRAMP_END)   * 0.01f;
			NvkImage* inkimg = NvkImage::Create( SCE_GS_PSMCT32, INK_RAMP_W, INK_RAMP_H, NULL, inkrampPxl );
			NV_ASSERT( inkimg );
			inkdpystate.Init();
			inkdpystate.SetCapabilities( DpyState_PS2::CF_HAS_RGBA | DpyState_PS2::CF_HAS_TEX0 );
			inkdpystate.SetSource( inkimg->GetBitmap() );
			inkdpystate.SetMode( DpyState::BM_NONE | DpyState::TF_DECAL | DpyState::TW_CLAMP_CLAMP );
			inkdpystate.SetAlphaPass( 0.f );		// // no alpha test
			inkdpystate.Activate();
			SafeRelease( inkimg );
			pkt.Append( &inkdpystate.texCtxt );

			// Init cte ramping bitmap (0->1)
			for( int i = 0 ; i < INK_RAMP_W ; i++ ) {
				float s  = float(i) / float(INK_RAMP_W-1);
				inkrampPxl[i] = GetPSM( PSM_ABGR32, int(s*255.f), int(s*255.f), int(s*255.f), int(s*128.f) );
			}
		}

		void
		Release	(		)
		{
			if( ShutDpyObject() ) {
				NV_ASSERT( !pkt.IsDrawing() );
				for( uint i = 0 ; i < surfaceCpt ; i++ )
					surfaces[i].dpystate.Shut();
				outldpystate.Shut();
				inkdpystate.Shut();
				SafeRelease( mesh );
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return mesh->GetInterface();
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				for( uint i = 0 ; i < surfaceCpt ; i++ )
					surfaces[i].dpystate.SetEnabledMask( enflags );
			}
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}

		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfaceCpt )
				return FALSE;
			surfaces[inIndex].hidden = TRUE;
			surfaces[inIndex].dpystate.ForceNoTexturing( TRUE );
			return TRUE;
		}

		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfaceCpt )
				return FALSE;
			surfaces[inIndex].hidden = FALSE;
			surfaces[inIndex].dpystate.ForceNoTexturing( FALSE );
			return TRUE;
		}

		bool
		HideAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfaceCpt ; i++ )
				HideSurface( i );
			return TRUE;
		}

		bool
		ShowAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfaceCpt ; i++ )
				ShowSurface( i );
			return TRUE;
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			if( inIndex < surfaceCpt )	return &surfaces[inIndex].dpystate;
			else						return NULL;
		}

		bool
		SetAmbient		(	uint			inIndex,
							Vec4*			inColor					)
		{
			if( !inColor || inIndex >= surfaceCpt )
				return FALSE;
			Vec4Copy( &surfaces[inIndex].ambient, inColor );
			return TRUE;
		}

		bool
		SetDiffuse		(	uint			inIndex,
							Vec4*			inColor			)
		{
			if( !inColor || inIndex >= surfaceCpt )
				return FALSE;
			Vec4Copy( &surfaces[inIndex].diffuse, inColor );
			return TRUE;
		}

		void
		SetShadingLevel		(	float			inLevel			)
		{
			shdlevel = Clamp( inLevel, 0.f, 1.f );
		}

		void
		SetInking			(	bool			inOnOff			)
		{
			if( inkable != inOnOff ) {
				inkable = inOnOff;
				for( uint i = 0 ; i < surfaceCpt ; i++ ) {
					// Blend with alpha inking
					if( inkable )	surfaces[i].dpystate.SetMode( DpyState::BM_MODULATE | DpyState::BS_BUFFER );
					else			surfaces[i].dpystate.SetMode( DpyState::BM_SELECT );
				}
			}
		}

		void
		SetInkingRamp			(	float			inStartLevel,
									float			inEndLevel		)
		{
			inkstart = Clamp( inStartLevel, 0.f, 1.f );
			inkend   = Clamp( inEndLevel, 0.f, 1.f );
			inkend   = Max( inkstart, inkend );
		}

		void
		SetOutlining		(	bool			inOnOff			)
		{
			outlinable = inOnOff;
		}

		void
		SetOutliningThickness	(	float			inThickness		)
		{
			outlineThickness = Max( inThickness, 0.f );
		}

		void
		SetOutliningQuality		(	float			inQuality		)
		{
			outlineQuality = Clamp( inQuality, 0.f, 1.f );
		}

		void
		_BuildDMA	(		)
		{
			dmac::Cursor* DC = dmac::GetFrontHeapCursor();
			NV_ASSERT_DC( DC );

			vu1::ClipMode clipMode;
			if( enflags & NvShader::EN_THROUGHMODE )	clipMode = vu1::CM_NONE;	// 2D => no clip !
			else if( enflags & NvShader::EN_CLIPPING )	clipMode = vu1::CM_CLIPPING;
			else if( enflags & NvShader::EN_CULLING )	clipMode = vu1::CM_CULLING;
			else										clipMode = vu1::CM_NONE;

			bool mode2D		  = (enflags&NvShader::EN_THROUGHMODE) != 0;
			bool do_inking    = inkable && (inkstart < 1.f);
			bool do_outlining = outlinable && (outlineThickness > 0.f);


			//
			// Write vu1-context stuffs for all drawings in cursor
			// full vu1-context for surface[0]
			// only vu1-context lightColors for surfaces

			uint bcl_flags = vu1::BCL_TOVIEW | (mode2D ? vu1::BCL_TL_MIN_2D : vu1::BCL_TL_MIN);

			vu1::ToLitViewContext *vu1ctxt,
								  *vu1ctxt0 = (vu1::ToLitViewContext*)
				vu1::BuildViewContextList(	DC,
											sizeof(vu1::ToLitViewContext),
											dpyctxt.chainHead,
											bcl_flags,
											xyzUnpack
											);

			vu1::ToLitFilters* vu1lf0 = (vu1::ToLitFilters*)
				vu1::BuildLightFiltersList(	DC,
											sizeof(vu1::ToLitFilters),
											dpyctxt.chainHead,
											surfaceCpt,
											&surfaces[0].ambient,
											&surfaces[0].diffuse,
											sizeof(Surface)
											);

			// Cartoon ctxt cte
			NV_ASSERT_DC( DC );
			vu1::cartoon::ContextToon* vu1ctxt_toon = (vu1::cartoon::ContextToon*) DC->vd;
			DC->vd = vu1ctxt_toon+1;
			{
				// inking
				float ink_d = ( inkend - inkstart );
				if( ink_d <= 0.f )
					ink_d = 0.001f;
				float ink_a = 1.f / ink_d;
				float ink_b = - ink_a * inkstart;
				vu1ctxt_toon->ink_ramp_a = ink_a;
				vu1ctxt_toon->ink_ramp_b = ink_b;
				int slevel = Clamp( int(shdlevel*255.f), 0, 255 );

				// dark shading
				vu1ctxt_toon->shade0_test0 = SCE_GS_SET_TEST( 1, 3, slevel, 0, 0, 0, 1, SCE_GS_ZGEQUAL );	// ATE=1/ATST<=LEQUAL
				vu1ctxt_toon->shade0_c[0]  = 1.f;
				vu1ctxt_toon->shade0_c[1]  = 0.f;
				vu1ctxt_toon->shade0_c[2]  = 255.f;

				// radiant shading
				vu1ctxt_toon->shade1_test0 = SCE_GS_SET_TEST( 1, 6, slevel, 0, 0, 0, 1, SCE_GS_ZGEQUAL );	// ATE=1/ATST=GREATER
				vu1ctxt_toon->shade1_c[0]  = 1.f;
				vu1ctxt_toon->shade1_c[1]  = 1.f;
				vu1ctxt_toon->shade1_c[2]  = 255.f;
			}

			const uint dark_itop    = vu1::cartoon::DO_DARK_SHADING;
			const uint radiant_itop = vu1::cartoon::DO_RADIANT_SHADING;
			const uint ink_itop     = vu1::cartoon::DO_INKING;
			const uint outl_itop	= vu1::cartoon::DO_OUTLINING;

			pkt.tadr = DC->i32;
			pkt.mcId = vu1::cartoon::Begin_CH1( DC, clipMode );

			// Send toon context cte
			vu1::cartoon::SetContext_CH1( DC, NULL, NULL, vu1ctxt_toon );


			//
			// PASS 1
			// clear alpha stencil for final outlining

			if( do_outlining )
			{
				gs::frame::ClearMod clearMod;
				gs::frame::Clear_CH1( DC, clearMod );
				clearMod.SetColor( 0 );
				clearMod.SetFBMSK( 0x00FFFFFF );	// keep rgb
				clearMod.SetZMSK( 1 );				// keep z
			}


			//
			// PASS 2
			// the DOT-SED inker, using a dot ramping

			if( do_inking )
			{
				vu1ctxt = vu1ctxt0;
				vu1::cartoon::SetDirect_CH1( DC, &inkdpystate.gsCtxt );
				for( int j = 0 ; j < dpyctxt.chainCpt ; j++ ) {
					vu1::cartoon::SetContext_CH1( DC, NULL, vu1ctxt++ );
					for( uint i = 0 ; i < surfaceCpt ; i++ ) {
						if( !surfaces[i].hidden )
							vu1::cartoon::SetData_CH1( DC, surfaces[i].dmaAddr, surfaces[i].dmaQSize, surfaces[i].dmaNbPkt, ink_itop );
					}
				}
			}


			//
			// PASS 3 & 4
			// draw the celshading lighting (dark & enlight areas)

			{
				if( do_outlining ) {
					// FBA is enabled to force outputed alpha >= 128, used as the outlining mask
					gs::tools::Gs_Open_CH1( DC );
					gs::SetADReg( DC, SCE_GS_FBA_1, SCE_GS_SET_FBA_1(1) );
					gs::tools::Gs_Close_CH1( DC );
				}

				frame::TexContext* tex = pkt.texList;
				for( uint i = 0 ; i < surfaceCpt ; i++ ) {
					NV_ASSERT( tex );
					vu1::cartoon::SetDirect_CH1( DC, tex->gsContext );
					tex = tex->next;
					vu1ctxt = vu1ctxt0;
					for( int j = 0 ; j < dpyctxt.chainCpt ; j++ ) {
						vu1::cartoon::SetContext_CH1( DC, NULL, vu1ctxt++, NULL, vu1lf0++ );
						if( !surfaces[i].hidden ) {
							vu1::cartoon::SetData_CH1( DC, surfaces[i].dmaAddr, surfaces[i].dmaQSize, surfaces[i].dmaNbPkt, dark_itop );
							vu1::cartoon::SetData_CH1( DC, surfaces[i].dmaAddr, surfaces[i].dmaQSize, surfaces[i].dmaNbPkt, radiant_itop );
						}
					}
				}

				if( do_outlining ) {
					// disable FBA
					gs::tools::Gs_Open_CH1( DC );
					gs::SetADReg( DC, SCE_GS_FBA_1, SCE_GS_SET_FBA_1(0) );
					gs::tools::Gs_Close_CH1( DC );
				}
			}


			//
			// PASS 5 ...
			// outlining with *n* passes of decaled black pixels only outside the alpha mask

			if( do_outlining )
			{
				vu1::cartoon::SetDirect_CH1( DC, &outldpystate.gsCtxt );
				Vec4 outlineProjTrans;
				// quality=0 => 4  steps (PI/4, 3PI/4, 5PI/4, 7PI/4)
				// quality=1 => 32 steps ...
				float nb_step   = (1.f-outlineQuality)*4.f + (32.f * outlineQuality);
				float angl_step = TwoPi / nb_step;
				float angl_base = Pi * 0.25f;
				for( float angl = 0.f ; angl < TwoPi ; angl += angl_step ) {
					float s, c;
					Sincosf( angl+angl_base, &s, &c );
					s *= outlineThickness;
					c *= outlineThickness;
					vu1ctxt = vu1ctxt0;
					for( int j = 0 ; j < dpyctxt.chainCpt ; j++ ) {
						// 2d translate
						outlineProjTrans.x = vu1ctxt->view.toprojTrans.x + c;
						outlineProjTrans.y = vu1ctxt->view.toprojTrans.y + s;
						outlineProjTrans.z = vu1ctxt->view.toprojTrans.z;
						outlineProjTrans.w = vu1ctxt->view.toprojTrans.w;
						vu1::cartoon::SetContext_CH1( DC, NULL, vu1ctxt++, NULL, NULL, &outlineProjTrans );
						for( uint i = 0 ; i < surfaceCpt ; i++ ) {
							if( !surfaces[i].hidden )
								vu1::cartoon::SetData_CH1( DC, surfaces[i].dmaAddr, surfaces[i].dmaQSize, surfaces[i].dmaNbPkt, outl_itop );
						}
					}
				}
			}

			pkt.link = DC->i32;
			vu1::cartoon::End_CH1( DC );
		}

		void
		UpdatePacket		(	frame::Packet*	inPkt		)
		{
			NV_ASSERT( inPkt == &pkt );

			mesh->UpdateBeforeDraw();

			for( uint i = 0 ; i < surfaceCpt ; i++ )
				surfaces[i].dpystate.Activate();
		}

		void
		UpdateGsContext	(	frame::GsContext*		inGsCtxt	)
		{
			// cel-shading passes need alpha = alpha_fragment, without texture alpha modulation !
			// => clear TCC to obtain this !
			inGsCtxt->ClearTCC();
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );
			_BuildDMA();
			frame::Push( &pkt );
			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			uint nbpass = (inkable && inkstart<1.f) ? 3 : 2;
			outTriCpt	= 0;
			outLocCpt	= 0;
			outVtxCpt	= 0;
			outPrimCpt	= 0;
			for( uint i = 0 ; i < mesh->GetBunchCpt() ; i++ ) {
				outTriCpt  += mesh->GetBunchA()[i].faceCpt * dpyctxt.chainCpt * nbpass;
				outLocCpt  += mesh->GetBunchA()[i].locCpt  * dpyctxt.chainCpt * nbpass;
				outVtxCpt  += mesh->GetBunchA()[i].vtxCpt  * dpyctxt.chainCpt * nbpass;
				outPrimCpt += mesh->GetBunchA()[i].primCpt * dpyctxt.chainCpt * nbpass;
			}
			return TRUE;
		}
	};


}




NvCartoonShader*
nv_cartoonshader_mesh_Create	(	NvMesh*		inMesh		)
{
	if( !inMesh )
		return NULL;

	if( !inMesh->GetKInterface() || !inMesh->GetKInterface()->IsLightable() ) {
		DebugPrintf( "NvCartoonShader: The nvmesh must be lightable !\n" );
		return NULL;
	}

	uint surfCpt = inMesh->GetSurfaceCpt();
	uint supply = 16 + sizeof(Shader::Surface) * surfCpt;
	Shader* shader = NvEngineNewAS( Shader,16,supply );
	if( !shader )
		return NULL;

	shader->surfaceCpt = surfCpt;
	shader->surfaces   = (Shader::Surface*) Round16(shader+1);
	shader->_Init( inMesh );
	return shader->GetInterface();
}




