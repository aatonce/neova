/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvGeomShader_Base[PS2].h"
#include <Kernel/PS2/NvkBitmap[PS2].h>
#include <Kernel/PS2/NvkMesh[PS2].h>
#include <Kernel/PS2/NvDpyState[PS2].h>
#include <Kernel/PS2/VU/vu1_Def.h>
using namespace nv;




namespace
{

	struct Shader : public NvGeomShaderBase
	{
		struct Surface {
			ALIGNED128( DpyState_PS2, dpystate );
			Vec4	ambient;		// in [0,1]^4
			Vec4	diffuse;		// in [0,1]^4
			uint32	dmaAddr;
			uint	dmaQSize;
			uint	dmaNbPkt;
			bool	hidden;
		};

		uint32					enflags;			// enable flags (EN_...)
		NvkMesh*				mesh;
		uint					surfaceCpt;
		Surface*				surfaces;
		bool					tolit;
		float					xyzUnpack;
		frame::Packet			pkt;


		virtual	~	Shader	(	) {}				// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init		(	NvMesh*		inMesh		)
		{
			NV_COMPILE_TIME_ASSERT( (sizeof(Surface)&15)==0 );

			InitDpyObject();

			enflags = NvShader::EN_DEFAULT;

			mesh = inMesh->GetKInterface();
			mesh->AddRef();
			xyzUnpack		= vu1::GetXYZUnpackF( mesh->GetPackIPartLen() );
			tolit			= mesh->IsLightable();

			pkt.Init( this );
			pkt.SetNeedUpdate( TRUE );

			// Setup internal surfaces
			NvkMesh::Shading* shadingA = mesh->GetSurfaceShadingA();
			for( uint i = 0 ; i < surfaceCpt ; i++ ) {
				// Init capabilities
				uint dpycaps = 0;
				if( tolit )								dpycaps |= DpyState_PS2::CF_HAS_NORMAL;
				if( shadingA[i].hasUV )					dpycaps |= DpyState_PS2::CF_HAS_TEX0;
				if( shadingA[i].hasColor || tolit )		dpycaps |= DpyState_PS2::CF_HAS_RGBA;

				// hidden
				surfaces[i].hidden = FALSE;

				// Init dpystate
				ConstructInPlace( &surfaces[i].dpystate );
				surfaces[i].dpystate.Init();
				surfaces[i].dpystate.SetCapabilities( dpycaps );
				surfaces[i].dpystate.SetEnabledMask( enflags );
				surfaces[i].dpystate.SetEnabled( shadingA[i].stateEnFlags );
				surfaces[i].dpystate.SetMode( shadingA[i].stateMdFlags );
				surfaces[i].dpystate.SetAlphaPass( shadingA[i].stateAlphaPass );
				surfaces[i].dpystate.SetBlendSrcCte( shadingA[i].stateBlendCte );
				surfaces[i].dpystate.SetTexturing( shadingA[i].bitmapUID, shadingA[i].mipL, shadingA[i].mipK, shadingA[i].mappingRatio );
				pkt.Append( &surfaces[i].dpystate.texCtxt );

				// Color filters
				if( tolit ) {
					ExtractRGBA( surfaces[i].ambient,  PSM_ABGR32, shadingA[i].ambient  );
					ExtractRGBA( surfaces[i].diffuse,  PSM_ABGR32, shadingA[i].diffuse  );
				}

				// dma data
				NvkMesh::BunchData* bunch = mesh->GetBunchDataA() + mesh->GetBunchBySurfA()[i].startIdx;
				NV_ASSERT( mesh->GetBunchBySurfA()[i].size == 1 );
				surfaces[i].dmaAddr  = bunch->dmaBOffset + mesh->GetBunchDataBase();
				surfaces[i].dmaQSize = bunch->dmaQSize;
				surfaces[i].dmaNbPkt = bunch->packetCpt;
			}
		}

		void
		Release	(		)
		{
			if( ShutDpyObject() ) {
				NV_ASSERT( !pkt.IsDrawing() );
				for( uint i = 0 ; i < surfaceCpt ; i++ )
					surfaces[i].dpystate.Shut();
				SafeRelease( mesh );
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return mesh->GetInterface();
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				for( uint i = 0 ; i < surfaceCpt ; i++ )
					surfaces[i].dpystate.SetEnabledMask( enflags );
			}
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}

		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfaceCpt )
				return FALSE;
			surfaces[inIndex].hidden = TRUE;
			surfaces[inIndex].dpystate.ForceNoTexturing( TRUE );
			return TRUE;
		}

		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfaceCpt )
				return FALSE;
			surfaces[inIndex].hidden = FALSE;
			surfaces[inIndex].dpystate.ForceNoTexturing( FALSE );
			return TRUE;
		}

		bool
		HideAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfaceCpt ; i++ )
				HideSurface( i );
			return TRUE;
		}

		bool
		ShowAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfaceCpt ; i++ )
				ShowSurface( i );
			return TRUE;
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			if( inIndex >= surfaceCpt )		return NULL;
			else							return &surfaces[inIndex].dpystate;
		}

		bool
		SetAmbient		(	uint			inIndex,
							Vec4*			inColor					)
		{
			if( !tolit || !inColor || inIndex >= surfaceCpt )
				return FALSE;
			Vec4Copy( &surfaces[inIndex].ambient, inColor );
			return TRUE;
		}

		bool
		SetDiffuse		(	uint			inIndex,
							Vec4*			inColor					)
		{
			if( !tolit || !inColor || inIndex >= surfaceCpt )
				return FALSE;
			Vec4Copy( &surfaces[inIndex].diffuse, inColor );
			return TRUE;
		}

		void
		_BuildDMA	(		)
		{
			dmac::Cursor* DC = dmac::GetFrontHeapCursor();
			NV_ASSERT_DC( DC );

			vu1::ClipMode clipMode;
			if( enflags & NvShader::EN_THROUGHMODE )	clipMode = vu1::CM_NONE;	// 2D => no clip !
			else if( enflags & NvShader::EN_CLIPPING )	clipMode = vu1::CM_CLIPPING;
			else if( enflags & NvShader::EN_CULLING )	clipMode = vu1::CM_CULLING;
			else										clipMode = vu1::CM_NONE;
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;


			//
			// Write vu1-context stuffs for all drawings in cursor
			// full vu1-context for surface[0]
			// only vu1-context lightColors all for surfaces

			uint bcl_flags  = ( mode2D ? vu1::BCL_MIN_2D : vu1::BCL_MIN );
			     bcl_flags |= tolit ? vu1::BCL_LIGHT_M : 0;

			vu1::mesh::Context *vu1ctxt,
							   *vu1ctxt0 = (vu1::mesh::Context*)
				vu1::BuildViewContextList(	DC,
											sizeof(vu1::mesh::Context),
											dpyctxt.chainHead,
											bcl_flags,
											xyzUnpack
											);
			vu1::ToLitFilters* vu1lf0 = (vu1::ToLitFilters*)
				vu1::BuildLightFiltersList(	DC,
											sizeof(vu1::ToLitFilters),
											dpyctxt.chainHead,
											tolit?surfaceCpt:0,
											&surfaces[0].ambient,
											&surfaces[0].diffuse,
											sizeof(Surface)
											);

			// Init
			pkt.tadr = DC->i32;
			pkt.mcId = vu1::mesh::Begin_CH1( DC, clipMode, tolit );

			// Surfaces
			frame::TexContext* tex = pkt.texList;
			for( uint i = 0 ; i < surfaceCpt ; i++ ) {
				NV_ASSERT( tex );
				vu1::mesh::SetDirect_CH1( DC, tex->gsContext );
				tex = tex->next;
				vu1ctxt = vu1ctxt0;
				for( int j = 0 ; j < dpyctxt.chainCpt ; j++ ) {
					vu1::mesh::SetContext_CH1( DC, vu1ctxt++, vu1lf0++ );
					if( !surfaces[i].hidden )
						vu1::mesh::SetData_CH1( DC, surfaces[i].dmaAddr, surfaces[i].dmaQSize, surfaces[i].dmaNbPkt );
				}
			}

			// Link
			pkt.link = DC->i32;
			vu1::mesh::End_CH1( DC );

			pkt.SetSeparator( (enflags&NvShader::EN_SORTING) ? FALSE : TRUE );
		}

		void
		UpdatePacket		(	frame::Packet*	inPkt		)
		{
			mesh->UpdateBeforeDraw();
			for( uint i = 0 ; i < surfaceCpt ; i++ )
				surfaces[i].dpystate.Activate();
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );
			_BuildDMA();
			frame::Push( &pkt );
			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			outTriCpt	= 0;
			outLocCpt	= 0;
			outVtxCpt	= 0;
			outPrimCpt	= 0;
			for( uint i = 0 ; i < mesh->GetBunchCpt() ; i++ ) {
				outTriCpt  += mesh->GetBunchA()[i].faceCpt * dpyctxt.chainCpt;
				outLocCpt  += mesh->GetBunchA()[i].locCpt  * dpyctxt.chainCpt;
				outVtxCpt  += mesh->GetBunchA()[i].vtxCpt  * dpyctxt.chainCpt;
				outPrimCpt += mesh->GetBunchA()[i].primCpt * dpyctxt.chainCpt;
			}
			return TRUE;
		}
	};


}




NvGeomShader*
nv_geomshader_mesh_Create	(	NvMesh*		inMesh		)
{
	if( !inMesh )
		return NULL;

	uint surfCpt = inMesh->GetSurfaceCpt();
	uint supply = 16 + sizeof(Shader::Surface) * surfCpt;
	Shader* shader = NvEngineNewAS( Shader,16,supply );
	if( !shader )
		return NULL;

	shader->surfaceCpt = surfCpt;
	shader->surfaces   = (Shader::Surface*) Round16(shader+1);
	shader->_Init( inMesh );
	return shader->GetInterface();
}




