/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvGeomShader_Base[PS2].h"
#include <Kernel/PS2/NvkBitmap[PS2].h>
#include <Kernel/PS2/NvkSurface[PS2].h>
#include <Kernel/PS2/NvDpyState[PS2].h>
#include <Kernel/PS2/VU/vu1_Def.h>
using namespace nv;



namespace
{

	struct Shader : public NvGeomShaderBase
	{
		frame::Packet			pkt;
		uint32					enflags;		// enable flags (EN_...)
		NvkSurface*				surf;
		uint					drawStart;
		uint					drawSize;
		bool					tolit;
		bool					hidden;
		ALIGNED128( DpyState_PS2, dpystate );

		virtual	~	Shader	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init	(	NvSurface*	inSurface		)
		{
			InitDpyObject();

			enflags = NvShader::EN_DEFAULT;

			surf = inSurface->GetKInterface();
			surf->AddRef();

			tolit  = surf->HasComponent( NvSurface::CO_NORMAL );
			hidden = FALSE;

			drawStart = 0;
			drawSize  = 0;

			pkt.Init( this );
			pkt.SetNeedUpdate( TRUE );

			// rebuild dpystate capabilities
			uint dpycaps = 0;
			if( surf->HasComponent(NvSurface::CO_NORMAL) )	dpycaps |= DpyState_PS2::CF_HAS_NORMAL;
			if( surf->HasComponent(NvSurface::CO_TEX) )		dpycaps |= DpyState_PS2::CF_HAS_TEX0;
			if( surf->HasComponent(NvSurface::CO_COLOR) )	dpycaps |= DpyState_PS2::CF_HAS_RGBA;

			// init dpystate
			dpystate.Init();
			dpystate.SetEnabledMask( enflags );
			dpystate.SetCapabilities( dpycaps );
			pkt.Append( &dpystate.texCtxt );
		}

		void
		Release			(					)
		{
			if( ShutDpyObject() ) {
				NV_ASSERT( !pkt.IsDrawing() );
				SafeRelease( surf );
				dpystate.Shut();
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return surf->GetInterface();
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			enflags = inEnableFlags;
			dpystate.SetEnabledMask( enflags );
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}

		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex != 0 )
				return FALSE;
			hidden = TRUE;
			return TRUE;
		}

		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex != 0 )
				return FALSE;
			hidden = FALSE;
			return TRUE;
		}

		bool
		HideAllSurfaces		(					)
		{
			hidden = TRUE;
			return TRUE;
		}

		bool
		ShowAllSurfaces		(					)
		{
			hidden = FALSE;
			return TRUE;
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return (inIndex==0) ? &dpystate : NULL;
		}

		uint
		GetDrawStride	(							)
		{
			return VU1_DMA_STRIDE;
		}

		bool
		SetDrawStart	(	uint			inOffset		)
		{
			uint drawAlign = inOffset & (VU1_DMA_STRIDE-1);
			if( drawAlign != 0 )
				return FALSE;	// not aligned !
			drawStart = inOffset;
			return TRUE;
		}

		bool
		SetDrawSize		(	uint			inSize			)
		{
			drawSize = inSize;
			return TRUE;
		}


		void
		_BuildDMA	(		)
		{
			dmac::Cursor* DC = dmac::GetFrontHeapCursor();

			vu1::ClipMode clipMode;
			if( enflags & NvShader::EN_THROUGHMODE )	clipMode = vu1::CM_NONE;	// 2D => no clip !
			else if( enflags & NvShader::EN_CLIPPING )	clipMode = vu1::CM_CLIPPING;
			else if( enflags & NvShader::EN_CULLING )	clipMode = vu1::CM_CULLING;
			else										clipMode = vu1::CM_NONE;
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;

			// Write VU1-context for all drawings in cursor

			uint bcl_flags  = ( mode2D ? vu1::BCL_MIN_2D : vu1::BCL_MIN );
			bcl_flags |= tolit ? (vu1::BCL_LIGHT_M|vu1::BCL_LIGHT_F) : 0;

			vu1::surface::Context *vu1ctxt0 = (vu1::surface::Context*)
				vu1::BuildViewContextList(	DC,
											sizeof(vu1::surface::Context),
											dpyctxt.chainHead,
											bcl_flags
											);

			// Build uploader (shared by all draws)
			uint32 uploader;
			vu1::surface::BuildData_CH1( uploader, DC, drawStart, drawSize, surf );

			// Init
			pkt.tadr = DC->i32;
			pkt.mcId = vu1::surface::Begin_CH1( DC, clipMode, tolit );

			vu1::surface::SetDirect_CH1( DC, pkt.texList->gsContext );
			for( int i = 0 ; i < dpyctxt.chainCpt ; i++ ) {
				vu1::surface::SetContext_CH1( DC, vu1ctxt0++ );
				vu1::surface::CallData_CH1( DC, uploader );
			}

			// Link
			pkt.link = DC->i32;
			vu1::surface::End_CH1( DC );

			pkt.SetSeparator( (enflags&NvShader::EN_SORTING) ? FALSE : TRUE );
		}

		void
		UpdatePacket	(	frame::Packet*	/*inPkt*/	)
		{
			dpystate.Activate();
		}

		bool
		ValidDrawRange	(		)
		{
			uint drawLast = drawStart + drawSize;
			uint drawMax  = surf->GetMaxSize();
			return (drawSize>0 && drawLast<=drawMax);
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );

			if( hidden )
				return FALSE;

			surf->UpdateBeforeDraw();
			if( !ValidDrawRange() )
				return FALSE;

			if( surf->HasComponent(NvSurface::CO_NORMAL) )
				return FALSE;	// not supported yet !

			_BuildDMA();

			frame::Push( &pkt );

			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			if( !ValidDrawRange() )		return FALSE;
			outTriCpt	= (drawSize-2) 	 * dpyctxt.chainCpt;
			outLocCpt	= drawSize		 * dpyctxt.chainCpt;
			outVtxCpt	= drawSize		 * dpyctxt.chainCpt;
			outPrimCpt	= drawSize		 * dpyctxt.chainCpt;
			return TRUE;
		}
	};

}




NvGeomShader*
nv_geomshader_surf_Create	(	NvSurface*		inSurface		)
{
	if( !inSurface )
		return NULL;

	// At least some components are needed !
	if( !inSurface->HasComponent(NvSurface::CO_LOC) )
		return NULL;
	if( inSurface->HasComponent(NvSurface::CO_NORMAL) )
		return NULL;	// Not supported yet !!!!

	Shader* shader = NvEngineNewA( Shader,16 );
	if( !shader )
		return NULL;

	shader->_Init( inSurface );
	return shader->GetInterface();
}





