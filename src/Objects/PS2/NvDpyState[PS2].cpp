/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/NvDpyState[PS2].h>
using namespace nv;


//#define	TEST_UV_OFFSET
//#define	DISABLE_TEXTURING


namespace
{

	inline uint	_UpdateMode	(	uint	inMode,
								uint	inMask,
								uint	inFlags		)
	{
		uint f = inFlags & inMask;
		return f ? (inMode&(~inMask))|f : inMode;
	}

}



void
DpyState_PS2::Init	(		)
{
	caps				= 0;
	enableOp			= EN_DEFAULT;
	enableOpMask		= EN_ALL_MASK;
	mode				= MODE_DEFAULT;
	src					= DpySource();
	Vec2Zero( &srcXY );
	srcBmp				= NULL;
	alphaPass			= 0.02f;
	blendCte			= 0.5f;
	dat					= uint( DAT_ALWAYS );
	wrColorMask			= 0xFFFFFFFF;
	SetMipmapping( 0, -3.75f );
	forceNoTexturing	= FALSE;
	changed				= TRUE;

	NV_ASSERT_A128( &gsCtxt );
	gsCtxt.Init( SCE_GS_SET_PRIM(4,1,0,0,0,0,0,0,0) );
	texCtxt.gsContext = &gsCtxt;
	texCtxt.id		  = -1;
	texCtxt.LODMask	  = 0;
	texCtxt.next	  = NULL;
}


void
DpyState_PS2::Copy	(	const DpyState_PS2&		inRef	)
{
	caps			= inRef.caps;
	enableOp		= inRef.enableOp;
	enableOpMask	= inRef.enableOpMask;
	mode			= inRef.mode;
	SetSource( inRef.src );
	srcXY			= inRef.srcXY;
	alphaPass		= inRef.alphaPass;
	blendCte		= inRef.blendCte;
	dat				= inRef.dat;
	wrColorMask		= inRef.wrColorMask;
	mipL			= inRef.mipL;
	mipK			= inRef.mipK;
	changed			= TRUE;
}


void
DpyState_PS2::Shut		(			)
{
	SafeRelease( srcBmp );
}


void
DpyState_PS2::Enable	(	uint		inEnableFlags		)
{
	SetEnabled( enableOp | inEnableFlags );
}


void
DpyState_PS2::Disable	(	uint		inEnableFlags		)
{
	SetEnabled( enableOp & (~inEnableFlags) );
}


void
DpyState_PS2::SetEnabled	(	uint	inEnableFlags		)
{
	uint old = (enableOp & enableOpMask);
	enableOp = (inEnableFlags & EN_ALL_MASK);
	uint cur = (enableOp & enableOpMask);
	if( cur != old )
		changed = TRUE;
}


uint
DpyState_PS2::GetEnabled	(					)
{
	return enableOp;
}


void
DpyState_PS2::SetEnabledMask	(	uint		inEnableFlags		)
{
	uint old = (enableOp & enableOpMask);
	enableOpMask = (inEnableFlags & EN_ALL_MASK);
	uint cur = (enableOp & enableOpMask);
	if( cur != old )
		changed = TRUE;
}


uint
DpyState_PS2::GetEnabledMask	(				)
{
	return enableOpMask;
}


uint
DpyState_PS2::GetGlobalEnabled	(				)
{
	return ( enableOp & enableOpMask );
}


void
DpyState_PS2::SetCapabilities		(	uint		inCapabilities		)
{
	if( caps != inCapabilities ) {
		caps = inCapabilities;
		changed = TRUE;
	}
}


uint
DpyState_PS2::GetCapabilities		(									)
{
	return caps;
}


void
DpyState_PS2::SetMode		(	uint		inModeFlags	)
{
	uint m = _UpdateMode( mode, TF_MASK, inModeFlags );
		 m = _UpdateMode( m,    TM_MASK, inModeFlags );
		 m = _UpdateMode( m,	   TR_MASK, inModeFlags );
		 m = _UpdateMode( m,    TW_MASK, inModeFlags );
		 m = _UpdateMode( m,    CM_MASK, inModeFlags );
		 m = _UpdateMode( m,    BM_MASK, inModeFlags );
		 m = _UpdateMode( m,    BS_MASK, inModeFlags );
	if( m != mode ) {
		changed = TRUE;
		mode = m;
	}
}


uint
DpyState_PS2::GetMode		(							)
{
	return mode;
}


bool
DpyState_PS2::SetSource	(	DpySource	inSource		)
{
	if( src == inSource )
		return TRUE;
	src = inSource;

	NvkBitmap* kbmp = NULL;
	if( src.Bitmap )
		kbmp = src.Bitmap->GetKInterface();
	SafeGetRef( srcBmp, kbmp );
	NV_ASSERT_IF( srcBmp, srcBmp->GetTRamId() >= 0 );

	changed = TRUE;
	return TRUE;
}


void
DpyState_PS2::GetSource		(	DpySource&	outSource		)
{
	outSource = src;
}


void
DpyState_PS2::SetSourceOffset	(	const Vec2&	inOffsetXY		)
{
	if( srcXY != inOffsetXY ) {
		srcXY = inOffsetXY;
		changed = TRUE;
	}
}


void
DpyState_PS2::GetSourceOffset	(	Vec2&		outOffsetXY		)
{
	Vec2Copy( &outOffsetXY, &srcXY );
}


bool
DpyState_PS2::SetAlphaPass	(	float		inAlphaPass		)
{
	inAlphaPass = Clamp( inAlphaPass, 0.0f, 1.0f );
	if( inAlphaPass != alphaPass ) {
		alphaPass = inAlphaPass;
		changed = TRUE;
	}
	return TRUE;
}


float
DpyState_PS2::GetAlphaPass	(				)
{
	return alphaPass;
}


bool
DpyState_PS2::SetBlendSrcCte	(	float		inCte	)
{
	inCte = Clamp( inCte, 0.0f, 1.0f );
	if( inCte != blendCte ) {
		blendCte = inCte;
		changed = TRUE;
	}
	return TRUE;
}


float
DpyState_PS2::GetBlendSrcCte	(			)
{
	return blendCte;
}


void
DpyState_PS2::SetWRColorMask	(	uint32		inRGBAMask		)
{
	if( wrColorMask != inRGBAMask ) {
		wrColorMask = inRGBAMask;
		changed = TRUE;
	}
}


uint32
DpyState_PS2::GetWRColorMask	(				)
{
	return wrColorMask;
}


void
DpyState_PS2::SetMipmapping	(	int			inL,
								float		inK		)
{
	mipL = int16( inL );
	if( inK < 0 )	inK += 256.0f;
	mipK = int16( inK * 16.0f );
	changed = TRUE;
}


void
DpyState_PS2::SetDestAlphaTest	(	DestAlphaTest		inDAT	)
{
	dat = uint( inDAT );
	changed = TRUE;
}


void
DpyState_PS2::ForceNoTexturing	(	bool		inOnOff		)
{
	if( forceNoTexturing != inOnOff ) {
		forceNoTexturing = inOnOff;
		changed = TRUE;
	}
}


bool
DpyState_PS2::HasValidSource	(		)
{
	NV_ASSERT_IF( srcBmp, srcBmp->GetTRamId() >= 0 );
	return ( srcBmp || src.Raster!=DPYR_NULL );
}


bool
DpyState_PS2::IsTexturing	(			)
{
	return (GetGlobalEnabled()&EN_TEXTURING) && (caps&CF_HAS_TEX0) && HasValidSource() && (!forceNoTexturing);
}


void
DpyState_PS2::SetTexturing	(	uint32			inBitmapUID,
								int				inL,
								float			inK,
								float			inMappingRatio	)
{
	NvBitmap* bmpP = NvBitmap::Create( inBitmapUID );
	SetSource( DpySource(bmpP) );
	SafeRelease( bmpP );
	if( !srcBmp )
		return;

	int   _mipL = 0;
	float _mipK = -3.75f;

	if( inL || inK )
	{
		_mipL = inL;
		_mipK = inK;
	}
	else if( inMappingRatio>0 && srcBmp && srcBmp->GetWidth()>0 )
	{
		// We consider here that :
		// o fov=PI/4 => aspect=1/Tanf(Pi/4)=1
		// o single fullscreen viewport
		float r  = inMappingRatio;
		float w  = int( srcBmp->GetWidth() );
		float s  = int( DpyManager::GetPhysicalWidth()/2 );
		float z0 = s /( w * r );
		_mipK	 = - Log2f( z0 );
		_mipK	 = Clamp( _mipK, -7.0f, 0.0f );
	//	Printf( "r=%f w=%f => K=%f\n", r, w, mipK );
	}

//	Printf( "set mipmapping L=%d K=%f\n", _mipL, _mipK );
	SetMipmapping( _mipL, _mipK );
}





void
DpyState_PS2::Activate	(		)
{
	#ifdef TEST_UV_OFFSET
	{
		nv::clock::Time t0;
		nv::clock::GetTime( &t0 );
		float t = float(t0);
		Vec2  stOffset = Vec2( Cos(t*10.f), Sin(t*10.f) ) * 0.1f;
		SetSourceOffset( stOffset );
	}
	#endif

	bool texturing = IsTexturing();
	#ifdef DISABLE_TEXTURING
	texturing = FALSE;
	#endif

	// Always update the source bitmap
	int tex_id = -1;
	if( texturing && srcBmp ) {
		srcBmp->UpdateBeforeDraw();
		tex_id = srcBmp->GetTRamId();
		NV_ASSERT( tex_id >= 0 );
	}

	// Changed ?
	if( !changed )
		return;
	changed = FALSE;


	//
	// Setup common GS registers

	uint enflags = GetGlobalEnabled();
	uint prim	 = gsCtxt.GetPRIM();

	// Blending
	static char alphaMode[] = {
	//  A, B, D
		0, 0, 0,	// (BM_SELECT)
		2, 2, 0,	// BM_NONE
		0, 1, 1,	// BM_FILTER
		0, 2, 1,	// BM_ADD
		2, 0, 1,	// BM_SUB
		0, 2, 2,	// BM_MODULATE
		1, 0, 0,	// BM_FILTER2
		1, 2, 0,	// BM_ADD2
		2, 1, 0,	// BM_SUB2
		1, 2, 2,	// BM_MODULATE2
		2, 2, 1,	// BM_DUMMY
		2, 2, 2,	// BM_ZERO
	};
	uint bs  = mode & BS_MASK;
	uint bm  = mode & BM_MASK;
	bool bmf = FALSE;
	if( bm == BM_SELECT ) {	// select ?
		if( texturing && srcBmp && srcBmp->GetAlphaStatus() == NvBitmap::AS_VARIED ) {
			bm  = BM_FILTER;
			bmf = TRUE;
		} else {
			bm = BM_NONE;
		}
	}
	NV_ASSERT( bm>BM_SELECT && bm<=BM_ZERO );
	NV_COMPILE_TIME_ASSERT( BM_SELECT   == (1<<13) );
	NV_COMPILE_TIME_ASSERT( BS_FRAGMENT == (1<<17) );
	uint aM = (bm>>13) - 1;
	uint aC = (bs>>17) - 1;
	NV_ASSERT( aM>0 && aM<=11 );
	NV_ASSERT( aC <= 2 );
	char* aMP = &alphaMode[aM*3];
	gsCtxt.alpha[0] = SCE_GS_SET_ALPHA( aMP[0], aMP[1], aC, aMP[2], 0 );
	gsCtxt.alpha[1] = uint32(blendCte * 128.0f);
	prim |= GS_PRIM_ABE_M;	// Always ABE=1 !

	// Alpha pass test
	uint aref = bmf ? 0 : uint( alphaPass * 128.0f );
	gsCtxt.test[0] &= ~(GS_TEST_ATE_M|GS_TEST_ATST_M|GS_TEST_AREF_M|GS_TEST_AFAIL_M);
	gsCtxt.test[0] |= (1<<GS_TEST_ATE_O) | (5<<GS_TEST_ATST_O) | (aref<<GS_TEST_AREF_O);

	// EN_WR_COLOR
	if( enflags & EN_WR_COLOR )
		gsCtxt.frame[1] = ~ ConvertRGBA( PSM_ABGR32, wrColorMask );
	else
		gsCtxt.frame[1] = ~ 0U;

	// EN_RD_DEPTH
	gsCtxt.test[0] &= ~(GS_TEST_ZTE_M|GS_TEST_ZTST_M);
	if( enflags & EN_RD_DEPTH )
		gsCtxt.test[0] |= (1<<GS_TEST_ZTE_O) | (2<<GS_TEST_ZTST_O);		// GEQUAL
	else
		gsCtxt.test[0] |= (1<<GS_TEST_ZTE_O) | (1<<GS_TEST_ZTST_O);		// ALWAYS

	// EN_WR_DEPTH
	gsCtxt.zbuf[1] = (!bmf && (enflags&EN_WR_DEPTH)) ? 0 : 1;

	// EN_RD_STENCIL
	gsCtxt.test[0] &= ~(GS_TEST_DATE_M|GS_TEST_DATM_M);
	gsCtxt.test[0] |= dat;


	//
	// Setup texturing GS registers

	if( texturing )
	{
		prim |= GS_PRIM_TME_M;
		gsCtxt.EnableTex();

		if( tex_id >= 0 ) {
			// bitmap as source
			texCtxt.id	    = tex_id;
			texCtxt.LODMask = ~0U;	// all mipmaps
		} else {
			// raster as source
			DpyManager::Raster& raster = DpyManager::raster[ int(src.Raster) ];
			cpy64to32( gsCtxt.tex1, &raster.rgbaTex1Reg );
			cpy64to32( gsCtxt.tex0, &raster.rgbaTex0Reg );
			// Ignore miptbp1 / miptbp2
			gsCtxt.__0x34 = SCE_GS_NOP;
			gsCtxt.__0x36 = SCE_GS_NOP;
		}

		// tex-function
		gsCtxt.tex0[1] &= ~(GS_TEX0_TFX_M>>32);
		uint tf = mode & TF_MASK;
		if( tf == TF_DECAL || (caps&(CF_HAS_RGBA|CF_HAS_RGB)) == 0 )		gsCtxt.tex0[1] |= 1 << (GS_TEX0_TFX_O-32);
		else if( tf == TF_MODULATE )										gsCtxt.tex0[1] |= 0 << (GS_TEX0_TFX_O-32);
		else if( tf == TF_HIGHLIGHT )										gsCtxt.tex0[1] |= 2 << (GS_TEX0_TFX_O-32);
		else if( tf == TF_HIGHLIGHT2 )										gsCtxt.tex0[1] |= 3 << (GS_TEX0_TFX_O-32);

		// tex-mag-sampler
		gsCtxt.tex1[0] &= ~(GS_TEX1_MMAG_M);
		uint tm = mode & TM_MASK;
		if( tm == TM_LINEAR )	gsCtxt.tex1[0] |= 1 << (GS_TEX1_MMAG_O);
		else					gsCtxt.tex1[0] |= 0 << (GS_TEX1_MMAG_O);

		// tex-reduce-sampler
		// minimum 8x8 pour bilinear et trilinear !
		gsCtxt.tex1[0] &= ~(GS_TEX1_MMIN_M);
		uint tr = mode & TR_MASK;
		if( tr == TR_NEAREST_MIP0 )				gsCtxt.tex1[0] |= (0<<GS_TEX1_MMIN_O);
		else if( tr == TR_LINEAR_MIP0 )			gsCtxt.tex1[0] |= (1<<GS_TEX1_MMIN_O);
		else if( tr == TR_NEAREST_NEAREST )		gsCtxt.tex1[0] |= (2<<GS_TEX1_MMIN_O);
		else if( tr == TR_NEAREST_LINEAR )		gsCtxt.tex1[0] |= (3<<GS_TEX1_MMIN_O);
		else if( tr == TR_LINEAR_NEAREST )		gsCtxt.tex1[0] |= (4<<GS_TEX1_MMIN_O);
		else if( tr == TR_LINEAR_LINEAR )		gsCtxt.tex1[0] |= (5<<GS_TEX1_MMIN_O);

		// L & K
		gsCtxt.tex1[0] &= ~(GS_TEX1_L_M);
		gsCtxt.tex1[0] |= mipL << GS_TEX1_L_O;
		gsCtxt.tex1[1]  = mipK;

		// tex-wrapping-mode
		gsCtxt.clamp[0] &= ~(GS_CLAMP_WMS_M|GS_CLAMP_WMT_M);
		uint tw = mode & TW_MASK;
		if( tw == TW_REPEAT_REPEAT )		gsCtxt.clamp[0] |= (0<<GS_CLAMP_WMS_O) | (0<<GS_CLAMP_WMT_O);
		else if( tw == TW_REPEAT_CLAMP )	gsCtxt.clamp[0] |= (0<<GS_CLAMP_WMS_O) | (1<<GS_CLAMP_WMT_O);
		else if( tw == TW_CLAMP_REPEAT )	gsCtxt.clamp[0] |= (1<<GS_CLAMP_WMS_O) | (0<<GS_CLAMP_WMT_O);
		else if( tw == TW_CLAMP_CLAMP )		gsCtxt.clamp[0] |= (1<<GS_CLAMP_WMS_O) | (1<<GS_CLAMP_WMT_O);

		// ST-offset
		gsCtxt.stOffset.x = srcXY.x;
		gsCtxt.stOffset.y = srcXY.y;
	}
	else
	{
		prim &= ~(GS_PRIM_TME_M);
		gsCtxt.DisableTex();
		texCtxt.id		= -1;
		texCtxt.LODMask = 0;	// no mipmap
	}

	// Writeback PRIM register
	gsCtxt.SetPRIM( prim );
}




