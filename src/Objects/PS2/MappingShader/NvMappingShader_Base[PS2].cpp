/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#include <NvMesh.h>
#include <NvSurface.h>
#include "NvMappingShader_Base[PS2].h"


NvMappingShader*	nv_mapshader_mesh_Create	(	NvMesh*		inMesh	);
NvMappingShader*	nv_mapshader_surf_Create	(	NvSurface*	inRsc	);


NvMappingShader*
NvMappingShader::Create	(	NvResource*		inRsc	)
{
	if( !inRsc )
		return NULL;
	if( inRsc->GetRscType() == NvMesh::TYPE )
		return nv_mapshader_mesh_Create( (NvMesh*)inRsc );
	if( inRsc->GetRscType() == NvSurface::TYPE )
		return nv_mapshader_surf_Create( (NvSurface*)inRsc );
	return NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( MappingShader,	NvInterface*,	GetBase												)
NVITF_CAL0( MappingShader,	void,			AddRef												)
NVITF_MTH0( MappingShader,	uint,			GetRefCpt											)
NVITF_CAL0( MappingShader,	void,			Release												)
NVITF_MTH0( MappingShader,	NvResource*,	GetResource											)
NVITF_CAL1( MappingShader,	void,			Enable,				uint32							)
NVITF_CAL1( MappingShader,	void,			Disable,			uint32							)
NVITF_CAL1( MappingShader,	void,			SetEnabled,			uint32							)
NVITF_MTH0( MappingShader,	uint32,			GetEnabled											)
NVITF_MTH1( MappingShader,	DpyState*,		GetDisplayState,	uint							)
NVITF_MTH1( MappingShader,	bool,			HideSurface,		uint							)
NVITF_MTH1( MappingShader,	bool,			ShowSurface,		uint							)
NVITF_MTH0( MappingShader,	bool,			HideAllSurfaces										)
NVITF_MTH0( MappingShader,	bool,			ShowAllSurfaces										)
NVITF_MTH0( MappingShader,	uint,			GetDrawStride										)
NVITF_MTH1( MappingShader,	bool,			SetDrawStart,		uint							)
NVITF_MTH1( MappingShader,	bool,			SetDrawSize,		uint							)
NVITF_MTH1( MappingShader,	bool,			SetMappingSrc,		MappingSrc						)
NVITF_MTH1( MappingShader,	bool,			SetMappingTR,		Matrix*							)
NVITF_MTH1( MappingShader,	bool,			SetColorFilter,		Vec4*							)


