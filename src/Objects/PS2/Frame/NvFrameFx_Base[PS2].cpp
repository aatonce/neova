/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/NvDpyObject[PS2].h>
#include <Kernel/PS2/NvkFrameFx[PS2].h>
#include <Kernel/Common/NvkCore_Mem.h>
using namespace nv;
using namespace gs::tools;



#define	DEFAULT_DFOG_RGBA		0x47495300
#define	MAP_ALLOC_MAX			32

#define	NO_PATH3_COMPLIANT_MSG	"NvFrameFx will slow down the PATH3 transfer performance !"
#define	NO_BEGIN_ERR_MSG		"No NvFrameFx sequence has been started. See the NvFrameFx::begin() method !"




void
NvkFrameFx::Kernel33::Reset	(		)
{
	((Kernel33*)this)->Reset();
}

void
NvkFrameFx::Kernel33::Normalize	(		)
{
	((Kernel33*)this)->Normalize();
}

void
NvkFrameFx::Kernel33::RotateCW	(		)
{
	((Kernel33*)this)->RotateCW();
}

void
NvkFrameFx::Kernel33::RotateCCW	(		)
{
	((Kernel33*)this)->RotateCCW();
}






//
// BASE



interface NvFrameFxBase : public NvDpyObject_PS2
{
	struct Map {
		int		paddr;			// addr in pages (-1 if unused)
		int		psize;			// size in pages
		int		sizex;
		int		sizey;
		uint	psm;
	};

	struct ClutMod {
		ALIGNED128( uint32, clut0[256] );		// front
		uint32		clut1[256];					// back
		int			baddr;						// 16x16 map
		bool		changed;
	};


	NvFrameFx					itf;
	NvkFrameFx					kitf;

	frame::Packet				pkt0, pkt1;
	frame::Packet				*buildPkt,				// user writing packet ...
								*drawPkt;				// drawing packet ...
	dmac::Cursor				buildDC;

	mem::Allocator*				mapAllocator;
	Map							map[ MAP_ALLOC_MAX ];
	int							frontMapId;
	int							frameMapId;
	int							depthMapId;
	int							offMapId;
	uint						texBAddr;				// texture buffer low baddr in vram

	uint						combineFlags;
	uint32						FBMSK;
	uint32						ZVALUE;
	uint32						RGBA;
	sceGsAlpha					gs_ALPHA;
	sceGsTest					gs_TEST;
	sceGsZbuf					gs_ZBUF;
	sceGsScissor				gs_SCISSOR;
	bool						drawScissor;			// Use DpyManager viewport context ?
	vector<uint64*>				drawScissorA;
	vector<ClutMod>				clutModA;

	// GenerateFX() parameters
	int							genfx_outlineClutNo;
	int							genfx_fogClutNo;
	int							genfx_dofClutNo;


	#define	GET_MAP1( inVar, inId )									\
			Map* inVar = GetMap( inId );							\
			if( !inVar )	return FALSE;

	#define	GET_MAP2( inVar0, inId0, inVar1, inId1 )				\
			GET_MAP1( inVar0, inId0 )								\
			GET_MAP1( inVar1, inId1 )


	virtual ~NvFrameFxBase	(	)	{}				// needed by nv::DestructInPlace<T>(ptr)


	void
	_Init		(		)
	{
		InitDpyObject();

		// init packets
		pkt0.Init( this );
		pkt0.SetSeparator( TRUE );			// always separator !
		pkt0.SetPATH3Compliant( FALSE );

		pkt1.Init( this );
		pkt1.SetSeparator( TRUE );			// always separator !
		pkt1.SetPATH3Compliant( FALSE );

		buildPkt = drawPkt = NULL;

		mapAllocator = NULL;

		genfx_outlineClutNo = -1;
		genfx_fogClutNo		= -1;
		genfx_dofClutNo		= -1;
	}


	void
	Release	(		)
	{
		if( ShutDpyObject() ) {
			NV_ASSERT( !pkt0.IsDrawing() );
			NV_ASSERT( !pkt1.IsDrawing() );
			SafeFree( pkt0.tadr );
			SafeFree( pkt1.tadr );
			SafeRelease( mapAllocator );
			NvEngineDelete( this );
		}
	}


	NvkFrameFx*
	GetKInterface	(			)
	{
		return &kitf;
	}


	NvFrameFx*
	GetInterface		(		)
	{
		return &itf;
	}


	uint
	_ApplyCombine	(	dmac::Cursor*	inDC,
						bool			inTME	)
	{
		uint gs_flags = 0;

		// alpha
		if( combineFlags & NvkFrameFx::CB_BLEND ) {
			gs::SetADReg( inDC, SCE_GS_ALPHA_2, AS_UINT64(gs_ALPHA) );
			gs_flags |= PF_ABE;
		}

		// scissor
		if( combineFlags & NvkFrameFx::CB_VIEWPORT ) {
			if( drawScissor ) {
				drawScissorA.push_back( inDC->i64 );
				buildPkt->SetNeedUpdate( TRUE );
			}
			gs::SetADReg( inDC, SCE_GS_SCISSOR_2, AS_UINT64(gs_SCISSOR) );
			gs_flags |= PF_NOSCISSOR;
		}

		// atest & ztest
		sceGsTest testReg = gs_TEST;
		if( !(combineFlags & NvkFrameFx::CB_ATEST) )
			testReg.ATE = testReg.DATE = 0;	// disabled
		if( !(combineFlags & NvkFrameFx::CB_ZTEST) )
			testReg.ZTST = 1;				// always (can't be disabled, cf SCE TRC)
		gs::SetADReg( inDC, SCE_GS_TEST_2, AS_UINT64(testReg) );

		// zbuf
		sceGsZbuf zbufReg = gs_ZBUF;
		zbufReg.ZMSK = (combineFlags & NvkFrameFx::CB_ZWRITE) ? 0 : 1;
		gs::SetADReg( inDC, SCE_GS_ZBUF_2, AS_UINT64(zbufReg) );

		// colwrap
		if( combineFlags & NvkFrameFx::CB_COLWRAP )	gs::Set_Wrap_COLCLAMP( inDC );
		else										gs::Set_Clamp_COLCLAMP( inDC );

		// nearest
		if( combineFlags & NvkFrameFx::CB_NEAREST )
			gs_flags |= PF_NEAREST;

		// rgba
		if( inTME ) {
			// For textured GS passes
			if( combineFlags & NvkFrameFx::CB_MODULATE ) {
				gs::Set_RGBAQ( inDC, GetDpyGouraudRGBA(PSM_ABGR32,RGBA) );
				gs_flags |= PF_MODULATE;
			}
			if( combineFlags & NvkFrameFx::CB_AEM1 ) {
				gs::Set_TEXA( inDC, 0xFF, 1, 0x80 );	// At = 0|255
				gs_flags |= PF_PSM24;
			}
		} else {
			// For no-textured GS passes
			gs::Set_RGBAQ( inDC, GetDpyColorRGBA(PSM_ABGR32,RGBA) );
		}

		return gs_flags;
	}


	bool
	Begin		(			)
	{
		if( buildPkt )
			return FALSE;

		// Select free pkt to build
		if( !pkt0.IsDrawing() )			buildPkt = &pkt0;
		else if( !pkt1.IsDrawing() )	buildPkt = &pkt1;
		else	return FALSE;
		drawPkt = NULL;

		// Init vram manager (unit is vram page )
		if( !mapAllocator )
			mapAllocator = mem::CreateOuterAllocator( 0, 512, MAP_ALLOC_MAX, FALSE );
		mapAllocator->Reset();
		for( int i = 0 ; i < MAP_ALLOC_MAX ; i++ )
			map[i].paddr = -1;

		// Alloc in place
		{
			frontMapId = 0;
			frameMapId = 1;
			depthMapId = 2;
			offMapId   = 3;
			NV_COMPILE_TIME_ASSERT( int(DpyManager::VRM_OFF) == 3 );
			uint baddr, psm, sx, sy;
			for( int i = 0 ; i < 4 ; i++ ) {
				DpyManager::GetVRamMapping( DpyManager::VRamBuffer(i), baddr, psm, sx, sy );
				map[i].paddr = baddr>>5;
				map[i].psize = gs::tools::GetPageSize( psm, sx, sy );
				map[i].sizex = sx;
				map[i].sizey = sy;
				map[i].psm   = psm;
				mapAllocator->AllocInPlace( (void*)map[i].paddr, map[i].psize );
			}
			// Get the texture buffer low addr
			DpyManager::GetVRamMapping( DpyManager::VRM_TEX, texBAddr, psm, sx, sy );
		}

		// Init state
		FBMSK  = 0;
		ZVALUE = 0;
		RGBA   = 0xFFFFFFFF;

		// Init combine
		AS_UINT64( gs_ALPHA   ) = SCE_GS_SET_ALPHA( 2, 2, 2, 2, 0 );
		AS_UINT64( gs_TEST    ) = SCE_GS_SET_TEST( 1, 1, 0, 0, 1, 0, 1, 1 );
		AS_UINT64( gs_ZBUF    ) = SCE_GS_SET_ZBUF( 0, 0, 1 );
		AS_UINT64( gs_SCISSOR ) = SCE_GS_SET_SCISSOR( 0, 31, 0, 31 );
		combineFlags = NvkFrameFx::CB_DEFAULT;
		drawScissor = FALSE;
		drawScissorA.resize(0);
		clutModA.resize(0);

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );

		// Init build packet
		SafeFree( buildPkt->tadr );
		buildPkt->tadr = DC->i32;
		buildPkt->SetNeedUpdate( FALSE );

		bool opened = Gs_Open_CH1( DC );
		NV_ASSERT( opened );

		// Reset GenerateFx()
		genfx_outlineClutNo = -1;
		genfx_fogClutNo		= -1;
		genfx_dofClutNo		= -1;

		return TRUE;
	}


	void
	End			(			)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );

		Gs_Close_CH1( DC );

		// Link
		NV_ASSERT_DC( DC );
		buildPkt->link = DC->i32;
		*(DC->i128++) = DMA_TAG_END;

		// Pkt Relocation
		pvoid DCDup = (uint8*) DC->Dup( buildPkt->tadr );
		DC->i32 = buildPkt->tadr;

		uint32 dp = uint32(DCDup) - uint32(buildPkt->tadr);
		buildPkt->Translate( dp );

		// drawScissor relocation
		for( uint i = 0 ; i < drawScissorA.size(); i++ ) {
			drawScissorA[i] = (uint64*)( uint32(drawScissorA[i]) + dp );
			NV_ASSERT_A64( drawScissorA[i] );
		}

		// Build -> Draw packet
		drawPkt = buildPkt;
		buildPkt = NULL;
	}


	int
	GetSystemMap		(	NvkFrameFx::SystemMap	inSysMap	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		if( inSysMap == NvkFrameFx::SM_INFRAME )	return frameMapId;
		if( inSysMap == NvkFrameFx::SM_OFFFRAME )	return offMapId;
		if( inSysMap == NvkFrameFx::SM_DEPTHFRAME )	return depthMapId;
		return -1;
	}


	Map*
	GetMap				(	int					inMapId		)
	{
		if( inMapId < 0 )				return NULL;
		if( inMapId >= MAP_ALLOC_MAX )	return NULL;
		if( map[inMapId].paddr < 0 )	return NULL;
		return &map[inMapId];
	}


	int
	FindFreeMap			(		)
	{
		for( int i = 0 ; i < MAP_ALLOC_MAX ; i++ ) {
			if( map[i].paddr < 0 )
				return i;
		}
		return -1;
	}


	int
	AllocMap			(	int					inSizeX,
							int					inSizeY		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		if( inSizeX<=0 || inSizeY<=0 )
			return -1;

		int mapId = FindFreeMap();
		if( mapId < 0 )	return -1;

		uint psize = gs::tools::GetPageSize( SCE_GS_PSMCT32, inSizeX, inSizeY );
		uint paddr = (uint) mapAllocator->Alloc( psize, 0 );
		if( paddr == 0 )
			return -1;	// alloc returns NULL, but 0 is the inplace front addr, never free'ed !

		map[ mapId ].paddr = paddr;
		map[ mapId ].psize = psize;
		map[ mapId ].sizex = inSizeX;
		map[ mapId ].sizey = inSizeY;
		map[ mapId ].psm   = SCE_GS_PSMCT32;
	/*
		// Map allocated in texture buffer ?
		uint map_baddr_end = ( paddr + psize ) << 5;
		if( map_baddr_end > texBAddr ) {
			NV_DEBUG_WARNING( NO_PATH3_COMPLIANT_MSG );
			buildPkt->SetPATH3Compliant( FALSE );
		}
	*/
		return mapId;
	}


	int
	GetMapWidth			(	int					inMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		Map* m = GetMap( inMapId );
		return m ? m->sizex : -1;
	}


	int
	GetMapHeight		(	int					inMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		Map* m = GetMap( inMapId );
		return m ? m->sizey : -1;
	}


	void
	FreeMap				(	int					inMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		// Can't free the front map !
		if( inMapId==frontMapId )
			return;
		Map* m = GetMap( inMapId );
		if( !m )
			return;
		mapAllocator->Free( (void*)m->paddr );
		m->paddr = -1;
		if( inMapId == frameMapId )			frameMapId = -1;
		else if( inMapId == depthMapId )	depthMapId = -1;
		else if( inMapId == offMapId )		offMapId   = -1;
	}


	void
	Enable			(	uint				inCombineFlags	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		combineFlags |= inCombineFlags;
	}


	void
	Disable			(	uint				inCombineFlags	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		combineFlags &= ~inCombineFlags;
	}


	void
	Setup			(	uint				inCombineFlags	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		combineFlags = inCombineFlags;
	}


	void
	SetBlend		(	NvkFrameFx::InputColor	inA,
						NvkFrameFx::InputColor	inB,
						NvkFrameFx::InputAlpha	inC,
						NvkFrameFx::InputColor	inD,
						float					inFIX = 0.f	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		gs_ALPHA.A   = int( inA );
		gs_ALPHA.B   = int( inB );
		gs_ALPHA.C   = int( inC );
		gs_ALPHA.D   = int( inD );
		gs_ALPHA.FIX = int( Clamp(inFIX,0.0f,1.9999f) * 128.0f );
	}


	void
	SetViewport		(	const Vec4&			inViewport	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		gs_SCISSOR.SCAX0 = int( inViewport.x );
		gs_SCISSOR.SCAX1 = int( inViewport.x + inViewport.z - 1.0f );
		gs_SCISSOR.SCAY0 = int( inViewport.y );
		gs_SCISSOR.SCAY1 = int( inViewport.y + inViewport.w - 1.0f );
		drawScissor = FALSE;
	}


	void
	SetDrawViewport	(					)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		drawScissor = TRUE;
	}


	void
	SetRGBAMask			(	uint			inRGBA		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		FBMSK = ConvertRGBA( PSM_ABGR32, inRGBA );
	}


	void
	SetRGBAValue		(	uint32			inRGBA		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		RGBA = inRGBA;
	}


	void
	SetAlphaTest		(	NvkFrameFx::FragmentATEST	inFragment,
							float						inFragmentRef,
							NvkFrameFx::BufferATEST		inBuffer		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		gs_TEST.ATE   = 1;
		gs_TEST.ATST  = int( inFragment ) + 1;
		gs_TEST.AREF  = int( Clamp(inFragmentRef,0.0f,1.9999f) * 128.0f );
		gs_TEST.AFAIL = 0;
		gs_TEST.DATE  = ( inBuffer == NvkFrameFx::BA_ALWAYS ) ? 0 : 1;
		gs_TEST.DATM  = ( inBuffer == NvkFrameFx::BA_ZERO ) ? 0 : 1;
	}


	void
	SetDepthValue		(	float				inZ,
							const Matrix&		inProjTR	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );
		ZVALUE = DpyManager::GetZProjBuffValue( inZ, (Matrix*)&inProjTR );
	}


	void
	SetDepthTest		(	int							inMapId,
							NvkFrameFx::DepthTEST		inTest			)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		Map* m = GetMap( inMapId );
		if( m ) {
			gs_TEST.ZTE  = 1;	// can't be disabled, cf SCE TRC
			gs_TEST.ZTST = int( inTest ) + 1;
			gs_ZBUF.ZBP  = m->paddr;
			gs_ZBUF.PSM  = 0;	// PSMZ32
			gs_ZBUF.ZMSK = 0;	// updated
		} else {
			gs_TEST.ZTE  = 1;	// can't be disabled, cf SCE TRC
			gs_TEST.ZTST = 1;	// always
			gs_ZBUF.ZMSK = 1;	// not updated
		}
	}


	bool
	ApplyPass		(	int					inFromToMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( m, inFromToMapId );
		uint flags = _ApplyCombine( DC, FALSE );
		Gs_ApplyPass_CH1( DC, m->sizex, m->sizey, m->paddr<<5, flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	ApplyDepthPass	(	int					inFromToMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( m, inFromToMapId );
		uint flags = _ApplyCombine( DC, FALSE );
		Gs_ApplyDepthPass_CH1( DC, m->sizex, m->sizey, m->paddr<<5, flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	InvertPass		(	int					inFromToMapId	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( ftm, inFromToMapId );
		uint flags = _ApplyCombine( DC, FALSE );
		Gs_InvertPass_CH1( DC, ftm->sizex, ftm->sizey, ftm->paddr<<5, flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	InvertDepthPass		(	int					inFromToMapId	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( ftm, inFromToMapId );
		uint flags = _ApplyCombine( DC, FALSE );
		Gs_InvertDepthPass_CH1( DC, ftm->sizex, ftm->sizey, ftm->paddr<<5, flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	NormalizePass		(	int					inFromToMapId,
							float				inFactor,
							uint32				inAddRGBA = 0,
							uint32				inSubRGBA = 0		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		if( inFactor < 0.0f )	return FALSE;
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( m, inFromToMapId );
		uint flags = _ApplyCombine( DC, FALSE );

		// Scale
		if( inFactor != 1.0f ) {
			gs::Set_ALPHA_2( DC, 1, 2, 2, 2, int(Clamp(inFactor,0.0f,1.99999f)*128.0f) );
			Gs_ApplyPass_CH1( DC, m->sizex, m->sizey, m->paddr<<5, flags|PF_ABE, FBMSK, ZVALUE );
		}
		// Add
		if( inAddRGBA ) {
			gs::Set_RGBAQ( DC, GetDpyColorRGBA(PSM_ABGR32,inAddRGBA) );
			gs::Set_ALPHA_2( DC, 0, 2, 2, 1, 0x80 );
			Gs_ApplyPass_CH1( DC, m->sizex, m->sizey, m->paddr<<5, flags|PF_ABE, FBMSK, ZVALUE );
		}
		// Sub
		if( inSubRGBA ) {
			gs::Set_RGBAQ( DC, GetDpyColorRGBA(PSM_ABGR32,inSubRGBA) );
			gs::Set_ALPHA_2( DC, 2, 0, 2, 1, 0x80 );
			Gs_ApplyPass_CH1( DC, m->sizex, m->sizey, m->paddr<<5, flags|PF_ABE, FBMSK, ZVALUE );
		}
		return TRUE;
	}


	bool
	NormalizeDepthPass	(	int				inFromToMapId,
							float			inFactor,
							uint32			inAddRGBA = 0,
							uint32			inSubRGBA = 0		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		if( inFactor < 0.0f )	return FALSE;
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( m, inFromToMapId );
		uint flags = _ApplyCombine( DC, FALSE );

		// Scale
		if( inFactor != 1.0f ) {
			gs::Set_ALPHA_2( DC, 1, 2, 2, 2, int(Clamp(inFactor,0.0f,1.99999f)*128.0f) );
			Gs_ApplyDepthPass_CH1( DC, m->sizex, m->sizey, m->paddr<<5, flags|PF_ABE, FBMSK, ZVALUE );
		}
		// Add
		if( inAddRGBA ) {
			gs::Set_RGBAQ( DC, GetDpyColorRGBA(PSM_ABGR32,inAddRGBA) );
			gs::Set_ALPHA_2( DC, 0, 2, 2, 1, int(Clamp(inFactor,0.0f,1.99999f)*128.0f) );
			Gs_ApplyDepthPass_CH1( DC, m->sizex, m->sizey, m->paddr<<5, flags|PF_ABE, FBMSK, ZVALUE );
		}
		// Sub
		if( inSubRGBA ) {
			gs::Set_RGBAQ( DC, GetDpyColorRGBA(PSM_ABGR32,inSubRGBA) );
			gs::Set_ALPHA_2( DC, 2, 0, 2, 1, int(Clamp(inFactor,0.0f,1.99999f)*128.0f) );
			Gs_ApplyDepthPass_CH1( DC, m->sizex, m->sizey, m->paddr<<5, flags|PF_ABE, FBMSK, ZVALUE );
		}
		return TRUE;
	}


	bool
	BlitPass			(	int					inFromMapId,
							Vec4*				inFromRegion,
							int					inToMapId,
							Vec4*				inToRegion,
							int					inClutId = -1	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		int clutBAddr = -1;
		if( inClutId >= 0 ) {
			NV_ASSERT( inClutId < int(clutModA.size()) );
			if( inClutId >= int(clutModA.size()) )	return FALSE;	// invalid clut !
			clutBAddr = clutModA[ inClutId ].baddr;
		}
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_BlitPass_CH1( DC, fm->sizex, fm->sizey, inFromRegion, fm->paddr<<5,
							 tm->sizex, tm->sizey, inToRegion,   tm->paddr<<5,
							 clutBAddr, flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	TranslatePass		(	int					inFromMapId,
							int					inToMapId,
							int					inToOffsetX = 0,
							int					inToOffsetY	= 0	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		if( fm->sizex != tm->sizex )	return FALSE;
		if( fm->sizey != tm->sizey )	return FALSE;
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_TranslatePass_CH1( DC, fm->sizex, fm->sizey, inToOffsetX, inToOffsetY, fm->paddr<<5, tm->paddr<<5, flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	CopyPass			(	int					inFromMapId,
							int					inToMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		if( fm->sizex != tm->sizex )	return FALSE;
		if( fm->sizey != tm->sizey )	return FALSE;
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_CopyPass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5, tm->paddr<<5, flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	ResizePass			(	int					inFromMapId,
							int					inToMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_ResizePass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5,
							   tm->sizex, tm->sizey, tm->paddr<<5,
							   flags, FBMSK, ZVALUE );
		return TRUE;
	}


	int
	Downx2Pass			(	int					inFromMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( fm, inFromMapId );
		int toMapId = AllocMap( fm->sizex>>1, fm->sizey>>1 );
		GET_MAP1( tm, toMapId );
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_Downx2Pass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5, tm->paddr<<5, flags, FBMSK, ZVALUE );
		return toMapId;
	}


	int
	Upx2Pass			(	int					inFromMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( fm, inFromMapId );
		int toMapId = AllocMap( fm->sizex<<1, fm->sizey<<1 );
		GET_MAP1( tm, toMapId );
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_Upx2Pass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5, tm->paddr<<5, flags, FBMSK, ZVALUE );
		return toMapId;
	}


	bool
	PreparePass			(	int					inFromMapId,
							int					inToMapId,
							float				inFactor	= 0.5f	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		if( inFactor <= 0.0f )	return FALSE;
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		uint flags = _ApplyCombine( DC, TRUE );
		return Gs_PreparePass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5,
									   tm->sizex, tm->sizey, tm->paddr<<5,
									   inFactor, flags, FBMSK, ZVALUE );
	}


	bool
	UnpreparePass		(	int					inFromMapId,
							int					inToMapId,
							float				inFactor	= 0.5f	)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		if( inFactor <= 0.0f )	return FALSE;
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		uint flags = _ApplyCombine( DC, TRUE );
		return Gs_UnpreparePass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5,
										 tm->sizex, tm->sizey, tm->paddr<<5,
										 inFactor, flags, FBMSK, ZVALUE );
	}


	bool
	ConvolvePass		(	int							inFromMapId,
							int							inToMapId,
							const NvkFrameFx::Kernel33&	inKernel		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		if( fm->sizex != tm->sizex )	return FALSE;
		if( fm->sizey != tm->sizey )	return FALSE;
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_ConvolvePass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5, tm->paddr<<5,
								 *((Kernel33*)&inKernel), flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	SepConvolvePass		(	int							inFromToMapId,
							const NvkFrameFx::Kernel33&	inKernel		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( ftm, inFromToMapId );
		int m0Id = AllocMap( ftm->sizex, ftm->sizey );
		GET_MAP1( m0, m0Id );
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_SepConvolvePass_CH1( DC, ftm->sizex, ftm->sizey, ftm->paddr<<5, m0->paddr<<5,
								    *((Kernel33*)&inKernel), flags, FBMSK, ZVALUE );
		FreeMap( m0Id );
		return TRUE;
	}


	bool
	DiffConvolvePass	(	int							inFromMapId,
							int							inToMapId,
							const NvkFrameFx::Kernel33&	inKernel		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		if( fm->sizex != tm->sizex )	return FALSE;
		if( fm->sizey != tm->sizey )	return FALSE;
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_DiffConvolvePass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5, tm->paddr<<5,
								 	 *((Kernel33*)&inKernel), flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	GaussianBlurPass	(	int					inFromToMapId,
							float				inRadius	= 2.f,
							float				inQuality	= 0.f		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( ftm, inFromToMapId );
		int m0Id = AllocMap( ftm->sizex, ftm->sizey );
		GET_MAP1( m0, m0Id );
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_GaussianBlurPass_CH1( DC, ftm->sizex, ftm->sizey, ftm->paddr<<5, m0->paddr<<5, inRadius, inQuality,
								     flags, FBMSK, ZVALUE );
		FreeMap( m0Id );
		return TRUE;
	}


	bool
	GlowBlurPass		(	int							inFromToMapId,
							const NvkFrameFx::Kernel33&	inKernel,
							float						inRadius  = 2.f,
							float						inQuality = 0.f		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( ftm, inFromToMapId );
		int m0Id = AllocMap( ftm->sizex, ftm->sizey );
		GET_MAP1( m0, m0Id );
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_GlowBlurPass_CH1( DC, ftm->sizex, ftm->sizey, ftm->paddr<<5, m0->paddr<<5, inRadius, inQuality,
								 (Kernel33*)&inKernel, flags, FBMSK, ZVALUE );
		FreeMap( m0Id );
		return TRUE;
	}


	bool
	FocalBlurPass		(	int							inFromToMapId,
							const NvkFrameFx::Kernel33&	inKernel,
							float						inRadius = 2.f,
							float						inQuality = 0.f		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( ftm, inFromToMapId );
		int m0Id = AllocMap( ftm->sizex, ftm->sizey );
		GET_MAP1( m0, m0Id );
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_FocalBlurPass_CH1( DC, ftm->sizex, ftm->sizey, ftm->paddr<<5, m0->paddr<<5, inRadius, inQuality,
								  (Kernel33*)&inKernel, flags, FBMSK, ZVALUE );
		FreeMap( m0Id );
		return TRUE;
	}


	bool
	DepthToAlphaPass	(	int					inFromMapId,
							int					inToMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		if( fm->sizex != tm->sizex )	return FALSE;
		if( fm->sizey != tm->sizey )	return FALSE;
		Gs_DepthToAlphaPass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5, tm->paddr<<5 );
		return TRUE;
	}


	bool
	RGBAToDepthPass		(	int					inFromMapId,
							int					inToMapId		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP2( fm, inFromMapId, tm, inToMapId );
		if( fm->sizex != tm->sizex )	return FALSE;
		if( fm->sizey != tm->sizey )	return FALSE;
		uint flags = _ApplyCombine( DC, TRUE );
		Gs_RGBAToDepthPass_CH1( DC, fm->sizex, fm->sizey, fm->paddr<<5, tm->paddr<<5, flags, FBMSK, ZVALUE );
		return TRUE;
	}


	bool
	TrxPass				(	int					inToMapId,
							uint128*			inData			)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		if( uint32(inData)&0xF )	return FALSE;
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( tm, inToMapId );
		Gs_TrxPass_CH1( DC, tm->sizex, tm->sizey, SCE_GS_PSMCT32, tm->paddr<<5, inData );
		// Gs_TrxPass_CH1 is not PATH3 compliant
	//	NV_DEBUG_WARNING( NO_PATH3_COMPLIANT_MSG );
	//	buildPkt->SetPATH3Compliant( FALSE );
		return TRUE;
	}


	bool
	AllocClut			(	int					inNbClut		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		if( inNbClut <= 0 )		return FALSE;
		if( clutModA.size() )	return FALSE;			// already done !
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		int nbPages = (inNbClut+7) >> 3;				// 8 cluts per page in PSMCT32
		int mapId = AllocMap( nbPages*64, 32 );
		GET_MAP1( cm, mapId );
		clutModA.resize( inNbClut );
		for( int i = 0 ; i < inNbClut ; i++ ) {
			clutModA[i].baddr   = (cm->paddr<<5) + i*4;		// 4 blocks per clut
			clutModA[i].changed = FALSE;
			NV_ASSERT_A128( clutModA[i].clut0 );
			Gs_TrxPass_CH1( DC, 16, 16, SCE_GS_PSMCT32, clutModA[i].baddr, (uint128*)clutModA[i].clut0 );
			buildPkt->SetNeedUpdate( TRUE );
			// Gs_TrxPass_CH1 is not PATH3 compliant
		//	NV_DEBUG_WARNING( NO_PATH3_COMPLIANT_MSG );
		//	buildPkt->SetPATH3Compliant( FALSE );
		}
		return TRUE;
	}


	bool
	SetClutColor		(	int					inClutId,
							uint8				inIndex,
							uint32				inRGBA			)
	{
	//	NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );	// Changed after buildPkt is done !
		if( inClutId < 0 || inClutId >= int(clutModA.size()) )	return FALSE;
		uint sIdx = (inIndex&0xE7) + ((inIndex&8)<<1) + ((inIndex&16)>>1);	// swizzled index
		clutModA[ inClutId ].clut1[ sIdx ] = GetDpyColorRGBA( PSM_ABGR32, inRGBA );
		clutModA[ inClutId ].changed = TRUE;	// has changed !!
		return TRUE;
	}


	bool
	GenerateHStripes	(	int					inToMapId,
							int					inStep	= 4		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( tm, inToMapId );
		Gs_GenerateHStripes_CH1( DC, inStep, tm->sizex, tm->sizey, tm->paddr<<5, FBMSK );
		return TRUE;
	}


	bool
	GenerateVStripes	(	int					inToMapId,
							int					inStep	= 4		)
	{
		NV_ASSERTC( buildPkt, NO_BEGIN_ERR_MSG );

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		GET_MAP1( tm, inToMapId );
		Gs_GenerateVStripes_CH1( DC, inStep, tm->sizex, tm->sizey, tm->paddr<<5, FBMSK );
		return TRUE;
	}


	void
	UpdatePacket	(	frame::Packet*		inPkt	)
	{
		NV_ASSERT( inPkt == drawPkt );

		// Update viewport
		if( drawScissorA.size() ) {
			NV_ASSERT( dpyctxt.chainHead >= 0 );
			NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + dpyctxt.chainHead;
			NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
			NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
			Vec4& vp = dview->viewport;
			for( uint i = 0 ; i < drawScissorA.size() ; i++ )
				*(drawScissorA[i]) = SCE_GS_SET_SCISSOR( int(vp.x), int(vp.x+vp.z-1), int(vp.y), int(vp.y+vp.w-1) );
		}

		// Update Clut modifiers (clut1 -> clut0)
		for( uint i = 0 ; i < clutModA.size() ; i++ ) {
			if( clutModA[i].changed ) {
				Memcpy( clutModA[i].clut0, clutModA[i].clut1, 256*4 );
				clutModA[i].changed = FALSE;
			}
		}
	}


	bool	GenerateFx	(	uint	inEN	)
	{
		if( !Begin() ) {
			DebugPrintf( "NvFrameFxBase: Begin() has failed !\n" );
			return FALSE;
		}

		bool doFog		= inEN & (1<<NvFrameFx::FX_DFOG);
		bool doDof		= inEN & (1<<NvFrameFx::FX_DOF);
		bool doBlur		= inEN & (1<<NvFrameFx::FX_BLUR);
		bool doBloom	= inEN & (1<<NvFrameFx::FX_BLOOM);
		bool doOutline	= inEN & (1<<NvFrameFx::FX_OUTLINE);

		// Get back buffer map
		int back  = GetSystemMap( NvkFrameFx::SM_INFRAME );
		int backW = GetMapWidth( back );
		int backH = GetMapHeight( back );

		// W-depth to back alpha ?
		if( doFog || doDof || doOutline ) {
			// Get back buffer map
			int depth = GetSystemMap( NvkFrameFx::SM_DEPTHFRAME );
			DepthToAlphaPass( depth, back );
			if( doFog || doDof ) {
				// Keep the depth map for outline
				// Free the depth map for others fx
				FreeMap( depth );
			}
		}

		// setup CLUTs
		int nbClut  = 0;
		if( doOutline ) {
			genfx_outlineClutNo = nbClut;
			nbClut++;
		}
		if( doFog ) {
			genfx_fogClutNo = nbClut;
			nbClut++;
		}
		if( doDof ) {
			genfx_dofClutNo = nbClut;
			nbClut++;
		}

		if( nbClut )
			AllocClut( nbClut );


		//
		// OUTLINE

		if( doOutline )
		{
			// Setup Outline clut
			for( int i = 0 ; i < 256 ; i++ ) {
				float z  = float(i) / 255.f;
				float cz = Pow( (1.f-z), 1.f );
			//	int   iz = Clamp(cz,0.f,1.f) * 255.f;
				int   iz = i;
				SetClutColor( genfx_outlineClutNo, i, GetPSM(PSM_RGBA32,iz,iz,iz,iz) );
			}

			float  tickness = 1.0f;
			float  nrm_thr  = 16.f/255.f;

			float  diff_w	 = 1.0f;
			int    step		 = 4;
			bool   showEdges = FALSE;

			uint32 nrm_thrc = GetPSM( PSM_RGBA32, Vec4(nrm_thr,nrm_thr,nrm_thr,nrm_thr) );

			NvkFrameFx::Kernel33 ker;
			ker.k00 = 0.f;		ker.k01 = diff_w;	ker.k02 = 0.f;
			ker.k10 = diff_w;	ker.k11 = 0.0f;		ker.k12 = diff_w;
			ker.k20 = 0.f;		ker.k21 = diff_w;	ker.k22 = 0.f;

			int sw   = backW / step;
			int sh   = backH / step;
			int tmp1 = AllocMap( sw, sh );
			int tmp2 = AllocMap( sw, sh );
			NV_ASSERT( tmp1 >= 0 );
			NV_ASSERT( tmp2 >= 0 );

			#ifdef _NVCOMP_ENABLE_DBG
			if( showEdges ) {
				SetRGBAMask( 0xFF );		// keep alpha
				SetRGBAValue( 0x00FF00FF );
				ApplyPass( back );			// clear back to green
			}
			#endif

			SetAlphaTest( NvkFrameFx::FA_GREATER, 0.f, NvkFrameFx::BA_ALWAYS );
			SetRGBAValue( 0x000000FF );
			SetRGBAMask( 0xFF );		// keep alpha
			for( int ix = 0 ; ix < step ; ix++ ) {
				for( int iy = 0 ; iy < step ; iy++ ) {
					Vec4 reg;
					reg.x = ix * sw;
					reg.y = iy * sh;
					reg.z = sw;
					reg.w = sh;

					BlitPass( back, &reg, tmp1, NULL, genfx_outlineClutNo );
					DiffConvolvePass( tmp1, tmp2, ker );
					if( tickness > 1.f )
						GaussianBlurPass( tmp2, tickness );
					NormalizePass( tmp2, 1.f, 0, nrm_thrc );

					Enable( NvkFrameFx::CB_AEM1 | NvkFrameFx::CB_MODULATE | NvkFrameFx::CB_ATEST );
					BlitPass( tmp2, NULL, back, &reg );
					Setup( NvkFrameFx::CB_DEFAULT );
				}
			}

			FreeMap( tmp1 );
			FreeMap( tmp2 );
		}


		//
		// DFOG

		if( doFog )
		{
			// Setup FOG clut
			for( int i = 0 ; i < 256 ; i++ )
				SetClutColor( genfx_fogClutNo, i, DEFAULT_DFOG_RGBA | (255-i) );
			SetClutColor( genfx_fogClutNo, 0, 0 );	// no FOG on background pixels !

			// Setup blending : fogC * A + backC * (1-A)
			SetBlend( NvkFrameFx::IC_FRAGMENT, NvkFrameFx::IC_BUFFER, NvkFrameFx::IA_FRAGMENT, NvkFrameFx::IC_BUFFER );
			SetRGBAMask( 0xFF );		// keep alpha
			Enable( NvkFrameFx::CB_BLEND );
			BlitPass( back, NULL, back, NULL, genfx_fogClutNo );
			Disable( NvkFrameFx::CB_BLEND );
		}


		//
		// DOF

		if( doDof )
		{
			// Setup DOF clut (only alpha component is used)
			for( int i = 0 ; i < 256 ; i++ )
				SetClutColor( genfx_dofClutNo, i, (255-i) );
			SetClutColor( genfx_dofClutNo, 0, 0 );	// no DOF on background pixels !

			// Apply DOF-CLUT in place in the back-alpha component
			SetRGBAMask( ~0xFF );		// write alpha only
			BlitPass( back, NULL, back, NULL, genfx_dofClutNo );
			SetRGBAMask( 0 );

			// Back buffer bluring
			int blur = AllocMap( 32+backW/2, 32+backH/2 );
			PreparePass( back, blur, 0.5f );
			GaussianBlurPass( blur, 2.0f, 1.0f );

			// Setup blending : blurC * A + backC * (1-A)
			SetBlend( NvkFrameFx::IC_FRAGMENT, NvkFrameFx::IC_BUFFER, NvkFrameFx::IA_BUFFER, NvkFrameFx::IC_BUFFER );
			Enable( NvkFrameFx::CB_BLEND );
			SetRGBAMask( 0xFF );		// keep alpha
			UnpreparePass( blur, back, 0.5f );
			Disable( NvkFrameFx::CB_BLEND );
			FreeMap( blur );
		}


		//
		// Bloom

		if( doBloom )
		{
			int tmp = AllocMap( 32+backW/2, 32+backH/2 );
			PreparePass( back, tmp, 0.5f );
			NormalizePass( tmp, 1.f, 0, 0x90909090 );
	
			NvkFrameFx::Kernel33 ker;
			ker.k00 = ker.k02 = ker.k20 = ker.k22 = 0.7f;
			ker.k01 = ker.k10 = ker.k12 = ker.k21 = 1.0f;
			ker.k11 = 0;
		//	ker.Normalize();
			GlowBlurPass( tmp, ker, 1.2f );
	
			SetBlend( NvkFrameFx::IC_FRAGMENT, NvkFrameFx::IC_ZERO, NvkFrameFx::IA_FIX, NvkFrameFx::IC_BUFFER, 0.3f );
			SetRGBAMask( 0xFF );		// keep alpha
			Enable( NvkFrameFx::CB_BLEND );
			UnpreparePass( tmp, back, 0.5f );
			Disable( NvkFrameFx::CB_BLEND );
			FreeMap( tmp );
		}


		//
		// BLUR

		if( doBlur )
		{
			// Back buffer bluring
			int blur = AllocMap( 32+backW/2, 32+backH/2 );
			PreparePass( back, blur, 0.5f );
			GaussianBlurPass( blur, 8.0f, 1.0f );
			SetRGBAMask( 0xFF );		// keep alpha
			UnpreparePass( blur, back, 0.5f );
			FreeMap( blur );
		}


		End();
		
		return TRUE;
	}


	bool
	SetDFogDepth		(	uint8			inDepthIdx,
							uint8			inDepth		)
	{
		return FALSE;
	}

	bool					
	SetDFogColor		(	uint32			inRGBA		)
	{
		//return SetClutColor( genfx_fogClutNo, inClutIdx, inRGBA );
		return FALSE;
	}

	bool
	SetDOFSharpness		(	uint8			inDepthIdx	,
							uint8			inSharpness	)
	{
		return FALSE;
	}
							
	bool
	SetBlurRadius		(	float			inRadius	)
	{
		return FALSE;
	}

	bool
	SetBloom			(	uint32			inRGBA,
							float			inRadius	)
	{
		return FALSE;
	}

	bool
	Draw	(		)
	{
		NV_ASSERT( refCpt );

		if( drawPkt ) {
			NV_ASSERT_A128( drawPkt->tadr );
			frame::Push( drawPkt );
		}

		return TRUE;
	}
};




NvFrameFx *
NvFrameFx::Create	(		)
{
	NvFrameFxBase * inst = NvEngineNewA( NvFrameFxBase,16 );
	if( !inst )
		return NULL;
	inst->_Init();
	return &inst->itf;
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameFx,	NvInterface*,	GetBase													)
NVITF_CAL0( FrameFx,	void,			AddRef													)
NVITF_MTH0( FrameFx,	uint,			GetRefCpt												)
NVITF_CAL0( FrameFx,	void,			Release													)
NVITF_MTH0( FrameFx,	NvkFrameFx*,	GetKInterface											)
NVITF_MTH1( FrameFx,	bool,			GenerateFx,			uint								)
NVITF_MTH1( FrameFx,	bool, 			SetDFogColor,		uint32								)
NVITF_MTH2( FrameFx,	bool, 			SetDFogDepth,		uint8,	uint8						)
NVITF_MTH2( FrameFx,	bool, 			SetDOFSharpness,	uint8,	uint8						)
NVITF_MTH1( FrameFx,	bool, 			SetBlurRadius,		float								)
NVITF_MTH2( FrameFx,	bool, 			SetBloom,			uint32,	float						)

NVKITF_MTH0( FrameFx,	NvInterface*,	GetBase													)
NVKITF_CAL0( FrameFx,	void,			AddRef													)
NVKITF_MTH0( FrameFx,	uint,			GetRefCpt												)
NVKITF_CAL0( FrameFx,	void,			Release													)
NVKITF_MTH0( FrameFx,	NvFrameFx*,		GetInterface											)
NVKITF_MTH0( FrameFx,	bool,			Begin													)
NVKITF_CAL0( FrameFx,	void,			End														)
NVKITF_MTH1( FrameFx,	int,			GetSystemMap,		SystemMap							)
NVKITF_MTH2( FrameFx,	int,			AllocMap,			int,	int							)
NVKITF_MTH1( FrameFx,	int,			GetMapWidth,		int									)
NVKITF_MTH1( FrameFx,	int,			GetMapHeight,		int									)
NVKITF_CAL1( FrameFx,	void,			FreeMap,			int									)

NVKITF_CAL1( FrameFx,	void,			Enable,				uint								)
NVKITF_CAL1( FrameFx,	void,			Disable,			uint								)
NVKITF_CAL1( FrameFx,	void,			Setup,				uint								)
NVKITF_CAL5( FrameFx,	void,			SetBlend,			InputColor,
															InputColor,
															InputAlpha,
															InputColor,
															float								)
NVKITF_CAL1( FrameFx,	void,			SetViewport,		const Vec4&							)
NVKITF_CAL0( FrameFx,	void,			SetDrawViewport											)
NVKITF_CAL1( FrameFx,	void,			SetRGBAMask,		uint32								)
NVKITF_CAL1( FrameFx,	void,			SetRGBAValue,		uint32								)
NVKITF_CAL3( FrameFx,	void,			SetAlphaTest,		FragmentATEST, float, BufferATEST	)
NVKITF_CAL2( FrameFx,	void,			SetDepthValue,		float, const Matrix&				)
NVKITF_CAL2( FrameFx,	void,			SetDepthTest,		int, DepthTEST						)
NVKITF_MTH1( FrameFx,	bool,			ApplyPass,			int									)
NVKITF_MTH1( FrameFx,	bool,			ApplyDepthPass,		int									)
NVKITF_MTH1( FrameFx,	bool,			InvertPass,			int									)
NVKITF_MTH1( FrameFx,	bool,			InvertDepthPass,	int									)
NVKITF_MTH4( FrameFx,	bool,			NormalizePass,		int, float, uint32, uint32			)
NVKITF_MTH4( FrameFx,	bool,			NormalizeDepthPass,	int, float, uint32, uint32			)
NVKITF_MTH5( FrameFx,	bool,			BlitPass,			int, Vec4*, int, Vec4*, int			)
NVKITF_MTH4( FrameFx,	bool,			TranslatePass,		int, int, int, int					)
NVKITF_MTH2( FrameFx,	bool,			CopyPass,			int, int							)
NVKITF_MTH2( FrameFx,	bool,			ResizePass,			int, int							)
NVKITF_MTH1( FrameFx,	int,			Downx2Pass,			int									)
NVKITF_MTH1( FrameFx,	int,			Upx2Pass,			int									)
NVKITF_MTH3( FrameFx,	bool,			PreparePass,		int, int, float						)
NVKITF_MTH3( FrameFx,	bool,			UnpreparePass,		int, int, float						)
NVKITF_MTH3( FrameFx,	bool,			ConvolvePass,		int, int, const Kernel33&			)
NVKITF_MTH2( FrameFx,	bool,			SepConvolvePass,	int, const Kernel33&				)
NVKITF_MTH3( FrameFx,	bool,			DiffConvolvePass,	int, int, const Kernel33&			)
NVKITF_MTH3( FrameFx,	bool,			GaussianBlurPass,	int, float, float					)
NVKITF_MTH4( FrameFx,	bool,			GlowBlurPass,		int, const Kernel33&, float, float	)
NVKITF_MTH4( FrameFx,	bool,			FocalBlurPass,		int, const Kernel33&, float, float	)
NVKITF_MTH2( FrameFx,	bool,			DepthToAlphaPass,	int, int							)
NVKITF_MTH2( FrameFx,	bool,			RGBAToDepthPass,	int, int							)
NVKITF_MTH2( FrameFx,	bool,			TrxPass,			int, uint128*						)
NVKITF_MTH1( FrameFx,	bool,			AllocClut,			int									)
NVKITF_MTH3( FrameFx,	bool,			SetClutColor,		int, uint8, uint32					)
NVKITF_MTH2( FrameFx,	bool,			GenerateHStripes,	int, int							)
NVKITF_MTH2( FrameFx,	bool,			GenerateVStripes,	int, int							)




