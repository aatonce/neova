/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/PS2/NvDpyObject[PS2].h>
#include <NvFrameClear.h>
using namespace nv;


//
// BASE

interface NvFrameClearBase : public NvDpyObject_PS2
{
	NvFrameClear			itf;
	frame::Packet			pkt;
	gs::frame::ClearMod		mod;
	bool					enColor;
	bool					enDepth;
	uint32					color;
	Vec4					vp;
	uint8					raster;
	bool					changed;


	virtual	~NvFrameClearBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)


	void
	_Init	(		)
	{
		InitDpyObject();

		enColor		= TRUE;
		enDepth		= TRUE;
		color		= 0;
		raster		= DPYR_NULL;
		changed		= TRUE;
		Vec4Zero( &vp );

		pkt.Init( this );
		pkt.SetSeparator( TRUE );		// always separator !
		pkt.SetNeedUpdate( FALSE );

		dmac::Cursor * DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );

		pkt.tadr = DC->i32;
		gs::frame::Clear_CH1( DC, mod	);
		pkt.link = DC->i32;
		*(DC->i128++) = DMA_TAG_END;
		NV_ASSERT_DC( DC );

		// relocation
		pvoid DCDup = DC->Dup( pkt.tadr );
		DC->i32 = pkt.tadr;
		uint32 dp = uint32(DCDup) - uint32(pkt.tadr);
		pkt.Translate( dp );
		mod.Translate( dp );
	}


	void
	Release	(		)
	{
		if( ShutDpyObject() ) {
			NV_ASSERT( !pkt.IsDrawing() );
			SafeFree( pkt.tadr );
			NvEngineDelete( this );
		}
	}


	void
	EnableDepth	(	bool	inOnOff		)
	{
		if( enDepth != inOnOff ) {
			enDepth = inOnOff;
			changed = TRUE;
		}
	}


	void
	EnableColor	(	bool	inOnOff		)
	{
		if( enColor != inOnOff ) {
			enColor = inOnOff;
			changed = TRUE;
		}
	}


	void
	SetColor	(	uint32		inRGBA		)
	{
		uint32 _c = InvertByteOrder( inRGBA );
		if( _c != color ) {
			color = _c;
			changed = TRUE;
		}
	}


	bool
	Draw	(		)
	{
		NV_ASSERT( refCpt );

		NV_ASSERT( dpyctxt.chainHead >= 0 );

		NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data()	+ dpyctxt.chainHead;
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()		+ dchain->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()			+ dctxt->viewIdx;

		if( changed || dctxt->raster!=raster || vp!=dview->viewport )
		{
			vp     = dview->viewport;
			raster = dctxt->raster;
			DpyManager::Raster& dpyraster = DpyManager::raster[ raster ];

			// valid region ?
			#ifdef _NVCOMP_ENABLE_DBG
			float sx = float(int(dpyraster.width));
			float sy = float(int(dpyraster.height));
			NV_ASSERTC( vp.x >= 0.f, "Invalid viewport !\n" );
			NV_ASSERTC( vp.x <  sx,  "Invalid viewport !\n" );
			NV_ASSERTC( vp.y >= 0.f, "Invalid viewport !\n" );
			NV_ASSERTC( vp.y <  sy,  "Invalid viewport !\n" );
			NV_ASSERTC( vp.z >= 0.f, "Invalid viewport !\n" );
			NV_ASSERTC( (vp.x+vp.z) <= sx, "Invalid viewport !\n" );
			NV_ASSERTC( vp.w >= 0.f, "Invalid viewport !\n" );
			NV_ASSERTC( (vp.y+vp.w) <= sy, "Invalid viewport !\n" );
			#endif

			*((uint64*)mod.gsFrame) = dpyraster.rgbaFrameReg;
			*((uint64*)mod.gsZBuf)  = dpyraster.depthZBufReg;

			mod.SetColor( color );
			mod.SetFBMSK( enColor ? 0 : ~0U );
			mod.SetZMSK( enDepth ? 0 : 1 );
			mod.SetRect( int(vp.x), int(vp.y), int(vp.z), int(vp.w) );
		}

		frame::Push( &pkt );
		return TRUE;
	}
};





NvFrameClear *
NvFrameClear::Create	(		)
{
	NvFrameClearBase * inst = NvEngineNewA( NvFrameClearBase,16);
	if( !inst )
		return NULL;
	inst->_Init();
	return &inst->itf;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameClear,	NvInterface*,	GetBase								)
NVITF_CAL0( FrameClear,	void,			AddRef								)
NVITF_MTH0( FrameClear,	uint,			GetRefCpt							)
NVITF_CAL0( FrameClear,	void,			Release								)
NVITF_CAL1( FrameClear,	void,			EnableColor,		bool			)
NVITF_CAL1( FrameClear,	void,			EnableDepth,		bool			)
NVITF_CAL1( FrameClear,	void,			SetColor,			uint32			)



