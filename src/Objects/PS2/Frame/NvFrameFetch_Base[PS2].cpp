/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/NvDpyObject[PS2].h>
#include <Kernel/Common/NvkCore_Mem.h>
#include <NvFrameFetch.h>
using namespace nv;



interface NvFrameFetchBase : public NvDpyObject_PS2
{
	NvFrameFetch		itf;
	
	virtual	~NvFrameFetchBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)

	
	NvFrameFetch*
	Init	(	uint inW ,uint inH	)
	{
		InitDpyObject();
		return &itf;
	}

	void
	Release	(		)
	{
		if( ShutDpyObject() )
		{
			NvEngineDelete( this );
		}
	}

	NvBitmap  *	
	GetFrameData	( )
	{
		return NULL;
	}

	bool
	Draw	(		)
	{
		return false;
	}
};





NvFrameFetch *
NvFrameFetch::Create	( uint inW, uint inH)
{
	return NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameFetch,	NvInterface*,	GetBase						)
NVITF_CAL0( FrameFetch,	void,			AddRef						)
NVITF_MTH0( FrameFetch,	uint,			GetRefCpt					)
NVITF_CAL0( FrameFetch,	void,			Release						)
NVITF_MTH0( FrameFetch,	NvBitmap  *,	GetFrameData				)

