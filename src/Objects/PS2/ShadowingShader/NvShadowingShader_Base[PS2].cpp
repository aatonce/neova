/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PS2/NvDpyObject[PS2].h>
#include <Kernel/PS2/NvkMesh[PS2].h>
#include <NvShadowingShader.h>
using namespace nv;




namespace
{

	int HClipQuad	(	int		cpt,	Vec4*	in_cl	)
	{
		uint andCF = 0x3F;
		uint orCF  = 0;
		for( int i = 0 ; i < cpt ; i++ ) {
			uint cf = in_cl[i].Clip();
			andCF &= cf;
			orCF  |= cf;
		}
		if( andCF!=0 )		return 0;			// all outside !
		if( orCF==0  )		return cpt;			// all inside !

		Vec4 *v0, *v1, *cP, *eP;

		for( int p = 0 ; p < 6 ; p++ )
		{
			v0 = in_cl;
			v1 = in_cl+1;
			cP = in_cl;
			eP = in_cl + cpt;
			*eP = *v0;
			while( v0 != eP ) {
				float wec0, wec1;
				if( p == 0 ) {
					wec0 = v0->w + v0->z;		// -z cut
					wec1 = v1->w + v1->z;
				} else if( p == 1 ) {
					wec0 = v0->w - v0->z;		// +z cut
					wec1 = v1->w - v1->z;
				} else if( p == 2 ) {
					wec0 = v0->w + v0->y;
					wec1 = v1->w + v1->y;
				} else if( p == 3 ) {
					wec0 = v0->w - v0->y;
					wec1 = v1->w - v1->y;
				} else if( p == 4 ) {
					wec0 = v0->w + v0->x;
					wec1 = v1->w + v1->x;
				} else {
					wec0 = v0->w - v0->x;
					wec1 = v1->w - v1->x;
				}

				#ifdef _NVCOMP_ENABLE_DBG
				uint cf0 = (v0->Clip()>>(5-p))&1;
				uint cf1 = (v1->Clip()>>(5-p))&1;
				// apr�s le -z cut => w > 0
				NV_ASSERT_IF( p>0, v0->w > 0.f );
				#endif

				if( wec0*wec1 < 0 ) {
					NV_ASSERT( cf0 != cf1 );
					float t = wec0 / (wec0 - wec1);
					NV_ASSERT( t>=0 && t<=1 );
					Vec4 inter;
					Vec4Lerp( &inter, v0, v1, t );
					*cP++ = inter;
				}
				if( wec1 >= 0 ) {
					NV_ASSERT( cf1 == 0 );
					Vec4Copy( cP++, v1 );
				}

				v0 = v1++;
			}
			cpt = cP - in_cl;
			if( cpt < 3 )	break;
		}

		return cpt;
	}



	bool	OutputFan	(	dmac::Cursor*	DC,
							int				nb,
							Vec4*			cl,
							uint32			inFrontABGR,
							uint32			inBackABGR,
							Vec4*			inProjScale,
							Vec4*			inProjTrans		)
	{
		nb = HClipQuad( nb, cl );
		if( nb < 3 )
			return FALSE;
		NV_ASSERT( nb < 16 );
		if( nb >= 16 )
			return FALSE;

		// to screen
		for( int i = 0 ; i < nb ; i++ )
			cl[i] *= 1.f / cl[i].w;

		// front face ?
		Vec2 r0( cl[2].x-cl[1].x, cl[2].y-cl[1].y );
		Vec2 r1( cl[0].x-cl[1].x, cl[0].y-cl[1].y );
		bool front = (r0 ^ r1) < 0;
		gs::Set_RGBAQ( DC, front ? inFrontABGR : inBackABGR );

		// reset FAN output
		gs::Set_PRIM( DC, 5, 0, 0, 0, 1, 0, 0, 1, 0 );		// FAN/ABE/CTXT2
		for( int i = 0 ; i < nb ; i++ ) {
			Vec4Mul( cl+i, cl+i, inProjScale );
			Vec4Add( cl+i, cl+i, inProjTrans );
			uint32 sz = (AS_UINT32(cl[i].z) & 0x0FFFFFF0 ) >> 4;	// packed as XYZF2 !
			gs::Set_XYZ2( DC, AS_UINT16(cl[i].x), AS_UINT16(cl[i].y), sz );
		}

		return TRUE;
	}

}




//
// BASE


struct NvShadowingShaderBase : public NvDpyObject_PS2
{
	enum {
		UPD_PROCESS		= (1<<0),
	};

	NvShadowingShader		itf;
	uint16					updflags;					// to update flags (UPD_...)
	NvkMesh*				mesh;
	frame::Packet			pkt;

	NvShadowingShader::Type	type;
	Vec3					source;
	float					offsetLength;
	float					extrudeLength;
	uint32					shadowSubABGR;
	uint					doflags;


	virtual	~	NvShadowingShaderBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

	void
	_Init		(	NvkMesh*	inKMesh	)
	{
		InitDpyObject();

		mesh = inKMesh;
		mesh->AddRef();

		pkt.Init( this );
		pkt.SetNeedUpdate( TRUE );
		pkt.SetSeparator( TRUE );			// disable sorting !
		pkt.SetPATH3Compliant( FALSE );		// never PATH3 compliant !

		type			= NvShadowingShader::T_DIRECTIONAL;
		source			= Vec3( 0, 0, -1 );
		offsetLength	= 0;
		extrudeLength	= 0;
		shadowSubABGR	= 0x40404040;
		doflags			= NvShadowingShader::F_DEFAULT;
		updflags		= 0;
	}

	void
	Release			(		)
	{
		if( ShutDpyObject() ) {
			NV_ASSERT( !pkt.IsDrawing() );
			SafeRelease( mesh );
			NvEngineDelete( this );
		}
	}

	void
	Enable		(	uint32		inEnableFlags	)
	{
		//
	}

	void
	Disable		(	uint32		inEnableFlags	)
	{
		//
	}

	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
		//
	}

	uint32
	GetEnabled		(		)
	{
		return 0;
	}

	NvResource*
	GetResource		(		)
	{
		return mesh->GetInterface();
	}

	bool
	Setup			(	NvShadowingShader::Type		inType,
						const Vec3&					inSource,
						float						inOffsetLength,
						float						inExtrudeLength		)
	{
		type			= inType;
		source			= inSource;
		offsetLength	= inOffsetLength;
		extrudeLength	= inExtrudeLength;
		if( type == NvShadowingShader::T_DIRECTIONAL )
			Vec3Normalize( &source, &source );
		updflags		= UPD_PROCESS;
		return TRUE;
	}

	void
	SetColor		(	uint32		inRGBA	)
	{
		shadowSubABGR	= ConvertRGBA( PSM_ABGR32, inRGBA );
		updflags		= UPD_PROCESS;
	}

	void
	SetFlags		(	uint		inFlags	)
	{
		doflags		= inFlags;
		updflags	= UPD_PROCESS;
	}

	uint
	GetFlags		(				)
	{
		return doflags;
	}

	void
	BuildDMA		(	)
	{
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		// Init
		NV_ASSERT_DC( DC );
		pkt.tadr = DC->i32;

		// start debug -------
//		doflags |= NvShadowingShader::F_DBG_SHOW_VOLUMES;
//		extrudeLength = 10.f;
//		offsetLength = 5.f;
		// end debug --------

		uint					vtxCpt;
		uint					faceCpt;
		NvkMesh::ShadowingVtx*	vtxA;
		NvkMesh::ShadowingFace*	faceA;
		mesh->GetShadowing(	vtxCpt, faceCpt, vtxA, faceA );
		NV_ASSERT( vtxCpt && faceCpt );
		NV_ASSERT( vtxA && faceA );

		// ctxt
		NV_ASSERT( dpyctxt.chainHead >= 0 );
		NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + dpyctxt.chainHead;
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
		Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;

		// stencil buffer 32BPP
		uint	stencilSizeX = gs::frame::SizeX;
		uint	stencilSizeY = gs::frame::SizeY;
		uint	stencilBAddr;
		uint	ZMSK;
		uint32	frontABGR, backABGR;


		//
		// pre-processing

		gs::tools::Gs_Open_CH1( DC );
		{
			// debug show volumes ?
			if( doflags & NvShadowingShader::F_DBG_SHOW_VOLUMES )
			{
				stencilBAddr = gs::frame::BackBAddr;
				ZMSK	  = 0;									// enable zwriting
				frontABGR = 0x400000FF;							// front is red
				backABGR  = 0x8000FF00;							// back is green
				gs::Set_Clamp_COLCLAMP( DC );					// RGB clamp
				gs::Set_ALPHA_2( DC, 2, 2, 2, 0, 0x80 );		// dummy
			}
			else
			{
				stencilBAddr = tram::GetPStart() << 5;
				ZMSK	  = 1;									// disable zwriting
				frontABGR = 0x40000001;							// R++
				backABGR  = 0x800000FF;							// R--
				gs::Set_Wrap_COLCLAMP( DC );					// RGB wrap
				gs::Set_ALPHA_2( DC, 0, 2, 2, 1, 0x80 );		// Add
	
				// clear stencil ?
				if( doflags & NvShadowingShader::F_CLEAR_STENCIL ) {
					gs::Set_RGBAQ( DC, 0 );
					gs::tools::Gs_ApplyPass_CH1( DC, stencilSizeX, stencilSizeY, stencilBAddr );
				}
			}

			gs::SetFrame_2( DC, stencilBAddr>>5, stencilSizeX, SCE_GS_PSMCT32, 0 );
			gs::Set_XYOFFSET_2( DC, ((2048-stencilSizeX/2)<<4), ((2048-stencilSizeY/2)<<4) );
			gs::Set_SCISSOR_2( DC, 0, stencilSizeX-1, 0, stencilSizeY-1 );
			gs::Set_ZBUF_2( DC, gs::frame::DepthBAddr>>5, 0, ZMSK	 );		// zwriting on/off
			gs::Set_TEST_2( DC, 0, 0, 0, 0, 0, 0, 1, SCE_GS_ZGREATER );		// enable ztesting
		}


		//
		// volume-processing

		if(		extrudeLength>0
			&&	(doflags & (NvShadowingShader::F_CAP_SIDES|NvShadowingShader::F_CAP_FRONT|NvShadowingShader::F_CAP_BACK)) )
		{
			Vec4 projScale, projTrans;
			DpyManager::raster[ dctxt->raster ].GetViewport( 1, dview->viewport, projScale, projTrans );

			ALIGNED128( Matrix,		wtr		);
			ALIGNED128( Matrix,		iwtr	);
			ALIGNED128( Matrix,		projM	);
			MatrixNormalize( &wtr, dwtr );
			MatrixFastInverse( &iwtr, &wtr );
			projM = wtr * dview->viewTR * dview->projTR * DpyManager::clipMatrix;

			ALIGNED128( Vec3, source3 );
			ALIGNED128( Vec4, source4 );
			if( type == NvShadowingShader::T_DIRECTIONAL ) {
				// dir light in objet world
				Vec3ApplyVector( &source3, &source, &iwtr );
				source4 = Vec4( source3.x, source3.y, source3.z, 0 );
			} else {
				// point light in objet world
				Vec3Apply( &source3, &source, &iwtr );
				source4 = Vec4( source3 );
			}

			// Sync SPR before using  !
			dmac::SyncFromSPR();
			Vec4* cl = (Vec4*) SPR_START;
			Vec4* ol = cl + 16;
			Vec4* el = ol + 3;
			char* faceVisA = (char*)( el + 3 );
			NV_ASSERTC( uint(faceVisA+faceCpt) < SPR_END, "Mesh has too many faces and can't be used has a shadow caster !" );

			// face visibility
			for( uint i = 0 ; i < faceCpt ; i++ ) {
				Vec3& l0 = vtxA[ faceA[i].vtx[0] ].location;
				Vec3& l1 = vtxA[ faceA[i].vtx[1] ].location;
				Vec3& l2 = vtxA[ faceA[i].vtx[2] ].location;
				Vec3   n = (l0-l1) ^ (l2-l1);
				Vec4  n4( n.x, n.y, n.z, -n*l0 );
				faceVisA[i] = (n4*source4)>0.f ? 1 : 0;
			}

			// capping
			int edgeNoSkip = doflags&(NvShadowingShader::F_CAP_FRONT|NvShadowingShader::F_CAP_BACK) ? 1 : 0;
			for( uint i = 0 ; i < faceCpt ; i++ )
			{
				if( faceVisA[i] )	continue;
				NV_ASSERT( faceA[i].adj[0]>=0 && faceA[i].adj[0]<int(faceCpt) );
				NV_ASSERT( faceA[i].adj[1]>=0 && faceA[i].adj[1]<int(faceCpt) );
				NV_ASSERT( faceA[i].adj[2]>=0 && faceA[i].adj[2]<int(faceCpt) );

				// check if edges cast shadow ?
				int edgeCast = 0;
				edgeCast |= faceVisA[ faceA[i].adj[0] ] << 0;
				edgeCast |= faceVisA[ faceA[i].adj[1] ] << 1;
				edgeCast |= faceVisA[ faceA[i].adj[2] ] << 2;
				if( (edgeCast|edgeNoSkip) == 0 ) 	continue;

				// extrude vertices
				if( type == NvShadowingShader::T_DIRECTIONAL )
				{
					ALIGNED128( Vec3, l );
					for( int k = 0 ; k < 3 ; k++ ) {
						l = vtxA[ faceA[i].vtx[k] ].location;
						ol[k] = Vec4( l + source3 * offsetLength  );
						el[k] = Vec4( l + source3 * extrudeLength );
						// to projective space
						Vec4Apply( ol+k, ol+k, &projM );
						Vec4Apply( el+k, el+k, &projM );
					}
				}
				else
				{
					ALIGNED128( Vec3, l );
					ALIGNED128( Vec3, d );
					for( int k = 0 ; k < 3 ; k++ ) {
						l = vtxA[ faceA[i].vtx[k] ].location;
						d = l - source3;
						Vec3Normalize( &d, &d );
						ol[k] = Vec4( l + d * offsetLength  );
						el[k] = Vec4( l + d * extrudeLength );
						// to projective space
						Vec4Apply( ol+k, ol+k, &projM );
						Vec4Apply( el+k, el+k, &projM );
					}
				}

				// front cap
				if( doflags & NvShadowingShader::F_CAP_FRONT ) {
					cl[0] = ol[0]; cl[1] = ol[1]; cl[2] = ol[2];
					OutputFan( DC, 3, cl, backABGR, frontABGR, &projScale, &projTrans );
				}

				// back cap
				if( doflags & NvShadowingShader::F_CAP_BACK ) {
					cl[0] = el[0]; cl[1] = el[1]; cl[2] = el[2];
					OutputFan( DC, 3, cl, frontABGR, backABGR, &projScale, &projTrans );
				}

				// side caps
				if( edgeCast && (doflags & NvShadowingShader::F_CAP_SIDES) ) {
					if( edgeCast & 1 ) {
						cl[0] = ol[0]; cl[1] = ol[1]; cl[2] = el[1]; cl[3] = el[0];
						OutputFan( DC, 4, cl, frontABGR, backABGR, &projScale, &projTrans );
					}
					if( edgeCast & 2 ) {
						cl[0] = ol[1]; cl[1] = ol[2]; cl[2] = el[2]; cl[3] = el[1];
						OutputFan( DC, 4, cl, frontABGR, backABGR, &projScale, &projTrans );
					}
					if( edgeCast & 4 ) {
						cl[0] = ol[2]; cl[1] = ol[0]; cl[2] = el[0]; cl[3] = el[2];
						OutputFan( DC, 4, cl, frontABGR, backABGR, &projScale, &projTrans );
					}
				}
			}
		}


		//
		// post-processing

		if( !(doflags & NvShadowingShader::F_DBG_SHOW_VOLUMES) )
		{
			if(		doflags & NvShadowingShader::F_BACK_STENCIL
				||	doflags & NvShadowingShader::F_DRAW_SHADOWS	)
			{
				gs::Set_ZBUF_2( DC, 0, 0, 1 );											// disable zwritting
				gs::Set_TEST_2( DC, 0, 0, 0, 0, 0, 0, 1, SCE_GS_ZALWAYS );				// disable ztesting

				// Copy stencil -> back alpha
				// stencil is RGB24 with TEXA.AEM=1
				// => A=0        if R==0
				// => A=TA0=0x80 if R!=0
				gs::Write_TEXFLUSH( DC );
				gs::Set_TEXA( DC, 0x80, 1, 0x80	);			// AEM=1 !
				gs::Set_CLAMP_2( DC, 2, 2, 0, stencilSizeX-1, 0, stencilSizeY-1 );										// Region clamping for no 2^x source !
				gs::SetTexture_2( DC, stencilBAddr, stencilSizeX, stencilSizeY, SCE_GS_PSMCT24,	SCE_GS_DECAL, 1 );		// linear for median filtering !

				gs::SetFrame_2( DC, gs::frame::BackBAddr>>5, gs::frame::SizeX, SCE_GS_PSMCT32, 0x00FFFFFF );
				gs::Set_SCISSOR_2( DC, 0, gs::frame::SizeX-1, 0, gs::frame::SizeY-1 );
				gs::Set_XYOFFSET_2( DC, 0, 0 );
				gs::Set_Clamp_COLCLAMP( DC );

				gs::Set_PRIM( DC, 6, 0, 1, 0, 0, 0, 1, 1, 0 );							// TME/UV/CTXT2
				Vec4 fromReg( 0, 0, stencilSizeX, stencilSizeY );
				Vec4 toReg( 0, 0, gs::frame::SizeX, gs::frame::SizeY );
				gs::tools::Gs_DrawSpritePass_CH1( DC, &toReg, &fromReg );

				// Median filter to remove most GS pixel errors
				// A cheaper median filtering is obtained by :
				// a) using linear filter when copty stencil->back alpha (alpha are 0x80 or less)
				// b) using alpha destination test to apply shadow on pixels with alpha = 0x80 only !
				//
				// Other method could be to reduce the clamped back-alpha channel to average the near alpha values
				// and then use a destination alpha test when applying the shadow ... but slower ...
			}

			if( doflags & NvShadowingShader::F_DRAW_SHADOWS )
			{
				// Shadowing using stored stencil in back-frame alpha channel
				// An alpha destination test is used to apply the shadow only on alpha=0x80 pixels as a median filter.
				gs::Set_TEST_2( DC, 0, 0, 0, 0, 1, 1, 1, SCE_GS_ZALWAYS );
				gs::Set_RGBAQ( DC, shadowSubABGR );
				gs::Set_ALPHA_2( DC, 2, 0, 1, 1, 0x80 );								// Sub with alpha dest
				gs::tools::Gs_ApplyPass_CH1(	DC,
												gs::frame::SizeX,
												gs::frame::SizeY,
												gs::frame::BackBAddr,
												gs::tools::PF_ABE,
												0xFF000000	);		// keep alpha
			}
		}

		gs::tools::Gs_Close_CH1( DC );


		// Link
		NV_ASSERT_DC( DC );
		pkt.link = DC->i32;
		*(DC->i128++) = DMA_TAG_END;		// !! reserved for the frame pkt manager !!
	}

	void
	UpdatePacket	(	frame::Packet*		inPkt	)
	{
		// mesh have been previously updated by drawing shader ...
		// safely update it again !
		mesh->UpdateBeforeDraw();
		BuildDMA();
		updflags = 0;
	}

	bool
	Draw			(	)
	{
		frame::Push( &pkt );
		return TRUE;
	}
};





NvShadowingShader*
NvShadowingShader::Create	(	NvResource*		inRsc		)
{
	if( !inRsc || inRsc->GetRscType() != NvMesh::TYPE )
		return NULL;

	NvkMesh* kmesh = ((NvMesh*)inRsc)->GetKInterface();
	if( !kmesh || !kmesh->IsShadowing() )
		return NULL;

	// check enough tram for stencil half-buffer
	// stencil is PSMCT24 for RGB->alpha clamping using TEXA.AEM=1 test !
	uint stencilPSize = gs::tools::GetPageSize( SCE_GS_PSMCT24, gs::frame::SizeX, gs::frame::SizeY );
	if( stencilPSize > tram::GetPSize() ) {
		DebugPrintf( "NvShadowingShader:: Failed to create a shader, not enought VRAM !\n" );
		return NULL;
	}

	NvShadowingShaderBase* shader = NULL;
	uint supply = 0;
	shader = NvEngineNewAS( NvShadowingShaderBase,16, supply );
	if( !shader )
		return NULL;

	shader->_Init( kmesh );

	return &shader->itf;
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( ShadowingShader,	NvInterface*,	GetBase												)
NVITF_CAL0( ShadowingShader,	void,			AddRef												)
NVITF_MTH0( ShadowingShader,	uint,			GetRefCpt											)
NVITF_CAL0( ShadowingShader,	void,			Release												)
NVITF_MTH0( ShadowingShader,	NvResource*,	GetResource											)
NVITF_CAL1( ShadowingShader,	void,			Enable,				uint32							)
NVITF_CAL1( ShadowingShader,	void,			Disable,			uint32							)
NVITF_CAL1( ShadowingShader,	void,			SetEnabled,			uint32							)
NVITF_MTH0( ShadowingShader,	uint32,			GetEnabled											)
NVITF_MTH4( ShadowingShader,	bool,			Setup,				Type, const Vec3&, float, float	)
NVITF_MTH1( ShadowingShader,	void,			SetColor,			uint32							)
NVITF_CAL1( ShadowingShader,	void,			SetFlags,			uint							)
NVITF_MTH0( ShadowingShader,	uint,			GetFlags											)



