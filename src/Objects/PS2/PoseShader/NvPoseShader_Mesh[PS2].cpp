/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "NvPoseShader_Base[PS2].h"
#include <Kernel/PS2/NvkBitmap[PS2].h>
#include <Kernel/PS2/NvkMesh[PS2].h>
#include <Kernel/PS2/NvDpyState[PS2].h>
#include <Kernel/PS2/VU/vu1_Def.h>
using namespace nv;




namespace
{

	struct Shader : public NvPoseShaderBase
	{
		uint32					enflags;		// enable flags (EN_...)
		NvkMesh*				mesh;
		NvPoseShader::Mode		mode;
		float					fadingInnerD;
		float					fadingOuterD;
		uint					components;
		uint					listSize;
		uint					drawSize;
		pvoid					dmaList;
		pvoid					accessList;
		bool					accessValidated;
		NvkMesh::BunchData*		bunchData;
		uint32					bunchDataBase;
		float					xyzUnpack;
		bool					hidden;
		frame::Packet			pkt;
		ALIGNED128( DpyState_PS2, dpystate );


		virtual	~	Shader	(	) {}			// needed by nv::DestructInPlace<T>(ptr)

		uint
		_CompoBSize		(	uint		inComponent		)
		{
			if( inComponent == (uint)NvPoseShader::C_LOCATION 		)		return sizeof(Vec3);
			if( inComponent == (uint)NvPoseShader::C_ROTATION 		)		return sizeof(Quat);
			if( inComponent == (uint)NvPoseShader::C_SCALE			)		return sizeof(Vec3);
			if( inComponent == (uint)NvPoseShader::C_RGBA32			)		return sizeof(uint32);
		/*	if( inComponent == (uint)NvPoseShader::C_DISPLACEMENT	)*/		return sizeof(Vec3);
		}

		uint
		_CompoListBSize	(	uint		inComponents	)
		{
			inComponents &= 0x1F;
			uint bsize    = 0;
			uint compo    = 1;
			while( inComponents ) {
				if( inComponents&1 )
					bsize += Round16( listSize * _CompoBSize(compo) );
				compo        <<= 1;
				inComponents >>= 1;
			}
			return bsize;
		}

		uint
		_CompoBOffset	(	uint		inComponent		)
		{
			uint previous_components = (inComponent - 1) & components;
			return _CompoListBSize( previous_components );
		}

		pvoid
		_CompoAccessPtr	(	uint		inComponent		)
		{
			if( !accessList || !(inComponent&components) )
				return NULL;
			return (pvoid)( uint(accessList) + _CompoBOffset(inComponent) );
		}

		pvoid
		_CompoDmaPtr	(	uint		inComponent		)
		{
			if( !dmaList || !(inComponent&components) )
				return NULL;
			return (pvoid)( uint(dmaList) + _CompoBOffset(inComponent) );
		}


		void
		_Init		(	NvMesh*		inMesh,
						uint		inComponents,
						uint		inSize	)
		{
			InitDpyObject();

			mesh				= inMesh->GetKInterface();
			mesh->AddRef();
			enflags    			= NvShader::EN_DEFAULT;
			mode	   			= NvPoseShader::M_LOCKED;
			fadingInnerD		= 50.0f;
			fadingOuterD		= 100.0f;
			components			= inComponents & 0x1F;
			listSize			= inSize;
			drawSize			= 0;
			dmaList				= EngineMallocA( Round64(_CompoListBSize(components)), 64 );
			accessList			= NULL;
			accessValidated		= FALSE;
			bunchData			= mesh->GetBunchDataA();
			bunchDataBase		= mesh->GetBunchDataBase();
			xyzUnpack			= vu1::GetXYZUnpackF( mesh->GetPackIPartLen() );

			NV_ASSERT( mesh->GetSurfaceCpt() == 1 );
			NV_ASSERT( mesh->GetBunchCpt() == 1 );
			NV_ASSERT( mesh->GetBunchDataCpt() == 1 );
			NvkMesh::Shading* shading = mesh->GetSurfaceShadingA();

			pkt.Init( this );
			pkt.SetNeedUpdate( TRUE );

			hidden = FALSE;

			// capabilities
			uint dpycaps = 0;
			if( shading->hasUV )		dpycaps |= DpyState_PS2::CF_HAS_TEX0;
			if( shading->hasColor )		dpycaps |= DpyState_PS2::CF_HAS_RGBA;

			// Init dpystate
			dpystate.Init();
			dpystate.SetCapabilities( dpycaps );
			dpystate.SetEnabledMask( enflags );
			dpystate.SetEnabled( shading->stateEnFlags );
			dpystate.SetMode( shading->stateMdFlags );
			dpystate.SetAlphaPass( shading->stateAlphaPass );
			dpystate.SetBlendSrcCte( shading->stateBlendCte );
			dpystate.SetTexturing( shading->bitmapUID, shading->mipL, shading->mipK, shading->mappingRatio );
			pkt.Append( &dpystate.texCtxt );
		}

		void
		Release	(		)
		{
			if( ShutDpyObject() ) {
				NV_ASSERT( !pkt.IsDrawing() );
				dpystate.Shut();
				SafeRelease( mesh );
				SafeFree( dmaList );
				SafeFree( accessList );
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return mesh->GetInterface();
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				dpystate.SetEnabledMask( enflags );
			}
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}

		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex != 0 )
				return FALSE;
			hidden = TRUE;
			return TRUE;
		}

		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex != 0 )
				return FALSE;
			hidden = FALSE;
			return TRUE;
		}

		bool
		HideAllSurfaces		(					)
		{
			hidden = TRUE;
			return TRUE;
		}

		bool
		ShowAllSurfaces		(					)
		{
			hidden = FALSE;
			return TRUE;
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return (inIndex==0) ? &dpystate : NULL;
		}

		void
		SetMode		(	NvPoseShader::Mode		inMode	)
		{
			mode = inMode;
		}

		void
		SetFading	(	float		inInnerD,
						float		inOuterD	)
		{
			NV_ASSERT( inInnerD>0 && inInnerD<inOuterD );
			if( inInnerD>0 && inInnerD<inOuterD ) {
				fadingInnerD = inInnerD;
				fadingOuterD = inOuterD;
			}
		}

		void
		SetListDrawSize	(	uint	inSize	)
		{
			drawSize = Min( listSize, inSize );
		}

		uint
		GetPoseComponents	(				)
		{
			return components;
		}

		uint
		GetListSize			(				)
		{
			return listSize;
		}

		bool
		CreateAccess	(						)
		{
			if( !accessList ) {
				accessList = EngineMallocA( _CompoListBSize(components), 16 );
				accessValidated = FALSE;
			}
			return ( accessList != NULL );
		}

		pvoid
		GetAccess		(	NvPoseShader::Component		inComponent,
							uint&						outStride,
							Psm*						outPSM		)
		{
			outStride = _CompoBSize( inComponent );
			if( outPSM )	*outPSM = PSM_ABGR32;
			return _CompoAccessPtr( uint(inComponent) );
		}

		void
		ValidateAccess	(	uint			inComponents,
							uint			inSizeFrom0			)
		{
			accessValidated = TRUE;
		}

		void
		ReleaseAccess	(					)
		{
			if( accessList ) {
				NV_ASSERTC( !pkt.IsDrawing(), "ReleaseAccess of drawing frame yields undeterminated consequences !" );
				_UpdateBeforeDraw();
				SafeFree( accessList );
			}
		}

		void
		_UpdateBeforeDraw	(				)
		{
			if( accessList && accessValidated ) {
				NV_ASSERT( dmaList );
				Swap( dmaList, accessList );
				accessValidated = FALSE;
			}
		}

		void
		_BuildDMA	(		)
		{
			dmac::Cursor* DC = dmac::GetFrontHeapCursor();

		//	bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;
			vu1::ClipMode clipMode;
			if( enflags & NvShader::EN_THROUGHMODE )	clipMode = vu1::CM_NONE;	// 2D => no clip !
			else if( enflags & NvShader::EN_CLIPPING )	clipMode = vu1::CM_CLIPPING;
			else if( enflags & NvShader::EN_CULLING )	clipMode = vu1::CM_CULLING;
			else										clipMode = vu1::CM_NONE;

			// Build all vu1-contexts in cursor
			vu1::poser::Context* vu1ctxt0 = vu1::poser::BuildContextList( DC, dpyctxt.chainHead, mode, fadingInnerD, fadingOuterD, xyzUnpack );

			// Build poselist uploader (shared by all draws)
			uint32 poselist_addr;
			vu1::poser::BuildPoseList_CH1(	poselist_addr, DC, 0, drawSize,
											(Vec3*)   _CompoDmaPtr(NvPoseShader::C_LOCATION),
											(Quat*)   _CompoDmaPtr(NvPoseShader::C_ROTATION),
											(Vec3*)   _CompoDmaPtr(NvPoseShader::C_SCALE),
											(uint32*) _CompoDmaPtr(NvPoseShader::C_RGBA32),
											(Vec3*)   _CompoDmaPtr(NvPoseShader::C_DISPLACEMENT)	);

			// Init
			pkt.tadr = DC->i32;
			pkt.mcId = vu1::poser::Begin_CH1( DC, clipMode, vu1::poser::DT_PRELIT_MESH );

			// Unpack prelit mesh
			vu1::poser::SetData_CH1( DC, bunchData->dmaBOffset+bunchDataBase, bunchData->dmaQSize );

			vu1::poser::SetDirect_CH1( DC, pkt.texList->gsContext );
			for( int j = 0 ; j < dpyctxt.chainCpt ; j++ ) {
				vu1::poser::SetContext_CH1( DC, vu1ctxt0++ );
				vu1::poser::CallPoseList_CH1( DC, poselist_addr );
			}

			// Link
			pkt.link = DC->i32;
			vu1::poser::End_CH1( DC );

			pkt.SetSeparator( (enflags&NvShader::EN_SORTING) ? FALSE : TRUE );
		}

		void
		UpdatePacket		(	frame::Packet*	inPkt		)
		{
			mesh->UpdateBeforeDraw();
			dpystate.Activate();
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );

			if( hidden )
				return FALSE;

			if( !drawSize )
				return FALSE;

			_UpdateBeforeDraw();
			_BuildDMA();

			frame::Push( &pkt );

			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			NV_ASSERT( mesh->GetBunchCpt() == 1 );
			NvkMesh::Bunch* bunch = mesh->GetBunchA();
			outTriCpt	= bunch->faceCpt * dpyctxt.chainCpt * drawSize;
			outLocCpt	= bunch->locCpt  * dpyctxt.chainCpt * drawSize;
			outVtxCpt	= bunch->vtxCpt	 * dpyctxt.chainCpt * drawSize;
			outPrimCpt	= bunch->primCpt * dpyctxt.chainCpt * drawSize;
			return TRUE;
		}
	};


}




NvPoseShader*
nv_poseshader_mesh_Create	(	NvMesh*		inMesh,
								uint		inComponents,
								uint		inSize	)
{
	if( !inMesh || !inSize || !inComponents || !(inComponents&NvPoseShader::C_LOCATION) )
		return NULL;

	NvkMesh* kmesh = inMesh->GetKInterface();
	if( kmesh->IsSkinnable() ) {
		DebugPrintf( "NvPoseShader: A skinnable nvmesh can't be posed !\n" );
		return NULL;
	}
	if( kmesh->IsMorphable() ) {
		DebugPrintf( "NvPoseShader: A morphable nvmesh can't be posed !\n" );
		return NULL;
	}
	if( kmesh->IsLightable() ) {
		DebugPrintf( "NvPoseShader: A lightable nvmesh can't be posed !\n" );
		return NULL;
	}
	if( kmesh->GetSurfaceCpt() != 1 ) {
		DebugPrintf( "NvPoseShader: A multi-surface nvmesh can't be posed !\n" );
		return NULL;
	}
	if( kmesh->GetBunchCpt() != 1 ) {
		DebugPrintf( "NvPoseShader: A multi-bunch nvmesh can't be posed !\n" );
		return NULL;
	}

	NV_ASSERT( kmesh->GetBunchDataCpt() == 1 );
	NvkMesh::BunchData* bunchData = kmesh->GetBunchDataA();
	if( bunchData->packetCpt > 1 ) {
		DebugPrintf( "NvPoseShader: A multi-packet nvmesh can't be posed !\n" );
		return NULL;
	}
	if( bunchData->unpackQSize > vu1::poser::GetDataMaxQS() ) {
		DebugPrintf( "NvPoseShader: nvmesh unpack-qsize overflows (%dpc) the pose D$ !\n", int(bunchData->unpackQSize*100/vu1::poser::GetDataMaxQS()) );
		return NULL;
	}

	Shader* shader = NvEngineNewA( Shader,16 );
	if( !shader )
		return NULL;

	shader->_Init( inMesh, inComponents, inSize );
	return shader->GetInterface();
}




