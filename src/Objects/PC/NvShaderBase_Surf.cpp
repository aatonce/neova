/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkBitmap[PC].h>
#include <Kernel/PC/NvkSurface[PC].h>
#include <NvShader.h>

#include "NvShaderBase_Surf.h"

uint
NvShaderBase_Surf::Init(	NvSurface*	inSurf	)
{
	NV_ASSERT(inSurf);

	enflags = (uint) NvShader::EN_DEFAULT;

	hidden = FALSE;

	surf = inSurf->GetKInterface();
	surf->AddRef();

	dpystate.Init();
	dpystate.SetEnabledMask( enflags );

	drawStart = 0;
	drawSize  = 0;

	lightFilters.emissive	= Vec4(1.0f,1.0f,1.0f,1.0f);
	lightFilters.ambient	= Vec4(1.0f,1.0f,1.0f,1.0f);
	lightFilters.diffuse	= Vec4(1.0f,1.0f,1.0f,1.0f);
	lightFilters.specular	= Vec4(1.0f,1.0f,1.0f,1.0f);
	lightFilters.glossiness	= 0;

	uint32 dpycaps = 0;				 	
	if( surf->GetComponents() & NvSurface::CO_NORMAL ) {
		dpycaps |= DpyState_PC::CF_HAS_NORMAL;
	}
	if( surf->GetComponents() & NvSurface::CO_TEX ) {
		dpycaps |= DpyState_PC::CF_HAS_TEX0;					
	}
	if( surf->GetComponents() & NvSurface::CO_COLOR ) {
		dpycaps |= DpyState_PC::CF_HAS_RGBA;
	}
	dpystate.SetCapabilities( dpycaps );

	return dpycaps;
}

void
NvShaderBase_Surf::Shut	(		)
{
	dpystate.Shut();
	SafeRelease( surf );
}

NvResource*
NvShaderBase_Surf::GetResource		(					)
{
	return surf->GetInterface();
}

void
NvShaderBase_Surf::Enable		(	uint32		inEnableFlags	)
{
	SetEnabled( enflags|inEnableFlags );
}

void
NvShaderBase_Surf::Disable		(	uint32		inEnableFlags	)
{
	SetEnabled( enflags&(~inEnableFlags) );
}

void
NvShaderBase_Surf::SetEnabled		(	uint32		inEnableFlags	)
{
	enflags = inEnableFlags;
	dpystate.SetEnabledMask( enflags );
}

bool
NvShaderBase_Surf::SetDrawStart	(	uint inOffset		)
{
	drawStart = inOffset;
	return TRUE;
}

bool
NvShaderBase_Surf::SetDrawSize	(uint			inSize			)
{

	drawSize = inSize;
	return TRUE;
}

uint32
NvShaderBase_Surf::GetEnabled		(		)
{
	return enflags;
}

bool
NvShaderBase_Surf::HideSurface			(	uint			inSurfaceIndex	)
{
	if ( inSurfaceIndex!=0 ) return FALSE;
		hidden = TRUE;
	return TRUE;
}

bool
NvShaderBase_Surf::ShowSurface			(	uint			inSurfaceIndex	)
{
	if ( inSurfaceIndex!=0 ) return FALSE;
		hidden = FALSE;
	return TRUE;
}

bool
NvShaderBase_Surf::HideAllSurfaces		(									)
{
	hidden = TRUE;
	return TRUE;
}

bool
NvShaderBase_Surf::ShowAllSurfaces		(									)
{
	hidden = FALSE;
	return TRUE;
}

DpyState*
NvShaderBase_Surf::GetDisplayState	(	uint		inIndex		)
{
	return inIndex==0?&dpystate:NULL;			
}

bool
NvShaderBase_Surf::SetEmissive		(	uint			inIndex,
					Vec4*			inColor					)
{
	if( inColor )
		Vec4Copy( &lightFilters.emissive, inColor );
	return TRUE;
}

bool
NvShaderBase_Surf::SetAmbient		(	uint			inIndex,
					Vec4*			inColor					)
{
	if( inColor )
		Vec4Copy( &lightFilters.ambient, inColor );
	return TRUE;
}

bool
NvShaderBase_Surf::SetDiffuse		(	uint			inIndex,
					Vec4*			inColor					)
{
	if( inColor )
		Vec4Copy( &lightFilters.diffuse, inColor );
	return TRUE;
}

bool
NvShaderBase_Surf::SetSpecular		(	uint			inIndex,
					Vec4*			inColor,
					float*			inLevel,
					float*			inGlossiness			)
{
	if( inColor )
		Vec4Copy( &lightFilters.specular, inColor );
	if( inGlossiness )
		lightFilters.glossiness = *inGlossiness;
	return TRUE;
}

bool
NvShaderBase_Surf::Draw		(	NvDpyObject_PC::DpyCtxt	& inDpyctxt	)
{
	if( !surf )
		return FALSE;

	if (hidden) 
		return FALSE;

	surf->UpdateBeforeDraw();

	Hrdw::SetVertexDeclaration(surf->GetVSDecl());

	SetShader ();

	NV_ASSERT( inDpyctxt.chainHead >= 0 );
	int chainIdx = inDpyctxt.chainHead;

	while( chainIdx >= 0 )
	{
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data() + 
												(DpyManager::context->contextChainA.data() + chainIdx)->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()    + dctxt->viewIdx;

		SetSurfaceConstant	(	chainIdx );
		chainIdx = (DpyManager::context->contextChainA.data() + chainIdx)->next;

		// RenderTarget
		Hrdw::SetRaster( DpyManager::hwraster[dctxt->raster] );
		// VP (X,Y,Width,height,minZ,maxZ);
		Hrdw::SetViewPort(dview->viewport.x, dview->viewport.y, dview->viewport.z, dview->viewport.w, 0.0f, 1.0f);		

		//dpystate.Activate();
		
		// Render DX VB
		Hrdw::IndexBuffer ib = 0;

		Hrdw::SetVertexBuffer(surf->GetVtxBufferBase(NvkSurface::BF_BACK), 0 , 0 , surf->GetVertexStride());

		ib = surf->GetIdxBufferBase(NvkSurface::BF_BACK);

		// Using an index-buffer ?
		if (ib) {				
			Hrdw::SetIndexBuffer(ib);
			Hrdw::DrawPrimitive(Hrdw::NV_TRIANGLESTRIP,
								surf->GetDrawingSize	(NvkSurface::BF_BACK, drawStart , drawSize)-2,
								surf->GetDrawingOffset	(NvkSurface::BF_BACK, drawStart),
								surf->GetVertexSize		(NvkSurface::BF_BACK)							);
		}
		else {
			Hrdw::DrawPrimitive(Hrdw::NV_TRIANGLESTRIP,
								surf->GetDrawingSize	(NvkSurface::BF_BACK, drawStart , drawSize)-2,
								surf->GetDrawingOffset	(NvkSurface::BF_BACK, drawStart)				);
		}
	}

	return TRUE;
}
