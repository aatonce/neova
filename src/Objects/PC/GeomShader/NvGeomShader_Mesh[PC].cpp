/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkBitmap[PC].h>
#include <Kernel/PC/NvkMesh[PC].h>

#include "NvGeomShader_Base[PC].h"
#include "../NvShaderBase_Mesh.h"
using namespace nv;




namespace
{	
	struct HLSLVSMapping {
		Matrix 	vpm;			// Transformation Matrix View and Projection
		float   haveTexCoord;
		float	haveCol;
		float   haveNormal;
		float	pad3;
		Vec4	lCol[3];		// Light Color		
		Vec4 	lDir[2];		// Light Direction
		Vec4	oCol[2];		// Object Color		
	};
	
	struct HLSLPSMapping {
		float   haveNormal;
		float   haveTexCoord;
		float	haveCol;
		float	pad0;
	};

	Hrdw::ShaderProgram HLSLVS;
	Hrdw::ShaderProgram HLSLPS;

	char GeomShaderCodeVHL[] = {
		#include "GeomShader.vhl.bin2h"
	};

	char GeomShaderCodePHL[] = {
		#include "GeomShader.phl.bin2h"
	};

	void _InitVS_Decl()
	{
		static bool init = false;
		if (init) return;
		init = true;
		HLSLVS = Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, GeomShaderCodeVHL,sizeof(GeomShaderCodeVHL),Hrdw::NV_VS_2, "main","TO_LIT=1");
		HLSLPS = Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, GeomShaderCodePHL,sizeof(GeomShaderCodePHL),Hrdw::NV_PS_2, "main");

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,vpm,"vpm"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,lCol,"lCol"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,lDir,"lDir"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,oCol,"objColor"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,haveNormal,"TexColNrm_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,haveTexCoord,"TexColNrm_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,haveCol,"TexColNrm_Condition"));

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,haveNormal,"NrmTexCol_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,haveTexCoord,"NrmTexCol_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,haveCol,"NrmTexCol_Condition"));			
	} 

	//
	// BASE

	struct Shader : public NvGeomShaderBase , public NvShaderBase_Mesh
	{
		HLSLVSMapping			mapVSH;
		HLSLPSMapping			mapPSH;		
				
		virtual	~	Shader	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init	(	NvMesh*		inMesh	)
		{
			_InitVS_Decl();
			InitDpyObject();

			uint dpycaps = NvShaderBase_Mesh::Init(inMesh);

			mapVSH.haveNormal = 	mapVSH.haveTexCoord = 	 mapVSH.haveCol = 0.0f;
			mapPSH.haveNormal = 	mapPSH.haveTexCoord = 	 mapPSH.haveCol = 0.0f;

			if( dpycaps & DpyState_PC::CF_HAS_NORMAL ) 			
				mapPSH.haveNormal = mapVSH.haveNormal = 1.0f;
			if( dpycaps & DpyState_PC::CF_HAS_TEX0 )
				mapPSH.haveTexCoord = mapVSH.haveTexCoord = 1.0f;
			if( dpycaps & DpyState_PC::CF_HAS_RGBA )
				mapPSH.haveCol = mapVSH.haveCol = 1.0f;
		}

		void
		AddRef		(		)
		{
			refCpt++;
		}

		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}

		void
		Release	(		)
		{
			if( ShutDpyObject(GetInterface()) ) {
				NvShaderBase_Mesh::Shut();
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return NvShaderBase_Mesh::GetResource(  );			
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled( enflags|inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled( enflags&(~inEnableFlags) );
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled(inEnableFlags);
		}

		uint32
		GetEnabled		(		)
		{
			return NvShaderBase_Mesh::GetEnabled();
		}

		bool
		HideSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Mesh::HideSurface (inSurfaceIndex	);
		}

		bool
		ShowSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Mesh::ShowSurface (inSurfaceIndex	);
		}

		bool
		HideAllSurfaces		(									)
		{
			return NvShaderBase_Mesh::HideAllSurfaces();
		}

		bool
		ShowAllSurfaces		(									)
		{
			return NvShaderBase_Mesh::ShowAllSurfaces();
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return NvShaderBase_Mesh::GetDisplayState(inIndex);
		}

		bool
		SetAmbient		(	uint			inIndex,
							Vec4*			inColor					)
		{
			return NvShaderBase_Mesh::SetAmbient(inIndex,inColor);
		}

		bool
		SetDiffuse		(	uint			inIndex,
							Vec4*			inColor					)
		{
			return NvShaderBase_Mesh::SetDiffuse(inIndex,inColor);
		}

		bool
		Draw		(				)
		{
			return NvShaderBase_Mesh::Draw(dpyctxt);
		}

		void			
		SetShader			(		)
		{ 
			Hrdw::SetShaderProgram(HLSLVS);
			Hrdw::SetShaderProgram(HLSLPS);
			Hrdw::FullUpdatePixelMapping(&mapPSH);
			mapVSH.oCol[0] = Vec4(1,1,1,1);
			mapVSH.oCol[1] = Vec4(1,1,1,1);
			Hrdw::UpdateVertexMapping(&mapVSH,&(mapVSH.oCol));
		}

		void
		SetObjectConstant	(	int inChainIdx	)
		{
			if (inChainIdx < 0 ) return ;

			NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data() + 
												(DpyManager::context->contextChainA.data() + inChainIdx)->contextIdx;
			NvDpyContext::View*			dview  = DpyManager::context->viewA.data()    + dctxt->viewIdx;
			NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()   + dctxt->lightIdx;
			Matrix*						dwtr   = DpyManager::context->wtrA.data()     + dctxt->wtrIdx;			

			Matrix toProjTR;
			MatrixMul(&toProjTR,&dview->viewTR,&dview->projTR);
			MatrixMul(&toProjTR,&toProjTR,&DpyManager::clipMatrix);

//			MatrixIdentity (  dwtr );
			// TR
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;
			if (!mode2D) {
				MatrixMul( &mapVSH.vpm, dwtr, &toProjTR);
			}
			else {
				Matrix proj2D;
				MatrixZero(&proj2D);
				proj2D.m11 =	2.0f / dview->viewport.z ;
				proj2D.m22 =	-2.0f / dview->viewport.w ;
				proj2D.m33 =	1.0f;
				proj2D.m41 =	-1.0f;
				proj2D.m42 =	1.0f;
				proj2D.m44 =	1.0f;
				MatrixMul( &mapVSH.vpm, dwtr, &proj2D );
			}			

			// Lights
			// c6, c7, c8 : color[0], color[1], color[2]
			// c9, c10    : dir[0],   dir1[1]
			// if lightable ...
			{
				Matrix dwtrn, iwtr; 
				MatrixNormalize( &dwtrn, dwtr );
				MatrixFastInverse( &iwtr, &dwtrn );
				Vec3ApplyVector	( (Vec3*)mapVSH.lDir, (Vec3*)dlight->direction, &iwtr, -1.0f );
				Vec3ApplyVector	( (Vec3*)(mapVSH.lDir+1), (Vec3*)(dlight->direction+1), &iwtr, -1.0f );						
				Vec3Normalize	((Vec3*)mapVSH.lDir,(Vec3*)mapVSH.lDir);
				Vec3Normalize	((Vec3*)(mapVSH.lDir+1),(Vec3*)(mapVSH.lDir+1));
				mapVSH.lCol[0] = dlight->color[0];
				mapVSH.lCol[1] = dlight->color[1];	
				mapVSH.lCol[2] = dlight->color[2];
			}
			Hrdw::UpdateVertexMapping(& mapVSH,&(mapVSH.vpm),12);
		}

		bool
		SetSurfaceConstant	(	Surface &		inSurf	)
		{
			inSurf.dpystate.Activate( );

			bool needUpdate = FALSE;
			if (mesh->IsLightable()) {
				Vec4Copy(& mapVSH.oCol[0],&inSurf.lightFilters.ambient);
				Vec4Copy(& mapVSH.oCol[1],&inSurf.lightFilters.diffuse);
				needUpdate = TRUE;
			}
			else if( !mapVSH.haveCol ) {
				Vec4Copy(& mapVSH.oCol[1],&inSurf.lightFilters.diffuse);
				needUpdate = TRUE;
			}
			bool textured = inSurf.dpystate.IsTexturing();
			if ( (textured  && (mapVSH.haveTexCoord!=1.0f)) ||
				(!textured && mapVSH.haveTexCoord)){
				mapPSH.haveTexCoord=mapVSH.haveTexCoord = textured?1.0f:0.0f;
				Hrdw::UpdatePixelMapping(&mapPSH,&mapPSH.haveTexCoord,1);
				needUpdate = TRUE;
			}				
			// update Shader constants only if needed
			if (needUpdate) 
				Hrdw::UpdateVertexMapping(&mapVSH,&(mapVSH.oCol));			
			return TRUE;
		}
	};

}




NvGeomShader*
nv_geomshader_mesh_Create	(	NvMesh*		inMesh	)
{
	if( !inMesh )
		return NULL;

	uint surfCpt = inMesh->GetSurfaceCpt();
	uint supply = sizeof(Shader::Surface) * surfCpt;
	Shader* shader = NvEngineNewS( ::Shader, supply );
	if( !shader )
		return NULL;

	shader->surfaceCpt = surfCpt;
	shader->surfaces   = (Shader::Surface*)(shader+1);
	shader->_Init( inMesh );
	return shader->GetInterface();
}


