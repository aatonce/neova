/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkBitmap[PC].h>
#include <Kernel/PC/NvkSurface[PC].h>


#include "NvGeomShader_Base[PC].h"
#include "../NvShaderBase_Surf.h"

namespace 
{	
	struct HLSLVSMapping {
		Matrix 	vpm;			// Transformation Matrix View and Projection
		float   haveTexCoord;
		float	haveCol;
		float	haveNormal;
		float	pad1;
	};

	struct HLSLVSMapping_tl : public HLSLVSMapping {
		Vec4	lCol[3];		// Light Color		
		Vec4 	lDir[2];		// Light Direction
		Vec4	oCol[2];		// Object Color		
	};
	
	struct HLSLPSMapping {
		float   haveNormal;
		float   haveTexCoord;
		float	haveCol;
		float	pad0;
	};

	Hrdw::ShaderProgram	HLSLVS,HLSLVS_tl;
	Hrdw::ShaderProgram	HLSLPS;

	char SurfShaderCodeVHL[] = {
		#include "GeomShader.vhl.bin2h"
	};

	char SurfShaderCodePHL[] = {
		#include "GeomShader.phl.bin2h"
	};

	void
	InitVS		(		)
	{
		static bool done = FALSE;
		if( done ) 
			return;
		done = TRUE;

		HLSLVS		= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER|Hrdw::NV_HLSL,SurfShaderCodeVHL,sizeof(SurfShaderCodeVHL),Hrdw::NV_VS_2,"main");
		HLSLVS_tl	= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER|Hrdw::NV_HLSL,SurfShaderCodeVHL,sizeof(SurfShaderCodeVHL),Hrdw::NV_VS_2,"main","TO_LIT=1");
		HLSLPS		= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER |Hrdw::NV_HLSL,SurfShaderCodePHL,sizeof(SurfShaderCodePHL),Hrdw::NV_PS_2,"main");		
		
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS_tl,HLSLVSMapping_tl,vpm,"vpm"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS_tl,HLSLVSMapping_tl,haveTexCoord,"TexColNrm_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS_tl,HLSLVSMapping_tl,haveCol,"TexColNrm_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS_tl,HLSLVSMapping_tl,haveNormal,"TexColNrm_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS_tl,HLSLVSMapping_tl,lCol,"lCol"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS_tl,HLSLVSMapping_tl,lDir,"lDir"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS_tl,HLSLVSMapping_tl,oCol,"objColor"));
		
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping_tl,vpm,"vpm"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping_tl,haveTexCoord,"TexColNrm_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping_tl,haveCol,"TexColNrm_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping_tl,haveNormal,"TexColNrm_Condition"));

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,haveNormal,"NrmTexCol_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,haveTexCoord,"NrmTexCol_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,haveCol,"NrmTexCol_Condition"));			

	}


	struct Shader : public NvGeomShaderBase , public NvShaderBase_Surf
	{
		HLSLVSMapping_tl	mapVSH_tl;
		HLSLPSMapping		mapPSH;
		virtual	~	Shader	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init(	NvSurface*	inSurf	)
		{
			InitDpyObject();

			mapVSH_tl.haveNormal= 	mapVSH_tl.haveTexCoord	= 	 mapVSH_tl.haveCol	= 0.0f;
			mapPSH.haveNormal	= 	mapPSH.haveTexCoord		= 	 mapPSH.haveCol		= 0.0f;
					
			uint32 dpycaps = NvShaderBase_Surf::Init(inSurf);

			if(  dpycaps & DpyState_PC::CF_HAS_NORMAL ) {
				mapVSH_tl.haveNormal = mapPSH.haveNormal = 1.0;
			}
			if( dpycaps & DpyState_PC::CF_HAS_RGBA ) {
				mapVSH_tl.haveCol = mapPSH.haveCol = 1.0f;					
			}
			else {
				mapVSH_tl.oCol[1] = lightFilters.diffuse;
			}

		}

		void
		AddRef		(		)
		{
			refCpt++;
		}
	
		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}
	
		void
		Release	(		)
		{
			if( ShutDpyObject( GetInterface() ) ) {
				dpystate.Shut();
				NvShaderBase_Surf::Shut();
				SafeRelease( surf );
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return NvShaderBase_Surf::GetResource();
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Surf::SetEnabled( enflags|inEnableFlags );
		}
	
		void
		Disable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Surf::SetEnabled( enflags&(~inEnableFlags) );
		}
	
		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Surf::SetEnabled(inEnableFlags);
		}

		uint
		GetDrawStride	(			)
		{
			return 1;
		}

		bool
		SetDrawStart	(	uint			inOffset		)
		{
			return NvShaderBase_Surf::SetDrawStart(inOffset);
		}

		bool
		SetDrawSize		(	uint			inSize			)
		{
			return NvShaderBase_Surf::SetDrawSize(inSize);
		}
	
		uint32
		GetEnabled		(		)
		{
			return NvShaderBase_Surf::GetEnabled();
		}
	
		bool
		HideSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Surf::HideSurface (inSurfaceIndex	);
		}

		bool
		ShowSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Surf::ShowSurface (inSurfaceIndex	);
		}

		bool
		HideAllSurfaces		(									)
		{
			return NvShaderBase_Surf::HideAllSurfaces();
		}

		bool
		ShowAllSurfaces		(									)
		{
			return NvShaderBase_Surf::ShowAllSurfaces();
		}

		DpyState*
		GetDisplayState	(	uint		inIndex		)
		{
			return NvShaderBase_Surf::GetDisplayState(inIndex);
		}

		bool
		SetAmbient		(	uint			inIndex,
							Vec4*			inColor					)
		{
			return NvShaderBase_Surf::SetAmbient(inIndex,inColor);
		}

		bool
		SetDiffuse		(	uint			inIndex,
							Vec4*			inColor					)
		{
			return NvShaderBase_Surf::SetDiffuse(inIndex,inColor);
		}

		bool
		Draw		(		)
		{
			return NvShaderBase_Surf::Draw(dpyctxt);
		}

		void
		SetShader (											)
		{
			if (mapVSH_tl.haveNormal)
				Hrdw::SetShaderProgram(HLSLVS_tl);
			else
				Hrdw::SetShaderProgram(HLSLVS);
			Hrdw::SetShaderProgram(HLSLPS);
		}

		void
		SetSurfaceConstant	(	int inChainIdx				)
		{
			if (inChainIdx < 0 ) return ;

			NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data() + 
												(DpyManager::context->contextChainA.data() + inChainIdx)->contextIdx;
			NvDpyContext::View*			dview  = DpyManager::context->viewA.data()    + dctxt->viewIdx;
			NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()   + dctxt->lightIdx;
			Matrix*						dwtr   = DpyManager::context->wtrA.data()     + dctxt->wtrIdx;			

			Matrix toProjTR;
			MatrixMul(&toProjTR,&dview->viewTR,&dview->projTR);
			MatrixMul(&toProjTR,&toProjTR,&DpyManager::clipMatrix);

			dpystate.Activate( );
			// TR
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;
			if (!mode2D) {
				MatrixMul( &mapVSH_tl.vpm, dwtr, &toProjTR );
			}
			else {
				Matrix proj2D;
				MatrixZero(&proj2D);
				proj2D.m11 =	2.0f / dview->viewport.z ;
				proj2D.m22 =	-2.0f / dview->viewport.w ;
				proj2D.m33 =	1.0f;
				proj2D.m41 =	-1.0f;
				proj2D.m42 =	1.0f;
				proj2D.m44 =	1.0f;
				MatrixMul( &mapVSH_tl.vpm, dwtr, &proj2D );
			}

			if( surf->GetComponents() & NvSurface::CO_NORMAL ) {
				mapVSH_tl.oCol[1] = lightFilters.diffuse;
				mapVSH_tl.oCol[0] = lightFilters.ambient;
				//mapVSH.oCol[2] = lightFilters.specular;
			}
			if( surf->GetComponents() & NvSurface::CO_COLOR ) {
				mapVSH_tl.haveCol = mapPSH.haveCol = 1.0f;					
			}
			else {
				mapVSH_tl.oCol[1] = lightFilters.diffuse;
			}

			{
				Matrix dwtrn, iwtr;
				MatrixNormalize( &dwtrn, dwtr );
				MatrixFastInverse( &iwtr, &dwtrn );
				Vec3ApplyVector( (Vec3*)&mapVSH_tl.lDir[0], (Vec3*)&dlight->direction[0], &iwtr, -1.0f );
				Vec3ApplyVector( (Vec3*)&mapVSH_tl.lDir[1], (Vec3*)&dlight->direction[1], &iwtr, -1.0f );			
				Vec3Normalize((Vec3*)&mapVSH_tl.lDir[0],(Vec3*)&mapVSH_tl.lDir[0]);
				Vec3Normalize((Vec3*)&mapVSH_tl.lDir[1],(Vec3*)&mapVSH_tl.lDir[1]);
				mapVSH_tl.lCol[0] = dlight->color[0];
				mapVSH_tl.lCol[1] = dlight->color[1];
				mapVSH_tl.lCol[2] = dlight->color[2];
			}

			mapVSH_tl.haveTexCoord = mapPSH.haveTexCoord = dpystate.IsTexturing()?1.0f:0.0f;
			
			if (mapVSH_tl.haveNormal)
				Hrdw::FullUpdateVertexMapping	(&mapVSH_tl);
			else{
				HLSLVSMapping * mapvsh = (HLSLVSMapping *)&mapVSH_tl;
				Hrdw::FullUpdateVertexMapping	(mapvsh);
			}
			Hrdw::FullUpdatePixelMapping	(&mapPSH);				
		}
	};
}



NvGeomShader*
nv_geomshader_surf_Create	(	NvSurface*		inSurface	)
{
	if( !inSurface )
		return NULL;

	InitVS();

	Shader* shaderSurf = NvEngineNew( Shader );
	if( !shaderSurf )
		return NULL;

	shaderSurf->_Init( inSurface );
	return shaderSurf->GetInterface();
}


