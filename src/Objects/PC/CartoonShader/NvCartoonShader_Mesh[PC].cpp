/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkBitmap[PC].h>
#include <Kernel/PC/NvkMesh[PC].h>


#include "NvCartoonShader_Base[PC].h"
#include "../NvShaderBase_Mesh.h"
using namespace nv;


namespace
{	
	enum RenderStage {
		RS_FirstCorner	= 0,
		RS_OtherCorner	= 1,
		RS_Normal		= 8,
		RS_Normal_2		= 9,	// For Tnl mode only
	};

	struct HLSLVSCartoonMapping {
		Matrix 	vpm;			// Transformation Matrix (view and proj)	
		float 	vm[12];			// Transformation Matrix (view) 3x3 matrix but 3 register of 4 float in Vertex shader
		Vec4 	lDir;			// Light Direction	
	};

	struct HLSLVSCartoonMapping_TNL {
		Matrix 	vpm;			// Transformation Matrix (view and proj)	
		Vec4 	lDir;			// Light Direction
		Vec4	vColor;
	};

	struct HLSLPSCartoonMapping {
		Vec4	oCol[2];		// Object Color	0=>Amb , 1=>Diff
		float	threshold	;
		float	texCond		;
		float	inkA		;
		float	inkB		;
	};

	struct HLSLVSOutliningMapping {
		Matrix 	vpm;			// Transformation Matrix (view and proj)
		Vec2	screenDelta;
		float	padding[2];
	};

	Hrdw::ShaderProgram HLSLVSCartoon;
	Hrdw::ShaderProgram HLSLVSCartoon_TNL;
	Hrdw::ShaderProgram HLSLPSCartoon;

	Hrdw::ShaderProgram HLSLVSOutlining;
	Hrdw::ShaderProgram HLSLPSOutlining;

	Hrdw::DeclIndex		smallVertexDeclaration;

	char CartoonShaderCodeVHL[] = {
		#include "CartoonShader_Mesh.vhl.bin2h"
	};

	char CartoonShaderCodeVHL_TNL[] = {
		#include "CartoonShader_Mesh_TNL.vhl.bin2h"
	};

	char CartoonShaderCodePHL[] = {
		#include "CartoonShader_Mesh.phl.bin2h"
	};

	char OutliningCodeVHL[] = {
		#include "Outlining_Mesh.vhl.bin2h"
	};

	char OutliningCodePHL[] = {
		#include "Outlining_Mesh.phl.bin2h"
	};

	void _InitVS_Decl(){
		static bool init = false;
		if (init) return;
		init = true;
		HLSLVSCartoon		= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, CartoonShaderCodeVHL	,sizeof(CartoonShaderCodeVHL)		,Hrdw::NV_VS_2, "main");
		HLSLVSCartoon_TNL   = Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, CartoonShaderCodeVHL_TNL	,sizeof(CartoonShaderCodeVHL_TNL)	,Hrdw::NV_VS_2, "main");
		HLSLPSCartoon		= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, CartoonShaderCodePHL	,sizeof(CartoonShaderCodePHL)		,Hrdw::NV_PS_2, "main");
		HLSLVSOutlining		= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, OutliningCodeVHL		,sizeof(OutliningCodeVHL)			,Hrdw::NV_VS_2, "main");
		HLSLPSOutlining		= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, OutliningCodePHL		,sizeof(OutliningCodePHL)			,Hrdw::NV_PS_2, "main");

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCartoon,HLSLVSCartoonMapping,vpm				,"vpMatrix"						));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCartoon,HLSLVSCartoonMapping,lDir				,"lDir"							));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCartoon,HLSLVSCartoonMapping,vm				,"vMatrix"						));

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCartoon_TNL,HLSLVSCartoonMapping_TNL,vpm			,"vpMatrix"						));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCartoon_TNL,HLSLVSCartoonMapping_TNL,lDir			,"lDir"							));


		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPSCartoon,HLSLPSCartoonMapping,oCol			,"objColor"						));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPSCartoon,HLSLPSCartoonMapping,threshold		,"threshold_texCond_inkA_inkB"	));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPSCartoon,HLSLPSCartoonMapping,texCond		,"threshold_texCond_inkA_inkB"	));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPSCartoon,HLSLPSCartoonMapping,inkA			,"threshold_texCond_inkA_inkB"	));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPSCartoon,HLSLPSCartoonMapping,inkB			,"threshold_texCond_inkA_inkB"	));

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSOutlining,HLSLVSOutliningMapping,vpm		,"vpMatrix"							));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSOutlining,HLSLVSOutliningMapping,screenDelta,"screenDelta"			));


		Hrdw::BeginVertexDeclaration();
		Hrdw::AddToVertexDeclaration(0,0,Hrdw::NV_FLOAT3,Hrdw::NV_POSITION);		
		smallVertexDeclaration = Hrdw::FinalizeVertexDeclaration();
	} 
	
	//
	// BASE		

	struct Shader : public NvCartoonShaderBase, public NvShaderBase_Mesh
	{
		HLSLVSCartoonMapping		mapVSHCartoon;
		HLSLVSCartoonMapping_TNL	mapVSHCartoon_TNL;
		HLSLPSCartoonMapping		mapPSHCartoon;
		HLSLVSOutliningMapping		mapVSHOutlining;

		Vec4 *					lightColor;

		float					threshold;

		bool					inkingState;
		float					inkStart;
		float					inkStop;

		bool					outliningState;
		float					outliningThickness;
		float					outlineQuality;

		float					shiftX;
		float					shiftY;

		RenderStage				renderStage;

		bool					TNL;

		virtual	~	Shader	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init	(	NvMesh*		inMesh	)
		{
			_InitVS_Decl();
			InitDpyObject();
			
			uint dpycaps = NvShaderBase_Mesh::Init(inMesh);

			mapPSHCartoon.texCond	= 0;	
			if( dpycaps & DpyState_PC::CF_HAS_TEX0 )
				mapPSHCartoon.texCond = 1;
		
			threshold		=  float(NvCartoonShader::DEF_SHADLEVEL/100.0f	);
			SetInkingRamp	(	float(NvCartoonShader::DEF_INKRAMP_START)/100.0f	,
								float(NvCartoonShader::DEF_INKRAMP_END)/100.0f		);

			inkingState			= TRUE;
			
			outliningState		= TRUE;
			outliningThickness	= float (NvCartoonShader::DEF_OUTL_THICKNESS);
			outlineQuality		= NvCartoonShader::DEF_OUTL_QUALITY;
		}

		void
		AddRef		(		)
		{
			refCpt++;
		}
	
		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}
	
		void
		Release	(		)
		{			
			if( ShutDpyObject(GetInterface()) ) {
				NvShaderBase_Mesh::Shut();
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return NvShaderBase_Mesh::GetMesh()->GetInterface();
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled( enflags|inEnableFlags );
		}
	
		void
		Disable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled( enflags&(~inEnableFlags) );
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled(inEnableFlags);
		}
	
		uint32
		GetEnabled		(		)
		{
			return NvShaderBase_Mesh::GetEnabled();
		}

		bool
		HideSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Mesh::HideSurface (inSurfaceIndex	);
		}

		bool
		ShowSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Mesh::ShowSurface (inSurfaceIndex	);
		}

		bool
		HideAllSurfaces		(									)
		{
			return NvShaderBase_Mesh::HideAllSurfaces();
		}

		bool
		ShowAllSurfaces		(									)
		{
			return NvShaderBase_Mesh::ShowAllSurfaces();
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return NvShaderBase_Mesh::GetDisplayState(inIndex);
		}

		bool
		SetAmbient		(	uint			inIndex,
							Vec4*			inColor					)
		{
			return NvShaderBase_Mesh::SetAmbient(inIndex,inColor);
		}
		
		bool
		SetDiffuse		(	uint			inSurfaceIndex			,
							Vec4*			inColor					){
			return NvShaderBase_Mesh::SetAmbient(inSurfaceIndex,inColor);
		}

		void 
		SetShadingLevel (	float inLevel		){
			threshold = Clamp(inLevel,0.0f,1.0f);
		}

		void 
		SetInkingRamp	(	float inStartLevel,
							float inEndLevel	)
		{
			inkStart = inStartLevel;
			inkStop	 = inEndLevel;		   
		}

		void
		SetInking			(	bool	inState			)
		{
			inkingState = inState;
		}

		void
		SetOutlining		(	bool	inState			)
		{
			outliningState	= inState;
		}

		void
		SetOutliningThickness	(	float	inThickness		)
		{
			outliningThickness	= Max(inThickness , 0.0f );
		}

		void
		SetOutliningQuality	(	float			inQuality		)
		{
			outlineQuality = Clamp(inQuality,0.0f,1.0f);
		}

		bool
		Draw		(				)
		{
			Hrdw::RunningMode RMode;
			nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
			TNL = RMode < Hrdw::RM_Limited;

			// Clear Stencil
			if (outliningState) {
				NV_ASSERT_RETURN_MTH		(FALSE, dpyctxt.chainHead >= 0 );
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + dpyctxt.chainHead;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;

				uint stencilBpp;
				Hrdw::GetRasterFormat( DpyManager::hwraster[dctxt->raster], NULL,NULL,&stencilBpp);
				NV_ASSERT_RETURN_MTH(FALSE, stencilBpp > 2 );

				Hrdw::SetRaster				(DpyManager::hwraster[dctxt->raster],FALSE,FALSE );
				Hrdw::SetViewPort			(dview->viewport.x, dview->viewport.y, dview->viewport.z, dview->viewport.w, 0.0f, 1.0f);

				Hrdw::ClearRaster(Hrdw::NV_CLEAR_STENCIL,0,0);
			}
			
			// Normal Stage ! Cartoon rendering and mark Stencil if needed
			{
				renderStage = RS_Normal;				
				if (! NvShaderBase_Mesh::Draw(dpyctxt)) {
					return FALSE;
				}
				if (TNL) {
					renderStage = RS_Normal_2;				
					if (! NvShaderBase_Mesh::Draw(dpyctxt)) {
						return FALSE;
					}
				}
			}

			// CornerStage, do outlining
			{
				if (outliningState) {
					float nb_step   = (1.f-outlineQuality)*4.f + (32.f * outlineQuality);
					float angl_step = TwoPi / nb_step;
					float angl_base = Pi * 0.25f;
					for( float angl = 0.f ; angl < TwoPi ; angl += angl_step ) {
						float s, c;
						Sincosf( angl+angl_base, &s, &c );
						shiftX =s* outliningThickness;
						shiftY =c* outliningThickness;
						if (angl == 0.0f) 
							renderStage = RS_FirstCorner;
						else 
							renderStage = RS_OtherCorner;
						if (! NvShaderBase_Mesh::Draw(dpyctxt)) {
							return FALSE;
						}
					}
				}
			}
			Hrdw::SetRenderState			(Hrdw::NV_STENCILENABLE		,FALSE);
			return TRUE;
		}

		void			
		SetShader			(		)
		{ 
			if (renderStage == RS_Normal || renderStage == RS_Normal_2) {
				if (TNL) {
					Hrdw::SetShaderProgram(HLSLVSCartoon_TNL);
				}
				else {
					Hrdw::SetShaderProgram(HLSLVSCartoon);
					Hrdw::SetShaderProgram(HLSLPSCartoon);
				}
			}
			else if (renderStage == 0) {
				Hrdw::SetShaderProgram(HLSLVSOutlining);
				Hrdw::SetShaderProgram(HLSLPSOutlining);
			}
		}

		void
		SetObjectConstant	(	int inChainIdx	)
		{
			if (inChainIdx < 0 ) return ;

			NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data() + 
												(DpyManager::context->contextChainA.data() + inChainIdx)->contextIdx;
			NvDpyContext::View*			dview  = DpyManager::context->viewA.data()    + dctxt->viewIdx;
			NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()   + dctxt->lightIdx;
			Matrix*						dwtr   = DpyManager::context->wtrA.data()     + dctxt->wtrIdx;			
		
			int	inFrameRaster = DpyManager::hwraster[dctxt->raster] ;
			float inFrameW = float(Hrdw::GetRasterWidth(inFrameRaster));
			float inFrameH = float( Hrdw::GetRasterHeight(inFrameRaster));

			if (renderStage < RS_Normal ) {
				NV_ASSERT ( int (renderStage) >= 0 && int (renderStage) < 8 ) ;
				mapVSHOutlining.screenDelta = Vec2( shiftX / inFrameW ,
													shiftY / inFrameH );
			}

			if (inkingState) {
				float  dot_S   = Clamp( inkStart, 0.0f, 1.0f );
				float  dot_E   = Clamp( inkStop, 0.0f, 1.0f );
					  dot_E   = max( dot_S, dot_E );
				float  dot_d   = (dot_E - dot_S);
				if( dot_d <= 0.f )
				   dot_d = 0.001f;			
				mapPSHCartoon.inkA   = 1.0f/float(dot_d);
				mapPSHCartoon.inkB   = -mapPSHCartoon.inkA*dot_S;
			}
			else {
				mapPSHCartoon.inkA   = 0.0f;
				mapPSHCartoon.inkB   = 1.0f;
			}

			mapPSHCartoon.threshold = threshold;

			if (renderStage == RS_FirstCorner || renderStage == RS_Normal) {
				Matrix * shaderMatrixParameter;
				if (renderStage == RS_Normal) 
					shaderMatrixParameter = & mapVSHCartoon.vpm;
				else
					shaderMatrixParameter = & mapVSHOutlining.vpm;

				Matrix toProjTR;
				MatrixMul(&toProjTR,&dview->viewTR,&dview->projTR);
				MatrixMul(&toProjTR,&toProjTR,&DpyManager::clipMatrix);

				// TR
				bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;
				if (!mode2D) {
					MatrixMul( shaderMatrixParameter, dwtr, &toProjTR );
				}
				else {
					Matrix proj2D;
					MatrixZero(&proj2D);
					proj2D.m11 =	2.0f / dview->viewport.z ;
					proj2D.m22 =	-2.0f / dview->viewport.w ;
					proj2D.m33 =	1.0f;					
					proj2D.m41 =	-1.0f;
					proj2D.m42 =	1.0f;
					proj2D.m44 =	1.0f;
					MatrixMul( shaderMatrixParameter, dwtr, &proj2D );
				}
				if ( renderStage == RS_Normal ) {
					Matrix viewMatrix;
					MatrixMul( &viewMatrix, dwtr, &dview->viewTR );	// toviewMatrix = wtr * view
					float ttt[16];
					MatrixMul( (Matrix*)(&ttt) , dwtr, &dview->viewTR );	// toviewMatrix = wtr * view
					Memcpy(mapVSHCartoon.vm,ttt,sizeof(float) * 12);
					{
						Matrix dwtrn, iwtr; 
						MatrixNormalize( &dwtrn, dwtr );
						MatrixFastInverse( &iwtr, &dwtrn );
						Vec3ApplyVector	( (Vec3*) &mapVSHCartoon.lDir	, dlight->direction		, &iwtr, -1.0f );
						Vec3Normalize	( (Vec3*) &mapVSHCartoon.lDir	, (Vec3*)  &mapVSHCartoon.lDir);

						lightColor = dlight->color;
					}			
				}
			}
			if ( renderStage == RS_Normal ) {
				Hrdw::UpdatePixelMapping(&mapPSHCartoon,&mapPSHCartoon.threshold);
				if (TNL) {
					mapVSHCartoon_TNL.vpm		= mapVSHCartoon.vpm;
					mapVSHCartoon_TNL.lDir		= mapVSHCartoon.lDir;
					mapVSHCartoon_TNL.vColor	= Vec4(1.0,1.0,0.0,0.0);
					Hrdw::FullUpdateVertexMapping(&mapVSHCartoon_TNL);
				}
				else
					Hrdw::FullUpdateVertexMapping(&mapVSHCartoon);
			}
			else {
				Hrdw::FullUpdateVertexMapping(&mapVSHOutlining);
			}
		}

		bool
		SetSurfaceConstant	(	Surface &		inSurf	)
		{			
			if ( renderStage == RS_Normal ) {

				inSurf.dpystate.Activate( );

				if (outliningState) {
					Hrdw::SetRenderState			(Hrdw::NV_STENCILENABLE		,TRUE);
					Hrdw::SetRenderState			(Hrdw::NV_STENCILREF		,0);
					Hrdw::SetRenderState			(Hrdw::NV_STENCILFUNC		,Hrdw::NV_CMP_ALWAYS);
					Hrdw::SetRenderState			(Hrdw::NV_STENCILZFAIL		,Hrdw::NV_STENCILOP_KEEP);
					Hrdw::SetRenderState			(Hrdw::NV_STENCILFAIL		,Hrdw::NV_STENCILOP_KEEP);
					Hrdw::SetRenderState			(Hrdw::NV_STENCILPASS		,Hrdw::NV_STENCILOP_INCR);
				}

				NV_ASSERT(lightColor);
				Vec4Mul(&mapPSHCartoon.oCol[0],&inSurf.lightFilters.ambient, &lightColor[0]);
				Vec4Mul(&mapPSHCartoon.oCol[1],&inSurf.lightFilters.diffuse, &lightColor[1]);
				Hrdw::UpdatePixelMapping(&mapPSHCartoon,&mapPSHCartoon.oCol,2)  ;
				
				if (TNL) {
					Hrdw::SetTextureStageState(0,Hrdw::NV_COLORARG1,	Hrdw::NV_TA_DIFFUSE);
					Hrdw::SetTextureStageState(0,Hrdw::NV_COLORARG2,	Hrdw::NV_TA_TEXTURE);
					Hrdw::SetTextureStageState(0,Hrdw::NV_ALPHAARG1,	Hrdw::NV_TA_DIFFUSE);
					Hrdw::SetTextureStageState(0,Hrdw::NV_ALPHAARG2,	Hrdw::NV_TA_TEXTURE);

					if ( inSurf.dpystate.IsTexturing() ) {
						Hrdw::SetTextureStageState( 0, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_MODULATE	);
						Hrdw::SetTextureStageState( 0, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_SELECTARG1	);
					}
					else {
						Hrdw::SetTextureStageState( 0, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_SELECTARG1	);
						Hrdw::SetTextureStageState( 0, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_SELECTARG1	);
					}

					Hrdw::SetTextureStageState( 1, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_DISABLE	);
					Hrdw::SetTextureStageState( 1, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_DISABLE	);				

					Hrdw::SetRenderState( Hrdw::NV_ALPHATESTENABLE, TRUE );
					DWORD alphaRef = (DWORD)(threshold*255.0f);
					Hrdw::SetRenderState( Hrdw::NV_ALPHAREF,	alphaRef );
					Hrdw::SetRenderState( Hrdw::NV_ALPHAFUNC,	Hrdw::NV_CMP_GREATEREQUAL );

					mapVSHCartoon_TNL.vColor = mapPSHCartoon.oCol[0] + mapPSHCartoon.oCol[1];
					Hrdw::UpdateVertexMapping(&mapVSHCartoon_TNL,&mapVSHCartoon_TNL.vColor);
				}
			}
			else if (renderStage == RS_Normal_2) {
				NV_ASSERT(TNL);

				inSurf.dpystate.Activate( );

				Hrdw::SetTextureStageState(0,Hrdw::NV_COLORARG1,	Hrdw::NV_TA_DIFFUSE);
				Hrdw::SetTextureStageState(0,Hrdw::NV_COLORARG2,	Hrdw::NV_TA_TEXTURE);
				Hrdw::SetTextureStageState(0,Hrdw::NV_ALPHAARG1,	Hrdw::NV_TA_DIFFUSE);
				Hrdw::SetTextureStageState(0,Hrdw::NV_ALPHAARG2,	Hrdw::NV_TA_TEXTURE);

				if ( inSurf.dpystate.IsTexturing() ) {
					Hrdw::SetTextureStageState( 0, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_MODULATE	);
					Hrdw::SetTextureStageState( 0, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_SELECTARG1	);
				}
				else {
					Hrdw::SetTextureStageState( 0, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_SELECTARG1	);
					Hrdw::SetTextureStageState( 0, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_SELECTARG1	);
				}

				Hrdw::SetTextureStageState( 1, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_DISABLE	);
				Hrdw::SetTextureStageState( 1, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_DISABLE	);				

				Hrdw::SetRenderState( Hrdw::NV_ALPHATESTENABLE, TRUE );
				DWORD alphaRef = (DWORD)(threshold*255.0f);
				Hrdw::SetRenderState( Hrdw::NV_ALPHAREF,	alphaRef );
				Hrdw::SetRenderState( Hrdw::NV_ALPHAFUNC,	Hrdw::NV_CMP_LESS );

				Vec4Mul(&mapVSHCartoon_TNL.vColor ,&inSurf.lightFilters.ambient, &lightColor[0]);
				Hrdw::FullUpdateVertexMapping(&mapVSHCartoon_TNL);

			}
			else {
				Hrdw::SetVertexDeclaration		(smallVertexDeclaration);
				Hrdw::SetRenderState			(Hrdw::NV_STENCILENABLE		,TRUE);
				Hrdw::SetRenderState			(Hrdw::NV_STENCILREF		,0);
				Hrdw::SetRenderState			(Hrdw::NV_STENCILFUNC		,Hrdw::NV_CMP_EQUAL);
				Hrdw::SetRenderState			(Hrdw::NV_STENCILZFAIL		,Hrdw::NV_STENCILOP_KEEP);
				Hrdw::SetRenderState			(Hrdw::NV_STENCILFAIL		,Hrdw::NV_STENCILOP_KEEP);
				Hrdw::SetRenderState			(Hrdw::NV_STENCILPASS		,Hrdw::NV_STENCILOP_KEEP);

				Hrdw::SetTexture(NO_TEXTURE,0);
				Hrdw::SetTextureStageState( 0, Hrdw::NV_COLOROP,   Hrdw::NV_TOP_DISABLE		);
				Hrdw::SetTextureStageState( 0, Hrdw::NV_ALPHAOP,   Hrdw::NV_TOP_DISABLE		);
				
				Hrdw::SetRenderState( Hrdw::NV_ALPHATESTENABLE, FALSE );
				Hrdw::SetRenderState( Hrdw::NV_ALPHABLENDENABLE,FALSE	);

			}
			return TRUE;
		}
	};

}




NvCartoonShader*
nv_Cartoonshader_mesh_Create	(	NvMesh*		inMesh	)
{
	if( !inMesh )
		return NULL;

	// CartoonShader need normal 
	NvkMesh::BunchHrdwData * hdrwData = inMesh->GetKInterface()->GetHrdwData();
	if (!hdrwData->vbNrm )
		return NULL;

	uint surfCpt = inMesh->GetSurfaceCpt();
	uint supply = sizeof(Shader::Surface) * surfCpt;
	Shader* shader = NvEngineNewS( Shader, supply );
	if( !shader )
		return NULL;

	shader->surfaceCpt = surfCpt;
	shader->surfaces   = (Shader::Surface*)(shader+1);
	shader->_Init( inMesh );
	return shader->GetInterface();
}


