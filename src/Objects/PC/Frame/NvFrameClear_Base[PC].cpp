/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvDpyObject[PC].h>
#include <NvFrameClear.h>



//
// BASE


interface NvFrameClearBase : public NvDpyObject_PC
{
	NvFrameClear		itf;

	uint				clrFlags;
	uint32				clrColor;


	NvFrameClearBase	(			)
	{
		InitDpyObject();
		clrFlags = Hrdw::NV_CLEAR_TARGET|Hrdw::NV_CLEAR_ZBUFFER;
		clrColor = 0;
	}

	virtual	~	NvFrameClearBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

	void
	AddRef	(		)
	{
		refCpt++;
	}

	uint
	GetRefCpt	(		)
	{
		return refCpt;
	}

	void
	Release	(		)
	{
		if( ShutDpyObject(&itf) ) {
			NvEngineDelete( this );
		}
	}

	void
	EnableDepth	(	bool	inOnOff		)
	{
		if( inOnOff )
			clrFlags |= Hrdw::NV_CLEAR_ZBUFFER;
		else
			clrFlags &= ~Hrdw::NV_CLEAR_ZBUFFER;
	}

	void
	EnableColor	(	bool	inOnOff		)
	{
		if( inOnOff )
			clrFlags |= Hrdw::NV_CLEAR_TARGET;
		else
			clrFlags &= ~Hrdw::NV_CLEAR_TARGET;
	}

	void
	SetColor		(	uint32		inRGBA		)
	{
		// RGBA -> ARGB
		uint r = (inRGBA >> 24) & 0xFF;
		uint g = (inRGBA >> 16) & 0xFF;
		uint b = (inRGBA >> 8)  & 0xFF;
		uint a = (inRGBA >> 0)  & 0xFF;
		clrColor = HRDW_COLOR_RGBA( r, g, b, a );
	}

	bool
	Draw		(			)
	{
		NV_ASSERT( dpyctxt.chainHead >= 0 );
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data() + 
											 (DpyManager::context->contextChainA.data() + dpyctxt.chainHead)->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()    + dctxt->viewIdx;

		Hrdw::SetRaster( DpyManager::hwraster[dctxt->raster],FALSE,FALSE );
		Hrdw::SetViewPort(dview->viewport.x,dview->viewport.y ,dview->viewport.z ,dview->viewport.w, 0.0f,1.0f );
		Hrdw::ClearRaster(clrFlags,clrColor,1.f);
		if ( Hrdw::SetRaster( DpyManager::hwraster[dctxt->raster],TRUE,TRUE ))
			Hrdw::ClearRaster(Hrdw::NV_CLEAR_TARGET,0x00000000,1.0f);
		return TRUE;
	}
};



NvFrameClear*
NvFrameClear::Create	(		)
{
	NvFrameClearBase * inst = NvEngineNew( NvFrameClearBase );
	if( inst )
		return &inst->itf;
	else
		return NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameClear,	NvInterface*,	GetBase								)
NVITF_CAL0( FrameClear,	void,			AddRef								)
NVITF_MTH0( FrameClear,	uint,			GetRefCpt							)
NVITF_CAL0( FrameClear,	void,			Release								)
NVITF_CAL1( FrameClear,	void,			EnableDepth,		bool			)
NVITF_CAL1( FrameClear,	void,			EnableColor,		bool			)
NVITF_CAL1( FrameClear,	void,			SetColor,			uint32			)




