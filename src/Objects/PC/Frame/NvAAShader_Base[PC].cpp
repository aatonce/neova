/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvDpyObject[PC].h>
#include <Kernel/PC/NvDpyState[PC].h>
#include <NvAAShader.h>

using namespace nv;


interface NvAAShaderBase : public NvDpyObject_PC
{
	NvAAShader			itf;
		
	virtual	~NvAAShaderBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)
	
	NvAAShader*
	Init	(		)
	{
		InitDpyObject();		
		return &itf;
	}

	void
	Release	(		)
	{
		if( ShutDpyObject(&itf) )
		{
			NvEngineDelete( this );
		}
	}

	void
	Enable		(	uint32		inEnableFlags	)
	{			
	}

	void
	Disable		(	uint32		inEnableFlags	)
	{
	}

	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
	}

	uint32
	GetEnabled		(		)
	{
		return 0;
	}

	// Basic properties
	DpyState*
	GetDisplayState	(	uint			inIndex		)
	{
		return NULL;
	}

	bool
	AddFrame (	uint			inId		,
				NvBitmap *		inFrame		)
	{
		return false;
	}
	
	uint GetNbValidFrame()
	{
		return 0;
	}
	
	bool
	Draw	(		)
	{
		return FALSE;
	}
};





NvAAShader *
NvAAShader::Create	( )
{
	return NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( AAShader,	NvInterface*,	GetBase						)
NVITF_CAL0( AAShader,	void,			AddRef						)
NVITF_MTH0( AAShader,	uint,			GetRefCpt					)
NVITF_CAL0( AAShader,	void,			Release						)
NVITF_MTH1( AAShader,	DpyState*,		GetDisplayState,	uint	)
NVITF_CAL1( AAShader,	void,			Enable,				uint32	)
NVITF_CAL1( AAShader,	void,			Disable,			uint32	)
NVITF_CAL1( AAShader,	void,			SetEnabled,			uint32	)
NVITF_MTH0( AAShader,	uint32,			GetEnabled					)
NVITF_MTH2( AAShader,	bool,			AddFrame,			uint ,	NvBitmap *)