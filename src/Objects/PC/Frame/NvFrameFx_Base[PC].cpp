/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvDpyObject[PC].h>
#include <NvFrameFx.h>
#include <Kernel/PC/NvkFrameFx[PC].h>
#include <Kernel/PC/NvHrdwTools.h>

namespace{
	static const int8 SamplesDefaultCoord[24] = {
			0	,	-2	,
			-1	,	-1	,
			0	,	-1	,
			1	,	-1	,
			-2	,	0	,
			-1	,	0	,
			1	,	0	,
			2	,	0	,
			-1	,	1	,
			0	,	1	,
			1	,	1	,
			0	,	2	,
	};

	struct PsCopyWithAAMapping {
		float   offsetTexCoord[2];
		float   pad[2];
		float	samples[24];
	};	

	struct PsSobelMapping {
		float   offsetTexCoord[2];
		float   pad[2];
		float	thresold;
	};	

	char FX_CopyWithAACodePHL[] = {
		#include "FX_CopyWithAA.phl.bin2h";
	};

	char FX_SobelCodePHL[] = {
		#include "FX_Sobel.phl.bin2h";
	};

	Hrdw::ShaderProgram		psCopyAA	= 0;	
	Hrdw::ShaderProgram		psSobel		= 0;	

	void SetUpShaders() 
	{
		if (!psCopyAA) {			
			psCopyAA	= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, FX_CopyWithAACodePHL	,sizeof(FX_CopyWithAACodePHL),Hrdw::NV_PS_2, "main");			
			psSobel		= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, FX_SobelCodePHL		,sizeof(FX_SobelCodePHL)	 ,Hrdw::NV_PS_2, "main");			
			NV_ASSERT (Hrdw_CheckMappingByName(psCopyAA,PsCopyWithAAMapping,offsetTexCoord,"pOffsetTexCoord"));
			NV_ASSERT (Hrdw_CheckMappingByName(psCopyAA,PsCopyWithAAMapping,samples,"samples"));
			NV_ASSERT (Hrdw_CheckMappingByName(psSobel,PsSobelMapping,offsetTexCoord,"pOffsetTexCoord"));
//			NV_ASSERT (Hrdw_CheckMappingByName(psSobel,PsSobelMapping,thresold,"thresold"));
		}
	}

	void InitTextureState(uint inSamplerId, bool inLinear = FALSE){
		//Hrdw::SetTextureStageState( inSamplerId,	Hrdw::NV_COLOROP,		Hrdw::NV_TOP_MODULATE	);
		if (inLinear){
			Hrdw::SetSamplerState( inSamplerId,		Hrdw::NV_MAGFILTER,		Hrdw::NV_TEXF_LINEAR	);
			Hrdw::SetSamplerState( inSamplerId,		Hrdw::NV_MINFILTER,		Hrdw::NV_TEXF_LINEAR	);
		}
		else{
			Hrdw::SetSamplerState( inSamplerId,		Hrdw::NV_MAGFILTER,		Hrdw::NV_TEXF_POINT );
			Hrdw::SetSamplerState( inSamplerId,		Hrdw::NV_MINFILTER,		Hrdw::NV_TEXF_POINT );			
		}
		Hrdw::SetSamplerState( inSamplerId,		Hrdw::NV_ADDRESSU,		Hrdw::NV_TADDRESS_CLAMP);
		Hrdw::SetSamplerState( inSamplerId,		Hrdw::NV_ADDRESSV,		Hrdw::NV_TADDRESS_CLAMP	);
	}

	void ResetTextureState(uint samplerId){
		Hrdw::SetTexture(NULL,samplerId );
		//Hrdw::SetTextureStageState( samplerId, Hrdw::NV_COLOROP,		Hrdw::NV_TOP_DISABLE);
		//Hrdw::SetTextureStageState( samplerId, Hrdw::NV_ALPHAOP,		Hrdw::NV_TOP_DISABLE);
	}
}

//
// BASE


interface NvFrameFxBase : public NvDpyObject_PC
{

	NvFrameFx						itf;
	NvkFrameFx						kitf;

	uint							enableFX;

	Hrdw::tools::VsCopyMappingTex	vsCopyMapping;
	PsCopyWithAAMapping				psCopyMappingAA;
	PsSobelMapping					psSobelMapping;

	int								tmpRaster;
	int16							currentSizeW;
	int16							currentSizeH;

	float							outLineThresold;
	NvFrameFxBase	(			)
	{
		InitDpyObject(  );		// one per frame !
		refCpt			= 1;
		tmpRaster		= -1;
		currentSizeW	= -1;	
		currentSizeH	= -1;
		enableFX		= 0 ;
		outLineThresold = 0.2f * 0.2f;
	}

	virtual	~	NvFrameFxBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)


	void
	AddRef				(								)
	{
		refCpt++;
	}

	uint
	GetRefCpt			(								)
	{
		return refCpt;
	}

	void
	Release				(								)
	{
		if( ShutDpyObject(&itf) ) {
			if (tmpRaster >= 0 ){
				Hrdw::FreeRaster(tmpRaster);
			}
			NvEngineDelete( this );
		}
	}

	NvkFrameFx*
	GetKInterface		(								)
	{
		return &kitf;
	}


	NvFrameFx*
	GetInterface		(								)
	{
		return &itf;
	}

	bool
	SetDFogDepth		(	uint8			inDepthIdx,
							uint8			inDepth		)
	{
		return FALSE;
	}

	bool					
	SetDFogColor		(	uint32			inRGBA		)
	{
		return FALSE;
	}

	bool
	SetDOFSharpness		(	uint8			inDepthIdx	,
							uint8			inSharpness	)
	{
		return FALSE;
	}
							
	bool
	SetBlurRadius		(	float			inRadius	)
	{
		return FALSE;
	}

	bool
	SetBloom			(	uint32			inRGBA,
							float			inRadius	)
	{
		return FALSE;
	}

	void
	SetOutLineThreshold (	float		Threshold		){
		outLineThresold = Threshold * Threshold;
	}

	bool	GenerateFx	(	uint	inEN				)
	{
		if (inEN & NvFrameFx::FX_OUTLINE){
			Hrdw::RunningMode RMode;
			nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
			bool TNL = RMode < Hrdw::RM_Limited;
			if (TNL)
				return FALSE;
		}
		enableFX = inEN;
		return TRUE;
	}

	bool
	Draw	(		)
	{	
		bool doFog		= (enableFX & (NvFrameFx::FX_DFOG))!=0;
		bool doDof		= (enableFX & (NvFrameFx::FX_DOF))!=0;
		bool doBlur		= (enableFX & (NvFrameFx::FX_BLUR))!=0;
		bool doBloom	= (enableFX & (1<<NvFrameFx::FX_BLOOM))!=0;
		bool doOutLine = (enableFX & (1<<NvFrameFx::FX_OUTLINE))!=0;

		//
		// DFOG
		if( doFog )		
		{
		}	
	
		//
		// DOF
		if( doDof )
		{
		}

		//
		// Bloom
		if( doBloom )
		{
		}

		//
		// BLUR
		if( doBlur )
		{
		}

		if( doOutLine )		
		{

			Hrdw::SetRenderState( Hrdw::NV_SRCBLEND,				Hrdw::NV_BLEND_SRCALPHA	);
			Hrdw::SetRenderState( Hrdw::NV_DESTBLEND,				Hrdw::NV_BLEND_INVSRCALPHA);
			Hrdw::SetRenderState( Hrdw::NV_ALPHABLENDENABLE,		FALSE				);
			Hrdw::SetRenderState( Hrdw::NV_ALPHATESTENABLE,			FALSE				);
			Hrdw::SetRenderState( Hrdw::NV_ZFUNC,					Hrdw::NV_CMP_ALWAYS	);
			Hrdw::SetRenderState( Hrdw::NV_ZWRITEENABLE,			FALSE				);
			Hrdw::SetRenderState( Hrdw::NV_ZENABLE,					Hrdw::NV_ZB_FALSE	);
			Hrdw::SetRenderState( Hrdw::NV_CLIPPING,				FALSE				);
			Hrdw::SetRenderState( Hrdw::NV_SPECULARENABLE,			FALSE				);
			Hrdw::SetRenderState( Hrdw::NV_FILLMODE,				Hrdw::NV_FILL_SOLID	);
			Hrdw::SetRenderState( Hrdw::NV_CULLMODE,				Hrdw::NV_CULL_NONE	);
			Hrdw::SetRenderState( Hrdw::NV_SHADEMODE,				Hrdw::NV_SHADE_FLAT	);
			Hrdw::SetRenderState( Hrdw::NV_COLORVERTEX,				TRUE				);
			Hrdw::SetRenderState( Hrdw::NV_DIFFUSEMATERIALSOURCE,	Hrdw:: NV_MCS_COLOR1  );

			Hrdw::tools::SetVertexBuffer	(	TRUE	);


			int	inFrameRaster = DpyManager::hwraster[DPYR_IN_FRAME] ;
			float inFrameW = float(Hrdw::GetRasterWidth(inFrameRaster));
			float inFrameH = float( Hrdw::GetRasterHeight(inFrameRaster));

			if (inFrameW != currentSizeW || inFrameH != currentSizeH) {
				if (tmpRaster>=0) 
					Hrdw::FreeRaster(tmpRaster);

				tmpRaster = -1;
				for (uint i = 0 ; i < 12 ; ++i ) {
					psCopyMappingAA.samples[i*2	]	= float(SamplesDefaultCoord[i] / inFrameW);
					psCopyMappingAA.samples[i*2 +1] = float(SamplesDefaultCoord[i] / inFrameH);
				}
				currentSizeW = inFrameW;
				currentSizeH = inFrameH;
				tmpRaster = Hrdw::CreateRaster(inFrameW,inFrameH,32,0,FALSE);

				vsCopyMapping.offsetTexCoord[0]		= 0.5f / inFrameW;
				vsCopyMapping.offsetTexCoord[1]		= 0.5f / inFrameH;
				psCopyMappingAA.offsetTexCoord[0]	= vsCopyMapping.offsetTexCoord[0];
				psCopyMappingAA.offsetTexCoord[1]	= vsCopyMapping.offsetTexCoord[1];
				psSobelMapping.offsetTexCoord[0]	= 0.5f / inFrameW;
				psSobelMapping.offsetTexCoord[1]	= 0.5f / inFrameH;
			}

			NV_ASSERT_RETURN_MTH(FALSE,tmpRaster >= 0);
			//----------------------------------------------/	// copy Back => auxRasterCopy 		

			Hrdw::tools::SetShader(TRUE,TRUE,FALSE);
			Hrdw::SetShaderProgram(psSobel);	
			psSobelMapping.thresold = outLineThresold;
			Hrdw::FullUpdateVertexMapping(&vsCopyMapping);
			InitTextureState (0,FALSE);
			ResetTextureState(1);

			Hrdw::SetSource(inFrameRaster,0,TRUE);
			Hrdw::SetRaster(tmpRaster,FALSE,FALSE);

			Hrdw::ClearRaster(Hrdw::NV_CLEAR_TARGET,0x00000000, -1.0f);
			Hrdw::tools::DrawSprite();
			
			Hrdw::SetRenderState( Hrdw::NV_ALPHATESTENABLE	,		TRUE						);
			Hrdw::SetRenderState( Hrdw::NV_ALPHAFUNC		,		Hrdw::NV_CMP_GREATER		);
			Hrdw::SetRenderState( Hrdw::NV_ALPHAREF			,		0x00000088					);

			//Hrdw::SetShaderProgram(psCopyAA);		
			//Hrdw::UpdatePixelMapping(&psCopyMappingAA,&psCopyMappingAA.samples);
			Hrdw::tools::SetShader(TRUE,FALSE,TRUE);
			InitTextureState (0,TRUE);
			Hrdw::SetSource(tmpRaster,0,FALSE);
			Hrdw::SetRaster(inFrameRaster,FALSE);

			Hrdw::tools::DrawSprite();
			//// DEBUG => copy Z to back 
			/*Hrdw::tools::SetShader(TRUE,TRUE,TRUE);
			Hrdw::SetSource(inFrameRaster,0,TRUE);
			Hrdw::SetRaster(inFrameRaster,FALSE,FALSE);
			Hrdw::tools::DrawSprite();*/
		}

		return TRUE;
	}
};


NvFrameFx *
NvFrameFx::Create	(		)
{
	SetUpShaders();

	NvFrameFxBase * inst = NvEngineNew( NvFrameFxBase );
	if( inst )
		return &inst->itf;
	else
		return NULL;
}




//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameFx,	NvInterface*,	GetBase													)
NVITF_CAL0( FrameFx,	void,			AddRef													)
NVITF_MTH0( FrameFx,	uint,			GetRefCpt												)
NVITF_CAL0( FrameFx,	void,			Release													)
NVITF_MTH0( FrameFx,	NvkFrameFx*,	GetKInterface											)
NVITF_MTH1( FrameFx,	bool,			GenerateFx,			uint								)
NVITF_MTH1( FrameFx,	bool, 			SetDFogColor,		uint32								)
NVITF_MTH2( FrameFx,	bool, 			SetDFogDepth,		uint8,	uint8						)
NVITF_MTH2( FrameFx,	bool, 			SetDOFSharpness,	uint8,	uint8						)
NVITF_MTH1( FrameFx,	bool, 			SetBlurRadius,		float								)
NVITF_MTH2( FrameFx,	bool, 			SetBloom,			uint32,	float						)

NVKITF_MTH0( FrameFx,	NvInterface*,	GetBase													)
NVKITF_CAL0( FrameFx,	void,			AddRef													)
NVKITF_MTH0( FrameFx,	uint,			GetRefCpt												)
NVKITF_CAL0( FrameFx,	void,			Release													)
NVKITF_MTH0( FrameFx,	NvFrameFx*,		GetInterface											)

NVKITF_CAL1( FrameFx,	void,			SetOutLineThreshold,float								);
