/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvShaderBase_Mesh_H_
#define	_NvShaderBase_Mesh_H_

#include <Kernel/PC/NvDpyObject[PC].h>


struct NvShaderBase_Mesh
{
	struct LighFilters {
		Vec4		emissive;			// in [0,1]^4
		Vec4		ambient;			// in [0,1]^4
		Vec4		diffuse;			// in [0,1]^4
		Vec4		specular;			// in [0,1]^4
		float		glossiness;
	};

	struct Surface {
		DpyState_PC			dpystate;
		LighFilters			lightFilters;
		uint				surfId;
		bool				hide;
	};

	uint					enflags;		// enable flags (EN_...)
	NvkMesh*				mesh;
	Surface*				surfaces;
	uint16					surfaceCpt;
	Hrdw::DeclIndex			VertexDeclaration;


	uint					Init				(	NvMesh*			inMesh					);	
	void					Shut				(											);
	NvkMesh*				GetMesh				(											);

	NvResource*				GetResource			(											);
	// Enable flags
	void					Enable				(	uint32			inEnableFlags			);
	void					Disable				(	uint32			inEnableFlags			);
	void					SetEnabled			(	uint32			inEnableFlags			);
	uint32					GetEnabled			(											);

	bool					HideSurface			(	uint			inSurfaceIndex			);
	bool					ShowSurface			(	uint			inSurfaceIndex			);
	bool					HideAllSurfaces		(											);
	bool					ShowAllSurfaces		(											);

	// Surface properties

	DpyState*				GetDisplayState		(	uint			inSurfaceIndex			);
	bool					SetDrawStart		(	uint			inSurfaceIndex,
													uint			inOffset				);
	bool					SetDrawSize			(	uint			inSurfaceIndex,
													uint			inSize					);
	bool					SetEmissive			(	uint			inSurfaceIndex,
													Vec4*			inColor					);
	bool					SetAmbient			(	uint			inSurfaceIndex,
													Vec4*			inColor					);
	bool					SetDiffuse			(	uint			inSurfaceIndex,
													Vec4*			inColor					);
	bool					SetSpecular			(	uint			inSurfaceIndex,
													Vec4*			inColor			= NULL,
													float*			inLevel			= NULL,
													float*			inGlossiness	= NULL	);
	// Draw					Mesh
	bool					Draw				(	NvDpyObject_PC::DpyCtxt	& inDpyctxt		);

	// Shader function 
	virtual void			SetShader			(											) = 0;
	virtual void			SetObjectConstant	(	int				inChainIdx				) = 0;
	virtual bool			SetSurfaceConstant	(	Surface &		inSurf					) = 0;
};



#endif // _NvShaderBase_Mesh_H_



