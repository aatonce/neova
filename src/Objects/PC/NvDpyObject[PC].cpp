/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvDpyObject[PC].h>
using namespace nv;
using namespace DpyManager;




void
NvDpyObject_PC::InitDpyObject	()
{
	refCpt						= 1;
	dpyctxt.drawFrameNo	  		= ~0U;
	dpyctxt.chainHead			= -1;
	dpyctxt.chainCpt			= 0;
}


bool
NvDpyObject_PC::ShutDpyObject	(	NvDpyObject*	inITF	)
{
	NV_ASSERT( refCpt > 0 );

	if( refCpt > 1 ) {
		refCpt--;
		return FALSE;
	}

	// drawing in progress ?
	NV_ASSERT( inITF );
	if( DpyManager::IsDrawing(inITF) ) {
		// -> add to DpyManager garbager
		// -> keep refCpt = 1 !
		DpyManager::AddToGarbager( inITF );
		return FALSE;
	}

	// full release
	refCpt = 0;
	return TRUE;
}


