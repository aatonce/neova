/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvPoseShader_Base[PC].h"
#include <NvMesh.h>
#include <NvSurface.h>


/*
NvPoseShader*	nv_poseshader_mesh_Create	(	NvMesh*,	uint, uint	);
NvPoseShader*	nv_poseshader_surf_Create	(	NvSurface*,	uint, uint	);
*/

NvPoseShader*
NvPoseShader::Create	(	NvResource*		inRsc,
							uint			inComponents,
							uint			inSize	)
{
/*	if( !inRsc || !inListSize )
		return NULL;

	if( inRsc->GetRscType() == NvMesh::TYPE )
		return nv_poseshader_mesh_Create( (NvMesh*)inRsc, inComponents, inSize );
	else if( inRsc->GetRscType() == NvSurface::TYPE )
		return nv_poseshader_surf_Create( (NvSurface*)inRsc, inComponents, inSize );
	else
*/		return NULL;
}



//
// INTERFACES


#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( PoseShader,	NvInterface*,			GetBase										)
NVITF_CAL0( PoseShader,	void,					AddRef										)
NVITF_MTH0( PoseShader,	uint,					GetRefCpt									)
NVITF_CAL0( PoseShader,	void,					Release										)
NVITF_MTH0( PoseShader,	NvResource*,			GetResource									)
NVITF_CAL1( PoseShader,	void,					Enable,				uint32					)
NVITF_CAL1( PoseShader,	void,					Disable,			uint32					)
NVITF_CAL1( PoseShader,	void,					SetEnabled,			uint32					)
NVITF_MTH0( PoseShader,	uint32,					GetEnabled									)
NVITF_MTH1( PoseShader,	bool,					HideSurface,		uint					)
NVITF_MTH1( PoseShader,	bool,					ShowSurface,		uint					)
NVITF_MTH0( PoseShader,	bool,					HideAllSurfaces								)
NVITF_MTH0( PoseShader,	bool,					ShowAllSurfaces								)
NVITF_MTH1( PoseShader,	DpyState*,				GetDisplayState,	uint					)
NVITF_MTH0( PoseShader,	uint,					GetDrawStride								)
NVITF_MTH1( PoseShader,	bool,					SetDrawStart,		uint					)
NVITF_MTH1( PoseShader,	bool,					SetDrawSize,		uint					)
NVITF_CAL1( PoseShader,	void,					SetMode,			NvPoseShader::Mode		)
NVITF_CAL2( PoseShader,	void,					SetFading,			float, float			)
NVITF_MTH0( PoseShader,	uint,					GetPoseComponents							)
NVITF_MTH0( PoseShader,	uint,					GetListSize									)
NVITF_CAL1( PoseShader,	void,					SetListDrawSize,	uint					)
NVITF_MTH0( PoseShader,	uint,					GetListDrawSize								)
NVITF_MTH0( PoseShader,	bool,					CreateAccess								)
NVITF_MTH3( PoseShader,	pvoid,					GetAccess,			Component, uint&, Psm*	)
NVITF_CAL2( PoseShader,	void,					ValidateAccess,		uint, uint				)
NVITF_CAL0( PoseShader,	void,					ReleaseAccess								)




