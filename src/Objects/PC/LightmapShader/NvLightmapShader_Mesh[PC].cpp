/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkBitmap[PC].h>
#include <Kernel/PC/NvkMesh[PC].h>

#include "NvLightmapShader_Base[PC].h"
#include "../NvShaderBase_Mesh.h"
using namespace nv;




namespace
{	
	struct HLSLVSMapping {
		Matrix 	vpm;			// Transformation Matrix View and Projection
		float   haveTexCoord;
		float	haveCol;
		float	pad3[2];
	};

	struct HLSLPSMapping {
		float   haveTexCoord;
		float	haveCol;
		float	pad0[2];
	};

	Hrdw::ShaderProgram HLSLVS;
	Hrdw::ShaderProgram HLSLPS;

	char LightmapShaderCodeVHL[] = {
#include "LightmapShader.vhl.bin2h"
	};

	char LightmapShaderCodePHL[] = {
#include "LightmapShader.phl.bin2h"
	};

	void _InitVS_Decl()
	{
		static bool init = false;
		if (init) return;
		init = true;
		HLSLVS = Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, LightmapShaderCodeVHL,sizeof(LightmapShaderCodeVHL),Hrdw::NV_VS_2, "main");
		HLSLPS = Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, LightmapShaderCodePHL,sizeof(LightmapShaderCodePHL),Hrdw::NV_PS_2, "main");

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,vpm,"vpm"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,haveTexCoord,"TexCol_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,haveCol,"TexCol_Condition"));

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,haveTexCoord,"TexCol_Condition"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,haveCol,"TexCol_Condition"));			
	} 

	//
	// BASE

	struct Shader : public NvLightmapShaderBase , public NvShaderBase_Mesh
	{
		HLSLVSMapping			mapVSH;
		HLSLPSMapping			mapPSH;		

		virtual	~	Shader	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		void
			_Init	(	NvMesh*		inMesh	)
		{
			_InitVS_Decl();
			InitDpyObject();

			uint dpycaps = NvShaderBase_Mesh::Init(inMesh);

			mapVSH.haveTexCoord = 	 mapVSH.haveCol = 0.0f;
			mapPSH.haveTexCoord = 	 mapPSH.haveCol = 0.0f;

			if( dpycaps & DpyState_PC::CF_HAS_TEX0 )
				mapPSH.haveTexCoord = mapVSH.haveTexCoord = 1.0f;
			if( dpycaps & DpyState_PC::CF_HAS_RGBA )
				mapPSH.haveCol = mapVSH.haveCol = 1.0f;
		}

		void
		AddRef		(		)
		{
			refCpt++;
		}

		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}

		void
		Release	(		)
		{
			if( ShutDpyObject(GetInterface()) ) {
				NvShaderBase_Mesh::Shut();
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return NvShaderBase_Mesh::GetResource(  );			
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled( enflags|inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled( enflags&(~inEnableFlags) );
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled(inEnableFlags);
		}

		uint32
		GetEnabled		(		)
		{
			return NvShaderBase_Mesh::GetEnabled();
		}

		bool
		HideSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Mesh::HideSurface (inSurfaceIndex	);
		}

		bool
		ShowSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Mesh::ShowSurface (inSurfaceIndex	);
		}

		bool
		HideAllSurfaces		(									)
		{
			return NvShaderBase_Mesh::HideAllSurfaces();
		}

		bool
		ShowAllSurfaces		(									)
		{
			return NvShaderBase_Mesh::ShowAllSurfaces();
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return NvShaderBase_Mesh::GetDisplayState(inIndex);
		}

		bool	
		LightmapSurface		(	uint	inSurfaceIndex			,
								bool	inEnable				)
		{

			if( inSurfaceIndex >= surfaceCpt )
				return FALSE;

			if (inEnable) 
				surfaces[inSurfaceIndex].dpystate.SetMode	( DpyState::BM_MUL | DpyState::BS_FRAGMENT ) ;	
			else 
				surfaces[inSurfaceIndex].dpystate.SetMode	( DpyState::BM_NONE | DpyState::BS_FRAGMENT ) ;	

			return TRUE;
		}

		bool
		Draw		(				)
		{
			return NvShaderBase_Mesh::Draw(dpyctxt);
		}

		void			
		SetShader			(		)
		{ 
			Hrdw::SetShaderProgram(HLSLVS);
			Hrdw::SetShaderProgram(HLSLPS);
			Hrdw::FullUpdatePixelMapping(&mapPSH);
		}

		void
		SetObjectConstant	(	int inChainIdx	)
		{
			if (inChainIdx < 0 ) return ;

			NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data() + 
												(DpyManager::context->contextChainA.data() + inChainIdx)->contextIdx;
			NvDpyContext::View*			dview  = DpyManager::context->viewA.data()    + dctxt->viewIdx;
			Matrix*						dwtr   = DpyManager::context->wtrA.data()     + dctxt->wtrIdx;			

			Matrix toProjTR;
			MatrixMul(&toProjTR,&dview->viewTR,&dview->projTR);
			MatrixMul(&toProjTR,&toProjTR,&DpyManager::clipMatrix);

			//			MatrixIdentity (  dwtr );
			// TR
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;
			if (!mode2D) {
				MatrixMul( &mapVSH.vpm, dwtr, &toProjTR);
			}
			else {
				Matrix proj2D;
				MatrixZero(&proj2D);
				proj2D.m11 =	2.0f / dview->viewport.z ;
				proj2D.m22 =	-2.0f / dview->viewport.w ;
				proj2D.m33 =	1.0f;
				proj2D.m41 =	-1.0f;
				proj2D.m42 =	1.0f;
				proj2D.m44 =	1.0f;
				MatrixMul( &mapVSH.vpm, dwtr, &proj2D );
			}

			Hrdw::FullUpdateVertexMapping(& mapVSH);
		}

		bool
		SetSurfaceConstant	(	Surface &		inSurf	)
		{
			inSurf.dpystate.Activate( );

			bool needUpdate = FALSE;
			bool textured = inSurf.dpystate.IsTexturing();
			if ( (textured  && (mapVSH.haveTexCoord!=1.0f)) ||
				(!textured && mapVSH.haveTexCoord)){
					mapPSH.haveTexCoord=mapVSH.haveTexCoord = textured?1.0f:0.0f;
					Hrdw::UpdatePixelMapping(&mapPSH,&mapPSH.haveTexCoord,1);
					needUpdate = TRUE;
			}				
			// update Shader constants only if needed
			if (needUpdate) 
				Hrdw::UpdateVertexMapping(&mapVSH,&(mapVSH.haveTexCoord));			
			return TRUE;
		}
	};

}




NvLightmapShader*
nv_lightmapshader_mesh_Create	(	NvMesh*		inMesh	)
{
	if( !inMesh )
		return NULL;

	uint surfCpt = inMesh->GetSurfaceCpt();
	uint supply = sizeof(Shader::Surface) * surfCpt;
	Shader* shader = NvEngineNewS( ::Shader, supply );
	if( !shader )
		return NULL;

	shader->surfaceCpt = surfCpt;
	shader->surfaces   = (Shader::Surface*)(shader+1);
	shader->_Init( inMesh );
	return shader->GetInterface();
}


