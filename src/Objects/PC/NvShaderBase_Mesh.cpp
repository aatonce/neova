/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkBitmap[PC].h>
#include <Kernel/PC/NvkMesh[PC].h>
#include <Kernel/PC/NvDpyState[PC].h>
#include <NvShader.h>

#include "NvShaderBase_Mesh.h"

using namespace nv;



uint
NvShaderBase_Mesh::Init	(	NvMesh*		inMesh	)
{
	NV_ASSERT(inMesh);

	enflags = uint( NvShader::EN_DEFAULT );

	mesh = inMesh->GetKInterface();
	mesh->AddRef();

	NvkMesh::BunchHrdwData * hdrwData = mesh->GetHrdwData();

	Hrdw::BeginVertexDeclaration();
	Hrdw::AddToVertexDeclaration(0,0,Hrdw::NV_FLOAT3,Hrdw::NV_POSITION);		

	uint dpycaps = 0;// surfaces[i].dpycaps = 0;

	if( hdrwData->vbNrm ) {	
		Hrdw::AddToVertexDeclaration(1,0,Hrdw::NV_FLOAT3,Hrdw::NV_NORMAL);
		dpycaps |= DpyState_PC::CF_HAS_NORMAL;
	}
	
	if( hdrwData->vbTex ){	
		Hrdw::AddToVertexDeclaration(2,0,Hrdw::NV_FLOAT2,Hrdw::NV_TEXCOORD);
		dpycaps |= DpyState_PC::CF_HAS_TEX0;
	}	

	if( hdrwData->vbCol /*|| (inMesh->IsLightable() && shading.vbNrm)*/ ){	
		Hrdw::AddToVertexDeclaration(3,0,Hrdw::NV_COLORT,Hrdw::NV_COLOR);
		dpycaps |= DpyState_PC::CF_HAS_RGBA;
	}
	VertexDeclaration = Hrdw::FinalizeVertexDeclaration();

	for( uint i = 0 ; i < surfaceCpt ; i++ )
	{
		NvkMesh::Shading shading;
		mesh->GetSurfaceShading( shading, i );			

		surfaces[i].surfId  = i;
		surfaces[i].hide	= FALSE;
		// Init dpystate
		ConstructInPlace( &surfaces[i].dpystate );
		surfaces[i].dpystate.Init();				
		surfaces[i].dpystate.SetEnabled( shading.stateEnFlags );
		surfaces[i].dpystate.SetEnabledMask( enflags );
		surfaces[i].dpystate.SetMode( shading.stateMdFlags );
		surfaces[i].dpystate.SetAlphaPass( shading.stateAlphaPass );
		surfaces[i].dpystate.SetBlendSrcCte( shading.stateBlendCte );
		if( shading.bitmapUID ) {
			NvBitmap* bmpP = NvBitmap::Create( shading.bitmapUID );
			if( bmpP ) {
				surfaces[i].dpystate.SetSource( DpySource(bmpP) );
				bmpP->Release();
			} else {
				bmpP = NULL;
			}
		}

		// Init capabilities
		surfaces[i].dpystate.SetCapabilities( dpycaps );
			
		// Color filters
		if( inMesh->IsLightable() ) {
			surfaces[i].lightFilters.emissive = Vec4((float)(( shading.emissive >>0)&0xFF),
														(float)(( shading.emissive >>8)&0xFF),
														(float)(( shading.emissive >>16)&0xFF),
														(float)255.0f);
			surfaces[i].lightFilters.emissive/=255.0;
			surfaces[i].lightFilters.ambient  = Vec4((float)(( shading.ambient >>0)&0xFF),
														(float)(( shading.ambient >>8)&0xFF),
														(float)(( shading.ambient >>16)&0xFF),
														(float)255.0f);
			surfaces[i].lightFilters.ambient /= 255.0;
			surfaces[i].lightFilters.diffuse  = Vec4((float)(( shading.diffuse >>0)&0xFF),
														(float)(( shading.diffuse >>8)&0xFF),
														(float)(( shading.diffuse >>16)&0xFF),
														(float)255.0f);
			surfaces[i].lightFilters.diffuse/=255.0;

			surfaces[i].lightFilters.specular = Vec4((float)(( shading.specular >>0)&0xFF),
														(float)(( shading.specular >>8)&0xFF),
														(float)(( shading.specular >>16)&0xFF),
														(float)255.0f);
			surfaces[i].lightFilters.specular/=255.0;
		}
	}

	return dpycaps;
}

void
NvShaderBase_Mesh::Shut	(		)
{
	for( uint i = 0 ; i < surfaceCpt ; i++ )
		surfaces[i].dpystate.Shut();
	SafeRelease( mesh );
	surfaceCpt = 0;
}

NvkMesh*
NvShaderBase_Mesh::GetMesh		(					)
{
	return mesh;
}

NvResource* 
NvShaderBase_Mesh::GetResource ( )
{
	return NvShaderBase_Mesh::GetMesh()->GetInterface();
}

void
NvShaderBase_Mesh::Enable		(	uint32		inEnableFlags	)
{
	SetEnabled( enflags|inEnableFlags );
}

void
NvShaderBase_Mesh::Disable		(	uint32		inEnableFlags	)
{
	SetEnabled( enflags&(~inEnableFlags) );
}

void
NvShaderBase_Mesh::SetEnabled		(	uint32		inEnableFlags	)
{
	if( inEnableFlags != enflags ) {
		enflags = inEnableFlags;
		for( uint i = 0 ; i < surfaceCpt ; i++ )
			surfaces[i].dpystate.SetEnabledMask( enflags );
	}
}

uint32
NvShaderBase_Mesh::GetEnabled		(		)
{
	return enflags;
}

bool
NvShaderBase_Mesh::HideSurface			(	uint			inSurfaceIndex	)
{
	if ( inSurfaceIndex>=surfaceCpt ) return FALSE;
	surfaces[inSurfaceIndex].hide = TRUE;
	return TRUE;
}

bool
NvShaderBase_Mesh::ShowSurface			(	uint			inSurfaceIndex	)
{
	if ( inSurfaceIndex>=surfaceCpt ) return FALSE;
	surfaces[inSurfaceIndex].hide = FALSE;
	return TRUE;
}

bool
NvShaderBase_Mesh::HideAllSurfaces		(									)
{
	bool ok = TRUE;
	for (uint i = 0 ; i < surfaceCpt ; ++i)
		ok &= NvShaderBase_Mesh::HideSurface(i);
	return ok;
}

bool
NvShaderBase_Mesh::ShowAllSurfaces		(									)
{
	bool ok = TRUE;
	for (uint i = 0 ; i < surfaceCpt ; ++i)
		ok &= NvShaderBase_Mesh::ShowSurface(i);
	return ok;
}

DpyState*
NvShaderBase_Mesh::GetDisplayState	(	uint			inIndex		)
{
	return ( inIndex<surfaceCpt ) ? &surfaces[inIndex].dpystate : NULL;
}

bool
NvShaderBase_Mesh::SetEmissive		(	uint			inIndex,
										Vec4*			inColor					)
{
	if( !mesh->IsLightable() || inIndex >= surfaceCpt )
		return FALSE;
	if( inColor )
		Vec4Copy( &surfaces[inIndex].lightFilters.emissive, inColor );
	return TRUE;
}

bool
NvShaderBase_Mesh::SetAmbient		(	uint			inIndex,
										Vec4*			inColor					)
{
	if( !mesh->IsLightable() || inIndex >= surfaceCpt )
		return FALSE;
	if( inColor )
		Vec4Copy( &surfaces[inIndex].lightFilters.ambient, inColor );
	return TRUE;
}

bool
NvShaderBase_Mesh::SetDiffuse		(	uint			inIndex,
										Vec4*			inColor					)
{
	if( !mesh->IsLightable() || inIndex >= surfaceCpt )
		return FALSE;
	if( inColor )
		Vec4Copy( &surfaces[inIndex].lightFilters.diffuse, inColor );
	return TRUE;
}

bool
NvShaderBase_Mesh::SetSpecular		(	uint			inIndex,
										Vec4*			inColor,
										float*			inLevel,
										float*			inGlossiness			)
{
	if( !mesh->IsLightable() || inIndex >= surfaceCpt )
		return FALSE;
	if( inColor )
		Vec4Copy( &surfaces[inIndex].lightFilters.specular, inColor );
	if (inGlossiness) 
		surfaces[inIndex].lightFilters.glossiness = *inGlossiness;

	return TRUE;
}

bool
NvShaderBase_Mesh::Draw				(	NvDpyObject_PC::DpyCtxt	& inDpyctxt	)
{
	// Update Mesh
	mesh->UpdateBeforeDraw();

	NV_ASSERT( inDpyctxt.chainHead >= 0 );
	int chainIdx = inDpyctxt.chainHead;

	while( chainIdx >= 0 )
	{
		SetShader();				
		SetObjectConstant(chainIdx);

		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data() + 
											(DpyManager::context->contextChainA.data() + chainIdx)->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()    + dctxt->viewIdx;

		chainIdx = (DpyManager::context->contextChainA.data() + chainIdx)->next;

		// RenderTarget
		Hrdw::SetRaster( DpyManager::hwraster[dctxt->raster] );
		// VP
		Hrdw::SetViewPort(dview->viewport.x, dview->viewport.y, dview->viewport.z, dview->viewport.w, 0.0f, 1.0f);		

		// Draw Mesh
		NvkMesh::BunchList *		bunchBySurf =  mesh->GetBunchBySurfA();
		NvkMesh::BunchHrdwData *	hrdwData   = (NvkMesh::BunchHrdwData *) mesh->GetHrdwData();

		Hrdw::SetVertexDeclaration(VertexDeclaration);
		// set streams
		Hrdw::SetVertexBuffer(hrdwData->vbLoc, 0, 0 ,12);
		if( hrdwData->vbNrm ) 
			Hrdw::SetVertexBuffer(hrdwData->vbNrm, 1, 0 ,12);
		else
			Hrdw::SetVertexBuffer(Hrdw::SD_DummyStream, 1, 0 ,0);

		if( hrdwData->vbTex ) 
			Hrdw::SetVertexBuffer(hrdwData->vbTex, 2, 0 ,8);
		else
			Hrdw::SetVertexBuffer(Hrdw::SD_DummyStream, 2, 0 ,0);

		if( hrdwData->vbCol )
			Hrdw::SetVertexBuffer(hrdwData->vbCol, 3, 0 ,4);
		else
			Hrdw::SetVertexBuffer(Hrdw::SD_DummyStream, 3, 0 ,0);
		Hrdw::SetIndexBuffer(hrdwData->ib);

		for( uint i = 0 ; i < surfaceCpt ; i++ )				
		{
			if ( surfaces[i].hide ) continue ;
			if ( ! SetSurfaceConstant(surfaces[i]) ) continue ;

			NvkMesh::BunchData * bunchData = mesh->GetBunchDataA() + bunchBySurf[i].startIdx;
			
			for (uint j = 0 ; j < bunchBySurf[i].size ; ++j ) 
			{						
				// draw surface						
				Hrdw::DrawPrimitive(Hrdw::NV_TRIANGLESTRIP,
									bunchData->ibSize-2,
									bunchData->indexOffset,
									bunchData->vbSize,
									bunchData->vertexDataOffset);

				bunchData = mesh->GetBunchDataA() + bunchData->nextBySurf;
			}
		}
	}
	return TRUE;
}