/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef	_NvMappingShaderBase_PC_H_
#define	_NvMappingShaderBase_PC_H_



#include <Kernel/PC/NvDpyObject[PC].h>
#include <Kernel/PC/NvDpyState[PC].h>
#include <NvMappingShader.h>



//
// BASE implementation


struct NvMappingShaderBase : public NvDpyObject_PC
{
private:
	friend struct NvMappingShader;
	NvMappingShader	itf;
public:
			NvMappingShader*		GetInterface		(															) { return &itf;  }
	virtual	void					AddRef				(															) = 0;
	virtual	uint					GetRefCpt			(															) = 0;
	virtual	void					Release				(															) = 0;
	virtual NvResource*				GetResource			(															) = 0;
	virtual	void					Enable				(	uint32							inEnableFlags			) = 0;
	virtual	void					Disable				(	uint32							inEnableFlags			) = 0;
	virtual	void					SetEnabled			(	uint32							inEnableFlags			) = 0;
	virtual	uint32					GetEnabled			(															) = 0;

	virtual bool					HideSurface			(	uint							inSurfaceIndex			) = 0;
	virtual bool					ShowSurface			(	uint							inSurfaceIndex			) = 0;
	virtual bool					HideAllSurfaces		(															) = 0;
	virtual bool					ShowAllSurfaces		(															) = 0;
	virtual DpyState*				GetDisplayState		(	uint							inIndex					) = 0;
	virtual	uint					GetDrawStride		(															) { return 0;		}
	virtual bool					SetDrawStart		(	uint							inOffset				) { return FALSE;	}
	virtual bool					SetDrawSize			(	uint							inSize					) { return FALSE;	}

	virtual bool					SetMappingSrc		(	NvMappingShader::MappingSrc		inSource				) = 0;
	virtual bool					SetMappingTR		(	Matrix*							inTransfo				) = 0;
	virtual bool					SetColorFilter		(	Vec4*							inColor					) = 0;
};



#endif _NvMappingShaderBase_PC_H_



