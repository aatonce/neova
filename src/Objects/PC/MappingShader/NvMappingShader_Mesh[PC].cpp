/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/PC/NvHrdw.h>
#include "NvMappingShader_Base[PC].h"
#include <Kernel/PC/NvkBitmap[PC].h>
#include <Kernel/PC/NvkMesh[PC].h>
#include "../NvShaderBase_Mesh.h"


using namespace nv;

namespace
{
	struct HLSLVSMapping {
		Matrix 	vpm;			// Transformation Matrix View and Projection
		Matrix	uvTR;
		Vec4	select;
	};
	
	struct HLSLPSMapping {
		Vec4	colorFilter;
		float	haveTexture;
		float	padding[3];
	};

	Hrdw::ShaderProgram HLSLVS;
	Hrdw::ShaderProgram HLSLPS;
	
	char GeomShaderCodeVHL[] = {
		#include "MappingShader.vhl.bin2h"
	};

	char GeomShaderCodePHL[] = {
		#include "MappingShader.phl.bin2h"
	};

	void _InitVS_Decl(){
		static bool init = false;
		if (init) return;
		init = true;

		HLSLPS = Hrdw::CreateShaderProgram(	Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, 
											GeomShaderCodePHL,
											sizeof(GeomShaderCodePHL),
											Hrdw::NV_PS_2,
											"main");		
		HLSLVS = Hrdw::CreateShaderProgram( Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, 
											GeomShaderCodeVHL,
											sizeof(GeomShaderCodeVHL),
											Hrdw::NV_VS_2,												
											"main");

/*		NV_ASSERT (Hrdw_CheckMappingByName(HLSLPS,HLSLPSMapping,colorFilter	,"colorFilter"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,vpm			,"vpm" ));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,uvTR		,"uvTR"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping,select		,"select"));*/
	}

	//
	// BASE

	struct Shader : public NvMappingShaderBase , public NvShaderBase_Mesh
	{		
		HLSLVSMapping			mapVSH;
		HLSLPSMapping			mapPSH;

		uint					dpycaps;


		virtual	~	Shader	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init	(	NvMesh*		inMesh	)
		{
			_InitVS_Decl();
			InitDpyObject();
			
			dpycaps = NvShaderBase_Mesh::Init(inMesh);


			MatrixIdentity( & mapVSH.uvTR );
			mapPSH.colorFilter = Vec4(1.0f,1.0f,1.0f,1.0f);
			SetMappingSrc(NvMappingShader::MS_LOC);
			// Adding Texture caps to surfaces !!
			for (uint i = 0 ; i < surfaceCpt ; ++i ) 
				surfaces[i].dpystate.SetCapabilities(dpycaps | DpyState_PC::CF_HAS_TEX0);
			mapPSH.haveTexture = TRUE;
		}

		void
		AddRef		(		)
		{
			refCpt++;
		}
	
		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}
	
		void
		Release	(		)
		{
			if( ShutDpyObject(GetInterface()) ) {
				NvShaderBase_Mesh::Shut();
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return NvShaderBase_Mesh::GetResource(  );			
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled( enflags|inEnableFlags );
		}
	
		void
		Disable		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled( enflags&(~inEnableFlags) );
		}
	
		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			NvShaderBase_Mesh::SetEnabled(inEnableFlags);
		}
	
		uint32
		GetEnabled		(		)
		{
			return NvShaderBase_Mesh::GetEnabled();
		}

		bool
		HideSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Mesh::HideSurface (inSurfaceIndex	);
		}

		bool
		ShowSurface			(	uint			inSurfaceIndex	)
		{
			return NvShaderBase_Mesh::ShowSurface (inSurfaceIndex	);
		}

		bool
		HideAllSurfaces		(									)
		{
			return NvShaderBase_Mesh::HideAllSurfaces();
		}

		bool
		ShowAllSurfaces		(									)
		{
			return NvShaderBase_Mesh::ShowAllSurfaces();
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return NvShaderBase_Mesh::GetDisplayState(inIndex);
		}

		// Mapping generation

		bool	SetMappingSrc	(	NvMappingShader::MappingSrc inSource			)
		{
			if (inSource == NvMappingShader::MS_NORMAL && (!(dpycaps & DpyState_PC::CF_HAS_NORMAL)))
				return FALSE;

			if (inSource == NvMappingShader::MS_TEX    && (!(dpycaps & DpyState_PC::CF_HAS_TEX0)))
				return FALSE;

			if (inSource == NvMappingShader::MS_LOC)
				mapVSH.select = Vec4(1.0,0.0,0.0,0.0); 
			else if (inSource == NvMappingShader::MS_NORMAL)
				mapVSH.select = Vec4(0.0,1.0,0.0,0.0); 
			else if (inSource == NvMappingShader::MS_TEX)
				mapVSH.select = Vec4(0.0,0.0,1.0,0.0); 

			return TRUE;
		}

		bool	SetMappingTR	(	Matrix*			inTransfo			)
		{
			if (! inTransfo)	return FALSE;

			MatrixCopy(&mapVSH.uvTR,inTransfo);
			return TRUE;
		}
		
		bool	SetColorFilter	(	Vec4*			inColor					)
		{
			if (inColor)	return FALSE;

			Vec4Copy(&mapPSH.colorFilter ,inColor);
			return TRUE;	
		}

		bool
		Draw		(				)
		{
			return NvShaderBase_Mesh::Draw(dpyctxt);
		}

		void			
		SetShader			(		)
		{ 
			Hrdw::SetShaderProgram		(HLSLPS);
			Hrdw::SetShaderProgram		(HLSLVS);			
		}

		void
		SetObjectConstant	(	int inChainIdx	)
		{
			if (inChainIdx < 0 ) return ;

			NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data() + 
												(DpyManager::context->contextChainA.data() + inChainIdx)->contextIdx;
			NvDpyContext::View*			dview  = DpyManager::context->viewA.data()    + dctxt->viewIdx;
			Matrix*						dwtr   = DpyManager::context->wtrA.data()     + dctxt->wtrIdx;			

			Matrix toProjTR;
			MatrixMul(&toProjTR,&dview->viewTR,&dview->projTR);
			MatrixMul(&toProjTR,&toProjTR,&DpyManager::clipMatrix);

			// TR
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;
			if (!mode2D) {
				MatrixMul( &mapVSH.vpm, dwtr, &toProjTR );
			}
			else {
				Matrix proj2D;
				MatrixZero(&proj2D);
				proj2D.m11 =	2.0f / dview->viewport.z ;
				proj2D.m22 =	-2.0f / dview->viewport.w ;
				proj2D.m33 =	1.0f;				
				proj2D.m41 =	-1.0f;
				proj2D.m42 =	1.0f;
				proj2D.m44 =	1.0f;
				MatrixMul( &mapVSH.vpm, dwtr, &proj2D );
			}
			
			//Hrdw::UpdateVertexMapping(& mapVSH,&(mapVSH.vpm),4);
			//Hrdw::UpdateVertexMapping	(&mapVSH,&mapVSH.uvTR);
			//Hrdw::FullUpdatePixelMapping(&mapPSH);
			Hrdw::FullUpdateVertexMapping(&mapVSH);
			Hrdw::FullUpdatePixelMapping(&mapPSH);	
		}

		bool
		SetSurfaceConstant	(	Surface &		inSurf	)
		{
			inSurf.dpystate.Activate( );
			bool textured = inSurf.dpystate.IsTexturing();
			if ( (textured  && (mapPSH.haveTexture!=1.0f)) ||
				 (!textured && (mapPSH.haveTexture)) ) {
				mapPSH.haveTexture = textured?1.0f:0.0f;
				Hrdw::UpdatePixelMapping(&mapPSH,&mapPSH.haveTexture,1);
			}				
			return TRUE;
		}

	};

}




NvMappingShader* 
nv_mappingshader_mesh_Create	(	NvMesh*		inMesh	)
{
	if( !inMesh )
		return NULL;

	uint surfCpt = inMesh->GetSurfaceCpt();
	uint supply = ( sizeof(Shader::Surface) ) * surfCpt ;
	Shader* shader = NvEngineNewS( Shader, supply );
	if( !shader )
		return NULL;

	shader->surfaceCpt =  surfCpt;
	shader->surfaces   = (Shader::Surface*)(shader+1);

	shader->_Init( inMesh );
	return shader->GetInterface();

}


