/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PC/NvDpyObject[PC].h>
#include <Kernel/PC/NvkMesh[PC].h>
#include <Kernel/PC/NvHrdwTools.h>
#include <NvShadowingShader.h>
using namespace nv;


//#define WITHOUT_CORRECTION
//#define ZFAIL_METHOD // Carmack !

namespace {
	struct HLSLVSMapping	{
		Matrix 	vpm;			// Transformation Matrix View and Projection		
	};
	struct HLSLVSMappingDBG	{
		Matrix 	vpm;			// Transformation Matrix View and Projection		
		Vec4	color;
	};
	struct HLSLVSMappingCorrection {
		Matrix 	vpm;			// Transformation Matrix View and Projection		
		Vec3	lightInvDir;
		float	pad1;
	};
	Hrdw::ShaderProgram			HLSLVS;
	Hrdw::ShaderProgram			HLSLPS;
	Hrdw::ShaderProgram			HLSLVSDBG;
	Hrdw::ShaderProgram			HLSLPSDBG;
	Hrdw::ShaderProgram			HLSLVSCorrectionPointLight;
	Hrdw::ShaderProgram			HLSLVSCorrectionDirLight;
	Hrdw::ShaderProgram			HLSLPSCorrection;
	
	Hrdw::DeclIndex				VertexDeclaration;
	Hrdw::DeclIndex				VertexDeclarationCorrection;

/*	void *						tmpMemory		= NULL;
	uint						tmpMemorySize	= 0;
	uint						tmpMemoryAcces	= 0;
*/
	uint						globalSharedCpt = 0;
	sysvector<char>				faceVisA;
	sysvector<char>				vtxVisA;

	char GeomShaderCodeVHL[] = {
		#include "ShadowingShader.vhl.bin2h"
	};
	char GeomShaderCodePHL[] = {
		#include "ShadowingShader.phl.bin2h"
	};
	char GeomShaderCodeVHLDBG[] = {
		#include "ShadowingShaderDBG.vhl.bin2h"
	};
	char GeomShaderCodePHLDBG[] = {
		#include "ShadowingShaderDBG.phl.bin2h"
	};
	char GeomShaderCodeVHLCorrectionPointLight[] = {
		#include "ShadowingShaderCorrectionPointLight.vhl.bin2h"
	};
	char GeomShaderCodeVHLCorrectionDirLight[] = {
		#include "ShadowingShaderCorrectionDirLight.vhl.bin2h"
	};
	char GeomShaderCodePHLCorrection[] = {
		#include "ShadowingShaderCorrection.phl.bin2h"
	};

	void _InitVS_Decl(){
		static bool init = false;
		if (init) return;
		init = true;

		HLSLVS						= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, GeomShaderCodeVHL,sizeof(GeomShaderCodeVHL),Hrdw::NV_VS_2, "main");
		HLSLPS						= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, GeomShaderCodePHL,sizeof(GeomShaderCodePHL),Hrdw::NV_PS_2, "main");
		HLSLVSDBG					= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, GeomShaderCodeVHLDBG,sizeof(GeomShaderCodeVHLDBG),Hrdw::NV_VS_2, "main");
		HLSLPSDBG					= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, GeomShaderCodePHLDBG,sizeof(GeomShaderCodePHLDBG),Hrdw::NV_PS_2, "main");
		HLSLVSCorrectionDirLight	= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, GeomShaderCodeVHLCorrectionDirLight,sizeof(GeomShaderCodeVHLCorrectionDirLight),Hrdw::NV_VS_2, "main");
		HLSLVSCorrectionPointLight	= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, GeomShaderCodeVHLCorrectionPointLight,sizeof(GeomShaderCodeVHLCorrectionPointLight),Hrdw::NV_VS_2, "main");
		HLSLPSCorrection			= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, GeomShaderCodePHLCorrection,sizeof(GeomShaderCodePHLCorrection),Hrdw::NV_PS_2, "main");

		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVS,HLSLVSMapping									,vpm		,"vpm"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSDBG,HLSLVSMappingDBG							,vpm		,"vpm"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSDBG,HLSLVSMappingDBG							,color		,"color"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCorrectionDirLight,HLSLVSMappingCorrection		,vpm		,"vpm"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCorrectionPointLight,HLSLVSMappingCorrection	,vpm		,"vpm"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCorrectionDirLight,HLSLVSMappingCorrection		,lightInvDir,"lightInvDir"));
		NV_ASSERT (Hrdw_CheckMappingByName(HLSLVSCorrectionPointLight,HLSLVSMappingCorrection	,lightInvDir,"lightPos"));

		Hrdw::BeginVertexDeclaration();
		Hrdw::AddToVertexDeclaration(0,0,Hrdw::NV_FLOAT4,Hrdw::NV_POSITION);		
		VertexDeclaration = Hrdw::FinalizeVertexDeclaration();

		Hrdw::BeginVertexDeclaration();
		Hrdw::AddToVertexDeclaration(0,0 ,Hrdw::NV_FLOAT3,Hrdw::NV_POSITION);		
		Hrdw::AddToVertexDeclaration(0,12,Hrdw::NV_FLOAT3,Hrdw::NV_NORMAL);		
		VertexDeclarationCorrection	= Hrdw::FinalizeVertexDeclaration();

	}
}


//
// BASE


struct NvShadowingShaderBase : public NvDpyObject_PC
{
	enum {
		UPD_PROCESS		= (1<<0),
	};

	NvShadowingShader			itf;
	uint16						updflags;					// to update flags (UPD_...)
	NvkMesh*					mesh;
	
	NvShadowingShader::Type		type;
	Vec3						source;
	float						offsetLength;
	float						extrudeLength;
	uint32						shadowSubRGBA;
	uint						doflags;
	
	Hrdw::VertexBuffer			shadowingVB;
	Hrdw::VertexBuffer			limitFaceVB;		

	HLSLVSMapping				mapVSH;
	HLSLVSMappingDBG			mapVSHDBG;
	HLSLVSMappingCorrection		mapVSHCorrec;
	Hrdw::tools::VsCopyMapping	vsCopyMapping;

	virtual	~	NvShadowingShaderBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

	void
	_Init		(	NvkMesh*	inKMesh	)
	{
		_InitVS_Decl();
		InitDpyObject();		
		mesh = inKMesh;
		mesh->AddRef();

		type			= NvShadowingShader::T_DIRECTIONAL;
		source			= Vec3( 0, 0, -1 );
		offsetLength	= 0;
		extrudeLength	= 0;
		shadowSubRGBA	= 0x40404040;
		doflags			= NvShadowingShader::F_DEFAULT;
		updflags		= 0;
		shadowingVB		= 0;

		uint					vtxCpt;
		uint					faceCpt;
		NvkMesh::ShadowingVtx*	vtxA;
		NvkMesh::ShadowingFace*	faceA;
		mesh->GetShadowing(	vtxCpt, faceCpt, vtxA, faceA );		
		// Can be reduce ?????
		limitFaceVB	= Hrdw::CreateVertexBuffer((sizeof(Vec3)+sizeof(Vec3)) * (faceCpt*3) , Hrdw::NV_DYNAMIC );
		shadowingVB = Hrdw::CreateVertexBuffer( sizeof(Vec4) * 8 * 3 * faceCpt , Hrdw::NV_DYNAMIC  );
		 
		if (! globalSharedCpt) {
			faceVisA.Init();
			vtxVisA.Init();			
		}
		globalSharedCpt ++;
	}

	void
	Release			(		)
	{
		if( ShutDpyObject(&itf )) {
			globalSharedCpt --;
			if (!globalSharedCpt) {
				faceVisA.Shut();
				vtxVisA.Shut();
			}
			SafeRelease( mesh );
			Hrdw::ReleaseVertexBuffer(shadowingVB);
			Hrdw::ReleaseVertexBuffer(limitFaceVB);
			NvEngineDelete( this );
		}
	}

	void
	AddRef		(		)
	{
		refCpt++;
	}

	uint
	GetRefCpt	(		)
	{
		return refCpt;
	}
	
	void
	Enable		(	uint32		inEnableFlags	)
	{
		//
	}

	void
	Disable		(	uint32		inEnableFlags	)
	{
		//
	}

	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
		//
	}

	uint32
	GetEnabled		(		)
	{
		return 0;
	}

	NvResource*
	GetResource		(		)
	{
		return mesh->GetInterface();
	}

	bool
	Setup			(	Vec3&			inTopLocation,
						float			inExtrudeLength		)
	{
		return FALSE;
	}

	bool
	Setup			(	NvShadowingShader::Type		inType,
						const Vec3&					inSource,
						float						inOffsetLength,
						float						inExtrudeLength		)
	{
		type			= inType;
		source			= inSource;
		offsetLength	= inOffsetLength;
		extrudeLength	= inExtrudeLength;
		if( type == NvShadowingShader::T_DIRECTIONAL )
			Vec3Normalize( &source, &source );
		updflags		= UPD_PROCESS;
		return TRUE;
	}
	
	void
	SetColor		(	uint32		inRGBA	)
	{
		shadowSubRGBA	= inRGBA;
		updflags		= UPD_PROCESS;
	}

	void
	SetFlags		(	uint		inFlags	)
	{
		doflags		= inFlags;
		updflags	= UPD_PROCESS;
	}

	uint
	GetFlags		(				)
	{
		return doflags;
	}

	bool
	Draw		(			)
	{
		uint					vtxCpt;
		uint					faceCpt;
		NvkMesh::ShadowingVtx*	vtxA;
		NvkMesh::ShadowingFace*	faceA;
		mesh->GetShadowing		(vtxCpt, faceCpt, vtxA, faceA );

		NV_ASSERT_RETURN_MTH	(FALSE, vtxCpt && faceCpt );
		NV_ASSERT_RETURN_MTH	(FALSE, vtxA && faceA );

		// ctxt
		NV_ASSERT_RETURN_MTH		(FALSE, dpyctxt.chainHead >= 0 );
		NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + dpyctxt.chainHead;
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
		Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;

		uint stencilBpp;
		Hrdw::GetRasterFormat( DpyManager::hwraster[dctxt->raster], NULL,NULL,&stencilBpp);
		NV_ASSERT_RETURN_MTH(FALSE, stencilBpp > 2 );
		
		int		nbFace			= 0;
		uint	nbCorretedFace	= 0;		

		//
		// volume-processing
		if(	 extrudeLength>0 &&
			(doflags & (NvShadowingShader::F_CAP_SIDES|NvShadowingShader::F_CAP_FRONT|NvShadowingShader::F_CAP_BACK)) )
		{
			Matrix wtr;
			Matrix iwtr;
			Matrix projM;
			MatrixNormalize( &wtr, dwtr );
			MatrixFastInverse( &iwtr, &wtr );

			Matrix toProjTR;
			MatrixMul(&toProjTR,&dview->viewTR,&dview->projTR);
			MatrixMul(&toProjTR,&toProjTR,&DpyManager::clipMatrix);

			MatrixMul (&mapVSH.vpm	, dwtr, &toProjTR );

			Vec4 cl[16];
			Vec4 ol[3];
			Vec4 el[3];			
			
			vtxVisA.resize(vtxCpt);
			faceVisA.resize(faceCpt);

			// Light dir/pos in object world
			Vec3 lightDir, lightPos;
			if( type == NvShadowingShader::T_DIRECTIONAL ) {
				Vec3ApplyVector( &lightDir, &source, &iwtr );
				for (uint i = 0 ; i < vtxCpt ; ++i) {
					vtxVisA[i]	= (vtxA[i].normal*lightDir)<0.0f ? 1 : 0;
				}
				mapVSHCorrec.lightInvDir = -lightDir;
			} else {
				Vec3Apply( &lightPos, &source, &iwtr );
				for (uint i = 0 ; i < vtxCpt ; ++i) {
					Vec3 toLight = lightPos - vtxA[i].location;
					vtxVisA[i]	 = (vtxA[i].normal*toLight)>=0.0f ? 1 : 0;
				}
				mapVSHCorrec.lightInvDir = lightPos;
			}

			// Check face visibility and Create Vertex buffer with face that may be correted. 
			Vec3 *	limitFace	= (Vec3 *)Hrdw::LockVertexBuffer(limitFaceVB,0,0,Hrdw::NV_DISCARD);						
			for( uint i = 0 ; i < faceCpt ; i++ ) {								
				faceVisA[i] = vtxVisA[faceA[i].vtx[0]] | (vtxVisA[faceA[i].vtx[1]] << 1) | (vtxVisA[faceA[i].vtx[2]] <<2);
#ifndef WITHOUT_CORRECTION
				if (faceVisA[i] != 0 && faceVisA[i] !=7) {
					// copy Position and Normal 
					Memcpy(limitFace  ,&vtxA[ faceA[i].vtx[0] ].location,2 * sizeof (Vec3));
					Memcpy(limitFace+2,&vtxA[ faceA[i].vtx[1] ].location,2 * sizeof (Vec3));
					Memcpy(limitFace+4,&vtxA[ faceA[i].vtx[2] ].location,2 * sizeof (Vec3));
					limitFace+=6;
					nbCorretedFace++;
				}
				faceVisA[i] = faceVisA[i]!=0;
#else 
				faceVisA[i] = faceVisA[i]==7;
#endif
			}

			Hrdw::UnlockVertexBuffer(limitFaceVB);

			Vec4 * shadowingFace = (Vec4 *)Hrdw::LockVertexBuffer(shadowingVB,0,0,Hrdw::NV_DISCARD);						
			// capping
			int edgeNoSkip = doflags&(NvShadowingShader::F_CAP_FRONT|NvShadowingShader::F_CAP_BACK) ? 1 : 0;	
			for( uint i = 0 ; i < faceCpt ; i++ )
			{
				if( faceVisA[i] )	continue;
				NV_ASSERT( faceA[i].adj[0]>=0 && faceA[i].adj[0]<int(faceCpt) );
				NV_ASSERT( faceA[i].adj[1]>=0 && faceA[i].adj[1]<int(faceCpt) );
				NV_ASSERT( faceA[i].adj[2]>=0 && faceA[i].adj[2]<int(faceCpt) );

				// check if edges cast shadow ?
				int edgeCast = 0;
				edgeCast |= faceVisA[ faceA[i].adj[0] ] << 0;
				edgeCast |= faceVisA[ faceA[i].adj[1] ] << 1;
				edgeCast |= faceVisA[ faceA[i].adj[2] ] << 2;
				if( edgeCast == 0 && edgeNoSkip == 0) 	continue;

				// extrude vertices
				if( type == NvShadowingShader::T_DIRECTIONAL )
				{
					ALIGNED128( Vec3, l );
					for( int k = 0 ; k < 3 ; k++ ) {
						l = vtxA[ faceA[i].vtx[k] ].location;
						ol[k] = Vec4( l + lightDir * offsetLength  );
						el[k] = Vec4( l + lightDir * extrudeLength );
					}
				}
				else
				{
					ALIGNED128( Vec3, l );
					ALIGNED128( Vec3, d );
					for( int k = 0 ; k < 3 ; k++ ) {
						l = vtxA[ faceA[i].vtx[k] ].location;
						d = l - lightPos;
						Vec3Normalize( &d, &d );
						ol[k] = Vec4( l + d * offsetLength  );
						el[k] = Vec4( l + d * extrudeLength );
					}
				}

				// front cap
				if( doflags & NvShadowingShader::F_CAP_FRONT ) {
					Vec4Copy(shadowingFace,ol);	Vec4Copy(shadowingFace+1,ol+2);		Vec4Copy(shadowingFace+2,ol+1);
					shadowingFace+=3;
					nbFace+=1;

					/*Vec4Copy(shadowingFace,ol);	Vec4Copy(shadowingFace+1,ol+1);		Vec4Copy(shadowingFace+2,ol+2);
					shadowingFace+=3;
					nbFace+=1;
					Vec4Copy(shadowingFace,ol+1);	Vec4Copy(shadowingFace+1,ol+2);		Vec4Copy(shadowingFace+2,ol+0);
					shadowingFace+=3;
					nbFace+=1;
					Vec4Copy(shadowingFace,ol+2);	Vec4Copy(shadowingFace+1,ol+0);		Vec4Copy(shadowingFace+2,ol+1);
					shadowingFace+=3;
					nbFace+=1;*/
				}
				// back cap
				if( doflags & NvShadowingShader::F_CAP_BACK ) {
					Vec4Copy(shadowingFace,el);		Vec4Copy(shadowingFace+1,el+1);		Vec4Copy(shadowingFace+2,el+2);
					shadowingFace+=3;
					nbFace+=1;
				}
				// side caps
				if( edgeCast && (doflags & NvShadowingShader::F_CAP_SIDES) ) {
					if( (edgeCast & 1) ) {
						Vec4Copy(shadowingFace,ol);	Vec4Copy(shadowingFace+1,ol+1); Vec4Copy(shadowingFace+2,el+1);
						Vec4Copy(shadowingFace+3,ol);	Vec4Copy(shadowingFace+4,el+1); Vec4Copy(shadowingFace+5,el);
						shadowingFace+=6;
						nbFace+=2;
					}
					if( (edgeCast & 2) ) {
						Vec4Copy(shadowingFace,ol+1);	Vec4Copy(shadowingFace+1,ol+2); Vec4Copy(shadowingFace+2,el+2);
						Vec4Copy(shadowingFace+3,ol+1);	Vec4Copy(shadowingFace+4,el+2); Vec4Copy(shadowingFace+5,el+1);						
						shadowingFace+=6;
						nbFace+=2;
					}
					if( (edgeCast & 4) ) {
						Vec4Copy(shadowingFace,ol+2);	Vec4Copy(shadowingFace+1,ol); Vec4Copy(shadowingFace+2,el);
						Vec4Copy(shadowingFace+3,ol+2);	Vec4Copy(shadowingFace+4,el); Vec4Copy(shadowingFace+5,el+2);
						shadowingFace+=6;
						nbFace+=2;
					}
				}
			}
			Hrdw::UnlockVertexBuffer(shadowingVB);
		}
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//------- Render Process : Clear stencil, corrected face, shadow volume , apply shadow --------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
		Hrdw::SetRaster				(DpyManager::hwraster[dctxt->raster],FALSE,FALSE );
		Hrdw::SetViewPort			(dview->viewport.x, dview->viewport.y, dview->viewport.z, dview->viewport.w, 0.0f, 1.0f);
//---------------------------------------------------------------------------------------------
		// clear stencil :
		if( doflags & NvShadowingShader::F_CLEAR_STENCIL ) {
			Hrdw::ClearRaster(Hrdw::NV_CLEAR_STENCIL,0,0);
		}
//---------------------------------------------------------------------------------------------		
		Hrdw::SetRenderState			(Hrdw::NV_ZWRITEENABLE		,FALSE);
		Hrdw::SetRenderState			(Hrdw::NV_ALPHABLENDENABLE	,FALSE);			
		Hrdw::SetRenderState			(Hrdw::NV_COLORWRITEENABLE	,Hrdw::NV_COLORWRITEENABLE_ALPHA);				
		Hrdw::SetRenderState			(Hrdw::NV_ALPHATESTENABLE	,FALSE);			
		Hrdw::SetRenderState			(Hrdw::NV_STENCILENABLE		,TRUE);
		Hrdw::SetRenderState			(Hrdw::NV_STENCILREF		,0);
		Hrdw::SetRenderState			(Hrdw::NV_STENCILFUNC		,Hrdw::NV_CMP_ALWAYS);
		Hrdw::SetRenderState			(Hrdw::NV_STENCILZFAIL		,Hrdw::NV_STENCILOP_KEEP);
		Hrdw::SetRenderState			(Hrdw::NV_STENCILFAIL		,Hrdw::NV_STENCILOP_KEEP);
		Hrdw::SetRenderState			(Hrdw::NV_STENCILPASS		,Hrdw::NV_STENCILOP_INCR);
		Hrdw::SetRenderState			(Hrdw::NV_ZENABLE			,TRUE);
		Hrdw::SetRenderState			(Hrdw::NV_ZFUNC				,Hrdw::NV_CMP_LESSEQUAL);
		Hrdw::SetRenderState			(Hrdw::NV_CULLMODE			,Hrdw::NV_CULL_CCW);
		Hrdw::FullUpdateVertexMapping	(&mapVSH);
//---------------------------------------------------------------------------------------------
		// corrected face :
#ifndef WITHOUT_CORRECTION
		if (nbCorretedFace ) {
			//Hrdw::SetRenderState			( Hrdw::NV_DEPTHBIAS,2 );
			Hrdw::SetVertexDeclaration		(VertexDeclarationCorrection);
			Hrdw::SetVertexBuffer			(limitFaceVB, 0, 0 ,24);
			Hrdw::UpdateVertexMapping		(&mapVSHCorrec,&mapVSHCorrec.lightInvDir);
			if (type == NvShadowingShader::T_DIRECTIONAL)
				Hrdw::SetShaderProgram			(HLSLVSCorrectionDirLight);
			else
				Hrdw::SetShaderProgram			(HLSLVSCorrectionPointLight);
			Hrdw::SetShaderProgram			(HLSLPSCorrection);
			Hrdw::DrawPrimitive				(Hrdw::NV_TRIANGLELIST,nbCorretedFace,0);
			//Hrdw::SetRenderState( Hrdw::NV_DEPTHBIAS,0 );
		}
#endif 
//---------------------------------------------------------------------------------------------		
		// Shadow volume :
		if (nbFace ) {
			// debug show volumes ?
			if( doflags & NvShadowingShader::F_DBG_SHOW_VOLUMES ){
				Hrdw::SetRenderState	(Hrdw::NV_COLORWRITEENABLE	,0xFFFFFFFF);
				//Hrdw::SetRenderState	(Hrdw::NV_ALPHABLENDENABLE	,TRUE);
				//Hrdw::SetRenderState	(Hrdw::NV_SRCBLEND			,Hrdw::NV_BLEND_SRCALPHA);
				//Hrdw::SetRenderState	(Hrdw::NV_DESTBLEND			,Hrdw::NV_BLEND_INVSRCALPHA);
				//Hrdw::SetRenderState	(Hrdw::NV_BLENDOP			,Hrdw::NV_BLENDOPALPHA);
				Hrdw::SetShaderProgram	(HLSLVSDBG);
				Hrdw::SetShaderProgram	(HLSLPSDBG);
			}
			else{	
				Hrdw::SetRenderState	(Hrdw::NV_COLORWRITEENABLE	,0x0);
				Hrdw::SetShaderProgram	(HLSLVS);
				Hrdw::SetShaderProgram	(HLSLPS);					
			}
			//Hrdw::SetRenderState		(Hrdw::NV_DEPTHBIAS	,16);
			Hrdw::SetVertexDeclaration	(VertexDeclaration);
			Hrdw::SetVertexBuffer		(shadowingVB, 0, 0 ,16);			

			Hrdw::SetRenderState		(Hrdw::NV_ZFUNC				,Hrdw::NV_CMP_LESS);
#ifdef ZFAIL_METHOD			
			Hrdw::SetRenderState		(Hrdw::NV_STENCILPASS		,Hrdw::NV_STENCILOP_KEEP);
#endif 		
			// Render Back Face
			Hrdw::SetRenderState(Hrdw::NV_CULLMODE,Hrdw::NV_CULL_CW);
			if( doflags & NvShadowingShader::F_DBG_SHOW_VOLUMES ){
				mapVSHDBG.color = Vec4(1.0,0.0,0.0,0.5);
				Hrdw::UpdateVertexMapping(&mapVSHDBG,&mapVSHDBG.color);
			}
#ifdef ZFAIL_METHOD
			Hrdw::SetRenderState			(Hrdw::NV_STENCILZFAIL		,Hrdw::NV_STENCILOP_INCR);
#else 
			Hrdw::SetRenderState			(Hrdw::NV_STENCILPASS		,Hrdw::NV_STENCILOP_DECR);
#endif
			Hrdw::DrawPrimitive	(Hrdw::NV_TRIANGLELIST,nbFace,0);
			
			// Render Front Face
			Hrdw::SetRenderState	(Hrdw::NV_CULLMODE,Hrdw::NV_CULL_CCW);
			if( doflags & NvShadowingShader::F_DBG_SHOW_VOLUMES ) {
				mapVSHDBG.color = Vec4(0.0,1.0,0.0,0.5);		
				Hrdw::UpdateVertexMapping(&mapVSHDBG,&mapVSHDBG.color);
			}
#ifdef ZFAIL_METHOD
			Hrdw::SetRenderState			(Hrdw::NV_STENCILZFAIL		,Hrdw::NV_STENCILOP_DECR);
#else 
			Hrdw::SetRenderState			(Hrdw::NV_STENCILPASS		,Hrdw::NV_STENCILOP_INCR);
#endif	
			Hrdw::DrawPrimitive(Hrdw::NV_TRIANGLELIST,nbFace,0);
		}
//---------------------------------------------------------------------------------------------
		//
		// post-processing
		if(		doflags & NvShadowingShader::F_BACK_STENCIL
			||	doflags & NvShadowingShader::F_DRAW_SHADOWS	){
		}
		if( doflags & NvShadowingShader::F_DRAW_SHADOWS )
		{				
			Hrdw::SetRenderState(Hrdw::NV_ALPHABLENDENABLE	,TRUE);
			Hrdw::SetRenderState(Hrdw::NV_SRCBLEND			,Hrdw::NV_BLEND_ONE);
			Hrdw::SetRenderState(Hrdw::NV_DESTBLEND			,Hrdw::NV_BLEND_ONE);
			Hrdw::SetRenderState(Hrdw::NV_BLENDOP			,Hrdw::NV_BLENDOP_REVSUBTRACT);
			Hrdw::SetRenderState(Hrdw::NV_COLORWRITEENABLE	,0xFFFFFFFF);				
			Hrdw::SetRenderState(Hrdw::NV_ZENABLE			,FALSE);
			Hrdw::SetRenderState(Hrdw::NV_STENCILREF		,0);
			Hrdw::SetRenderState(Hrdw::NV_STENCILFUNC		,Hrdw::NV_CMP_NOTEQUAL);
			Hrdw::SetRenderState(Hrdw::NV_STENCILFAIL		,Hrdw::NV_STENCILOP_KEEP);
			Hrdw::SetRenderState(Hrdw::NV_STENCILZFAIL		,Hrdw::NV_STENCILOP_KEEP);
			Hrdw::SetRenderState(Hrdw::NV_STENCILPASS		,Hrdw::NV_STENCILOP_KEEP);

			Hrdw::tools::SetShader(FALSE,TRUE,TRUE);
			Hrdw::tools::SetVertexBuffer(FALSE);
			//shadowSubRGBA
			vsCopyMapping.color = Vec4(	(shadowSubRGBA>>24	)		/ 255.0,
										((shadowSubRGBA>>16	)&0xFF)	/ 255.0,
										((shadowSubRGBA>>8	)&0xFF)	/ 255.0,
										((shadowSubRGBA		)&0xFF)	/ 255.0);
			Hrdw::FullUpdateVertexMapping(&vsCopyMapping);	
			Hrdw::tools::DrawSprite();
		}
//---------------------------------------------------------------------------------------------
		// Restore context :
		Hrdw::SetRenderState	(Hrdw::NV_ZENABLE			,TRUE);
		Hrdw::SetRenderState	(Hrdw::NV_ZWRITEENABLE		,TRUE);
		Hrdw::SetRenderState	(Hrdw::NV_STENCILENABLE		,FALSE);
		Hrdw::SetRenderState	(Hrdw::NV_ALPHABLENDENABLE	,FALSE);
		Hrdw::SetRenderState	(Hrdw::NV_ALPHATESTENABLE	,TRUE);
		Hrdw::SetRenderState	(Hrdw::NV_COLORWRITEENABLE	,0xFFFFFFFF);
		//Hrdw::SetRenderState		(Hrdw::NV_DEPTHBIAS	,0);
		return TRUE;
	}
};





NvShadowingShader*
NvShadowingShader::Create	(	NvResource*		inRsc		)
{
	if( !inRsc || inRsc->GetRscType() != NvMesh::TYPE )
		return NULL;

	NvkMesh* kmesh = ((NvMesh*)inRsc)->GetKInterface();
	if( !kmesh || !kmesh->IsShadowing() )
		return NULL;

	NvShadowingShaderBase* shader = NULL;
	uint supply = 0;
	shader = NvEngineNewS( NvShadowingShaderBase, supply );
	if( !shader )
		return NULL;

	shader->_Init( kmesh );

	return &shader->itf;
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( ShadowingShader,	NvInterface*,	GetBase										)
NVITF_CAL0( ShadowingShader,	void,			AddRef										)
NVITF_MTH0( ShadowingShader,	uint,			GetRefCpt									)
NVITF_CAL0( ShadowingShader,	void,			Release										)
NVITF_MTH0( ShadowingShader,	NvResource*,	GetResource									)
NVITF_CAL1( ShadowingShader,	void,			Enable,				uint32					)
NVITF_CAL1( ShadowingShader,	void,			Disable,			uint32					)
NVITF_CAL1( ShadowingShader,	void,			SetEnabled,			uint32					)
NVITF_MTH0( ShadowingShader,	uint32,			GetEnabled									)
NVITF_MTH4( ShadowingShader,	bool,			Setup,				Type, const Vec3&, float, float	)
NVITF_MTH1( ShadowingShader,	void,			SetColor,			uint32					)
NVITF_CAL1( ShadowingShader,	void,			SetFlags,			uint					)
NVITF_MTH0( ShadowingShader,	uint,			GetFlags									)



