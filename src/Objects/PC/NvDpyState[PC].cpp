/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvDpyState[PC].h>
#include <Kernel/PC/NvHrdw.h>
using namespace nv;

//#define	TEST_UV_OFFSET

namespace
{

	inline uint	_UpdateMode	(	uint	inMode,
								uint	inMask,
								uint	inFlags	)
	{
		uint f = inFlags & inMask;
		return f ? (inMode&(~inMask))|f : inMode;
	}
}

void
DpyState_PC::Init	(		)
{
	caps			= 0;
	enableOp		= (uint8)EN_DEFAULT;
	enableOpMask	= EN_ALL_MASK;
	mode			= MODE_DEFAULT;
	src				= DpySource();
	Vec2Zero( &srcXY );
	srcBmp			= NULL;
	alphaPass		= 0.02f;
	blendCte		= 0.5f;
	wrColorMask		= 0xFFFFFFFF;
	changed			= TRUE;
	textureIndex	= 0;
}


void
DpyState_PC::Shut		(			)
{
	SafeRelease( srcBmp );
}


void
DpyState_PC::Enable	(	uint		inEnableFlags		)
{
	SetEnabled( enableOp | inEnableFlags );
}


void
DpyState_PC::Disable	(	uint		inEnableFlags		)
{
	SetEnabled( enableOp & (~inEnableFlags) );
}


void
DpyState_PC::SetEnabled	(	uint	inEnableFlags		)
{
	uint old = (enableOp & enableOpMask);
	enableOp = (inEnableFlags & EN_ALL_MASK);
	uint cur = (enableOp & enableOpMask);
	if( cur != old )
		changed = TRUE;
}


uint
DpyState_PC::GetEnabled	(					)
{
	return enableOp;
}


void
DpyState_PC::SetEnabledMask	(	uint		inEnableFlags		)
{
	uint old = (enableOp & enableOpMask);
	enableOpMask = (inEnableFlags & EN_ALL_MASK);
	uint cur = (enableOp & enableOpMask);
	if( cur != old )
		changed = TRUE;
}


uint
DpyState_PC::GetEnabledMask	(				)
{
	return enableOpMask;
}


uint
DpyState_PC::GetGlobalEnabled	(				)
{
	return ( enableOp & enableOpMask );
}


void
DpyState_PC::SetCapabilities		(	uint		inCapabilities		)
{
	if( caps != inCapabilities ) {
		caps = inCapabilities;
		changed = TRUE;
	}
}


uint
DpyState_PC::GetCapabilities		(									)
{
	return caps;
}


void
DpyState_PC::SetMode		(	uint		inModeFlags	)
{
	uint m = _UpdateMode( mode, TF_MASK, inModeFlags );
		 m = _UpdateMode( m,    TM_MASK, inModeFlags );
		 m = _UpdateMode( m,	TR_MASK, inModeFlags );
		 m = _UpdateMode( m,    TW_MASK, inModeFlags );
		 m = _UpdateMode( m,    CM_MASK, inModeFlags );
		 m = _UpdateMode( m,    BM_MASK, inModeFlags );
		 m = _UpdateMode( m,    BS_MASK, inModeFlags );
	if( m != mode ) {
		changed = TRUE;
		mode = m;
	}
}


uint
DpyState_PC::GetMode		(							)
{
	return mode;
}


bool
DpyState_PC::SetSource	(	DpySource	inSource		)
{
	if( src == inSource )
		return TRUE;
	src = inSource;

	NvkBitmap* kbmp = NULL;
	if( src.Bitmap )
		kbmp = src.Bitmap->GetKInterface();
	SafeGetRef( srcBmp, kbmp );

	changed = TRUE;
	return TRUE;
}


void
DpyState_PC::GetSource		(	DpySource&	outSource		)
{
	outSource = src;
}


void
DpyState_PC::SetSourceOffset	(	const Vec2&	inOffsetXY		)
{
	if( srcXY != inOffsetXY ) {
		srcXY = inOffsetXY;
		changed = TRUE;
	}
}


void
DpyState_PC::GetSourceOffset	(	Vec2&		outOffsetXY		)
{
	Vec2Copy( &outOffsetXY, &srcXY );
}


bool
DpyState_PC::SetAlphaPass	(	float		inAlphaPass		)
{
	inAlphaPass = Clamp( inAlphaPass, 0.0f, 1.0f );
	if( inAlphaPass != alphaPass ) {
		alphaPass = inAlphaPass;
		changed = TRUE;
	}
	return TRUE;
}


float
DpyState_PC::GetAlphaPass	(				)
{
	return alphaPass;
}


bool
DpyState_PC::SetBlendSrcCte	(	float		inCte	)
{
	inCte = Clamp( inCte, 0.0f, 1.0f );
	if( inCte != blendCte ) {
		blendCte = inCte;
		changed = TRUE;
	}
	return TRUE;
}


float
DpyState_PC::GetBlendSrcCte	(			)
{
	return blendCte;
}


void
DpyState_PC::SetWRColorMask	(	uint		inRGBAMask		)
{
	if( wrColorMask != inRGBAMask ) {
		wrColorMask = inRGBAMask;
		changed = TRUE;
	}
}


uint32
DpyState_PC::GetWRColorMask	(				)
{
	return wrColorMask;
}


void
DpyState_PC::SetTextureIndex (	uint32 inTI )
{
	textureIndex = inTI;
}


bool
DpyState_PC::HasChanged		(			)
{
	return changed;
}


bool
DpyState_PC::HasValidSource	(		)
{
	return ( srcBmp || src.Raster!=DPYR_NULL );
}


bool
DpyState_PC::IsTexturing	(			)
{
	return HasValidSource() && (caps&CF_HAS_TEX0) && (GetGlobalEnabled()&EN_TEXTURING);
}


void
DpyState_PC::Activate(			)
{
	#ifdef TEST_UV_OFFSET
	{
		nv::clock::Time t0;
		nv::clock::GetTime( &t0 );
		float t = float(t0);
		Vec2  stOffset = Vec2( Cos(t*10.f), Sin(t*10.f) ) * 0.1f;
		SetSourceOffset( stOffset );
	}
	#endif


	// Combine with shader flags
	uint32 enable_flags = GetGlobalEnabled();
	Hrdw::SetRenderState(Hrdw::NV_SHADEMODE	,	Hrdw::NV_SHADE_GOURAUD	);
	Hrdw::SetRenderState(Hrdw::NV_ZENABLE	,	Hrdw::NV_ZB_TRUE		);
	//
	// Texturing

	bool texturing = FALSE;
	
	if( (enable_flags&EN_TEXTURING) && (caps&CF_HAS_TEX0) && (srcBmp || (src.Raster <= DPYR_OFF_FRAME_C32Z32 && src.Raster > DPYR_NULL)))
	{
		Hrdw::Texture tex = 0;
		if (srcBmp) {
			tex = srcBmp->GetTexture();
			srcBmp->UpdateBeforeDraw();
			Hrdw::SetTexture(tex,textureIndex);
			if(srcBmp->GetTextureClutIdx() >= 0 )
				Hrdw::SetCurrentPalette(uint(srcBmp->GetTextureClutIdx()));
		}
		else {
			Hrdw::SetSource(DpyManager::hwraster[src.Raster],textureIndex,FALSE);
		}

		texturing = TRUE;			

		// tex-function
		uint lastStage = textureIndex + 1;
		Hrdw::SetTextureStageState(textureIndex,Hrdw::NV_COLORARG1,Hrdw::NV_TA_DIFFUSE);
		Hrdw::SetTextureStageState(textureIndex,Hrdw::NV_COLORARG2,Hrdw::NV_TA_TEXTURE);
		Hrdw::SetTextureStageState(textureIndex,Hrdw::NV_ALPHAARG1,Hrdw::NV_TA_DIFFUSE);
		Hrdw::SetTextureStageState(textureIndex,Hrdw::NV_ALPHAARG2, Hrdw::NV_TA_TEXTURE);

		uint tf = mode & TF_MASK;
		if( tf == TF_DECAL ||  ( (caps&(CF_HAS_RGBA|CF_HAS_RGB)) == 0 && (caps&(CF_HAS_NORMAL)) == 0)) {
			// Ct
			// At
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_SELECTARG2	);
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_SELECTARG2	);					
		}
		else if( tf == TF_MODULATE ) {
			// Ct * Cf
			// At * Af
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_COLOROP,	/*Hrdw::NV_TOP_MODULATE2X*/Hrdw::NV_TOP_MODULATE	);
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_MODULATE		);
		}
		else if( tf == TF_HIGHLIGHT ) {
			// Ct * Cf + Af
			// At + Af
			// stage 0 :	modulate2(Ct,Cf)				At		-> temp
			// stage 1 :	X-X=0							X-X=0
			// stage 2 :	(1-0)*tmp+Af					tmp+Af
			//		   =	modulate2(Ct,Cf)+Af				At+Af
			Hrdw::SetTexture(NO_TEXTURE,textureIndex+1);
			Hrdw::SetTexture(NO_TEXTURE,textureIndex+2);
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_COLOROP,	/*Hrdw::NV_TOP_MODULATE2X*/Hrdw::NV_TOP_MODULATE );
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_ALPHAOP,	 Hrdw::NV_TOP_SELECTARG2 );
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_RESULTARG, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_COLORARG1, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_COLORARG2, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_ALPHAARG1, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_ALPHAARG2, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_SUBTRACT );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_SUBTRACT );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_RESULTARG, Hrdw::NV_TA_CURRENT );	// =(0,0,0,0)
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_COLORARG1, Hrdw::NV_TA_CURRENT );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_COLORARG2, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_ALPHAARG1, Hrdw::NV_TA_DIFFUSE );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_ALPHAARG2, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_COLOROP,	 Hrdw::NV_TOP_MODULATEINVCOLOR_ADDALPHA );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_ALPHAOP,	 Hrdw::NV_TOP_ADD );					
			lastStage = textureIndex+3;
		}
		else if( tf == TF_HIGHLIGHT2 ) {
			// Ct * Cf + Af
			// At
			// stage 0 :	modulate2(Ct,Cf)				At		-> temp
			// stage 1 :	X-X=0							X-X=0
			// stage 2 :	(1-0)*tmp+Af					tmp
			//		   =	modulate2(Ct,Cf)+Af				At
			Hrdw::SetTexture(NO_TEXTURE,textureIndex+1);
			Hrdw::SetTexture(NO_TEXTURE,textureIndex+2);
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_COLOROP,	 /*Hrdw::NV_TOP_MODULATE2X*/Hrdw::NV_TOP_MODULATE );
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_ALPHAOP,	 Hrdw::NV_TOP_SELECTARG2 );
			Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_RESULTARG, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_COLORARG1, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_COLORARG2, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_ALPHAARG1, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_ALPHAARG2, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_SUBTRACT );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_SUBTRACT );
			Hrdw::SetTextureStageState( textureIndex+1, Hrdw::NV_RESULTARG, Hrdw::NV_TA_CURRENT );	// =(0,0,0,0)
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_COLORARG1, Hrdw::NV_TA_CURRENT );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_COLORARG2, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_ALPHAARG1, Hrdw::NV_TA_DIFFUSE );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_ALPHAARG2, Hrdw::NV_TA_TEMP );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_COLOROP,	 Hrdw::NV_TOP_MODULATEINVCOLOR_ADDALPHA );
			Hrdw::SetTextureStageState( textureIndex+2, Hrdw::NV_ALPHAOP,	 Hrdw::NV_TOP_SELECTARG2 );					
			
			lastStage = textureIndex+ 3;
		}		
		Hrdw::SetTextureStageState( lastStage, Hrdw::NV_COLOROP,Hrdw::NV_TOP_DISABLE	);
		Hrdw::SetTextureStageState( lastStage, Hrdw::NV_ALPHAOP,Hrdw::NV_TOP_DISABLE	);				
		Hrdw::SetTexture(NO_TEXTURE,lastStage);
		// tex-mag-sampler
		uint tm = mode & TM_MASK;
		if( tm == TM_NEAREST )
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MAGFILTER,	Hrdw::NV_TEXF_POINT	);
		else
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MAGFILTER,	Hrdw::NV_TEXF_LINEAR	);

		// tex-reduce-sampler
		uint tr = mode & TR_MASK;
		if( tr == TR_NEAREST_MIP0 ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MINFILTER, Hrdw::NV_TEXF_POINT	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MIPFILTER, Hrdw::NV_TEXF_NONE	);
		}
		else if( tr == TR_LINEAR_MIP0 ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MINFILTER, Hrdw::NV_TEXF_LINEAR	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MIPFILTER, Hrdw::NV_TEXF_NONE	);
		}
		else if( tr == TR_NEAREST_NEAREST ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MINFILTER, Hrdw::NV_TEXF_POINT	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MIPFILTER, Hrdw::GetMipmapLevel(tex)>1 ? Hrdw::NV_TEXF_POINT : Hrdw::NV_TEXF_NONE );
		}
		else if( tr == TR_NEAREST_LINEAR ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MINFILTER, Hrdw::NV_TEXF_POINT	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MIPFILTER, Hrdw::GetMipmapLevel(tex)>1 ? Hrdw::NV_TEXF_LINEAR : Hrdw::NV_TEXF_NONE );
		}
		else if( tr == TR_LINEAR_NEAREST ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MINFILTER, Hrdw::NV_TEXF_LINEAR	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MIPFILTER, Hrdw::GetMipmapLevel(tex)>1 ? Hrdw::NV_TEXF_POINT : Hrdw::NV_TEXF_NONE );
		}
		else if( tr == TR_LINEAR_LINEAR ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MINFILTER, Hrdw::NV_TEXF_LINEAR	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_MIPFILTER, Hrdw::GetMipmapLevel(tex)>1 ? Hrdw::NV_TEXF_LINEAR : Hrdw::NV_TEXF_NONE );
		}

		// tex-wrapping-mode
		uint tw = mode & TW_MASK;
		if( tw == TW_REPEAT_REPEAT ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_ADDRESSU,		Hrdw::NV_TADDRESS_WRAP	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_ADDRESSV,		Hrdw::NV_TADDRESS_WRAP	);
		}
		else if( tw == TW_REPEAT_CLAMP ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_ADDRESSU,		Hrdw::NV_TADDRESS_WRAP	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_ADDRESSV,		Hrdw::NV_TADDRESS_CLAMP	);
		}
		else if( tw == TW_CLAMP_REPEAT ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_ADDRESSU,		Hrdw::NV_TADDRESS_CLAMP	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_ADDRESSV,		Hrdw::NV_TADDRESS_WRAP	);
		}
		else if( tw == TW_CLAMP_CLAMP ) {
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_ADDRESSU,		Hrdw::NV_TADDRESS_CLAMP	);
			Hrdw::SetSamplerState( textureIndex, Hrdw::NV_ADDRESSV,		Hrdw::NV_TADDRESS_CLAMP	);
		}
	}
	else
	{
		Hrdw::SetTexture(NO_TEXTURE,textureIndex);
		Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_COLOROP,   Hrdw::NV_TOP_DISABLE		);
		Hrdw::SetTextureStageState( textureIndex, Hrdw::NV_ALPHAOP,   Hrdw::NV_TOP_DISABLE		);
	}


	//
	// Culling

	uint cm = mode & CM_MASK;
	uint dxcm;
	if( cm == CM_FRONTSIDED )		dxcm = Hrdw::NV_CULL_CCW;
	else if( cm == CM_BACKSIDED )	dxcm = Hrdw::NV_CULL_CW;
	else 							dxcm = Hrdw::NV_CULL_NONE;

	Hrdw::SetRenderState( Hrdw::NV_CULLMODE, dxcm );


	//
	// Blending

	uint bs  = mode & BS_MASK;
	uint bm  = mode & BM_MASK;
	bool bmf = FALSE;
	if( bm == BM_SELECT ) {	// select ?
		if( texturing && srcBmp && srcBmp->GetAlphaStatus() == NvBitmap::AS_VARIED ) {
			bm  = BM_FILTER;
			bmf = TRUE;
		} else {
			bm = BM_NONE;
		}
	}
	if(	bm == BM_NONE	)
	{
		Hrdw::SetRenderState( Hrdw::NV_ALPHABLENDENABLE,FALSE	);
	}
	else
	{
		{
			Hrdw::BlendOP op = Hrdw::NV_BLENDOP_ADD;
			Hrdw::Blend   asrc, adst;

			if( bs == BS_FRAGMENT ) {
				asrc = Hrdw::NV_BLEND_SRCALPHA;
				adst = Hrdw::NV_BLEND_INVSRCALPHA;
			} else if( bs == BS_BUFFER ) {
				asrc = Hrdw::NV_BLEND_DESTALPHA;
				adst = Hrdw::NV_BLEND_INVDESTALPHA;
			} else {
				asrc = Hrdw::NV_BLEND_BLENDFACTOR;
				adst = Hrdw::NV_BLEND_INVBLENDFACTOR;
			}
			if( bm == BM_FILTER ) {
				op = Hrdw::NV_BLENDOP_ADD;
			} else if( bm == BM_ADD ) {
				op = Hrdw::NV_BLENDOP_ADD;
				adst = Hrdw::NV_BLEND_ONE;
			} else if( bm == BM_SUB ) {
				op = Hrdw::NV_BLENDOP_REVSUBTRACT;
				adst = Hrdw::NV_BLEND_ONE;
			} else if( bm == BM_MODULATE ) {
				op = Hrdw::NV_BLENDOP_ADD;
				adst = Hrdw::NV_BLEND_ZERO;
			} else if( bm == BM_FILTER2 ) {
				op = Hrdw::NV_BLENDOP_ADD;
				Swap( asrc, adst );
			} else if( bm == BM_ADD2 ) {
				op = Hrdw::NV_BLENDOP_ADD;
				adst = asrc;
				asrc = Hrdw::NV_BLEND_ONE;
			} else if( bm == BM_SUB2 ) {
				op = Hrdw::NV_BLENDOP_REVSUBTRACT;
				adst = asrc;
				asrc = Hrdw::NV_BLEND_ONE;
			} else if( bm == BM_MODULATE2 ) {
				op = Hrdw::NV_BLENDOP_ADD;
				adst = asrc;
				asrc = Hrdw::NV_BLEND_ZERO;
			} else if( bm == BM_DUMMY ) {
				op = Hrdw::NV_BLENDOP_ADD;
				asrc = Hrdw::NV_BLEND_ZERO;
				adst = Hrdw::NV_BLEND_ONE;
			} else if( bm == BM_ZERO ) {
				op = Hrdw::NV_BLENDOP_ADD;
				asrc = Hrdw::NV_BLEND_ZERO;
				adst = Hrdw::NV_BLEND_ZERO;
			} else if( bm == BM_MUL ) {
				op = Hrdw::NV_BLENDOP_ADD;
				asrc = Hrdw::NV_BLEND_DESTCOLOR;
				adst = Hrdw::NV_BLEND_ZERO;
			} else {
				NV_ERROR("Invalid blend mode");
			}
			Hrdw::SetRenderState( Hrdw::NV_ALPHABLENDENABLE,	TRUE	);
			Hrdw::SetRenderState( Hrdw::NV_BLENDOP,				op		);
			Hrdw::SetRenderState( Hrdw::NV_SRCBLEND,			asrc	);
			Hrdw::SetRenderState( Hrdw::NV_DESTBLEND,			adst	);
		} 

		uint A = blendCte * 255.0f;
		uint C = HRDW_COLOR_ARGB(A,A,A,A);
		Hrdw::SetRenderState( Hrdw::NV_BLENDFACTOR, C );
	}


	//
	// Alpha pass test
	Hrdw::SetRenderState( Hrdw::NV_ALPHATESTENABLE, TRUE );
	DWORD alphaRef = bmf ? 0 : (DWORD)(alphaPass*255.0f);
	Hrdw::SetRenderState( Hrdw::NV_ALPHAREF,	alphaRef );
	Hrdw::SetRenderState( Hrdw::NV_ALPHAFUNC,	Hrdw::NV_CMP_GREATEREQUAL );

	// EN_WR_COLOR
	uint dxWrMask = 0;
	if( enable_flags & EN_WR_COLOR ) {
		if( wrColorMask & 0xFF000000 )	dxWrMask |= Hrdw::NV_COLORWRITEENABLE_RED;
		if( wrColorMask & 0x00FF0000 )	dxWrMask |= Hrdw::NV_COLORWRITEENABLE_GREEN;
		if( wrColorMask & 0x0000FF00 )	dxWrMask |= Hrdw::NV_COLORWRITEENABLE_BLUE;
		if( wrColorMask & 0x000000FF )	dxWrMask |= Hrdw::NV_COLORWRITEENABLE_ALPHA;
	}
	Hrdw::SetRenderState(Hrdw::NV_COLORWRITEENABLE, dxWrMask);


	// EN_WR_DEPTH
	Hrdw::SetRenderState( Hrdw::NV_ZWRITEENABLE,
							!bmf && (enable_flags&EN_WR_DEPTH) ? TRUE : FALSE );
	/*// EN_RD_DEPTH
	Hrdw::SetRenderState( Hrdw::NV_ZFUNC,
		 					(enable_flags&EN_RD_DEPTH) ? Hrdw::NV_CMP_LESSEQUAL : Hrdw::NV_CMP_ALWAYS	);*/
	if (enable_flags&EN_RD_DEPTH) {
		Hrdw::SetRenderState( Hrdw::NV_ZENABLE, Hrdw::NV_ZB_TRUE );
		Hrdw::SetRenderState( Hrdw::NV_ZFUNC, Hrdw::NV_CMP_LESSEQUAL );
	}
	else
		Hrdw::SetRenderState( Hrdw::NV_ZENABLE, Hrdw::NV_ZB_FALSE );
	// EN_RD_STENCIL
//		if( enable_flags & EN_RD_STENCIL ) {
//			Hrdw::SetRenderState( Hrdw::NV_STENCILENABLE, TRUE );
//		}
//		else {
//			Hrdw::SetRenderState( Hrdw::NV_STENCILENABLE, FALSE );
//		}
}