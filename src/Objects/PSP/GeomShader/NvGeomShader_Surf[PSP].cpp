/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvGeomShader_Base[PSP].h"
#include <Kernel/PSP/NvkBitmap[PSP].h>
#include <Kernel/PSP/NvkSurface[PSP].h>
#include <Kernel/PSP/NvDpyState[PSP].h>
using namespace nv;



//#define	TEST_CLIPPING_MODE	1
//#define	TEST_UV_OFFSET
//#define	DISABLE_TEXTURING
//#define	DISABLE_LIGHTING




namespace
{

	interface Shader : public NvGeomShaderBase
	{
		uint32				enflags;				// enable flags (EN_...)
		NvkSurface*			surf;
		DpyState_PSP		dpystate;
		frame::Packet		pkt;
		uint				drawStart;
		uint				drawSize;
		bool				hidden;

		virtual	~	Shader	(	) {}				// needed by nv::DestructInPlace<T>(ptr)

		void
		_Init		(	NvSurface*		inSurface	)
		{
			InitDpyObject();

			enflags    = NvShader::EN_DEFAULT;
			#ifdef DISABLE_TEXTURING
			enflags   ^= NvShader::EN_TEXTURING;
			#endif

			surf = inSurface->GetKInterface();
			surf->AddRef();
			drawStart = 0;
			drawSize  = 0;

			hidden = FALSE;

			pkt.Init( this );
			pkt.ctxtA	= (frame::GeContext*)( this+1 );
			pkt.ctxtA->Init();
			pkt.ctxtCpt	= 1;

			// rebuild dpystate capabilities
			uint dpycaps = 0;
			if( surf->HasComponent(NvSurface::CO_NORMAL) )	dpycaps |= DpyState_PSP::CF_HAS_NORMAL;
			if( surf->HasComponent(NvSurface::CO_TEX) )		dpycaps |= DpyState_PSP::CF_HAS_TEX0;
			if( surf->HasComponent(NvSurface::CO_COLOR) )	dpycaps |= DpyState_PSP::CF_HAS_RGBA;

			// init dpystate
			dpystate.Init();
			dpystate.SetEnabledMask( enflags );
			dpystate.SetCapabilities( dpycaps );
		}

		void
		Release	(		)
		{
			if( ShutDpyObject() ) {
				NV_ASSERT( !pkt.IsDrawing() );
				dpystate.Shut();
				SafeRelease( surf );
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return surf->GetInterface();
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			enflags = inEnableFlags;
			dpystate.SetEnabledMask( enflags );
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}

		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex != 0 )
				return FALSE;
			hidden = TRUE;
			return TRUE;
		}

		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex != 0 )
				return FALSE;
			hidden = FALSE;
			return TRUE;
		}

		bool
		HideAllSurfaces		(					)
		{
			hidden = TRUE;
			return TRUE;
		}

		bool
		ShowAllSurfaces		(					)
		{
			hidden = FALSE;
			return TRUE;
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return (inIndex==0) ? &dpystate : NULL;
		}

		uint
		GetDrawStride	(			)
		{
			return 1;
		}

		bool
		SetDrawStart	(	uint			inOffset		)
		{
			drawStart = inOffset;
			return TRUE;
		}

		bool
		SetDrawSize		(	uint			inSize			)
		{
			drawSize = inSize;
			return TRUE;
		}

		void
		GeneratePacket	(	dmac::Cursor*		DC,
							frame::Packet*		inPkt	)
		{
			NV_ASSERT_DC( DC );

			#ifdef TEST_UV_OFFSET
				nv::clock::Time t0;
				nv::clock::GetTime( &t0 );
				float t = float(t0);
				float stOffsetX = Cos( t*10.f ) * 0.1f;
				float stOffsetY = Sin( t*10.f ) * 0.1f;
				surfaces[i].dpystate.SetSourceOffset( Vec2(stOffsetX,stOffsetY) );
			#endif

			// 2D ?
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;

			// Clipping
			uint cle = !mode2D && (enflags&NvShader::EN_CLIPPING) ? 1 : 0;
			#ifdef TEST_CLIPPING_MODE
			cle = TEST_CLIPPING_MODE;
			#endif
			*DC->i32++ = SCE_GE_SET_CLE( cle );

			// Lighting
			uint lte = surf->HasComponent(NvSurface::CO_NORMAL) ? 1 : 0;
			#ifdef DISABLE_LIGHTING
			lte = 0;
			#endif
			if( lte ) {
				*DC->i32++ = SCE_GE_SET_LTE( 1 );
				*DC->i32++ = SCE_GE_SET_LMODE( SCE_GE_LMODE_SEPARATE_SPECULAR_COLOR );
				*DC->i32++ = SCE_GE_SET_AC_RGB24( 0x000000 );
				*DC->i32++ = SCE_GE_SET_AA( 0xFF );

				*DC->i32++ = SCE_GE_SET_LE0( 1 );
				*DC->i32++ = SCE_GE_SET_LTYPEn( 0, SCE_GE_LTYPE_DIFFUSE, SCE_GE_LTYPE_DIRECTION );
				*DC->i32++ = SCE_GE_SET_LKAn( 0, ExtFp24(1.f) );
				*DC->i32++ = SCE_GE_SET_LKBn( 0, ExtFp24(0.f) );
				*DC->i32++ = SCE_GE_SET_LKCn( 0, ExtFp24(0.f) );

				*DC->i32++ = SCE_GE_SET_LACn_RGB24( 0, 0x000000 );
				*DC->i32++ = SCE_GE_SET_LDCn_RGB24( 0, 0xFFFFFF );
				*DC->i32++ = SCE_GE_SET_LSCn_RGB24( 0, 0xFFFFFF );

				// In world space
				*DC->i32++ = SCE_GE_SET_LXn( 0, ExtFp24(0.f) );
				*DC->i32++ = SCE_GE_SET_LYn( 0, ExtFp24(0.f) );
				*DC->i32++ = SCE_GE_SET_LZn( 0, ExtFp24(1.f) );

				*DC->i32++ = SCE_GE_SET_LE1( 0 );
				*DC->i32++ = SCE_GE_SET_LE2( 0 );
				*DC->i32++ = SCE_GE_SET_LE3( 0 );

				// Setup material parameters
				*DC->i32++ = SCE_GE_SET_MEC_RGB24( 0x000000 );
				*DC->i32++ = SCE_GE_SET_MAC_RGB24( 0x000000 );
				*DC->i32++ = SCE_GE_SET_MAA( 0xFF );
				*DC->i32++ = SCE_GE_SET_MDC_RGB24( 0xFFFFFF );
				*DC->i32++ = SCE_GE_SET_MSC_RGB24( 0xFFFFFF );
			} else {
				DC->i32[0] = SCE_GE_SET_LTE( 0 );
				DC->i32[1] = SCE_GE_SET_MAC_RGB24( 0xFFFFFF );
				DC->i32[2] = SCE_GE_SET_MAA( 0xFF );
				DC->i32   += 3;
			}

			int chainIdx = dpyctxt.chainHead;
			NV_ASSERT( chainIdx >= 0 );
			while( chainIdx >= 0 )
			{
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
				Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;

				// Setup drawing GE commands
				DpyManager::SetupDrawing( DC, DpyRaster(dctxt->raster), dview, dwtr, mode2D, TRUE );

				// GeContext call-list
				NV_ASSERT( pkt.ctxtA );
				NV_ASSERT( pkt.ctxtA[0].cadr );
				DC->i32[0] = SCE_GE_SET_BASE_BASE8( uint32(pkt.ctxtA[0].cadr) );
				DC->i32[1] = SCE_GE_SET_CALL( uint32(pkt.ctxtA[0].cadr) );
				DC->i32   += 2;

				// draw
				surf->GetBufferDrawing( NvkSurface::BF_BACK, drawStart, drawSize, DC->i32 );
				DC->i32 += 5;

				chainIdx = dchain->next;
			}

			NV_ASSERT_DC( DC );
		}

		void
		UpdatePacket	(	frame::Packet*			inPkt	)
		{
			dpystate.Activate( pkt.ctxtA );
		}

		bool
		ValidDrawRange	(		)
		{
			uint drawLast = drawStart + drawSize;
			uint drawMax  = surf->GetMaxSize();
			return (drawSize>0 && drawLast<=drawMax);
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );

			if( hidden )
				return FALSE;

			surf->UpdateBeforeDraw();

			if( !ValidDrawRange() )
				return FALSE;

			pkt.SetSeparator( (enflags&NvShader::EN_SORTING) ? FALSE : TRUE );
			pkt.SetNeedUpdate( TRUE );
			frame::Push( &pkt );
			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxWeldCpt,
								uint32&		outVtxProcCpt	)
		{
			if( !ValidDrawRange() )		return FALSE;
			outTriCpt		= (drawSize-2) 	 * dpyctxt.chainCpt;
			outLocCpt		= drawSize		 * dpyctxt.chainCpt;
			outVtxWeldCpt	= drawSize		 * dpyctxt.chainCpt;
			outVtxProcCpt	= drawSize		 * dpyctxt.chainCpt;
			return TRUE;
		}
	};


}




NvGeomShader*
nv_geomshader_surf_Create	(	NvSurface*		inSurface		)
{
	if( !inSurface )
		return NULL;

	// At least some components are needed !
	if( !inSurface->HasComponent(NvSurface::CO_LOC) )
		return NULL;

	uint supply = sizeof( frame::GeContext );
	Shader* shader = NvEngineNewS( Shader, supply );
	if( !shader )
		return NULL;

	shader->_Init( inSurface );
	return shader->GetInterface();
}



