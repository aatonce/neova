/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvGeomShader_Base[PSP].h"
#include <Kernel/PSP/NvkBitmap[PSP].h>
#include <Kernel/PSP/NvkMesh[PSP].h>
#include <Kernel/PSP/NvDpyState[PSP].h>
using namespace nv;




//#define	TEST_CLIPPING_MODE	1
//#define	DISABLE_TEXTURING
//#define	DISABLE_LIGHTING




namespace
{

	interface Shader : public NvGeomShaderBase
	{
		struct Surface {
			DpyState_PSP		dpystate;
			uint32				mec;
			uint32				mac;
			uint32				maa;
			uint32				mdc;
			uint32				msc;
			bool				hidden;
		};

		uint32					enflags;			// enable flags (EN_...)
		NvkMesh*				mesh;
		Matrix					unpackLocM;
		uint					surfaceCpt;
		Surface*				surfaces;
		uint32					bunchDataBase;
		uint32					bunchDataVTYPE;
		NvkMesh::Bunch*			bunchA;
		NvkMesh::BunchData*		bunchDataA;
		NvkMesh::BunchList*		bunchListA;
		NvkMesh::BunchBlend*	bunchBlendA;
		frame::GeContext*		geContextA;
		frame::Packet			pkt;


		virtual	~	Shader	(	) {}				// needed by nv::DestructInPlace<T>(ptr)


		void
		_Init		(	NvMesh*		inMesh		)
		{
			InitDpyObject();

			enflags    = NvShader::EN_DEFAULT;
			#ifdef DISABLE_TEXTURING
			enflags   ^= NvShader::EN_TEXTURING;
			#endif

			mesh = inMesh->GetKInterface();
			mesh->AddRef();

			pkt.Init( this );
			pkt.ctxtA	= geContextA;
			pkt.ctxtCpt	= surfaceCpt;

			// Mesh unpacking parameters
			float unpackLocS;
			Vec3  unpackLocT;
			float unpackTexS;
			Vec2  unpackTexT;
			mesh->GetUnpacking( &unpackLocS, &unpackLocT, &unpackTexS, &unpackTexT );
			MatrixScaling( &unpackLocM, unpackLocS, unpackLocS, unpackLocS );
			MatrixTranslate( &unpackLocM, &unpackLocM, &unpackLocT );

			// Setup bunches
			bunchDataBase	= mesh->GetBunchDataBase();
			bunchDataVTYPE	= mesh->GetBunchDataVTYPE();
			bunchA			= mesh->GetBunchA();
			bunchDataA		= mesh->GetBunchDataA();
			bunchListA		= mesh->GetBunchBySurfA();
			bunchBlendA		= mesh->GetBunchBlendA();

			// Init capabilities
			bool lightable = mesh->IsLightable();
			uint dpycaps = 0;
			if( lightable )									dpycaps |= DpyState_PSP::CF_HAS_NORMAL;
			if( GET_VTYPE_TT(bunchDataVTYPE) )				dpycaps |= DpyState_PSP::CF_HAS_TEX0;
			if( GET_VTYPE_CT(bunchDataVTYPE) || lightable )	dpycaps |= DpyState_PSP::CF_HAS_RGBA;

			// Setup internal surfaces
			NvkMesh::Shading* shadingA = mesh->GetSurfaceShadingA();
			for( uint i = 0 ; i < surfaceCpt ; i++ ) {
				NV_ASSERT_A32( &geContextA[i] );
				geContextA[i].Init();

				// hidden
				surfaces[i].hidden = FALSE;

				// Init dpystate
				ConstructInPlace( &surfaces[i].dpystate );
				surfaces[i].dpystate.Init();
				surfaces[i].dpystate.SetCapabilities( dpycaps );
				surfaces[i].dpystate.SetEnabledMask( enflags );
				surfaces[i].dpystate.SetEnabled( shadingA[i].stateEnFlags );
				surfaces[i].dpystate.SetMode( shadingA[i].stateMdFlags );
				surfaces[i].dpystate.SetAlphaPass( shadingA[i].stateAlphaPass );
				surfaces[i].dpystate.SetBlendSrcCte( shadingA[i].stateBlendCte );
				surfaces[i].dpystate.SetTexturing( shadingA[i].bitmapUID, shadingA[i].mipOFFL, shadingA[i].mappingRatio );
				surfaces[i].dpystate.SetSourceUnpack( unpackTexS, unpackTexT );
				surfaces[i].dpystate.Activate( &geContextA[i] );

				// Color filters
				surfaces[i].mec = SCE_GE_SET_MEC_RGB24( shadingA[i].emissive&0xFFFFFF );
				if( lightable ) {
					surfaces[i].mac = SCE_GE_SET_MAC_RGB24( shadingA[i].ambient&0xFFFFFF );
					surfaces[i].maa = SCE_GE_SET_MAA( shadingA[i].ambient>>24 );
				} else {
					surfaces[i].mac = SCE_GE_SET_MAC_RGB24( shadingA[i].diffuse&0xFFFFFF );
					surfaces[i].maa = SCE_GE_SET_MAA( shadingA[i].diffuse>>24 );
				}
				surfaces[i].mdc = SCE_GE_SET_MDC_RGB24( shadingA[i].diffuse&0xFFFFFF );
				surfaces[i].msc = SCE_GE_SET_MSC_RGB24( shadingA[i].specular&0xFFFFFF );
			}
		}

		void
		Release	(		)
		{
			if( ShutDpyObject() ) {
				NV_ASSERT( !pkt.IsDrawing() );
				for( uint i = 0 ; i < surfaceCpt ; i++ )
					surfaces[i].dpystate.Shut();
				SafeRelease( mesh );
				NvEngineDelete( this );
			}
		}

		NvResource*
		GetResource		(					)
		{
			return mesh->GetInterface();
		}

		void
		Enable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags|inEnableFlags );
		}

		void
		Disable		(	uint32		inEnableFlags	)
		{
			SetEnabled( enflags&(~inEnableFlags) );
		}

		void
		SetEnabled		(	uint32		inEnableFlags	)
		{
			if( inEnableFlags != enflags ) {
				enflags = inEnableFlags;
				for( uint i = 0 ; i < surfaceCpt ; i++ )
					surfaces[i].dpystate.SetEnabledMask( enflags );
			}
		}

		uint32
		GetEnabled		(		)
		{
			return enflags;
		}

		bool
		HideSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfaceCpt )
				return FALSE;
			surfaces[inIndex].hidden = TRUE;
			return TRUE;
		}

		bool
		ShowSurface		(	uint	inIndex	)
		{
			if( inIndex >= surfaceCpt )
				return FALSE;
			surfaces[inIndex].hidden = FALSE;
			return TRUE;
		}

		bool
		HideAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfaceCpt ; i++ )
				HideSurface( i );
			return TRUE;
		}

		bool
		ShowAllSurfaces		(					)
		{
			for( uint i = 0 ; i < surfaceCpt ; i++ )
				ShowSurface( i );
			return TRUE;
		}

		DpyState*
		GetDisplayState	(	uint			inIndex		)
		{
			return ( inIndex<surfaceCpt ) ? &surfaces[inIndex].dpystate : NULL;
		}

		bool
		SetAmbient		(	uint			inIndex,
							Vec4*			inColor					)
		{
			if( !mesh->IsLightable() || inIndex >= surfaceCpt || !inColor )
				return FALSE;
			uint32 abgr = GetPSM( PSM_ABGR32, *inColor );
			surfaces[inIndex].mac = SCE_GE_SET_MAC_RGB24( abgr&0xFFFFFF );
			surfaces[inIndex].maa = SCE_GE_SET_MAA( abgr>>24 );
			return TRUE;
		}

		bool
		SetDiffuse		(	uint			inIndex,
							Vec4*			inColor					)
		{
			if( !mesh->IsLightable() || inIndex >= surfaceCpt || !inColor )
				return FALSE;
			uint32 abgr = GetPSM( PSM_ABGR32, *inColor );
			surfaces[inIndex].mdc = SCE_GE_SET_MDC_RGB24( abgr&0xFFFFFF );
			return TRUE;
		}

		void
		GeneratePacket	(	dmac::Cursor*		DC,
							frame::Packet*		inPkt	)
		{
			NV_ASSERT_DC( DC );

			// 2D ?
			bool mode2D = (enflags&NvShader::EN_THROUGHMODE) != 0;

			// Clipping
			uint cle = !mode2D && (enflags&NvShader::EN_CLIPPING) ? 1 : 0;
			#ifdef TEST_CLIPPING_MODE
			cle = TEST_CLIPPING_MODE;
			#endif
			*DC->i32++ = SCE_GE_SET_CLE( cle );

			// Lighting
			uint lte = mesh->IsLightable();
			#ifdef DISABLE_LIGHTING
			lte = 0;
			#endif
			if( lte ) {
				*DC->i32++ = SCE_GE_SET_LTE( 1 );
				*DC->i32++ = SCE_GE_SET_LMODE( SCE_GE_LMODE_SINGLE_COLOR );
			//	*DC->i32++ = SCE_GE_SET_LMODE( SCE_GE_LMODE_SEPARATE_SPECULAR_COLOR );
				*DC->i32++ = SCE_GE_SET_LTYPEn( 0, SCE_GE_LTYPE_DIFFUSE, SCE_GE_LTYPE_DIRECTION );
				*DC->i32++ = SCE_GE_SET_LTYPEn( 1, SCE_GE_LTYPE_DIFFUSE, SCE_GE_LTYPE_DIRECTION );
				*DC->i32++ = SCE_GE_SET_LEn( 0, 0 );
				*DC->i32++ = SCE_GE_SET_LEn( 1, 0 );
				*DC->i32++ = SCE_GE_SET_LEn( 2, 0 );
				*DC->i32++ = SCE_GE_SET_LEn( 3, 0 );
				*DC->i32++ = SCE_GE_SET_LACn_RGB24( 0, 0x000000 );
				*DC->i32++ = SCE_GE_SET_LACn_RGB24( 1, 0x000000 );
			} else {
				DC->i32[0] = SCE_GE_SET_LTE( 0 );
				DC->i32[1] = SCE_GE_SET_MAC_RGB24( 0xFFFFFF );
				DC->i32[2] = SCE_GE_SET_MAA( 0xFF );
				DC->i32   += 3;
			}

			// Vertex-type
			*DC->i32++ = bunchDataVTYPE;

			int chainIdx     = dpyctxt.chainHead;
			int lastLightIdx = -1;
			NV_ASSERT( chainIdx >= 0 );
			while( chainIdx >= 0 )
			{
				NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data() + chainIdx;
				NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()	   + dchain->contextIdx;
				NvDpyContext::View*			dview  = DpyManager::context->viewA.data()		   + dctxt->viewIdx;
				NvDpyContext::Light*		dlight = DpyManager::context->lightA.data()		   + dctxt->lightIdx;
				Matrix*						dwtr   = DpyManager::context->wtrA.data()		   + dctxt->wtrIdx;

				if( lte && (dctxt->lightIdx!=lastLightIdx) ) {
					// Ambient light
					uint32 am_abgr = GetPSM( PSM_ABGR32, dlight->color[0] );
					*DC->i32++ = SCE_GE_SET_AC_RGB24( am_abgr&0xFF );
					*DC->i32++ = SCE_GE_SET_AA( am_abgr>>24 );

					// Diffuse/Specular lights
					for( int l = 0 ; l < 2 ; l++ ) {
						Vec3&  l_dir  = dlight->direction[l];
						uint32 l_abgr = GetPSM( PSM_ABGR32, dlight->color[l+1] );
						if( l_abgr==0 )
							continue;
						*DC->i32++ = SCE_GE_SET_LEn( l, 1 );
						*DC->i32++ = SCE_GE_SET_LDCn_RGB24( l, l_abgr&0xFFFFFF );
					//	*DC->i32++ = SCE_GE_SET_LSCn_RGB24( l, l_abgr&0xFFFFFF );
						// In world space
						*DC->i32++ = SCE_GE_SET_LXn( l, ExtFp24(-l_dir.x) );
						*DC->i32++ = SCE_GE_SET_LYn( l, ExtFp24(-l_dir.y) );
						*DC->i32++ = SCE_GE_SET_LZn( l, ExtFp24(-l_dir.z) );
					}

					lastLightIdx = dctxt->lightIdx;
				}

				// Setup drawing GE commands
				Matrix unpackM;
				MatrixMul( &unpackM, &unpackLocM, dwtr );
				DpyManager::SetupDrawing( DC, DpyRaster(dctxt->raster), dview, &unpackM, mode2D, TRUE );

				for( uint i = 0 ; i < surfaceCpt ; i++ )
				{
					if( surfaces[i].hidden )
						continue;

					// Setup material parameters
					if( lte ) {
						*DC->i32++ = surfaces[i].mec;
						*DC->i32++ = surfaces[i].mac;
						*DC->i32++ = surfaces[i].maa;
						*DC->i32++ = surfaces[i].mdc;
					//	*DC->i32++ = surfaces[i].msc;
					} else if( !GET_VTYPE_CT(bunchDataVTYPE) ) {
						*DC->i32++ = surfaces[i].mac;		// default vertex-color
						*DC->i32++ = surfaces[i].maa;		// default vertex-color
					}

					// GeContext call-list
					NV_ASSERT( pkt.ctxtA );
					NV_ASSERT( pkt.ctxtA[i].cadr );
					*DC->i32++ = SCE_GE_SET_BASE_BASE8( uint32(pkt.ctxtA[i].cadr) );
					*DC->i32++ = SCE_GE_SET_CALL( uint32(pkt.ctxtA[i].cadr) );

					// draw
					NvkMesh::BunchData* cur_bunch  = bunchDataA + bunchListA[i].startIdx;
					NvkMesh::BunchData* last_bunch = cur_bunch;		// itself !
					NV_ASSERT( bunchListA[i].size );
					do {
						NV_ASSERT( cur_bunch->prim );
						if( bunchBlendA ) {
							*DC->i32++ = SCE_GE_SET_BONEN(0);
							NvkMesh::BunchBlend* cur_blend = bunchBlendA + cur_bunch->bunchNo;
							Memcpy( DC->i32, cur_blend->boneBlend[0], 12*4 );	DC->i32 += 12;
							Memcpy( DC->i32, cur_blend->boneBlend[1], 12*4 );	DC->i32 += 12;
							Memcpy( DC->i32, cur_blend->boneBlend[2], 12*4 );	DC->i32 += 12;
						}
						uint32 vadrP = cur_bunch->vadrBOffset + bunchDataBase;
						uint32 iadrP = cur_bunch->iadrBOffset + bunchDataBase;
						*DC->i32++ = SCE_GE_SET_BASE_BASE8( vadrP );
						*DC->i32++ = SCE_GE_SET_VADR( vadrP );
						*DC->i32++ = SCE_GE_SET_BASE_BASE8( iadrP );
						*DC->i32++ = SCE_GE_SET_IADR( iadrP );
						*DC->i32++ = cur_bunch->prim;
						cur_bunch = bunchDataA + cur_bunch->nextBySurf;
					} while( cur_bunch != last_bunch );
				}

				chainIdx = dchain->next;
			}

			NV_ASSERT_DC( DC );
		}

		void
		UpdatePacket	(	frame::Packet*			inPkt	)
		{
			mesh->UpdateBeforeDraw();
			for( uint i = 0 ; i < surfaceCpt ; i++ )
				surfaces[i].dpystate.Activate( pkt.ctxtA+i );
		}

		bool
		Draw		(		)
		{
			NV_ASSERT( refCpt );
			pkt.SetNeedUpdate( TRUE );
			pkt.SetSeparator( (enflags&NvShader::EN_SORTING) ? FALSE : TRUE );
			frame::Push( &pkt );
			return TRUE;
		}

		bool
		GetPerformances		(	uint32&		outTriCpt,
								uint32&		outLocCpt,
								uint32&		outVtxCpt,
								uint32&		outPrimCpt	)
		{
			outTriCpt	= 0;
			outLocCpt	= 0;
			outVtxCpt	= 0;
			outPrimCpt	= 0;
			for( uint i = 0 ; i < mesh->GetBunchCpt() ; i++ ) {
				outTriCpt  += bunchA[i].faceCpt * dpyctxt.chainCpt;
				outLocCpt  += bunchA[i].locCpt  * dpyctxt.chainCpt;
				outVtxCpt  += bunchA[i].vtxCpt  * dpyctxt.chainCpt;
				outPrimCpt += bunchA[i].primCpt * dpyctxt.chainCpt;
			}
			return TRUE;
		}
	};


}




NvGeomShader*
nv_geomshader_mesh_Create	(	NvMesh*		inMesh		)
{
	if( !inMesh )
		return NULL;

	uint surfCpt = inMesh->GetSurfaceCpt();
	uint supply  = sizeof(Shader::Surface)  * surfCpt
				 + sizeof(frame::GeContext)	* surfCpt;
	Shader* shader = NvEngineNewS( Shader, supply );
	if( !shader )
		return NULL;

	shader->surfaceCpt = surfCpt;
	shader->surfaces   = (Shader::Surface*)( shader + 1 );
	shader->geContextA = (frame::GeContext*)( shader->surfaces + surfCpt );
	shader->_Init( inMesh );
	return shader->GetInterface();
}




