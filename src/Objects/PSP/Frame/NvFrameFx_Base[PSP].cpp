/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/NvDpyObject[PSP].h>
#include <NvFrameFx.h>
#include <Kernel/PSP/NvkFrameFx[PSP].h>
#include <Kernel/Common/NvkCore_Mem.h>

using namespace nv;
using namespace ge::tools;

#define FOG_DEF_COLOR_R	 			0xD9
#define FOG_DEF_COLOR_G  			0xE3
#define FOG_DEF_COLOR_B  			0xF4

//HeapBase = 8ko	 => 	so NULL is INVALID
#define MINADDR 					8192
#define MAXALLOC					64

namespace {	
	
	enum MapType {
		MT_FRONT_FRAME = 0	,
		MT_BACK_FRAME		,
		MT_DEPTH_FRAME		,
		MT_CUSTOM			
	};
	
	struct Map {
		uint 	addr;			// addr in Vram
		uint 	maddr;			// addr 
		uint16  sizeX;
		uint16	sizeY;
		uint16  sizeXPow2;
		uint16	sizeYPow2;		
		int16	psm;
		int16	bufferWidth;
		MapType	type;
	};
	
	uint32	  			reallocValue	;	// value use to translate with Xor op the FBP et TBP command.	
	
	void
	InitClut(uint32 inFogClut32[],uint16 inZToAlphaClut16[])
	{
		for (uint i = 0 ; i < 256 ; ++i ) {
			inFogClut32[i]		=	( 	(255 - i		) 	<<24) 	|		//A
									( 	(FOG_DEF_COLOR_B)	<<16) 	|		//B
									(	(FOG_DEF_COLOR_G) 	<<8 )  	| 		//G
						 			(	 FOG_DEF_COLOR_R		)	;		//R												 
			inZToAlphaClut16[i] = ( (i*i) / 255 )<<8;
		}
		inFogClut32[0]			= 0;
		inZToAlphaClut16[0] 	= 255<<8;
	}	
}	

interface NvFrameFxBase : public NvDpyObject_PSP
{
	NvFrameFx			itf;
	NvkFrameFx			kitf;
	
	frame::Packet		pkt0,pkt1;				// drawing packet ...
	frame::Packet * 	drawPkt;				// drawing packet ...
	uint32 *			cmdAddrList	;			// an array of pointer on FBP and TBP command to translate if we are in swap mode.
	
	uint8 				ZShift;
	uint16				enFlags;				// Activated FX
	
	nv::mem::Allocator * outerAllocator;

	ALIGNED128( uint32, fogClut32[256] );
	ALIGNED128( uint16, zToAlphaClut16[256] );	
			
	NvFrameFxBase				(	)	{}
	virtual ~NvFrameFxBase		(	)	{}			// needed by nv::DestructInPlace<T>(ptr)

	void
	_Init		(		)
	{
		InitDpyObject();

		pkt0.Init();
		pkt0.owner = this;
		pkt0.SetSeparator( TRUE );
		pkt1.Init();
		pkt1.owner = this;
		pkt1.SetSeparator( TRUE );
			
		drawPkt			= &pkt0;
		ZShift			=	3;		
		reallocValue 	= (ge::frame::FrontAddr == 0)?ge::frame::BackAddr:ge::frame::FrontAddr;		
		
		outerAllocator = nv::mem::CreateOuterAllocator ((void *)MINADDR,ge::frame::VRamUpperAddr,MAXALLOC );
		NV_ASSERT(outerAllocator);
		
		InitClut(fogClut32,zToAlphaClut16);
	}

	void
	Release	(		)
	{
		if( ShutDpyObject() ) {
			NV_ASSERT( !pkt0.IsDrawing() );
			NV_ASSERT( !pkt1.IsDrawing() );
			SafeFree( pkt0.cadr );
			SafeFree( pkt1.cadr );	
			SafeRelease(outerAllocator);	
			NvEngineDelete( this );
		}
	}

	NvkFrameFx*
	GetKInterface	(			)
	{
		return &kitf;
	}

	NvFrameFx*
	GetInterface		(		)
	{
		return &itf;
	}

	void 
	_ResetMap()
	{				
		NV_ASSERT_RESULT( outerAllocator->Reset() );
		
		// Alloc in place System Map ...
		uint size;
		void * addr;
		// Front
		size = GetBufferByteSize(ge::frame::FrontPSM,ge::frame::PhysicalSizeX,ge::frame::PhysicalSizeY);
		addr = outerAllocator->AllocInPlace( (void *)(ge::frame::FrontAddr +MINADDR),size);
		NV_ASSERT(addr ==  (void *)(ge::frame::FrontAddr +MINADDR));
		// Back
		size = GetBufferByteSize(ge::frame::BackPSM,ge::frame::SizeX,ge::frame::SizeY);
		addr = outerAllocator->AllocInPlace( (void *)(ge::frame::BackAddr +MINADDR),size);
		NV_ASSERT(addr == (void *)(ge::frame::BackAddr +MINADDR));
		// depth
		size = GetBufferByteSize(SCE_GE_TPF_IDTEX16,ge::frame::SizeX,ge::frame::SizeY);
		addr= outerAllocator->AllocInPlace( (void *)(ge::frame::DepthAddr +MINADDR),size);
		NV_ASSERT(addr ==  (void *)(ge::frame::DepthAddr +MINADDR));		

	}
	
	void
	_GetSystemMap(Map & outMap, MapType inType)
	{
		outMap.type = inType ;
		switch(inType) {
			case MT_FRONT_FRAME :
				outMap.addr 		= (uint) ge::frame::FrontAddr;
				outMap.maddr 		= (uint)ge::GetVRamPointer (uint(outMap.addr));
				outMap.psm 			= ge::frame::FrontPSM;
				outMap.sizeX 		= ge::frame::PhysicalSizeX;
				outMap.sizeY 		= ge::frame::PhysicalSizeY;
				outMap.bufferWidth 	= RoundX(ge::frame::PhysicalSizeX,64);				
				break;
			case MT_BACK_FRAME :
				outMap.addr 		= (uint)ge::frame::BackAddr;
				outMap.maddr 		= (uint)ge::GetVRamPointer (uint(outMap.addr));				
				outMap.psm 			= ge::frame::BackPSM;
				outMap.sizeX 		= ge::frame::SizeX;
				outMap.sizeY 		= ge::frame::SizeY;
				outMap.bufferWidth 	= RoundX(ge::frame::SizeX,64);				
				break;
			case MT_DEPTH_FRAME :
				outMap.addr 		= (uint)ge::frame::DepthAddr;
				outMap.maddr 		= (uint)( uint32(ge::GetVRamPointer (uint(outMap.addr))) + 0x600000 );			
				outMap.psm 			= SCE_GE_TPF_IDTEX16;						// To convert Z value to alpha value
				outMap.sizeX 		= ge::frame::SizeX;
				outMap.sizeY 		= ge::frame::SizeY;
				outMap.bufferWidth 	= ge::frame::DepthWidth;							
				break;
			default : NV_ASSERT(false);
				break;
		}
		outMap.sizeXPow2		= GetCeilPow2(outMap.sizeX  );
		outMap.sizeYPow2		= GetCeilPow2(outMap.sizeY  );
	}
	
	bool	
	_CreateMap(Map & outMap, int inPsm , int inSizeX , int inSizeY) 
	{
		uint size = GetBufferByteSize( inPsm, inSizeX, inSizeY );
		int ptr = int(outerAllocator->AllocA(size,8*1024,0));
		if (ptr) {			
			outMap.addr 			= (uint)(ptr-MINADDR);
			outMap.maddr 			= (uint)ge::GetVRamPointer (uint(outMap.addr));			
			outMap.sizeX 			= inSizeX;
			outMap.sizeY 			= inSizeY;
			outMap.sizeXPow2		= GetCeilPow2(outMap.sizeX  );
			outMap.sizeYPow2		= GetCeilPow2(outMap.sizeY  );			
			outMap.psm  			= inPsm;
			outMap.bufferWidth  	= Round64(inSizeX);
			outMap.type 			= MT_CUSTOM ;
			return TRUE;
		}
		else
			return FALSE;
	}
	
	
	void
	_FreeMap(Map & ioMap)
	{
		outerAllocator->Free( (void *) (uint(ioMap.addr) + MINADDR));
		ioMap.addr 			= (uint)(0xFFFFFFFF);
		ioMap.bufferWidth 	= 0;
	}

	bool
	SetDFogDepth		(	uint8			inDepthIdx,
							uint8			inDepth		)
	{
		return FALSE;
	}

	bool					
	SetDFogColor		(	uint32			inRGBA		)
	{
		//fogClut32[inClutIdx] = inRGBA;
		return FALSE;
	}

	bool
	SetDOFSharpness		(	uint8			inDepthIdx	,
							uint8			inSharpness	)
	{
		return FALSE;
	}
							
	bool
	SetBlurRadius		(	float			inRadius	)
	{
		return FALSE;
	}

	bool
	SetBloom			(	uint32			inRGBA,
							float			inRadius	)
	{
		return FALSE;
	}

	void
	SetZShift			(	uint8		inZShift		)
	{
		ZShift = inZShift;
		GenerateFx(enFlags);
	}

	bool	GenerateFx	(	uint	inEN	)
	{
		frame::Packet * 	buildPkt;		// Just a temporary packet to create display list
		if (pkt0.IsDrawing())
			buildPkt = &pkt1;
		else 
			buildPkt = &pkt0;		

		bool doFog		= inEN & (1<<NvFrameFx::FX_DFOG);
		bool doDof		= inEN & (1<<NvFrameFx::FX_DOF);
		bool doBlur		= inEN & (1<<NvFrameFx::FX_BLUR);
		bool doBloom	= inEN & (1<<NvFrameFx::FX_BLOOM);
		
		enFlags=inEN;
		if (!doFog && !doDof && !doBlur && !doBloom) {			
			SafeFree( buildPkt->cadr );
			buildPkt->cadr = NULL;
			buildPkt->SetNeedUpdate( FALSE );
			drawPkt = buildPkt;
			return TRUE;
		}

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );
		SafeFree( buildPkt->cadr );
		buildPkt->cadr = DC->i32;
		buildPkt->SetNeedUpdate( TRUE );

		Map frontM,backM,depthM;
		_ResetMap();		
		_GetSystemMap(frontM	,MT_FRONT_FRAME);
		_GetSystemMap(backM		,MT_BACK_FRAME);
		_GetSystemMap(depthM	,MT_DEPTH_FRAME);
		
		Vec4 rec, tex;
		vector<uint32 * >	backAddrTranslationA;
		
		Map blurM,tmpM;
		bool ret  	= _CreateMap(blurM,SCE_GE_TPF_8888, (backM.sizeX >> 1) + 32 , (backM.sizeY >> 1) + 32);		
		NV_ASSERT ( ret );
		ret  		= _CreateMap(tmpM,SCE_GE_TPF_8888, blurM.sizeX , blurM.sizeY);		
		NV_ASSERT ( ret );
		
		InitContextCmds(DC);
		//
		// DFOG
		if( doFog )		
		{
			*DC->i32++ 	= SCE_GE_SET_ABE		( 1 );		
			*DC->i32++ 	= SCE_GE_SET_ZTE		( 1 );

			LoadClut(DC,(uint)	fogClut32,SCE_GE_CLUT_CPF_8888,ZShift,0xFF);
			
			// Blend Parameter : Csrc * Asrc + Cdst * (255 - Asrc)
			*DC->i32++ = SCE_GE_SET_BLEND		(2,3,0);			
			
			uint16 Zval = (1 << ( 8 + ZShift))-1;	// Keep inZShift+8 first Bits
			uint32 * cmdAddr;
			CopyZAsIdxTex(DC,depthM.maddr,backM.addr,backM.sizeX,backM.sizeY,Zval,CF_FilteringNearest | CF_UseAlpha | CF_TFlush | CF_TSync,&cmdAddr);
			if (cmdAddr) backAddrTranslationA.push_back(cmdAddr);
						
			*DC->i32++ 	= SCE_GE_SET_ABE		( 0 );		
			*DC->i32++ 	= SCE_GE_SET_ZTE		( 0 );
		}	
	
		//
		// DOF

		if( doDof )
		{				
			LoadClut(DC,(uint)zToAlphaClut16,SCE_GE_CLUT_CPF_5650,ZShift,0xFF);
			
			uint32 * cmdAddr;						
			CopyZToAlpha(DC,depthM.maddr,backM.addr,backM.sizeX,backM.sizeY,&cmdAddr);
			if (cmdAddr) backAddrTranslationA.push_back(cmdAddr);														

			PreparePass (DC,backM.maddr,blurM.addr,backM.sizeX,backM.sizeY,blurM.sizeX,blurM.sizeY,&cmdAddr);		
			if (cmdAddr) backAddrTranslationA.push_back(cmdAddr);
			GaussianBlur(DC, blurM.addr,tmpM.addr,blurM.sizeX,blurM.sizeY,1.5f,1.f );						

			uint16 Zval = (1 << ( 8 + ZShift))-1;	// Keep inZShift+8 first Bits		 
			
			*DC->i32++ = SCE_GE_SET_ABE 	( 1 );
			*DC->i32++ = SCE_GE_SET_BLEND	(5,4,0);							
			*DC->i32++ = SCE_GE_SET_ZTE		( 1 );
			UnPreparePass(DC,blurM.maddr,backM.addr,blurM.sizeX,blurM.sizeY,backM.sizeX,backM.sizeY,Zval,CF_FilteringLinear | CF_TSync | CF_UseAlpha, &cmdAddr);			
			if (cmdAddr) backAddrTranslationA.push_back(cmdAddr);
			*DC->i32++ = SCE_GE_SET_ABE 	( 0 );
			*DC->i32++ = SCE_GE_SET_ZTE		( 0 );			

		}

		//
		// Bloom

		if( doBloom )
		{
			uint32 * cmdAddr;
			PreparePass (DC,backM.maddr,blurM.addr,backM.sizeX,backM.sizeY,blurM.sizeX,blurM.sizeY,&cmdAddr);		
			if (cmdAddr) backAddrTranslationA.push_back(cmdAddr);
			
			*DC->i32++ = SCE_GE_SET_TFLUSH	(	);
			*DC->i32++ = SCE_GE_SET_TSYNC	(	);		
			
			*DC->i32++ = SCE_GE_SET_TME		( 0	);
			*DC->i32++ = SCE_GE_SET_ABE 	( 1 );
			*DC->i32++ = SCE_GE_SET_FIXA 	( 255,255,255 );
			*DC->i32++ = SCE_GE_SET_FIXB 	( 255,255,255 );
			*DC->i32++ = SCE_GE_SET_BLEND	(10,10,2);							
			rec.Set	(0,0,blurM.sizeX,blurM.sizeY);		
			FillBuffer(	DC,blurM.addr,blurM.sizeX,rec,0,0x90909090);
											
			*DC->i32++ = SCE_GE_SET_TME		( 1	);	
			*DC->i32++ = SCE_GE_SET_ABE 	( 0 );
			GlowBlur(DC, blurM.addr,tmpM.addr,blurM.sizeX,blurM.sizeY,30.0f,0.f );
			*DC->i32++ = SCE_GE_SET_ABE 	( 1 );
			*DC->i32++ = SCE_GE_SET_FIXA 	( 255,255,255 );
			*DC->i32++ = SCE_GE_SET_FIXB 	( 255,255,255 );
			*DC->i32++ = SCE_GE_SET_BLEND	(10,10,0);				
			UnPreparePass(DC,blurM.maddr,backM.addr,blurM.sizeX,blurM.sizeY,backM.sizeX,backM.sizeY,0,CF_FilteringLinear | CF_TSync | CF_UseAlpha, &cmdAddr);			
			if (cmdAddr) backAddrTranslationA.push_back(cmdAddr);			
		}

		//
		// BLUR
		if( doBlur )
		{
			uint32 * cmdAddr;
			PreparePass (DC,backM.maddr,blurM.addr,backM.sizeX,backM.sizeY,blurM.sizeX,blurM.sizeY,&cmdAddr);		
			if (cmdAddr) backAddrTranslationA.push_back(cmdAddr);
			GaussianBlur(DC, blurM.addr,tmpM.addr,blurM.sizeX,blurM.sizeY,15.0f,1.f );		
			UnPreparePass(DC,blurM.maddr,backM.addr,blurM.sizeX,blurM.sizeY,backM.sizeX,backM.sizeY,0,CF_FilteringLinear | CF_TSync, &cmdAddr);			
			if (cmdAddr) backAddrTranslationA.push_back(cmdAddr);
		}
		
		RestoreContextCmds(DC);
		
		*DC->i32++ = SCE_GE_SET_RET();
		
		// copy the list of cmd addr that may be translate if we are in swap mode.
		cmdAddrList = DC->i32;
		if (backAddrTranslationA.size()){
			Memcpy(DC->i32,&backAddrTranslationA[0],backAddrTranslationA.size()*sizeof(uint32*));
			DC->i32+=backAddrTranslationA.size();
		}
		*DC->i32++ = 0xFFFFFFFF;

		pvoid DCDup = (uint8*) DC->Dup( buildPkt->cadr );
		int reallocationOffset = int(DCDup) - int(buildPkt->cadr);
		DC->i32 = (uint32*)buildPkt->cadr;
		buildPkt->cadr=DCDup;
		drawPkt = buildPkt;
		
		backAddrTranslationA.clear();
		(uint8*)(cmdAddrList) += reallocationOffset;				
		uint32 * ptr = cmdAddrList;
		while (*ptr != 0xFFFFFFFF) {
			*ptr+=reallocationOffset;
			(** ((uint32**)(ptr)))^=reallocValue;
			ptr++;
		}
		return TRUE;
	}
	
	void
	GeneratePacket	(	dmac::Cursor*		inDC,
						frame::Packet*		inPkt	)
	{		
		
	}

	void
	UpdatePacket	(	frame::Packet*		inPkt	)
	{			
		NV_ASSERT(inPkt == drawPkt);
		uint32 ** ptr = (uint32 **)cmdAddrList;
		while (uint(*ptr) != 0xFFFFFFFF) {
			(**ptr)^=reallocValue;
			ptr++;
		}
	}	

	bool
	Draw	(		)
	{
		NV_ASSERT( refCpt );
		frame::Push( drawPkt );			
		return TRUE;
	}
};




NvFrameFx *
NvFrameFx::Create	(		)
{
	NvFrameFxBase * inst = NvEngineNew( NvFrameFxBase );
	if( !inst )
		return NULL;
	inst->_Init();
	return &inst->itf;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameFx,	NvInterface*,	GetBase													)
NVITF_CAL0( FrameFx,	void,			AddRef													)
NVITF_MTH0( FrameFx,	uint,			GetRefCpt												)
NVITF_CAL0( FrameFx,	void,			Release													)
NVITF_MTH0( FrameFx,	NvkFrameFx*,	GetKInterface											)
NVITF_MTH1( FrameFx,	bool,			GenerateFx,			uint								)
NVITF_MTH1( FrameFx,	bool, 			SetDFogColor,		uint32								)
NVITF_MTH2( FrameFx,	bool, 			SetDFogDepth,		uint8,	uint8						)
NVITF_MTH2( FrameFx,	bool, 			SetDOFSharpness,	uint8,	uint8						)
NVITF_MTH1( FrameFx,	bool, 			SetBlurRadius,		float								)
NVITF_MTH2( FrameFx,	bool, 			SetBloom,			uint32,	float						)

NVKITF_MTH0( FrameFx,	NvInterface*,	GetBase													)
NVKITF_CAL0( FrameFx,	void,			AddRef													)
NVKITF_MTH0( FrameFx,	uint,			GetRefCpt												)
NVKITF_CAL0( FrameFx,	void,			Release													)
NVKITF_MTH0( FrameFx,	NvFrameFx*,		GetInterface											)
NVKITF_CAL1( FrameFx, 	void,			SetZShift,			uint8								)


