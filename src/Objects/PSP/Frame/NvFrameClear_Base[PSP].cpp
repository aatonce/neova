/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/NvDpyObject[PSP].h>
#include <NvFrameClear.h>
using namespace nv;




//
// BASE

interface NvFrameClearBase : public NvDpyObject_PSP
{
	NvFrameClear			itf;

	frame::Packet			pkt;
	ge::frame::ClearMod		pkt_mod;
	void*					pkt_cadr;

	uint32					color;
	bool					enColor;
	bool					enDepth;
	Vec4					vp;


	NvFrameClearBase	(	)	{}
	virtual ~NvFrameClearBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)


	void
	_Init	(		)
	{
		InitDpyObject();

		enColor = TRUE;
		enDepth = TRUE;
		color	= 0;
		Vec4Zero( &vp );

		pkt.Init( this );
		pkt.SetSeparator( TRUE );		// always separator !
		pkt.SetNeedUpdate( FALSE );
		pkt_cadr = NULL;

		BuildDMA();
	}


	void
	Release	(		)
	{
		if( ShutDpyObject() ) {
			NV_ASSERT( !pkt.IsDrawing() );
			SafeFree( pkt_cadr );
			NvEngineDelete( this );
		}
	}


	void
	EnableDepth	(	bool	inOnOff		)
	{
		if( enDepth != inOnOff ) {
			enDepth = inOnOff;
			pkt.SetNeedUpdate( TRUE );
		}
	}


	void
	EnableColor	(	bool	inOnOff		)
	{
		if( enColor != inOnOff ) {
			enColor = inOnOff;
			pkt.SetNeedUpdate( TRUE );
		}
	}


	void
	SetColor	(	uint32		inRGBA		)
	{
		uint32 _c = InvertByteOrder( inRGBA );
		if( _c != color ) {
			color = _c;
			pkt.SetNeedUpdate( TRUE );
		}
	}


	void
	BuildDMA	(		)
	{
		SafeFree( pkt_cadr );

		dmac::Cursor * DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );
		pvoid dc0 = DC->vd;

		ge::frame::Clear_CH( DC, pkt_mod );
		*DC->i32++ = SCE_GE_SET_RET();
		NV_ASSERT_DC( DC );

		pkt_cadr = DC->Dup( dc0 );
		pkt_mod.Translate( uint32(pkt_cadr)-uint32(dc0) );
		DC->vd = dc0;
	}


	void
	UpdatePacket	(	frame::Packet*	inPkt	)
	{
		NV_ASSERT( inPkt == &pkt );
		pkt_mod.SetColor( color );
		pkt_mod.EnableColor( enColor );
		pkt_mod.EnableDepth( enDepth );
		pkt_mod.SetRect( int(vp.x), int(vp.y), int(vp.z), int(vp.w) );
		pkt.SetNeedUpdate( FALSE );
	}


	void
	GeneratePacket	(	dmac::Cursor*		DC,
						frame::Packet*		inPkt	)
	{
		// target to clear
		NV_ASSERT( dpyctxt.chainHead >= 0 );
		NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data()	+ dpyctxt.chainHead;
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()		+ dchain->contextIdx;

		uint8 raster = dctxt->raster;
		DC->i32[0] = DpyManager::raster[raster].fbp;
		DC->i32[1] = DpyManager::raster[raster].fbw;
		DC->i32[2] = DpyManager::raster[raster].fpf;
		DC->i32[3] = DpyManager::raster[raster].offsetxy[0];
		DC->i32[4] = DpyManager::raster[raster].offsetxy[1];
		DC->i32[5] = DpyManager::raster[raster].zbp;
		DC->i32[6] = DpyManager::raster[raster].zbw;
		DC->i32[7] = DpyManager::raster[raster].zmsk;
		DC->i32   += 8;

		DC->i32[0] = SCE_GE_SET_BASE_BASE8( uint32(pkt_cadr) );
		DC->i32[1] = SCE_GE_SET_CALL( uint32(pkt_cadr) );
		DC->i32   += 2;
	}


	bool
	Draw	(		)
	{
		NV_ASSERT( refCpt );

		// region to clear
		NV_ASSERT( dpyctxt.chainHead >= 0 );
		NvDpyContext::ContextChain*	dchain = DpyManager::context->contextChainA.data()	+ dpyctxt.chainHead;
		NvDpyContext::Context*		dctxt  = DpyManager::context->contextA.data()		+ dchain->contextIdx;
		NvDpyContext::View*			dview  = DpyManager::context->viewA.data()			+ dctxt->viewIdx;
		Vec4& dvp = dview->viewport;

		if( dvp != vp ) {
			Vec4Copy( &vp , &dvp );
			pkt.SetNeedUpdate( TRUE );
		}

		frame::Push( &pkt );
		return TRUE;
	}
};



NvFrameClear *
NvFrameClear::Create	(		)
{
	NvFrameClearBase* inst = NvEngineNew( NvFrameClearBase );
	if( !inst )
		return NULL;

	inst->_Init();
	return &inst->itf;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( FrameClear,	NvInterface*,	GetBase								)
NVITF_CAL0( FrameClear,	void,			AddRef								)
NVITF_MTH0( FrameClear,	uint,			GetRefCpt							)
NVITF_CAL0( FrameClear,	void,			Release								)
NVITF_CAL1( FrameClear,	void,			EnableColor,		bool			)
NVITF_CAL1( FrameClear,	void,			EnableDepth,		bool			)
NVITF_CAL1( FrameClear,	void,			SetColor,			uint32			)



