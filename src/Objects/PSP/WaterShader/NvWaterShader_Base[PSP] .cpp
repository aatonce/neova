/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <NvMesh.h>
#include <NvSurface.h>
#include "NvWaterShader_Base[PSP].h"


NvWaterShader*
NvWaterShader::Create	(		)
{
	return NULL;
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>


NVITF_MTH0( WaterShader,	NvInterface*,	GetBase												)
NVITF_CAL0( WaterShader,	void,			AddRef												)
NVITF_MTH0( WaterShader,	uint,			GetRefCpt											)
NVITF_CAL0( WaterShader,	void,			Release												)
NVITF_MTH0( WaterShader,	NvResource*,	GetResource											)
NVITF_CAL1( WaterShader,	void,			Enable,				uint32							)
NVITF_CAL1( WaterShader,	void,			Disable,			uint32							)
NVITF_CAL1( WaterShader,	void,			SetEnabled,			uint32							)
NVITF_MTH0( WaterShader,	uint32,			GetEnabled											)
NVITF_MTH1( WaterShader,	DpyState*,		GetDisplayState,	uint							)


NVITF_MTH2( WaterShader,	bool,			SetMap,				NvWaterShader::MapID,	NvBitmap *)
NVITF_MTH2( WaterShader,	bool,			SetMatrix,			NvWaterShader::MapID,	Matrix *)
NVITF_CAL2( WaterShader,	void,			SetSize,			float,					float	)
NVITF_CAL2( WaterShader,	void,			SetTextureArea,		float,					float	)



