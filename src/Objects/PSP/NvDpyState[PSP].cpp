/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/NvDpyState[PSP].h>
using namespace nv;


//#define	TEST_UV_OFFSET
//#define	DISABLE_FACECULLING
//#define	DISABLE_TEXTURING



namespace
{

	inline uint	_UpdateMode	(	uint	inMode,
								uint	inMask,
								uint	inFlags		)
	{
		uint f = inFlags & inMask;
		return f ? (inMode&(~inMask))|f : inMode;
	}

}





void
DpyState_PSP::Init	(		)
{
	caps			= 0;
	enableOp		= EN_DEFAULT;
	enableOpMask	= EN_ALL_MASK;
	mode			= MODE_DEFAULT;
	src				= DpySource();
	srcT			= Vec2::ZERO;
	srcUnpackT		= Vec2::ZERO;
	srcUnpackS		= 1.f;
	alphaPass		= 0.02f;
	blendCte		= 0.5f;
	wrColorMask		= 0xFFFFFFFF;
	SetMipmapping( 0.f );
	changed			= TRUE;
}


void
DpyState_PSP::Shut		(			)
{
	SafeRelease( src.Bitmap );
}


void
DpyState_PSP::Enable	(	uint		inEnableFlags		)
{
	SetEnabled( enableOp | inEnableFlags );
}


void
DpyState_PSP::Disable	(	uint		inEnableFlags		)
{
	SetEnabled( enableOp & (~inEnableFlags) );
}


void
DpyState_PSP::SetEnabled	(	uint	inEnableFlags		)
{
	uint old = (enableOp & enableOpMask);
	enableOp = (inEnableFlags & EN_ALL_MASK);
	uint cur = (enableOp & enableOpMask);
	if( cur != old )
		changed = TRUE;
}


uint
DpyState_PSP::GetEnabled	(					)
{
	return enableOp;
}


void
DpyState_PSP::SetEnabledMask	(	uint		inEnableFlags		)
{
	uint old = (enableOp & enableOpMask);
	enableOpMask = (inEnableFlags & EN_ALL_MASK);
	uint cur = (enableOp & enableOpMask);
	if( cur != old )
		changed = TRUE;
}


uint
DpyState_PSP::GetEnabledMask	(				)
{
	return enableOpMask;
}


uint
DpyState_PSP::GetGlobalEnabled	(				)
{
	return ( enableOp & enableOpMask );
}


void
DpyState_PSP::SetCapabilities		(	uint		inCapabilities		)
{
	if( caps != inCapabilities ) {
		caps = inCapabilities;
		changed = TRUE;
	}
}


uint
DpyState_PSP::GetCapabilities		(									)
{
	return caps;
}


void
DpyState_PSP::SetMode		(	uint		inModeFlags	)
{
	uint m = _UpdateMode( mode, TF_MASK, inModeFlags );
		 m = _UpdateMode( m,    TM_MASK, inModeFlags );
		 m = _UpdateMode( m,	   TR_MASK, inModeFlags );
		 m = _UpdateMode( m,    TW_MASK, inModeFlags );
		 m = _UpdateMode( m,    CM_MASK, inModeFlags );
		 m = _UpdateMode( m,    BM_MASK, inModeFlags );
		 m = _UpdateMode( m,    BS_MASK, inModeFlags );
	if( m != mode ) {
		changed = TRUE;
		mode = m;
	}
}


uint
DpyState_PSP::GetMode		(							)
{
	return mode;
}


bool
DpyState_PSP::SetSource	(	DpySource	inSource		)
{
	if( src == inSource )
		return TRUE;
	SafeRelease( src.Bitmap );
	src = inSource;
	if( src.Bitmap )
		src.Bitmap->AddRef();
	changed = TRUE;
	return TRUE;
}


void
DpyState_PSP::GetSource		(	DpySource&	outSource		)
{
	outSource = src;
}


void
DpyState_PSP::SetSourceOffset	(	const Vec2&	inOffset	)
{
	if( srcT != inOffset ) {
		srcT = inOffset;
		changed = TRUE;
	}
}


void
DpyState_PSP::GetSourceOffset	(	Vec2&		outOffset	)
{
	Vec2Copy( &outOffset, &srcT );
}


void
DpyState_PSP::SetSourceUnpack	(	float		inScale,
									const Vec2&	inOffset	)
{
	if( srcUnpackS!=inScale || srcUnpackT!=inOffset ) {
		srcUnpackS = inScale;
		srcUnpackT = inOffset;
		changed = TRUE;
	}
}


bool
DpyState_PSP::SetAlphaPass	(	float		inAlphaPass		)
{
	inAlphaPass = Clamp( inAlphaPass, 0.0f, 1.0f );
	if( inAlphaPass != alphaPass ) {
		alphaPass = inAlphaPass;
		changed = TRUE;
	}
	return TRUE;
}


float
DpyState_PSP::GetAlphaPass	(				)
{
	return alphaPass;
}


bool
DpyState_PSP::SetBlendSrcCte	(	float		inCte	)
{
	inCte = Clamp( inCte, 0.0f, 1.0f );
	if( inCte != blendCte ) {
		blendCte = inCte;
		changed = TRUE;
	}
	return TRUE;
}


float
DpyState_PSP::GetBlendSrcCte	(			)
{
	return blendCte;
}


void
DpyState_PSP::SetWRColorMask	(	uint32		inRGBAMask		)
{
	if( wrColorMask != inRGBAMask ) {
		wrColorMask = inRGBAMask;
		changed = TRUE;
	}
}


uint32
DpyState_PSP::GetWRColorMask	(				)
{
	return wrColorMask;
}


bool
DpyState_PSP::HasChanged		(			)
{
	return changed;
}


bool
DpyState_PSP::HasValidSource	(		)
{
	return ( src.Bitmap || src.Raster!=DPYR_NULL );
}


bool
DpyState_PSP::IsTexturing	(			)
{
	return HasValidSource() && (caps&CF_HAS_TEX0) && (GetGlobalEnabled()&EN_TEXTURING);
}


void
DpyState_PSP::SetMipmapping	(	float		inOffset	)
{
	int ioff = int( inOffset * 16.f );
	    ioff = Clamp( ioff, -128, +127 );
	mipOFFL = ioff;
	changed = TRUE;
}


NvkBitmap*
DpyState_PSP::SetTexturing	(	uint32		inBitmapUID,
								float		inMappingOffset,
								float		inMappingRatio		)
{
	NvBitmap* bmpP = NvBitmap::Create( inBitmapUID );
	SetSource( DpySource(bmpP) );
	SafeRelease( bmpP );
	if( !src.Bitmap )
		return NULL;
	NvkBitmap* kbmp = src.Bitmap->GetKInterface();
	NV_ASSERT( kbmp );

	float _mipOffset = -3.75f;

	if( inMappingOffset != 0.f )
	{
		_mipOffset = inMappingOffset;
	}
	else if( inMappingRatio>0 && kbmp && kbmp->GetWidth()>0 )
	{
		// We consider here that :
		// o fov=PI/4 => aspect=1/Tanf(Pi/4)=1
		// o single fullscreen viewport
		float r  = inMappingRatio;
		float w  = int( kbmp->GetWidth() );
		float s  = int( DpyManager::GetPhysicalWidth()/2 );
		float z0 = s /( w * r );
		_mipOffset = - Log2f( z0 );
		_mipOffset = Clamp( _mipOffset, -7.0f, 0.0f );
	//	Printf( "r=%f w=%f => OFFL=%f\n", r, w, _mipOffset );
	}

//	Printf( "set mipmapping OFFL=%f\n", _mipOffset );
//	SetMipmapping( _mipOffset );
	SetMipmapping( 0.f );
	return kbmp;
}




void
DpyState_PSP::Activate(		frame::GeContext*	ioContext	)
{
	#ifdef TEST_UV_OFFSET
	{
		nv::clock::Time t0;
		nv::clock::GetTime( &t0 );
		float t = float(t0);
		Vec2  stOffset = Vec2( Cos(t*10.f), Sin(t*10.f) ) * 0.1f;
		SetSourceOffset( stOffset );
	}
	#endif


	NV_ASSERT( ioContext );
	frame::GeCallList& cmd  = ioContext->cmd;
	uint		enflags		= GetGlobalEnabled();
	bool		texturing	= IsTexturing();
	NvkBitmap*	kbmp		= src.Bitmap ? src.Bitmap->GetKInterface() : NULL;

	#ifdef DISABLE_TEXTURING
	texturing = FALSE;
	#endif

	// Always update the source bitmap
	if( texturing ) {
		if( kbmp ) {
			kbmp->UpdateBeforeDraw();
		} else if( src.Raster == DPYR_IN_FRAME ) {
			// update swapped TBP
			cmd.tbp[0] = DpyManager::raster[DPYR_IN_FRAME].tbp0;
		}
	}

	// Changed ?
	if( !changed )
		return;
	changed = FALSE;


	//
	// Common registers

	// Blend mode
	static uchar alphaMode[] = {
	//  A,	B,	EQU, FIXA,	FIXB
		0,	0,	0,	 0,		0,		// (BM_SELECT)
		10,	10,	0,	 255,	0,		// BM_NONE (FIXA=255, FIXB=0)
		2,	3,	0,	 0,		0,		// BM_FILTER
		2,	10, 0,	 0,		255,	// BM_ADD (FIXB=255)
		2,	10,	2,	 0,		255,	// BM_SUB (FIXB=255)
		2,	10,	0,	 0,		0,		// BM_MODULATE (FIXB=0)
		3,	2,	0,	 0,		0,		// BM_FILTER2
		10,	2,	0,	 255,	0,		// BM_ADD2 (FIXA=255)
		10,	2,	1,	 255,	0,		// BM_SUB2 (FIXA=255)
		10,	2,	0,	 0,		0,		// BM_MODULATE2 (FIXA=0)
		10,	10,	0,	 0,		255,	// BM_DUMMY (FIXA=0, FIXB=255)
		10,	10,	0,	 0,		0		// BM_ZERO (FIXA=FIXB=0)
	};
	uint bs  = mode & BS_MASK;
	uint bm  = mode & BM_MASK;
	bool bmf = FALSE;
	if( bm == BM_SELECT ) {	// select ?
		if( texturing && kbmp && kbmp->GetAlphaStatus() == NvBitmap::AS_VARIED ) {
			bm  = BM_FILTER;
			bmf = TRUE;
		} else {
			bm = BM_NONE;
		}
	}
	NV_ASSERT( bm>BM_SELECT && bm <= BM_ZERO );
	NV_COMPILE_TIME_ASSERT( BM_SELECT == (1<<13) );
	uint aM = (bm>>13) - 1;
	uint bA  = alphaMode[aM*5+0];
	uint bB  = alphaMode[aM*5+1];
	uint bE  = alphaMode[aM*5+2];
	uint bFA = alphaMode[aM*5+3];
	uint bFB = alphaMode[aM*5+4];
	uint bREF= uint(int(blendCte*255.0f));
	if( bs == BS_BUFFER ) {
		if( bA != 10 )	bA += 2;	// 2/3 -> 4/5
		if( bB != 10 )	bB += 2;	// 2/3 -> 4/5
	} else if( bs == BS_CTE ) {
		if( bA != 10 ) { bA = 10; bFA = (bA==2)?bREF:255U-bREF; }
		if( bB != 10 ) { bB = 10; bFB = (bB==2)?bREF:255U-bREF; }
	}
	cmd.blend = SCE_GE_SET_BLEND( bA, bB, bE );
	cmd.fix[0]= SCE_GE_SET_FIXA( bFA, bFA, bFA );
	cmd.fix[1]= SCE_GE_SET_FIXB( bFB, bFB, bFB );

	// Face culling
	uint cm = mode & CM_MASK;
	if( cm == CM_TWOSIDED ) {
		cmd.bce = SCE_GE_SET_BCE( 0 );
	} else {
		cmd.bce  = SCE_GE_SET_BCE( 1 );
		cmd.cull = SCE_GE_SET_CULL( (cm==CM_FRONTSIDED)?SCE_GE_CULL_CW:SCE_GE_CULL_CCW );
	}
	#ifdef DISABLE_FACECULLING
	cmd.bce = SCE_GE_SET_BCE( 0 );
	#endif

	// Alpha pass test
	uint aref = bmf ? 0 : int( alphaPass * 255.0f );
	cmd.atest = SCE_GE_SET_ATEST( SCE_GE_ATEST_GEQUAL, aref, 0xFF );

	// EN_WR_COLOR
	if( enflags & EN_WR_COLOR ) {
		uint32 abgr_mask = ~ ConvertRGBA( PSM_ABGR32, wrColorMask );
		cmd.pmsk[0] = SCE_GE_SET_PMSK1_RGB24( abgr_mask );
		cmd.pmsk[1] = SCE_GE_SET_PMSK2( abgr_mask>>24 );
	} else {
		cmd.pmsk[0] = SCE_GE_SET_PMSK1_RGB24( 0xFFFFFF );
		cmd.pmsk[1] = SCE_GE_SET_PMSK2( 0xFF );
	}

	// EN_RD_DEPTH
	cmd.ztest = SCE_GE_SET_ZTEST( (enflags&EN_RD_DEPTH)?SCE_GE_ZTEST_GEQUAL:SCE_GE_ZTEST_ALWAYS );

	// EN_WR_DEPTH
	cmd.zmsk = SCE_GE_SET_ZMSK( (!bmf && (enflags&EN_WR_DEPTH)) ? 0 : 1 );

	// EN_RD_STENCIL
	cmd.stest = SCE_GE_SET_STEST( (enflags&EN_RD_STENCIL)?SCE_GE_STEST_NOTEQUAL:SCE_GE_STEST_ALWAYS, 0, 0xFF );


	//
	// Texturing registers

	int	texId = kbmp ? kbmp->GetTRamId() : -1;
	ioContext->texId  = texId;
	cmd.tme	   		  = SCE_GE_SET_TME( int(texturing) );
	cmd.tflush_or_ret = texturing ? SCE_GE_SET_TFLUSH() : SCE_GE_SET_RET();

	if( texturing )
	{
		Vec2 suv = Vec2( srcUnpackS, srcUnpackS );
		Vec2 tuv = srcUnpackT + srcT;

		if( texId < 0 )
		{
			// raster as source
			cmd.cload = SCE_GE_SET_TSYNC();
			NV_ASSERT( src.Raster>DPYR_NULL && src.Raster<=DPYR_OFF_FRAME_C32Z32 );
			DpyManager::Raster& raster = DpyManager::raster[ int(src.Raster) ];
			cmd.zmsk     = raster.zmsk;
			cmd.tbp[0]   = raster.tbp0;
			cmd.tbw[0]   = raster.tbw0;
			cmd.tsize[0] = raster.tsize0;
			cmd.tpf		 = raster.tpf;
			cmd.tmode    = raster.tmode;
			suv.x *= raster.tratio.x;
			suv.y *= raster.tratio.y;
		}
		else
		{
			// bitmap as source
			const uint my_texcmd_bs = MOFFSET(frame::GeCallList,ret) - MOFFSET(frame::GeCallList,cbp);
			const uint td_texcmd_bs = sizeof(tram::TexDesc::GeCmd);
			NV_COMPILE_TIME_ASSERT( my_texcmd_bs == td_texcmd_bs );
			tram::TexReg*  treg  = tram::GetRegistered( texId );
			NV_ASSERT( treg );
			tram::TexDesc* tdesc = treg->desc;
			NV_ASSERT( tdesc );
			Memcpy( &cmd.cbp, &tdesc->geCmd, td_texcmd_bs );
		}

		// tex-function
		uint tf = mode & TF_MASK;
		if( tf == TF_DECAL || (caps&(CF_HAS_RGBA|CF_HAS_RGB)) == 0 )	cmd.tfunc = SCE_GE_SET_TFUNC( 3, 1, 0 );	// Neova's DECAL is PSP's REPLACE !
		else if( tf == TF_MODULATE )									cmd.tfunc = SCE_GE_SET_TFUNC( 0, 1, 0 );
		else if( tf == TF_HIGHLIGHT )									cmd.tfunc = SCE_GE_SET_TFUNC( 4, 1, 0 );	// Neova's HIGHLIGHT is PSP's ADD !
		else if( tf == TF_HIGHLIGHT2 )									cmd.tfunc = SCE_GE_SET_TFUNC( 1, 1, 0 );	// Neova's HIGHLIGHT2 is PSP's (blend-)DECAL !

		// tex-filter
		static uchar tfilter[] = { 0, 1, 4, 6, 5, 7 };
		uint tm = mode & TM_MASK;
		uint tr = mode & TR_MASK;
		NV_COMPILE_TIME_ASSERT( TM_NEAREST == (1<<3) );
		NV_COMPILE_TIME_ASSERT( TR_NEAREST_MIP0 == (1<<5) );
		uint tf_mag = (tm>>3)-1;
		uint tf_min = (tr>>5)-1;
		cmd.tfilter = SCE_GE_SET_TFILTER( tfilter[tf_min], tfilter[tf_mag] );

		// tex-OFFL
		cmd.tlevel = SCE_GE_SET_TLEVEL( SCE_GE_TLEVEL_VARIABLE1, mipOFFL );

		// tex-wrapping-mode
		uint tw = mode & TW_MASK;
		if( tw == TW_REPEAT_REPEAT )		cmd.twrap = SCE_GE_SET_TWRAP( 0, 0 );
		else if( tw == TW_REPEAT_CLAMP )	cmd.twrap = SCE_GE_SET_TWRAP( 0, 1 );
		else if( tw == TW_CLAMP_REPEAT )	cmd.twrap = SCE_GE_SET_TWRAP( 1, 0 );
		else if( tw == TW_CLAMP_CLAMP )		cmd.twrap = SCE_GE_SET_TWRAP( 1, 1 );

		// tex-mapping
		cmd.tmap = SCE_GE_SET_TMAP( SCE_GE_TMAP_TMN_UV, 0 );

		// st' = st * S + T
		cmd.suv[0] = SCE_GE_SET_SU( ExtFp24(suv.x) );
		cmd.suv[1] = SCE_GE_SET_SV( ExtFp24(suv.y) );
		cmd.tuv[0] = SCE_GE_SET_TU( ExtFp24(tuv.x) );
		cmd.tuv[1] = SCE_GE_SET_TV( ExtFp24(tuv.y) );
	}
}




