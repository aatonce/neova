/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include "NvHrdwTranslation.h"
#include <Nova.h>
#include <Windef.h>

using namespace nv;
using namespace Hrdw;


namespace
{
	struct _texture{
		LPDIRECT3DTEXTURE9	texture;
		uint 				width;
		uint				height;
		MipmapLevel			level;
		DWORD				usage;
		D3DPOOL				pool;
		D3DFORMAT			format;
		bool				validData;
	};

	sysvector<_texture>			textureA;	
	sysvector<uint16>			textureEmptyA;	
	int32						firstTextureEmpty;

	#define GET_FIRST_TEX_EMPTY(idx)								\
	if (firstTextureEmpty <0){										\
		_texture	NullValue;										\
		Memset(&NullValue,0,sizeof(_texture));						\
		uint addedsize = textureA.size()/2;							\
		uint newsize   = textureA.size() + addedsize;				\
		textureA.resize(newsize,NullValue);							\
		textureEmptyA.resize(newsize-1,0);							\
		for (uint i = 0 ; i < addedsize ; ++i)						\
			textureEmptyA[i] = newsize-1 - i;						\
		firstTextureEmpty = addedsize -1;							\
	}																\
	idx = textureEmptyA[firstTextureEmpty];							\
	firstTextureEmpty--; 


	#ifdef _NVCOMP_ENABLE_DBG
	bool isTexInit = FALSE;
	#define CHECK_TEX_INIT NV_ASSERT(isTexInit)
	#else
	#define CHECK_TEX_INIT 
	#endif //_NVCOMP_ENABLE_DBG
}


namespace Hrdw {
	bool				
	TextureOnDeviceLost		(											)
	{
		for (uint i = 0 ; i < textureA.size() ; ++i ) {
			if (textureA[i].texture && (textureA[i].pool == D3DPOOL_DEFAULT) && textureA[i].width && textureA[i].height) {
				SafeRelease(textureA[i].texture);
				textureA[i].texture		= NULL;
				textureA[i].validData	= FALSE;
			}
		}
		return TRUE;
	}

	bool
	TextureOnDeviceReset		(											)
	{
		bool creationDone = TRUE;
		for (uint i = 0 ; i < textureA.size() ; ++i ) {
			if ((!textureA[i].texture) && (textureA[i].pool == D3DPOOL_DEFAULT) && textureA[i].width && textureA[i].height) {

				creationDone &= (ddev->CreateTexture(	textureA[i].width,
														textureA[i].height,
														uint(textureA[i].level),
														(textureA[i].level!=1 ? D3DUSAGE_AUTOGENMIPMAP : 0) | textureA[i].usage,
														textureA[i].format,
														textureA[i].pool,
														&textureA[i].texture, NULL )== D3D_OK );
			}
		}
		return creationDone;
	}
}

void
Hrdw_InitTexture  		(	)
{
	#ifdef _NVCOMP_ENABLE_DBG
	NV_ASSERT(!isTexInit);
	isTexInit = TRUE;	
	#endif //_NVCOMP_ENABLE_DBG

	textureA.Init();
	textureEmptyA.Init();

	_texture texNull;
	Memset(&texNull,0,sizeof(_texture));

	textureA.resize(64,texNull);
	textureEmptyA.resize(64-1,0);

	for (uint i = 0 ; i < textureEmptyA.size() ; ++i ) 
		textureEmptyA[i] = 64-1-i;
	
	firstTextureEmpty= textureEmptyA.size()-1;
}

void
Hrdw_ShutTexture		(	)
{
	CHECK_TEX_INIT;

	for (uint i = 1 ; i < textureA.size() ; ++i ) 
		SafeRelease(textureA[i].texture);

	textureA.Shut();
	textureEmptyA.Shut();
	

	#ifdef _NVCOMP_ENABLE_DBG	
	isTexInit = FALSE;	
	#endif //_NVCOMP_ENABLE_DBG
}

Hrdw::Texture
Hrdw::CreateTexture		(uint 				inWidth				,
						 uint				inHeight			,
						 MipmapLevel		inLevel				,
						 TextureFormat 		inFormat			,
						 Usage		 		inUsage				)
{
	CHECK_TEX_INIT;
	NV_ASSERT(inWidth);	
	NV_ASSERT(inHeight);	

	uint16 idx;
	GET_FIRST_TEX_EMPTY(idx);

	int D3DUsage =  ((inLevel!=1) ? D3DUSAGE_AUTOGENMIPMAP : 0); // DYNAMIC is incompatible with MANAGED :) ;

	_texture & t = textureA[idx];

	HRESULT hr = ddev->CreateTexture(	inWidth,
										inHeight,
										uint(inLevel),
										D3DUsage,
										TEXTUREFORMAT_TRANSLATION[inFormat],
										D3DPOOL_MANAGED,
										&t.texture, NULL );				
	NV_ASSERT_DX(hr);
	
	t.width		= inWidth;
	t.height	= inHeight;
	t.level		= inLevel;
	t.usage		= D3DUsage;
	t.pool		= D3DPOOL_MANAGED;
	t.format	= TEXTUREFORMAT_TRANSLATION[inFormat];
	t.validData	= TRUE;

	return (hr == D3D_OK)?Texture(idx):Texture(0);
}											 
											 
void
Hrdw::ReleaseTexture		(Texture 		inTex					)
{
	CHECK_TEX_INIT;
	if( inTex )
	{
		NV_ASSERT(inTex < textureA.size());
		NV_ASSERT(textureA[inTex].texture != NULL);

		SafeRelease(textureA[inTex].texture);
		Memset(&textureA[inTex],0,sizeof(_texture));

		firstTextureEmpty++;
		textureEmptyA[firstTextureEmpty] = inTex;
	}
}


void
Hrdw::SetTexture			(Texture 			inTex,
							 uint				inSampler				)
{
	CHECK_TEX_INIT;
	NV_ASSERT(inTex < textureA.size());		
	NV_ASSERTC(!inTex || textureA[inTex].validData,"A Lost device event have been occured, and a texture was not reloaded. ");
	HRESULT hr = ddev->SetTexture(inSampler,textureA[inTex].texture);
	NV_ASSERT_DX(hr);
}

void *
Hrdw::LockTexture			(Texture 			inTex,		
							 uint				inLockFlag,
							 int &				outPitch,
							 uint 				inLeft,
							 uint				inTop,
							 uint 				inRight,
							 uint				inBottom)
{
	CHECK_TEX_INIT;
	NV_ASSERT(inTex);		
	NV_ASSERT(inTex < textureA.size());
	NV_ASSERT(textureA[inTex].texture != NULL);
	
	int D3DLock = 0;
	for (uint i = 0 ; i < 5 ; ++i) {
		if (inLockFlag & (1<<i))
		D3DLock |= LOCKFLAGS_TRANSLATION[i];				
	}

	D3DLOCKED_RECT lrect;
	HRESULT hr;
	if (inRight == inLeft && inBottom == inTop)
		hr = textureA[inTex].texture->LockRect( 0, &lrect, NULL,D3DLock);
	else {
		RECT rect = {inLeft,inTop,inRight,inBottom};
		hr = textureA[inTex].texture->LockRect( 0, &lrect,&rect,D3DLock & (~D3DLOCK_DISCARD));			
	}	
	NV_ASSERT_DX(hr);
	outPitch = lrect.Pitch;
	return lrect.pBits;
}

void
Hrdw::UnlockTexture		(Texture 		inTex,
						 bool 			genMipmapNow)
{
	CHECK_TEX_INIT;
	NV_ASSERT(inTex);		
	NV_ASSERT(inTex < textureA.size());
	NV_ASSERT(textureA[inTex].texture != NULL);

	HRESULT hr = textureA[inTex].texture->UnlockRect( 0 );
	NV_ASSERT_DX(hr);
	
	if (genMipmapNow)
		textureA[inTex].texture->GenerateMipSubLevels();
}

void
Hrdw::SetPaletteEntries	(uint				inPaletteNum,
						 uint32 *			inEntry	)
{
	HRESULT hr = ddev->SetPaletteEntries(inPaletteNum,  (PALETTEENTRY *) inEntry);
	NV_ASSERT_DX(hr);
}

void
Hrdw::SetCurrentPalette	(uint				inPaletteNum)
{
	HRESULT hr = ddev->SetCurrentTexturePalette( inPaletteNum );
	NV_ASSERT_DX(hr);
}

int
Hrdw::GetMipmapLevel		(Texture 		inTex					)
{	
	CHECK_TEX_INIT;
	if (! inTex) return 0;

	NV_ASSERT(inTex < textureA.size());
	NV_ASSERT(textureA[inTex].texture != NULL);
	return int(textureA[inTex].texture->GetLevelCount());
}	

bool
Hrdw::TexValidData	(Texture 			inTex	)
{
	NV_ASSERT(inTex < textureA.size());
	NV_ASSERT(textureA[inTex].texture != NULL);
	return textureA[inTex].validData;
}

void
Hrdw::TexSetValidData		(Texture 	inTex	)
{
	NV_ASSERT(inTex < textureA.size());
	NV_ASSERT(textureA[inTex].texture != NULL);
	textureA[inTex].validData = TRUE;
}