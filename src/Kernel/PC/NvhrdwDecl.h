/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include "NvHrdwTranslation.h"
#include <Nova.h>

using namespace nv;
using namespace Hrdw;

#define MAX_VERTEX_DECLARATION 256 // Max 256 !!!
#define MAX_VERTEX_ELEMENT 		32 // TODO : this number can be reduce


namespace
{
	D3DVERTEXELEMENT9 					TmpElement[MAX_VERTEX_ELEMENT];
	uint								CurElement;										
										
	struct DeclarationEntry {
		LPDIRECT3DVERTEXDECLARATION9 	Decl;
		uint8							NbElem;
		D3DVERTEXELEMENT9 			  	Elem[MAX_VERTEX_ELEMENT];
	};

	DeclarationEntry *					(DeclA[8][6][MAX_VERTEX_DECLARATION]);
	//DeclarationEntry * ***				DeclA = NULL;
							    		// 1 array for each number of stream [1 .. 8]
										// for each number of stream, 1 array for each number of
										// elements : [1,2,3,4,5,more than 5],[2,3,4,5,6,more than 6]
										// [2,3,4,5,6,more than 6] .. [7,8,9,10,11,more than 11]
										
	uint16								NbDecl[8][6];
	uint16								SortedIndex[8][6][MAX_VERTEX_DECLARATION];
	
	// Size ~= 130Ko for NbDecl + SortedIndex + DeclA


	// return -1 D1 < D2, 0 D1 == D2, 1 D1 > D2
	inline int				
	_CompareDeclarationEntry(DeclarationEntry &D1,
							 DeclarationEntry &D2)
	{
		if (D1.NbElem < D2.NbElem) return -1;
		if (D1.NbElem > D2.NbElem) return 1;
		
		for (uint i = 0 ; i < D1.NbElem ; ++i ) {
			if (D1.Elem[i].Stream != D2.Elem[i].Stream)
				if (D1.Elem[i].Stream < D2.Elem[i].Stream) return -1;
				else return 1;
			if (D1.Elem[i].Offset != D2.Elem[i].Offset)
				if (D1.Elem[i].Offset < D2.Elem[i].Offset) return -1;
				else return 1;				
			if (D1.Elem[i].Type != D2.Elem[i].Type)
				if (D1.Elem[i].Type < D2.Elem[i].Type) return -1;
				else return 1;				
			if (D1.Elem[i].Method != D2.Elem[i].Method)
				if (D1.Elem[i].Method < D2.Elem[i].Method) return -1;
				else return 1;				
			if (D1.Elem[i].Usage != D2.Elem[i].Usage)
				if (D1.Elem[i].Usage < D2.Elem[i].Usage) return -1;
				else return 1;				
			if (D1.Elem[i].UsageIndex != D2.Elem[i].UsageIndex)
				if (D1.Elem[i].UsageIndex < D2.Elem[i].UsageIndex) return -1;
				else return 1;																				
		}
		
		return 0;
    }
    
	inline void // A remplacer par un memcopy
	_CopyDeclarationEntry(DeclarationEntry * D1 , 
						  DeclarationEntry * D2)
    {
		nv::libc::Memcpy(D2,D1,	 sizeof(DeclarationEntry));
	}
	
	inline uint16 
	_InsertDeclarationEntry(D3DVERTEXELEMENT9 inElements[MAX_VERTEX_ELEMENT], uint inNbElem){
		DeclarationEntry d;		
		uint nbStream = 1;

		// Find nbStream and create DeclarationEntry d		
		Memcpy(d.Elem,inElements,sizeof(D3DVERTEXELEMENT9)); //d.Elem[0] = inElements[0];		

		for (uint i = 1 ; i < inNbElem ; ++i){
			bool SameStream = FALSE;
			for (uint j = 0 ; j < i ; ++j){
				if (inElements[j].Stream == inElements[i].Stream){
					SameStream = TRUE;
					break;
				}
			}
			if (!SameStream)
				nbStream++;
			Memcpy(d.Elem+i,inElements+i,sizeof(D3DVERTEXELEMENT9)); //d.Elem[i] = inElements[i];;		
		}
		d.Elem[inNbElem] = inElements[inNbElem];	// save DECL_END() 
		d.NbElem = inNbElem + 1;
		
		NV_ASSERT(nbStream <= 8);
		NV_ASSERT(inNbElem >= nbStream);

		DeclarationEntry ** 	ptrDecl;
		uint16 * 			 	ptrNbDecl;		
		uint16 *				ptrShortedIndex;
		uint tmp = inNbElem - nbStream;
		if (tmp > 5) tmp = 5;
		
		ptrDecl   = DeclA[nbStream-1][tmp];
		ptrNbDecl = (NbDecl[nbStream-1])+tmp;
		ptrShortedIndex = SortedIndex[nbStream-1][tmp];

					
		NV_ASSERT(*ptrNbDecl < MAX_VERTEX_DECLARATION -1 );

		FlushPrintf();

		if (*ptrNbDecl == 0){
			HRESULT hr = ddev->CreateVertexDeclaration( d.Elem, & d.Decl);		
			NV_ASSERT_DX(hr);
			DeclarationEntry * ptrd = (DeclarationEntry *) EngineMalloc(sizeof(DeclarationEntry));
			_CopyDeclarationEntry(&d,ptrd);
			ptrDecl[0] = ptrd;
			ptrShortedIndex[0]=0;
			(*ptrNbDecl)++;
			return (uint16) ((nbStream - 1) << 13) + (tmp << 9 )+(1<<8);
		}
		else {
			uint 	upperBound = *ptrNbDecl, 
					lowerBound = 0 , 
					cur = *ptrNbDecl /2,
					curPred = *ptrNbDecl /2,
					comp;
			bool isIn = FALSE;					
			
			while (1){
				comp = _CompareDeclarationEntry(d,   *(ptrDecl[ptrShortedIndex[cur]])  );
				if (comp < 0) 	 		// == -1 => d < ptrDecl[cur]
					upperBound = cur;
				else if (comp > 0)		// ==  1 => d > ptrDecl[cur]
					lowerBound = cur;
				else {					// ==  0 => d == ptrDecl[cur]
					isIn = TRUE;
					break;					
				}
				curPred = cur;
				cur = lowerBound + ((upperBound - lowerBound) / 2);
				if (upperBound == lowerBound || cur == curPred)
					break;
			}
			if (isIn){  // Declaration already exist, it is cur
				// ret = Ob sss0eee1iiiiiiii : s = Stream Number, e = element Number , i = array index	
				//	     Ob 0000000100000000 => valid ret != 0
				return (uint16) ((nbStream - 1) << 13) + (tmp << 9 )+(1<<8) + (uint8) ptrShortedIndex[cur];
			}
			else {		// Declaration do not exist
				HRESULT hr = ddev->CreateVertexDeclaration( d.Elem, & d.Decl);
				NV_ASSERT_DX(hr);
				DeclarationEntry * ptrd = (DeclarationEntry *) EngineMalloc(sizeof(DeclarationEntry));
				_CopyDeclarationEntry(&d,ptrd);
				ptrDecl[*ptrNbDecl] = ptrd;
				uint16 ret = (uint16) ((nbStream - 1) << 13) + (tmp << 9 )+(1<<8) + (uint8) (*ptrNbDecl);				
				(*ptrNbDecl)++;
				
				if (cur == curPred){ // place after Cur;
					uint16 	val = ptrShortedIndex[cur +1],
							val2;
					ptrShortedIndex[cur+1] = (*ptrNbDecl) - 1;
					for (uint i = cur+2 ; i < (*ptrNbDecl) ; ++ i ){
						val2 = val;
						val = ptrShortedIndex[i];
						ptrShortedIndex[i] = val2;
					}
				}
				else {				 // place before Cur;
					uint16 	val = ptrShortedIndex[cur],
							val2;
					ptrShortedIndex[cur] = (*ptrNbDecl) - 1;
					for (uint i = cur+1 ; i < (*ptrNbDecl) ; ++ i ){
						val2 = val;
						val = ptrShortedIndex[i];
						ptrShortedIndex[i] = val2;
					}
				}
				
				return ret;
			}
		}
	}
	
	inline	DeclarationEntry * 
	_GetDeclarationEntry(uint16 id)
	{
		uint8 nbStream 	= (id >>13) ;
		uint8 nbElem 	= (id & 0x0FFF) >> 9;
		uint index  	= (id & 0x00FF);	
		
		NV_ASSERT(nbStream < 8);
		NV_ASSERT(nbElem < 6);
		NV_ASSERT(index < NbDecl[nbStream][nbElem]);
		
		return DeclA[nbStream][nbElem][index];
	}
}

Hrdw::DeclIndex CurDecl = 0;

void
Hrdw_InitDeclaration()
{
	nv::libc::Memset(DeclA,0,sizeof(DeclarationEntry*) * 8 * 6 * MAX_VERTEX_DECLARATION);
	nv::libc::Memset(SortedIndex,0,sizeof(uint16) * 8 * 6 * MAX_VERTEX_DECLARATION);
	nv::libc::Memset(NbDecl,0,sizeof(uint16) * 8 * 6);
}

void
Hrdw_ShutDeclaration()
{
	for (uint i = 0 ; i < 8 ; ++ i ){
		for (uint j = 0 ; j < 6 ; ++j) {
			NV_ASSERT(NbDecl[i][j] <= MAX_VERTEX_DECLARATION);
			for (uint k = 0; k < NbDecl[i][j] ; ++k) {
				SafeRelease(DeclA[i][j][k]->Decl);
				EngineFree(DeclA[i][j][k]);
			}
		}
	}					
	nv::libc::Memset(DeclA,0,sizeof(DeclarationEntry*) * 8 * 6 * MAX_VERTEX_DECLARATION);
	nv::libc::Memset(SortedIndex,0,sizeof(uint16) * 8 * 6 * MAX_VERTEX_DECLARATION);
	nv::libc::Memset(NbDecl,0,sizeof(uint16) * 8 * 6);
}

void
Hrdw::BeginVertexDeclaration()
{
	CurElement = 0;	
}

void
Hrdw::AddToVertexDeclaration(	uint8					Stream,
								uint8					Offset,
								ElementType				Type,			
								ElementUsage			Usage,
								uint8					Index)
{
	NV_ASSERT(CurElement < MAX_VERTEX_ELEMENT - 1);
	
	TmpElement[CurElement].Stream		= Stream;
	TmpElement[CurElement].Offset		= Offset;
	TmpElement[CurElement].Type			= ELEMENTTYPE_TRANSLATION[Type];
	TmpElement[CurElement].Method		= D3DDECLMETHOD_DEFAULT;
	TmpElement[CurElement].Usage		= ELEMENTUSAGE_TRANSLATION[Usage];
	TmpElement[CurElement].UsageIndex	= Index;
	CurElement++;
}

Hrdw::DeclIndex 
Hrdw::FinalizeVertexDeclaration()
{
	D3DVERTEXELEMENT9 tmpe = D3DDECL_END();
	TmpElement[CurElement] = tmpe;
	uint16 ret = _InsertDeclarationEntry(TmpElement,CurElement);	
	CurElement = 0;		
	return DeclIndex(ret);
}

bool	//ActiveDeclaration
Hrdw::SetVertexDeclaration(DeclIndex inDecl)
{
	if ( inDecl == CurDecl) {
		return true;
	}
	CurDecl = inDecl;
	
	DeclarationEntry * ptr = _GetDeclarationEntry(inDecl);
	HRESULT hr = ddev->SetVertexDeclaration( ptr->Decl );
	NV_ASSERT_DX(hr);
	return ( hr == D3D_OK );	
}