/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/PC/NvHrdwTools.h>

namespace {
	Hrdw::DeclIndex			declTex		= 0;
	Hrdw::VertexBuffer		vertexBTex	= 0;
	uint					vbStrideTex	= 0;

	Hrdw::DeclIndex			decl		= 0;
	Hrdw::VertexBuffer		vertexB		= 0;
	uint					vbStride	= 0;

	Hrdw::ShaderProgram		vsCopy		= 0;
	Hrdw::ShaderProgram		psCopy		= 0;
	Hrdw::ShaderProgram		vsCopyTex	= 0;
	Hrdw::ShaderProgram		psCopyTex	= 0;

	char SimpleCopyCodePHL[]	= {
		#include "SimpleCopy.phl.bin2h"		
	};
	char SimpleCopyCodeVHL[]	= {
		#include "SimpleCopy.vhl.bin2h"		
	};
	char SimpleCopyCodePHLTex[] = { 
		#include "SimpleCopyTex.phl.bin2h"
	};
	char SimpleCopyCodeVHLTex[] = {
		#include "SimpleCopyTex.vhl.bin2h"
	};

	void SetUpShaders() 
	{
		if (!vsCopy) {
			vsCopy		= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, SimpleCopyCodeVHL	,sizeof(SimpleCopyCodeVHL)		,Hrdw::NV_VS_2, "main");
			psCopy		= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, SimpleCopyCodePHL	,sizeof(SimpleCopyCodePHL)		,Hrdw::NV_PS_2, "main");
			vsCopyTex	= Hrdw::CreateShaderProgram(Hrdw::NV_VERTEXSHADER | Hrdw::NV_HLSL, SimpleCopyCodeVHLTex	,sizeof(SimpleCopyCodeVHLTex)	,Hrdw::NV_VS_2, "main");
			psCopyTex	= Hrdw::CreateShaderProgram(Hrdw::NV_PIXELSHADER  | Hrdw::NV_HLSL, SimpleCopyCodePHLTex	,sizeof(SimpleCopyCodePHLTex)	,Hrdw::NV_PS_2, "main");
			NV_ASSERT (Hrdw_CheckMappingByName(vsCopyTex,Hrdw::tools::VsCopyMappingTex,offsetTexCoord,"pOffsetTexCoord"));
			NV_ASSERT (Hrdw_CheckMappingByName(vsCopy   ,Hrdw::tools::VsCopyMapping   ,color,"color"));
		}
	}

	void SetUpVertexBuffer()
	{
		if ( ! decl) {
			Hrdw::BeginVertexDeclaration();
			vbStrideTex	= 0;
			Hrdw::AddToVertexDeclaration(0,vbStrideTex,Hrdw::NV_FLOAT3,Hrdw::NV_POSITION);
			vbStrideTex += 12;
			Hrdw::AddToVertexDeclaration(0,vbStrideTex,Hrdw::NV_FLOAT2,Hrdw::NV_TEXCOORD);
			vbStrideTex += 8;
			declTex =Hrdw::FinalizeVertexDeclaration();

			Hrdw::BeginVertexDeclaration();
			vbStride	= 0;
			Hrdw::AddToVertexDeclaration(0,vbStride,Hrdw::NV_FLOAT3,Hrdw::NV_POSITION);
			vbStride += 12;
			decl =Hrdw::FinalizeVertexDeclaration();
		}
		if (!vertexB || !Hrdw::VBValidData(vertexB) || !Hrdw::VBValidData(vertexBTex)){
			if (!vertexBTex)
				vertexBTex		= Hrdw::CreateVertexBuffer(vbStrideTex * 6	, Hrdw::NV_STATIC);
			if (!vertexB)
				vertexB			= Hrdw::CreateVertexBuffer(vbStride * 6		, Hrdw::NV_STATIC);

			Hrdw::VBSetValidData(vertexB);
			Hrdw::VBSetValidData(vertexBTex);

			uint8 *ptrTex	= (uint8*)Hrdw::LockVertexBuffer(vertexBTex,0,0,0);
			uint8 *ptr		= (uint8*)Hrdw::LockVertexBuffer(vertexB,0,0,0);
		
			Vec3 * v = (Vec3 * ) ptrTex;
			Vec2 * t = (Vec2 * )(ptrTex + 12);
			v->x = -1.0f;v->y = 1.0f ;v->z = 0.5f;
			t->x = 0.0f;t->y = 0.0f;
			Vec3Copy((Vec3 * ) ptr,v);
			ptrTex	+=vbStrideTex;
			ptr		+=vbStride;

			v = (Vec3 * ) ptrTex;
			t = (Vec2 * ) (ptrTex + 12);
			v->x = 1.0f ; v->y = 1.0f ;v->z = 0.5f;
			t->x = 1.0f;t->y = 0.0f;
			Vec3Copy((Vec3 * ) ptr,v);
			ptrTex	+=vbStrideTex;
			ptr		+=vbStride;

			v = (Vec3 * ) ptrTex;
			t = (Vec2 * ) (ptrTex + 12);
			v->x = 1.0f ;v->y = -1.0f;v->z = 0.5f;
			t->x = 1.0f;t->y = 1.0f;
			Vec3Copy((Vec3 * ) ptr,v);
			ptrTex	+=vbStrideTex;
			ptr		+=vbStride;

			v = (Vec3 * ) ptrTex;
			t = (Vec2 * ) (ptrTex + 12);
			v->x = -1.0f;v->y = 1.0f ;v->z = 0.5f;
			t->x = 0.0f;t->y = 0.0f;
			Vec3Copy((Vec3 * ) ptr,v);
			ptrTex	+=vbStrideTex;	
			ptr		+=vbStride;

			v = (Vec3 * ) ptrTex;
			t = (Vec2 * ) (ptrTex + 12);
			v->x = 1.0f ;v->y = -1.0f;v->z = 0.5f;
			t->x = 1.0f;t->y = 1.0f;
			Vec3Copy((Vec3 * ) ptr,v);
			ptrTex	+=vbStrideTex;
			ptr		+=vbStride;

			v = (Vec3 * ) ptrTex;
			t = (Vec2 * ) (ptrTex + 12);
			v->x = -1.0f;v->y = -1.0f;v->z = 0.5f;
			t->x = 0.0f;t->y = 1.0f;
			Vec3Copy((Vec3 * ) ptr,v);

			Hrdw::UnlockVertexBuffer(vertexBTex);
			Hrdw::UnlockVertexBuffer(vertexB);
		}
	}

}

void Hrdw::tools::ReleaseData		(					)
{
	if (vsCopy){
		Hrdw::ReleaseShader(vsCopy);
		Hrdw::ReleaseShader(psCopy);
		Hrdw::ReleaseShader(vsCopyTex);
		Hrdw::ReleaseShader(psCopyTex);
	}

	if (decl) {
		Hrdw::ReleaseVertexBuffer(vertexBTex);
		Hrdw::ReleaseVertexBuffer(vertexB);
	}
}

void Hrdw::tools::SetVertexBuffer	(bool inWithTex){
	SetUpVertexBuffer();
	if (inWithTex) {
		Hrdw::SetVertexDeclaration(declTex);
		Hrdw::SetVertexBuffer(vertexBTex,0,0,vbStrideTex);
	}
	else {
		Hrdw::SetVertexDeclaration(decl);
		Hrdw::SetVertexBuffer(vertexB,0,0,vbStride);
	}
}

void Hrdw::tools::DrawSprite			(){
	Hrdw::DrawPrimitive(Hrdw::NV_TRIANGLELIST,2,0);
}

void Hrdw::tools::SetShader(bool inWithTex, bool inVertexS, bool inPixelS){
	SetUpShaders();

	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool TNL = RMode < Hrdw::RM_Limited;

	if (inWithTex) {
		if (inVertexS)
			Hrdw::SetShaderProgram(vsCopyTex); 
		if (inPixelS && !TNL)
			Hrdw::SetShaderProgram(psCopyTex);
	}
	else {
		if (inVertexS)
			Hrdw::SetShaderProgram(vsCopy); 
		if (inPixelS && !TNL)
			Hrdw::SetShaderProgram(psCopy);
	}

	if (TNL && inPixelS) {
		if (inWithTex) {
			Hrdw::SetTextureStageState( 0, Hrdw::NV_COLOROP,	Hrdw::NV_TOP_SELECTARG2	);
			Hrdw::SetTextureStageState( 0, Hrdw::NV_ALPHAOP,	Hrdw::NV_TOP_SELECTARG2	);					
		}
		else{
			Hrdw::SetTextureStageState( 0, Hrdw::NV_COLOROP,   Hrdw::NV_TOP_DISABLE		);
			Hrdw::SetTextureStageState( 0, Hrdw::NV_ALPHAOP,   Hrdw::NV_TOP_DISABLE		);
		}
	}
}