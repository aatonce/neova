/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#ifdef _DEBUG



#define NV_ASSERT_DX( ret )			\
	{	if ((ret)!= D3D_OK)			\
			OutputDXError(ret);		\
		NV_ASSERT(ret == D3D_OK);	\
	}

#else 
#define NV_ASSERT_DX( ret ) {}
#endif //_DEBUG

/*
#if defined(_NVENGINE) && defined(new)
	#undef		new
	#undef		delete
	#include	<d3d9.h>
	#include	<d3dx9.h>
	#undef		new
	#undef		delete
	#define		new					$illegal new operator in engine part$
	#define		delete				$illegal delete operator in engine part$
#else
	#include	<d3d9.h>
	#include	<d3dx9.h>
#endif
*/

namespace
{

	#ifdef _DEBUG
	struct D3D_Error
	{
       HRESULT     hr;
       PCSTR       text;
	};

    D3D_Error d3dError[] =
	{
        {     D3D_OK,                                   "D3D Error: No error occurred."                                                             },
        {     D3DERR_CONFLICTINGRENDERSTATE,			"D3D Error: The currently set render states cannot be used together."        },
        {     D3DERR_CONFLICTINGTEXTUREFILTER,			"D3D Error: The current texture filters cannot be used together." },
        {     D3DERR_CONFLICTINGTEXTUREPALETTE,			"D3D Error: The current textures cannot be used simultaneously. This generally occurs when a multitexture device requires that all palletized textures simultaneously enabled also share the same palette." },
        {     D3DERR_DEVICELOST,                        "D3D Error: The device is lost and cannot be restored at the current time, so rendering is not possible." },
        {     D3DERR_DEVICENOTRESET,					"D3D Error: The device cannot be reset."     },
        {     D3DERR_DRIVERINTERNALERROR,				"D3D Error: Internal driver error."     },
        {     D3DERR_INVALIDCALL,                       "D3D Error: The method call is invalid. For example, a method's parameter may have an invalid value." },
        {     D3DERR_INVALIDDEVICE,						"D3D Error: The requested device type is not valid."     },
        {     D3DERR_MOREDATA,							"D3D Error: There is more data available than the specified buffer size can hold."  },
        {     D3DERR_NOTAVAILABLE,						"D3D Error: This device does not support the queried technique."    },
        {     D3DERR_NOTFOUND,							"D3D Error: The requested item was not found." },
        {     D3DERR_OUTOFVIDEOMEMORY,					"D3D Error: Direct3D does not have enough display memory to perform the operation."     },
        {     D3DERR_TOOMANYOPERATIONS,					"D3D Error: The application is requesting more texture-filtering operations than the device supports."     },
        {     D3DERR_UNSUPPORTEDALPHAARG,				"D3D Error: The device does not support a specified texture-blending argument for the alpha channel." },
        {     D3DERR_UNSUPPORTEDALPHAOPERATION,			"D3D Error: The device does not support a specified texture-blending operation for the alpha channel." },
        {     D3DERR_UNSUPPORTEDCOLORARG,				"D3D Error: The device does not support a specified texture-blending argument for color values." },
        {     D3DERR_UNSUPPORTEDCOLOROPERATION,			"D3D Error: The device does not support a specified texture-blending operation for color values." },
        {     D3DERR_UNSUPPORTEDFACTORVALUE,			"D3D Error: The device does not support the specified texture factor value." },
        {     D3DERR_UNSUPPORTEDTEXTUREFILTER,			"D3D Error: The device does not support the specified texture filter." },
        {     D3DERR_WRONGTEXTUREFORMAT,				"D3D Error: The pixel format of the texture surface is not valid." },
        {     E_FAIL,                                   "D3D Error: An undetermined error occurred inside the Direct3D subsystem."      },
        {     E_INVALIDARG,                             "D3D Error: An invalid parameter was passed to the returning function." },
        {     E_OUTOFMEMORY,                            "D3D Error: Direct3D could not allocate sufficient memory to complete the call."      },
    };

	void OutputDXError(HRESULT  inRet)
	{
		uint N = sizeof(d3dError) / sizeof(D3D_Error);
		for( uint i = 0 ; i < N ; i++ ) {
			if( inRet == d3dError[i].hr ) {
				DebugPrintf("%s, No : 0x%x\n", d3dError[i].text,inRet);
				return ;
			}
		}
		DebugPrintf("D3D Error No : 0x%x\n",inRet);
	}
	#endif // _DEBUG

}

#include "NvHrdwTranslation.h"


namespace
{

	NV_D3D					dd3d	= NULL;
	NV_D3DDEVICE			ddev	= NULL;	
	NV_D3DDEVTYPE			adapterDevType;
	uint32					adapter;
	NV_D3DCAPS				ddevCaps;

}


#include "NvHrdwDecl.h"
#include "NvHrdwShader.h"
#include "NvHrdwState.h"
#include "NvHrdwStream.h"
#include "NvHrdwTexture.h"
#include "NvHrdwMisc.h"
#include "NvHrdwCaps.h"






