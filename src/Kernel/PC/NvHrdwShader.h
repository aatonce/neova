/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include "NvHrdwTranslation.h"
#include <Nova.h>

//#define USE_CG

#ifdef USE_CG
#include <CG/cg.h>
#include <CG/cgD3D9.h> 
#endif

#include <stdio.h>

using namespace nv;
using namespace Hrdw;

#define MAX_DX_MACRO 15
#define DEFAULT_MACRO_STRING_LENGTH 64u

namespace 
{
	struct _Shader {
		uint8 Type; // 
		union {
			LPDIRECT3DVERTEXSHADER9 VS;
			LPDIRECT3DPIXELSHADER9  PS;
		}D3DShader;
		union {
			LPD3DXCONSTANTTABLE		D3DConstantTable; 
			#ifdef USE_CG
			CGprogram				CGShader;
#endif
		}Lang;			

#ifdef _NVCOMP_ENABLE_DBG
		uint16 idCst;
		uint8  nbCst;
#endif //_NVCOMP_ENABLE_DBG
	};

#ifdef USE_CG
	CGcontext					cgContext;
#endif
	sysvector<_Shader>			shaderA;
	sysvector<uint16>			shaderEmptyA;	
	int32						firstShaderEmpty;

	#define GET_FIRST_SHADER_EMPTY(idx)								\
	if (firstShaderEmpty <0){										\
		_Shader		__nullValue;									\
		Memset(&__nullValue,0,sizeof(_Shader));						\
		__nullValue.Type = NV_UNDEF;								\
		uint __addedsize = shaderA.size()/2;						\
		uint __newsize   = shaderA.size() + __addedsize;			\
		shaderA.resize(__newsize,__nullValue);						\
		shaderEmptyA.resize(__newsize-1,0);							\
		for (uint __i = 0 ; __i < __addedsize ; ++__i)				\
			shaderEmptyA[__i] = __newsize-1 - __i;					\
		firstShaderEmpty = __addedsize -1;							\
	}																\
	idx = shaderEmptyA[firstShaderEmpty];							\
	firstShaderEmpty--;

#ifdef _NVCOMP_ENABLE_DBG
	struct CGConstant{
		char name[128];
		uint16 reg;
		uint16 nbReg;
		~CGConstant(){}
	};

	sysvector<CGConstant> cstA; // use only for CG shaders

	void FindConstantRegisterInformation(const char * progSrc,uint8 & nbCst){
		uint k,i=0;
		uint start = cstA.size();
		nbCst = 0;
		CGConstant cst;
		static const char * VarToken = "//var ";
		if (progSrc[i] != '\0'){
			while (1) {
				k=0; 
				while ( (progSrc[i] == ' ' || progSrc[i] == '\t') && progSrc[i] != '\n' && progSrc[i] != '\0' )
					i++; // find the first word
				while (k < 6 && progSrc[i] != '\n' && progSrc[i] != '\0' ){
					if (progSrc[i] != VarToken[k]){
						break;
					}
					i++;k++;
				}
				if (k == 6) {  // this line have var definition
					i++;
					while (progSrc[i] != ' ' && progSrc[i] != '\n' && progSrc[i] != '\0' ) i++;// skip type
					i++;

					uint j = 0;				
					while (j < 255 && progSrc[i] != ' ' && progSrc[i] != '[' && progSrc[i] != '\n' && progSrc[i] != '\0' ){
						cst.name[j] = progSrc[i];
						i++;j++;
					}
					cst.name[j] = '\0';
					while (progSrc[i] != ':' && progSrc[i] != '\n' && progSrc[i] != '\0' ) i++;
					i++;
					while (progSrc[i] != ':' && progSrc[i] != '\n' && progSrc[i] != '\0' ) {
						if(progSrc[i] != ' ') goto InputVar;
						i++;
					}
					i++;
					while (progSrc[i] != '[' && progSrc[i] != '\n' && progSrc[i] != '\0' ) i++;
					i++;
					char * stop;
					cst.reg = (uint) libc::Strtoul(progSrc+i,&stop,10);
					NV_ASSERT(*stop == ']');
					while (progSrc[i] != ',' && progSrc[i] != ':' && progSrc[i] != '\n' && progSrc[i] != '\0' ) i++;
					cst.nbReg=1;
					if (progSrc[i] == ','){
						i+=2; // skip space
						cst.nbReg = (uint) libc::Strtoul(progSrc+i,&stop,10);
						NV_ASSERT(*stop == ' ');
					}		
					if (nbCst!=0 && libc::Strcmp(cst.name,(cstA)[cstA.size()-1].name) == 0) {
						(cstA)[cstA.size()-1].reg   = min((cstA)[cstA.size()-1].reg, cst.reg);
						(cstA)[cstA.size()-1].nbReg = (cstA)[cstA.size()-1].nbReg + cst.nbReg;
					}
					else {
						cstA.push_back(cst);
						nbCst++;
					}
				}
	InputVar :
				while ( progSrc[i] != '\n' && progSrc[i] != '\0' ) 
					i++; // goto new line or the end of string

				if (progSrc[i] == '\0') break;
				i++;
			}
		}

		for (uint cpt = start; cpt < start+nbCst ; ++ cpt) {
			libc::Printf("%s : %u , %u \n",cstA[cpt].name,cstA[cpt].reg,cstA[cpt].nbReg);					
		}
	}
#endif //_NVCOMP_ENABLE_DBG
	
	
	struct ReservedConstant {
		const char *	name;
		uint8			nameLenght;
		uint8			reg;
	};
	static const ReservedConstant RCA [] = {
		{"ModelViewPR",11,200},
		{"ModelViewTR",11,204}
	};
	uint nbReservedConstant = 2;

	void CreateReservedZone(char * file){
	/*	char * buff, * word;
		uint size, nb;
		file::DumpFromFile(file,((pvoid &)buff),size);
		uint i = 0;
		while (buff[i] != '\0') {
//////		
			nb = 0;
			while ( buff[i] < 65 || (buff[i] > 90 && buff[i] < 97) || buff[i] > 122 || buff[i]!= 45 || buff[i]!= 95 ) i++;
			word = buff+i;
			while ( (buff[i] >= 65 && buff[i] <= 90) || ( buff[i] >= 97 && buff[i] <= 122) || buff[i]== 45 || buff[i]== 95 ) i++, nb++;

			for ( uint j = 0 ; j < nbReservedConstant ; ++j ) {
				if (nb == RCA[j].nameLenght && libc::Strncmp(RCA[j].name,word,nb) == 0 ){
					// find a reservedWord
				}
			}
//////
			while (buff[i] != '\n' ) i++;
			i++;
		}	*/	
	}


	D3DXMACRO* StringToDxMacro(const char * inDefines){
		static D3DXMACRO ioDxMacro[MAX_DX_MACRO]		;
		static char *	 tmpStr					= NULL	;
		static uint		 tmpStrLn				= 0		;

		NV_ASSERT(MAX_DX_MACRO >= 2);
		if (! inDefines) return NULL;

		uint strln = Strlen(inDefines) +1;
		if (tmpStrLn < strln && tmpStr) {
			NV_ASSERT(tmpStr);
			NvFree(tmpStr);
			tmpStr = NULL;
			tmpStrLn = 0;
		}
		if (!tmpStr) {
			tmpStrLn = Max(DEFAULT_MACRO_STRING_LENGTH,strln);
			tmpStr = (char*)NvMalloc(tmpStrLn);
		}
		
		Memcpy(tmpStr,(void*)inDefines,strln);

		uint nbDefines	= 0;

		char * curPtr	= tmpStr;
		char * name		= NULL;
		char * def    = NULL;

		while (TRUE) {
			ioDxMacro[nbDefines].Name		= NULL;
			ioDxMacro[nbDefines].Definition = NULL;

			// Look for name :
			name	= curPtr;
			curPtr	= Strchr(name,'=') ;
			if (!curPtr	) break;
			*curPtr	= '\0';
			 curPtr++;
			 if (curPtr >= tmpStr + strln) break;

			// look for value ;
			def	= curPtr;
			curPtr	= Strchr(def ,';') ;
			if (curPtr){
				*curPtr	= '\0';
				curPtr++;
			}
			ioDxMacro[nbDefines].Name		= name;
			ioDxMacro[nbDefines].Definition = def;

			nbDefines ++ ;
			if( curPtr == NULL) break;
			if( *curPtr == '\0') break;
			if (curPtr >= tmpStr + strln) break;
			if (nbDefines >= (MAX_DX_MACRO-1)) break;
		}

		if (!nbDefines) return NULL;

		NV_ASSERT(nbDefines < MAX_DX_MACRO);
		ioDxMacro[nbDefines].Definition = NULL;
		ioDxMacro[nbDefines].Name		= NULL;
		return ioDxMacro;
	}
}

void 
Hrdw_InitShaders()
{
	_Shader	nullValue;
	Memset(&nullValue,0,sizeof(_Shader));
	nullValue.Type = NV_UNDEF;

	shaderA.Init();
	shaderEmptyA.Init();
	shaderA.resize(4,nullValue); /// 4 ///////////////////////////////////////////////////////////////////////////////// TEST !!!!!!!!!!!!!!!!!!!!!!!!
	shaderEmptyA.resize(4-2,0);

#ifdef _NVCOMP_ENABLE_DBG
	cstA.Init();
#endif //_NVCOMP_ENABLE_DBG
	
	shaderA[0].Type = NV_VERTEXSHADER|NV_ASM;	
	shaderA[0].D3DShader.VS = NULL;
	shaderA[1].Type = NV_PIXELSHADER|NV_ASM;	
	shaderA[1].D3DShader.VS = NULL;

	for (uint i = 0 ; i < shaderEmptyA.size() ; ++i ) 
		shaderEmptyA[i] = 4-2-i;	
	firstShaderEmpty = shaderEmptyA.size()-2;
#ifdef USE_CG
	cgContext = cgCreateContext();
#endif
}

void 
Hrdw_ShutShaders()
{
	for (uint i = 2 ; i < shaderA.size() ; ++i ) {
		if ( shaderA[i].Type & NV_HLSL) 
			SafeRelease(shaderA[i].Lang.D3DConstantTable);		
		if ( shaderA[i].Type & NV_VERTEXSHADER )
			SafeRelease(shaderA[i].D3DShader.VS);
		else 
			SafeRelease(shaderA[i].D3DShader.PS);
	}
#ifdef USE_CG
	cgDestroyContext(cgContext); // delete all program in this context!!
#endif	
	shaderA.Shut();
	shaderEmptyA.Shut();
#ifdef _NVCOMP_ENABLE_DBG
	cstA.Shut();
#endif //_NVCOMP_ENABLE_DBG
}


Hrdw::ShaderProgram		
Hrdw::CreateShaderProgramFromFile	(uint				inType,
									char * 				inStrPath,
									ShaderProfile 		inVersion,									
									char * 				inEntryPointName,
									const char *		inDefines,
									bool				inCreateReservedRegister)
{
	NV_ASSERT(	inStrPath);
	NV_ASSERT( (inType & NV_VERTEXSHADER) || (inType & NV_PIXELSHADER) );	
	NV_ASSERT( (inType & NV_ASM) || (inType & NV_HLSL) || (inType & NV_CG));

	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool TNL = RMode < Hrdw::RM_Limited;
	if (TNL && inType & NV_PIXELSHADER)
		return 	ShaderProgram(1);

	HRESULT	hr = D3D_OK;	
	LPD3DXBUFFER bcode = NULL, errMsg = NULL;

	uint16 idx = 0;
	GET_FIRST_SHADER_EMPTY(idx);

	if (inType & NV_VERTEXSHADER){
		if (inType & NV_ASM){
			hr = D3DXAssembleShaderFromFile(inStrPath,NULL,NULL,0,&bcode,&errMsg);
			NV_ASSERT_DX(hr);
			hr = ddev->CreateVertexShader( (DWORD *)bcode->GetBufferPointer(), & (shaderA[idx].D3DShader.VS) );
			shaderA[idx].Type = NV_VERTEXSHADER|NV_ASM;			
		}
		else if (inType & NV_HLSL) {
			NV_ASSERT(inEntryPointName) ;
			
			HRESULT	hr;
			// Compile HLSL shader
			DWORD shaderFlags = D3DXSHADER_PREFER_FLOW_CONTROL;
			#ifdef DEBUG_VS
				shaderFlags |= D3DXSHADER_SKIPOPTIMIZATION|D3DXSHADER_DEBUG;
			#endif
			hr = D3DXCompileShaderFromFile( inStrPath, StringToDxMacro(inDefines), NULL, inEntryPointName,
 											SHADERPROFILE_TRANSLATION[inVersion], shaderFlags, &bcode,
											&errMsg, &(shaderA[idx].Lang.D3DConstantTable) ) ;
			NV_ASSERT_DX(hr);
			NV_ASSERT(bcode);			
			hr = ddev->CreateVertexShader((DWORD*)bcode->GetBufferPointer(), & (shaderA[idx].D3DShader.VS) );
			shaderA[idx].Type = NV_VERTEXSHADER|NV_HLSL;
		}
		else {
#ifdef USE_CG
			NV_ASSERT(cgIsContext(cgContext));
			NV_ASSERT(inEntryPointName) ;

			shaderA[idx].Lang.CGShader = cgCreateProgramFromFile(cgContext,CG_SOURCE,inStrPath,CG_PROFILE_VS_2_X,inEntryPointName,NULL);
			NV_ASSERT(shaderA[idx].Lang.CGShader);
			
			const char* progSrc = cgGetProgramString(shaderA[idx].Lang.CGShader,CG_COMPILED_PROGRAM);
			
		#ifdef _NVCOMP_ENABLE_DBG
			shaderA[idx].idCst = cstA.size();
			FindConstantRegisterInformation(progSrc,shaderA[idx].nbCst);
		#endif //_NVCOMP_ENABLE_DBG
			
			DWORD shaderFlags = 0;
			#ifdef DEBUG_VS		
				shaderFlags |= D3DXSHADER_SKIPOPTIMIZATION|D3DXSHADER_DEBUG;
			#endif
			
			HRESULT hr = D3DXAssembleShader(progSrc, libc::Strlen(progSrc), 0, 0, shaderFlags,& bcode, &errMsg);
			NV_ASSERT_DX(hr);

			hr = ddev->CreateVertexShader((DWORD*)bcode->GetBufferPointer(), & (shaderA[idx].D3DShader.VS) );
			shaderA[idx].Type = NV_VERTEXSHADER|NV_CG;
#else
			NV_ASSERTC(false,"define USE_CG !");
#endif
		}
	}
	else { // NV_PIXELSHADER
		if (inType & NV_ASM){			
			hr = D3DXAssembleShaderFromFile(inStrPath,NULL,NULL,0,&bcode,&errMsg);
			NV_ASSERT_DX(hr);
			hr = ddev->CreatePixelShader( (DWORD *)bcode->GetBufferPointer(), & (shaderA[idx].D3DShader.PS) );
			shaderA[idx].Type = NV_PIXELSHADER|NV_ASM;			
		}
		else if (inType & NV_HLSL) {
			NV_ASSERT(inEntryPointName) ;

			HRESULT	hr;
			// Compile HLSL shader
			DWORD shaderFlags = D3DXSHADER_PREFER_FLOW_CONTROL;
			#ifdef DEBUG_PS
				shaderFlags |= D3DXSHADER_SKIPOPTIMIZATION|D3DXSHADER_DEBUG;
			#endif
		    
			hr = D3DXCompileShaderFromFile( inStrPath, StringToDxMacro(inDefines), NULL, inEntryPointName,
 											SHADERPROFILE_TRANSLATION[inVersion], shaderFlags, &bcode,
											& errMsg, &(shaderA[idx].Lang.D3DConstantTable) ) ;
		                                    
			NV_ASSERT_DX(hr);
			NV_ASSERT(bcode);	
			
			hr = ddev->CreatePixelShader((DWORD*)bcode->GetBufferPointer(), & (shaderA[idx].D3DShader.PS) );			
			shaderA[idx].Type = NV_PIXELSHADER|NV_HLSL;
		}
		else {
#ifdef USE_CG
			NV_ASSERT(inEntryPointName) ;
			NV_ASSERT(cgIsContext(cgContext));

			shaderA[idx].Lang.CGShader = cgCreateProgramFromFile(cgContext,CG_SOURCE,inStrPath,CG_PROFILE_PS_2_X,inEntryPointName,NULL);
			NV_ASSERT(shaderA[idx].Lang.CGShader);
			
			const char* progSrc = cgGetProgramString(shaderA[idx].Lang.CGShader,CG_COMPILED_PROGRAM);
			
		#ifdef _NVCOMP_ENABLE_DBG
			shaderA[idx].idCst = cstA.size();
			FindConstantRegisterInformation(progSrc,shaderA[idx].nbCst);
		#endif //_NVCOMP_ENABLE_DBG
			
			DWORD shaderFlags = 0;
			#ifdef DEBUG_PS
				shaderFlags |= D3DXSHADER_SKIPOPTIMIZATION|D3DXSHADER_DEBUG;
			#endif

			HRESULT hr = D3DXAssembleShader(progSrc, libc::Strlen(progSrc), 0, 0, shaderFlags,& bcode, & errMsg);
			NV_ASSERT_DX(hr);

			hr = ddev->CreatePixelShader((DWORD*)bcode->GetBufferPointer(), & (shaderA[idx].D3DShader.PS) );			
			shaderA[idx].Type = NV_PIXELSHADER|NV_CG;
#else
			NV_ASSERTC(false,"define USE_CG !");
#endif
		}
	}
	SafeRelease(errMsg);
	SafeRelease(bcode);
	NV_ASSERT_DX(hr);
	return 	ShaderProgram(idx);
}

Hrdw::ShaderProgram		
Hrdw::CreateShaderProgram			(uint				inType,
									 char * 			inStr,
									 uint 				inSize,
									 ShaderProfile 		inVersion,									 
									 char * 			inEntryPointName,
								     const char *		inDefines,
									 bool				inCreateReservedRegister)
{
	NV_ASSERT(inStr);
	NV_ASSERT( (inType & NV_VERTEXSHADER) || (inType & NV_PIXELSHADER) );	
	NV_ASSERT( (inType & NV_ASM) || (inType & NV_HLSL) || (inType & NV_CG));

	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool TNL = RMode < Hrdw::RM_Limited;
	if (TNL && inType & NV_PIXELSHADER)
		return 	ShaderProgram(1);

	HRESULT	hr = D3D_OK;	
	LPD3DXBUFFER bcode = NULL, errMsg = NULL;

	uint16 idx = 0;
	GET_FIRST_SHADER_EMPTY(idx);

	if (inType & NV_VERTEXSHADER){
		if (inType & NV_ASM){			
			hr = ddev->CreateVertexShader( (DWORD *)inStr, & (shaderA[idx].D3DShader.VS) );
			shaderA[idx].Type = NV_VERTEXSHADER|NV_ASM;			
		}
		else if (inType & NV_HLSL) {
			NV_ASSERT(inEntryPointName) ;
			
			HRESULT	hr;
			// Compile HLSL shader
			DWORD shaderFlags = D3DXSHADER_PREFER_FLOW_CONTROL;
			#ifdef DEBUG_VS
				shaderFlags |= D3DXSHADER_SKIPOPTIMIZATION|D3DXSHADER_DEBUG;
			#endif 
			hr = D3DXCompileShader( inStr,inSize, StringToDxMacro(inDefines), NULL, inEntryPointName,
 									SHADERPROFILE_TRANSLATION[inVersion], shaderFlags, &bcode,
									&errMsg, &(shaderA[idx].Lang.D3DConstantTable) ) ;
			NV_ASSERT_DX(hr);
			NV_ASSERT(bcode);			
			hr = ddev->CreateVertexShader((DWORD*)bcode->GetBufferPointer(), & (shaderA[idx].D3DShader.VS) );
			shaderA[idx].Type = NV_VERTEXSHADER|NV_HLSL;
		}
		else {
			NV_ASSERT(FALSE);
		}
	}
	else { // NV_PIXELSHADER
		if (inType & NV_ASM){				
			hr = ddev->CreatePixelShader( (DWORD *)inStr, & (shaderA[idx].D3DShader.PS) );
			shaderA[idx].Type = NV_PIXELSHADER|NV_ASM;	
		}
		else if (inType & NV_HLSL) {
			NV_ASSERT(inEntryPointName) ;

			HRESULT	hr;
			// Compile HLSL shader
			DWORD shaderFlags = D3DXSHADER_PREFER_FLOW_CONTROL;
			#ifdef DEBUG_PS
				shaderFlags |= D3DXSHADER_SKIPOPTIMIZATION|D3DXSHADER_DEBUG;
			#endif
		    
			hr = D3DXCompileShader( inStr,inSize, StringToDxMacro(inDefines), NULL, inEntryPointName,
									SHADERPROFILE_TRANSLATION[inVersion], shaderFlags, &bcode,
									& errMsg, &(shaderA[idx].Lang.D3DConstantTable) ) ;		                                    
			NV_ASSERT_DX(hr);
			NV_ASSERT(bcode);				
			hr = ddev->CreatePixelShader((DWORD*)bcode->GetBufferPointer(), & (shaderA[idx].D3DShader.PS) );			
			shaderA[idx].Type = NV_PIXELSHADER|NV_HLSL;
		}
		else {	
			NV_ASSERT(FALSE);
		}
	}
	SafeRelease(errMsg);
	SafeRelease(bcode);
	NV_ASSERT_DX(hr);
	return 	ShaderProgram(idx);
}

void
Hrdw::ReleaseShader	(ShaderProgram 		inVS )
{
	if (inVS<= 0 || inVS >=shaderA.size())
		return;	

	if ( shaderA[inVS].Type & NV_HLSL) 
		SafeRelease(shaderA[inVS].Lang.D3DConstantTable);		
	if ( shaderA[inVS].Type & NV_VERTEXSHADER )
		SafeRelease(shaderA[inVS].D3DShader.VS);
	else 
		SafeRelease(shaderA[inVS].D3DShader.PS);

	shaderA[inVS].D3DShader.VS = NULL;
	shaderA[inVS].Type = NV_UNDEF;

	firstShaderEmpty++;
	shaderEmptyA[firstShaderEmpty] = inVS;
}

bool 		
Hrdw::SetShaderProgram	(ShaderProgram 	inS	)
{
	static int currentVS = -1;
	static int currentPS = -1;

	NV_ASSERT(inS < shaderA.size());
	NV_ASSERT((shaderA[inS].Type & NV_VERTEXSHADER) || (shaderA[inS].Type & NV_PIXELSHADER));
	
	HRESULT	hr = D3D_OK;	

	if ((shaderA[inS].Type & NV_VERTEXSHADER) && inS != currentVS) {
		hr = ddev->SetVertexShader (shaderA[inS].D3DShader.VS);
		currentVS = inS;
	}
	else if ((shaderA[inS].Type & NV_PIXELSHADER) && inS != currentPS ){
		Hrdw::RunningMode RMode;
		nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
		bool TNL = RMode < Hrdw::RM_Limited;
		if (!TNL) {
			hr = ddev->SetPixelShader (shaderA[inS].D3DShader.PS);
			currentPS = inS;
		}
	}

	NV_ASSERT_DX(hr);
	return hr == D3D_OK;
}

bool 		
Hrdw::SetVertexShaderConstantB	(uint			inRegister,
								 bool * 		inData,
								 uint 			inNbVect		)
{
	HRESULT	hr = ddev->SetVertexShaderConstantB(inRegister, (BOOL *) inData, inNbVect);	
	NV_ASSERT_DX(hr);
	return hr == D3D_OK;
}

										 
bool 		
Hrdw::SetVertexShaderConstantF	(uint			inRegister,
								 float * 		inData,
								 uint 			inNbVect 		)
{
	HRESULT	hr = ddev->SetVertexShaderConstantF(inRegister, inData, inNbVect);	
	NV_ASSERT_DX(hr);
	return hr == D3D_OK;	
}

										 
bool 		
Hrdw::SetVertexShaderConstantI	(uint			inRegister,
								 int * 		inData,
								 uint 		inNbVect 		)
{
	HRESULT	hr = ddev->SetVertexShaderConstantI(inRegister, inData, inNbVect);	
	NV_ASSERT_DX(hr);
	return hr == D3D_OK;		
}


void
Hrdw::InternalUpdateVertexMapping	(uint16			inFirstRegister	,													  	
									 uint16 		inSize,
									 void * 		inData			)
{
	HRESULT	hr = ddev->SetVertexShaderConstantF(inFirstRegister, (float *) inData, inSize);	
	NV_ASSERT_DX(hr);
}
	
bool
Hrdw::SetPixelShaderConstantB		(uint			inRegister,
							 	 	 bool * 		inData,
								 	 uint 			inNbVect		)
{
	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool TNL = RMode < Hrdw::RM_Limited;
	if (!TNL) {
		HRESULT	hr = ddev->SetPixelShaderConstantB(inRegister, (BOOL *) inData, inNbVect);	
		NV_ASSERT_DX(hr);
		return hr == D3D_OK;
	}
	return FALSE;
}	
										 
bool
Hrdw::SetPixelShaderConstantF		(uint			inRegister,
								  	 float * 		inData,
								 	 uint 			inNbVect 		)
{
	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool TNL = RMode < Hrdw::RM_Limited;
	if (!TNL) {
		HRESULT	hr = ddev->SetPixelShaderConstantF(inRegister, (FLOAT *) inData, inNbVect);	
		NV_ASSERT_DX(hr);
		return hr == D3D_OK;
	}
	return FALSE;
}	
											 
bool
Hrdw::SetPixelShaderConstantI		(uint			inRegister,
								 	 int * 			inData,
								  	 uint 			inNbVect 		)
{
	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool TNL = RMode < Hrdw::RM_Limited;
	if (!TNL) {
		HRESULT	hr = ddev->SetPixelShaderConstantI(inRegister, (INT *) inData, inNbVect);	
		NV_ASSERT_DX(hr);
		return hr == D3D_OK;
	}
	return FALSE;
}	
												  	 
													  	 
void
Hrdw::InternalUpdatePixelMapping	(uint16			inFirstRegister	,													  	
									 uint16 		inSize,
									 void * 		inData			)
{
	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool TNL = RMode < Hrdw::RM_Limited;
	if (!TNL) {
		HRESULT	hr = ddev->SetPixelShaderConstantF(inFirstRegister, (float *) inData, inSize);	
		NV_ASSERT_DX(hr);
	}
}

bool
Hrdw::InternalCheckMapping	(ShaderProgram 			inS,
							 uint16					inRegister,
							 uint16					inSize,
							 char * 				inName)
{
#ifdef _NVCOMP_ENABLE_DBG
	if (inS == 0 || inS == 1)
		return TRUE;
	NV_ASSERT(inS);
	NV_ASSERT(inS <shaderA.size());
	NV_ASSERT(!(shaderA[inS].Type & NV_UNDEF));
	NV_ASSERT(!(shaderA[inS].Type & NV_ASM));


	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool TNL = RMode < Hrdw::RM_Limited;
	if ( TNL && (shaderA[inS].Type & NV_PIXELSHADER) )
		return TRUE;



	if ( shaderA[inS].Type & NV_HLSL) {
		D3DXHANDLE cst = shaderA[inS].Lang.D3DConstantTable->GetConstantByName(NULL,(LPCSTR)inName);
		if (!cst) return FALSE;
		D3DXCONSTANT_DESC Desc;
		uint nbDesc = 1;
		HRESULT hr = shaderA[inS].Lang.D3DConstantTable->GetConstantDesc(cst,&Desc,&nbDesc);
		NV_ASSERT_DX(hr);
		return (Desc.RegisterIndex == (int32)inRegister) && ( (inSize<4u)?1:(inSize / 4u) <= Desc.RegisterCount);
	}
	else {// CG
		for (uint i = shaderA[inS].idCst ; i < shaderA[inS].nbCst; ++i ) {
			if (libc::Strcmp(inName,cstA[i].name) == 0)
				return (inRegister == cstA[i].reg) && ((inSize<4)?1:(inSize / 4) == cstA[i].nbReg) ; 
		}
		return false;
	}
#else
	return true;
#endif // _NVCOMP_ENABLE_DBG
}

