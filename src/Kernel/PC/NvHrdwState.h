/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include "NvHrdwTranslation.h"

void 
Hrdw::SetRenderState(		RenderState inRS ,
								uint inValue)
{
	HRESULT hr;
	uint writec = 0;
	
	switch ((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS]) 
	{
		case D3DRS_ZENABLE:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],ZBUFFERTYPE_TRANSLATION[inValue]);
			break;
			
		case D3DRS_FILLMODE:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],FILLMODE_TRANSLATION[inValue]);
			break;
			
		case D3DRS_SHADEMODE:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],SHADEMODE_TRANSLATION[inValue]);
			break;
			
		case D3DRS_SRCBLEND:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],BLEND_TRANSLATION[inValue]);
			break;
			
		case D3DRS_DESTBLEND:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],BLEND_TRANSLATION[inValue]);
			break;
						
		case D3DRS_ZFUNC			:
		case D3DRS_ALPHAFUNC		:
		case D3DRS_STENCILFUNC		:
		case D3DRS_CCW_STENCILFUNC	:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],CMPFUNC_TRANSLATION[inValue]);
			break;
			
		case D3DRS_FOGTABLEMODE:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],FOGMODE_TRANSLATION[inValue]);
			break;
			
		case D3DRS_STENCILFAIL		:
		case D3DRS_STENCILZFAIL		:
		case D3DRS_STENCILPASS		:
		case D3DRS_CCW_STENCILFAIL	:
		case D3DRS_CCW_STENCILZFAIL	:
		case D3DRS_CCW_STENCILPASS	:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],STENCILOP_TRANSLATION[inValue]);			
			break;
			
		case D3DRS_DIFFUSEMATERIALSOURCE:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],MATERIALCOLORSOURCE_TRANSLATION[inValue]);
			break;
			
		case D3DRS_VERTEXBLEND:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],VERTEXBLENDFLAGS_TRANSLATION[inValue]);
			break;
			
		case D3DRS_PATCHEDGESTYLE:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],PATCHEDGESTYLE_TRANSLATION[inValue]);
			break;
			
		case D3DRS_DEBUGMONITORTOKEN:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],DEBUGMONITORTOKENS_TRANSLATION[inValue]);
			break;
			
		case D3DRS_BLENDOP:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],BLENDOP_TRANSLATION[inValue]);
			break;
			
		case D3DRS_POSITIONDEGREE:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],DEGREETYPE_TRANSLATION[inValue]);
			break;
			
		case D3DRS_CULLMODE:		
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],CULL_TRANSLATION[inValue]);
			break;			
			
		case D3DRS_COLORWRITEENABLE:
			if (inValue & 0x1) writec |= D3DCOLORWRITEENABLE_ALPHA;
			if (inValue & 0x2) writec |= D3DCOLORWRITEENABLE_BLUE;
			if (inValue & 0x4) writec |= D3DCOLORWRITEENABLE_GREEN;
			if (inValue & 0x8) writec |= D3DCOLORWRITEENABLE_RED;
			hr = ddev->SetRenderState(D3DRS_COLORWRITEENABLE,writec);			
			break;			
			
		default:
			hr = ddev->SetRenderState((D3DRENDERSTATETYPE)RENDERSTATE_TRANSLATION[inRS],inValue);
	}	
	NV_ASSERT_DX(hr);
}


void
Hrdw::SetTextureStageState(	int 					inStage,
							 TextureStageStateType 	inType,
							 uint					inValue					)
{
	HRESULT hr;
	int flags = 0;
	
	switch (TEXTURESTAGESTATETYPE_TRANSLATION[inType]) {

	case D3DTSS_COLOROP: 
		hr = ddev->SetTextureStageState(inStage,D3DTSS_COLOROP ,TEXTUREOP_TRANSLATION[inValue] );
		break;	
		
	case D3DTSS_ALPHAOP: 
		hr = ddev->SetTextureStageState(inStage,D3DTSS_ALPHAOP ,TEXTUREOP_TRANSLATION[inValue] );
		break;
		
	case D3DTSS_COLORARG1: 
	case D3DTSS_COLORARG2: 
	case D3DTSS_ALPHAARG1: 	
	case D3DTSS_ALPHAARG2: 	
	case D3DTSS_COLORARG0:
	case D3DTSS_ALPHAARG0:
	case D3DTSS_RESULTARG:

		for (uint i = 0 ; i < 10 ; ++i ) {
			if (inValue & (1 << i) ) 
				flags |= TEXTUREARGUMENT_TRANSLATION[i];
		}
		hr = ddev->SetTextureStageState(inStage,TEXTURESTAGESTATETYPE_TRANSLATION[inType],flags );
		break;			

	case D3DTSS_TEXCOORDINDEX: 
		hr = ddev->SetTextureStageState(inStage, D3DTSS_TEXCOORDINDEX,TEXTURECOORDINATE_TRANSLATION[inValue] );
		break;		

	case D3DTSS_TEXTURETRANSFORMFLAGS: 
		hr = ddev->SetTextureStageState(inStage,D3DTSS_TEXTURETRANSFORMFLAGS ,TEXTURETRANSFORM_TRANSLATION[inValue] );
		break;		
				
	default:
		hr = ddev->SetTextureStageState(inStage,TEXTURESTAGESTATETYPE_TRANSLATION[inType],inValue);
	}	
	NV_ASSERT_DX(hr);
}

void
Hrdw::SetSamplerState		(int 					inSampler,
							 SamplerStageType	 	inType,
							 uint					inValue	)
{
	HRESULT hr;

	switch (SAMPLERSTATETYPE_TRANSLATION[inType]) {

	case D3DSAMP_ADDRESSU: 
	case D3DSAMP_ADDRESSW: 
	case D3DSAMP_ADDRESSV: 
		hr = ddev->SetSamplerState(inSampler,SAMPLERSTATETYPE_TRANSLATION[inType], TEXTUREADDRESS_TRANSLATION[inValue] );
		break;
				
	case D3DSAMP_MAGFILTER: 
	case D3DSAMP_MINFILTER:
	case D3DSAMP_MIPFILTER:
		hr = ddev->SetSamplerState(inSampler,SAMPLERSTATETYPE_TRANSLATION[inType], TEXTUREFILTER_TRANSLATION[inValue] );
		break;
										
	default:
		hr = ddev->SetSamplerState(inSampler,SAMPLERSTATETYPE_TRANSLATION[inType],inValue );
	}	
	NV_ASSERT_DX(hr);
}