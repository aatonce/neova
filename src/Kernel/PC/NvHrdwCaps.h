
/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include "NvHrdwTranslation.h"
#include <Nova.h>

using namespace nv;
using namespace Hrdw;


PCSTR
Hrdw::GetRunningModeName( RunningMode		inRM	)
{
	if( inRM == RM_Unknown )	return "Unknown";
	if( inRM == RM_None )		return "None";
	if( inRM == RM_TNL )		return "TnL";
	if( inRM == RM_VtxNL )		return "Vtx-nL";
	if( inRM == RM_Limited )	return "Limited";
	if( inRM == RM_Full )		return "Full";
	return "Invalid";
}



Hrdw::RunningMode
Hrdw::GetBestRunningMode( uint		inAdapter )
{
	D3DCAPS9	m_caps;
	HRESULT		hr;

	// Get caps
	if( !dd3d )
	{
		NV_D3D m_dd3d = NV_D3D_CREATE( D3D_SDK_VERSION );
		if( !m_dd3d )
			return RM_Unknown;
		adapter = inAdapter;
		if( adapter > m_dd3d->GetAdapterCount() )
			adapter = D3DADAPTER_DEFAULT;
		adapterDevType = D3DDEVTYPE_HAL;
		hr = m_dd3d->GetDeviceCaps( adapter, adapterDevType, &m_caps );
		SafeRelease( m_dd3d );
		if( hr != D3D_OK )
			return RM_Unknown;
	}
	else
	{
		hr = dd3d->GetDeviceCaps( adapter, adapterDevType, &m_caps );
		if( hr != D3D_OK )
			return RM_Unknown;
	}

	if( !(m_caps.DevCaps&D3DDEVCAPS_HWTRANSFORMANDLIGHT) )
		return RM_None;

	if( !(m_caps.DevCaps & D3DDEVCAPS_HWRASTERIZATION) )
		return RM_None;

	if ( ! (m_caps.ZCmpCaps & D3DPCMPCAPS_LESSEQUAL) )
		return RM_None;

	if ( m_caps.MaxStreams < REQ_HwStreams )
		return RM_TNL;

	int vs_major = REQ_HwVertexShaderVersion / 10;
	int vs_minor = REQ_HwVertexShaderVersion % 10;
	if ( m_caps.VertexShaderVersion < D3DVS_VERSION(vs_major,vs_minor) )
		return RM_TNL;

	int ps_major = REQ_HwPixelShaderVersion / 10;
	int ps_minor = REQ_HwPixelShaderVersion % 10;
	if ( m_caps.PixelShaderVersion < D3DPS_VERSION(ps_major,ps_minor) )
		return RM_VtxNL;

	if (	!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_BLENDFACTOR )		||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_DESTALPHA )		||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_DESTCOLOR )		||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_INVDESTALPHA )	||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_INVDESTCOLOR )	||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_INVSRCALPHA )		||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_INVSRCCOLOR )		||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_ONE )				||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_SRCALPHA )		||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_SRCCOLOR )		||
			!(m_caps.SrcBlendCaps & D3DPBLENDCAPS_ZERO ) )	{	
		return RM_Limited ;
	}

	if ( ! (m_caps.AlphaCmpCaps & D3DPCMPCAPS_LESSEQUAL) )
		return RM_Limited;

	if (	! (m_caps.StencilCaps  & D3DSTENCILCAPS_KEEP)		&&
			! (m_caps.StencilCaps  & D3DSTENCILCAPS_ZERO)		&&
			! (m_caps.StencilCaps  & D3DSTENCILCAPS_REPLACE)	&&
			! (m_caps.StencilCaps  & D3DSTENCILCAPS_INCRSAT)	&&
			! (m_caps.StencilCaps  & D3DSTENCILCAPS_DECRSAT)	&&
			! (m_caps.StencilCaps  & D3DSTENCILCAPS_INCR)		&&
			! (m_caps.StencilCaps  & D3DSTENCILCAPS_DECR)		&&
			! (m_caps.StencilCaps  & D3DSTENCILCAPS_TWOSIDED)	)
		return RM_Limited;

	if ( m_caps.MaxSimultaneousTextures < REQ_SimultaneousTextures )
		return RM_Limited;

	if ( m_caps.NumSimultaneousRTs  < REQ_SimultaneousRT )
		return RM_Limited;

	return RM_Full;
}


