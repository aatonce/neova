/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <kernel/PC/NvHrdw.h>
#include "NvHrdwTranslation.h"
#include <Nova.h>

using namespace nv;
using namespace Hrdw;


namespace {

	struct 	_vertexBuffer {
		LPDIRECT3DVERTEXBUFFER9 VB;
		uint 					Size;
		DWORD					usage;
		D3DPOOL					pool;
		bool					validData;
	};
	
	struct _indexBuffer {
		LPDIRECT3DINDEXBUFFER9 	IB;
		uint 					Size;
		DWORD					usage;
		D3DPOOL					pool;
		D3DFORMAT				format;
		bool					validData;
	};
	
	sysvector<_vertexBuffer>	vertexBufferA;
	sysvector<_indexBuffer>		indexBufferA;
	sysvector<uint16>			vertexBufferEmptyA;
	sysvector<uint16>			indexBufferEmptyA;
	int32						firstindexBufferEmpty;
	int32						firstvertexBufferEmpty;


	#define GET_FIRST_STREAM_EMPTY(idx,type)									\
		if (first##type##BufferEmpty <0){								\
			_##type##Buffer NullValue={NULL,0};							\
			uint addedsize = type##BufferA.size()/2;					\
			uint newsize = type##BufferA.size() + addedsize;			\
			type##BufferA.resize(newsize,NullValue);					\
			type##BufferEmptyA.resize(newsize-1,0);						\
			for (uint i = 0 ; i < addedsize ; ++i)						\
				type##BufferEmptyA[i] = newsize-1 - i;					\
			first##type##BufferEmpty = addedsize -1;					\
		}																\
		idx = type##BufferEmptyA[first##type##BufferEmpty];				\
		first##type##BufferEmpty--; 



	#ifdef _NVCOMP_ENABLE_DBG
	bool isStreamInit = FALSE;
	#define CHECK_STREAM_INIT NV_ASSERT(isStreamInit)
	#else
	#define CHECK_STREAM_INIT 
	#endif //_NVCOMP_ENABLE_DBG
}

namespace Hrdw {
	bool				
	StreamOnDeviceLost		(											)
	{
		for (uint i = 0 ; i < vertexBufferA.size() ; ++i ) {
			if (vertexBufferA[i].VB && vertexBufferA[i].pool == D3DPOOL_DEFAULT && vertexBufferA[i].Size) {
				SafeRelease(vertexBufferA[i].VB);
				vertexBufferA[i].VB = NULL;
				vertexBufferA[i].validData = FALSE;
			}
		}

		for (uint i = 0 ; i < indexBufferA.size() ; ++i ) {
			if (indexBufferA[i].IB && indexBufferA[i].pool == D3DPOOL_DEFAULT && indexBufferA[i].Size) {
				SafeRelease(indexBufferA[i].IB);
				indexBufferA[i].IB = NULL;
				indexBufferA[i].validData = FALSE;
			}
		}

		return TRUE;
	}

	bool
	StreamOnDeviceReset		(											)
	{
		bool creationDone = TRUE;
		for (uint i = 0 ; i < vertexBufferA.size() ; ++i ) {
			if ((!vertexBufferA[i].VB) && vertexBufferA[i].pool == D3DPOOL_DEFAULT && vertexBufferA[i].Size) {
				creationDone &= ( ddev->CreateVertexBuffer(	vertexBufferA[i].Size,
															vertexBufferA[i].usage,
															0,
															vertexBufferA[i].pool,
															& (vertexBufferA[i].VB) , NULL ) == D3D_OK );
			}
		}

		for (uint i = 0 ; i < indexBufferA.size() ; ++i ) {
			if ((!indexBufferA[i].IB) && indexBufferA[i].pool == D3DPOOL_DEFAULT && indexBufferA[i].Size) {
				creationDone &= ( ddev->CreateIndexBuffer(	indexBufferA[i].Size,
												indexBufferA[i].usage,
												indexBufferA[i].format,
												indexBufferA[i].pool,
												& (indexBufferA[i].IB), NULL	) == D3D_OK);
			}
		}
		VBSetValidData(Hrdw::SD_DummyStream);
		return creationDone;
	}
}

void
Hrdw_InitStream(	)
{
	#ifdef _NVCOMP_ENABLE_DBG
	NV_ASSERT(!isStreamInit);
	isStreamInit = TRUE;	
	#endif //_NVCOMP_ENABLE_DBG

	vertexBufferA.Init();
	vertexBufferEmptyA.Init();
	indexBufferA.Init();
	indexBufferEmptyA.Init();

	_vertexBuffer vBNull={NULL,0};
	_indexBuffer  iBNull={NULL,0};

	vertexBufferA.resize(64,vBNull);
	vertexBufferEmptyA.resize(64-1,0);
	indexBufferA.resize(64,iBNull);
	indexBufferEmptyA.resize(64-1,0);

	for (uint i = 0 ; i < vertexBufferEmptyA.size() ; ++i ) 
		vertexBufferEmptyA[i] = 64-1-i;
	for (uint i = 0 ; i < indexBufferEmptyA.size() ; ++i ) 
		indexBufferEmptyA[i] = 64-1-i;
	firstindexBufferEmpty = vertexBufferEmptyA.size()-1;
	firstvertexBufferEmpty = indexBufferEmptyA.size()-1;

	Hrdw::VertexBuffer id =  Hrdw::CreateVertexBuffer	( 4 * 4,Hrdw::NV_STATIC);
	NV_ASSERT(id == Hrdw::SD_DummyStream);
}

void
Hrdw_ShutStream(	)
{		
	CHECK_STREAM_INIT;

	ReleaseVertexBuffer(Hrdw::SD_DummyStream);

	#ifdef _NVCOMP_ENABLE_DBG
	isStreamInit = FALSE;
	#endif //_NVCOMP_ENABLE_DBG
	
	for (uint i = 1 ; i < vertexBufferA.size() ; ++i ) 
		SafeRelease(vertexBufferA[i].VB);
	for (uint i = 1 ; i < indexBufferA.size() ; ++i ) 
		SafeRelease(indexBufferA[i].IB);
	
	vertexBufferA.Shut();
	vertexBufferEmptyA.Shut();
	indexBufferA.Shut();
	indexBufferEmptyA.Shut();
}

Hrdw::VertexBuffer 	
Hrdw::CreateVertexBuffer	(	uint	 				inSize,
								Usage		 		/*	inUsage	*/	)
{
	CHECK_STREAM_INIT;
	uint16 idx;
	GET_FIRST_STREAM_EMPTY(idx,vertex);

	HRESULT hr = ddev->CreateVertexBuffer(	inSize,
											D3DUSAGE_WRITEONLY,
											0,
											D3DPOOL_MANAGED,
											& (vertexBufferA[idx].VB) , NULL );
	vertexBufferA[idx].Size		= inSize;
	vertexBufferA[idx].pool		= D3DPOOL_MANAGED;
	vertexBufferA[idx].usage	= D3DUSAGE_WRITEONLY;
	vertexBufferA[idx].validData = true;

	NV_ASSERT_DX(hr);
	return (hr==D3D_OK)?VertexBuffer(idx):0;
}											 

											 
Hrdw::IndexBuffer	
Hrdw::CreateIndexBuffer		( uint 				inSize				,
							  IndexFormat		inFormat			,
							  Usage		 	/*	inUsage		*/		)
{
	CHECK_STREAM_INIT;
	uint16 idx;
	GET_FIRST_STREAM_EMPTY(idx,index);

	HRESULT hr = ddev->CreateIndexBuffer(	inSize,
											D3DUSAGE_WRITEONLY,
											INDEXFORMAT_TRANSLATION[inFormat],
											D3DPOOL_MANAGED,
											& (indexBufferA[idx].IB), NULL	);
	indexBufferA[idx].Size		= inSize;
	indexBufferA[idx].pool		= D3DPOOL_MANAGED;
	indexBufferA[idx].usage		= D3DUSAGE_WRITEONLY;
	indexBufferA[idx].format	= INDEXFORMAT_TRANSLATION[inFormat];
	indexBufferA[idx].validData = true;

	NV_ASSERT_DX(hr);
	return (hr == D3D_OK)?IndexBuffer(idx):0;
}

void
Hrdw::ReleaseVertexBuffer   ( VertexBuffer 	inVB )
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inVB != 0);
	NV_ASSERT(inVB < vertexBufferA.size());
	NV_ASSERT(vertexBufferA[inVB].VB != NULL);

	SafeRelease(vertexBufferA[inVB].VB);
	vertexBufferA[inVB].VB = NULL;
	vertexBufferA[inVB].Size = 0;

	firstvertexBufferEmpty++;
	vertexBufferEmptyA[firstvertexBufferEmpty] = inVB;
}

void
Hrdw::ReleaseIndexBuffer 	( IndexBuffer 	inIB )
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inIB != 0);
	NV_ASSERT(inIB < indexBufferA.size());
	NV_ASSERT(indexBufferA[inIB].IB != NULL);

	SafeRelease(indexBufferA[inIB].IB);
	indexBufferA[inIB].IB = NULL;
	indexBufferA[inIB].Size = 0;

	firstindexBufferEmpty++;
	indexBufferEmptyA[firstindexBufferEmpty] = inIB;
}

void * 				
Hrdw::LockVertexBuffer		( VertexBuffer 			inVB,
							  uint 					inOffset,
							  uint 					inSize,
							  int					inLockFlag/*LockFlags*/	)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inVB != 0);
	NV_ASSERT(inVB < vertexBufferA.size());
	NV_ASSERT(vertexBufferA[inVB].VB != NULL);

	int D3DLock = 0;
	for (uint i = 0 ; i < 5 ; ++i) {
		if (inLockFlag & (1<<i))
		D3DLock |= LOCKFLAGS_TRANSLATION[i];				
	}
	
	void * ptr;
	HRESULT hr = vertexBufferA[inVB].VB->Lock(inOffset, inSize, &ptr, D3DLock);
	NV_ASSERT_DX(hr);
	return (hr == D3D_OK)?ptr:NULL;
}
											 
void 				
Hrdw::UnlockVertexBuffer	( VertexBuffer 	inVB	)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inVB != 0);
	NV_ASSERT(inVB < vertexBufferA.size());
	NV_ASSERT(vertexBufferA[inVB].VB != NULL);

	HRESULT hr = vertexBufferA[inVB].VB->Unlock();
	NV_ASSERT_DX(hr);
}	

void *				
Hrdw::LockIndexBuffer		( IndexBuffer 			inIB,
							  uint 					inOffset,
							  uint 					inSize,
							  int					inLockFlag/*LockFlags*/	)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inIB != 0);
	NV_ASSERT(inIB < indexBufferA.size());
	NV_ASSERT(indexBufferA[inIB].IB != NULL);

	int D3DLock = 0;
	for (uint i = 0 ; i < 5 ; ++i) {
		if (inLockFlag & (1<<i))
			D3DLock |= LOCKFLAGS_TRANSLATION[i];				
	}

	void * ptr;
	HRESULT hr = indexBufferA[inIB].IB->Lock(inOffset, inSize, &ptr, D3DLock);
	NV_ASSERT_DX(hr);
	return (hr == D3D_OK)?ptr:NULL;
}	
										 
void 				
Hrdw::UnlockIndexBuffer		( IndexBuffer 	inIB	)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inIB != 0);
	NV_ASSERT(inIB < indexBufferA.size());
	NV_ASSERT(indexBufferA[inIB].IB != NULL);
	HRESULT hr = indexBufferA[inIB].IB->Unlock();
	NV_ASSERT_DX(hr);
}

bool 				
Hrdw::SetVertexBuffer		( VertexBuffer 			inVB,
							  uint					inStreamNumber,
							  uint					inOffet,
							  uint					inStride				)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inVB != 0);
	NV_ASSERT(inVB < vertexBufferA.size());
	NV_ASSERT(vertexBufferA[inVB].VB != NULL);
	NV_ASSERTC(vertexBufferA[inVB].validData,"A Lost device event have been occured, and a stream was not reloaded. ");
	HRESULT hr = ddev->SetStreamSource(inStreamNumber, vertexBufferA[inVB].VB, inOffet , inStride);
	NV_ASSERT_DX(hr);
	return (hr == D3D_OK);	
}
											 
bool				
Hrdw::SetIndexBuffer		( IndexBuffer 	inIB)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inIB != 0);
	NV_ASSERT(inIB < indexBufferA.size());
	NV_ASSERT(indexBufferA[inIB].IB != NULL);
	NV_ASSERTC(indexBufferA[inIB].validData,"A Lost device event have been occured, and a stream was not reloaded. ");
	HRESULT hr = ddev->SetIndices( indexBufferA[inIB].IB);
	NV_ASSERT_DX(hr);
	return (hr == D3D_OK);	
}

void
Hrdw::DrawPrimitive			( PrimitiveType 		inType,
							  uint			 		inNbPrimitiveToRender,
							  uint					inIndexOffset,
							  uint					inNbVertexUsed,
							  uint					inVertexOffset)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inNbPrimitiveToRender);	
	NV_ASSERT(inNbVertexUsed);
	HRESULT hr = ddev->DrawIndexedPrimitive(	PRIMITIVETYPE_TRANSLATION[inType],
												inVertexOffset,
												0,
												inNbVertexUsed,
												inIndexOffset,
												inNbPrimitiveToRender);
	NV_ASSERT_DX(hr);
}

void
Hrdw::DrawPrimitive			( PrimitiveType 		inType,
							  uint			 		inNbPrimitiveToRender,
							  uint					inVertexOffset		)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inNbPrimitiveToRender);
	HRESULT hr = ddev->DrawPrimitive(	PRIMITIVETYPE_TRANSLATION[inType],
										inVertexOffset,
										inNbPrimitiveToRender);	
	NV_ASSERT_DX(hr);
}

bool
Hrdw::IBValidData				(	IndexBuffer			inIB				)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inIB != 0);
	NV_ASSERT(inIB < indexBufferA.size());
	NV_ASSERT(indexBufferA[inIB].IB != NULL);
	return indexBufferA[inIB].validData;
}

bool
Hrdw::VBValidData				(	VertexBuffer		inVB				)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inVB != 0);
	NV_ASSERT(inVB < vertexBufferA.size());
	NV_ASSERT(vertexBufferA[inVB].VB != NULL);
	return vertexBufferA[inVB].validData;
}

void
Hrdw::IBSetValidData			(	IndexBuffer			inIB				)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inIB != 0);
	NV_ASSERT(inIB < indexBufferA.size());
	NV_ASSERT(indexBufferA[inIB].IB != NULL);
	indexBufferA[inIB].validData = true;
}

void
Hrdw::VBSetValidData			(	VertexBuffer		inVB				)
{
	CHECK_STREAM_INIT;
	NV_ASSERT(inVB != 0);
	NV_ASSERT(inVB < vertexBufferA.size());
	NV_ASSERT(vertexBufferA[inVB].VB != NULL);
	vertexBufferA[inVB].validData = TRUE;
}