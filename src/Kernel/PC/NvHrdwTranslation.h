/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef NVHRDWTRANSLATION_H_
#define NVHRDWTRANSLATION_H_

//#define NO_DEFAULT_RESSOURCE

	static uint CLEARTARGET_TRANSLATION[]={	D3DCLEAR_STENCIL, 
											D3DCLEAR_TARGET, 
											D3DCLEAR_ZBUFFER };
									
									
//////////////////////////////////////////////////////////////////////////////////////////////////////
//									State
	static uint8 RENDERSTATE_TRANSLATION[]={D3DRS_ZENABLE ,
										    D3DRS_FILLMODE ,
										    D3DRS_SHADEMODE ,
										    D3DRS_ZWRITEENABLE,
										    D3DRS_ALPHATESTENABLE ,
										    D3DRS_LASTPIXEL ,
										    D3DRS_SRCBLEND ,
										    D3DRS_DESTBLEND ,
										    D3DRS_CULLMODE ,
										    D3DRS_ZFUNC ,
										    D3DRS_ALPHAREF ,
										    D3DRS_ALPHAFUNC ,
										    D3DRS_DITHERENABLE ,
										    D3DRS_ALPHABLENDENABLE ,
										    D3DRS_FOGENABLE ,
										    D3DRS_SPECULARENABLE ,
										    D3DRS_FOGCOLOR ,
										    D3DRS_FOGTABLEMODE,
										    D3DRS_FOGSTART ,
										    D3DRS_FOGEND ,
										    D3DRS_FOGDENSITY ,
										    D3DRS_RANGEFOGENABLE ,
										    D3DRS_STENCILENABLE ,
										    D3DRS_STENCILFAIL ,
										    D3DRS_STENCILZFAIL ,
										    D3DRS_STENCILPASS ,
										    D3DRS_STENCILFUNC ,
										    D3DRS_STENCILREF ,
										    D3DRS_STENCILMASK ,
										    D3DRS_STENCILWRITEMASK ,
										    D3DRS_TEXTUREFACTOR ,
										    D3DRS_WRAP0 ,
										    D3DRS_WRAP1 ,
										    D3DRS_WRAP2 ,
										    D3DRS_WRAP3 ,
										    D3DRS_WRAP4 ,
										    D3DRS_WRAP5 ,
										    D3DRS_WRAP6 ,
										    D3DRS_WRAP7 ,
										    D3DRS_CLIPPING ,
										    D3DRS_LIGHTING ,
										    D3DRS_AMBIENT ,
										    D3DRS_FOGVERTEXMODE ,
										    D3DRS_COLORVERTEX ,
										    D3DRS_LOCALVIEWER ,
										    D3DRS_NORMALIZENORMALS ,
										    D3DRS_DIFFUSEMATERIALSOURCE ,
										    D3DRS_SPECULARMATERIALSOURCE ,
										    D3DRS_AMBIENTMATERIALSOURCE ,
										    D3DRS_EMISSIVEMATERIALSOURCE,
										    D3DRS_VERTEXBLEND ,
										    D3DRS_CLIPPLANEENABLE ,
										    D3DRS_POINTSIZE ,
										    D3DRS_POINTSIZE_MIN ,
										    D3DRS_POINTSPRITEENABLE ,
										    D3DRS_POINTSCALEENABLE ,
										    D3DRS_POINTSCALE_A ,
										    D3DRS_POINTSCALE_B ,
										    D3DRS_POINTSCALE_C ,
										    D3DRS_MULTISAMPLEANTIALIAS ,
										    D3DRS_MULTISAMPLEMASK ,
										    D3DRS_PATCHEDGESTYLE ,
										    D3DRS_DEBUGMONITORTOKEN ,
										    D3DRS_POINTSIZE_MAX ,
										    D3DRS_INDEXEDVERTEXBLENDENABLE ,
										    D3DRS_COLORWRITEENABLE ,
										    D3DRS_TWEENFACTOR ,
										    D3DRS_BLENDOP ,
										    D3DRS_POSITIONDEGREE ,
										    D3DRS_NORMALDEGREE ,
										    D3DRS_SCISSORTESTENABLE,
										    D3DRS_SLOPESCALEDEPTHBIAS,
										    D3DRS_ANTIALIASEDLINEENABLE ,
										    D3DRS_MINTESSELLATIONLEVEL ,
										    D3DRS_MAXTESSELLATIONLEVEL,
										    D3DRS_ADAPTIVETESS_X,
										    D3DRS_ADAPTIVETESS_Y ,
										    D3DRS_ADAPTIVETESS_Z ,
										    D3DRS_ADAPTIVETESS_W,
										    D3DRS_ENABLEADAPTIVETESSELLATION ,
										    D3DRS_TWOSIDEDSTENCILMODE ,
										    D3DRS_CCW_STENCILFAIL,
										    D3DRS_CCW_STENCILZFAIL,
										    D3DRS_CCW_STENCILPASS ,
										    D3DRS_CCW_STENCILFUNC,
										    D3DRS_COLORWRITEENABLE1,
										    D3DRS_COLORWRITEENABLE2,
										    D3DRS_COLORWRITEENABLE3 ,
										    D3DRS_BLENDFACTOR,
										    D3DRS_SRGBWRITEENABLE,
										    D3DRS_DEPTHBIAS ,
										    D3DRS_WRAP8 ,
										    D3DRS_WRAP9 ,
										    D3DRS_WRAP10 ,
										    D3DRS_WRAP11 ,
										    D3DRS_WRAP12 ,
										    D3DRS_WRAP13,
										    D3DRS_WRAP14 ,
										    D3DRS_WRAP15,
										    D3DRS_SEPARATEALPHABLENDENABLE,
										    D3DRS_SRCBLENDALPHA ,
										    D3DRS_DESTBLENDALPHA ,
										    D3DRS_BLENDOPALPHA};

	static uint8 ZBUFFERTYPE_TRANSLATION[]={		D3DZB_FALSE,
										    D3DZB_TRUE,
										    D3DZB_USEW};
		
	static uint8 FILLMODE_TRANSLATION[]={			D3DFILL_POINT,
			    							D3DFILL_WIREFRAME,
										    D3DFILL_SOLID};
		
	static uint8 SHADEMODE_TRANSLATION[] = {		D3DSHADE_FLAT,
											D3DSHADE_GOURAUD,
											D3DSHADE_PHONG};
	
	static uint8 CULL_TRANSLATION[] ={				D3DCULL_NONE,
										    D3DCULL_CW,
										    D3DCULL_CCW};
							    
	static uint8 BLEND_TRANSLATION[] = {			D3DBLEND_ZERO,
										    D3DBLEND_ONE,
										    D3DBLEND_SRCCOLOR,
										    D3DBLEND_INVSRCCOLOR,
										    D3DBLEND_SRCALPHA,
										    D3DBLEND_INVSRCALPHA,
										    D3DBLEND_DESTALPHA,
										    D3DBLEND_INVDESTALPHA,
										    D3DBLEND_DESTCOLOR,
										    D3DBLEND_INVDESTCOLOR,
										    D3DBLEND_SRCALPHASAT,
										    D3DBLEND_BOTHSRCALPHA,
										    D3DBLEND_BOTHINVSRCALPHA,
										    D3DBLEND_BLENDFACTOR,
										    D3DBLEND_INVBLENDFACTOR};
	
	static uint8 CMPFUNC_TRANSLATION [] = {		D3DCMP_NEVER,
											D3DCMP_LESS,
											D3DCMP_EQUAL,
											D3DCMP_LESSEQUAL,
											D3DCMP_GREATER,
											D3DCMP_NOTEQUAL,
											D3DCMP_GREATEREQUAL,
											D3DCMP_ALWAYS};
	
	static uint8 FOGMODE_TRANSLATION [] =  {		D3DFOG_NONE,
											D3DFOG_EXP,
											D3DFOG_EXP2,
											D3DFOG_LINEAR};
		
	static uint8 STENCILOP_TRANSLATION[] = {		D3DSTENCILOP_KEEP,
										    D3DSTENCILOP_ZERO,
										    D3DSTENCILOP_REPLACE,
										    D3DSTENCILOP_INCRSAT,
										    D3DSTENCILOP_DECRSAT,
										    D3DSTENCILOP_INVERT,
										    D3DSTENCILOP_INCR,
										    D3DSTENCILOP_DECR};

	static uint8 DEBUGMONITORTOKENS_TRANSLATION[]={D3DDMT_ENABLE,
											D3DDMT_DISABLE};	
											
	static uint8 PATCHEDGESTYLE_TRANSLATION[]={	D3DPATCHEDGE_DISCRETE ,
										    D3DPATCHEDGE_CONTINUOUS};			

	static uint16 VERTEXBLENDFLAGS_TRANSLATION[]={	D3DVBF_DISABLE,
	    								   	D3DVBF_1WEIGHTS,
				 	 		  			   	D3DVBF_2WEIGHTS,
										    D3DVBF_3WEIGHTS,
										    D3DVBF_TWEENING,
										    D3DVBF_0WEIGHTS};	
										    
	static uint8 MATERIALCOLORSOURCE_TRANSLATION[]={	D3DMCS_MATERIAL,
	    										D3DMCS_COLOR1,
	    										D3DMCS_COLOR2};
	    										
	static uint8 DEGREETYPE_TRANSLATION[]={		D3DDEGREE_LINEAR,
										    D3DDEGREE_QUADRATIC,
										    D3DDEGREE_CUBIC,
										    D3DDEGREE_QUINTIC};		    										
										    
	static uint8 BLENDOP_TRANSLATION[]={			D3DBLENDOP_ADD,
										    D3DBLENDOP_SUBTRACT,
										    D3DBLENDOP_REVSUBTRACT,
										    D3DBLENDOP_MIN,
										    D3DBLENDOP_MAX};										    

	static D3DTEXTURESTAGESTATETYPE TEXTURESTAGESTATETYPE_TRANSLATION[]={	D3DTSS_COLOROP ,
																    D3DTSS_COLORARG1,
																    D3DTSS_COLORARG2,
																    D3DTSS_ALPHAOP ,
																    D3DTSS_ALPHAARG1 ,
																    D3DTSS_ALPHAARG2 ,
																    D3DTSS_BUMPENVMAT00 ,
																    D3DTSS_BUMPENVMAT01 ,
																    D3DTSS_BUMPENVMAT10 ,
																    D3DTSS_BUMPENVMAT11 ,
																    D3DTSS_TEXCOORDINDEX,
																    D3DTSS_BUMPENVLSCALE,
																    D3DTSS_BUMPENVLOFFSET,
																    D3DTSS_TEXTURETRANSFORMFLAGS ,
																    D3DTSS_COLORARG0 ,
																    D3DTSS_ALPHAARG0 ,
																    D3DTSS_RESULTARG ,
																    D3DTSS_CONSTANT };										    
																    
	static D3DTEXTUREOP TEXTUREOP_TRANSLATION[]={							D3DTOP_DISABLE,
																    D3DTOP_SELECTARG1,
																    D3DTOP_SELECTARG2,
																    D3DTOP_MODULATE,
																    D3DTOP_MODULATE2X,
																    D3DTOP_MODULATE4X,
																    D3DTOP_ADD,
																    D3DTOP_ADDSIGNED,
																    D3DTOP_ADDSIGNED2X,
																    D3DTOP_SUBTRACT,
																    D3DTOP_ADDSMOOTH,
																    D3DTOP_BLENDDIFFUSEALPHA,
																    D3DTOP_BLENDTEXTUREALPHA,
																    D3DTOP_BLENDFACTORALPHA,
																    D3DTOP_BLENDTEXTUREALPHAPM,
																    D3DTOP_BLENDCURRENTALPHA,
																    D3DTOP_PREMODULATE,
																    D3DTOP_MODULATEALPHA_ADDCOLOR,
																    D3DTOP_MODULATECOLOR_ADDALPHA,
																    D3DTOP_MODULATEINVALPHA_ADDCOLOR,
																    D3DTOP_MODULATEINVCOLOR_ADDALPHA,
																    D3DTOP_BUMPENVMAP,
																    D3DTOP_BUMPENVMAPLUMINANCE,
																    D3DTOP_DOTPRODUCT3,
																    D3DTOP_MULTIPLYADD,
																    D3DTOP_LERP};	

	static long TEXTUREARGUMENT_TRANSLATION[]={							D3DTA_CONSTANT  ,
																	D3DTA_CURRENT ,
																	D3DTA_DIFFUSE ,
																	D3DTA_SELECTMASK ,
																	D3DTA_SPECULAR ,
																	D3DTA_TEMP ,
																	D3DTA_TEXTURE  ,
																	D3DTA_TFACTOR ,
																	// Modifier flags
																	D3DTA_ALPHAREPLICATE  ,
																	D3DTA_COMPLEMENT };	
																	
	static ulong TEXTURECOORDINATE_TRANSLATION[]={							D3DTSS_TCI_PASSTHRU ,
																	D3DTSS_TCI_CAMERASPACENORMAL ,
																	D3DTSS_TCI_CAMERASPACEPOSITION ,
																	D3DTSS_TCI_CAMERASPACEREFLECTIONVECTOR ,
																	D3DTSS_TCI_SPHEREMAP };
																	
	static D3DTEXTURETRANSFORMFLAGS TEXTURETRANSFORM_TRANSLATION[]={		D3DTTFF_DISABLE,
																    D3DTTFF_COUNT1,
																    D3DTTFF_COUNT2,
															    	D3DTTFF_COUNT3,
																    D3DTTFF_COUNT4,
																    D3DTTFF_PROJECTED};


// Sampler State
	static D3DTEXTUREFILTERTYPE TEXTUREFILTER_TRANSLATION[]={    	D3DTEXF_NONE,
														    D3DTEXF_POINT,
														    D3DTEXF_LINEAR,
														    D3DTEXF_ANISOTROPIC,
														    D3DTEXF_PYRAMIDALQUAD,
														    D3DTEXF_GAUSSIANQUAD};
	
	static D3DTEXTUREADDRESS TEXTUREADDRESS_TRANSLATION[]={		D3DTADDRESS_WRAP,
															D3DTADDRESS_MIRROR,
															D3DTADDRESS_CLAMP,
															D3DTADDRESS_BORDER,
															D3DTADDRESS_MIRRORONCE};
	
	static D3DSAMPLERSTATETYPE SAMPLERSTATETYPE_TRANSLATION[]={	D3DSAMP_ADDRESSU,
														    D3DSAMP_ADDRESSV,
														    D3DSAMP_ADDRESSW,
														    D3DSAMP_BORDERCOLOR,
														    D3DSAMP_MAGFILTER,
														    D3DSAMP_MINFILTER,
														    D3DSAMP_MIPFILTER,
														    D3DSAMP_MIPMAPLODBIAS,
														    D3DSAMP_MAXMIPLEVEL,
														    D3DSAMP_MAXANISOTROPY,
														    D3DSAMP_SRGBTEXTURE,
														    D3DSAMP_ELEMENTINDEX,
														    D3DSAMP_DMAPOFFSET};

//////////////////////////////////////////////////////////////////////////////////////////////////////
//									Texture
	static D3DFORMAT TEXTUREFORMAT_TRANSLATION[]={	D3DFMT_A8R8G8B8,
													D3DFMT_A8B8G8R8,
													D3DFMT_P8,
													D3DFMT_A16B16G16R16F,
													D3DFMT_A8,
													D3DFMT_A8P8,
													D3DFMT_R5G6B5		};
		/*								
	static D3DPOOL POOL_TRANSLATION[]=	{	
#ifdef NO_DEFAULT_RESSOURCE
										D3DPOOL_SYSTEMMEM	,
#else
										D3DPOOL_DEFAULT		,
#endif
										D3DPOOL_MANAGED		,
										D3DPOOL_SYSTEMMEM	,
										D3DPOOL_MANAGED		,
										};
*/
	/*
	static uint TEXTUREUSAGE_TRANSLATION[]=   {	D3DUSAGE_DYNAMIC,												
												D3DUSAGE_WRITEONLY	};	
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
//									Decl
	static uint8 ELEMENTTYPE_TRANSLATION[]={	D3DDECLTYPE_FLOAT1,
									    D3DDECLTYPE_FLOAT2 ,
									    D3DDECLTYPE_FLOAT3 ,
									    D3DDECLTYPE_FLOAT4 ,
									    D3DDECLTYPE_D3DCOLOR,
									    D3DDECLTYPE_UBYTE4,
									    D3DDECLTYPE_SHORT2,
									    D3DDECLTYPE_SHORT4,
									    D3DDECLTYPE_UBYTE4N,
									    D3DDECLTYPE_SHORT2N,
									    D3DDECLTYPE_SHORT4N,
									    D3DDECLTYPE_USHORT2N,
									    D3DDECLTYPE_USHORT4N,
									    D3DDECLTYPE_UDEC3,
									    D3DDECLTYPE_DEC3N,
									    D3DDECLTYPE_FLOAT16_2,
									    D3DDECLTYPE_FLOAT16_4,
									    D3DDECLTYPE_UNUSED};
									    
	static uint8 ELEMENTUSAGE_TRANSLATION[]={	D3DDECLUSAGE_POSITION ,
									    D3DDECLUSAGE_BLENDWEIGHT,
									    D3DDECLUSAGE_BLENDINDICES,
									    D3DDECLUSAGE_NORMAL,
									    D3DDECLUSAGE_PSIZE,
									    D3DDECLUSAGE_TEXCOORD,
									    D3DDECLUSAGE_TANGENT,
									    D3DDECLUSAGE_BINORMAL,
									    D3DDECLUSAGE_TESSFACTOR,
									    D3DDECLUSAGE_POSITIONT,
									    D3DDECLUSAGE_COLOR,
									    D3DDECLUSAGE_FOG ,
									    D3DDECLUSAGE_DEPTH ,
									    D3DDECLUSAGE_SAMPLE};
									    

//////////////////////////////////////////////////////////////////////////////////////////////////////
//									Stream
/*	static uint STREAMUSAGE_TRANSLATION[]=   {	D3DUSAGE_DONOTCLIP,
												D3DUSAGE_DYNAMIC,
												D3DUSAGE_NPATCHES,
												D3DUSAGE_POINTS,
												D3DUSAGE_RTPATCHES,
												D3DUSAGE_WRITEONLY};	
*/										
	static D3DFORMAT INDEXFORMAT_TRANSLATION[]=  {	D3DFMT_INDEX16,
													D3DFMT_INDEX32	};										

	static D3DPRIMITIVETYPE PRIMITIVETYPE_TRANSLATION[]={	D3DPT_POINTLIST,
												    D3DPT_LINELIST,
												    D3DPT_LINESTRIP,
												    D3DPT_TRIANGLELIST,
												    D3DPT_TRIANGLESTRIP,
												    D3DPT_TRIANGLEFAN};									   
										
	static uint LOCKFLAGS_TRANSLATION[]={		D3DLOCK_DISCARD,
										D3DLOCK_NO_DIRTY_UPDATE,
										D3DLOCK_NOSYSLOCK,
										D3DLOCK_READONLY,
										D3DLOCK_NOOVERWRITE};												    

//////////////////////////////////////////////////////////////////////////////////////////////////////
//									Shader
static const char * SHADERPROFILE_TRANSLATION[] = {
		"vs_1_1",
		"vs_2_0",
		"vs_2_a",
		"vs_3_0",
		"ps_1_1",
		"ps_1_4",
		"ps_2_0",
		"ps_2_a",
		"ps_2_b",
		"ps_3_0"
	};
#endif /*NVHRDWTRANSLATION_H_*/
