/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvHrdwTools.h>
#include <Kernel/PC/NvDpyManager[PC].h>
#include "NvHrdwTranslation.h"

#define	DXM(  PTR )				((D3DXMATRIX*)(PTR))
using namespace nv;
using namespace Hrdw;


namespace
{
	//-----------------------------------------------------------------------------------------
	//								Structures declaration
	//-----------------------------------------------------------------------------------------	

	struct Raster
	{
		bool					inused;
		uint					width;
		uint					height;
		NV_D3DFORMAT			colorFmt;
		uint					colorBpp;
		NV_D3DFORMAT			depthFmt;
		NV_D3DTEXTURE			tex;
		NV_D3DSURFACE			surf;
		NV_D3DSURFACE			depth;		
		NV_D3DTEXTURE			depthTex;
		NV_D3DSURFACE			depthSurf;
	};

	struct Target 
	{
		bool					inused;
		HWND					hwnd;
		NV_D3DSWAPCHAIN			swapChain;
		NV_D3DSURFACE			back;
		uint					width;
		uint					height;
	};

	//-----------------------------------------------------------------------------------------
	//								Variables declaration
	//-----------------------------------------------------------------------------------------	
#ifdef _NVCOMP_ENABLE_DBG
	bool 						BeginFrame = FALSE;
#endif //_NVCOMP_ENABLE_DBG

	NV_D3DDISPLAY_MODE			adapterMode;

	bool						fullscreenMode;
	NV_D3DSURFACE				fullscreenBackBuffer;

	sysvector<Raster>			raster;
	sysvector<Target>			target;
	int							curRaster;
	int							curTarget;
	
	DeviceState					devState;

	// Presentation parameter for init and reset fct
	NV_D3DPRESENT_PARAMETERS	ddevpp; 

	inline
	Raster*
	GetRaster	(	int		inId	)
	{
		NV_ASSERT_RETURN_MTH( NULL, inId>=0 );
		NV_ASSERT_RETURN_MTH( NULL, uint(inId)<raster.size() );
		NV_ASSERT_RETURN_MTH( NULL, raster[inId].inused );
		return &raster[inId];
	}


	inline
	Raster*
	GetCurRaster	()
	{
		return GetRaster( curRaster );
	}


	inline
	Target*
	GetTarget	(	int		inId	)
	{
		NV_ASSERT_RETURN_MTH( NULL, inId>=0 );
		NV_ASSERT_RETURN_MTH( NULL, uint(inId)<target.size() );
		NV_ASSERT_RETURN_MTH( NULL, target[inId].inused );
		return &target[inId];
	}


	inline
	Target*
	GetCurTarget	()
	{
		return GetTarget( curTarget );
	}


	NV_D3DSURFACE
	GetCurTargetBackBuffer	()
	{
		if( fullscreenMode ) {
			NV_ASSERT_RETURN_MTH( NULL, fullscreenBackBuffer );
			return fullscreenBackBuffer;
		} else {
			Target* t = GetCurTarget();
			return t ? t->back : NULL;
		}
	}


	D3DFORMAT
	FindColorFormat (	int		inBpp	)
	{
		D3DFORMAT fmtA[]	 = {	D3DFMT_R3G3B2, D3DFMT_A8R3G3B2,
									D3DFMT_X4R4G4B4, D3DFMT_A4R4G4B4, D3DFMT_X1R5G5B5,
									D3DFMT_R5G6B5, D3DFMT_A1R5G5B5,
									D3DFMT_X8R8G8B8, D3DFMT_R8G8B8, D3DFMT_A8R8G8B8	};

		int		  fmtBppA[] =	{	16, 16,
									16, 16, 16,
									16, 16,
									24, 24,
									32			};

		D3DFORMAT fmt = D3DFMT_UNKNOWN;
		HRESULT   hr;
		for( uint i = 0 ; i < 10 ; i++ ) {
			hr = dd3d->CheckDeviceFormat(		adapter,
												adapterDevType,
												adapterMode.Format,
												D3DUSAGE_RENDERTARGET,
												D3DRTYPE_SURFACE,
												fmtA[i]	);
			if( !SUCCEEDED(hr) )		continue;
			if( fmtBppA[i] != inBpp )	continue;
			fmt = fmtA[i];
		}

		return fmt;
	}


	D3DFORMAT
	FindDepthFormat (	int			inBpp,
						D3DFORMAT	inColorFmt	)
	{
		D3DFORMAT fmtA[]	 = {	D3DFMT_D15S1, D3DFMT_D16,
									D3DFMT_D24X8, D3DFMT_D24X4S4, D3DFMT_D24S8,
									D3DFMT_D32	};

		int		  fmtBppA[]  = {	16, 16,
									24, 24, 24,
									32			};

		D3DFORMAT fmt = D3DFMT_UNKNOWN;
		HRESULT   hr;
		for( uint i = 0 ; i < 6 ; i++ ) {
			hr = dd3d->CheckDeviceFormat(		adapter,
												adapterDevType,
												adapterMode.Format,
												D3DUSAGE_DEPTHSTENCIL,
												D3DRTYPE_SURFACE,
												fmtA[i]	);
			if( !SUCCEEDED(hr) )			continue;
			hr =  dd3d->CheckDepthStencilMatch(	adapter,
												adapterDevType,
												adapterMode.Format,
												inColorFmt,
												fmtA[i]	);
			if( !SUCCEEDED(hr) )			continue;
			if( fmtBppA[i] != inBpp )		continue;
			fmt = fmtA[i];
		}
	
		return fmt;
	}													


	D3DFORMAT
	FindBestDepthFormat		(	int			inBpp,
								D3DFORMAT	inColorFmt	)
	{
		// 32 BPP
		if( inBpp >= 32 ) {
			D3DFORMAT fmt = FindDepthFormat( 32, inColorFmt );
			if( fmt != D3DFMT_UNKNOWN )
				return fmt;
		}
		// 24 BPP
		if( inBpp >= 24 ) {
			D3DFORMAT fmt = FindDepthFormat( 24, inColorFmt );
			if( fmt != D3DFMT_UNKNOWN )
				return fmt;
		}
		// 16 BPP
		if( inBpp >= 16 ) {
			D3DFORMAT fmt = FindDepthFormat( 16, inColorFmt );
			if( fmt != D3DFMT_UNKNOWN )
				return fmt;
		}
		return D3DFMT_UNKNOWN;
	}


	bool	CreateRasterBase	(	uint				inW,									
									uint				inH,
									D3DFORMAT			inColorFmt,
									D3DFORMAT			inDepthFmt,
									NV_D3DTEXTURE*		outTex,
									NV_D3DSURFACE*		outSurf,
									NV_D3DSURFACE*		outDepth		= NULL,
									NV_D3DTEXTURE*		outDepthTex		= NULL,
									NV_D3DSURFACE*		outDepthSurf	= NULL	)
	{
		NV_ASSERT_RETURN_MTH( FALSE, inW > 0 );
		NV_ASSERT_RETURN_MTH( FALSE, inH > 0 );
		NV_ASSERT_RETURN_MTH( FALSE, outTex  );
		NV_ASSERT_RETURN_MTH( FALSE, outSurf );
		NV_ASSERT_RETURN_MTH( FALSE, inDepthFmt==D3DFMT_UNKNOWN || outDepth );

		if( outTex )		*outTex			= NULL;
		if( outSurf )		*outSurf		= NULL;
		if( outDepth )		*outDepth		= NULL;
		if( outDepthTex )	*outDepthTex	= NULL;
		if( outDepthSurf )	*outDepthSurf	= NULL;

		// Texture as render target
		HRESULT hr;
		hr = ddev->CreateTexture( inW, inH, 1, D3DUSAGE_RENDERTARGET, inColorFmt, D3DPOOL_DEFAULT, outTex, NULL );
		NV_ASSERT_DX(hr);
		if( hr != D3D_OK )
			return FALSE;

		// Surface of texture
		hr = (*outTex)->GetSurfaceLevel( 0, outSurf );
		NV_ASSERT_DX( hr );
		if( hr != D3D_OK ) {
			SafeRelease( *outTex );
			return FALSE;
		}

		if( inDepthFmt!=D3DFMT_UNKNOWN && outDepth )
		{
			hr = ddev->CreateDepthStencilSurface( inW, inH, inDepthFmt, D3DMULTISAMPLE_NONE, 0, TRUE, outDepth, NULL );
			NV_ASSERT_DX(hr);
			if( hr != D3D_OK ) {
				SafeRelease( *outTex );
				SafeRelease( *outSurf );
				return FALSE;
			}

			if( outDepthTex && outDepthSurf )
			{
				hr = ddev->CreateTexture( inW, inH, 1, D3DUSAGE_RENDERTARGET, /*D3DFMT_R32F*/inColorFmt, D3DPOOL_DEFAULT, outDepthTex, NULL );
				NV_ASSERT_DX(hr);
				if( hr != D3D_OK ) {
					SafeRelease( *outTex );
					SafeRelease( *outSurf );
					SafeRelease( *outDepth );
					return FALSE;
				}

				hr = (*outDepthTex)->GetSurfaceLevel( 0, outDepthSurf );
				NV_ASSERT_DX(hr);
				if( hr != D3D_OK ) {
					SafeRelease( *outTex );
					SafeRelease( *outSurf );
					SafeRelease( *outDepth );
					SafeRelease( *outDepthTex );
					return FALSE;
				}
			}
		}

		return TRUE;
	}


	bool	CreateTargetBase	(	HWND				inHwnd,
									uint				inWidth,
									uint				inHeight,
									NV_D3DSWAPCHAIN&	outSwapChain,
									NV_D3DSURFACE&		outBack	)
	{
		D3DPRESENT_PARAMETERS vpp;
		memset( &vpp, 0, sizeof(D3DPRESENT_PARAMETERS) );
		vpp.BackBufferWidth			= inWidth;
		vpp.BackBufferHeight		= inHeight;
		vpp.BackBufferCount			= 1;
		vpp.BackBufferFormat		= D3DFMT_UNKNOWN;
		vpp.Windowed				= TRUE;
		vpp.hDeviceWindow			= inHwnd;
		vpp.MultiSampleType			= D3DMULTISAMPLE_NONE;
		vpp.MultiSampleQuality		= 0;
		vpp.EnableAutoDepthStencil	= FALSE;
		vpp.AutoDepthStencilFormat	= D3DFMT_UNKNOWN;
		vpp.SwapEffect				= D3DSWAPEFFECT_COPY;
		vpp.Flags					= 0;
		vpp.PresentationInterval	= D3DPRESENT_INTERVAL_IMMEDIATE;	//D3DPRESENT_INTERVAL_DEFAULT;

		HRESULT	hr;

		hr = ddev->CreateAdditionalSwapChain( &vpp, &outSwapChain );
		NV_ASSERT( SUCCEEDED(hr) );
		if( !SUCCEEDED(hr) ) {
			return FALSE;
		}

		hr = outSwapChain->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &outBack );
		NV_ASSERT( SUCCEEDED(hr) );
		if( !SUCCEEDED(hr) ) {
			outSwapChain->Release();
			outSwapChain = NULL;
			return FALSE;
		}

		return TRUE;
	}


	bool RegReadDWORD( DWORD* outData, HKEY inHKey, PCSTR inPath, PCSTR inValue )
	{
		bool	res = FALSE;
		HKEY	k;
		DWORD	data;
		DWORD	dwType = REG_DWORD;
		DWORD	dwSize = sizeof(DWORD);
		if( RegOpenKeyEx(inHKey,inPath,0L,KEY_ALL_ACCESS,&k) == ERROR_SUCCESS ) {
			if( RegQueryValueEx(k,inValue,NULL,&dwType,(LPBYTE)&data,&dwSize) == ERROR_SUCCESS ) {
				if( outData )
					*outData = data;
				res = TRUE;
			}
		    RegCloseKey( k );
		}
		return res;
	}

	struct D3D_ctrlvalue
	{
		bool	avai;
		DWORD	data;
	};

	struct D3D_ctrlpanel
	{
		D3D_ctrlvalue	LoadDebugRuntime;
		D3D_ctrlvalue	EnableDebugging;
		D3D_ctrlvalue	FullDebug;
		D3D_ctrlvalue	BreakOnMemLeak;
		D3D_ctrlvalue	BreakOnDPF;
		D3D_ctrlvalue	BreakOnAllocID;
		D3D_ctrlvalue	EnableMultimonDebugging;
		D3D_ctrlvalue	EnumRamp;
		D3D_ctrlvalue	EnumReference;
		D3D_ctrlvalue	SoftwareOnly;

		bool	RegRead()
		{
			#define	REG_READ_CTRL_VALUE( inName, inPath )	inName.avai = RegReadDWORD( &inName.data, HKEY_LOCAL_MACHINE, inPath, #inName );
			REG_READ_CTRL_VALUE( LoadDebugRuntime, "SOFTWARE\\Microsoft\\Direct3D" )
			REG_READ_CTRL_VALUE( EnableDebugging, "SOFTWARE\\Microsoft\\Direct3D" )
			REG_READ_CTRL_VALUE( FullDebug, "SOFTWARE\\Microsoft\\Direct3D" )
			REG_READ_CTRL_VALUE( BreakOnMemLeak, "SOFTWARE\\Microsoft\\Direct3D" )
			REG_READ_CTRL_VALUE( BreakOnDPF, "SOFTWARE\\Microsoft\\Direct3D" )
			REG_READ_CTRL_VALUE( BreakOnAllocID, "SOFTWARE\\Microsoft\\Direct3D" )
			REG_READ_CTRL_VALUE( EnableMultimonDebugging, "SOFTWARE\\Microsoft\\Direct3D" )
			REG_READ_CTRL_VALUE( EnumRamp, "SOFTWARE\\Microsoft\\Direct3D\\Drivers" )
			REG_READ_CTRL_VALUE( EnumReference, "SOFTWARE\\Microsoft\\Direct3D\\Drivers" )
			REG_READ_CTRL_VALUE( SoftwareOnly, "SOFTWARE\\Microsoft\\Direct3D\\Drivers" )
			#undef	REG_READ_CTRL_VALUE
			return TRUE;
		}
	};
}


extern Hrdw::DeclIndex CurDecl;	
	

uint
Hrdw::GetNbScreen		(							)
{
	bool dd3d_to_release = FALSE;
	if( !dd3d ) {
		dd3d = NV_D3D_CREATE( D3D_SDK_VERSION );
		if( !dd3d )
			return 0;
		dd3d_to_release = TRUE;
	}

	uint nbs = dd3d->GetAdapterCount();

	if( dd3d_to_release )
		SafeRelease( dd3d );

	return nbs;
}


bool
Hrdw::ProbeFullscreen	(	uint			inAdapter,
							uint			inWidth,
							uint			inHeight				)
{
	bool dd3d_to_release = FALSE;
	if( !dd3d ) {
		dd3d = NV_D3D_CREATE( D3D_SDK_VERSION );
		if( !dd3d )
			return 0;
		dd3d_to_release = TRUE;
	}

	// Find best display mode
	uint modeCpt = dd3d->GetAdapterModeCount( inAdapter, D3DFMT_X8R8G8B8 );
	bool modeFound = FALSE;
	for( uint m = 0 ; m < modeCpt ; m++ ) {
		NV_D3DDISPLAY_MODE mode;
		HRESULT hr = dd3d->EnumAdapterModes( inAdapter, D3DFMT_X8R8G8B8, m, &mode );
		NV_ASSERT( SUCCEEDED(hr) );
		if( mode.Width  != inWidth  )			continue;
		if( mode.Height != inHeight )			continue;
		if( mode.Format != D3DFMT_X8R8G8B8 )	continue;
		if( mode.RefreshRate != 60 )			continue;
		modeFound = TRUE;
		break;
	}

	if( dd3d_to_release )
		SafeRelease( dd3d );

	return modeFound;
}


bool
Hrdw::FindBestFullscreen	(	uint			inAdapter,
								uint			inWidth,
								uint			inHeight,
								uint&			outBestWidth,
								uint&			outBestHeight			)
{
	bool dd3d_to_release = FALSE;
	if( !dd3d ) {
		dd3d = NV_D3D_CREATE( D3D_SDK_VERSION );
		if( !dd3d )
			return 0;
		dd3d_to_release = TRUE;
	}

	outBestWidth  = 0;
	outBestHeight = 0;

	// Find best display mode
	uint modeCpt = dd3d->GetAdapterModeCount( inAdapter, D3DFMT_X8R8G8B8 );
	bool modeFound = FALSE;
	for( uint m = 0 ; m < modeCpt ; m++ ) {
		NV_D3DDISPLAY_MODE mode;
		HRESULT hr = dd3d->EnumAdapterModes( inAdapter, D3DFMT_X8R8G8B8, m, &mode );
		NV_ASSERT( SUCCEEDED(hr) );
		if( mode.Format != D3DFMT_X8R8G8B8 )	continue;
		if( mode.RefreshRate != 60 && mode.RefreshRate != 0) continue;
		int best_dw = int( outBestWidth )  - int( inWidth );
		int best_dh = int( outBestHeight ) - int( inHeight );
		int best_d  = best_dw*best_dw + best_dh*best_dh;
		int dw		= int( mode.Width )    - int( inWidth );
		int dh		= int( mode.Height )   - int( inHeight );
		int d		= dw*dw + dh*dh;
		if( (d < best_d) || ( (best_d!=0) && (best_dw>0 || best_dh>0) ) ) {
			outBestWidth  = mode.Width;
			outBestHeight = mode.Height;
			modeFound = TRUE;
		}
	}

	if( dd3d_to_release )
		SafeRelease( dd3d );

	return modeFound;
}

DeviceState 
Hrdw::GetDeviceState ()
{
	return devState;
}

void
Hrdw::UpdateDeviceState () {
	if (devState == NV_DEVICE_NULL) 
		return;

	NV_ASSERT(ddev);
	HRESULT hr = ddev->TestCooperativeLevel();
	// D3DERR_DEVICELOST
	// D3DERR_DEVICENOTRESET
	// D3DERR_DRIVERINTERNALERROR
	// D3D_OK
	if (hr == D3DERR_DRIVERINTERNALERROR)
		return;

	switch (devState) {
		case NV_DEVICE_OK:
			if (hr == D3DERR_DEVICELOST) {
				devState = NV_DEVICE_LOST;
			}
			else if (hr == D3DERR_DEVICENOTRESET) {
				devState = NV_DEVICE_NEED_RESET;
			}
			break;

		case NV_DEVICE_LOST:
			if (hr == D3DERR_DEVICENOTRESET) {
				devState = NV_DEVICE_NEED_RESET;
			}
			else if (hr == D3D_OK ) {
				devState = NV_DEVICE_OK;
			}
			break;

		case NV_DEVICE_NEED_RESET:
			if (hr == D3DERR_DEVICELOST) {
				devState = NV_DEVICE_LOST;
			}
			else if (hr == D3D_OK ) {
				devState = NV_DEVICE_OK;
			}
			break;
	};
}

bool
Hrdw::Init	(	uint		inAdapter,
				HWND		inFocusWindow,
				bool		inFullscreen,
				uint		inFullscreenWidth,
				uint		inFullscreenHeight,
				bool		inSoftVtxProcessing	)
{
	devState = NV_DEVICE_NULL;

	D3D_ctrlpanel d3dpanel;
	d3dpanel.RegRead();
	if( d3dpanel.LoadDebugRuntime.avai )		Printf(		"DpyManager: D3D control pannel settings: LoadDebugRuntime (%xh)\n",	uint(d3dpanel.LoadDebugRuntime.data)		);
	if( d3dpanel.EnableDebugging.avai )			Printf(		"DpyManager: D3D control pannel settings: EnableDebugging (%xh)\n",		uint(d3dpanel.EnableDebugging.data)			);
	if( d3dpanel.FullDebug.avai )				Printf(		"DpyManager: D3D control pannel settings: FullDebug (%xh)\n",			uint(d3dpanel.FullDebug.data)				);
	if( d3dpanel.BreakOnMemLeak.avai )			Printf(		"DpyManager: D3D control pannel settings: BreakOnMemLeak (%xh)\n",		uint(d3dpanel.BreakOnMemLeak.data)			);
	if( d3dpanel.BreakOnDPF.avai )				Printf(		"DpyManager: D3D control pannel settings: BreakOnDPF (%xh)\n",			uint(d3dpanel.BreakOnDPF.data)				);
	if( d3dpanel.BreakOnAllocID.avai )			Printf(		"DpyManager: D3D control pannel settings: BreakOnAllocID (%xh)\n",		uint(d3dpanel.BreakOnAllocID.data)			);
	if( d3dpanel.EnableMultimonDebugging.avai )	DebugPrintf("DpyManager: D3D: EnableMultimonDebugging (%xh)\n",						uint(d3dpanel.EnableMultimonDebugging.data)	);
	if( d3dpanel.EnumRamp.avai )				Printf(		"DpyManager: D3D control pannel settings: EnumRamp (%xh)\n",			uint(d3dpanel.EnumRamp.data)				);
	if( d3dpanel.EnumReference.avai )			Printf(		"DpyManager: D3D control pannel settings: EnumReference (%xh)\n",		uint(d3dpanel.EnumReference.data)			);
	if( d3dpanel.SoftwareOnly.avai )			Printf(		"DpyManager: D3D control pannel settings: SoftwareOnly (%xh)\n",		uint(d3dpanel.SoftwareOnly.data)			);

	// Check "Maximum validation" is Off !
	if(		d3dpanel.LoadDebugRuntime.avai
		&&	d3dpanel.EnableDebugging.avai
		&&	d3dpanel.FullDebug.avai )
	{
		if(		d3dpanel.LoadDebugRuntime.data
			&&	d3dpanel.EnableDebugging.data
			&&	d3dpanel.FullDebug.data	)
		{
			NV_WARNING_ONCE( "DpyManager :\n>> Maximum debug validation is activated on shaders.\n>> This can lead to some D3D debug warnings/errors.\n>> Please turn off the Maximum-Validation option in the DirectX SDK control panel !" );
		}
	}

	HRESULT hr;

	// D3D interface
	ddev = NULL;
	dd3d = NV_D3D_CREATE( D3D_SDK_VERSION );
	if( dd3d == NULL )
		return FALSE;

	devState = NV_DEVICE_OK;

	// Setup adapter
	adapter = inAdapter;
	if( adapter > dd3d->GetAdapterCount() )
		adapter = D3DADAPTER_DEFAULT;
	adapterDevType	= D3DDEVTYPE_HAL;

	// Adapter caps
	hr = dd3d->GetDeviceCaps( adapter, adapterDevType, &ddevCaps );
	if( !SUCCEEDED(hr) )
	{
		// No HW device (perhaps debug mode) -> REF
		adapterDevType = D3DDEVTYPE_REF;
		hr = dd3d->GetDeviceCaps( adapter, adapterDevType, &ddevCaps );
		if( !SUCCEEDED(hr) )
		{
			dd3d->Release();
			return FALSE;
		}
	}

	DWORD devBhvFlags;
	if( inSoftVtxProcessing )
	{
		devBhvFlags = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
		DebugPrintf( "DpyManager: Software vertex processing !\n");
	}
	else
	{
		devBhvFlags = D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE;
		DebugPrintf( "DpyManager: Hardware vertex processing !\n");
	}

	//
	// Init application device

	fullscreenMode = inFullscreen;
	if( fullscreenMode )
	{
		char tmp[256];
		// Find best display mode
		uint modeCpt = dd3d->GetAdapterModeCount( adapter, D3DFMT_X8R8G8B8 );
		bool found = FALSE;
		for( uint m = 0 ; m < modeCpt ; m++ ) {
			hr = dd3d->EnumAdapterModes( adapter, D3DFMT_X8R8G8B8, m, &adapterMode );
			NV_ASSERT( SUCCEEDED(hr) );

			Sprintf(tmp,"DX enum-mode [%d]: %d x %d, frate=%d\n", m, adapterMode.Width, adapterMode.Height, adapterMode.RefreshRate );
			OutputDebugStr(tmp);
			if( adapterMode.Width  != inFullscreenWidth  )	continue;
			if( adapterMode.Height != inFullscreenHeight )	continue;
			if( adapterMode.Format != D3DFMT_X8R8G8B8 )		continue;
			if( adapterMode.RefreshRate != 60 && adapterMode.RefreshRate != 0)				continue;
			found = TRUE;
			break;
		}
		if( !found ) {
			NV_ERROR( "No matching fullscreen display mode found !" );
			dd3d->Release();
			return FALSE;
		}
	}
	else
	{
		// Get windowed display mode
		dd3d->GetAdapterDisplayMode( adapter, &adapterMode );
	}

	// Check support of 32Bits Texture
	hr = dd3d->CheckDeviceFormat( adapter, adapterDevType, adapterMode.Format, 0, D3DRTYPE_TEXTURE, D3DFMT_A8R8G8B8 );
	NV_ASSERT( SUCCEEDED(hr) );
	if( !SUCCEEDED(hr) )
	{
		NV_ERROR( "32bpp texture not supported !" );
		dd3d->Release();
		return FALSE;
	}

	// Create device
	// Pas d'antialiasing en utilisant le multisample
	// car le back buffer est une texture et non un render target !
	// -> supersampling � la place

	Memset( &ddevpp, 0, sizeof(NV_D3DPRESENT_PARAMETERS) );
	ddevpp.Flags							= 0;
	ddevpp.MultiSampleType					= D3DMULTISAMPLE_NONE;
	ddevpp.MultiSampleQuality				= 0;
	ddevpp.EnableAutoDepthStencil			= FALSE;
	ddevpp.AutoDepthStencilFormat			= D3DFMT_UNKNOWN;
	ddevpp.BackBufferCount					= 1;
	ddevpp.BackBufferWidth					= fullscreenMode ? adapterMode.Width		: 16;
	ddevpp.BackBufferHeight					= fullscreenMode ? adapterMode.Height		: 16;
	ddevpp.BackBufferFormat					= fullscreenMode ? D3DFMT_A8R8G8B8			: D3DFMT_UNKNOWN;
	ddevpp.SwapEffect						= fullscreenMode ? D3DSWAPEFFECT_FLIP		: D3DSWAPEFFECT_DISCARD;
	ddevpp.hDeviceWindow					= fullscreenMode ? inFocusWindow			: GetDesktopWindow();
	ddevpp.Windowed							= fullscreenMode ? FALSE					: TRUE;
	ddevpp.FullScreen_RefreshRateInHz		= fullscreenMode ? adapterMode.RefreshRate	: 0;
	ddevpp.PresentationInterval				= fullscreenMode ? D3DPRESENT_INTERVAL_ONE	: 0;

	hr = dd3d->CreateDevice( adapter, adapterDevType, inFocusWindow, devBhvFlags, &ddevpp, &ddev );
	NV_ASSERT( SUCCEEDED(hr) );
	if( !SUCCEEDED(hr) )
	{
		NV_ERROR( "Creation of DX device has failed !" );
		dd3d->Release();
		return FALSE;
	}

	if( fullscreenMode )
	{
		hr = ddev->GetBackBuffer( 0, 0, D3DBACKBUFFER_TYPE_MONO, &fullscreenBackBuffer );
		NV_ASSERT( SUCCEEDED(hr) );
		ddev->ShowCursor( FALSE );
	}
	else
	{
		bool hide_cursor;
		core::GetParameter( core::PR_HIDE_CURSOR, &hide_cursor );
		if( hide_cursor )
			ddev->ShowCursor( FALSE );
		fullscreenBackBuffer = NULL;
	}

	raster.Init();
	target.Init();
	curRaster = -1;
	curTarget = -1;

	Hrdw_InitShaders();
	Hrdw_InitStream();
	Hrdw_InitDeclaration();
	Hrdw_InitTexture();

	core::SetParameter( core::PR_DX_D3D,	uint32(dd3d) );
	core::SetParameter( core::PR_DX_D3DDEV, uint32(ddev) );

	return TRUE;
}



bool Hrdw::Shut	(		)
{
	tools::ReleaseData	(	);


	for( uint i = 0 ; i < 8 ; i++ )
		ddev->SetTexture( i, NULL );

	for( uint i = 0 ; i < raster.size() ; i++ ) {
		if( !raster[i].inused )	continue;
		SafeRelease( raster[i].tex );
		SafeRelease( raster[i].surf );
		SafeRelease( raster[i].depth );
		SafeRelease( raster[i].depthSurf );
		SafeRelease( raster[i].depthTex );
	}	
	raster.Shut();

	for( uint i = 0 ; i < target.size() ; i++ ) {
		if( !target[i].inused )	continue;
		SafeRelease( target[i].swapChain );
		SafeRelease( target[i].back );
	}	
	target.Shut();

	Hrdw_ShutShaders();
	Hrdw_ShutStream();
	Hrdw_ShutDeclaration();
	Hrdw_ShutTexture();

	SafeRelease( fullscreenBackBuffer );
	SafeRelease( ddev );
	SafeRelease( dd3d );	

	devState = NV_DEVICE_NULL;
	return TRUE;	
}




bool Hrdw::ResetDevice ( ){
	HRESULT hr;

	// Release All target and raster
	SafeRelease( fullscreenBackBuffer );

	if(!fullscreenMode) {
		for( uint i = 0 ; i < target.size() ; i++ ) {
			if( target[i].inused ) {
				Target & t = target[i];
				SafeRelease( t.swapChain );
				SafeRelease( t.back );
				t.swapChain = 0; t.back = 0;
			}
		}
	}

	for( uint i = 0 ; i < raster.size() ; i++ ) {
		if( raster[i].inused ) {
			Raster& r = raster[i];
			SafeRelease( r.tex );
			SafeRelease( r.surf );
			SafeRelease( r.depth );
			SafeRelease( r.depthTex );
			SafeRelease( r.depthSurf );
			r.tex = 0; r.surf = 0; r.depth = 0; r.depthTex = 0; r.depthSurf = 0;
		}
	}

	// Release all Stream that have been allocated with D3DDEFAULT_POOL flag.
	StreamOnDeviceLost();
	TextureOnDeviceLost();

	// reset D3D device
	hr = ddev->Reset(&ddevpp);
	NV_ASSERT(hr == D3D_OK);
	if (hr!=D3D_OK)
		return FALSE;

	// recreate all raster / target
	if( fullscreenMode )
	{
		hr = ddev->GetBackBuffer( 0, 0, D3DBACKBUFFER_TYPE_MONO, &fullscreenBackBuffer );
		NV_ASSERT( SUCCEEDED(hr) );
		if (!SUCCEEDED(hr)) return FALSE;
		ddev->ShowCursor( FALSE );
	}
	else
	{
		bool hide_cursor;
		core::GetParameter( core::PR_HIDE_CURSOR, &hide_cursor );
		fullscreenBackBuffer = NULL;
		if( hide_cursor )
			ddev->ShowCursor( FALSE );
	}

	bool creationDone = true;
	if(!fullscreenMode) {
		for( uint i = 0 ; i < target.size() ; i++ ) {
			if( target[i].inused ) {
				Target & t = target[i];
				creationDone &= CreateTargetBase(t.hwnd,t.width,t.height,t.swapChain,t.back);
			}
		}
	}

	for( uint i = 0 ; i < raster.size() ; i++ ) {
		if( raster[i].inused ) {
			Raster& r = raster[i];
			creationDone &= CreateRasterBase( r.width,r.height,r.colorFmt,r.depthFmt,
											  &r.tex, &r.surf,	&r.depth,
											  r.depthTex	?	&r.depthTex	:NULL,
											  r.depthSurf	?	&r.depthSurf:NULL) ;
		}
	}

	// Re-allocate all Stream that have been allocated with D3DDEFAULT_POOL flag.
	creationDone &= StreamOnDeviceReset();
	creationDone &= TextureOnDeviceReset();
	return creationDone;
}

bool
Hrdw::IsFullscreen			(									)
{
	return fullscreenMode;
}

int
Hrdw::CreateTarget			(	HWND			inHwnd,
								uint			inWidth,
								uint			inHeight		)
{
	if( fullscreenMode		)	return -1;		// No aux target in fullscreen !
	if( !inHwnd				)	return -1;
	if( !IsWindow(inHwnd)	)	return -1;

	if( !inWidth || !inHeight ) {
		RECT r;
		GetClientRect( inHwnd, &r );
		if( !inWidth )		inWidth  = r.right;
		if( !inHeight )		inHeight = r.bottom;
		if( !inWidth || !inHeight )		return -1;
	}

	NV_D3DSWAPCHAIN	swapChain;
	NV_D3DSURFACE	back;
	if( !CreateTargetBase(inHwnd,inWidth,inHeight,swapChain,back) )
		return -1;

	Target t;
	t.inused	= TRUE;
	t.hwnd		= inHwnd;
	t.swapChain	= swapChain;
	t.back		= back;
	t.width		= inWidth;
	t.height	= inHeight;

	for( uint i = 0 ; i < target.size() ; i++ ) {
		if( !target[i].inused ) {
			target[i] = t;
			return i;
		}
	}

	target.push_back( t );
	return int( target.size() - 1 );
}


bool
Hrdw::FreeTarget			(	int				inId			)
{
	Target* t = GetTarget( inId );
	if( !t )	return FALSE;
	SafeRelease( t->swapChain );
	SafeRelease( t->back );
	Memset( t, 0, sizeof(Target) );
	if( curTarget == inId )
		curTarget = -1;
	return TRUE;
}


bool
Hrdw::ResizeTarget			(	int				inId,
								uint			inWidth,
								uint			inHeight			)
{
	Target* t = GetTarget( inId );
	if( !t )	return FALSE;

	if( !IsWindow(t->hwnd) ) {
		FreeTarget( inId );
		return FALSE;
	}

	if( !inWidth || !inHeight ) {
		RECT r;
		GetClientRect( t->hwnd, &r );
		if( !inWidth )		inWidth  = r.right;
		if( !inHeight )		inHeight = r.bottom;
		if( !inWidth || !inHeight )		return FALSE;
	}

	NV_D3DSURFACEDESC sd;
	t->back->GetDesc( &sd );
	if( sd.Width==inWidth && sd.Height==inHeight )
		return TRUE;

	NV_D3DSWAPCHAIN	swapChain;
	NV_D3DSURFACE	back;
	if( !CreateTargetBase(t->hwnd,inWidth,inHeight,swapChain,back) )
		return FALSE;

	SafeRelease( t->swapChain );
	SafeRelease( t->back );
	t->swapChain	= swapChain;
	t->back			= back;
	return TRUE;
}


uint
Hrdw::GetTargetHeight		(	int				inId				)
{
	Target* t = GetTarget( inId );
	if( !t )	return 0;
	NV_D3DSURFACEDESC sd;
	t->back->GetDesc( &sd );
	return sd.Height;
}


uint
Hrdw::GetTargetWidth		(	int				inId				)
{
	Target* t = GetTarget( inId );
	if( !t )	return 0;
	NV_D3DSURFACEDESC sd;
	t->back->GetDesc( &sd );
	return sd.Width;
}

int
Hrdw::CreateRaster			(	uint			inW,									
								uint			inH,
								uint			inColorBPP,
								uint			inDepthBPP,
								bool			inWithDepthTex		)
{
	if( !inColorBPP )
		return -1;

	D3DFORMAT colorFmt = FindColorFormat( inColorBPP );
	D3DFORMAT depthFmt = inDepthBPP ? FindBestDepthFormat(inDepthBPP,colorFmt) : D3DFMT_UNKNOWN;
	NV_ASSERT_RETURN_MTH( -1, !inDepthBPP || depthFmt!=D3DFMT_UNKNOWN );

	NV_D3DTEXTURE	tex, depthTex=NULL;
	NV_D3DSURFACE	surf, depth, depthSurf=NULL;
	if( !CreateRasterBase(inW,inH,colorFmt,depthFmt,
						  &tex, &surf,&depth,
						  inWithDepthTex?&depthTex:NULL,
						  inWithDepthTex?&depthSurf:NULL) )
		return -1;

	Raster r;
	r.inused	= TRUE;
	r.width		= inW;
	r.height	= inH;
	r.colorFmt	= colorFmt;
	r.colorBpp	= inColorBPP;
	r.depthFmt	= depthFmt;
	r.tex		= tex;
	r.surf		= surf;
	r.depth		= depth;
	r.depthTex	= depthTex;
	r.depthSurf	= depthSurf;

	for( uint i = 0 ; i < raster.size() ; i++ ) {
		if( !raster[i].inused ) {
			raster[i] = r;
			return i;
		}
	}

	raster.push_back( r );
	return int( raster.size() - 1 );
}


bool
Hrdw::FreeRaster			(	int			inId				)
{
	Raster* r = GetRaster( inId );
	if( !r )	return FALSE;
	SafeRelease( r->tex );
	SafeRelease( r->surf );
	SafeRelease( r->depth );
	SafeRelease( r->depthTex );
	SafeRelease( r->depthSurf );
	Memset( r, 0, sizeof(Raster) );
	if( curRaster == inId )
		curRaster = -1;
	return TRUE;
}


uint
Hrdw::GetRasterHeight		(	int			inId				)
{
	Raster* r = GetRaster( inId );
	return r ? r->height : 0;
}


uint
Hrdw::GetRasterWidth		(	int			inId				)
{
	Raster* r = GetRaster( inId );
	return r ? r->width : 0;
}

void
Hrdw::GetRasterFormat	(	int				inId				,
							uint *			oColorBPP			,
							uint *			oDepthBPP			,
							uint *			oStencilBPP			)
{
	Raster* r = GetRaster( inId );
	if (!r) return ;

	const D3DFORMAT	fmtA[]	 = {	D3DFMT_D15S1, D3DFMT_D16,
									D3DFMT_D24X8, D3DFMT_D24X4S4, D3DFMT_D24S8,
									D3DFMT_D32	};
	const uint		dbppA[]	 = {	15, 16,
									24, 24, 24,
									32	};
	const uint		sbppA[]	 = {	1, 0,
									0, 4, 8,
									0	};

	if (oColorBPP)
		*oColorBPP=r->colorBpp;

	for (uint i = 0 ; i < 6 ; ++i ) {
		if (r->depthFmt == 	fmtA[i]) {
			if (oDepthBPP) 
				* oDepthBPP = dbppA[i];
			if (oStencilBPP)
				*oStencilBPP = sbppA[i];
			return;
		}
	}
	if (oDepthBPP) 
		* oDepthBPP = 0;
	if (oStencilBPP)
		*oStencilBPP = 0;
}

bool
Hrdw::ResizeRaster			(	int				inId,
								uint			inWidth,
								uint			inHeight		)
{
	Raster* r = GetRaster( inId );
	if( !r )	return FALSE;

	if( !inWidth || !inHeight )
		return FALSE;

	if( r->width==inWidth && r->height==inHeight )
		return TRUE;

	NV_D3DTEXTURE	tex, depthTex=NULL;
	NV_D3DSURFACE	surf, depth, depthSurf=NULL;
	if( !CreateRasterBase(inWidth,inHeight,r->colorFmt,r->depthFmt,
						  &tex, &surf,&depth,
						  r->depthTex?&depthTex:NULL,
						  r->depthSurf?&depthSurf:NULL) )
		return FALSE;

	r->width		= inWidth;
	r->height		= inHeight;
	r->tex			= tex;
	r->surf			= surf;
	r->depth		= depth;
	r->depthTex		= depthTex;
	r->depthSurf	= depthSurf;

	return TRUE;
}


bool
Hrdw::ReadRaster	(	pvoid	outBuffer,
						int		inId		)
{
	if( !outBuffer )	return FALSE;

	Raster* r = GetRaster( inId );
	if( !r )			return FALSE;
	NV_ASSERT_RETURN_MTH( FALSE, r->surf );

	// Check StretchRect() support
	if( (ddevCaps.DevCaps2 & D3DDEVCAPS2_CAN_STRETCHRECT_FROM_TEXTURES) == 0 )
		return FALSE;

	HRESULT		  hr;
	NV_D3DSURFACE surf = NULL;
	hr = ddev->CreateRenderTarget(	r->width,
									r->height,
									r->colorFmt,
									D3DMULTISAMPLE_NONE,
									0,
									TRUE,
									&surf,
									NULL );
	NV_ASSERT_DX(hr);
	if( hr != D3D_OK )
		return FALSE;

	hr = ddev->StretchRect( r->surf, NULL, surf, NULL, D3DTEXF_NONE );
	if( hr != D3D_OK ) {
		surf->Release();
		return FALSE;
	}

	D3DLOCKED_RECT rect;
	hr = surf->LockRect( &rect, NULL, D3DLOCK_READONLY  );
	if( hr != D3D_OK ) {
		surf->Release();
		return FALSE;
	}

	uint   bytePerPixel = r->colorBpp >> 3;
	uint   bytePerLine  = r->width * bytePerPixel;
	uint8* dst = (uint8*) outBuffer;
	for( uint y = 0 ; y < r->height ; y++ ) {
		uint8* src = ((uint8*)rect.pBits) + y * rect.Pitch;
		Memcpy( dst, src, bytePerLine );
		dst += bytePerLine;
	}

	hr = surf->UnlockRect();
	surf->Release();
	return ( hr == D3D_OK );
}

bool
Hrdw::WriteRaster	(	pvoid	inBuffer,
						int		inId		)
{

	if( !inBuffer )		return FALSE;

	Raster* r = GetRaster( inId );
	if( !r )			return FALSE;
	NV_ASSERT_RETURN_MTH( FALSE, r->surf );

	RECT rect;
	rect.top	= 0;
	rect.bottom = r->height;
	rect.left	= 0;
	rect.right	= r->width;

	HRESULT		  hr;
	hr = D3DXLoadSurfaceFromMemory(
		r->surf,NULL,NULL,inBuffer,
		r->colorFmt,4 * r->width,NULL,&rect,D3DX_FILTER_POINT,0);

	return (hr == D3D_OK) ;
}


bool
Hrdw::SetSource				(	int				inId,				
								uint			inSamplerNum,
								bool			inUseDepth)
{
	Raster* r = GetRaster( inId );
	if( !r )	return FALSE;
	HRESULT hr;
	if( !inUseDepth ) {
		NV_ASSERT_RETURN_MTH( FALSE, r->tex );
		hr = ddev->SetTexture( inSamplerNum, r->tex );
	} else {
		NV_ASSERT_RETURN_MTH( FALSE, r->depthTex );
		hr = ddev->SetTexture( inSamplerNum, r->depthTex );
	}
	NV_ASSERT_DX(hr);
	return TRUE;
}


uint
Hrdw::GetFullscreenWidth	(				)
{
	if( !fullscreenMode )
		return 0;
	NV_D3DSURFACEDESC sd;
	fullscreenBackBuffer->GetDesc( &sd );
	return sd.Width;
}


uint
Hrdw::GetFullscreenHeight	(				)
{
	if( !fullscreenMode )
		return 0;
	NV_D3DSURFACEDESC sd;
	fullscreenBackBuffer->GetDesc( &sd );
	return sd.Height;
}


uint
Hrdw::GetVSyncRate	(			)
{
	return adapterMode.RefreshRate;
}


void 
Hrdw::ResetStates		(		)
{
	CurDecl   = 0;

	Hrdw::SetShaderProgram (Hrdw::SD_NoVertexShader);	
	Hrdw::SetShaderProgram (Hrdw::SD_NoPixelShader);		

	ddev->SetTexture( 0, NULL );
	ddev->SetSamplerState( 0, D3DSAMP_MAGFILTER,	D3DTEXF_LINEAR		);
	ddev->SetSamplerState( 0, D3DSAMP_MINFILTER,	D3DTEXF_LINEAR		);
	ddev->SetSamplerState( 0, D3DSAMP_ADDRESSU,		D3DTADDRESS_WRAP	);
	ddev->SetSamplerState( 0, D3DSAMP_ADDRESSV,		D3DTADDRESS_WRAP	);
	ddev->SetTextureStageState( 0, D3DTSS_COLOROP,	D3DTOP_MODULATE		);
	ddev->SetTextureStageState( 0, D3DTSS_COLORARG1,D3DTA_DIFFUSE		);
	ddev->SetTextureStageState( 0, D3DTSS_COLORARG2,D3DTA_TEXTURE		);
	ddev->SetTextureStageState( 0, D3DTSS_ALPHAOP,	D3DTOP_MODULATE		);
	ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG1,D3DTA_DIFFUSE		);
	ddev->SetTextureStageState( 0, D3DTSS_ALPHAARG2,D3DTA_TEXTURE		);
	ddev->SetTexture( 1, NULL );
	ddev->SetTextureStageState( 1, D3DTSS_COLOROP,	 D3DTOP_DISABLE	);
	ddev->SetTextureStageState( 1, D3DTSS_ALPHAOP,	 D3DTOP_DISABLE	);

	ddev->SetRenderState( D3DRS_CULLMODE,		D3DCULL_CCW				);
	ddev->SetRenderState( D3DRS_SHADEMODE,		D3DSHADE_GOURAUD		);
	ddev->SetRenderState( D3DRS_COLORVERTEX,	TRUE					);
	ddev->SetRenderState( D3DRS_ZWRITEENABLE,	TRUE					);
 	ddev->SetRenderState( D3DRS_ZFUNC,			D3DCMP_LESSEQUAL		);
	ddev->SetRenderState( D3DRS_ZENABLE,		D3DZB_TRUE				);
	ddev->SetRenderState( D3DRS_STENCILENABLE,  FALSE 					);
}


bool
Hrdw::CheckDeviceFormatForMipMap	(	TextureFormat		CheckFormat		)
{
	return dd3d->CheckDeviceFormat(	adapter,adapterDevType,adapterMode.Format,
									D3DUSAGE_AUTOGENMIPMAP,D3DRTYPE_TEXTURE,TEXTUREFORMAT_TRANSLATION[CheckFormat]) == D3D_OK;	
}


bool
Hrdw::CheckDeviceFormatForDynamic	(	TextureFormat		CheckFormat		)
{
	return dd3d->CheckDeviceFormat(	adapter,adapterDevType,adapterMode.Format,
									D3DUSAGE_DYNAMIC,D3DRTYPE_TEXTURE,TEXTUREFORMAT_TRANSLATION[CheckFormat]	) == D3D_OK;		
}


bool
Hrdw::SetRaster		(	int		inRasterId , bool inSetRTZ , bool inSetOnlyZ )
{
	Raster* r = GetRaster( inRasterId );
	if( !r )	return FALSE;
	NV_ASSERT_RETURN_MTH( FALSE, r->surf );

	HRESULT hr;

	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool MRT = RMode == Hrdw::RM_Full;

	// target 0
	if ( !inSetOnlyZ || !r->depthSurf ) {
		hr = ddev->SetRenderTarget( 0, r->surf );
		NV_ASSERT_DX(hr);
	}
	else if (MRT) {
		hr = ddev->SetRenderTarget( 1, NULL );
		NV_ASSERT_DX(hr);
	}
	hr = ddev->SetDepthStencilSurface( r->depth );
	NV_ASSERT_DX(hr);

	// target 1
	if (MRT) {
		if( inSetRTZ && r->depthSurf )
			hr = ddev->SetRenderTarget( inSetOnlyZ?0:1, r->depthSurf );
		else {
			hr = ddev->SetRenderTarget( 1, NULL );
			if (inSetOnlyZ) return FALSE;
		}
		NV_ASSERT_DX(hr);
	}
	curRaster = inRasterId;

	if (inSetOnlyZ && !MRT)
		return FALSE;

	return TRUE;
}


bool
Hrdw::SetTarget		(	int		inTargetId		)
{
	if( fullscreenMode )	return FALSE;
	Target* t = GetTarget( inTargetId );
	if( !t )	return FALSE;
	NV_ASSERT_RETURN_MTH( FALSE, t->swapChain );
	NV_ASSERT_RETURN_MTH( FALSE, t->back );
	curTarget = inTargetId;
	return TRUE;
}


int
Hrdw::GetMaxTextureWidth	(	)
{
	return ddevCaps.MaxTextureWidth;
}
int
Hrdw::GetMaxTextureHeight	(	){
	return ddevCaps.MaxTextureHeight;
}
bool
Hrdw::GetTextureCapability	(	TextureCapabilities inTexCaps		)
{
	switch(inTexCaps){
		case TEX_SQUAREONLY:
			return (ddevCaps.TextureCaps & D3DPTEXTURECAPS_SQUAREONLY)>=1;
		case TEX_POW2:
			return (ddevCaps.TextureCaps & D3DPTEXTURECAPS_POW2)>=1;
	}
	return false;
}

bool
Hrdw::GetDriverCapability		(	DriverCapabilities  inDrvCaps		)
{
	switch(inDrvCaps){
		case DRV_AUTOGENMIPMAP:
			return (ddevCaps.Caps2 & D3DCAPS2_CANAUTOGENMIPMAP)>=1;		
	}
	return false;
}


bool
Hrdw::BeginScene()
{	
#ifdef _NVCOMP_ENABLE_DBG
	NV_ASSERT(!BeginFrame);
	BeginFrame = TRUE;
#endif //_NVCOMP_ENABLE_DBG
	
	HRESULT hr;
	hr = ddev->BeginScene();	
	NV_ASSERT_DX(hr);
	return TRUE;
}


bool
Hrdw::EndScene()
{
#ifdef _NVCOMP_ENABLE_DBG
	NV_ASSERT(BeginFrame);
	BeginFrame = FALSE;
#endif //_NVCOMP_ENABLE_DBG

	HRESULT hr;
	ddev->EndScene();
	// Present
	if( fullscreenMode )
	{
		hr = ddev->Present(	NULL, NULL, NULL, NULL );
	}
	else
	{
		Target* t = GetCurTarget();
		if( !t )	return FALSE;
		hr = t->swapChain->Present( NULL, NULL, NULL, NULL, 0 /*D3DPRESENT_DONOTWAIT*/ );
	}

	if ( D3DERR_DEVICELOST == hr ) {
		devState = NV_DEVICE_LOST;
		return FALSE;
	}
	return (hr == D3D_OK);
}


void 
Hrdw::SetViewPort	(	int					X, 
						int 				Y, 
						int 				Width,
						int					Height, 
						float				MinZ,
						float				MaxZ)
{
	NV_D3DVIEWPORT vp;
	vp.X		= X;
	vp.Y		= Y;
	vp.Width	= Width;
	vp.Height	= Height;
	vp.MinZ		= MinZ;
	vp.MaxZ		= MaxZ;
	HRESULT hr = ddev->SetViewport( &vp );
	NV_ASSERT_DX(hr);
}


void				
Hrdw::ClearRaster		(	uint	flags, 
							uint32	argb, 
							float	z		)
{
	uint target = 0;
	if ( flags & 0x1) target |= D3DCLEAR_STENCIL;
	if ( flags & 0x2) target |= D3DCLEAR_TARGET;
	if ( flags & 0x4) target |= D3DCLEAR_ZBUFFER;
	HRESULT hr = ddev->Clear( 0, NULL, target, argb, z, 0L );
	NV_ASSERT_DX(hr);
}


bool
Hrdw::CopyBackToFront	(		)
{
	Hrdw::SetRenderState( Hrdw::NV_ALPHABLENDENABLE,		FALSE				);
	Hrdw::SetRenderState( Hrdw::NV_ALPHATESTENABLE,			FALSE				);
	Hrdw::SetRenderState( Hrdw::NV_COLORWRITEENABLE,		0xFFFFFFFF			);
	Hrdw::SetRenderState( Hrdw::NV_STENCILENABLE,			FALSE				);

	Hrdw::SetRenderState( Hrdw::NV_ZWRITEENABLE,			FALSE				);
	Hrdw::SetRenderState( Hrdw::NV_ZENABLE,					Hrdw::NV_ZB_FALSE	);

	Hrdw::SetRenderState( Hrdw::NV_CLIPPING,				TRUE				);

	Hrdw::SetRenderState( Hrdw::NV_FILLMODE,				Hrdw::NV_FILL_SOLID	);
	Hrdw::SetRenderState( Hrdw::NV_CULLMODE,				Hrdw::NV_CULL_NONE	);	

	Raster* r = GetRaster( curRaster );
	if	( !r || !r->tex)	return FALSE;
	
	Hrdw::tools::VsCopyMappingTex		vsCopyMapping;
	vsCopyMapping.offsetTexCoord[0]		= 0.5f / r->width;
	vsCopyMapping.offsetTexCoord[1]		= 0.5f / r->height;


	int	inFrameRaster = DpyManager::hwraster[DPYR_IN_FRAME] ;
	Hrdw::SetSource(inFrameRaster,0,FALSE);
	Hrdw::SetTexture(NO_TEXTURE,1);

	NV_D3DSURFACE bb = GetCurTargetBackBuffer();
	if( !bb )	return FALSE;
	HRESULT hr;

	D3DSURFACE_DESC desc;
	hr = bb->GetDesc(&desc);
	NV_ASSERT_DX(hr);

	bool copyLinear = false;
	if (r->width != desc.Width || r->height != desc.Height)
		copyLinear = true;
	TextureFilter filter = copyLinear?(Hrdw::NV_TEXF_LINEAR):(Hrdw::NV_TEXF_POINT);
	Hrdw::SetSamplerState( 0, Hrdw::NV_MAGFILTER,	filter	);
	Hrdw::SetSamplerState( 0, Hrdw::NV_MINFILTER,	filter	);
	Hrdw::SetSamplerState( 0, Hrdw::NV_MIPFILTER,	Hrdw::NV_TEXF_NONE	);
	Hrdw::SetSamplerState( 0, Hrdw::NV_ADDRESSU,	Hrdw::NV_TADDRESS_CLAMP	);
	Hrdw::SetSamplerState( 0, Hrdw::NV_ADDRESSV,	Hrdw::NV_TADDRESS_CLAMP	);


	// Set a target as raster :
	hr = ddev->SetRenderTarget( 0, bb );	
	NV_ASSERT_DX(hr);

	Hrdw::RunningMode RMode;
	nv::core::GetParameter(nv::core::PR_DX_RUNNING_MODE,(uint32*)(&RMode));	
	bool MRT = (RMode == Hrdw::RM_Full);
	if (MRT){
		hr = ddev->SetRenderTarget( 1, NULL );
		NV_ASSERT_DX(hr);
	}
	hr = ddev->SetDepthStencilSurface( NULL );
	NV_ASSERT_DX(hr);

	Hrdw::FullUpdateVertexMapping(&vsCopyMapping);
	tools::SetVertexBuffer	(	TRUE	);
	tools::SetShader		(	TRUE	, TRUE	, TRUE	);
	tools::DrawSprite		(			);

	return TRUE;
}




