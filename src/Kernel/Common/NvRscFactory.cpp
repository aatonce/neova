/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/Common/NvRscFactory.h>
using namespace nv;



void
RscFactory::TranslateData		(	pvoid		inDataPtr,
									uint32		inBOffset,
									uint32*		inRelocationTab		)
{
	if(		!inDataPtr
		||	!inBOffset
		||	!inRelocationTab	)
		return;

	NV_ASSERT_A32( inRelocationTab );

	uint N = *inRelocationTab++;
	NV_ASSERT( N > 0 );
	while( N-- ) {
		uint32* ptr = (uint32*)( uint32(inDataPtr) + (*inRelocationTab++) );
		TranslatePointer( ptr, inBOffset );
	}
}



void
RscFactory::TranslatePointer	(	pvoid		inPointerAddr,
									uint32		inBOffset		)
{
	NV_ASSERT_A32( inPointerAddr );
	uint32* ptr = (uint32*) inPointerAddr;
	// No relocation of 0 offset -> NULL pointer
	if( *ptr )
		*ptr += inBOffset;
}



