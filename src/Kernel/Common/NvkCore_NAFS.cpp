/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#define _CRT_SECURE_NO_DEPRECATE

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <Nova.h>
#include <Kernel/Common/NvkCore_NAFS.h>
using namespace nv;



const char*		nv::nafs::DEF_SHNAME	= "public";		// Default share name



void
nv::nafs::ShareInfo::Init		(				)
{
	addr[0]   = 0;
	port	  = DEF_PORT;
	name[0]   = 0;
	passwd[0] = 0;
}


bool
nv::nafs::ShareInfo::Init	(	pcstr		inAddr,
								uint16		inPort			)
{
	Init();
	if( !inAddr || Strlen(inAddr)>=LIMIT_SHLEN )
		return FALSE;
	if( !inPort )
		return FALSE;
	Strcpy( addr, inAddr );
	port = inPort;
	return TRUE;
}


bool
nv::nafs::ShareInfo::Init	(	pcstr		inFullname,
								char*		outPath,
								uint		inPathMaxLen	)
{
	Init();

	if( !inFullname )	return FALSE;
	pstr addr_end	= libc::Strchr( inFullname,   ',' );		if( !addr_end )		return FALSE;
	pstr port_end	= libc::Strchr( addr_end+1,   ',' );		if( !port_end )		return FALSE;
	pstr shname_end = libc::Strchr( port_end+1,   ',' );		if( !shname_end )	return FALSE;
	pstr shpass_end = libc::Strchr( shname_end+1, ',' );		if( !shpass_end )	return FALSE;

	char tmp[LIMIT_SHLEN];
	uint len;

	// extract addr
	Zero( tmp );
	len = addr_end - inFullname;
	if( len >= LIMIT_SHLEN )	return FALSE;
	if( len )	libc::Memcpy( tmp, (void*)inFullname, len );
	else		libc::Strcpy( tmp, "127.0.0.1" );
	if( !net::GetHostAddr(tmp,0,addr) ) {
		if( len>=16 )	return FALSE;
		libc::Strcpy( addr, tmp );
	}

	// extract port
	len = port_end - (addr_end+1);
	if( len >= LIMIT_SHLEN )	return FALSE;
	if( len ) {
		Zero( tmp );
		libc::Memcpy( tmp, addr_end+1, len );
		port = libc::Strtoul( tmp, NULL, 10 );
	} else {
		port = DEF_PORT;
	}

	// extract shname
	len = shname_end - (port_end+1);
	if( len >= LIMIT_SHLEN )	return FALSE;
	if( len ) {
		Zero( tmp );
		libc::Memcpy( tmp, port_end+1, len );
		libc::Strcpy( name, tmp );
	} else {
		libc::Strcpy( name, DEF_SHNAME );
	}

	// extract shpasswd
	len = shpass_end - (shname_end+1);
	if( len >= LIMIT_SHLEN )	return FALSE;
	Zero( tmp );
	libc::Memcpy( tmp, shname_end+1, len );
	libc::Strcpy( passwd, tmp );

	// extract path
	uint pathLen = libc::Strlen( shpass_end+1 );
	if( pathLen == 0 )				return FALSE;
	if( pathLen >= inPathMaxLen )	return FALSE;
	libc::Strcpy( outPath, shpass_end+1 );

//	DebugPrintf( "nafs: host:%s port:%d name:%s passwd:%s\n", host, port, name, passwd );
	return TRUE;
}




uint8
nv::nafs::DecodeHexByte	(	uint8*		inHB		)
{
	uint hi = inHB[0];
	uint lo = inHB[1];
	if     ( hi<='9' )	hi -= '0';
	else if( hi<='F' )	hi -= 'A' - 10;
	else				hi -= 'a' - 10;
	if     ( lo<='9' )	lo -= '0';
	else if( lo<='F' )	lo -= 'A' - 10;
	else				lo -= 'a' - 10;
	NV_ASSERT( hi <= 15 );
	NV_ASSERT( lo <= 15 );
	return (hi<<4) | lo;
}


void
nv::nafs::EncodeHexByte	(	uint8*		outHB,
							uint8		inB			)
{
	uint hi = inB>>4;
	uint lo = inB&15;
	if( hi<10 )			hi += '0';
	else				hi += 'a'-10;
	if( lo<10 )			lo += '0';
	else				lo += 'a'-10;
	outHB[0] = hi;
	outHB[1] = lo;
}



void
nv::nafs::Command::Init		(		)
{
	sockId			= -1;
	tosend_bsize	= 0;
	torcv_bsize		= 0;
}


void
nv::nafs::Command::Shut		(		)
{
	Disconnect();
}


bool
nv::nafs::Command::Connect	(	pcstr				inAddr,
								uint16				inPort		)
{
	sockId = net::Create( net::T_TCP );
	if( sockId < 0 )
		return FALSE;

	// connect
	if( !net::ConnectWait(sockId,0,inAddr,inPort) ) {
		Disconnect();
		return FALSE;
	}

	// nafs capacity
	SetCapacity( 1024 );
	if( !ExecWait("capacity;") ) {
		Disconnect();
		return FALSE;
	}
	uint capBSize = libc::Strtoul( GetResult(), NULL, 16 );
	if( capBSize==0 || capBSize>128*1024 ) {
		Disconnect();
		return FALSE;
	}
	SetCapacity( capBSize );
	
	tosend_bsize	= 0;
	torcv_bsize		= 0;
	return TRUE;
}


bool
nv::nafs::Command::Connect	(	const ShareInfo&	inShInfo	)
{
	return Connect( inShInfo.addr, inShInfo.port );
}


void
nv::nafs::Command::Disconnect	(			)
{
	net::Release( sockId );
	sockId			= -1;
	tosend_bsize	= 0;
	torcv_bsize		= 0;
}


bool
nv::nafs::Command::IsConnected		(			)
{
	return net::IsConnected( sockId );
}


uint
nv::nafs::Command::GetCapacity		(			)
{
	return buffer.size();
}


void
nv::nafs::Command::SetCapacity		(	uint		inBSize		)
{
	if( buffer.size() < inBSize )
		buffer.resize( inBSize );
}


uint8*
nv::nafs::Command::GetBuffer		(			)
{
	return buffer.data();
}


bool
nv::nafs::Command::ExecBuffer		(	uint		inBSize		)
{
	if( !IsConnected() )			return FALSE;
	if( !IsReady() )				return FALSE;
	if( inBSize==0 )				return FALSE;
	if( inBSize>GetCapacity() )		return FALSE;

	// prepare
	tosend_bsize	= inBSize;
	tosend_pos		= 0;
	torcv_bsize		= GetCapacity();	// maximum for the moment !
	torcv_pos		= 0;

	Update();
	return IsConnected();
}


bool
nv::nafs::Command::ExecBufferWait	(	uint		inBSize		)
{
	if( !IsConnected() )			return FALSE;
	if( !IsReady() )				return FALSE;
	if( inBSize==0 )				return FALSE;
	if( inBSize>GetCapacity() )		return FALSE;

	// send all
	if( !net::SendWait(sockId,0,buffer.data(),inBSize) ) {
		Disconnect();
		return FALSE;
	}

	// rcv '0xSSSS' header
	SetCapacity( 16 );
	if( !net::ReceiveWait(sockId,0,&buffer[0],6) ) {
		Disconnect();
		return FALSE;
	}
	buffer[6] = 0;
	uint bs = libc::Strtoul( (char*)&buffer[0], NULL, 16 );

	// rcv overflow ?
	if( bs > GetCapacity() ) {
		Disconnect();
		return FALSE;
	}

	// rcv remaining
	if( !net::ReceiveWait(sockId,0,&buffer[6],bs-6) ) {
		Disconnect();
		return FALSE;
	}

//	libc::DumpData( 0, buffer.data(), RoundX(bs,16)	);
	return TRUE;
}


bool
nv::nafs::Command::Exec		(	pcstr inFmt, ...		)
{
	if( !IsConnected() )	return FALSE;
	if( !IsReady() )		return FALSE;
	if( !inFmt )			return FALSE;

	SetCapacity( 1024 );
	char* cmds = (char*)buffer.data();

	va_list	arg;
	va_start( arg, inFmt );
	vsprintf( cmds, inFmt, arg );
	va_end( arg );

	return ExecBuffer( Strlen(cmds) );
}


bool
nv::nafs::Command::ExecWait	(	pcstr inFmt, ...		)
{
	if( !IsConnected() )	return FALSE;
	if( !IsReady() )		return FALSE;
	if( !inFmt )			return FALSE;

	SetCapacity( 1024 );
	char* cmds = (char*)buffer.data();

	va_list	arg;
	va_start( arg, inFmt );
	vsprintf( cmds, inFmt, arg );
	va_end( arg );

	return ExecBufferWait( Strlen(cmds) );
}


void
nv::nafs::Command::Update			(							)
{
	if( IsReady() )
		return;

	// sending ?

	if( tosend_bsize )
	{
		NV_ASSERT( tosend_pos < tosend_bsize );
		uint32 rembs = tosend_bsize - tosend_pos;
		uint32 sbs;
		if( !net::Send(sockId,sbs,&buffer[tosend_pos],rembs) ) {
			Disconnect();
			return;
		}
		NV_ASSERT( sbs <= rembs );
		tosend_pos += sbs;
		if( tosend_pos < tosend_bsize )
			return;
		// end of sending !
		tosend_bsize = 0;
	}

	// receiving ?

	if( torcv_bsize )
	{
		NV_ASSERT( torcv_pos < torcv_bsize );
		uint32 rembs = torcv_bsize - torcv_pos;
		uint32 rbs;
		if( !net::Receive(sockId,rbs,&buffer[torcv_pos],rembs) ) {
			Disconnect();
			return;
		}
		NV_ASSERT( rbs <= rembs );
		uint old_pos = torcv_pos;
		uint new_pos = torcv_pos + rbs;
		// '0xSSSS' header received ?
		if( old_pos<6 && new_pos>= 6 ) {
			// adjust bsize to receive
			char hdr[16];
			Memcpy( hdr, &buffer[0], 6 );
			hdr[6] = 0;
			torcv_bsize = libc::Strtoul( hdr, NULL, 16 );
			// overflow or underflow ?
			if( torcv_bsize>GetCapacity() || torcv_bsize<new_pos ) {
				Disconnect();
				return;
			}
		}
		torcv_pos = new_pos;
		if( torcv_pos < torcv_bsize )
			return;
		// end of receiving !
		torcv_bsize = 0;
	}
}


bool
nv::nafs::Command::IsReady			(							)
{
	if( !IsConnected() )	return TRUE;
	return (tosend_bsize==0 && torcv_bsize==0);
}


char*
nv::nafs::Command::GetResult		(							)
{
	if( !IsConnected() )	return NULL;
	if( !IsReady() )		return NULL;

	// '0xSSSS error' or '0xSSSS done'
	if( GetCapacity() < 11 )				return NULL;
	if( buffer[1]!='x' || buffer[7]!='d' )	return NULL;
	char hdr[16];
	Memcpy( hdr, &buffer[0], 6 );
	hdr[6] = 0;
	uint bs = libc::Strtoul( hdr, NULL, 16 );

	if( bs >= GetCapacity() )				return NULL;
	buffer[bs] = 0;
//	libc::DumpData( 0, buffer.data(), RoundX(bs,16)	);
	return (char*) &buffer[11];
}


bool
nv::nafs::Command::GetFilesize	(	const ShareInfo&	inShInfo,
									pcstr				inFilename,
									uint32&				outBSize	)
{
	if( !IsConnected() )	return FALSE;
	if( !IsReady() )		return FALSE;

	bool done = ExecWait( "filesize \"%s\" \"%s\" \"%s\";", inShInfo.name, inShInfo.passwd, inFilename );
	if( !done )		return FALSE;

	char* res = GetResult();
	if( !res )		return FALSE;

	outBSize = libc::Strtoul( res, NULL, 16 );
	return TRUE;
}


bool
nv::nafs::Command::ReadFile		(	const ShareInfo&	inShInfo,
									pcstr				inFilename,
									pvoid				inBuffer,
									uint32				inBSize,
									uint32				inBOffset		)
{
	if( !IsConnected() )	return FALSE;
	if( !IsReady() )		return FALSE;
	if( !inBSize )			return TRUE;
	if( !inBuffer )			return FALSE;

	uint8* cur_ptr     = (uint8*) inBuffer;
	uint   cur_bsize   = inBSize;
	uint   cur_boffset = inBOffset;

	while( cur_bsize )
	{
		bool done = ExecWait( "readfile \"%s\" \"%s\" \"%s\" 0x%x 0x%x;",
							inShInfo.name,
							inShInfo.passwd,
							inFilename,
							cur_bsize,
							cur_boffset );
		if( !done )		return FALSE;

		char*  res = GetResult();
		if( !res )		return FALSE;

		pstr   rdata;
		uint32 rbs = Strtoul( res, &rdata, 16 );
		NV_ASSERT( rbs <= cur_bsize );

		rdata = Strchr( rdata, '\"' );
		if( !rdata )	return FALSE;

		rdata++;
		cur_boffset += rbs;
		cur_bsize   -= rbs;
		while( rbs-- ) {
			cur_ptr[0] = nafs::DecodeHexByte( (uint8*)rdata );
			rdata   += 2;
			cur_ptr += 1;
		}
	}

	return TRUE;
}


bool
nv::nafs::Command::WriteFile	(	const ShareInfo&	inShInfo,
									pcstr				inFilename,
									pvoid				inBuffer,
									uint32				inBSize,
									uint32				inBOffset		)
{
	if( !IsConnected() )	return FALSE;
	if( !IsReady() )		return FALSE;
	if( !inBSize )			return TRUE;
	if( !inBuffer )			return FALSE;

	uint8* cur_ptr     = (uint8*) inBuffer;
	uint   cur_bsize   = inBSize;
	uint   cur_boffset = inBOffset;
	uint   cap_maxbs   = (GetCapacity()-128)/2;

	while( cur_bsize )
	{
		uint sbs = Min( cur_bsize, cap_maxbs );

		uint8* cmds = buffer.data();
		cmds += Sprintf( (char*)cmds, "writefile \"%s\" \"%s\" \"%s\" 0x%x \"",
							inShInfo.name,
							inShInfo.passwd,
							inFilename,
							cur_boffset );

		cur_bsize   -= sbs;
		cur_boffset += sbs;
		while( sbs-- ) {
			EncodeHexByte( cmds, cur_ptr[0] );
			cmds += 2;
			cur_ptr += 1;
		}
		*cmds++ = '\"';
		*cmds++ = ';';

		uint slen = cmds - buffer.data();
		NV_ASSERT( slen <= GetCapacity() );

		bool done = ExecBufferWait( slen );
		if( !done )			return FALSE;
		if( !GetResult() )	return FALSE;
	}

	return TRUE;
}


bool
nv::nafs::Command::DumpFromFile	(	const ShareInfo&	inShInfo,
									pcstr				inFilename,
									pvoid&				outBuffer,
									uint32&				outBSize		)
{
	// filesize
	if( !GetFilesize(inShInfo,inFilename,outBSize) )
		return FALSE;

	if( outBSize == 0 )
		return FALSE;

	outBuffer = NvMalloc( outBSize );
	if( !outBuffer )
		return FALSE;

	// readfile
	if( !ReadFile(inShInfo,inFilename,outBuffer,outBSize,0) )
		return FALSE;

	return TRUE;
}


bool
nv::nafs::Command::DumpToFile	(	const ShareInfo&	inShInfo,
									pcstr				inFilename,
									pvoid				inBuffer,
									uint32				inBSize			)
{
	if( !IsConnected() )	return FALSE;
	if( !IsReady() )		return FALSE;

	// rmfile
	if( !ExecWait("rmfile \"%s\" \"%s\" \"%s\";",inShInfo.name,inShInfo.passwd,inFilename) )
		return FALSE;

	// createfile
	if( !ExecWait("createfile \"%s\" \"%s\" \"%s\";",inShInfo.name,inShInfo.passwd,inFilename) )
		return FALSE;

	// writefile
	if( !WriteFile(inShInfo,inFilename,inBuffer,inBSize,0) )
		return FALSE;

	return TRUE;
}


bool
nv::nafs::Command::AppendToFile	(	const ShareInfo&	inShInfo,
									pcstr				inFilename,
									pvoid				inBuffer,
									uint32				inBSize			)
{
	// filesize
	uint32 cur_bsize;
	if( !GetFilesize(inShInfo,inFilename,cur_bsize) )
		return FALSE;

	// writefile
	if( !WriteFile(inShInfo,inFilename,inBuffer,inBSize,cur_bsize) )
		return FALSE;

	return TRUE;
}



