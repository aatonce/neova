/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>
using namespace nv;



namespace
{

	interface OuterAllocator : public nv::mem::Allocator
	{

		struct Bloc {
			int		addr;
			int		size;
			int		prev;
			int		next;
		};
		
		uint		refCpt;
		bool		canprof;
				
		uint		maxAlloc;
		int			firstFree;
		int			firstUsed;
		Bloc*		blocs;

		uint32		heapBase;
		uint32		heapBSize;


		virtual    ~OuterAllocator() { 		}


		bool
		_Init	(	void*			inHeapBase,
					uint32			inHeapBSize,
					uint			inMaxAlloc,
					bool			inCanProfile	)
		{
			refCpt		= 1;
			canprof		= inCanProfile;
			heapBase 	= uint32( inHeapBase );
			heapBSize	= inHeapBSize;
			maxAlloc 	= inMaxAlloc;
			Reset();
			return TRUE;
		}


		void
		_Shut	(		)
		{
			NativeDelete( this );
		}


		bool
		_FindInPlace	(	int inAddr, int inBSize, int& outPrev, int& outNext )
		{
			NV_ASSERT( inAddr >= 0 && inAddr + inBSize < int(heapBSize) );
			NV_ASSERT( inBSize > 0 );
			outPrev = -1;
			outNext = -1;
			int m;
			for( m = firstUsed ; m >= 0 ; ) {				
				int d = int(inAddr) - int(blocs[m].addr);
				if( d>=0 ) {
					if( d<blocs[m].size )	return FALSE;	// overlaps !
					outPrev = m;
				} else {
					if( -d<inBSize )		return FALSE;	// overlaps !
					else					break;
				}
				m = blocs[m].next;
			}
			if( outPrev >= 0 ) {
				outNext = blocs[outPrev].next;
			}
			else if ( m == firstUsed ) {
				outNext = firstUsed;
			}
			return TRUE;
		}
		

		bool
		_FindHole	( int  inSize, int& outAddr, int& outPrev, int& outNext, uint inBAlignment )
		{
			NV_ASSERT( inSize > 0 );
			NV_ASSERT( inSize <= int(heapBSize) );
			NV_ASSERT( inBAlignment );
			NV_ASSERT( IsPow2(inBAlignment) );
			NV_ASSERT( (uint32(heapBase)%inBAlignment)==0 );

			outAddr = 0;
			outPrev  = -1;
			outNext  = firstUsed;
			for( int m = firstUsed ; m >= 0 ; ) {
				NV_ASSERT( blocs[m].addr >= outAddr );
				int d = blocs[m].addr - outAddr;
				if( d >= inSize )		return TRUE;
				outAddr = RoundX( blocs[m].addr+blocs[m].size, inBAlignment );
				outPrev = m;
				m = blocs[m].next;
				outNext = m;
			}
			NV_ASSERT( outNext == -1 );
			return ( (outAddr+inSize) <= int(heapBSize) );
		}

		int
		_InsertBloc	(	const Bloc&	inBloc	)
		{
			int m = firstFree;
			if( m < 0 )
				return -1;
			// next free
			firstFree = blocs[m].next;
			// link
			blocs[m] = inBloc;
			if( inBloc.prev >= 0 )	blocs[ inBloc.prev ].next = m;
			else					firstUsed = m;
			if( inBloc.next >= 0 )	blocs[ inBloc.next ].prev = m;
			return m;
		}
		
		int
		_FindBloc	(int	inAddr)
		{
			int blocId = firstUsed;
			while (blocId >=0){
				if (blocs[blocId].addr < inAddr){
					blocId = blocs[blocId].next;
				}
				else if (blocs[blocId].addr > inAddr){
					blocId = -1;
					break;
				}
				else 	//(blocs[blocId].addr == inAddr) 
					break;
			}
			return blocId;		
		}
		
		NvInterface*	
		GetBase	(		)
		{
			return this;
		}
		
		void			
		AddRef		(		)
		{
			refCpt++;
		}
		
		uint			
		GetRefCpt	(		)
		{
			return refCpt;
		}
		
		void			
		Release		(		)
		{
			if( refCpt > 1 )
			{
				refCpt--;
			}
			else
			{
				_Shut();
			}
		}

		bool
		Reset	(		)
		{
			for( int i = 0 ; i < int(maxAlloc) ; i++ ) {				
				blocs[i].addr = -1;
				blocs[i].prev  = i-1;
				blocs[i].next  = (i== (int(maxAlloc)-1)) ? -1 : i+1;
			}
			firstFree = 0;
			firstUsed = -1;	
			
			return TRUE;
		}

		void			
		Update	(		)
		{
			// 
		}

		void*			
		AllocInPlace	(	void*	inPointer,
							uint32	inBSize				)
		{
			NV_ASSERT( uint32(inPointer) >= uint32(heapBase) );
			Bloc m;
			m.addr  = int(inPointer) - heapBase;
			m.size  = inBSize;
			bool found = _FindInPlace( m.addr, m.size, m.prev, m.next );
			if( !found )	return NULL;
			_InsertBloc	(	m	);
			return inPointer;												
		}

		void*			
		Alloc	(	uint32	inBSize,
					uint	inFlags				)
		{
			Bloc m;
			m.size = inBSize;
			bool found = _FindHole( m.size, m.addr, m.prev, m.next, 1 );
			if( !found )	return NULL;
			_InsertBloc	(	m	);
			return (void *)(m.addr + heapBase);
		}

		void*			
		AllocA	(	uint32	inBSize,
					uint	inBAlignment,
					uint	inFlags				)
		{
			Bloc m;
			m.size = inBSize;
			bool found = _FindHole( m.size, m.addr, m.prev, m.next, inBAlignment);
			if( !found )	return NULL;
			_InsertBloc	(	m	);
			return (void *)(m.addr + heapBase);
		}
		
		void*			
		Realloc	(	void*	inPointer,
					uint32	inBSize,
					uint	inFlags				)
		{
			return NULL;
		}

		void			
		Free	(	void*	inPointer			)
		{
			NV_ASSERT( uint32(inPointer) >= uint32(heapBase) );
			NV_ASSERT( uint32(inPointer) <  uint32(heapBase+heapBSize) );

			int addr = int(inPointer) - heapBase;
			if( addr<0 || addr>= int(heapBSize) ) return;
			int blocId = _FindBloc(addr);
			if (blocId < 0) return ;

			Bloc& m = blocs[blocId];
			if( m.prev >= 0 )	blocs[ m.prev ].next = m.next;
			else				firstUsed = m.next;
			if( m.next >= 0 )	blocs[ m.next ].prev = m.prev;
			m.addr = -1;
			m.next  = firstFree;
			firstFree = blocId;
		}

		void			
		EnableProfiling		(		)
		{
			//
		}
		
		void			
		DisableProfiling	(		)
		{
			//
		}
		
		bool			
		IsProfiling		(		)
		{
			return FALSE;
		}
		
		uint32			
		GetTotBSize		(		)
		{
			return 0;
		}
		
		uint32			
		GetAllocCpt		(		)
		{
			return 0;
		}
		
		uint32			
		GetAllocTotBSize	(		)
		{
			return 0;
		}
		
		uint32			
		GetAllocTotBSizePeak(		)
		{
			return 0;
		}
		
		uint32			
		GetAllocNb		(		)
		{
			return 0;
		}
		
		uint32			
		GetAllocNbPeak	(		)
		{
			return 0;
		}

		mem::RangeStatus		
		CheckRange	(	void*	inPointer,
						uint32	inBSize				)
		{
			return mem::RS_NOT_AVAILABLE;							
		}
	};
}




nv::mem::Allocator*
nv::mem::CreateOuterAllocator	(	void*			inHeapBase,
									uint32			inHeapBSize,
									uint			inMaxAlloc,
									bool			inCanProfile	)
{
	if (inMaxAlloc == 0 ) return NULL;
	
	uint supply = inMaxAlloc * sizeof(OuterAllocator::Bloc);
	OuterAllocator* ator = NativeNewS<OuterAllocator>( supply );
	if( !ator )
		return NULL;

	ator->blocs = (OuterAllocator::Bloc *) (ator+1);
	if( !ator->_Init(inHeapBase,inHeapBSize,inMaxAlloc,inCanProfile) ) {
		NativeDelete( ator );
		ator = NULL;
	}

	return ator;
}



