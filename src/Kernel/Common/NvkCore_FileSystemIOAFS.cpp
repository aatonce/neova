/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_File.h>
using namespace nv;





// native system i/o
bool				nvcore_File_io_Init					(															);
void				nvcore_File_io_Shut					(															);
bool				nvcore_File_io_Open					(	pcstr	inFilename,	 uint32&	outBSize				);
bool				nvcore_File_io_Close				(															);
nv::file::Status	nvcore_File_io_GetStatus			(															);
bool				nvcore_File_io_IsOpened				(															);
bool				nvcore_File_io_IsReady				(															);
bool				nvcore_File_io_IsSuccess			(															);
void				nvcore_File_io_Sync					(															);
bool				nvcore_File_io_Read					(	uint	inSysFlags,
															pvoid	inBufferPtr, uint32		inBSize,
															uint32	inBOffset0,	 uint32		inBOffset1,
															uint32	inBOffset2,	 uint32		inBOffset3				);
bool				nvcore_File_io_DumpFrom				(	pcstr	inFilename, pvoid& outBuffer, uint& outBSize	);
bool				nvcore_File_io_DumpTo				(	pcstr	inFilename, pvoid inBuffer, uint inBSize		);
bool				nvcore_File_io_Append				(	pcstr	inFilename, pvoid inBuffer, uint inBSize		);
uint				nvcore_File_io_GetNbSubDevice		(															);
pcstr				nvcore_File_io_GetSubDeviceName		(	uint	inSubDevNo										);
pcstr				nvcore_File_io_GetSubDevicePrefix	(	uint	inSubDevNo										);
pcstr				nvcore_File_io_GetSubDeviceSuffix	(	uint	inSubDevNo										);




namespace
{

	struct SystemIO_AFS : public file::AbstractFS
	{
		bool	Init			(								)
		{
			return nvcore_File_io_Init();
		}

		void	Shut			(								)
		{
			return nvcore_File_io_Shut();
		}

		bool	Open			(	pcstr			inFilename,
									uint32&			outBSize	)
		{
			return nvcore_File_io_Open( inFilename, outBSize );
		}

		bool	Close			(								)
		{
			return nvcore_File_io_Close();
		}

		nv::file::Status	GetStatus		(								)
		{
			return nvcore_File_io_GetStatus();
		}

		bool	IsOpened		(								)
		{
			return nvcore_File_io_IsOpened();
		}

		bool	IsReady			(								)
		{
			return nvcore_File_io_IsReady();
		}
		
		bool	IsSuccess		(								)
		{
			return nvcore_File_io_IsSuccess();
		}
		
		void	Sync			(								)
		{
			nvcore_File_io_Sync();
		}

		bool	Read			(	uint			inSysFlags,
									pvoid			inBufferPtr,
									uint32			inBSize,
									uint32			inBOffset0,	uint32	inBOffset1,	uint32	inBOffset2,	uint32	inBOffset3	)
		{
			return nvcore_File_io_Read( inSysFlags, inBufferPtr, inBSize, inBOffset0, inBOffset1, inBOffset2, inBOffset3 );
		}

		bool	DumpFromFile	(	pcstr			inFilename,
									pvoid&			outBuffer,
									uint&			outBSize		)
		{
			return nvcore_File_io_DumpFrom( inFilename, outBuffer, outBSize );
		}

		bool	DumpToFile		(	pcstr			inFilename,
									pvoid			inBuffer,
									uint			inBSize			)
		{
			return nvcore_File_io_DumpTo( inFilename, inBuffer, inBSize );
		}

		bool	AppendToFile	(	pcstr			inFilename,
									pvoid			inBuffer,
									uint			inBSize			)
		{
			return nvcore_File_io_Append( inFilename, inBuffer, inBSize );
		}

		uint	GetNbSubDevice		(			)
		{
			return nvcore_File_io_GetNbSubDevice();
		}

		pcstr	GetSubDeviceName	(	uint		inSubdevNo		)
		{
			return nvcore_File_io_GetSubDeviceName(inSubdevNo);
		}

		pcstr	GetSubDevicePrefix	(	uint		inSubdevNo		)
		{
			return nvcore_File_io_GetSubDevicePrefix(inSubdevNo);
		}

		pcstr	GetSubDeviceSuffix	(	uint		inSubdevNo		)
		{
			return nvcore_File_io_GetSubDeviceSuffix(inSubdevNo);
		}

	} systemIO_AFS;

}



nv::file::AbstractFS*
nv::file::GetSystemIOAbstractFS		(				)
{
	return &systemIO_AFS;
}


