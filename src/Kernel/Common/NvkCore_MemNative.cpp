/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>

#ifdef _NGC
	#include <dolphin/os.h>
	#ifdef RVL
	#include <revolution/mem.h>
	#else
	#include <dolphin/ar.h>
	#endif 
#elif defined(_PS3)
	#include <stdlib.h>
#else 
	#include <malloc.h>
#endif 

using namespace nv;





namespace
{

	// ARAM outer DMM for NGC
	#if defined(_NGC) && !defined(_RVL)
	#define ARAM_ATOR
	mem::Allocator*		aram_ator = NULL;
	#endif

	bool native_ready = FALSE;
	
	uint32 mem1_lo = 0;
	uint32 mem1_hi = 0;
	uint32 mem2_lo = 0;
	uint32 mem2_hi = 0;
}




bool
nv::mem::NativeInit		(			)
{
	if( native_ready )
		return TRUE;

#ifdef _NGC

	uint32 maxHeaps, xfb_bs, fifo_bs;
	core::GetParameter( core::PR_NGC_ARENA_MAXHEAPS,  &maxHeaps );
	core::GetParameter( core::PR_NGC_XFB_BSIZE,		  &xfb_bs	);
	core::GetParameter( core::PR_NGC_GX_FIFO_BSIZE,	  &fifo_bs	);
	maxHeaps = Clamp( maxHeaps, 1U, 256U );

	// alloc GX XFB
	#ifdef _RVL
	void* xfb_ba = OSAllocFromMEM2ArenaHi( xfb_bs, 32 );
	#else
	void* xfb_ba = OSAllocFromArenaHi( xfb_bs, 32 );
	#endif
	core::SetParameter( core::PR_NGC_XFB_BASE, uint32(xfb_ba) );

	// alloc GX FIFO
	void* fifo_ba = OSAllocFromArenaHi( fifo_bs, 32 );
	core::SetParameter( core::PR_NGC_GX_FIFO_BASE, uint32(fifo_ba) );

	// OSInitAlloc should only ever be invoked once.
	void* alo = OSGetArenaLo();
	void* ahi = OSGetArenaHi();
	mem1_lo = uint32(alo);
	mem1_hi = uint32(ahi);
	alo = OSInitAlloc( alo, ahi, maxHeaps );
	OSSetArenaLo( alo );

	// Default heap covering the whole arena
    OSHeapHandle hh = OSCreateHeap( alo, ahi );
	OSSetArenaLo( alo );
	OSSetCurrentHeap( hh );
	core::SetParameter(	core::PR_NGC_HEAP_HANDLE, uint32(hh) );

	// Default heap for mem2 covering the whole arena too.
	#ifdef RVL
		alo = OSGetMEM2ArenaLo();
		ahi = OSGetMEM2ArenaHi();
		mem2_lo = uint32(alo);
		mem2_hi = uint32(ahi);
	    MEMHeapHandle mhh = MEMCreateExpHeap( alo, uint32(ahi)-uint32(alo) );
		NV_ASSERT( mhh != MEM_HEAP_INVALID_HANDLE );
		core::SetParameter(	core::PR_NGC_MEM2_HANDLE, uint32(mhh) );
		OSSetMEM2ArenaLo( ahi );
	#else 
		ARInit(NULL, 0);
		void* ar_ba = (void*) ARGetBaseAddress();
		uint  sm_bs = ARGetSize() - ( 16 * 1024);	// 16Ko reserved by system
		uint32 	aram_max;
		core::GetParameter( core::PR_NGC_ARAM_MAXALLOC, &aram_max );
		aram_ator = mem::CreateOuterAllocator( ar_ba, sm_bs, aram_max );
		NV_ASSERT( aram_ator );
	#endif
#endif

	native_ready = TRUE;

	return TRUE;
}





void
nv::mem::NativeShut		(			)
{
#ifdef ARAM_ATOR
	SafeRelease( aram_ator );
#endif

	native_ready = FALSE;
}




bool
nv::mem::NativeIsReady		(				)
{
	return native_ready;
}



bool
nv::mem::NativeIsExists		(	MemId			inMemId		)
{
#if defined(_RVL)

	return (inMemId==MEM_MAIN) || (inMemId==MEM_NGC_MEM2);

#elif defined(_NGC)

	return (inMemId==MEM_MAIN) || (inMemId==MEM_NGC_ARAM);

#else

	return (inMemId==MEM_MAIN);

#endif
}





void*
nv::mem::NativeMalloc	(	uint32		inBSize,
							uint		inBAlign,
							MemId		inMemId		)
{
	if( !NativeIsExists(inMemId) )
		return NULL;

	if( inMemId == MEM_MAIN )
	{
		void* ptr = NULL;
		#if defined(_WIN32)
			ptr = _aligned_malloc( inBSize, inBAlign );
		#elif defined(_NGC)
			NV_ASSERTC( inBAlign<=32, "NativeMalloc can align on up to 32-byte boundaries !" );
			OSHeapHandle hh;
			core::GetParameter(	core::PR_NGC_HEAP_HANDLE, (uint32*)&hh );
			ptr = OSAllocFromHeap( hh,inBSize );
		#else
			ptr = memalign( inBAlign, inBSize );
		#endif

		#if defined( _NVCOMP_ENABLE_DBG )
		uint32 resetValue;
		core::GetParameter(	core::PR_MEM_RESET_VALUE, &resetValue );
		Memset( ptr, resetValue, inBSize );
		#endif

		return ptr;
	}

	if( inMemId == MEM_NGC_ARAM )
	{
		#if defined(ARAM_ATOR)
			return aram_ator ? aram_ator->AllocA(inBSize,inBAlign,0) : NULL;
		#else
			return NULL;
		#endif
	}

	if( inMemId == MEM_NGC_MEM2 )
	{
		#if defined(_RVL)
			MEMHeapHandle hh;
			core::GetParameter( core::PR_NGC_MEM2_HANDLE, (uint32*)&hh );
			return MEMAllocFromExpHeapEx( hh, inBSize, inBAlign );
		#else
			return NULL;
		#endif
	}

	// ???
	return NULL;
}




void*
nv::mem::NativeRealloc	(	void*		inPointer,
							uint32		inBSize,
							MemId		inMemId		)
{
	if( !NativeIsExists(inMemId) )
		return NULL;

	if( inMemId == MEM_MAIN )
	{
		#if defined(_WIN32)
			void* ptr = _aligned_realloc( inPointer, inBSize, 16 );
			return ptr;
		#elif defined(_NGC)
			if( inPointer && !inBSize ) {
				NativeFree( inPointer, inMemId );
				return NULL;
			}
			else if( !inPointer && inBSize ) {
				return NativeMalloc( inBSize, 32, inMemId );
			}
			else {	
				void* np = NativeMalloc( inBSize, 32, inMemId );
				if( np )
					Memcpy( np, inPointer, inBSize );
				NativeFree( inPointer, inMemId );
				return np;
			}
		#else
			return realloc( inPointer, inBSize );
		#endif
	}

	if( inMemId == MEM_NGC_ARAM )
	{
		NV_ERROR( "NativeRealloc not exist on this platform !" );
		return NULL;
	}

	if( inMemId == MEM_NGC_MEM2 )
	{
		#if defined(_RVL)
			if( inPointer && !inBSize ) {
				NativeFree( inPointer, inMemId );
				return NULL;
			}
			else if( !inPointer && inBSize ) {
				return NativeMalloc( inBSize, 32, inMemId );
			}
			else {
				void* np = NativeMalloc( inBSize, 32, inMemId );
				if( np )
					Memcpy( np, inPointer, inBSize );
				NativeFree( inPointer, inMemId );
				return np;
			}
		#else
			return NULL;
		#endif
	}

	// ???
	return NULL;
}






void
nv::mem::NativeFree		(	void*		inPointer,
							MemId		inMemId		)
{
	if( !inPointer ||  !NativeIsExists(inMemId) )
		return;

	if( inMemId == MEM_MAIN )
	{
		#if defined(_WIN32)
			_aligned_free( inPointer );
		#elif defined(_NGC)
			OSHeapHandle hh;
			core::GetParameter(	core::PR_NGC_HEAP_HANDLE, (uint32*)&hh );
			OSFreeToHeap( hh, inPointer );
		#else
			free( inPointer );
		#endif
		return;
	}

	if( inMemId == MEM_NGC_ARAM )
	{
		#if defined(ARAM_ATOR)
			aram_ator->Free( inPointer );
		#endif
		return;
	}

	if( inMemId == MEM_NGC_MEM2 )
	{
		#if defined(_RVL)
			MEMHeapHandle hh;
			core::GetParameter( core::PR_NGC_MEM2_HANDLE, (uint32*)&hh );
			MEMFreeToExpHeap( hh, inPointer );
		#endif
		return;
	}
}


uint32
nv::mem::NativeUsedBSize	(	MemId	inMemId	)
{

#if defined(_NGC)
	uint32 freeBSize = NativeFreeBSize(inMemId);
	
	if( inMemId == MEM_MAIN )
	{
		uint memSize = (mem1_hi - mem1_lo);
		return  memSize-freeBSize;
	}

	if( inMemId == MEM_NGC_MEM2 )
	{
		uint memSize = (mem2_hi - mem2_lo);
		return  memSize-freeBSize;
	}
#endif	
	return 0U;
}

uint32
nv::mem::NativeFreeBSize	(	MemId	inMemId	)
{
	if( inMemId == MEM_MAIN )
	{
		#if defined(_NGC)
			OSHeapHandle hh;
			core::GetParameter(	core::PR_NGC_HEAP_HANDLE, (uint32*)&hh );
			return  OSCheckHeap(hh);
		#endif
		return 0U;
	}

	if( inMemId == MEM_NGC_ARAM )
	{
		return 0U;
	}

	if( inMemId == MEM_NGC_MEM2 )
	{
		#if defined(_RVL)
			MEMHeapHandle hh2;
			core::GetParameter( core::PR_NGC_MEM2_HANDLE, (uint32*)&hh2 );
			if (!hh2) return 0U;
			return MEMGetTotalFreeSizeForExpHeap(hh2);
		#endif
		return 0U;
	}
	return 0U;
}



