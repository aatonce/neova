/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>
using namespace nv;



namespace
{

	interface PoolSubAllocator : public nv::mem::Allocator
	{
		struct Item {
			Item*	next;
		};
		#define	ITEM_RIGHT( I )		((Item*)(uint32(I)+itemBSize))
		#define	ITEM_LEFT( I )		((Item*)(uint32(I)-itemBSize))

		uint		refCpt;
		bool		canprof;
		uint32		heapBase;
		uint32		heapBSize;
		uint32		itemBSize;

		Item*		freeList;
		uint32		allocCpt;
		uint32		allocNb;
		uint32		allocNbPeak;



		virtual    ~PoolSubAllocator() {}



		bool
		_Init	(	void*			inHeapBase,
					uint32			inHeapBSize,
					uint			inItemBSize,
					bool			inCanProfile	)
		{
			NV_COMPILE_TIME_ASSERT( sizeof(void*) == 4 );
			NV_COMPILE_TIME_ASSERT( sizeof(uint32*) == 4 );
			uint32 hbase = uint32( inHeapBase );
			if( !hbase )						return FALSE;	// valid base !
			if( inItemBSize < 4 )				return FALSE;	// item is at least 4 bytes !
			if( (inItemBSize&3) != 0 )			return FALSE;	// item is 4 bytes units !
			if( inHeapBSize < inItemBSize*2 )	return FALSE;	// at least 2 items !

			refCpt	  = 1;
			canprof	  = inCanProfile;
			heapBase  = hbase;
			heapBSize = inHeapBSize;
			itemBSize = inItemBSize;

			return Reset();
		}

		void
		_Shut	(		)
		{
			NativeDelete( this );
		}

		NvInterface*
		GetBase		(		)
		{
			return this;
		}

		void
		AddRef		(		)
		{
			refCpt++;
		}

		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}

		void
		Release	(		)
		{
			if( refCpt > 1 )
			{
				refCpt--;
			}
			else
			{
				_Shut();
			}
		}

		bool
		Reset	(		)
		{
			uint32 base_mod   = (heapBase % itemBSize);
			uint32 base_align = heapBase + (base_mod ? itemBSize-base_mod : 0);
			uint32 end_align  = heapBase + heapBSize;
				   end_align -= (end_align % itemBSize);

			// link items
			Item* item_ptr	  = (Item*) base_align;
			Item* item_end	  = (Item*) end_align;
			while( item_ptr < item_end ) {
				Item* item_next = ITEM_RIGHT( item_ptr );
				item_ptr->next  = item_next;
				item_ptr		= item_next;
			}
			ITEM_LEFT( item_ptr )->next = NULL;

			freeList			= (Item*) base_align;
			allocCpt			= 0;
			allocNb				= 0;
			allocNbPeak			= 0;

			return TRUE;
		}

		void
		Update	(		)
		{
			//
		}

		void*
		AllocInPlace(	void*			inPointer,
						uint32			inBSize		)
		{
			return NULL;
		}

		void*
		Alloc		(	uint32			inBSize,
						uint			inFlags		)
		{
			if( inBSize > itemBSize )	return NULL;
			if( !freeList )				return NULL;
			Item* it = freeList;
			freeList = freeList->next;
			allocCpt++;
			allocNb++;
			allocNbPeak = Max( allocNbPeak, allocNb );
			#ifdef _NVCOMP_ENABLE_PROFILING
			mem::LogAppendAllocOp( it, inBSize, inFlags, "pool alloc" );
			#endif
			return it;
		}

		void*
		AllocA		(	uint32			inBSize,
						uint			inBAlignment,
						uint			inFlags		)
		{
			return NULL;
		}

		void*
		Realloc		(	void*			inPointer,
						uint32			inBSize,
						uint			inFlags		)
		{
			return NULL;
		}

		void
		Free		(	void*			inPointer	)
		{
			if( inPointer ) {
				NV_ASSERT( (uint32(inPointer)%itemBSize) == 0 );
				NV_ASSERT( allocNb );
				allocNb--;
				Item* it = (Item*) inPointer;
				it->next = freeList;
				freeList = it;
				#ifdef _NVCOMP_ENABLE_PROFILING
				mem::LogAppendFreeOp( inPointer, "pool free" );
				#endif
			}
		}

		void
		EnableProfiling		(			)
		{
			//
		}

		void
		DisableProfiling	(			)
		{
			//
		}

		bool
		IsProfiling			(			)
		{
			return TRUE;
		}

		uint32
		GetTotBSize			(			)
		{
			return heapBSize;
		}

		uint32
		GetAllocCpt			(			)
		{
			return allocCpt;
		}

		uint32
		GetAllocTotBSize	(			)
		{
			return allocNb * itemBSize;
		}

		uint32
		GetAllocTotBSizePeak		(			)
		{
			return allocNbPeak * itemBSize;
		}

		uint32
		GetAllocNb			(		)
		{
			return allocNb;
		}

		uint32
		GetAllocNbPeak		(		)
		{
			return allocNbPeak;
		}

		mem::RangeStatus
		CheckRange			(	void*			inPointer,
								uint32			inBSize		)
		{
			return mem::RS_NOT_AVAILABLE;
		}
	};

}



nv::mem::Allocator*
nv::mem::CreatePoolAllocator		(	void*			inHeapBase,
										uint32			inHeapBSize,
										uint			inItemBSize,
										bool			inCanProfile	)
{
	PoolSubAllocator* ator = NativeNew<PoolSubAllocator>( );
	if( !ator )	return NULL;

	if( !ator->_Init(inHeapBase,inHeapBSize,inItemBSize,inCanProfile) ) {
		NativeDelete( ator );
		ator = NULL;
	}

	return ator;
}



