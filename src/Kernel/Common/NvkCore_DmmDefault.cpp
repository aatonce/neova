/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>
using namespace nv;




namespace
{

	interface DefaultAllocator : public nv::mem::Allocator
	{
		uint		refCpt;
		bool		canprof;
		Allocator*	ator;


		virtual	~DefaultAllocator() {}


		bool
		_Init	(	bool	inCanProfile	)
		{
			refCpt		= 1;
			canprof		= inCanProfile;

			// native allocator
			ator = mem::CreateNativeAllocator( mem::MEM_MAIN, inCanProfile );

			return TRUE;
		}

		void
		_Shut	(			)
		{
			SafeRelease( ator );
			mem::NativeDelete( this );
		}

		NvInterface*
		GetBase		(		)
		{
			return this;
		}

		void
		AddRef		(		)
		{
			refCpt++;
		}

		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}

		void
		Release	(		)
		{
			if( refCpt > 1 )
			{
				refCpt--;
			}
			else
			{
				_Shut();
			}
		}

		bool
		Reset	(		)
		{
			return FALSE;	// can't !
		}

		void
		Update	(		)
		{
			ator->Update();
		}

		void*
		AllocInPlace(	void*			inPointer,
						uint32			inBSize		)
		{
			return ator->AllocInPlace( inPointer, inBSize );
		}

		void*
		Alloc		(	uint32			inBSize,
						uint			inFlags		)
		{
			return ator->Alloc( inBSize, inFlags );
		}

		void*
		AllocA		(	uint32			inBSize,
						uint			inBAlignment,
						uint			inFlags		)
		{
			return ator->AllocA( inBSize, inBAlignment, inFlags );
		}

		void*
		Realloc		(	void*			inPointer,
						uint32			inBSize,
						uint			inFlags		)
		{
			if( !inPointer )
			{
				return Alloc( inBSize, inFlags );
			}
			else
			{
				return ator->Realloc( inPointer, inBSize, inFlags );
			}
		}

		void
		Free		(	void*			inPointer	)
		{
			ator->Free( inPointer );
		}

		void
		EnableProfiling		(			)
		{
			ator->EnableProfiling();
		}

		void
		DisableProfiling	(			)
		{
			ator->DisableProfiling();
		}

		bool
		IsProfiling			(			)
		{
			return ator->IsProfiling();
		}

		uint32
		GetTotBSize		(		)
		{
			return ator->GetTotBSize();
		}

		uint32
		GetAllocTotBSize	(		)
		{
			return ator->GetAllocTotBSize();
		}

		uint32
		GetAllocTotBSizePeak	(		)
		{
			return ator->GetAllocTotBSizePeak();
		}

		uint32
		GetAllocNb		(		)
		{
			return ator->GetAllocNb();
		}

		uint32
		GetAllocNbPeak	(		)
		{
			return ator->GetAllocNbPeak();
		}

		uint32
		GetAllocCpt		(		)
		{
			return ator->GetAllocCpt();
		}

		mem::RangeStatus
		CheckRange		(	void*		inPointer,
							uint32		inBSize		)
		{
			return ator->CheckRange( inPointer, inBSize );
		}
	};

}



nv::mem::Allocator*
nv::mem::CreateCoreEngineDefAllocator	(		bool		inCanProfile	)
{
	DefaultAllocator* ator = NativeNewS<DefaultAllocator>( 0 );
	if( !ator )
		return NULL;

	if( !ator->_Init(inCanProfile) ) {
		NativeDelete( ator );
		ator = NULL;
	}

	return ator;
}



