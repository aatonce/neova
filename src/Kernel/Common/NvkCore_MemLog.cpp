/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <NvDpyManager.h>
#include <Kernel/Common/NvkCore_Mem.h>
using namespace nv;



namespace
{

	pstr					logBuffer = NULL;
	pstr					logBufferWPtr;
	uint32					logBufferBSize;



	bool
	FlushLog		(								)
	{
		if( !logBuffer )
			return FALSE;

		NV_ASSERT( logBufferWPtr >= logBuffer );
		uint toflushBSize = logBufferWPtr - logBuffer;
		NV_ASSERT( toflushBSize <= logBufferBSize );
		if( toflushBSize == 0 )
			return TRUE;

		pcstr	paramLogFn;
		if(		!core::GetParameter(core::PR_MEM_LOG_FILENAME,&paramLogFn)
			||	!paramLogFn
			||	!Strlen(paramLogFn)
			||	!file::AppendToFile(paramLogFn,logBuffer,toflushBSize) )
			return FALSE;

		// rewind
		logBufferWPtr = logBuffer;

		//DebugPrintf( "<Neova> Memory log flushed !\n" );
		return TRUE;
	}


	void
	CloseLog		(				)
	{
		if( !logBuffer )
			return;

		FlushLog();

		mem::NativeFree( logBuffer );

		logBuffer      = NULL;
		logBufferWPtr  = NULL;
		logBufferBSize = 0;

		DebugPrintf( "<Neova> Close memory log.\n" );
	}


	bool
	AppendToLog		(	pcstr			inText		)
	{
		if( !logBuffer || !inText )
			return FALSE;
		uint textLen = Strlen( inText );
		if( !textLen )		return FALSE;

		// The whole log buffer is to small to store the message => close the log !
		if( logBufferBSize <= textLen ) {
			CloseLog();
			return FALSE;
		}

		// Need to flush the log before to store the message ?
		NV_ASSERT( logBufferWPtr >= logBuffer );
		uint usedBSize = logBufferWPtr - logBuffer;
		NV_ASSERT( usedBSize <= logBufferBSize );
		uint remainBSize = logBufferBSize - usedBSize;
		if( remainBSize < textLen ) {
			if( !FlushLog() ) {
				CloseLog();
				return FALSE;
			}
		}

		Memcpy( logBufferWPtr, (pvoid)inText, textLen );
		logBufferWPtr += textLen;
		NV_ASSERT( uint(logBufferWPtr-logBuffer) <= logBufferBSize );

		return TRUE;
	}


	bool
	AppendToLog		(		char		inOperation,	// 'm' for malloc, 'f' for free
							void*		inPointer,
							uint32		inBSize,
							uint		inFlags,
							pcstr		inComment		)
	{
		if( !logBuffer )
			return FALSE;

		// Get the full callstack
		pvoid	callStack[64];
		uint	callStackNb = 0;
		stack::Frame sframe[2];
		if( stack::GetFrame(sframe[0]) ) {
			stack::Frame* f0 = &sframe[0];
			stack::Frame* f1 = &sframe[1];
			do {
				callStack[ callStackNb++ ] = f0->retAddr;
				Swap( f0, f1 );
			} while( (callStackNb<63) && stack::GetBackFrame(*f0,*f1) );
		}

		uint fmsgMaxBSize = 64 + callStackNb*9 + (inComment ? Strlen(inComment) : 0);
		pstr fmsg = (pstr) alloca( fmsgMaxBSize );
		if( !fmsg ) {
			CloseLog();
			return FALSE;
		}

		pstr fmsgWPtr = fmsg;

		// Op. header
		Sprintf( fmsgWPtr, "%08x %c %08X %08x %04x",
			DpyManager::GetFrameNo(),
			inOperation,
			uint32(inPointer),
			inBSize,
			inFlags		);
		fmsgWPtr += Strlen( fmsgWPtr );

		// Op. callstack
		if( callStackNb ) {
			Sprintf( fmsgWPtr, " <" );
			fmsgWPtr += Strlen( fmsgWPtr );
			for( uint i = 0 ; i < callStackNb ; i++ ) {
				Sprintf( fmsgWPtr, " %08X", uint32( callStack[i] ) );
				fmsgWPtr += Strlen( fmsgWPtr );
			}
			Sprintf( fmsgWPtr, " >" );
			fmsgWPtr += Strlen( fmsgWPtr );
		}

		// Comment
		if( inComment ) {
			Sprintf( fmsgWPtr, " [%s]", inComment );
			fmsgWPtr += Strlen( fmsgWPtr );
		}

		*fmsgWPtr++ = '\n';
		*fmsgWPtr++ = 0;

		// Check overflow
		uint fmsgBSize = fmsgWPtr - fmsg;
		NV_ASSERT( fmsgBSize <= fmsgMaxBSize );

		return AppendToLog( fmsg );
	}

	bool
	OpenLog		(					)
	{
		if( logBuffer )
			return FALSE;		// Already opened !

		uint32	paramLogBSize;
		pcstr	paramLogFn;
		if(		!core::GetParameter(core::PR_MEM_LOG_MAXBSIZE,&paramLogBSize)
			||	!core::GetParameter(core::PR_MEM_LOG_FILENAME,&paramLogFn)
			||	(paramLogBSize < 1024)
			||	!paramLogFn
			||	!Strlen(paramLogFn)		)
			return FALSE;

		pvoid log_buff = mem::NativeMalloc( paramLogBSize, 16 );
		if( !log_buff )		return FALSE;

		DebugPrintf( "<Neova> Memory log opened : \"%s\" (%d Ko)\n", paramLogFn, paramLogBSize>>10 );

		logBuffer      = (pstr) log_buff;
		logBufferWPtr  = logBuffer;
		logBufferBSize = paramLogBSize;

		char initMsg[128];
		Sprintf( initMsg, "# Neova CoreEngine build %d.%d-%s\n",
					core::GetReleaseNumber(),
					core::GetReleaseRevision(),
					core::GetDebugLevel()	);
		if( !file::DumpToFile(paramLogFn,initMsg,Strlen(initMsg)) ) {
			CloseLog();
			return FALSE;
		}

		AppendToLog( "# Comment starts with '#'\n" );
		AppendToLog( "# Simple message format: (text)\n" );
		AppendToLog( "# DMM operation format: frame-no op-tag addr bsize flags < callstack > [comment]\n" );

		return TRUE;
	}


}






bool
nv::mem::LogOpen		(				)
{
	return OpenLog();
}


void
nv::mem::LogClose		(				)
{
	CloseLog();
}


int
nv::mem::LogGetFillingRate		(					)
{
	if( !logBuffer )
		return -1;
	NV_ASSERT( logBufferWPtr >= logBuffer );
	uint32 fillingBSize = logBufferWPtr - logBuffer;
	NV_ASSERT( fillingBSize <= logBufferBSize );
	return int(fillingBSize) * 100 / int(logBufferBSize);
}


bool
nv::mem::LogAppendAllocOp		(	void*			inPointer,
									uint32			inBSize,
									uint			inFlags,
									pcstr			inComment		)
{
	return AppendToLog( 'm', inPointer, inBSize, inFlags, inComment );
}


bool
nv::mem::LogAppendFreeOp		(	void*			inPointer,
									pcstr			inComment		)
{
	return AppendToLog( 'f', inPointer, 0, 0, inComment );
}


bool
nv::mem::LogAppendMessage		(	pcstr			inMessage		)
{
	if( !logBuffer || !inMessage )
		return FALSE;
	uint slen = Strlen( inMessage );
	if( !slen )		return FALSE;
	pstr fmsg = (pstr) alloca( slen+16 );
	if( !fmsg )		return FALSE;
	Sprintf( fmsg, "(%s)\n", inMessage );
	return AppendToLog( fmsg );
}


bool
nv::mem::LogFlushCache			(									)
{
	return FlushLog();
}




