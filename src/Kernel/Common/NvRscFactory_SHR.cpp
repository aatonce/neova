/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/Common/NvRscFactory_SHR.h>
using namespace nv;




RscFactory_SHR::Desc*
RscFactory_SHR::AddDesc(	uint32		inUID,
							uint*		outIndex	)
{
	if( !inUID )
		return NULL;

	NV_ASSERT( !FindDesc(inUID) );

	uint N = descA.size();
	if( N == 0 ) {
		if( outIndex )
			*outIndex = 0;
		descA.push_back( Desc() );
		return descA.data();
	}

	int i = 0,
		j = int(N) - 1,
		k;
	while( j > i ) {
		k = (i+j)>>1;
		if( descA[k].uid > inUID )
			j = k;
		else
			i = k+1;
	}
	NV_ASSERT( i >= 0 );
	NV_ASSERT( i < int(descA.size()) );
	NV_ASSERT( i <= j );

	if( descA[i].uid > inUID ) {
		// before ?
		descA.insert( descA.begin()+i, Desc() );
	}
	else {
		// after
		descA.insert( descA.begin()+i+1, Desc() );
		i++;
	}

	NV_ASSERT_IF( i>0,						descA[i-1].uid < inUID );
	NV_ASSERT_IF( i<int(descA.size())-1,	descA[i+1].uid > inUID );

	if( outIndex )
		*outIndex = i;
	return descA.data()+i;
}



RscFactory_SHR::Desc*
RscFactory_SHR::FindDesc(	uint32		inUID,
							uint*		outIndex	)
{
	if( !inUID )
		return NULL;

	uint N = descA.size();
	if( N == 0 )
		return NULL;

	int i = 0,
		j = int(N)-1,
		k;
	while( j >= i ) {
		k = (i+j)>>1;

		// found
		if( descA[k].uid == inUID ) {
			if( outIndex )
				*outIndex = k;
			return descA.data()+k;
		}
		else if( i==j ) {
			return NULL;	// not found
		}

		// continue
		if( descA[k].uid > inUID )
			j = k;
		else
			i = k+1;
	}

	return NULL;
}


bool
RscFactory_SHR::IsAvailableRsc	(	uint32		inUID	)
{
	return FindDesc(inUID) != NULL;
}


bool
RscFactory_SHR::EnumAvailableRsc ( uint inIdx, uint* outUID, uint* outNbInstance, uint* outNbReference, uint* outMemUBSize )
{
	if( outUID )			*outUID = 0;
	if( outNbInstance )		*outNbInstance = 0;
	if( outNbReference )	*outNbReference = 0;
	if( outMemUBSize )		*outMemUBSize = 0;

	if( inIdx < descA.size() )
	{

		uint32 uid = descA[inIdx].uid;
		uint nb_inst = 0;
		uint nb_ref = 0;
		uint mem_bs = 0;

		mem_bs += descA[inIdx].bsize;

		Instance* inst = descA[inIdx].head;
		while( inst )
		{
			nb_inst += 1;
			nb_ref += inst->inst.refCpt;
			mem_bs += inst->inst.databsize;
			inst = inst->inst.next;
		}

		if( outUID )			*outUID = uid;
		if( outNbInstance )		*outNbInstance = nb_inst;
		if( outNbReference )	*outNbReference = nb_ref;
		if( outMemUBSize )		*outMemUBSize = mem_bs;
		return TRUE;

	}
	else
	{

		return FALSE;

	}
}


pvoid
RscFactory_SHR::AllocMemRsc		(	uint32		inBSize	)
{
	return EngineMalloc( inBSize );
}


void
RscFactory_SHR::FreeMemRsc		(	pvoid		inPtr,
									uint32	/*	inBSize	*/	)
{
	EngineFree( inPtr );
}


uint32
RscFactory_SHR::GetToPrefetchRscBSize	(	uint32		inUID,
											uint32		inBSize		)
{
	return inUID ? inBSize : 0;
}


bool
RscFactory_SHR::PrefetchRsc		(	uint32		inUID,
									pvoid		inPtr,
									uint32		inBSize		)
{
	if(		!inUID
		||	!inPtr
		||	!inBSize
		||	IsAvailableRsc(inUID)	)
		return FALSE;

	if( !CheckRsc(inPtr,inBSize) ) {
		#ifdef _NVCOMP_ENABLE_CONSOLE
			char tmp[64];
			Sprintf( tmp, "Invalid rsc format (UID: 0x%X) !", inUID );
			NV_WARNING( tmp );
		#endif
		return FALSE;
	}

	// Original data relocation
	TranslateRsc( inPtr, uint32(inPtr) );

	Desc* desc	= AddDesc( inUID );
	NV_ASSERT( desc );

	desc->uid	= inUID;
	desc->ptr	= inPtr;
	desc->bsize	= inBSize;
	desc->shrm	= GetRscSHRMode( inPtr );
	desc->head	= NULL;

	return TRUE;
}


bool
RscFactory_SHR::UnloadRsc		(	uint32		inUID		)
{
	if( !inUID )
		return FALSE;

	uint32 descIndex;
	Desc * desc = FindDesc(	inUID, &descIndex );
	if( !desc )
		return TRUE;	// not available <=> unloaded ok !

	// In USED !
	if( desc->head )
		return FALSE;

	// prefetched but not instancied yet !
	if( desc->ptr )
		FreeMemRsc( desc->ptr, desc->bsize );

	descA.erase( descA.begin()+descIndex );
	return TRUE;
}


NvResource*
RscFactory_SHR::CreateInstance	(	uint32	inUID	)
{
	Desc* desc = FindDesc( inUID );
	if( !desc )
		return NULL;
	NV_ASSERT( desc->bsize );
	NV_ASSERT( desc->uid   );

	// Manage the sharing mode

	Instance*	inst = NULL;

	if( desc->shrm == SHRM_CUSTOM )
	{
		inst = CreateInstanceObject( desc, NULL, 0 );
		if( !inst )		return NULL;
	}

	else if( desc->shrm == SHRM_FULL )
	{
		if( desc->head ) {
			// first instance already done => addRef & return the head instance ...
			NV_ASSERT( desc->head->inst.refCpt > 0 );
			desc->head->inst.refCpt++;
			return GetInstanceITF( desc->head );
		}
		NV_ASSERT( desc->ptr );
		inst = CreateInstanceObject( desc, desc->ptr, desc->bsize );
		if( !inst )		return NULL;
	}

	else if( desc->shrm == SHRM_DATA_SHR )
	{
		NV_ASSERT( desc->ptr );
		inst = CreateInstanceObject( desc, desc->ptr, desc->bsize );
		if( !inst )		return NULL;
	}

	else if( desc->shrm == SHRM_DATA_DUP )
	{
		if( !desc->head )
		{
			// first instance use prefetched data ...
			NV_ASSERT( desc->ptr );
			inst = CreateInstanceObject( desc, desc->ptr, desc->bsize );
			if( !inst )		return NULL;
			desc->ptr = NULL;	// prefetched data lost !
		}
		else
		{
			// following instances clone another instance data
			NV_ASSERT( desc->ptr == NULL );
			pvoid toPtr   = AllocMemRsc( desc->bsize );
			if( !toPtr )	return NULL;
			pvoid fromPtr = desc->head->inst.dataptr;
			NV_ASSERT( fromPtr );
			Memcpy( toPtr, fromPtr, desc->bsize );
			TranslateRsc( toPtr, uint32(toPtr)-uint32(fromPtr) );
			inst = CreateInstanceObject( desc, toPtr, desc->bsize );
			if( !inst ) {
				FreeMemRsc( toPtr, desc->bsize );
				return NULL;
			}
		}
	}

	else if( desc->shrm == SHRM_DATA_DUP0 )
	{
		NV_ASSERT( desc->ptr );
		pvoid toPtr = AllocMemRsc( desc->bsize );
		if( !toPtr )	return NULL;
		Memcpy( toPtr, desc->ptr, desc->bsize );
		TranslateRsc( toPtr, uint32(toPtr)-uint32(desc->ptr) );
		inst = CreateInstanceObject( desc, toPtr, desc->bsize );
		if( !inst ) {
			FreeMemRsc( toPtr, desc->bsize );
			return NULL;
		}
	}

	else
	{
		NV_ERROR( "Unsupported sharing mode !" );
		return NULL;
	}

	// Link the instance

	NV_ASSERT( inst );
	NV_ASSERT( inst->inst.next == NULL );
	NV_ASSERT( inst->inst.prev == NULL );
	if( desc->head ) {
		inst->inst.next = desc->head;
		desc->head->inst.prev = inst;
	}
	desc->head = inst;

	return GetInstanceITF( inst );
}


void
RscFactory_SHR::FreeInstance	(	NvResource*	inPtr	)
{
	if( !inPtr )
		return;

	uint32 descIndex;
	Desc* desc = FindDesc( inPtr->GetRscUID(), &descIndex );
	if( !desc )
		return;

	// ITF to base - instance
	Instance* inst = (Instance*)inPtr->GetBase();
	if( !inst )
		return;

	Instance* inst_prev		 = inst->inst.prev;
	Instance* inst_next		 = inst->inst.next;
	pvoid	  inst_dataPtr	 = inst->inst.dataptr;
	uint32	  inst_dataBSize = inst->inst.databsize;

	// Release the instance objet	
	ReleaseInstanceObject( desc, inst );

	// Free duplicated data for instance in DUP & DUP0 shring mode !
	if( desc->shrm == SHRM_DATA_DUP || desc->shrm == SHRM_DATA_DUP0 )
		FreeMemRsc( inst_dataPtr, inst_dataBSize );

	// Remove the instance from list
	if( inst_prev )
		inst_prev->inst.next = inst_next;
	if( inst_next )
		inst_next->inst.prev = inst_prev;
	if( desc->head == inst )
		desc->head = inst_next;

	// not more instance -> unload
	if( desc->head == NULL ) {
		if( desc->ptr )
			FreeMemRsc( desc->ptr, desc->bsize );
		descA.erase( descA.begin()+descIndex );
	}
}



void
RscFactory_SHR::InitInstanceObject	(	Instance*	inInst,
										Desc*		inDesc,
										pvoid		inRscData,
										uint32		inRscBSize		)
{
	NV_ASSERT( inInst );
	NV_ASSERT( inDesc );
	inInst->inst.dataptr	= inRscData;
	inInst->inst.databsize	= inRscBSize;
	inInst->inst.uid		= inDesc->uid;
	inInst->inst.refCpt		= 1;
	inInst->inst.prev		= NULL;
	inInst->inst.next		= NULL;
}


