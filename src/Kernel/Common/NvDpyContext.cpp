/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/Common/NvDpyContext.h>




bool
NvDpyContext::Init()
{
	//
	return TRUE;
}



bool
NvDpyContext::Shut()
{
	//
	return TRUE;
}



bool				
NvDpyContext::Reset			(				)
{
	// light context
	curLight.color[0]		= Vec4( 1, 1, 1, 1 );
	curLight.color[1]		= Vec4( 1, 1, 1, 1 );
	curLight.color[2]		= Vec4( 1, 1, 1, 1 );
	curLight.direction[0]	= Vec3::UNIT_X;
	curLight.direction[1]	= Vec3::UNIT_Y;
	lightA.resize( 1 );
	lightA[0] = curLight;

	// view context
	MatrixIdentity( &curView.viewTR );
	MatrixIdentity( &curView.projTR );
	curView.clipRange = Vec2( 0.1f, 500.0f );
	curView.viewport  = Vec4( 0, 0, 256, 256 );
	viewA.resize( 1 );
	viewA[0] = curView;

	// WTR context
	MatrixIdentity( &curWTR );
	wtrA.resize( 1 );
	wtrA[0] = curWTR;

	// Global context
	curContext.raster		= DPYR_IN_FRAME;
	curContext.viewIdx		= 0;
	curContext.lightIdx		= 0;
	curContext.wtrIdx		= 0;
	contextA.resize( 1 );
	contextA[0] = curContext;

	// Global context-chain
	contextChainA.resize(1);
	contextChainA.clear();
	curState = 0;

	return TRUE;	
}


