/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_File.h>
#include <Kernel/Common/NvkCore_NAFS.h>
using namespace nv;




namespace
{

	struct Network_AFS : public file::AbstractFS
	{
		enum State {
			AFS_CLOSED		= 0,
			AFS_OPENED,
			AFS_READING,
		};


		State				state;
		nafs::ShareInfo		shinfo;
		nafs::Command		cmd;
		char				filename[256];

		// reading state
		uint32				rd_boffset;
		uint32				rd_bsize;
		uint8*				rd_buffptr;
		bool				rd_success;

		virtual	~Network_AFS	(			) {}

		bool	Init			(								)
		{
			cmd.Init();
			state	   = AFS_CLOSED;
			rd_success = TRUE;
			return TRUE;
		}


		void	Shut			(								)
		{
			cmd.Shut();
			NvEngineDelete( this );
		}


		bool	Open			(	pcstr			inFullname,
									uint32&			outBSize	)
		{
			if( IsOpened() )
				return FALSE;

			if( !shinfo.Init(inFullname,filename,sizeof(filename)) )
				return FALSE;

			if( !cmd.Connect(shinfo) )
				return FALSE;

			if( !cmd.GetFilesize(shinfo,filename,outBSize) ) {
				cmd.Disconnect();
				return FALSE;
			}

			state = AFS_OPENED;
			return TRUE;
		}


		bool	Close			(								)
		{
			cmd.Disconnect();
			state = AFS_CLOSED;
			return TRUE;
		}


		nv::file::Status	GetStatus	(		)
		{
			return nv::file::FS_OK;
		}


		bool	IsOpened		(								)
		{
			if( state == AFS_CLOSED )
				return FALSE;

			if( !cmd.IsConnected() ) {
				state = AFS_CLOSED;
				return FALSE;
			}

			return TRUE;
		}


		bool	Read			(	uint			inSysFlags,
									pvoid			inBufferPtr,
									uint32			inBSize,
									uint32			inBOffset0,	uint32	inBOffset1,	uint32	inBOffset2,	uint32	inBOffset3	)
		{
			if( !IsOpened() )		return FALSE;
			if( !IsReady() )		return FALSE;

			if( !inBSize ) {
				rd_success = TRUE;
				return TRUE;
			}

			if( !inBufferPtr )
				return FALSE;

			rd_boffset	= inBOffset0;
			rd_bsize	= inBSize;
			rd_buffptr	= (uint8*) inBufferPtr;

			// <= readfile shname [passwd] path bsize boffset binary
			// => bsize "...."
			bool done = cmd.Exec( "readfile \"%s\" \"%s\" \"%s\" 0x%x 0x%x binary;", shinfo.name, shinfo.passwd, filename, rd_bsize, rd_boffset );
			if( !done )	return FALSE;

			state = AFS_READING;
			return TRUE;
		}


		void	_Update_Read	(		)
		{
			// cmd idle ?
			cmd.Update();
			if( !cmd.IsReady() )
				return;

			// Append rcved data
			char* res = cmd.GetResult();
			if( !res ) {
				rd_success = FALSE;
				state = AFS_OPENED;
				return;
			}

			pstr   rdata;
			uint32 rbs = Strtoul( res, &rdata, 16 );
			NV_ASSERT( rbs <= rd_bsize );

			// find start marker '"' of binary data
			while( *rdata != '\"' )	rdata++;

			libc::Memcpy( rd_buffptr, rdata+1, rbs );
			rd_boffset += rbs;
			rd_buffptr += rbs;
			rd_bsize   -= rbs;

			// reading ended ?
			if( rd_bsize == 0 ) {
				rd_success = TRUE;
				state = AFS_OPENED;
				return;
			}

			// <= readfile shname [passwd] path bsize boffset binary
			// => bsize "...."
			bool done = cmd.Exec( "readfile \"%s\" \"%s\" \"%s\" 0x%x 0x%x binary;", shinfo.name, shinfo.passwd, filename, rd_bsize, rd_boffset );
			if( !done ) {
				rd_success = FALSE;
				state = AFS_OPENED;
				return;
			}
		}

		bool	IsReady			(								)
		{
			if( !IsOpened() )	return TRUE;
			if( state == AFS_READING )
				_Update_Read();
			return ( state != AFS_READING );
		}

		bool	IsSuccess		(								)
		{
			return rd_success;
		}

		void	Sync			(								)
		{
			//
		}

		bool	DumpFromFile	(	pcstr			inFullname,
									pvoid&			outBuffer,
									uint&			outBSize		)
		{
			nafs::ShareInfo		l_shinfo;
			nafs::Command		l_cmd;
			char				l_filename[256];
			bool res = FALSE;
			l_cmd.Init();

			if( l_shinfo.Init(inFullname,l_filename,sizeof(l_filename)) )
				if( l_cmd.Connect(l_shinfo) )
					if( l_cmd.DumpFromFile(l_shinfo,l_filename,outBuffer,outBSize) )
						res = TRUE;

			l_cmd.Shut();
			return res;
		}

		bool	DumpToFile		(	pcstr			inFullname,
									pvoid			inBuffer,
									uint			inBSize			)
		{
			nafs::ShareInfo		l_shinfo;
			nafs::Command		l_cmd;
			char				l_filename[256];
			bool res = FALSE;
			l_cmd.Init();

			if( l_shinfo.Init(inFullname,l_filename,sizeof(l_filename)) )
				if( l_cmd.Connect(l_shinfo) )
					if( l_cmd.DumpToFile(l_shinfo,l_filename,inBuffer,inBSize) )
						res = TRUE;

			l_cmd.Shut();
			return res;
		}

		bool	AppendToFile	(	pcstr			inFullname,
									pvoid			inBuffer,
									uint			inBSize			)
		{
			nafs::ShareInfo		l_shinfo;
			nafs::Command		l_cmd;
			char				l_filename[256];
			bool res = FALSE;
			l_cmd.Init();

			if( l_shinfo.Init(inFullname,l_filename,sizeof(l_filename)) )
				if( l_cmd.Connect(l_shinfo) )
					if( l_cmd.AppendToFile(l_shinfo,l_filename,inBuffer,inBSize) )
						res = TRUE;

			l_cmd.Shut();
			return res;
		}

		uint	GetNbSubDevice		(			)
		{
			return 1;
		}

		pcstr	GetSubDeviceName	(	uint		inSubdevNo		)
		{
			if( inSubdevNo != 0 )	return NULL;
			return "Network AFS";
		}

		pcstr	GetSubDevicePrefix	(	uint		inSubdevNo		)
		{
			if( inSubdevNo != 0 )	return NULL;
			return ",,,,";
		}

		pcstr	GetSubDeviceSuffix	(	uint		inSubdevNo		)
		{
			return NULL;
		}
	};

}



nv::file::AbstractFS*
nv::file::CreateNetworkAbstractFS		(				)
{
	Network_AFS* afs = NvEngineNew( Network_AFS );
	if( !afs->Init() ) {
		NvEngineDelete( afs );
		afs = NULL;
	}
	return afs;
}


