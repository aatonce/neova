/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>
using namespace nv;




// Profiling pool unit byte size
#define		PROFILING_POOL_BSIZE	(64*1024)

// Enable profiling ?
#ifdef		_NVCOMP_ENABLE_PROFILING
#define		ENABLE_PROFILING
#endif

// Enable profiling at start time ?
#ifdef		_NVCOMP_ENABLE_DBG
#define		START_PROFILING
#endif



namespace
{


	struct ABlock {
		void*	addr;
		uint32	bsize;
	};


	struct ABlockCmp {
		bool operator () ( const ABlock& inB0, const ABlock& inB1 ) const
		{
			return uint32(inB0.addr) < uint32(inB1.addr);
		}
	};


	struct ABlockTree : public ptree<ABlock,ABlockCmp>
	{
		void*	allocPool	(	uint32&		outBSize,
								uint		inNodeBSize		)
		{
			outBSize = PROFILING_POOL_BSIZE;
			return mem::NativeMalloc( PROFILING_POOL_BSIZE, 16 );
		}

		void	freePool	(	void*		inPointer		)
		{
			if( inPointer )
				mem::NativeFree( inPointer );
		}
	};


	typedef ABlockTree::Node	ABlockNode;




	struct NativeProfiler
	{
		bool			enabled;
		uint32			allocCpt;
		uint32			allocNb;
		uint32			allocNbPeak;
		uint32			allocTotBSize;
		uint32			allocTotBSizePeak;
		ABlockTree		allocTree;

		void
		Init	(		)
		{
			allocTree.Init();
			enabled	 = FALSE;
			Clear();
		}

		void
		Shut	(		)
		{
			Clear();
			allocTree.Shut();
		}

		void
		Clear	(		)
		{
			allocTree.freemem();
			allocCpt			 = 0;
			allocNb				 = 0;
			allocNbPeak			 = 0;
			allocTotBSize		 = 0;
			allocTotBSizePeak	 = 0;
		}

		void
		Enable	(		)
		{
			enabled = TRUE;
		}

		void
		Disable	(		)
		{
			enabled = FALSE;
			Clear();
		}

		bool
		IsEnabled	(		)
		{
			return enabled;
		}

		bool
		RegisterAlloc	(	void*	inAddr,		uint32	inBSize		)
		{
			if( !enabled )		return FALSE;
			ABlock ab;
			ab.addr  = inAddr;
			ab.bsize = inBSize;
			if( !allocTree.insertUnique(ab) ) {
				DebugPrintf( "<Neova> Native DMM allocator: Invalid alloc !\n" );
				return FALSE;
			}
			allocCpt++;
			allocNb++;
			allocNbPeak = Max( allocNbPeak, allocNb );
			allocTotBSize += inBSize;
			allocTotBSizePeak = Max( allocTotBSizePeak, allocTotBSize );
			return TRUE;
		}

		bool
		UnregisterAlloc	(	void*	inAddr		)
		{
			if( !enabled )		return FALSE;
			ABlock ab;
			ab.addr = inAddr;
			ABlockNode* n = allocTree.find( ab );
			if( !n ) {
				DebugPrintf( "<Neova> Native DMM Allocator: Invalid free !\n" );
				return FALSE;
			}
			uint32 absize = n->key.bsize;
			NV_ASSERT( allocNb >= 1 );
			if( allocNb )	allocNb--;
			NV_ASSERT( allocTotBSize >= absize );
			if( allocTotBSize >= absize )	allocTotBSize -= absize;
			allocTree.erase( n );
			return TRUE;
		}

		uint32
		FindAllocBSize	(	void*	inAddr		)
		{
			if( !enabled )		return 0;
			ABlock ab;
			ab.addr = inAddr;
			ABlockNode* n = allocTree.find( ab );
			if( n )		return n->key.bsize;
			DebugPrintf( "<Neova> Native DMM Allocator: Invalid alloc bsize !\n" );
			return 0;
		}

		void
		DebugOutput		(			)
		{
			uint        nb = allocTree.size();
			ABlockNode* n  = allocTree.first();
			DebugPrintf( "<Neova> Native DMM Allocator output: #=%d\n", nb );
			for( uint i = 0 ; i < nb ; i++ ) {
				NV_ASSERT( n );
				DebugPrintf( "(%08x) @=%08x S=%08x\n", i, uint32(n->key.addr), n->key.bsize );
				n = n->next();
			}
		}
	};



	interface NativeAllocator : public nv::mem::Allocator
	{
		uint				refCpt;
		mem::MemId			memId;
		uint32				defbalign;
		bool				canprof;
		NativeProfiler		profiler;

		virtual			  ~NativeAllocator()	{}


		bool
		_Init	(	mem::MemId		inMemId,
					bool			inCanProf,
					uint			inDefBAlign		)
		{
			refCpt		= 1;
			memId		= inMemId;
			canprof		= inCanProf;
			defbalign	= inDefBAlign;

			profiler.Init();

			if( canprof )
			{
				#if defined(ENABLE_PROFILING) && defined(START_PROFILING)
				profiler.Enable();
				#else
				profiler.Disable();
				#endif
			}

			return TRUE;
		}

		void
		_Shut	(		)
		{
			#ifdef _NVCOMP_ENABLE_DBG
			profiler.DebugOutput();
			#endif
			profiler.Shut();

			NativeDelete( this );
		}

		NvInterface*
		GetBase		(		)
		{
			return this;
		}

		void
		AddRef		(		)
		{
			refCpt++;
		}

		uint
		GetRefCpt	(		)
		{
			return refCpt;
		}

		void			
		Release		(		)
		{
			if( refCpt > 1 )
			{
				refCpt--;
			}
			else
			{
				_Shut();
			}
		}

		bool
		Reset	(		)
		{
			return FALSE;	// can't !
		}

		void
		Update	(		)
		{
			//
		}

		void*
		AllocInPlace(	void*			inPointer,
						uint32			inBSize		)
		{
			// not supported !
			return NULL;
		}

		void*
		Alloc		(	uint32			inBSize,
						uint			inFlags		)
		{
			return AllocA( inBSize, defbalign, inFlags );
		}

		void*
		AllocA		(	uint32			inBSize,
						uint			inBAlignment,
						uint			inFlags		)
		{
			if( !inBSize )
				return NULL;

			inBAlignment = Max( inBAlignment, defbalign );
			if( !IsPow2(inBAlignment) )
				return NULL;

			void* ptr = mem::NativeMalloc( inBSize, inBAlignment, memId);
			if( !ptr )
				return NULL;

			profiler.RegisterAlloc( ptr, inBSize );
			#ifdef _NVCOMP_ENABLE_PROFILING
			mem::LogAppendAllocOp( ptr, inBSize, inFlags, "native alloc" );
			#endif
			return ptr;
		}

		void*
		Realloc		(	void*			inPointer,
						uint32			inBSize,
						uint			inFlags		)
		{
			if( !inBSize ) {
				Free( inPointer );
				return NULL;
			}
			if( !inPointer )
				return Alloc( inBSize, inFlags );

			void* ptr = mem::NativeRealloc( inPointer, inBSize, memId);
			if( !ptr )
				return NULL;

			profiler.UnregisterAlloc( inPointer );
			profiler.RegisterAlloc( ptr, inBSize );
			#ifdef _NVCOMP_ENABLE_PROFILING
			mem::LogAppendFreeOp( inPointer, "native realloc" );
			mem::LogAppendAllocOp( ptr, inBSize, inFlags, "native realloc" );
			#endif
			return ptr;
		}

		void
		Free		(	void*			inPointer	)
		{
			if( inPointer ) {
				#if defined( _NVCOMP_ENABLE_DBG )
				uint32 allocBSize = profiler.FindAllocBSize( inPointer );
				if( allocBSize ) {
					uint32 resetValue;
					core::GetParameter(	core::PR_MEM_RESET_VALUE, &resetValue );
					Memset( inPointer, resetValue, allocBSize );
				}
				#endif

				mem::NativeFree( inPointer, memId );

				profiler.UnregisterAlloc( inPointer );
				#ifdef _NVCOMP_ENABLE_PROFILING
				mem::LogAppendFreeOp( inPointer, "native free" );
				#endif
			}
		}

		void
		EnableProfiling		(			)
		{
			if( canprof )
			{
				#if defined(ENABLE_PROFILING)
				profiler.Enable();
				#endif
			}
		}

		void
		DisableProfiling	(			)
		{
			#if defined(ENABLE_PROFILING)
			profiler.Disable();
			#endif
		}

		bool
		IsProfiling			(			)
		{
			return profiler.IsEnabled();
		}

		uint32
		GetTotBSize		(		)
		{
			return 0;
		}

		uint32
		GetAllocTotBSize	(		)
		{
			return profiler.allocTotBSize;
		}

		uint32
		GetAllocTotBSizePeak	(		)
		{
			return profiler.allocTotBSizePeak;
		}

		uint32
		GetAllocNb		(		)
		{
			return profiler.allocNb;
		}

		uint32
		GetAllocNbPeak	(		)
		{
			return profiler.allocNbPeak;
		}

		uint32
		GetAllocCpt		(		)
		{
			return profiler.allocCpt;
		}

		mem::RangeStatus
		CheckRange		(	void*		inPointer,
							uint32		inBSize		)
		{
			return mem::RS_NOT_AVAILABLE;
		}
	};

}



nv::mem::Allocator*
nv::mem::CreateNativeAllocator		(	MemId		inMemId,
										bool		inCanProfile		)
{
	uint32 defbalign;
	core::GetParameter( core::PR_MEM_BALIGN, &defbalign );
	if( !defbalign )
		defbalign = 32;

	NativeAllocator* ator = NativeNewS<NativeAllocator>( 0 );
	if( !ator )
		return NULL;

	if( !ator->_Init(inMemId,inCanProfile,defbalign) ) {
		NativeDelete( ator );
		ator = NULL;
	}

	return ator;
}



