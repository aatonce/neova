/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/ALG/ALG.h>
using namespace nv;


namespace
{

	uint	dispMode;
	uint	dispSizeX;
	uint	dispSizeY;
	pvoid	dispAddr;
	uint	dispWidth;
	uint	dispPSM;
	bool	dispEnabled;
	int		vsyncIntrIdx;


	void vsyncHandler( int, void* )
	{
		ge::crtc::vsyncCpt++;
	}


	void RemoveVSyncHandler	(	)
	{
		// disable vsync-intr
		if( vsyncIntrIdx >= 0 )
			sceDisplaySetVblankCallback( vsyncIntrIdx, NULL, NULL );
		vsyncIntrIdx = -1;
	}


	void SetupVSyncHandler	(	)
	{
		core::GetParameter( core::PR_ALG_VSYNC_INTR_IDX, (uint32*)&vsyncIntrIdx );
		if( vsyncIntrIdx<0 || vsyncIntrIdx>15 )
			vsyncIntrIdx = 0;
		int res = sceDisplaySetVblankCallback( vsyncIntrIdx, vsyncHandler, NULL );
		NV_ASSERTC( res==0, "<Neova> sceDisplaySetVblankCallback() failed !" );
	}


	ge::crtc::Mode ChangeDisplay( pvoid inAddr, uint inSizeX, uint inSizeY, uint inWidth, uint inPSM )
	{
		bool consoleOnly = FALSE;
		core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
		if( consoleOnly )
			return ge::crtc::NEXT_HSYNC;

		// When either the width or pixel format of the frame buffer is changed, the update cannot be performed immediately.
		// Be sure to specify that switching be performed during the next VBLANK interval by setting the iMode argument to SCE_DISPLAY_UPDATETIMING_NEXTVSYNC.

		if( !inAddr )
			inWidth = 0;

		bool ch0  = (inSizeX!=dispSizeX || inSizeY!=dispSizeY);
		bool ch1  = (inWidth!=dispWidth || inPSM!=dispPSM);
		bool ch2  = (inAddr!=dispAddr);
		if( ch0 ) {
			sceDisplaySetMode( dispMode, inSizeX, inSizeY );
			dispSizeX = inSizeX;
			dispSizeY = inSizeY;
		}
		if( ch1 || ch2 ) {
			uint mode = ch1 ? SCE_DISPLAY_UPDATETIMING_NEXTVSYNC : SCE_DISPLAY_UPDATETIMING_NEXTHSYNC;
			sceDisplaySetFrameBuf( inAddr, inWidth, inPSM, mode );
			dispAddr  = inAddr;
			dispWidth = inWidth;
			dispPSM   = inPSM;
		} 
		return (ch0||ch1) ? ge::crtc::NEXT_VSYNC : ge::crtc::NEXT_HSYNC;
	}

}



volatile uint32		ge::crtc::vsyncCpt;
uint32				ge::crtc::vsyncRate;



void
ge_crtc_Reset(	bool	inEnable	)
{
	RemoveVSyncHandler();
	SetupVSyncHandler();

	if( inEnable )		ge::crtc::Enable();
	else				ge::crtc::Disable();

	ge::crtc::vsyncCpt  = 0;
	ge::crtc::vsyncRate = (uint) Ceil( sceDisplayGetFramePerSec() );
}



void
ge_crtc_Init( bool	inEnable )
{
	uint32 vmode = 0;
	core::GetParameter( core::PR_ALG_VMODE, &vmode );

	#if defined(_ATMON) || !defined(_NVCOMP_DEVKIT)
	vmode &= ~(ge::VM_VESA1A|ge::VM_VGA);
	vmode |= ge::VM_LCD;	// only LCD is valid !
	core::SetParameter( core::PR_ALG_VMODE, vmode );
	#endif
	if( vmode & ge::VM_LCD )			dispMode = SCE_DISPLAY_MODE_LCD;
	else if( vmode & ge::VM_VESA1A )	dispMode = SCE_DISPLAY_MODE_VESA1A;
	else if( vmode & ge::VM_VGA )		dispMode = SCE_DISPLAY_MODE_PSEUDO_VGA;
	else								dispMode = SCE_DISPLAY_MODE_LCD;

	dispSizeX	= ge::frame::PhysicalSizeX;
	dispSizeY	= ge::frame::PhysicalSizeY;
	dispAddr	= 0;
	dispWidth	= 0;
	dispPSM		= 0;

	vsyncIntrIdx = -1;

	ge_crtc_Reset( inEnable );
}


void
ge_crtc_Shut	(	)
{
	RemoveVSyncHandler();
}


ge::crtc::Mode
ge::crtc::Enable()
{
	dispEnabled = TRUE;
	uint32 vmode = 0;
	core::GetParameter( core::PR_ALG_VMODE, &vmode );
	if( vmode & ge::VM_SHOWBACK )	return ShowBack();
	else							return ShowFront();
}


ge::crtc::Mode
ge::crtc::Disable()
{
	dispEnabled = FALSE;
	return ChangeDisplay( 0, dispSizeX, dispSizeY, 0, 0 );
}


ge::crtc::Mode
ge::crtc::ShowBack	(			)
{
	if( !dispEnabled )
		return ge::crtc::NEXT_HSYNC;
	else
		return ChangeDisplay(	ge::GetVRamAddr(ge::frame::BackAddr),
								ge::frame::SizeX,
								ge::frame::SizeY,
								ge::frame::BackWidth,
								ge::frame::BackPSM	);
}


ge::crtc::Mode
ge::crtc::ShowFront	(		)
{
	if( !dispEnabled )
		return ge::crtc::NEXT_HSYNC;
	else
		return ChangeDisplay(	ge::GetVRamAddr(ge::frame::FrontAddr),
								ge::frame::PhysicalSizeX,
								ge::frame::PhysicalSizeY,
								ge::frame::FrontWidth,
								ge::frame::FrontPSM	);
}


void
ge::crtc::VSync( uint inVBlankCpt )
{
	if( inVBlankCpt == 0 ) {
		sceDisplayWaitVblank();
		return;
	}
	while( inVBlankCpt-- )
		sceDisplayWaitVblankStart();
}




