/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/ALG/ALG.h>
#include <libsas.h>
using namespace nv;



#define		AUDIO_OUT_ACCUM			(4)
#define		AUDIO_OUT_SAMPLES		(SCE_SAS_GRAIN_SAMPLES*AUDIO_OUT_ACCUM)
#define		SAS_ALLADSR				(SCE_SAS_ATTACK_VALID|SCE_SAS_DECAY_VALID|SCE_SAS_SUSTAIN_VALID|SCE_SAS_RELEASE_VALID)
#define		TH_STACKSIZE			(1024*16)


namespace
{

	short					audioOut[2][AUDIO_OUT_SAMPLES*2]		__attribute__((aligned(64)));
	short*					audioOut_front;
	short*					audioOut_back;

	file::ReadRequest		rr;
	int						loadingSdId;

	struct Snd {
		audio::SdState		state;
		uint				mode;
		uint				baseFreq;
		uint32				boffset;
		uint				bsize;
		float				duration;
		void*				data;
		uint32				chMask;
		bool				autoFree;
	};

	enum {
		CHAN_KEYON		= (1<<0),
		CHAN_KEYOFF		= (1<<1),
		CHAN_PITCH		= (1<<2),
		CHAN_VOL		= (1<<3),
		CHAN_CMDS		= 0xF,
		CHAN_ENDED		= (1<<4)
	};

	struct Chan {
		uint8				flags;
		int8				sid;
		uint				pitch;
		uint				vol[4];
	};

	Snd						sndA[audio::NB_SOUND_MAX];
	Chan					chA[audio::NB_CHANNEL];
	float					masterVol;
	float					fxVol;
	uint					mixSw;

	SceUID					thId;
	bool					thOn;
	volatile uint			thCpt;


	SceInt32	soundTH(	SceSize		args,
							ScePVoid	argp	)
	{
		while( thOn )
		{
			// Apply sas cmds
			for( int i = 0 ; i < audio::NB_CHANNEL ; i++ ) {
				uint flags = chA[i].flags;
				int  sid   = chA[i].sid;
				if( (flags&CHAN_CMDS) && (sid>=0) ) {
					chA[i].flags = 0;
					if( flags & CHAN_KEYOFF )
						sceSasSetKeyOff( i );
		            if( flags & CHAN_KEYON ) {
						sceSasSetKeyOff( i );
	                	sceSasSetVoice( i, (uchar*)sndA[sid].data, sndA[sid].bsize, SCE_SAS_LOOP_ENABLE );
		            }
		            if( flags & CHAN_PITCH )
						sceSasSetPitch(i, chA[i].pitch );
					if( flags & CHAN_VOL ) {
						sceSasSetADSR( i, SAS_ALLADSR, 0x40000000, 100, 100, 0x10000000 );
	                	sceSasSetVolume( i, chA[i].vol[0], chA[i].vol[1], chA[i].vol[2], chA[i].vol[3] );
					}
		            if( flags & CHAN_KEYON )
						sceSasSetKeyOn( i );
				}
			}

			uint32 chEnd  = 0;
			uint   rssize = 0;
			short* rptr   = audioOut_front;
			while( rssize != AUDIO_OUT_SAMPLES ) {
				int res = sceSasCore( rptr );
				NV_ASSERT( res == SCE_OK );
				rptr   += SCE_SAS_GRAIN_SAMPLES*2;
				rssize += SCE_SAS_GRAIN_SAMPLES;
				chEnd  |= sceSasGetEndFlag();
			}

			// Apply sas cmds
			for( int i = 0 ; i < audio::NB_CHANNEL ; i++ ) {
				if( chEnd&1 )
					chA[i].flags |= CHAN_ENDED;
				chEnd >>= 1;
			}

			thCpt++;
			Swap( audioOut_front, audioOut_back );
			sceWaveAudioWriteBlocking( 0, 32768, 32768, audioOut_back );
		}

		while( sceWaveAudioGetRestSample(0) > 0 )
			sceKernelDelayThread( 1000 );

		sceKernelExitThread(0);
		return 0;
	}


	bool	Start_soundTH(	)
	{
		thCpt  	= 0;
		thOn  	= TRUE;
		thId   	= sceKernelCreateThread( "NvSoundTH", soundTH, SCE_KERNEL_USER_HIGHEST_PRIORITY, TH_STACKSIZE, 0, NULL );
		NV_ASSERT( thId >= 0 );
		SceInt32 thStatus = sceKernelStartThread( thId, 0, 0 );
		NV_ASSERT( thStatus == SCE_KERNEL_ERROR_OK );
		return TRUE;
	}


	void	Stop_soundTH(	)
	{
		thOn = FALSE;
		SceInt32 thStatus = sceKernelWaitThreadEnd( thId, NULL);
		NV_ASSERT( thStatus == SCE_KERNEL_ERROR_OK );
		thStatus = sceKernelDeleteThread( thId );
		NV_ASSERT( thStatus == SCE_KERNEL_ERROR_OK );
	}

}




bool
audio::Init		(				)
{
	loadingSdId = -1;
	NV_COMPILE_TIME_ASSERT( audio::NB_CHANNEL == SCE_SAS_VOICE_MAX );

	for( uint i = 0 ; i < NB_SOUND_MAX ; i++ ) {
		sndA[i].state = SD_UNUSED;
		sndA[i].data  = NULL;
	}
	for( uint i = 0 ; i < NB_CHANNEL ; i++ ) {
		chA[i].flags = 0;
		chA[i].sid   = -1;
	}

	int res = sceWaveInit();
	if( res != SCE_OK )
		return FALSE;

	sceWaveAudioSetSample( 0, AUDIO_OUT_SAMPLES );
	sceWaveAudioSetFormat( 0, SCE_WAVE_AUDIO_FMT_S16_STEREO );
	sceWaveAudioSetVolume( 0, 32768, 32768 );
	audioOut_back    = &audioOut[0][0];
	audioOut_front   = &audioOut[1][0];

	res = sceSasInit();
	if( res != SCE_OK )
		return FALSE;

	sceSasSetEffectVolume( SCE_SAS_FX_VOLUME_MAX, SCE_SAS_FX_VOLUME_MAX );
	sceSasSetEffectType( SCE_SAS_FX_TYPE_ECHO );
	sceSasSetEffectParam( 64, 80 );
	sceSasSetEffect( 1, 0 );

	masterVol = 1.f;
	fxVol     = 1.f;
	mixSw	  = MX_DRY;

	return Start_soundTH();
}


void
audio::Update	(			)
{
	// Update channel end

	uint32 chm = 1;
	for( int i = 0 ; i < NB_CHANNEL ; i++ ) {
		int sid = chA[i].sid;
		if( sid>=0 && (chA[i].flags&CHAN_ENDED) ) {
			NV_ASSERT( sid < NB_SOUND_MAX );
			NV_ASSERT( sndA[sid].state==SD_LOCKED || sndA[sid].state==SD_UNLOCKING );
			NV_ASSERT( sndA[sid].chMask & chm );
			sndA[sid].chMask &= ~chm;
			chA[i].sid = -1;
		}
		chm <<= 1;
	}

	// Load completed ?

	if( loadingSdId >= 0 ) {
		NV_ASSERT( sndA[loadingSdId].state==SD_LOCKING );
		if( rr.IsReady() ) {
			if( rr.GetState() == file::ReadRequest::COMPLETED ) {
				sndA[loadingSdId].autoFree      = FALSE;
				sndA[loadingSdId].chMask		= 0;
				sndA[loadingSdId].state			= SD_LOCKED;
			} else {
				sndA[loadingSdId].state			= SD_UNLOCKED;
				SafeFree( sndA[loadingSdId].data );
			}
			loadingSdId = -1;
		}
	}

	// Update sounds

	for( uint i = 0 ; i < NB_SOUND_MAX ; i++ )
	{
		if( sndA[i].state == SD_LOCKING )
		{
			if( loadingSdId < 0 ) {
				sndA[i].data = EngineMallocA( sndA[i].bsize+64, 16 );
				if( sndA[i].data ) {
					rr.Setup( sndA[i].data, sndA[i].bsize, sndA[i].boffset );
					file::AddReadRequest( &rr );
					loadingSdId = i;
					FlushDCache();
				}
			}
		}
		else if( sndA[i].state == SD_UNLOCKING )
		{
			if( sndA[i].chMask == 0 ) {
				SafeFree( sndA[i].data );
				sndA[i].state = sndA[i].autoFree ? SD_UNUSED : SD_UNLOCKED;
			}
		}
	}	
}


void
audio::Shut			(			)
{
	Stop_soundTH();
	sceSasExit();
	sceWaveExit();
}



void
audio::EnableMix	(	MixSwitch		inMix		)
{
	mixSw |= inMix;
	sceSasSetEffect( mixSw&MX_DRY?1:0, mixSw&MX_WET?1:0 );
}


void
audio::DisableMix	(	MixSwitch		inMix		)
{
	mixSw &= ~inMix;
	sceSasSetEffect( mixSw&MX_DRY?1:0, mixSw&MX_WET?1:0 );
}


void
audio::SetMasterVolume		(	float		inVol		)
{
	masterVol = Clamp( inVol, 0.f, 1.f );
}


void
audio::SetEffectVolume		(	float		inVol		)
{
	fxVol = Clamp( inVol, 0.f, 1.f );
}


void
audio::SetEffectType		(	FxType		inFxType	)
{
	sceSasSetEffectType( int(inFxType) );
}


void
audio::SetEffectParam		(	uint		inDT,
								uint		inFB		)
{
	sceSasSetEffectParam( inDT, inFB );
}




audio::SdState
audio::SdGetState		(	int				inSdId			)
{
	if( inSdId<0 || inSdId>=NB_SOUND_MAX )
		return SD_UNUSED;
	return sndA[inSdId].state;
}


uint
audio::SdGetMode	(	int			inSdId			)
{
	if( inSdId<0 || inSdId>=NB_SOUND_MAX )
		return 0;
	return sndA[inSdId].mode;
}


uint
audio::SdGetBaseFreq	(	int		inSdId		)
{
	if( inSdId<0 || inSdId>=NB_SOUND_MAX )
		return 0;
	return sndA[inSdId].baseFreq;
}


float
audio::SdGetDuration	(	int		inSdId		)
{
	if( inSdId<0 || inSdId>=NB_SOUND_MAX )
		return 0.f;
	return sndA[inSdId].duration;
}


bool
audio::SdLock	(	int				inSdId,
					uint			inMode,
					uint			inDataFreq,
					uint32			inDataBOffset,
					uint32			inDataBSize,
					float			inDuration,
					float			inLockFromTime,
					float			inLockDuration	)
{
	if( inSdId<0 || inSdId>=NB_SOUND_MAX ) {
		DebugPrintf( "audio::SdLock: Invalide sound-ID %d (boffset=0x%x08) !\n", inSdId, inDataBOffset );
		return FALSE;
	}

	if( SdGetState(inSdId) != SD_UNLOCKED ) {
		DebugPrintf( "audio::SdLock: Sound-ID %d is already locking (boffset=0x%x08) !\n", inSdId, inDataBOffset );
		return FALSE;
	}

	if( !inDataBSize || !inDataFreq ) {
		DebugPrintf( "audio::SdLock: Invalid bSize=%d and/or freq=%d (boffset=0x%x08) !\n", inDataBSize, inDataFreq, inDataBOffset );
		return FALSE;
	}

	if( inDuration<=0.f ) {
		DebugPrintf( "audio::SdLock: Invalid duration=%f (boffset=0x%x08) !\n", inDuration, inDataBOffset );
		return FALSE;
	}

	// Check streamed segment !
	if( inMode != 0 )	return FALSE;
	if( inMode && inLockFromTime==0.f && inLockDuration==0.f )
		inLockDuration = inDuration;
	if( inMode && (inLockFromTime<0.f || inLockDuration<=0.f) ) {
		DebugPrintf( "audio::SdLock: Invalid segment [%f,%f] (boffset=0x%x08) !\n", inLockFromTime, inLockDuration, inDataBOffset );
		return FALSE;
	}

	sndA[inSdId].mode	  = inMode;
	sndA[inSdId].baseFreq = inDataFreq;
	sndA[inSdId].boffset  = inDataBOffset;
	sndA[inSdId].bsize    = inDataBSize;
	sndA[inSdId].duration = inDuration;
	sndA[inSdId].state    = SD_LOCKING;
	return TRUE;
}


bool
audio::SdUnlock			(	int				inSdId		)
{
	if( inSdId<0 || inSdId>=NB_SOUND_MAX )
		return FALSE;
	if( sndA[inSdId].state <= SD_UNLOCKED )		return TRUE;
	if( sndA[inSdId].state == SD_UNLOCKING )	return TRUE;
	if( sndA[inSdId].state != SD_LOCKED )		return FALSE;
	sndA[inSdId].state    = SD_UNLOCKING;
	sndA[inSdId].autoFree = FALSE;
	return TRUE;
}


void
audio::SdFree		(	int			inSdId		)
{
	if( inSdId<0 || inSdId>=NB_SOUND_MAX )
		return;
	SdUnlock( inSdId );
	if( sndA[inSdId].state == SD_UNLOCKED )
		sndA[inSdId].state = SD_UNUSED;
	else if( sndA[inSdId].state == SD_UNLOCKING )
		sndA[inSdId].autoFree = TRUE;
}


int
audio::SdAlloc		(	int		inSdId		)
{
	if( inSdId<0 || inSdId>=NB_SOUND_MAX )
		return -1;
	if( sndA[inSdId].state != SD_UNUSED )
		return -1;
	sndA[inSdId].state = SD_UNLOCKED;
	return inSdId;
}


int
audio::SdAny		(			)
{
	for( int i = 0 ; i < NB_SOUND_MAX ; i++ ) {
		if( sndA[i].state == SD_UNUSED )
			return i;
	}
	return -1;
}




audio::ChState
audio::ChGetState	(	int			inChannel		)
{
	return ChGetSoundId(inChannel)<0 ? CH_IDLE : CH_PLAYING;
}

int
audio::ChGetSoundId	(	int			inChannel		)
{
	if( inChannel<0 || inChannel>=NB_CHANNEL )
		return -1;
	return chA[inChannel].sid;
}


bool
audio::ChPlay	(	int				inChannel,
					int				inSdId,
					uint			inPitch,
					int				inVolL,
					int				inVolR		)
{
	if( inChannel<0 || inChannel>=NB_CHANNEL || chA[inChannel].sid>=0 )
		return FALSE;
	if( inSdId<0 || inSdId>=NB_SOUND_MAX || sndA[inSdId].state!=SD_LOCKED )
		return FALSE;

	sndA[inSdId].chMask  |= 1U<<inChannel;
	chA[inChannel].sid    = inSdId;
	chA[inChannel].pitch  = inPitch ? inPitch : 1;
	chA[inChannel].vol[0] = int( float(inVolL) * masterVol );
	chA[inChannel].vol[1] = int( float(inVolR) * masterVol );
	chA[inChannel].vol[2] = int( float(inVolL) * fxVol );
	chA[inChannel].vol[3] = int( float(inVolR) * fxVol );
	chA[inChannel].flags  = CHAN_KEYON | CHAN_PITCH | CHAN_VOL;
	return TRUE;
}


bool
audio::ChStop		(	int			inChannel		)
{
	if( inChannel<0 || inChannel>=NB_CHANNEL || chA[inChannel].sid<0 )
		return FALSE;
	chA[inChannel].flags = CHAN_KEYOFF;
	return TRUE;
}


bool
audio::ChSetVolume	(	int			inChannel,
						int			inVolL,
						int			inVolR		)
{
	if( inChannel<0 || inChannel>=NB_CHANNEL || chA[inChannel].sid<0 )
		return FALSE;
	chA[inChannel].vol[0] = int( float(inVolL) * masterVol );
	chA[inChannel].vol[1] = int( float(inVolR) * masterVol );
	chA[inChannel].vol[2] = int( float(inVolL) * fxVol );
	chA[inChannel].vol[3] = int( float(inVolR) * fxVol );
	chA[inChannel].flags |= CHAN_VOL;
	return TRUE;
}


bool
audio::ChSetPitch	(	int				inChannel,
						uint			inPitch				)
{
	if( inChannel<0 || inChannel>=NB_CHANNEL || chA[inChannel].sid<0 )
		return FALSE;
	chA[inChannel].pitch  = inPitch ? inPitch : 1;
	chA[inChannel].flags  = CHAN_PITCH;
	return TRUE;
}


int
audio::ChAny		(			)
{
	for( int i = 0 ; i < NB_CHANNEL ; i++ ) {
		if( chA[i].sid < 0 )
			return i;
	}
	return -1;
}



