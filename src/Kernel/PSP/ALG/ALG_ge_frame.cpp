/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/ALG/ALG.h>
using namespace nv;



uint32		ge::frame::PhysicalSizeX;
uint32		ge::frame::PhysicalSizeY;
uint32		ge::frame::VirtualSizeX;
uint32		ge::frame::VirtualSizeY;
uint32		ge::frame::SizeX;
uint32		ge::frame::SizeY;
uint32		ge::frame::SizeX2;
uint32		ge::frame::SizeY2;
uint32		ge::frame::OffsetX;
uint32		ge::frame::OffsetY;
uint32		ge::frame::FrontAddr;
uint32		ge::frame::FrontWidth;
uint32		ge::frame::FrontBSize;
uint32		ge::frame::FrontPSM;
uint32		ge::frame::BackAddr;
uint32		ge::frame::BackWidth;
uint32		ge::frame::BackBSize;
uint32		ge::frame::BackPSM;
uint32		ge::frame::DepthAddr;
uint32		ge::frame::DepthWidth;
uint32		ge::frame::DepthBSize;
uint32		ge::frame::VRamTopAddr;
uint32		ge::frame::VRamLowerAddr;
uint32		ge::frame::VRamUpperAddr;
ge::Context	ge::frame::DefContext;


namespace
{

	int		swapCpt;
	bool	swappingEnabled;

}



void
ge_frame_Reset()
{
	NV_COMPILE_TIME_ASSERT( SCE_DISPLAY_PIXEL_RGB565   == SCE_GE_FPF_5650 );
	NV_COMPILE_TIME_ASSERT( SCE_DISPLAY_PIXEL_RGBA5551 == SCE_GE_FPF_5551 );
	NV_COMPILE_TIME_ASSERT( SCE_DISPLAY_PIXEL_RGBA4444 == SCE_GE_FPF_4444 );
	NV_COMPILE_TIME_ASSERT( SCE_DISPLAY_PIXEL_RGBA8888 == SCE_GE_FPF_8888 );

	uint32 vmode, frontW, frontH, backW, backH;
	core::GetParameter( nv::core::PR_ALG_VMODE,		&vmode	);
	core::GetParameter( nv::core::PR_FRONT_FRAME_W,	&frontW	);
	core::GetParameter( nv::core::PR_FRONT_FRAME_H,	&frontH	);
	core::GetParameter( nv::core::PR_BACK_FRAME_W,	&backW	);
	core::GetParameter( nv::core::PR_BACK_FRAME_H,	&backH	);

	// force swapping ?
	swapCpt			= 0;
	swappingEnabled = FALSE;
	if( vmode & ge::VM_SWAPPING ) {
		swappingEnabled = TRUE;
		backW  = frontW = Min( backW, frontW );		// Minimal front & back !
		backH  = frontH = Min( backH, frontH );
		vmode &= ~ge::VM_PSM16;						// Always 32BPP !
		core::SetParameter( nv::core::PR_ALG_VMODE,		vmode	);
		core::SetParameter( nv::core::PR_FRONT_FRAME_W,	frontW	);
		core::SetParameter( nv::core::PR_FRONT_FRAME_H,	frontH	);
		core::SetParameter( nv::core::PR_BACK_FRAME_W,	backW	);
		core::SetParameter( nv::core::PR_BACK_FRAME_H,	backH	);
	}

	//
	// vram mapping

	uint fw		= Round64( frontW );
	uint fbs	= fw * frontH * ((vmode&ge::VM_PSM16)?2:4);
	uint bw		= Round64( backW );
	uint bbs	= bw * backH * 4;
	uint dw		= Round64( backW );
	uint dbs	= dw * backH * 2;		// always uint16 !

	ge::frame::PhysicalSizeX	= frontW;
	ge::frame::PhysicalSizeY	= frontH;
	ge::frame::VirtualSizeX		= frontW;
	ge::frame::VirtualSizeY		= frontH;
	ge::frame::SizeX			= backW;
	ge::frame::SizeY			= backH;
	ge::frame::SizeX2			= GetCeilPow2( ge::frame::SizeX );
	ge::frame::SizeY2			= GetCeilPow2( ge::frame::SizeY );
	ge::frame::OffsetX			= (2048-backW/2)<<4;
	ge::frame::OffsetY			= (2048-backH/2)<<4;
	ge::frame::FrontAddr		= 0;
	ge::frame::FrontWidth		= fw;
	ge::frame::FrontBSize		= fbs;
	ge::frame::FrontPSM			= (vmode&ge::VM_PSM16) ? SCE_GE_FPF_5551 : SCE_GE_FPF_8888;		// 16BPP 5551 is prefered to 565 to reduce green hue on special effects like smoke ...
	ge::frame::BackAddr			= ge::frame::FrontAddr + RoundX( fbs, 8*1024 );	// 8Ko alignement
	ge::frame::BackWidth		= bw;
	ge::frame::BackBSize		= bbs;
	ge::frame::BackPSM			= SCE_GE_FPF_8888;
	ge::frame::DepthAddr		= ge::frame::BackAddr + RoundX( bbs, 8*1024 );	// 8Ko alignement
	ge::frame::DepthWidth		= dw;
	ge::frame::DepthBSize		= dbs;
	ge::frame::VRamTopAddr		= 0;
	ge::frame::VRamLowerAddr	= ge::frame::DepthAddr + RoundX( dbs, 8*1024 );	// 8Ko alignement
	ge::frame::VRamUpperAddr	= ge::GetVRamBSize();
	NV_ASSERT( ge::frame::VRamLowerAddr < ge::frame::VRamUpperAddr );

	//
	// Default context

	Zero( ge::frame::DefContext );
	ge::frame::DefContext.cmode			= SCE_GE_SET_CMODE( 0, 1, 1, 1 );		// Normal mode
	ge::frame::DefContext.vtype			= SCE_GE_SET_VTYPE( 1, 5, 0, 2, 0, 0, 0, 0, 0 );
	ge::frame::DefContext.region[0]		= SCE_GE_SET_REGION1( 0, 0 );
	ge::frame::DefContext.region[1]		= SCE_GE_SET_REGION2( backW-1, backH-1 );
	ge::frame::DefContext.scissor[0]	= SCE_GE_SET_SCISSOR1( 0, 0 );
	ge::frame::DefContext.scissor[1]	= SCE_GE_SET_SCISSOR2( backW-1, backH-1 );
	ge::frame::DefContext.offsetxy[0]	= SCE_GE_SET_OFFSETX( ge::frame::OffsetX );
	ge::frame::DefContext.offsetxy[1]	= SCE_GE_SET_OFFSETY( ge::frame::OffsetY );
	for( int i = 0 ; i < 16 ; i++ )
		ge::frame::DefContext.projd[i]	= SCE_GE_SET_PROJD( ExtFp24(0.f) );
	ge::frame::DefContext.projd[0]		= SCE_GE_SET_PROJD( ExtFp24(1.f) );
	ge::frame::DefContext.projd[5]		= SCE_GE_SET_PROJD( ExtFp24(1.f) );
	ge::frame::DefContext.projd[10]		= SCE_GE_SET_PROJD( ExtFp24(1.f) );
	ge::frame::DefContext.projd[15]		= SCE_GE_SET_PROJD( ExtFp24(1.f) );
	ge::frame::DefContext.sxyz[0]		= SCE_GE_SET_SX( ExtFp24(2048.f) );
	ge::frame::DefContext.sxyz[1]		= SCE_GE_SET_SY( ExtFp24(-2048.f) );
	ge::frame::DefContext.sxyz[2]		= SCE_GE_SET_SZ( ExtFp24(1.f) );
	ge::frame::DefContext.txyz[0]		= SCE_GE_SET_TX( ExtFp24(2048.f) );
	ge::frame::DefContext.txyz[1]		= SCE_GE_SET_TY( ExtFp24(2048.f) );
	ge::frame::DefContext.txyz[2]		= SCE_GE_SET_TZ( ExtFp24(0.f) );
	ge::frame::DefContext.fbp			= SCE_GE_SET_FBP_ADDR24( ge::frame::BackAddr );
	ge::frame::DefContext.fbw			= SCE_GE_SET_FBW_BASE8( ge::frame::BackWidth, ge::frame::BackAddr );
	ge::frame::DefContext.fpf			= SCE_GE_SET_FPF( ge::frame::BackPSM );
	ge::frame::DefContext.zbp			= SCE_GE_SET_ZBP_ADDR24( ge::frame::DepthAddr );
	ge::frame::DefContext.zbw			= SCE_GE_SET_ZBW_BASE8( ge::frame::DepthWidth, ge::frame::DepthAddr );
	ge::frame::DefContext.minmaxz[0]	= SCE_GE_SET_MINZ( 0 );
	ge::frame::DefContext.minmaxz[1]	= SCE_GE_SET_MAXZ( 0xFFFF );
	ge::frame::DefContext.cull			= SCE_GE_SET_CULL( SCE_GE_CULL_CW );
	ge::frame::DefContext.shade			= SCE_GE_SET_SHADE( 1 );				// gouraud
	ge::frame::DefContext.cref			= SCE_GE_SET_CREF_RGB24( 0xFF00FF );	// magenta is colorkey !
	ge::frame::DefContext.ctest			= SCE_GE_SET_CTEST( SCE_GE_CTEST_NOTEQUAL );
	ge::frame::DefContext.cmsk			= SCE_GE_SET_CMSK_RGB24( 0xFFFFFF );
	ge::frame::DefContext.atest			= SCE_GE_SET_ATEST( SCE_GE_ATEST_GREATER, 0x80, 0xFF );
	ge::frame::DefContext.stest			= SCE_GE_SET_STEST( SCE_GE_STEST_ALWAYS, 0x00, 0xFF );
	ge::frame::DefContext.ztest			= SCE_GE_SET_ZTEST( SCE_GE_ZTEST_GEQUAL );
	ge::frame::DefContext.zmsk			= SCE_GE_SET_ZMSK( 0 );
	ge::frame::DefContext.sop			= SCE_GE_SET_SOP( SCE_GE_SOP_KEEP, SCE_GE_SOP_KEEP, SCE_GE_SOP_KEEP );
	ge::frame::DefContext.lop			= SCE_GE_SET_LOP( SCE_GE_LOP_COPY );
	ge::frame::DefContext.blend			= SCE_GE_SET_BLEND( SCE_GE_BLEND_A_AS, SCE_GE_BLEND_B_255_AS, SCE_GE_BLEND_CSA_PLUS_CDB );	// normal blend !
	ge::frame::DefContext.fix[0]		= SCE_GE_SET_FIXA_RGB24( 0xFFFFFF );
	ge::frame::DefContext.fix[1]		= SCE_GE_SET_FIXB_RGB24( 0xFFFFFF );
	ge::frame::DefContext.dith[0]		= SCE_GE_SET_DITH1( 4, 2, 5, 3 );
	ge::frame::DefContext.dith[1]		= SCE_GE_SET_DITH2( 0, 6, 1, 7 );
	ge::frame::DefContext.dith[2]		= SCE_GE_SET_DITH3( 5, 3, 4, 2 );
	ge::frame::DefContext.dith[3]		= SCE_GE_SET_DITH4( 1, 7, 0, 6 );
	ge::frame::DefContext.pmsk[0]		= SCE_GE_SET_PMSK1_RGB24( 0x000000 );
	ge::frame::DefContext.pmsk[1]		= SCE_GE_SET_PMSK2( 0x00 );
	ge::frame::DefContext.lte			= SCE_GE_SET_LTE( 0 );		// light off
	ge::frame::DefContext.cle			= SCE_GE_SET_CLE( 0 );		// clipping off
	ge::frame::DefContext.bce			= SCE_GE_SET_BCE( 1 );		// cull on
	ge::frame::DefContext.tme			= SCE_GE_SET_TME( 0 );		// tex off
	ge::frame::DefContext.fge			= SCE_GE_SET_FGE( 0 );		// fog off
	ge::frame::DefContext.dte			= SCE_GE_SET_DTE( 0 );		// dith off
	ge::frame::DefContext.abe			= SCE_GE_SET_ABE( 1 );		// blend on
	ge::frame::DefContext.ate			= SCE_GE_SET_ATE( 1 );		// a-test on
	ge::frame::DefContext.zte			= SCE_GE_SET_ZTE( 1 );		// z-test on
	ge::frame::DefContext.ste			= SCE_GE_SET_STE( 1 );		// s-test on
	ge::frame::DefContext.aae			= SCE_GE_SET_AAE( 0 );		// aa off
	ge::frame::DefContext.cte			= SCE_GE_SET_CTE( 1 );		// c-test on
	ge::frame::DefContext.loe			= SCE_GE_SET_LOE( 1 );		// log-op. on
	ge::frame::DefContext.nrev			= SCE_GE_SET_NREV( 0 );		// normal direct
	ge::frame::DefContext.pce			= SCE_GE_SET_PCE( 1 );		// patch cull on
	ge::frame::DefContext.pface			= SCE_GE_SET_PFACE( SCE_GE_PFACE_CW );
	ge::frame::DefContext.suv[0]		= SCE_GE_SET_SU( ExtFp24(1.f) );
	ge::frame::DefContext.suv[1]		= SCE_GE_SET_SV( ExtFp24(1.f) );
	ge::frame::DefContext.tuv[0]		= SCE_GE_SET_TU( ExtFp24(0.f) );
	ge::frame::DefContext.tuv[1]		= SCE_GE_SET_TV( ExtFp24(0.f) );
	ge::frame::DefContext.tfunc			= SCE_GE_SET_TFUNC( SCE_GE_TFUNC_MODULATE, 1, 0 );	// Modulate / RGBA
	ge::frame::DefContext.tlevel		= SCE_GE_SET_TLEVEL( SCE_GE_TLEVEL_CONSTANT, 0<<4 );
	ge::frame::DefContext.tmap			= SCE_GE_SET_TMAP( SCE_GE_TMAP_TMN_UV, 0 );
	ge::frame::DefContext.tmode			= SCE_GE_SET_TMODE( SCE_GE_TMODE_HSM_HIGHSPEED, 0, 0 );		// cache swizzled !
	ge::frame::DefContext.tfilter		= SCE_GE_SET_TFILTER( SCE_GE_TFILTER_LINEAR, SCE_GE_TFILTER_LINEAR );	// linear / linear
	ge::frame::DefContext.twrap			= SCE_GE_SET_TWRAP( SCE_GE_TWRAP_REPEAT, SCE_GE_TWRAP_REPEAT );	// repeat / repeat
	ge::frame::DefContext.tslope		= SCE_GE_SET_TSLOPE( ExtFp24(0.f) );		// ??
	ge::frame::DefContext.tec			= SCE_GE_SET_TEC_RGB24( 0xFFFFFF );
	ge::frame::DefContext.fog[0]		= SCE_GE_SET_FOG1( ExtFp24(0.f) );
	ge::frame::DefContext.fog[1]		= SCE_GE_SET_FOG2( ExtFp24(0.f) );
	ge::frame::DefContext.fc			= SCE_GE_SET_FC_RGB24( 0xFFFFFF );
}


void
ge_frame_Init	(	)
{
	ge_frame_Reset();
}


void
ge_frame_Shut	(	)
{
	//
}


bool
ge::frame::IsSwapMode			(			)
{
	return swappingEnabled;
}


bool
ge::frame::IsSwapCompliant		(			)
{
	return	(PhysicalSizeX	== SizeX )
		&&	(PhysicalSizeY	== SizeY )
		&&	(FrontWidth		== BackWidth )
		&&	(FrontPSM		== BackPSM );
}


void
ge::frame::Swap	(		)
{
	if( IsSwapCompliant() )
	{
		// Fast swap
		::Swap( FrontAddr,  BackAddr  );
		DefContext.fbp = SCE_GE_SET_FBP_ADDR24( BackAddr );
	}
	else
	{
		// Heavy swap
		::Swap( PhysicalSizeX, SizeX );
		::Swap( PhysicalSizeY, SizeY );
		VirtualSizeX	= PhysicalSizeX;
		VirtualSizeY	= PhysicalSizeY;
		SizeX2			= GetCeilPow2( SizeX );
		SizeY2			= GetCeilPow2( SizeY );
		OffsetX			= (2048-SizeX/2)<<4;
		OffsetY			= (2048-SizeY/2)<<4;
		::Swap( FrontAddr,  BackAddr  );
		::Swap( FrontWidth, BackWidth );
		::Swap( FrontBSize, BackBSize );
		::Swap( FrontPSM,   BackPSM   );
		DefContext.region[1]	= SCE_GE_SET_REGION2( SizeX-1, SizeY-1 );
		DefContext.scissor[1]	= SCE_GE_SET_SCISSOR2( SizeX-1, SizeY-1 );
		DefContext.offsetxy[0]	= SCE_GE_SET_OFFSETX( OffsetX );
		DefContext.offsetxy[1]	= SCE_GE_SET_OFFSETY( OffsetY );
		DefContext.fbp			= SCE_GE_SET_FBP_ADDR24( BackAddr );
		DefContext.fbw			= SCE_GE_SET_FBW_BASE8( BackWidth, BackAddr );
		DefContext.fpf			= SCE_GE_SET_FPF( BackPSM );
	}

	swapCpt++;
}


bool
ge::frame::IsOddFrame	(			)
{
	return (swapCpt&1) == 1;
}


bool
ge::frame::IsEvenFrame	(			)
{
	return (swapCpt&1) == 0;
}


void
ge::frame::PutBack_CH	(	dmac::Cursor*		inDC,
							pvoid				inData		)
{
	NV_ASSERT_DC( inDC );
	NV_ASSERT_A32( inData );
	//
}


void
ge::frame::PutBack		(	pvoid				inData		)
{
	NV_ASSERT_A32( inData );
	uint8* srcP = (uint8*) inData;
	uint8* dstP = (uint8*) GetVRamPointer(BackAddr);
	for( uint y = 0 ; y < SizeY ; y++ ) {
		libc::Memcpy( dstP, srcP, SizeX*4 );
		srcP += SizeX*4;
		dstP += BackWidth*4;
	}
}


void
ge::frame::GetBack		(	pvoid				inBuffer	)
{
	NV_ASSERT_A32( inBuffer );
	uint8* dstP = (uint8*) inBuffer;
	uint8* srcP = (uint8*) GetVRamPointer(BackAddr);
	for( uint y = 0 ; y < SizeY ; y++ ) {
		libc::Memcpy( dstP, srcP, SizeX*4 );
		srcP += BackWidth*4;
		dstP += SizeX*4;
	}
}


void
ge::frame::GetDepth		(	pvoid				inBuffer	)
{
	NV_ASSERT_A32( inBuffer );
	uint8* dstP  = (uint8*) inBuffer;
	uint8* srcP  = (uint8*) GetVRamPointer(DepthAddr);
		   srcP += (BackPSM == SCE_GE_FPF_8888) ? 0x600000 : 0x200000;	// depth buffer linear access !
	for( uint y = 0 ; y < SizeY ; y++ ) {
		libc::Memcpy( dstP, srcP, SizeX*2 );
		srcP += BackWidth*2;
		dstP += SizeX*2;
	}
}





namespace
{

	#define	PRESENT_STRIDE	16

	struct PresentVertex {
		uint16	u, v;
		uint16	x,y,z;
	};
	ALIGNED128( PresentVertex, PresentVB[(512/PRESENT_STRIDE)*2] );

	int InitPresentVB()
	{
		static bool done = FALSE;
		int nb = ge::frame::PhysicalSizeX / PRESENT_STRIDE;
		NV_ASSERT( (ge::frame::PhysicalSizeX%PRESENT_STRIDE) == 0 );
		if( !done ) {
			for( int i = 0 ; i < nb ; i++ ) {
				PresentVB[i*2+0].u	= (ge::frame::SizeX*(i+0))/nb;
				PresentVB[i*2+0].v	= 0;
				PresentVB[i*2+1].u	= (ge::frame::SizeX*(i+1))/nb;
				PresentVB[i*2+1].v	= ge::frame::SizeY;
				PresentVB[i*2+0].x	= i*PRESENT_STRIDE;
				PresentVB[i*2+0].y	= 0;
				PresentVB[i*2+0].z	= 0;
				PresentVB[i*2+1].x	= (i+1)*PRESENT_STRIDE;
				PresentVB[i*2+1].y	= ge::frame::PhysicalSizeY;
				PresentVB[i*2+1].z	= 0;
			}
			done = TRUE;
		}
		return nb*2;
	}



	#define	CLEAR_STRIDE	16
	#define	CLEAR_WIDTH		512
	#define	CLEAR_HEIGHT	512

	struct ClearVertex {
		uint16	x,y,z;
	};
	ALIGNED128( ClearVertex, ClearVB[(CLEAR_WIDTH/CLEAR_STRIDE)*2] );

	int InitClearVB()
	{
		static bool done = FALSE;
		int nb = CLEAR_WIDTH / CLEAR_STRIDE;
		NV_ASSERT( (CLEAR_WIDTH%CLEAR_STRIDE) == 0 );
		if( !done ) {
			for( int i = 0 ; i < nb ; i++ ) {
				ClearVB[i*2+0].x	= i*CLEAR_STRIDE;
				ClearVB[i*2+0].y	= 0;
				ClearVB[i*2+0].z	= 0;
				ClearVB[i*2+1].x	= (i+1)*CLEAR_STRIDE;
				ClearVB[i*2+1].y	= CLEAR_HEIGHT;
				ClearVB[i*2+1].z	= 0;
			}
			done = TRUE;
		}
		return nb*2;
	}

}


//
// Frame present

void
ge::frame::Present_CH	(	dmac::Cursor*		inDC		)
{
	NV_ASSERT_DC( inDC );

	int	   vbSize	   = InitPresentVB();
	uint32 texBackAddr = (uint32)GetVRamAddr( BackAddr );
	uint   tfilter	   = ( PhysicalSizeX==SizeX && PhysicalSizeY==SizeY )
					   ? SCE_GE_TFILTER_NEAREST
					   : SCE_GE_TFILTER_LINEAR;
	uint   dte		   = (FrontPSM == SCE_GE_FPF_8888) ? 0 : 1;		// dithering for 16BPP destination

	// dst is front
	*inDC->i32++ = SCE_GE_SET_ABE( 0 );
	*inDC->i32++ = SCE_GE_SET_ATE( 0 );
	*inDC->i32++ = SCE_GE_SET_ZTE( 0 );
	*inDC->i32++ = SCE_GE_SET_STE( 0 );
	*inDC->i32++ = SCE_GE_SET_CTE( 0 );
	*inDC->i32++ = SCE_GE_SET_LOE( 0 );
	*inDC->i32++ = SCE_GE_SET_DTE( dte );
	*inDC->i32++ = SCE_GE_SET_TME( 1 );
	*inDC->i32++ = SCE_GE_SET_ZMSK( 1 );
	*inDC->i32++ = SCE_GE_SET_SCISSOR1( 0, 0 );
	*inDC->i32++ = SCE_GE_SET_SCISSOR2( PhysicalSizeX-1, PhysicalSizeY-1 );
	*inDC->i32++ = SCE_GE_SET_FBP_ADDR24( FrontAddr );
	*inDC->i32++ = SCE_GE_SET_FBW_BASE8( FrontWidth, FrontAddr );
	*inDC->i32++ = SCE_GE_SET_FPF( FrontPSM );

	// src is back
	*inDC->i32++ = SCE_GE_SET_TFLUSH();
	*inDC->i32++ = SCE_GE_SET_TSYNC();
	*inDC->i32++ = SCE_GE_SET_TBP0_ADDR24( texBackAddr );
	*inDC->i32++ = SCE_GE_SET_TBW0_BASE8( BackWidth, texBackAddr );
	*inDC->i32++ = SCE_GE_SET_TSIZE0( SizeX2, SizeY2 );
	*inDC->i32++ = SCE_GE_SET_TPF( SCE_GE_TPF_8888, 0 );
	*inDC->i32++ = SCE_GE_SET_TMODE( SCE_GE_TMODE_HSM_NORMAL, 0, 0 );
	*inDC->i32++ = SCE_GE_SET_TFILTER( tfilter, tfilter );
	*inDC->i32++ = SCE_GE_SET_TWRAP( SCE_GE_TWRAP_CLAMP, SCE_GE_TWRAP_CLAMP );
	*inDC->i32++ = SCE_GE_SET_TFUNC( SCE_GE_TFUNC_REPLACE, 0, 0 );

	// rects
	*inDC->i32++ = SCE_GE_SET_BASE_BASE8( uint32(PresentVB) );
	*inDC->i32++ = SCE_GE_SET_VADR( uint32(PresentVB) );
	*inDC->i32++ = SCE_GE_SET_VTYPE( 2, 0, 0, 2, 0, 0, 0, 0, 1 );
	*inDC->i32++ = SCE_GE_SET_PRIM ( vbSize, 6 );

	// restore
	*inDC->i32++ = DefContext.abe;
	*inDC->i32++ = DefContext.ate;
	*inDC->i32++ = DefContext.zte;
	*inDC->i32++ = DefContext.ste;
	*inDC->i32++ = DefContext.cte;
	*inDC->i32++ = DefContext.loe;
	*inDC->i32++ = DefContext.dte;
	*inDC->i32++ = DefContext.tme;
	*inDC->i32++ = DefContext.zmsk;
	*inDC->i32++ = DefContext.scissor[0];
	*inDC->i32++ = DefContext.scissor[1];
	*inDC->i32++ = DefContext.fbp;
	*inDC->i32++ = DefContext.fbw;
	*inDC->i32++ = DefContext.fpf;

	NV_ASSERT_DC( inDC );
}



//
// Frame clear


void
ge::frame::ClearMod::Translate	(	uint32		inD		)
{
	AS_UINT32( _cm )	+= inD;
	AS_UINT32( _sciss )	+= inD;
	AS_UINT32( _color )	+= inD;
}

void
ge::frame::ClearMod::SetColor	(	uint32	inABGR	)
{
	NV_ASSERT_A32( _color );
	_color[0] = SCE_GE_SET_MAC_RGB24( inABGR );
	_color[1] = SCE_GE_SET_MAA( (inABGR>>24) );
}

void
ge::frame::ClearMod::SetRect(	int	inX, int inY, int inW, int inH )
{
	NV_ASSERT_A32( _sciss );
	int x0 = Clamp( inX, 0, int(SizeX) );
	int y0 = Clamp( inY, 0, int(SizeY) );
	int x1 = x0 + inW;	x1 = Min( x1, int(SizeX) );
	int y1 = y0 + inH;	y1 = Min( y1, int(SizeY) );
	_sciss[0] = SCE_GE_SET_SCISSOR1( x0, y0 );
	_sciss[1] = SCE_GE_SET_SCISSOR2( x1-1, y1-1 );
}

void
ge::frame::ClearMod::EnableColor	(	bool		inOnOff		)
{
	// update CMODE.CEN/AEN
	NV_ASSERT_A32( _cm );
	*_cm = InsBits( *_cm, inOnOff?3:0, 8, 2 );
}

void
ge::frame::ClearMod::EnableDepth	(	bool		inOnOff		)
{
	// update CMODE.ZEN
	NV_ASSERT_A32( _cm );
	*_cm = InsBits( *_cm, inOnOff?1:0, 10, 1 );
}

void
ge::frame::Clear_CH		(		dmac::Cursor*		inDC,
								ClearMod&			outMod		)
{
	NV_ASSERT_DC( inDC );

	int vbSize = InitClearVB();

	// clear mode
	outMod._cm   = inDC->i32;
	*inDC->i32++ = SCE_GE_SET_CMODE( 1, 1, 1, 1 );
	outMod._sciss= inDC->i32;
	*inDC->i32++ = DefContext.scissor[0];
	*inDC->i32++ = DefContext.scissor[1];

	// draw
	outMod._color = inDC->i32;
	*inDC->i32++ = SCE_GE_SET_MAC_RGB24( 0x000000 );	// Cp.rgb
	*inDC->i32++ = SCE_GE_SET_MAA( 0x00 );				// Cp.a
	*inDC->i32++ = SCE_GE_SET_BASE_BASE8( uint32(ClearVB) );
	*inDC->i32++ = SCE_GE_SET_VADR( uint32(ClearVB) );
	*inDC->i32++ = SCE_GE_SET_VTYPE( 0, 0, 0, 2, 0, 0, 0, 0, 1 );
	*inDC->i32++ = SCE_GE_SET_PRIM ( vbSize, 6 );

	// restore
	*inDC->i32++ = DefContext.scissor[0];
	*inDC->i32++ = DefContext.scissor[1];
	*inDC->i32++ = DefContext.cmode;

	NV_ASSERT_DC( inDC );
}




