/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/ALG/ALG.h>



void	ge_crtc_Init( bool inEnable );
void	ge_crtc_Reset( bool inEnable );
void	ge_crtc_Shut();
void	ge_frame_Init();
void	ge_frame_Reset();
void	ge_frame_Shut();




namespace
{

	void ClearAllVRam	(	uint32	inABGR	)
	{
		uint32* fp 	  = (uint32*) ge::GetVRamTop();
		uint32  bsize = ge::GetVRamBSize();
		while( bsize > 4 ) {
			*fp++  = inABGR;
			bsize -= 4;
		}
		SyncMemory();
	}


	uint32	vramBSize = 0;
	uint8*	vramTop_cached;
	uint8*	vramTop_uncached;


	void InitVRamTop()
	{
		if( !vramBSize ) {
			int intr = sceKernelCpuSuspendIntr();
			vramBSize			= sceGeEdramGetSize();
			vramTop_cached		= CachedPointer( (uint8*)sceGeEdramGetAddr() );
			vramTop_uncached	= UncachedPointer( (uint8*)sceGeEdramGetAddr() );
			sceKernelCpuResumeIntr( intr );
		}
	}
}



void
ge::Init(	bool	inEnableCRTC	)
{
	InitVRamTop();
	ge_frame_Init();
	ge_crtc_Init( inEnableCRTC );

/*	bool   consoleOnly	 = FALSE;
	uint32 resetVRam     = 0;
	uint32 resetVRamRGBA = 0;
	nv::core::GetParameter( nv::core::PR_RESET_ALLVRAM,		 &resetVRam		);
	nv::core::GetParameter( nv::core::PR_RESET_ALLVRAM_RGBA, &resetVRamRGBA	);
	nv::core::GetParameter( nv::core::PR_CONSOLE_ONLY,		 &consoleOnly	);
	if( resetVRam && !consoleOnly )
		ClearAllVRam( GetDpyColorRGBA(PSM_ABGR32,resetVRamRGBA) );
*/
}



void
ge::Reset(	bool	inEnableCRTC	)
{
	ge_frame_Reset();
	ge_crtc_Reset( inEnableCRTC );
}



void
ge::Shut	(	)
{
	ge_frame_Shut();
	ge_crtc_Shut();
}




bool
ge::IsBusy()
{
	// Check global geman activity
	int res = sceGeDrawSync( 1 );
	return (res != SCE_GE_LIST_COMPLETED);
}



void
ge::Sync()
{
	// Global geman sync
	sceGeDrawSync( 0 );
}


bool
ge::IsPaused	(			)
{
	// Global geman stalling ?
	int res = sceGeDrawSync( 1 );
	return (res == SCE_GE_LIST_STALLING);
}


void
ge::Pause	(		)
{
	// Global geman pause
	SceGeBreakParam brkParam;
	sceGeBreak( 0, &brkParam );
}


void
ge::Resume		(			)
{
	// Global geman continue
	if( IsPaused() )
		sceGeContinue();
}


uint32
ge::GetVRamBSize	(			)
{
	InitVRamTop();
	return vramBSize;
}


pvoid
ge::GetVRamTop	(			)
{
	InitVRamTop();
	return vramTop_uncached;
}


pvoid
ge::GetVRamPointer	(	uint32		inLocalAddr		)
{
	InitVRamTop();
	return (vramTop_uncached + inLocalAddr);
}


pvoid
ge::GetVRamAddr		(	uint32		inLocalAddr		)
{
	InitVRamTop();
	return (vramTop_cached + inLocalAddr);
}





void
ge::FlushContext_CH	(	dmac::Cursor*	inDC,
						const Context*	inCtxt,
						uint64			inRegistersMask,
						uint32**		outFBPCmdAddr		)
{
	NV_ASSERT_DC( inDC );
	if( !inCtxt )					return;
	if( inRegistersMask == 0 )		return;
	if( outFBPCmdAddr )
		*outFBPCmdAddr = NULL;

	if( inRegistersMask & CMODE )
		*inDC->i32++ = inCtxt->cmode;
	if( inRegistersMask & VTYPE )
		*inDC->i32++ = inCtxt->vtype;
	if( inRegistersMask & REGION ) {
		inDC->i32[0] = inCtxt->region[0];
		inDC->i32[1] = inCtxt->region[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & SCISSOR ) {
		inDC->i32[0] = inCtxt->scissor[0];
		inDC->i32[1] = inCtxt->scissor[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & OFFSETXY ) {
		inDC->i32[0] = inCtxt->offsetxy[0];
		inDC->i32[1] = inCtxt->offsetxy[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & PROJD ) {
		inDC->i32[0] = SCE_GE_SET_PROJN( 0 );
		for( int i = 0 ; i < 16 ; i++ )
			inDC->i32[i+1] = inCtxt->projd[i];
		inDC->i32 += 17;
	}
	if( inRegistersMask & SXYZ ) {
		inDC->i32[0] = inCtxt->sxyz[0];
		inDC->i32[1] = inCtxt->sxyz[1];
		inDC->i32[2] = inCtxt->sxyz[2];
		inDC->i32 += 3;
	}
	if( inRegistersMask & TXYZ ) {
		inDC->i32[0] = inCtxt->txyz[0];
		inDC->i32[1] = inCtxt->txyz[1];
		inDC->i32[2] = inCtxt->txyz[2];
		inDC->i32 += 3;
	}
	if( inRegistersMask & FBP ) {
		if( outFBPCmdAddr )
			*outFBPCmdAddr = inDC->i32;
		*inDC->i32++ = inCtxt->fbp;
	}
	if( inRegistersMask & FBW )
		*inDC->i32++ = inCtxt->fbw;
	if( inRegistersMask & FPF )
		*inDC->i32++ = inCtxt->fpf;
	if( inRegistersMask & ZBP )
		*inDC->i32++ = inCtxt->zbp;
	if( inRegistersMask & ZBW )
		*inDC->i32++ = inCtxt->zbw;
	if( inRegistersMask & MINMAXZ ) {
		inDC->i32[0] = inCtxt->minmaxz[0];
		inDC->i32[1] = inCtxt->minmaxz[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & CULL )
		*inDC->i32++ = inCtxt->cull;
	if( inRegistersMask & SHADE )
		*inDC->i32++ = inCtxt->shade;
	if( inRegistersMask & CREF )
		*inDC->i32++ = inCtxt->cref;
	if( inRegistersMask & CTEST )
		*inDC->i32++ = inCtxt->ctest;
	if( inRegistersMask & CMSK )
		*inDC->i32++ = inCtxt->cmsk;
	if( inRegistersMask & ATEST )
		*inDC->i32++ = inCtxt->atest;
	if( inRegistersMask & STEST )
		*inDC->i32++ = inCtxt->stest;
	if( inRegistersMask & ZTEST )
		*inDC->i32++ = inCtxt->ztest;
	if( inRegistersMask & ZMSK )
		*inDC->i32++ = inCtxt->zmsk;
	if( inRegistersMask & SOP )
		*inDC->i32++ = inCtxt->sop;
	if( inRegistersMask & LOP )
		*inDC->i32++ = inCtxt->lop;
	if( inRegistersMask & BLEND )
		*inDC->i32++ = inCtxt->blend;
	if( inRegistersMask & FIX ) {
		inDC->i32[0] = inCtxt->fix[0];
		inDC->i32[1] = inCtxt->fix[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & DITH ) {
		inDC->i32[0] = inCtxt->dith[0];
		inDC->i32[1] = inCtxt->dith[1];
		inDC->i32[2] = inCtxt->dith[2];
		inDC->i32[3] = inCtxt->dith[3];
		inDC->i32 += 4;
	}
	if( inRegistersMask & PMSK ) {
		inDC->i32[0] = inCtxt->pmsk[0];
		inDC->i32[1] = inCtxt->pmsk[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & LTE )
		*inDC->i32++ = inCtxt->lte;
	if( inRegistersMask & CLE )
		*inDC->i32++ = inCtxt->cle;
	if( inRegistersMask & BCE )
		*inDC->i32++ = inCtxt->bce;
	if( inRegistersMask & TME )
		*inDC->i32++ = inCtxt->tme;
	if( inRegistersMask & FGE )
		*inDC->i32++ = inCtxt->fge;
	if( inRegistersMask & DTE )
		*inDC->i32++ = inCtxt->dte;
	if( inRegistersMask & ABE )
		*inDC->i32++ = inCtxt->abe;
	if( inRegistersMask & ATE )
		*inDC->i32++ = inCtxt->ate;
	if( inRegistersMask & ZTE )
		*inDC->i32++ = inCtxt->zte;
	if( inRegistersMask & STE )
		*inDC->i32++ = inCtxt->ste;
	if( inRegistersMask & AAE )
		*inDC->i32++ = inCtxt->aae;
	if( inRegistersMask & PCE )
		*inDC->i32++ = inCtxt->pce;
	if( inRegistersMask & CTE )
		*inDC->i32++ = inCtxt->cte;
	if( inRegistersMask & LOE )
		*inDC->i32++ = inCtxt->loe;
	if( inRegistersMask & NREV )
		*inDC->i32++ = inCtxt->nrev;
	if( inRegistersMask & PFACE )
		*inDC->i32++ = inCtxt->pface;
	if( inRegistersMask & SUV ) {
		inDC->i32[0] = inCtxt->suv[0];
		inDC->i32[1] = inCtxt->suv[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & TUV ) {
		inDC->i32[0] = inCtxt->tuv[0];
		inDC->i32[1] = inCtxt->tuv[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & TLEVEL )
		*inDC->i32++ = inCtxt->tlevel;
	if( inRegistersMask & TFUNC )
		*inDC->i32++ = inCtxt->tfunc;
	if( inRegistersMask & TMAP )
		*inDC->i32++ = inCtxt->tmap;
	if( inRegistersMask & TMODE )
		*inDC->i32++ = inCtxt->tmode;
	if( inRegistersMask & TFILTER )
		*inDC->i32++ = inCtxt->tfilter;
	if( inRegistersMask & TWRAP )
		*inDC->i32++ = inCtxt->twrap;
	if( inRegistersMask & TSLOPE )
		*inDC->i32++ = inCtxt->tslope;
	if( inRegistersMask & TEC )
		*inDC->i32++ = inCtxt->tec;
	if( inRegistersMask & FOG ) {
		inDC->i32[0] = inCtxt->fog[0];
		inDC->i32[1] = inCtxt->fog[1];
		inDC->i32 += 2;
	}
	if( inRegistersMask & FC )
		*inDC->i32++ = inCtxt->fc;

	// reset base & offset
	*inDC->i32++ = SCE_GE_SET_BASE( 0 );
	*inDC->i32++ = SCE_GE_SET_OFFSET( 0 );


	NV_ASSERT_DC( inDC );
}


void
ge::TransferToVRam_CH	(	dmac::Cursor*	inDC		)
{
	NV_ASSERT_DC( inDC );
	//
}


void
ge::TransferFromVRam_CH	(	dmac::Cursor*	inDC		)
{
	NV_ASSERT_DC( inDC );
	//
}




uint32
ge::PackSFloat	(	uint		inType,
					float		inV,
					float		inS,
					float		inT			)
{
	union {
		int8	s8 [4];
		int16	s16[2];
		float	f32;
		uint32	i32;
	};
	i32 = 0;
	float v = inV * inS + inT;
	if( inType ==SCE_GE_VTYPE_CHAR ) {
		// in [-1,+1]
		NV_ASSERT( v>=-1.f && v <= 1.f );
		int sv = Clamp( int(v*128.f), -128, +127 );
		s8[0] = sv;
	} else if( inType ==SCE_GE_VTYPE_SHORT ) {
		// in [-1,+1]
		NV_ASSERT( v>=-1.f && v <= 1.f );
		int sv = Clamp( int(v*32768.f), -32768, +32767 );
		s16[0] = sv;
	} else if( inType ==SCE_GE_VTYPE_FLOAT ) {
		f32 = v;
	} else {
		NV_ERROR( "Unsupported format !" );
	}
	return i32;
}


uint32
ge::PackUFloat	(	uint		inType,
					float		inV,
					float		inS,
					float		inT			)
{
	union {
		uint8	i8 [4];
		uint16	i16[2];
		float	f32;
		uint32	i32;
	};
	i32 = 0;
	float v = inV * inS + inT;
	if( inType ==SCE_GE_VTYPE_UCHAR ) {
		// in [0,2]
		NV_ASSERT( v>=0.f && v <= 2.f );
		int sv = Clamp( int(v*128.f), +0, +255 );
		i8[0] = sv;
	} else if( inType ==SCE_GE_VTYPE_USHORT ) {
		// in [0,2]
		NV_ASSERT( v>=0.f && v <= 2.f );
		int sv = Clamp( int(v*32768.f), +0, +65535 );
		i16[0] = sv;
	} else if( inType ==SCE_GE_VTYPE_FLOAT ) {
		f32 = v;
	} else {
		NV_ERROR( "Unsupported format !" );
	}
	return i32;
}


uint32
ge::PackColor	(	uint		inType,
					uint32		inRGBA	)
{
	uint r = (inRGBA>>24) & 0xFF;
	uint g = (inRGBA>>16) & 0xFF;
	uint b = (inRGBA>> 8) & 0xFF;
	uint a = (inRGBA>> 0) & 0xFF;
	// psp is abrg psm !
	if( inType == SCE_GE_VTYPE_CT5650 )
		return ((b>>3)<<11) | ((g>>2)<<5) | ((r>>3)<<0);	// expanded with a=255 !
	else if( inType == SCE_GE_VTYPE_CT5551 )
		return ((a>>7)<<15) | ((b>>3)<<10) | ((g>>3)<<5) | ((r>>3)<<0);
	else if( inType == SCE_GE_VTYPE_CT4444 )
		return ((a>>4)<<12) | ((b>>4)<<8) | ((g>>4)<<4) | ((r>>4)<<0);
	else if( inType == SCE_GE_VTYPE_CT8888 )
		return (a<<24) | (b<<16) | (g<<8) | (r<<0);
	else {
		NV_ERROR( "Unsupported format !" );
		return 0;
	}
}


uint
ge::GetVTypeBSize	(	uint		inType	)
{
	if( inType < SCE_GE_VTYPE_FLOAT )
		return inType;	// 0, 1, 2
	if( inType==SCE_GE_VTYPE_FLOAT || inType==SCE_GE_VTYPE_CT8888 )
		return 4;
	NV_ASSERT( inType>=SCE_GE_VTYPE_CT5650 && inType<=SCE_GE_VTYPE_CT4444 );
	return 2;
}


void
ge::VertexMap::Init	(	uint32		inVType		)
{
	Memset( this, 0, sizeof(VertexMap) );
	vtype  = inVType;
	cba[0] = GetVTypeBSize( GET_VTYPE_WT(inVType) );
	cba[1] = GetVTypeBSize( GET_VTYPE_TT(inVType) );
	cba[2] = GetVTypeBSize( GET_VTYPE_CT(inVType) );
	cba[3] = GetVTypeBSize( GET_VTYPE_NT(inVType) );
	cba[4] = GetVTypeBSize( GET_VTYPE_VT(inVType) );
	cbs[0] = cba[0] * (GET_VTYPE_WC(inVType)+1);	// .WT * .WC
	cbs[1] = cba[1] * 2;
	cbs[2] = cba[2] * 1;
	cbs[3] = cba[3] * 3;
	cbs[4] = cba[4] * 3;
	uint cur_cbo = 0;
	uint max_cba = 0;
	for( int i = 0 ; i < 5 ; i++ ) {
		if( !cbs[i] )		continue;
		max_cba = Max( uint(max_cba), uint(cba[i]) );
		cbo[i]	= RoundX( cur_cbo, cba[i] );
		cur_cbo	= cbo[i] + cbs[i];
	}
	NV_ASSERT( max_cba );
	bsize = RoundX( cur_cbo, max_cba );
	padbs = bsize - cur_cbo;
	NV_ASSERT( bsize <= 68 );
}


uint
ge::VertexMap::GetComponentType	(	Component	inC	)
{
	NV_ASSERT( HasComponent(inC) );	// component must be defined !
	if( inC == WEIGHT )	return GET_VTYPE_WT( vtype );
	if( inC == ST )		return GET_VTYPE_TT( vtype );
	if( inC == COLOR )	return GET_VTYPE_CT( vtype );
	if( inC == NXYZ )	return GET_VTYPE_NT( vtype );
	if( inC == XYZ )	return GET_VTYPE_VT( vtype );
	NV_ERROR( "What's ?" );
	return 0;
}


void
ge::VertexBuffer::Init	(	uint32			inVType,
							pvoid			inBase,
							uint			inSize,
							float			inLocS,
							const Vec3&		inLocT,
							float			inTexS,
							const Vec2&		inTexT	)
{
	NV_ASSERT_A32( inBase );
	NV_ASSERT( inSize );
	NV_ASSERT( inVType );
	base	= uint32(inBase);
	size	= inSize;
	locS	= inLocS;
	locT	= inLocT;
	texS	= inTexS;
	texT	= inTexT;
	vmap.Init( inVType );
}


void*
ge::VertexBuffer::SetValue	(	uint					inVIndex,
								uint					inUIndex,
								VertexMap::Component	inC,
								uint32					inV		)
{
	union {
		uint8*		pi8;
		uint16*		pi16;
		uint32*		pi32;
	};
	NV_ASSERT( HasComponent(inC) );
	if( !HasComponent(inC) )
		return NULL;
	NV_ASSERT( (inC!=VertexMap::WEIGHT) || (inUIndex<GetNbWeight()) );
	     pi8 = vmap.GetComponentPointer( At(inVIndex), inC );
	uint ubs = GetVTypeBSize( vmap.GetComponentType(inC) );
	if( ubs == 1 ) {
		pi8[inUIndex] = inV;
		return &pi8[inUIndex];
	} else if( ubs == 2 ) {
		pi16[inUIndex] = inV;
		return &pi16[inUIndex];
	} else {
		pi32[inUIndex] = inV;
		return &pi32[inUIndex];
	}
}


void*
ge::VertexBuffer::SetWeight	(	uint			inVIndex,
								uint			inWIndex,
								float			inW			)
{
	VertexMap::Component comp = VertexMap::WEIGHT;
	NV_ASSERT( HasComponent(comp) );
	uint typ = vmap.GetComponentType( comp );
	uint pkw = PackUFloat( typ, inW );
	return SetValue( inVIndex, inWIndex, comp, pkw );
}


void*
ge::VertexBuffer::SetTex		(	uint			inVIndex,
									const Vec2&		inST		)
{
	VertexMap::Component comp = VertexMap::ST;
	NV_ASSERT( HasComponent(comp) );
	uint typ = vmap.GetComponentType( comp );
	uint pks = PackUFloat( typ, inST.x, texS, texT.x );
	uint pkt = PackUFloat( typ, inST.y, texS, texT.y );
	void* storedP = SetValue( inVIndex, 0, comp, pks );
					SetValue( inVIndex, 1, comp, pkt );
	return storedP;
}


void*
ge::VertexBuffer::SetColor	(	uint			inVIndex,
								uint32			inRGBA		)
{
	VertexMap::Component comp = VertexMap::COLOR;
	NV_ASSERT( HasComponent(comp) );
	uint typ = vmap.GetComponentType( comp );
	uint pkc = PackColor( typ, inRGBA );
	return SetValue( inVIndex, 0, comp, pkc );
}


void*
ge::VertexBuffer::SetNormal	(	uint			inVIndex,
								const Vec3&		inNXYZ		)
{
	VertexMap::Component comp = VertexMap::NXYZ;
	NV_ASSERT( HasComponent(comp) );
	uint typ = vmap.GetComponentType( comp );
	uint pkx = PackSFloat( typ, inNXYZ.x );
	uint pky = PackSFloat( typ, inNXYZ.y );
	uint pkz = PackSFloat( typ, inNXYZ.z );
	void* storedP = SetValue( inVIndex, 0, comp, pkx );
					SetValue( inVIndex, 1, comp, pky );
					SetValue( inVIndex, 2, comp, pkz );
	return storedP;
}


void*
ge::VertexBuffer::SetLocation	(	uint			inVIndex,
									const Vec3&		inXYZ		)
{
	VertexMap::Component comp = VertexMap::XYZ;
	NV_ASSERT( HasComponent(comp) );
	uint typ = vmap.GetComponentType( comp );
	uint pkx = PackSFloat( typ, inXYZ.x, locS, locT.x );
	uint pky = PackSFloat( typ, inXYZ.y, locS, locT.y );
	uint pkz = PackSFloat( typ, inXYZ.z, locS, locT.z );
	void* storedP = SetValue( inVIndex, 0, comp, pkx );
					SetValue( inVIndex, 1, comp, pky );
					SetValue( inVIndex, 2, comp, pkz );
	return storedP;
}




