/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/ALG/ALG.h>
using namespace nv;


//#define	ENABLE_SEMA_SYNC

#ifdef _NVCOMP_ENABLE_DBG
#define	CHECK_DMA_TIMEOUT
#endif


namespace
{

	dmac::Cursor		dcBuff[2];
	dmac::Cursor*		frontDC;
	dmac::Cursor*		backDC;

	int					geman_intrid;
	int					geman_qid;
	volatile uint32		dmaBeginHC, dmaEndHC;
	volatile uint16		markId;
	volatile uint16		markCpt;
	#ifdef ENABLE_SEMA_SYNC
	SceUID				markSema;
	volatile uint16		markSemaPrevId;
	volatile uint16		markSemaWaitId;
	#endif


	#if SCE_DEVKIT_VERSION < 0x02060000
	#define	GE_CB_HANDLER_PARAMS	void*
	#else
	#define	GE_CB_HANDLER_PARAMS	void*, const void *
	#endif


	void geSignalHandler( int intrcode, GE_CB_HANDLER_PARAMS )
	{
		if( markCpt==0 )
			dmaBeginHC = sceDisplayGetAccumulatedHcount();
		markCpt++;
		markId = intrcode&0xFFFF;
		#ifdef ENABLE_SEMA_SYNC
		if( markId >= markSemaWaitId ) {
			uint16 d_markId = markId - markSemaPrevId;
			sceKernelSignalSema( markSema, d_markId );		// inc sema currentCount with *n* marks !
			markSemaPrevId = markId;
			markSemaWaitId = 0xFFFF;
		}
		#endif
	}


	void geFinishHandler( int intrcode, GE_CB_HANDLER_PARAMS )
	{
		dmaEndHC = sceDisplayGetAccumulatedHcount();
		markId = 0xFFFF;
		#ifdef ENABLE_SEMA_SYNC
		sceKernelSignalSema( markSema, 0xFFFF );			// set sema currentCount to max 0xFFFF !
		#endif
	}


	void geman_InstHandler (	)
	{
		if( geman_intrid < 0 ) {
			SceGeCbParam cbParam;
			Zero( cbParam );
			cbParam.pSignalFunc   = geSignalHandler;
			cbParam.pFinishFunc   = geFinishHandler;
			geman_intrid = sceGeSetCallback( &cbParam );
			NV_ASSERT( geman_intrid >= 0 );
			#ifdef ENABLE_SEMA_SYNC
			markSema = sceKernelCreateSema( "NvFrameMarkSema", SCE_KERNEL_SA_THFIFO, 0xFFFF, 0xFFFF, NULL );
			NV_ASSERT( markSema > 0 );
			#endif
		}
	}


	void geman_RemoveHandler (	)
	{
		if( geman_intrid >= 0 ) {
			sceGeUnsetCallback( geman_intrid );
			geman_intrid = -1;
			#ifdef ENABLE_SEMA_SYNC
			sceKernelDeleteSema( markSema );
			markSema = 0;
			#endif
		}
	}


}


void
dmac::Init()
{
	// DMA Buffer
	uint32 dmaHeaps;
	bool   dmaUncached;
	core::GetParameter( nv::core::PR_ALG_DMAC_HEAPS_BSIZE,   &dmaHeaps );
	core::GetParameter( nv::core::PR_ALG_DMAC_HEAPS_UNCACHE, &dmaUncached );

	// DMA Heap : 64bytes D$ line alignment to prevent
	// concurrent cached & uncached memory access !
	uint32 dmaHeap = (dmaHeaps>>1) & 0xFFFFFFF0;
	void * dmaBuff0 = EngineMallocA( dmaHeap, 64 );
	void * dmaBuff1 = EngineMallocA( dmaHeap, 64 );

	dcBuff[0].i8	= dcBuff[0].start = (uint8*)( dmaUncached ? UncachedPointer(dmaBuff0) : CachedPointer(dmaBuff0) );
	dcBuff[0].bsize = dmaHeap;

	dcBuff[1].i8	= dcBuff[1].start = (uint8*)( dmaUncached ? UncachedPointer(dmaBuff1) : CachedPointer(dmaBuff1) );
	dcBuff[1].bsize = dmaHeap;

	Printf( "<Neova> DMAC heaps: MODE=%s BSIZE=2*%dKo\n",
			dmaUncached ? "D$-uncached" : "D$-cached",
			(dmaHeap>>10)	);
	Printf( "<Neova> DMAC heaps: @0=0x%08x @1=0x%08x\n",
			uint32(dmaBuff0),
			uint32(dmaBuff1)	);
	NV_ASSERT( dmaBuff0 );
	NV_ASSERT( dmaBuff1 );
	FlushDCache();

	geman_intrid = -1;
	#ifdef ENABLE_SEMA_SYNC
	markSema     = 0;
	#endif
	Reset();
}



void
dmac::Reset()
{
	geman_RemoveHandler();
	geman_InstHandler();

	frontDC		= &dcBuff[0];
	backDC		= &dcBuff[1];
	frontDC->i8	= frontDC->start;
	backDC->i8	= backDC->start;
	geman_qid	= -1;
	markId		= 0xFFFF;
	markCpt		= 0;
	dmaBeginHC = dmaEndHC = 0;
	#ifdef ENABLE_SEMA_SYNC
	sceKernelSignalSema( markSema, 0xFFFF );	// set sema currentCount to max 0xFFFF !
	#endif
}


void
dmac::Shut()
{
	geman_RemoveHandler();
	EngineFree( (void*)DMA_ADDR(dcBuff[0].start) );
	EngineFree( (void*)DMA_ADDR(dcBuff[1].start) );
}



void
dmac::Start		(	void*		inMADR		)
{
	NV_ASSERT( !IsBusy() );
	NV_ASSERT_A32( inMADR );
	markId	   = 0;
	markCpt	   = 0;
	#ifdef ENABLE_SEMA_SYNC
	markSemaPrevId = 0;
	markSemaWaitId = 0xFFFF;
	sceKernelPollSema( markSema, 0xFFFF );	// set sema currentCount to 0 !
	#endif
//	FlushDCache();
//  WBB writeback is done by geman !
	for( ;; ) {
		geman_qid = sceGeListEnQueue( inMADR, NULL, geman_intrid, NULL );
		NV_ASSERTC( geman_qid >= 0, "geman error: sceGeListEnQueue() has failed !" );
		if( geman_qid >= 0 )
			break;
	}
}


float
dmac::GetPerfTime	(				)
{
	uint32 dhc;
	if( dmaEndHC > dmaBeginHC )		dhc = dmaEndHC - dmaBeginHC;
	else							dhc = dmaEndHC + (0x7FFFFFFF-dmaBeginHC);
	const float ht = 1.f/(60.f*HCNT_PER_V);
	return float(int(dhc))*ht;
}


bool
dmac::IsBusy	(							)
{
	if( geman_qid < 0 )
		return FALSE;
	int res = sceGeListSync( geman_qid, 1 );
	if( res != SCE_GE_LIST_COMPLETED )
		return TRUE;
	sceGeDrawSync( 0 );		// release the queue ID !
	geman_qid = -1;
	#ifdef ENABLE_SEMA_SYNC
	sceKernelSignalSema( markSema, 0xFFFF );	// set sema currentCount to max 0xFFFF !
	#endif
	return FALSE;
}


bool
dmac::Sync			(				)
{
	if( geman_qid < 0 )
		return TRUE;
	int res = sceGeListSync( geman_qid, 1 );
	NV_ASSERT( res != SCE_GE_LIST_STALLING );	// unsupported here !
	NV_ASSERT( res != SCE_GE_LIST_PAUSED );		// unsupported here !

	bool stalling = FALSE;

	if( res != SCE_GE_LIST_COMPLETED ) {
		#ifdef CHECK_DMA_TIMEOUT
		stalling = !WaitMark( 0xFFFF );
		#else
		sceGeListSync( geman_qid, 0 );
		#endif
	}

	if( stalling )
	{
		NV_WARNING( "<Neova> ** DMAC stalls **" );
		return FALSE;
	}
	else
	{
		sceGeDrawSync( 0 );		// release the queue ID !
		geman_qid = -1;
		#ifdef ENABLE_SEMA_SYNC
		sceKernelSignalSema( markSema, 0xFFFF );	// set sema currentCount to max 0xFFFF !
		#endif
		return TRUE;
	}
}


void
dmac::Stop		(							)
{
	if( !IsBusy() )
		return;
	// dequeueing failed ?
	if( sceGeListDeQueue(geman_qid) < 0 ) {
		// global break !
		SceGeBreakParam brkData;
		int _qid = sceGeBreak( 1, &brkData );
		if( _qid>=0 && _qid!=geman_qid )
			DebugPrintf( "<Neova> Cancel of an unknow displaylist %d !\n", _qid );
	}
	sceGeDrawSync( 0 );		// release the queue ID !
	geman_qid = -1;
	#ifdef ENABLE_SEMA_SYNC
	sceKernelSignalSema( markSema, 0xFFFF );	// set sema currentCount to max 0xFFFF !
	#endif
}


void
dmac::Pause		(							)
{
	SceGeBreakParam brkData;
	int _qid = sceGeBreak( 0, &brkData );
	if( _qid>=0 && _qid!=geman_qid )
		DebugPrintf( "<Neova> Cancel of an unknow displaylist %d !\n", _qid );
}


void
dmac::Continue		(						)
{
	sceGeContinue();
}


volatile uint16
dmac::GetMark		(						)
{
	return markId;
}


volatile uint16
dmac::GetMarkCpt	(						)
{
	return markCpt;
}


bool
dmac::WaitNextMark	(		)
{
	uint16 next_markId = markId;
	if( next_markId < 0xFFFF )
		next_markId++;
	return WaitMark( next_markId );
}


bool
dmac::WaitMark		(	uint16		inMark	)
{
	if( markId >= inMark )
		return TRUE;

#ifdef ENABLE_SEMA_SYNC

	NV_ASSERT( markSema > 0 );
	int res;

	// Get *n* marks
	SceUInt* timeoutDelayP = NULL;
	#ifdef CHECK_DMA_TIMEOUT
	SceUInt timeoutDelay = 1000000U;	// 1s
	timeoutDelayP = &timeoutDelay;
	#endif
	markSemaWaitId = inMark;
	res = sceKernelWaitSema( markSema, inMark, timeoutDelayP );
	NV_ASSERT( res>=0 || res==SCE_KERNEL_ERROR_WAIT_TIMEOUT );
	if( res == SCE_KERNEL_ERROR_WAIT_TIMEOUT ) {
		NV_WARNING( "<Neova> ** DMAC stalls **" );
		return FALSE;	// TIMEOUT !
	}
	// Restore *n* marks
	res = sceKernelSignalSema( markSema, inMark );
	NV_ASSERT( res>=0 || res==SCE_KERNEL_ERROR_SEMA_OVF );

	return TRUE;

#else

	#ifdef CHECK_DMA_TIMEOUT
	uint32 wcycMax = GetTimeCycleCpt( 1.f );
	uint32 wcyc0   = GetCycleCpt();
	#endif
	for( ;; ) {
		// Wait some cycles with DMAC full speed (no BUS access)
		WaitCycleCpt( 32 );
		if( markId >= inMark )
			return TRUE;
		#ifdef CHECK_DMA_TIMEOUT
		uint32 wcyc1 = GetCycleCpt();
		if( (wcyc1-wcyc0) > wcycMax ) {
			NV_WARNING( "<Neova> ** DMAC stalls **" );
			return FALSE;	// TIMEOUT !
		}
		#endif
	}

#endif
}




//
//	HEAP BUFFER


void
dmac::SwapHeap()
{
	NV_ASSERT_DC( frontDC );
	NV_ASSERT_DC( backDC  );

	// swap
	Swap( frontDC, backDC );

	// rewind front
	frontDC->i8  = frontDC->start;
	NV_ASSERT_DC( frontDC );
}

dmac::Cursor *
dmac::GetFrontHeapCursor()
{
	NV_ASSERT_DC( frontDC );
	return frontDC;
}

dmac::Cursor *
dmac::GetBackHeapCursor()
{
	NV_ASSERT_DC( backDC );
	return backDC;
}




//	CURSOR

void
dmac::Cursor::Memcpy( void * inPtr, uint inBSize )
{
	NV_ASSERT_DC( this );
	NV_ASSERT( inPtr );
	if( !inBSize )	return;
	libc::Memcpy( vd, inPtr, inBSize );
	i8 += inBSize;
}


void *
dmac::Cursor::Dup(	void *	inStart, uint inBAlignement	)
{
	NV_ASSERT_DC( this );
	uint8 * dup_start = inStart ? (uint8*)inStart : start;
	NV_ASSERT( (dup_start>=start) && (dup_start<(start+bsize)) );
	uint memSize = i8 - dup_start;
	void * dup_copy = EngineMallocA( memSize, inBAlignement );
	libc::Memcpy( dup_copy, dup_start, memSize );
	return dup_copy;
}


uint32
dmac::Cursor::GetLeftBSize()
{
	NV_ASSERT_DC( this );
	if( !start )			return 0;
	NV_ASSERT( i8 >= start );
	if( i8 < start )		return 0;
	uint32 offset = i8 - start;
	NV_ASSERT( offset <= bsize );
	return ( offset >= bsize ? 0 : bsize-offset );
}


uint32
dmac::Cursor::GetUsedBSize()
{
	if( !start )			return 0;
	NV_ASSERT( i8 >= start );
	if( i8 < start )		return 0;
	uint32 offset = i8 - start;
	NV_ASSERT( offset <= bsize );
	return ( offset >= bsize ? bsize : offset );
}


void
dmac::Cursor::Align32()
{
	while( uint32(i8)&0x3 )
		*i8++ = 0;
}

void
dmac::Cursor::Align64()
{
	while( uint32(i8)&0x7 )
		*i8++ = 0;
}

void
dmac::Cursor::Align128()
{
	while( uint32(i8)&0xF )
		*i8++ = 0;
}

void
dmac::Cursor::Align128_32()
{
	while( (uint32(i8)&0xF) != (16-4) )
		*i8++ = 0;
}

void
dmac::Cursor::Align8QW()
{
	while( uint32(i8)&0x7F )
		*i8++ = 0;
}

void
dmac::Cursor::Align8QW_1()
{
	while( (uint32(i8)&0x7F) != (128-16) )
		*i8++ = 0;
}




