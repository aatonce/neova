/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/ALG/ALG.h>

#define MAGIC_TEXTURE_OFFSET_VALUE 	0.0625f

// buffer have 64 strip so several combinaition can be done
// COPY_STRIDE_X * 64 = MAX_BUFFER_WIDTH_X
#define NB_STRIP						32//64
#define COPY_STRIDE_DEF 				32//64
#define MAX_BUFFER_WIDTH_DEF			(COPY_STRIDE_DEF * NB_STRIP)

#define	COPY_STRIDE_COPY				COPY_STRIDE_DEF
#define COPY_STRIDE_FILL				COPY_STRIDE_DEF

namespace {
	struct CopyVertex {
		float	u, v;
		float	x,y,z;
	};	
	struct FillVertex {
		float	x,y,z;
	};

	struct CopyVertex2DMode {
		uint16	u, v;
		int16	x,y,z;
	};
	
	FillVertex  		vertexBufferFill 	[2*(MAX_BUFFER_WIDTH_DEF/COPY_STRIDE_DEF)]	;		
	CopyVertex 			vertexBufferCopy 	[2*(MAX_BUFFER_WIDTH_DEF/COPY_STRIDE_DEF)]	;		
	CopyVertex2DMode 	vertexBufferZToAlpha[2*480]	;	
	uint 				nbPrim 				 = 2*(MAX_BUFFER_WIDTH_DEF/COPY_STRIDE_DEF)	;	
	
	void InitVertexBuffer(){
		static bool isInit = FALSE;
		if (!isInit) {
			isInit = TRUE;
			float xstep = 1.0f/(MAX_BUFFER_WIDTH_DEF/COPY_STRIDE_DEF);
			
			uint i = 0 ;
			for (float x = 0.0f ; x <= (1.0f-xstep) ; x+=xstep) {
				vertexBufferFill [i].x 	= 	x;
				vertexBufferCopy [i].x	=	x;
				vertexBufferFill [i].y 	= 	0.0f;
				vertexBufferCopy [i].y	=	0.0f;
				vertexBufferFill [i].z	= 	0.0f;				
				vertexBufferCopy [i].z	=	0.0f;			
				vertexBufferCopy [i].u	=	x;
				vertexBufferCopy [i].v	=	0.0f;

				vertexBufferFill [i+1].x=	x+xstep; 				
				vertexBufferCopy [i+1].x=	x+xstep;				
				vertexBufferFill [i+1].y=	1.0f;										
				vertexBufferCopy [i+1].y=	1.0f;
				vertexBufferFill [i+1].z=	0.0f;										
				vertexBufferCopy [i+1].z=	0.0f;			
				vertexBufferCopy [i+1].u=	x+xstep;
				vertexBufferCopy [i+1].v=	1.0f;
													
				i+=2;
			}
			NV_ASSERT(i == nbPrim);

			for (i = 0 ; i < 480 ; ++i ) {
				uint i2 = i * 2;
				vertexBufferZToAlpha[i2].x	= int16(i2+1);
				vertexBufferZToAlpha[i2].y	= int16(0);
				vertexBufferZToAlpha[i2].z	= int16(0);
				vertexBufferZToAlpha[i2].u	= uint16(i);
				vertexBufferZToAlpha[i2].v	= uint16(0);
				
				vertexBufferZToAlpha[i2+1].x=int16(i2+2);
				vertexBufferZToAlpha[i2+1].y=int16(272);
				vertexBufferZToAlpha[i2+1].z=int16(0);
				vertexBufferZToAlpha[i2+1].u=uint16(i);
				vertexBufferZToAlpha[i2+1].v=uint16(272);				
			}
		}
	}
	
	uint
	SetTransformation (			dmac::Cursor * 	inDC						,
								Vec4 & 			inRec 						, 
								Vec4 * 			inTex 						, 
								uint16			inSrcSizeXPow2				,			
								uint16			inSrcSizeYPow2				,								
								float 			inZ						, 
								int				inStride = COPY_STRIDE_DEF	) 			
	{
		NV_ASSERT(inDC);
		if (!inDC) return 0;
		
		NV_ASSERT(inStride>0 && IsPow2(uint(inStride)));
		if (inStride <= 0 || !IsPow2(uint(inStride))) return 0;
		
		uint maxBufferWidth = inStride * NB_STRIP ;
		
		*inDC->i32++ = SCE_GE_SET_SCISSOR2		( uint(inRec.z-1)	, uint(inRec.w-1) );	
		
		float 	width  = 		inRec.z-inRec.x;		
		if (inTex) {
			float 	tsizex	= float (1<<inSrcSizeXPow2);
			float 	tsizey	= float (1<<inSrcSizeYPow2);			
			// su = (inTex->z - inTex->x) / float(1<<inSrcSizeXPow2) * // because Tsize0 is pow of 2
			//		float(maxBufferWidth) / float(width);					// because we do N pixel width bands;			
			float 	su 		= ((inTex->z - inTex->x) / width ) * (float(maxBufferWidth) / tsizex);
			float 	tu 		= (inTex->x+ MAGIC_TEXTURE_OFFSET_VALUE) / tsizex;			
			float 	sv 		= (inTex->w - inTex->y) / tsizey;
			float 	tv 		= (inTex->y + MAGIC_TEXTURE_OFFSET_VALUE) / tsizey ;		
			*inDC->i32++ 	= SCE_GE_SET_SU_FLOAT24			( su 	);
			*inDC->i32++ 	= SCE_GE_SET_SV_FLOAT24			( sv 	);
			*inDC->i32++ 	= SCE_GE_SET_TU_FLOAT24			( tu 	);
			*inDC->i32++ 	= SCE_GE_SET_TV_FLOAT24			( tv 	);		
		}		
		float sx 		= maxBufferWidth;
		float sy 		= inRec.w-inRec.y;			
		float tx 		= inRec.x ;
		float ty 		= inRec.y ;
		float tz 		= inZ ;
		*inDC->i32++ 	= SCE_GE_SET_WORLDN				( 0 	);
		*inDC->i32++ 	= SCE_GE_SET_WORLDD_FLOAT24		( sx 	);
		*inDC->i32++ 	= SCE_GE_SET_WORLDN				( 4 	);
		*inDC->i32++ 	= SCE_GE_SET_WORLDD_FLOAT24		( sy 	);
		*inDC->i32++ 	= SCE_GE_SET_WORLDN				( 9 	);
		*inDC->i32++ 	= SCE_GE_SET_WORLDD_FLOAT24		( tx 	);
		*inDC->i32++ 	= SCE_GE_SET_WORLDD_FLOAT24		( ty 	);
		*inDC->i32++ 	= SCE_GE_SET_WORLDD_FLOAT24		( tz	);

		return uint(Ceil( width /float(inStride) ));
	}
}

uint
ge::tools::GetPSMBitsSize	( int inPsm	)
{
		NV_ASSERT(inPsm < 8);	// 	inPsm must not be in [SCE_GE_TPF_DXT1, SCE_GE_TPF_DXT3, SCE_GE_TPF_DXT5]		
		NV_ASSERT(inPsm >= 0);
		if (inPsm >= 8 || inPsm < 0) return 0;
		
		if ( (inPsm & 0x3)==0x3) 	return 32; 	// SCE_GE_TPF_IDTEX32 (7) ,SCE_GE_TPF_8888	(3)
		if (  inPsm == 4 ) 			return 4;  	// SCE_GE_TPF_IDTEX4 (4) 
		if (  inPsm == 5 ) 			return 8;	// SCE_GE_TPF_IDTEX8 (5)
		return 16;								// SCE_GE_TPF_5650 (0),SCE_GE_TPF_5551 (1), SCE_GE_TPF_4444 (2), SCE_GE_TPF_IDTEX16 (6)
}

uint 
ge::tools::GetBufferByteSize ( int inPsm, uint inSizeX , uint inSizeY) 
{
	uint sizeBits = GetPSMBitsSize(inPsm) * inSizeY * Round64(inSizeX);	
	NV_ASSERT( (sizeBits & 0x3) == 0);
	return 	sizeBits >> 3 ; // sizeBits / 8
}

void	
ge::tools::Kernel33::Reset	()
{
	k00 = k01 = k02 = 0;
	k10 = k11 = k12 = 0;
	k20 = k21 = k22 = 0;	
}

void	
ge::tools::Kernel33::Normalize	()
{
	float s  = k00 + k01 + k02;
		  s += k10 + k11 + k12;
		  s += k20 + k21 + k22;
	if( s > 0.0f ) {
		float oos = 1.0f / s;
		k00 *= oos;		k01 *= oos;		k02 *= oos;
		k10 *= oos;		k11 *= oos;		k12 *= oos;
		k20 *= oos;		k21 *= oos;		k22 *= oos;
	}
}

void	
ge::tools::Kernel33::RotateCW	()
{
	float _k;
	_k = k00; k00 = k20; k20 = k22; k22 = k02; k02 = _k;
	_k = k01; k01 = k10; k10 = k21; k21 = k12; k12 = _k;
}

void	
ge::tools::Kernel33::RotateCCW	()
{
	float _k;
	_k = k00; k00 = k02; k02 = k22; k22 = k20; k20 = _k;
	_k = k01; k01 = k12; k12 = k21; k21 = k10; k10 = _k;
}
	
int 
ge::tools::Compute_MipmapedGaussConvol	(	MipmapGC*	outMipGC/*[16]*/,
											int			inSizeX,
											int			inSizeY,
											float		inRadius,
											float		inQuality		)
{
	//
	// Computes Gaussian normalized sequence

	float gWeights[128];
	float gSigma = GaussianDeviation( inRadius );
	int   gWidth = GaussianWidth( gSigma );
	if( gWidth < 2 || gWidth > 128 )
		return 0;
	GaussianNormSequence( gWeights, gSigma, TRUE );

	//
	// N mipmaps, from (inSizeX,inSizeY) -> (8,8)

	int mip_cpt = 0;
	int minSize = Min( inSizeX, inSizeY );
	while( (minSize>>(mip_cpt+1)) >= 8 )
		mip_cpt++;
	if( mip_cpt == 0 || mip_cpt >= 16 )
		return 0;

	//
	// Computes mipmaps weight

	float sum_mipw[16];
		  sum_mipw[ mip_cpt+1 ] = 0.0f;

	float cur_avg = 0.0f;
	for( int mip = mip_cpt ; mip >= 0 ; mip-- ) {
		MipmapGC* gc = & outMipGC[mip];

		// average mipmap weights
		int   w1  = Min( 1 << mip,    gWidth );
		int   w0  = Min( (1<<mip)>>1, gWidth );
		float s   = 0.0f;
		for( int wi = w0 ; wi < w1 ; wi++ )
			s += gWeights[wi];
		float avg  = (w1>w0) ? s / float( w1-w0 ) : 0.0f;

		// mipmap factor to reach the average from the current average
		float w			= 1.0f / float( 1<<mip );
		float mipw		= (avg - cur_avg) / w;
		cur_avg			= avg;
		sum_mipw[mip]	= sum_mipw[ mip+1 ] + mipw;
		gc->weight      = mipw;
		// Smooth mipmap for better quality, based on mipmap ratio 1/2^N
		gc->quality    = ( inQuality >= w );

	//	Printf( "mip%d [%d,%d[ avg=%f  w=%f quality=%c\n", mip, w0, w1, avg, mipw, gc->quality?'Y':'N' );
	}

	//		
	// Computes recursive blending alpha factors (a,b,c,...z),
	// such that :
	// mip(i-1) = mip(i  ) * a + mip(i-1) * (1-a)
	// mip(i-2) = mip(i-1) * b + mip(i-2) * (1-b)
	// ...
	// mip(0)   = mip(1)   * z + mip(0)   * (1-z)
	// fromBAddr += mip(0)
	//
	// a = wa      / (wa+wb)
	// b = (wa+wb) / (wa+wb+wc)
	// ...

	outMipGC[0].blend = int( sum_mipw[0] * 128.0f );	// ~1.0
	for( int mip = mip_cpt ; mip > 0 ; mip-- ) {
		float sw0   = sum_mipw[ mip   ];
		float sw1   = sum_mipw[ mip-1 ];
		float blend = ( sw0 && sw1 ) ? sw0 / sw1 : 0.0f;
		int alpha   = int( blend * 128.0f );
		if( alpha == 0 )	mip_cpt = mip - 1;
		else				outMipGC[mip].blend = alpha;
	//	Printf( "mip%d mip_blend=%d\n", mip, mip_blend[mip] );
	}
	//	Printf( "mip%d mip_blend=%d\n", 0, mip_blend[0] );
	NV_ASSERT( mip_cpt > 0 );

	return mip_cpt;
}


void 
ge::tools::LoadClut	(	dmac::Cursor * 	inDC,
						uint 			inClutAddr	, 
						uint8 			inPsm		, 
						uint8 			inZShift	, 
						uint8 			inMask		) 
{ // Load clut with 256 colors	
	*inDC->i32++ = SCE_GE_SET_CBP_ADDR24	(inClutAddr);		
	*inDC->i32++ = SCE_GE_SET_CBW_BASE8	(inClutAddr);		
	*inDC->i32++ = SCE_GE_SET_CLUT		(inPsm,inZShift,inMask,0);
	//loading Palettes 16 by 16 for 16 bits and 8 by 8 for 32bits => 256Ko by 256ko
	uint nbLoading = (inPsm==SCE_GE_CLUT_CPF_8888)?32:16; // else inPsm == SCE_GE_CLUT_CPF_5650 || inPsm == SCE_GE_CLUT_CPF_5551 || inPsm == SCE_GE_CLUT_CPF_4444	
	*inDC->i32++ = SCE_GE_SET_CLOAD		(nbLoading);
}
			
void 
ge::tools::InitContextCmds(dmac::Cursor * inDC) 
{
	NV_ASSERT(inDC);
	if (!inDC) return ;
	
	// Set Matrix 
	Matrix id;
	MatrixIdentity(&id);
	
	*inDC->i32++ = SCE_GE_SET_WORLDN		( 0 );
	BuildGeMatrix12( inDC->i32, &id, SCE_GE_CMD_WORLDD );	inDC->i32 += 12;
	*inDC->i32++ = SCE_GE_SET_VIEWN			( 0 );
	BuildGeMatrix12( inDC->i32, &id, SCE_GE_CMD_VIEWD );	inDC->i32 += 12;
	*inDC->i32++ = SCE_GE_SET_PROJN			( 0 );
	BuildGeMatrix16( inDC->i32, &id, SCE_GE_CMD_PROJD );	inDC->i32 += 16;		
	*inDC->i32++ = SCE_GE_SET_PROJN			( 15 );	
	// Value for clipping 
	*inDC->i32++ = SCE_GE_SET_PROJD			( 0x478000 );	// 65536
	
	// Set Texture Transformation
	*inDC->i32++ = SCE_GE_SET_SU			( 0x3F8000 			);
	*inDC->i32++ = SCE_GE_SET_SV			( 0x3F8000 			);
	*inDC->i32++ = SCE_GE_SET_TU			( 0x0   			);
	*inDC->i32++ = SCE_GE_SET_TV			( 0x0 				);
	
	
	*inDC->i32++ = SCE_GE_SET_SX			( 0x478000 			);	// 65536
	*inDC->i32++ = SCE_GE_SET_SY			( 0x478000 			); 	// 65536
	*inDC->i32++ = SCE_GE_SET_SZ			( 0x478000 			);	// 65536
	*inDC->i32++ = SCE_GE_SET_TX			( 0x0 				); 
	*inDC->i32++ = SCE_GE_SET_TY			( 0x0	 			);
	*inDC->i32++ = SCE_GE_SET_TZ			( 0x0   			);
	
	*inDC->i32++ = SCE_GE_SET_OFFSETX		( 0 				);
	*inDC->i32++ = SCE_GE_SET_OFFSETY		( 0 				);		

	// Region because region can be apply like SCISSOR
	*inDC->i32++ = SCE_GE_SET_REGION1		( 0			, 0 	);
	*inDC->i32++ = SCE_GE_SET_REGION2		( 1023		, 1023	);	
	
	// Left scissor do not changed
	*inDC->i32++ = SCE_GE_SET_SCISSOR1		( 0			, 0		);
					
	// Context init ...
	*inDC->i32++ = SCE_GE_SET_CLE			( 0					);	
	*inDC->i32++ = SCE_GE_SET_BCE			( 0 				);			
	*inDC->i32++ = SCE_GE_SET_ATE			( 0 				);
	*inDC->i32++ = SCE_GE_SET_AAE			( 0 				);		
	*inDC->i32++ = SCE_GE_SET_ZTEST			( 7					);
	*inDC->i32++ = SCE_GE_SET_STE			( 0 				);
	*inDC->i32++ = SCE_GE_SET_CTE			( 0 				);
	*inDC->i32++ = SCE_GE_SET_LOE			( 0 				);
	*inDC->i32++ = SCE_GE_SET_LTE			( 0 				);		
	*inDC->i32++ = SCE_GE_SET_DTE			( 0 				);
	*inDC->i32++ = SCE_GE_SET_TME			( 1 				);
	*inDC->i32++ = SCE_GE_SET_ZMSK			( 1 				);		
	*inDC->i32++ = SCE_GE_SET_ABE			( 0 				);		
	*inDC->i32++ = SCE_GE_SET_ZTE			( 0 				);			
}

void 		
ge::tools::RestoreContextCmds (	dmac::Cursor * 		inDC) 
{
	// restore
	*inDC->i32++ = ge::frame::DefContext.abe;
	*inDC->i32++ = ge::frame::DefContext.ate;
	*inDC->i32++ = ge::frame::DefContext.zte;
	*inDC->i32++ = ge::frame::DefContext.ztest;
	*inDC->i32++ = ge::frame::DefContext.ste; 
	*inDC->i32++ = ge::frame::DefContext.cte;
	*inDC->i32++ = ge::frame::DefContext.loe;
	*inDC->i32++ = ge::frame::DefContext.dte;
	*inDC->i32++ = ge::frame::DefContext.tme;
	*inDC->i32++ = ge::frame::DefContext.zmsk;
	*inDC->i32++ = ge::frame::DefContext.scissor[0];
	*inDC->i32++ = ge::frame::DefContext.scissor[1];
	*inDC->i32++ = SCE_GE_SET_FPF			( ge::frame::BackPSM );				
	*inDC->i32++ = SCE_GE_SET_PMSK1		(0x00,0x00,0x00);
	*inDC->i32++ = SCE_GE_SET_PMSK2		(0x00);		
}

void 
ge::tools::CopyZToAlpha(	dmac::Cursor*		inDC			,
							uint				inFromMAddr		,
							uint				inToVAddr		,
							uint				inSizeX			,
							uint				inSizeY			,				
							uint32**			outGeFBPCmd		)
{
	NV_ASSERT (inDC									);

	if (!inDC) return ;
	
	InitVertexBuffer();
	
	uint xpow2 			= GetCeilPow2	(inSizeX),
		 ypow2 			= GetCeilPow2	(inSizeY),
		 bufferWitdh 	= Round64		(inSizeX);
		 
	if( outGeFBPCmd )
		*outGeFBPCmd = inDC->i32;
	*inDC->i32++ = SCE_GE_SET_FBP_ADDR24	( inToVAddr													);
	*inDC->i32++ = SCE_GE_SET_FBW_BASE8		( bufferWitdh * 2			, inToVAddr						);
	*inDC->i32++ = SCE_GE_SET_FPF			( SCE_GE_FPF_5650											);				
	
	*inDC->i32++ = SCE_GE_SET_SCISSOR2		( (inSizeX*2)-1				, (inSizeY)-1					);					
	// PMSK to write value only on alpha chanel
	// Caution with convertion 565 => 888		
	*inDC->i32++ = SCE_GE_SET_PMSK1			( 0xFF						, 0x1F					,0x00	);									
			
	*inDC->i32++ = SCE_GE_SET_TFLUSH		(															);
	*inDC->i32++ = SCE_GE_SET_TSYNC			(															);
	
	*inDC->i32++ = SCE_GE_SET_TBP0_ADDR24	( inFromMAddr												);		
	*inDC->i32++ = SCE_GE_SET_TBW0_BASE8	( bufferWitdh				, inFromMAddr					);
	*inDC->i32++ = SCE_GE_SET_TSIZE0		( xpow2						, ypow2							);
	*inDC->i32++ = SCE_GE_SET_TPF			( SCE_GE_TPF_IDTEX16		, 0 							);
	*inDC->i32++ = SCE_GE_SET_TMODE			( SCE_GE_TMODE_HSM_NORMAL	, 0						, 0	 	);
	*inDC->i32++ = SCE_GE_SET_TFILTER		( SCE_GE_TFILTER_NEAREST	, SCE_GE_TFILTER_NEAREST		);
	*inDC->i32++ = SCE_GE_SET_TWRAP			( SCE_GE_TWRAP_CLAMP		, SCE_GE_TWRAP_CLAMP 			);
	*inDC->i32++ = SCE_GE_SET_TFUNC			( SCE_GE_TFUNC_REPLACE		, SCE_GE_TFUNC_RGBA		, 0 	);
		
	*inDC->i32++ = SCE_GE_SET_BASE_BASE8	( uint32(vertexBufferZToAlpha )								);		
	*inDC->i32++ = SCE_GE_SET_VADR			( uint32(vertexBufferZToAlpha )								);		
	*inDC->i32++ = SCE_GE_SET_VTYPE			( 2	, 0, 0, 2, 0, 0, 0, 0, 1 								);
	*inDC->i32++ = SCE_GE_SET_PRIM 			( 480 * 2					, 6	 							);
	*inDC->i32++ = SCE_GE_SET_PMSK1			( 0x00						,0x00					,0x00	);											
}

void 		
ge::tools::CopyZAsIdxTex		(	dmac::Cursor*		inDC 																	,
									uint				inFromMAddr																,
									uint				inToVAddr																,
									uint				inSizeX																	,
									uint				inSizeY																	,						
									uint16				inZ																		,
									uint				inFlafs																	,
									uint32**			outGeFBPCmd																)
{
	NV_ASSERT (	 inDC													);
	NV_ASSERT (  inSizeX < 1024    &&  inSizeY < 1024	);

	if (!inDC || inSizeX >= 1024 || inSizeY >= 1024 ) return ;
	
	InitVertexBuffer();
	
	uint 	sizeXPow2 	= GetCeilPow2	(inSizeX),
		 	sizeYPow2 	= GetCeilPow2	(inSizeY),
		 	bufferWitdh = Round64		(inSizeX),
			filteringMode 	= 	(inFlafs & CF_FilteringNearest)	?
								SCE_GE_TFILTER_NEAREST			:
								SCE_GE_TFILTER_LINEAR			;		 	
	if (outGeFBPCmd)
		*outGeFBPCmd = inDC->i32;					
	*inDC->i32++ = SCE_GE_SET_FBP_ADDR24	( inToVAddr												);
	*inDC->i32++ = SCE_GE_SET_FBW_BASE8		( bufferWitdh				, inToVAddr					);
	*inDC->i32++ = SCE_GE_SET_FPF			( SCE_GE_FPF_8888										);				
	
	// set src as Texture
	if ( inFlafs & CF_TFlush)
		*inDC->i32++ = SCE_GE_SET_TFLUSH	(														);
	if ( inFlafs & CF_TSync)			
		*inDC->i32++ = SCE_GE_SET_TSYNC		(														);
	
	*inDC->i32++ = SCE_GE_SET_TBP0_ADDR24	( inFromMAddr											);		
	*inDC->i32++ = SCE_GE_SET_TBW0_BASE8	( bufferWitdh				, inFromMAddr				);
	*inDC->i32++ = SCE_GE_SET_TSIZE0		( sizeXPow2					, sizeYPow2					);
	*inDC->i32++ = SCE_GE_SET_TPF			( SCE_GE_TPF_IDTEX16		, 0 						);
	*inDC->i32++ = SCE_GE_SET_TMODE			( SCE_GE_TMODE_HSM_NORMAL	, 0						, 0 );

	*inDC->i32++ = SCE_GE_SET_TFILTER		( filteringMode				, filteringMode				);
	*inDC->i32++ = SCE_GE_SET_TWRAP			( SCE_GE_TWRAP_CLAMP		, SCE_GE_TWRAP_CLAMP 		);
	*inDC->i32++ = SCE_GE_SET_TFUNC			( SCE_GE_TFUNC_REPLACE		, (inFlafs & CF_UseAlpha)	?
																		   SCE_GE_TFUNC_RGBA		:
																		   SCE_GE_TFUNC_RGB		, 0 );
	Vec4 Rec(0,0,inSizeX,inSizeY);
	uint nbRecToRender = SetTransformation  ( inDC,Rec,&Rec,sizeXPow2,sizeYPow2,inZ,COPY_STRIDE_DEF);
								
	*inDC->i32++ = SCE_GE_SET_BASE_BASE8	( uint32(vertexBufferCopy )								);		
	*inDC->i32++ = SCE_GE_SET_VADR			( uint32(vertexBufferCopy )								);		
	*inDC->i32++ = SCE_GE_SET_VTYPE			( 3	, 0, 0, 3, 0, 0, 0, 0, 0 							);
	*inDC->i32++ = SCE_GE_SET_PRIM 			( nbRecToRender * 2			, 6	 						);																								
}
											

void 
ge::tools::CopyBuffer(	dmac::Cursor*		inDC 		,
						uint				inFromMAddr	,
						uint				inToVAddr	,
						uint				inSrcSizeX	,
						uint				inSrcSizeY	,
						uint				inDstSizeX	,
						uint				inDstSizeY	,
						Vec4&				inRec		,
						Vec4&				inTex		,
						uint16				inZ			,
						uint				inFlafs		,
						uint32**			outGeFBPCmd	,
						uint32**			outGeTBPCmd	)
{	
	NV_ASSERT (	 inDC													);
	NV_ASSERT (  inSrcSizeX < 1024    &&  inSrcSizeY < 1024	);

	if (!inDC || inSrcSizeX >= 1024 || inSrcSizeY >= 1024 ) return ;
	
	InitVertexBuffer();
	
	uint 	srcSizeXPow2 	= GetCeilPow2	(inSrcSizeX),
		 	srcSizeYPow2 	= GetCeilPow2	(inSrcSizeY),
		 	srcBufferWitdh 	= Round64		(inSrcSizeX),
		 	dstBufferWitdh	= Round64		(inDstSizeX),
			filteringMode 	= 	(inFlafs & CF_FilteringNearest)	?
								SCE_GE_TFILTER_NEAREST			:
								SCE_GE_TFILTER_LINEAR			;		 	
	if (outGeFBPCmd)
		*outGeFBPCmd = inDC->i32;					
	*inDC->i32++ = SCE_GE_SET_FBP_ADDR24	( inToVAddr												);
	*inDC->i32++ = SCE_GE_SET_FBW_BASE8		( dstBufferWitdh		, inToVAddr						);
	*inDC->i32++ = SCE_GE_SET_FPF			( SCE_GE_FPF_8888										);				
	
	// set src as Texture
	if ( inFlafs & CF_TFlush)
		*inDC->i32++ = SCE_GE_SET_TFLUSH	(														);
	if ( inFlafs & CF_TSync)			
		*inDC->i32++ = SCE_GE_SET_TSYNC		(														);
	
	if (outGeTBPCmd)
		*outGeTBPCmd = inDC->i32;					
	*inDC->i32++ = SCE_GE_SET_TBP0_ADDR24	( inFromMAddr											);		
	*inDC->i32++ = SCE_GE_SET_TBW0_BASE8	( srcBufferWitdh			, inFromMAddr				);
	*inDC->i32++ = SCE_GE_SET_TSIZE0		( srcSizeXPow2				, srcSizeYPow2				);
	*inDC->i32++ = SCE_GE_SET_TPF			( SCE_GE_TPF_8888			, 0 						);
	*inDC->i32++ = SCE_GE_SET_TMODE			( SCE_GE_TMODE_HSM_NORMAL	, 0						, 0 );

	*inDC->i32++ = SCE_GE_SET_TFILTER		( filteringMode				, filteringMode				);
	*inDC->i32++ = SCE_GE_SET_TWRAP			( SCE_GE_TWRAP_CLAMP		, SCE_GE_TWRAP_CLAMP 		);
	*inDC->i32++ = SCE_GE_SET_TFUNC			( SCE_GE_TFUNC_REPLACE		, (inFlafs & CF_UseAlpha)	?
																		   SCE_GE_TFUNC_RGBA		:
																		   SCE_GE_TFUNC_RGB		, 0 );
	
	uint nbRecToRender = SetTransformation  ( inDC,inRec,&inTex,srcSizeXPow2,srcSizeYPow2,inZ,COPY_STRIDE_DEF);
								
	*inDC->i32++ = SCE_GE_SET_BASE_BASE8	( uint32(vertexBufferCopy )								);		
	*inDC->i32++ = SCE_GE_SET_VADR			( uint32(vertexBufferCopy )								);		
	*inDC->i32++ = SCE_GE_SET_VTYPE			( 3	, 0, 0, 3, 0, 0, 0, 0, 0 							);
	*inDC->i32++ = SCE_GE_SET_PRIM 			( nbRecToRender * 2			, 6	 						);
}

void 		
ge::tools::SimpleCopyBuffer	(	dmac::Cursor*		inDC 		,
								uint				inFromMAddr	,
								uint				inToVAddr	,
								uint				inSizeX		,
								uint				inSizeY		,
								uint16				inZ			,
								uint				inFlafs		,
								uint32**			outGeFBPCmd	,
								uint32**			outGeTBPCmd	){
	Vec4 vec ( 0 , 0 , inSizeX , inSizeY ) ;
	CopyBuffer(	inDC,inFromMAddr,inToVAddr,inSizeX,inSizeY,inSizeX,inSizeY,vec,vec,inZ,inFlafs,outGeFBPCmd,outGeTBPCmd);
}

void 
ge::tools::FillBuffer(	dmac::Cursor*		inDC 			,
						uint				inToVAddr		,
						uint				inDstSizeX		,
						Vec4&				inRec			,
						uint16				inZ				,
						uint32				inColorABGR		,
						uint32**			outGeFBPCmd		)
{	
	NV_ASSERT (	 inDC									);
	if (!inDC)	return ;

	InitVertexBuffer();
	
	uint dstBufferWitdh	= Round64			(inDstSizeX);
	
	if (outGeFBPCmd)
		*outGeFBPCmd = inDC->i32;
	*inDC->i32++ = SCE_GE_SET_FBP_ADDR24	( inToVAddr										);
	*inDC->i32++ = SCE_GE_SET_FBW_BASE8		( dstBufferWitdh		, inToVAddr				);
	*inDC->i32++ = SCE_GE_SET_FPF			( SCE_GE_FPF_8888								);				
	
	*inDC->i32++ = SCE_GE_SET_MAC_RGB24		( inColorABGR &0x00FFFFFF						);	
	*inDC->i32++ = SCE_GE_SET_MAA			( inColorABGR >>24								);
					
	uint nbRecToRender = SetTransformation  ( inDC,inRec,NULL,0,0,inZ,COPY_STRIDE_FILL		);
		
	*inDC->i32++ = SCE_GE_SET_BASE_BASE8	( uint32(vertexBufferFill )						);		
	*inDC->i32++ = SCE_GE_SET_VADR			( uint32(vertexBufferFill )						);		
	*inDC->i32++ = SCE_GE_SET_VTYPE			( 0	, 0, 0, 3, 0, 0, 0, 0, 0 					);
	*inDC->i32++ = SCE_GE_SET_PRIM 			(nbRecToRender * 2		, 6						);						
}
/*
void 
ge::tools::MakeImageTest	(	dmac::Cursor * 	inDC			,
								uint			inToVAddr 		,
								uint			inSizeX			,
								uint			inSizeY			,
								int 			inStep			,
								uint32**		outGeFBPCmd		) // Pb : il en faut bcp ...
{
	NV_ASSERT (	inDC );

	Vec4 rec;		
	
	rec.Set(0,0,inSizeX , inSizeY);
	*inDC->i32++ = SCE_GE_SET_TME ( 0 );
	FillBuffer(inDC, inToVAddr, inSizeX, rec, 0, 0xFFFFFF00, outGeFBPCmd );
						
	uint offsetX = 0;
	uint offsetY = 32;
	uint32 value = 0xFFFFFFFF;		
	uint32 color = 0xFFFFFFFF;
	for (uint i = offsetX ; i < inSizeX-inStep-offsetX ; i+= inStep) {
		rec.Set(i,offsetY,i+inStep,inSizeY-offsetY);
		FillBuffer(inDC, inToVAddr, inSizeX, rec, 0, color, outGeFBPCmd );		
		color^=value;
	}
	rec.Set(offsetX,offsetY,offsetX+inStep,inSizeY-offsetY);
	FillBuffer(inDC, inToVAddr, inSizeX, rec, 0, 0xFF0000FF, outGeFBPCmd );			
	
	rec.Set(inSizeX-inStep-offsetX,offsetY,inSizeX-offsetX,inSizeY-offsetY);
	FillBuffer(inDC, inToVAddr, inSizeX, rec, 0, 0xFF00FF00, outGeFBPCmd );		
			
	*inDC->i32++ = SCE_GE_SET_TME ( 1 );
}
*/


bool
ge::tools::PreparePass		(	dmac::Cursor*		inDC 				,
								uint				inFromMAddr			,
								uint				inToVAddr			,
								uint				inSrcSizeX			,
								uint				inSrcSizeY			,
								uint				inDstSizeX			,
								uint				inDstSizeY			,
								uint32**			outGeTBPCmd			)
{
	*outGeTBPCmd = NULL;
	
	// Resizing
	int sx = inDstSizeX;
	int sy = inDstSizeY;
	int rx = int( float(inSrcSizeX) / 2.0f );
	int ry = int( float(inSrcSizeY) / 2.0f );
	
	// Safeband
	if( sx < rx )			return FALSE;	// invalid factor !?
	if( sy < ry )			return FALSE;	// invalid factor !?
	if( (sx-rx)&1	)		rx--;
	if( (sy-ry)&1	)		ry--;
	if( rx == 0 )			return FALSE;	// invalid factor !?
	if( ry == 0 )			return FALSE;	// invalid factor !?
	int bx = (sx - rx) >> 1;
	int by = (sy - ry) >> 1;

	uint toMAddr = (uint)ge::GetVRamPointer (inToVAddr);	
	
	// copy full frame in center
	Vec4 rec(bx,by,rx+bx,ry+by);
	Vec4 tex(0,0,inSrcSizeX,inSrcSizeY);				
	CopyBuffer( inDC, inFromMAddr, inToVAddr, 
				inSrcSizeX, inSrcSizeY, inDstSizeX, inDstSizeY,
				rec, tex, 0, CF_FilteringLinear | CF_TSync | CF_TFlush,
				NULL, outGeTBPCmd );
						
	// make left guard band
	rec.Set(0 ,by,bx,ry+by);
	tex.Set(bx,by,bx,ry+by);
	CopyBuffer( inDC, toMAddr, inToVAddr, 
				inDstSizeX, inDstSizeY, inDstSizeX, inDstSizeY,
				rec, tex, 0, CF_FilteringNearest | CF_TSync,
				NULL, NULL );
					
	// make right guard band
	rec.Set(rx+bx	, by , sx 		, ry+by);
	tex.Set(rx+bx-1 , by , rx+bx-1	, ry+by);
	CopyBuffer( inDC, toMAddr, inToVAddr, 
				inDstSizeX, inDstSizeY, inDstSizeX, inDstSizeY,
				rec, tex, 0, CF_FilteringNearest,
				NULL, NULL );
					
	// make top guard band
	rec.Set(0,0 ,sx,by);
	tex.Set(0,by,sx,by);
	CopyBuffer( inDC, toMAddr, inToVAddr, 
				inDstSizeX, inDstSizeY, inDstSizeX, inDstSizeY,
				rec, tex, 0, CF_FilteringNearest | CF_TSync,
				NULL, NULL );
					
	// make bottom guard band
	rec.Set(0,ry+by  ,sx,sy);
	tex.Set(0,ry+by-1,sx,ry+by-1);
	CopyBuffer( inDC, toMAddr, inToVAddr, 
				inDstSizeX, inDstSizeY, inDstSizeX, inDstSizeY,
				rec, tex, 0, CF_FilteringNearest,
				NULL, NULL );	

	return TRUE;
}



bool
ge::tools::UnPreparePass	(	dmac::Cursor*		inDC 		,
								uint				inFromMAddr	,
								uint				inToVAddr	,
								uint				inSrcSizeX	,
								uint				inSrcSizeY	,
								uint				inDstSizeX	,
								uint				inDstSizeY	,								
								uint16				inZ		,
								int					inCopyFlags	,
								uint32**			outGeFBPCmd	)
{		
	NV_ASSERT(inDC);
	if (!inDC) return FALSE;
	
	// Resizing
	int sx = inSrcSizeX;
	int sy = inSrcSizeY;
	int rx = int( float(inDstSizeX) / 2.0f );
	int ry = int( float(inDstSizeY) / 2.0f );
	
	// Safeband
	if( sx < rx )			return FALSE;	// invalid factor !?
	if( sy < ry )			return FALSE;	// invalid factor !?
	if( (sx-rx)&1	)		rx--;
	if( (sy-ry)&1	)		ry--;
	if( rx == 0 )			return FALSE;	// invalid factor !?
	if( ry == 0 )			return FALSE;	// invalid factor !?
	int bx = (sx - rx) >> 1;
	int by = (sy - ry) >> 1;
	
	Vec4 rec(0,0,inDstSizeX,inDstSizeY);	
	Vec4 tex(bx,by,rx+bx,ry+by);
	CopyBuffer( inDC, inFromMAddr, inToVAddr, 
				inSrcSizeX, inSrcSizeY, inDstSizeX, inDstSizeY,
				rec, tex, inZ, inCopyFlags,
				outGeFBPCmd, NULL );		
	return TRUE;
}


void
ge::tools::DiffConvolvePass		(	dmac::Cursor*			inDC		,
									uint					inFromMAddr	,
									uint					inToVAddr	,
									uint					inTmpVAddr	,
									uint					inSizeX		,
									uint					inSizeY		,									
									const Kernel33&			inKernel	,
									uint16					inZ			)
{
	// Convolution diff�rentielle additive
	// Le principe est d'ajouter au centre la diff�rence POSITIVE du voisinage
	// Le centre a un diff�rentiel de 0, i.e. P(i,j)-P(i,j) = 0
	// L'effet obtenu est un �largissement des zones les plus lumineuses
	// P(i,j) += % ABS( P(i,j) - P(i-k,j-l) )

	// From outer to inner for round error accumulation
	//				     00	    02	   20	  22	 10		12		01,		21	   11
	int   kidxA[9]   = { 0*4+0, 0*4+2, 2*4+0, 2*4+2, 1*4+0,	1*4+2,	0*4+1,	2*4+1, 1*4+1 };
	const float d256 = 1.0f / 256.0f;

	uint tmpMaddr = (uint)ge::GetVRamPointer (inTmpVAddr);
	Vec4 rec, tex;	

	float round_err = 0.0f;							// Cumul round alpha error to preserve the global brightness
	bool firstStep = TRUE;

	// Le centre (k11) � un differentiel de 0 et n'a pas besoin d'�tre calcul� !
	// donc kidx va de 0 � 8

	// flush and sync for copy from => to (only the first copy)
	*inDC->i32++ = SCE_GE_SET_TSYNC	();
	*inDC->i32++ = SCE_GE_SET_TFLUSH	();
	
	// Two passes :
	// - first for >0 kernel weights
	// - second for <0 kernel weights
	for( int sign = 0 ; sign < 2 ; sign++ )
	{
		for( int kidx = 0 ; kidx < 8 ; kidx++ ) {
			int   ki = kidxA[ kidx ] & 3;
			int   kj = kidxA[ kidx ] >> 2;
			float k  = inKernel.k[ kj ][ ki ];

			// >0 xor <0 kernel weights
			NV_ASSERT( (k<=0.0f) <= 1 );
			if( sign != (k<=0.0f) )	continue;

			float fa = ( k + round_err ) * 256.0f;
			int   ia = int( fa );
			round_err = ( fa - ia ) * d256;
			if( ia > 0 ) {					
				// Copy region to tmp
				*inDC->i32++ = SCE_GE_SET_ABE 	( 0 );
				SimpleCopyBuffer( 	inDC, inFromMAddr, inTmpVAddr, 
									inSizeX, inSizeY, inZ, CF_FilteringNearest,
									NULL, NULL );		
							
				// difference positive -> sub & store with color clamping
				rec.Set( 1-ki, 1-kj,inSizeX, inSizeY );
				tex.Set  ( 0, 0, inSizeX , inSizeY );				
				*inDC->i32++ = SCE_GE_SET_ABE 	( 1 );
				*inDC->i32++ = SCE_GE_SET_BLEND	(10,10,5);						
				CopyBuffer( inDC, inFromMAddr, inTmpVAddr, 
							inSizeX, inSizeY, inSizeX, inSizeY,
							rec, tex, inZ, CF_FilteringNearest | CF_TSync,
							NULL, NULL );		
												   
				// Cumul %(difference) in dest
				rec.Set  ( 0, 0, inSizeX, inSizeY );
				*inDC->i32++ = SCE_GE_SET_FIXA 	( ia,ia,ia );
				uint8 val = firstStep?0x0:0xFF;
				*inDC->i32++ = SCE_GE_SET_FIXB 	( val,val,val );
				if (!sign) 
					*inDC->i32++ = SCE_GE_SET_BLEND	(10,10,0);				
				else 
					*inDC->i32++ = SCE_GE_SET_BLEND	(10,10,1);				
				SimpleCopyBuffer( 	inDC, tmpMaddr, inToVAddr, 
									inSizeX, inSizeY, inZ, CF_FilteringNearest | CF_TSync,
									NULL, NULL );						
				firstStep = FALSE;
			}
		}
		// Global convolving is <= 0 ?
		if( firstStep )	return;
	}
}


void 
ge::tools::GaussianBlur(	dmac::Cursor*		inDC			,
							uint				inBlurVAddr		,
							uint				inTmpVAddr		,
							uint				inSizeX			,
							uint				inSizeY			,
							float				inRadius		,
							float				inQuality		)
{
						
	MipmapGC	mip_gc		[16];	
	uint 		tempVAddr	[6]	;
	uint		tempMAddr	[6]	;
	uint		tempSizeX	[6]	;
	uint		tempSizeY	[6]	;		
	Vec4 		rec				,
				tex				;
	
	// Computes Mipmaped Gaussian convolution interpolation	
	int mip_cpt = Compute_MipmapedGaussConvol( mip_gc, inSizeX, inSizeY, inRadius, inQuality );
	NV_ASSERT	( mip_cpt );
	NV_ASSERT	( mip_cpt <= 5 );

	tempVAddr[0] = inBlurVAddr;	
	tempMAddr[0] = (uint)ge::GetVRamPointer (inBlurVAddr);
	tempSizeX[0] = inSizeX;
	tempSizeY[0] = inSizeY;		
	// compute addr of tmp memory to store mipmap
	for( int mip = 1 ; mip <= mip_cpt ; mip++ ) {
		tempVAddr[mip] = inTmpVAddr;
		tempMAddr[mip] = (uint)ge::GetVRamPointer (inTmpVAddr);
		tempSizeX[mip] = inSizeX>>(mip);
		tempSizeY[mip] = inSizeY>>(mip);
		inTmpVAddr += RoundX( Round64(tempSizeX[mip]) * (tempSizeY[mip]) * 4 , 8 * 1024); // 8Ko alignment for frame buffer
	}		
	// Computes mipmaps
	for( int mip = 1 ; mip <= mip_cpt ; mip++ ) {
		rec.Set(0,0,tempSizeX[mip],tempSizeY[mip]);
		tex.Set(0,0,tempSizeX[mip-1],tempSizeY[mip-1]);
		CopyBuffer(	inDC, tempMAddr[mip-1], tempVAddr[mip], 
					tempSizeX[mip-1], tempSizeY[mip-1], tempSizeX[mip], tempSizeY[mip],
					rec, tex, 0, CF_FilteringLinear | CF_TSync,
					NULL, NULL );													
		// Smooth mipmap for better quality ?
		if( mip_gc[mip].quality ) {
			//Gs_SepConvolvePass_CH1( inDC, inSizeX>>mip, inSizeY>>mip, fromBAddr, toBAddr, smoothK, inFlags, inFBMSK, inZ );
		}
	}
	// Expand mipmaps with low-rez -> high-rez blending
	*inDC->i32++ = SCE_GE_SET_ABE			( 1 );
	// Blend Parameter : Csrc * FixA(cst) + Cdst * FixB(255 - cst)		
	*inDC->i32++ = SCE_GE_SET_BLEND			(10,10,0);							
	for( int mip = mip_cpt ; mip > 0 ; mip-- ) {
		mip_gc[mip].blend = mip_gc[mip].blend<<1;
		*inDC->i32++ = SCE_GE_SET_FIXA			(uint8(mip_gc[mip].blend),uint8(mip_gc[mip].blend),uint8(mip_gc[mip].blend));
		*inDC->i32++ = SCE_GE_SET_FIXB			(uint8(255-mip_gc[mip].blend),uint8(255-mip_gc[mip].blend),uint8(255-mip_gc[mip].blend));		
		rec.Set(0,0,tempSizeX[mip-1],tempSizeY[mip-1]);
		tex.Set(0,0,tempSizeX[mip],tempSizeY[mip]);
		CopyBuffer(	inDC,tempMAddr[mip],tempVAddr[mip-1], 
					tempSizeX[mip], tempSizeY[mip], tempSizeX[mip-1], tempSizeY[mip-1],
					rec, tex, 0, CF_FilteringLinear | CF_TSync | CF_TFlush,
					NULL, NULL );		
	}		
	*inDC->i32++ = SCE_GE_SET_ABE			( 0 );
}

void ge::tools::GlowBlur	(	dmac::Cursor*		inDC				,
								uint				inBlurVAddr			,
								uint				inTmpVAddr			,
								uint				inSizeX				,
								uint				inSizeY				,
								float				inRadius			,
								float				inQuality			)
{
	Kernel33 diffK;
	diffK.k00 = diffK.k02 = diffK.k20 = diffK.k22 = 0.7f;
	diffK.k01 = diffK.k21 = diffK.k10 = diffK.k12 = 1.0f;
	diffK.k11 = 0.0f;
	diffK.Normalize();
				
	MipmapGC	mip_gc		[16];
	uint 		tempVAddr	[7]	;
	uint		tempMAddr	[7]	;
	uint		tempSizeX	[7]	;
	uint		tempSizeY	[7]	;
	Vec4 		rec				,
				tex				;
	
	// Computes Mipmaped Gaussian convolution interpolation	
	int mip_cpt = Compute_MipmapedGaussConvol( mip_gc, inSizeX, inSizeY, inRadius, inQuality );
	NV_ASSERT( mip_cpt );
	NV_ASSERT( mip_cpt <= 5 );

	tempVAddr[0] = inBlurVAddr;	
	tempMAddr[0] = (uint)ge::GetVRamPointer (inBlurVAddr);
	tempSizeX[0] = inSizeX;
	tempSizeY[0] = inSizeY;		
	// compute addr of tmp memory to store mipmap
	for( int mip = 1 ; mip <= mip_cpt+1 ; mip++ ) {
		tempVAddr[mip] = inTmpVAddr;
		tempMAddr[mip] = (uint)ge::GetVRamPointer (inTmpVAddr);
		tempSizeX[mip] = inSizeX>>(mip);
		tempSizeY[mip] = inSizeY>>(mip);
		inTmpVAddr += RoundX( Round64(tempSizeX[mip]) * tempSizeY[mip] * 4 , 8 * 1024); // 8Ko alignment for frame buffer
	}		
	
	// Computes mipmaps
	for( int mip = 1 ; mip <= mip_cpt ; mip++ ) {
		rec.Set(0,0,tempSizeX[mip],tempSizeY[mip]);
		tex.Set(0,0,tempSizeX[mip-1],tempSizeY[mip-1]);
		CopyBuffer(	inDC, tempMAddr[mip-1], tempVAddr[mip], 
					tempSizeX[mip-1], tempSizeY[mip-1], tempSizeX[mip], tempSizeY[mip],
					rec, tex, 0, CF_FilteringLinear | CF_TSync,
					NULL, NULL );	
		
		// Dilatation
		uint tmpAddrForDiffConvol = tempVAddr[mip+1] + RoundX( Round64(tempSizeX[mip]) * tempSizeY[mip] * 4 , 8 * 1024);
		DiffConvolvePass ( 	inDC, tempMAddr[mip],tempVAddr[mip+1], tmpAddrForDiffConvol,
							tempSizeX[mip], tempSizeY[mip], diffK, 0);
									
		*inDC->i32++ = SCE_GE_SET_FIXA 	( 255,255,255 );
		*inDC->i32++ = SCE_GE_SET_FIXB 	( 255,255,255 );
		*inDC->i32++ = SCE_GE_SET_BLEND	( 10 , 10, 0  );										
		SimpleCopyBuffer(	inDC, tempMAddr[mip+1], tempVAddr[mip], 
							tempSizeX[mip], tempSizeY[mip], 0, CF_FilteringNearest | CF_TSync | CF_TFlush,
							NULL, NULL );
		*inDC->i32++ = SCE_GE_SET_ABE	(  0  );	
	}
	
	// Expand mipmaps with low-rez -> high-rez blending
	*inDC->i32++ = SCE_GE_SET_ABE			( 1 );
	// Blend Parameter : Csrc * FixA(cst) + Cdst * FixB(255 - cst)		
	*inDC->i32++ = SCE_GE_SET_BLEND			(10,10,0);							
	for( int mip = mip_cpt ; mip > 0 ; mip-- ) {
		mip_gc[mip].blend = mip_gc[mip].blend<<1;
		*inDC->i32++ = SCE_GE_SET_FIXA			(uint8(mip_gc[mip].blend),uint8(mip_gc[mip].blend),uint8(mip_gc[mip].blend));
		*inDC->i32++ = SCE_GE_SET_FIXB			(uint8(255-mip_gc[mip].blend),uint8(255-mip_gc[mip].blend),uint8(255-mip_gc[mip].blend));				
		rec.Set(0,0,tempSizeX[mip-1],tempSizeY[mip-1]);
		tex.Set(0,0,tempSizeX[mip],tempSizeY[mip]);
		CopyBuffer(	inDC,tempMAddr[mip],tempVAddr[mip-1], 
					tempSizeX[mip], tempSizeY[mip], tempSizeX[mip-1], tempSizeY[mip-1],
					rec, tex, 0, CF_FilteringLinear | CF_TSync | CF_TFlush,
					NULL, NULL );	
	}		
	*inDC->i32++ = SCE_GE_SET_ABE			( 0 );
}
