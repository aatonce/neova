/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/ALG/ALG.h>
using namespace nv;



namespace
{

	struct {
		uint32		beginT, endT;
		uint32		begin_beginT;
		uint32		syncUpdateBeginT, syncUpdateEndT, syncUpdateIdleT;
		uint32		prepareFlushBeginT, prepareFlushEndT;
		uint32		vsyncBeginT, vsyncEndT;
		uint32		flushBeginT, flushEndT;
		uint32		dmaBeginT, dmaEndT;
		uint32		waitDmaBeginT, waitDmaEndT;
		uint32		trxdTexBSize;
	} perf;


	struct PacketQ {
		frame::Packet*		pkt;
		// additional debug informations ...
		#ifdef _NVCOMP_ENABLE_DBG
		uint32*				madr;
		uint32*				eadr;
		#endif
	};

	typedef sysvector<PacketQ>			PacketQA;
	typedef sysvector<frame::Packet*>	PacketA;

	PacketQA		packetQ[2];
	PacketQA*		frontPktQ;
	PacketQA*		backPktQ;
	PacketA			sortQ;

	bool			async_mode;			// asynchronous ?
	pvoid			async_ch_cur;		// current async dma madr
	pvoid			async_ch_next;		// next async dma madr


	void	SafeSyncDMA		(	pcstr			inLabel,
								PacketQ*		inPktQ	= NULL	)
	{
		if( dmac::Sync() )
			return;

		char warMsg[64];
		Sprintf( warMsg, "<%s> stalls !", inLabel );
		NV_WARNING( warMsg );

		Printf( "\n" );
		Printf( "-- GE MARK: %d/%d\n", dmac::GetMark(), 0 );
		Printf( "\n" );

		if( inPktQ )
		{
			frame::Packet* pkt		= inPktQ->pkt;
			uint32*		   pkt_madr = NULL;
			uint32*		   pkt_eadr = NULL;
			#ifdef _NVCOMP_ENABLE_DBG
			pkt_madr = inPktQ->madr;
			pkt_eadr = inPktQ->eadr;
			#endif

			if( !dmac::IsValidMemRange(pkt,sizeof(frame::Packet)) )
			{
				Printf( "\n" );
				Printf( "-- packet: ERROR: Out of memory (addr=%08xh, madr=%08xh, eadr=%08xh, owner=%08xh, base=%08xh) !\n",
					uint32(pkt),
					uint32(pkt_madr),
					uint32(pkt_eadr),
					uint32(pkt->owner),
					pkt->owner ? uint32(pkt->owner->GetBase()) : 0 );
				Printf( "\n" );
			}
			else
			{
				Printf( "\n" );
				Printf( "-- packet: addr=%08xh, madr=%08xh, eadr=%08xh, owner=%08xh, base=%08xh\n",
					uint32(pkt),
					uint32(pkt_madr),
					uint32(pkt_eadr),
					uint32(pkt->owner),
					pkt->owner ? uint32(pkt->owner->GetBase()) : 0 );
				// dump madr to eadr
				if( pkt_madr && pkt_eadr )
					dmac::CheckChain( (void*)pkt_madr, (void*)pkt_eadr, TRUE );
				Printf( "\n" );
			}
		}
		else
		{
			Printf( "\n" );
			Printf( "-- packet: Not available !\n" );
			Printf( "\n" );
		}

		// Output the whole chain
		Printf( "\n" );
		Printf( "-- CH: %08xh\n", uint32(async_ch_cur) );
		Printf( "\n" );
		if( async_ch_cur )
			dmac::CheckChain( (void*)async_ch_cur, NULL, TRUE );

		NV_ERROR( "** DMA STALLS **" );
	}



	void
	Output_Pkt_Info	(	frame::Packet*	inPkt	)
	{
		if( !inPkt )
			return;

		char msg[256];
		msg[0] = 0;
		char* pmsg = msg;
		Sprintf( pmsg, "pkt 0x%08x: #ctxt:%d ", uint32(inPkt), inPkt->ctxtCpt );
		pmsg += Strlen( pmsg );

		// pkt texturing ?
		if( inPkt->ctxtA ) {
			Sprintf( pmsg, "texId={ " );
			pmsg += Strlen( pmsg );
			for( uint i = 0 ; i < inPkt->ctxtCpt ; i++ ) {
				int	texId = inPkt->ctxtA[i].texId;
				if(	texId < 0 )							continue;
				if( inPkt->ctxtA[i].texLODMask == 0 )	continue;
				Sprintf( pmsg, "%d ", texId );
				pmsg += Strlen( pmsg );
			}
			Sprintf( pmsg, "} " );
			pmsg += Strlen( pmsg );
		}

		Printf( "%s\n", msg );
	}



	//
	// SYNC update pending packet

	void SyncUpdatePkt()
	{
		perf.syncUpdateIdleT = 0;
		if( backPktQ->size() == 0 )
			return;

		PacketQ*		pkt_startP = backPktQ->data();
		PacketQ*		pkt_endP   = pkt_startP + backPktQ->size();
		PacketQ*		pktP       = pkt_startP;
		PacketQ* 		pkt_markP  = pkt_startP;
		frame::Packet*  pkt;

		// ge CH are finished ?
		if( !dmac::IsBusy() )
			pkt_markP = pkt_endP;

		while( pktP != pkt_endP )
		{
			// Sync width DMAC
			while( pkt_markP == pktP ) {
				uint32 syncT0  = GetCycleCpt();
				if( !dmac::WaitNextMark() )
					SafeSyncDMA( "SyncUpdatePkt()", pktP );
				perf.syncUpdateIdleT += GetCycleCpt() - syncT0;
				pkt_markP = pkt_startP + dmac::GetMark();
				NV_ASSERT( pkt_markP >= pktP );
				if( pkt_markP >= pkt_endP )
					pkt_markP = pkt_endP;
			}

			// Go go go
			while( pktP != pkt_markP ) {
				pkt = pktP->pkt;
				NV_ASSERT( pkt );

				// Setup flags
				NV_ASSERT( pkt->flags & frame::Packet::STT_DRAWING );
				if( pkt->flags & frame::Packet::STT_QUEUING ) {
					pkt->flags ^= frame::Packet::STT_QUEUING;
					if( (pkt->flags&frame::Packet::FLG_UPD_REQUESTED) && pkt->owner )
						pkt->owner->UpdatePacket( pkt );
				} else {
					pkt->flags ^= frame::Packet::STT_DRAWING;
				}

				pktP++;
			}
		}

		backPktQ->clear();
	}



	//
	// ASYNC FLUSH

	void*	PrepareASyncFlushQ	(	)
	{
		#ifdef _NVCOMP_ENABLE_DBG
		bool asyncLog = FALSE;
		core::GetParameter( core::PR_ASYNC_LOG, &asyncLog );
		if( asyncLog ) {
			Printf( "\n" );
			Printf( "\n" );
			Printf( "\n" );
			Printf( "\n" );
		}
		#endif

		perf.trxdTexBSize	= 0;
		if( frontPktQ->size() == 0 )
			return NULL;

		PacketQ*		pktP		= frontPktQ->data();
		PacketQ*		pkt_endP	= frontPktQ->data() + frontPktQ->size();
		uint			pkt_idx		= 0;
		frame::Packet*	pkt;

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );
		pvoid async_madr = DC->vd;
		dmac::InsertMark_CH( DC, 0 );	// Insert initial bench signal 0 !

		while( pktP != pkt_endP )
		{
			pkt = pktP->pkt;
			NV_ASSERT( pkt );

			#ifdef _NVCOMP_ENABLE_DBG
			pktP->madr = DC->i32;
			#endif

			NV_ASSERT_DC( DC );
			if( pkt->cadr ) {
				DC->i32[0] = SCE_GE_SET_BASE_BASE8( uint32(pkt->cadr) );
				DC->i32[1] = SCE_GE_SET_CALL( uint32(pkt->cadr) );
				DC->i32   += 2;
			} else if( pkt->owner ) {
				pkt->owner->GeneratePacket(	DC, pkt );
			}
			NV_ASSERT_DC( DC );

			#ifdef _NVCOMP_ENABLE_DBG
			pktP->eadr = DC->i32;
			#endif

			// Setup flags
			NV_ASSERT( pkt->flags & frame::Packet::STT_QUEUING );
			bool drawing = (pkt->flags & frame::Packet::STT_DRAWING);
			bool needupd = (pkt->flags & frame::Packet::FLG_UPD_REQUESTED) && pkt->owner;
			if( needupd )
				dmac::InsertMark_CH( DC, pkt_idx+1 );		// Insert signal mark !
			if( !drawing ) {
				pkt->flags |= frame::Packet::STT_DRAWING;
				pkt->flags ^= frame::Packet::STT_QUEUING;
				if( needupd )
					pkt->owner->UpdatePacket( pkt );
			}

			#ifdef _NVCOMP_ENABLE_DBG
			if( asyncLog )
				Output_Pkt_Info( pkt );
			#endif

			pkt_idx++;
			pktP++;
		}

		// Terminate CH
//		dmac::InsertMark_CH( DC, 0xFFFF );		// last mark=0xFFFF done by the dmac FINISH signal !
		DC->i32[0] = SCE_GE_SET_FINISH();
		DC->i32[1] = SCE_GE_SET_END();
		DC->i32   += 2;

		#ifdef _NVCOMP_ENABLE_DBG
		if( asyncLog ) {
			Printf( "----------------------\n" );
			Printf( "\n" );
			Printf( "\n" );
			Printf( "\n" );
			Printf( "\n" );
			core::SetParameter( core::PR_ASYNC_LOG, FALSE );
		}
		#endif

		return async_madr;
	}


	void ASyncFlushQ (	pvoid	inMadr	)
	{
		if( frontPktQ->size() == 0 )
			return;

		bool async_check = FALSE;
		bool async_break = FALSE;
		bool en_drawing	 = FALSE;
		bool consoleOnly = FALSE;
		core::GetParameter( core::PR_ASYNC_CHECK,		 &async_check );
		core::GetParameter( core::PR_ASYNC_BREAK,		 &async_break );
		core::GetParameter( core::PR_CONSOLE_ONLY,		 &consoleOnly );
		core::GetParameter( core::PR_ENABLE_GPU_DRAWING, &en_drawing  );

		// Check DMA-code ?
		if( async_check && inMadr ) {
			FlushDCache();
			if( !dmac::CheckChain((void*)inMadr,NULL,FALSE) )
				dmac::CheckChain( (void*)inMadr, NULL, TRUE );
		}

		perf.dmaBeginT = perf.dmaEndT = GetCycleCpt();
		if( inMadr && en_drawing && !consoleOnly )
		{
			FlushDCache();
			dmac::Start( inMadr );
			async_ch_cur = inMadr;
		}
		else
		{
			perf.trxdTexBSize = 0;
			async_ch_cur = NULL;
		}
	}



	//
	// SYNC FLUSH

	void SyncFlushQ()
	{
		if( frontPktQ->size() == 0 )
			return;

		perf.dmaBeginT = perf.dmaEndT = GetCycleCpt();
		perf.trxdTexBSize = 0;
		async_ch_cur = NULL;

		bool consoleOnly = FALSE;
		core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
		if( consoleOnly )
			return;

		bool enDrawing = FALSE;
		core::GetParameter( core::PR_ENABLE_GPU_DRAWING, &enDrawing );
		if( !enDrawing )
			return;

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );
		uint32* madr = DC->i32;

		PacketQ*		pktP     = frontPktQ->data();
		PacketQ*		pkt_endP = pktP + frontPktQ->size();
		frame::Packet*  pkt;

		while( pktP != pkt_endP )
		{
			pkt = pktP->pkt;
			NV_ASSERT( pkt );

			// Clear state flags
			pkt->flags &= ~(frame::Packet::STT_QUEUING | frame::Packet::STT_DRAWING);

			NV_ASSERT_DC( DC );
			if( pkt->cadr ) {
				DC->i32[0] = SCE_GE_SET_BASE_BASE8( uint32(pkt->cadr) );
				DC->i32[1] = SCE_GE_SET_CALL( uint32(pkt->cadr) );
				DC->i32   += 2;
			} else if( pkt->owner ) {
				pkt->owner->GeneratePacket(	DC, pkt );
			}
			NV_ASSERT_DC( DC );

			if(	(pkt->flags & frame::Packet::FLG_UPD_REQUESTED) && pkt->owner )
				pkt->owner->UpdatePacket( pkt );

			DC->i32[0] = SCE_GE_SET_FINISH();
			DC->i32[1] = SCE_GE_SET_END();
			DC->i32   += 2;

			FlushDCache();
			dmac::Start( madr );
			SafeSyncDMA( "SyncFlushQ()", pktP );
			DC->i32 = madr;

			pktP++;
		}

		NV_ASSERT_DC( DC );
		perf.dmaEndT = GetCycleCpt();
	}

}






void
frame::Init()
{
	packetQ[0].Init();
	packetQ[1].Init();
	sortQ.Init();
	Reset();
}



void
frame::Reset()
{
	dmac::Sync();

	packetQ[0].clear();
	packetQ[1].clear();
	sortQ.clear();

	frontPktQ		= & packetQ[0];
	backPktQ		= & packetQ[1];
	async_ch_cur	= NULL;
	async_ch_next	= NULL;
	async_mode		= TRUE;
}


void
frame::Shut()
{
	Reset();
	packetQ[0].Shut();
	packetQ[1].Shut();
	sortQ.Shut();
}


void
frame::Begin(	bool		inASync		)
{
	uint cycT			= GetCycleCpt();
	perf.begin_beginT	= cycT - perf.beginT;
	perf.beginT			= cycT;
	Swap( frontPktQ, backPktQ );
	frontPktQ->clear();
	sortQ.clear();
	async_mode    = inASync;
	async_ch_next = NULL;
}


void
frame::Push(	Packet*		inPkt	)
{
	if( !inPkt )							return;
	NV_ASSERTC( inPkt->cadr || inPkt->owner, "NULL packet !" );		// cadr OR owner is required !
	if( !inPkt->cadr && !inPkt->owner )		return;

#if defined( _NVCOMP_ENABLE_DBG )
	uint32 breakPktAddr = 0;
	core::GetParameter( core::PR_ALG_DMAC_BREAK_ON_PKT, &breakPktAddr );
	if( breakPktAddr && breakPktAddr==uint32(inPkt) ) {
		char warnMsg[64];
		Sprintf( warnMsg, "Break on packet 0x%08 [owner=0x%08x, base=0x%08x]\n",
			breakPktAddr,
			uint32(inPkt->owner),
			inPkt->owner ? uint32(inPkt->owner->GetBase()) : 0 );
		NV_WARNING_BREAK( warnMsg );
	}
#endif

	inPkt->flags |= Packet::STT_QUEUING;

	// Separator ?
	if( inPkt->flags & Packet::FLG_SEPARATOR ) {
		Sort();
		sortQ.push_back( inPkt );
		Sort();
	} else {
		// Default HKEY ?
		if( !inPkt->hkey )
			inPkt->BuildBasicHKey();
		sortQ.push_back( inPkt );
	}
}


void
frame::Sort	(	)
{
	uint S = sortQ.size();
	if( S == 0 )
		return;

	// pkt hkey Radix in SPR
	// TODO !

	// push pkts
	PacketQ pktq;
	Zero( pktq );
	for( uint i = 0 ; i < S ; i++ ) {
		pktq.pkt = sortQ[i];
		frontPktQ->push_back( pktq );
	}

	sortQ.clear();
}


void
frame::End()
{
	Sort();
	perf.endT = GetCycleCpt();
}


void
frame::WaitDMA()
{
	if( dmac::IsBusy() ) {
		SafeSyncDMA( "WaitDMA" );
		perf.dmaEndT = GetCycleCpt();
	}
}


void
frame::Sync		(			)
{
	// Prepare async flush ?
	perf.prepareFlushBeginT = GetCycleCpt();
	if( async_mode )
		async_ch_next = PrepareASyncFlushQ();
	perf.prepareFlushEndT = GetCycleCpt();

	// Sync drawing packets ?
	perf.syncUpdateBeginT = GetCycleCpt();
	if( async_mode || async_ch_cur )
		SyncUpdatePkt();
	perf.syncUpdateEndT   = GetCycleCpt();

	perf.waitDmaBeginT = GetCycleCpt();
	WaitDMA();
	perf.waitDmaEndT = GetCycleCpt();
	async_ch_cur = NULL;
}


void
frame::Flush	(	bool	inVSync,
					bool	inShowFront		)
{
	// vsync ?
	perf.vsyncBeginT = perf.vsyncEndT = GetCycleCpt();
	if( inVSync ) {
		ge::crtc::VSync(1);
		perf.vsyncEndT = GetCycleCpt();
	}

	if( inShowFront )
		ge::crtc::ShowFront();

	// Flush !
	perf.flushBeginT = GetCycleCpt();
	if( async_mode )	ASyncFlushQ( async_ch_next );
	else				SyncFlushQ();
	perf.flushEndT = GetCycleCpt();
}




float
frame::GetPerformance		(	Performance	inPerf		)
{
	uint32 cyct = 0;

	if( inPerf == PERF_BEGIN_END )			cyct = perf.endT - perf.beginT;
	if( inPerf == PERF_BEGIN_BEGIN )		cyct = perf.begin_beginT;
	if( inPerf == PERF_SYNC_UPDATE )		cyct = perf.syncUpdateEndT - perf.syncUpdateBeginT;
	if( inPerf == PERF_SYNC_UPDATE_IDLE )	cyct = perf.syncUpdateIdleT;
	if( inPerf == PERF_PREPARE_FLUSH )		cyct = perf.prepareFlushEndT - perf.prepareFlushBeginT;
	if( inPerf == PERF_VSYNC_IDLE )			cyct = perf.vsyncEndT - perf.vsyncBeginT;
	if( inPerf == PERF_FLUSH )				cyct = perf.flushEndT - perf.flushBeginT;
	if( inPerf == PERF_DMA )				cyct = perf.dmaEndT - perf.dmaBeginT;
	if( inPerf == PERF_WAIT_DMA )			cyct = perf.waitDmaEndT - perf.waitDmaBeginT;
	if( inPerf == PERF_TRXD_TEXBSIZE )		return float(int(perf.trxdTexBSize));

	// Use benched dmac time instead !
	if( inPerf == PERF_DMA )
		return dmac::GetPerfTime();

	return GetCycleCptTime( cyct );
}




void
frame::Packet::Init		(	Agent*		inOwner		)
{
	hkey				= 0;
	enterContextMask	= ~0U;
	leaveContextMask	= ~0U;
	owner				= inOwner;
	cadr				= NULL;
	ctxtA				= NULL;
	ctxtCpt				= 0;
	flags				= 0;

#if defined( _NVCOMP_ENABLE_DBG )
	uint32 breakPktAddr = 0;
	core::GetParameter( core::PR_ALG_DMAC_BREAK_ON_PKT, &breakPktAddr );
	if( breakPktAddr && breakPktAddr==uint32(this) ) {
		char warnMsg[64];
		Sprintf( warnMsg, "Break on packet 0x%08 !\n", breakPktAddr );
		NV_WARNING_BREAK( warnMsg );
	}
#endif
}


void
frame::Packet::BuildBasicHKey	(		)
{
	// TODO
	hkey = ~0U;
}


bool
frame::Packet::IsTextured	(		)
{
	if( !ctxtA )
		return FALSE;
	for( uint i = 0 ; i < ctxtCpt ; i++ ) {
		if(	ctxtA[i].texId < 0 )	continue;
		if( !ctxtA[i].cadr )		continue;
		return TRUE;		// At least one !
	}
	return FALSE;
}


void
frame::GeContext::Init	(		)
{
	texId		= -1;
	texLODMask	= ~0U;
	cadr		= &cmd;
	NV_ASSERT_A32( cadr );

	// Init with default ocntext
	Zero( cmd );
	cmd.tflush_or_ret	= SCE_GE_SET_RET();
	cmd.suv[0]			= SCE_GE_SET_SU( ExtFp24(1.f) );
	cmd.suv[1]			= SCE_GE_SET_SV( ExtFp24(1.f) );
	cmd.tuv[0]			= SCE_GE_SET_TU( ExtFp24(0.f) );
	cmd.tuv[1]			= SCE_GE_SET_TV( ExtFp24(0.f) );
	cmd.ret				= SCE_GE_SET_RET();
}



