/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/ALG/ALG.h>
using namespace nv;

#define	MAX_MIPMAP_NB		4U

namespace
{
	sysvector<tram::TexReg>		texRegA;
	int							texRegFirstFree;
}



void		
tram::Init				(										)
{
	texRegA.Init();
	Reset();	
}


void		
tram::Reset				(										)
{
	texRegA.clear();
	texRegFirstFree = -1;
}


void		
tram::Shut				(										)
{
	texRegA.Shut();
}


//
// TexDesc Register / Release / Build / Translate

int			
tram::RegisterDesc		(	TexDesc*		inDesc				)
{
	if( !inDesc )
		return -1;

	// Valid ?
	NV_ASSERT( inDesc->psm == SCE_GE_TPF_IDTEX4 || inDesc->psm == SCE_GE_TPF_IDTEX8 || inDesc->psm == SCE_GE_TPF_8888);
	NV_ASSERT( inDesc->mipmapCpt   > 0 );
	NV_ASSERT( inDesc->L2_W        > 0 );
	NV_ASSERT( inDesc->L2_H 	   > 0 );
	NV_ASSERT( inDesc->resBSize	   > 0 );

	if(		(		inDesc->psm!=SCE_GE_TPF_IDTEX4
				&&	inDesc->psm!=SCE_GE_TPF_IDTEX8 
				&&  inDesc->psm!=SCE_GE_TPF_8888)
		||	inDesc->mipmapCpt == 0
		||	inDesc->L2_W == 0
		||	inDesc->L2_H == 0
		||	inDesc->resBSize == 0)
		return -1;

	// No reg free ?
	if( texRegFirstFree == -1 )
	{
		uint N = texRegA.size();
		uint P = N+64;
		texRegA.resize( P );
		texRegFirstFree = N;
		for( uint i = N ; i < P ; i++ ) {
			texRegA[i].desc		 = NULL;
			texRegA[i].next_free = i+1;
		}
		texRegA[P-1].next_free = -1;
	}

	// Registration
	int id = texRegFirstFree;
	TexReg * reg = & texRegA[id];
	texRegFirstFree		= reg->next_free;
	reg->desc			= inDesc;
	reg->allBmpBSize	= 0;
	for( uint i = 0 ; i < 8 ; i++ )
		reg->allBmpBSize += inDesc->trx[i].bitmapBSize;

	// Check mipmaping ?
	#if defined( _NVCOMP_ENABLE_DBG )
	uint32 checkMipMode;
	uint32 checkMipLods;
	core::GetParameter( core::PR_CHECK_MIPMAPPING_MODE, &checkMipMode );
	core::GetParameter( core::PR_CHECK_MIPMAPPING_LODS, &checkMipLods );
	if( checkMipMode )
		TestMipmaping( inDesc, checkMipMode-1, checkMipLods );
	#endif
	return id;
}

int			
tram::FindDesc			(	TexDesc*		inDesc				)
{
	if( !inDesc )
		return -1;

	uint N = texRegA.size();
	for( uint i = 0 ; i < N ; i++ ) {
		if( texRegA[i].desc == inDesc )
			return int(i);
	}
	
	return -1;
}

tram::TexReg*		
tram::GetRegistered		(	int				inTexId				)
{
	if( inTexId < 0 )
		return NULL;
	NV_ASSERT( inTexId < int(texRegA.size()) );
	if(	inTexId < int(texRegA.size()) )
		return & texRegA[ inTexId ];
	else
		return NULL;
}

void		
tram::ReleaseDesc		(	int				inTexId				)
{
	if( inTexId >= 0 )
	{
		NV_ASSERT( inTexId < int(texRegA.size()) );
		NV_ASSERT( texRegA[ inTexId ].desc  );
		if(		inTexId < int(texRegA.size())
			&&	texRegA[ inTexId ].desc	)
		{
			TexReg* reg = & texRegA[ inTexId ];
			reg->desc		= NULL;
			reg->next_free	= texRegFirstFree;
			texRegFirstFree = inTexId;
		}
	}
}

tram::TexReg*		
tram::GetRegistrationA	(	uint*			outSize		 ,
							int*			outFirstFree 	)
{
	if( outSize )
		*outSize = texRegA.size();

	if( outFirstFree )
		*outFirstFree = texRegFirstFree;

	return texRegA.data();	
}
									

bool		
tram::BuildDesc			(	TexDesc*		outDesc,
							uint			inPSM,					
							uint			inL2W,					
							uint			inL2H,					
							pvoid			inCLUTPtr	,		
							pvoid			inTexelPtr	)
{
	NV_ASSERT(outDesc);
	NV_ASSERT(inPSM==SCE_GE_TPF_IDTEX8 || inPSM==SCE_GE_TPF_8888);
	if ( inPSM==SCE_GE_TPF_IDTEX8){
		NV_ASSERT(inCLUTPtr);
	}
	NV_ASSERT(inTexelPtr);
	NV_ASSERT(inL2W >= 3 && inL2H >= 3 && inL2W <= 9 && inL2H <= 9);
	if (!outDesc 		 			) return false;
	if (inPSM!=SCE_GE_TPF_IDTEX8 && 
		inPSM!=SCE_GE_TPF_8888 		) return false;
	if (inPSM==SCE_GE_TPF_8888 && 
		inCLUTPtr 					) return false;
	if (inPSM!=SCE_GE_TPF_8888 && 
		!inCLUTPtr					) return false;
	if (!inTexelPtr					) return false;
	if (inL2W < 3 || inL2H < 3	||	
		inL2W > 9 || inL2H > 9		) return false;
	
	outDesc->flags		 = 0;
	outDesc->psm		 = inPSM;
	outDesc->L2_W		 = inL2W;
	outDesc->L2_H		 = inL2H;
	outDesc->mipmapCpt	 = 1;
	
	// init clut if needed
	if (inCLUTPtr) {
		NV_ASSERT_A128(inCLUTPtr);
		uint NbColor = 256; //inPSM == SCE_GE_TPF_IDTEX8
		outDesc->trx[0].bitmapBSize	=	NbColor*4;	// in Bytes	RGBA 8.8.8.8
		outDesc->trx[0].TBW			=	1;			// Invalid Paramater for CLUT
		outDesc->trx[0].bitmapData	=	(uint8 * ) inCLUTPtr;
	}
	else {
		outDesc->trx[0].bitmapBSize	=	0;
		outDesc->trx[0].TBW			=	0;
		outDesc->trx[0].bitmapData	=	NULL;
	}
	
	// init image data
	NV_ASSERT_A128(	inTexelPtr );	// Data must be aligned	
	uint tsizeW = (1 << inL2W);		// Texture size in pixel
	uint tsizeH = (1 << inL2H);
	// compute data buffer width and size
	if (inPSM == SCE_GE_TPF_IDTEX8){
		// width must be a multiple of 16
		outDesc->trx[1].TBW				=	((tsizeW<16)? 16 : tsizeW);
		outDesc->trx[1].bitmapBSize		=	tsizeH * outDesc->trx[1].TBW; //in Bytes
	}
	else { //inPSM4 == SCE_GE_TPF_8888
		// width must be a multiple of 4 but image size is greater than 2^3
		outDesc->trx[1].TBW				=	tsizeW;
		outDesc->trx[1].bitmapBSize		=	tsizeH * outDesc->trx[1].TBW * 4; //in Bytes
	}
	outDesc->trx[1].bitmapData =	(uint8*)inTexelPtr;		
	
	for( uint i = 2 ; i <= MAX_MIPMAP_NB ; ++i) {
			outDesc->trx[i].TBW				=	0;
			outDesc->trx[i].bitmapBSize		=	0; 		//in Bytes
			outDesc->trx[i].bitmapData 		= 	NULL;		
	}

	// Init Ge Instructions 
	if (inCLUTPtr) {
		outDesc->geCmd.cbp   = SCE_GE_SET_CBP	((uint)outDesc->trx[0].bitmapData );
		outDesc->geCmd.cbw   = SCE_GE_SET_CBW	(((uint) (outDesc->trx[0].bitmapData) >>24) & 0x0F );
		outDesc->geCmd.clut  = SCE_GE_SET_CLUT	(SCE_GE_CLUT_CPF_8888,0,0xFF,0);
		
		//loading Palettes 8 by 8,
		outDesc->geCmd.cload = SCE_GE_SET_CLOAD	(((outDesc->trx[0].bitmapBSize / 4) / 8) +  
												((((outDesc->trx[0].bitmapBSize / 4) % 8) != 0)? 1 : 0 ));	 									
	}
	else {
		outDesc->geCmd.cbp = outDesc->geCmd.cbw = outDesc->geCmd.clut = outDesc->geCmd.cload = SCE_GE_CMD_NOP;
	}

	
	outDesc->geCmd.tbp[0]   	= SCE_GE_SET_TBPn		(0,(uint)outDesc->trx[1].bitmapData );
	outDesc->geCmd.tbw[0]		= SCE_GE_SET_TBWn		(0,outDesc->trx[1].TBW,((uint)outDesc->trx[1].bitmapData) >>24 );	
	outDesc->geCmd.tsize[0] 	= SCE_GE_SET_TSIZEn		(0,outDesc->L2_W,outDesc->L2_H);	
	for (uint i = 1; i < MAX_MIPMAP_NB; ++i){ // cmd for trx[i+1] with trx[0]=>clut
		outDesc->geCmd.tbp[i]   	= SCE_GE_CMD_NOP;
		outDesc->geCmd.tbw[i]		= SCE_GE_CMD_NOP;	
		outDesc->geCmd.tsize[i] 	= SCE_GE_CMD_NOP;
	}

	outDesc->geCmd.tpf 		= SCE_GE_SET_TPF( outDesc->psm, 0 );
	outDesc->geCmd.tmode 	= SCE_GE_SET_TMODE(0, 0, 0 );
	outDesc->resBSize 		= sizeof(TexDesc);

	return TRUE;
}
									
									
void		
tram::DMATranslateDesc	(	TexDesc*		ioDesc,
							uint32			inAddrOffset		)
{
	for( int i = 0 ; i < 5 ; ++i ) {
		uint8** bmpAddr = &ioDesc->trx[i].bitmapData;
		(*bmpAddr) += inAddrOffset;
		uint32* xbp = (i==0) ? &ioDesc->geCmd.cbp : &ioDesc->geCmd.tbp[i-1];
		uint32* xbw = (i==0) ? &ioDesc->geCmd.cbw : &ioDesc->geCmd.tbw[i-1];
		*xbp = InsBits( *xbp, uint32(*bmpAddr),     0, 24 );
		*xbw = InsBits( *xbw, uint32(*bmpAddr)>>24, 16, 8 );
	}	
}
									

//
// Test the texture mipmaping usage
// Mode :
// 0 => Paint by LOD's index
// 1 => Paint by LOD's sup_pow2( MAX(w,h) )
// 2 => Paint by LOD's sup_pow2( MIN(w,h) )
// 3 => Paint by LOD's sup_pow2( (w+h)/2 )

bool		
tram::TestMipmaping		(	TexDesc*		inDesc,
							uint			inMode,
							uint			inLODMask			)
{
	if( inDesc->mipmapCpt == 1 )
		return FALSE;
	uint allLodMask	= (1<<inDesc->mipmapCpt) - 1;
	inLODMask &= allLodMask;
	if( inLODMask == 0 )
		return FALSE;

	//						ABGR
	uint32 mipColor[] = {	0xFF888888,
							0xFF0000FF,			// 512
							0xFF00FF00,			// 256
							0xFFFF0000,			// 128
							0xFF00FFFF,			// 64
							0xFFFFFF00,			// 32
							0xFFFF00FF,			// 16
							0xFFFFFFFF,			// 8
							0xFF40FF80,			// 4
							0xFFFF4080	};		// 2

	// Rewrite CLUT ?
	if( inDesc->psm == SCE_GE_TPF_IDTEX4 || inDesc->psm == SCE_GE_TPF_IDTEX8) {
		uint32* clutPtr = (uint32*) inDesc->trx[0].bitmapData;
		for( uint i = 0 ; i < sizeof(mipColor)/sizeof(uint32) ; i++ )
			clutPtr[i] = mipColor[i];
	}

	for( uint i = 0 ; i < inDesc->mipmapCpt ; i++ )
	{
		if( ((1<<i)&inLODMask) == 0 )
			continue;
		uint v;
		if( inMode == 1 )		v = 10 - Max( inDesc->L2_W-i, inDesc->L2_H-i );
		else if( inMode == 2 )	v = 10 - Min( inDesc->L2_W-i, inDesc->L2_H-i );
		else if( inMode == 3 )	v = 10 - (inDesc->L2_W-i+inDesc->L2_H-i)/2;
		else					v = i+1;

		uint8* map = inDesc->trx[i+1].bitmapData;
		uint   N   = inDesc->trx[i+1].bitmapBSize;
		if( inDesc->psm == SCE_GE_TPF_IDTEX4 ) {
			v = (v<<4)|v;
			while( N-- )
				*map++ = v;
		} else if( inDesc->psm == SCE_GE_TPF_IDTEX8 ) {
			while( N-- )
				*map++ = v;
		} else { // SCE_GE_TPF_8888
			uint32* map32 = (uint32*) map;
			uint    N32   = N / 4;
			while( N32-- )
				*map32++ = mipColor[v];
		}
	}

	return TRUE;	
}

