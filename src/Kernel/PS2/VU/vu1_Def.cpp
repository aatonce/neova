/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <Kernel/PS2/NvkSurface[PS2].h>
#include <Kernel/PS2/VU/vu1_Def.h>
using namespace vu1;



#define VU1_REGISTER_MC( _NAME )	vu::RegisterMC( 1, _NAME##_CodeStart, _NAME##_CodeEnd )




int vu1::FindXYZPackIPartLen( float	inRadius )
{
	if( inRadius <= 0.f )
		return -1;
	const int XYZ_PACK_IPART_MAX = 12;
	int ipartLen = 0;
	while( float(1<<ipartLen) < inRadius )
		ipartLen += 1;
	if( ipartLen > XYZ_PACK_IPART_MAX )
		return -1;
	return ipartLen;
}


void*
vu1::BuildViewContextList	(	dmac::Cursor*			DC,
								uint					inSizeof,
								int						inChainIdx,
								uint					inFlags,
								float					inXYZUnpack		)
{
	NV_ASSERT_DC( DC );
	NV_ASSERT( inChainIdx >= 0 );
	NV_COMPILE_TIME_ASSERT( (sizeof(ViewContext)&15)==0 );
	NV_COMPILE_TIME_ASSERT( (sizeof(ToLitFilters)&15)==0 );
	NV_COMPILE_TIME_ASSERT( (sizeof(ToLitContext)&15)==0 );
	NV_COMPILE_TIME_ASSERT( (sizeof(ToLitViewContext)&15)==0 );

	NV_ASSERT( inSizeof );
	NV_ASSERT( (inSizeof&15)==0 );
	NV_ASSERT( inSizeof <= (VU1_CONTEXT_QS*16) );
	if( inSizeof > (VU1_CONTEXT_QS*16) )
		return NULL;
	if( (inSizeof&15)!=0 )
		return NULL;

	NvDpyContext::ContextChain*	dchainA = DpyManager::context->contextChainA.data();
	NvDpyContext::Context*		dctxtA  = DpyManager::context->contextA.data();
	NvDpyContext::View*			dviewA  = DpyManager::context->viewA.data();
	NvDpyContext::Light*		dlightA = DpyManager::context->lightA.data();
	Matrix*						dwtrA   = DpyManager::context->wtrA.data();

	void* ctxt0 = DC->vd;
	ViewContext* ctxt  = (ViewContext*) ctxt0;
	uint					draster;
	NvDpyContext::Context*  dctxt;
	NvDpyContext::View*		dview;
	Matrix*					dwtr;
	NvDpyContext::Light*    dlight;
	while( inChainIdx >= 0 )
	{
		dctxt   = dctxtA + dchainA[inChainIdx].contextIdx;
		draster = dctxt->raster;
		dview   = dviewA  + dctxt->viewIdx;
		dlight  = dlightA + dctxt->lightIdx;
		dwtr    = dwtrA   + dctxt->wtrIdx;

		// param
		ctxt->xyzUnpack	= inXYZUnpack;

		if( inFlags & (BCL_TOPROJ|BCL_TOVIEW|BCL_PROJ) )
		{
			// toproj*
			if( inFlags & BCL_TOPROJ ) {
				if( inFlags & BCL_THROUGHMODE ) {
					DpyManager::raster[ draster ].GetViewport( 0, dview->viewport, ctxt->toprojScale, ctxt->toprojTrans );
					MatrixCopy( &ctxt->toprojMatrix, dwtr );
				} else {
					DpyManager::raster[ draster ].GetViewport( 1, dview->viewport, ctxt->toprojScale, ctxt->toprojTrans );
					Matrix toclipTR;	// cam^-1 * proj * clip
					MatrixMul( &toclipTR, &dview->viewTR, &dview->projTR );
					MatrixMul( &toclipTR, &toclipTR, &DpyManager::clipMatrix );
					MatrixMul( &ctxt->toprojMatrix, dwtr, &toclipTR );
				}
			}

			// toviewMatrix
			if( inFlags & BCL_TOVIEW ) {
				MatrixMul( &ctxt->toviewMatrix, dwtr, &dview->viewTR );
			}

			// projMatrix
			if( inFlags & BCL_PROJ ) {
				MatrixMul( &ctxt->projMatrix, &dview->projTR, &DpyManager::clipMatrix );
			}
		}

		// VP
		if( inFlags & BCL_VP ) {
			ctxt->vpFrameReg	= DpyManager::raster[ draster ].rgbaFrameReg;
			ctxt->vpXYOffsetReg	= DpyManager::raster[ draster ].rgbaXYOffsetReg;
			ctxt->vpZBufReg		= DpyManager::raster[ draster ].depthZBufReg;
			ctxt->vpScissorReg	= SCE_GS_SET_SCISSOR(	int(dview->viewport.x),
														int(dview->viewport.x)+int(dview->viewport.z)-1,
														int(dview->viewport.y),
														int(dview->viewport.y)+int(dview->viewport.w)-1 );
		}

		// tl.dotMatrix
		if( inFlags & BCL_LIGHT_M ) {
			// local dirs
			Matrix iwtr, wtrn;
			MatrixNormalize( &wtrn, dwtr );		// to prevent scaled lighting
			MatrixFastInverse( &iwtr, &wtrn );
			Vec3 ldir[2];
			Vec3ApplyVector( &ldir[0], &dlight->direction[0], &iwtr );
			Vec3ApplyVector( &ldir[1], &dlight->direction[1], &iwtr );

			// lights matrix
			ToLitViewContext* tl_ctxt = (ToLitViewContext*) ctxt;
			tl_ctxt->tl.dotMatrix[0].x = - ldir[0].x;
			tl_ctxt->tl.dotMatrix[1].x = - ldir[0].y;
			tl_ctxt->tl.dotMatrix[2].x = - ldir[0].z;
			tl_ctxt->tl.dotMatrix[0].y = - ldir[1].x;
			tl_ctxt->tl.dotMatrix[1].y = - ldir[1].y;
			tl_ctxt->tl.dotMatrix[2].y = - ldir[1].z;
		}

		// tl.filters (without modulation)
		if( inFlags & BCL_LIGHT_F ) {
			ToLitViewContext* tl_ctxt = (ToLitViewContext*) ctxt;
			Vec4Copy( tl_ctxt->tl.filters.colors+0, dlight->color+0 );
			Vec4Copy( tl_ctxt->tl.filters.colors+1, dlight->color+1 );
			Vec4Copy( tl_ctxt->tl.filters.colors+2, dlight->color+2 );
		}

		ctxt = (ViewContext*)( uint32(ctxt) + inSizeof );
		inChainIdx = dchainA[inChainIdx].next;
	}

	DC->vd = ctxt;
	NV_ASSERT_DC( DC );
	return ctxt0;
}



void*
vu1::BuildLightFiltersList	(	dmac::Cursor*			DC,
								uint					inSizeof,
								int						inChainIdx,
								uint					inNbFilters,
								Vec4*					inAmbientFilter,
								Vec4*					inDiffuseFilter,
								uint					inFilterStride	)
{
	NV_ASSERT_DC( DC );
	NV_ASSERT( inChainIdx >= 0 );
	NV_COMPILE_TIME_ASSERT( (sizeof(ToLitFilters)&15)==0 );
	NV_ASSERT( inFilterStride==0 || (inFilterStride&3)==0 );

	NV_ASSERT( inSizeof );
	NV_ASSERT( (inSizeof&15)==0 );
	NV_ASSERT( inSizeof <= (VU1_CONTEXT_QS*16) );
	if( inSizeof > (VU1_CONTEXT_QS*16) )
		return NULL;
	if( (inSizeof&15)!=0 )
		return NULL;

	if( !inAmbientFilter || !inDiffuseFilter ) {
		inAmbientFilter = (Vec4*) &Vec4::ONE;
		inDiffuseFilter = (Vec4*) &Vec4::ONE;
		inFilterStride  = 0;
	}
	if( (inFilterStride&3)!=0 )
		return NULL;

	NvDpyContext::ContextChain*	dchainA = DpyManager::context->contextChainA.data();
	NvDpyContext::Context*		dctxtA  = DpyManager::context->contextA.data();
	NvDpyContext::Light*		dlightA = DpyManager::context->lightA.data();

	void* lf0 = DC->vd;
	ToLitFilters* lf = (ToLitFilters*) lf0;
	NvDpyContext::Context*  dctxt;
	NvDpyContext::Light*    dlight;
	for( uint i = 0 ; i < inNbFilters ; i++ ) {
		int chainIdx = inChainIdx;
		while( chainIdx >= 0 ) {
			dctxt   = dctxtA + dchainA[chainIdx].contextIdx;
			dlight  = dlightA + dctxt->lightIdx;
			Vec4Mul( lf->colors+0, dlight->color+0, inAmbientFilter );
			Vec4Mul( lf->colors+1, dlight->color+1, inDiffuseFilter );
			Vec4Mul( lf->colors+2, dlight->color+2, inDiffuseFilter );
			lf = (ToLitFilters*)( uint32(lf) + inSizeof );
			chainIdx = dchainA[chainIdx].next;
		}
		inAmbientFilter = (Vec4*)( uint32(inAmbientFilter) + inFilterStride );
		inDiffuseFilter = (Vec4*)( uint32(inDiffuseFilter) + inFilterStride );
	}

	DC->vd = lf;
	NV_ASSERT_DC( DC );
	return lf0;
}




namespace
{

	inline
	void
	vu1_Begin_CH1		(	dmac::Cursor*		DC,
							uint				inBASE,
							uint				inOFFSET	)
	{
		NV_ASSERT_DC( DC );
		NV_ASSERT( (inBASE+inOFFSET*2) <= 1024 );

		DC->i64[0] = DMA_TAG_CNT | 1;
		DC->i32[2] = SCE_VIF1_SET_FLUSH( 0 );
		DC->i32[3] = SCE_VIF1_SET_STMOD( 0, 0 );
		DC->i32[4] = SCE_VIF1_SET_STCYCL( 1, 1, 0 );
		DC->i32[5] = SCE_VIF1_SET_BASE( inBASE, 0 );
		DC->i32[6] = SCE_VIF1_SET_OFFSET( inOFFSET, 0 );
		DC->i32[7] = 0;
		DC->i128  += 2;
	}

	inline
	void
	vu1_End_CH1			(	dmac::Cursor*		DC				)
	{
		NV_ASSERT_DC( DC );
		DC->i64[0] = DMA_TAG_END;
		DC->i64[1] = 0;			// !! reserved for the frame pkt manager !!
		DC->i128  += 1;
	}

	inline
	void
	vu1_UnpackDirect_CH1	(	dmac::Cursor*		DC,
								frame::GsContext*	inGsRegs,
								uint				inQAddr,
								bool				inNeedFlush = FALSE		)
	{
		NV_ASSERT_DC( DC );
		NV_ASSERT_A128( inGsRegs );
		NV_ASSERT( inQAddr <= (1024-16) );
		NV_COMPILE_TIME_ASSERT( sizeof(frame::GsContext) == 12*16 );
		NV_COMPILE_TIME_ASSERT( VU1_DIRECT_QS >= 16 );

		DC->i32[0] = DMA_TAG_REF | 12;
		DC->i32[1] = DMA_ADDR( inGsRegs );
		DC->i32[2] = inNeedFlush ? SCE_VIF1_SET_FLUSH(0) : 0;
		DC->i32[3] = SCE_VIF1_SET_UNPACK_U( inQAddr, 16, UNPACK_V3_32, 0 );
		DC->i128  += 1;
	}

	inline
	void
	vu1_UnpackContext_CH1	(	dmac::Cursor*		DC,
								void*				inData,
								uint				inQAddr,
								uint				inQSize,
								bool				inNeedFlush	= FALSE		)
	{
		NV_ASSERT_DC( DC );
		NV_ASSERT_A128( inData );
		NV_ASSERT( inQAddr <= (1024-inQSize) );
		NV_ASSERT( inQSize <= VU1_CONTEXT_QS );

		DC->i32[0] = DMA_TAG_REF | inQSize;
		DC->i32[1] = DMA_ADDR( inData );
		DC->i32[2] = inNeedFlush ? SCE_VIF1_SET_FLUSH(0) : 0;
		DC->i32[3] = SCE_VIF1_SET_UNPACK( inQAddr, inQSize, UNPACK_V4_32, 0 );
		DC->i128  += 1;
	}

}


#include "vu1_Def_Mesh.h"
#include "vu1_Def_Surf.h"
#include "vu1_Def_Poser.h"
#include "vu1_Def_Cartoon.h"
#include "vu1_Def_Mapping.h"



