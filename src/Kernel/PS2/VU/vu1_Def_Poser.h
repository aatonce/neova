/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




IMPORT_VU_CODE_NAME( VU1_POSEMESH_CULL				)


namespace
{

	namespace psr
	{

		enum {
			// ITOP flags is cccc|dd|nnnn
			// cccc: components flags as DISPLACEMENT|RGBA32|SCALE|ROTATION (LOCATION is implicit)
			// dd  : DO_... flags
			// nnnn: #pose to output in [1, VU1_POSEMESH_POSE_NB]
			//		 special value nnnn=0 is used as no output needed (=> b SWITCH)
			DO_RELOAD		=	(1<<4),
			DO_DIRECT		=	(1<<5),
			// mapping
			CTXT_QA				= (VU1_NO_DCACHE_CTXT_QA),
			DIRECT_QA			= (VU1_NO_DCACHE_DIRECT_QA),
			POSE_NB				= (VU1_DMA_STRIDE*2),
			POSE_QS				= (5),
			POSELIST_QS			= (POSE_NB*POSE_QS),
			POSELIST_0			= (DIRECT_QA+VU1_DIRECT_QS),
			POSELIST_1			= (POSELIST_0+POSELIST_QS),
			BUFF_QA				= (POSELIST_1+POSELIST_QS),
			BUFF_QS				= ((1024-BUFF_QA)/3),		// 3 buffers
			DATA_QA				= (BUFF_QA+0*BUFF_QS),		// intput data buffer
			OUT0_QA				= (BUFF_QA+1*BUFF_QS),		// output buffer 0
			OUT1_QA				= (BUFF_QA+2*BUFF_QS),		// output buffer 1
			BASE_QA				= (POSELIST_0),
			OFFSET_QS			= (POSELIST_QS)
		};

		int					mesh_mcId;
		uint				itop;
		poser::DataType		dt;

		void		Init()
		{
			NV_COMPILE_TIME_ASSERT( (sizeof(vu1::poser::Context)&15)==0 );
			NV_COMPILE_TIME_ASSERT( sizeof(vu1::poser::Context) <= (VU1_CONTEXT_QS*16) );

			static bool done = FALSE;
			if( done )	return;
			done = TRUE;
			mesh_mcId	= VU1_REGISTER_MC( VU1_POSEMESH_CULL		);
		}
	}

}



uint
vu1::poser::GetDataMaxQS		(										)
{
	return psr::BUFF_QS;
}


int
vu1::poser::Probe_MC			(	ClipMode			inClipMode,
									DataType			inDataType		)
{
	psr::Init();
	return (inDataType==DT_PRELIT_MESH) ? psr::mesh_mcId : -1;
}


vu1::poser::Context*
vu1::poser::BuildContextList	(	dmac::Cursor*			DC,
									int						inChainIdx,
									uint					inMode,
									float					inFadInnerD,
									float					inFadOuterD,
									float					inXYZUnpack		)
{
	NV_ASSERT_DC( DC );
	NV_COMPILE_TIME_ASSERT( (sizeof(Context)&15)==0 );
	NV_ASSERT( inChainIdx >= 0 );

	NvDpyContext::ContextChain*	dchainA = DpyManager::context->contextChainA.data();
	NvDpyContext::Context*		dctxtA  = DpyManager::context->contextA.data();
	NvDpyContext::View*			dviewA  = DpyManager::context->viewA.data();
	Matrix*						dwtrA   = DpyManager::context->wtrA.data();

	Context* ctxt0 = (Context*) DC->i8;
	Context* ctxt  = ctxt0;
	while( inChainIdx >= 0 ) {
		NvDpyContext::Context*	dctxt   = dctxtA + dchainA[inChainIdx].contextIdx;
		NvDpyContext::View*		dview   = dviewA + dctxt->viewIdx;
		Matrix*					dwtr    = dwtrA  + dctxt->wtrIdx;
		uint					draster = dctxt->raster;

		DpyManager::raster[ draster ].GetViewport( 1, dview->viewport, ctxt->view.toprojScale, ctxt->view.toprojTrans );
		MatrixMul( &ctxt->view.projMatrix, &dview->projTR, &DpyManager::clipMatrix );	// proj * clip
		MatrixMul( &ctxt->view.toviewMatrix, dwtr, &dview->viewTR );					// wtr  * view

		// VP
		ctxt->view.vpFrameReg		= DpyManager::raster[ draster ].rgbaFrameReg;
		ctxt->view.vpXYOffsetReg	= DpyManager::raster[ draster ].rgbaXYOffsetReg;
		ctxt->view.vpZBufReg		= DpyManager::raster[ draster ].depthZBufReg;
		ctxt->view.vpScissorReg		= SCE_GS_SET_SCISSOR( int(dview->viewport.x), int(dview->viewport.x)+int(dview->viewport.z)-1, int(dview->viewport.y), int(dview->viewport.y)+int(dview->viewport.w)-1 );

		// mode
		ctxt->pose.mode = inMode;

		// Fading(z,Inner,Outer) => ax+b=0
		// projected -z in [-1,+1]
		// output fading alpha in [0,128]
		float projInnerZ = DpyManager::GetZProjNormalised( inFadInnerD, &dview->projTR );
		float projOuterZ = DpyManager::GetZProjNormalised( inFadOuterD, &dview->projTR );
		NV_ASSERTC( projOuterZ > projInnerZ, "Coordinate system error !" );
		float ooProjDZ   = 128.0f / ( projOuterZ - projInnerZ );
		ctxt->pose.fadingA = - ooProjDZ;
		ctxt->pose.fadingB = projOuterZ * ooProjDZ;

		// XYZ Unpack
		ctxt->view.xyzUnpack	= inXYZUnpack;

		ctxt++;
		inChainIdx = dchainA[inChainIdx].next;
	}

	DC->vd = ctxt;
	NV_ASSERT_DC( DC );
	return ctxt0;
}


int
vu1::poser::Begin_CH1			(	dmac::Cursor*		DC,
									ClipMode			inClipMode,
									DataType			inDataType				)
{
	if( inDataType != DT_PRELIT_MESH )
		return -1;	// not supported yet !

	NV_ASSERT_DC( DC );

	DC->i64[0] = DMA_TAG_CNT | 0;
	DC->i32[2] = SCE_VIF1_SET_FLUSH(0);
	DC->i32[3] = SCE_VIF1_SET_ITOP( 0, 0 );		// set ITOP for the first (#pose=0 => b SWITCH)
	DC->i128  += 1;

	psr::itop = 0;
	psr::dt   = inDataType;

	return Probe_MC( inClipMode, inDataType );
}


void
vu1::poser::SetContext_CH1		(	dmac::Cursor*		DC,
									Context*			inContext				)
{
	vu1_UnpackContext_CH1( DC, inContext, psr::CTXT_QA, sizeof(Context)>>4 );
	psr::itop |= psr::DO_RELOAD;
}


void
vu1::poser::SetDirect_CH1		(	dmac::Cursor*		DC,
									frame::GsContext*	inGsRegs				)
{
	vu1_UnpackDirect_CH1( DC, inGsRegs, psr::DIRECT_QA );
	psr::itop |= psr::DO_DIRECT;
}


void
vu1::poser::SetData_CH1			(	dmac::Cursor*		DC,
									uint32				inAddr,
									uint				inQSize					)
{
	NV_ASSERT_DC( DC );
	NV_ASSERT( psr::dt == DT_PRELIT_MESH );
	NV_ASSERT_A128( inAddr );
	NV_ASSERT( inQSize );

	// Unpack prelit mesh data (MSCNT + reset STCYCL + reset STMOD)
	DC->i32[0] = DMA_TAG_REF | inQSize;
	DC->i32[1] = DMA_ADDR( inAddr );
	DC->i32[2] = SCE_VIF1_SET_BASE( psr::DATA_QA, 0 );
	DC->i32[3] = SCE_VIF1_SET_OFFSET( 0, 0 );
	DC->i128  += 1;
}


void
vu1::poser::CallPoseList_CH1	(	dmac::Cursor*		DC,
									uint32				inPoseListAddr,
									uint				inITOPFlags			)
{
	NV_ASSERT_DC( DC );
	NV_ASSERT_A128( inPoseListAddr );

	// Call poselist
	uint32* pl_itop = (uint32*)inPoseListAddr;
	NV_ASSERT_A128( pl_itop+4 );
	DC->i32[0] = DMA_TAG_CALL | 0;
	DC->i32[1] = DMA_ADDR( pl_itop+4 );
	DC->i64[1] = SCE_VIF1_SET_ITOP( psr::itop|inITOPFlags|pl_itop[0], 0 );	// + first poselist packet itop
	DC->i128  += 1;

	// reset itop flags
	psr::itop = 0;
}


void
vu1::poser::End_CH1				(	dmac::Cursor*		DC						)
{
	vu1_End_CH1( DC );
}


bool
vu1::poser::BuildPoseList_CH1	(	uint32&				outPoseListAddr,
									dmac::Cursor*		DC,
									uint				inStart,
									uint				inSize,
									Vec3*				inLocA,
									Quat*				inRotA,
									Vec3*				inSclA,
									uint32*				inColA,
									Vec3*				inDisplacA				)
{
	if( !inLocA )
		return FALSE;

	NV_ASSERT_DC( DC );
	NV_ASSERT( inLocA );
	NV_ASSERT( inSize );
	NV_ASSERT( (inStart%(VU1_DMA_STRIDE)) == 0 );
	NV_ASSERT( ((psr::POSE_NB)%(VU1_DMA_STRIDE)) == 0 );

	uint ccccITOP  = 0;
	if( inRotA )		ccccITOP |= (1<<6);
	if( inSclA )		ccccITOP |= (2<<6);
	if( inColA )		ccccITOP |= (4<<6);
	if( inDisplacA )	ccccITOP |= (8<<6);

	outPoseListAddr = uint32( DC->vd );
	uint32* pl_itop   = DC->i32+0;
	uint32* pl_pktcpt = DC->i32+1;
	uint32* pl_cccc   = DC->i32+2;
	*pl_pktcpt = 0;
	*pl_cccc   = ccccITOP;
	DC->i128 += 1;

	// Setup poselist DBF
	DC->i64[0] = DMA_TAG_CNT | 0;
	DC->i32[2] = SCE_VIF1_SET_BASE( psr::BASE_QA, 0 );
	DC->i32[3] = SCE_VIF1_SET_OFFSET( psr::OFFSET_QS, 0 );
	DC->i128  += 1;

	while( inSize )
	{
		uint procSize = Min( inSize, uint(psr::POSE_NB) );

		{
			NV_ASSERT_A128( inLocA+inStart );
			DC->i32[0] = DMA_TAG_REF | ((psr::POSE_NB*12)>>4);
			DC->i32[1] = DMA_ADDR( inLocA+inStart );
			DC->i32[2] = SCE_VIF1_SET_STCYCL( 1, psr::POSE_QS, 0 );
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 0, psr::POSE_NB, UNPACK_V3_32, 0 );
			DC->i128  += 1;
		}

		if( inRotA ) {
			NV_ASSERT_A128( inRotA+inStart );
			DC->i32[0] = DMA_TAG_REF | (psr::POSE_NB);
			DC->i32[1] = DMA_ADDR( inRotA+inStart );
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 1, psr::POSE_NB, UNPACK_V4_32, 0 );
			DC->i128  += 1;
		}

		if( inSclA ) {
			NV_ASSERT_A128( inSclA+inStart );
			DC->i32[0] = DMA_TAG_REF | ((psr::POSE_NB*12)>>4);
			DC->i32[1] = DMA_ADDR( inSclA+inStart );
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 2, psr::POSE_NB, UNPACK_V3_32, 0 );
			DC->i128  += 1;
		}

		if( inColA ) {
			NV_ASSERT_A128( inColA+inStart );
			DC->i32[0] = DMA_TAG_REF | ((psr::POSE_NB*4)>>4);
			DC->i32[1] = DMA_ADDR( inColA+inStart );
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 3, psr::POSE_NB, UNPACK_V4_8, 0 );
			DC->i128  += 1;
		}

		if( inDisplacA ) {
			NV_ASSERT_A128( inDisplacA+inStart );
			DC->i32[0] = DMA_TAG_REF | ((psr::POSE_NB*12)>>4);
			DC->i32[1] = DMA_ADDR( inDisplacA+inStart );
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 4, psr::POSE_NB, UNPACK_V3_32, 0 );
			DC->i128  += 1;
		}

		uint itop = ccccITOP | procSize;
		if( *pl_pktcpt==0 ) {
			// For the first packet, ITOP is set the the poselist caller
			DC->i64[0] = DMA_TAG_CNT | 0;
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_MSCNT( 0 );
			DC->i128  += 1;
			*pl_itop = itop;	// backup the first itop
		} else {
			// For the next packets, only fields cccc && nnnn are needed.
			DC->i64[0] = DMA_TAG_CNT | 0;
			DC->i32[2] = SCE_VIF1_SET_ITOP( itop, 0 );
			DC->i32[3] = SCE_VIF1_SET_MSCNT( 0 );
			DC->i128  += 1;
		}

		inStart    += procSize;
		inSize     -= procSize;
		*pl_pktcpt += 1;
	}

	DC->i64[0] = DMA_TAG_RET | 0;
	DC->i64[1] = 0;
	DC->i128  += 1;

	return TRUE;
}



