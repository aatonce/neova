/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




IMPORT_VU_CODE_NAME( VU1_SURF_PRELIT_FAST			)
IMPORT_VU_CODE_NAME( VU1_SURF_PRELIT_CULL			)
IMPORT_VU_CODE_NAME( VU1_SURF_PRELIT_CLIP			)
IMPORT_VU_CODE_NAME( VU1_SURF_TOLIT_FAST			)
IMPORT_VU_CODE_NAME( VU1_SURF_TOLIT_CULL			)
IMPORT_VU_CODE_NAME( VU1_SURF_TOLIT_CLIP			)


namespace
{

	namespace bs
	{
		enum {
			// ITOP flags
			DO_RELOAD			= (1<<0),
			DO_DIRECT			= (1<<1),
			// mapping
			CTXT_QA				= (VU1_NO_DCACHE_CTXT_QA),
			DIRECT_QA			= (VU1_NO_DCACHE_DIRECT_QA),
			BASE_QA				= (VU1_NO_DCACHE_BASE_QA),
			OFFSET_QS			= (VU1_NO_DCACHE_OFFSET_QS)
		};

		int			prelit_mcId[3];
		int			tolit_mcId[3];
		uint		itop;
		bool		tolit;
		bool		needFlush;

		void		Init()
		{
			NV_COMPILE_TIME_ASSERT( ((BASE_QA)+2*(OFFSET_QS)) <= 1024 );
			NV_COMPILE_TIME_ASSERT( (sizeof(vu1::surface::Context)&15)==0 );
			NV_COMPILE_TIME_ASSERT( sizeof(vu1::surface::Context) <= (VU1_CONTEXT_QS*16) );

			static bool done = FALSE;
			if( done )	return;
			done = TRUE;
			prelit_mcId[0]	= VU1_REGISTER_MC( VU1_SURF_PRELIT_FAST		);
			prelit_mcId[1]	= VU1_REGISTER_MC( VU1_SURF_PRELIT_CULL		);
			prelit_mcId[2]	= VU1_REGISTER_MC( VU1_SURF_PRELIT_CLIP		);
			tolit_mcId[0]	= VU1_REGISTER_MC( VU1_SURF_TOLIT_FAST		);
			tolit_mcId[1]	= VU1_REGISTER_MC( VU1_SURF_TOLIT_CULL		);
			tolit_mcId[2]	= VU1_REGISTER_MC( VU1_SURF_TOLIT_CLIP		);
		}
	}

}



int
vu1::surface::Probe_MC		(	ClipMode				inClipMode,
								bool					inToLit					)
{
	bs::Init();
	if( inToLit )
		return bs::tolit_mcId[inClipMode];
	else
		return bs::prelit_mcId[inClipMode];
}



bool
vu1::surface::BuildData_CH1		(	uint32&				outUploaderAddr,
									dmac::Cursor*		DC,
									uint				inStart,
									uint				inSize,
									Vec4*				inLocA,
									Vec2*				inTexA,
									uint32*				inColA,
									Vec3*				inNrmA					)
{
	NV_ASSERT( inLocA );
	NV_ASSERT( inSize );
	NV_ASSERT_DC( DC );
	NV_ASSERT( (inStart%VU1_DMA_STRIDE) == 0 );

	outUploaderAddr = uint32( DC->vd );
	uint32* pkt_cpt = DC->i32;
	*pkt_cpt = 0;
	DC->i128 += 1;

	uint UNPACK_VSIZE = ((bs::OFFSET_QS/2)-1)/3;		// -HEADER, loc, tex, col XOR nrm
	uint OUTPUT_VSIZE = ((bs::OFFSET_QS/2)-1)/3;		// -GIFTAG
	uint PACKET_VSIZE = Min( UNPACK_VSIZE, OUTPUT_VSIZE );

	// Must to be a multiple of 2 for vcl <LoopCS 2, 0>
	// and must to be a multiple of stride for DMA_TAG_REF !
	PACKET_VSIZE -= (PACKET_VSIZE & 1);
	PACKET_VSIZE -= (PACKET_VSIZE & (VU1_DMA_STRIDE-1));
	NV_ASSERT( PACKET_VSIZE > 0 );
	NV_ASSERT( PACKET_VSIZE <= UNPACK_VSIZE );
	NV_ASSERT( PACKET_VSIZE <= PACKET_VSIZE );

	// Unpack
	uint16* EOS   = NULL;
	uint    mscnt = 0;
	while( inSize ) {
		uint packetLen = Min( PACKET_VSIZE, inSize );
		uint unpackLen = RoundX( packetLen, VU1_DMA_STRIDE );

		// HEADER <#vertices>
		DC->i64[0] = DMA_TAG_CNT | 1;
		DC->i64[1] = mscnt;
		DC->i32[4] = SCE_VIF1_SET_STCYCL( 1, 3, 0 );
		DC->i32[5] = SCE_VIF1_SET_UNPACK_RU( 0, 1, UNPACK_V4_16, 0 );
		DC->i16[12]= packetLen | 0x8000;					// .x = GIFTAG NLOOP
		DC->i16[13]= Max( Round2(packetLen), 4U ) * 3;		// .y = #proc*3 (LoopCS 2 => multiple de 2 sup. et au moins 4)
		DC->i32[7] = 0;										// .w = EOS continue flag
		DC->i128  += 2;
		EOS		   = DC->i16 - 1;

		// XYZ
		{
			NV_ASSERT_A128( inLocA+inStart );
			DC->i32[0] = DMA_TAG_REF | packetLen;			// *16/16
			DC->i32[1] = DMA_ADDR( inLocA+inStart );
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 1, packetLen, UNPACK_V4_32, 0 );
			DC->i128  += 1;
		}

		// ST
		if( inTexA )
		{
			NV_ASSERT_A128( inTexA+inStart );
			DC->i32[0] = DMA_TAG_REF | ((unpackLen*8)>>4);
			DC->i32[1] = DMA_ADDR( inTexA+inStart );
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 2, unpackLen, UNPACK_V2_32, 0 );
			DC->i128  += 1;
		}

		// RGBA
		if( inColA )
		{
			NV_ASSERT_A128( inColA+inStart );
			DC->i32[0] = DMA_TAG_REF | ((unpackLen*4)>>4);
			DC->i32[1] = DMA_ADDR( inColA+inStart );
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 3, unpackLen, UNPACK_V4_8, 0 );
			DC->i128  += 1;
		}
		// NORMAL
		else if( inNrmA )
		{
			NV_ASSERT_A128( inNrmA+inStart );
			DC->i32[0] = DMA_TAG_REF | ((unpackLen*12)>>4);
			DC->i32[1] = DMA_ADDR( inNrmA+inStart );
			DC->i32[2] = 0;
			DC->i32[3] = SCE_VIF1_SET_UNPACK_RU( 3, unpackLen, UNPACK_V3_32, 0 );
			DC->i128  += 1;
		}

		inSize   -= packetLen;
		inStart  += packetLen;
		*pkt_cpt += 1;
		mscnt = SCE_VIF1_SET_MSCNT( 0 );
	}

	if( *pkt_cpt == 0 )
		return FALSE;

	// End of surface flag
	NV_ASSERT( EOS );
	*EOS = 1;

	DC->i64[0] = DMA_TAG_RET | 0;
	DC->i32[2] = SCE_VIF1_SET_MSCNT(0);
	DC->i32[3] = SCE_VIF1_SET_STCYCL(1,1,0);
	DC->i128  += 1;

	return TRUE;
}


bool
vu1::surface::BuildData_CH1		(	uint32&				outUploaderAddr,
									dmac::Cursor*		DC,
									uint				inStart,
									uint				inSize,
									NvkSurface*			inSurf					)
{
	if( !inSurf || !inSize )
		return FALSE;
	Vec4*	locData = (Vec4*)	inSurf->GetComponentBase( NvkSurface::BF_BACK, NvSurface::CO_LOC );
	Vec2*	texData = (Vec2*)	inSurf->GetComponentBase( NvkSurface::BF_BACK, NvSurface::CO_TEX );
	uint32*	colData = (uint32*)	inSurf->GetComponentBase( NvkSurface::BF_BACK, NvSurface::CO_COLOR );
	Vec3*	nrmData = (Vec3*)	inSurf->GetComponentBase( NvkSurface::BF_BACK, NvSurface::CO_NORMAL );
	NV_ASSERT( locData );
	if( !locData )
		return FALSE;
	return BuildData_CH1( outUploaderAddr, DC, inStart, inSize, locData, texData, colData, nrmData );
}


int
vu1::surface::Begin_CH1			(	dmac::Cursor*		DC,
									ClipMode			inClipMode,
									bool				inToLit					)
{
	vu1_Begin_CH1( DC, bs::BASE_QA, bs::OFFSET_QS );
	bs::tolit	  = inToLit;
	bs::itop	  = 0;
	bs::needFlush = FALSE;
	return Probe_MC( inClipMode, inToLit );
}


void
vu1::surface::SetContext_CH1	(	dmac::Cursor*		DC,
									Context*			inContext,
									ToLitFilters*		inTLFilters	)
{
	if( inContext ) {
		vu1_UnpackContext_CH1( DC, inContext, bs::CTXT_QA, sizeof(Context)>>4, bs::needFlush );
		bs::needFlush = FALSE;
	}

	if( bs::tolit && inTLFilters ) {
		const uint lc_QA = bs::CTXT_QA + MQOFFSET( Context, tl_view.tl.filters );
		vu1_UnpackContext_CH1( DC, inTLFilters, lc_QA, sizeof(ToLitFilters)>>4, bs::needFlush );
		bs::needFlush = FALSE;
	}

	bs::itop |= bs::DO_RELOAD;
}


void
vu1::surface::SetDirect_CH1		(	dmac::Cursor*		DC,
									frame::GsContext*	inGsRegs				)
{
	vu1_UnpackDirect_CH1( DC, inGsRegs, bs::DIRECT_QA, bs::needFlush );
	bs::itop      |= bs::DO_DIRECT;
	bs::needFlush  = FALSE;
}


void
vu1::surface::CallData_CH1		(	dmac::Cursor*		DC,
									uint32				inUploaderAddr,
									uint				inITOPFlags			)
{
	NV_ASSERT_DC( DC );
	NV_ASSERT( inUploaderAddr );

	// 4 uint32 header
	uint32* pkt_cpt = (uint32*)inUploaderAddr;
	NV_ASSERT( *pkt_cpt >= 1 );
	NV_ASSERT_A128( pkt_cpt+4 );

	DC->i32[0] = DMA_TAG_CALL | 0;
	DC->i32[1] = DMA_ADDR( pkt_cpt+4 );
	DC->i64[1] = SCE_VIF1_SET_ITOP( bs::itop|inITOPFlags, 0 );
	DC->i128  += 1;

	// if packetCpt > 1 then xgkick is already done & flush is not needed !
	bs::needFlush = (*pkt_cpt==1);

	// reset itop flags
	bs::itop = 0;
}


void
vu1::surface::End_CH1			(	dmac::Cursor*		DC						)
{
	vu1_End_CH1( DC );
}


