/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#define	_USING_DCACHE
#define	_USING_LIGHT
#include <Kernel/PS2/VU/vu1_Mapping.h>
#include <Kernel/PS2/VU/vu1_Tools.h>




		.name		VU1_MESH_TOLIT_CULL


		.vcl
		.vcl_optim_loop
		.vcl_semicolon_is_newline
		.init_vf_all
		.init_vi_all
		.syntax new


		--enter
		in_hw_clip		clip
		in_vi			ZMSK0							// original direct sceGsZbuf.ZMSK
		in_vf			CTE
		in_vf			TOPROJM0
		in_vf			TOPROJM1
		in_vf			TOPROJM2
		in_vf			TOPROJM3
		in_vf			TOPROJS
		in_vf			TOPROJT
		in_vf			LIGHTM0
		in_vf			LIGHTM1
		in_vf			LIGHTM2
		in_vf			LIGHTA
		in_vf			LIGHTD1
		in_vf			LIGHTD2
		in_vf			STOFFSET
		--endenter


		BUILD_ONES()
		BUILD_ADC_CONSTANTS( CLIP_CTE )

		// CTE = { NXYZ-unpack, 255, 128, xyzUnpack }
		loi				VU1_UNPACK_NXYZ;	maxi.x	CTE,	vf00,	i
		loi				255;				maxi.y	CTE,	vf00,	i
		loi				128;				maxi.zw	CTE,	vf00,	i

		--cont	// SYNC



SWITCH:
		xitop			doMask
		ibeq			doMask, vi00, END_SWITCH				// do something ?

		// DIRECT changed ?
		iaddiu			directA, vi00, DIRECT
		jump_if_unset( doMask, 1, noDirect )
		ilw.y			ZMSK0,		4(directA)					// load original sceGsZbuf.ZMSK
noDirect:

		// setup viewport with context
		ilw.z			zmsk,		CTXT_VP+1(vi00)
		ior				zmsk,		zmsk, ZMSK0					// viewport-zmsk | direct-ZMSK0
		lq				reg0,		CTXT_VP+0(vi00)
		lq				reg2,		CTXT_VP+1(vi00)
		mr32			reg1,		reg0
		mr32			reg1,		reg1
		mr32			reg3,		reg2
		mr32			reg3,		reg3
		mfir.y			reg3,		zmsk
		sq.x			reg0,		1(directA)					// sceGsFrame (no FBMSK overwrite !)
		sq.xy			reg1,		2(directA)					// sceGsXyoffset
		sq.xy			reg2,		3(directA)					// sceGsScissor
		sq.xy			reg3,		4(directA)					// sceGsZbuf  (ORed zmsk !)
		xgkick			directA									// PATH1 direct & write PRIM !

		// load context
		lq				TOPROJM0,	CTXT_TOPROJ_M+0(vi00)
		lq				TOPROJM1,	CTXT_TOPROJ_M+1(vi00)
		lq				TOPROJM2,	CTXT_TOPROJ_M+2(vi00)
		lq				TOPROJM3,	CTXT_TOPROJ_M+3(vi00)
		lq				TOPROJS,	CTXT_TOPROJ_S(vi00)
		lq				TOPROJT,	CTXT_TOPROJ_T(vi00)
		lq				LIGHTM0,	CTXT_LIGHT_M+0(vi00)
		lq				LIGHTM1,	CTXT_LIGHT_M+1(vi00)
		lq				LIGHTM2,	CTXT_LIGHT_M+2(vi00)
		lq				LIGHTA,		CTXT_LIGHT_C+0(vi00)
		lq				LIGHTD1,	CTXT_LIGHT_C+1(vi00)
		lq				LIGHTD2,	CTXT_LIGHT_C+2(vi00)
		ilw.z			tex0_addr,	10(directA)
		loi_if_equ( tex0_addr, 0x06, 128, 255, tme )			// i = tme ? 128 : 255
		muli			LIGHTD1,	LIGHTD1, i					// [0,1] => [0,i]
		muli			LIGHTD2,	LIGHTD2, i					// [0,1] => [0,i]
		muli			LIGHTA,		LIGHTA,  i					// [0,1] => [0,i]
		minix.w			LIGHTD1,	vf00,	vf00				// a =0
		minix.w			LIGHTD2,	vf00,	vf00				// a =0
		mulz.w			LIGHTA,		vf00,	CTE					// a = 128
		lq.w			CTE,		CTXT_PARAM(vi00)
		lq.xy			STOFFSET,	14(directA)
		loi				VU1_UNPACK_ST
		subi.xy			STOFFSET,	STOFFSET,	i
END_SWITCH:



C1_proc:
		xtop			unpackPtr
		iaddiu			xgkickPtr, unpackPtr, OUTPUT_OFFSET
		ilw.x			nloop,		0(unpackPtr)
		ilw.y			nproc,		0(unpackPtr)				// multiple de 2 pour LoopCS !
		ilw.z			noutput,	0(unpackPtr)				// multiple de 2 pour LoopCS !
		ilw.w			EOS,		0(unpackPtr)
		iaddiu			unpackPtr, unpackPtr, 1

		mr32			stq@,		vf00
		mr32			stq@@,		vf00						// stq@ = stq@@ = <0,0,1,0>

		iaddiu			outPtr,		  xgkickPtr,	1
		iadd			outPtrEnd,	  outPtr,		noutput
		iadd			unpackPtrEnd, unpackPtr,    nproc
		ibeq			unpackPtr, unpackPtrEnd,	C1_out_loop


		// PROCESSING

C1_proc_loop:
		--LoopCS 1, 0

		lq				xyzu@,	0(unpackPtr)					// <= <x,y,z,->
		lq				nxyz@,	1(unpackPtr)					// <= <nx,ny,nz,procQA>
		iaddiu			unpackPtr, unpackPtr, 2

		subw.xyz		xyzu@,	 xyzu@,		CTE					// unpack xyz
		subx.xyz		nxyz@,	 nxyz@,		CTE					// unpack nxyz
		mtir			procQA@, nxyz@[w]

		mulax			acc,	TOPROJM0,	xyzu@
		madday			acc,	TOPROJM1,	xyzu@
		maddaz			acc,	TOPROJM2,	xyzu@
		maddw			hloc@,	TOPROJM3,	vf00
		sq				hloc@,	0(procQA@)						// => <hx,hy,hz,hw>

		mulax			acc,	LIGHTM0,	nxyz@
		madday			acc,	LIGHTM1,	nxyz@
		maddz			scalars@,LIGHTM2,	nxyz@
		maxx			scalars@,scalars@,	vf00				// minimize [in 0,1]
		mulax			acc,	LIGHTD1,	scalars@
		madday			acc,	LIGHTD2,	scalars@
		maddw			rgba@,	LIGHTA,		vf00
		miniy			rgba@,  rgba@,		CTE					// maximize [in 0, 255]
		ftoi0			rgba@,	rgba@
		sq				rgba@,	1(procQA@)						// => <r,g,b,a=0x80>

		.vcl_loop_copy
		ibne			unpackPtr, unpackPtrEnd, C1_proc_loop


		// OUTPUTING

C1_out_loop:
		--LoopCS 1, 0

		lqi				fcache@,	(unpackPtr++)				// <= <s,t,procQA,strip-ADC>

		mtir			procQA@, fcache@[z]
		lq				hloc@,	0(procQA@)
		lq				rgba@,	1(procQA@)

		clipw			hloc@,	hloc@
		fcand			vi01,	0x03FFFF
		lq.w			ploc@,	CLIP_CTE(vi01)
		add.w			ploc@,	ploc@,		fcache@				// ADC

		div				q, 		vf00[w],	hloc@[w]
		add.xy			stq@,	fcache@,	STOFFSET
		mulq			pstq@,	stq@,		q

		mulq.xyz		ploc@,	hloc@,		q
		mula.xyz		acc,	TOPROJS,	ploc@
		maddw.xyz		ploc@,	TOPROJT,	vf00

		sq				pstq@,	0(outPtr)
		sq				rgba@,	1(outPtr)
		sq				ploc@,	2(outPtr)
		iaddiu			outPtr, outPtr, 3

		.vcl_loop_copy
		ibne			outPtr, outPtrEnd, C1_out_loop


		// KICKING

		BUILD_CNT_GIFTAG( GIFTAG_cnt, nloop )
		sq				GIFTAG_cnt,	0(xgkickPtr)
		xgkick			xgkickPtr


		--cont		// SYNC
		ibeq			EOS, vi00, C1_proc
		b				SWITCH


	--exit
	--endexit



