/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



IMPORT_VU_CODE_NAME( VU1_CARTOON_CULL				)



namespace
{

	namespace toon
	{

		enum {
			// ITOP flags
			DO_RELOAD			= (1<<0),
			DO_DIRECT			= (1<<1),
			CTXT_QA				= (VU1_WITH_DCACHE_CTXT_QA),
			DIRECT_QA			= (VU1_WITH_DCACHE_DIRECT_QA),
			BASE_QA				= (VU1_WITH_DCACHE_BASE_QA),
			OFFSET_QS			= (VU1_WITH_DCACHE_OFFSET_QS)
		};

		int			mesh_mcId[3];
		uint		itop;
		bool		needFlush;

		void		Init()
		{
			NV_COMPILE_TIME_ASSERT( (sizeof(vu1::cartoon::ContextToon)&15) == 0 );
			NV_COMPILE_TIME_ASSERT( (sizeof(vu1::cartoon::Context)&15) == 0 );
			NV_COMPILE_TIME_ASSERT( sizeof(vu1::cartoon::Context) <= (VU1_CONTEXT_QS*16) );

			static bool done = FALSE;
			if( done )	return;
			done = TRUE;
			mesh_mcId[0] = VU1_REGISTER_MC( VU1_CARTOON_CULL	);
			mesh_mcId[1] = VU1_REGISTER_MC( VU1_CARTOON_CULL	);
			mesh_mcId[2] = VU1_REGISTER_MC( VU1_CARTOON_CULL	);
		}
	}

}




int
vu1::cartoon::Probe_MC	(	ClipMode			inClipMode		)
{
	toon::Init();
	return toon::mesh_mcId[ int(inClipMode) ];
}



int
vu1::cartoon::Begin_CH1		(	dmac::Cursor*			DC,
								ClipMode				inClipMode		)
{
	vu1_Begin_CH1( DC, toon::BASE_QA, toon::OFFSET_QS );
	toon::itop      = 0;
	toon::needFlush = FALSE;
	return Probe_MC( inClipMode );
}


void
vu1::cartoon::SetContext_CH1	(	dmac::Cursor*			DC,
									Context*				inContext,
									ToLitViewContext*		inContextBase,
									ContextToon*			inContextToon,
									ToLitFilters*			inTLFilters,
									Vec4*					inToProjTans	)
{
	NV_ASSERT_DC( DC );

	if( inContext ) {
		vu1_UnpackContext_CH1( DC, inContext, toon::CTXT_QA, sizeof(Context)>>4, toon::needFlush );
		toon::needFlush = FALSE;
	}

	if( inContextBase ) {
		vu1_UnpackContext_CH1( DC, inContextBase, toon::CTXT_QA, sizeof(ToLitViewContext)>>4, toon::needFlush );
		toon::needFlush = FALSE;
	}

	if( inContextToon ) {
		const uint ex_QA = toon::CTXT_QA + MQOFFSET( Context, toon );
		vu1_UnpackContext_CH1( DC, inContextToon, ex_QA, sizeof(ContextToon)>>4, toon::needFlush );
		toon::needFlush = FALSE;
	}

	if( inTLFilters ) {
		const uint lc_QA = toon::CTXT_QA + MQOFFSET( Context, tl_view.tl.filters );
		vu1_UnpackContext_CH1( DC, inTLFilters, lc_QA, sizeof(ToLitFilters)>>4, toon::needFlush );
		toon::needFlush = FALSE;
	}

	if( inToProjTans ) {
		const uint t_QA = toon::CTXT_QA + MQOFFSET( Context, tl_view.view.toprojTrans );
		DC->i64[0] = DMA_TAG_CNT | 1;
		DC->i32[2] = toon::needFlush ? SCE_VIF1_SET_FLUSH(0) : 0;
		DC->i32[3] = SCE_VIF1_SET_UNPACK( t_QA, 1, UNPACK_V4_32, 0 );
		DC->f32[4] = inToProjTans->x;
		DC->f32[5] = inToProjTans->y;
		DC->f32[6] = inToProjTans->z;
		DC->f32[7] = inToProjTans->w;
		DC->i128  += 2;
		toon::needFlush = FALSE;
	}

	toon::itop |= toon::DO_RELOAD;
}


void
vu1::cartoon::SetDirect_CH1	(	dmac::Cursor*			DC,
								frame::GsContext*		inGsRegs				)
{
	vu1_UnpackDirect_CH1( DC, inGsRegs, toon::DIRECT_QA, toon::needFlush );
	toon::itop      |= toon::DO_DIRECT;
	toon::needFlush  = FALSE;
}


void
vu1::cartoon::SetData_CH1	(	dmac::Cursor*			DC,
								uint32					inAddr,
								uint					inQSize,
								uint					inNbPacket,
								uint					inITOPFlags		)
{
	NV_ASSERT_DC( DC );
	NV_ASSERT_A128( inAddr );
	NV_ASSERT( inQSize );
	NV_ASSERT( inNbPacket >= 1 );

	DC->i32[0] = DMA_TAG_REF | inQSize;
	DC->i32[1] = DMA_ADDR( inAddr );
	DC->i64[1] = SCE_VIF1_SET_ITOP( toon::itop|inITOPFlags, 0 );
	DC->i128  += 1;

	// if packetCpt > 1 then xgkick is already done & flush is not needed !
	toon::needFlush = (inNbPacket==1);

	// reset itop flags
	toon::itop = 0;
}


void
vu1::cartoon::End_CH1		(	dmac::Cursor*			DC		)
{
	vu1_End_CH1( DC );
}



