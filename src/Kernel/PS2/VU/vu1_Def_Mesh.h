/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



IMPORT_VU_CODE_NAME( VU1_MESH_PRELIT_FAST			)
IMPORT_VU_CODE_NAME( VU1_MESH_PRELIT_CULL			)
IMPORT_VU_CODE_NAME( VU1_MESH_PRELIT_CLIP			)
IMPORT_VU_CODE_NAME( VU1_MESH_TOLIT_FAST			)
IMPORT_VU_CODE_NAME( VU1_MESH_TOLIT_CULL			)
IMPORT_VU_CODE_NAME( VU1_MESH_TOLIT_CLIP			)


namespace
{

	namespace bm
	{

		enum {
			// ITOP flags
			DO_RELOAD			= (1<<0),
			DO_DIRECT			= (1<<1),
			// ToLit mapping
			TOLIT_CTXT_QA		= (VU1_WITH_DCACHE_CTXT_QA),
			TOLIT_DIRECT_QA		= (VU1_WITH_DCACHE_DIRECT_QA),
			TOLIT_BASE_QA		= (VU1_WITH_DCACHE_BASE_QA),
			TOLIT_OFFSET_QS		= (VU1_WITH_DCACHE_OFFSET_QS),
			// PreLit mapping
			PRELIT_CTXT_QA		= (VU1_NO_DCACHE_CTXT_QA),
			PRELIT_DIRECT_QA	= (VU1_NO_DCACHE_DIRECT_QA),
			PRELIT_BASE_QA		= (VU1_NO_DCACHE_BASE_QA),
			PRELIT_OFFSET_QS	= (VU1_NO_DCACHE_OFFSET_QS)
		};

		int			prelit_mcId[3];
		int			tolit_mcId[3];
		uint		itop;
		bool		tolit;
		bool		needFlush;

		void		Init()
		{
			NV_COMPILE_TIME_ASSERT( (TOLIT_BASE_QA +2*TOLIT_OFFSET_QS)   <= 1024 );
			NV_COMPILE_TIME_ASSERT( (PRELIT_BASE_QA+2*PRELIT_OFFSET_QS) <= 1024 );
			NV_COMPILE_TIME_ASSERT( (sizeof(vu1::mesh::Context)&15)==0 );
			NV_COMPILE_TIME_ASSERT( sizeof(vu1::mesh::Context) <= (VU1_CONTEXT_QS*16) );

			static bool done = FALSE;
			if( done )	return;
			done = TRUE;
			prelit_mcId[0]	= VU1_REGISTER_MC( VU1_MESH_PRELIT_FAST		);
			prelit_mcId[1]	= VU1_REGISTER_MC( VU1_MESH_PRELIT_CULL		);
			prelit_mcId[2]	= VU1_REGISTER_MC( VU1_MESH_PRELIT_CLIP		);
			tolit_mcId[0]	= VU1_REGISTER_MC( VU1_MESH_TOLIT_FAST		);
			tolit_mcId[1]	= VU1_REGISTER_MC( VU1_MESH_TOLIT_CULL		);
			tolit_mcId[2]	= VU1_REGISTER_MC( VU1_MESH_TOLIT_CLIP		);
		}
	}

}




int
vu1::mesh::Probe_MC			(	ClipMode				inClipMode,
								bool					inToLit					)
{
	bm::Init();
	if( inToLit )
		return bm::tolit_mcId[inClipMode];
	else
		return bm::prelit_mcId[inClipMode];
}



int
vu1::mesh::Begin_CH1		(	dmac::Cursor*			DC,
								ClipMode				inClipMode,
								bool					inToLit					)
{
	if( inToLit )	vu1_Begin_CH1( DC, bm::TOLIT_BASE_QA, bm::TOLIT_OFFSET_QS );
	else			vu1_Begin_CH1( DC, bm::PRELIT_BASE_QA, bm::PRELIT_OFFSET_QS );
	bm::itop      = 0;
	bm::tolit     = inToLit;
	bm::needFlush = FALSE;
	return Probe_MC( inClipMode, inToLit );
}


void
vu1::mesh::SetContext_CH1	(	dmac::Cursor*			DC,
								Context*				inContext,
								ToLitFilters*			inTLFilters,
								Vec4*					inToProjTans		)
{
	uint c_QA = ( bm::tolit ? bm::TOLIT_CTXT_QA : bm::PRELIT_CTXT_QA );

	if( inContext ) {
		vu1_UnpackContext_CH1( DC, inContext, c_QA, sizeof(Context)>>4, bm::needFlush );
		bm::needFlush = FALSE;
	}

	if( bm::tolit && inTLFilters ) {
		const uint lc_QA = c_QA + MQOFFSET( Context, tl_view.tl.filters );
		vu1_UnpackContext_CH1( DC, inTLFilters, lc_QA, sizeof(ToLitFilters)>>4, bm::needFlush );
		bm::needFlush = FALSE;
	}

	if( inToProjTans ) {
		uint t_QA = c_QA + MQOFFSET( Context, tl_view.view.toprojTrans );
		DC->i64[0] = DMA_TAG_CNT | 1;
		DC->i32[2] = bm::needFlush ? SCE_VIF1_SET_FLUSH(0) : 0;
		DC->i32[3] = SCE_VIF1_SET_UNPACK( t_QA, 1, UNPACK_V4_32, 0 );
		DC->f32[4] = inToProjTans->x;
		DC->f32[5] = inToProjTans->y;
		DC->f32[6] = inToProjTans->z;
		DC->f32[7] = inToProjTans->w;
		DC->i128  += 2;
		bm::needFlush = FALSE;
	}

	bm::itop |= bm::DO_RELOAD;
}


void
vu1::mesh::SetDirect_CH1	(	dmac::Cursor*			DC,
								frame::GsContext*		inGsRegs				)
{
	uint qa = ( bm::tolit ? bm::TOLIT_DIRECT_QA : bm::PRELIT_DIRECT_QA );
	vu1_UnpackDirect_CH1( DC, inGsRegs, qa, bm::needFlush );
	bm::itop      |= bm::DO_DIRECT;
	bm::needFlush  = FALSE;
}


void
vu1::mesh::SetData_CH1	(	dmac::Cursor*			DC,
							uint32					inAddr,
							uint					inQSize,
							uint					inNbPacket,
							uint					inITOPFlags		)
{
	NV_ASSERT_DC( DC );
	NV_ASSERT_A128( inAddr );
	NV_ASSERT( inQSize );
	NV_ASSERT( inNbPacket >= 1 );

	DC->i32[0] = DMA_TAG_REF | inQSize;
	DC->i32[1] = DMA_ADDR( inAddr );
	DC->i64[1] = SCE_VIF1_SET_ITOP( bm::itop|inITOPFlags, 0 );
	DC->i128  += 1;

	// if packetCpt > 1 then xgkick is already done & flush is not needed !
	bm::needFlush = (inNbPacket==1);

	// reset itop flags
	bm::itop = 0;
}


void
vu1::mesh::End_CH1		(	dmac::Cursor*		DC						)
{
	vu1_End_CH1( DC );
}




