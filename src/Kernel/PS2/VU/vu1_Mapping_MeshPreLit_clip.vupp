/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/VU/vu1_Mapping.h>
#include <Kernel/PS2/VU/vu1_Tools.h>
#include <Kernel/PS2/VU/vu1_Clip4D.h>

#define	CTXT_MAPSRC		(CTXT_LAST+0)
#define	CTXT_MAPCOLOR	(CTXT_LAST+1)
#define	CTXT_MAPM		(CTXT_LAST+2)


		.name			VU1_MAPPING_MESH_PRELIT_CLIP


		.vcl
		.vcl_optim_loop
		.vcl_semicolon_is_newline
		.init_vf_all
		.init_vi_all
		.syntax new


		--enter
		in_hw_clip		clip
		in_vi			ZMSK0							// original direct sceGsZbuf.ZMSK
		in_vf			CTE
		in_vf			TOPROJM0
		in_vf			TOPROJM1
		in_vf			TOPROJM2
		in_vf			TOPROJM3
		in_vf			TOPROJS
		in_vf			TOPROJT
		in_vf			MAPSRC
		in_vf			MAPCOLOR
		in_vf			MAPM0
		in_vf			MAPM1
		in_vf			MAPM2
		in_vf			MAPM3
		in_vf			STOFFSET
		--endenter


		BUILD_ONES()
		BUILD_ADC_CONSTANTS( CLIP_CTE )

		// CTE = { NXYZ-unpack, 255, 128, xyzUnpack }
		loi				VU1_UNPACK_NXYZ;	maxi.x	CTE,	vf00,	i
		loi				255;				maxi.y	CTE,	vf00,	i
		loi				128;				maxi.zw	CTE,	vf00,	i

		--cont	// SYNC




SWITCH:
		CLIP_RESET()
		xitop			doMask
		ibeq			doMask, vi00, END_SWITCH				// do something ?

		// DIRECT changed ?
		iaddiu			directA, vi00, DIRECT
		jump_if_unset( doMask, 1, noDirect )
		// setup clip GIFTAG
		lq.y			directGIFTAG, 0(directA)
		CLIP_SET_PRIM_NREG1( directGIFTAG )						// .y : { NREG=1, PRIM=strip, PRE=1 }
		ilw.y			ZMSK0,		4(directA)					// load original sceGsZbuf.ZMSK
noDirect:

		// setup viewport with context
		ilw.z			zmsk,		CTXT_VP+1(vi00)
		ior				zmsk,		zmsk, ZMSK0					// viewport-zmsk | direct-ZMSK0
		lq				reg0,		CTXT_VP+0(vi00)
		lq				reg2,		CTXT_VP+1(vi00)
		mr32			reg1,		reg0
		mr32			reg1,		reg1
		mr32			reg3,		reg2
		mr32			reg3,		reg3
		mfir.y			reg3,		zmsk
		sq.x			reg0,		1(directA)					// sceGsFrame (no FBMSK overwrite !)
		sq.xy			reg1,		2(directA)					// sceGsXyoffset
		sq.xy			reg2,		3(directA)					// sceGsScissor
		sq.xy			reg3,		4(directA)					// sceGsZbuf  (ORed zmsk !)
		xgkick			directA									// PATH1 direct & write PRIM !

		// load context
		lq				TOPROJM0,	CTXT_TOPROJ_M+0(vi00)
		lq				TOPROJM1,	CTXT_TOPROJ_M+1(vi00)
		lq				TOPROJM2,	CTXT_TOPROJ_M+2(vi00)
		lq				TOPROJM3,	CTXT_TOPROJ_M+3(vi00)
		lq				TOPROJS,	CTXT_TOPROJ_S(vi00)
		lq				TOPROJT,	CTXT_TOPROJ_T(vi00)
		lq				MAPM0,		CTXT_MAPM+0(vi00)
		lq				MAPM1,		CTXT_MAPM+1(vi00)
		lq				MAPM2,		CTXT_MAPM+2(vi00)
		lq				MAPM3,		CTXT_MAPM+3(vi00)
		lq				MAPSRC,		CTXT_MAPSRC(vi00)
		lq				MAPCOLOR,	CTXT_MAPCOLOR(vi00)
		ilw.z			tex0_addr,	10(directA)
		loi_if_equ( tex0_addr, 0x06, 128, 255, tme )			// i = tme ? 128 : 255
		muli.xyz		MAPCOLOR,	MAPCOLOR,  i				// [0,1] => [0,i]
		mulz.w			MAPCOLOR,	MAPCOLOR,  CTE				// a in [0,128]
		ftoi0			MAPCOLOR,	MAPCOLOR
		lq.w			CTE,		CTXT_PARAM(vi00)
		lq.xy			STOFFSET,	14(directA)
		loi				VU1_UNPACK_ST
		subi.xy			STOFFSET,	STOFFSET,	i
END_SWITCH:





C1_proc:
		xtop			unpackPtr
		iaddiu			xgkickPtr,	unpackPtr,	OUTPUT_OFFSET
		ilw.x			nloop,		0(unpackPtr)
		ilw.y			nproc,		0(unpackPtr)				// #*3, multiple de 2 pour LoopCS !
		ilw.w			EOS,		0(unpackPtr)
		iaddiu			unpackPtr,	unpackPtr,	1

		iadd			unpackPtrEnd, unpackPtr, nproc
		max				src_tex@,	vf00, vf00
		max				src_tex@@,	vf00, vf00					// src_tex = <0,0,0,1>
		mr32			stq@,		vf00
		mr32			stq@@,		vf00						// stq@ = stq@@ = <0,0,1,0>
		mulx.w			need_clip, vf00, vf00					// clear need to clip flag

C1_loop:
		--LoopCS 1, 0

		lq				xyzk@,		0(unpackPtr)
		lq.xy			src_tex@,	1(unpackPtr)

		// Unpack
		subw.xyz		xyzk@,		xyzk@,		CTE
		add.xy			src_tex@,	src_tex@,	STOFFSET

		// map source
		mulax			acc,		xyzk@,		MAPSRC
		maddy			src@,		src_tex@,	MAPSRC
		mulax			acc,		MAPM0,		src@
		madday			acc,		MAPM1,		src@
		maddaz			acc,		MAPM2,		src@
		maddw			src@,		MAPM3,		vf00
		div				q,			vf00[w],	src@[w]
		mulq.xy			stq@,		src@,		q

		mulax			acc,		TOPROJM0,	xyzk@
		madday			acc,		TOPROJM1,	xyzk@
		maddaz			acc,		TOPROJM2,	xyzk@
		maddw			pxyqc@,		TOPROJM3,	vf00

		sq				pxyqc@,		0(unpackPtr)				// for clipping 4D pxyzw
		sq.xyz			stq@,		1(unpackPtr)				// for clipping unpacked s.t.q
		sq.w			xyzk@,		1(unpackPtr)				// for clipping strip-ADC

		div				q,			vf00[w],	pxyqc@[w]

		clipw			pxyqc@,		pxyqc@
		fcand			vi01,		0x03FFFF
		lq.w			pxyqc@,		CLIP_CTE(vi01)
		add.w			need_clip,	need_clip,	pxyqc@
		add.w			pxyqc@,		pxyqc@,		xyzk@[w]		// ADC

		mulq.xyz		pxyqc@,		pxyqc@,		q
		mulq			pstq@,		stq@,		q

		mula.xyz		acc,		TOPROJS,	pxyqc@
		maddw.xyz		pxyqc@,		TOPROJT,	vf00

		sq				pstq@,		0+OUTPUT_OFFSET(unpackPtr)
		sq				MAPCOLOR,	1+OUTPUT_OFFSET(unpackPtr)
		sq				pxyqc@,		2+OUTPUT_OFFSET(unpackPtr)
		iaddiu			unpackPtr, unpackPtr, 3

		.vcl_loop_copy
		ibne			unpackPtr, unpackPtrEnd, C1_loop


		// KICKING

		BUILD_CNT_GIFTAG( GIFTAG_cnt, nloop )
		sq				GIFTAG_cnt,	0(xgkickPtr)
		xgkick			xgkickPtr


		// CLIPPING

		xtop			unpackPtr
		iaddiu			unpackPtr,	unpackPtr,	1				// skip header
		mtir			need_clip_i, need_clip[w]

#define		CLIP_NEXT_VERTICE( outLoc, outCol, outTex, outStripADC )	\
				lq				outLoc,	0(unpackPtr);					\
				lq				outTex,	1(unpackPtr);					\
				lq				outCol,	2(unpackPtr);					\
				itof0			outCol,	outCol;							\
				ilw.w			outStripADC, 1(unpackPtr);				\
				iaddiu			unpackPtr, unpackPtr, 3;

#define		CLIP_FORWARD_VERTICE( inNb )								\
				iadd			unpackPtr, unpackPtr, inNb;				\
				iadd			unpackPtr, unpackPtr, inNb;				\
				iadd			unpackPtr, unpackPtr, inNb;


		CLIP_BACKUP_4( TOPROJM0, TOPROJM1, TOPROJM2, TOPROJM3 )
		CLIP_PROCESS( xgkickPtr, need_clip_i )
		#include <Kernel/PS2/VU/vu1_Clip4D_Loop.h>
		CLIP_RESTORE_4( TOPROJM0, TOPROJM1, TOPROJM2, TOPROJM3 )


		// CONTINUE

		--cont		// SYNC
		ibeq			EOS, vi00, C1_proc
		b				SWITCH

		--exit
		--endexit





