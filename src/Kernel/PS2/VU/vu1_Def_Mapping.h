/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




IMPORT_VU_CODE_NAME( VU1_MAPPING_MESH_TOLIT_CULL	)
IMPORT_VU_CODE_NAME( VU1_MAPPING_MESH_PRELIT_CULL	)
IMPORT_VU_CODE_NAME( VU1_MAPPING_MESH_PRELIT_CLIP	)




namespace
{

	namespace map
	{
		enum {
			// ITOP flags
			DO_RELOAD			= (1<<0),
			DO_DIRECT			= (1<<1),
			// ToLit mapping
			TOLIT_CTXT_QA		= (VU1_WITH_DCACHE_CTXT_QA),
			TOLIT_DIRECT_QA		= (VU1_WITH_DCACHE_DIRECT_QA),
			TOLIT_BASE_QA		= (VU1_WITH_DCACHE_BASE_QA),
			TOLIT_OFFSET_QS		= (VU1_WITH_DCACHE_OFFSET_QS),
			// PreLit mapping
			PRELIT_CTXT_QA		= (VU1_NO_DCACHE_CTXT_QA),
			PRELIT_DIRECT_QA	= (VU1_NO_DCACHE_DIRECT_QA),
			PRELIT_BASE_QA		= (VU1_NO_DCACHE_BASE_QA),
			PRELIT_OFFSET_QS	= (VU1_NO_DCACHE_OFFSET_QS)
		};


		int					mesh_tolit_mcId[3];		// with D$ mesh
		int					mesh_prelit_mcId[3];
		uint				itop;
		bool				needFlush;
		mapping::DataType	dt;


		void		Init()
		{
			NV_COMPILE_TIME_ASSERT( (TOLIT_BASE_QA +2*TOLIT_OFFSET_QS)  <= 1024 );
			NV_COMPILE_TIME_ASSERT( (PRELIT_BASE_QA+2*PRELIT_OFFSET_QS) <= 1024 );
			NV_COMPILE_TIME_ASSERT( (sizeof(vu1::mapping::Context)&15)==0 );
			NV_COMPILE_TIME_ASSERT( sizeof(vu1::mapping::Context) <= (VU1_CONTEXT_QS*16) );

			static bool done = FALSE;
			if( done )	return;
			done = TRUE;
			mesh_tolit_mcId[0]  = VU1_REGISTER_MC( VU1_MAPPING_MESH_TOLIT_CULL );
			mesh_tolit_mcId[1]  = VU1_REGISTER_MC( VU1_MAPPING_MESH_TOLIT_CULL );
			mesh_tolit_mcId[2]	= VU1_REGISTER_MC( VU1_MAPPING_MESH_TOLIT_CULL );
			mesh_prelit_mcId[0] = VU1_REGISTER_MC( VU1_MAPPING_MESH_PRELIT_CULL );
			mesh_prelit_mcId[1] = VU1_REGISTER_MC( VU1_MAPPING_MESH_PRELIT_CULL );
			mesh_prelit_mcId[2] = VU1_REGISTER_MC( VU1_MAPPING_MESH_PRELIT_CLIP );
		}
	}

}





int
vu1::mapping::Probe_MC	(	ClipMode		inClipMode,
							DataType		inDataType		)
{
	map::Init();
	if( inDataType==DT_PRELIT_MESH )
		return map::mesh_prelit_mcId[inClipMode];
	else if( inDataType==DT_TOLIT_MESH )
		return map::mesh_tolit_mcId[inClipMode];
	else
		return -1;
}


int
vu1::mapping::Begin_CH1		(	dmac::Cursor*			DC,
								ClipMode				inClipMode,
								DataType				inDataType			)
{
	if( inDataType==DT_PRELIT_MESH )
		vu1_Begin_CH1( DC, map::PRELIT_BASE_QA, map::PRELIT_OFFSET_QS );
	else if( inDataType==DT_TOLIT_MESH )
		vu1_Begin_CH1( DC, map::TOLIT_BASE_QA, map::TOLIT_OFFSET_QS );

	map::itop		= 0;
	map::dt			= inDataType;
	map::needFlush	= FALSE;
	return Probe_MC( inClipMode, inDataType );
}


void
vu1::mapping::SetContext_CH1(	dmac::Cursor*			DC,
								Context*				inContext,
								ViewContext*			inContextBase,
								ContextMap*				inContextMap	)
{
	uint c_QA = ( map::dt==DT_TOLIT_MESH ? map::TOLIT_CTXT_QA : map::PRELIT_CTXT_QA );

	if( inContext ) {
		vu1_UnpackContext_CH1( DC, inContext, c_QA, sizeof(Context)>>4, map::needFlush );
		map::needFlush = FALSE;
	}

	if( inContextBase ) {
		vu1_UnpackContext_CH1( DC, inContextBase, c_QA, sizeof(ViewContext)>>4, map::needFlush );
		map::needFlush = FALSE;
	}

	if( inContextMap ) {
		uint ex_QA = c_QA + MQOFFSET( Context, map );
		vu1_UnpackContext_CH1( DC, inContextMap, ex_QA, sizeof(ContextMap)>>4, map::needFlush );
		map::needFlush = FALSE;
	}

	map::itop |= map::DO_RELOAD;
}


void
vu1::mapping::SetDirect_CH1	(	dmac::Cursor*			DC,
								frame::GsContext*		inGsRegs				)
{
	uint qa = ( map::dt==DT_TOLIT_MESH ? map::TOLIT_DIRECT_QA : map::PRELIT_DIRECT_QA );
	vu1_UnpackDirect_CH1( DC, inGsRegs, qa, map::needFlush );
	map::itop      |= map::DO_DIRECT;
	map::needFlush  = FALSE;
}


void
vu1::mapping::SetData_CH1	(	dmac::Cursor*			DC,
								uint32					inAddr,
								uint					inQSize,
								uint					inNbPacket,
								uint					inITOPFlags		)
{
	NV_ASSERT_DC( DC );
	NV_ASSERT_A128( inAddr );
	NV_ASSERT( inQSize );
	NV_ASSERT( inNbPacket >= 1 );

	DC->i32[0] = DMA_TAG_REF | inQSize;
	DC->i32[1] = DMA_ADDR( inAddr );
	DC->i64[1] = SCE_VIF1_SET_ITOP( map::itop|inITOPFlags, 0 );
	DC->i128  += 1;

	// if packetCpt > 1 then xgkick is already done & flush is not needed !
	map::needFlush = (inNbPacket==1);

	// reset itop flags
	map::itop = 0;
}


void
vu1::mapping::End_CH1		(	dmac::Cursor*		DC			)
{
	vu1_End_CH1( DC );
}




