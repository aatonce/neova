/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <libpkt.h>
#include <sifdev.h>
using namespace nv;


#ifdef _NVCOMP_ENABLE_CONSOLE


#define SCREEN_WIDTH				640
#define SCREEN_HEIGHT				224
#define OFFX						(((4096-SCREEN_WIDTH)/2)<<4)
#define OFFY						(((4096-SCREEN_HEIGHT)/2)<<4)
#define	NORMAL_VSYNC_DELAY			5
#define	CONSOLE_ONLY_VSYNC_DELAY	10


OPEN_C_INTERFACE
extern void sceDevConsLocate(int cd, u_int lx, u_int ly);
CLOSE_C_INTERFACE



namespace
{

	void Gs_Initialize()
	{
		sceVif1Packet packet;
		u_long128 packet_buff[0x100];
		sceDmaEnv env;
		sceDmaChan *p1;
		u_long giftagAD[2] = { SCE_GIF_SET_TAG(0, 1, 0, 0, 0, 1), 0x000000000000000eL };

		sceVif1PkInit( &packet, (u_long128 *)packet_buff);

		sceDmaGetEnv(&env);
		env.notify = 1<<SCE_DMA_VIF1; /* notify channel */
		sceDmaPutEnv(&env);
	
		p1 = sceDmaGetChan(SCE_DMA_VIF1);
		p1->chcr.TTE = 1;

		sceGsResetGraph(0, SCE_GS_INTERLACE, SCE_GS_NTSC, SCE_GS_FRAME);

		sceVif1PkReset(&packet);
		sceVif1PkCnt(&packet, 0);
		sceVif1PkOpenDirectCode(&packet, 0);
		sceVif1PkOpenGifTag(&packet, *(u_long128*)&giftagAD[0]);
		sceVif1PkReserve(&packet, sceGsSetDefAlphaEnv((sceGsAlphaEnv *)packet.pCurrent,0) * 4);
		sceVif1PkCloseGifTag(&packet);
		sceVif1PkCloseDirectCode(&packet);
		sceVif1PkEnd(&packet, 0);
		sceVif1PkTerminate(&packet);

		FlushCache(0);
		sceDmaSend(p1,(u_int *)(((u_int)packet.pBase)));
		sceGsSyncPath(0,0);
	}

	ALIGNED128( sceGsDBuff,	db );
	int						console = -1;
	int						frameNo;
}



void
devcons::Init	(		)
{
	if( console >= 0 )
		return;

	sceResetEE();
	Gs_Initialize();

	sceGsSetDefDBuff( &db, SCE_GS_PSMCT32, SCREEN_WIDTH, SCREEN_HEIGHT, SCE_GS_ZGEQUAL, SCE_GS_PSMZ24, SCE_GS_CLEAR	);
	db.clear0.rgbaq.R = 0x40;
	db.clear0.rgbaq.G = 0x40;
	db.clear0.rgbaq.B = 0x80;
	db.clear1.rgbaq.R = 0x40;
	db.clear1.rgbaq.G = 0x40;
	db.clear1.rgbaq.B = 0x80;
	sceGsSetHalfOffset(&db.draw1,2048,2048, 0);
	sceGsSetHalfOffset(&db.draw0,2048,2048, 0);

	sceDevConsInit();
	console = sceDevConsOpen( OFFX+(8<<4), OFFY, 75, 28 );
	sceDevConsClear(console);
	sceDevConsLocate(console, 0, 0);

	frameNo = 0;
	sceGsSyncPath(0,0);
	FlushCache(0);
	sceGsSwapDBuff(&db, frameNo++);
	sceGsSwapDBuff(&db, frameNo++);
}


void
devcons::Shut	(		)
{
	if( console < 0 )
		return;

	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	if( !consoleOnly ) {
		sceDevConsClose( console );
		console = -1;
	}
}


void
devcons::Printf	(	pcstr	inText	)
{
	if( console<0 || !inText )
		return;
	sceDevConsPrintf( console, inText );
	sceDevConsDraw( console );

	sceGsSyncPath(0,0);
	sceGsSyncV(0);
	FlushCache(0);
	sceGsSwapDBuff(&db, frameNo++);

	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	uint vsyncDelay = consoleOnly ? CONSOLE_ONLY_VSYNC_DELAY : NORMAL_VSYNC_DELAY;
	while( vsyncDelay-- )
		sceGsSyncV(0);
}


#else


void
devcons::Init	(		)
{
	//
}


void
devcons::Shut	(		)
{
	//
}


void
devcons::Printf	(	pcstr	inText	)
{
	//
}


#endif // _NVCOMP_ENABLE_CONSOLE




