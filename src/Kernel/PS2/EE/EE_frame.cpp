/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <sifdev.h>
#include <sifrpc.h>
#include <libvifpk.h>
using namespace nv;



//#define	FORCE_RESET_MCCACHE
//#define	DISABLE_PATH3_SYNC
//#define	SAFE_PKT_UPDATE





// Setup pkt texturing context

// HERE for inlining in frame manager code !!

inline
void
frame::GsContext::ApplyTexTRX(		tram::TexTRX*	in		)
{
	NV_ASSERT_A64( this );
	NV_ASSERT_A64( in );
	typedef tram::TexTRX     _TexTRX;
	typedef frame::GsContext _GsContext;
	NV_COMPILE_TIME_ASSERT( MOFFSET(_TexTRX,TEX0)	 == 0	);
	NV_COMPILE_TIME_ASSERT( MOFFSET(_TexTRX,TEX1)	 == 8	);
	NV_COMPILE_TIME_ASSERT( MOFFSET(_TexTRX,MIP1)	 == 16	);
	NV_COMPILE_TIME_ASSERT( MOFFSET(_TexTRX,MIP2)	 == 24	);
	NV_COMPILE_TIME_ASSERT( MOFFSET(_GsContext,tex0) == 120	);
	NV_COMPILE_TIME_ASSERT( MOFFSET(_GsContext,tex1) == 108	);
	NV_COMPILE_TIME_ASSERT( MOFFSET(_GsContext,mip1) == 132	);
	NV_COMPILE_TIME_ASSERT( MOFFSET(_GsContext,mip2) == 144	);

	uint32 tmp0, tmp1;

	asm volatile ( "
		.set	noreorder
		.set	nomacro

		# TEX0 (masking TFX & TCC)
		ld		%0, 0(%3)
		sw		%0, 120(%2)
		dsrl32	%0, %0, 0
		lui		%1, 0xFFFF
		ori		%1, 0xFFE7
		and		%0, %0, %1
		lw		%1, 124(%2)
		andi	%1, %1, 0x18
		or		%0, %0, %1
		sw		%0, 124(%2)

		# TEX1 (masking MMAG,MMIN,L,K)
		lh		%0, 8(%3)
		andi	%0, %0, 0x21F
		lh		%1, 108(%2)
		andi	%1, %1, 0x1E0
		or		%0, %0, %1
		sh		%0, 108(%2)

		# MIP1
		ld		%0, 16(%3)
		dsrl32	%1, %0, 0
		sw		%0, 132(%2)
		sw		%1, 136(%2)

		# MIP2
		ld		%0, 24(%3)
		dsrl32	%1, %0, 0
		sw		%0, 144(%2)
		sw		%1, 148(%2)

		.set	reorder
		.set	macro
	" : "=&r"(tmp0), "=&r"(tmp1)
	  : "r"(this), "r"(in) );

	if( owner )
		owner->UpdateGsContext( this );
}







namespace
{

	struct {
		uint32		beginT, endT;
		uint32		begin_beginT;
		uint32		syncUpdateBeginT, syncUpdateEndT, syncUpdateIdleT;
		uint32		prepareFlushBeginT, prepareFlushEndT;
		uint32		vsyncBeginT, vsyncEndT;
		uint32		flushBeginT, flushEndT;
		uint32		dmaBeginT, dmaEndT;
		uint32		waitDmaBeginT, waitDmaEndT;
		uint32		trxdTexBSize;
	} perf;


	typedef sysvector<frame::Packet*>		PacketA;
	PacketA		packetQ[2];
	PacketA *	frontPktQ;
	PacketA *	backPktQ;
	PacketA		sortQ;



	void	SafeSyncDMA		(	pcstr			inLabel,
								uint32			inDmaChannels,
								frame::Packet*	inPkt		= NULL,
								PacketA*		inPktQueue	= NULL	)
	{
		uint32 stallChannels = dmac::SyncTimeout( inDmaChannels );
		if( !stallChannels )
			return;

		char warMsg[64];
		Sprintf( warMsg, "<%s> stalls !", inLabel );
		NV_WARNING( warMsg );


		// Output various registers
		dmac::OutputRegisters( stallChannels );


		//
		// MARKed packet infos

		frame::Packet* markPkt = NULL;

		if( inPktQueue )
		{
			uint mark_pkt_no  = (*VIF1_MARK) & 0xFFFF;
			uint pktQueueSize = inPktQueue->size();
			if( mark_pkt_no >= pktQueueSize )
			{
				Printf( "\n" );
				Printf( "-- VIF1_MARK'ed packet [%d/%d]: ERROR: Packet queue overflow !\n",
					mark_pkt_no,
					pktQueueSize	);
				Printf( "\n" );
			}
			else
			{
				markPkt = inPktQueue->at( mark_pkt_no );
				if( markPkt )
				{
					if( !dmac::IsValidMemRange(markPkt,sizeof(frame::Packet)) )
					{
						Printf( "\n" );
						Printf( "-- VIF1_MARK'ed packet [%d/%d]: ERROR: Out of memory (addr=0x%08x, tadr=0x%08x, owner=0x%08x, base=0x%08x) !\n",
							mark_pkt_no,
							pktQueueSize,
							uint32(markPkt),
							uint32(markPkt->tadr),
							uint32(markPkt->owner),
							markPkt->owner ? uint32(markPkt->owner->GetBase()) : 0 );
						Printf( "\n" );
					}
					else
					{
						Printf( "\n" );
						Printf( "-- VIF1_MARK'ed packet [%d/%d]: addr=0x%08x, tadr=0x%08x, owner=0x%08x, base=0x%08x\n",
							mark_pkt_no,
							pktQueueSize,
							uint32(markPkt),
							uint32(markPkt->tadr),
							uint32(markPkt->owner),
							markPkt->owner ? uint32(markPkt->owner->GetBase()) : 0 );
						// dump DMA
						dmac::CheckChain_CH1( (void*)(markPkt->tadr), (void*)(markPkt->link), TRUE );
						Printf( "\n" );
					}
				}
				else
				{
					Printf( "\n" );
					Printf( "-- VIF1_MARK'ed packet [%d/%d]: ERROR: Packet is NULL !\n",
							mark_pkt_no,
							pktQueueSize	);
					Printf( "\n" );
				}
			}
		}
		else
		{
			Printf( "\n" );
			Printf( "-- VIF1_MARK'ed packet: Queue is not available !\n" );
			Printf( "\n" );
		}


		//
		// current packet infos

		if( inPkt != markPkt )
		{
			if( inPkt )
			{
				if( !dmac::IsValidMemRange(inPkt,sizeof(frame::Packet)) )
				{
					Printf( "\n" );
					Printf( "-- current-packet: ERROR: Out of memory (addr=0x%08x, tadr=0x%08x, owner=0x%08x, base=0x%08x) !\n",
						uint32(inPkt),
						uint32(inPkt->tadr),
						uint32(inPkt->owner),
						inPkt->owner ? uint32(inPkt->owner->GetBase()) : 0 );
					Printf( "\n" );
				}
				else
				{
					Printf( "\n" );
					Printf( "-- current-packet: addr=0x%08x, tadr=0x%08x, owner=0x%08x, base=0x%08x\n",
						uint32(inPkt),
						uint32(inPkt->tadr),
						uint32(inPkt->owner),
						inPkt->owner ? uint32(inPkt->owner->GetBase()) : 0 );
					// dump TADR
					dmac::CheckChain_CH1( (void*)(inPkt->tadr), (void*)(inPkt->link), TRUE );
					Printf( "\n" );
				}
			}
			else
			{
				Printf( "\n" );
				Printf( "-- current-packet: Not available !\n" );
				Printf( "\n" );
			}
		}


		// Output current D1_TADR
		if( stallChannels & dmac::CH1_VIF1_M ) {
			Printf( "\n" );
			Printf( "-- D1_TADR\n" );
			Printf( "\n" );
			dmac::CheckPacket_CH1( (void*)(*D1_TADR), TRUE );
		}

		NV_ERROR( "** DMA STALLS **" );
	}



	void
	Output_Pkt_Info	(	frame::Packet*	inPkt	)
	{
		if( !inPkt )
			return;

		char msg[256];
		msg[0] = 0;
		char* pmsg = msg;
		Sprintf( pmsg, "pkt 0x%08x: ", uint32(inPkt) );
		pmsg += Strlen( pmsg );

		// PATH3 compliant ?
		if( inPkt->flags & frame::Packet::FLG_DISABLE_PATH3 ) {
			Sprintf( pmsg, "***P3-***" );
			pmsg += Strlen( pmsg );
		}

		// pkt texturing ?
		if( inPkt->texList ) {
			Sprintf( pmsg, "texids={ " );
			pmsg += Strlen( pmsg );
			frame::TexContext* tex = inPkt->texList;
			while( tex ) {
				if( tex->id<0 || !tex->gsContext || !tex->LODMask ) {
					tex = tex->next;
					continue;
				}
				Sprintf( pmsg, "%d ", tex->id );
				pmsg += Strlen( pmsg );
				tex = tex->next;
			}
			Sprintf( pmsg, "} " );
			pmsg += Strlen( pmsg );
		}

		Printf( "%s\n", msg );
	}




	//
	// SYNC update pending packet

	void SyncUpdatePkt()
	{
		perf.syncUpdateIdleT = 0;
		if( backPktQ->size() == 0 )
			return;

		frame::Packet** pkt_startP = backPktQ->data();
		frame::Packet** pkt_endP   = pkt_startP + backPktQ->size();
		frame::Packet** pktP       = pkt_startP;
		frame::Packet** pkt_markP  = pkt_startP;
		frame::Packet*  pkt;

		// CH1/2 are finished ?
		const uint dmacChannels = ( dmac::CH1_VIF1_M | dmac::CH2_GIF_M );
		#ifdef SAFE_PKT_UPDATE
		SafeSyncDMA( "Safe SyncUpdatePkt", dmacChannels );
		#endif
		if( (dmac::GetBusyMask() & dmacChannels) == 0 )
			pkt_markP = pkt_endP;

		while( pktP != pkt_endP )
		{
			// Sync DMAC ?
			if( pkt_markP == pktP )
			{
				for( ;; ) {
					// Sync on the VIF1_MARK index
					uint pktIndexCH1 = (*VIF1_MARK) & 0xFFFF;
					pkt_markP = pkt_startP + pktIndexCH1;
					NV_ASSERT( pkt_markP >= pktP );
					if( pkt_markP >= pkt_endP ) {
						pkt_markP = pkt_endP;
						break;
					}
					if( pkt_markP != pktP )
						break;

					// Wait some cycles with DMAC full speed (no BUS access)
					uint32 syncT0 = GetCycleCpt();
					WaitCycleCpt( 64 );
					perf.syncUpdateIdleT += GetCycleCpt() - syncT0;

					// DMA stalls after 1s ?
					if( perf.syncUpdateIdleT > 294912000U ) {
						SafeSyncDMA( "SyncUpdatePkt()", dmacChannels, *pktP, backPktQ );
						perf.syncUpdateIdleT = 0;
					}
				}
			}

			// Go go go
			while( pktP != pkt_markP )
			{
				pkt = *pktP;
				NV_ASSERT( pkt );
				pktP++;

				// Clear DRAWING flag
				pkt->flags &= ~frame::Packet::STT_DRAWING;

				// STT_UPD_PENDING flag is fixed when an UPDATE is requested by the owner
				// and when the pakt is in the front drawing queue.
				if(	pkt->flags & frame::Packet::STT_UPD_PENDING ) {
					#ifdef _NVCOMP_ENABLE_DBG
					gs::crtc::SetBGColor( 0xF4A85B );
					#endif
					pkt->flags ^= frame::Packet::STT_UPD_PENDING;
					if( pkt->owner )
						pkt->owner->UpdatePacket( pkt );
					#ifdef _NVCOMP_ENABLE_DBG
					gs::crtc::SetBGColor( 0x51381E );
					#endif
				}
			}
		}

		backPktQ->clear();
	}



	//
	// ASYNC FLUSH

	// frame packets considerations are :
	// - with (T+) ou without (T-) texture to trx'ing ;
	// - compliant (P3+) or not (P3-) with PATH3 trx process.
	//
	// frame packets : (p0) (p1,t1) (p2,t2) (p3) (p4,t4) (p5*) (p6*) (p7*,t7) (p8) (p9,t9)
	//  (*) not PATH3 compliant
	//
	// PATH3 trxing   *=> [-----] t1 t2 (EOP=1) [--------] t4 (EOP=1) [----]            [---] t7 (EOP=1) [----]        [---] t9 (EOP=1) [--------]
	// VU1 processing  *=>  [cnt] p0            [sync+cnt] p1 p2 p3   [sync] p4 p5* p6* [cnt]            [sync] p7* p8 [cnt]            [sync+cnt] p9
	//                                                                                                 \______________________________________________/
	//                                                                                   optimized ->    [sync] p7*    [cnt] p8         [sync+cnt] p9
	//
	// CH2 half-buffers HB are	<p0,p1,p2,p3>
	//							<p4,p5*,p6*>
	//							<p7*,p8>
	//							<p9>
	//
	// Remarks :
	// - PATH3 (ch2) starts before VU1 (ch1).
	// - PATH3 is initially closed by VIF.
	// - A CH2 half-buffer HB is (P3-) if it contains at least one (P3-) packet.
	// - In a (P3-) HB, (P3-) packets are always at the end of the HB, i.e. in the form zero-or_more( P3+ ) one-or_more( P3- )
	//
	// With theses considerations, a CH2 HB is closed for any of the following condition :
	// a) there is no more packet to process ;
	// b) the next packet is (T+) and the HB is full (limit is reached) ;
	// c) the next packet is (T+) and the HB is (P3-) (i.e. not compliant to compliant requiered).
	//
	// Rules :
	// - A (P3+) HB does a PATH3-sync-and-continue before its first (T+) packet.
	// - A (P3-) HB does a PATH3-sync before its first (T+) packet AND does a PATH3-continue after its last packet
	//	 because a (P3-) HB ends before the next (T+) packet (or no more packet to chain).
	//   Note: In a (P3-) HB, the PATH3-continue position can also be optimized for a more efficient PATH3 transfer, as shown above.
	//		   In this case, the PATH3-continue flag can be located after the last (P3-) packet.
	//		   Improvement is done by transfering texture data concurrently with following (P3+,T-) packet of the (P3-) HB.

	ALIGNED128( uint32, first_pkt_ch1[] ) = {		// TTE=1
		DMA_TAG_END | 0,							// link
		0,
		0,
		SCE_VIF1_SET_MARK( 0, 0 )					// clear MARK index
	};

	ALIGNED128( uint32, last_pkt_ch1[] ) = {		// TTE=1
		DMA_TAG_END | 0,
		0,
		SCE_VIF1_SET_MSKPATH3( 0, 0 ),				// open PATH3 (to avoid PATH3 stalling ...)
		SCE_VIF1_SET_MARK( 0xFFFF, 0 )				// max MARK index
	};

	ALIGNED128( uint32, first_pkt_ch2[] ) = {		// TTE=0
		DMA_TAG_END | 0,							// link
		0,
		0,
		0,
	};

	ALIGNED128( uint32, last_pkt_ch2[] ) = {		// TTE=0
		DMA_TAG_END | 0,
		0,
		0,
		0,
	};


	struct CH2_HalfBuffer
	{
		struct TexUpdate {
			frame::GsContext*		gsContext;
			tram::TexTRX*			trx;
		};

		struct TexFlush {
			tram::TexTRX*			trx;			// cur trx
			uint16					allbsize;		// all trxd block size
			uint8					LODMask;		// LODMask to apply
			uint8					hbNo;			// half-buffer #
		};

		TexFlush*					texFlush0;
		TexUpdate*					texUpdate0;
		TexFlush**					texList0;
		tram::TexReg*				texRegA;
		uint						texRegCpt;
		uint						hb_baddr[2];
		uint						hb_maxBSize;	// max blocks per half-buffer
		uint						hb_no;			// current half-buffer number (0/1)
		uint						hb_bsize;		// current HB size in blocks
		uint32						hb_bmpBSize;	// current HB trxed texel in byte size (for perf)
		TexFlush**					hb_texList;
		TexUpdate*					hb_texUpdate;
		bool						p3_paused;		// PATH3 paused (need a P3-continue before a P3-sync) ?
		bool						p3_compliant;	// PATH3 compliant ?
		frame::Packet*				p3_syncPkt;		// pkt to P3-sync before
		frame::Packet*				p3_cntPkt;		// pkt to P3-continue before


		void	Init	(		)
		{
			static vector<TexFlush>	texFlushA;

			// Sync SPR before using it !
			dmac::SyncSPR();

			// Setup texture array
			texRegA = tram::GetRegistrationA( &texRegCpt );

			NV_COMPILE_TIME_ASSERT( sizeof(TexFlush)  == 8 );
			NV_COMPILE_TIME_ASSERT( sizeof(TexUpdate) == 8 );
			if( texRegCpt > 1024 ) {
				// In memory ...
				texFlushA.resize( texRegCpt );
				texFlush0  = texFlushA.data();
				texUpdate0 = (TexUpdate*) SPR_START;
				texList0   = (TexFlush**) SPR_END;
			} else {
				// In SPR ...
				texFlush0  = (TexFlush*)  SPR_START;
				texUpdate0 = (TexUpdate*) (texFlush0 + texRegCpt);
				texList0   = (TexFlush**) SPR_END;
			}
			Zero( texFlush0, sizeof(TexFlush) * texRegCpt );

			// tram double buffer
			hb_no		= 0;
			hb_maxBSize	= (tram::GetPSize() >> 1) << 5;
			hb_baddr[0]	= tram::GetPStart() << 5;
			hb_baddr[1]	= hb_baddr[0] + hb_maxBSize;

			// Fill texture half-buffer
			hb_bsize	 = 0;
			hb_texList	 = texList0;
			hb_texUpdate = texUpdate0;
			hb_bmpBSize  = 0;

			// PATH3 vars
			p3_paused	 = TRUE;	// P3 is initially closed !
			p3_compliant = TRUE;
			p3_syncPkt	 = NULL;
			p3_cntPkt	 = NULL;
		}


		bool	TryAppend	(	frame::Packet*	pkt		)
		{
			NV_ASSERT( pkt );

			if( pkt->texList )
			{
				// Parse pkt's textures ...
				TexFlush**	pkt_texList   = hb_texList;
				TexUpdate*	pkt_texUpdate = hb_texUpdate;
				uint		pkt_bsize	  = 0;
				uint32		pkt_bmpbsize  = 0;

				frame::TexContext* tex = pkt->texList;
				while( tex ) {
					if( tex->id<0 || !tex->gsContext || !tex->LODMask ) {
						tex = tex->next;
						continue;
					}

					int	texId = tex->id;
					NV_ASSERT( texId < int(texRegCpt) );

					// current hb is not path3 compliant => terminate the current texture half-buffer before !
					if( !p3_compliant )
						return FALSE;

					TexFlush* texf = texFlush0 + texId;

					// First use in frame ?
					if( texf->allbsize == 0 ) {
						NV_ASSERT( texRegA[texId].desc );
						NV_ASSERT( texRegA[texId].desc->trxAllBSize > 0 );
						texf->trx  			= texRegA[texId].firstTRX;
						texf->allbsize		= texRegA[texId].desc->trxAllBSize;
						texf->hbNo			= ~0U;	// => first use in half-buffer !
					}

					// First use in half-buffer ?
					if( texf->hbNo != hb_no ) {
						pkt_bsize    += texf->allbsize;
						pkt_bmpbsize += texRegA[texId].allBmpBSize;
						// Alloc trx
						if( !texf->trx ) {
							texf->trx = tram::AllocateManagedTRX( texId );
							NV_ASSERT( texf->trx );
						}
						texf->LODMask = 0;
						texf->hbNo    = hb_no;
						*(--pkt_texList) = texf;
						NV_ASSERT( uint32(pkt_texList) > uint32(pkt_texUpdate) );
					}

					texf->LODMask |= tex->LODMask;
					pkt_texUpdate->gsContext = tex->gsContext;
					pkt_texUpdate->trx		 = texf->trx;
					pkt_texUpdate++;
					NV_ASSERT( uint32(pkt_texList) > uint32(pkt_texUpdate) );

					tex = tex->next;
				}

				// Textured ?
				if( pkt_texUpdate != hb_texUpdate )
				{
					// append textured packet in P3 compliant !
					NV_ASSERT( p3_compliant );
					if( pkt_bsize > hb_maxBSize ) {
						// Single packet overflow the texture half-buffer (too much texture in a single packet)
						// -> skip pkt with random texturing !
						NV_ERROR( "Packet overflows the texture buffer size !" );
					} else {
						hb_bsize += pkt_bsize;
						// Half-buffer full ?
						if( hb_bsize > hb_maxBSize )
							return FALSE;
						// Append pkt to hb
						hb_bmpBSize  += pkt_bmpbsize;
						hb_texUpdate  = pkt_texUpdate;
						hb_texList    = pkt_texList;
						if( !p3_syncPkt )
							p3_syncPkt = p3_cntPkt = pkt;	// PATH3-sync&cnt at the first textured packet !
					}
				}
			}

			// PATH3 compliant ?
			if( pkt->flags & frame::Packet::FLG_DISABLE_PATH3 )
			{
				p3_compliant = FALSE;
				p3_cntPkt	 = NULL;	// no PATH3-cnt before this (P3-) packet !
			}
			else if( !p3_cntPkt )
			{
				p3_cntPkt = pkt;		// PATH3-cnt as soon as possible, before this (P3+,T-) packet
			}

			return TRUE;
		}


		void	BuildCH2	(	pvoid&	ioLinkCH2	)
		{
			// Setup TRX
			uint hb_bstart = hb_baddr[ hb_no&1 ];
			tram::TexTRX* trx = NULL;
			while( hb_texList != texList0 ) {
				TexFlush* texf = *hb_texList++;
				trx = texf->trx;	NV_ASSERT( trx );
				// update
				trx->Update( hb_bstart, texf->LODMask );
				hb_bstart += texf->allbsize;
				// link
				NV_ASSERT_A128( ioLinkCH2 );
				SET_DMATAG_NEXT( ioLinkCH2, trx->tadr );
				ioLinkCH2 = trx->link;
				// change to next uploader
				texf->trx = trx->next;
			}
			if( trx ) {
				trx->EnableEOP();	// EOP=1 for the last PATH3 TRX
				hb_no++;			// => next half-buffer
			}

			// Setup pkt texturing context
			while( hb_texUpdate != texUpdate0 ) {
				hb_texUpdate--;
				hb_texUpdate->gsContext->ApplyTexTRX( hb_texUpdate->trx );
			}

			hb_bsize	 = 0;
			hb_bmpBSize  = 0;
			p3_compliant = TRUE;
			p3_paused	 = (p3_cntPkt == NULL);		// PATH3 is currently paused if no PATH3-continue has been done in the HB !
			p3_syncPkt	 = NULL;
			p3_cntPkt	 = NULL;
		}

	};


	void	PrepareASyncFlushQ	(	)
	{
		#ifdef _NVCOMP_ENABLE_DBG
			bool asyncLog = FALSE;
			core::GetParameter( core::PR_ASYNC_LOG, &asyncLog );
			if( asyncLog ) {
				Printf( "\n" );
				Printf( "\n" );
				Printf( "\n" );
				Printf( "\n" );
			}
		#endif

		if( frontPktQ->size() == 0 ) {
			SET_DMATAG_NEXT( first_pkt_ch1, last_pkt_ch1 );
			SET_DMATAG_NEXT( first_pkt_ch2, last_pkt_ch2 );
			return;
		}

		frame::Packet** pktA		= frontPktQ->data(),
					 ** pktA_end	= frontPktQ->data() + frontPktQ->size();

		// direct link because DMAs are not running !
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );
		pvoid linkCH1 = first_pkt_ch1;
		pvoid linkCH2 = first_pkt_ch2;

		CH2_HalfBuffer hb;
		hb.Init();

		uint  pktIdx		= 0;
		int	  curMcId		= -1;
		perf.trxdTexBSize	= 0;

		while( pktA != pktA_end )
		{
			// Build the CH2 half-buffer
			frame::Packet** pktA_hb = pktA;
			while( pktA_hb != pktA_end ) {
				if( !hb.TryAppend(*pktA_hb) )
					break;
				pktA_hb++;
			}

			// add trd tex bsize
			perf.trxdTexBSize += hb.hb_bmpBSize;

			#ifdef _NVCOMP_ENABLE_DBG
			if( asyncLog )
				Printf( "----------------------\n" );
			#endif

			// PATH3-sync with PATH3 paused ?
			if( hb.p3_paused && hb.p3_syncPkt ) {
				#ifndef DISABLE_PATH3_SYNC
				gs::InsertPATH3_CH1( DC, linkCH1, FALSE, TRUE );
				#endif
				#ifdef _NVCOMP_ENABLE_DBG
				if( asyncLog )
					Printf( ">> PATH3: resumed\n" );
				#endif
			}

			// Build CH1
			while( pktA != pktA_hb ) {
				frame::Packet* pkt = *pktA++;
				NV_ASSERT( pkt );

				// Setup flags
				NV_ASSERT( (pkt->flags & frame::Packet::STT_DRAWING) == 0 );
				pkt->flags |= frame::Packet::STT_DRAWING;

				// Need vu1 mc ?
				if(	pkt->mcId >= 0 ) {
					// Reset vu1 cache ?
					#ifndef FORCE_RESET_MCCACHE
					if( pkt->flags & frame::Packet::FLG_RESET_MCCACHE )
					#endif
					{
						vu::ResetMCCache( 1 );
						curMcId = -1;
					}
					// Change current mc ?
					if( pkt->mcId != curMcId ) {
						curMcId = pkt->mcId;
						NV_ASSERT_DC( DC );
						NV_ASSERT_A128( linkCH1 );
						SET_DMATAG_NEXT( linkCH1, DC->vd );
						linkCH1 = (pvoid) vu::RequestMC( DC, pkt->mcId );
					}
				}

				// PATH3-sync/cnt ?
				bool p3_sync = ( pkt == hb.p3_syncPkt );
				bool p3_cnt  = ( pkt == hb.p3_cntPkt  );
				#ifndef DISABLE_PATH3_SYNC
				gs::InsertPATH3_CH1( DC, linkCH1, p3_sync, p3_cnt );
				#endif
				#ifdef _NVCOMP_ENABLE_DBG
				if( asyncLog ) {
					if( p3_sync )	Printf( ">> PATH3: sync\n" );
					if( p3_cnt )	Printf( ">> PATH3: continue\n" );
				}
				#endif

				// Append packet
				pkt->link[2] = SCE_VIF1_SET_MARK( ++pktIdx, 0 );	// VIF1_MARKs the pkt with the idx of the next pkt ...
				NV_ASSERT_A128( linkCH1 );
				SET_DMATAG_NEXT( linkCH1, pkt->tadr );
				linkCH1 = pkt->link;

				#ifdef _NVCOMP_ENABLE_DBG
				if( asyncLog )
					Output_Pkt_Info( pkt );
				#endif
			}

			hb.BuildCH2( linkCH2 );
		}

		// Terminate CH1
		NV_ASSERT_A128( linkCH1 );
		NV_ASSERT_A128( last_pkt_ch1 );
		SET_DMATAG_NEXT( linkCH1, last_pkt_ch1 );

		// Terminate CH2
		NV_ASSERT_A128( linkCH2 );
		NV_ASSERT_A128( last_pkt_ch2 );
		SET_DMATAG_NEXT( linkCH2, last_pkt_ch2 );

		#ifdef _NVCOMP_ENABLE_DBG
		if( asyncLog ) {
			Printf( "----------------------\n" );
			Printf( "\n" );
			Printf( "\n" );
			Printf( "\n" );
			Printf( "\n" );
			core::SetParameter( core::PR_ASYNC_LOG, FALSE );
		}
		#endif
	}


	void ASyncFlushQ (	)
	{
		if( frontPktQ->size() == 0 )
			return;

		NV_ASSERT_A128( first_pkt_ch1 );
		NV_ASSERT_A128( first_pkt_ch2 );

		bool async_check = FALSE;
		bool async_break = FALSE;
		bool en_drawing	 = FALSE;
		bool consoleOnly = FALSE;
		core::GetParameter( core::PR_ASYNC_CHECK,		 &async_check );
		core::GetParameter( core::PR_ASYNC_BREAK,		 &async_break );
		core::GetParameter( core::PR_CONSOLE_ONLY,		 &consoleOnly );
		core::GetParameter( core::PR_ENABLE_GPU_DRAWING, &en_drawing  );

		// Check CH1 DMA-code ?
		if( async_check ) {
			FlushCache( WRITEBACK_DCACHE );
			if( !dmac::CheckChain_CH1(first_pkt_ch1,NULL,FALSE) )
				dmac::CheckChain_CH1( first_pkt_ch1,NULL,TRUE );
		}

		if( en_drawing && !consoleOnly )
		{
			// open/close PATH3, intermittent mode
			// The GIF is disabled before starting the GIF DMA transfer.
			// This ensures that the GIF doesn't transfer more than one GS packet before the VIF1 processes a FLUSHA VIFcode.
			// Once the mskpath3 enable VIFcode is processed, the GIF-FIFO will drain to the GS-FIFO.
			#ifdef DISABLE_PATH3_SYNC
			gs::SetupPATH3_CH1( 0, 0, 0 );
			#else
			gs::SetupPATH3_CH1( 0, 1, 1 );
			#endif
			// Break before ?
			NV_BREAK_IF( async_break );
			perf.dmaBeginT = perf.dmaEndT = GetCycleCpt();
			FlushCache( WRITEBACK_DCACHE );
			dmac::Start_CH2( first_pkt_ch2 );
			dmac::Start_CH1( first_pkt_ch1 );
		}
		else
		{
			perf.trxdTexBSize = 0;
			DPUT_VIF1_MARK( 0xFFFF );
		}
	}



	//
	// SYNC FLUSH

	void SyncFlushQ()
	{
		if( frontPktQ->size() == 0 )
			return;

		perf.dmaBeginT = perf.dmaEndT = GetCycleCpt();
		perf.trxdTexBSize = 0;

		bool consoleOnly = FALSE;
		core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
		if( consoleOnly )
			return;

		bool enDrawing = FALSE;
		core::GetParameter( core::PR_ENABLE_GPU_DRAWING, &enDrawing );
		if( !enDrawing )
			return;

		// PATH3 in continuous transfer mode
		gs::SetupPATH3_CH1( 0, 0, 0 );


		//
		// tram double buffer

		uint tram_buffBSize  = (tram::GetPSize() >> 1) << 5;
		uint tram_buffBStart = tram::GetPStart() << 5;
		uint tram_buffBEnd   = tram_buffBStart + tram_buffBSize;

		frame::Packet** pktP     = frontPktQ->data();
		frame::Packet** pkt_endP = pktP + frontPktQ->size();
		frame::Packet*  pkt;

		dmac::Cursor * DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );

		int	curMcId = -1;

		while( pktP != pkt_endP )
		{
			pkt = *pktP;
			NV_ASSERT( pkt );
			pktP++;


			//
			// CH2 - Send texture

			if( pkt->texList )
			{
				uint tram_baddr = tram_buffBStart;
				frame::TexContext* tex = pkt->texList;
				while( tex ) {
					if( tex->id<0 || !tex->gsContext || !tex->LODMask ) {
						tex = tex->next;
						continue;
					}
					int	texId = tex->id;

					tram::TexReg* reg = tram::GetRegistered( texId );
					if( !reg ) {
						tex = tex->next;
						continue;
					}

					tram::TexTRX* trx = reg->firstTRX;
					if( !trx )
						trx = tram::AllocateManagedTRX( texId );
					NV_ASSERT( trx );

					trx->Update( tram_baddr, tex->LODMask );
					trx->EnableEOP();
					tram_baddr += reg->desc->trxAllBSize;
					if( tram_baddr > tram_buffBEnd ) {
						NV_ERROR( "Packet overflows the texture buffer size !\n" );
						tram_baddr -= reg->desc->trxAllBSize;
					} else {
						FlushCache( WRITEBACK_DCACHE );
						dmac::Start_CH2( trx->tadr );
						SafeSyncDMA( "SyncFlushQ()[ch2]", dmac::CH2_GIF_M, pkt );
					}

					// Setup pkt texturing context
					tex->gsContext->ApplyTexTRX( trx );

					tex = tex->next;
				}
			}


			//
			// VU1 mc

			// Reset vu1 cache ?
			#ifndef FORCE_RESET_MCCACHE
			if( pkt->flags & frame::Packet::FLG_RESET_MCCACHE )
			#endif
			{
				vu::ResetMCCache( 1 );
				curMcId = -1;
			}

			// Need vu1 mc ?
			int mcId = pkt->mcId;
			if(	mcId >= 0 && mcId != curMcId ) {
				curMcId = mcId;

				NV_ASSERT_DC( DC );
				pvoid tadr = DC->vd;
				vu::RequestMC( DC, mcId );

				// CH1 - activate mc
				FlushCache( WRITEBACK_DCACHE );
				dmac::Start_CH1( tadr );
				SafeSyncDMA( "SyncFlushQ()[mc]", dmac::CH1_VIF1_M, pkt );
			}

			// CH1 - Send geometry
			FlushCache( WRITEBACK_DCACHE );
			dmac::Start_CH1( pkt->tadr );
			SafeSyncDMA( "SyncFlushQ()[ch1]", dmac::CH1_VIF1_M, pkt );

			// Setup flags
			NV_ASSERT( (pkt->flags & frame::Packet::STT_DRAWING) == 0 );
		}

		NV_ASSERT_DC( DC );
		perf.dmaEndT = GetCycleCpt();
	}

}








void
frame::Init()
{
	packetQ[0].Init();
	packetQ[1].Init();
	sortQ.Init();

	Reset();
}



void
frame::Reset()
{
	dmac::Sync( dmac::CH1_VIF1_M | dmac::CH2_GIF_M );

	// disable it on CH1/CH2
	*D_STAT = dmac::GetDisableI( dmac::CH1_VIF1_M | dmac::CH2_GIF_M );

	packetQ[0].clear();
	packetQ[1].clear();

	sortQ.clear();
	frontPktQ = & packetQ[0];
	backPktQ  = & packetQ[1];
}


void
frame::Shut()
{
	Reset();
	packetQ[0].Shut();
	packetQ[1].Shut();
	sortQ.Shut();
}



//
//	Handler Queue

void
frame::Begin(		)
{
	uint cycT			= GetCycleCpt();
	perf.begin_beginT	= cycT - perf.beginT;
	perf.beginT			= cycT;
	Swap( frontPktQ, backPktQ );
	frontPktQ->clear();
	sortQ.clear();
}


void
frame::Push(	Packet*		inPkt	)
{
	if( !inPkt )
		return;

	//
	// Setup pkt

	Packet* pkt = inPkt;
	while( pkt )
	{
		#if defined( _NVCOMP_ENABLE_DBG )
			uint32 breakPktAddr = 0;
			core::GetParameter( core::PR_EE_DMAC_BREAK_ON_PKT, &breakPktAddr );
			if( breakPktAddr && breakPktAddr==uint32(pkt) ) {
				char warnMsg[64];
				Sprintf( warnMsg, "Break on packet 0x%08 [owner=0x%08x, base=0x%08x]\n",
					breakPktAddr,
					uint32(pkt->owner),
					pkt->owner ? uint32(pkt->owner->GetBase()) : 0 );
				NV_WARNING_BREAK( warnMsg );
			}
		#endif

		pkt->flags &= ~Packet::STT_UPD_PENDING;
		// Need UPDATE ?
		if(	pkt->flags & Packet::FLG_UPD_REQUESTED ) {
			// Can't update now (pkt is drawing in back-queue) ?
			if( pkt->flags & Packet::STT_DRAWING )
				pkt->flags |= Packet::STT_UPD_PENDING;
			else if( pkt->owner )
				pkt->owner->UpdatePacket( pkt );
		}
		pkt = pkt->subList;
	}


	//
	// Separator ?

	if( inPkt->flags & Packet::FLG_SEPARATOR )
	{
		Sort();
		sortQ.push_back( inPkt );
		Sort();
	}
	else
	{
		// Default HKEY ?
		if( !inPkt->hkey )
			inPkt->BuildBasicHKey();
		sortQ.push_back( inPkt );
	}
}


void
frame::Sort	(	)
{
	uint S = sortQ.size();
	if( S == 0 )
		return;

	// pkt hkey Radix in SPR
	// TODO !

	// push pkt
	for( uint i = 0 ; i < S ; i++ ) {
		Packet* pkt = sortQ[i];
		while( pkt ) {
			frontPktQ->push_back( pkt );
			pkt = pkt->subList;
		}
	}

	sortQ.clear();
}


void
frame::End()
{
	Sort();
	perf.endT = GetCycleCpt();
}


void
frame::WaitDMA()
{
	if( dmac::IsBusy(dmac::CH1_VIF1_M|dmac::CH2_GIF_M) ) {
		SafeSyncDMA( "WaitDMA", dmac::CH1_VIF1_M|dmac::CH2_GIF_M );
		perf.dmaEndT = GetCycleCpt();
	}
}


void
frame::Sync		(			)
{
	// Update pending packets
	perf.syncUpdateBeginT = GetCycleCpt();
	SyncUpdatePkt();
	perf.syncUpdateEndT   = GetCycleCpt();

	perf.waitDmaBeginT = GetCycleCpt();
	WaitDMA();
	perf.waitDmaEndT = GetCycleCpt();
}


void
frame::Flush	(	bool	inASync,
					bool	inVSync		)
{
	// asserts DMA channels CH1/2 are stopped by a previous Sync() !
	WaitDMA();

	// Prepare async flush ?
	perf.prepareFlushBeginT = perf.prepareFlushEndT = GetCycleCpt();
	if( inASync ) {
		gs::crtc::SetBGColor( 0x00FF00 );
		PrepareASyncFlushQ();
		perf.prepareFlushEndT = GetCycleCpt();
	}

	// VSYNC ?
	gs::crtc::SetBGColor( 0 );
	perf.vsyncBeginT = perf.vsyncEndT = GetCycleCpt();
	if( inVSync ) {
		gs::crtc::VSync();
		perf.vsyncEndT = GetCycleCpt();
	}

	// Flush !
	gs::crtc::SetBGColor( ~0U );
	perf.flushBeginT = GetCycleCpt();
	if( inASync )	ASyncFlushQ();
	else			SyncFlushQ();
	perf.flushEndT = GetCycleCpt();
}



uint
frame::GetQSize()
{
	return frontPktQ->size();
}


frame::Packet*
frame::GetQEntry(	uint	inNo	)
{
	if( inNo >= GetQSize() )
		return NULL;
	else
		return (*frontPktQ)[ inNo ];
}


pvoid
frame::GetAsyncFirstCH1		(			)
{
	return (pvoid)first_pkt_ch1;
}


pvoid
frame::GetAsyncFirstCH2		(			)
{
	return (pvoid)first_pkt_ch2;
}


float
frame::GetPerformance		(	Performance	inPerf		)
{
	uint32 cyct = 0;


	if( inPerf == PERF_BEGIN_END )			cyct = perf.endT - perf.beginT;
	if( inPerf == PERF_BEGIN_BEGIN )		cyct = perf.begin_beginT;
	if( inPerf == PERF_SYNC_UPDATE )		cyct = perf.syncUpdateEndT - perf.syncUpdateBeginT;
	if( inPerf == PERF_SYNC_UPDATE_IDLE )	cyct = perf.syncUpdateIdleT;
	if( inPerf == PERF_PREPARE_FLUSH )		cyct = perf.prepareFlushEndT - perf.prepareFlushBeginT;
	if( inPerf == PERF_VSYNC_IDLE )			cyct = perf.vsyncEndT - perf.vsyncBeginT;
	if( inPerf == PERF_FLUSH )				cyct = perf.flushEndT - perf.flushBeginT;
	if( inPerf == PERF_DMA )				cyct = perf.dmaEndT - perf.dmaBeginT;
	if( inPerf == PERF_WAIT_DMA )			cyct = perf.waitDmaEndT - perf.waitDmaBeginT;
	if( inPerf == PERF_TRXD_TEXBSIZE )		return float(int(perf.trxdTexBSize));

	return GetCycleCptTime( cyct );
}



