/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <libgraph.h>



void	gs_crtc_Init( bool inEnable );
void	gs_crtc_Reset( bool inEnable );
void	gs_crtc_Shut();
void	gs_frame_Init();
void	gs_frame_Reset();
void	gs_frame_Shut();




namespace
{

	u_char s_RegisterAddr_CTX1[] =
	{
		SCE_GS_TEXFLUSH,
		SCE_GS_XYOFFSET_1,
		SCE_GS_TEX2_1,
		SCE_GS_TEX1_1,
		SCE_GS_TEX0_1,
		SCE_GS_CLAMP_1,
		SCE_GS_MIPTBP1_1,
		SCE_GS_MIPTBP2_1,
		SCE_GS_SCISSOR_1,
		SCE_GS_ALPHA_1,
		SCE_GS_TEST_1,
		SCE_GS_FBA_1,
		SCE_GS_FRAME_1,
		SCE_GS_ZBUF_1,
	};

	u_char s_RegisterAddr_CTX2[] =
	{
		SCE_GS_TEXFLUSH,
		SCE_GS_XYOFFSET_2,
		SCE_GS_TEX2_2,
		SCE_GS_TEX1_2,
		SCE_GS_TEX0_2,
		SCE_GS_CLAMP_2,
		SCE_GS_MIPTBP1_2,
		SCE_GS_MIPTBP2_2,
		SCE_GS_SCISSOR_2,
		SCE_GS_ALPHA_2,
		SCE_GS_TEST_2,
		SCE_GS_FBA_2,
		SCE_GS_FRAME_2,
		SCE_GS_ZBUF_2,
	};

	u_char s_RegisterAddr_SHR[] =
	{
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 0,
		SCE_GS_TEXA,
		SCE_GS_FOGCOL,
		SCE_GS_DTHE,
		SCE_GS_DIMX,
		SCE_GS_PABE,
		SCE_GS_COLCLAMP,
		SCE_GS_PRIM,
		SCE_GS_PRMODE,
		SCE_GS_PRMODECONT,
	};


	void ClearAllVRam	(	uint32	inABGR	)
	{
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );

		uint32* tadr = DC->i32;

		gs::tools::Gs_Open_CH1( DC );
		gs::Set_RGBAQ( DC, inABGR );
		gs::tools::Gs_ApplyPass_CH1( DC, 1024, 1024, 0 );
//		gs::Write_FINISH( DC );
		gs::tools::Gs_Close_CH1( DC );
		*DC->i128++ = DMA_TAG_END;
		NV_ASSERT_DC( DC );

		dmac::Sync( dmac::CH1_VIF1_M );
//		DPUT_GS_CSR( GS_CSR_FINISH_M );
		FlushCache( WRITEBACK_DCACHE );
		dmac::Start_CH1( (void*)DMA_ADDR(tadr) );
		dmac::Sync( dmac::CH1_VIF1_M );
//		while( (DGET_GS_CSR() & GS_CSR_FINISH_M) == 0 ) {};

		DC->i32 = tadr;
	}

}



void
gs::Init(	bool	inEnableCRTC	)
{
	sceGsResetPath();
	sceGsSyncPath(0,0);

	gs_frame_Init();
	gs_crtc_Init( inEnableCRTC );

	bool   consoleOnly	 = FALSE;
	uint32 resetVRam     = 0;
	uint32 resetVRamRGBA = 0;
	nv::core::GetParameter( nv::core::PR_RESET_ALLVRAM,		 &resetVRam		);
	nv::core::GetParameter( nv::core::PR_RESET_ALLVRAM_RGBA, &resetVRamRGBA	);
	nv::core::GetParameter( nv::core::PR_CONSOLE_ONLY,		 &consoleOnly	);
	if( resetVRam && !consoleOnly )
		ClearAllVRam( GetDpyColorRGBA(PSM_ABGR32,resetVRamRGBA) );

	sceGsSyncPath(0,0);
}



void
gs::Reset(	bool	inEnableCRTC	)
{
	*GIF_CTRL = 1;		// RST=1 / PSE=0
	SyncRW();
	gs_frame_Reset();
	gs_crtc_Reset( inEnableCRTC );
}


void
gs::Shut()
{
	gs_frame_Shut();
	gs_crtc_Shut();
}



bool
gs::IsBusy()
{
	// check FQC, APATH-P3Q
	const uint32 busyMask = (0x1F<<24) | (0x3F<<6);
	return ((*GIF_STAT) & busyMask) != 0;
}



void
gs::Sync()
{
	while( IsBusy() )
		WaitCycleCpt( 32 );
}


bool
gs::IsPaused	(			)
{
	tGIF_STAT gifStat;
	AS_UINT32( gifStat ) = *GIF_STAT;
	return ( gifStat.PSE ? TRUE : FALSE );
}


void
gs::Pause	(		)
{
	*GIF_CTRL = 1<<3;	// PSE=1
	SyncRW();
}


void
gs::Resume		(			)
{
	*GIF_CTRL = 0;		// PSE=0
	SyncRW();
}


void
gs::FlushContext_CH1(	dmac::Cursor *	inDC,
						const Context *	inCtxt,
						uint32			inRegistersMask,
						uint32			inContextsMask )
{
	NV_ASSERT_DC( inDC );
	if( !inCtxt )					return;
	if( inRegistersMask == 0 )		return;

	uint32 *	start = inDC->i32;
	inDC->i128 += 2; // JUMP OVER DMA AND THE GIF TAG
	uint32		qwc = 2;

	uint32		mask = inRegistersMask, mask1;
	uint32		index;
	uint64 *	cregs = (uint64*)inCtxt;

	while( mask )
	{
		// Reg index
		index = GetLowestBitNo( mask );
		mask1 = (1L<<index);
		mask ^= mask1;

		if( mask1 & CONTEXT_SPECIFIC )
		{
			if( inContextsMask & CONTEXT1 )
			{
				*(inDC->i64)++ = cregs[ index ];
				*(inDC->i64)++ = s_RegisterAddr_CTX1[ index ];
				qwc++;
			}
			if( inContextsMask & CONTEXT2 )
			{
 				*(inDC->i64)++ = cregs[ index ];
				*(inDC->i64)++ = s_RegisterAddr_CTX2[ index ];
				qwc++;
			}
		}
		else	// SHARED CONTEXT
		{
			*(inDC->i64)++ = cregs[ index ];
			*(inDC->i64)++ = s_RegisterAddr_SHR[ index ];
			qwc++;
		}
	}

	// PRMODE or PRMODECONT have changed ?
	#ifdef _NVCOMP_ENABLE_CONSOLE
	if(		(inRegistersMask&(PRMODE|PRMODECONT))
		&&	(inCtxt != &(frame::DefContext)) )
	{
		NV_WARNING( "PRMODE changed !" );
	}
	#endif

	// Add DMA Tag
	*start++ = DMA_TAG_CNT | (qwc-1);
	*start++ = 0;
	*start++ = 0;
	*start++ = SCE_VIF1_SET_DIRECT( (qwc-1), 0 );

	// Add GIF Tag
	*((uint64*)start)++ = SCE_GIF_SET_PACKED_TAG( (qwc-2), 1, 0, 0, 1 );
	*((uint64*)start)++ = 0x0E;	// A+D

	NV_ASSERT_DC( inDC );
}



//
// PATH 3

namespace
{

	ALIGNED512( uint128, path3_sync_padding[24] ) =	{
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0
	};

	// open PATH3 & VIF1-FIFO padding with 24QW
	ALIGNED512( uint32, ch1_open_path3[] ) = {
		DMA_TAG_REFE | 24,
		DMA_ADDR( path3_sync_padding ),
		0,
		SCE_VIF1_SET_MSKPATH3( 0, 0 )
	};

	// close PATH3 & VIF1-FIFO padding with 24QW
	ALIGNED512( uint32, ch1_close_path3[] ) = {
		DMA_TAG_REFE | 24,
		DMA_ADDR( path3_sync_padding ),
		0,
		SCE_VIF1_SET_MSKPATH3( 0x8000, 0 )
	};

}


void
gs::SetupPATH3_CH1	(	int	inM3R,	int	inM3P,	int	inIMT	)
{
	dmac::Sync( dmac::CH1_VIF1_M | dmac::CH2_GIF_M );

	uint MODE = (inIMT<<2) | (inM3P<<1) | inM3R;
	*GIF_MODE = MODE;
	SyncRW();

	dmac::Start_CH1( inM3P ? ch1_close_path3 : ch1_open_path3 );
	dmac::Sync( dmac::CH1_VIF1_M | dmac::CH2_GIF_M );

	// Checks status
	// VIF1-FIFO is 16QW and path3_[open|close]_ch1 is 24QW padding.
	// This asserts that vifcode MSKPATH3 has been executed when CH1 is finished.
	SyncRW();
	NV_ASSERT( ((*GIF_STAT)&7) == MODE );
	if( ((*GIF_STAT)&7) != MODE ) {
		Printf( "<Neova> SetupPATH3_CH1() has failed !\n" );
	}
}


void
gs::InsertPATH3_CH1	(	dmac::Cursor*	inDC,
						pvoid&			ioLinkCH1,
						bool			inSync,
						bool			inContinue	)
{
	NV_ASSERT_DC( inDC );
	NV_ASSERT_A128( ioLinkCH1 );

	// sync PATH3 ?
	// Problem can occurs with VIF1_FLUSHA
	// => Read the VIF1_FLUSHA bug report technical note below.
	if( inSync || inContinue )
	{
		inDC->i64[0] = DMA_TAG_END | 3;
		inDC->i32[2] = SCE_VIF1_SET_FLUSH(0);
		inDC->i32[3] = SCE_VIF1_SET_DIRECT( 2, 0 );
		inDC->i64[2] = SCE_GIF_SET_PACKED_TAG( 1, 1, 0, 0, 1 );
		inDC->i64[3] = 0x0E;		// A+D
		inDC->i64[4] = 0;
		inDC->i64[5] = SCE_GS_NOP;
		inDC->i64[6] = 0;
		inDC->i64[7] = SCE_VIF1_SET_FLUSHA(0);
		SET_DMATAG_NEXT( ioLinkCH1, inDC->i128 );
		ioLinkCH1    = inDC->i128;
		inDC->i128  += 4;
	}

	// PATH3 continue ?
	if( inContinue )
	{
		inDC->i32[0] = DMA_TAG_REF | 24;
		inDC->i32[1] = DMA_ADDR( path3_sync_padding );
		inDC->i64[1] = SCE_VIF1_SET_MSKPATH3(0,0);			// open PATH3
		inDC->i64[2] = DMA_TAG_END | 0;
		inDC->i64[3] = SCE_VIF1_SET_MSKPATH3(0x8000,0);		// close PATH3
		SET_DMATAG_NEXT( ioLinkCH1, inDC->i128 );
		ioLinkCH1    = inDC->i128+1;
		inDC->i128  += 2;
	}
}


/*

	VIFcode FLUSHA Command Problem - Bug Report
	===========================================

	This problem occurs when the internal flag signal used for FLUSHA synchronization is set and reset 
	simultaneously. Although you would expect the set to take precedence, in actuality, the reset takes 
	precedence. This causes the synchronization flag used by the FLUSHA command to get negated and the 
	wait state for the stall to get cancelled, even if a GIF transfer is being performed.

	The problem occurs when the following conditions are satisified.

	  (1) The problem occurs during the third 300-MHz cycle after the GIF transfer
	  (2) The GIF is stopped in this cycle
	  (3) The GIF is activated during this cycle

	"GIF transfer" and "GIF activation" refer to transfers on each of the paths: PATH1, PATH2 and
	PATH3.


	Recommended workaround
	----------------------

	- The best way to avoid this problem is to use the FLUSH+DIRECT+FLUSHA commands in place of the 
	  FLUSHA command.

	The FLUSH command ensures that the VU will be stopped (by making sure that PATH1 will not be 
	activated), and that PATH1 and PATH2 will be stopped. The DIRECT command ensures that PATH3 will be 
	stopped. The subsequent FLUSHA command also ensures that PATH3 will be stopped when the DIRECT 
	command completes when in IMT mode.

	Since it may be that after the FLUSH command performs synchronization, PATH3 will still be 
	operating, a dummy DIRECT command (PATH2) transfer is initiated, and the GIF activation arbiter will
	wait for the PATH3 transfer to complete.

	When a PATH3 transfer is being performed in IMAGE mode with IMT set to on, GIF activation 
	arbitration will occur every 8QW. Consequently, the dummy DIRECT command (PATH2) may end up getting 
	activated during this PATH3 transfer when IMAGE mode is terminated.

	However, when PATH3 recurs because of the IMT setting, since the DIRECT command will always cause 
	PATH3 to be activated one cycle after the PATH2 transfer completes, the problem described above in 
	(1) will not occur. As a result, the FLUSHA command will work normally after the return from IMT, 
	and synchronization with PATH3 termination can be achieved.


	Other workarounds that were considered
	--------------------------------------

	- DIRECT+FLUSHA

	Since the DIRECT command cannot ensure that the VU is stopped, it is possible that PATH1 may become 
	activated after the command completes. Consequently, after PATH3 terminates, because of the timing 
	of PATH1 activation, the FLUSHA command may not be able to accomplish synchronization.

	- FLUSH+FLUSHA or FLUSHA+FLUSH

	When a subsequent GIF transfer is restricted to using PATH1 or PATH2, the problem can be avoided by 
	using the above commands. This is also true even if PATH3 is available for a succeeding transfer.

	However, if PATH3 gets activated one 300-MHz cycle before it is masked, the FLUSHA command may not 
	be able to accomplish synchronization.

	For the FLUSHA+FLUSH case, a PATH1 or PATH2 transfer that gets through the FLUSHA command will be 
	synchronized by the FLUSH command. In this case, however, it is necessary to wait for data to be 
	output from the EE (three cycles after the GIF transfer), and substitute the FLUSH+NOP+NOP commands 
	in order to perform the equivalent function of the FLUSHA command.

	- FLUSH+DIRECT

	 When the subsequent PATH3 state is neither an IMAGE mode transfer nor one in which IMT is enabled, 
	 the problem can be avoided.

	In this case, the FLUSH command will not be able to synchronize PATH3, however the DIRECT command 
	will perform the synchronization because of the PATH2 transfer. However, if this PATH2 transfer 
	begins during a PATH3 transfer as a result of the IMT arbitration every 8QW, synchronization may 
	still not be able to be achieved.

*/


