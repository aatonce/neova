/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <libipu.h>
using namespace nv;



#define DTD(flags)				(((flags) >> 2) & 0x1)
#define IPU_CTRL_VAL(flags)		(((flags) & ~0x00000004) << 16)



namespace
{

	ipu::Callback*	ipuCB;

	int				toipu_handlerId;
	int				fromipu_handlerId;

	int ch34_handler( int ch )
	{
		int streamId = (ch == DMAC_TO_IPU) ? 0 : 1;
		if( ipuCB )
			ipuCB->OnStreamEnded( streamId );
		ExitHandler();
		return(0);
	}

	u_int getFlags()
	{
	    u_int ret;
	    sceIpuFDEC(0);
		ipu::Sync();
	    ret = sceIpuGetFVdecResult();
	    ret = (ret >> 24) & 0xff;	// get flags;
		ipu::Sync();
	    sceIpuFDEC(8);				// proceed 7 bit
		ipu::Sync();
	    return ret;
	}

}


void
ipu::Init		(				)
{
	sceIpuInit();
	Reset();

    toipu_handlerId   = AddDmacHandler( DMAC_TO_IPU, ch34_handler, 0 );
    fromipu_handlerId = AddDmacHandler( DMAC_FROM_IPU, ch34_handler, 0 );
	EnableDmac( DMAC_TO_IPU );
	EnableDmac( DMAC_FROM_IPU );
}


void
ipu::Reset		(				)
{
	DI();	// disable dmac handler
	dmac::Stop( dmac::CH3_FROM_IPU_M | dmac::CH4_TO_IPU_M );
	EI();

	// abort cmd
	sceIpuReset();
	Sync();

	// reset in_FIFO
	sceIpuBCLR( 0 );
	Sync();

	ipuCB = NULL;
}


void
ipu::Shut		(				)
{
	Reset();

    DisableDmac( DMAC_TO_IPU );
    DisableDmac( DMAC_FROM_IPU );
    RemoveDmacHandler( DMAC_TO_IPU, toipu_handlerId );
    RemoveDmacHandler( DMAC_FROM_IPU, fromipu_handlerId );
}


ipu::Callback*
ipu::GetCallback	(			)
{
	return ipuCB;
}


void
ipu::SetCallback	(	Callback*			inCallback		)
{
	ipuCB = inCallback;
}


bool
ipu::IsError			(			)
{
	return sceIpuIsError();
}


bool
ipu::IsBusy		(		)
{
	return sceIpuIsBusy();
}


void
ipu::Sync			(		)
{
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	while( sceIpuIsBusy() ) {
		clock::GetTime( &t1 );
		if( (t1-t0) > 1.f ) {
			NV_WARNING( "ipu::Sync() stalls !" );
			Printf( "   (is=%d os=%d error=%d)\n",
					int(ipu::Has_OutputStream()),
					int(ipu::Has_InputStream()),
					int(ipu::IsError())		);
		}
	}
}



void
ipu::GetStreams		(	Stream&				outStream		)
{
	outStream.ch4_tadr		= *D4_TADR;
	outStream.ch4_madr		= *D4_MADR;
	outStream.ch4_qwc		= *D4_QWC;
	uint32 ipu_bp			= *IPU_BP;
	outStream.bp	 		= ipu_bp & IPU_BP_BP_M;
	outStream.in_fifo_qwc	= ( (ipu_bp&IPU_BP_IFC_M) >> IPU_BP_IFC_O )
							+ ( (ipu_bp&IPU_BP_FP_M)  >> IPU_BP_FP_O  );

	outStream.ch3_madr		= *D3_MADR;
	outStream.ch3_qwc		= *D3_QWC;
}


bool
ipu::Has_InputStream	(			)
{
	return ((*D4_CHCR) & D_CHCR_STR_M) != 0;
}


bool
ipu::Has_OutputStream	(		)
{
	return ((*D3_CHCR) & D_CHCR_STR_M) != 0;
}


bool
ipu::BeginFrame_Input	(	uint				inBP,								// in [0,127]
							void*				inPtr_0,
							uint				inQSize_0,
							void*				inPtr_1,
							uint				inQSize_1	)
{
	NV_ASSERT( inBP <= 127U );
	NV_ASSERT_NA128( inPtr_0 );
	NV_ASSERT_NA128( inPtr_1 );
	NV_ASSERT( inQSize_0 < 65534 );
	NV_ASSERT( inQSize_1 < 65534 );
	if( ipu::IsBusy() || ipu::Has_InputStream() )
		return FALSE;

	// nothing to do with (ptr0,qsize0) ?
	if( !inPtr_0 || !inQSize_0 ) {
		Swap( inPtr_0,   inPtr_1 );
		Swap( inQSize_0, inQSize_1 );
	}

	// setup IPU bit-position before trx'ing
	sceIpuBCLR( inBP );
	Sync();

	uint CIM = ipuCB ? dmac::GetEnableI( dmac::CH4_TO_IPU_M ) : dmac::GetDisableI( dmac::CH4_TO_IPU_M );
	*D_STAT = D_STAT_CIS4_M | CIM;

	if( inPtr_1 && inQSize_1 )
	{
		// by TADR
		static ALIGNED128( uint32, tags[8] );
		tags[0] = DMA_TAG_REF | inQSize_0;
		tags[1] = DMA_ADDR( inPtr_0 );
		tags[2] = 0;
		tags[3] = 0;
		tags[4] = DMA_TAG_REFE | inQSize_1;
		tags[5] = DMA_ADDR( inPtr_1 );
		tags[6] = 0;
		tags[7] = 0;

		FlushCache( WRITEBACK_DCACHE );
		*D4_QWC = 0;
		*D4_TADR = DMA_ADDR( tags );
		SyncRW();
		*D4_CHCR = (1<<D_CHCR_DIR_O)
				 | (1<<D_CHCR_MOD_O)		// source chain
				 | (1<<D_CHCR_STR_O);
	}
	else
	{
		// by MADR
		*D4_QWC  = inQSize_0;
		*D4_MADR = DMA_ADDR( inPtr_0 );
		SyncRW();
		*D4_CHCR = (1<<D_CHCR_DIR_O)
				 | (1<<D_CHCR_STR_O);
	}

	// Get frame flags from bstream
    uint flags = getFlags();

	// write IPU_CTRL.MP1 to .IDP
    DPUT_IPU_CTRL(IPU_CTRL_VAL(flags));

	// IDEC with .DTD from bstream
    sceIpuIDEC( 0, 0, 0, DTD(flags), 0, 0 );

    return TRUE;
}



bool
ipu::BeginFrame_Output	(	void*				inPtr,
							uint				inQSize			)
{
	if( !inPtr || !inQSize )
		return FALSE;
	if( Has_OutputStream() )
		return FALSE;
	dmac::FeedFromIPU( inPtr, inQSize, (ipuCB!=NULL) );
	return TRUE;
}


void
ipu::EndFrame		(	Stream&				outStream	)
{
	// wait IDEC cmd to complete
	Sync();

	DI();	// disable dmac handler
	dmac::Stop( dmac::CH3_FROM_IPU_M | dmac::CH4_TO_IPU_M );
	EI();

	GetStreams( outStream );
}


void
ipu::ResetFrame		(				)
{
	DI();	// disable dmac handler
	dmac::Stop( dmac::CH3_FROM_IPU_M | dmac::CH4_TO_IPU_M );
	EI();

	// abort cmd
	sceIpuReset();
	Sync();
	// reset in_FIFO
	sceIpuBCLR( 0 );
	Sync();
}



