/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
typedef unsigned int		uint;
typedef unsigned long long	uint64;


float			sitofp(int);
int				fptosi(float);
uint			fptoui(float);
double			fptodp(float);
double			litodp(long);
float			dptofp(double);
void		 	dptoli(void);

double			dpadd(double,double);
double			dpsub(double,double);
double			dpmul(double,double);
double			dpdiv(double,double);
int				dpcmp(double,double);
int				_dpfeq(double left, double right);
int				_dpfne(double left, double right);
int				_dpflt(double left, double right);
int				_dpfle(double left, double right);
int				_dpfgt(double left, double right);
int				_dpfge(double left, double right);
*/

/*
float dptofp(double _Value)
{
	uint64  data = *(uint64*)&_Value;
	if (data == 0) return 0;
	uint64 f = data & 0x0FFFFFFFFFFFFF;
	uint64 e = (data >> 52) & 0x7FF;
	f >>= 29;
	int e2 = (int)e;
	e2 = (e2 - 1023) + 127;
	if (e2<0) e2 = 0;
	if (e2>255) e2 = 255;
	uint d = (uint)((e2 << 23) | f | ((data >> 32) & 0x80000000));
	return (*(float*)&d);
}

int dpcmp(double a,double b)
{
	float af,bf,rf;
	af = dptofp(a);
	bf = dptofp(b);
	if (af < bf) return -1;
	if (af == bf) return 0;
	return 1;
}
*/

/*
int fptosi(float v)
{
	uint data = *(uint*)&v;
	if (data == 0) return 0;
	uint f = data & 0x7FFFFF | 0x800000;
	uint e = (data >> 23) & 0xFF;
	if (e < 127) return 0;
	int i = f >> (23 - (e-127));
	if (data & 0x80000000) return -i;
	return i;
}

float sitofp(int v)
{
	uint data = v;
	if( v < 0) data = -v;

	if (data == 0) return 0.f;

	int e = 1;
	while (data >> e) e++;

	if (e <= 23)
		data <<= 24 - e;
	else
		data >>= e - 24;
	
	data &= 0x7FFFFF;
	
	data |= ((e-1)+127) << 23;

	if( v < 0) data &= 0x80000000;
	
	return *(float*)&data;
}

UINT fptoui(FLOAT _Value)
{
	UINT data = *(UINT*)&_Value;
	if (data == 0) return 0;
	UINT f = data & 0x7FFFFF | 0x800000;
	UINT e = (data >> 23) & 0xFF;
	if (e < 127) return 0;
	INT i = f >> (23 - (e-127));
	return i;
}

uint64

INT dptosi(double _Value)
{
	UINT64  data = *(UINT64*)&_Value;
	if (data == 0) return 0;
	UINT64 f = data & 0x0FFFFFFFFFFFFF | (0x10000000000000);
	UINT64 e = (data >> 52) & 0x7FF;
	if (e < 1023) return 0;
	UINT64 t = f >> (52 - (e-1023));
	INT i = (INT)t;
	if (data & 0x8000000000000000) return -i;
	return i;
}


UINT dptoui(double _Value)
{
	UINT64  data = *(UINT64*)&_Value;
	if (data == 0) return 0;
	UINT64 f = data & 0x0FFFFFFFFFFFFF | (0x10000000000000);
	UINT64 e = (data >> 52) & 0x7FF;
	if (e < 1023) return 0;
	UINT64 t = f >> (52 - (e-1023));
	UINT i = (UINT)t;
	return i;
}

double fptodp(FLOAT _Value)
{
	UINT64 data = *(UINT*)&_Value;

	if (data == 0) return 0;

	UINT64 f = data & 0x7FFFFF;
	UINT64 e = (data >> 23) & 0xFF;

	f <<= 29;
	e += (1023-127);
	
	UINT64 d = (e << 52) | f | ((data & 0x80000000) << 32);
	
	return (*(double*)&d);
}

double dpadd(double a,double b)
{
	FLOAT af,bf,rf;
	af = dptofp(a);
	bf = dptofp(b);
	rf = af + bf;
	return fptodp(rf);
}


double dpsub(double a,double b)
{
	FLOAT af,bf,rf;
	af = dptofp(a);
	bf = dptofp(b);
	rf = af - bf;
	return fptodp(rf);
}

double dpmul(double a,double b)
{
	FLOAT af,bf,rf;
	af = dptofp(a);
	bf = dptofp(b);
	rf = af * bf;
	return fptodp(rf);
}

double dpdiv(double a,double b)
{
	FLOAT af,bf,rf;
	af = dptofp(a);
	bf = dptofp(b);
	rf = af / bf;
	return fptodp(rf);
}

void dptoli(void)
{
}

double litodp(long _v)
{
	FLOAT f = sitofp((INT)_v);
	return fptodp(f);
}
*/



/*
 *	The GCC runtime has only one float compare function that
 *	return a number to tell how the float compare.  Our backend
 *	assume the compare functions will return a boolean.  
 *	This wrapper makes the intreface between GCC and our backend.
 *
 */

/*
int _dpfeq(double left, double right)
{
	return dpcmp(left, right) == 0;
}

int _dpfne(double left, double right)
{
	return dpcmp(left, right) != 0;
}

int _dpflt(double left, double right)
{
	return dpcmp(left, right) < 0;
}

int _dpfle(double left, double right)
{
	return dpcmp(left, right) <= 0;
}

int _dpfgt(double left, double right)
{
	return dpcmp(left, right) > 0;
}

int _dpfge(double left, double right)
{
	return dpcmp(left, right) >= 0;
}
*/

#ifdef __cplusplus
}
#endif


