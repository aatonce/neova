/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
using namespace nv;



#define	VU_MC_MAX	64



namespace
{

	typedef struct {
		union {
			uint128	q;
			uint64	d[2];
			uint32	s[4];
		};
	} qword;

	void vu0MemWriteQ( uint16 vuaddr, uint128 value )
	{
		register uint128 reg = value;
		register uint128 fbak;
		register uint ibak;
		__asm__ volatile (
			"cfc2 %0, $vi01\n"
			"qmfc2 %1, $vf01\n"
			"qmtc2 %2, $vf01\n"
			"ctc2 %3, $vi01\n"
			"vsqi.xyzw $vf01, ($vi01++)\n"
			"ctc2 %0, $vi01\n"
			"qmtc2 %1, $vf01\n"
			: "=&r"(ibak), "=&r"(fbak) : "r"(reg), "r"(vuaddr)
		);
	}


	uint128 vu0MemReadQ( uint16 vuaddr )
	{
		register uint128 fbak;
		register uint ibak;
		register uint128 reg;
		__asm__ volatile (
			"cfc2 %0, $vi01\n"
			"qmfc2 %1, $vf01\n"
			"ctc2 %3, $vi01\n"
			"vlqi.xyzw $vf01, ($vi01++)\n"
			"qmfc2 %2, $vf01\n"
			"ctc2 %0, $vi01\n"
			"qmtc2 %1, $vf01\n"
			: "=&r"(ibak), "=&r"(fbak), "=r"(reg) : "r"(vuaddr)
		);
		return reg;
	}


	struct VUText {
		uint32	bstart;
		uint32	bsize;
		int16	vuno;					// 0/1
		int16	execIAddr;
	};


	VUText		vuText[ VU_MC_MAX ];
	int			vuCurId;
	uint32		vuLastExecAddr	[2];		// 0/1

}



void
vu::Init()
{
	Reset();
}



void
vu::Reset	(	)
{
	sceDevVif0Reset();		// resets the entire VIF0 including VIF0-FIF0
	sceDevVif1Reset();		// resets the entire VIF1 including VIF1-FIF0
	sceDevVu0Reset();
	sceDevVu1Reset();

	// Mask all (see EE User's Manual or Restriction.pdf)
	DPUT_VIF0_ERR( 0xF );
	DPUT_VIF1_ERR( 0xF );

	// Clear MicroMem0/1 & VUMem0/1
	Memset( (void*)VU0_MEM,		0xCD,	4*1024 );
	Memset( (void*)VU0_MICRO,	0xCD,	4*1024 );
	Memset( (void*)VU1_MEM,		0xCD,	16*1024 );
	Memset( (void*)VU1_MICRO,	0xCD,	16*1024 );

	vuCurId				= 0;
	vuLastExecAddr[0]	= 0;
	vuLastExecAddr[1]	= 0;
}


void
vu::Shut	(	)
{
	sceDevVif0Reset();		// resets the entire VIF0 including VIF0-FIF0
	sceDevVif1Reset();		// resets the entire VIF1 including VIF1-FIF0
	sceDevVu0Reset();
	sceDevVu1Reset();
}


uint32
vu::GetVPU_STAT		(		)
{
	register u_int reg;
   	__asm__ volatile( "cfc2 %0, $vi29" : "=r"(reg) );
   	return reg;
}


uint16
vu::GetVPU_TPC		(	int		inVUNo	)
{
	NV_ASSERT( inVUNo==0 || inVUNo==1 );
	if( inVUNo==0 ) {
		register u_int reg;
	   	__asm__ volatile( "cfc2 %0, $vi26" : "=r"(reg) );
	   	return reg;
	} else {
		qword qw;
		qw.q = vu0MemReadQ( 0x43a );
		return qw.d[0] & 0xFFFF;
	}
}


bool
vu::IsVIFBusy(		int		inVUNo	 )
{
	NV_ASSERT( inVUNo==0 || inVUNo==1 );
	if( inVUNo==0 )			return ((*VIF0_STAT)&(0x7|(0xF<<24))) != 0;
	else if( inVUNo==1 )	return ((*VIF1_STAT)&(0x7|(0xF<<24))) != 0;
	return FALSE;
}


bool
vu::IsVPUBusy(		int		inVUNo	 )
{
	NV_ASSERT( inVUNo==0 || inVUNo==1 );
	uint32 vpu_stat = GetVPU_STAT();
	if( inVUNo==0 )			return ((vpu_stat>>0)&15) == 1;
	else if( inVUNo==1 )	return ((vpu_stat>>8)&15) == 1;
	return FALSE;
}


void
vu::SyncVIF		(	int			inVUNo		)
{
	NV_ASSERT( inVUNo==0 || inVUNo==1 );
	while( IsVIFBusy(inVUNo) )
		WaitCycleCpt( 64 );
}


void
vu::SyncVPU		(	int			inVUNo		)
{
	NV_ASSERT( inVUNo==0 || inVUNo==1 );
	while( IsVPUBusy(inVUNo) )
		WaitCycleCpt( 64 );
}


uint16
vu::GetVU1_IR	(	int			inIRNo		)
{
	qword qw;
	qw.q = vu0MemReadQ(0x420+inIRNo);
	return qw.d[0] & 0xFFFF;
}


Vec4
vu::GetVU1_FR	(	int			inIFNo		)
{
	uint128 u128 = vu0MemReadQ(0x400+inIFNo);
	return Vec4( (float*)&u128 );
}


void
vu::ForceBreak	(	int		inVUNo		)
{
	NV_ASSERT( inVUNo==0 || inVUNo==1 );

	if( inVUNo == 0 )
	{
		// VIF0.FBRST.FBK/RST=1
		*VIF0_FBRST = 0x3;

		// VU0.FBRST.FB1/0 = 1
		__asm__ volatile (
	    "cfc2 $2, $vi28\n"
	    "ori  $2, $2, 0x1\n"
	    "ctc2 $2, $vi28\n"
	    : : : "$2"	);
	}
	else
	{
		// VIF1.FBRST.FBK/RST=1
		*VIF1_FBRST = 0x3;

		// VU1.FBRST.FB1/0 = 1
		__asm__ volatile (
	    "cfc2 $2, $vi28\n"
	    "ori  $2, $2, 0x100\n"
	    "ctc2 $2, $vi28\n"
	    : : : "$2"	);
	}
}


bool
vu::ContinueVU1		(	uint16		inAddr		)
{
	uint32 vpu_stat = GetVPU_STAT();
	// VU1 busy ?
//	if( vpu_stat&0x100 )
//		return FALSE;
	// ForceBreaked ?
//	if( ((vpu_stat>>11)&1) == 0 )
//		return FALSE;
	register u_int reg = inAddr;
	__asm__ volatile( "ctc2 %0, $vi31" :: "r"(reg) );
	return TRUE;
}


void
vu::Break_PATH12	(			)
{
	dmac::Stop( dmac::CH1_VIF1_M | dmac::CH2_GIF_M );
	vu::ForceBreak( 0 );
	vu::ForceBreak( 1 );
}


uint16
vu::StepVU1	(	uint32	inCycDelay	)
{
	uint16 tpc = GetVPU_TPC( 1 );
	if( !ContinueVU1(tpc) )
		return ~0U;
	if( inCycDelay )
		WaitCycleCpt( inCycDelay );
	ForceBreak( 1 );
	tpc = GetVPU_TPC( 1 );
	return tpc;
}


void
vu::ResetMCCache	(	int		inVUNo	 )
{
	NV_ASSERT( inVUNo==0 || inVUNo==1 );
	NV_ASSERT( vuCurId <= VU_MC_MAX );
	for( int i = 0 ; i < vuCurId ; i++ ) {
		if( vuText[i].vuno == inVUNo )
			vuText[i].execIAddr = -1;
	}
	vuLastExecAddr[inVUNo] = 0;
}


int
vu::RegisterMC	(	int				inVUNo,
					uint*			inMCStart,
					uint*			inMCEnd		)
{
	NV_ASSERT( inVUNo==0 || inVUNo==1 );
	NV_ASSERT( vuCurId <= VU_MC_MAX );
	if( inVUNo!=0 && inVUNo!=1 )
		return -1;

	// QW aligned ?
	NV_ASSERT_A128( inMCStart );
	NV_ASSERT_A128( inMCEnd	);
	if( uint32(inMCStart)&0xF || uint32(inMCEnd)&0xF )
		return -1;

	// remains ID ?
	if( vuCurId >= VU_MC_MAX ) {
		NV_ERROR( "vu:: Too many microcodes registered !\n" );
		return -1;
	}

	int mcId = vuCurId++;

	vuText[mcId].bstart		= uint32(inMCStart) & DMA_MEM;
	vuText[mcId].bsize		= uint32(inMCEnd)-uint32(inMCStart);
	vuText[mcId].vuno		= inVUNo;
	vuText[mcId].execIAddr	= -1;

	return mcId;
}



bool
vu::UploadMC	(	dmac::Cursor*		inDC,
					int					inMCNo		)
{
	NV_ASSERT_DC( inDC );
	NV_ASSERT( inMCNo>=0 && inMCNo<vuCurId );
	VUText* vut = & vuText[inMCNo];

	if( vut->execIAddr >= 0 )
		return FALSE;

	int16 vuEndExecAddr = vut->vuno ? 2048 : 512;
	vut->execIAddr = vuLastExecAddr[vut->vuno];
	vuLastExecAddr[vut->vuno] += (vut->bsize)>>3;

	// Overflow ?
	if( vuLastExecAddr[vut->vuno] > vuEndExecAddr ) {
		ResetMCCache( vut->vuno );
		vut->execIAddr = vuLastExecAddr[vut->vuno];
		vuLastExecAddr[vut->vuno] += (vut->bsize)>>3;
		NV_ASSERT( vuLastExecAddr[vut->vuno] < vuEndExecAddr );
	}

	NV_ASSERT( vut->execIAddr >= 0 );
	NV_ASSERT_A128( vut->bsize );		// taille multiple de 16 pour le REF

	uint remain_mpgISize		= (vut->bsize)>>3;
	uint remain_mpgExecIAddr	= vut->execIAddr;
	uint remain_mpgBStart		= vut->bstart;

	while( remain_mpgISize ) {
		uint mpgISize = Min( remain_mpgISize, 254U );
		NV_ASSERT_A128(remain_mpgBStart );

		inDC->i32[0] = DMA_TAG_REF | (mpgISize>>1);
		inDC->i32[1] = remain_mpgBStart;
		inDC->i32[2] = 0;
		inDC->i32[3] = SCE_VIF1_SET_MPG( remain_mpgExecIAddr, mpgISize, 0 );
		inDC->i32   += 4;

		remain_mpgISize		-= mpgISize;
		remain_mpgExecIAddr += mpgISize;
		remain_mpgBStart	+= mpgISize<<3;
	}

	return TRUE;
}


uint32*
vu::RequestMC	(	dmac::Cursor *		inDC,
					int					inMCNo		)
{
	NV_ASSERT_DC( inDC );
	NV_ASSERT( inMCNo>=0 && inMCNo<vuCurId );
	VUText* vut = & vuText[inMCNo];

	UploadMC( inDC, inMCNo );
	NV_ASSERT( vut->execIAddr >= 0 );

	uint32* link = inDC->i32;
	inDC->i64[0] = DMA_TAG_END | 0;
	inDC->i32[2] = 0;
	inDC->i32[3] = SCE_VIF0_SET_MSCAL( vut->execIAddr, 0 );
	inDC->i32   += 4;
	NV_ASSERT_DC( inDC );

	return link;
}


int
vu::GetMCExecIAddr	(	int		inMCNo	)
{
	NV_ASSERT( inMCNo>=0 && inMCNo<vuCurId );
	if( inMCNo<0 || inMCNo>=vuCurId )
		return -1;
	VUText* vut = & vuText[inMCNo];
	return vut->execIAddr;
}



