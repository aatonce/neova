/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <libdev.h>
using namespace nv;


#define	SYNC_USING_CPCOND





namespace
{

	dmac::Cursor	dcBuff[2];
	dmac::Cursor *	frontDC;
	dmac::Cursor *	backDC;

}



void
dmac::Init()
{
	// DMA Buffer
	uint32 dmaHeaps;
	bool   dmaFastUncached;
	core::GetParameter( nv::core::PR_EE_DMAC_HEAPS_BSIZE, &dmaHeaps );
	core::GetParameter( nv::core::PR_EE_DMAC_HEAPS_UNCACHE, &dmaFastUncached );

	// DMA Heap : 64bytes D$ line alignment to prevent
	// concurrent cached & uncached memory access !
	uint32 dmaHeap = (dmaHeaps>>1) & 0xFFFFFFF0;
	void * dmaBuff0 = EngineMallocA( dmaHeap, 64 );
	void * dmaBuff1 = EngineMallocA( dmaHeap, 64 );
	FlushCache( WRITEBACK_DCACHE );

	dcBuff[0].i8	= dcBuff[0].start = (uint8*) ( dmaFastUncached ? FastUncachedPointer(dmaBuff0) : dmaBuff0 );
	dcBuff[0].bsize = dmaHeap;
	dcBuff[0].tag	= NULL;

	dcBuff[1].i8	= dcBuff[1].start = (uint8*) ( dmaFastUncached ? FastUncachedPointer(dmaBuff1) : dmaBuff1 );
	dcBuff[1].bsize = dmaHeap;
	dcBuff[1].tag	= NULL;

	Printf( "<Neova> DMAC heaps: MODE=%s  BSIZE=2*%dKo @0=0x%08x @1=0x%08x\n",
			dmaFastUncached ? "D$-fast-uncached" : "D$-cached",
			(dmaHeap>>10),
			uint32(dmaBuff0),
			uint32(dmaBuff1)	);

	Printf( "<Neova> DMAC async heads: @CH1=0x%08x @CH2=0x%08x\n",
			uint32(frame::GetAsyncFirstCH1()),
			uint32(frame::GetAsyncFirstCH2())	);

	Reset();
}



void
dmac::Reset()
{
	sceDmaReset( 1 );

	// Clear all CISn states & disable all interrupt handlers CIMn
	*D_STAT = CH_ALL_M | GetDisableI( CH_ALL_M );
	*D_PCR	= 0;						// Priority reset
	DisableCycStealing();

	frontDC			= & dcBuff[0];
	backDC			= & dcBuff[1];
	frontDC->i8		= frontDC->start;
	frontDC->tag	= NULL;
	backDC->i8		= backDC->start;
	backDC->tag		= NULL;
}


void
dmac::Shut()
{
	dmac::Sync( dmac::CH_ALL_M );
	EngineFree( (void*)DMA_ADDR(dcBuff[0].start) );
	EngineFree( (void*)DMA_ADDR(dcBuff[1].start) );
}


void
dmac::EnableCycStealing		(				)
{
	*D_CTRL	= (1<<D_CTRL_DMAE_O)		// Enable all Channels
		    | (1<<D_CTRL_RELE_O)		// Cycle Stealing On !
		    | (0<<D_CTRL_RCYC_O);		// 8 cycles release
	SyncMemory();
}


void
dmac::DisableCycStealing	(				)
{
	*D_CTRL	= (1<<D_CTRL_DMAE_O)		// Enable all Channels
		    | (0<<D_CTRL_RCYC_O);		// 8 cycles release
	SyncMemory();
}


//
//	HEAP BUFFER


void
dmac::SwapHeap()
{
	NV_ASSERT_DC( frontDC );
	NV_ASSERT_DC( backDC  );

	// swap
	Swap( frontDC, backDC );

	// rewind front
	frontDC->i8  = frontDC->start;
	frontDC->tag = NULL;
	NV_ASSERT_DC( frontDC );
}

dmac::Cursor *
dmac::GetFrontHeapCursor()
{
	NV_ASSERT_DC( frontDC );
	return frontDC;
}

dmac::Cursor *
dmac::GetBackHeapCursor()
{
	NV_ASSERT_DC( backDC );
	return backDC;
}


//
//	CURSOR



void
dmac::Cursor::Memcpy( void * inPtr, uint inBSize )
{
	NV_ASSERT_DC( this );
	NV_ASSERT( inPtr );
	if( !inBSize )	return;
	libc::Memcpy( vd, inPtr, inBSize );
	i8 += inBSize;
}


void *
dmac::Cursor::Dup(	void *	inStart, uint inBAlignement	)
{
	NV_ASSERT_DC( this );
	uint8 * dup_start = inStart ? (uint8*)inStart : start;
	NV_ASSERT( (dup_start>=start) && (dup_start<(start+bsize)) );
	uint memSize = i8 - dup_start;
	void * dup_copy = EngineMallocA( memSize, inBAlignement );
	libc::Memcpy( dup_copy, dup_start, memSize );
	return dup_copy;
}


uint32
dmac::Cursor::GetLeftBSize()
{
	NV_ASSERT_DC( this );
	if( !start )			return 0;
	NV_ASSERT( i8 >= start );
	if( i8 < start )		return 0;
	uint32 offset = i8 - start;
	NV_ASSERT( offset <= bsize );
	return ( offset >= bsize ? 0 : bsize-offset );
}


uint32
dmac::Cursor::GetUsedBSize()
{
	if( !start )			return 0;
	NV_ASSERT( i8 >= start );
	if( i8 < start )		return 0;
	uint32 offset = i8 - start;
	NV_ASSERT( offset <= bsize );
	return ( offset >= bsize ? bsize : offset );
}


void
dmac::Cursor::Align32()
{
	while( uint32(i8)&0x3 )
		*i8++ = 0;
}

void
dmac::Cursor::Align64()
{
	while( uint32(i8)&0x7 )
		*i8++ = 0;
}

void
dmac::Cursor::Align128()
{
	while( uint32(i8)&0xF )
		*i8++ = 0;
}

void
dmac::Cursor::Align128_32()
{
	while( (uint32(i8)&0xF) != (16-4) )
		*i8++ = 0;
}

void
dmac::Cursor::Align8QW()
{
	while( uint32(i8)&0x7F )
		*i8++ = 0;
}

void
dmac::Cursor::Align8QW_1()
{
	while( (uint32(i8)&0x7F) != (128-16) )
		*i8++ = 0;
}

void
dmac::Cursor::SetTag	(		)
{
	NV_ASSERT_DC( this );
	tag = i32;
}

void
dmac::Cursor::NullTag	(		)
{
	tag = NULL;
}

uint32*
dmac::Cursor::GetTag	(		)
{
	return (uint32*)tag;
}

uint
dmac::Cursor::GetTagQSize	(		)
{
	if( tag ) {
		uint d = uint32(i32) - uint32(tag);
		NV_ASSERT( d >= 16 );
		return (d-16)>>4;
	} else {
		return 0;
	}
}

void
dmac::Cursor::SetTagQSize	(		)
{
	if( tag ) {
		NV_ASSERT_DC( this );
		((uint16*)tag)[0] = GetTagQSize();
	}
}




volatile uint32
dmac::GetBusyMask()
{
	return 	( ((*D0_CHCR)&D_CHCR_STR_M) >> 8)
		|	( ((*D1_CHCR)&D_CHCR_STR_M) >> 7)
		|	( ((*D2_CHCR)&D_CHCR_STR_M) >> 6)
		|	( ((*D3_CHCR)&D_CHCR_STR_M) >> 5)
		|	( ((*D4_CHCR)&D_CHCR_STR_M) >> 4)
		|	( ((*D8_CHCR)&D_CHCR_STR_M) >> 0)
		|	( ((*D9_CHCR)&D_CHCR_STR_M) << 1);
}



volatile bool
dmac::IsBusy	(	uint32		inChannelsMask		)
{
	return ( GetBusyMask() & inChannelsMask ) != 0;
}


uint32
dmac::SyncTimeout	(	uint32		inChannelsMask	)
{
	NV_ASSERT( inChannelsMask );
	NV_ASSERT( (inChannelsMask&CH_ALL_M) == inChannelsMask );
	uint32 waitAndBusyMask = GetBusyMask() & inChannelsMask;

#ifdef SYNC_USING_CPCOND

	if( waitAndBusyMask == 0 )
		return 0;

	// Finished when all the DMA processes with CPCn bit are finished.
	register int32 timeout;
	register int32 tmp0, tmp1, tmp2;
	*D_PCR = waitAndBusyMask;

	asm volatile (
	"	.set noreorder\n"
	"	sync.l\n"
	"	sync.p\n"
	"	mfc0	%1, $9\n"			// start time
	"	li		%2, 294912000\n"	// 1s time-out
	"lp:\n"
	"	mfc0	%3, $9\n"
	"	nop\n"
	"	subu	%3, %3, %1\n"
	"	nop\n"
	"	slt		%3, %2, %3\n"		// time-out < cur-time ?
	"	addiu	%0, $0, 1\n"
	"	bgtz	%3, ex\n"
	"	nop\n"
	"	bc0f	lp\n"				// CPCOND[0] false ?
	"	add		%0, $0, $0\n"
	"ex:\n"
	"	.set	reorder\n"
	: "=&r"(timeout), "=&r"(tmp0), "=&r"(tmp1), "=&r"(tmp2) );

#else

	int timeout = 0;
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	for( ;; ) {
		waitAndBusyMask = GetBusyMask() & inChannelsMask;
		if( !waitAndBusyMask )
			break;
		clock::GetTime( &t1 );
		if( (t1-t0) > 1.f ) {
			Printf( "--- DMAC timeout %x ---\n", GetBusyMask() );
			timeout = 1;
			break;
		}
	}

#endif

	waitAndBusyMask = GetBusyMask() & inChannelsMask;

	// no timeout reached ...
	if( !timeout ) {
		NV_ASSERT( waitAndBusyMask == 0 );
		return waitAndBusyMask;
	}

	if( waitAndBusyMask == 0 )
	{
		NV_WARNING( "-- DMAC timeout restored --" );
		return 0;
	}
	else
	{
		// DMA stalls !!!
		NV_WARNING( "-- DMAC stalls detected --" );
		char tmp[128];
		Sprintf( tmp, "DMA stalls on channel(s) : " );
		for( uint ch = 0 ; ch <= 9 ; ch++ ) {
			if( (1<<ch) & waitAndBusyMask )
				Sprintf( tmp+Strlen(tmp), "%d ", ch );
		}
		NV_WARNING( tmp );
		return waitAndBusyMask;
	}
}


void
dmac::Sync( uint32 inChannelsMask )
{
	uint32 waitAndBusyMask = SyncTimeout( inChannelsMask );
	if( waitAndBusyMask == 0 )
		return;

	OutputRegisters( waitAndBusyMask );

	if( waitAndBusyMask & CH1_VIF1_M )
		dmac::CheckPacket_CH1( (void*)*D1_TADR, TRUE );

	NV_ERROR( "** DMA STALLS **" );
}


void
dmac::Stop	(		uint32		inChannelsMask		)
{
	SyncMemory();
	int intr = DI();
	*D_ENABLEW = 1 << 16;
	SyncMemory();
	if( inChannelsMask & CH0_VIF0_M )		*D0_CHCR = 0;
	if( inChannelsMask & CH1_VIF1_M )		*D1_CHCR = 0;
	if( inChannelsMask & CH2_GIF_M )		*D2_CHCR = 0;
	if( inChannelsMask & CH3_FROM_IPU_M )	*D3_CHCR = 0;
	if( inChannelsMask & CH4_TO_IPU_M )		*D4_CHCR = 0;
	if( inChannelsMask & CH8_FROM_SPR_M )	*D8_CHCR = 0;
	if( inChannelsMask & CH9_TO_SPR_M )		*D9_CHCR = 0;
	*D_ENABLEW = 0 << 16;
	SyncMemory();
	if( intr )	EI();
}



typedef int __attribute__ ((mode (TI))) elink_type __attribute__((aligned(16)));
extern elink_type _start;
extern elink_type _end;

bool
dmac::IsValidMemRange(	void*		inPtr,
						uint		inBSize		)
{
	// Check in cached mem range
	inPtr = CachedPointer( inPtr );

	// In elf ? (.text, .data, .bss )
	uint32 p0 = DMA_ADDR( inPtr );
	uint32 p1 = p0 + inBSize;
	if( p0 >= uint32(&_start) && p0 < uint32(&_end) && p1 <= uint32(&_end) )
		return TRUE;

	// In allocator's heaps ?
	mem::RangeStatus rs = mem::CheckRange( inPtr, inBSize );
	return ( rs==mem::RS_ALLOCATED || rs==mem::RS_NOT_AVAILABLE );
}


void
dmac::OutputRegisters	(	uint32		inChannelsMask	)
{
	//
	// -- DMAC

	Printf( "\n" );
	Printf( "-- DMAC\n" );
	Printf( "\n" );

	struct {
	    unsigned CIS: 10;
	    unsigned p0  : 3;
	    unsigned SIS : 1;   /* DAM Stall interrupt status */
	    unsigned MEIS: 1;   /* Memory FIFO empty interrupt status */
	    unsigned BEIS: 1;   /* BUSERR interrupt status */
	    unsigned CIM: 10;
	    unsigned p1  : 3;
	    unsigned SIM : 1;   /* DMA Stall interrupt mask */
	    unsigned MEIM: 1;   /* Memory FIFO empty interrupt mask */
	    unsigned p2  : 1;
	} d_STAT;

	struct {
	    unsigned CPC: 10;   /* COP control */
	    unsigned p0  : 6;
	    unsigned CDE: 10;   /* Channel DMA enable */
	    unsigned p1  : 5;
	    unsigned PCE : 1;   /* Priority control enable */
	} d_PCR;

	AS_UINT32( d_STAT )	= *D_STAT;
	AS_UINT32( d_PCR )	= *D_PCR;

	tD_CHCR		d_CHCR[3];
	tD_MADR		d_MADR[3];
	tD_QWC		d_QWC[3];
	tD_TADR		d_TADR[3];
	AS_UINT32( d_CHCR[0] )	= *D0_CHCR;
	AS_UINT32( d_CHCR[1] )	= *D1_CHCR;
	AS_UINT32( d_CHCR[2] )	= *D2_CHCR;
	AS_UINT32( d_MADR[0] )	= *D0_MADR;
	AS_UINT32( d_MADR[1] )	= *D1_MADR;
	AS_UINT32( d_MADR[2] )	= *D2_MADR;
	AS_UINT32( d_QWC[0] )	= *D0_QWC;
	AS_UINT32( d_QWC[1] )	= *D1_QWC;
	AS_UINT32( d_QWC[2] )	= *D2_QWC;
	AS_UINT32( d_TADR[0] )	= *D0_TADR;
	AS_UINT32( d_TADR[1] )	= *D1_TADR;
	AS_UINT32( d_TADR[2] )	= *D2_TADR;

	char* tagId[] = { "refe", "cnt", "next", "ref", "refs", "call", "ret", "end" };

	for( uint i = 0 ; i < 3 ; i++ ) {
		if( (1<<i) & inChannelsMask ) {
			Printf( "D%d_CHCR: DIR=%d MOD=%d ASP=%d TTE=%d STR=%d TAG.ID=%s\n",
				i,
				d_CHCR[i].DIR,
				d_CHCR[i].MOD,
				d_CHCR[i].ASP,
				d_CHCR[i].TTE,
				d_CHCR[i].STR,
				tagId[(d_CHCR[i].TAG>>12)&3]	);
			Printf( "D%d_TADR: ADDR=0x%08x SPR=%d\n",
				i,
				d_TADR[i].ADDR,
				d_TADR[i].SPR	);
			Printf( "D%d_MADR: ADDR=0x%08x SPR=%d\n",
				i,
				d_MADR[i].ADDR,
				d_MADR[i].SPR	);
			Printf( "D%d_QWC: QWC=%d (%d bytes)\n",
				i,
				d_QWC[i].QWC,
				d_QWC[i].QWC<<4	);
			Printf( "D_STAT: CIS%d=%d SIS=%d BEIS=%d CIM%d=%d\n",
				i,	(d_STAT.CIS>>i)&1,
				d_STAT.SIS,
				d_STAT.BEIS,
				i,	(d_STAT.CIM>>i)&1	);
			Printf( "D_PCR: CPC%d=%d CDE%d=%d PCE=%d\n",
				i,	(d_PCR.CPC>>i)&1,
				i,	(d_PCR.CDE>>i)&1,
				d_PCR.PCE	);
		}
	}

	for( uint i = 0 ; i < 3 ; i++ ) {
		if( (1<<i) & inChannelsMask ) {
			if( d_CHCR[i].STR )	Printf( "DMA channel %d is operating !\n", i );
			else				Printf( "DMA channel %d is stopped !\n", i );
		}
	}


	//
	// VPU1

	Printf( "\n" );
	Printf( "-- VPU1\n" );
	Printf( "\n" );

	tVIF1_STAT vif1Stat;
	AS_UINT32( vif1Stat ) = *VIF1_STAT;
	Printf( "VIF1_STAT: VPS=%d VEW=%d VGW=%d MRK=%d VSS=%d VFS=%d VIS=%d INT=%d ER0=%d ER1=%d FQC=%d\n",
		vif1Stat.VPS,
		vif1Stat.VEW,
		vif1Stat.VGW,
		vif1Stat.MRK,
		vif1Stat.VSS,
		vif1Stat.VFS,
		vif1Stat.VIS,
		vif1Stat.INT,
		vif1Stat.ERO,
		vif1Stat.ER1,
		vif1Stat.FQC	);

	Printf( "VIF1_ITOP: %d (0x%x)\n", DGET_VIF1_ITOP(), DGET_VIF1_ITOP() );
	Printf( "VIF1_ITOPS: %d (0x%x)\n", DGET_VIF1_ITOPS(), DGET_VIF1_ITOPS() );
	Printf( "VIF1_TOP: %d (0x%x)\n", DGET_VIF1_TOP(), DGET_VIF1_TOP() );
	Printf( "VIF1_TOPS: %d (0x%x)\n", DGET_VIF1_TOPS(), DGET_VIF1_TOPS() );
	Printf( "VIF1_BASE: %d (0x%x)\n", DGET_VIF1_BASE(), DGET_VIF1_BASE() );
	Printf( "VIF1_OFST: %d (0x%x)\n", DGET_VIF1_OFST(), DGET_VIF1_OFST() );
	Printf( "VIF1_MARK: %d (0x%x)\n", DGET_VIF1_MARK(), DGET_VIF1_MARK() );

	if( vif1Stat.ERO )
		Printf( "** DMAtag Mismatch error (DMAtag has been detected in the VIF packet) !\n" );
	if( vif1Stat.ER1 )
		Printf( "** VIFcode Error (Undefined VIFcode has been detected) !\n" );

	uint32 vpu_stat = vu::GetVPU_STAT();
	Printf( "VPU_STAT: VBS0=%d ... VBS1=%d VDS1=%d VTS1=%d VFS1=%d VGW1=%d\n",
		(vpu_stat>> 0)&1,
		(vpu_stat>> 8)&1,
		(vpu_stat>> 9)&1,
		(vpu_stat>>10)&1,
		(vpu_stat>>11)&1,
		(vpu_stat>>12)&1	);

	Printf( "VIF1 is %s\n", vu::IsVIFBusy(1) ? "busy" : "idle" );
	Printf( "VPU1 is %s\n", vu::IsVPUBusy(1) ? "busy" : "idle" );
	if( vpu_stat&(1<<12) )
		Printf( "VPU1 is syncing on XGKICK !\n" );


	//
	// GIF

	Printf( "\n" );
	Printf( "-- GIF\n" );
	Printf( "\n" );

	tGIF_STAT gifStat;
	AS_UINT32( gifStat ) = *GIF_STAT;
	Printf( "GIF_STAT: M3R=%d M3P=%d IMT=%d PSE=%d IP3=%d P3Q=%d P2Q=%d P1Q=%d OPH=%d APATH=%d FQC=%d\n",
		gifStat.M3R,
		gifStat.M3P,
		gifStat.IMT,
		gifStat.PSE,
		gifStat.IP3,
		gifStat.P3Q,
		gifStat.P2Q,
		gifStat.P1Q,
		gifStat.OPH,
		gifStat.APATH,
		gifStat.FQC		);

/*
	struct {
	    unsigned long NLOOP:15;
	    unsigned long EOP:1;
	    unsigned long pad16:16;
	    unsigned long id:14;
	    unsigned long PRE:1;
	    unsigned long PRIM:11;
	    unsigned long FLG:2;
	    unsigned long NREG:4;
	    unsigned long REGS0_7:32;
	    unsigned long REGS8_15:32;
	} gif_TAG;

	gs::Pause();
	((uint32*)&gif_TAG)[0] = *GIF_TAG0;
	((uint32*)&gif_TAG)[1] = *GIF_TAG1;
	((uint32*)&gif_TAG)[2] = *GIF_TAG2;
	((uint32*)&gif_TAG)[3] = *GIF_TAG3;
	gs::Resume();

	Printf( "GIF-TAG0123: NLOOP=%d EOP=%d NREG=%d REGS=0x%08x%08x\n",
			gif_TAG.NLOOP,
			gif_TAG.EOP,
			gif_TAG.NREG,
			gif_TAG.REGS8_15,
			gif_TAG.REGS0_7	);
*/
	if( gifStat.PSE )
		Printf( "GIF transfer is paused !\n" );

	bool gif_idle = TRUE;
	for( uint i = 0 ; i < 16 ; i++ ) {
		if( i>0 )
			AS_UINT32( gifStat ) = *GIF_STAT;
		Printf( "[%03d] %s output-path:%d FIFO:%d PATH3:%s %s%s%s%s\n",
			i,
			gifStat.OPH ? "busy, " : "idle, ",
			gifStat.APATH,
			gifStat.FQC,
			(gifStat.M3R||gifStat.M3P) ? "disabled" : "enabled",
			(gifStat.P1Q || gifStat.P2Q || gifStat.P3Q ) ? "PATH-in-queue:" : "",
			gifStat.P1Q ? "1" : "",
			gifStat.P2Q ? "2" : "",
			gifStat.P3Q ? "3" : ""	);
		if( gifStat.OPH || gifStat.APATH )
			gif_idle = FALSE;
	}
	if( gif_idle && gifStat.FQC ) {
		Printf( "\n" );
		Printf( "** GIF seems stalled with not empty FIFO => GS/VU1 is stalling ?\n" );
	}


	//
	// VPU1

	Printf( "\n" );
	Printf( "-- VPU1 (** force-breaked **)\n" );
	Printf( "\n" );

	vu::Break_PATH12();

	vpu_stat = vu::GetVPU_STAT();
	Printf( "VPU_STAT: VBS0=%d ... VBS1=%d VDS1=%d VTS1=%d VFS1=%d VGW1=%d\n",
		(vpu_stat>> 0)&1,
		(vpu_stat>> 8)&1,
		(vpu_stat>> 9)&1,
		(vpu_stat>>10)&1,
		(vpu_stat>>11)&1,
		(vpu_stat>>12)&1	);

	uint16 vu1_tpc = vu::GetVPU_TPC( 1 );
	Printf( "VU1_TPC: 0x%04x\n", vu1_tpc*8 );

/*	Printf( "\n" );
	for( uint i = 0 ; i < 16 ; i++ )
		Printf( "VU1_VI%02d: 0x%04x\n", i, vu::GetVU1_IR(i) );

	Printf( "\n" );
	for( uint i = 0 ; i < 32 ; i++ ) {
		Vec4 v4 = vu::GetVU1_FR( i );
		Printf( "VU1_VF%02d: %f, %f, %f, %f\n", i, v4.x, v4.y, v4.z, v4.w );
	}
*/
//	gs::Pause();
	Printf( "\n" );
	Printf( "VU1 micromem :\n" );
	for( uint i = 0 ; i < 2048 ; i++ ) {
		uint32 lower = ((uint32*)VU1_MICRO)[i*2+0];
		uint32 upper = ((uint32*)VU1_MICRO)[i*2+1];
		if( lower == 0xcdcdcdcd )
			break;
		Printf( "[%04x] %08X %08X\n", i*8, InvertByteOrder(lower), InvertByteOrder(upper) );
	}

	Printf( "\n" );
	Printf( "VU1 datamem :\n" );
	for( uint i = 0 ; i < 1024 ; i++ ) {
		float* memP = ((float*)VU1_MEM)+(i*4);
		Printf( "[%04x] %08X %08X %08X %08X    {%f %f %f %f}\n",
			i*16,
			AS_UINT32( memP[0] ),
			AS_UINT32( memP[1] ),
			AS_UINT32( memP[2] ),
			AS_UINT32( memP[3] ),
			memP[0],
			memP[1],
			memP[2],
			memP[3]		);
	}
//	gs::Resume();

	Printf( "\n" );
	for( uint i = 0 ; i < 16 ; i++ ) {

		if( i > 0 ) {
			vu1_tpc  = vu::StepVU1();
			vpu_stat = vu::GetVPU_STAT();
			AS_UINT32( gifStat ) = *GIF_STAT;
		}

	//	gs::Pause();
		uint64 vu1_opcode = ((uint64*)VU1_MICRO)[vu1_tpc];
		char ir[256], *irP = ir;
		for( uint j = 0 ; j < 16 ; j++ ) {
			Sprintf( irP, "%d:0x%x ", j, vu::GetVU1_IR(j) );
			irP += Strlen( irP );
		}
	//	gs::Resume();

		Printf( "[%03d]\n", i );

		Printf( "    {VU1 tpc:0x%04x lower:%08X upper:%08X %s%s%s%s%s}\n",
			vu1_tpc*8,
			InvertByteOrder( uint32(vu1_opcode&0xFFFFFFFF) ),
			InvertByteOrder( uint32(vu1_opcode>>32) ),
			vpu_stat&(1<<8) ? "[running]" : (vpu_stat&(1<<11)?"":"[E]"),		// non force-break => [E] if idle
			vpu_stat&(1<<9) ? "[D]" : "",
			vpu_stat&(1<<10)? "[T]" : "",
			vpu_stat&(1<<11)? "[FBRK]" : "",
			vpu_stat&(1<<12)? "[XGKICK]" : ""	);

		Printf( "    {VU1_VI %s}\n", ir	);

		Printf( "    {GIF output:%s output-path:%d FIFO:%d PATH3:%s PATH-in-queue:%s%s%s}\n",
			gifStat.OPH ? "busy" : "idle",
			gifStat.APATH,
			gifStat.FQC,
			(gifStat.M3R||gifStat.M3P) ? "disabled" : "enabled",
			gifStat.P1Q ? "1" : "",
			gifStat.P2Q ? "2" : "",
			gifStat.P3Q ? "3" : ""	);
	}

	Printf( "\n" );
	Printf( "\n" );
}





namespace
{

	struct ChainContext {

		typedef struct {
		    unsigned QWC	:16;
		    unsigned p0  	:10;
		    unsigned PCE  	:2;
		    unsigned ID  	:3;
		    unsigned IRQ  	:1;
		    unsigned ADDR 	:31;
		    unsigned SPR 	:1;
		} DMATag;

		enum {
			TAG_REFE	= 0,
			TAG_CNT		= 1,
			TAG_NEXT	= 2,
			TAG_REF		= 3,
			TAG_REFS	= 4,
			TAG_CALL	= 5,
			TAG_RET		= 6,
			TAG_END		= 7
		};

		typedef struct {
		    unsigned immediate :16;
		    unsigned num       : 8;
		    unsigned CMD       : 7;
		    unsigned i         : 1;
		} myVIF_CODE;

		typedef struct {
		    unsigned long NLOOP:15;
		    unsigned long EOP:1;
		    unsigned long pad16:16;
		    unsigned long id:14;
		    unsigned long PRE:1;
		    unsigned long PRIM:11;
		    unsigned long FLG:2;
		    unsigned long NREG:4;
		    unsigned long REGS0_7:32;
		    unsigned long REGS8_15:32;
		} myGIF_TAG;


		bool			verbose;
		uint32			trxBSize;
		vector<void*>	asr;
		uint			vifDirectBSize;			// VIF-DIRECT ?
		bool			vifDirectGIFTAGDone;	// VIF-DIRECT GIFTag done ?
		myGIF_TAG		vifDirectGIFTAG;		// VIF-DIRECT GIFTag
		uint			vifDataBSize;			// VIF-MPG,UNPACK,STMASK,STROW,STCOL


		void	Init(	bool	inVerbose = FALSE )
		{
			verbose		   = inVerbose;
			trxBSize	   = 0;
			asr.clear();
			vifDirectBSize = 0;
			vifDataBSize   = 0;
		}


		void	SetVerbose	(	bool	inVerbose	)
		{
			verbose = inVerbose;
		}


		bool	IsTagRef( uint inId )
		{
			return ( inId==TAG_REFE || inId==TAG_REFS || inId==TAG_REF );
		}


		char*	GetTagStringID	(	uint	inID	)
		{
			if( inID == TAG_REFE )		return "REFE";
			if( inID == TAG_CNT  )		return "CNT";
			if( inID == TAG_NEXT )		return "NEXT";
			if( inID == TAG_REF  )		return "REF";
			if( inID == TAG_REFS )		return "REFS";
			if( inID == TAG_CALL )		return "CALL";
			if( inID == TAG_RET  )		return "RET";
			if( inID == TAG_END  )		return "END";
			NV_ERROR( "Invalid TAG ID !" );
			return "??";
		}


		char*	GetVIFStringCMD	(	uint	inCMD	)
		{
			if( inCMD == 0 )			return "NOP";
			if( inCMD == 1 )			return "STCYCL";
			if( inCMD == 2 )			return "OFFSET";
			if( inCMD == 3 )			return "BASE";
			if( inCMD == 4 )			return "ITOP";
			if( inCMD == 5 )			return "STMOD";
			if( inCMD == 6 )			return "MSKPATH3";
			if( inCMD == 7 )			return "MARK";
			if( inCMD == 16 )			return "FLUSHE";
			if( inCMD == 17 )			return "FLUSH";
			if( inCMD == 19 )			return "FLUSHA";
			if( inCMD == 20 )			return "MSCAL";
			if( inCMD == 23 )			return "MSCNT";
			if( inCMD == 21 )			return "MSCALF";
			if( inCMD == 32 )			return "STMASK";
			if( inCMD == 48 )			return "STROW";
			if( inCMD == 49 )			return "STCOL";
			if( inCMD == 74 )			return "MPG";
			if( inCMD == 80 )			return "DIRECT";
			if( inCMD == 81 )			return "DIRECTHL";
			if( (inCMD&0x60)==0x60 ) {
				uint vl = inCMD & 3;
				uint vn = (inCMD>>2) & 3;
				static char unpackS[ 16 ];
				if( vn == 0 )
					Sprintf( unpackS, "UNPACK-S%d", Max(5,(32>>vl)) );
				else
					Sprintf( unpackS, "UNPACK-V%d-%d", vn+1, Max(5,(32>>vl)) );
				return unpackS;
			}
			NV_ERROR( "Invalid VIF CMD !" );
			return "??";
		}


		char*	GetGifTagStringID	(	uint	inFLG	)
		{
			if( inFLG == 0 )	return "packed";
			if( inFLG == 1 )	return "reglist";
			if( inFLG == 2 )	return "image";
			NV_ERROR( "Invalid GIFTAG FLG !" );
			return "??";
		}


		char*	GetGsAddrString		(	uint	inAddr	)
		{
			if( inAddr == 0x42 )	return "ALPHA_1";
			if( inAddr == 0x43 )	return "ALPHA_2";
			if( inAddr == 0x50 )	return "BITBLTBUF";
			if( inAddr == 0x8 )		return "CLAMP_1";
			if( inAddr == 0x9 )		return "CLAMP_2";
			if( inAddr == 0x46 )	return "COLCLAMP";
			if( inAddr == 0x44 )	return "DIMX";
			if( inAddr == 0x45 )	return "DTHE";
			if( inAddr == 0x4a )	return "FBA_1";
			if( inAddr == 0x4b )	return "FBA_2";
			if( inAddr == 0x61 )	return "FINISH";
			if( inAddr == 0xa )		return "FOG";
			if( inAddr == 0x3d )	return "FOGCOL";
			if( inAddr == 0x4c )	return "FRAME_1";
			if( inAddr == 0x4d )	return "FRAME_2";
			if( inAddr == 0x54 )	return "HWREG";
			if( inAddr == 0x62 )	return "LABEL";
			if( inAddr == 0x34 )	return "MIPTBP1_1";
			if( inAddr == 0x35 )	return "MIPTBP1_2";
			if( inAddr == 0x36 )	return "MIPTBP2_1";
			if( inAddr == 0x37 )	return "MIPTBP2_2";
			if( inAddr == 0x49 )	return "PABE";
			if( inAddr == 0x0 )		return "PRIM";
			if( inAddr == 0x1b )	return "PRMODE";
			if( inAddr == 0x1a )	return "PRMODECONT";
			if( inAddr == 0x1 )		return "RGBAQ";
			if( inAddr == 0x22 )	return "SCANMSK";
			if( inAddr == 0x40 )	return "SCISSOR_1";
			if( inAddr == 0x41 )	return "SCISSOR_2";
			if( inAddr == 0x60 )	return "SIGNAL";
			if( inAddr == 0x2 )		return "ST";
			if( inAddr == 0x47 )	return "TEST_1";
			if( inAddr == 0x48 )	return "TEST_2";
			if( inAddr == 0x6 )		return "TEX0_1";
			if( inAddr == 0x7 )		return "TEX0_2";
			if( inAddr == 0x14 )	return "TEX1_1";
			if( inAddr == 0x15 )	return "TEX1_2";
			if( inAddr == 0x16 )	return "TEX2_1";
			if( inAddr == 0x17 )	return "TEX2_2";
			if( inAddr == 0x3b )	return "TEXA";
			if( inAddr == 0x1c )	return "TEXCLUT";
			if( inAddr == 0x3f )	return "TEXFLUSH";
			if( inAddr == 0x53 )	return "TEXDIR";
			if( inAddr == 0x51 )	return "TRXPOS";
			if( inAddr == 0x52 )	return "TRXREG";
			if( inAddr == 0x3 )		return "UV";
			if( inAddr == 0x18 )	return "XYOFFSET_1";
			if( inAddr == 0x19 )	return "XYOFFSET_2";
			if( inAddr == 0x5 )		return "XYZ2";
			if( inAddr == 0xd )		return "XYZ3";
			if( inAddr == 0x4 )		return "XYZF2";
			if( inAddr == 0xc )		return "XYZF3";
			if( inAddr == 0x4e )	return "ZBUF_1";
			if( inAddr == 0x4f )	return "ZBUF_2";
			NV_ERROR( "Invalid GS Addr !" );
			return "??";
		}


		void* 	GetNextTag	(	void*	inTADR	)
		{
			inTADR = (void*)DMA_ADDR( inTADR );
			NV_COMPILE_TIME_ASSERT( sizeof(DMATag) == 8 );
			if( !inTADR )	return NULL;
			uint32  tadr = uint32( inTADR );
			DMATag* tag  = (DMATag*) inTADR;
			switch( tag->ID ) {
				case TAG_REFE :
				case TAG_END :
					return NULL;
				case TAG_CNT :
					return (void*)( tadr + ((1+tag->QWC)<<4) );
				case TAG_NEXT :
					return (void*)( tag->ADDR );
				case TAG_REF :
				case TAG_REFS :
					return (void*)( tadr + 16 );
				case TAG_CALL :
					asr.push_back( (void*)( tadr + ((1+tag->QWC)<<4) ) );
					return (void*)( tag->ADDR );
				case TAG_RET :
					if( asr.size() ) {
						void* last = asr.back();
						asr.pop_back();
						return last;
					} else {
						return NULL;
					}
				default :
					NV_ERROR( "Invalid TAG ID !" );
					return NULL;
			}
		}


		bool
		CheckGIF_CH1		(	void*	&		ioPtr,
								uint	&		ioBSize	)
		{
			NV_ASSERT( vifDirectBSize );
			NV_ASSERT( vifDirectBSize >= 16 );
			ioPtr = (void*)DMA_ADDR( ioPtr );
			if( vifDirectBSize < 16 )
				return FALSE;

			tD_MADR d1_MADR;
			AS_UINT32( d1_MADR ) = *D1_MADR;

			if( !vifDirectGIFTAGDone ) {
				Memcpy( &vifDirectGIFTAG, ioPtr, 16 );
				if( verbose ) {
					Printf( "[0x%08x:0x%08x] %s++++ GIFTAG-%s [NLOOP=%d EOP=%d NREG=%d REGS=0x%08x%08x]\n",
							trxBSize,
							uint32(ioPtr),
							(uint32(ioPtr) == d1_MADR.ADDR) ? "(*)" : "",
							GetGifTagStringID( vifDirectGIFTAG.FLG ),
							vifDirectGIFTAG.NLOOP,
							vifDirectGIFTAG.EOP,
							vifDirectGIFTAG.NREG,
							vifDirectGIFTAG.REGS8_15,
							vifDirectGIFTAG.REGS0_7	);
				}
				vifDirectGIFTAGDone = TRUE;
				AS_UINT32( ioPtr ) += 16;
				vifDirectBSize	   -= 16;
				trxBSize		   += 16;
				ioBSize			   -= 16;
				return TRUE;
			}

			// PACKED
			if( vifDirectGIFTAG.FLG == 0 )
			{
//				NV_ASSERT( vifDirectGIFTAG.NREG == 1 );
//				NV_ASSERT( (vifDirectGIFTAG.REGS0_7&15) == 0xe );	// A+D
				uint64 ad_data = ((uint64*)ioPtr)[0];
				uint64 ad_addr = ((uint64*)ioPtr)[1];
				if( verbose ) {
					char args[128];
					// FRAME
					if( ad_addr==0x4c || ad_addr==0x4d ) {
						sceGsFrame reg;
						AS_UINT64( reg ) = ad_data;
						Sprintf( args, "FBP=%d FBW=%d PSM=%d FBMSK=0x%08x",
								reg.FBP,
								reg.FBW,
								reg.PSM,
								reg.FBMSK	);
					}
					// ZBUF
					else if( ad_addr==0x4e || ad_addr==0x4f ) {
						sceGsZbuf reg;
						AS_UINT64( reg ) = ad_data;
						Sprintf( args, "ZBP=%d PSM=%d ZMSK=%d",
								reg.ZBP,
								reg.PSM,
								reg.ZMSK	);
					}
					// TEST
					else if( ad_addr==0x47 || ad_addr==0x48 ) {
						sceGsTest reg;
						AS_UINT64( reg ) = ad_data;
						Sprintf( args, "ATE=%d ATST=%d AREF=%d AFAIL=%d DATE=%d DATM=%d ZTE=%d ZTST=%d",
								reg.ATE,
								reg.ATST,
								reg.AREF,
								reg.AFAIL,
								reg.DATE,
								reg.DATM,
								reg.ZTE,
								reg.ZTST	);
					} else {
						args[0] = 0;
					}
					Printf( "[0x%08x:0x%08x] %s++++ %s [%s]\n",
							trxBSize,
							uint32(ioPtr),
							(uint32(ioPtr) == d1_MADR.ADDR) ? "(*)" : "",
							GetGsAddrString( ad_addr ),
							args	);
				}
				AS_UINT32( ioPtr ) += 16;
				ioBSize			   -= 16;
				vifDirectBSize     -= 16;
				trxBSize		   += 16;
			}
			else {
				uint trx = Min( ioBSize, vifDirectBSize );
				AS_UINT32( ioPtr ) += trx;
				ioBSize			   -= trx;
				vifDirectBSize     -= trx;
				trxBSize		   += trx;
			}

			return TRUE;
		}


		bool
		CheckVIF_CH1		(	void*			inPtr,
								uint			inBSize	)
		{
			inPtr = (void*)DMA_ADDR( inPtr );

			tD_MADR d1_MADR;
			AS_UINT32( d1_MADR ) = *D1_MADR;

			while( inBSize )
			{
				if( inBSize < 4 ) {
					if( verbose )
						Printf( "Error: VIF data alignment !\n" );
					return FALSE;
				}

				// Direct is operating ?
				if( vifDirectBSize ) {
					bool res = CheckGIF_CH1( inPtr, inBSize	);
					if( !res )
						return FALSE;
				}

				// Data trx is operating ?
				else if( vifDataBSize ) {
					uint bsize = Min( inBSize, vifDataBSize );
					if( verbose ) {
						uint32 d0 = uint32(inPtr);
						uint32 d1 = d0 + bsize;
						Printf( "[0x%08x:0x%08x] %s.. (VIF-DATA %d bytes)\n",
								trxBSize,
								d0,
								(d1_MADR.ADDR >= d0 && d1_MADR.ADDR < d1) ? "(*)" : "",
								bsize	);
					}
					AS_UINT32( inPtr ) += bsize;
					inBSize			   -= bsize;
					vifDataBSize	   -= bsize;
					trxBSize		   += bsize;
				}

				// Code
				else {
					myVIF_CODE vifCode;
					Memcpy( &vifCode, inPtr, 4 );

					if( verbose ) {
						Printf( "[0x%08x:0x%08x] %s-- %s [NUM:%d IMM:0x%x]\n",
								trxBSize,
								uint32(inPtr),
								(uint32(inPtr) == d1_MADR.ADDR) ? "(*)" : "",
								GetVIFStringCMD( vifCode.CMD ),
								vifCode.num,
								vifCode.immediate	);
					}

					AS_UINT32( inPtr ) += 4;
					inBSize			   -= 4;
					trxBSize		   += 4;

					// DIRECT[HL] ?
					if( vifCode.CMD==80 || vifCode.CMD==81 ) {
						// check 128 bits aligned
						if( uint32(inPtr)&15 ) {
							if( verbose )
								Printf( "Error: VIF DIRECT[HL] data alignment !\n" );
							return FALSE;
						} 
						vifDirectBSize = (vifCode.immediate?vifCode.immediate:65536) * 16;
						vifDirectGIFTAGDone = FALSE;
					}
					// MPG ?
					if( vifCode.CMD==74 ) {
						// check 64 bits aligned
						if( uint32(inPtr)&7 ) {
							if( verbose )
								Printf( "Error: VIF MPG data alignment !\n" );
							return FALSE;
						} 
						vifDataBSize = (vifCode.num?vifCode.num:256) * 8;
					}
					// STMASK ?
					else if( vifCode.CMD==32 ) {
						vifDataBSize = 4;
					}
					// STCOL/STROW ?
					else if( vifCode.CMD==48 || vifCode.CMD==49 ) {
						vifDataBSize = 16;
					}
					// UNPACK ?
					else if( (vifCode.CMD&0x60)==0x60 ) {
						// V4-5 not supported !
						uint vl = vifCode.CMD & 3;
						uint vn = (vifCode.CMD>>2) & 3;
						uint unitBSize = (4>>vl) * (vn+1);
						vifDataBSize = Round4( vifCode.num * unitBSize );
					}
				}
			}

			return TRUE;
		}


		bool
		CheckData_CH1		(	void *			inPtr,
								uint			inBSize	)
		{
			inPtr = (void*)DMA_ADDR( inPtr );
			if( !inPtr )
				return FALSE;
			if( inBSize == 0 )
				return TRUE;


/*			if( verbose ) {
				uint32 d0 = uint32(inPtr);
				uint32 d1 = d0 + inBSize;

				tD_MADR d1_MADR;
				AS_UINT32( d1_MADR ) = *D1_MADR;
				Printf( "[0x%08x:0x%08x] %sDATA [ADDR:0x%08x BSIZE:%d]\n",
						trxBSize,
						uint32(inPtr),
						(d1_MADR.ADDR >= d0 && d1_MADR.ADDR < d1) ? "(*) " : "",
						inBSize		);
			}
*/
			// Check data memory validity
			if( !dmac::IsValidMemRange(inPtr,inBSize) ) {
				if( verbose )
					Printf( "Error: Data is not a valid memory area !\n" );
				return FALSE;
			}

			return CheckVIF_CH1( inPtr, inBSize );
		}


		bool
		CheckPacket_CH1	(	void *			inTADR	)
		{
			inTADR = (void*)DMA_ADDR( inTADR );
			if( !inTADR )
				return FALSE;
			NV_COMPILE_TIME_ASSERT( sizeof(DMATag) == 8 );
			DMATag* tag = (DMATag*) inTADR;

			if( verbose ) {
				tD_TADR d1_TADR;
				AS_UINT32( d1_TADR ) = *D1_TADR;
				Printf( "[0x%08x:0x%08x] %sDMATAG-%s  [QWC:%d ID:%d ADDR:0x%08x SPR:%d]\n",
						trxBSize,
						uint32(inTADR),
						(d1_TADR.ADDR == uint32(tag)) ? "(*) " : "",
						GetTagStringID(tag->ID),
						tag->QWC,
						tag->ID,
						tag->ADDR,
						tag->SPR	);
			}

			// Check tag alignment
			if( uint32(inTADR)&15 ) {
				if( verbose )
					Printf( "Error: Not aligned dma TAG !\n" );
				return FALSE;
			}

			// Check tag memory validity
			if( !dmac::IsValidMemRange(inTADR,16) ) {
				if( verbose )
					Printf( "Error: TAG itself (16 bytes) is not a valid memory area !\n" );
				return FALSE;
			}

			// TRX tag
			trxBSize += 8;

			// Parse TAG data (TTE=1)
			bool res;
			if( IsTagRef(tag->ID) )
			{
				res = CheckData_CH1( (tag+1), 8 );						// Data in tag
				if( res )
					CheckData_CH1( (void*)tag->ADDR, (tag->QWC)<<4 );	// Referenced data
			}
			else
			{
				res = CheckData_CH1( (tag+1), 8+((tag->QWC)<<4) );		// Data following the tag
			}

			return res;
		}


		bool	CheckChain_CH1	(	void*		inFromTADR,
									void*		inToTADR		)
		{
			NV_COMPILE_TIME_ASSERT( sizeof(DMATag) == 8 );
			inFromTADR = (void*)DMA_ADDR( inFromTADR );
			inToTADR   = (void*)DMA_ADDR( inToTADR );

			if( verbose ) {
				Printf( "\n" );
				if( inToTADR )
					Printf( "DMAC CH1: from TADR=0x%08x to TADR=0x%08x", uint32(inFromTADR), uint32(inToTADR) );
				else
					Printf( "DMAC CH1: from TADR=0x%08x\n", uint32(inFromTADR) );
				Printf( "\n" );
			}

			void* tadr = inFromTADR;
			while( tadr ) {
				bool res = CheckPacket_CH1( tadr );
				if( !res )
					return FALSE;
				if( tadr == inToTADR )
					break;
				tadr = GetNextTag( tadr );
				if( verbose && tadr )
					Printf( "\n" );
			}

			return TRUE;
		}
	};

}



bool
dmac::CheckData_CH1		(	void*		inPtr,
							uint		inBSize,
							bool		inVerbose		)
{
	ChainContext ctxt;
	ctxt.Init( inVerbose );
	return ctxt.CheckData_CH1( inPtr, inBSize );
}


bool
dmac::CheckPacket_CH1	(	void*		inTADR,
							bool		inVerbose		)
{
	ChainContext ctxt;
	ctxt.Init( inVerbose );
	return ctxt.CheckPacket_CH1( inTADR );
}


bool
dmac::CheckChain_CH1	(	void*		inFromTADR,
							void*		inToTADR,
							bool		inVerbose		)
{
	ChainContext ctxt;
	ctxt.Init( inVerbose );
	return ctxt.CheckChain_CH1( inFromTADR, inToTADR );
}



