/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <libvifpk.h>
using namespace nv;



namespace
{

	//
	// Gaussian convolution based on mipmaps linear interpolation

	struct MipmapGC {
		float	weight;		// in [0,1]
		int		blend;		// in [0,128]
		bool	quality;	// TRUE if best quality is required !
	};

	int Compute_MipmapedGaussConvol	(	MipmapGC*	outMipGC/*[16]*/,
										int			inSizeX,
										int			inSizeY,
										float		inRadius,
										float		inQuality		)
	{
		//
		// Computes Gaussian normalized sequence

		float gWeights[128];
		float gSigma = GaussianDeviation( inRadius );
		int   gWidth = GaussianWidth( gSigma );
		if( gWidth < 2 || gWidth > 128 )
			return 0;
		GaussianNormSequence( gWeights, gSigma, TRUE );

		//
		// N mipmaps, from (inSizeX,inSizeY) -> (8,8)

		int mip_cpt = 0;
		int minSize = Min( inSizeX, inSizeY );
		while( (minSize>>(mip_cpt+1)) >= 8 )
			mip_cpt++;
		if( mip_cpt == 0 || mip_cpt >= 16 )
			return 0;

		//
		// Computes mipmaps weight

		float sum_mipw[16];
			  sum_mipw[ mip_cpt+1 ] = 0.0f;

		float cur_avg = 0.0f;
		for( int mip = mip_cpt ; mip >= 0 ; mip-- ) {
			MipmapGC* gc = & outMipGC[mip];

			// average mipmap weights
			int   w1  = Min( 1 << mip,    gWidth );
			int   w0  = Min( (1<<mip)>>1, gWidth );
			float s   = 0.0f;
			for( int wi = w0 ; wi < w1 ; wi++ )
				s += gWeights[wi];
			float avg  = (w1>w0) ? s / float( w1-w0 ) : 0.0f;

			// mipmap factor to reach the average from the current average
			float w			= 1.0f / float( 1<<mip );
			float mipw		= (avg - cur_avg) / w;
			NV_ASSERT( mipw >= 0.f );
			NV_ASSERT( mipw <= 1.f );
			mipw			= Clamp( mipw, 0.f, 1.f );
			cur_avg			= avg;
			sum_mipw[mip]	= sum_mipw[ mip+1 ] + mipw;
			gc->weight      = mipw;
			// Smooth mipmap for better quality, based on mipmap ratio 1/2^N
			gc->quality    = ( inQuality >= w );

		//	Printf( "mip%d [%d,%d[ avg=%f  w=%f quality=%c\n", mip, w0, w1, avg, mipw, gc->quality?'Y':'N' );
		}

		//		
		// Computes recursive blending alpha factors (a,b,c,...z),
		// such that :
		// mip(i-1) = mip(i  ) * a + mip(i-1) * (1-a)
		// mip(i-2) = mip(i-1) * b + mip(i-2) * (1-b)
		// ...
		// mip(0)   = mip(1)   * z + mip(0)   * (1-z)
		// fromBAddr += mip(0)
		//
		// a = wa      / (wa+wb)
		// b = (wa+wb) / (wa+wb+wc)
		// ...

		outMipGC[0].blend = int( sum_mipw[0] * 128.0f );	// ~1.0
		for( int mip = mip_cpt ; mip > 0 ; mip-- ) {
			float sw0   = sum_mipw[ mip   ];
			float sw1   = sum_mipw[ mip-1 ];
			float blend = ( sw0 && sw1 ) ? sw0 / sw1 : 0.0f;
			int alpha   = int( blend * 128.0f );
			if( alpha == 0 )	mip_cpt = mip - 1;
			else				outMipGC[mip].blend = alpha;
		//	Printf( "mip%d mip_blend=%d\n", mip, mip_blend[mip] );
		}
		//	Printf( "mip%d mip_blend=%d\n", 0, mip_blend[0] );
		NV_ASSERT( mip_cpt > 0 );

		return mip_cpt;
	}



	void	Grab_CH1(	uint128 *	base_addr,
						uint16		start_baddr,
						uint16		pixel_mode,
						uint16		buff_width,
						uint16		x,
						uint16		y,
						uint16		width,
						uint16		height )
	{
		int texture_qwc;
		sceVif1Packet vif1_pkt;	
		u_long128 settup_base[10];
		static	u_int enable_path3[4] __attribute__((aligned(16))) =
		{
			0x06000000,
			0x00000000,
			0x00000000,
			0x00000000,
		};

		// get quad word count for image
		u_int psmlen =0;
		if( pixel_mode == SCE_GS_PSMCT32 )				psmlen = 32;
	 	else if( pixel_mode == SCE_GS_PSMCT24 )			psmlen = 24;
	 	else if( pixel_mode == SCE_GS_PSMCT16 )			psmlen = 16;
	 	else if( pixel_mode == SCE_GS_PSMT8 )			psmlen = 8;
	 	else if( pixel_mode == SCE_GS_PSMT4 )			psmlen = 4;
	 	else if( pixel_mode == SCE_GS_PSMZ32 )			psmlen = 32;
	 	else if( pixel_mode == SCE_GS_PSMZ24 )			psmlen = 24;
	 	else if( pixel_mode == SCE_GS_PSMZ16 )			psmlen = 16;
		NV_ASSERT( psmlen > 0 );

		texture_qwc = (width*height*psmlen) >> 7;
		buff_width >>= 6;
		if( buff_width == 0 )
		    buff_width = 1;

		// set base address of GIF packet
		sceVif1PkInit(&vif1_pkt, &settup_base[0] );
		sceVif1PkReset(&vif1_pkt);

		// will start transfer with VIF code and GS data will follow
		sceVif1PkAddCode(&vif1_pkt, SCE_VIF1_SET_NOP(0)); 
		// disable PATH 3 transfer
		sceVif1PkAddCode(&vif1_pkt, SCE_VIF1_SET_MSKPATH3(0x8000, 0)); 
		// wait for all 3 PATHS to GS to be complete
		sceVif1PkAddCode(&vif1_pkt, SCE_VIF1_SET_FLUSHA(0)); 
		// transfer 6 QW's to GS
		sceVif1PkAddCode(&vif1_pkt, SCE_VIF1_SET_DIRECT(6, 0)); 

		// GIF tag for texture settings		
		sceVif1PkAddGsData(&vif1_pkt, SCE_GIF_SET_TAG(5,1,NULL,NULL,SCE_GIF_PACKED,1) );
		sceVif1PkAddGsData(&vif1_pkt, 0xEL );

		// set transmission between buffers
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_SET_BITBLTBUF( start_baddr, buff_width, pixel_mode, // SRC
																	NULL, NULL, NULL ) );// DEST
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_BITBLTBUF );

		// set transmission area between buffers	( source x,y  dest x,y  and direction )
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_SET_TRXPOS(x, y, 0, 0, 0) );
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_TRXPOS );

		// set size of transmission area 
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_SET_TRXREG(width, height) );
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_TRXREG );

		// set FINISH event occurrence request
		sceVif1PkAddGsData(&vif1_pkt, (u_long)(0x0) );
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_FINISH ); 	

		// set transmission direction  ( LOCAL -> HOST Transmission )
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_SET_TRXDIR(1) );
		sceVif1PkAddGsData(&vif1_pkt, SCE_GS_TRXDIR ); 	

		// get packet size in quad words	
		sceVif1PkTerminate(&vif1_pkt);

		// check if DMA is complete (STR=0)
		while( (DGET_D1_CHCR() & D_CHCR_STR_M) ) {};

		// set the FINISH event
		DPUT_GS_CSR( GS_CSR_FINISH_M );

		// DMA from memory and start DMA transfer
		FlushCache(WRITEBACK_DCACHE);
		DPUT_D1_QWC( 0x7 );
		DPUT_D1_MADR( (u_int)vif1_pkt.pBase & 0x0fffffff );
		DPUT_D1_CHCR( 1 | (1<<8) );

		// check if DMA is complete (STR=0)
		while( (DGET_D1_CHCR() & D_CHCR_STR_M) ) {};

		// check if FINISH event occured
		while( (DGET_GS_CSR() & GS_CSR_FINISH_M) == 0 ) {};

		// change VIF1-FIFO transfer direction (VIF1 -> MAIN MEM or SPR)
		*VIF1_STAT = 0x00800000;

		// change GS bus direction (LOCAL->HOST)
		DPUT_GS_BUSDIR((u_long)0x00000001);

		// DMA to memory and start DMA transfer
		FlushCache(WRITEBACK_DCACHE);
		DPUT_D1_QWC( texture_qwc );
		DPUT_D1_MADR( (u_int)base_addr & 0x0fffffff );
		DPUT_D1_CHCR( 0 | (1<<8) );

		// check if DMA is complete (STR=0)
		while( (DGET_D1_CHCR() & D_CHCR_STR_M) ) {};

		// change VIF1-FIFO transfer direction (MAIN MEM or SPR -> VIF1)
		*VIF1_STAT = 0;
		
		// change GS bus direction (HOST->LOCAL)
		DPUT_GS_BUSDIR((u_long)0);

		// MSKPATH3 is now enabled to allow transfer via PATH3
		DPUT_VIF1_FIFO(*(u_long128 *)enable_path3);
	}



	// Open / Continue / Close
	uint32* openDMATag = NULL;

}





int
gs::tools::GetPSM_BPP			(	uint				inPSM				)
{
	if( (inPSM&15) <= 1 )				return 32;	// SCE_GS_PSMCT32, SCE_GS_PSMCT24, SCE_GS_PSMZ32, SCE_GS_PSMZ24
	else if( (inPSM&3) == 2 )			return 16;	// SCE_GS_PSMCT16, SCE_GS_PSMCT16S, SCE_GS_PSMZ16, SCE_GS_PSMZ16S
	else if( inPSM == SCE_GS_PSMT8 )	return 8;
	else								return 4;	// SCE_GS_PSM4
}



int
gs::tools::GetPSM_BWidth2		(	uint				inPSM				)
{
	if( (inPSM&15) <= 1 )				return 3;	// SCE_GS_PSMCT32, SCE_GS_PSMCT24, SCE_GS_PSMZ32, SCE_GS_PSMZ24
	else if( (inPSM&3) == 2 )			return 4;	// SCE_GS_PSMCT16, SCE_GS_PSMCT16S, SCE_GS_PSMZ16, SCE_GS_PSMZ16S
	else if( inPSM == SCE_GS_PSMT8 )	return 4;
	else								return 5;	// SCE_GS_PSM4
}


int
gs::tools::GetPSM_BHeight2		(	uint				inPSM				)
{
	if( (inPSM&15) <= 1 )				return 3;	// SCE_GS_PSMCT32, SCE_GS_PSMCT24, SCE_GS_PSMZ32, SCE_GS_PSMZ24
	else if( (inPSM&3) == 2 )			return 3;	// SCE_GS_PSMCT16, SCE_GS_PSMCT16S, SCE_GS_PSMZ16, SCE_GS_PSMZ16S
	else if( inPSM == SCE_GS_PSMT8 )	return 4;
	else								return 4;	// SCE_GS_PSM4
}


int
gs::tools::GetPSM_PWidth2		(	uint				inPSM				)
{
	if( (inPSM&15) <= 1 )				return 6;	// SCE_GS_PSMCT32, SCE_GS_PSMCT24, SCE_GS_PSMZ32, SCE_GS_PSMZ24
	else if( (inPSM&3) == 2 )			return 6;	// SCE_GS_PSMCT16, SCE_GS_PSMCT16S, SCE_GS_PSMZ16, SCE_GS_PSMZ16S
	else if( inPSM == SCE_GS_PSMT8 )	return 7;
	else								return 7;	// SCE_GS_PSM4
}


int
gs::tools::GetPSM_PHeight2		(	uint				inPSM				)
{
	if( (inPSM&15) <= 1 )				return 5;	// SCE_GS_PSMCT32, SCE_GS_PSMCT24, SCE_GS_PSMZ32, SCE_GS_PSMZ24
	else if( (inPSM&3) == 2 )			return 6;	// SCE_GS_PSMCT16, SCE_GS_PSMCT16S, SCE_GS_PSMZ16, SCE_GS_PSMZ16S
	else if( inPSM == SCE_GS_PSMT8 )	return 6;
	else								return 7;	// SCE_GS_PSM4
}


int
gs::tools::GetBlockSize		(	uint					inPSM,
								uint					inSizeX,
								uint					inSizeY			)
{
	uint xp2 = GetPSM_BWidth2( inPSM );
	uint yp2 = GetPSM_BHeight2( inPSM );
	return	(( inSizeX + (1<<xp2)-1 ) >> xp2)
		*	(( inSizeY + (1<<yp2)-1 ) >> yp2);
}


int
gs::tools::GetPageSize		(	uint					inPSM,
								uint					inSizeX,
								uint					inSizeY			)
{
	uint xp2 = GetPSM_PWidth2( inPSM );
	uint yp2 = GetPSM_PHeight2( inPSM );
	return	(( inSizeX + (1<<xp2)-1 ) >> xp2)
		*	(( inSizeY + (1<<yp2)-1 ) >> yp2);
}


int
gs::tools::GetPSM32_ABSize	(	uint					inSizeX,
								uint					inSizeY			)
{
	return GetPageSize( SCE_GS_PSMCT32, inSizeX, inSizeY ) << 5;
}


int
gs::tools::GetPSM16_ABSize	(	uint					inSizeX,
								uint					inSizeY			)
{
	return GetPageSize( SCE_GS_PSMCT16, inSizeX, inSizeY ) << 5;
}



void
gs::tools::GetDDABilinearOffset	(	float&		outOffset0,
									float&		outOffset1,
									int			inFromSize,
									int			inToSize		)
{
	float DDA_inc = float(inFromSize) / float(inToSize);
	const float uv_inc  = 1.0f / 16.0f;

	if( DDA_inc == 2.0f ) {
		// Recuding 2:1
		// P0 = 50%(T0) + 50%(T1)
		// P1 = 50%(T2) + 50%(T3)
		outOffset0 = 1.0f;
		outOffset1 = IsPow2(uint(inFromSize)) ? 1.0f : 1.0f+uv_inc;
	}
	else if( DDA_inc == 1.0f ) {
		// Copying 1:1
		// P0 = T0
		// P1 = T1
		outOffset0 = 0.5f;
		outOffset1 = IsPow2(uint(inFromSize)) ? 0.5f : 0.5f+uv_inc;
	}
	else if( DDA_inc == 0.5f ) {
		// Expanding 1:2
		// Sampling uniforme
		// Centrage avec r�p�tition des premiers et derniers texels (0) et (j-1)
		// P0   = T0
		// P1   = 75%(T0) + 25%(T1)
		// P2   = 25%(T0) + 75%(T1)
		// P3   = 75%(T2) + 25%(T3)
		// P4   = 25%(T2) + 75%(T3)
		// ...
		// Pi-3 = 75%(Tj-2) + 25%(Tj-1)
		// Pi-2 = 25%(Tj-2) + 75%(Tj-1)
		// Pi-1 = Tj-1
		outOffset0 = 0.25f;
		outOffset1 = 0.25f; // IsPow2(uint(inFromSize)) ? -0.25f : 0.25f+uv_inc;

/*		// Expanding 1:2
		// Sampling non uniforme
		// w0 est la pond�ration du texel(0) dans le pixel(0), valeur valide dans le range [0.5->0.95]
		// La valeur 0.75 donne la meilleure constance de sampling
		// outOffset1 est calcul� de mani�re � avoir la m�me pond�ration des texels (0) et (j-1)
		// P0   = w0%(T0) + (1-w0)%(T1)
		// P1   = w1%(T0) + (1-w1)%(T1)
		// ...
		// Pi-2 = (1-w1)%(Tj-2) + w1%(Tj-1)
		// Pi-1 = (1-w0)%(Tj-2) + w0%(Tj-1)
		// !!!!!! R�sulat incorrect d� suremment aux arrondis internes du GS !!!
		float w0 = 0.75f;
		outOffset0 = 1.5f - w0;
		outOffset1 = (float(inFromSize) - outOffset0 * float(inToSize+1)) / float(inToSize-1);
*/
	}
	else {
		// Expanding > 1:2 or Reducing < 2:1
		outOffset0 = 0.5f;
		outOffset1 = 0.5f;
	}
}




bool
gs::tools::Gs_Open_CH1		(	dmac::Cursor*	inDC,
								bool			inEnableInit	)
{
	NV_ASSERT_DC( inDC );
	bool opened = ( openDMATag == NULL );
	if( opened ) {
		openDMATag = inDC->i32;
		inDC->i128 += 2;
	}

	// Init GS CTXT2 ?
	if( inEnableInit ) {
		gs::Disable_PRMODE( inDC );
		gs::Disable_DTHE( inDC );
		gs::Disable_PABE( inDC );
		gs::Set_Clamp_COLCLAMP( inDC );
		gs::Set_Clamp_CLAMP_2( inDC );
		gs::Set_ZBUF_2( inDC, 0, 0, 1 );								// disable zwritting
		gs::Set_TEST_2( inDC, 1, 1, 0, 0, 0, 0, 1, SCE_GS_ZALWAYS );	// disable ztesting
		gs::Set_XYOFFSET_2( inDC, 0, 0 );
	}

	return opened;
}



uint
gs::tools::Gs_QSize_CH1		(	dmac::Cursor*			inDC	)
{
	NV_ASSERT_DC( inDC );
	if( !openDMATag )
		return 0;
	NV_ASSERT( inDC->i32 > openDMATag );
	return (uint32(inDC->i32) - uint32(openDMATag)) >> 4;
}



bool
gs::tools::Gs_Continue_CH1	(	dmac::Cursor*	inDC	)
{
	NV_ASSERT_DC( inDC );
	bool closed = Gs_Close_CH1( inDC );
	if( !closed )	return FALSE;
	Gs_Open_CH1( inDC, FALSE );
	return TRUE;
}



bool
gs::tools::Gs_Close_CH1	(	dmac::Cursor*		inDC	)
{
	NV_ASSERT_DC( inDC );
	if( !openDMATag )
		return FALSE;

	uint qsize = Gs_QSize_CH1( inDC );
	NV_ASSERT( qsize >= 2 );
	NV_ASSERT( qsize <  32000 );
	AS_UINT32( openDMATag[0] ) = DMA_TAG_CNT | (qsize-1);
	AS_UINT32( openDMATag[1] ) = 0;
	AS_UINT32( openDMATag[2] ) = SCE_VIF1_SET_FLUSH( 0 );								// GS sync
	AS_UINT32( openDMATag[3] ) = SCE_VIF1_SET_DIRECT( (qsize-1), 0 );
	AS_UINT64( openDMATag[4] ) = SCE_GIF_SET_PACKED_TAG( (qsize-2), 1, 0, 0, 1 );		// limited to 32KQ
	AS_UINT64( openDMATag[6] ) = 0x0E;		// A+D

	openDMATag = NULL;
	return TRUE;
}



void
gs::tools::Gs_TrxPass_CH1	(	dmac::Cursor*		inDC,
								int					inSizeX,
								int					inSizeY,
								uint				inPSM,
								int					inToBAddr,
								uint128*			inData		)
{
	NV_ASSERT_A128( inData );
	NV_ASSERT( inSizeX );
	NV_ASSERT( inSizeY );

	bool closed = Gs_Close_CH1( inDC );

	// Beware that PATH3 access TRX GS registers in parallel from the PATH1.
	// To prevent an access conflit, a sync is needed using FLUSHA !
	// - FLUSHA : sync with texture PATH3 opened/closed

	// data qsize
	uint psmBPP = GetPSM_BPP( inPSM );
	uint qsize  = (inSizeX * inSizeY * psmBPP) >> (4+3);
	uint part   = (qsize+8191) >> 13;

	int iy = inSizeY / part;
	for( int y = 0 ; y < inSizeY ; y += iy ) {
		int RRH   = Min( inSizeY-y, iy );
		    qsize = (inSizeX * RRH * psmBPP + 127) >> 7;

		// Qsize is limited by VIF1_DIRECT(16bits) & GIF_TAG.NLOOP(15bits)
		NV_ASSERT( qsize < 32768 );

		inDC->i32[0] = DMA_TAG_CNT | 6;
		inDC->i32[1] = 0;
		inDC->i32[2] = SCE_VIF1_SET_FLUSH( 0 );
		inDC->i32[3] = SCE_VIF1_SET_DIRECT( 6, 0 );
		inDC->i64[2] = SCE_GIF_SET_PACKED_TAG( 4, 0, 0, 0, 1 );		// EOP=0
		inDC->i64[3] = 0x0E;										// A+D
		inDC->i64[4] = SCE_GS_SET_BITBLTBUF( 0, 0, 0, inToBAddr, (inSizeX+63)>>6, inPSM );
		inDC->i64[5] = SCE_GS_BITBLTBUF;
		inDC->i64[6] = SCE_GS_SET_TRXREG( inSizeX, RRH );
		inDC->i64[7] = SCE_GS_TRXREG;
		inDC->i64[8] = SCE_GS_SET_TRXPOS( 0, 0, 0, y, 0 );
		inDC->i64[9] = SCE_GS_TRXPOS;
		inDC->i64[10]= SCE_GS_SET_TRXDIR( 0 );						// HOST TO LOCAL
		inDC->i64[11]= SCE_GS_TRXDIR;
		inDC->i64[12]= SCE_GIF_SET_IMAGE_TAG( qsize, 1 );			// EOP=1 (15bits NLOOP)
		inDC->i64[13]= 0;
		inDC->i128  += 7;

		inDC->i32[0] = DMA_TAG_REF | qsize;
		inDC->i32[1] = DMA_ADDR( inData );
		inDC->i32[2] = 0;
		inDC->i32[3] = SCE_VIF1_SET_DIRECT( qsize, 0 );				// (16bits DIRECT)
		inDC->i128  += 1;

		inData      += qsize;
	}

	NV_ASSERT_DC( inDC );

	if( closed )
		Gs_Open_CH1( inDC, FALSE );
}



void
gs::tools::Gs_GrabPass_CH1	(	int						inSizeX,
								int						inSizeY,
								uint					inPSM,
								int						inFromBAddr,
								uint128*				inBuffer		)
{
	NV_ASSERT_A128( inBuffer );
	NV_ASSERT( inSizeX );
	NV_ASSERT( inSizeY );

	// data qsize
	uint psmBPP = GetPSM_BPP( inPSM );
	uint qsize  = (inSizeX * inSizeY * psmBPP) >> (4+3);
	uint part   = (qsize+8191)/8192;

	int iy = inSizeY / part;
	for( int y = 0 ; y < inSizeY ; y += iy ) {
		int dy = Min( inSizeY-y, iy );
		Grab_CH1( inBuffer, inFromBAddr, inPSM, inSizeX, 0, y, inSizeX, dy );
		qsize  = (inSizeX * dy * psmBPP) >> (4+3);
		inBuffer += qsize;
	}
}



void
gs::tools::Gs_DrawSpritePass_CH1	(	dmac::Cursor*			inDC,
										Vec4*					inToRegion,			// { x0, y0, x1, y1 }
										Vec4*					inFromRegion,		// { u0, v0, u1, v1 }
										uint32					inZ			)
{
	if( !inToRegion )	return;
	if( inFromRegion ) {
		gs::Set_UVf	 ( inDC,	inFromRegion->x,	inFromRegion->y		);
		gs::Set_XYZ2f( inDC,	inToRegion->x,		inToRegion->y,	inZ	);
		gs::Set_UVf	 ( inDC,	inFromRegion->z,	inFromRegion->w		);
		gs::Set_XYZ2f( inDC,	inToRegion->z, 		inToRegion->w,	inZ	);
	} else {
		gs::Set_XYZ2f( inDC,	inToRegion->x,		inToRegion->y,	inZ	);
		gs::Set_XYZ2f( inDC,	inToRegion->z, 		inToRegion->w,	inZ	);
	}
}



void
gs::tools::Gs_ApplyPass_CH1		(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inBAddr,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	NV_ASSERT( (inBAddr&31) == 0 );

	gs::SetFrame_2( inDC, inBAddr>>5, inSizeX, SCE_GS_PSMCT32, inFBMSK );
	gs::Set_XYOFFSET_2( inDC, 0, 0 );
	if( !(inFlags&PF_NOSCISSOR) )
		gs::Set_SCISSOR_2( inDC, 0, inSizeX-1, 0, inSizeY-1 );
	gs::Set_PRIM( inDC, 6, 0, 0, 0, (inFlags&PF_ABE)?1:0, 0, 1, 1, 0 );		// [ABE]/UV/CTXT2

	Vec4 toReg( 0, 0, inSizeX, inSizeY );
	Gs_DrawSpritePass_CH1( inDC, &toReg, NULL, inZ );
}



void
gs::tools::Gs_ApplyDepthPass_CH1	(	dmac::Cursor*			inDC,
										int						inSizeX,
										int						inSizeY,
										int						inBAddr,
										uint					inFlags,
										uint32					inFBMSK,
										uint32					inZ			)
{
	NV_ASSERT( (inBAddr&31) == 0 );

	gs::SetFrame_2( inDC, inBAddr>>5, inSizeX, SCE_GS_PSMZ32, inFBMSK );
	gs::Set_XYOFFSET_2( inDC, 0, 0 );
	if( !(inFlags&PF_NOSCISSOR) )
		gs::Set_SCISSOR_2( inDC, 0, inSizeX-1, 0, inSizeY-1 );
	gs::Set_PRIM( inDC, 6, 0, 0, 0, (inFlags&PF_ABE)?1:0, 0, 1, 1, 0 );		// [ABE]/UV/CTXT2

	Vec4 toReg( 0, 0, inSizeX, inSizeY );
	Gs_DrawSpritePass_CH1( inDC, &toReg, NULL, inZ );
}



void
gs::tools::Gs_InvertPass_CH1		(	dmac::Cursor*			inDC,
										int						inSizeX,
										int						inSizeY,
										int						inBAddr,
										uint					inFlags,
										uint32					inFBMSK,
										uint32					inZ			)
{
	// The ALPHA component is not modified by the GS alpha blending process
	// -> A is keeped !
	inFBMSK |= 0xFF000000;
	gs::Set_RGBAQ( inDC, 0xFFFFFFFF );
	gs::Set_ALPHA_2( inDC, 0, 1, 2, 2, 0x80 );
	Gs_ApplyPass_CH1(	inDC,
						inSizeX, inSizeY, inBAddr,
						inFlags|PF_ABE, inFBMSK, inZ );
}



void
gs::tools::Gs_InvertDepthPass_CH1	(	dmac::Cursor*			inDC,
										int						inSizeX,
										int						inSizeY,
										int						inBAddr,
										uint					inFlags,
										uint32					inFBMSK,
										uint32					inZ			)
{
	// The ALPHA component is not modified by the GS alpha blending process
	// -> A is keeped !
	inFBMSK |= 0xFF000000;
	gs::Set_RGBAQ( inDC, 0xFFFFFFFF );
	gs::Set_ALPHA_2( inDC, 0, 1, 2, 2, 0x80 );
	Gs_ApplyDepthPass_CH1(	inDC,
							inSizeX, inSizeY, inBAddr,
							inFlags|PF_ABE, inFBMSK, inZ );
}





void
gs::tools::Gs_BlitPass_CH1		(	dmac::Cursor*			inDC,
									int						inFromSizeX,
									int						inFromSizeY,
									Vec4*					inFromRegion,
									int						inFromBAddr,
									int						inToSizeX,
									int						inToSizeY,
									Vec4*					inToRegion,
									int						inToBAddr,
									int						inClutBAddr,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	NV_ASSERT( (inToBAddr&31) == 0 );

	Vec4 fromReg;	// {u0,v0,u1,v1}
	fromReg.x = 			(inFromRegion ? int(inFromRegion->x) : 0);
	fromReg.y = 			(inFromRegion ? int(inFromRegion->y) : 0);
	fromReg.z = fromReg.x + (inFromRegion ? int(inFromRegion->z) : inFromSizeX);
	fromReg.w = fromReg.y + (inFromRegion ? int(inFromRegion->w) : inFromSizeY);

	Vec4 toReg;		// {x0,y0,x1,y1} + 1024
	toReg.x   = 1024.0f + (inToRegion ? int(inToRegion->x) : 0);
	toReg.y   = 1024.0f	+ (inToRegion ? int(inToRegion->y) : 0);
	toReg.z   = toReg.x + (inToRegion ? int(inToRegion->z) : inToSizeX);
	toReg.w   = toReg.y + (inToRegion ? int(inToRegion->w) : inToSizeY);

	// DDA correction for uv
	if( inFlags & PF_NEAREST ) {
		fromReg += Vec4(0.5f,0.5f,0.5f,0.5f);		// center of texels
	} else {
		float du0, du1, dv0, dv1;
		GetDDABilinearOffset( du0, du1, int(fromReg.z-fromReg.x), int(toReg.z-toReg.x) );
		GetDDABilinearOffset( dv0, dv1, int(fromReg.w-fromReg.y), int(toReg.w-toReg.y) );
		fromReg += Vec4( du0, dv0, du1, dv1 );
	//	Printf("DDA_inc_u = %f, du0=%f, dv0=%f du1=%f dv1=%f\n", float(inFromRegionSizeX)/float(inToRegionSizeX), du0, dv0, du1, dv1 );
	}

	uint tex_psm, tex_tfx, tex_mm;
	if( inClutBAddr >= 0 )		tex_psm = SCE_GS_PSMT8H;
	else if( inFlags&PF_PSM24 )	tex_psm = SCE_GS_PSMCT24;
	else						tex_psm = SCE_GS_PSMCT32;
	if( inFlags&PF_MODULATE )	tex_tfx = SCE_GS_MODULATE;
	else						tex_tfx = SCE_GS_DECAL;
	if( inFlags&PF_NEAREST )	tex_mm	= 0;
	else						tex_mm	= 1;
	gs::SetTexture_2( inDC, inFromBAddr, inFromSizeX, inFromSizeY, tex_psm, tex_tfx, tex_mm, inClutBAddr );

	if( !(inFlags&PF_NOCLAMP) )
		gs::Set_CLAMP_2( inDC, 2, 2, 0, inFromSizeX-1, 0, inFromSizeY-1 );		// Region clamping for no 2^x source !

	if( !(inFlags&PF_NOSCISSOR) )
		gs::Set_SCISSOR_2( inDC, 0, inToSizeX-1, 0, inToSizeY-1 );

	gs::SetFrame_2( inDC, inToBAddr>>5, inToSizeX, SCE_GS_PSMCT32, inFBMSK );
	gs::Set_XYOFFSET_2( inDC, 1024<<4, 1024<<4 );
	gs::Set_PRIM( inDC, 6, 0, 1, 0, (inFlags&PF_ABE)?1:0, 0, 1, 1, 0 );			// TME/[ABE]/UV/CTXT2
	Gs_DrawSpritePass_CH1( inDC, &toReg, &fromReg, inZ );
	gs::Set_XYOFFSET_2( inDC, 0, 0 );
}




void
gs::tools::Gs_TranslatePass_CH1	(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inToOffsetX,
									int						inToOffsetY,
									int						inFromBAddr,
									int						inToBAddr,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	Vec4 toReg( inToOffsetX, inToOffsetY, inSizeX, inSizeY );
	Gs_BlitPass_CH1	(	inDC,
						inSizeX, inSizeY, NULL, inFromBAddr,
						inSizeX, inSizeY, &toReg, inToBAddr,
						-1, inFlags|PF_NEAREST, inFBMSK, inZ );
}



void
gs::tools::Gs_CopyPass_CH1	(	dmac::Cursor*			inDC,
								int						inSizeX,
								int						inSizeY,
								int						inFromBAddr,
								int						inToBAddr,
								uint					inFlags,
								uint32					inFBMSK,
								uint32					inZ		)
{
	Gs_TranslatePass_CH1( inDC, inSizeX, inSizeY, 0, 0, inFromBAddr, inToBAddr, inFlags, inFBMSK, inZ );
}



void
gs::tools::Gs_ResizePass_CH1	(	dmac::Cursor*			inDC,
									int						inFromSizeX,
									int						inFromSizeY,
									int						inFromBAddr,
									int						inToSizeX,
									int						inToSizeY,
									int						inToBAddr,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	Gs_BlitPass_CH1	(	inDC,	inFromSizeX, inFromSizeY, NULL, inFromBAddr,
								inToSizeX,   inToSizeY,   NULL, inToBAddr,
								-1, inFlags, inFBMSK, inZ );
}



void
gs::tools::Gs_Downx2Pass_CH1	(	dmac::Cursor*			inDC,
									int						inFromSizeX,
									int						inFromSizeY,
									int						inFromBAddr,
									int						inToBAddr,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	Gs_BlitPass_CH1	(	inDC,	inFromSizeX,    inFromSizeY,    NULL, inFromBAddr,
								inFromSizeX>>1, inFromSizeY>>1, NULL, inToBAddr,
								-1, inFlags, inFBMSK, inZ );
}



void
gs::tools::Gs_Upx2Pass_CH1		(	dmac::Cursor*			inDC,
									int						inFromSizeX,
									int						inFromSizeY,
									int						inFromBAddr,
									int						inToBAddr,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	Gs_BlitPass_CH1	(	inDC,	inFromSizeX,    inFromSizeY,    NULL, inFromBAddr,
								inFromSizeX<<1, inFromSizeY<<1, NULL, inToBAddr,
								-1, inFlags, inFBMSK, inZ );
}



bool
gs::tools::Gs_PreparePass_CH1	(	dmac::Cursor*			inDC,
									int						inFromSizeX,
									int						inFromSizeY,
									int						inFromBAddr,
									int						inToSizeX,
									int						inToSizeY,
									int						inToBAddr,
									float					inFactor,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	NV_ASSERT( inFactor > 0.0f );
	if( inFactor <= 0.0f )	return FALSE;

	// Resizing
	int sx = inToSizeX;
	int sy = inToSizeY;
	int rx = int( float(inFromSizeX) * inFactor );
	int ry = int( float(inFromSizeY) * inFactor );

	// Safeband
	if( sx < rx )			return FALSE;	// invalid factor !?
	if( sy < ry )			return FALSE;	// invalid factor !?
	if( (sx-rx)&1	)		rx--;
	if( (sy-ry)&1	)		ry--;
	if( rx == 0 )			return FALSE;	// invalid factor !?
	if( ry == 0 )			return FALSE;	// invalid factor !?
	int bx = (sx - rx) >> 1;
	int by = (sy - ry) >> 1;

	// Resize
	Vec4 fromReg, toReg;
	toReg.Set( bx, by, rx, ry );
	Gs_BlitPass_CH1( inDC,	inFromSizeX, inFromSizeY, NULL,   inFromBAddr,
							sx,          sy,          &toReg, inToBAddr,
							-1, inFlags, inFBMSK, inZ );

	// Clamp right
	fromReg.Set( bx, by, 0,  ry );
	toReg.Set  ( 0,  by, bx, ry );
	Gs_BlitPass_CH1( inDC, sx, sy, &fromReg, inToBAddr,
						   sx, sy, &toReg,   inToBAddr,
						   -1, inFlags|PF_NEAREST, inFBMSK, inZ );

	// Clamp left
	fromReg.Set( sx-bx-1, by, 0,  ry );
	toReg.Set  ( sx-bx,   by, bx, ry );
	Gs_BlitPass_CH1( inDC, sx, sy, &fromReg, inToBAddr,
						   sx, sy, &toReg,   inToBAddr,
						   -1, inFlags|PF_NEAREST, inFBMSK, inZ );

	// Clamp top
	fromReg.Set( 0, by, sx, 0  );
	toReg.Set  ( 0, 0,  sx, by );
	Gs_BlitPass_CH1( inDC, sx, sy, &fromReg, inToBAddr,
						   sx, sy, &toReg,   inToBAddr,
						   -1, inFlags|PF_NEAREST, inFBMSK, inZ );

	// Clamp bottom
	fromReg.Set( 0, sy-by-1, sx, 0  );
	toReg.Set  ( 0, sy-by,   sx, by );
	Gs_BlitPass_CH1( inDC, sx, sy, &fromReg, inToBAddr,
						   sx, sy, &toReg,   inToBAddr,
						   -1, inFlags|PF_NEAREST, inFBMSK, inZ );

	return TRUE;
}



bool
gs::tools::Gs_UnpreparePass_CH1	(	dmac::Cursor*			inDC,
									int						inFromSizeX,
									int						inFromSizeY,
									int						inFromBAddr,
									int						inToSizeX,
									int						inToSizeY,
									int						inToBAddr,
									float					inFactor,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	NV_ASSERT( inFactor > 0.0f );
	if( inFactor <= 0.0f )	return FALSE;

	// Resizing
	int sx = inFromSizeX;
	int sy = inFromSizeY;
	int rx = int( float(inToSizeX) * inFactor );
	int ry = int( float(inToSizeY) * inFactor );

	// Safeband
	if( sx < rx )			return FALSE;	// invalid factor !?
	if( sy < ry )			return FALSE;	// invalid factor !?
	if( (sx-rx)&1	)		rx--;
	if( (sy-ry)&1	)		ry--;
	if( rx == 0 )			return FALSE;	// invalid factor !?
	if( ry == 0 )			return FALSE;	// invalid factor !?
	int bx = (sx - rx) >> 1;
	int by = (sy - ry) >> 1;

	Vec4 fromReg( bx, by, rx, ry );
	Gs_BlitPass_CH1( inDC, inFromSizeX, inFromSizeY, &fromReg, inFromBAddr,
						   inToSizeX,   inToSizeY,   NULL,     inToBAddr,
						   -1, inFlags, inFBMSK, inZ );

	return TRUE;
}



void
gs::tools::Kernel33::Reset	(		)
{
	k00 = k01 = k02 = 0;
	k10 = k11 = k12 = 0;
	k20 = k21 = k22 = 0;
}



void
gs::tools::Kernel33::Normalize	(		)
{
	float s  = k00 + k01 + k02;
		  s += k10 + k11 + k12;
		  s += k20 + k21 + k22;
	if( s > 0.0f ) {
		float oos = 1.0f / s;
		k00 *= oos;		k01 *= oos;		k02 *= oos;
		k10 *= oos;		k11 *= oos;		k12 *= oos;
		k20 *= oos;		k21 *= oos;		k22 *= oos;
	}
}



void
gs::tools::Kernel33::RotateCW	(		)
{
	float _k;
	_k = k00; k00 = k20; k20 = k22; k22 = k02; k02 = _k;
	_k = k01; k01 = k10; k10 = k21; k21 = k12; k12 = _k;
}



void
gs::tools::Kernel33::RotateCCW	(		)
{
	float _k;
	_k = k00; k00 = k02; k02 = k22; k22 = k20; k20 = _k;
	_k = k01; k01 = k12; k12 = k21; k21 = k10; k10 = _k;
}



void
gs::tools::Gs_ConvolvePass_CH1	(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inFromBAddr,
									int						inToBAddr,
									const Kernel33&			inKernel,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	// Ajout du voisinnage

	// From outer to inner for round error accumulation
	//				     00	    02	   20	  22	 10		12		01,		21	   11
	int   kidxA[9]   = { 0*4+0, 0*4+2, 2*4+0, 2*4+2, 1*4+0,	1*4+2,	0*4+1,	2*4+1, 1*4+1 };
	float round_err  = 0.0f;						// Cumul round alpha error to preserve the global brightness
	const float d128 = 1.0f / 128.0f;
	int   alpha_A = 0, alpha_B = 2, alpha_D = 2;	// ALPHA Add with 0

	// Two passes :
	// - first for >0 kernel weights
	// - second for <0 kernel weights
	for( int sign = 0 ; sign < 2 ; sign++ )
	{
		for( int kidx = 0 ; kidx < 9 ; kidx++ ) {
			int   ki = kidxA[ kidx ] & 3;
			int   kj = kidxA[ kidx ] >> 2;
			float k  = inKernel.k[ kj ][ ki ];

			// >0 xor <0 kernel weights
			NV_ASSERT( (k<=0.0f) <= 1 );
			if( sign != (k<=0.0f) )	continue;

			float fa = ( k + round_err ) * 128.0f;
			int   ia = int( fa );
			round_err = ( fa - ia ) * d128;
			if( ia > 0 ) {
				gs::Set_ALPHA_2( inDC, alpha_A, alpha_B, 2, alpha_D, ia );	// Add/Sub ((A-B) is signed on GS!)
				Gs_TranslatePass_CH1( inDC, inSizeX, inSizeY, 1-ki, 1-kj, inFromBAddr, inToBAddr, inFlags|PF_ABE, inFBMSK, inZ );
				alpha_D = 1;	// ALPHA.D = frame
			}
		}
		// Global convolving is <= 0 ?
		if( alpha_D == 2 )	return;
		Swap( alpha_A, alpha_B );	// Sub
	}
}



void
gs::tools::Gs_SepConvolvePass_CH1	(	dmac::Cursor*			inDC,
										int						inSizeX,
										int						inSizeY,
										int						inBAddr,
										int						inTmpBAddr,
										const Kernel33&			inKernel,
										uint					inFlags,
										uint32					inFBMSK,
										uint32					inZ			)
{
	Kernel33 k;
	k.Reset();

	// Horizontal
	k.k10 = inKernel.k10;
	k.k11 = inKernel.k11;
	k.k12 = inKernel.k12;
	Gs_ConvolvePass_CH1( inDC, inSizeX, inSizeY, inBAddr, inTmpBAddr, k, inFlags, inFBMSK, inZ );

	// Vertical
	k.RotateCW();
	Gs_ConvolvePass_CH1( inDC, inSizeX, inSizeY, inTmpBAddr, inBAddr, k, inFlags, inFBMSK, inZ );
}



void
gs::tools::Gs_DiffConvolvePass_CH1(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inFromBAddr,
									int						inToBAddr,
									const Kernel33&			inKernel,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	// Convolution diff�rentielle additive
	// Le principe est d'ajouter au centre la diff�rence POSITIVE du voisinage
	// Le centre a un diff�rentiel de 0, i.e. P(i,j)-P(i,j) = 0
	// L'effet obtenu est un �largissement des zones les plus lumineuses
	// P(i,j) += % ABS( P(i,j) - P(i-k,j-l) )

	// From outer to inner for round error accumulation
	//				     00	    02	   20	  22	 10		12		01,		21	   11
	int   kidxA[9]   = { 0*4+0, 0*4+2, 2*4+0, 2*4+2, 1*4+0,	1*4+2,	0*4+1,	2*4+1, 1*4+1 };
	const float d128 = 1.0f / 128.0f;

	int tmpBAddr = inToBAddr + GetPSM32_ABSize( inSizeX, inSizeY );
	Vec4 fromReg, toReg;

	for( int x = 0 ; x < inSizeX ; x += 32 ) {
		int   dx        = Min( inSizeX-x, 32 );
		float round_err = 0.0f;							// Cumul round alpha error to preserve the global brightness
		int   alpha_A   = 0, alpha_B = 2, alpha_D = 2;	// ALPHA Add with 0

		// Le centre (k11) � un differentiel de 0 et n'a pas besoin d'�tre calcul� !
		// donc kidx va de 0 � 8

		// Two passes :
		// - first for >0 kernel weights
		// - second for <0 kernel weights
		for( int sign = 0 ; sign < 2 ; sign++ )
		{
			for( int kidx = 0 ; kidx < 8 ; kidx++ ) {
				int   ki = kidxA[ kidx ] & 3;
				int   kj = kidxA[ kidx ] >> 2;
				float k  = inKernel.k[ kj ][ ki ];

				// >0 xor <0 kernel weights
				NV_ASSERT( (k<=0.0f) <= 1 );
				if( sign != (k<=0.0f) )	continue;

				float fa = ( k + round_err ) * 128.0f;
				int   ia = int( fa );
				round_err = ( fa - ia ) * d128;
				if( ia > 0 ) {
					// Copy region to tmp
					fromReg.Set( x, 0, dx, inSizeY );
					toReg.Set  ( 0, 0, dx, inSizeY );
					Gs_BlitPass_CH1( inDC, inSizeX, inSizeY, &fromReg, inFromBAddr,
										   dx,      inSizeY, &toReg,   tmpBAddr,
										   -1, inFlags|PF_NEAREST, inFBMSK, inZ );

					// difference positive -> sub & store with color clamping
					fromReg.Set( x+1-ki, 1-kj, dx, inSizeY );
					toReg.Set  ( 0,      0,    dx, inSizeY );
					gs::Set_ALPHA_2( inDC, 0, 1, 2, 2, 0x80 );
					Gs_BlitPass_CH1( inDC, inSizeX, inSizeY, &fromReg, inFromBAddr,
										   dx,      inSizeY, &toReg,   tmpBAddr,
										   -1, inFlags|PF_NEAREST|PF_ABE, inFBMSK, inZ );

					// Cumul %(difference) in dest
					fromReg.Set( 0, 0, dx, inSizeY );
					toReg.Set  ( x, 0, dx, inSizeY );
					gs::Set_ALPHA_2( inDC, alpha_A, alpha_B, 2, alpha_D, ia );	// Add/Sub ((A-B) is signed on GS!)
					Gs_BlitPass_CH1( inDC, dx,      inSizeY, &fromReg, tmpBAddr,
										   inSizeX, inSizeY, &toReg,   inToBAddr,
										   -1, inFlags|PF_NEAREST|PF_ABE, inFBMSK, inZ );
					alpha_D = 1;	// ALPHA.D = frame
				}
			}
			// Global convolving is <= 0 ?
			if( alpha_D == 2 )	return;
			Swap( alpha_A, alpha_B );	// Sub
		}
	}
}



void
gs::tools::Gs_GaussianBlurPass_CH1(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inBAddr,
									int						inTmpBAddr,
									float					inRadius,
									float					inQuality,		// in [0,1]
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	//
	// Blur Gaussien bas� sur le m�lange d'un ou plusieurs mipmaps de l'image source

	MipmapGC	mip_gc[16];
	int			mip_baddr[16];
	Kernel33	smoothK;
	smoothK.Reset();
	smoothK.k11 = 0.5f;
	smoothK.k10 = smoothK.k12 = smoothK.k01 = smoothK.k21 = 0.25f;

	// Computes Mipmaped Gaussian convolution interpolation

	int mip_cpt = Compute_MipmapedGaussConvol( mip_gc, inSizeX, inSizeY, inRadius, inQuality );
	if( mip_cpt == 0 )
		return;

	// Computes mipmaps

	int   fromBAddr  = mip_baddr[0] = inBAddr;
	int   toBAddr    = inTmpBAddr;
	for( int mip = 1 ; mip <= mip_cpt ; mip++ ) {
		// Downsizing
		Gs_Downx2Pass_CH1( inDC, inSizeX>>(mip-1), inSizeY>>(mip-1), fromBAddr, toBAddr, inFlags, inFBMSK, inZ );
		mip_baddr[mip] = toBAddr;
		fromBAddr  = toBAddr;
		toBAddr   += GetPSM32_ABSize( inSizeX>>mip, inSizeY>>mip );

		// Smooth mipmap for better quality ?
	//	if( mip_gc[mip].quality )
	//		Gs_SepConvolvePass_CH1( inDC, inSizeX>>mip, inSizeY>>mip, fromBAddr, toBAddr, smoothK, inFlags, inFBMSK, inZ );
	}

	// Expand mipmaps with low-rez -> high-rez blending

	for( int mip = mip_cpt ; mip > 0 ; mip-- ) {
		gs::Set_ALPHA_2( inDC, 0, 1, 2, 1, mip_gc[mip].blend );		// Blend M(i)*a + M(i-1)*(1-a)
		Gs_Upx2Pass_CH1( inDC, inSizeX>>mip, inSizeY>>mip, mip_baddr[mip], mip_baddr[mip-1], PF_ABE|inFlags, inFBMSK, inZ );
	}
}



void
gs::tools::Gs_GlowBlurPass_CH1	(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inBAddr,
									int						inTmpBAddr,
									float					inRadius,
									float					inQuality,
									Kernel33*				inKernel,		// 3x3 matrix
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	Kernel33 diffK;
	if( !inKernel ) {
		diffK.k00 = diffK.k02 = diffK.k20 = diffK.k22 = 0.7f;
		diffK.k01 = diffK.k21 = diffK.k10 = diffK.k12 = 1.0f;
		diffK.k11 = 0.0f;
		diffK.Normalize();
		inKernel = &diffK;
	}

	//
	// Blur Gaussien bas� sur le m�lange d'un ou plusieurs mipmaps de l'image source

	MipmapGC	mip_gc[16];
	int			mip_baddr[16];

	// Computes Mipmaped Gaussian convolution interpolation

	int mip_cpt = Compute_MipmapedGaussConvol( mip_gc, inSizeX, inSizeY, inRadius, inQuality );
	if( mip_cpt == 0 )
		return;

	// Computes mipmaps

	int   fromBAddr  = mip_baddr[0] = inBAddr;
	int   toBAddr    = inTmpBAddr;
	for( int mip = 1 ; mip <= mip_cpt ; mip++ ) {
		// Downsizing
		Gs_Downx2Pass_CH1( inDC, inSizeX>>(mip-1), inSizeY>>(mip-1), fromBAddr, toBAddr, inFlags, inFBMSK, inZ );
		mip_baddr[mip] = toBAddr;
		fromBAddr  = toBAddr;
		toBAddr   += GetPSM32_ABSize( inSizeX>>mip, inSizeY>>mip );

		// Dilatation
		Gs_DiffConvolvePass_CH1( inDC, inSizeX>>mip, inSizeY>>mip, fromBAddr, toBAddr, *inKernel, inFlags, inFBMSK, inZ );
		gs::Set_ALPHA_2( inDC, 0, 2, 2, 1, 0x80	);		// Add
		Gs_CopyPass_CH1( inDC, inSizeX>>mip, inSizeY>>mip, toBAddr, fromBAddr, PF_ABE|inFlags, inFBMSK, inZ );
	}

	// Expand mipmaps with low-rez -> high-rez blending

	for( int mip = mip_cpt ; mip > 0 ; mip-- ) {
		gs::Set_ALPHA_2( inDC, 0, 1, 2, 1, mip_gc[mip].blend );		// Blend M(i)*a + M(i-1)*(1-a)
		Gs_Upx2Pass_CH1( inDC, inSizeX>>mip, inSizeY>>mip, mip_baddr[mip], mip_baddr[mip-1], /*PF_ABE|*/inFlags, inFBMSK, inZ );
	}
}



void
gs::tools::Gs_FocalBlurPass_CH1	(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inBAddr,
									int						inTmpBAddr,
									float					inRadius,
									float					inQuality,
									Kernel33*				inKernel,		// 3x3 matrix
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ			)
{
	Kernel33 diffK;
	if( !inKernel ) {
		diffK.k00 = diffK.k02 = diffK.k20 = diffK.k22 = 0.7f;
		diffK.k01 = diffK.k21 = diffK.k10 = diffK.k12 = 1.0f;
		diffK.k11 = 0.0f;
		inKernel = &diffK;
	}

	while( inRadius > 0.0f ) {
		Gs_DiffConvolvePass_CH1( inDC, inSizeX, inSizeY, inBAddr, inTmpBAddr, *inKernel, inFlags, inFBMSK, inZ );
		gs::Set_ALPHA_2( inDC, 0, 2, 2, 1, 0x80	);		// Add
		Gs_CopyPass_CH1( inDC, inSizeX, inSizeY, inTmpBAddr, inBAddr, PF_ABE|inFlags, inFBMSK, inZ );
		inRadius--;
	}
}



void
gs::tools::Gs_DepthToAlphaPass_CH1(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inFromBAddr,
									int						inToBAddr	)
{
	NV_ASSERT( (inToBAddr%32) == 0 );
	NV_ASSERT( (inFromBAddr%32) == 0 );

	// Copie les bits [15-8] du ZBuffer vers l'alpha du BackBuffer.
	// R�sultat :
	// alpha  *<--------- 255 -------->*<------ 255 to 0 --------->*
	// Z     Max                    0x10000                        0
	//
	// Cette op�ration est obtenue par un move RG into BA en PSMCT16
	// Un test en Z est utilis� pour s�lectionner un range en Z de pixels
	// Cette m�thode lib�re compl�tement le ZBuffer qui peut �tre r�utilis� ult�rieurement
	// 3 Passes :
	// A- Remise � 0xFF de la composante alpha du BackBuffer
	// B- Flag des pixels du BackBuffer (alpha=0) dans le range de Z souhait�
	// C- Copie Z[15-8] -> Back[alpha] avec destination alpha test (equal to 0 pass)
	//
	// Remarque: Une version avec seulement la passe (C) est possible mais n�cessite
	// de pr�server le ZBuffer pour le test en SCE_GS_ZGREATER ult�rieur.

	gs::Set_TEST_2( inDC, 1, 1, 0, 0, 0, 0, 1, SCE_GS_ZALWAYS );
	gs::Set_ZBUF_2( inDC, inFromBAddr>>5, 0, 1 );		// ZMSK=1

	// A- Remise � 0xFF de la composante alpha du BackBuffer
	gs::Set_RGBAQ( inDC, 0xFF000000 );
	Gs_ApplyPass_CH1( inDC, inSizeX, inSizeY, inToBAddr, 0, 0x00FFFFFF );

	// B- Flag des pixels (alpha=0) dans le range de Z souhait�
	// 0x49000000 correspond � ITOF4_pack = 524288.0F
	// (>>4) pour le mode XYZF2 !
	// 0x10000 pour couvrir les 16 bits bas
	uint32	zFTOI4_EXP	= ((0x49000000&0x0FFFFFF0)>>4);
	uint32	ZMin		= zFTOI4_EXP | 0x10000;
	gs::Set_TEST_2( inDC, 1, 1, 0, 0, 0, 0, 1, SCE_GS_ZGREATER );
	gs::Set_RGBAQ( inDC, 0 );
	Gs_ApplyPass_CH1( inDC, inSizeX, inSizeY, inToBAddr, 0, 0x00FFFFFF, ZMin );

	// C- Copie Z[15-8] -> Back[alpha] avec destination alpha test (equal to 0 pass)
	//    PSM16 GR->AB (5 bits of G, 3 bits of R -> A)
	uint h_psm16 = inSizeY << 1;
	gs::Set_TEST_2( inDC, 1, 1, 0, 0, 1, 0, 1, SCE_GS_ZALWAYS );	// Dest alpha test on !
	gs::Set_XYOFFSET_2( inDC, 0, 0 );
	gs::Set_SCISSOR_2( inDC, 0, inSizeX-1, 0, h_psm16-1 );			// PSM16 (w,h*2) window
	gs::SetFrame_2( inDC, inToBAddr>>5, inSizeX, SCE_GS_PSMCT16, 0x00003FFF );
	gs::Set_PRIM( inDC, 6, 0, 1, 0, 0, 0, 1, 1, 0 );				// TME/UV/CTXT2
	gs::Set_TEXA( inDC, 0x0, 0x0, 0x80 );
	if( h_psm16 < 1024 )
	{
		gs::SetTexture_2( inDC, inFromBAddr, inSizeX, h_psm16, SCE_GS_PSMZ16, SCE_GS_DECAL, 0 );
		for( int x = 0 ; x < inSizeX ; x += 16 ) {
			gs::Set_UVic	( inDC,	x,		0				);
			gs::Set_XYZ2i	( inDC,	x+8,	0,			0	);
			gs::Set_UVic	( inDC,	x+8,	h_psm16			);			// nearest copy 1:1
			gs::Set_XYZ2i	( inDC,	x+16,	h_psm16,	0	);
		}
	}
	else
	{
		// Draw in two parts to prevent Set_UVic() 10.4 overflow !
		// upper part (0 -> h_psm16/2)
		gs::SetTexture_2( inDC, inFromBAddr, inSizeX, inSizeY, SCE_GS_PSMZ16, SCE_GS_DECAL, 0 );
		for( int x = 0 ; x < inSizeX ; x += 16 ) {
			gs::Set_UVic	( inDC,	x,		0				);
			gs::Set_XYZ2i	( inDC,	x+8,	0,			0	);
			gs::Set_UVic	( inDC,	x+8,	inSizeY			);			// nearest copy 1:1
			gs::Set_XYZ2i	( inDC,	x+16,	inSizeY,	0	);
		}
		// lower part (h_psm16/2 -> h_psm16)
		uint midBAddr = inFromBAddr + (GetBlockSize(SCE_GS_PSMCT32,inSizeX,inSizeY) >> 1);
		gs::SetTexture_2( inDC, midBAddr, inSizeX, inSizeY, SCE_GS_PSMZ16, SCE_GS_DECAL, 0 );
		for( int x = 0 ; x < inSizeX ; x += 16 ) {
			gs::Set_UVic	( inDC,	x,		0				);
			gs::Set_XYZ2i	( inDC,	x+8,	inSizeY,	0	);
			gs::Set_UVic	( inDC,	x+8,	inSizeY			);			// nearest copy 1:1
			gs::Set_XYZ2i	( inDC,	x+16,	h_psm16,	0	);
		}
	}
	gs::Set_TEST_2( inDC, 1, 1, 0, 0, 0, 0, 1, SCE_GS_ZALWAYS );
	SetADReg( inDC, SCE_GS_TEXA, gs::frame::DefContext.texa );
}



void
gs::tools::Gs_RGBAToDepthPass_CH1(	dmac::Cursor*			inDC,
									int						inSizeX,
									int						inSizeY,
									int						inFromBAddr,
									int						inToBAddr,
									uint					inFlags,
									uint32					inFBMSK,
									uint32					inZ				)
{
	NV_ASSERT( (inToBAddr&31) == 0 );

	gs::SetTexture_2( inDC, inFromBAddr, inSizeX, inSizeY, SCE_GS_PSMCT32,
							(inFlags&PF_MODULATE) ? SCE_GS_MODULATE : SCE_GS_DECAL,
							0 	);	// NEAREST

	gs::SetFrame_2( inDC, inToBAddr>>5, inSizeX, SCE_GS_PSMZ32, inFBMSK );
	if( !(inFlags&PF_NOSCISSOR) )
		gs::Set_SCISSOR_2( inDC, 0, inSizeX-1, 0, inSizeY-1 );
	gs::Set_PRIM( inDC, 6, 0, 1, 0, (inFlags&PF_ABE)?1:0, 0, 1, 1, 0 );		// TME/[ABE]/UV/CTXT2
	gs::Set_XYOFFSET_2( inDC, 0, 0 );

	Vec4 reg( 0, 0, inSizeX, inSizeY );
	Gs_DrawSpritePass_CH1( inDC, &reg, &reg, inZ );
}



void
gs::tools::Gs_GenerateHStripes_CH1	(	dmac::Cursor*			inDC,
										uint					inStep,
										int						inSizeX,
										int						inSizeY,
										int						inBAddr,
										uint32					inFBMSK		)
{
	NV_ASSERT( (inBAddr&31) == 0 );

	// all white
	gs::Set_RGBAQ( inDC, 0xFFFFFF );
	gs::SetFrame_2( inDC, inBAddr>>5, inSizeX, SCE_GS_PSMCT32, inFBMSK );
	gs::Set_XYOFFSET_2( inDC, 0, 0 );
	gs::Set_SCISSOR_2( inDC, 0, inSizeX-1, 0, inSizeY-1 );
	gs::Set_PRIM( inDC, 6, 0, 0, 0, 0, 0, 1, 1, 0 );		// UV/CTXT2
	gs::Set_XYZ2i( inDC, 0,		  0,	   0 );
	gs::Set_XYZ2i( inDC, inSizeX, inSizeY, 0 );

	// black
	gs::Set_RGBAQ( inDC, 0 );
	for( int y = inStep ; y < inSizeY ; y += 2*inStep ) {
		gs::Set_XYZ2i( inDC, 0,		  y,		0 );
		gs::Set_XYZ2i( inDC, inSizeX, y+inStep,	0 );
	}

	// First is red
	gs::Set_RGBAQ( inDC, 0x000000FF );
	gs::Set_XYZ2i( inDC, 0,		  0,		0 );
	gs::Set_XYZ2i( inDC, inSizeX, inStep,	0 );

	// Last is blue
	gs::Set_RGBAQ( inDC, 0x00FF0000 );
	gs::Set_XYZ2i( inDC, 0,			inSizeY-inStep,	0	);
	gs::Set_XYZ2i( inDC, inSizeX,	inSizeY,		0	);
}



void
gs::tools::Gs_GenerateVStripes_CH1	(	dmac::Cursor*			inDC,
										uint					inStep,
										int						inSizeX,
										int						inSizeY,
										int						inBAddr,
										uint32					inFBMSK		)
{
	NV_ASSERT( (inBAddr&31) == 0 );

	// all white
	gs::Set_RGBAQ( inDC, 0xFFFFFF );
	gs::SetFrame_2( inDC, inBAddr>>5, inSizeX, SCE_GS_PSMCT32, inFBMSK );
	gs::Set_XYOFFSET_2( inDC, 0, 0 );
	gs::Set_SCISSOR_2( inDC, 0, inSizeX-1, 0, inSizeY-1 );
	gs::Set_PRIM( inDC, 6, 0, 0, 0, 0, 0, 1, 1, 0 );		// UV/CTXT2
	gs::Set_XYZ2i( inDC, 0,			0,			0 );
	gs::Set_XYZ2i( inDC, inSizeX,	inSizeY,	0 );

	// black, first is red, last is blue
	gs::Set_RGBAQ( inDC, 0 );
	for( int x = inStep ; x < inSizeX ; x += 2*inStep ) {
		gs::Set_XYZ2i( inDC, x,			0,			0	);
		gs::Set_XYZ2i( inDC, x+inStep,	inSizeY,	0	);
	}

	// First is red
	gs::Set_RGBAQ( inDC, 0x000000FF );
	gs::Set_XYZ2i( inDC, 0, 		0,				0	);
	gs::Set_XYZ2i( inDC, inStep,	inSizeY,		0	);

	// Last is blue
	gs::Set_RGBAQ( inDC, 0x00FF0000 );
	gs::Set_XYZ2i( inDC, inSizeX-inStep,	0,			0	);
	gs::Set_XYZ2i( inDC, inSizeX,			inSizeY,	0	);
}


