/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <sifdev.h>
#include <sifrpc.h>
#include <libvifpk.h>
using namespace nv;


void
frame::GsContext::Init	(	uint	inPRIM	)
{
	NV_ASSERT_A32( giftag );
	if( !inPRIM )	inPRIM = SCE_GS_SET_PRIM(4,1,0,0,0,0,0,0,0);
	uint64 tag = SCE_GIF_SET_PACKED_TAG( 13, 1, 1, inPRIM, 1 );
	cpy64to32( giftag, (uint64*)&tag );
	__0x0e		= SCE_GS_A_D;

	cpy64to32( frame, (uint64*)&gs::frame::DefContext.frame );
	__0x4c		= SCE_GS_FRAME_1;

	cpy64to32( xyoffset, (uint64*)&gs::frame::DefContext.xyoffset );
	__0x18		= SCE_GS_XYOFFSET_1;

	cpy64to32( scissor, (uint64*)&gs::frame::DefContext.scissor );
	__0x40		= SCE_GS_SCISSOR_1;

	cpy64to32( zbuf, (uint64*)&gs::frame::DefContext.zbuf );
	__0x4e		= SCE_GS_ZBUF_1;

	cpy64to32( test, (uint64*)&gs::frame::DefContext.test );
	__0x47		= SCE_GS_TEST_1;

	cpy64to32( alpha, (uint64*)&gs::frame::DefContext.alpha );
	__0x42		= SCE_GS_ALPHA_1;

	cpy64to32( colclamp, (uint64*)&gs::frame::DefContext.colclamp );
	__0x46		= SCE_GS_COLCLAMP;

	// Default is no texturing !

	texflush[0]	= 0;
	texflush[1]	= 0;
	__0x3f		= SCE_GS_NOP;		// SCE_GS_TEXFLUSH;

	cpy64to32( tex1, (uint64*)&gs::frame::DefContext.tex1 );
	__0x14		= SCE_GS_NOP;		// SCE_GS_TEX1_1;

	cpy64to32( tex0, (uint64*)&gs::frame::DefContext.tex0 );
	__0x06		= SCE_GS_NOP;		// SCE_GS_TEX0_1;

	cpy64to32( mip1, (uint64*)&gs::frame::DefContext.mip1 );
	__0x34		= SCE_GS_NOP;		// SCE_GS_MIPTBP1_1;

	cpy64to32( mip2, (uint64*)&gs::frame::DefContext.mip2 );
	__0x36		= SCE_GS_NOP;		// SCE_GS_MIPTBP2_1;

	cpy64to32( clamp, (uint64*)&gs::frame::DefContext.clamp );
	__0x08		= SCE_GS_NOP;		// SCE_GS_CLAMP_1;

	Vec2Zero( &stOffset );

	owner = NULL;
}





