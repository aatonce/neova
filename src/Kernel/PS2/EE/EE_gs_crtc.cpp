/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
using namespace nv;

//
//	2-Lines Flicker-Filter :
//
//	FrontBuffer de X*Y*16, BackBuffer de X*Y*32, ZBuffer de X*Y*32.
//	Le backBuffer est recopi� en sprite avec un dithering ds le frontBuffer.
//	Les deux CRTCs sont utilis�s pour d�coder le frontBuffer :
//	- CRTC0 d�code normalement l'image en mode Field (+0,+2,... / +1,+3,...),
//	  � partir de la position (0,0) dans le FrontBuffer et � la position (0,0) � l'�cran
//	- CRTC1 d�code le m�me frontBuffer en mode Field mais � partir de la position (0,1)
//    (+1,+3,... / +2,+4,...) dans le frontBuffer et � la position (1/2,0) � l'�cran.
//	Les deux signaux sont mix�s � 50%.
//
//	Le r�sultat est :
//	- Supperposition des deux signaux avec un d�calage de +X=1/2 (layering vers la droite)
//  - Lignes r�ellement affich�es en mode Field sont :
//	  ligne �cran 0 :	+0 blend +1					= +0( 50%) +1( 50%)
//	  ligne �cran 1 :				+1 blend +2		=          +1( 50%) +2( 50%)
//	  ligne �cran 2 :	+2 blend +3				    =                   +2( 50%) +3( 50%)
//	  ligne �cran 3 :				+3 blend +4	    =                            +3( 50%) +4( 50%)
//	  ...				...							=					...
//	  ---------------------------------------------------------------------------------------...
//													= +0( 50%) +1(100%) +2(100%) +3(100%) +4(100%) ...
//
//	Remarque : La derni�re ligne est incorrecte et ne doit pas �tre affich�e.


#define	VCK_WIDTH		(2560)		// sub-pixels per line



void	nvcore_Clock_iUpdate	(	);



namespace
{

	int VSyncHandler( int ca )
	{
		if( ca == INTC_VBLANK_S ) {
			gs::crtc::vsyncCpt++;
			// Prevent clock overflow
			nvcore_Clock_iUpdate();
		}
		ExitHandler();
		return -1;
	}


	struct Context {
		tGS_PMODE			pmode;
		tGS_SMODE2			smode2;
		tGS_BGCOLOR			bgcolor;
		tGS_DISPFB1			dispfb1;
		tGS_DISPLAY1		display1;
		tGS_DISPFB2			dispfb2;
		tGS_DISPLAY2		display2;
		tGS_EXTBUF			extbuf;
		tGS_EXTDATA			extdata;
		tGS_IMR				imr;
	};

	ALIGNED64( Context, ctxt0 );
	uint				vsync0Field;	// CSR.FIELD. 0=EVEN 1=ODD

}



volatile uint32		gs::crtc::vsyncCpt;
uint32				gs::crtc::vsyncRate;



void
gs_crtc_Reset( bool	inEnable )
{
	Zero( ctxt0.bgcolor );

	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	if( !consoleOnly ) {
		*GS_PMODE		= inEnable ? AS_UINT64( ctxt0.pmode	) : 0;
		*GS_SMODE2		= AS_UINT64( ctxt0.smode2	);
		*GS_BGCOLOR		= AS_UINT64( ctxt0.bgcolor	);
		*GS_DISPFB1		= AS_UINT64( ctxt0.dispfb1	);
		*GS_DISPLAY1	= AS_UINT64( ctxt0.display1	);
		*GS_DISPFB2		= AS_UINT64( ctxt0.dispfb2	);
		*GS_DISPLAY2	= AS_UINT64( ctxt0.display2	);
	}

	sceGsSyncVCallback( VSyncHandler );
	*GS_IMR = ~(1<<11);	// VSync intr only !

	gs::crtc::vsyncCpt  = 0;
	vsync0Field = sceGsSyncV(0)^1;
}



void
gs_crtc_Init( bool	inEnable )
{
	uint32 vmode, dpyW, dpyH;
	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_EE_VMODE, &vmode );
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );

	// display back buffer ?
	sceGsFrame* dpyFrame = NULL;
	if( vmode & gs::VM_SHOWBACK ) {
		dpyFrame = (sceGsFrame*) &gs::frame::BackFrameReg;
		core::GetParameter( nv::core::PR_BACK_FRAME_W, &dpyW );
		core::GetParameter( nv::core::PR_BACK_FRAME_H, &dpyH );
	} else {
		dpyFrame = (sceGsFrame*) &gs::frame::FrontFrameReg;
		core::GetParameter( nv::core::PR_FRONT_FRAME_W, &dpyW );
		core::GetParameter( nv::core::PR_FRONT_FRAME_H, &dpyH );
	}

	// EE-Reference-Graphics.pdf says :
	// Because settings may differ as a function of GS chip version, this functions
	// should be used for initializing the GS, otherwise display problems may occur.
	if( !consoleOnly ) {
		sceGsResetGraph (	0,
							SCE_GS_INTERLACE,
							(vmode&gs::VM_PAL)   ? SCE_GS_PAL   : SCE_GS_NTSC,
							(vmode&gs::VM_FIELD) ? SCE_GS_FIELD : SCE_GS_FRAME );
	}

	Zero( ctxt0 );

	// PMODE
	ctxt0.pmode.EN1		= 1;
	ctxt0.pmode.EN2		= 1;
	ctxt0.pmode.CRTMD	= 0;
	ctxt0.pmode.MMOD	= 1;
	ctxt0.pmode.AMOD	= 0;
	ctxt0.pmode.SLBG	= 0;
	ctxt0.pmode.ALP		= 0xFF;		// 100%

	// SMODE2
	ctxt0.smode2.INT	= 1;
	ctxt0.smode2.FFMD	= (vmode & gs::VM_FIELD) ? SCE_GS_FIELD : SCE_GS_FRAME;

	// DISPFB1 & 2
	ctxt0.dispfb1.FBP	= dpyFrame->FBP;
	ctxt0.dispfb1.FBW	= dpyFrame->FBW;
	ctxt0.dispfb1.PSM	= dpyFrame->PSM;
	AS_UINT64( ctxt0.dispfb2 ) = AS_UINT64( ctxt0.dispfb1 );

	// DISPLAY1 & 2
	if( vmode & gs::VM_AFLICKER )	dpyH  -= 1;		// disable last incorrect blended line !
	if( !(vmode & gs::VM_FIELD) )	dpyH <<= 1;
	ctxt0.display1.MAGH	= (VCK_WIDTH+dpyW-1)/dpyW - 1;
	ctxt0.display1.MAGV	= 0;	// => 1
	ctxt0.display1.DW	= VCK_WIDTH-1;
	ctxt0.display1.DH	= dpyH-1;
	ctxt0.display1.DX	= (vmode & gs::VM_PAL) ? 0x290 : 0x27c;
	ctxt0.display1.DY	= (vmode & gs::VM_PAL) ? 72 : 50;
	ctxt0.display1.DY  += (((vmode & gs::VM_PAL) ? 512:448) - dpyH) >> 1;
	AS_UINT64( ctxt0.display2 ) = AS_UINT64( ctxt0.display1 );

	// 2-CRTC anti-flicker ?
	if( vmode & gs::VM_AFLICKER ) {
		ctxt0.dispfb2.DBY  += 1;
		ctxt0.pmode.ALP		= 0x80;	// 50%
	}

	gs::crtc::vsyncRate = (vmode & gs::VM_PAL) ? 50 : 60;

	gs_crtc_Reset( inEnable );
}



void
gs_crtc_Shut()
{
	sceGsSyncVCallback( NULL );
}



void
gs::crtc::Enable()
{
	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	if( !consoleOnly )
		*GS_PMODE = AS_UINT64( ctxt0.pmode );
}



void
gs::crtc::Disable()
{
	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	if( !consoleOnly )
		*GS_PMODE = 0;
}



bool
gs::crtc::IsEvenFrame	(					)
{
	return ((vsyncCpt^vsync0Field)&1) == 0;
//	return ( ((*GS_CSR)&(1<<13)) == 0 );
}


void
gs::crtc::Translate(	int		inX0,	int		inY0 )
{
	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	if( !consoleOnly ) {
		tGS_DISPLAY1 display1;
		tGS_DISPLAY2 display2;
		Memcpy( &display1, &ctxt0.display1, sizeof(display1) );
		Memcpy( &display2, &ctxt0.display2, sizeof(display2) );
		display1.DX += inX0 * display1.MAGH;
		display1.DY += inX0;
		display2.DX += inX0 * display2.MAGH;
		display2.DY += inX0;
		*GS_DISPLAY1 = AS_UINT64( display1 );
		*GS_DISPLAY2 = AS_UINT64( display2 );
	}
}


void
gs::crtc::VSync()
{
	uint32 curVSCpt = vsyncCpt;
	while( vsyncCpt == curVSCpt )	{}
}


uint
gs::crtc::SetBGColor( uint32 inRGB )
{
#if defined( _NVCOMP_ENABLE_DBG )
	uint prev = GetBGColor();
	bool bgcolor_rw;
	if( core::GetParameter(nv::core::PR_BGCOLOR_RW,&bgcolor_rw) && bgcolor_rw ) {
		ctxt0.bgcolor.R = (inRGB>>16) & 0xFF;
		ctxt0.bgcolor.G = (inRGB>>8)  & 0xFF;
		ctxt0.bgcolor.B = (inRGB>>0)  & 0xFF;
		*GS_BGCOLOR = AS_UINT64( ctxt0.bgcolor );
	}
	return prev;
#else
	return 0;
#endif
}


uint
gs::crtc::GetBGColor(  )
{
#ifdef _MASTER
	return 0;
#else
	return		(uint(ctxt0.bgcolor.R)<<16)
			|	(uint(ctxt0.bgcolor.G)<<8)
			|	(uint(ctxt0.bgcolor.B)<<0);
#endif
}


