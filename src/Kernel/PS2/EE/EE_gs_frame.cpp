/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
using namespace nv;
using namespace gs::tools;



uint32		gs::frame::PhysicalSizeX;
uint32		gs::frame::PhysicalSizeY;
uint32		gs::frame::VirtualSizeX;
uint32		gs::frame::VirtualSizeY;
uint32		gs::frame::SizeX;
uint32		gs::frame::SizeY;
uint32		gs::frame::SizeX2;
uint32		gs::frame::SizeY2;
uint32		gs::frame::FrontBAddr;
uint32		gs::frame::BackBAddr;
uint32		gs::frame::DepthBAddr;
uint32		gs::frame::VRamLowerBAddr;
uint32		gs::frame::VRamUpperBAddr;
uint64		gs::frame::FrontFrameReg;
uint64		gs::frame::BackFrameReg;
uint64		gs::frame::DepthFrameReg;
uint64		gs::frame::DepthZBufReg;
gs::Context	gs::frame::DefContext;






void
gs_frame_Reset()
{
	uint32 vmode, frontW, frontH, backW, backH;
	core::GetParameter( nv::core::PR_EE_VMODE, &vmode );
	core::GetParameter( nv::core::PR_FRONT_FRAME_W, &frontW );
	core::GetParameter( nv::core::PR_FRONT_FRAME_H, &frontH );
	core::GetParameter( nv::core::PR_BACK_FRAME_W, &backW  );
	core::GetParameter( nv::core::PR_BACK_FRAME_H, &backH  );

	gs::frame::PhysicalSizeX	= frontW;
	gs::frame::PhysicalSizeY	= frontH;
	gs::frame::VirtualSizeX		= (frontW==512) ? 640 : frontW;
	gs::frame::VirtualSizeY		= (vmode&gs::VM_FIELD) ? frontH : frontH*2;
	gs::frame::SizeX			= backW;
	gs::frame::SizeY			= backH;
	gs::frame::SizeX2			= GetCeilPow2( gs::frame::SizeX );
	gs::frame::SizeY2			= GetCeilPow2( gs::frame::SizeY );
	gs::frame::FrontBAddr		= 0;
	gs::frame::BackBAddr		= (vmode&gs::VM_PSM16) ? gs::tools::GetPSM16_ABSize(frontW,frontH) : gs::tools::GetPSM32_ABSize(frontW,frontH);
	gs::frame::DepthBAddr		= gs::frame::BackBAddr  + gs::tools::GetPSM32_ABSize( backW, backH );
	gs::frame::VRamLowerBAddr	= gs::frame::DepthBAddr + gs::tools::GetPSM32_ABSize( backW, backH );
	gs::frame::VRamUpperBAddr	= 512*32;

	gs::frame::FrontFrameReg	= SCE_GS_SET_FRAME(	gs::frame::FrontBAddr/32,
													(frontW+63)/64,
													(vmode&gs::VM_PSM16) ? SCE_GS_PSMCT16S : SCE_GS_PSMCT32,
													0	);

	gs::frame::BackFrameReg		= SCE_GS_SET_FRAME(	gs::frame::BackBAddr/32,
													(backW+63)/64,
													SCE_GS_PSMCT32,
													0	);

	gs::frame::DepthFrameReg	= SCE_GS_SET_FRAME(	gs::frame::DepthBAddr/32,
													(backW+63)/64,
													SCE_GS_PSMZ32,
													0	);

	gs::frame::DepthZBufReg		= SCE_GS_SET_ZBUF(	gs::frame::DepthBAddr/32,
													(SCE_GS_PSMZ32-SCE_GS_PSMZ32),
													0	);

	// Default context :
	// ----------------
	// Centered screen
	// ZBuff in > test mode
	// Texturing : bilinear, repeat
	// Color clamping in [0,255]
	// Back/Deph buffer setup
	// PunchThrough, Blending disabled
	// PRMODE disabled

	gs::frame::DefContext.texflush		= 0;
	gs::frame::DefContext.xyoffset		= SCE_GS_SET_XYOFFSET( ((2048-backW/2)<<4), ((2048-backH/2)<<4) ),
	gs::frame::DefContext.tex2			= SCE_GS_SET_TEX2( 0, 0, 0, 0, 0, 0 );
	gs::frame::DefContext.tex1			= SCE_GS_SET_TEX1( 0, 0, 1, 1, 0, 0, 0 );
	gs::frame::DefContext.tex0			= SCE_GS_SET_TEX0( 0, 0, 0, 0, 0, 1, SCE_GS_MODULATE, 0, 0, 0, 0, 0 );
	gs::frame::DefContext.clamp			= SCE_GS_SET_CLAMP( 0, 0, 0, 0, 0, 0 );
	gs::frame::DefContext.mip1			= SCE_GS_SET_MIPTBP1( 0, 0, 0, 0, 0, 0 );
	gs::frame::DefContext.mip2			= SCE_GS_SET_MIPTBP2( 0, 0, 0, 0, 0, 0 );
	gs::frame::DefContext.scissor		= SCE_GS_SET_SCISSOR( 0, backW-1, 0, backH-1 );
	gs::frame::DefContext.alpha			= SCE_GS_SET_ALPHA( 0, 1, 0, 1, 80 );
	gs::frame::DefContext.test			= SCE_GS_SET_TEST(1, 5, 64, 0, 0, 0, 1, SCE_GS_ZGEQUAL );
	gs::frame::DefContext.fba			= SCE_GS_SET_FBA( 0 );
	gs::frame::DefContext.frame			= gs::frame::BackFrameReg;
	gs::frame::DefContext.zbuf			= gs::frame::DepthZBufReg;
	gs::frame::DefContext.texa			= SCE_GS_SET_TEXA( 0x80, 1, 0x80 );		// TA0=0x80 for RGB24=0 !
	gs::frame::DefContext.fogcol		= SCE_GS_SET_FOGCOL( 54, 54, 80 );
	gs::frame::DefContext.dthe			= SCE_GS_SET_DTHE( 0 );
	gs::frame::DefContext.dimx			= SCE_GS_SET_DIMX( 4, 2, 5, 3, 0, 6, 1, 7, 5, 3, 4, 2, 1, 7, 0, 6 );
	gs::frame::DefContext.pabe			= SCE_GS_SET_PABE( 0 );
	gs::frame::DefContext.colclamp		= SCE_GS_SET_COLCLAMP( 1 );
	gs::frame::DefContext.prim			= SCE_GS_SET_PRIM( 4, 1, 1, 1, 0, 0, 0, 0, 0 );
	gs::frame::DefContext.prmode		= SCE_GS_SET_PRMODE( 1, 1, 1, 0, 0, 0, 0, 0 );
	gs::frame::DefContext.prmodecont	= SCE_GS_SET_PRMODECONT( 1 );
}


void
gs_frame_Init()
{
	gs_frame_Reset();
}


void
gs_frame_Shut()
{
	//
}


void
gs::frame::PutBack24_CH1	(	dmac::Cursor *	inDC,
								uint128*		inData	)
{
	gs::tools::Gs_TrxPass_CH1( inDC, SizeX, SizeY, SCE_GS_PSMCT24, BackBAddr, inData );
}



void
gs::frame::PutBack32_CH1	(	dmac::Cursor *	inDC,
								uint128*		inData	)
{
	gs::tools::Gs_TrxPass_CH1( inDC, SizeX, SizeY, SCE_GS_PSMCT32, BackBAddr, inData );
}




void
gs::frame::GetBack_CH1		(	uint128*		inBuffer	)
{
	gs::tools::Gs_GrabPass_CH1( SizeX, SizeY, SCE_GS_PSMCT32, BackBAddr, inBuffer );
}



void
gs::frame::GetDepth_CH1		(	uint128*		inBuffer	)
{
	gs::tools::Gs_GrabPass_CH1( SizeX, SizeY, SCE_GS_PSMZ32, DepthBAddr, inBuffer );
}






//
// Frame present


void
gs::frame::PresentMod::Translate		(	uint32		inD			)
{
	AS_UINT32( bgr )		+= inD;
	AS_UINT32( opacity )	+= inD;
	AS_UINT32( uv0 )		+= inD;
}

void
gs::frame::PresentMod::SetColor		(	uint32		inBGR	)
{
	*bgr = inBGR;
}

void
gs::frame::PresentMod::SetOpacity		(	float		inOpacity	)
{
	if( inOpacity > 0.99f )			*opacity = 0x80;
	else if( inOpacity <= 0.0f )	*opacity = 0;
	else							*opacity = int( inOpacity * 128.0f );
}

void
gs::frame::PresentMod::SetEvenFrame	(		)
{
	// no vertical smoothing -> return
	if( PhysicalSizeY == SizeY )
		return;
	// +0,+2,+4,...
	uint v0 = (0<<4)     + 8;
	uint v1 = (SizeY<<4) + 8;
	uint i  = 0;
	for( uint32 x = 0 ; x < PhysicalSizeX ; x+=32,i+=32 ) {
		uv0[ 1+i] = v0;
		uv0[17+i] = v1;
	}
}

void
gs::frame::PresentMod::SetOddFrame	(		)
{
	// no vertical smoothing -> return
	if( PhysicalSizeY == SizeY )
		return;
	// +1,+3,+5,...
	uint v0 = (1<<4)         + 8;
	uint v1 = ((SizeY+1)<<4) + 8;
	uint i  = 0;
	for( uint32 x = 0 ; x < PhysicalSizeX ; x+=32,i+=32 ) {
		uv0[ 1+i] = v0;
		uv0[17+i] = v1;
	}
}



void
gs::frame::Present_CH1	(	dmac::Cursor*		inDC,
							PresentMod&			outMod		)
{
	Gs_Open_CH1( inDC );

	uint32 vmode;
	core::GetParameter( nv::core::PR_EE_VMODE, &vmode );

	// front PSM16 dithering ?
	if( vmode & gs::VM_PSM16 ) {
		gs::Set_DIMX( inDC, 4, 2, 5, 3, 0, 6, 1, 7, 5, 3, 4, 2, 1, 7, 0, 6 );
		gs::Enable_DTHE( inDC );
	}

	// Smoothing ?
	gs::SetTexture_2( inDC, BackBAddr, SizeX, SizeY, SCE_GS_PSMCT32, SCE_GS_MODULATE, SCE_GS_LINEAR );
	*(inDC->i64++) = FrontFrameReg;
	*(inDC->i64++) = SCE_GS_FRAME_2;
	gs::Set_SCISSOR_2( inDC, 0, PhysicalSizeX-1, 0, PhysicalSizeY-1 );

	outMod.opacity = inDC->i8 + 4;
	gs::Set_ALPHA_2( inDC, 0, 1, 2, 1, 0x80/*fix*/ );

	outMod.bgr = inDC->i32;
	gs::Set_RGBAQ( inDC, 0x00808080 );

	gs::Set_PRIM( inDC, 6, 0, 1, 0, 1, 0, 1, 1, 0 );	// ABE

	outMod.uv0 = inDC->i16;
	float iu = float(int32(SizeX*32)) / float(int32(PhysicalSizeX));
	float u  = 0;
	for( uint32 x = 0 ; x < PhysicalSizeX ; x += 32, u += iu ) {
		gs::Set_UVf	  ( inDC, u+0.5f,		0.5f					);
		gs::Set_XYZ2i ( inDC, x,			0,					0	);
		gs::Set_UVf	  ( inDC, u+iu+0.5f,	float(SizeY)+0.5f		);
		gs::Set_XYZ2i ( inDC, x+32,			PhysicalSizeY,		0	);
	}

	// front PSM16 dithering ?
	if( vmode & gs::VM_PSM16 )
		gs::Disable_DTHE( inDC );

	Gs_Close_CH1( inDC );
}





//
// Frame clear


void
gs::frame::ClearMod::Translate	(	uint32		inD		)
{
	AS_UINT32( z )			+= inD;
	AS_UINT32( abgr )		+= inD;
	AS_UINT32( gsFrame )	+= inD;
	AS_UINT32( gsScissor )	+= inD;
	AS_UINT32( gsZBuf )		+= inD;
}

void
gs::frame::ClearMod::SetColor	(	uint32		inABGR	)
{
	NV_ASSERT_A64( abgr );
	*abgr = inABGR;
}

void
gs::frame::ClearMod::SetZ	(	uint32		inZ	)
{
	NV_ASSERT_A64( gsScissor );
	uint w = gsScissor->SCAX1+1;
	uint i = 0;
	for( uint32 x = 0 ; x < w ; x+=32, i+=8 ) {
		z[1+i] = inZ;
		z[5+i] = inZ;
	}
}

void
gs::frame::ClearMod::SetRect(	int	inX, int inY, int inW, int inH )
{
	int x0 = Clamp( inX, 0, int(SizeX) );
	int y0 = Clamp( inY, 0, int(SizeY) );
	int x1 = x0 + inW;	x1 = Min( x1, int(SizeX) );
	int y1 = y0 + inH;	y1 = Min( y1, int(SizeY) );

	NV_ASSERT_A64( gsScissor );
	gsScissor->SCAX0 = x0;
	gsScissor->SCAX1 = x1-1;
	gsScissor->SCAY0 = y0;
	gsScissor->SCAY1 = y1-1;

	// adjust dma code for GS speed
	int x32 = x0 & (~31);
	uint16 * xyz2 = (uint16*)(z-1);
	for( int x = 0 ; x < int(SizeX) ; x+=32,xyz2+=16 ) {
		xyz2[1]  = y0*16;
		xyz2[9]  = y1*16;
		// enable this sprite ?
		xyz2[4] = xyz2[12] = ( x >= x32 && x < x1 ) ? SCE_GS_XYZ2 : SCE_GS_NOP;
	}
}


void
gs::frame::ClearMod::SetFBMSK		(	uint32		inABGR		)
{
	NV_ASSERT_A64( gsFrame );
	gsFrame->FBMSK = inABGR;
}

void
gs::frame::ClearMod::SetZMSK		(	uint		inZ_0_1		)
{
	NV_ASSERT_A64( gsZBuf );
	gsZBuf->ZMSK = inZ_0_1 ? 1 :  0;
}


void
gs::frame::Clear_CH1(		dmac::Cursor*		inDC,
							ClearMod&			outMod			)
{
	Gs_Open_CH1( inDC );

	outMod.gsZBuf = (sceGsZbuf*) inDC->i64;
	gs::SetADReg( inDC, SCE_GS_ZBUF_2, DepthZBufReg );

	outMod.gsFrame = (sceGsFrame*) inDC->i64;
	gs::SetADReg( inDC, SCE_GS_FRAME_2, BackFrameReg );

	outMod.abgr = inDC->i32;
	gs::Set_RGBAQ( inDC, 0, 0 );

	outMod.gsScissor = (sceGsScissor*) inDC->i64;
	gs::Set_SCISSOR_2( inDC, 0, SizeX-1, 0, SizeY-1 );

	gs::Set_PRIM( inDC, 6, 0, 0, 0, 0, 0, 0, 1, 0 );

	outMod.z = inDC->i32+1;
	for( uint32 x = 0 ; x < SizeX ; x += 32 ) {
		gs::Set_XYZ2i( inDC, x,		0,		0 	);
		gs::Set_XYZ2i( inDC, x+32,	SizeY,	0	);
	}

	Gs_Close_CH1( inDC );
}




