/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
using namespace nv;






namespace
{

	struct TRXBlock {
		tram::TexTRX			trx[32];
		uint32					usageMask;		// bit[i]=1 <=> trx[i] is free !
	};
	sysvector<TRXBlock*>		trxBlockA;

	sysvector<tram::TexReg>		texRegA;
	int							texRegFirstFree;

	uint						PStart;
	uint						PSize;

}






//
//	Init/Reset

void
tram::Init(	uint32	inPStart )
{
	NV_ASSERTC( (inPStart+16)<512, "Not enought VRAM for texture buffers !" );
	if( (inPStart+16)>512 )
		inPStart = 512 - 16;

	PStart	= inPStart;
	PSize	= 512 - inPStart;

	trxBlockA.Init();
	texRegA.Init();

	Reset();
}



void
tram::Reset()
{
	texRegA.clear();
	texRegFirstFree = -1;

	uint N = trxBlockA.size();
	for( uint i = 0 ; i < N ; i++ )
		EngineFree( trxBlockA[i] );
	trxBlockA.clear();
}



void
tram::Shut()
{
	trxBlockA.Shut();
	texRegA.Shut();
}



uint
tram::GetPStart()
{
	return PStart;
}


uint
tram::GetPSize()
{
	return PSize;
}



//
// TexDesc Register / Release


int
tram::RegisterDesc	(	TexDesc*		inDesc		)
{
	if( !inDesc )
		return -1;

	// Valid ?
	NV_ASSERT( inDesc->psm == SCE_GS_PSMT4 || inDesc->psm == SCE_GS_PSMT8 || inDesc->psm == SCE_GS_PSMCT24 || inDesc->psm == SCE_GS_PSMCT32 );
	NV_ASSERT( inDesc->mipmapCpt   > 0 );
	NV_ASSERT( inDesc->L2_W        > 0 );
	NV_ASSERT( inDesc->L2_H 		> 0 );
	NV_ASSERT( inDesc->resBSize	> 0 );
	NV_ASSERT( inDesc->trxAllBSize > 0 );
	if(		(		inDesc->psm!=SCE_GS_PSMT4
				&&	inDesc->psm!=SCE_GS_PSMT8
				&&	inDesc->psm!=SCE_GS_PSMCT24
				&&	inDesc->psm!=SCE_GS_PSMCT32	)
		||	inDesc->mipmapCpt == 0
		||	inDesc->L2_W == 0
		||	inDesc->L2_H == 0
		||	inDesc->resBSize == 0
		||	inDesc->trxAllBSize == 0	)
		return -1;

	// No reg free ?
	if( texRegFirstFree == -1 )
	{
		uint N = texRegA.size();
		uint P = N+64;
		texRegA.resize( P );
		texRegFirstFree = N;
		for( uint i = N ; i < P ; i++ ) {
			texRegA[i].desc		 = NULL;
			texRegA[i].next_free = i+1;
		}
		texRegA[P-1].next_free = -1;
	}

	// Registration
	int id = texRegFirstFree;
	TexReg * reg = & texRegA[id];
	texRegFirstFree		= reg->next_free;
	reg->desc			= inDesc;
	reg->firstTRX		= NULL;
	reg->lastTRX		= NULL;
	reg->allBmpBSize	= 0;
	for( uint i = 0 ; i < 8 ; i++ )
		reg->allBmpBSize += inDesc->trx[i].bitmapBSize;

	// Check mipmaping ?
#if defined( _NVCOMP_ENABLE_DBG )
	uint32 checkMipMode;
	uint32 checkMipLods;
	core::GetParameter( core::PR_CHECK_MIPMAPPING_MODE, &checkMipMode );
	core::GetParameter( core::PR_CHECK_MIPMAPPING_LODS, &checkMipLods );
	if( checkMipMode )
		TestMipmaping( inDesc, checkMipMode-1, checkMipLods );
#endif

	return id;
}


tram::TexReg*
tram::GetRegistered		(	int		inTexId		)
{
	if( inTexId < 0 )
		return NULL;
	NV_ASSERT( inTexId < int(texRegA.size()) );
	if(	inTexId < int(texRegA.size()) )
		return & texRegA[ inTexId ];
	else
		return NULL;
}


int
tram::FindDesc		(	TexDesc*	inDesc		)
{
	if( !inDesc )
		return -1;

	uint N = texRegA.size();
	for( uint i = 0 ; i < N ; i++ ) {
		if( texRegA[i].desc == inDesc )
			return int(i);
	}

	return -1;
}


void
tram::ReleaseDesc	(	int	inTexId	)
{
	if( inTexId >= 0 )
	{
		NV_ASSERT( inTexId < int(texRegA.size()) );
		NV_ASSERT( texRegA[ inTexId ].desc  );
		if(		inTexId < int(texRegA.size())
			&&	texRegA[ inTexId ].desc	)
		{
			TexReg* reg = & texRegA[ inTexId ];
			reg->desc		= NULL;
			reg->next_free	= texRegFirstFree;
			texRegFirstFree = inTexId;

			// Free managed TRX
			TexTRX* trx = reg->firstTRX;
			while( trx ) {
				TexTRX* next = trx->next;
				ReleaseTRX( trx );
				trx = next;
			}
		}
	}
}


tram::TexReg*
tram::GetRegistrationA	(	uint*		outSize,
							int*		outFirstFree	)
{
	if( outSize )
		*outSize = texRegA.size();

	if( outFirstFree )
		*outFirstFree = texRegFirstFree;

	return texRegA.data();
}



//
// TexTRX Allocate / Free

tram::TexTRX*
tram::AllocateTRX	(		)
{
	uint N = trxBlockA.size();
	for( uint i = 0 ; i < N ; i++ )
	{
		TRXBlock * b = trxBlockA[i];
		NV_ASSERT( b );
		if( b->usageMask != 0 )
		{
			uint bNo = GetLowestBitNo(b->usageMask);
			b->usageMask ^= 1<<bNo;

			TexTRX* trx = & b->trx[bNo];
			trx->next = NULL;
			return trx;
		}
	}

	TRXBlock * b = (TRXBlock*) EngineMallocA( sizeof(TRXBlock), 16 );
	b->usageMask = -2U;	// 0b11...1110
	trxBlockA.push_back( b );

	TexTRX* trx = & b->trx[0];
	trx->next = NULL;
	return trx;
}


void
tram::ReleaseTRX	(	TexTRX*		inTRX		)
{
	if( !inTRX )
		return;

	uint N = trxBlockA.size();
	for( uint i = 0 ; i < N ; i++ )
	{
		TRXBlock * b = trxBlockA[i];
		NV_ASSERT( b );
		if( inTRX >= &b->trx[0] && inTRX <= &b->trx[31] )
		{
			uint bNo = inTRX - &b->trx[0];
			NV_ASSERT( bNo>=0 && bNo<=31 );
			b->usageMask |= 1<<bNo;
			if( b->usageMask == ~0U )
				trxBlockA.erase( trxBlockA.begin()+i );
			break;
		}
	}
}


tram::TexTRX*
tram::AllocateManagedTRX	(	int			inTexId		)
{
	TexReg* reg = GetRegistered( inTexId );
	if( !reg )
		return NULL;

	TexTRX* trx = AllocateTRX();
	if( !trx )
		return NULL;

	// init
	trx->Init( reg->desc );

	// append
	if( reg->lastTRX )
		reg->lastTRX->next = trx;
	else
		reg->firstTRX = trx;
	reg->lastTRX = trx;

	return trx;
}




bool
tram::TexTRX::Init	(	TexDesc *	inDesc	)
{
	if( !inDesc )
		return FALSE;

	baddr		= 0;
	allLodMask	= (1<<inDesc->mipmapCpt) - 1;
	lodMask		= (1<<inDesc->mipmapCpt) - 1;
	NV_ASSERT( lodMask );

	// Clut + up to 7 Mipmaps

	uint16 DBP[8];
	DBP[0] = baddr;							// clut
	DBP[1] = DBP[0] + inDesc->trxBSize[0];	// mip0
	DBP[2] = DBP[1] + inDesc->trxBSize[1];	// ...
	DBP[3] = DBP[2] + inDesc->trxBSize[2];
	DBP[4] = DBP[3] + inDesc->trxBSize[3];
	DBP[5] = DBP[4] + inDesc->trxBSize[4];
	DBP[6] = DBP[5] + inDesc->trxBSize[5];
	DBP[7] = DBP[6] + inDesc->trxBSize[6];

	// Setup DMA CODE (PATH 3, TTE=0)

	bool trx_called = (inDesc->flags & TexDesc::F_DMA_CALLED) != 0;

	uint32* dc = tadr;
	for( uint i = 0 ; i < 8 ; i++ ) {
		// Mipmap available ?
		if( inDesc->trxBSize[i] )
		{
			NV_ASSERT_A128( inDesc->trx[i].dmaData );
			NV_ASSERT( inDesc->trx[i].dmaQSize );

			// Upload BITBLTBUF REG (3QW)
			AS_UINT64( dc[0] )	= DMA_TAG_CNT | 2;
			AS_UINT64( dc[2] )	= 0;
			AS_UINT64( dc[4] )	= SCE_GIF_SET_PACKED_TAG( 1, 0, 0, 0, 1 );	// EOP=0
			AS_UINT64( dc[6] )	= 0xE;										// A+D on PATH 3 : EE User Manual 7.3.2 -> may cause the GS to hang on rare occasion ! Really ?
			AS_UINT32( dc[8] )	= inDesc->trx[i].BITBLTBUF[0];
			AS_UINT32( dc[9] )	= inDesc->trx[i].BITBLTBUF[1] | uint32(DBP[i]);
			AS_UINT64( dc[10] )	= SCE_GS_BITBLTBUF;

			// Upload Bitmap (1QW)
			AS_UINT32( dc[12] )	= trx_called ? (DMA_TAG_CALL|0) : (DMA_TAG_REF|(inDesc->trx[i].dmaQSize));
			AS_UINT32( dc[13] )	= DMA_ADDR(inDesc->trx[i].dmaData);
			AS_UINT64( dc[14] )	= 0;
		}
		else
		{
			// skip ...
			AS_UINT32( dc[0] )	= DMA_TAG_NEXT | 0;
			AS_UINT32( dc[1] )	= DMA_ADDR(dc+16);
			AS_UINT64( dc[2] )	= 0;
		}
		dc += 16;
	}

	// Link (also set EOP)
	link = dc;
	AS_UINT64( dc[0] )	= DMA_TAG_END | 1;		// TTE=0 -> trx 1QW for GIF_TAG !
	AS_UINT64( dc[2] )	= 0;
	AS_UINT64( dc[4] )	= SCE_GIF_SET_PACKED_TAG( 0, 1/*EOP*/, 0, 0, 0 );
	AS_UINT64( dc[6] )	= 0;

	AS_UINT64(TEX0) = SCE_GS_SET_TEX0(		DBP[1],
											inDesc->trx[1].TBW,
											inDesc->psm,
											inDesc->L2_W,
											inDesc->L2_H,
											1,						// RGBA (TEXA is used for no-alpha PSM) !
											SCE_GS_MODULATE,		// overwritten !
											DBP[0],
											0, 0, 0, 1 );

	AS_UINT64(TEX1) = SCE_GS_SET_TEX1(		0,
											inDesc->mipmapCpt-1,
											1,						// overwritten !
											4,						// overwritten !
											0,
											2,						// overwritten !
											0	);					// overwritten !

	AS_UINT64(MIP1) = SCE_GS_SET_MIPTBP1(	DBP[2],
											inDesc->trx[2].TBW,
											DBP[3],
											inDesc->trx[3].TBW,
											DBP[4],
											inDesc->trx[4].TBW	);

	AS_UINT64(MIP2) = SCE_GS_SET_MIPTBP2(	DBP[5],
											inDesc->trx[5].TBW,
											DBP[6],
											inDesc->trx[6].TBW,
											DBP[7],
											inDesc->trx[7].TBW	);

//	if( inDesc->mipmapCpt > 1 )
//		Printf( "#mips = %d\n", inDesc->mipmapCpt );

	return TRUE;
}




void
tram::TexTRX::Update	(	uint			inBAddr,
							uint8			inLODMask	)
{
	DisableEOP();

	// Translate ?
	if( baddr != inBAddr ) {
		uint16 dp = uint16(inBAddr) - uint16(baddr);
		baddr = inBAddr;

		// BITBLTBUF.DBP (clut + up to 7 mipmaps)
		uint16* DBP = (uint16*) &tadr[9];
		DBP[0*32] += dp;
		DBP[1*32] += dp;
		DBP[2*32] += dp;
		DBP[3*32] += dp;
		DBP[4*32] += dp;
		DBP[5*32] += dp;
		DBP[6*32] += dp;
		DBP[7*32] += dp;

		// GS context
		TEX0.CBP  += dp;
		TEX0.TBP0 += dp;
		MIP1.TBP1 += dp;
		MIP1.TBP2 += dp;
		MIP1.TBP3 += dp;
		MIP2.TBP4 += dp;
		MIP2.TBP5 += dp;
		MIP2.TBP6 += dp;
	}

	// Setup TRX according to lodMask
	inLODMask &= allLodMask;
	if( lodMask != inLODMask ) {
		lodMask = inLODMask;
		// TODO
	}
}





bool
tram::TestMipmaping	(	tram::TexDesc*	inDesc,
						uint			inMode,
						uint			inLODMask	 )
{
	if( inDesc->mipmapCpt == 1 )
		return FALSE;
	uint allLodMask	= (1<<inDesc->mipmapCpt) - 1;
	inLODMask &= allLodMask;
	if( inLODMask == 0 )
		return FALSE;

	//						ABGR
	uint32 mipColor[] = {	0x80000000,
							0x800000FF,			// 512
							0x8000FF00,			// 256
							0x80FF0000,			// 128
							0x8000FFFF,			// 64
							0x80FFFF00,			// 32
							0x80FF00FF,			// 16
							0x80FFFFFF,			// 8
							0x8040FF80,			// 4
							0x80FF4080	};		// 2

	// Rewrite CLUT ?
	if( inDesc->psm == SCE_GS_PSMT4 || inDesc->psm == SCE_GS_PSMT8 ) {
		uint32* clutPtr = (uint32*) inDesc->trx[0].bitmapData;
		for( uint i = 0 ; i < sizeof(mipColor)/sizeof(uint32) ; i++ )
			clutPtr[i] = mipColor[i];
	}

	for( uint i = 0 ; i < inDesc->mipmapCpt ; i++ )
	{
		if( ((1<<i)&inLODMask) == 0 )
			continue;
		uint v;
		if( inMode == 1 )		v = 10 - Max( inDesc->L2_W-i, inDesc->L2_H-i );
		else if( inMode == 2 )	v = 10 - Min( inDesc->L2_W-i, inDesc->L2_H-i );
		else					v = i+1;

		uint8* map = inDesc->trx[i+1].bitmapData;
		uint   N   = inDesc->trx[i+1].bitmapBSize;
		if( inDesc->psm == SCE_GS_PSMT4 ) {
			v = (v<<4)|v;
			while( N-- )
				*map++ = v;
		} else if( inDesc->psm == SCE_GS_PSMT8 ) {
			while( N-- )
				*map++ = v;
		} else { // SCE_GS_PSMCT32
			uint32* map32 = (uint32*) map;
			uint    N32   = N / 4;
			while( N32-- )
				*map32++ = mipColor[v];
		}
	}

	return TRUE;
}



bool
tram::BuildDesc		(	TexDesc*		outDesc,
						dmac::Cursor*	inDC,
						uint			inPSM,
						uint			inL2W,
						uint			inL2H,
						pvoid			inCLUTPtr,
						pvoid			inTexelPtr,
						uint			inBlockL2W,
						uint			inBlockL2H		)
{
	if( !outDesc || !inDC )
		return FALSE;

	uint inTrxPSM = ( inPSM == SCE_GS_PSMCT24 ? SCE_GS_PSMCT32 : inPSM );

	NV_ASSERT( inTexelPtr );
	NV_ASSERT_A128( inTexelPtr );
	NV_ASSERT_NA128( inCLUTPtr );
	NV_ASSERT_IF( inPSM==SCE_GS_PSMT8, inCLUTPtr );
	NV_ASSERT_IF( inPSM!=SCE_GS_PSMT8, !inCLUTPtr );

	if( inBlockL2W == 0 )
		inBlockL2W = inL2W;
	if( inBlockL2H == 0 )
		inBlockL2H = inL2H;

	if( !inTexelPtr )
		return FALSE;
	if( inPSM!=SCE_GS_PSMT8 && inPSM!=SCE_GS_PSMCT24 && inPSM!=SCE_GS_PSMCT32 )
		return FALSE;
	if( inPSM==SCE_GS_PSMT8 && !inCLUTPtr )
		return FALSE;
	if( inPSM!=SCE_GS_PSMT8 && inCLUTPtr )
		return FALSE;

	// 8x8 min
	if( inL2W < 3 || inL2H < 3 )
		return FALSE;

	uint w			= 1<<inL2W;
	uint h			= 1<<inL2H;
	uint bw			= 1<<inBlockL2W;
	uint bh			= 1<<inBlockL2H;
	uint psize		= gs::tools::GetPageSize( inPSM, w, h );
	uint fullpsize	= inCLUTPtr ? psize+1 : psize;
	if( fullpsize > tram::GetPSize() || w>512 || w>512 )
		return FALSE;

	uint blockQSIZE = (bw * bh * (inCLUTPtr?1:4)) >> 4;
	if( blockQSIZE > 32760U )	// GIFTAG.NLOOP 15bits limitation !
		return FALSE;


	//
	// Init desc

	Zero( *outDesc );
	outDesc->flags	    = TexDesc::F_DMA_CALLED;	// DMA-CALL'ed uploader !
	outDesc->psm		= inPSM;
	outDesc->mipmapCpt	= 1;
	outDesc->L2_W		= inL2W;
	outDesc->L2_H		= inL2H;


	//
	// CLUT uploader

	uint8* dma0 = inDC->i8;
	MType mtype;
	if( inCLUTPtr )
	{
		TexDesc::Trx& trx = outDesc->trx[0];
		trx.dmaData			= inDC->i128;
		trx.dmaQSize		= 8;
		trx.bitmapBSize		= 64<<4;
		trx.bitmapData		= (uint8*)inCLUTPtr;
		trx.TBW				= 1;
		mtype.i64[0]		= SCE_GS_SET_BITBLTBUF( 0,0,0, 0/*DBP*/, 1, SCE_GS_PSMCT32 );
		trx.BITBLTBUF[0]	= mtype.i32[0];
		trx.BITBLTBUF[1]	= mtype.i32[1];

		// 8 QW
		NV_ASSERT_A128( inDC->vd );
		inDC->i64[0]		= DMA_TAG_CNT | 5;
		inDC->i64[1]		= 0;
		inDC->i64[2]		= SCE_GIF_SET_PACKED_TAG( 3, 0/*EOP*/, 0, 0, 1 );
		inDC->i64[3]		= 0xE;
		inDC->i64[4]		= SCE_GS_SET_TRXPOS( 0,0,0,0,0 );		// RESET POS
		inDC->i64[5]		= SCE_GS_TRXPOS;
		inDC->i64[6]		= SCE_GS_SET_TRXREG( 16, 16 );
		inDC->i64[7]		= SCE_GS_TRXREG;
		inDC->i64[8]		= SCE_GS_SET_TRXDIR( 0 );
		inDC->i64[9]		= SCE_GS_TRXDIR;
		inDC->i64[10]		= SCE_GIF_SET_IMAGE_TAG( 64, 0/*EOP*/ );	// 15bits NLOOP !
		inDC->i64[11]		= 0;
		inDC->i32[24]		= DMA_TAG_REF | 64;
		inDC->i32[25]		= DMA_ADDR( inCLUTPtr );
		inDC->i64[13]		= 0;
		inDC->i64[14]		= DMA_TAG_RET | 0;
		inDC->i64[15]		= 0;
		inDC->i64 += 16;

		outDesc->trxBSize[0]  = 32;
		outDesc->trxAllBSize += outDesc->trxBSize[0];
	}


	//
	// Mip0 uploader

	{
		TexDesc::Trx& trx = outDesc->trx[1];
		trx.dmaData			= inDC->i128;
		trx.dmaQSize		= 0;
		trx.bitmapBSize		= (w * h * (inCLUTPtr?1:4));
		trx.bitmapData		= (uint8*)inTexelPtr;
		trx.TBW				= (w+63)/64;
		mtype.i64[0]		= SCE_GS_SET_BITBLTBUF( 0,0,0, 0/*DBP*/, trx.TBW, inTrxPSM );
		trx.BITBLTBUF[0]	= mtype.i32[0];
		trx.BITBLTBUF[1]	= mtype.i32[1];

		// 4 QW
		NV_ASSERT_A128( inDC->vd );
		uint8* mip_dma0		= inDC->i8;
		uint32 mip_data		= (uint32) inTexelPtr;
		inDC->i64[0]		= DMA_TAG_CNT | 2;
		inDC->i64[1]		= 0;
		inDC->i64[2]		= SCE_GIF_SET_PACKED_TAG( 1, 0/*EOP*/, 0, 0, 1 );
		inDC->i64[3]		= 0xE;
		inDC->i64[4]		= SCE_GS_SET_TRXREG( bw, bh );
		inDC->i64[5]		= SCE_GS_TRXREG;
		inDC->i64 += 6;
		for( uint by = 0 ; by < h ; by+=bh ) {
			for( uint bx = 0 ; bx < w ; bx+=bw ) {
				NV_ASSERT_A128( inDC->vd );
				NV_ASSERT( blockQSIZE <= 32760U );
				inDC->i64[0]		= DMA_TAG_CNT | 4;
				inDC->i64[1]		= 0;
				inDC->i64[2]		= SCE_GIF_SET_PACKED_TAG( 2, 0/*EOP*/, 0, 0, 1 );
				inDC->i64[3]		= 0xE;
				inDC->i64[4]		= SCE_GS_SET_TRXPOS( 0, 0, bx, by, 0 );
				inDC->i64[5]		= SCE_GS_TRXPOS;
				inDC->i64[6]		= SCE_GS_SET_TRXDIR( 0 );
				inDC->i64[7]		= SCE_GS_TRXDIR;
				inDC->i64[8]		= SCE_GIF_SET_IMAGE_TAG( blockQSIZE, 0/*EOP*/ );	// 15bits NLOOP !
				inDC->i64[9]		= 0;
				inDC->i32[20]		= DMA_TAG_REF | blockQSIZE;
				inDC->i32[21]		= DMA_ADDR( mip_data );
				inDC->i64[11]		= 0;
				inDC->i64		   += 12;
				mip_data += blockQSIZE<<4;
			}
		}
		NV_ASSERT_DC( inDC );
		inDC->i64[0]		= DMA_TAG_RET | 0;
		inDC->i64[1]		= 0;
		inDC->i64 += 2;

		trx.dmaQSize		= uint32(inDC->i8 - mip_dma0) >> 4;

		outDesc->trxBSize[1]  = psize * 32;
		outDesc->trxAllBSize += outDesc->trxBSize[1];
	}

	outDesc->resBSize = uint32(inDC->i8 - dma0);

	NV_ASSERT_DC( inDC );
	return TRUE;
}



void
tram::DMATranslateDesc	(	TexDesc*		ioDesc,
							uint32			inAddrOffset	)
{
	if( !ioDesc )
		return;
	for( int i = 0 ; i < 8 ; i++ ) {
		if( ioDesc->trx[i].dmaData ) {
			AS_UINT32( ioDesc->trx[i].dmaData ) += inAddrOffset;
		}
	}
}



