/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
using namespace nv;



void
frame::Packet::Init		(	Agent*		inOwner		)
{
	owner			= inOwner;
	hkey			= 0;
	tadr			= NULL;
	link			= NULL;
	mcId			= -1;
	texList			= NULL;
	subList			= NULL;
	flags			= 0;

#if defined( _NVCOMP_ENABLE_DBG )
	uint32 breakPktAddr = 0;
	core::GetParameter( core::PR_EE_DMAC_BREAK_ON_PKT, &breakPktAddr );
	if( breakPktAddr && breakPktAddr==uint32(this) ) {
		char warnMsg[64];
		Sprintf( warnMsg, "Break on packet 0x%08 !\n", breakPktAddr );
		NV_WARNING_BREAK( warnMsg );
	}
#endif
}



void
frame::Packet::Translate	(	uint32		inBOffset	)
{
	if( tadr )
		AS_UINT32( tadr	) += inBOffset;

	if( link )
		AS_UINT32( link	) += inBOffset;
}


bool
frame::Packet::IsTextured	(		)
{
	TexContext* tex = texList;
	if( !tex )
		return FALSE;
	while( tex ) {
		if( tex->id>=0 && tex->gsContext )
			return TRUE;		// At least one !
		tex = tex->next;
	}
	return FALSE;
}


void
frame::Packet::Append		(	TexContext*		inTexContext	)
{
	NV_ASSERT( inTexContext );
	inTexContext->next = NULL;
	if( !texList ) {
		texList = inTexContext;
	} else {
		TexContext* tex = texList;
		while( tex->next )
			tex = tex->next;
		tex->next = inTexContext;
	}
}


void
frame::Packet::Append		(	Packet*		inSubPkt	)
{
	NV_ASSERT( inSubPkt );
	NV_ASSERT( inSubPkt != this );
	inSubPkt->subList = NULL;
	if( !subList ) {
		subList = inSubPkt;
	} else {
		Packet* pkt = subList;
		while( pkt->subList )
			pkt = pkt->subList;
		pkt->subList = inSubPkt;
	}
}


void
frame::Packet::BuildBasicHKey	(		)
{
	// TODO
	hkey = ~0U;
}



