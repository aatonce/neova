/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "sys.h"
#include "core.h"
#include "core_SPU2.h"
#include "core_Timer.h"
#include "core_EE.h"
#include "core_File.h"
#include "core_Stream.h"
#include "core_EELoading.h"
#include "core_Sound.h"



static uint32		cmdListOut0	[ NVIOP_RPC_CMD_BSIZE/4 ] ALIGNED512;
static uint32		cmdListOut1	[ NVIOP_RPC_CMD_BSIZE/4 ] ALIGNED512;
static Cursor		cursOut;

static int			thUpdateId;
static int			semUpdateId;
static int			semLockId;
static uint			update_count;
static uint			th_update_count;




static int th_update_hdl( u_long /*arg*/ )
{

	for( ;; ) {
		WaitSema( semUpdateId );		// wait CB signals
		WaitSema( semLockId );			// core lock
		Core_Update();
		SignalSema( semLockId );		// core unlock
		th_update_count++;
	}
}




void
Core_Init	(	uint	inUpdateThreadPriority,
				uint	inUpdateTimerFreq,
				uint	inStreamCacheBSize,
				uint	inEELoadReqMaxBSize,
				uint	inVoiceReqMaxBSize,
				uint	inSndLockNbMax,
				uint	inMediaMode			)
{
	FlushDcache();
	CpuEnableIntr();

	update_count    = 0;
	th_update_count = 0;

	PrintfA(( pfa, ">> Core_Init: Before IOP memory state: size[%dKo] free[%dko] max[%dko]\n",
					(int)QueryMemSize()>>10,
					(int)QueryTotalFreeMemSize()>>10,
					(int)QueryMaxFreeMemSize()>>10	));

	// Fix system parameters
	inUpdateThreadPriority	= CLAMP( inUpdateThreadPriority, USER_HIGHEST_PRIORITY/*9*/, USER_LOWEST_PRIORITY/*123*/ );
	inUpdateTimerFreq		= CLAMP( inUpdateTimerFreq, 60, 100*60 );
	inStreamCacheBSize		= MAX( inStreamCacheBSize, 64*1024 );
	inEELoadReqMaxBSize		= CLAMP( inEELoadReqMaxBSize, inStreamCacheBSize/8, inStreamCacheBSize	);
	inVoiceReqMaxBSize		= CLAMP( inVoiceReqMaxBSize,  inStreamCacheBSize/8, inStreamCacheBSize	);
	inVoiceReqMaxBSize		= GetUpperRound( inVoiceReqMaxBSize, 128 );
	Printf( ">> Parameters: \n" );
	PrintfA(( pfa, "    Update Thread Pri.    : %d\n", inUpdateThreadPriority				));
	PrintfA(( pfa, "    Update Timer Freq.    : %d\n", inUpdateTimerFreq					));
	PrintfA(( pfa, "    Stream cache bsize    : %d\n", inStreamCacheBSize					));
	PrintfA(( pfa, "    EELoad req. max bsize : %d\n", inEELoadReqMaxBSize					));
	PrintfA(( pfa, "    Voice req. max bsize  : %d\n", inVoiceReqMaxBSize					));
	PrintfA(( pfa, "    Sound lock nb max     : %d\n", inSndLockNbMax						));
	PrintfA(( pfa, "    MediaMode			  : %s\n", inMediaMode==SCECdCD ? "CD" : "DVD"	));

	// Init cursors
	cursOut.start = cmdListOut0;
	cursOut.t.i32 = cmdListOut0;

	// Init services
	EE_Init();
	SPU2_Init();
	File_Init( inMediaMode );
	Timer_Init( inUpdateTimerFreq );
	Stream_Init( inStreamCacheBSize );

	// Init streams
	EELoading_Init	( inEELoadReqMaxBSize	);
	Sound_Init		( inSndLockNbMax,
					  inVoiceReqMaxBSize	);

	// Create update semaphore
	{
		static struct SemaParam	sem0Param;
		sem0Param.attr			= SA_THFIFO;
		sem0Param.initCount		= 0;
		sem0Param.maxCount		= 1;
		sem0Param.option		= 0;
		semUpdateId 			= CreateSema( &sem0Param );
		ASSERT( semUpdateId >= 0 );
	}

	// Create lock semaphore
	{
		static struct SemaParam	sem1Param;
		sem1Param.attr			= SA_THFIFO;
		sem1Param.initCount		= 0;			// start with locked sema !
		sem1Param.maxCount		= 1;
		sem1Param.option		= 0;
		semLockId 				= CreateSema( &sem1Param );
		ASSERT( semLockId >= 0 );
	}

	// Create update thread
	{
		static struct ThreadParam thParam;
		thParam.attr         	= TH_C;
		thParam.entry       	= th_update_hdl;
		thParam.initPriority	= inUpdateThreadPriority;
		thParam.stackSize		= NVIOP_TH_STACK_BSIZE;
		thParam.option			= 0;
		thUpdateId 				= CreateThread( &thParam );
		ASSERT( thUpdateId > 0 );
		StartThread( thUpdateId, 0 );
	}

	// Starts services interrupt callback
	SPU2_EnableCB( semUpdateId );
	File_EnableCB( semUpdateId );
	Timer_EnableCB( semUpdateId );

	PrintfA(( pfa, ">> Core_Init: After IOP memory state: size[%dKo] free[%dko] max[%dko]\n",
					(int)QueryMemSize()>>10,
					(int)QueryTotalFreeMemSize()>>10,
					(int)QueryMaxFreeMemSize()>>10	));
	Printf( ">> Core initialised.\n" );

	ASSERT( update_count == 0 );
	Core_Unlock();
}


void
Core_Shut	(		)
{
	Core_CloseFile();
	Stream_Shut();
	File_Shut();
	Timer_Shut();
	EE_Shut();
	SPU2_Shut();
	EELoading_Shut();
	Sound_Shut();

	if( thUpdateId >= 0 ) {
		TerminateThread( thUpdateId );
		DeleteThread( thUpdateId );
		thUpdateId = -1;
	}
	if( semUpdateId >= 0 ) {
		DeleteSema( semUpdateId );
		semUpdateId = -1;
	}

	Printf( ">> Core shuted.\n" );
}


void
Core_Reset	(				)
{
	File_Reset();
	SPU2_Reset();
	Timer_Reset();
	EE_Reset();
	Stream_Reset();
}


void
Core_WakeUpdate		(		)
{
	ASSERT( semUpdateId >= 0 );
	SignalSema( semUpdateId );
}


void
Core_Lock		(		)
{
	WaitSema( semLockId );
//	PrintfA(( pfa, "- lock [update=%d,ti=%d,th=%d]\n", Core_GetUpdateCounter(), Timer_GetCBCounter(), th_update_count ));
}


void
Core_Unlock		(		)
{
//	PrintfA(( pfa, "- unlock [update=%d,ti=%d]\n", Core_GetUpdateCounter(), Timer_GetCBCounter() ));
	SignalSema( semLockId );
}


uint
Core_GetUpdateCounter	(			)
{
	return update_count;
}


bool
Core_OpenFile	(	pcstr		inFilename,
					uint32 *	outBSize	)
{
	if( File_IsOpened() || !File_Open(inFilename) )
		return FALSE;
	if( outBSize )
		*outBSize = File_GetBSize();
	return TRUE;
}


bool
Core_CloseFile	(			)
{
	return File_Close();	// Could be locked !?
}


Cursor*
Core_GetOutputCursor	(			)
{
	return &cursOut;
}


pvoid
Core_GetOutputCmdList	(			)
{
	uint32 outBSize;

	// Add IOP status
	cursOut.t.i32[0] = NVIOP_STATUS;
	cursOut.t.i32[1] = QueryTotalFreeMemSize();
	cursOut.t.i32[2] = QueryMaxFreeMemSize();
	cursOut.t.i32[3] = SPU2_GetENDX_Core0();;
	cursOut.t.i32[4] = SPU2_GetENDX_Core1();
	cursOut.t.i32[5] = SPU2_GetENVX_Core0();
	cursOut.t.i32[6] = SPU2_GetENVX_Core1();
	cursOut.t.i32[7] = SPU2_GetLastDown_Core0();
	cursOut.t.i32[8] = SPU2_GetLastDown_Core1();
	cursOut.t.i32   += 9;

	// End of packet
	cursOut.t.i32[0] = NVIOP_EOP;
	cursOut.t.i32   += 1;

	ASSERT( (((uint32)cursOut.t.i32)&3) == 0 );
	ASSERT( ((uint32)cursOut.t.i32) >= ((uint32)cursOut.start) );
	outBSize = ((uint32)cursOut.t.i32) - ((uint32)cursOut.start);
	ASSERT( outBSize <= NVIOP_RPC_CMD_BSIZE );

	// Swap cursors
	if( cursOut.start == cmdListOut0 ) {
		cursOut.start = cmdListOut1;
		cursOut.t.i32 = cmdListOut1;
		return cmdListOut0;
	} else {
		cursOut.start = cmdListOut0;
		cursOut.t.i32 = cmdListOut0;
		return cmdListOut1;
	}
}


void
Core_ParseInputCmdList	(	Cursor *	inCmdListStart,
							Cursor *	inCmdListEnd	)
{
	Cursor cin  = *inCmdListStart;
	Cursor cend = *inCmdListEnd;

	// Starts SD commands
	SPU2_BeginSdCmd();

	for( ;; )
	{
		// Completed ?
		if(		cin.t.i8 >= cend.t.i8
			||	cin.t.i32[0] == NVIOP_EOP	)
			break;

		// PrintfA(( pfa, "iop-cmd: %d\n", cin.t.i32[0] ));

		// Switch on cmd
		switch( cin.t.i32[0] )
		{
			// Load Data ?
			case NVIOP_LOAD_DATA :
			{
				EELoading_Start(	cin.t.i16[8],
									cin.t.i32[1],
									cin.t.i32[2],	// bOffset 0
									cin.t.i32[2],	// bOffset 1
									cin.t.i32[2],	// bOffset 2
									cin.t.i32[2],	// bOffset 3
									cin.t.i32[3],
									cin.t.i16[9]	);
				cin.t.i32 += 5;
				break;
			}

			// Lock sound
			case NVIOP_SD_LOCK :
			{
				Sound_Lock	(	cin.t.i16[2],		// lock id
								cin.t.i16[3],		// mode
								cin.t.i32[2],		// sram baddr
								cin.t.i32[3],		// sram bsize
								cin.t.i32[4],		// data boffset
								cin.t.i32[5]	);	// data bsize
				cin.t.i32 += 6;
				break;
			}

			// Unlock sound
			case NVIOP_SD_UNLOCK :
			{
				Sound_Unlock( cin.t.i16[2] );
				cin.t.i32 += 2;
				break;
			}

			// Start sound
			case NVIOP_SD_START :
			{
				Sound_Start(	cin.t.i16[2],
								cin.t.i16[3],
								cin.t.i32[3],
								cin.t.i16[4],
								cin.t.i16[5]	);
				cin.t.i32 += 4;
				break;
			}

			// Stop sound
			case NVIOP_SD_STOP :
			{
				Sound_Stop(		cin.t.i16[2],
								cin.t.i16[3]	);
				cin.t.i32 += 2;
				break;
			}

			// Break sound
			case NVIOP_SD_BREAK :
			{
				Sound_Break(	cin.t.i32[1]	);
				cin.t.i32 += 2;
				break;
			}

			// SD command ?
			case NVIOP_SD_CMD :
			{
				SPU2_WriteSdCmd( cin.t.i16[2],		// reg
								 cin.t.i16[3],		// ch or core
								 cin.t.i32[2] );	// value
				cin.t.i32 += 3;
				break;
			}

			// Set sound volume
			case NVIOP_SD_SETVOL :
			{
				Sound_SetVolume(	cin.t.i16[2],
									cin.t.i16[3],
									cin.t.i16[4],
									cin.t.i16[5]	);
				cin.t.i32 += 3;
				break;
			}

			// Set sound pitch
			case NVIOP_SD_SETPITCH :
			{
				Sound_SetPitch(	cin.t.i16[2],
								cin.t.i16[3],
								cin.t.i32[2]	);
				cin.t.i32 += 3;
				break;
			}

			// Set streamed sound segment
			case NVIOP_SD_SETSEGMENT :
			{
				Sound_SetSegment(	cin.t.i32[1],			// lockId
									cin.t.i32[2],			// dataBOffset
									cin.t.i32[3]	);		// dataBSize
				cin.t.i32 += 4;
				break;
			}

			// Set streamed no more segment
			case NVIOP_SD_NOSEGMENT :
			{
				Sound_NoSegment(	cin.t.i32[1]	);		// lockId
				cin.t.i32 += 2;
				break;
			}

			// Mem alloc ?
			case NVIOP_MEM_ALLOC :
			{
				uint bsize	   = cin.t.i32[1];
				cin.t.i32 	  += 2;

				cursOut.t.i32[0] = NVIOP_MEM_ALLOC;
				cursOut.t.i32[1] = (uint32) Malloc( GetUpperRound(bsize,16) );
				cursOut.t.i32   += 2;
				break;
			}

			// Mem free ?
			case NVIOP_MEM_FREE :
			{
				pvoid ptr	   = (pvoid) cin.t.i32[1];
				if( ptr )	Free( ptr );
				cin.t.i32 	  += 2;
				break;
			}

			// unknow -> parsing error
			default :
				PrintfA(( pfa, "Err: Unknow or unexpected command %d !\n", cin.t.i32[0] ));
				cin.t.i32[0] = NVIOP_EOP;
				break;
		}
	}

	// Flush SD commands
	SPU2_EndSdCmd();
}





void
Core_Update		(			)
{
	SPU2_BeginSdCmd();		// Starts SD commands
	if( EELoading_IsNeedPriority() ) {
		EELoading_Update();
		Sound_Update();
	} else {
		Sound_Update();
		EELoading_Update();
	}
	SPU2_EndSdCmd();	// Flush SD commands

	Stream_Update();

	update_count++;
}




