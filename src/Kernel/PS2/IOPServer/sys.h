/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	SYS_H
#define	SYS_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <kernel.h>
#include <introld.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sif.h>
#include <sifcmd.h>
#include <sifrpc.h>
#include <libsd.h>
#include <libcdvd.h>

typedef unsigned char			uchar;
typedef unsigned char			uint8;
typedef	signed	 short			int16;
typedef unsigned short			uint16;
typedef unsigned int			uint32;
typedef	signed	 int			int32;
typedef long long				int64;
typedef unsigned long long		uint64;
typedef void *					pvoid;
typedef char *					pstr;
typedef const char *			pcstr;

typedef char					bool;
#define	TRUE					(1)
#define	FALSE					(0)



typedef struct {
	pvoid		start;
	union {
	uint8	*	i8;
	uint16	*	i16;
	uint32	*	i32;
	} t;
} Cursor;


#define	ALIGNED32		__attribute__((aligned (4)))
#define	ALIGNED64		__attribute__((aligned (8)))
#define	ALIGNED128		__attribute__((aligned (16)))	// 1QW
#define	ALIGNED512		__attribute__((aligned (64)))	// 4QW
#define	ALIGNED1024		__attribute__((aligned (128)))	// 8QW


pvoid	Malloc( uint bsize );
void	Free( pvoid ptr );

//	GetUpperRound( 5, 16 )	= 16
//	GetUpperRound( 5, 0 )	= 5
int32	GetUpperRound( uint32 inV, uint32 inA );

bool	IsPow2( uint32 inV );

//	GetLowestPowOf2( 100010100b ) = 000000100b
//	GetLowestPowOf2( 100010000b ) = 000010000b
//	returned 0 if v == 0 !
uint32	GetLowestPowOf2( uint32 v );


#define	MAX( inA, inB )				( ((inA)>(inB)) ? (inA) : (inB) )
#define	MIN( inA, inB )				( ((inA)<(inB)) ? (inA) : (inB) )
#define	CLAMP( inV, inMin, inMax )	MAX( MIN( (inV), (inMax) ) , (inMin) )
#define MCC32( in0, in1, in2, in3 )	(((uint32)in0)|(((uint32)in1)<<8)|(((uint32)in2)<<16)|(((uint32)in3)<<24))


#include "printf.h"
#include "cmd.h"


#ifdef	_DEBUG
#define ASSERT(inC)		if(!(inC)) { PrintfA(( pfa, "Error: Assert has failed on "#inC" [%s,%d]\n", __FILE__, __LINE__ )); }
#else
#define ASSERT(inC)
#endif


#endif	SYS_H


