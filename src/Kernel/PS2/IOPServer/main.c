/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "sys.h"
#include "core.h"


static ModuleInfo	Module = {"NovaIOP", 0x0101};
static uint32		rpcRecvBuffer[ NVIOP_RPC_CMD_BSIZE/4 ] ALIGNED512;
static uint32		rpcSendBuffer[ NVIOP_RPC_CMD_BSIZE/4 ] ALIGNED512;



void*
rpc_server_hdl( unsigned int fno, void * rpcInput, int rpcInputSize )
{
	switch( fno )
	{
	case NVIOP_FCT_INIT :
		{
			uint32* in32 = (uint32*) rpcInput;
			uint rpcThreadPriority	  = in32[0];
			uint updateThreadPriority = in32[1];
			uint updateTimerFreq	  = in32[2];
			uint streamCacheBSize	  = in32[3];
			uint eeLoadReqMaxBSize	  = in32[4];
			uint voiceReqMaxBSize	  = in32[5];
			uint sndLockNbMax		  = in32[6];
			uint mediaMode			  = in32[7];
			ASSERT( sndLockNbMax <= 1024 );
			ASSERT( (((uint)rpcInput)&3) == 0 );

			rpcThreadPriority    = CLAMP( rpcThreadPriority,    USER_HIGHEST_PRIORITY/*9*/, USER_LOWEST_PRIORITY/*123*/ );
			updateThreadPriority = CLAMP( updateThreadPriority, USER_HIGHEST_PRIORITY/*9*/, USER_LOWEST_PRIORITY/*123*/ );

			// Change RPC Thread priority
			ChangeThreadPriority( TH_SELF, rpcThreadPriority );

			// Init Core
			Core_Init(	updateThreadPriority,
						updateTimerFreq,
						streamCacheBSize,
						eeLoadReqMaxBSize,
						voiceReqMaxBSize,
						sndLockNbMax,
						mediaMode		);

			// returns irx version
			rpcSendBuffer[0] = NVIOP_VERSION;
			return rpcSendBuffer;
		}

	case NVIOP_FCT_SHUT :
		{
			Core_Shut();
			return rpcSendBuffer;		// no return data used!
		}

	case NVIOP_FCT_RESET :
		{
			Core_Lock();
				Core_Reset();
				rpcSendBuffer[0] = TRUE;
			Core_Unlock();
			return rpcSendBuffer;
		}

	case NVIOP_FCT_OPEN_FILE :
		{
			Core_Lock();
				if( Core_OpenFile((pcstr)rpcInput,rpcSendBuffer) ) {
					PrintfA(( pfa, "* File Opened : [\"%s\"] done (%d Ko).\n", (pcstr)rpcInput, rpcSendBuffer[0]>>10 ));
					// result is bsize in fct_res[0] !
				} else {
					PrintfA(( pfa, "* File Opened : [\"%s\"] failed !\n", (pcstr)rpcInput ));
					rpcSendBuffer[0] = 0;
				}
			Core_Unlock();
			return rpcSendBuffer;
		}

	case NVIOP_FCT_CLOSE_FILE :
		{
			Core_Lock();
				if( Core_CloseFile() ) {
					Printf( "* File Closed.\n" );
					rpcSendBuffer[0] = 1;
				} else {
					rpcSendBuffer[0] = 0;
				}
			Core_Unlock();
			return rpcSendBuffer;
		}

	case NVIOP_FCT_CMD_BATCH :
		{
			pvoid outCmd = NULL;
			Core_Lock();
				// Input cmd parsing
				if( rpcInputSize ) {
					Cursor inputStartCursor;
					Cursor inputEndCursor;
					inputStartCursor.t.i8 = (uint8*) rpcInput;
					inputEndCursor.t.i8	  = inputStartCursor.t.i8 + rpcInputSize;
					Core_ParseInputCmdList( &inputStartCursor, &inputEndCursor );
				}
				Core_Update();
				// Output cmd
				outCmd = Core_GetOutputCmdList();
			Core_Unlock();
			return outCmd;
		}

	default :
		Printf( "<nova-iop> Invalid RPC function !\n" );
		return NULL;		// no return data used!
	}
}



int rpc_server_loop()
{
	sceSifQueueData qd;
	sceSifServeData sd;

	if( !sceSifCheckInit() )
		sceSifInit();
	sceSifInitRpc(0);
	sceSifSetRpcQueue( &qd, GetThreadId() );
	sceSifRegisterRpc( &sd, NVIOP_BIND_ID, rpc_server_hdl, (pvoid)rpcRecvBuffer, NULL, NULL, &qd );

	// SERVER LOOP
	sceSifRpcLoop(&qd);

//	sceSifRemoveRpc( &sd, &qd );
//	sceSifRemoveRpcQueue( &qd );
	return 0;
}



int start(	int		argc,
			char **	argv	)
{
	struct ThreadParam	param;
	int					th;

	FlushDcache();
	CpuEnableIntr();

	Printf_Init( argc, argv );

	PrintfA(( pfa, ">> Neova IOP server is running [THPRI=%d, BIND_ID=0x%08x].\n", NVIOP_RPC_TH_PRIORITY, (uint32)NVIOP_BIND_ID	));
	Printf( ">> (c)2002-2007 AtOnce Technologies\n" );
	PrintfA(( pfa, ">> IOP memory state: size[%dKo] free[%dko] max[%dko]\n", (int)QueryMemSize()>>10, (int)QueryTotalFreeMemSize()>>10,	(int)QueryMaxFreeMemSize()>>10	));

	param.attr         = TH_C;
	param.entry        = rpc_server_loop;
	param.initPriority = NVIOP_RPC_TH_PRIORITY;
	param.stackSize    = NVIOP_TH_STACK_BSIZE;
	param.option       = 0;
	th = CreateThread(&param);
	if( th > 0 ) {
		StartThread(th,0);
		return RESIDENT_END;
	}
	return NO_RESIDENT_END;
}



