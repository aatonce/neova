/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _Core_EELoading_H_
#define	_Core_EELoading_H_



bool	EELoading_Init				(	uint			inReqMaxBSize	);
void	EELoading_Shut				(									);
bool	EELoading_IsNeedPriority	(									);
void	EELoading_Update			(									);

void	EELoading_Start				(	StreamTarget	inTargetRam,
										uint32			inTargetPtr,
										uint32			inBOffset0,
										uint32			inBOffset1,
										uint32			inBOffset2,
										uint32			inBOffset3,
										uint32			inBSize,
										uint			inTryReload		);



#endif






