/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "sys.h"
#include "core.h"
#include "core_SPU2.h"


#define	SPU2_PCM_DMA	0
#define	SPU2_RAM_DMA	1


static int		semUpdateId;
static uint32	pcmBufferStart;
static uint32	pcmBufferEnd;
static uint32	pcmBufferPtr;
static uint		sd_KEYON[2],
				sd_KEYOFF[2];
static uint32	prev_ENVX[2];
static uint32	last_down[2];



static
int
sram_dma_hdl( int ch, pvoid /*data*/ )
{
	if( ch == SPU2_RAM_DMA && semUpdateId >= 0 )
		iSignalSema( semUpdateId );
	return 0;
}



static
void
_update_last_down	(		)
{
	// Down (1) for ENVX 1->0 ( front descendant : (PREV^CUR)&PREV )
	uint32 prv_ENVX0 = prev_ENVX[0];
	uint32 prv_ENVX1 = prev_ENVX[1];
	uint32 cur_ENVX0 = SPU2_GetENVX_Core0();
	uint32 cur_ENVX1 = SPU2_GetENVX_Core1();
	last_down[0] |= ( prv_ENVX0 ^ cur_ENVX0 ) & prv_ENVX0;
	last_down[1] |= ( prv_ENVX1 ^ cur_ENVX1 ) & prv_ENVX1;
	prev_ENVX[0] = cur_ENVX0;
	prev_ENVX[1] = cur_ENVX1;
}




bool
SPU2_Init		(					)
{
	pcmBufferStart = 0;
	SPU2_Reset();
	return TRUE;
}


void
SPU2_Reset	(			)
{
	uint ch;

	sceSdInit( SD_INIT_COLD );

	sceSdSetCoreAttr( SD_C_SPDIF_MODE, SD_SPDIF_MEDIA_DVD|SD_SPDIF_OUT_PCM|SD_SPDIF_COPY_NORMAL );
	sceSdBlockTrans	( SPU2_PCM_DMA, SD_TRANS_MODE_STOP, 0, 0 );
	sceSdBlockTrans	( SPU2_RAM_DMA, SD_TRANS_MODE_STOP, 0, 0 );

	// Setup intr handlers
	sceSdSetTransIntrHandler( SPU2_PCM_DMA, NULL, NULL );
	sceSdSetTransIntrHandler( SPU2_RAM_DMA, NULL, NULL );

	// - No external input for Core0
	// - Memory input for Core0
	sceSdSetParam( SD_CORE_0 | SD_P_MMIX, SD_MMIX_MSNDL|SD_MMIX_MSNDR|SD_MMIX_MINL|SD_MMIX_MINR );
	// - External input for Core1 (from Core0)
	// - No memory input for Core1
	sceSdSetParam( SD_CORE_1 | SD_P_MMIX, SD_MMIX_MSNDL|SD_MMIX_MSNDR|SD_MMIX_SINL|SD_MMIX_SINR );

	// Cores outputs
	sceSdSetParam( SD_CORE_0 | SD_P_MVOLL, 0x3FFF );
	sceSdSetParam( SD_CORE_0 | SD_P_MVOLR, 0x3FFF );
	sceSdSetParam( SD_CORE_1 | SD_P_MVOLL, 0x3FFF );
	sceSdSetParam( SD_CORE_1 | SD_P_MVOLR, 0x3FFF );

	// From Core0 output
	sceSdSetParam( SD_CORE_1 | SD_P_AVOLL, 0x7FFF );
	sceSdSetParam( SD_CORE_1 | SD_P_AVOLR, 0x7FFF );

	// PCM input muting
	sceSdSetParam( SD_CORE_0 | SD_P_BVOLL, 0 );
	sceSdSetParam( SD_CORE_0 | SD_P_BVOLR, 0 );
	sceSdSetParam( SD_CORE_1 | SD_P_BVOLL, 0 );
	sceSdSetParam( SD_CORE_1 | SD_P_BVOLR, 0 );

	// Fx (Wet) volumes
	sceSdSetParam( SD_CORE_0 | SD_P_EVOLL, 0 );
	sceSdSetParam( SD_CORE_0 | SD_P_EVOLR, 0 );
	sceSdSetParam( SD_CORE_1 | SD_P_EVOLL, 0 );
	sceSdSetParam( SD_CORE_1 | SD_P_EVOLR, 0 );

	// Reset ENDX
	sceSdSetSwitch( SD_CORE_0 | SD_S_ENDX, 1 );
	sceSdSetSwitch( SD_CORE_1 | SD_S_ENDX, 1 );

	// Reset VMIXL/R/EL/ER
	sceSdSetSwitch( SD_CORE_0 | SD_S_VMIXL,  0xFFFFFF	);
	sceSdSetSwitch( SD_CORE_0 | SD_S_VMIXR,  0xFFFFFF	);
	sceSdSetSwitch( SD_CORE_1 | SD_S_VMIXL,  0xFFFFFF	);
	sceSdSetSwitch( SD_CORE_1 | SD_S_VMIXR,  0xFFFFFF	);
	sceSdSetSwitch( SD_CORE_0 | SD_S_VMIXEL, 0			);
	sceSdSetSwitch( SD_CORE_0 | SD_S_VMIXER, 0			);
	sceSdSetSwitch( SD_CORE_1 | SD_S_VMIXEL, 0			);
	sceSdSetSwitch( SD_CORE_1 | SD_S_VMIXER, 0			);

	// Set ADSR1/2 - VOLL/R
	for( ch = 0 ; ch < 24 ; ch++ ) {
		sceSdSetParam( SD_CORE_0 | SD_VOICE(ch) | SD_VP_VOLL,  0		);
		sceSdSetParam( SD_CORE_1 | SD_VOICE(ch) | SD_VP_VOLR,  0		);
		sceSdSetParam( SD_CORE_0 | SD_VOICE(ch) | SD_VP_ADSR1, (18<<8)	);
		sceSdSetParam( SD_CORE_1 | SD_VOICE(ch) | SD_VP_ADSR2, 8		);
	}

	// initial ENVX values
	prev_ENVX[0] = SPU2_GetENVX_Core0();
	prev_ENVX[1] = SPU2_GetENVX_Core0();
	last_down[0] = 0;
	last_down[1] = 0;
}


void
SPU2_Shut		(					)
{
	SPU2_DisableCB();
	SPU2_StopPcmPlayback();
	SPU2_SyncTRX();
    sceSdInit( SD_INIT_COLD );
}


void
SPU2_EnableCB	(	int		inSemId		)
{
	semUpdateId = inSemId;
	sceSdSetTransIntrHandler( SPU2_RAM_DMA, sram_dma_hdl, NULL );
}


void
SPU2_DisableCB	(					)
{
	sceSdSetTransIntrHandler( SPU2_RAM_DMA, NULL, NULL );
}


bool
SPU2_IsValidChannel		(	uint		inChannel		)
{
	return ( inChannel>=0 && inChannel<48 );
}


bool
SPU2_IsPlaying	(	uint		inChannel		)
{
	uint core = inChannel >= 24 ? SD_CORE_1 : SD_CORE_0;
	uint vc   = inChannel >= 24 ? SD_VOICE(inChannel-24) : SD_VOICE(inChannel);
	ASSERT( SPU2_IsValidChannel(inChannel) );
	return ( sceSdGetParam(core|vc|SD_VP_ENVX) != 0 );
}


uint
SPU2_GetNAX		(	uint		inChannel		)
{
	uint core = inChannel >= 24 ? SD_CORE_1 : SD_CORE_0;
	uint vc   = inChannel >= 24 ? SD_VOICE(inChannel-24) : SD_VOICE(inChannel);
	ASSERT( SPU2_IsValidChannel(inChannel) );
	return sceSdGetAddr( core|vc|SD_VA_NAX );
}


void
SPU2_SetLSAX	(	uint		inChannel,
					uint		inSRamAddr		)
{
	uint core = inChannel >= 24 ? SD_CORE_1 : SD_CORE_0;
	uint vc   = inChannel >= 24 ? SD_VOICE(inChannel-24) : SD_VOICE(inChannel);
	ASSERT( SPU2_IsValidChannel(inChannel) );
	sceSdSetAddr( core|vc|SD_VA_LSAX, inSRamAddr );
}


uint32
SPU2_GetENVX_Core0	(		)
{
	uint32 envx = 0;
	int	   ch;
	for( ch = 0 ; ch < 24 ; ch++ )
		if( sceSdGetParam(SD_CORE_0|SD_VOICE(ch)|SD_VP_ENVX) )
			envx |= 1<<ch;
	return envx;
}


uint32
SPU2_GetENVX_Core1	(		)
{
	uint32 envx = 0;
	int	   ch;
	for( ch = 0 ; ch < 24 ; ch++ )
		if( sceSdGetParam(SD_CORE_1|SD_VOICE(ch)|SD_VP_ENVX) )
			envx |= 1<<ch;
	return envx;
}


uint32
SPU2_GetENDX_Core0	(			)
{
	return sceSdGetSwitch(SD_CORE_0|SD_S_ENDX) & 0xFFFFFF;
}


uint32
SPU2_GetENDX_Core1	(			)
{
	return sceSdGetSwitch(SD_CORE_1|SD_S_ENDX) & 0xFFFFFF;
}


uint32
SPU2_GetLastDown_Core0	(		)
{
	uint32 down0;
	_update_last_down();
	down0 = last_down[0];
	last_down[0] = 0;
	return down0;
}


uint32
SPU2_GetLastDown_Core1	(		)
{
	uint32 down1;
	_update_last_down();
	down1 = last_down[1];
	last_down[1] = 0;
	return down1;
}


void
SPU2_BeginSdCmd		(			)
{
	// SPU KeyOn/Off for the 2 cores to prevent 2Ts delay
	sd_KEYON[0]  = sd_KEYON[1]  = 0;
	sd_KEYOFF[0] = sd_KEYOFF[1] = 0;
}


void
SPU2_WriteSdCmd		(	uint		reg,
						uint		chcore,
						uint		value			)
{
	if( reg >= SD_VP_VOLL && reg <= SD_VP_VOLXR ) {
		// SD_VP_ : ( reg, channel, value )
		if( chcore >= 24 )	reg = SD_CORE_1 | SD_VOICE((chcore-24)) | reg;
		else				reg = SD_CORE_0 | SD_VOICE(chcore) | reg;
		sceSdSetParam( reg, value );
	}
	else if( reg >= SD_P_MMIX && reg <= SD_P_MVOLXR ) {
		// SD_P_ : ( reg, core, value )
		reg = chcore | reg;
		sceSdSetParam( reg, value );
	}
	else if( reg == SD_S_KON ) {
		// SD_S_KON : ( reg, channel, 0/1 )
		if( value ) {
			reg   = chcore >= 24 ? 1 : 0;
			value = 1U << ( chcore >= 24 ? chcore-24 : chcore );
			sd_KEYON[ reg ] |= value;
		}
	}
	else if( reg == SD_S_KOFF ) {
		// SD_S_KOFF : ( reg, channel, 0/1 )
		if( value ) {
			reg   = chcore >= 24 ? 1 : 0;
			value = 1U << ( chcore >= 24 ? chcore-24 : chcore );
			sd_KEYOFF[ reg ] |= value;
		}
	}
	else if( reg >= SD_S_PMON && reg <= SD_S_VMIXER ) {
		// SD_S_ : ( reg, channel, 0/1 )
		uint v0, mask;
		reg   = chcore >= 24 ? SD_CORE_1|reg : SD_CORE_0|reg;
		v0    = sceSdGetSwitch( reg );
		mask  = 1U << ( chcore >= 24 ? chcore-24 : chcore );
		value = value&1 ? v0|mask : v0&(~mask);
		sceSdSetSwitch( reg, value );
	}
	else if( reg >= SD_A_ESA && reg <= SD_A_IRQA ) {
		// SD_A_ : ( reg, core, value )
		reg = chcore | reg;
		sceSdSetAddr( reg, value );
	}
	else if( reg >= SD_VA_SSA && reg <= SD_VA_NAX ) {
		// SD_VA_ : ( reg, channel, value )
		if( chcore >= 24 )	reg = SD_CORE_1 | SD_VOICE((chcore-24)) | reg;
		else				reg = SD_CORE_0 | SD_VOICE(chcore) | reg;
		sceSdSetAddr( reg, value );
	}
	else if( reg >= SD_C_EFFECT_ENABLE && reg <= SD_C_SPDIF_MODE ) {
		// SD_C_ : ( reg, -, value )
		sceSdSetCoreAttr( reg, value );
	}
}


void
SPU2_EndSdCmd		(								)
{
	// no KON&KOFF at the same 1T time !
	uint32 k0 = ( sd_KEYON[0] & sd_KEYOFF[0] ) & 0xFFFFFF;
	uint32 k1 = ( sd_KEYON[1] & sd_KEYOFF[1] ) & 0xFFFFFF;
	if( (k0|k1) ) {
		uint32 kLo = (k1<<24) | k0;
		uint32 kHi = (k1>> 8);
		PrintfA(( pfa, "SPU2_EndSdCmd: KON/KOFF in same 1T for %04x%08x !\n", kHi, kLo ));
		// disable KON && flag as KOFF'ed
		sd_KEYON[0] ^= k0;	last_down[0] |= k0;
		sd_KEYON[1] ^= k1;	last_down[1] |= k1;
	}

	// Set SPU2 KeyOn/Off
	if( sd_KEYON[0] )
		sceSdSetSwitch( SD_CORE_0|SD_S_KON, sd_KEYON[0] );
	if( sd_KEYON[1] )
		sceSdSetSwitch( SD_CORE_1|SD_S_KON, sd_KEYON[1] );
	if( sd_KEYOFF[0] )
		sceSdSetSwitch( SD_CORE_0|SD_S_KOFF, sd_KEYOFF[0] );
	if( sd_KEYOFF[1] )
		sceSdSetSwitch( SD_CORE_1|SD_S_KOFF, sd_KEYOFF[1] );

	// KeyOn set ENVX to 1 -> update last down
	_update_last_down();
}


bool
SPU2_StartPcmPlayback	(	uint	inBufferBSize		)
{
	if( pcmBufferStart )
		return FALSE;

	pcmBufferStart = (uint) Malloc( inBufferBSize );
	if( !pcmBufferStart )
		return FALSE;
	pcmBufferEnd   = pcmBufferStart + inBufferBSize;
	pcmBufferPtr   = pcmBufferStart;
	memset( (pvoid)pcmBufferStart, 0, inBufferBSize );

	// Start PCM playback
 	sceSdBlockTrans	(	SPU2_PCM_DMA, SD_TRANS_MODE_STOP, 0, 0 );
	sceSdBlockTrans	(	SPU2_PCM_DMA, SD_TRANS_MODE_WRITE|SD_BLOCK_LOOP,
						(pvoid)pcmBufferStart,
						inBufferBSize );
	sceSdSetParam( SD_CORE_0 | SD_P_BVOLL, 0x7FFF );
	sceSdSetParam( SD_CORE_0 | SD_P_BVOLR, 0x7FFF );

	return TRUE;
}


void
SPU2_StopPcmPlayback	(					)
{
	if( pcmBufferStart ) {
		sceSdSetParam( SD_CORE_0 | SD_P_BVOLL, 0 );
		sceSdSetParam( SD_CORE_0 | SD_P_BVOLR, 0 );
		sceSdBlockTrans	(	SPU2_PCM_DMA, SD_TRANS_MODE_STOP, 0, 0 );
		Free( (void*)pcmBufferStart );
		pcmBufferStart = 0;
	}
}


bool
SPU2_GetPcmPlaybackFill		(	uint  *		outLen,
								int16 **	outPtr		)
{
	uint32 dmaPtr, toFill;

	if( !pcmBufferStart )
		return FALSE;

	dmaPtr = sceSdBlockTransStatus( SPU2_PCM_DMA, 0 ) & 0x00FFFFFF;
	if( dmaPtr == 0 )
		return FALSE;

	// [xxxxW.......Pxxxx]
	if( dmaPtr >= pcmBufferPtr )
	{
		toFill = dmaPtr - pcmBufferPtr;
		if( toFill < 2048 )
			return FALSE;
		toFill -= 1024;
	}
	// [....PxxxxxxxW....]
	else
	{
		toFill = pcmBufferEnd - pcmBufferPtr;
	}

	// # packet of 256 stereo samples (1024bytes)
	*outLen	= (toFill>>10);
	*outPtr	= (int16*)pcmBufferPtr;
	return TRUE;
}


void
SPU2_SetPcmPlaybackFill	(	uint		inLen		)
{
	if( pcmBufferStart ) {
		// #packet -> bytes
		pcmBufferPtr += (inLen<<10);
		if( pcmBufferPtr >= pcmBufferEnd )
			pcmBufferPtr = pcmBufferStart;
	}
}


bool
SPU2_StartTRX	(	uint32	inSPU2BufferPtr,
					uint32	inIOPBufferPtr,
					uint32	inBSize			)
{
	int res, intrStat;

	if( inSPU2BufferPtr&0xF ) {
		Printf( "SPU2_StartTRX: dest not is 16-aligned !\n" );
		return FALSE;
	}
	if( inIOPBufferPtr&0x3 ) {
		Printf( "SPU2_StartTRX: source not is 4-aligned !\n" );
		return FALSE;
	}
//	if( inBSize&63 ) {
//		Printf( "SPU2_StartTRX: 64-byte units expected !\n" );
//		return FALSE;
//	}

	CpuSuspendIntr( &intrStat );
	res = sceSdVoiceTrans (	SPU2_RAM_DMA,
							SD_TRANS_MODE_WRITE|SD_TRANS_BY_DMA,
							(uchar*)inIOPBufferPtr,
							inSPU2BufferPtr,
							inBSize			);
	CpuResumeIntr( intrStat );

	return (res >= 0);
}


bool
SPU2_IsTRXReady		(			)
{
	return ( sceSdVoiceTransStatus(SPU2_RAM_DMA,SD_TRANS_STATUS_CHECK) == 1 );
}


void
SPU2_SyncTRX		(			)
{
	while( !SPU2_IsTRXReady() )
		DelayThread( 10 );
}




