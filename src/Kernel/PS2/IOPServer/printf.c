/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "sys.h"
#include "printf.h"


#ifdef  	ENABLE_PRINTF


static int	atLogFd = -1;
char		tmpPrintf[ 256 ];


void
Printf_Init(	int		argc,
				char **	argv	)
{
	int i;

	if( atLogFd >= 0 ) {
		close( atLogFd );
		atLogFd = -1;
	}

	// Find AtMon file prefix (atfile:x.y.z.w!9000,) ?
	for( i = 0 ; i < argc ; i++ ) {
		if( argv[i] && strncmp(argv[i],"atfile:",7)==0 )
		{
			char tmp[128];
			pstr strptr;
			strcpy( tmp, argv[i] );
			strptr = strchr( tmp, ',' );
			strptr[1] = 0;
			strcat( tmp, "mwAtLogIOP.txt" );
			atLogFd = open( tmp, O_RDWR|O_CREAT|O_TRUNC|O_NOBUF );
		}
	}
}


void
Printf( char * inFmt )
{
	if( atLogFd >= 0 )
		write( atLogFd, inFmt, strlen(inFmt) );
	else
		Kprintf( inFmt );
}


#endif


