/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "sys.h"
#include "core.h"
#include "core_SPU2.h"
#include "core_Stream.h"
#include "core_Sound.h"



#define ADPCM_1S_BKCPT		(48000/28)
#define	ADPCM_1S_BSIZE		(ADPCM_1S_BKCPT*16)




// Stereo RIGHT channel for music stream
// This channel is reserved and can't be used for any other purpose
#define	MUSIC_CH		(47)



typedef enum {
	SND_INVALID			=  0,
	SND_LOCKING			=  1,
	SND_LOCKED			=  2
} LockState;

typedef enum {
	SEG_UNDEF			=  0,					// no segment given ...
	SEG_BREAK_ASKING	=  1,					// a break-segment is required from the EE side
	SEG_LOOP_ASKING		=  2,					// a loop-segment is required from the EE side
	SEG_AVAILABLE		=  3,					// the next segment is available
	SEG_NOMORE			=  4					// the EE side has rejected the next segment !
} SegState;

typedef struct {
	uint32			bOffset;					// start bOffset
	uint32			bSize;						// bSize
} Segment;

typedef struct {
	// fixed
	uint			mode;						// 0:spot-SFX 1:stream-SFX 2:stream-MUSIC
	uint32			sramAddr;
	uint32			sramBSize;
	// state
	LockState		state;
	Segment			data;						// cur data segment to play
	uint32			loadPos;					// loading position in cur data segment
	SegState		nextState;					// next segment status
	Segment			nextSegment;				// next segment
	uint32			sramWPos;					// cur write position
	int				streamId;					// stream service id (-1:unused)
	int				streamCh;					// streamed channel (-1:unused)
	uint			streamChPlay;				// streamed channel play cpt
	uint			streamWrap;					// stream wrap cpt
	pvoid			streamTmp;
} Sound;


static uint			sys_lockNbMax;
static uint			sys_reqMaxBSize;
static Sound*		soundA;

#include "core_Sound_Tools.h"






bool
Sound_Init			(	uint		inLockNbMax,
						uint		inReqMaxBSize		)
{
	uint i;

	ASSERT( inLockNbMax );
	if( !inLockNbMax || !inReqMaxBSize )
		return FALSE;

	sys_lockNbMax	= inLockNbMax;
	sys_reqMaxBSize = inReqMaxBSize;

	soundA = (Sound*) Malloc( sizeof(Sound) * sys_lockNbMax );
	ASSERT( soundA );
	for( i = 0 ; i < sys_lockNbMax ; i++ )
		soundA[i].state = SND_INVALID;

	return TRUE;
}


void
Sound_Shut			(			)
{
	Free( soundA );
}


bool
Sound_IsValid	(	uint	inLockId	)
{
	ASSERT( inLockId < sys_lockNbMax );
	if( inLockId >= sys_lockNbMax )		return FALSE;
	return ( soundA[inLockId].state != SND_INVALID );
}


bool
Sound_IsLocked	(	uint		inLockId	)
{
	return Sound_IsValid(inLockId) && ( soundA[inLockId].state == SND_LOCKED );
}


bool
Sound_IsLocking	(	uint		inLockId	)
{
	return Sound_IsValid(inLockId) && ( soundA[inLockId].state == SND_LOCKING );
}


bool
Sound_IsInLock	(	uint		inLockId	)
{
	return Sound_IsLocked(inLockId) || Sound_IsLocking(inLockId);
}


bool
Sound_IsStreamed	(	uint		inLockId	)
{
	return Sound_IsValid(inLockId) && ( soundA[inLockId].mode > 0 );
}


bool
Sound_IsMusic		(	uint		inLockId	)
{
	return Sound_IsValid(inLockId) && ( soundA[inLockId].mode == 2 );
}


bool
Sound_CheckLockSync		(	uint	inLockId	)
{
	// The sound could be auto-unlocked at IOP side (no repeat for streamed sound for exemple),
	// while the sound is marked as locked at EE side until the next SndManager::Update() call.
	// Periodic call of SndManager::Update() can solve this problem !
	if( !Sound_IsLocked(inLockId) )
	{
		// Sync lost !
		PrintfA(( pfa, "Warning: Unsynced lock - a call to SndManager::Update() is expected ...\n" ));
		return FALSE;
	}
	else
	{
		// => Synced
		return TRUE;
	}
}



void
Sound_Lock		(	uint		inLockId,
					uint		inMode,				// 0:mono-SFX 1:mono-STREAM 2:MUSIC
					uint		inSRamBAddr,
					uint		inSRamBSize,
					uint32		inDataBOffset,
					uint32		inDataBSize		)
{
	Sound* s = & soundA[ inLockId ];
	ASSERT( inLockId < sys_lockNbMax );
	ASSERT( s->state == SND_INVALID );
	ASSERT( (inSRamBSize&63) == 0 );		// 64-bytes aligned !

	PrintfA(( pfa, "Sound_Lock: Enter locking [SPU baddr=0x%x bsize=0x%x] (boffset=0x%08x, bsize=0x%x) ...\n", inSRamBAddr, inSRamBSize, inDataBOffset, inDataBSize ));

	// Music already locked or playing/unlocking ?
	if( inMode == 2 ) {
		uint i;
		for( i = 0 ; i < sys_lockNbMax ; i++ ) {
			if(	Sound_IsMusic(i) ) {
				PrintfA(( pfa, "Sound_Lock: Another music is already locked (boffset=0x%08x) !\n", inDataBOffset ));
				RetLockSuccess( s, FALSE );		// music is busy => lock failure !
				return;
			}
		}
		ASSERT( !SPU2_IsPlaying(MUSIC_CH) );
	}

	// Lock stream !
	if(	!Stream_Lock() ) {
		PrintfA(( pfa, "Sound_Lock: Stream_Lock() has failed (boffset=0x%08x) !\n", inDataBOffset ));
		RetLockSuccess( s, FALSE );
		return;
	}

	// Prepare for locking
	s->mode					= inMode;
	s->sramAddr				= inSRamBAddr;
	s->sramBSize			= inSRamBSize;
	s->data.bOffset			= inDataBOffset;
	s->data.bSize			= inDataBSize;
	s->loadPos				= 0;
	s->sramWPos				= 0;
	s->streamId				= -1;
	s->streamCh				= -1;
	s->streamChPlay			= 0;
	s->streamWrap			= 0;
	s->streamTmp			= NULL;
	s->nextState			= SEG_UNDEF;

	if( inMode == 0 )
	{
		// spot-fx entirely in SRAM !
		ASSERT( inDataBSize <= inSRamBSize );
	}
	else if( inMode == 1 )
	{
		// 2 * 64-bytes aligned !	
		ASSERT( (inSRamBSize&127) == 0 );
		s->data.bSize = (inDataBSize>>8)<<8;
	}
	else if( inMode == 2 )
	{
		// 4 * 64-bytes aligned !
		ASSERT( (inSRamBSize&255) == 0 );
		s->data.bSize = (inDataBSize>>8)<<8;
		// music uninterleave buffers
		s->streamTmp = Malloc( inSRamBSize>>2 );
		if( !s->streamTmp ) {
			Free( s->streamTmp );
			Stream_Unlock();
			PrintfA(( pfa, "Sound_Lock: Music iop-buffer allocation has failed (boffset=0x%x08) !\n", inDataBOffset ));
			RetLockSuccess( s, FALSE );
			return;
		}
		PrintfA(( pfa, "Sound_Lock: Allocation of %d bytes for music streaming (boffset=0x%x08) !\n", (inSRamBSize>>2), inDataBOffset ));
	}

	// locking ...
	s->state = SND_LOCKING;
}



void
Sound_Unlock		(	uint		inLockId		)
{
	Sound* s = & soundA[ inLockId ];

	// Assume locked ...
	if( !Sound_CheckLockSync(inLockId) )
		return;

	UnlockSound( s, TRUE );
}



void
Sound_Start			(	uint		inLockId,
						uint		inChannel,
						uint		inPitch,
						uint		inVolL,
						uint		inVolR		)
{
#ifdef ENABLE_PRINTF
	uint32 envx0  = SPU2_GetENVX_Core0();
	uint32 envx1  = SPU2_GetENVX_Core1();
	uint32 envxLo = (envx1<<24) | envx0;
	uint32 envxHi = (envx1>> 8);
#endif
	Sound* s = & soundA[ inLockId ];
	ASSERT( inChannel < 48 );
	ASSERT( inChannel != MUSIC_CH );

	// Assume locked ...
	if( !Sound_CheckLockSync(inLockId) )
		return;

	// Streamed -> store the channel ?
	if( s->mode != 0 ) {
		ASSERT( s->streamCh < 0 );
		s->streamCh = inChannel;
		s->streamChPlay = 0;
	}

	if( s->mode == 2 )
	{
		// two channels for stereo
		uint hbS  = s->sramBSize >> 2;
		uint hbL0 = s->sramAddr + hbS*0;
		uint hbL1 = s->sramAddr + hbS*1;
		uint hbR0 = s->sramAddr + hbS*2;
		uint hbR1 = s->sramAddr + hbS*3;

		SPU2_WriteSdCmd( SD_VP_PITCH,inChannel,	inPitch );
		SPU2_WriteSdCmd( SD_VP_VOLL, inChannel,	inVolL	);
		SPU2_WriteSdCmd( SD_VP_VOLR, inChannel,	0		);
		SPU2_WriteSdCmd( SD_VA_SSA,  inChannel, hbL0	);
		SPU2_WriteSdCmd( SD_VA_LSAX, inChannel, hbL0	);
		SPU2_WriteSdCmd( SD_S_KON,   inChannel, 1		);

		SPU2_WriteSdCmd( SD_VP_PITCH,MUSIC_CH,	inPitch );
		SPU2_WriteSdCmd( SD_VP_VOLL, MUSIC_CH,	0		);
		SPU2_WriteSdCmd( SD_VP_VOLR, MUSIC_CH,	inVolR	);
		SPU2_WriteSdCmd( SD_VA_SSA,  MUSIC_CH,	hbR0	);
		SPU2_WriteSdCmd( SD_VA_LSAX, MUSIC_CH,	hbR0	);
		SPU2_WriteSdCmd( SD_S_KON,   MUSIC_CH,	1		);
		PrintfA(( pfa, "Sound_Start: Starts channels <L=%d,R=%d> (ENVX=%04x%08x)\n", inChannel, MUSIC_CH, envxHi, envxLo ));
	}
	else
	{
		// one mono channel
		uint ssa = s->sramAddr;
		SPU2_WriteSdCmd( SD_VP_PITCH,inChannel,	inPitch );
		SPU2_WriteSdCmd( SD_VP_VOLL, inChannel,	inVolL	);
		SPU2_WriteSdCmd( SD_VP_VOLR, inChannel,	inVolR	);
		SPU2_WriteSdCmd( SD_VA_SSA,	 inChannel, ssa		);
		SPU2_WriteSdCmd( SD_VA_LSAX, inChannel, ssa		);
		SPU2_WriteSdCmd( SD_S_KON,	 inChannel, 1		);
		PrintfA(( pfa, "Sound_Start: Starts channel <%d> (ENVX=%04x%08x)\n", inChannel, envxHi, envxLo ));
	}
}



void
Sound_Stop			(	uint		inLockId,
						uint		inChannel		)
{
	Sound* s = & soundA[ inLockId ];
	ASSERT( SPU2_IsValidChannel(inChannel) );

	// Assume locked ...
	if( !Sound_CheckLockSync(inLockId) )
		return;

	StopSoundChannel( s, inChannel );

	// Streamed => auto-unlock
	if( Sound_IsStreamed(inLockId) )
		UnlockSound( s, TRUE );
}



void
Sound_SetVolume		(	uint		inLockId,
						uint		inChannel,
						uint		inVolL,
						uint		inVolR		)
{
	Sound* s = & soundA[ inLockId ];
	ASSERT( inChannel < 48 );
	ASSERT( inChannel != MUSIC_CH );

	// Assume locked ...
	if( !Sound_CheckLockSync(inLockId) )
		return;

	// Streamed ?
	if( s->mode ) {
		ASSERT( s->streamCh == inChannel );
	}

	if( s->mode == 2 )
	{
		SPU2_WriteSdCmd( SD_VP_VOLL, inChannel,	inVolL	);
		SPU2_WriteSdCmd( SD_VP_VOLR, inChannel,	0		);
		SPU2_WriteSdCmd( SD_VP_VOLL, MUSIC_CH,	0		);
		SPU2_WriteSdCmd( SD_VP_VOLR, MUSIC_CH,	inVolR	);
	}
	else
	{
		SPU2_WriteSdCmd( SD_VP_VOLL, inChannel,	inVolL	);
		SPU2_WriteSdCmd( SD_VP_VOLR, inChannel,	inVolR	);
	}
}



void
Sound_SetPitch		(	uint		inLockId,
						uint		inChannel,
						uint		inPitch		)
{
	Sound* s = & soundA[ inLockId ];
	ASSERT( inChannel < 48 );
	ASSERT( inChannel != MUSIC_CH );

	// Assume locked ...
	if( !Sound_CheckLockSync(inLockId) )
		return;

	// Streamed ?
	if( s->mode ) {
		ASSERT( s->streamCh == inChannel );
	}

	if( s->mode == 2 )
	{
		SPU2_WriteSdCmd( SD_VP_PITCH,inChannel,	inPitch );
		SPU2_WriteSdCmd( SD_VP_PITCH,MUSIC_CH,	inPitch );
	}
	else
	{
		SPU2_WriteSdCmd( SD_VP_PITCH,inChannel,	inPitch );
	}
}



void
Sound_Break			(	uint		inLockId			)
{
	Sound* s = & soundA[ inLockId ];
	PrintfA(( pfa, "Sound_Break: %d !\n", inLockId ));

	// in lock ...
	if( !Sound_IsInLock(inLockId) )
		return;

	// no streamed ?
	if( s->mode == 0 ) {
		PrintfA(( pfa, "Sound_Break: Invalid operation on no-streamed sound !\n" ));
		return;
	}

	s->nextState = SEG_UNDEF;
	AskNextSegment( s, SEG_BREAK_ASKING );
}



void
Sound_NoSegment		(	uint		inLockId			)
{
	Sound* s = & soundA[ inLockId ];

	// in lock ...
	if( !Sound_IsInLock(inLockId) )
		return;

	// no streamed ?
	if( s->mode == 0 ) {
		PrintfA(( pfa, "Sound_NoSegment: Invalid operation on no-streamed sound !\n" ));
		return;
	}

	if( s->nextState == SEG_BREAK_ASKING )
	{
		s->nextState = SEG_UNDEF;
		Sound_Stop( inLockId, s->streamCh );
		PrintfA(( pfa, "Sound_NoSegment: lid:%d, SEG_BREAK_ASKING\n", inLockId ));
	}
	else if( s->nextState == SEG_LOOP_ASKING )
	{
		s->nextState = SEG_NOMORE;
		PrintfA(( pfa, "Sound_NoSegment: lid:%d, SEG_LOOP_ASKING\n", inLockId ));
	}
	else
	{
		// ignored !
		PrintfA(( pfa, "Sound_NoSegment: lid:%d, ignored !\n", inLockId ));
	}
}



void
Sound_SetSegment	(	uint		inLockId,
						uint32		inDataBOffset,
						uint32		inDataBSize			)
{
	Sound* s = & soundA[ inLockId ];

	// in lock ...
	if( !Sound_IsInLock(inLockId) )
		return;

	// no streamed ?
	if( s->mode == 0 ) {
		PrintfA(( pfa, "Sound_SetSegment: Invalid operation on no-streamed sound !\n" ));
		return;
	}

	if( s->nextState == SEG_BREAK_ASKING )
	{
		// ASAP
		s->loadPos		= 0;
		s->data.bOffset	= inDataBOffset;
		s->data.bSize	= (inDataBSize>>8)<<8;
		s->nextState	= SEG_UNDEF;
		PrintfA(( pfa, "Sound_SetSegment: lid:%d, SEG_BREAK_ASKING\n", inLockId ));
	}
	else if( s->nextState == SEG_LOOP_ASKING )
	{
		// push to next segment
		s->nextSegment.bOffset	= inDataBOffset;
		s->nextSegment.bSize	= (inDataBSize>>8)<<8;
		s->nextState			= SEG_AVAILABLE;
		PrintfA(( pfa, "Sound_SetSegment: lid:%d, SEG_LOOP_ASKING, new segment (boffset=0x%08x, bsize=0x%x)\n", inLockId, inDataBOffset, inDataBSize ));
	}
	else
	{
		// ignored !
		PrintfA(( pfa, "Sound_SetSegment: lid:%d, ignored !\n", inLockId ));
	}
}




static
void
SpotFX_Update	(	Sound*		s	)
{
	uint remain, toload;
	ASSERT( s->state != SND_INVALID );
	ASSERT( s->mode == 0 );

	// locking ?
	if( s->state != SND_LOCKING )
		return;

	// loading ?
	if( !CheckLoadingContinue(s) )
		return;

	// remain to load
	remain = GetRemainLoading( s );
	toload = MIN( remain, sys_reqMaxBSize );

	// completed ?
	if( toload == 0 ) {
		RetLockSuccess( s, TRUE );
		s->state = SND_LOCKED;
		return;
	}

	// load ?
	StartLoading( s, toload, NULL );
}






static
void
StreamFX_CB	(	uint32	inAddr,
				uint32	inBSize,
				pvoid	inCBParam	)
{
	// Callback called after stream packet is LOADED and before TRXing.
	Sound*		s	= (Sound*)inCBParam;
	SPU2_Block* b0	= (SPU2_Block*) inAddr;
	SPU2_Block*	b1	= b0 + (inBSize>>4);
	ASSERT( sizeof(SPU2_Block) == 16 );
	ASSERT( (inBSize&15) == 0 );

	if( IsEndOfLoading(s) )
	{
		// final marker to stop the voice (auto KeyOff)
		SetAdpcmFinalMark( b1, inBSize );
	//	PrintfA(( pfa, "Stream_CB: set final marker !\n" ));
	}
	else
	{
		// loop marker if the writing position has looped !
		if( s->sramWPos == 0 )
			(b1-1)->loopflag = LF_LOOP|LF_END;
	}
}




static
void
Music_CB	(	uint32	inAddr,
				uint32	inBSize,
				pvoid	inCBParam	)
{
	// Callback called after stream packet is LOADED and before TRXing.
	Sound*		s	 = (Sound*)inCBParam;
	uint		nb	 = inBSize>>5;
	SPU2_Block* b0	 = (SPU2_Block*) inAddr;
	SPU2_Block* b1	 = b0 + nb*2;
	SPU2_Block* L0, * R0;
	ASSERT( sizeof(SPU2_Block) == 16 );
	ASSERT( (inBSize&15) == 0 );

	// Un-Interleaved
	{
		SPU2_Block* b = b0;
		SPU2_Block* L = b0;
		SPU2_Block* R = (SPU2_Block*) s->streamTmp;
		while( b < b1 ) {
			*L++ = b[0];
			*R++ = b[1];
			b += 2;
		}
		L0 = b0;
		R0 = L0 + nb;
		memcpy( R0, s->streamTmp, nb*16 );
	}

	if( IsEndOfLoading(s) )
	{
		// final marker to stop the voice (auto KeyOff)
		SetAdpcmFinalMark( L0+nb, inBSize/2 );
		SetAdpcmFinalMark( R0+nb, inBSize/2 );
	}
	else
	{
		// loop marker if the writing position has looped !
		if( s->sramWPos == 0 ) {
			L0[nb-1].loopflag = LF_LOOP|LF_END;
			R0[nb-1].loopflag = LF_LOOP|LF_END;
		}
	}
}




static
void
Stream_Update	(	Sound*		s	)
{
	uint remain, tofill, toload = 0;
	ASSERT( s->state != SND_INVALID );
	ASSERT( s->mode > 0 );

	if( !CheckLoadingContinue(s) )
		return;

	remain = GetRemainLoading( s );
	tofill = GetStream_ToLoad( s );
	toload = MIN( tofill, remain );
	toload = MIN( toload, sys_reqMaxBSize );

	// locking ?
	if( s->state == SND_LOCKING )
	{
		// completed ?
		if( toload==0 )
		{
			RetLockSuccess( s, TRUE );
			s->state = SND_LOCKED;
			PrintfA(( pfa, "Stream_Update: Locked (boffset=0x%08x) !\n", s->data.bOffset ));
			return;
		}
	}
	else if( s->state == SND_LOCKED )
	{
		int  ch;
		bool playing;

		ch = s->streamCh;
		playing = (ch>=0) && SPU2_IsValidChannel(ch) && SPU2_IsPlaying(ch);
		if( playing )
			s->streamChPlay++;

		// completed ?
		if( (s->streamChPlay>0) && IsEndOfLoading(s) )
		{
			// Auto KeyOff is done by the VAG final marker !
			// Waits KeyOff before auto-unlock !
			if( !playing ) {
				PrintfA(( pfa, "Stream_Update: Auto-unlock (boffset=0x%08x) !\n", s->data.bOffset ));
				UnlockSound( s, TRUE );
			} else {
				PrintfA(( pfa, "Stream_Update: Wait channel %d auto-keyoff (boffset=0x%08x) !\n", s->streamCh, s->data.bOffset ));
			}
			return;
		}
	}

	// load ?
	if( toload )
	{
		if( IsLastLoading(s,toload) )
		{
			AskNextSegment( s, SEG_LOOP_ASKING );
			if( s->nextState == SEG_LOOP_ASKING )
				return;		// waiting for the loop segment ...
		}

		if( s->mode== 1 )
			StartLoading( s, toload, StreamFX_CB );
		else
			StartLoading( s, toload, Music_CB );
	}
}




void
Sound_Update	(			)
{
	uint i;
	for( i = 0 ; i < sys_lockNbMax ; i++ )
	{
		Sound* s = soundA + i;

		// locked ?
		if( s->state == SND_INVALID )
			continue;

		if( s->mode > 0 )	Stream_Update( s );
		else				SpotFX_Update	( s );
	}
}



