/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifndef _Core_SPU2_H_
#define	_Core_SPU2_H_



bool	SPU2_Init				(								);
void	SPU2_Reset				(								);
void	SPU2_Shut				(								);

void	SPU2_EnableCB			(	int			inSemId			);
void	SPU2_DisableCB			(								);

// Stats
bool	SPU2_IsValidChannel		(	uint		inChannel		);
bool	SPU2_IsPlaying			(	uint		inChannel		);
uint	SPU2_GetNAX				(	uint		inChannel		);
void	SPU2_SetLSAX			(	uint		inChannel,
									uint		inSRamAddr		);
uint32	SPU2_GetENVX_Core0		(								);	// Core0 ENVX reg
uint32	SPU2_GetENVX_Core1		(								);	// Core1 ENVX reg
uint32	SPU2_GetENDX_Core0		(								);	// Core0 ENDX reg
uint32	SPU2_GetENDX_Core1		(								);	// Core1 ENDX reg
uint32	SPU2_GetLastDown_Core0	(								);	// Core0 last down voices (1->0)
uint32	SPU2_GetLastDown_Core1	(								);	// Core1 last down voices (1->0)

// Sd commands
void	SPU2_BeginSdCmd			(								);
void	SPU2_WriteSdCmd			(	uint		inReg,
									uint		inChOrCore,
									uint		inValue			);
void	SPU2_EndSdCmd			(								);

// PCM playback
bool	SPU2_StartPcmPlayback	(	uint		inBufferBSize	);
void	SPU2_StopPcmPlayback	(								);
bool	SPU2_GetPcmPlaybackFill	(	uint  *		outLen,
									int16 **	outPtr			);
void	SPU2_SetPcmPlaybackFill	(	uint		inLen			);

// TRX to SRAM
bool	SPU2_StartTRX			(	uint32		inSPU2BufferPtr,	// 16 bytes aligned !
									uint32		inIOPBufferPtr,		// 4 bytes aligned !
									uint32		inBSize			);	// 64 bytes blocks are trxed !
bool	SPU2_IsTRXReady			(								);
void	SPU2_SyncTRX			(								);





typedef struct {
	uint8			decparam;
	uint8			loopflag;
	uint16			sd[7];
} SPU2_Block;

enum {
	LF_MASK		=	7,
	LF_START	=	(1<<2),
	LF_LOOP		=	(1<<1),
	LF_END		=	(1<<0)
};




#endif


