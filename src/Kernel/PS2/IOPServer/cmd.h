/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _NVIOP_CMD_H_
#define _NVIOP_CMD_H_



enum
{
	NVIOP_VERSION				=	12,
	NVIOP_BIND_ID				=	MCC32('N','V','I','O'),
	NVIOP_RPC_CMD_BSIZE			=	2048,		// RPC command buffers in bytes
	NVIOP_RPC_TH_PRIORITY		=	50,			// Normal user from 9 (highest) to 123 (lowest) (this pri. must be >= than update & net thread)
	NVIOP_TH_STACK_BSIZE		=	2048,		// Stack bytes-size for all threads

	//
	// Main functions

	NVIOP_FCT_INIT				=	0,
	NVIOP_FCT_RESET				=	1,
	NVIOP_FCT_SHUT				=	2,
	NVIOP_FCT_OPEN_FILE			=	3,
	NVIOP_FCT_CLOSE_FILE		=	4,
	NVIOP_FCT_CMD_BATCH			=	5,

	//
	// Commands

	NVIOP_LOAD_DATA				=	10,

	NVIOP_SD_LOCK				=	20,
	NVIOP_SD_UNLOCK				=	21,
	NVIOP_SD_CMD				=	22,
	NVIOP_SD_START				=	23,
	NVIOP_SD_STOP				=	24,
	NVIOP_SD_BREAK				=	25,
	NVIOP_SD_SETVOL				=	26,
	NVIOP_SD_SETPITCH			=	27,
	NVIOP_SD_ASKSEGMENT			=	28,
	NVIOP_SD_SETSEGMENT			=	29,
	NVIOP_SD_NOSEGMENT			=	30,

	NVIOP_MEM_ALLOC				=	40,
	NVIOP_MEM_FREE				=	41,

	NVIOP_STATUS				=	50,

	NVIOP_EOP					=	255			// End of Packet
};




#endif // _NVIOP_CMD_H_


