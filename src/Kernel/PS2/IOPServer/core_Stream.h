/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifndef _Core_Stream_H_
#define	_Core_Stream_H_


//
// Based on core_File.h !
//


typedef enum {
	STREAM_TO_EE	=	0,
	STREAM_TO_IOP	=	1,
	STREAM_TO_SPU2	=	2
} StreamTarget;


typedef void (*StreamLoadedCB)	(	uint32			inAddr,
									uint32			inBSize,
									pvoid			inCBParam		);


bool	Stream_Init				(	uint32			inCacheBSize	);
void	Stream_Reset			(									);
void	Stream_Shut				(									);
void	Stream_Update			(									);
bool	Stream_IsIdle			(									);
bool	Stream_Lock				(									);
bool	Stream_Unlock			(									);

// Append a stream request :
// This is composed of :
// 1- Loading a data packet from file.
// 2- Calling the optionnal callback when data are loaded.
//	  This CB can modify the data packet on the fly and before TRXing (3).
// 3- TRXing the data packet to the target RAM.
int		Stream_AddRequest		(	StreamTarget	inTarget,
									uint32			inTargetAddr0,
									uint32			inTargetAddr1,
									uint32			inBOffset,
									uint32			inBSize,
									StreamLoadedCB	inCB,
									pvoid			inCBParam		);

// If returns TRUE, the inReqId becomes invalid and cache buffer is released.
bool	Stream_IsDone			(	int				inReqId,
									bool*			outHasFailed	);

// Syncing ...
void	Stream_Sync				(	int				inReqId,
									bool*			outHasFailed	);
void	Stream_SyncAll			(									);


#endif


