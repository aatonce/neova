/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "sys.h"
#include "core.h"
#include "core_EE.h"
#include "core_SPU2.h"
#include "core_File.h"
#include "core_Stream.h"



#define	MAX_REQUEST		16



typedef enum {
	RS_PENDING	= 0,
	RS_LOADING,
	RS_LOADED,
	RS_TRXING,
	RS_DONE,
	RS_ERROR
} StrState;


typedef struct {
	StrState		state;
	StreamLoadedCB	cb;
	pvoid			cbParam;
	// From, To
	StreamTarget	target;
	uint32			targetAddr0;
	uint32			targetAddr1;
	uint32			bSize;			// user bsize
	uint32			bOffset;		// aligned offset
	uint32			bPadding;		// alignment padding in first page (bOffset+bPadding = user bOffset)
	// paging
	int				p0;				// start page no
	int				pcpt;			// used page cpt
	// page mapping link order from top -> bottom
	int				prev;
	int				next;
} StrReq;


static uint8*		pageA;
static int			pageCpt;

static StrReq		reqA[MAX_REQUEST];
static int			pagemapping_req_head;
static int			reqFreeQ[MAX_REQUEST];
static int			reqBusyQ[MAX_REQUEST];
static int			reqBusyQCpt;



static
bool FindPageHole	(	uint	inNbPage,
						int*	outP0,
						int*	outPrevReq,
						int*	outNextReq	)
{
	int	p0, r, prev;
	ASSERT( inNbPage <= pageCpt );

	p0   = 0;
	r	 = pagemapping_req_head;
	prev = -1;

	while( r >= 0 ) {
		ASSERT( r < MAX_REQUEST );
		ASSERT( reqA[r].prev == prev );
		// Sorted !
		ASSERT( reqA[r].p0 >= p0 );
		if( (reqA[r].p0-p0) >= inNbPage ) {
			// found between r.prev & r
			*outP0	    = p0;
			*outPrevReq = prev;
			*outNextReq = r;
			return TRUE;
		}
		p0   = reqA[r].p0 + reqA[r].pcpt;
		prev = r;
		r    = reqA[r].next;
		ASSERT( p0 <= pageCpt );
	}

	if( (pageCpt-p0) >= inNbPage ) {
		// found between last & end
		*outP0	    = p0;
		*outPrevReq = prev;
		*outNextReq = -1;
		return TRUE;
	}

	// not found
	return FALSE;
}


static
int	AllocRequest	(	uint	inNbPage	)
{
	int p0, prev, next, r;

	// full ?
	if( reqBusyQCpt == MAX_REQUEST )
		return -1;

	// enough pages free ?
	if( !FindPageHole(inNbPage,&p0,&prev,&next) )
		return -1;

	// Enqueue as busy
	r = reqFreeQ[MAX_REQUEST-reqBusyQCpt-1];
	ASSERT( r >= 0 && r < MAX_REQUEST );
	reqBusyQ[reqBusyQCpt++] = r;

	// partial setup
	reqA[r].state	= RS_PENDING;
	reqA[r].p0		= p0;
	reqA[r].pcpt	= inNbPage;
	reqA[r].prev	= prev;
	reqA[r].next	= next;

	// link page mapping order
	if( prev == -1 )	pagemapping_req_head = r;
	else				reqA[prev].next = r;
	if( next != -1 )	reqA[next].prev = r;

	return r;
}


static
void ReleaseRequest	(	int		inReqId		)
{
	int r = inReqId, i, prev, next;

	ASSERT( r >= 0 );
	ASSERT( r < MAX_REQUEST );
	ASSERT( reqBusyQCpt > 0 );
	ASSERT( reqBusyQCpt <= MAX_REQUEST );

	// Enqueue as free
	reqFreeQ[MAX_REQUEST-reqBusyQCpt] = r;
	// Dequeue from busy
	for( i = 0 ; i < reqBusyQCpt ; i++ )
		if( reqBusyQ[i] == r )	break;		// find ..
	ASSERT( i < reqBusyQCpt );				// found !
	for( ; i < reqBusyQCpt-1 ; i++ )
		reqBusyQ[i] = reqBusyQ[i+1];		// remove from queue
	reqBusyQCpt--;

	// unlink page mapping order
	prev = reqA[r].prev;
	next = reqA[r].next;
	if( prev == -1 )	pagemapping_req_head = next;
	else				reqA[prev].next = next;
	if( next != -1 )	reqA[next].prev = prev;
}


static
bool StartLoading	(	StrReq*		inReq	)
{
	uint8*	bufferPtr = pageA + ((inReq->p0)<<11);
	uint32	rdPadding, rdBSize, wrBSize;
	bool	done;

	ASSERT( inReq );
	ASSERT( inReq->state == RS_PENDING );
	ASSERT( File_IsReady() );

	done = File_Read(	(pvoid)bufferPtr,
						inReq->bOffset,
						inReq->bSize+inReq->bPadding,
						&rdPadding,
						&rdBSize,
						&wrBSize );

	if( done ) {
		uint32 wrBSizeMax = (inReq->pcpt)<<11;
		ASSERT( rdPadding == 0 );								// padding is already managed !
		ASSERT( rdBSize == (inReq->bSize+inReq->bPadding) );	// required bsize !
		ASSERT( wrBSize <= wrBSizeMax );						// maximum bsize that can be loaded !
	}

	inReq->state = done ? RS_LOADING : RS_ERROR;
	return done;
}


static
bool StartTRXing	(	StrReq*		inReq	)
{
	uint32  srcPage  = (uint32)pageA + ((inReq->p0)<<11);
	uint32	srcPtr   = srcPage + inReq->bPadding;
	uint32	srcBSize = inReq->bSize;
	uint32	t0		 = inReq->targetAddr0;
	uint32	t1		 = inReq->targetAddr1;
	bool done;
	ASSERT( inReq );
	ASSERT( inReq->state == RS_LOADED );
	ASSERT( srcPage <= srcPtr );
	ASSERT( t0 ||t1 );

	// Align data to 4bytes for DMA trxing
	// (if not aligned -> reset padding)
	if( (srcPtr&3) && (inReq->target!=STREAM_TO_IOP) )
	{
		ASSERT( (srcPage&3) == 0 );		// pages always aligned to 4bytes !
		memmove( (pvoid)srcPage, (pvoid)srcPtr, srcBSize );
		srcPtr = srcPage;
		inReq->bPadding = 0;
	}

	// Call CB ?
	if( inReq->cb )
	{
		inReq->cb( srcPtr, srcBSize, inReq->cbParam );
		inReq->cb = NULL;
	}

	// TRX to IOP ?
	if( inReq->target == STREAM_TO_IOP )
	{
		if( t0 && t1 ) {
			memcpy( (pvoid)t0, (pvoid)srcPtr,			   srcBSize/2 );
			memcpy( (pvoid)t1, (pvoid)(srcPtr+srcBSize/2), srcBSize/2 );
		} else {
			memcpy( (pvoid)t0, (pvoid)srcPtr, srcBSize );
		}
		inReq->state = RS_DONE;
		return TRUE;
	}
	else
	{
		// TRX to EE or SPU2
		uint32 t, s, sbs;
		if( t0 && t1 ) {
			t   = t0;
			s   = srcPtr;
			sbs = srcBSize/2;
			inReq->targetAddr0 = 0;
		} else if( t1 ) {
			t   = t1;
			s   = srcPtr+srcBSize/2;
			sbs = srcBSize/2;
			inReq->targetAddr1 = 0;
		} else {
			t   = t0;
			s   = srcPtr;
			sbs = srcBSize;
			inReq->targetAddr0 = 0;
		}
		if( inReq->target == STREAM_TO_EE ) {
			ASSERT( EE_IsTRXReady() );
			done = EE_StartTRX( t, s, sbs );
		} else {
			ASSERT( SPU2_IsTRXReady() );
			done = SPU2_StartTRX( t, s, sbs );
		}
		inReq->state = done ? RS_TRXING : RS_ERROR;
		return done;
	}

	ASSERT( FALSE );	// Invalid target !
	return FALSE;
}






bool
Stream_Init		(	uint		inCacheBSize	)
{
	int r;

	pageCpt = (inCacheBSize+2047)>>11;
	if( pageCpt < 8 )
		return FALSE;	// at least 8 pages of 2Ko

	// Alloc pages
	pageA = (uint8*) Malloc( pageCpt<<11 );
	if( !pageA )	return FALSE;
	ASSERT( (((uint)pageA)&0x3) == 0 );	// 4 bytes aligned !
	if( ((uint)pageA)&0x3 ) {
		Free( pageA );
		return FALSE;
	}

	// Init Requests queues
	pagemapping_req_head = -1;
	reqBusyQCpt = 0;
	for( r = 0 ; r < MAX_REQUEST ; r++ )
		reqFreeQ[r] = r;

	return TRUE;
}


void
Stream_Reset	(			)
{
	//
}


void
Stream_Shut		(			)
{
	if( pageA ) {
		Stream_SyncAll();
		Free( pageA );
		pageA = NULL;
	}
}


void
Stream_Update	(			)
{
	bool file_isReady, ee_isReady, spu2_isReady;
	int  i, pass;
	if( reqBusyQCpt == 0 )
		return;

	if( !File_IsOpened() ) {
		Printf( "Stream_Update error: streamed file is closed !\n" );
		return;
	}

	// Get async states
	file_isReady = File_IsReady();
	ee_isReady	 = EE_IsTRXReady();
	spu2_isReady = SPU2_IsTRXReady();

	// All busy -> nothing to do ...
	if( !file_isReady )
	if( !ee_isReady   )
	if( !spu2_isReady )
		return;

	for( pass = 0 ; pass < 3 ; pass++ )
	{
		for( i = 0 ; i < reqBusyQCpt ; i++ )
		{
			int		 	 r	 = reqBusyQ[i];
			StrState	 stt = reqA[r].state;
			StreamTarget trg = reqA[r].target;

			// PASS0: update HW-waiting states
			if( pass == 0 )
			{
				if( file_isReady && stt == RS_LOADING )
				{
					if( File_IsError() )
					{
						reqA[r].state = RS_ERROR;
					}
					else
					{
						reqA[r].state = RS_LOADED;
					}
				}
				else if( stt == RS_TRXING )
				{
					ASSERT( trg != STREAM_TO_IOP );
					if(	trg==STREAM_TO_EE && ee_isReady )
					{
						reqA[r].state = reqA[r].targetAddr1 ? RS_LOADED : RS_DONE;
					}
					else if( trg==STREAM_TO_SPU2 && spu2_isReady )
					{
						reqA[r].state = reqA[r].targetAddr1 ? RS_LOADED : RS_DONE;
					}
				}
			}

			// PASS1: start loading
			else if( pass == 1 )
			{
				if( file_isReady && stt == RS_PENDING )
				{
					file_isReady = StartLoading(&reqA[r]) ? FALSE : TRUE;
				}
			}

			// PASS2: dma trxing
			else if( pass == 2 )
			{
				if( stt == RS_LOADED )
				{
					if( trg == STREAM_TO_EE && ee_isReady )
					{
						ee_isReady = StartTRXing(&reqA[r]) ? FALSE : TRUE;
					}
					else if( trg == STREAM_TO_SPU2 && spu2_isReady )
					{
						spu2_isReady = StartTRXing(&reqA[r]) ? FALSE : TRUE;
					}
					else if( trg == STREAM_TO_IOP )
					{
						StartTRXing( &reqA[r] );
					}
				}
			}
		}
	}
}


bool
Stream_IsIdle		(			)
{
	return ( reqBusyQCpt == 0 );
}


int
Stream_AddRequest	(	StreamTarget	inTarget,
						uint32			inTargetAddr0,
						uint32			inTargetAddr1,
						uint32			inBOffset,
						uint			inBSize,
						StreamLoadedCB	inCB,
						pvoid			inCBParam		)
{
	uint32	boffset, bpadding;
	uint	nbpages;
	int		r;

	if( !File_IsOpened() )
		return -1;

	// valid ?
	if( inTargetAddr0&0xF ) {
		Printf( "Stream_AddRequest: target not 16-aligned !\n" );
		return -1;
	}
	if( inTargetAddr1&0xF ) {
		Printf( "Stream_AddRequest: target not 16-aligned !\n" );
		return -1;
	}
	if( inBSize == 0 ) {
		Printf( "Stream_AddRequest: invalid inBSize == 0 !\n" );
		return -1;
	}
	if( inBOffset >= File_GetBSize() ) {
		Printf( "Stream_AddRequest: inBOffset is out of range !\n" );
		return -1;
	}

	// setup req
	boffset  = inBOffset & (~2047);					// sector alignement for loading
	bpadding = inBOffset & 2047;					// padding for loading
	nbpages  = ( inBSize + bpadding + 2047 ) >> 11;	// # pages

	r = AllocRequest( nbpages );
	if( r < 0 )
		return -1;	// full !

	reqA[r].target		= inTarget;
	reqA[r].targetAddr0	= inTargetAddr0;
	reqA[r].targetAddr1	= inTargetAddr1;
	reqA[r].bSize		= inBSize;
	reqA[r].bOffset		= boffset;
	reqA[r].bPadding	= bpadding;
	reqA[r].cb			= inCB;
	reqA[r].cbParam		= inCBParam;
	return r;
}


bool
Stream_Lock		(			)
{
	return File_Lock();
}


bool
Stream_Unlock	(			)
{
	return File_Unlock();
}


bool
Stream_IsDone		(	int				inReqId,
						bool*			outHasFailed	)
{
	StrState stt;

	ASSERT( inReqId >= 0 );
	ASSERT( inReqId < MAX_REQUEST );
	ASSERT( reqBusyQCpt > 0 );
	ASSERT( reqBusyQCpt <= MAX_REQUEST );

	stt = reqA[inReqId].state;

	if( stt == RS_DONE || stt == RS_ERROR ) {
		ReleaseRequest( inReqId );
		if( outHasFailed )
			*outHasFailed = (stt == RS_ERROR);
		return TRUE;
	}

	return FALSE;
}


void
Stream_Sync			(	int				inReqId,
						bool*			outHasFailed	)
{
	while( !Stream_IsDone(inReqId,outHasFailed) ) {
		DelayThread( 100 );
		Stream_Update();
	}
}


void
Stream_SyncAll		(			)
{
	while( reqBusyQCpt )
		Stream_Sync( reqBusyQ[0], NULL );
}



