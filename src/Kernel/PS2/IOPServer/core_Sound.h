/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _Core_Sound_H_
#define	_Core_Sound_H_



bool	Sound_Init			(	uint		inLockNbMax,
								uint		inReqMaxBSize		);		// must be multiple of 128 !
void	Sound_Shut			(									);
void	Sound_Update		(									);


// ...
bool	Sound_IsValid		(	uint		inLockId			);
bool	Sound_IsLocked		(	uint		inLockId			);
bool	Sound_IsLocking		(	uint		inLockId			);
bool	Sound_IsInLock		(	uint		inLockId			);
bool	Sound_IsStreamed	(	uint		inLockId			);
bool	Sound_IsMusic		(	uint		inLockId			);
bool	Sound_CheckLockSync	(	uint		inLockId			);

void	Sound_Lock			(	uint		inLockId,
								uint		inMode,						// 0:mono-SFX 1:mono-STREAM 2:MUSIC
								uint		inSRamBAddr,
								uint		inSRamBSize,
								uint32		inDataBOffset,
								uint32		inDataBSize			);

void	Sound_Unlock		(	uint		inLockId			);


// ...
void	Sound_Start			(	uint		inLockId,
								uint		inChannel,
								uint		inPitch,
								uint		inVolL,
								uint		inVolR				);

void	Sound_Stop			(	uint		inLockId,
								uint		inChannel			);

void	Sound_SetVolume		(	uint		inLockId,
								uint		inChannel,
								uint		inVolL,
								uint		inVolR				);

void	Sound_SetPitch		(	uint		inLockId,
								uint		inChannel,
								uint		inPitch				);

void	Sound_Break			(	uint		inLockId			);

void	Sound_SetSegment	(	uint		inLockId,
								uint32		inDataBOffset,
								uint32		inDataBSize			);

void	Sound_NoSegment		(	uint		inLockId			);





#endif



