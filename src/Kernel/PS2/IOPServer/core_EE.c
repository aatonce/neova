/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "sys.h"
#include "core.h"
#include "core_EE.h"


static uint				sifDmaId = 0;
static sceSifDmaData	sifDmaData;



bool
EE_Init		(			)
{
	sifDmaId = 0;
	return TRUE;
}


void
EE_Reset	(			)
{
	//
}


void
EE_Shut		(			)
{
	EE_SyncTRX();
}


bool
EE_StartTRX	(	uint32	inEEBufferPtr,
				uint32	inIOPBufferPtr,
				uint32	inBSize			)
{
	int	intrStat;

	if( sifDmaId > 0 )
		return FALSE;

	if( inEEBufferPtr&0xF ) {
		Printf( "EE_StartTRX: dest not 16-aligned !\n" );
		return FALSE;
	}
	if( inIOPBufferPtr&0x3 ) {
		Printf( "EE_StartTRX: source not 4-aligned !\n" );
		return FALSE;
	}

	// 16 bytes blocks are trxed !
	inBSize = (inBSize+15U) & (~15U);

	sifDmaData.data = inIOPBufferPtr;
	sifDmaData.addr = inEEBufferPtr;
	sifDmaData.size = inBSize;
	sifDmaData.mode = 0;

	CpuSuspendIntr( &intrStat );
	sifDmaId = sceSifSetDma( &sifDmaData, 1 );
	CpuResumeIntr( intrStat );

	return ( sifDmaId > 0 );
}


bool
EE_IsTRXReady	(	)
{
	int	intrStat, res;

	if( sifDmaId == 0 )
		return TRUE;

	CpuSuspendIntr( &intrStat );
	res = sceSifDmaStat( sifDmaId );
	CpuResumeIntr( intrStat );

	if( res < 0 ) {
		sifDmaId = 0;
		return TRUE;
	}
	else
		return FALSE;
}


void
EE_SyncTRX	(			)
{
	while( !EE_IsTRXReady() )
		DelayThread( 100 );
}




