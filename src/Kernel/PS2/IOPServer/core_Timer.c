/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "sys.h"
#include "core.h"
#include "core_Timer.h"



typedef struct {
	uint	count;			// counter
	uint	interval;		// clock interval
	int		semUpdateId;	// core update semaphore id
} TimerCommon;


static int			timerId = -1;
static TimerCommon	timerCommon;



static
uint
timer_hdl( TimerCommon* common )
{
	common->count++;
	if( common->semUpdateId >= 0 )
		iSignalSema( common->semUpdateId );		// wakeup the main core update thread !
	// returns new compare value !
    return common->interval;
}



bool
Timer_Init	(	uint	inFreq		)
{
	int    res0, res1, res2;
	struct SysClock clock;

	timerId = AllocHardTimer( TC_SYSCLOCK, 32, 1 );
	if( timerId < 0 ) {
		Printf( "Core Timer alloc failed !\n" );
		return FALSE;
	}

	// Freq -> sys clock ticks counter
	USec2SysClock( (1000*1000)/inFreq, &clock );

	res0 = SetTimerHandler(		timerId,
								(u_long)clock.low,
								(pvoid)timer_hdl,
								(pvoid)&timerCommon	);
	res1 = SetOverflowHandler(	timerId,
								(pvoid)timer_hdl,
								(pvoid)&timerCommon	);
	res2 = SetupHardTimer( timerId, TC_SYSCLOCK, TM_NO_GATE, 1 );

	if( timerId<0 || res0!=KE_OK || res1!=KE_OK || res2!=KE_OK ) {
		Printf( "Timer_Init: Setup failed !\n" );
		return FALSE;
	}

	// init common data
	timerCommon.count       = 0;
	timerCommon.interval    = clock.low;
	timerCommon.semUpdateId = -1;
	return TRUE;
}


void
Timer_Shut	(			)
{
	if( timerId >= 0 ) {
		StopHardTimer( timerId );
		FreeHardTimer( timerId );
		timerId = -1;
	}
}


void
Timer_Reset	(			)
{
	//
}


void
Timer_EnableCB		(	int		inSemId		)
{
	if( timerId >= 0 ) {
		int res;
		timerCommon.semUpdateId = inSemId;
		res = StartHardTimer( timerId );
		if( res != KE_OK )
			Printf( "Timer_EnableCB: StartHardTimer() failed !\n" );
	}
}


void
Timer_DisableCB		(			)
{
	if( timerId >= 0 ) {
		StopHardTimer( timerId );
		timerCommon.semUpdateId = -1;
	}
}


uint
Timer_GetCBCounter	(			)
{
	return timerCommon.count;
}



