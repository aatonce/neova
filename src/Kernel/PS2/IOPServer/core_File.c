/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "sys.h"
#include "core.h"
#include "core_File.h"




typedef struct {
	bool		isOpened;
	uint		lockCpt;
	int			hdl;						// sceOpen()
	uint		lsn;						// start sector
	uint32		bSize;						// size in bytes
	uint32		bOffset;					// current head offset in bytes
} File;


static File		c_file;
static int		semUpdateId;



static
void
cdvd_sync_hdl( int /*reason*/ )
{
//	if( reason == SCECdFuncRead || reason == SCECdFuncSeek )
//	for any reason !!!
	if( semUpdateId >= 0 )
		iSignalSema( semUpdateId );
}




bool
File_Init	(	int		inMediaMode		)
{
	c_file.isOpened	= FALSE;
	ASSERT( inMediaMode==SCECdCD || inMediaMode==SCECdDVD );
	sceCdInit( SCECdINIT );
	sceCdMmode( inMediaMode );
	sceCdDiskReady( 0 );
	return TRUE;
}


void
File_Reset	(			)
{
	sceCdDiskReady( 0 );
}


void
File_Shut	(			)
{
	sceCdCallback(0);
	sceCdInit( SCECdEXIT );
}


void
File_EnableCB	(	int		inSemId		)
{
	semUpdateId = inSemId;
	sceCdCallback( cdvd_sync_hdl );
}


void
File_DisableCB	(			)
{
	sceCdCallback( 0 );
}


bool
File_Open	(	pcstr	inFilename	)
{
	if( c_file.isOpened )
		return FALSE;

	// CDVD ?
	if( strncmp(inFilename,"cdrom0:",7)==0 )
	{
		char tmp[ 256 ], * p;
		sceCdlFILE cdFile;
		int res;

		strcpy( tmp, inFilename+7 );
		p = tmp + strlen(tmp) - 2;
		if( strncmp(p,";1",2) != 0 )
			strcat( tmp, ";1" );

		sceCdSync(0x10);
		res = sceCdSearchFile( &cdFile, tmp );
		if( !res )
			return FALSE;
		if( cdFile.size==0 )
			return FALSE;

		c_file.bSize    = cdFile.size;
		c_file.bOffset	= 0;
		c_file.lsn		= cdFile.lsn;
		c_file.hdl		= -1;
	}

	// Others using stdio FS
	else
	{
		// File size
		c_file.hdl = open( inFilename, O_RDONLY );
		if( c_file.hdl < 0 )
			return FALSE;
		c_file.bSize = lseek( c_file.hdl, 0, SEEK_END );
		if( c_file.bSize <= 0 ) {
			close( c_file.hdl );
			return FALSE;
		}

		lseek( c_file.hdl, 0, SEEK_SET );
		c_file.bOffset	= 0;
	}

	c_file.isOpened = TRUE;
	c_file.lockCpt  = 0;
	return TRUE;
}


bool
File_Close()
{
	if( c_file.isOpened ) {
		if( c_file.lockCpt ) {
			Printf( "File_Close: file locked !\n" );
			return FALSE;
		}
		if( c_file.hdl >= 0 ) {
			close( c_file.hdl );
			c_file.hdl = -1;
		}
		c_file.isOpened = FALSE;
	}
	return TRUE;
}


bool
File_Lock		(	)
{
	if( !c_file.isOpened )
		return FALSE;
	c_file.lockCpt++;
	return TRUE;
}


bool
File_Unlock		(	)
{
	if( !c_file.isOpened )
		return FALSE;
	ASSERT( c_file.lockCpt > 0 );
	if( c_file.lockCpt )
		c_file.lockCpt--;
	return TRUE;
}


bool
File_IsLocked	(		)
{
	return c_file.isOpened && (c_file.lockCpt > 0);
}


bool
File_IsOpened	(			)
{
	return c_file.isOpened;
}


uint
File_GetBSize	(			)
{
	return c_file.isOpened ? c_file.bSize : 0;
}


bool
File_IsReady	(			)
{
	if( !c_file.isOpened )
		return TRUE;
	if( c_file.hdl >= 0 )
		return TRUE;
	else
		return ( sceCdSync(0x01) == 0 );
}


bool
File_IsError	(			)
{
	if( !c_file.isOpened )
		return TRUE;
	if( c_file.hdl >= 0 ) {
		return FALSE;
	} else {
		int cderr = sceCdGetError();
		if( cderr == SCECdErNO ) {
			return FALSE;
		} else {
			PrintfA(( pfa, "File_IsError: CD error %d !!\n", cderr ));
			return TRUE;
		}
	}
}


void
File_Sync	(			)
{
	while( !File_IsReady() )
		DelayThread( 1000 );
}


bool
File_Seek	(	uint32	inBOffset	)
{
	if( !c_file.isOpened )
		return FALSE;

	ASSERT( File_IsReady() );

	if( c_file.hdl >= 0 )
	{
		lseek( c_file.hdl, inBOffset, SEEK_SET );
		c_file.bOffset = inBOffset;
	}
	else
	{
		uint lsn = c_file.lsn + (inBOffset>>11);
		if( sceCdSeek(lsn) == 0 )
			return FALSE;
		c_file.bOffset = inBOffset;
	}

	return TRUE;
}


bool
File_Read	(	pvoid		inBufferPtr,
				uint32		inBOffset,
				uint32		inBSize,
				uint32 *	outPaddingBSize,
				uint32 *	outReadBSize,
				uint32 *	outWrittenBSize	)
{
	if( !c_file.isOpened )
		return FALSE;

	ASSERT( File_IsReady() );
	ASSERT( (((uint)inBufferPtr)&3) == 0 );

	if( inBOffset >= File_GetBSize() ) {
		PrintfA(( pfa, "File_Read: BOffset 0x%x is out of range (file-bsize=0x%x) !\n", inBOffset, File_GetBSize() ));
	}

	if( c_file.hdl >= 0 )
	{
		// always seek is safer !!!
		uint32 rmBSize   = inBOffset < File_GetBSize() ? File_GetBSize()-inBOffset : 0;
		uint32 safeBSize = MIN( inBSize, rmBSize );
		lseek( c_file.hdl, inBOffset, SEEK_SET );
		read( c_file.hdl, inBufferPtr, safeBSize );
		if( outPaddingBSize )	*outPaddingBSize	= 0;
		if( outReadBSize )		*outReadBSize		= inBSize;
		if( outWrittenBSize )	*outWrittenBSize	= inBSize;
		c_file.bOffset	= inBOffset + inBSize;
	}
	else
	{
		sceCdRMode	cdMode;
		uint32		lsn, padding;

		lsn					= c_file.lsn + (inBOffset>>11);
		padding				= inBOffset & 0x7FF;
		ASSERT( padding < 2048 );

		cdMode.trycount		= 255;
		cdMode.spindlctrl	= SCECdSpinStm; // SCECdSpinNom;
		cdMode.datapattern	= SCECdSecS2048;

		// Align� sur 1 secteur ?
		if( padding == 0 )
		{
			uint32 cdbsize = (((inBSize+2047)>>11)<<11);
			int res = sceCdRead( lsn, cdbsize>>11, inBufferPtr, &cdMode );
			if( res == 0 )
				return FALSE;
			if( outPaddingBSize )	*outPaddingBSize	= 0;
			if( outReadBSize )		*outReadBSize		= inBSize;
			if( outWrittenBSize )	*outWrittenBSize	= cdbsize;
			c_file.bOffset = inBOffset + cdbsize;
		}
		else
		{
			// Charge 1 secteur pour compl�ter le padding
			// (1 seul secteur pour limiter le memove du padding !)
			int res = sceCdRead( lsn, 1, inBufferPtr, &cdMode );
			if( res == 0 )
				return FALSE;
			if( outPaddingBSize )	*outPaddingBSize	= padding;
			if( outReadBSize )		*outReadBSize		= MIN( 2048-padding, inBSize );
			if( outWrittenBSize )	*outWrittenBSize	= 2048;
			c_file.bOffset	= inBOffset + 2048;
		}
	}

	return TRUE;
}




