/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/





static
void	SetAdpcmFinalMark(	SPU2_Block*		inEnd,
							uint			inBSize		)
{
	// Set ADPCM final mark to stop the streamed voice
	// Flag les deux derniers blocks du VAG :
	// - arr�t sur l'avant dernier block
	// - loop/start/end sur le dernier block
	// Si le buffer ne contient qu'un seul block (16 octets),
	// on rajoute un block de fin en consid�rant :
	// - que les pages de streaming sont de 2048 bytes, donc pas d'�crasement possible, ya de la place !
	// - que les transferts sont des multiples de 64-byte (sceSdVoiceTrans), donc le dernier block rajout� sera transf�r� !
	ASSERT( inBSize >= 16 );
	inEnd -= (inBSize==16) ? 1 : 2;
	inEnd[0].loopflag = LF_END;
	inEnd[1].loopflag = LF_LOOP|LF_START|LF_END;
}



static
void	StopSoundChannel	(	Sound* 		s,
								uint		inChannel	)
{
	ASSERT( s->state != SND_INVALID );
	ASSERT( s >= soundA );

	if( s->mode == 2 )
	{
		ASSERT( (uint)s->streamCh == inChannel );
		if( SPU2_IsValidChannel(s->streamCh) ) {
			SPU2_WriteSdCmd( SD_S_KOFF, s->streamCh, 1 );
			SPU2_WriteSdCmd( SD_S_KOFF, MUSIC_CH,    1 );
			s->streamCh = -1;
		}
	}
	else if( s->mode == 1 )
	{
		ASSERT( (uint)s->streamCh == inChannel );
		if( SPU2_IsValidChannel(s->streamCh) ) {
			SPU2_WriteSdCmd( SD_S_KOFF, s->streamCh, 1 );
			s->streamCh = -1;
		}
	}
	else
	{
		if( SPU2_IsValidChannel(inChannel) ) {
			ASSERT( inChannel != MUSIC_CH );
			SPU2_WriteSdCmd( SD_S_KOFF, inChannel, 1 );
		}
	}
}



static
uint	GetLockIdFromPtr	(	Sound*		s	)
{
	uint lockId = s - soundA;
	ASSERT( s->state != SND_INVALID );
	ASSERT( s >= soundA );
	ASSERT( lockId < sys_lockNbMax );
	ASSERT( &soundA[lockId] == s );
	return lockId;
}



static
void	RetLockSuccess( Sound* s, bool inSuccess )
{
	Cursor* cout = Core_GetOutputCursor();
	cout->t.i32[0] = NVIOP_SD_LOCK;
	cout->t.i16[2] = GetLockIdFromPtr(s);
	cout->t.i16[3] = inSuccess ? 1 : 0;
	cout->t.i32   += 2;
}



static
void	RetUnlockSuccess( Sound* s, bool inSuccess )
{
	Cursor* cout = Core_GetOutputCursor();
	cout->t.i32[0] = NVIOP_SD_UNLOCK;
	cout->t.i16[2] = GetLockIdFromPtr(s);
	cout->t.i16[3] = inSuccess ? 1 : 0;
	cout->t.i32   += 2;
}



static
void	UnlockSound	(	Sound* s,	bool inSuccess	)
{
	ASSERT( s->state == SND_LOCKING || s->state == SND_LOCKED );
	if( s->state == SND_INVALID )
		return;

	PrintfA(( pfa, "UnlockSound: Unlocking (boffset=0x%x08) !\n", s->data.bOffset ));

	if( s->state == SND_LOCKING )
		RetLockSuccess  ( s, inSuccess );
	else
		RetUnlockSuccess( s, inSuccess );

	if( s->streamId >= 0 ) {
		Stream_Sync( s->streamId, NULL );
		s->streamId = -1;
	}

	if( s->streamTmp ) {
		Free( s->streamTmp );
		s->streamTmp = NULL;
	}

	// Streamed => stop channel
	if( s->mode>0 )
		StopSoundChannel( s, s->streamCh );

	s->state = SND_INVALID;
	Stream_Unlock();
}



static
bool	AskNextSegment ( Sound* s, SegState inState )
{
	ASSERT( s->mode > 0 );
	if( s->nextState == SEG_UNDEF )
	{
		Cursor* cout = Core_GetOutputCursor();
		PrintfA(( pfa, "AskNextSegment: id:%d\n", GetLockIdFromPtr(s) ));
		cout->t.i32[0] = NVIOP_SD_ASKSEGMENT;
		cout->t.i32[1] = GetLockIdFromPtr(s);
		cout->t.i32   += 2;
		s->nextState = inState;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}



static
uint	GetStream_ToLoad	(	Sound*	s	)
{
	ASSERT( s->mode > 0 );
	if( s->state == SND_LOCKING )
	{
		uint sh = (s->mode==2) ? 1 : 0;
		// wrapped ? => no more data to load ...
		if( s->streamWrap )
			return 0;
		// remain to preload
		ASSERT( s->sramWPos <= s->sramBSize );
		return (s->sramBSize) - (s->sramWPos<<sh);
	}
	else if( s->state == SND_LOCKED )
	{
		int  ch;
		uint nax, bs, hb0, hb1, tl;
		uint sh = (s->mode==2) ? 1 : 0;

		// Half-buffers 0/1
		bs  = s->sramBSize >> sh;
		hb0 = bs>>1;
		hb1 = bs>>0;

		// playing ?
		// (coz NAX reg is invalid before keyOn !)
		ch = s->streamCh;
		if( ch<0 || !SPU2_IsPlaying(ch) )
			return 0;

		// NAX : Check validity (if not, may be not already started ...)
		nax = SPU2_GetNAX( ch );
		if( nax < s->sramAddr || nax >= (s->sramAddr+bs) )
			return 0;
		nax -= s->sramAddr;

		// Write pointer location ?
		ASSERT( s->sramWPos <= hb1 );
		if( s->sramWPos < hb0 ) {
			// W in Half-buffer 0
			if( nax < hb0 )		tl = 0;	// no need to stream ...
			else				tl = hb0 - s->sramWPos;
		} else {
			// W in Half-buffer 1
			if( nax >= hb0 )	tl = 0;	// no need to stream ...
			else				tl = hb1 - s->sramWPos;
		}

		tl = ((tl>>6)<<6);		// 64-bytes units
		return tl << sh;
	}
	else
	{
		return 0;
	}
}



static
int		GetStream_HBNo	(	Sound*	s	)
{
	int  ch;
	uint nax, bs, hb0, hb1;

	// Half-buffers 0/1
	bs  = s->sramBSize >> ((s->mode==2)?1:0);
	hb0 = bs>>1;
	hb1 = bs>>0;

	// playing ?
	// (coz NAX reg is invalid before keyOn !)
	ch = s->streamCh;
	if( ch<0 || !SPU2_IsPlaying(ch) )
		return -1;

	// NAX : Check validity (if not, may be not already started ...)
	nax = SPU2_GetNAX( ch );
	if( nax < s->sramAddr || nax >= (s->sramAddr+bs) )
		return -1;

	nax -= s->sramAddr;
	return (nax < hb0) ? 0 : 1;
}



static
bool	CheckLoadingContinue	(	Sound*	s	)
{
	bool failed;

	// loading ?
	if( s->streamId < 0 )
		return TRUE;	// continue !

	// done ?
	if( !Stream_IsDone(s->streamId,&failed) )
		return FALSE;	// returns !
	s->streamId = -1;

	// finished !
	if( failed ) {
		PrintfA(( pfa, "CheckLoadingContinue: Loading has failed (boffset=0x%x08) !\n", s->data.bOffset ));
		StopSoundChannel( s, s->streamCh );
		UnlockSound( s, FALSE );
		return FALSE;	// returns !
	}

	return TRUE;	// continue !
}



static
uint	GetRemainLoading	(	Sound*		s	)
{
	uint remain;
	ASSERT( s->loadPos <= s->data.bSize );
	remain = s->data.bSize - s->loadPos;
	if( remain )
		return remain;

	// streamed ?
	if( s->mode )
	{
		// next segment available ?
		if( s->nextState == SEG_AVAILABLE )
		{
			s->data = s->nextSegment;
			s->nextState = SEG_UNDEF;
			s->loadPos = 0;
			remain = s->data.bSize - s->loadPos;
			return remain;
		}
	}

	return remain;
}



static
bool	IsLastLoading		(	Sound*		s,
								uint		inToLoad	)
{
	return ( GetRemainLoading(s) == inToLoad );
}



static
bool	IsEndOfLoading		(	Sound*		s	)
{
	return ( GetRemainLoading(s) == 0 );
}



static
bool	StartLoading	(	Sound*			s,
							uint			inBSize,
							StreamLoadedCB	inCB		)
{
	ASSERT( s->streamId < 0 );
	ASSERT( inBSize <= sys_reqMaxBSize );

	if( inBSize > 0 )
	{
		uint32 pos = s->data.bOffset + s->loadPos;
		uint32 t0  = s->sramAddr + s->sramWPos;
		uint32 t1  = 0;
		if( s->mode == 2 )
			t1 = t0 + s->sramBSize/2;

		s->streamId = Stream_AddRequest(	STREAM_TO_SPU2,
											t0,	t1,
											pos,
											inBSize,
											inCB,
											s	);
		if( s->streamId >= 0 )
		{
			uint32 wpos = s->sramWPos;
			if( s->mode == 0 )		s->sramWPos += inBSize;
			if( s->mode == 1 )		s->sramWPos  = (s->sramWPos+(inBSize)) % (s->sramBSize);
			if( s->mode == 2 )		s->sramWPos  = (s->sramWPos+(inBSize/2)) % (s->sramBSize/2);
			s->loadPos += inBSize;
			if( s->sramWPos <= wpos )
				s->streamWrap++;
			return TRUE;
		}
	}

	return FALSE;
}



