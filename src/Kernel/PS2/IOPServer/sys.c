/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "sys.h"


pvoid
Malloc( uint bsize )
{
    int intrstat;
    pvoid ret;
    CpuSuspendIntr( &intrstat );
    ret = AllocSysMemory( 0, bsize, NULL );
    CpuResumeIntr( intrstat );
	return ret;
}


void
Free( pvoid ptr )
{
	if( ptr ) {
	    int intrstat;
	    CpuSuspendIntr( &intrstat );
		FreeSysMemory( ptr );
	    CpuResumeIntr( intrstat );
	}
}


//	GetUpperRound( 5, 16 )	= 16
//	GetUpperRound( 5, 0 )	= 5

int32	GetUpperRound( uint32 inV, uint32 inA )
{
	return ( inA ? (inV+(inA-1))&(~(inA-1)) : inV );
}


bool	IsPow2( uint32 inV )
{
	return ((inV&(inV-1)) == 0);
}



//	GetLowestPowOf2( 100010100b ) = 000000100b
//	GetLowestPowOf2( 100010000b ) = 000010000b
//	returned 0 if v == 0 !

uint32	GetLowestPowOf2( uint32 v )
{
	return v & (v^(v-1));
}


