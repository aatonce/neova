/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "sys.h"
#include "core.h"
#include "core_EE.h"
#include "core_Stream.h"
#include "core_EELoading.h"



typedef struct {
	bool			busy;
	StreamTarget	targetRam;		// eq. to iop::TargetRam
	uint32			targetPtr;
	uint32			bOffset[4];
	uint32			bSize;
	int				streamId[2];
	uint			tryCpt;			// try counter (decreased if loading is rejected !)
	uint			tryReload;		// initial try counter value
} EELoad;


static uint			sys_reqMaxBSize;
static EELoad		eeLoad;



bool
EELoading_Init			(	uint	inReqMaxBSize	)
{
	sys_reqMaxBSize = inReqMaxBSize;
	eeLoad.busy = FALSE;
	return TRUE;
}



void
EELoading_Shut			(			)
{
	// In progress ?
	if( eeLoad.busy ) {
		Printf( "EELoading_Shut: EELoad in progress aborted !\n" );
	}
}


bool
EELoading_IsNeedPriority	(		)
{
	return ( eeLoad.busy && (eeLoad.tryCpt==0) );
}


void
EELoading_Start		(	StreamTarget	inTargetRam,
						uint32			inTargetPtr,
						uint32			inBOffset0,
						uint32			inBOffset1,
						uint32			inBOffset2,
						uint32			inBOffset3,
						uint32			inBSize,
						uint			inTryReload		)
{
	ASSERT( inBSize );
	if( eeLoad.busy ) {
		Printf( "EELoading_Start: Is busy !\n" );
		return;
	}
	if( !Stream_Lock() ) {
		// Send error to EE
		Cursor* cout = Core_GetOutputCursor();
		cout->t.i32[0] = NVIOP_LOAD_DATA;
		cout->t.i32[1] = 0;	// failed !
		cout->t.i32   += 2;
		Printf( "EELoading_Start: Stream_Lock() failed !\n" );
		return;
	}

	eeLoad.targetRam	= inTargetRam;
	eeLoad.targetPtr	= inTargetPtr;
	eeLoad.bOffset[0]	= inBOffset0;
	eeLoad.bOffset[1]	= inBOffset1;
	eeLoad.bOffset[2]	= inBOffset2;
	eeLoad.bOffset[3]	= inBOffset3;
	eeLoad.bSize		= inBSize;
	eeLoad.tryReload	= inTryReload;
	eeLoad.tryCpt		= inTryReload;
	eeLoad.streamId[0]	= -1;
	eeLoad.streamId[1]	= -1;
	eeLoad.busy			= TRUE;
}



void
EELoading_Update		(			)
{
	int	 i;
	bool streaming = FALSE;
	Cursor* cout = Core_GetOutputCursor();

	if( !eeLoad.busy )
		return;

	for( i = 0 ; i < 2 ; i++ ) {
		// Streaming ?
		if( eeLoad.streamId[i] >= 0 ) {
			// Done ?
			bool failed;
			if( !Stream_IsDone(eeLoad.streamId[i],&failed) ) {
				streaming = TRUE;
				continue;
			}
			// Failed ?
			if( failed ) {
				// Sync other
				if( eeLoad.streamId[i^1] >= 0 )
					Stream_Sync( eeLoad.streamId[i^1], NULL );
				// Send error to EE
				cout->t.i32[0] = NVIOP_LOAD_DATA;
				cout->t.i32[1] = 0;	// failed !
				cout->t.i32   += 2;
				eeLoad.busy = FALSE;
				Stream_Unlock();
				return;
			}
			eeLoad.streamId[i] = -1;
		}

		// Add request to stream ?
		if( eeLoad.bSize > 0 ) {
			// maxbsize/2 - 2048 (padding) for each of the 2 streams ...
			uint bsize = MIN( eeLoad.bSize, (sys_reqMaxBSize/2-2048) );
			int strid  = Stream_AddRequest(	eeLoad.targetRam,
											eeLoad.targetPtr,
											0,
											eeLoad.bOffset[0],
											bsize,
											NULL, NULL	);
			// Accepted ?
			if( strid >= 0 ) {
				streaming			 = TRUE;
				eeLoad.targetPtr	+= bsize;
				eeLoad.bOffset[0]	+= bsize;
				eeLoad.bOffset[1]	+= bsize;
				eeLoad.bOffset[2]	+= bsize;
				eeLoad.bOffset[3]	+= bsize;
				eeLoad.bSize		-= bsize;
				eeLoad.streamId[i]	 = strid;
				eeLoad.tryCpt		 = eeLoad.tryReload;	// Reload try counter !
			} else {
				// Decrease try counter !
				if( eeLoad.tryCpt )
					eeLoad.tryCpt--;
			}
		}
	}

	// Finished ?
	if( !streaming && (eeLoad.bSize == 0) ) {
		// Send finish to EE
		cout->t.i32[0] = NVIOP_LOAD_DATA;
		cout->t.i32[1] = 1;	// success !
		cout->t.i32   += 2;
		eeLoad.busy = FALSE;
		Stream_Unlock();
	}
}


