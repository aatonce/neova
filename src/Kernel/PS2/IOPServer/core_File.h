/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _Core_File_H_
#define	_Core_File_H_


bool		File_Init		(	int			inMediaMode		);		// SCECdCD / SCECdDVD
void		File_Reset		(								);
void		File_Shut		(								);

void		File_EnableCB	(	int			inSemId			);
void		File_DisableCB	(								);

bool		File_Open		(	pcstr		inFilename		);
bool		File_Close		(								);
bool		File_IsOpened	(								);
uint		File_GetBSize	(								);
bool		File_Lock		(								);
bool		File_Unlock		(								);
bool		File_IsLocked	(								);

bool		File_Seek		(	uint32		inBOffset		);		//              <------------written---------->
bool		File_Read		(	pvoid		inBufferPtr,			// output is : [<--padding--><--readen--><---->]
								uint32		inBOffset,				// byte offset in the file
								uint32		inBSize,				// byte-size required to be readen from boffset
								uint32*		outPaddingBSize,		// boffset of required data in bufferPtr
								uint32*		outReadBSize,			// bsize of required data available (at padding location)
								uint32*		outWrittenBSize	);		// bsize written in bufferPtr (>= outPaddingBSize + outReadBSize)

bool		File_IsReady	(								);
bool		File_IsError	(								);
void		File_Sync		(								);


#endif


