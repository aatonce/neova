/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _Core_H_
#define	_Core_H_


void		Core_Init				(	uint		inUpdateThreadPriority,		// def 50
										uint		inUpdateTimerFreq,			// def 8*60
										uint		inStreamCacheBSize,			// def 64Ko
										uint		inEELoadReqMaxBSize,		// def 32Ko
										uint		inVoiceReqMaxBSize,			// def 16Ko
										uint		inSndLockNbMax,				// def 512
										uint		inMediaMode				);	// SCECdCD / SCECdDVD
void		Core_Shut				(										);
void		Core_Reset				(										);
void		Core_Lock				(										);
void		Core_Unlock				(										);

// Open / Close
bool		Core_OpenFile			(	pcstr		inFilename,
										uint32 *	outBSize				);
bool		Core_CloseFile			(										);

// Commands
void		Core_ParseInputCmdList	(	Cursor *	inCmdListStart,
										Cursor *	inCmdListEnd			);
pvoid		Core_GetOutputCmdList	(										);
Cursor*		Core_GetOutputCursor	(										);

// Update
void		Core_Update				(										);	// Direct core update !
void		Core_WakeUpdate			(										);	// Wake Thread update with semaphore
uint		Core_GetUpdateCounter	(										);


#endif


