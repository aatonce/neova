/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <Kernel/PS2/IOP/IOP.h>
#include "../IOPServer/cmd.h"
#include <sif.h>
#include <sifrpc.h>
#include <sifdev.h>
#include <sifcmd.h>
#include <sdmacro.h>
#include <libspu2m.h>
#include <libcdvd.h>
using namespace nv;




// Valeur de seuil � partir de laquelle un ForceFlush() est appell�
// avant �criture de nouvelles commandes dans le buffer d'envoi courant.
// L'objectif est d'�viter les d�bordement du buffer d'envoi des commandes vers l'IOP.
// La cons�quence peut �tre une perte temporaire de performance.
#define		OVERFLOW_FILLING_THR		(80)


//#define	DBG_VERBOSE

#ifdef		DBG_VERBOSE
#define		DbgVerbosePrintf		DebugPrintf
#else
inline void	NULL_DbgVerbosePrintf( pcstr, ... )		{	}
#define		DbgVerbosePrintf		NULL_DbgVerbosePrintf
#endif




namespace
{
	sceSifClientData *		cd = NULL;
	sceSifClientData		l_cd;

	// IOP->EE buffer (D$ aligned !)
	ALIGNED512(	uint32,		dataIn	[ NVIOP_RPC_CMD_BSIZE/4 ] );

	// EE->IOP buffers (D$ aligned !)
	ALIGNED512( uint32,		dataOut0[ NVIOP_RPC_CMD_BSIZE/4 ] );
	ALIGNED512( uint32,		dataOut1[ NVIOP_RPC_CMD_BSIZE/4 ] );
	dmac::Cursor			cursOut;

	struct SdAllocData {
		iop::SdState	state;
		uint8			autoFree;		// Automatic free when unlocked ?
		uint8			mode;
		uint16			dataFreq;
		uint32			dataBOffset;
		uint32			dataBSize;
		float			duration;		// s
		uint32			sramBAddr;
		uint32			sramBSize;
	};

	struct SdAskSegment {
		int				sid;
	};

	// Context
	bool				c_waitStatus;
	bool				c_loadDataBusy;
	bool				c_loadDataSuccess;
	bool				c_waitMalloc;
	pvoid				c_mallocPtr;
	uint32				c_IOPFreeMemSize;
	uint32				c_IOPFreeMaxSize;
	SdAllocData			c_sdAllocA[iop::SD_NB_SOUND_MAX];
	uint64				c_sdENDX;
	uint64				c_sdENVX;
	uint64				c_sdDown;
	SdAskSegment		c_askSegmentA[64];
	uint				c_askSegmentSize;
	iop::StreamHandler	c_strHandler;

	// Sram manager
	char				sramManagerData[ SCESPU2MEM_TABLE_UNITSIZE * (iop::SD_NB_SOUND_MAX+1) ];



	void
	_PreventFlushOverflow	(		)
	{
		int filling = iop::GetFlushFilling();
		if( filling >= OVERFLOW_FILLING_THR ) {
			iop::ForceFlush();
			// Inc system parameter to inform the user
			uint32 overflowCpt;
			core::GetParameter( core::PR_IOP_CMDBUF_OVERFLOW, &overflowCpt );
			core::SetParameter( core::PR_IOP_CMDBUF_OVERFLOW, overflowCpt+1 );
			#ifdef _NVCOMP_ENABLE_CONSOLE
			NV_MESSAGE( "IOP commands buffer overflow detected. Check the core::PR_IOP_CMDBUF_OVERFLOW parameter." );
			#endif
			filling = iop::GetFlushFilling();
			NV_ASSERT( filling < OVERFLOW_FILLING_THR );
		}
		core::SetParameter( core::PR_IOP_CMDBUF_FILLING, uint32(filling) );
	}


	void
	_UpdateContext()
	{
		dmac::Cursor cin;
		cin.start = (uint8*) dataIn;
		cin.i8    = (uint8*) dataIn;

		for( ;; )
		{
			if( cin.i32[0] == NVIOP_EOP )
				break;

			switch( cin.i32[0] )
			{
				// Load Data ?
				case NVIOP_LOAD_DATA :
				{
					c_loadDataBusy    = FALSE;
					c_loadDataSuccess = (cin.i32[1] != 0);
					cin.i32 += 2;
					break;
				}

				// Mem operation ?
				case NVIOP_MEM_ALLOC :
				{
					c_waitMalloc	= FALSE;
					c_mallocPtr		= (pvoid) cin.i32[1];
					cin.i32		   += 2;
					break;
				}

				// Lock completed ?
				case NVIOP_SD_LOCK :
				{
					int id		 = int( cin.i16[2] );
					bool success = (cin.i16[3] != 0);
					cin.i32		+= 2;
					DbgVerbosePrintf( "iop::_UpdateContext: Lock received: id:%d success:%d state:%d\n", id, success?1:0, c_sdAllocA[id].state );
					NV_ASSERT( iop::SdIsValid(id) );
					NV_ASSERT( c_sdAllocA[id].state == iop::SD_LOCKING );
					c_sdAllocA[id].state = success ? iop::SD_LOCKED : iop::SD_UNLOCKED;
					if( !success )
						sceSpu2MemFree( c_sdAllocA[id].sramBAddr );
					break;
				}

				// Unlock completed ?
				case NVIOP_SD_UNLOCK :
				{
					int id	 	 = int( cin.i16[2] );
					bool success = (cin.i16[3] != 0);
					cin.i32		+= 2;
					DbgVerbosePrintf( "iop::_UpdateContext: Unlock received: id:%d success:%d state:%d\n", id, success?1:0, c_sdAllocA[id].state );
					NV_ASSERT( iop::SdIsValid(id) );
					NV_ASSERT(		c_sdAllocA[id].state == iop::SD_UNLOCKING		// from SdUnlock()
								||	c_sdAllocA[id].state == iop::SD_LOCKED		);	// from auto-unlock (SdStop() ou play ended)

					c_sdAllocA[id].state = iop::SD_UNLOCKED;
					sceSpu2MemFree( c_sdAllocA[id].sramBAddr );
					// auto-free ?
					if( c_sdAllocA[id].autoFree ) {
						DbgVerbosePrintf( "iop::_UpdateContext: Auto-freed: id:%d\n", id );
						iop::SdFree( id );
					}
					// unlock on cd-error ?
					if( cin.i16[3]==0 ) {
						DbgVerbosePrintf( "iop::_UpdateContext: Unlock has failed on CD-error: id:%d\n", id );
					}
					break;
				}

				// Stream segment asked ?
				case NVIOP_SD_ASKSEGMENT :
				{
					NV_ASSERT( c_askSegmentSize < 64 );
					int sid = int( cin.i32[1] );
					cin.i32 += 2;
					DbgVerbosePrintf( "iop::_UpdateContext: Segment asked: id:%d\n", sid );
					c_askSegmentA[ c_askSegmentSize ].sid = sid;
					c_askSegmentSize++;
					NV_ASSERT( iop::SdIsValid(sid) );
					break;
				}

				// Status ?
				case NVIOP_STATUS :
				{
					c_IOPFreeMemSize	= cin.i32[1];
					c_IOPFreeMaxSize	= cin.i32[2];
					c_sdENDX			= (uint64(cin.i32[4])<<24) | uint64(cin.i32[3]);	// set    Core1<<24 | Core0
					c_sdENVX			= (uint64(cin.i32[6])<<24) | uint64(cin.i32[5]);	// set    Core1<<24 | Core0
					c_sdDown		   |= (uint64(cin.i32[8])<<24) | uint64(cin.i32[7]);	// append Core1<<24 | Core0
					cin.i32			   += 9;
					break;
				}

				// unknow -> parsing error
				default :
					Printf( "iop::_UpdateContext: Unknow cmd id %d !\n", cin.i32[0] );
					cin.i32[0] = NVIOP_EOP;
					break;
			}
		}
	}


	void
	_ResetContext()
	{
		c_waitStatus		= FALSE;
		c_loadDataBusy		= FALSE;
		c_waitMalloc		= FALSE;
		c_mallocPtr			= NULL;
		c_IOPFreeMemSize	= 0;
		c_IOPFreeMaxSize	= 0;
		c_askSegmentSize	= 0;
		c_strHandler		= NULL;

		// Init Sd
		for( int i = 0 ; i < iop::SD_NB_SOUND_MAX ; i++ )
			c_sdAllocA[i].state = iop::SD_UNUSED;
		c_sdENDX		= 0;
		c_sdENVX		= 0;
		c_sdDown		= 0;

		// Init SRam manager
		sceSpu2MemInit( sramManagerData, iop::SD_NB_SOUND_MAX, SCESPU2MEM_USE_EFFECT );

		// Init cursor
		cursOut.bsize = sizeof(dataOut0);
		cursOut.start = cursOut.i8 = (uint8*) dataOut0;
		core::SetParameter( core::PR_IOP_CMDBUF_FILLING, 0U );
	}


}




bool
iop::Init		(	int		mediaMode	)		// SCECdDVD or SCECdCD
{
	cd = NULL;
	for( ;; ) {
		if( sceSifBindRpc(&l_cd,NVIOP_BIND_ID,0) < 0)
			break;
		if( l_cd.serve != 0 ) {
			cd = &l_cd;
			break;
		}
		WaitCycleCpt( 4096 );
	}
	if( !cd ) {
		NV_ERROR( "IOP rpc-bind error !" );
		return FALSE;
	}

	// Init server
	uint32	rpcThreadPriority,
			updateThreadPriority,
			updateTimerFreq,
			streamCacheBSize,
			eeLoadReqMaxBSize,
			voiceReqMaxBSize;

	core::GetParameter( core::PR_IOP_RPC_THREADPRI,			&rpcThreadPriority		);
	core::GetParameter( core::PR_IOP_UPD_THREADPRI,			&updateThreadPriority	);
	core::GetParameter( core::PR_IOP_UPD_TIMERFREQ,			&updateTimerFreq		);
	core::GetParameter( core::PR_IOP_STREAM_CACHEBSIZE,		&streamCacheBSize		);
	core::GetParameter( core::PR_IOP_EELOAD_REQMAXBSIZE,	&eeLoadReqMaxBSize		);
	core::GetParameter( core::PR_IOP_VOICE_REQMAXBSIZE,		&voiceReqMaxBSize		);
	NV_ASSERT( (mediaMode==SCECdCD) || (mediaMode==SCECdDVD) );

	dataOut0[0] = rpcThreadPriority;
	dataOut0[1] = updateThreadPriority;
	dataOut0[2] = updateTimerFreq;
	dataOut0[3] = streamCacheBSize;
	dataOut0[4] = eeLoadReqMaxBSize;
	dataOut0[5] = voiceReqMaxBSize;
	dataOut0[6] = SD_NB_SOUND_MAX;
	dataOut0[7] = mediaMode;
	FlushCache( WRITEBACK_DCACHE );
	sceSifCallRpc(	cd, NVIOP_FCT_INIT, 0,		// synchronous
					dataOut0, Round16(8*4),
					dataIn, 16,
					0, 0 );

	// IOP irx version
	uint32 irxVersion = dataIn[0];
	if( irxVersion != NVIOP_VERSION ) {
		Printf( "iop::Init: IRX version is %d, expected %d\n", irxVersion, NVIOP_VERSION );
		NV_ERROR( "Invalid IRX version !" );
		return FALSE;
	}

	// Reset
	_ResetContext();
	return TRUE;
}


void
iop::Shut	(		)
{
	if( !cd )
		return;

	SyncFlush();
	sceSifCallRpc( cd, NVIOP_FCT_SHUT, 0,		// synchronous
					NULL, 0,
					dataIn, 16,
					0, 0	);

	sceSpu2MemQuit();
}


void
iop::Reset		(		)
{
	if( !cd )
		return;

	SyncFlush();
	sceSifCallRpc	(	cd, NVIOP_FCT_RESET, 0,		// synchronous
						NULL,0,
						dataIn, 16, 0, 0	);

	_ResetContext();

	DbgVerbosePrintf( "iop::Reset: Done !\n" );
}


void
iop::Update		(		)
{
	if( !cd )
		return;

	// Send segments !
	for( uint i = 0 ; i < c_askSegmentSize ; i++ )
	{
		int sid = c_askSegmentA[i].sid;
		float fromt=0.f, duration=0.f;
		if( c_strHandler && (*c_strHandler)(sid,fromt,duration) )
			SdSetSegment( sid, fromt, duration );
		else
			SdNoSegment( sid );
	}
	c_askSegmentSize = 0;
}


iop::StreamHandler
iop::SetHandler		(	StreamHandler	inHandler	)
{
	iop::StreamHandler old_handler = c_strHandler;
	c_strHandler = inHandler;
	return old_handler;
}




bool
iop::IsFlushReady	(		)
{
	if( !cd )
		return TRUE;

	// Waiting IOP status ?
	if( c_waitStatus ) {
		bool ready = ( sceSifCheckStatRpc((sceSifRpcData*)cd) == 0 );
		if( !ready )
			return FALSE;
		// Update iop context
		_UpdateContext();
		c_waitStatus = FALSE;
	}

	return TRUE;
}


void
iop::SyncFlush		(		)
{
	if( !cd )
		return;
	while( !IsFlushReady() ) {
		// release BUS some cycles ...
		WaitCycleCpt( 512 );
	}
}


uint
iop::GetFlushBCapacity	(		)
{
	NV_ASSERT( sizeof(dataOut0) == sizeof(dataIn) );
	return sizeof(dataOut0);
}


uint
iop::GetFlushBSize		(		)
{
	NV_ASSERT( cursOut.start );
	uint bsize  = cursOut.GetUsedBSize();
		 bsize += 4;					// -NVIOP_EOP- cmd marker
		 bsize  = Round16( bsize );		// 16 bytes DMA aligned
	NV_ASSERT( bsize <= GetFlushBCapacity() );
	return bsize;
}


int
iop::GetFlushFilling	(		)
{
	int capacity = (int) GetFlushBCapacity();
	int bsize    = (int) GetFlushBSize();
	NV_ASSERT( bsize <= capacity );
	return bsize * 100 / capacity;
}


bool
iop::Flush		(		)
{
	if( !cd )
		return TRUE;

	if( !IsFlushReady() )
		return FALSE;

	// Flush new commands & get status
	pvoid cmdStart = NULL;
	uint  cmdBSize = 0;

	// Not empty ?
	if( cursOut.GetUsedBSize() ) {
		cursOut.i32[0] = NVIOP_EOP;
		cursOut.i32++;
		cmdBSize = Round16( cursOut.GetUsedBSize() );
		if( cmdBSize > sizeof(dataOut0) ) {
			NV_ERROR( "IOP commands buffer overflows !!" );
		}
		cmdStart = cursOut.start;
	}

	// Flush cache for EE->IOP & IOP->EE transfers as data loading ...
	// sceSifCallRpc only flush D$ for sending & receiving buffer areas.
	FlushCache( WRITEBACK_DCACHE );

	sceSifCallRpc	(	cd, NVIOP_FCT_CMD_BATCH, SIF_RPCM_NOWAIT,
						cmdStart, cmdBSize,
						dataIn, sizeof(dataIn),
						0, 0	);

	c_waitStatus = TRUE;

	// Swap cursor
	cursOut.start = cursOut.i8 = ( cursOut.start == (uint8*)dataOut0 )
							   ? (uint8*) dataOut1
							   : (uint8*) dataOut0;
	core::SetParameter( core::PR_IOP_CMDBUF_FILLING, 0U );

	return TRUE;
}


bool
iop::ForceFlush		(		)
{
	iop::SyncFlush();
	return iop::Flush();
}



//
// ----------------



bool
iop::OpenFile	(	pcstr		inFilename,
					uint32 *	outBSize	)
{
	if( !cd )
		return FALSE;

	if( !inFilename || Strlen(inFilename)==0 )
		return FALSE;

	ALIGNED512( char, tmp[256] );		// D$ aligned !
	Strcpy( tmp, inFilename );
	uint ssize = Strlen(tmp)+1;
		 ssize = Round16( ssize );

	SyncFlush();
	FlushCache( WRITEBACK_DCACHE );
	sceSifCallRpc	(	cd, NVIOP_FCT_OPEN_FILE, 0,
						tmp, ssize,
						dataIn, 16, 0, 0	);

	if( outBSize )
		*outBSize = dataIn[0];

	return ( dataIn[0] != 0 );
}


bool
iop::CloseFile	(			)
{
	if( !cd )
		return FALSE;

	SyncFlush();
	FlushCache( WRITEBACK_DCACHE );
	sceSifCallRpc	(	cd, NVIOP_FCT_CLOSE_FILE, 0,
						NULL, 0,
						dataIn, 16, 0, 0	);

	return ( dataIn[0] != 0 );
}


bool
iop::LoadData	(	TargetRam	inTargetRam,
					pvoid		inBufferPtr,
					uint32		inBSize,
					uint32		inBOffset0,
					uint32	/*	inBOffset1*/,
					uint32	/*	inBOffset2*/,
					uint32	/*	inBOffset3*/	)
{
	if( !cd )
		return FALSE;

	// In use ?
	if(	c_loadDataBusy )
		return FALSE;

	// size 16-bytes aligned
	if(	inBSize == 0 || (inBSize&0xF)!=0 )
		return FALSE;

	// Flush the EE dma'ed memory
	void* addr0 = inBufferPtr;
	void* addr1 = (void*)( ((uint32)addr0)+inBSize );
	//FlushCache( WRITEBACK_DCACHE );
	SyncDCache( addr0, addr1 );

	// EE Ram -> convert to DMA_MEM range ?
	if( inTargetRam == LOAD_EE_RAM )
		inBufferPtr = (pvoid) DMA_ADDR( inBufferPtr );

	uint32 eeLoadTryCounter;
	core::GetParameter(	core::PR_IOP_EELOAD_TRYCOUNTER, &eeLoadTryCounter );

	cursOut.i32[0] = NVIOP_LOAD_DATA;
	cursOut.i32[1] = uint32(inBufferPtr);
	cursOut.i32[2] = inBOffset0;
	cursOut.i32[3] = inBSize;
	cursOut.i16[8] = uint16(inTargetRam);
	cursOut.i16[9] = Clamp( eeLoadTryCounter, 1U, 256U );
	cursOut.i32   += 5;

	c_loadDataBusy	= TRUE;

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::IsLoadReady	(		)
{
	return !c_loadDataBusy;
}


bool
iop::IsLoadSuccess	(		)
{
	return c_loadDataSuccess;
}



//
// ----------------


bool
iop::AllocSPU2Mem	(	uint32			inBSize,
						void*&			outAddr		)
{
	int addr = sceSpu2MemAllocate( inBSize );
	if( addr < 0 ) {
		outAddr = NULL;
		return FALSE;
	}
	outAddr = (void*) addr;
	return TRUE;
}


void
iop::FreeSPU2Mem	(	pvoid			inPtr		)
{
	sceSpu2MemFree( (uint)inPtr );
}


bool
iop::SdIsValid	(	int		inSdId		)
{
	NV_ASSERT( cd );
	if( inSdId<0 || inSdId>=SD_NB_SOUND_MAX )
		return FALSE;
	NV_ASSERT( c_sdAllocA[inSdId].state >= SD_UNUSED );
	NV_ASSERT( c_sdAllocA[inSdId].state <= SD_UNLOCKING );
	return ( c_sdAllocA[inSdId].state != SD_UNUSED );
}


iop::SdState
iop::SdGetState		(	int			inSdId			)
{
	return SdIsValid(inSdId) ? c_sdAllocA[inSdId].state : SD_UNUSED;
}


uint
iop::SdGetMode		(	int				inSdId		)
{
	NV_ASSERT( cd );
	return SdIsValid(inSdId) ? c_sdAllocA[inSdId].mode : 0;
}


uint
iop::SdGetBaseFreq	(	int				inSdId		)
{
	NV_ASSERT( cd );
	return SdIsValid(inSdId) ? c_sdAllocA[inSdId].dataFreq : 0;
}


float
iop::SdGetDuration	(	int				inSdId		)
{
	NV_ASSERT( cd );
	return SdIsValid(inSdId) ? c_sdAllocA[inSdId].duration : 0.f;
}


int
iop::SdAny			(			)
{
	NV_ASSERT( cd );
	for( int i = 0 ; i < SD_NB_SOUND_MAX ; i++ ) {
		if( c_sdAllocA[i].state == iop::SD_UNUSED )
			return i;
	}
	return -1;
}


int
iop::SdAlloc		(	int			inSdId		)
{
	NV_ASSERT( cd );
	if( inSdId<0 || inSdId>=SD_NB_SOUND_MAX )
		return -1;
	if( c_sdAllocA[inSdId].state != SD_UNUSED )
		return -1;
	c_sdAllocA[inSdId].state = SD_UNLOCKED;
	DbgVerbosePrintf( "iop::SdAlloc: id:%d\n", inSdId );
	return inSdId;
}


bool
iop::SdFree			(	int			inSdId		)
{
	NV_ASSERT( cd );
	if( !SdIsValid(inSdId) )
		return FALSE;
	DbgVerbosePrintf( "iop::SdFree: id:%d state:%d\n", inSdId, c_sdAllocA[inSdId].state );
	SdUnlock( inSdId );
	if( c_sdAllocA[inSdId].state == SD_UNLOCKED ) {
		c_sdAllocA[inSdId].state = SD_UNUSED;
		return TRUE;
	}
	else if( c_sdAllocA[inSdId].state == SD_UNLOCKING ) {
		c_sdAllocA[inSdId].autoFree = TRUE;
		return TRUE;
	}
	return FALSE;
}


bool
iop::SdLock		(	int				inSdId,
					uint			inMode,
					uint			inDataFreq,
					uint32			inDataBOffset,
					uint32			inDataBSize,
					float			inDuration,
					float			inLockFromTime,
					float			inLockDuration		)
{
	NV_ASSERT( cd );
	NV_ASSERT( inMode <= 2 );
	SdState sdstate = SdGetState( inSdId );

	if( !SdIsValid(inSdId) ) {
		DebugPrintf( "iop::SdLock: Invalid sound-ID %d (boffset=0x%x08, state=%d) !\n", inSdId, inDataBOffset, int(sdstate) );
		return FALSE;
	}
	if( SdGetState(inSdId) != SD_UNLOCKED ) {
		DebugPrintf( "iop::SdLock: Sound-ID %d is already locking (boffset=0x%x08, state=%d) !\n", inSdId, inDataBOffset, int(sdstate) );
		return FALSE;
	}
	if( !inDataBSize || !inDataFreq ) {
		DebugPrintf( "iop::SdLock: Invalid bSize=%d and/or freq=%d (boffset=0x%x08) !\n", inDataBSize, inDataFreq, inDataBOffset );
		return FALSE;
	}
	if( inDuration<=0.f ) {
		DebugPrintf( "iop::SdLock: Invalid duration=%f (boffset=0x%x08) !\n", inDuration, inDataBOffset );
		return FALSE;
	}

	// Locking segment
	uint32	seg_bOffset,
			seg_bSize;
	if( inMode )
	{
		// check stream segment !
		if( inLockFromTime<0.f || inLockDuration<0.f || inLockFromTime>=inDuration ) {
			DebugPrintf( "iop::SdLock: Invalid segment [%f,%f] (boffset=0x%x08) !\n", inLockFromTime, inLockDuration, inDataBOffset );
			return FALSE;
		}
		float max_lock_dur = inDuration - inLockFromTime;
		if( inLockDuration==0.f )	inLockDuration = max_lock_dur;
		else						inLockDuration = Min( inLockDuration, max_lock_dur );

		// stream segment
		float  t0    = inLockFromTime,
			   t1    = inLockFromTime + inLockDuration;
		uint   bL2	 = (inMode==1) ? 4 : 5;
		float  bps   = float(int(inDataFreq)) / 28.f;
		uint32 bOff0 = uint32( t0*bps ) << bL2;
		uint32 bOff1 = uint32( t1*bps ) << bL2;
		if( bOff0 >= inDataBSize )		return FALSE;
		if( bOff1 >  inDataBSize )		bOff1 = inDataBSize;
		seg_bOffset	= inDataBOffset + bOff0;
		seg_bSize	= bOff1 - bOff0;
	}
	else
	{
		// whole segment
		seg_bOffset	= inDataBOffset;
		seg_bSize	= inDataBSize;
	}

	// sram allocation (sceSdVoiceTrans() 64bytes unit !)
	uint32 streamMST = 0;
	core::GetParameter( core::PR_SNDMAN_STREAM_CACHEMST, &streamMST );
	uint32 streamCacheBSize = (inDataFreq * streamMST * 16) / (28 * 1000);
	int sramBSize=0, sramBAddr=0;
	if( inMode == 0 )		sramBSize = Round64( inDataBSize );
	else if( inMode == 1 )	sramBSize = Round128( streamCacheBSize );
	else if( inMode == 2 )	sramBSize = Round256( streamCacheBSize*2 );
	else 					return FALSE;
	NV_ASSERT( sramBSize );
	if( sramBSize == 0 )	return FALSE;
	sramBAddr = sceSpu2MemAllocate( sramBSize );
	if( sramBAddr < 0 ) {
		DebugPrintf( "iop::SdLock: No more SRAM available (boffset=0x%x08) !\n", inDataBOffset );
		return FALSE;
	}

	c_sdAllocA[inSdId].state		= SD_LOCKING;
	c_sdAllocA[inSdId].autoFree		= FALSE;
	c_sdAllocA[inSdId].mode			= inMode;
	c_sdAllocA[inSdId].dataFreq		= inDataFreq;
	c_sdAllocA[inSdId].dataBOffset	= inDataBOffset;
	c_sdAllocA[inSdId].dataBSize	= inDataBSize;
	c_sdAllocA[inSdId].duration		= inDuration;
	c_sdAllocA[inSdId].sramBAddr	= sramBAddr;
	c_sdAllocA[inSdId].sramBSize	= sramBSize;

	DbgVerbosePrintf( "iop::SdLock: id:%d mode:%d boffset:%x bsize:%d sramAddr:%x sramBS:%d\n",
					inSdId,
					inMode,
					inDataBOffset,
					inDataBSize,
					sramBAddr,
					sramBSize	);

	cursOut.i32[0] = NVIOP_SD_LOCK;
	cursOut.i16[2] = inSdId;
	cursOut.i16[3] = inMode;
	cursOut.i32[2] = sramBAddr;
	cursOut.i32[3] = sramBSize;
	cursOut.i32[4] = seg_bOffset;
	cursOut.i32[5] = seg_bSize;
	cursOut.i32   += 6;

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdUnlock		(	int			inSdId		)
{
	NV_ASSERT( cd );
	if( !SdIsValid(inSdId) )						return FALSE;
	DbgVerbosePrintf( "iop::SdUnlock: id:%d state:%d\n", inSdId, c_sdAllocA[inSdId].state );
	if( c_sdAllocA[inSdId].state <= SD_UNLOCKED )	return TRUE;
	if( c_sdAllocA[inSdId].state == SD_UNLOCKING )	return TRUE;
	if( c_sdAllocA[inSdId].state != SD_LOCKED )		return FALSE;
	c_sdAllocA[inSdId].state    = SD_UNLOCKING;
	c_sdAllocA[inSdId].autoFree = FALSE;

	cursOut.i32[0] = NVIOP_SD_UNLOCK;
	cursOut.i16[2] = inSdId;
	cursOut.i32   += 2;

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdSet	(	uint16		inRegister,
				uint16		inChannelOrCore,
				uint32		inValue			)
{
	NV_ASSERT( cd );
	if( inChannelOrCore >= 48 )
		return FALSE;

	cursOut.i32[0] = NVIOP_SD_CMD;
	cursOut.i16[2] = inRegister;
	cursOut.i16[3] = inChannelOrCore;
	cursOut.i32[2] = inValue;
	cursOut.i32   += 3;

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdStart	(	int			inSdId,
					uint		inChannel,
					uint		inPitch,
					uint		inVolL,
					uint		inVolR		)
{
	NV_ASSERT( cd );
	if( inChannel >= 48 )					return FALSE;
	if( SdGetState(inSdId) != SD_LOCKED )	return FALSE;

	DbgVerbosePrintf( "iop::SdStart: id:%d ch:%d state:%d\n", inSdId, inChannel, c_sdAllocA[inSdId].state );

	cursOut.i32[0] = NVIOP_SD_START;
	cursOut.i16[2] = inSdId;
	cursOut.i16[3] = inChannel;
	cursOut.i16[4] = inVolL;
	cursOut.i16[5] = inVolR;
	cursOut.i32[3] = inPitch;
	cursOut.i32   += 4;

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdStop		(	int			inSdId,
					uint		inChannel		)
{
	DbgVerbosePrintf( "iop::SdStop (enter): id:%d ch:%d\n", inSdId, inChannel );

	NV_ASSERT( cd );
	if( inChannel >= 48 )					return FALSE;
	if( SdGetState(inSdId) != SD_LOCKED )	return FALSE;

	DbgVerbosePrintf( "iop::SdStop: id:%d ch:%d\n", inSdId, inChannel );

	cursOut.i32[0] = NVIOP_SD_STOP;
	cursOut.i16[2] = inSdId;
	cursOut.i16[3] = inChannel;
	cursOut.i32   += 2;

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdBreak	(	int			inSdId		)
{
	NV_ASSERT( cd );
	if( SdGetState(inSdId) != SD_LOCKED )	return FALSE;
	if( SdGetMode(inSdId) == 0 )			return FALSE;

	cursOut.i32[0] = NVIOP_SD_BREAK;
	cursOut.i32[1] = inSdId;
	cursOut.i32   += 2;

	DbgVerbosePrintf( "iop::SdBreak: id:%d\n", inSdId );

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdSetVolume	(	int			inSdId,
						uint		inChannel,
						uint		inVolL,
						uint		inVolR		)
{
	NV_ASSERT( cd );
	if( inChannel >= 48 )					return FALSE;
	if( SdGetState(inSdId) != SD_LOCKED )	return FALSE;

	cursOut.i32[0] = NVIOP_SD_SETVOL;
	cursOut.i16[2] = inSdId;
	cursOut.i16[3] = inChannel;
	cursOut.i16[4] = inVolL;
	cursOut.i16[5] = inVolR;
	cursOut.i32   += 3;

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdSetPitch		(	int			inSdId,
						uint		inChannel,
						uint		inPitch		)
{
	NV_ASSERT( cd );
	if( inChannel >= 48 )					return FALSE;
	if( SdGetState(inSdId) != SD_LOCKED )	return FALSE;

	cursOut.i32[0] = NVIOP_SD_SETPITCH;
	cursOut.i16[2] = inSdId;
	cursOut.i16[3] = inChannel;
	cursOut.i32[2] = inPitch;
	cursOut.i32   += 3;

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdSetSegment	(	int			inSdId,
						float		inFromTime,
						float		inDuration	)
{
	NV_ASSERT( cd );
	bool inlock = (SdGetState(inSdId)==SD_LOCKED) || (SdGetState(inSdId)==SD_LOCKING);
	if( !inlock )	return FALSE;
	SdAllocData* s = & c_sdAllocA[inSdId];

	// streamed ?
	if( s->mode == 0 )
		return FALSE;

	// valid segment ?
	if( inFromTime<0 || inFromTime>=s->duration || inDuration<0 )
		return FALSE;
	float max_dur = s->duration - inFromTime;
	if( inDuration==0.f )	inDuration = max_dur;
	else					inDuration = Min( inDuration, max_dur );

	// Segment
	float  t0    = inFromTime,
		   t1    = inFromTime + inDuration;
	uint   bL2   = (s->mode==1) ? 4 : 5;
	float  bps   = float(int(s->dataFreq)) / 28.f;
	uint32 bOff0 = uint32( t0*bps ) << bL2;
	uint32 bOff1 = uint32( t1*bps ) << bL2;
	if( bOff0 >= s->dataBSize )		return FALSE;
	if( bOff1 >  s->dataBSize )		bOff1 = s->dataBSize;

	// skip BRR init filter
	if( bOff0 == 0 )
		bOff0 = uint32( 1<<bL2 );

	cursOut.i32[0] = NVIOP_SD_SETSEGMENT;
	cursOut.i32[1] = inSdId;
	cursOut.i32[2] = s->dataBOffset + bOff0;	// bOffset
	cursOut.i32[3] = bOff1 - bOff0;				// bSize
	cursOut.i32   += 4;

	DbgVerbosePrintf( "iop::SdSetSegment: id:%d\n", inSdId );

	_PreventFlushOverflow();
	return TRUE;
}


bool
iop::SdNoSegment	(	int			inSdId	)
{
	NV_ASSERT( cd );
	bool inlock = (SdGetState(inSdId)==SD_LOCKED) || (SdGetState(inSdId)==SD_LOCKING);
	if( !inlock )	return FALSE;
	SdAllocData* s = & c_sdAllocA[inSdId];

	// streamed ?
	if( s->mode == 0 )
		return FALSE;

	cursOut.i32[0] = NVIOP_SD_NOSEGMENT;
	cursOut.i32[1] = inSdId;
	cursOut.i32   += 2;

	DbgVerbosePrintf( "iop::SdNoSegment: id:%d\n", inSdId );

	_PreventFlushOverflow();
	return TRUE;
}


uint64
iop::SdGetENDX		(							)
{
	return c_sdENDX;
}


uint64
iop::SdGetENVX		(							)
{
	return c_sdENVX;
}


bool
iop::SdIsPlaying	(	int		inChannel		)
{
	if( inChannel<0 || inChannel>=48 )	return FALSE;
	uint64 chm = uint64(1) << inChannel;
	return ( chm & c_sdENVX ) != 0;
}


uint64
iop::SdGetLastDown	(							)
{
	uint64 down = c_sdDown;
	c_sdDown = 0;
	return down;
}




//
// ----------------


bool
iop::AllocIOPMem	(	uint32		inBSize		)
{
	NV_ASSERT( cd );
	if( c_waitMalloc || inBSize==0 )
		return FALSE;

	cursOut.i32[0] = NVIOP_MEM_ALLOC;
	cursOut.i32[1] = inBSize;
	cursOut.i32   += 2;

	c_waitMalloc	= TRUE;
	c_mallocPtr		= NULL;

	_PreventFlushOverflow();
	return TRUE;
}


void
iop::FreeIOPMem		(	pvoid		inPtr		)
{
	NV_ASSERT( cd );

	cursOut.i32[0] = NVIOP_MEM_FREE;
	cursOut.i32[1] = uint32(inPtr);
	cursOut.i32   += 2;

	_PreventFlushOverflow();
}


bool
iop::GetAllocIOPMemPtr	(	pvoid *		outPtr	)
{
	NV_ASSERT( cd );
	if( c_waitMalloc )
		return FALSE;

	if( outPtr ) {
		*outPtr		= c_mallocPtr;
		c_mallocPtr	= NULL;
	}

	return TRUE;
}


uint32
iop::GetIOPFreeMemSize	(			)
{
	return c_IOPFreeMemSize;
}


uint32
iop::GetIOPFreeMaxSize		(			)
{
	return c_IOPFreeMaxSize;
}




