/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#ifdef RVL

#define _NvCore_Math_NOT_GLOBAL_

#include <Nova.h>
#include <Kernel/NGC/NvkCore_Ctrl[NGC].h>
#include <Kernel/Common/NvkCore_Mem.h>
#include <Kernel/NGC/NvkSound[NGC].h>

#include <dolphin/pad.h>
#include <revolution/kpad.h>

//#include <revolution/os.h>
//#include <revolution/ax.h>
//#include <revolution/mix.h>
//#include <revolution/seq.h>
//#include <revolution/syn.h>
#include <revolution/wenc.h>
#include <revolution/wpad.h>


using namespace nv;

 
extern KPADStatus			kpads		[ WPAD_MAX_CONTROLLERS ][ KPAD_MAX_READ_BUFS ];
extern int32				kpad_reads 	[ WPAD_MAX_CONTROLLERS ]					  ;

uint32
nv::ctrl::GetKStatus (int32 chan , KStatus* outStatus, uint32 nbStatus	)
{	
	if( !outStatus || !nbStatus )
		return 0;
	if( chan >= WPAD_MAX_CONTROLLERS ||chan < 0)
		return 0;

	Memset( outStatus, 0, sizeof(KStatus) * nbStatus );
	
	uint nbOutS = nv::math::Min( kpad_reads[chan] , int(nbStatus) );

	for (uint i = 0 ; i < nbOutS ; ++i)
	{
		Zero( outStatus[i] );

		outStatus[i].hold			= kpads[chan][i].hold;
		outStatus[i].trig			= kpads[chan][i].trig;
		outStatus[i].release 		= kpads[chan][i].release;

		outStatus[i].acc.x 			= kpads[chan][i].acc.x;
		outStatus[i].acc.y 			= kpads[chan][i].acc.y;		
		outStatus[i].acc.z 			= kpads[chan][i].acc.z;
		outStatus[i].accValue 		= kpads[chan][i].acc_value;
		outStatus[i].accSpeed 		= kpads[chan][i].acc_speed;

		outStatus[i].pos.x 			= kpads[chan][i].pos.x;
		outStatus[i].pos.y 			= kpads[chan][i].pos.y;
		outStatus[i].vec.x			= kpads[chan][i].vec.x;
		outStatus[i].vec.y			= kpads[chan][i].vec.y;
		
		outStatus[i].speed 			= kpads[chan][i].speed;

		outStatus[i].horizon.x 		= kpads[chan][i].horizon.x;
		outStatus[i].horizon.y 		= kpads[chan][i].horizon.y;
		
		outStatus[i].horiVec.x		= kpads[chan][i].hori_vec.x;
		outStatus[i].horiVec.y		= kpads[chan][i].hori_vec.y;
		
		outStatus[i].horiSpeed 		= kpads[chan][i].hori_speed;

		outStatus[i].dist 			= kpads[chan][i].dist;
		outStatus[i].distVec		= kpads[chan][i].dist_vec;
		outStatus[i].distSpeed 		= kpads[chan][i].dist_speed;

		outStatus[i].accVertical.x 	= kpads[chan][i].acc_vertical.x;
		outStatus[i].accVertical.y 	= kpads[chan][i].acc_vertical.y;

		outStatus[i].devType 		= kpads[chan][i].dev_type;
		outStatus[i].err			= kpads[chan][i].wpad_err;
		outStatus[i].dpdValidFg 	= kpads[chan][i].dpd_valid_fg;
		outStatus[i].dataFormat 	= kpads[chan][i].data_format;

		if( outStatus[i].devType == WPAD_DEV_FREESTYLE )		    
		{
			outStatus[i].fs.stick.x		= kpads[chan][i].ex_status.fs.stick.x ;
			outStatus[i].fs.stick.y		= kpads[chan][i].ex_status.fs.stick.y ;		

			outStatus[i].fs.acc.x 		= kpads[chan][i].ex_status.fs.acc.x ;
			outStatus[i].fs.acc.y 		= kpads[chan][i].ex_status.fs.acc.y ;		
			outStatus[i].fs.acc.z 		= kpads[chan][i].ex_status.fs.acc.z ;
					
			outStatus[i].fs.accValue 	= kpads[chan][i].ex_status.fs.acc_value;
			outStatus[i].fs.accSpeed 	= kpads[chan][i].ex_status.fs.acc_speed;
		}

		if( outStatus[i].devType == WPAD_DEV_CLASSIC )
		{
			outStatus[i].cl.hold		= kpads[chan][i].ex_status.cl.hold;
			outStatus[i].cl.trig		= kpads[chan][i].ex_status.cl.trig;
			outStatus[i].cl.release		= kpads[chan][i].ex_status.cl.release;
			
			outStatus[i].cl.lStick.x	= kpads[chan][i].ex_status.cl.lstick.x;
			outStatus[i].cl.lStick.y	= kpads[chan][i].ex_status.cl.lstick.y;		
			outStatus[i].cl.rStick.x	= kpads[chan][i].ex_status.cl.rstick.x;
			outStatus[i].cl.rStick.y	= kpads[chan][i].ex_status.cl.rstick.y;		

			outStatus[i].cl.lTrigger	= kpads[chan][i].ex_status.cl.ltrigger;
			outStatus[i].cl.rTrigger	= kpads[chan][i].ex_status.cl.rtrigger;
		}
	}
	
	return nbOutS;
}

bool
nv::ctrl::IsConnected (	int32 chan	)
{
	if( chan >= WPAD_MAX_CONTROLLERS || chan < 0)
		return FALSE;
	
	u32 type;
	s32 ret = WPADProbe(chan,&type);
	
	if (ret == WPAD_ERR_NO_CONTROLLER)
		return FALSE;
	
	if (ret == WPAD_ERR_BUSY || ret == WPAD_ERR_TRANSFER) 
	{
		return TRUE;
	}
	
	if ( type == WPAD_DEV_NOT_FOUND )
		return FALSE;
	
	return TRUE;
}

void 	
nv::ctrl::SetPosParam ( int32 chan ,  float play_radius	, float sensitivity )
{
	KPADSetPosParam ( chan ,  play_radius	,  sensitivity );
}
	
void 	
nv::ctrl::SetHoriParam ( int32 chan ,  float  play_radius	, float sensitivity )
{
	KPADSetHoriParam ( chan ,  play_radius	, sensitivity );
}
									
void 
nv::ctrl::SetDistParam ( int32 chan ,  float play_radius	, float sensitivity )
{
	KPADSetDistParam ( chan ,  play_radius	, sensitivity );
}
									
void 	
nv::ctrl::SetAccParam ( int32 chan ,  float play_radius	,  float sensitivity )
{
	KPADSetAccParam ( chan ,  play_radius	,  sensitivity );
}
									
void 	
nv::ctrl::SetBtnRepeat ( int32 chan ,  float delay_sec ,  float pulse_sec )
{
	KPADSetBtnRepeat ( chan ,  delay_sec ,  pulse_sec );
}

void 	
nv::ctrl::SetObjInterval ( float interval )
{
	KPADSetObjInterval ( interval );
}

void 
nv::ctrl::SetSensorHeight ( int32 chan ,  float level )
{
	KPADSetSensorHeight ( chan ,  level );
}

int32
nv::ctrl::CalibrateDPD ( int32 chan )
{
	return KPADCalibrateDPD ( chan );
}
	
void
nv::ctrl::SetFSStickClamp ( int8 min , int8 max )
{
	KPADSetFSStickClamp ( min , max );
}

void
nv::ctrl::EnableDPD ( int32 chan )
{
	KPADEnableDPD ( chan );
}

void
nv::ctrl::DisableDPD ( int32 chan )
{
	KPADDisableDPD ( chan );
}
	
void
nv::ctrl::EnableAimingMode ( int32 chan )
{
	KPADEnableAimingMode ( chan );
}
	
void 	
nv::ctrl::DisableAimingMode ( int32 chan )
{
	KPADDisableAimingMode ( chan );
}

void
nv::ctrl::GetProjectionPos ( nv::math::Vec2 * dst ,  const nv::math::Vec2 *	src ,  const nv::math::Vec4 *	projRect ,  float viRatio 	)
{
	KPADGetProjectionPos ( (Vec2*)dst, (const Vec2*) src, ( const Rect *)projRect ,  viRatio 	);
}

void
nv::ctrl::EnableStickCrossClamp ( )
{
	KPADEnableStickCrossClamp (  );
}

void
nv::ctrl::DisableStickCrossClamp(  )
{
	KPADDisableStickCrossClamp(  );
}

int	
nv::ctrl::CtrlNoToChan	(	uint	inCtrlNo	){
	if (inCtrlNo > PAD_MAX_CONTROLLERS + WPAD_MAX_CONTROLLERS)
		return -1 ;
	
	return inCtrlNo - PAD_MAX_CONTROLLERS;
}

namespace 
{
	#define MEM_SOUND	nv::mem::MEM_NGC_MEM2
	static const uint MaxSlot = 16;
	
	
	struct SpeakerSlot
	{
		uint8 *	buffer;
		uint 	bsize;
		bool	playingInChan[WPAD_MAX_CONTROLLERS];
		bool	toUnlock;
		
		struct Trx 
		{
			nv::file::ReadRequest	rr 			;
			bool					isLoading	;
			uint 					byteAlign	;
			uint					realBSize	;
		} trx ;
	};

	struct SpeakerChan
	{
		int		slot	;
		int		pos		;
		
		bool	enable	;
		bool	playing	;
		bool	mute	;
	};
	
	SpeakerSlot		slots[MaxSlot];
	SpeakerChan		chans[WPAD_MAX_CONTROLLERS];
	bool			isInit	=	FALSE;
	
	void * 
	SoundAlloc( uint32 inBSize )
	{
		uint rbs = inBSize + 32; // 32 for aligment
		return nv::mem::NativeMalloc( rbs , 32, MEM_SOUND );
	}

	void 
	SoundFree( void * inPtr )
	{
		nv::mem::NativeFree( inPtr, MEM_SOUND );
	}

	void disableCallBack(s32 chan, s32 result)
	{
		if (result == WPAD_ERR_NONE)
			chans[chan].enable = FALSE;
	}
	
	void playCallBack(s32 chan, s32 result)
	{
		if (result == WPAD_ERR_NONE) 
			chans[chan].playing = TRUE;
		else 
		{
			slots[chans[chan].slot].playingInChan[chan] = FALSE;
			chans[chan].slot = -1;
			chans[chan].enable = FALSE;
			WPADControlSpeaker( chan, WPAD_SPEAKER_OFF  , disableCallBack);
		}
	}
	
	void enableCallBack(s32 chan, s32 result)
	{
		if (result == WPAD_ERR_NONE) 
		{
			chans[chan].enable = TRUE;
			
			s32 err = WPADControlSpeaker( chan, WPAD_SPEAKER_PLAY  , playCallBack );
			if ( err != WPAD_ERR_NONE ) 
			{
				slots[chans[chan].slot].playingInChan[chan] = FALSE;	
				chans[chan].slot = -1;
				chans[chan].enable = FALSE;
				WPADControlSpeaker( chan, WPAD_SPEAKER_OFF  , disableCallBack);
			}
		}
		else {
			slots[chans[chan].slot].playingInChan[chan] = FALSE;
			chans[chan].slot = -1;
		}
	}

}

void ctrl_InitSpeaker	()
{
	if (isInit) return ;
	isInit = TRUE;
	Memset(slots,0,sizeof(slots));
	Memset(chans,0,sizeof(chans));
	
	for (uint i = 0 ; i < WPAD_MAX_CONTROLLERS ; ++i)
		chans[i].slot = -1;
}

void ctrl_ShutSpeaker	()
{
	if (!isInit) return ;
	isInit = FALSE;
	for (uint i = 0 ; i < MaxSlot ; ++i)
	{
		if (slots[i].buffer)
		{
			SoundFree(slots[i].buffer);
			slots[i].buffer = NULL;
		}
	}
}

void ctrl_UpdateSpeaker	()
{
	if (!isInit) return ;
	
	for (uint i = 0 ; i < nv::ctrl::SpeakerGetMaxSlot() ; ++i )
	{
		if ( slots[i].trx.isLoading && slots[i].trx.rr.IsReady())
		{
			if (slots[i].trx.rr.GetState() == file::ReadRequest::COMPLETED && !slots[i].toUnlock)
			{
				uint nbSample 	= slots[i].trx.realBSize >> 1 ;
				
				slots[i].bsize  = WENCGetEncodeBufferSize(nbSample);
				slots[i].buffer = (uint8*)SoundAlloc(slots[i].bsize);

				uint8 * buffer = ((uint8*)slots[i].trx.rr.bufferPtr) + slots[i].trx.byteAlign;

				WENCInfo info;
				WENCGetEncodeData(	&info,WENC_FLAG_FIRST,
									(int16*)buffer,nbSample,
                      				slots[i].buffer	);
			}
			
			slots[i].trx.isLoading = FALSE;
			SoundFree(slots[i].trx.rr.bufferPtr);
			slots[i].trx.rr.bufferPtr = NULL;
			
			if (slots[i].toUnlock)
				Memset(slots+i,0,sizeof(SpeakerSlot));
		}
	}
}

uint
nv::ctrl::SpeakerGetMaxSlot		(	)
{
	return MaxSlot;
}
	
bool
nv::ctrl::SpeakerLockSlot	(	uint		inSlot		,
								NvSound	*	inSnd		)
{
	if (!isInit) return FALSE;
	if ( inSlot >= SpeakerGetMaxSlot() ) return FALSE;
	if ( slots[inSlot].buffer ) return FALSE;
	if (! inSnd ) return FALSE;
	NvkSound * ksound = inSnd->GetKInterface();
	
	if (! ksound->IsWMSpeaker() ) return FALSE;
	
	inSnd->AddRef();
	
	uint32 boffset;
	uint32 bsize;
	ksound->GetBFSoundData (boffset,bsize);	

	NV_ASSERT( ! (bsize & 0x1));
	uint8 * tmpBuffer 			= (uint8 * )SoundAlloc( bsize + 64);
	slots[inSlot].trx.byteAlign = boffset & 0x3;
	
	slots[inSlot].trx.rr.Setup(tmpBuffer,bsize +32 , boffset - slots[inSlot].trx.byteAlign);
	slots[inSlot].trx.realBSize	 = bsize;
	
	if ( !file::AddReadRequest(&slots[inSlot].trx.rr,8 ) )
	{
		SoundFree(tmpBuffer);
		inSnd->Release();
		return FALSE;
	}
	
	slots[inSlot].trx.isLoading = TRUE;
	for (uint i = 0 ; i < WPAD_MAX_CONTROLLERS ; ++i)
		slots[inSlot].playingInChan[i] = FALSE;
		
	return TRUE;
}
											
bool
nv::ctrl::SpeakerUnlockSlot	(	uint	inSlot	)
{
	if (!isInit	) return FALSE;
	if ( inSlot >= SpeakerGetMaxSlot() ) return FALSE;
	if ( !slots[inSlot].buffer && slots[inSlot].trx.isLoading) return TRUE;
	if ( slots[inSlot].buffer )
		for (uint i = 0 ; i < WPAD_MAX_CONTROLLERS ; ++i) 
			if (slots[inSlot].playingInChan[i]) return FALSE;
	
	if ( slots[inSlot].buffer )
	{
		SoundFree(slots[inSlot].buffer);
		Memset(& slots[inSlot],0,sizeof(SpeakerSlot));
	}
	else if ( slots[inSlot].trx.isLoading )
	{
		slots[inSlot].toUnlock = TRUE;
	}
	
	return TRUE;
}

nv::ctrl::SlotState
nv::ctrl::SpeakerGetSlotState	(	uint	inSlot	)
{
	if (!isInit) return ST_Unlocked;
	if ( inSlot >= SpeakerGetMaxSlot() ) return ST_Unlocked;
	
	if ( slots[inSlot].buffer )
		return ST_Locked;
	
	if ( slots[inSlot].trx.isLoading ) 
	{
		if (slots[inSlot].toUnlock)	
			return ST_Unlocking;
		else 
			return ST_Locking;
	}  
		
	return ST_Unlocked;
}

/*
bool
nv::ctrl::SpeakerEnable	(	uint	inChan		,
							bool	inEnable	)
{
	if (!isInit) 						return FALSE;
	if (inChan >= WPAD_MAX_CONTROLLERS ) 	return FALSE;

	if ( chans[inChan].enable == inEnable ) return TRUE;
	
	int32 err = WPAD_ERR_NONE;
	
	if (inEnable)
		err = WPADControlSpeaker( inChan, WPAD_SPEAKER_ON  , enableCallBack );
	else 
		err = WPADControlSpeaker( inChan, WPAD_SPEAKER_OFF , disableCallBack );

	return err == WPAD_ERR_NONE ;
}
*/											
bool
nv::ctrl::SpeakerPlay	(	uint	inChan	,
							uint	inSlot	)
{
	if (!isInit) return FALSE;
	if (inChan >= WPAD_MAX_CONTROLLERS 			) 	return FALSE;
	if (inSlot >= SpeakerGetMaxSlot() 			)	return FALSE;
	if (SpeakerGetSlotState(inSlot)!=ST_Locked	)	return FALSE;
	
	bool intr = OSDisableInterrupts();
	
	if (chans[inChan].slot >= 0) 
	{
		if ( !chans[inChan].playing ) {
			OSRestoreInterrupts(intr);
			return FALSE;	
		}
		
		NV_ASSERT(SpeakerGetSlotState(chans[inChan].slot) == ST_Locked)
		slots[chans[inChan].slot].playingInChan[inChan] = FALSE;
		
		chans[inChan].slot 	= inSlot;
		chans[inChan].pos 	= 0;
	}
	else 
	{
		if ( chans[inChan].playing ) {
			OSRestoreInterrupts(intr);
			return FALSE;	
		}
		
		chans[inChan].slot 	= inSlot;
		chans[inChan].pos 	= 0;
		chans[inChan].mute  = FALSE;

		int32 err = WPADControlSpeaker( inChan, WPAD_SPEAKER_ON  , enableCallBack );
		if (err != WPAD_ERR_NONE){
			OSRestoreInterrupts(intr);
			chans[inChan].slot = -1;
			return FALSE;	
		}
	}
	
	slots[chans[inChan].slot].playingInChan[inChan] = TRUE;

	OSRestoreInterrupts(intr);

	return TRUE;	
}
											
bool
nv::ctrl::SpeakerStop	(	uint	inChan	)
{
	if (!isInit) return FALSE;
	return FALSE;
}

bool
nv::ctrl::SpeakerMute(	uint inChan		,
						bool inMute		)
{
	return FALSE;
}
													
nv::ctrl::SpeakerState
nv::ctrl::SpeakerGetWMState		(	uint	inChan	)
{
	if (!isInit) return SS_Stop;
	return SS_Stop;
}

#endif RVL
