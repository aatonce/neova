/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#ifdef RVL

//	**********************************************
//	* HomeMenuWii 								 *
//	**********************************************

#include <NGC/GEK/GEK.h>
#define Vec2 Vector2
#define Rect WiiRect
#include <revolution/kpad.h>
#undef Vec2
#undef WiiRect
#include <revolution/mem/allocator.h>
#include <revolution/hbm.h>
#include <revolution/tpl.h>
#include <revolution/sc.h>
#include <kernel/NGC/GEK/GEK_gx.h>

/* Declarations for Wii Remote speakers */
#include <revolution/wenc.h>
#include <revolution/mix.h>
#include <revolution/syn.h>
#include <revolution/seq.h>

#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>
#include <NvSndManager.h>
#include <NvDpyManager.h>
#include <NvFrameClear.h>
#include <NvRawData.h>


#include <Kernel/NGC/NvkCore_HBM[NGC].h>

//#define DISABLE_BACK_COPY
//#define HBM_NO_SAVE
//#define BTN_NUM_3

// ****************************************************************************
// ****************************************************************************
#ifdef HBM_NO_SAVE
    const char * saveMessage = "\\home_nosave.csv";
#else
	const char * saveMessage = "\\home.csv";
#endif

#ifdef BTN_NUM_3
    const char dirName[] = "*HomeButton3";
#else
    const char dirName[] = "*HomeButton2";
#endif

// ****************************************************************************
// ****************************************************************************
namespace 
{

	bool 	UpdateHomeButton		();
	bool 	CalcDigitalCursorPos	( uint32 _Button, Vec2* _Pos )	;
	float 	AbsClamp				( float _val, float _max );
	bool 	CalcAnalogCursorPos		( float _StickX, float _StickY, Vec2* _Pos );
	void	Leave();
	bool	Loop();
	
	bool				hbmIsInit				= false;
	bool				hbmIsLoad				= false;
	HBMDataInfo 		s_HBMInfo;
	HBMControllerData 	s_ConData;
	NvFrameClear * 		pClear 					= NULL;	
	void* 				m_Sound_Buf 			= NULL;
	void* 				m_Sound_Data			= NULL;
	void*				layoutBuf				= NULL;
	void*				spkSeBuf				= NULL;
	void*				msgBuf					= NULL;
	void*				configBuf				= NULL;
	bool				m_MustRestartEngine		= FALSE;
	bool 				m_PowerResetHomeState	= FALSE;
	bool 				m_TemporaryResetState	= FALSE;
	const float 		s_SCStickMoveCoe 		= 2048.0f/72.0f; /* Analog stick movement coefficient */
	uint32				configBSize				= 0;
	volatile s32		asyncResult				= 0;
	volatile bool		reading					= false;
	
	void DrawTexQuad(int inSizeX, int inSizeY, int inZ = 0)
	{
		GXSetNumChans		( 0 );
	    if (!inZ)
	    	GXSetZMode		(GX_FALSE,GX_ALWAYS ,GX_FALSE );
		GXSetAlphaCompare	(GX_ALWAYS,0U,GX_AOP_AND,GX_ALWAYS,0U);
	    
	    // set vertex descriptor
	    GXClearVtxDesc();
	    GXSetVtxDesc(GX_VA_POS,  GX_DIRECT);
	    GXSetVtxDesc(GX_VA_TEX0, GX_DIRECT);
		GXSetVtxAttrFmt( GX_VTXFMT0, GX_VA_POS ,GX_POS_XYZ, GX_S16,0);
		GXSetVtxAttrFmt( GX_VTXFMT0, GX_VA_TEX0,	GX_TEX_ST, GX_S8,0);	
	
	    GXBegin(GX_QUADS, GX_VTXFMT0, 4);
	    	uint16 z = uint16(inZ);
	        GXPosition3s16( 0, inSizeY	, z );
	        GXTexCoord2s8 ( 0, 1 );
	        GXPosition3s16( inSizeX  , inSizeY	, z );
	        GXTexCoord2s8 ( 1, 1);
	        GXPosition3s16( inSizeX  , 0, z );
	        GXTexCoord2s8 ( 1, 0 );
	        GXPosition3s16( 0, 0, z );
	        GXTexCoord2s8 ( 0, 0 );
	    GXEnd();
	}
	
	void FixTevTex() {
		GXSetBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
		GXSetNumTexGens		(1);
    	GXSetTexCoordGen	(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
		GXSetNumTevStages	(1);
    	GXSetTevOp			(GX_TEVSTAGE0, GX_REPLACE);
    	GXSetTevOrder		(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR_NULL);
	}
	
	void
	SetupDrawing 	(uint inW , uint inH)
	{	
						
		GXSetViewport(0, 0, inW, inH, 0.0f, 1.0f);
		GXSetScissor (0, 0, inW, inH );				
	    
		// TR
		
		Matrix proj2D;
		MatrixZero(&proj2D);
		proj2D.m11 =	2.0f  / inW ;
		proj2D.m22 =	-2.0f / inH ;
		proj2D.m33 =	1.0f;
		proj2D.m14 =	-1.0f ;
		proj2D.m24 =	1.0f  ;
		proj2D.m34 =	-1.0f ;
		proj2D.m44 =	1.0f  ;
	    GXSetProjection(proj2D.m, GX_ORTHOGRAPHIC);
		
		Matrix 	 toView;
		MatrixIdentity(&toView)	;
		GXLoadPosMtxImm	(toView.m, GX_PNMTX0);
		GXSetCurrentMtx( GX_PNMTX0 );
	}

	static void 
	AudioFrameCallback(void)
	{
	    //	***	run the sequencer
	    SEQRunAudioFrame();

	    //	***	run the synth
	    SYNRunAudioFrame();

	    //	***	tell the mixer to update settings
	    MIXUpdateSettings();
	}

	static void *
	aram_malloc( u32 num_bytes )
	{
		return nv::mem::NativeMalloc( num_bytes+32, 32, nv::mem::MEM_NGC_MEM2 );
	}

	static u8 
	aram_free( void * ptr )
	{
		nv::mem::NativeFree( ptr, nv::mem::MEM_NGC_MEM2 );
		return 1;
	}

	/* Event/Sound callback function */
	static int 
	SoundCallback( int evt, int arg )
	{
	    //Printf( "SoundCallback: %d, %d\n", evt, arg );
	    return HBMSEV_RET_NONE;
	}


	void ReadDvdFileCallback(s32 result, DVDFileInfo* fileInfo)
	{
		asyncResult = result;
		reading		= false;
	}
	
	bool dump_uid_from_bf (	uint32		inUID			,
							void*&		outAddr			,
							uint&		outBSize		,
							bool		inKeepIt = true	)
	{
		NvRawData* raw;
		raw = NvRawData::Create( inUID );
		if( !raw )
			return FALSE;

		if (inKeepIt) 
		{
			outBSize = raw->GetDataBSize();
			outAddr  = (byte*) NvMalloc( outBSize );
			NV_ASSERT( outAddr );
			Memcpy( (void*)outAddr, raw->GetDataPtr(), outBSize );
		}
		
		SafeRelease( raw );
		return TRUE;
	}

/*	
	static void* 
	ReadDvdFile(		const char*     fileName		,
	    				u32* 			fileSize = NULL	)
	{
	    u32	 		fileLen		= 0, 
	    			fileLenUp32 = 0;
	    void* 		readBuf		= NULL;
	    s32 		readBytes	= 0;
	    DVDFileInfo fileInfo;
	    
		if (reading) return NULL;
		if (! DVDOpen(fileName, &fileInfo))
	        return NULL;

	    fileLen     = DVDGetLength(&fileInfo);
	    if( (fileLen % 32) != 0 )	fileLenUp32 = fileLen + (32 - (fileLen % 32));
	    else						fileLenUp32 = fileLen;
	    
	    readBuf 	= NvMalloc(fileLenUp32);

		asyncResult = 0;
		reading		= true;
		
		DVDReadAsync(&fileInfo,readBuf,fileLenUp32,0,(DVDCallback)&ReadDvdFileCallback);
		bool canceled = false;
	    while (reading) 
	    {
	    	if (!canceled)
	    	{
		    	s32 status1 = DVDGetFileInfoStatus(&fileInfo);
		    	s32 status2 = DVDGetDriveStatus();
		    	if (	status1 == DVD_STATE_FATAL_ERROR	|| 
		    			status1 == DVD_STATE_NO_DISK 		|| 
		    			status1 == DVD_STATE_WRONG_DISK 	|| 
		    			status1 == DVD_STATE_CANCELED 		|| 
		    			status1 == DVD_STATE_RETRY 			||
		    			status2 == DVD_STATE_FATAL_ERROR	|| 
		    			status2 == DVD_STATE_NO_DISK 		|| 
		    			status2 == DVD_STATE_WRONG_DISK 	|| 
		    			status2 == DVD_STATE_CANCELED 		|| 
		    			status2 == DVD_STATE_RETRY 			)
		    	{
		    		DVDCancel(&fileInfo.cb);
		    		canceled = true;
		    	}
	    	}
	    }
	    if ( asyncResult < int(fileLen) )
	    {
	    	NvFree(readBuf);
	    	readBuf = NULL;
	    }

	    if( fileSize )
	        *fileSize   = fileLen;
	    DVDClose(&fileInfo);
	    return readBuf;
	}
*/
	bool	
	Loop()
	{
		bool l_Result = UpdateHomeButton();
		
		OSInitFastCast();
	    GXInvalidateVtxCache();
	    GXInvalidateTexAll();
	   	GXSetPixelFmt(GX_PF_RGB8_Z24,GX_ZC_LINEAR);

	    GXClearVtxDesc();
	    Mtx44 projMtx;
	//	if(g_System.GetRenderer().IsWideScreen())
		{
		//	MTXOrtho(projMtx, 228.0f, -228.0f, -416.0f, 416.0f, 0.0f, 500.0f);	
		}
	//	else
		{
			MTXOrtho(projMtx, 228.0f, -228.0f, -304.0f, 304.0f, 0.0f, 500.0f);	
		}
	    
	    Mtx44 mv;
	    MTXIdentity(mv);
	    GXLoadPosMtxImm(mv, GX_PNMTX1);
		GXLoadTexMtxImm(mv,GX_TEXMTX0,GX_MTX3x4);

	    GXClearVtxDesc();
		GXSetProjection(projMtx, GX_ORTHOGRAPHIC);
	    GXSetVtxAttrFmt(GX_VTXFMT4, GX_VA_POS,  GX_POS_XY,  GX_F32, 0);
	    GXSetVtxAttrFmt(GX_VTXFMT4, GX_VA_CLR0, GX_CLR_RGB, GX_RGB8, 0);
	    GXSetVtxDesc(GX_VA_POS,  GX_DIRECT);
	    GXSetVtxDesc(GX_VA_CLR0, GX_DIRECT);
	    GXSetNumChans(1);
	    GXSetNumTexGens(0);
	    GXSetNumTevStages(1);
	    GXSetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEXMAP_NULL, GX_COLOR0A0);
	    GXSetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
	    GXSetBlendMode(GX_BM_NONE, GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR);
	    GXSetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE);
	    GXSetCurrentMtx( GX_PNMTX1 );

		HBMDraw();	
	    GXSetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE);
	    GXSetColorUpdate(GX_TRUE);
		GXDrawDone();

		
		DpyManager::BeginFrame();
		DpyManager::Draw(pClear);
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
					
		return l_Result;	
	}

	void	
	Leave()
	{
		//	*** Shut AX Sound System	
		SEQQuit();
		SYNQuit();
		MIXQuit();
		AXQuit();
		AIReset();	
		
		//	*** Reset Rendering Engine
	//	DpyManager::Shut();
	//	DpyManager::Init(DpyManager::CS_RIGHT_HANDED);

		//	*** Restart SoundEngine
		if(m_MustRestartEngine)
		{
			SndManager::Init();
		}
		
		m_MustRestartEngine = FALSE;

		nv::ctrl::Open();	
		nv::core::Update();
		
	//tt	g_System.GetPadManager().Update();
	//tt	g_System.DisablePowerResetHome(m_PowerResetHomeState);
	//tt	g_System.DisableResetTemporary(m_TemporaryResetState);
	}

	float 
	AbsClamp( float _val, float _max )
	{
	    return ( ( _val > _max ) ? _max : ( _val < -_max ) ? -_max : _val );
	}

	bool 
	CalcAnalogCursorPos( float _StickX, float _StickY, Vec2* _Pos )
	{
	    float x,y;
	    x = ( _StickX / s_SCStickMoveCoe );
	    y = ( _StickY / s_SCStickMoveCoe );
	    x = AbsClamp( x, 1.0f );
	    y = AbsClamp( y, 1.0f );
	    if( x == 0.0f && y == 0.0f ) return FALSE;
	    _Pos->x = AbsClamp( _Pos->x + x, 1.0f );
	    _Pos->y = AbsClamp( _Pos->y - y, 1.0f );
	    return TRUE;
	}


	bool 
	CalcDigitalCursorPos( uint32 _Button, Vec2* _Pos )
	{
		//	*** Cursor movement process using +control pad
	    float l_Spd =1.0f / s_SCStickMoveCoe;
	    float l_Spd2= l_Spd * 0.7071f;

	    _Button&=KPAD_CL_BUTTON_UP|KPAD_CL_BUTTON_LEFT|KPAD_CL_BUTTON_DOWN|KPAD_CL_BUTTON_RIGHT;
	    switch (_Button)
	    {
	    case KPAD_CL_BUTTON_UP:     _Pos->y-=l_Spd; break;
	    case KPAD_CL_BUTTON_LEFT:   _Pos->x-=l_Spd; break;
	    case KPAD_CL_BUTTON_DOWN:   _Pos->y+=l_Spd; break;
	    case KPAD_CL_BUTTON_RIGHT:  _Pos->x+=l_Spd; break;
	    case KPAD_CL_BUTTON_UP  |KPAD_CL_BUTTON_LEFT:   _Pos->y-=l_Spd2; _Pos->x-=l_Spd2; break;
	    case KPAD_CL_BUTTON_UP  |KPAD_CL_BUTTON_RIGHT:  _Pos->y-=l_Spd2; _Pos->x+=l_Spd2; break;
	    case KPAD_CL_BUTTON_DOWN|KPAD_CL_BUTTON_LEFT:   _Pos->y+=l_Spd2; _Pos->x-=l_Spd2; break;
	    case KPAD_CL_BUTTON_DOWN|KPAD_CL_BUTTON_RIGHT:  _Pos->y+=l_Spd2; _Pos->x+=l_Spd2; break;
	    default: return FALSE;
	    }
	    _Pos->x = AbsClamp( _Pos->x, 1.0f );
	    _Pos->y = AbsClamp( _Pos->y, 1.0f );
	    return TRUE;
	}

	bool
	UpdateHomeButton()
	{
		int32 				l_Wpad_Result[WPAD_MAX_CONTROLLERS];
		uint32 				l_Pad_Type[WPAD_MAX_CONTROLLERS];
		int32 				l_Kpad_Read[WPAD_MAX_CONTROLLERS];
		static KPADStatus 	l_Kpads[ WPAD_MAX_CONTROLLERS ][ KPAD_MAX_READ_BUFS ];
		

	   	//	***	 Wii controllers
	   	uint32 i;
	    for( i = 0; i < WPAD_MAX_CONTROLLERS; i++ )
	    {
	        l_Wpad_Result[i] = WPADProbe( (long)i,(unsigned long*) &l_Pad_Type[i] );
	        s_ConData.wiiCon[i].use_devtype = l_Pad_Type[i];
	        l_Kpad_Read[i] = KPADRead( i, &l_Kpads[i][0], KPAD_MAX_READ_BUFS );
	        
	        switch( l_Wpad_Result[i] )
	        {
	        	//	***	During the following error state, directly apply the value obtained through KPADRead
	        case WPAD_ERR_BUSY:
	        case WPAD_ERR_TRANSFER:
	        case WPAD_ERR_INVALID:
	        case WPAD_ERR_CORRUPTED:
	        case WPAD_ERR_NONE:
	            Zero(&(l_Kpads[i][0].ex_status),sizeof(KPADEXStatus));
	            s_ConData.wiiCon[i].kpad = &l_Kpads[i][0];  
	            {
	                /* 
	                According to the guidelines, when there is input to the Classic Controller, the priority is given to the Classic Controller, and the DPD coordinates are inherited.
	                
	                When there is no input to the Classic Controller, the DPD absolute coordinates are specified.
	                */
	                bool l_Input_Classic = FALSE;
	      /*          l_Input_Classic = CalcDigitalCursorPos(
	                    s_ConData.wiiCon[i].kpad->ex_status.cl.hold,
	                    &s_ConData.wiiCon[i].pos );
	                
	                
	                l_Input_Classic = l_Input_Classic | CalcAnalogCursorPos(
	                    s_ConData.wiiCon[i].kpad->ex_status.cl.lstick.x,
	                    s_ConData.wiiCon[i].kpad->ex_status.cl.lstick.y,
	                    &s_ConData.wiiCon[i].pos );
	    */                
	                if( !l_Input_Classic && s_ConData.wiiCon[i].kpad->dpd_valid_fg > 0)
	                
	                {
	                    s_ConData.wiiCon[i].pos.x = s_ConData.wiiCon[i].kpad->pos.x;
	                    s_ConData.wiiCon[i].pos.y = s_ConData.wiiCon[i].kpad->pos.y;
	                }
	                
	            }
	                 
	            if( l_Kpads[i][0].trig == KPAD_BUTTON_1 )
	            {
	                 VISetBlack(FALSE);
	                 VIFlush();
	            }
	            break;

	       /* Use NULL for the following error state */
	        case WPAD_ERR_NO_CONTROLLER:
	        default:
	            s_ConData.wiiCon[i].kpad = NULL;
	            break;
	        }
	    }
	    for ( i = 0; i < WPAD_MAX_CONTROLLERS; i++ )
	    {
	    	/* Changed the pad used in the HOME button menu */
	        if ( WPAD_ERR_NONE == l_Wpad_Result[i] 
	        	&&	0 < l_Kpad_Read[i] 
	        	&&  s_ConData.wiiCon[i].kpad )
	        {
	            if ( s_ConData.wiiCon[i].use_devtype == WPAD_DEV_CLASSIC 
	            	 && l_Kpads[i]->dev_type != WPAD_DEV_CLASSIC)
	            {
	                s_ConData.wiiCon[i].use_devtype = WPAD_DEV_CORE;
	            }
	        }
	    }
	        
	    HBMSelectBtnNum l_Button = HBMCalc( &s_ConData);
	    HBMUpdateSound();
	    
		if((l_Button==HBM_SELECT_HOMEBTN) 
			||(l_Button==HBM_SELECT_BTN1)
			||(l_Button==HBM_SELECT_BTN2))
	    {
	   		Printf("Button Pressed\n");
	   		return false;
	    }
	    
	    return true;
	    
	}
}

bool
nv::hbm::PreLoad(NvUID * inNvUid)
{
	if (hbmIsLoad) return true;
	char 		l_NameBuf[128];
	char 		l_NameBuf2[128];
	void * 		foo;
	uint32 		bar;
	UInt32A 	uidA;
	int32 		uid = INVALID_RSC_UID;
	bool		foundIt = false;
	
    //	***	Load Layout
    Strcpy((char *) l_NameBuf, dirName );
    
    uint8 lg = SCGetLanguage();
    
    if (lg == SC_LANG_ENGLISH) 
    {
    	foundIt = true;
    	Strcat( (char *)l_NameBuf, "\\homeBtn_ENG.arc" );
    }
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn_ENG.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }
    
	if (lg == SC_LANG_GERMAN) 
	{
		foundIt = true;
		Strcat( (char *)l_NameBuf, "\\homeBtn_GER.arc" );
	}
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn_GER.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }
    
    if (lg == SC_LANG_FRENCH) 
    {
    	foundIt = true;
    	Strcat( (char *)l_NameBuf, "\\homeBtn_FRA.arc" );
    }
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn_FRA.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }
    
	if (lg == SC_LANG_SPANISH) 
	{
		foundIt = true;
		Strcat( (char *)l_NameBuf, "\\homeBtn_SPA.arc" );
	}
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn_SPA.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }

	if (lg == SC_LANG_ITALIAN) 
	{
		foundIt = true;
		Strcat( (char *)l_NameBuf, "\\homeBtn_ITA.arc" );
	}
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn_ITA.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }

	if (lg == SC_LANG_DUTCH) 
	{
		foundIt = true;
		Strcat( (char *)l_NameBuf, "\\homeBtn_NED.arc" );
	}
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn_NED.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }

	if (lg == SC_LANG_SIMP_CHINESE || lg == SC_LANG_TRAD_CHINESE) 
	{
		foundIt = true;
		Strcat( (char *)l_NameBuf, "\\homeBtn_CHN.arc" );
	}
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn_CHN.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }

	if (lg == SC_LANG_KOREAN )
	{
		foundIt = true;
		Strcat( (char *)l_NameBuf, "\\homeBtn_KOR.arc" );
	}
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn_KOR.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }

	if (lg == SC_LANG_JAPANESE || !foundIt)
	{
        s_HBMInfo.region=SC_LANG_JAPANESE;
        Strcat( (char *)l_NameBuf, "\\homeBtn.arc" );
    }
    else
    {
    	Strcpy((char *) l_NameBuf2, dirName );
    	Strcat((char *) l_NameBuf2, "\\homeBtn.arc" );
    	inNvUid->FindUIDByName( uidA, l_NameBuf2, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
		uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
		dump_uid_from_bf(uid,foo,bar,false);
    }
    
	uint32 bsize;
   
	inNvUid->FindUIDByName( uidA, l_NameBuf, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
	uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
	dump_uid_from_bf(uid,layoutBuf,bsize);
    
	//	***	Load Sounds
	Strcpy( (char *)l_NameBuf, dirName );
	Strcat( (char *)l_NameBuf, "\\SpeakerSe.arc" );
	inNvUid->FindUIDByName( uidA, l_NameBuf, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
	uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
	dump_uid_from_bf(uid,spkSeBuf,bsize);

	//	***	Load save messages
	Strcpy( (char *)l_NameBuf, dirName );
	Strcat( (char *)l_NameBuf, saveMessage );
	inNvUid->FindUIDByName( uidA, l_NameBuf, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
	uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
	dump_uid_from_bf(uid,msgBuf,bsize);

	//	***	Load config    
	Strcpy( (char *)l_NameBuf, dirName );
	Strcat( (char *)l_NameBuf, "\\config.txt" );
	inNvUid->FindUIDByName( uidA, l_NameBuf, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
	uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
	dump_uid_from_bf(uid,configBuf,configBSize);
	s_HBMInfo.configBufSize = bsize;
	
	//	***	Load Sound
	Strcpy((char*) l_NameBuf, dirName );
	Strcat((char*)l_NameBuf, "\\HomeButtonSe.arc" );
	inNvUid->FindUIDByName( uidA, l_NameBuf, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
	uid = (uidA.size() == 1U) ? uidA[0] : INVALID_RSC_UID;
	dump_uid_from_bf(uid,m_Sound_Data,bsize);


	if ( !m_Sound_Data || !configBuf || !msgBuf || !spkSeBuf || !layoutBuf){
		SafeFree	(layoutBuf);
		SafeFree	(spkSeBuf);
		SafeFree	(msgBuf);
		SafeFree	(configBuf);
		SafeFree	(m_Sound_Data);
		return false;
	}

	hbmIsLoad = true;
	return true;
}

// ****************************************************************************
// ****************************************************************************

bool	
nv::hbm::Init()
{
	if ( hbmIsInit) return true;
	if (!hbmIsLoad)	return false;

	m_Sound_Buf = nv::mem::NativeMalloc( HBM_MEM2_SIZE_DVD+32, 32, nv::mem::MEM_NGC_MEM2 );

	//	***	Home Button
	Zero(&s_HBMInfo,sizeof(s_HBMInfo));

    s_HBMInfo.region	= SCGetLanguage();
    s_HBMInfo.layoutBuf = layoutBuf;
	s_HBMInfo.spkSeBuf  = spkSeBuf;
	s_HBMInfo.msgBuf 	= msgBuf;    
	s_HBMInfo.configBuf = configBuf;
	s_HBMInfo.configBufSize = configBSize;
	//	***	Init sound    
    s_HBMInfo.sound_callback = SoundCallback;
    s_HBMInfo.backFlag       = 1;
    s_HBMInfo.cursor         = 0;
    
    //	*** Allocate memory for HBM
    s_HBMInfo.mem = NvMalloc( HBM_MEM_SIZE );
    s_HBMInfo.memSize = HBM_MEM_SIZE;
    s_HBMInfo.pAllocator = NULL;
    s_HBMInfo.messageFlag = 0  ;

    //	*** Set the adjust value depending on the screen mode*/
 	bool l_Wideflag = false;
 	bool l_Tvmode 	= TRUE;
 	
	if( !l_Wideflag ){	        /* 4:3 */
        s_HBMInfo.adjust.x       = 1.0f;
        s_HBMInfo.adjust.y       = 1.0f;
    }
    else {						/* 16:9 */
        s_HBMInfo.adjust.x       = 832.f / 608.f;
        s_HBMInfo.adjust.y       = 1.0f;
    }
    
    if(l_Tvmode == FALSE)		/* NTSC : 60Hz */
		s_HBMInfo.frameDelta     = 1.0f;
    else						/* PAL : 50Hz */
        s_HBMInfo.frameDelta     = 1.2f;

	m_PowerResetHomeState = FALSE;
	m_TemporaryResetState = FALSE;

	pClear = NvFrameClear::Create ();
	pClear->SetColor(0x00000000);
	pClear->EnableDepth	( true	);
	pClear->EnableColor	( true	);

	hbmIsInit = true;
	return true;
}

void	
nv::hbm::Shut()
{
	if (!hbmIsInit ) return ;
	hbmIsInit = false;
	
	if (m_Sound_Data) {
		HBMDeleteSound	( );
		HBMDelete		( );
		nv::mem::NativeFree( m_Sound_Buf , nv::mem::MEM_NGC_MEM2 );
		NvFree			( s_HBMInfo.mem );
		Zero			( s_HBMInfo );
		SafeRelease		( pClear );
	}
	
	Leave();
}

void		
nv::hbm::Release	(	)
{
	if (!hbmIsLoad)	return ;
	
	SafeFree	(layoutBuf);
	SafeFree	(spkSeBuf);
	SafeFree	(msgBuf);
	SafeFree	(configBuf);
	SafeFree	(m_Sound_Data);
	
	hbmIsLoad = false;
}

void
nv::hbm::Execute()
{
	NV_ASSERTC(m_Sound_Buf,"Hbm must be initialized")
	
#ifndef DISABLE_BACK_COPY

	uint 		efbW 	 = gx::GetWidth();
	uint 	 	efbH 	 = gx::GetHeight();
	uint 		bsize 	 = GXGetTexBufferSize(efbW, efbH, GX_TF_RGBA8, GX_FALSE, 0);
	uint8 * 	backPict = (uint8 *)nv::mem::NativeMalloc( bsize , 32 , nv::mem::MEM_NGC_MEM2 );
//	Memset(backPict,255,bsize);
	GXTexObj 	backTex;
	
	if (backPict) 
	{
		NV_ASSERT(backPict);
		gx::CopyTex(backPict,gx::CF_RGBA8);
		DCFlushRange(backPict,bsize);
		GXTexModeSync();
		GXPixModeSync();
		GXInvalidateTexAll();
		GXFlush();
		
		GXInitTexObj	( &backTex, backPict , efbW , efbH , GX_TF_RGBA8, GX_CLAMP, GX_CLAMP, GX_FALSE );
    	GXInitTexObjLOD	( &backTex, GX_NEAR, GX_NEAR, 0, 0, 0,GX_FALSE, GX_FALSE, GX_ANISO_1 );
	}
	
#endif 

	//	***	Clear frame
	DpyManager::BeginFrame();
	DpyManager::Draw(pClear);
	DpyManager::EndFrame();
	DpyManager::FlushFrame();
	
	nv::ctrl::Close();

	m_MustRestartEngine = TRUE;
	while (!SndManager::Shut()) 
	{
		
		nv::core::Update();
		SndManager::Update();
	}
	

	//	*** Init Pad
	KPADInit();
	for( int i = 0; i < WPAD_MAX_CONTROLLERS; i++ )
	{
		KPADEnableAimingMode( i );
	}
	
	WPADRegisterAllocator( aram_malloc, aram_free );
	
	uint32 i;
    for( i = 0; i < WPAD_MAX_CONTROLLERS; i++ )
    {
   	 	KPADEnableDPD(i);
   	 	
       	// Stop Rumble
 	   	WPADStopMotor(i);
        s_ConData.wiiCon[i].pos.x = 0.f;
        s_ConData.wiiCon[i].pos.y = 0.f;
        s_ConData.wiiCon[i].use_devtype = WPAD_DEV_CORE;
    }
	
	//	*** Init AX Sound System
	AIInit( NULL );
	AXInit();
	MIXInit();
	SYNInit();
	SEQInit();
	AXRegisterCallback(&AudioFrameCallback);
	    
    //	*** Init HBM
	HBMCreate( &s_HBMInfo );
	HBMSetAdjustFlag( TRUE );
	HBMInit();

    HBMCreateSound( m_Sound_Data, m_Sound_Buf, HBM_MEM2_SIZE_DVD );
    
	bool l_FadeOut = FALSE;
	
	//	***	Launch loop
	while(Loop())	
	{
		uint ret = nv::core::Update();
		if ( (ret & (nv::core::UPD_EXIT_ASKED|nv::core::UPD_EXITED)) && (!l_FadeOut) ) 
		{
			HBMStartBlackOut();
			l_FadeOut = TRUE;
		}
	}
	
	bool needXbfcopy = false;
	if (!l_FadeOut)
	{
	    switch( HBMGetSelectBtnNum() )
		{
	        case HBM_SELECT_BTN1:
	       		OSReturnToMenu();
 				break;
	        case HBM_SELECT_BTN2:
	        	OSRestart(0);
				break;
	        default:
	        	needXbfcopy = true;
				break;
		}
	}
#ifndef DISABLE_BACK_COPY
	if (needXbfcopy && backPict) 
	{
		pClear->SetColor(0xFFFFFFFF);
		pClear->EnableDepth	( true	);
		pClear->EnableColor	( true	);
		DCStoreRange(backPict,bsize);
		
		DpyManager::BeginFrame();
		SetupDrawing 	(efbW , efbH);
		GXLoadTexObj(&backTex, GX_TEXMAP0);
		FixTevTex();

		GXSetCullMode( GX_CULL_NONE );
		GXSetBlendMode( GX_BM_NONE, GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR );
		GXSetClipMode( GX_CLIP_DISABLE );
		GXSetAlphaCompare	( GX_ALWAYS, 0, GX_AOP_AND, GX_ALWAYS, 0 );
	
		DrawTexQuad(efbW,efbH);
		
		GXFlush();
		GXTexModeSync();
		GXPixModeSync();
		GXInvalidateTexAll();
		GXFlush();

		GXSetClipMode( GX_CLIP_ENABLE );
		vtxfmt::Reset();	
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
		
		DpyManager::BeginFrame();
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
		
		DpyManager::BeginFrame();
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}
	if (backPict)
		nv::mem::NativeFree( backPict, nv::mem::MEM_NGC_MEM2 );
#endif
}
#endif // RVL




