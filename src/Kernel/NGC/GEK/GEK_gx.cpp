/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
using namespace nv;



namespace
{
	GXRenderModeObj PalOverscanInt1 = 
	{
	    VI_TVMODE_PAL_INT,       // viDisplayMode
	    640,             // fbWidth
	    512,             // efbHeight
	    574,             // xfbHeight
	    35,		         // viXOrigin
	    0, 	   			 // viYOrigin
	    660,             // viWidth
	    574,             // viHeight
	    VI_XFBMODE_DF,   // xFBmode
	    GX_FALSE,        // field_rendering
	    GX_FALSE,        // aa

	    // sample points arranged in increasing Y order
	     6,  6,  6,  6,  6,  6,  // pix 0, 3 sample points, 1/12 units, 4 bits each
	     6,  6,  6,  6,  6,  6,  // pix 1
	     6,  6,  6,  6,  6,  6,  // pix 2
	     6,  6,  6,  6,  6,  6,  // pix 3

	    // vertical filter[7], 1/64 units, 6 bits each
	     0,         // line n-1
	     0,         // line n-1
	    21,         // line n
	    22,         // line n
	    21,         // line n
	     0,         // line n+1
	     0          // line n+1
	};


	/*GXRenderModeObj PalOverscanInt2 = 
	{
	    VI_TVMODE_PAL_INT,	 // viDisplayMode
	    640,	             // fbWidth
	    480,    	         // efbHeight
	    534,        	     // xfbHeight

	    (VI_MAX_WIDTH_PAL  - 640 )/2,        // viXOrigin
    	(VI_MAX_HEIGHT_PAL - 534)/2,       // viYOrigin
	    640,             // viWidth
	    534,             // viHeight
	    VI_XFBMODE_DF,   // xFBmode
	    GX_FALSE,        // field_rendering
	    GX_FALSE,        // aa

	    // sample points arranged in increasing Y order
	     6,  6,  6,  6,  6,  6,  // pix 0, 3 sample points, 1/12 units, 4 bits each
	     6,  6,  6,  6,  6,  6,  // pix 1
	     6,  6,  6,  6,  6,  6,  // pix 2
	     6,  6,  6,  6,  6,  6,  // pix 3

	    // vertical filter[7], 1/64 units, 6 bits each
	     0,         // line n-1
	     0,         // line n-1
	    21,         // line n
	    22,         // line n
	    21,         // line n
	     0,         // line n+1
	     0          // line n+1
	};*/


	GXRenderModeObj PalOverscanInt2 = 
	{
	    VI_TVMODE_PAL_INT,	 // viDisplayMode
	    640,	             // fbWidth
	    480,    	         // efbHeight
	    564,        	     // xfbHeight

	    27,		       	 // viXOrigin
    	5,     			 // viYOrigin
	    666,             // viWidth
	    564,             // viHeight
	    VI_XFBMODE_DF,   // xFBmode
	    GX_FALSE,        // field_rendering
	    GX_FALSE,        // aa

	    // sample points arranged in increasing Y order
	     6,  6,  6,  6,  6,  6,  // pix 0, 3 sample points, 1/12 units, 4 bits each
	     6,  6,  6,  6,  6,  6,  // pix 1
	     6,  6,  6,  6,  6,  6,  // pix 2
	     6,  6,  6,  6,  6,  6,  // pix 3

	    // vertical filter[7], 1/64 units, 6 bits each
	     0,         // line n-1
	     0,         // line n-1
	    21,         // line n
	    22,         // line n
	    21,         // line n
	     0,         // line n+1
	     0          // line n+1
	};
	
	/*
	GXRenderModeObj PalOverscanInt2 = 
	{
	    VI_TVMODE_PAL_INT,	 // viDisplayMode
	    640,	             // fbWidth
	    480,    	         // efbHeight
	    544,        	     // xfbHeight

	    (VI_MAX_WIDTH_PAL  - 666 )/2,        // viXOrigin
    	(VI_MAX_HEIGHT_PAL - 544)/2,       // viYOrigin
	    666,             // viWidth
	    544,             // viHeight
	    VI_XFBMODE_DF,   // xFBmode
	    GX_FALSE,        // field_rendering
	    GX_FALSE,        // aa

	    // sample points arranged in increasing Y order
	     6,  6,  6,  6,  6,  6,  // pix 0, 3 sample points, 1/12 units, 4 bits each
	     6,  6,  6,  6,  6,  6,  // pix 1
	     6,  6,  6,  6,  6,  6,  // pix 2
	     6,  6,  6,  6,  6,  6,  // pix 3

	    // vertical filter[7], 1/64 units, 6 bits each
	     0,         // line n-1
	     0,         // line n-1
	    21,         // line n
	    22,         // line n
	    21,         // line n
	     0,         // line n+1
	     0          // line n+1
	};*/
	
	GXRenderModeObj Pal574IntDfScale = 
	{
	    VI_TVMODE_PAL_INT,      // viDisplayMode
	    640,             // fbWidth
	    480,             // efbHeight
	    574,             // xfbHeight
	    (VI_MAX_WIDTH_PAL - 640)/2,         // viXOrigin
	    (VI_MAX_HEIGHT_PAL - 574)/2,        // viYOrigin
	    640,             // viWidth
	    574,             // viHeight
	    VI_XFBMODE_DF,   // xFBmode
	    GX_FALSE,        // field_rendering
	    GX_FALSE,        // aa

	    // sample points arranged in increasing Y order
	     6,  6,  6,  6,  6,  6,  // pix 0, 3 sample points, 1/12 units, 4 bits each
	     6,  6,  6,  6,  6,  6,  // pix 1
	     6,  6,  6,  6,  6,  6,  // pix 2
	     6,  6,  6,  6,  6,  6,  // pix 3

	    // vertical filter[7], 1/64 units, 6 bits each
	     8,         // line n-1
	     8,         // line n-1
	    10,         // line n
	    12,         // line n
	    10,         // line n
	     8,         // line n+1
	     8          // line n+1
	};

	GXFifoObj* 			gx_fifo_obj = NULL;
	void*				gx_fifo_base;
	uint				gx_fifo_bsize;

	void*				gx_xfb_base;

	GXRenderModeObj		gx_rmode;


	void vi_configure_lot_check( GXRenderModeObj* switch_to_rmode, bool inEnableCRTC )
	{
		if( !switch_to_rmode )
			return;

		const uint test_tvmode = VI_TVMODE( VI_PAL, VI_PROGRESSIVE );
		NV_COMPILE_TIME_ASSERT( ((test_tvmode>>0)&3) == VI_PROGRESSIVE );
		NV_COMPILE_TIME_ASSERT( ((test_tvmode>>2)&7) == VI_PAL );

		VITVMode sto_tvmode = switch_to_rmode->viTVmode;
		uint sto_INT = (sto_tvmode>>0) & 3;
		uint sto_FMT = (sto_tvmode>>2) & 7;
		uint sto_vf  = (sto_FMT==VI_NTSC || sto_FMT==VI_DEBUG || sto_FMT==VI_EURGB60) ? 60 : 50;

		uint cur_INT = VIGetTvFormat();
		uint cur_FMT = VIGetScanMode();
		uint cur_vf  = (cur_FMT==VI_NTSC || cur_FMT==VI_DEBUG || cur_FMT==VI_EURGB60) ? 60 : 50;

		if( sto_INT==cur_INT && sto_FMT==cur_FMT )
		{
		    VIConfigure( &gx_rmode );
		}
		else
		{
			int need_sync = 20;

			// PROGRESSIVE has changed ?
			if( (sto_INT!=cur_INT) && (sto_INT==VI_PROGRESSIVE || cur_INT==VI_PROGRESSIVE) )
				need_sync = 120;

			// freq has changed ?
			if( sto_vf != cur_vf )
				need_sync = 120;

			VISetBlack( TRUE );
			VIFlush();
		    VIConfigure( &gx_rmode );
		    VIFlush();
			while( need_sync-- )
				VIWaitForRetrace();
			VISetBlack( !inEnableCRTC );
			VIFlush();
		}
	}

}



void
gx::Init	(		)
{
	if( gx_fifo_obj )
		return;

	uint32 fifo_base, fifo_bsize;
	core::GetParameter( core::PR_NGC_GX_FIFO_BSIZE,	&fifo_bsize );
	core::GetParameter( core::PR_NGC_GX_FIFO_BASE,	&fifo_base  );
	NV_ASSERT( fifo_base && fifo_bsize );

	gx_fifo_base	= (void*) fifo_base;
	gx_fifo_bsize	= fifo_bsize;

	Memset( gx_fifo_base, 0, gx_fifo_bsize );
	DCStoreRange( gx_fifo_base, gx_fifo_bsize );

    gx_fifo_obj = GXInit( gx_fifo_base, gx_fifo_bsize );

	VIInit();
	VISetBlack( TRUE );
	VIFlush();
}



void
gx::Shut	(			)
{
	//
}


void
gx::ResetRegisters	(			)
{
    GXPokeColorUpdate( GX_TRUE );
    GXPokeAlphaUpdate( GX_TRUE );
    GXPokeDither( GX_FALSE );
    GXPokeBlendMode( GX_BM_NONE, GX_BL_ZERO, GX_BL_ONE, GX_LO_SET );
    GXPokeAlphaMode( GX_ALWAYS, 0 );
    GXPokeAlphaRead( GX_READ_FF );
    GXPokeDstAlpha( GX_DISABLE, 0 );
    GXPokeZMode( GX_TRUE, GX_ALWAYS, GX_TRUE );
    GXClearBoundingBox();
	GXSetZCompLoc( GX_FALSE );
	GXSetAlphaCompare( GX_ALWAYS, 0, GX_AOP_AND, GX_ALWAYS, 0 );	
	GXSetTexCoordGen( GX_TEXCOORD0, GX_TG_MTX3x4, GX_TG_TEX0, GX_IDENTITY );
	GXInvalidateTexAll();

#ifdef _NVCOMP_ENABLE_PROFILING 
    GXSetGPMetric( GX_PERF0_NONE, GX_PERF1_NONE );
    GXClearGPMetric();
#endif
}


void
gx::ResetCRTC	(	bool	inEnableCRTC	)
{
	uint32 xfb_base, xfb_bs;
	core::GetParameter( core::PR_NGC_XFB_BSIZE,	&xfb_bs   );
	core::GetParameter( core::PR_NGC_XFB_BASE,	&xfb_base );

	NV_ASSERT( xfb_base );
	gx_xfb_base = (void*) xfb_base;

	uint32 vmode_xfb_bs = VIPadFrameBufferWidth(gx_rmode.fbWidth) * gx_rmode.xfbHeight * 2;
	NV_ASSERT( vmode_xfb_bs <= xfb_bs );

	// VI

	VIInit();
	vi_configure_lot_check( &gx_rmode, inEnableCRTC );
    VISetNextFrameBuffer( gx_xfb_base );
	VISetBlack( !inEnableCRTC );
	VIFlush();

	// GX EFB -> XFB

	float scale = GXGetYScaleFactor(gx_rmode.efbHeight,gx_rmode.xfbHeight);
//float(gx_rmode.xfbHeight) / float(gx_rmode.efbHeight)

	GXColor black = { 0,0,0,0 };
	GXSetPixelFmt( GX_PF_RGBA6_Z24, GX_ZC_LINEAR );
    GXSetCopyClear( black, GX_MAX_Z24 );
    GXSetDispCopySrc( 0, 0, gx_rmode.fbWidth, gx_rmode.efbHeight );
    GXSetDispCopyDst( gx_rmode.fbWidth, gx_rmode.xfbHeight );
    GXSetDispCopyYScale( scale );
    GXSetCopyClamp( (GXFBClamp)(GX_CLAMP_TOP | GX_CLAMP_BOTTOM) );
    GXSetCopyFilter( gx_rmode.aa, gx_rmode.sample_pattern, GX_TRUE, gx_rmode.vfilter );
    GXSetDispCopyGamma( GX_GM_1_0 ); 
    GXSetDispCopyFrame2Field( GX_COPY_PROGRESSIVE );
    GXFlush();
    GXDrawDone();        
}


void
gx::Reset	(	bool	inEnableCRTC	)
{
	ResetCRTC( inEnableCRTC );
	ResetRegisters();
}



void
gx::Setup	(			)
{
	uint32 vmode;
	core::GetParameter( core::PR_NGC_VMODE, &vmode );
	Setup( VMode(vmode) );
}



void
gx::Setup	(	VMode		inMode	)
{	

	GXRenderModeObj  * rmode = NULL;
	core::GetParameter( core::PR_NGC_CUSTOM_RENDERMODEOBJ, (uint32*)&rmode );

	uint hor=0, ver=16;

	if( inMode == VM_NTSC_240_DS )				rmode = &GXNtsc240Ds;
	else if( inMode == VM_NTSC_240_INT )		rmode = &GXNtsc240Int;
	else if( inMode == VM_NTSC_480_INT_DF )		rmode = &GXNtsc480IntDf;
	else if( inMode == VM_NTSC_480_INT )		rmode = &GXNtsc480Int;
	else if( inMode == VM_NTSC_480_PROG )		rmode = &GXNtsc480Prog;
	else if( inMode == VM_NTSC_480_PROG_SOFT )	rmode = &GXNtsc480ProgSoft;
	else if( inMode == VM_MPAL_240_DS )			rmode = &GXMpal240Ds;
	else if( inMode == VM_MPAL_240_INT )		rmode = &GXMpal240Int;
	else if( inMode == VM_MPAL_480_INT_DF )		rmode = &GXMpal480IntDf;
	else if( inMode == VM_MPAL_480_INT )		rmode = &GXMpal480Int;
	else if( inMode == VM_PAL_264_DS )			rmode = &GXPal264Ds;
	else if( inMode == VM_PAL_264_INT )			rmode = &GXPal264Int;
	else if( inMode == VM_PAL_528_INT_DF )		rmode = &GXPal528IntDf;
	else if( inMode == VM_PAL_528_INT )			rmode = &GXPal528Int;
	else if( inMode == VM_EURGB60_240_DS )		rmode = &GXEurgb60Hz240Ds;
	else if( inMode == VM_EURGB60_240_INT )		rmode = &GXEurgb60Hz240Int;
	else if( inMode == VM_EURGB60_480_INT_DF )	rmode = &GXEurgb60Hz480IntDf;
	else if( inMode == VM_EURGB60_480_INT )		rmode = &GXEurgb60Hz480Int;
	else if( inMode == VM_PAL_574_INT )			rmode = &Pal574IntDfScale;
	else if( inMode == VM_PAL_OVERSCAN_INT1 )	rmode = &PalOverscanInt1;	
	else if( inMode == VM_PAL_OVERSCAN_INT2 )	
	{
		hor = ver = 0;
		rmode = &PalOverscanInt2;
	}
#ifdef RVL
	else if( inMode == VM_EURGB60_480_PROG )	rmode = &GXEurgb60Hz480Prog; 
#endif
	else if( inMode == VM_CUSTOM && rmode )
	{
		hor = ver = 0;
	}
	else
	{
		// VM_SELECT
		u32 tvfmt = VIGetTvFormat();
		if( tvfmt == VI_NTSC )					rmode = &GXNtsc480IntDf;
		else if( tvfmt == VI_PAL )				rmode = &GXPal528IntDf;
		else if( tvfmt == VI_EURGB60 )			rmode = &GXEurgb60Hz480IntDf;
		else if( tvfmt == VI_MPAL )				rmode = &GXMpal480IntDf;
		else									rmode = &GXNtsc480IntDf;
	}
	GXRenderModeObj adjust_rmode;
	if( hor || ver )	GXAdjustForOverscan( rmode, &adjust_rmode, hor , ver );
	else				adjust_rmode = *rmode;

	Setup( adjust_rmode );
}



void
gx::Setup	(	const GXRenderModeObj&		inMode		)
{
	gx_rmode = inMode;
}    



GXRenderModeObj*
gx::GetVMode	(		)
{
	return &gx_rmode;
}



int
gx::GetWidth		(			)
{
	return gx_rmode.fbWidth;
}


int
gx::GetHeight		(			)
{
	return gx_rmode.efbHeight;
}


void*
gx::GetXFBBase		(			)
{
	return gx_xfb_base;
}


uint
gx::GetXFBLineStride	(			)
{
	return VIPadFrameBufferWidth(gx_rmode.fbWidth) * 2;
}


int
gx::GetXFBWidth			(			)
{
	return gx_rmode.fbWidth;
}


int
gx::GetXFBHeight		(			)
{
	return gx_rmode.xfbHeight;
}


int
gx::GetVirtualWidth		(			)
{
	return GetXFBWidth();
}


int
gx::GetVirtualHeight	(			)
{
/*	if( gx_rmode.field_rendering )
		return GetXFBHeight() * 2;
	else
		return GetXFBHeight();
*/
	return GetHeight();
}


uint32
gx::GetVSyncRate		(			)
{
	VITVMode tvmode = gx_rmode.viTVmode;
	uint tvftm = ( uint(tvmode) >> 2 ) & 7;
	if( tvftm == VI_NTSC )			return 60;
	if( tvftm == VI_PAL )			return 50;
	if( tvftm == VI_MPAL )			return 60;
	if( tvftm == VI_MPAL )			return 60;
	if( tvftm == VI_DEBUG )			return 60;
	if( tvftm == VI_DEBUG_PAL )		return 50;
	if( tvftm == VI_EURGB60 )		return 60;
	return 60;	
}



volatile uint32
gx::GetVSyncCpt		(		)
{
	return VIGetRetraceCount();
}



void
gx::EnableCRTC		(		)
{
	VISetBlack( FALSE );
    VIFlush();
}



void
gx::DisableCRTC		(		)
{
	VISetBlack( TRUE );
    VIFlush();
}


void
gx::TranslateCRTC	(	int		inX0,
						int		inY0	)
{
	VIConfigurePan( inX0+gx_rmode.viXOrigin,
					inY0+gx_rmode.viYOrigin,
					gx_rmode.viWidth,
					gx_rmode.viHeight	);
    VIFlush();
}


bool
gx::IsEvenFrame		(					)
{
	return VIGetNextField() == VI_FIELD_ABOVE;
}


void
gx::VSync		(			)
{
    VIWaitForRetrace();
    VIFlush();
}


void
gx::Present		(	bool	inDoClear	)
{
	GXCopyDisp( gx_xfb_base, inDoClear 	);
}


void
gx::Flush		(			)
{
    GXFlush();
}


void
gx::Sync		(			)
{
    GXFlush();
    GXDrawDone();        
}

void
gx::ClearBack	(	uint32			inClrColor	)
{
	GXSetCullMode( GX_CULL_NONE );
	GXSetBlendMode( GX_BM_NONE, GX_BL_ZERO, GX_BL_ZERO, GX_LO_CLEAR );
	GXSetDstAlpha( GX_ENABLE, inClrColor>>24 );
	GXSetClipMode( GX_CLIP_DISABLE );

	GXSetNumChans		( 1 );
	GXSetNumTexGens		( 0 );
	GXSetNumTevStages 	( 1 );
	GXSetTevOrder		( GX_TEVSTAGE0, GX_TEXCOORD_NULL, GX_TEX_DISABLE,  GX_COLOR0A0				);			
	GXSetTevColorOp		( GX_TEVSTAGE0, GX_TEV_ADD, GX_TB_ZERO, GX_CS_SCALE_1,GX_TRUE, GX_TEVPREV	); 	// stage0, op, bias , scale, clamp, outputReg
	GXSetTevColorIn		( GX_TEVSTAGE0, GX_CC_ZERO, GX_CC_ZERO, GX_CC_ZERO, GX_CC_RASC				); 	// stage0, 0, 0, 0, Texture color				
    GXSetTevAlphaIn		( GX_TEVSTAGE0, GX_CA_ZERO, GX_CA_ZERO, GX_CA_ZERO, GX_CA_RASA 				); 	// stage0, 0, 0, 0, Texture alpha		
	GXSetAlphaCompare	( GX_ALWAYS, 0, GX_AOP_AND, GX_ALWAYS, 0 );
	
	GXClearVtxDesc();
	GXSetVtxDesc( GX_VA_POS,  GX_DIRECT );
	GXSetVtxDesc( GX_VA_CLR0, GX_DIRECT );
	GXSetVtxAttrFmt( GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0 );
	GXSetVtxAttrFmt( GX_VTXFMT0, GX_VA_CLR0, GX_CLR_RGBA, GX_RGBA8, 0 );

	GXSetChanCtrl(	GX_COLOR0A0,GX_FALSE,GX_SRC_VTX,GX_SRC_VTX,GX_LIGHT_NULL, GX_DF_CLAMP,GX_AF_NONE	);
	
	// normalized clipping space is <[-1,+1],[-1,+1],[-1,+0]>
	float xy = 1.01f;				// fullscreen area
	float z  = -0.5f;				// z in [-1,0] to pass the HW rejecting test
	GXSetZScaleOffset( 0.f, 1.f );	// force z in EFB = GX_MAX_Z24
	GXBegin( GX_QUADS, GX_VTXFMT0, 4 );
		GXPosition3f32( -xy, -xy, z );
		GXColor1u32( inClrColor );
		GXPosition3f32( -xy, xy, z );
		GXColor1u32( inClrColor );
		GXPosition3f32( xy, xy, z );
		GXColor1u32( inClrColor );
		GXPosition3f32( xy, -xy, z );
		GXColor1u32( inClrColor );
	GXEnd();

	GXSetZScaleOffset( 1.f, 0.f );	// restore
	GXSetClipMode( GX_CLIP_ENABLE );
	vtxfmt::Reset();
}

void
gx::ClearBack_Direct	(	const Vec4&					inXYWH,
							uint32						inRGBA		)
{
    GXPokeAlphaMode		( GX_ALWAYS, 0);
    GXPokeAlphaUpdate	( GX_TRUE );
    GXPokeBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_COPY );
    GXPokeColorUpdate	( GX_TRUE );

	u16 x0 = u16( inXYWH.x );
	u16 x1 = u16( inXYWH.z ) + x0;
	u16 y0 = u16( inXYWH.y );
	u16 y1 = u16( inXYWH.w ) + y0;
	uint32 argb = ConvertRGBA( PSM_ARGB32, inRGBA );

	for( u16 y = y0 ; y < y1 ; y++ ) {
		for( u16 x = x0 ; x < x1 ; x++ ) {
			GXPokeARGB( x, y, argb );
		}
	}
}

void
gx::CopyTex			( 	uint8 *						ioBuffer	,
						CopyFormat					inFormat	,
						uint						inWidth		,
						uint						inHeight	,
						uint						inX			,
						uint						inY			)
{
	if (! ioBuffer ) return ;
	
	uint efbWidth 	= GetWidth();
	uint efbHeight 	= GetHeight();
	
	if ( (inX + inWidth ) > efbWidth || inWidth > efbWidth ||
		 (inY + inHeight) > efbHeight|| inHeight> efbHeight) return ;

	
	uint xoff, yoff;
	uint w, h;
	
	if (!inWidth || !inHeight) {	// Get entire buffer
		w = efbWidth;
		h = efbHeight;
		xoff = yoff = 0;
	}
	else {
		xoff = inX;
		yoff = inY;
		w	 = inWidth;
		h 	 = inHeight;
	}
	
	static const GXTexFmt Format_Translation[CF_MAX] ={
		GX_TF_Z8,		//CF_Z8
		GX_CTF_Z8M,		//CF_Z8
		GX_CTF_Z8L,		//CF_Z8
		GX_CTF_Z16L,	//CF_Z8
		GX_TF_Z16,		//CF_Z16
		GX_TF_Z24X8	,	//CF_Z24X8
		//---- Color Format ----
		GX_TF_RGB565,	//CF_RGB565
		GX_TF_RGB5A3,	//CF_RGB5A3
		GX_TF_RGBA8,	//CF_RGBA8
		GX_CTF_R8,		//CF_RGBA8
	};
	
	NV_ASSERT( uint(inFormat) >= 0 && uint(inFormat) < CF_MAX);

	// remove de-fliker for copy	
	GXSetCopyFilter(GX_FALSE, NULL, GX_FALSE, NULL);

    // Copy RGB/Z Buffer
    GXSetTexCopySrc	( xoff, yoff , w, h 	);
    GXSetTexCopyDst	( w, h,Format_Translation[inFormat] , GX_FALSE );
    GXCopyTex		( ioBuffer, GX_FALSE 	);

	// Wait for finishing all rendering task in the graphics pipeline
	// while allowing CPU to continue
	GXPixModeSync();

    // Restore vertical de-flicker
    GXSetCopyFilter( gx_rmode.aa, gx_rmode.sample_pattern, GX_TRUE, gx_rmode.vfilter );
}
											
void
gx::CopyBack_Direct		(	const Vec2&					inToXY,
							const Vec2&					inFromXY,
							const Vec2&					inWH		)
{
    GXPokeAlphaMode		( GX_ALWAYS, 0);
    GXPokeAlphaUpdate	( GX_TRUE );
    GXPokeBlendMode		( GX_BM_NONE,GX_BL_ZERO, GX_BL_ZERO, GX_LO_COPY );
    GXPokeColorUpdate	( GX_TRUE );

	u16 w  = u16( inWH.x );
	u16 h  = u16( inWH.y );
	u16 x0 = u16( inToXY.x );
	u16 y0 = u16( inToXY.y );
	u16 x1 = u16( inFromXY.x );
	u16 y1 = u16( inFromXY.y );

	for( u16 y = 0 ; y < h ; y++ ) {
		for( u16 x = 0 ; x < w ; x++ ) {
			u32 c;
			GXPeekARGB( x1+x, y1+y, &c );
			GXPokeARGB( x0+x, y0+y, c );
		}
	}
}


void
gx::PutBack24_Direct	(	const Vec4&		inXYWH,
							uint8*			inData,
							uint			inStride	)
{
    GXPokeAlphaMode		( GX_ALWAYS, 0);
    GXPokeAlphaUpdate	( GX_FALSE );
    GXPokeBlendMode		( GX_BM_NONE, GX_BL_ZERO, GX_BL_ZERO, GX_LO_COPY );
    GXPokeColorUpdate	( GX_TRUE );

	u16 x0 = u16( inXYWH.x );
	u16 x1 = u16( inXYWH.z ) + x0;
	u16 y0 = u16( inXYWH.y );
	u16 y1 = u16( inXYWH.w ) + y0;

	for( u16 y = y0 ; y < y1 ; y++ ) {
		uint8* c8 = inData;
		for( u16 x = x0 ; x < x1 ; x++ ) {
			u32 argb = GetPSM( PSM_ARGB32, c8[0], c8[1], c8[2], 0 );
			GXPokeARGB( x, y, argb );
			c8 += 3;
		}
		inData += inStride;
	}
}


void
gx::PutBack32_Direct	(	const Vec4&		inXYWH,
							uint8*			inData,
							uint			inStride	)
{
    GXPokeAlphaMode		( GX_ALWAYS, 0);
    GXPokeAlphaUpdate	( GX_TRUE );
    GXPokeBlendMode		( GX_BM_NONE, GX_BL_ZERO, GX_BL_ZERO, GX_LO_COPY );
    GXPokeColorUpdate	( GX_TRUE );

	u16 x0 = u16( inXYWH.x );
	u16 x1 = u16( inXYWH.z ) + x0;
	u16 y0 = u16( inXYWH.y );
	u16 y1 = u16( inXYWH.w ) + y0;

	for( u16 y = y0 ; y < y1 ; y++ ) {
		uint8* c8 = inData;
		for( u16 x = x0 ; x < x1 ; x++ ) {
			u32 argb = GetPSM( PSM_ARGB32, c8[1], c8[2], c8[3], c8[0] );
			GXPokeARGB( x, y, argb );
			c8 += 4;
		}
		inData += inStride;
	}
}


void
gx::GetBack_Direct		(	const Vec4&		inXYWH,
							uint8*			inBuffer,
							uint			inStride	)
{
	u16 x0 = u16( inXYWH.x );
	u16 x1 = u16( inXYWH.z ) + x0;
	u16 y0 = u16( inXYWH.y );
	u16 y1 = u16( inXYWH.w ) + y0;

	for( u16 y = y0 ; y < y1 ; y++ ) {
		uint32* c32 = (uint32*) inBuffer;
		for( u16 x = x0 ; x < x1 ; x++ ) {
			GXPeekARGB( x, y, (u32*)c32 );
			c32++;
		}
		inBuffer += inStride;
	}
}


void
gx::GetDepth_Direct		(	const Vec4&		inXYWH,
							uint8*			inBuffer,
							uint			inStride	)
{
	u16 x0 = u16( inXYWH.x );
	u16 x1 = u16( inXYWH.z ) + x0;
	u16 y0 = u16( inXYWH.y );
	u16 y1 = u16( inXYWH.w ) + y0;

	for( u16 y = y0 ; y < y1 ; y++ ) {
		uint32* z32 = (uint32*)inBuffer;
		for( u16 x = x0 ; x < x1 ; x++ ) {
			GXPeekZ( x, y, (u32*)z32 );
			z32++;
		}
		inBuffer += inStride;
	}
}



void
gx::EncodeXFBLine	(	uint32*		inFromRGBA,
						uint8*		inToLineAddr,
						uint		inSize			)
{
	NV_ASSERT( inSize <= 1024 );
	NV_ASSERT( (inSize&1) == 0 );

	struct YUV {
		float	y;
		float	u;
		float	v;
	} yuv[1024];

	struct YUYV {
		float	u;
		float	v;
	} uv[512];

	struct Y_UV {
		uint8		y;
		union {
			uint8	u;
			uint8	v;
		};
	};

	// rgb -> yuv
	for( int x = 0 ; x < inSize ; x++ ) {
		float r = float( int( ((inFromRGBA[x]>>24) & 0xFF) ));
		float g = float( int( ((inFromRGBA[x]>>16) & 0xFF) ));
		float b = float( int( ((inFromRGBA[x]>>8)  & 0xFF) ));
		float y = ( 0.257f*r) + (0.504f*g) + (0.098f*b) + 16.f;
		float u = (-0.148f*r) - (0.291f*g) + (0.439f*b) + 128.f;
		float v = ( 0.439f*r) - (0.368f*g) - (0.071f*b) + 128.f;
		yuv[x].y = Clamp( y, 16.0f , 235.0f );
		yuv[x].u = u;
		yuv[x].v = v;
	}
	
	// yuv -> uv
	for( int x = 0 ; x < inSize ; x+=2 ) {
		int x0 = (x > 0 ? x-1 :  x),
			x1 = x,
			x2 = (x < inSize-1 ? x+1 : x);
		uv[x>>1].v = yuv[x0].v * 0.25f + yuv[x1].v * 0.5f + yuv[x2].v * 0.25f;
		uv[x>>1].u = yuv[x0].u * 0.25f + yuv[x1].u * 0.5f + yuv[x2].u * 0.25f;

	}

	// write back
	Y_UV* y_uv = (Y_UV*) inToLineAddr;
	for( int x = 0 ; x < inSize ; x+=2 ) {
		y_uv[x+0].y = uint8( yuv[x].y );
		y_uv[x+0].u = uint8( uv[x>>1].u );
		y_uv[x+1].y = uint8( yuv[x+1].y );
		y_uv[x+1].v = uint8( uv[x>>1].v );
	}		
}



bool
gx::CopyEFBToMem	( void * ioDest, uint inLeft , uint inTop , uint inWidth, uint inHeight , GXTexFmt inFmt)
{
	if (! ioDest ) return FALSE;
	
	NV_ASSERT_A32	( ioDest );
		
	GXSetTexCopySrc	( 0  , 0, inWidth	 , inHeight	);
	GXSetTexCopyDst	( inWidth , inHeight , inFmt	, FALSE 	);
	
	GXCopyTex		( ioDest  , FALSE 							);
	
	GXPixModeSync();
	GXInvalidateTexAll();
	
	return TRUE;
}


