/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
using namespace nv;


namespace
{

	dmac::Cursor		dcBuff[2];
	dmac::Cursor*		frontDC;
	dmac::Cursor*		backDC;

}


void
dmac::Init()
{
	// DMA Buffer
	uint32 dmaHeaps;
	bool   dmaUncached;
	core::GetParameter( nv::core::PR_NGC_DMAC_HEAPS_BSIZE,   &dmaHeaps );
	core::GetParameter( nv::core::PR_NGC_DMAC_HEAPS_UNCACHE, &dmaUncached );

	// DMA Heap : D$ line alignment to prevent concurrent cached & uncached memory access !
	uint32 dmaHeap = (dmaHeaps>>1) & 0xFFFFFF00;
	void * dmaBuff0 = EngineMallocA( dmaHeap, NVHW_DCACHE_BALIGN );
	void * dmaBuff1 = EngineMallocA( dmaHeap, NVHW_DCACHE_BALIGN );

	dcBuff[0].i8	= dcBuff[0].start = (uint8*)( dmaUncached ? UncachedPointer(dmaBuff0) : CachedPointer(dmaBuff0) );
	dcBuff[0].bsize = dmaHeap;

	dcBuff[1].i8	= dcBuff[1].start = (uint8*)( dmaUncached ? UncachedPointer(dmaBuff1) : CachedPointer(dmaBuff1) );
	dcBuff[1].bsize = dmaHeap;

	Printf( "<Neova> DMAC heaps: MODE=%s BSIZE=2*%dKo\n",
			dmaUncached ? "D$-uncached" : "D$-cached",
			(dmaHeap>>10)	);
	Printf( "<Neova> DMAC heaps: @0=0x%08x @1=0x%08x\n",
			uint32(dmaBuff0),
			uint32(dmaBuff1)	);
	NV_ASSERT( dmaBuff0 );
	NV_ASSERT( dmaBuff1 );

	SyncDCache( dmaBuff0, dmaHeap );
	SyncDCache( dmaBuff1, dmaHeap );

	Reset();
}



void
dmac::Reset()
{
	frontDC		= &dcBuff[0];
	backDC		= &dcBuff[1];
	frontDC->i8	= frontDC->start;
	backDC->i8	= backDC->start;
}


void
dmac::Shut()
{
	EngineFree( CachedPointer(dcBuff[0].start) );
	EngineFree( CachedPointer(dcBuff[1].start) );
}



//
//	HEAP BUFFER


void
dmac::SwapHeap()
{
	NV_ASSERT_DC( frontDC );
	NV_ASSERT_DC( backDC  );

	// swap
	Swap( frontDC, backDC );

	// rewind front
	frontDC->i8  = frontDC->start;
	NV_ASSERT_DC( frontDC );
}


dmac::Cursor *
dmac::GetFrontHeapCursor()
{
	NV_ASSERT_DC( frontDC );
	return frontDC;
}


dmac::Cursor *
dmac::GetBackHeapCursor()
{
	NV_ASSERT_DC( backDC );
	return backDC;
}


