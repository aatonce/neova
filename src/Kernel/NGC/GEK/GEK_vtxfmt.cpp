/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/GEK/GEK.h>
using namespace nv;
using namespace vtxfmt;


#define	FRAC_LOC_S16		10
#define	FRAC_TEX_S16		10
#define	FRAC_NRM_S8			6		// Can't be changed in HW2 !




namespace
{

	struct VtxDesc {
		GXVtxDescList		c[5];
	};

	struct VtxAttr {
		GXVtxAttrFmtList	c[5];
	};

	struct VtxFmt {
		int8	descIdx;			// lookup in vtxdesc
		int8	attrIdx;			// lookup in vtxattr
	};


	VtxDesc					vtx_desc[8];
	VtxAttr					vtx_attr[8];
	VtxFmt					vtx_fmt[256];


	bool CheckComponents	(	uint	inComponents )
	{
		if( inComponents > 255 )	return FALSE;
		uint locCo = (inComponents) & CO_LOC_MASK;
		uint texCo = (inComponents) & CO_TEX_MASK;
		uint colCo = (inComponents) & CO_COL_MASK;
		uint nrmCo = (inComponents) & CO_NRM_MASK;
		
		if( !(locCo==CO_LOC_S16 || locCo==CO_LOC_F32) )						return FALSE;
		if( !(texCo==0 || texCo==CO_TEX_S16 || texCo==CO_TEX_F32) )			return FALSE;
		if( !(colCo==0 || colCo==CO_COLOR) )								return FALSE;
		if( !(nrmCo==0 || nrmCo==CO_NORMAL_S8 || nrmCo==CO_NORMAL_F32) )	return FALSE;
		return TRUE;
	}

}


void
vtxfmt::Init	(								)
{
	// Check if GXVtxFmt value can be used as index;	
	NV_COMPILE_TIME_ASSERT( int(GX_VTXFMT0) == 0 );
	NV_COMPILE_TIME_ASSERT( int(GX_VTXFMT1) == 1 );
	NV_COMPILE_TIME_ASSERT( int(GX_VTXFMT2) == 2 );
	NV_COMPILE_TIME_ASSERT( int(GX_VTXFMT3) == 3 );
	NV_COMPILE_TIME_ASSERT( int(GX_VTXFMT4) == 4 );
	NV_COMPILE_TIME_ASSERT( int(GX_VTXFMT5) == 5 );
	NV_COMPILE_TIME_ASSERT( int(GX_VTXFMT6) == 6 );
	NV_COMPILE_TIME_ASSERT( int(GX_VTXFMT7) == 7 );

	for( uint i = 0 ; i < 8 ; i++ ) {
		vtx_desc[i].c[0].attr		= GX_VA_POS;
		vtx_desc[i].c[1].attr		= GX_VA_TEX0;
		vtx_desc[i].c[2].attr		= GX_VA_CLR0;
		vtx_desc[i].c[3].attr		= GX_VA_NRM;
		vtx_desc[i].c[4].attr		= GX_VA_NULL;
		vtx_desc[i].c[0].type		= GX_INDEX16;
		vtx_desc[i].c[1].type		= i&1 ? GX_INDEX16 : GX_NONE;
		vtx_desc[i].c[2].type		= i&2 ? GX_INDEX16 : GX_NONE;
		vtx_desc[i].c[3].type		= i&4 ? GX_INDEX16 : GX_NONE;
	}

	for( uint i = 0 ; i < 8 ; i++ ) {
		vtx_attr[i].c[0].attr		= GX_VA_POS;
		vtx_attr[i].c[1].attr		= GX_VA_TEX0;
		vtx_attr[i].c[2].attr		= GX_VA_CLR0;
		vtx_attr[i].c[3].attr		= GX_VA_NRM;
		vtx_attr[i].c[4].attr		= GX_VA_NULL;
		vtx_attr[i].c[0].cnt		= GX_POS_XYZ;
		vtx_attr[i].c[1].cnt		= GX_TEX_ST;
		vtx_attr[i].c[2].cnt		= GX_CLR_RGBA;
		vtx_attr[i].c[3].cnt		= GX_NRM_XYZ;
		vtx_attr[i].c[0].type		= i&1 ? GX_F32 : GX_S16;
		vtx_attr[i].c[1].type		= i&2 ? GX_F32 : GX_S16;
		vtx_attr[i].c[2].type		= GX_RGBA8;
		vtx_attr[i].c[3].type		= i&4 ? GX_F32 : GX_S8;
		vtx_attr[i].c[0].frac		= i&1 ? 0 : FRAC_LOC_S16;
		vtx_attr[i].c[1].frac		= i&2 ? 0 : FRAC_TEX_S16;
		vtx_attr[i].c[2].frac		= 0;
		vtx_attr[i].c[3].frac		= i&4 ? 0 : FRAC_NRM_S8;
	}

	Memset( vtx_fmt, -1, sizeof(vtx_fmt) );
	for( uint i = 0 ; i < 255 ; i++ ) {
		if( !CheckComponents(i) )
			continue;
		uint locCo = (i) & CO_LOC_MASK;
		uint texCo = (i) & CO_TEX_MASK;
		uint colCo = (i) & CO_COL_MASK;
		uint nrmCo = (i) & CO_NRM_MASK;
		uint descIdx = (texCo ? 1 : 0)
					 | (colCo ? 2 : 0)
					 | (nrmCo ? 4 : 0);
		uint attrIdx = (locCo==CO_LOC_F32    ? 1 : 0)
					 | (texCo==CO_TEX_F32    ? 2 : 0)
					 | (nrmCo==CO_NORMAL_F32 ? 4 : 0);
		vtx_fmt[i].descIdx = descIdx;
		vtx_fmt[i].attrIdx = attrIdx;
	}

  	DCFlushRange( vtx_attr, sizeof(vtx_attr) );
  	DCFlushRange( vtx_desc,sizeof(vtx_desc) );

	Reset();
}


void
vtxfmt::Shut	(								)
{
	//
}


void
vtxfmt::Reset	(								)
{
	for( int i = 0 ; i < 8 ; i++ )
		GXSetVtxAttrFmtv( GXVtxFmt(i), vtx_attr[i].c );
	GXClearVtxDesc();
}


GXVtxFmt
vtxfmt::Probe	(	uint		inComponents	)
{
	NV_ASSERT( inComponents <= 255 );
	int attrIdx  = vtx_fmt[ inComponents ].attrIdx;
	NV_ASSERT( attrIdx >= 0 );
	return GXVtxFmt(attrIdx);
}


GXVtxFmt
vtxfmt::Activate	(	uint		inComponents,
						int			inLocFrac		)
{
	NV_ASSERT( inComponents <= 255 );
	int descIdx = vtx_fmt[ inComponents ].descIdx;
	int attrIdx = vtx_fmt[ inComponents ].attrIdx;

	NV_ASSERT( descIdx >= 0 );
	NV_ASSERT( descIdx <  8 );
	NV_ASSERT( attrIdx >= 0 );
	NV_ASSERT( attrIdx <  8 );

	#ifdef _DEBUG
	VtxDesc* thisdesc = &vtx_desc[descIdx];
	VtxAttr* thisattr = &vtx_attr[attrIdx];
	#endif

	GXVtxFmt fmt = GXVtxFmt(attrIdx);

	GXSetVtxDescv( vtx_desc[descIdx].c );
	
	if (inLocFrac < 0) inLocFrac = 0;
	
	GXSetVtxAttrFmt( fmt, vtx_attr[attrIdx].c[0].attr, vtx_attr[attrIdx].c[0].cnt, vtx_attr[attrIdx].c[0].type, inLocFrac );

	return fmt;
}


uint
vtxfmt::GetStride	(	Component		inComponent		)
{
	if( inComponent == CO_LOC_S16	 )		return (3 * 2);
	if( inComponent == CO_LOC_F32	 )		return (3 * 4);
	if( inComponent == CO_TEX_S16	 )		return (2 * 2);
	if( inComponent == CO_TEX_F32	 )		return (2 * 4);
	if( inComponent == CO_COLOR		 )		return (1 * 4);
	if( inComponent == CO_NORMAL_S8	 )		return (3 * 1);
	if( inComponent == CO_NORMAL_F32 )		return (3 * 4);
	return 0;
}


GXCompType
vtxfmt::GetType		(	Component		inComponent		)
{
	if( inComponent == CO_LOC_S16	 )		return GX_S16;
	if( inComponent == CO_LOC_F32	 )		return GX_F32;
	if( inComponent == CO_TEX_S16	 )		return GX_S16;
	if( inComponent == CO_TEX_F32	 )		return GX_F32;
	if( inComponent == CO_COLOR		 )		return GX_RGBA8;
	if( inComponent == CO_NORMAL_S8	 )		return GX_S8;
	if( inComponent == CO_NORMAL_F32 )		return GX_F32;
	return GXCompType(0xff);
}




