/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PSP/NvDpyManager[PSP].h>
#include <Kernel/PSP/NvkMesh[PSP].h>
#include <Kernel/Common/NvRscFactory_SHR.h>
#include <NvMorphTarget.h>
#include <NvBitmap.h>
using namespace nv;




//#define		ENABLE_APPLY_BENCHING




namespace
{

	RscFactory*		meshFactory = NULL;

}



//
// BASE


struct NvMeshBase : public RscFactory_SHR::Instance
{

	#include <Projects\Plugin NovaPSP\comp_NvMesh_format.h>

	struct BoneMat {
		uint32		boned[12];		// CMD_BONED
		uint32		ret;			// CMD_RET
	};

	NvMesh					itf;
	NvkMesh					kitf;
	NvBitmap**				lockBitmapA;		// NvBitmap* for each surface
	Matrix*					skelBlendA;
	BoneMat*				skelBoneMatA;		// Mapped bones matrices
	uint16*					skelBoneIdxA;		// Mapped bones indices
	NvkMesh::BunchBlend*	bunchBlendA;		// Mapped bones for each bunch
	pvoid					dmaData;			// instance dma-data
	uint					enflags;			// enabled flags
	uint					validflags;			// valid flags
	uint					updflags;			// toupdate flags
	uint32					drawFrameNo;


	virtual	~	NvMeshBase	(	) {}			// needed by nv::DestructInPlace<T>(ptr)

	Header*
	hdr			(		)
	{
		return (Header*)inst.dataptr;
	}

	void
	_Init		(		)
	{
		skelBlendA	= NULL;
		NV_ASSERT( dmaData );
		NV_ASSERT_A128( dmaData );

		enflags	 = NvMesh::EN_DEFAULT;
		#ifdef FORCE_NRMSKINNING
		enflags |= NvMesh::EN_NRM_SKINNING;
		#endif

		validflags	= 0;
		updflags	= 0;
		drawFrameNo = ~0U;

		_ResetBones();
	}

	void
	_Shut		(		)
	{
		UnrefBitmaps();
		UnmapSkeleton();
		SetMorphSlotCpt(0);
	}

	void
	_ResetBones	(		)
	{
		if( !IsSkinnable() )
			return;
		for( uint i = 0 ; i < hdr()->boneCpt ; i++ ) {
			BuildGeMatrix12( skelBoneMatA[i].boned, &Matrix::UNIT, SCE_GE_CMD_BONED );
			skelBoneMatA[i].ret = SCE_GE_SET_RET();
		}
		for( uint i = 0 ; i < hdr()->bunchCpt ; i++ ) {
			bunchBlendA[i].boneBlend[0] = skelBoneMatA[ hdr()->bunchA[i].boneIdx[0] ].boned;
			bunchBlendA[i].boneBlend[1] = skelBoneMatA[ hdr()->bunchA[i].boneIdx[1] ].boned;
			bunchBlendA[i].boneBlend[2] = skelBoneMatA[ hdr()->bunchA[i].boneIdx[2] ].boned;
		}
	}

	void
	AddRef		(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release		(		)
	{
		NV_ASSERT( inst.refCpt );
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			NV_ASSERT( !IsDrawing() );		// Referenced in shader !!!

			// Safe to delete ?
			if( IsDrawing() )
			{
				DpyManager::AddToGarbager( this );
				return;
			}
			else
			{
				NV_ASSERT( meshFactory );
				meshFactory->FreeInstance( &itf );
			}
		}
	}

	uint32
	GetRscType	(		)
	{
		return NvMesh::TYPE;
	}

	uint32
	GetRscUID	(		)
	{
		return inst.uid;
	}

	NvMesh*
	GetInterface		(		)
	{
		return &itf;
	}

	NvkMesh*
	GetKInterface		(		)
	{
		return &kitf;
	}

	Box3*
	GetBBox				(		)
	{
		return &hdr()->bbox;
	}

	uint32
	GetNameCRC			(		)
	{
		return hdr()->nameCRC;
	}

	bool
	IsSkinnable			(		)
	{
		return hdr()->skinnable;
	}

	bool
	IsNrmSkinnable			(		)
	{
		return hdr()->nrmskinnable;
	}

	bool
	IsMorphable			(		)
	{
//		return hdr()->morphable;
		return FALSE;
	}

	bool
	IsLightable			(		)
	{
		return hdr()->lightable;
	}

	bool
	IsShadowing		(	)
	{
//		return hdr()->shadowing;
		return FALSE;
	}

	bool
	IsFullOpaque		(		)
	{
		return hdr()->fullopaque;
	}

	void
	Enable		(	uint32		inEnableFlags	)
	{
		enflags |= inEnableFlags;
	}

	void
	Disable		(	uint32		inEnableFlags	)
	{
		enflags &= ~inEnableFlags;
	}

	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
		enflags = inEnableFlags;
	}

	uint32
	GetEnabled		(		)
	{
		return enflags;
	}

	bool
	MapSkeleton			(	uint			inBoneCount,
							uint32*			inBoneNameA,
							Matrix*			inBlend16A		)
	{
		UnmapSkeleton();

		if(		!IsSkinnable()
			||	!inBoneNameA
			||	!inBoneCount
			||	!inBlend16A		)
			return FALSE;

		// 16 bytes aligned ?
		if( (uint32(inBlend16A)&0xF) != 0 ) {
			NV_DEBUG_WARNING( "NvMesh::Map() : inBlend16A must be 16bytes aligned !" );
			return FALSE;
		}

		_ResetBones();

		// Bone mapping
		for( uint i = 0 ; i < hdr()->boneCpt ; i++ ) {
			bool boneFound = FALSE;
			for( uint j = 0 ; j < inBoneCount ; j++ ) {
				if( inBoneNameA[j] == hdr()->boneNameA[i] ) {
					skelBoneIdxA[i] = j;
					boneFound = TRUE;
					break;
				}
			}
			if( !boneFound )
				return FALSE;	// no partial mapping !
		}

		skelBlendA  = inBlend16A;

		// enables skinning
		validflags |= NvMesh::EN_LOC_SKINNING;
		validflags |= NvMesh::EN_NRM_SKINNING;
		validflags |= NvMesh::EN_SHD_SKINNING;
		return TRUE;
	}

	void
	UnmapSkeleton		(									)
	{
		_ResetBones();
		skelBlendA  = NULL;
		validflags &= ~( NvMesh::EN_LOC_SKINNING | NvMesh::EN_NRM_SKINNING | NvMesh::EN_SHD_SKINNING );	// disables all skinning
	}

	uint
	GetMorphSlotCpt		(									)
	{
		return 0;
	}

	void
	SetMorphSlotCpt		(	uint			inSize			)
	{
		//
	}

	NvMorphTarget*
	GetMorphSlotTarget	(	uint			inSlotNo		)
	{
		return NULL;
	}

	void
	SetMorphSlotTarget	(	uint			inSlotNo,
							NvMorphTarget*	inTarget		)
	{
		//
	}

	void
	SetMorphSlotWeight	(	uint			inSlotNo,
							float			inWeight		)
	{
		return;
	}

	void
	GetUnpacking		(	float*				outLocUS,
							Vec3*				outLocT,
							float*				outTexUS,
							Vec2*				outTexT		)
	{
		if( outLocUS )		*outLocUS = hdr()->unpackLocS;
		if( outLocT )		*outLocT  = hdr()->unpackLocT;
		if( outTexUS )		*outTexUS = hdr()->unpackTexS;
		if( outTexT )		*outTexT  = hdr()->unpackTexT;
	}

	uint
	GetBunchCpt			(			)
	{
		return hdr()->bunchCpt;
	}

	uint
	GetBunchDataCpt			(			)
	{
		return hdr()->bunchDataCpt;
	}

	uint32
	GetBunchDataBase	(			)
	{
		return uint32(dmaData);
	}

	uint32
	GetBunchDataVTYPE	(			)
	{
		return hdr()->vtype;
	}

	NvkMesh::Bunch*
	GetBunchA			(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(Bunch) == sizeof(NvkMesh::Bunch) );
		return (NvkMesh::Bunch*) hdr()->bunchA;
	}

	NvkMesh::BunchBBox*
	GetBunchBBoxA			(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(BunchBBox) == sizeof(NvkMesh::BunchBBox) );
		return (NvkMesh::BunchBBox*) hdr()->bunchBBoxA;
	}

	NvkMesh::BunchData*
	GetBunchDataA		(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(BunchData) == sizeof(NvkMesh::BunchData) );
		return (NvkMesh::BunchData*) hdr()->bunchDataA;
	}

	NvkMesh::BunchList*
	GetBunchByBunchA		(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(BunchList) == sizeof(NvkMesh::BunchList) );
		return (NvkMesh::BunchList*) hdr()->bunchByBunchA;
	}

	NvkMesh::BunchList*
	GetBunchBySurfA		(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(BunchList) == sizeof(NvkMesh::BunchList) );
		return (NvkMesh::BunchList*) hdr()->bunchBySurfA;
	}

	NvkMesh::BunchBlend*
	GetBunchBlendA		(			)
	{
		return bunchBlendA;
	}

	uint
	GetSurfaceCpt		(			)
	{
		return hdr()->surfCpt;
	}

	NvkMesh::Shading*
	GetSurfaceShadingA	(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(Surface) == sizeof(NvkMesh::Shading) );
		return (NvkMesh::Shading*) hdr()->surfA;
	}

	uint32
	GetSurfaceNameCRC	(	uint		inSurfaceNo		)
	{
		if( inSurfaceNo >= hdr()->surfCpt )
			return 0;
		Surface* surf = hdr()->surfA + inSurfaceNo;
		return surf->nameCRC;
	}

	uint32
	GetSurfaceBitmapUID	(	uint		inSurfaceNo		)
	{
		if( inSurfaceNo >= hdr()->surfCpt )
			return 0;
		Surface* surf = hdr()->surfA + inSurfaceNo;
		return surf->bitmapUID;
	}

	void
	RefBitmaps	(			)
	{
		if( !lockBitmapA )
			return;
		Surface* surfA   = hdr()->surfA;
		uint	 surfCpt = hdr()->surfCpt;
		for( uint i = 0 ; i < surfCpt ; i++ ) {
			if( !lockBitmapA[i] )
				lockBitmapA[i] = NvBitmap::Create( surfA[i].bitmapUID );
		}
	}

	void
	UnrefBitmaps	(			)
	{
		if( !lockBitmapA )
			return;
		uint surfCpt = hdr()->surfCpt;
		for( uint i = 0 ; i < surfCpt ; i++ )
			SafeRelease( lockBitmapA[i] );
	}

	bool
	GetShadowing		(	uint&						outVtxCpt,
							uint&						outFaceCpt,
							NvkMesh::ShadowingVtx*&		outVtxA,
							NvkMesh::ShadowingFace*&	outFaceA		)
	{
		return TRUE;
	}

	void
	Update				(									)
	{
		// save flags to update before drawing ...
		updflags = enflags;
	}

	void
	UpdateBeforeDraw	(		)
	{
		// update todo flags with current state of the NvMesh ...
		updflags &= validflags;
		if( updflags == 0 )
			return;

		if( updflags & (NvMesh::EN_LOC_SKINNING|NvMesh::EN_NRM_SKINNING) )
		{
			NV_ASSERT( skelBlendA );
			uint boneCpt = hdr()->boneCpt;
			for( uint i = 0 ; i < boneCpt ; i++ ) {
				Matrix* boneMat = skelBlendA + skelBoneIdxA[i];
				BuildGeMatrix12( skelBoneMatA[i].boned, boneMat, SCE_GE_CMD_BONED );
			}
		}

		updflags = 0;
		drawFrameNo = DpyManager::GetFrameNo();
	}


	bool
	IsDrawing	(		)
	{
		return DpyManager::IsDrawing( drawFrameNo );
	}

};





//
// FACTORY


namespace
{

	struct NvMeshFactory : public RscFactory_SHR
	{
		uint32
		GetType		(			)
		{
			return NvMesh::TYPE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvMeshBase::Header * hdr = (NvMeshBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvMeshBase::Header)
					&&	hdr->ver == NV_MESH_CVER					);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvMeshBase::Header * hdr = (NvMeshBase::Header*) inRscData;
			TranslatePointer( &hdr->surfA,			inBOffset );
			TranslatePointer( &hdr->bunchA,			inBOffset );
			TranslatePointer( &hdr->bunchBBoxA,		inBOffset );
			TranslatePointer( &hdr->bunchDataA,		inBOffset );
			TranslatePointer( &hdr->bunchBySurfA,	inBOffset );
			TranslatePointer( &hdr->bunchByBunchA,	inBOffset );
			TranslatePointer( &hdr->boneNameA,		inBOffset );
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			// Skinning or not, morphing or not => always shared
			// The CUSTOM is the same as SHR. So we use SHR ...
			return SHRM_DATA_SHR;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvMeshBase * inst = (NvMeshBase*) inInst;
			return &inst->itf;
		}

		bool
		IsRscDmaDataIsUsed	(	Desc*		inDesc		)
		{
			// Find if an instance is using the original rsc DMAData section
			NV_ASSERT( inDesc->ptr );
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inDesc->ptr;
			pvoid rscDmaData = (void*)( uint32(inDesc->ptr) + hdr->dmaSectionBOffset );
			// For all instances ...
			NvMeshBase* inst = (NvMeshBase*) inDesc->head;
			while( inst ) {
				if( inst->dmaData == rscDmaData )
					return TRUE;
				inst = (NvMeshBase*) inst->inst.next;
			}
			return FALSE;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NV_ASSERT( inRscData );
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inRscData;
			uint supplyBSize = 0;

			if( hdr->surfCpt )
				supplyBSize += sizeof(NvBitmap*) * hdr->surfCpt;

			if( hdr->skinnable ) {
				supplyBSize += sizeof(NvkMesh::BunchBlend) * hdr->bunchCpt;		// bunchBlendA
				supplyBSize += sizeof(NvMeshBase::BoneMat) * hdr->boneCpt;		// skelBoneMatA
				supplyBSize += sizeof(uint16) * hdr->boneCpt;					// skelBoneIdxA
			}

			pvoid rscDmaData	= (void*)(uint32(inRscData)+hdr->dmaSectionBOffset);	// DMAData of the original & shared resource
			pvoid instDmaData	= rscDmaData;											// DMAData of the instance
		//	if( (hdr->skinnable || hdr->morphable) && IsRscDmaDataIsUsed(inDesc) ) {
		//		// The instance must owns its DMAData section !
		//		supplyBSize += hdr->dmaSectionQSize*16 + 32;	// 16bytes aligned before & after
		//		instDmaData = NULL;
		//	}

			NvMeshBase* inst = NvEngineNewAS( NvMeshBase, 128, supplyBSize );
			if( !inst )		return NULL;
			NV_ASSERT_ALIGNED( inst, 128 );
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			uint32 supplyPtr0 = uint32( inst + 1 );
			uint32 supplyPtr  = supplyPtr0;

			inst->lockBitmapA = NULL;
			if( hdr->surfCpt ) {
				uint bsize = sizeof(NvBitmap*) * hdr->surfCpt;
				inst->lockBitmapA = (NvBitmap**) supplyPtr;
				supplyPtr += bsize;
				Memset( inst->lockBitmapA, 0, bsize );
			}

			inst->bunchBlendA = NULL;
			inst->skelBoneMatA = NULL;
			inst->skelBoneIdxA = NULL;
			if( hdr->skinnable ) {
				uint32 bsize0 = sizeof(NvkMesh::BunchBlend) * hdr->bunchCpt;	// bunchBlendA
				uint32 bsize1 = sizeof(NvMeshBase::BoneMat) * hdr->boneCpt;		// skelBoneMatA
				uint32 bsize2 = sizeof(uint16) * hdr->boneCpt;					// skelBoneIdxA
				inst->bunchBlendA  = (NvkMesh::BunchBlend*) supplyPtr;	supplyPtr += bsize0;
				inst->skelBoneMatA = (NvMeshBase::BoneMat*) supplyPtr;	supplyPtr += bsize1;
				inst->skelBoneIdxA = (uint16*)	            supplyPtr;	supplyPtr += bsize2;
			}

	/*		if( !instDmaData ) {
				// DUP the resource whole DMAData section
				uint bsize = hdr->dmaSectionQSize*16;
				instDmaData = (void*) Round16( supplyPtr );				// D$ align before dma section
				Memcpy( instDmaData, rscDmaData, bsize );
				supplyPtr = Round16( uint32(instDmaData) + bsize );		// D$ align after dma section
			//	Printf( "[UID:%x] NvMesh shared %dKo/%dKo\n", inDesc->uid, bsize>>10, inRscBSize>>10 );
			}
	*/		inst->dmaData = instDmaData;

			NV_ASSERT( (supplyPtr-supplyPtr0) <= supplyBSize );
			inst->_Init();
			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvMeshBase * inst = (NvMeshBase*) inInst;
			inst->_Shut();
			NvEngineDelete( inst );
		}
	};

}


bool
rscfactory_NvMesh_Reg()
{
	meshFactory = NvEngineNew( NvMeshFactory );
	return RscManager::RegisterFactory( meshFactory );
}



NvMesh *
NvMesh::Create	(	uint32	inUID	)
{
	if( !meshFactory )
		return NULL;
	return (NvMesh*) meshFactory->CreateInstance( inUID );
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvMesh::TYPE = 0xC57DF82A;	// CRC("NvMesh")

NVITF_MTH0(		Mesh, NvInterface*,				GetBase												)
NVITF_CAL0(		Mesh, void,						AddRef												)
NVITF_MTH0(		Mesh, uint,						GetRefCpt											)
NVITF_CAL0(		Mesh, void,						Release												)
NVITF_MTH0(		Mesh, uint32,					GetRscType											)
NVITF_MTH0(		Mesh, uint32,					GetRscUID											)
NVITF_MTH0(		Mesh, NvkMesh*,					GetKInterface										)
NVITF_MTH0(		Mesh, uint32,					GetNameCRC											)
NVITF_MTH0(		Mesh, Box3*,					GetBBox												)
NVITF_MTH0(		Mesh, uint,						GetSurfaceCpt										)
NVITF_MTH1(		Mesh, uint32,					GetSurfaceNameCRC,		uint						)
NVITF_MTH1(		Mesh, uint32,					GetSurfaceBitmapUID,	uint						)
NVITF_CAL0(		Mesh, void,						RefBitmaps											)
NVITF_CAL0(		Mesh, void,						UnrefBitmaps										)
NVITF_MTH0(		Mesh, bool,						IsSkinnable											)
NVITF_MTH0(		Mesh, bool,						IsNrmSkinnable										)
NVITF_MTH0(		Mesh, bool,						IsMorphable											)
NVITF_MTH0(		Mesh, bool,						IsLightable											)
NVITF_MTH0(		Mesh, bool,						IsShadowing											)
NVITF_MTH0(		Mesh, bool,						IsFullOpaque										)
NVITF_CAL1(		Mesh, void,						Enable,					uint32						)
NVITF_CAL1(		Mesh, void,						Disable,				uint32						)
NVITF_CAL1(		Mesh, void,						SetEnabled,				uint32						)
NVITF_MTH0(		Mesh, uint32,					GetEnabled											)
NVITF_MTH3(		Mesh, bool,						MapSkeleton,			uint, uint32*, Matrix*		)
NVITF_CAL0(		Mesh, void,						UnmapSkeleton										)
NVITF_MTH0(		Mesh, uint,						GetMorphSlotCpt										)
NVITF_CAL1(		Mesh, void,						SetMorphSlotCpt,		uint						)
NVITF_MTH1(		Mesh, NvMorphTarget*,			GetMorphSlotTarget,		uint						)
NVITF_CAL2(		Mesh, void,						SetMorphSlotTarget,		uint, NvMorphTarget*		)
NVITF_CAL2(		Mesh, void,						SetMorphSlotWeight,		uint, float					)
NVITF_CAL0(		Mesh, void,						Update												)

NVKITF_MTH0(	Mesh,	NvInterface*,			GetBase												)
NVKITF_CAL0(	Mesh,	void,					AddRef												)
NVKITF_MTH0(	Mesh,	uint,					GetRefCpt											)
NVKITF_CAL0(	Mesh,	void,					Release												)
NVKITF_MTH0(	Mesh,	uint32,					GetRscType											)
NVKITF_MTH0(	Mesh,	uint32,					GetRscUID											)
NVKITF_MTH0(	Mesh,	NvMesh*,				GetInterface										)
NVKITF_MTH0(	Mesh,	Box3*,					GetBBox												)
NVKITF_MTH0(	Mesh,	uint32,					GetNameCRC											)
NVKITF_MTH0(	Mesh,	bool,					IsSkinnable											)
NVKITF_MTH0(	Mesh,	bool,					IsNrmSkinnable										)
NVKITF_MTH0(	Mesh,	bool,					IsMorphable											)
NVKITF_MTH0(	Mesh,	bool,					IsLightable											)
NVKITF_MTH0(	Mesh,	bool,					IsFullOpaque										)
NVKITF_MTH0(	Mesh,	bool,					IsShadowing											)
NVKITF_CAL4(	Mesh,	void,					GetUnpacking,			float*,Vec3*,float*,Vec2*	)
NVKITF_MTH0(	Mesh,	uint,					GetBunchCpt											)
NVKITF_MTH0(	Mesh,	uint,					GetBunchDataCpt										)
NVKITF_MTH0(	Mesh,	uint32,					GetBunchDataBase									)
NVKITF_MTH0(	Mesh,	uint32,					GetBunchDataVTYPE									)
NVKITF_MTH0(	Mesh,	NvkMesh::Bunch*,		GetBunchA											)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchBBox*,	GetBunchBBoxA										)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchData*,	GetBunchDataA										)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchBlend*,	GetBunchBlendA										)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchList*,	GetBunchByBunchA									)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchList*,	GetBunchBySurfA										)
NVKITF_CAL0(	Mesh,	void,					RefBitmaps											)
NVKITF_CAL0(	Mesh,	void,					UnrefBitmaps										)
NVKITF_MTH0(	Mesh,	uint,					GetSurfaceCpt										)
NVKITF_MTH0(	Mesh,	NvkMesh::Shading*,		GetSurfaceShadingA									)
NVKITF_MTH4(	Mesh,	bool,					GetShadowing,			uint&, uint&,
																		NvkMesh::ShadowingVtx*&,
																		NvkMesh::ShadowingFace*&	)
NVKITF_CAL0(	Mesh,	void,					UpdateBeforeDraw									)
NVKITF_MTH0(	Mesh,	bool,					IsDrawing											)



