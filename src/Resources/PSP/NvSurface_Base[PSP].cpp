/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/NvkSurface[PSP].h>
#include <Kernel/Common/NvRscFactory_SHR.h>



namespace
{

	NvSurface::Component		_CoFromNative	(	ge::VertexMap::Component	inComponent		)
	{
		if( inComponent == ge::VertexMap::ST )		return NvSurface::CO_TEX;
		if( inComponent == ge::VertexMap::COLOR )	return NvSurface::CO_COLOR;
		if( inComponent == ge::VertexMap::NXYZ )	return NvSurface::CO_NORMAL;
		if( inComponent == ge::VertexMap::XYZ )		return NvSurface::CO_LOC;
		// Invalid !
		return NvSurface::Component( 0 );
	}

	ge::VertexMap::Component	_CoToNative		(	NvSurface::Component	 	inComponent		)
	{
		if( inComponent == NvSurface::CO_TEX )		return ge::VertexMap::ST;
		if( inComponent == NvSurface::CO_COLOR )	return ge::VertexMap::COLOR;
		if( inComponent == NvSurface::CO_NORMAL )	return ge::VertexMap::NXYZ;
		if( inComponent == NvSurface::CO_LOC )		return ge::VertexMap::XYZ;
		// Invalid !
		return ge::VertexMap::Component( 8 );
	}

}




NvSurface::Component
NvkSurface::CoFromNative	(	ge::VertexMap::Component	inComponent		)
{
	return _CoFromNative( inComponent );
}



ge::VertexMap::Component
NvkSurface::CoToNative		(	NvSurface::Component	 	inComponent		)
{
	return _CoToNative( inComponent );
}





//
// SURFACE BASE


struct NvSurfaceBase : NvInterface
{
	enum {
		FLG_EN_PRESENT		= NvkSurface::EN_PRESENT,
		FLG_EN_BEGIN_END	= NvkSurface::EN_BEGIN_END,
		FLG_EN_GENERATE_IDX	= NvkSurface::EN_GENERATE_IDX,
		//--
		FLG_EN_DBF			= (NvkSurface::EN_ALL+1),
		FLG_NEED_PRESENT	= (FLG_EN_DBF<<1),
		FLG_COPY_PRESENT	= (FLG_NEED_PRESENT<<1),		// otherwise, swap present
		FLG_BUILDING		= (FLG_COPY_PRESENT<<1)
	};

	uint				refCpt;
	NvSurface			itf;
	NvkSurface			kitf;
	ge::VertexMap		vmap;
	uint16				maxSize;
	NvSurface::PackMode	packMode;
	uint8				components;
	uint8				flags;					// FLG_* bit-mask
	uint8*				backData;
	uint8*				frontData;
	uint16				buildPos;
	uint8*				buildVtxP;
	uint32*				buildKickP;
	uint32				drawFrameNo;


	virtual	~	NvSurfaceBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)


	NvInterface*
	GetBase		(		)
	{
		return this;
	}

	void
	AddRef		(		)
	{
		refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return refCpt;
	}

	void
	Release		(		)
	{
		NV_ASSERT( refCpt );
		if( refCpt > 1 )
		{
			refCpt--;
		}
		else
		{
			NV_ASSERT( !IsDrawing() );		// Referenced in shader !!!

			// Safe to delete ?
			if( IsDrawing() )
			{
				DpyManager::AddToGarbager( this );
				return;
			}
			else
			{
				SafeFree( backData );
				SafeFree( frontData );
				NvEngineDelete( this );
			}
		}
	}

	uint32
	GetRscType		(		)
	{
		return NvSurface::TYPE;
	}

	uint32
	GetRscUID		(		)
	{
		return 0;
	}

	NvkSurface*
	GetKInterface	(		)
	{
		return &kitf;
	}

	NvSurface*
	GetInterface	(		)
	{
		return &itf;
	}

	uint
	GetComponents	(		)
	{
		return components;
	}

	bool
	HasComponent	(	NvSurface::Component	inComponent		)
	{
		return (components & uint(inComponent));
	}

	NvSurface::PackMode
	GetPackingMode	(		)
	{
		return NvSurface::PackMode( packMode );
	}

	uint
	GetMaxSize		(		)
	{
		return maxSize;
	}

	bool
	PresentBuffer	(			)
	{
		flags |= FLG_NEED_PRESENT;
		return TRUE;
	}

	void
	DoPresentBuffer	(		)
	{
		if( !(flags&FLG_NEED_PRESENT) )		return;
		if( !(flags&FLG_EN_PRESENT) )		return;

		if( flags & FLG_EN_GENERATE_IDX )
			GenerateIdxFromKick( frontData ? NvkSurface::BF_FRONT : NvkSurface::BF_BACK );

		NV_ASSERT( backData );
		if( frontData ) {
			if( flags & FLG_COPY_PRESENT )
				Memcpy( backData, frontData, GetBufferBSize() );
			else
				Swap( backData, frontData );
		}

		flags &= ~(FLG_NEED_PRESENT|FLG_BUILDING);
	}

	uint32
	GetBufferVTYPE	(		)
	{
		return vmap.vtype;
	}

	uint8*
	AllocBuffer		(			)
	{
		uint bs = GetBufferBSize();
		if( !bs )
			return NULL;
		return (uint8*) EngineMallocA( bs, 64 );	// D$ aligned
	}

	void
	FreeBuffer		(	uint8*		inBuffer	)
	{
		SafeFree( inBuffer );
	}

	uint
	GetBufferBSize	(		)
	{
		uint it = GET_VTYPE_IT( vmap.vtype );
		NV_ASSERT( it==0 || it==2 );
		uint vtxBSize  = vmap.bsize * maxSize;
		uint kickBSize = it ? Round32(maxSize)/8 : 0;	// one bit per vertex
		uint idxBSize  = it ? maxSize * 2 * 2 : 0;		// 16bits index, max 2 swap per vertex
		uint iltBSize  = it ? maxSize * 2 : 0;			// 16bits index count per vertex
		uint bs = Round4(vtxBSize) + Round4(kickBSize) + Round4(idxBSize) + iltBSize;
		return Round64( bs );	// D$ aligned
	}

	uint8*
	GetBufferBase		(	NvkSurface::Buffer		inBuffer	)
	{
		return (inBuffer == NvkSurface::BF_BACK) ? backData : frontData;
	}

	uint8*
	GetSubBufferBase	(	NvkSurface::Buffer		inBuffer,
							NvkSurface::SubBuffer	inSubBuffer	)
	{
		uint8* base  = GetBufferBase( inBuffer );
		if( !base )		return NULL;
		if( inSubBuffer == NvkSurface::SBF_VTX )	return base;
		if( !HasComponent(NvSurface::CO_KICK) )		return NULL;
		uint vtxBSize  = vmap.bsize * maxSize;
		uint kickBSize = Round32(maxSize)/8;
		uint idxBSize  = maxSize * 2 * 2;
		if( inSubBuffer == NvkSurface::SBF_KICK )	return base + Round4(vtxBSize);
		if( inSubBuffer == NvkSurface::SBF_IDX )	return base + Round4(vtxBSize) + Round4(kickBSize);
		if( inSubBuffer == NvkSurface::SBF_ILT )	return base + Round4(vtxBSize) + Round4(kickBSize) + Round4(idxBSize);
		return NULL;
	}

	uint8*
	GetComponentBase	(	NvkSurface::Buffer		inBuffer,
							NvSurface::Component	inComponent,
							uint*					outStride	= NULL,
							uint*					outPsm		= NULL		)
	{
		if( !HasComponent(inComponent) )
			return NULL;

		if( inComponent == NvSurface::CO_KICK ) {
			if( outStride )		*outStride = 4;
			if( outPsm )		*outPsm    = 0;
			return GetSubBufferBase( inBuffer, NvkSurface::SBF_KICK );
		}

		uint8* base = GetSubBufferBase( inBuffer, NvkSurface::SBF_VTX );
		if( !base )		return NULL;
		if( outStride )	*outStride = vmap.bsize;
		if( outPsm )	*outPsm    = vmap.GetComponentType( _CoToNative(inComponent) );
		return vmap.GetComponentPointer( base, _CoToNative(inComponent) );
	}

	bool
	GetBufferDrawing	(	NvkSurface::Buffer		inBuffer,
							uint					inOffset,
							uint					inSize,
							uint32*					outGeCmds	)	// 0:BASE 1:VADR 2:IADR 3:VTYPE 4:PRIM,
	{
		if( !outGeCmds || (inOffset+inSize) > maxSize )
			return FALSE;

		uint8*  vtxBase = (uint8*)  GetSubBufferBase( inBuffer, NvkSurface::SBF_VTX );
		uint16* idxBase = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_IDX );
		uint16* iltBase = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_ILT );
		if( !vtxBase )
			return FALSE;

		outGeCmds[0] = SCE_GE_SET_BASE( uint32(vtxBase)>>24 );
		outGeCmds[3] = vmap.vtype;
		if( idxBase ) {
			NV_ASSERT( GET_VTYPE_IT(vmap.vtype) == SCE_GE_VTYPE_USHORT );
			uint idx0 = iltBase[inOffset];
			uint idx1 = iltBase[inOffset+inSize-1];
			outGeCmds[1] = SCE_GE_SET_VADR( uint32(vtxBase) );
			outGeCmds[2] = SCE_GE_SET_IADR( uint32(idxBase+idx0) );
			outGeCmds[4] = SCE_GE_SET_PRIM( (idx1-idx0+1), SCE_GE_PRIM_TRIANGLE_STRIP );
		} else {
			outGeCmds[1] = SCE_GE_SET_VADR( uint32(vtxBase+inOffset*vmap.bsize) );
			outGeCmds[2] = SCE_GE_SET_IADR( 0 );
			outGeCmds[4] = SCE_GE_SET_PRIM( inSize, SCE_GE_PRIM_TRIANGLE_STRIP );
		}
		return TRUE;
	}

	void
	GenerateIdxFromKick	(	NvkSurface::Buffer		inBuffer	)
	{
		uint32* kickBase = (uint32*) GetSubBufferBase( inBuffer, NvkSurface::SBF_KICK );
		uint16* idxBase  = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_IDX );
		uint16* iltBase  = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_ILT );
		if( !kickBase )
			return;
		uint idxCount = 0;
		for( uint i = 0 ; i < maxSize ; i++ ) {
			uint kick = kickBase[i>>5] & (1U<<(i&31));
			if( !kick && i>1 )
				idxBase[idxCount++] = i-1;
			iltBase[i] = idxCount;
			idxBase[idxCount++] = i;
		}
	}

	bool
	Resize			(	uint		inMaxSize		)
	{
		if( IsDrawing() || inMaxSize < 3 )
			return FALSE;
		bool hasBack  = (backData  != NULL);
		bool hasFront = (frontData != NULL);
		SafeFree( backData );
		SafeFree( frontData );
		maxSize		= inMaxSize;
		backData	= hasBack  ? AllocBuffer() : NULL;
		frontData	= hasFront ? AllocBuffer() : NULL;
		flags	   &= ~(FLG_NEED_PRESENT|FLG_BUILDING);
		NV_ASSERT( !hasBack  || backData  );
		NV_ASSERT( !hasFront || frontData );
		return TRUE;
	}

	void
	EnableDBF		(	NvSurface::PresentMode	inPresentMode,
						bool					inCreateNow		)
	{
		if( inCreateNow && !frontData ) {
			frontData	= AllocBuffer();
			NV_ASSERT( frontData );
		}
		flags |=  FLG_EN_DBF;
		if( inPresentMode==NvSurface::SM_COPY )		flags |=  FLG_COPY_PRESENT;
		else										flags &= ~FLG_COPY_PRESENT;
	}

	void
	DisableDBF		(	bool		inReleaseNow	)
	{
		if( inReleaseNow ) {
			DoPresentBuffer();
			SafeFree( frontData );
		}
		flags &= ~FLG_EN_DBF;
	}

	bool
	IsEnabledDBF	(		)
	{
		return (flags & FLG_EN_DBF);
	}

	bool
	HasDBF			(		)
	{
		return (backData && frontData);
	}

	void
	Enable		(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		flags |= inEnableFlags;
	}

	void
	Disable		(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		flags &= ~inEnableFlags;
		if( !(flags&FLG_EN_BEGIN_END) )
			flags &= ~FLG_BUILDING;
	}

	bool
	IsEnabled	(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		return (flags & inEnableFlags);
	}

	bool
	Begin			(	uint		inStartOffset			)
	{
		if( !(flags&FLG_EN_BEGIN_END) )		return FALSE;
		if( (flags&FLG_BUILDING) )			return FALSE;
		if( inStartOffset>=maxSize )		return FALSE;

		// Choose the buffer to write : front or back ?
		NvkSurface::Buffer buildBuffer;
		if( !frontData && !IsDrawing() ) {
			// Use back
			NV_ASSERT( backData );
			buildBuffer = NvkSurface::BF_BACK;
		} else {
			// Use front
			if( !(flags&FLG_EN_DBF) )	return FALSE;
			if( !frontData )
				frontData = AllocBuffer();
			NV_ASSERT( frontData );
			if( !frontData )			return FALSE;
			buildBuffer = NvkSurface::BF_FRONT;
		}

		buildVtxP	= GetSubBufferBase( buildBuffer, NvkSurface::SBF_VTX ) + (inStartOffset * vmap.bsize);
		buildKickP	= (uint32*) GetSubBufferBase( buildBuffer, NvkSurface::SBF_KICK ) + (0);
		buildPos	= inStartOffset;
		flags |= FLG_BUILDING;
		return TRUE;
	}

	void
	TexCoord		(	const Vec2&		inST		)
	{
		if( !(flags&FLG_BUILDING) )					return;
		if( !HasComponent(NvSurface::CO_TEX) )		return;
		if( buildPos>=maxSize )						return;
		NV_ASSERT( GET_VTYPE_TT(vmap.vtype) == SCE_GE_VTYPE_FLOAT );
		Vec2Copy( (Vec2*)(buildVtxP+vmap.cbo[1]), &inST );
	}

	void
	Color			(	uint32			inColor,
						Psm				inPSM	)
	{
		if( !(flags&FLG_BUILDING) )					return;
		if( !HasComponent(NvSurface::CO_COLOR) )	return;
		if( buildPos>=maxSize )						return;
		NV_ASSERT( GET_VTYPE_CT(vmap.vtype) == SCE_GE_VTYPE_CT8888 );
		if( inPSM != PSM_ABGR32 )
			inColor = ConvertPSM( PSM_ABGR32, inPSM, inColor );
		*((uint32*)(buildVtxP+vmap.cbo[2])) = inColor;
	}

	void
	Normal			(	const Vec3&		inNXYZ		)
	{
		if( !(flags&FLG_BUILDING) )					return;
		if( !HasComponent(NvSurface::CO_NORMAL) )	return;
		if( buildPos>=maxSize )						return;
		NV_ASSERT( GET_VTYPE_NT(vmap.vtype) == SCE_GE_VTYPE_FLOAT );
		Vec3Copy( (Vec3*)(buildVtxP+vmap.cbo[3]), &inNXYZ );
	}

	void
	Vertex			(	const Vec3&		inXYZ,
						bool			inKick		)
	{
		if( !(flags&FLG_BUILDING) )					return;
		if( !HasComponent(NvSurface::CO_LOC) )		return;
		if( buildPos>=maxSize )						return;
		NV_ASSERT( GET_VTYPE_VT(vmap.vtype) == SCE_GE_VTYPE_FLOAT );
		Vec3Copy( (Vec3*)(buildVtxP+vmap.cbo[4]), &inXYZ );
		buildVtxP += vmap.bsize;
		if( buildKickP ) {
			uint i32 = buildPos>>5;
			uint m32 = 1U << (buildPos&31);
			if( inKick )	buildKickP[i32] |=  m32;
			else			buildKickP[i32] &= ~m32;
		}
		buildPos++;
	}

	void
	End				(			)
	{
		if( !(flags&FLG_BUILDING) )		return;
		flags &= ~FLG_BUILDING;
		PresentBuffer();
	}

	void
	UpdateBeforeDraw	(		)
	{
		NV_ASSERT( refCpt > 0 );
		drawFrameNo = DpyManager::GetFrameNo();
		DoPresentBuffer();
		// release front ?
		if( frontData && !(flags&FLG_EN_DBF) )
			DisableDBF( TRUE );
	}

	bool
	IsDrawing	(		)
	{
		return DpyManager::IsDrawing( drawFrameNo );
	}
};






NvSurface*
NvSurface::Create	(	uint			inMaxSize,
						uint			inComponents,
						PackMode		inPackMode	)
{
	if( inMaxSize<3 || (inComponents&CO_LOC)==0 )
		return NULL;

	// Force not packed !
	inPackMode = PM_NONE;

	// Build VTYPE
	uint32 vtype = 0;
	{
		uint vt	= SCE_GE_VTYPE_NONE;
		uint tt = SCE_GE_VTYPE_NONE;
		uint ct	= SCE_GE_VTYPE_NONE;
		uint nt	= SCE_GE_VTYPE_NONE;
		uint it	= SCE_GE_VTYPE_NONE;
		if( inComponents & CO_LOC ) {
			if( inPackMode == PM_NONE )			vt = SCE_GE_VTYPE_FLOAT;
			else if( inPackMode == PM_SAFE )	vt = SCE_GE_VTYPE_SHORT;
			else 								vt = SCE_GE_VTYPE_CHAR;
		}
		if( inComponents & CO_TEX ) {
			if( inPackMode == PM_NONE )			tt = SCE_GE_VTYPE_FLOAT;
			else if( inPackMode == PM_SAFE )	tt = SCE_GE_VTYPE_USHORT;
			else 								tt = SCE_GE_VTYPE_UCHAR;
		}
		if( inComponents & CO_COLOR ) {
			if( inPackMode == PM_NONE )			ct = SCE_GE_VTYPE_CT8888;
			else if( inPackMode == PM_SAFE )	ct = SCE_GE_VTYPE_CT4444;
			else 								ct = SCE_GE_VTYPE_CT5551;
		}
		if( inComponents & CO_NORMAL ) {
			if( inPackMode == PM_NONE )			nt = SCE_GE_VTYPE_FLOAT;
			else 								nt = SCE_GE_VTYPE_CHAR;
		}
		if( inComponents & CO_KICK )
			it = SCE_GE_VTYPE_USHORT;
		vtype = SCE_GE_SET_VTYPE( tt, ct, nt, vt, 0, it, 0, 0, 0 );
	}

	NvSurfaceBase* inst = NvEngineNew( NvSurfaceBase );
	if( !inst )
		return NULL;

	inst->refCpt		= 1;
	inst->vmap.Init( vtype );
	inst->maxSize		= inMaxSize;
	inst->packMode		= inPackMode;
	inst->components	= inComponents;
	inst->flags			= NvkSurface::EN_DEFAULT;
	inst->backData		= NULL;
	inst->frontData		= NULL;
	inst->buildPos		= 0;
	inst->buildVtxP		= NULL;
	inst->buildKickP	= NULL;
	inst->drawFrameNo	= ~0U;

	inst->backData		= inst->AllocBuffer();
	if( !inst->backData ) {
		NvEngineDelete( inst );
		return NULL;
	}

	return &(inst->itf);
}




//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvSurface::TYPE = 0x7D788970;	// CRC("NvSurface")

NVITF_MTH0(		Surface,	NvInterface*,			GetBase										)
NVITF_CAL0(		Surface,	void,					AddRef										)
NVITF_MTH0(		Surface,	uint,					GetRefCpt									)
NVITF_CAL0(		Surface,	void,					Release										)
NVITF_MTH0(		Surface,	uint32,					GetRscType									)
NVITF_MTH0(		Surface,	uint32,					GetRscUID									)
NVITF_MTH0(		Surface,	NvkSurface*,			GetKInterface								)
NVITF_MTH0(		Surface,	uint,					GetComponents								)
NVITF_MTH1(		Surface,	bool,					HasComponent,		NvSurface::Component	)
NVITF_MTH0(		Surface,	NvSurface::PackMode,	GetPackingMode								)
NVITF_MTH0(		Surface,	uint,					GetMaxSize									)
NVITF_MTH1(		Surface,	bool,					Resize,				uint					)
NVITF_CAL2(		Surface,	void,					EnableDBF,			NvSurface::PresentMode,
																		bool					)
NVITF_CAL1(		Surface,	void,					DisableDBF,			bool					)
NVITF_MTH0(		Surface,	bool,					IsEnabledDBF								)
NVITF_MTH0(		Surface,	bool,					HasDBF										)
NVITF_MTH1(		Surface,	bool,					Begin,				uint					)
NVITF_CAL1(		Surface,	void,					TexCoord,			const Vec2&				)
NVITF_CAL2(		Surface,	void,					Color,				uint32, Psm				)
NVITF_CAL1(		Surface,	void,					Normal,				const Vec3&				)
NVITF_CAL2(		Surface,	void,					Vertex,				const Vec3&, bool		)
NVITF_CAL0(		Surface,	void,					End											)

NVKITF_MTH0(	Surface,	NvInterface*,			GetBase										)
NVKITF_CAL0(	Surface,	void,					AddRef										)
NVKITF_MTH0(	Surface,	uint,					GetRefCpt									)
NVKITF_CAL0(	Surface,	void,					Release										)
NVKITF_MTH0(	Surface,	uint32,					GetRscType									)
NVKITF_MTH0(	Surface,	uint32,					GetRscUID									)
NVKITF_MTH0(	Surface,	NvSurface*,				GetInterface								)
NVKITF_MTH0(	Surface,	uint,					GetComponents								)
NVKITF_MTH1(	Surface,	bool,					HasComponent,		NvSurface::Component	)
NVKITF_MTH0(	Surface,	NvSurface::PackMode,	GetPackingMode								)
NVKITF_MTH0(	Surface,	uint,					GetMaxSize									)
NVKITF_MTH1(	Surface,	bool,					Resize,				uint					)
NVKITF_CAL2(	Surface,	void,					EnableDBF,			NvSurface::PresentMode,
																		bool					)
NVKITF_CAL1(	Surface,	void,					DisableDBF,			bool					)
NVKITF_MTH0(	Surface,	bool,					IsEnabledDBF								)
NVKITF_MTH0(	Surface,	bool,					HasDBF										)
NVKITF_CAL1(	Surface,	void,					Enable,				uint					)
NVKITF_CAL1(	Surface,	void,					Disable,			uint					)
NVKITF_MTH1(	Surface,	bool,					IsEnabled,			uint					)
NVKITF_MTH0(	Surface,	bool,					PresentBuffer								)
NVKITF_MTH0(	Surface,	uint8*,					AllocBuffer									)
NVKITF_CAL1(	Surface,	void,					FreeBuffer,			uint8*					)
NVKITF_MTH0(	Surface,	uint,					GetBufferBSize								)
NVKITF_MTH0(	Surface,	uint32,					GetBufferVTYPE								)
NVKITF_MTH1(	Surface,	uint8*,					GetBufferBase,		NvkSurface::Buffer		)
NVKITF_MTH2(	Surface,	uint8*,					GetSubBufferBase,	NvkSurface::Buffer,
																		NvkSurface::SubBuffer	)
NVKITF_MTH4(	Surface,	uint8*,					GetComponentBase, 	NvkSurface::Buffer,
																		NvSurface::Component,
																		uint*, uint*			)
NVKITF_MTH4(	Surface,	bool,					GetBufferDrawing, 	NvkSurface::Buffer,
																		uint, uint, uint32*		);
NVKITF_CAL0(	Surface,	void,					UpdateBeforeDraw							)
NVKITF_MTH0(	Surface,	bool,					IsDrawing									)



Psm
NvSurface::GetNativeColorPSM	(		)
{
	return PSM_ABGR32;
}


Psm
NvkSurface::GetNativeColorPSM	(		)
{
	return PSM_ABGR32;
}



