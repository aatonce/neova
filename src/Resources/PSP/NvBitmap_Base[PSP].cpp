/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvBitmap_Base[PSP].h"



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvBitmap::TYPE = 0xB067EBB2;	// CRC("NvBitmap")

NVITF_MTH0(		Bitmap,	NvInterface*,			GetBase							)
NVITF_CAL0(		Bitmap,	void,					AddRef							)
NVITF_MTH0(		Bitmap,	uint,					GetRefCpt						)
NVITF_CAL0(		Bitmap,	void,					Release							)
NVITF_MTH0(		Bitmap,	uint32,					GetRscType						)
NVITF_MTH0(		Bitmap,	uint32,					GetRscUID						)
NVITF_MTH0(		Bitmap,	NvkBitmap*,				GetKInterface					)
NVITF_MTH0(		Bitmap,	uint32,					GetWidth						)
NVITF_MTH0(		Bitmap,	uint32,					GetHeight						)
NVITF_MTH0(		Bitmap,	uint32,					GetWPad							)
NVITF_MTH0(		Bitmap,	uint32,					GetHPad							)
NVITF_MTH0(		Bitmap,	NvBitmap::AlphaStatus,	GetAlphaStatus					)
NVITF_MTH0(		Bitmap,	bool,					IsBanner							)
NVITF_MTH0(		Bitmap,	bool,					IsMask							)
NVITF_MTH2(		Bitmap,	bool,					Pick,	uint, uint				)

NVKITF_MTH0(	Bitmap,	NvInterface*,			GetBase							)
NVKITF_CAL0(	Bitmap,	void,					AddRef							)
NVKITF_MTH0(	Bitmap,	uint,					GetRefCpt						)
NVKITF_CAL0(	Bitmap,	void,					Release							)
NVKITF_MTH0(	Bitmap,	uint32,					GetRscType						)
NVKITF_MTH0(	Bitmap,	uint32,					GetRscUID						)
NVKITF_MTH0(	Bitmap,	NvBitmap*,				GetInterface					)
NVKITF_MTH0(	Bitmap,	uint32,					GetWidth						)
NVKITF_MTH0(	Bitmap,	uint32,					GetHeight						)
NVKITF_MTH0(	Bitmap,	uint32,					GetWPad							)
NVKITF_MTH0(	Bitmap,	uint32,					GetHPad							)
NVKITF_MTH0(	Bitmap,	NvBitmap::AlphaStatus,	GetAlphaStatus					)
NVKITF_MTH0(	Bitmap,	int,					GetTRamId						)
NVKITF_CAL0(	Bitmap,	void,					UpdateBeforeDraw				)
NVKITF_MTH0(	Bitmap,	bool,					IsDrawing						)




