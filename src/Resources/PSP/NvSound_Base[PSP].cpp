/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/NvkSound[PSP].h>
#include <Kernel/Common/NvRscFactory_SHR.h>




namespace
{
	RscFactory* soundFactory = NULL;
}




//
// BASE


struct NvSoundBase : public RscFactory_SHR::Instance
{
	#include <Projects\Plugin NovaPSP\comp_NvSound_format.h>

	NvSound				itf;
	NvkSound			kitf;
	uint32				dataBOffset;
	uint32				dataBSize;


	virtual	~	NvSoundBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

	Header*
	hdr				(		)
	{
		return (Header*)inst.dataptr;
	}

	void
	AddRef			(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release			(		)
	{
		NV_ASSERT( inst.refCpt );
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			soundFactory->FreeInstance( &itf );
		}
	}

	uint32
	GetRscType		(		)
	{
		return NvSound::TYPE;
	}

	uint32
	GetRscUID		(		)
	{
		return inst.uid;
	}

	NvkSound*
	GetKInterface	(		)
	{
		return &kitf;
	}

	NvSound*
	GetInterface	(		)
	{
		return &itf;
	}

	float
	GetDuration		(		)
	{
		return hdr()->duration;
	}

	bool
	IsLooped	(		)
	{
		return ( hdr()->loop != 0 );
	}

	bool
	IsStreamed			(		)
	{
		return ( hdr()->mode != 0 );
	}

	bool
	IsMusic				(		)
	{
		return ( hdr()->mode == 2 );
	}

	uint
	GetFreq			(			)
	{
		return hdr()->freq;
	}

	uint
	GetMarkerCpt	(			)
	{
		return hdr()->markerCpt;
	}

	uint32
	GetMarkerNameCRC	(	uint		inMarkerNo		)
	{
		if( inMarkerNo >= hdr()->markerCpt )
			return 0;
		// Markers follow the header
		Marker* markers = (Marker*)( hdr()+1 );
		return markers[inMarkerNo].nameCRC;
	}

	float
	GetMarkerTime		(	uint		inMarkerNo		)
	{
		if( inMarkerNo >= hdr()->markerCpt )
			return -1.0f;
		// Markers follow the header
		Marker* markers = (Marker*)( hdr()+1 );
		return markers[inMarkerNo].t;
	}

	int
	FindMarker			(	uint32		inMarkerName	)
	{
		if( inMarkerName == 0 )
			return -1;
		uint nb = GetMarkerCpt();
		for( uint i = 0 ; i < nb ; i++ )
			if( GetMarkerNameCRC(i) == inMarkerName )
				return i;
		return -1;
	}

	uint
	GetMode		(							)
	{
		return hdr()->mode;
	}

	void
	GetBFData	(	uint32	&	outBOffset,
					uint32	&	outBSize	)
	{
		outBOffset	= dataBOffset;
		outBSize	= dataBSize;
	}

};





//
// FACTORY


namespace
{

	struct NvSoundFactory : public RscFactory_SHR
	{
		uint32
		GetType	(	)
		{
			return NvSound::TYPE;
		}

		uint32
		GetToPrefetchRscBSize	(	uint32		inUID,
									uint32		inBSize		)
		{
			if( !inUID )
				return 0;
			// Read header + up to 256 markers !
			uint32 pbsize  = sizeof(NvSoundBase::Header);
				   pbsize += sizeof(NvSoundBase::Marker) * 256;
			return Min( pbsize, inBSize );
		}

		bool
		PrefetchRsc		(	uint32		inUID,
							pvoid		inPtr,
							uint32		inBSize		)
		{
			// Overrides PrefetchRsc() to remove unused markers from memory
			// before the first instanciation !
			if( !RscFactory_SHR::PrefetchRsc(inUID,inPtr,inBSize) )
				return FALSE;	// failed !
			// Modify the prefeteched data ...
			Desc* desc = FindDesc( inUID );
			if( desc ) {
				NvSoundBase::Header* hdr = (NvSoundBase::Header*) desc->ptr;
				uint32 pbsize  = sizeof(NvSoundBase::Header);
				       pbsize += sizeof(NvSoundBase::Marker) * hdr->markerCpt;
				if( desc->bsize > pbsize ) {
					desc->bsize = pbsize;
					desc->ptr   = EngineRealloc( desc->ptr, desc->bsize );
					NV_ASSERT( desc->ptr );
				}
			}
			return TRUE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvSoundBase::Header * hdr = (NvSoundBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvSoundBase::Header)
					&&	hdr->ver == NV_SOUND_CVER					);
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_FULL;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvSoundBase * inst = (NvSoundBase*) inInst;
			return &inst->itf;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NvSoundBase * inst = NvEngineNew( NvSoundBase );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			// Get dataBOffset & dataBSize in the BigFile (minus the header bsize & markers)
			RscManager::RscInfo rsc;
			bool done = RscManager::GetRscInfo( inst->inst.uid, &rsc );
			NV_ASSERT( done );
			NV_ASSERT( rsc.bsize >= sizeof(NvSoundBase::Header) );
			if( !done || rsc.bsize < sizeof(NvSoundBase::Header) ) {
				// Bigfile close ?!
				NvEngineDelete( inst );
				return NULL;
			}

			inst->dataBSize    = rsc.bsize;
			inst->dataBOffset  = rsc.boffset[0];
			inst->dataBSize   -= sizeof(NvSoundBase::Header);
			inst->dataBOffset += sizeof(NvSoundBase::Header);
			inst->dataBSize   -= sizeof(NvSoundBase::Marker) * inst->hdr()->markerCpt;
			inst->dataBOffset += sizeof(NvSoundBase::Marker) * inst->hdr()->markerCpt;

			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvSoundBase * inst = (NvSoundBase*) inInst;
			NvEngineDelete( inst );
		}
	};

}


bool
rscfactory_NvSound_Reg()
{
	soundFactory = NvEngineNew( NvSoundFactory );
	return RscManager::RegisterFactory( soundFactory );
}



NvSound *
NvSound::Create	(	uint32	inUID	)
{
	if( !soundFactory )
		return NULL;
	return (NvSound*) soundFactory->CreateInstance( inUID );
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvSound::TYPE = 0x9A77CC99;	// CRC("NvSound")

NVITF_MTH0(		Sound,	NvInterface*,	GetBase										)
NVITF_CAL0(		Sound,	void,			AddRef										)
NVITF_MTH0(		Sound,	uint,			GetRefCpt									)
NVITF_CAL0(		Sound,	void,			Release										)
NVITF_MTH0(		Sound,	uint32,			GetRscType									)
NVITF_MTH0(		Sound,	uint32,			GetRscUID									)
NVITF_MTH0(		Sound,	NvkSound*,		GetKInterface								)
NVITF_MTH0(		Sound,	float,			GetDuration									)
NVITF_MTH0(		Sound,	bool,			IsStreamed									)
NVITF_MTH0(		Sound,	bool,			IsLooped									)
NVITF_MTH0(		Sound,	bool,			IsMusic										)
NVITF_MTH0(		Sound,	uint,			GetMarkerCpt								)
NVITF_MTH1(		Sound,	uint32,			GetMarkerNameCRC,	uint					)
NVITF_MTH1(		Sound,	float,			GetMarkerTime,		uint					)
NVITF_MTH1(		Sound,	int,			FindMarker,			uint32					)

NVKITF_MTH0(	Sound,	NvInterface*,	GetBase										)
NVKITF_CAL0(	Sound,	void,			AddRef										)
NVKITF_MTH0(	Sound,	uint,			GetRefCpt									)
NVKITF_CAL0(	Sound,	void,			Release										)
NVKITF_MTH0(	Sound,	uint32,			GetRscType									)
NVKITF_MTH0(	Sound,	uint32,			GetRscUID									)
NVKITF_MTH0(	Sound,	NvSound*,		GetInterface								)
NVKITF_MTH0(	Sound,	uint,			GetFreq										)
NVKITF_MTH0(	Sound,	uint,			GetMode										)
NVKITF_MTH0(	Sound,	float,			GetDuration									)
NVKITF_MTH0(	Sound,	bool,			IsStreamed									)
NVKITF_MTH0(	Sound,	bool,			IsLooped									)
NVKITF_MTH0(	Sound,	bool,			IsMusic										)
NVKITF_CAL2(	Sound,	void,			GetBFData,			uint32&,	uint32&		)


