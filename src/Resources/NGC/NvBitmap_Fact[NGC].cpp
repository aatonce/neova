/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/NvDpyManager[NGC].h>
#include <Kernel/Common/NvkCore_Mem.h>
#include "NvBitmap_Base[NGC].h"

#include <dolphin/gx.h>


namespace
{
	#define NB_MIP_COLOR 3
	
	uint32 mipColor32[NB_MIP_COLOR] = {	0xFFFF0000,
										0xFF00FF00,			
										0xFF0000FF,	};

	uint16 mipColor16[NB_MIP_COLOR] = {	0xFC00,	// in RGB555
										0x83E0,	
										0x801F,	};
	uint8 S3TCImg8x8[NB_MIP_COLOR][8] = 
	{
		{
			0x0,0x0,0xe0,0x7,
			0x55,0x55,0x55,0x55,
		},
		{
			0x0,0x0,0x1f,0x0,
			0x55,0x55,0x55,0x55,
		},
		{
			0x0,0x0,0x0,0xf8,
			0x55,0x55,0x55,0x55,
		}
	};
	

	RscFactory*  bitmapFactory = NULL;


	struct Bitmap : public NvBitmapBase
	{
		#include <Projects\Plugin NovaNGC\comp_NvBitmap_format.h>

		void
		WriteS3TCBitmap	(uint	inLODMask)
		{
			uint curImg  	= 0;
			uint8 * data 	= (uint8 *)CachedPointer(GetTexel());
			uint writeTile 	= 0;
			
			for (uint m = 0 ; m < hdr()->mipmapCpt; ++m) 
			{
				if( ((1<<m)&inLODMask) == 0 )
					continue;
			
				uint mipW 			= GetWidth() >> m;
				uint mipH 			= GetHeight() >> m;
				uint nbTileX		= mipW >> 2;
				uint nbTileY		= mipH >> 2;

				uint padTileX		= (mipW & 0x7) >> 2;
				uint padTileY		= (mipH & 0x7) >> 2;
				
				uint nbTotalTileX	= nbTileX + padTileX;
				uint nbTotalTileY	= nbTileY + padTileY;

				uint nbTotalTile	= (nbTotalTileX) * (nbTotalTileY);

				for (uint t = 0 ; t < nbTotalTile ; ++t ) {
					Memcpy(data,S3TCImg8x8[curImg],8);
					data+=8;
					writeTile++;
				}
				
				curImg = ( curImg + 1 ) % NB_MIP_COLOR;
			}
			SyncDCache( CachedPointer(GetTexel()) , writeTile * 32);
		}
		
		void 
		WriteRGBA32Bitmap(uint	inLODMask)
		{
			uint 	curImg  	= 0;
			uint16* data 	= (uint16 *)CachedPointer(GetTexel());
			
			for (uint m = 0 ; m < hdr()->mipmapCpt; ++m) 
			{
				if( ((1<<m)&inLODMask) == 0 )
					continue;
			
				uint mipW 			= GetWidth() >> m;
				uint mipH 			= GetHeight() >> m;

				uint16 tmpAR[4*4];
				uint16 tmpGB[4*4];

				// NGC swizzling
				for (uint line = 0 ; line < mipH ; line += 4)
				{
					for (uint col = 0 ; col < mipW ; col += 4 )
					{
						uint tmpId = 0;
						for (uint j = 0 ; j < 4 ; ++j) {
							for (u32 i = 0 ; i < 4 ; ++i) {
								tmpAR[tmpId] = uint16((mipColor32[curImg] >>16)	& 0xFFFF);
								tmpGB[tmpId] = uint16((mipColor32[curImg]     ) 	& 0xFFFF);
								tmpId ++;		
							}
						}

						for( int i = 0 ; i < 16 ; ++i)
						{
							*data= tmpAR[i];
							data++;
						}
							

						for( int i = 0 ; i < 16 ; ++i)
						{
							*data= tmpGB[i];
							data++;
						}
					}
				}
				
				curImg = ( curImg + 1 ) % NB_MIP_COLOR;
			}
			SyncDCache( CachedPointer(GetTexel()) , uint32(data) - uint32 (CachedPointer(GetTexel()) ));
		}
		
		void 
		WriteRGBA16Bitmap(uint	inLODMask)
		{
			uint 	curImg  	= 0;
			uint16* data 	= (uint16 *)CachedPointer(GetTexel());
			
			for (uint m = 0 ; m < hdr()->mipmapCpt; ++m) 
			{
				if( ((1<<m)&inLODMask) == 0 )
					continue;
				uint mipW 			= GetWidth() >> m;
				uint mipH 			= GetHeight() >> m;
				
				// NGC swizzling
				for (uint line = 0 ; line < mipH ; line += 4){
					for (uint col = 0 ; col < mipW ; col += 4 ){
						for (uint j = 0 ; j < 4 ; ++j) {
							for (u32 i = 0 ; i < 4 ; ++i) {
								*data = mipColor16[curImg]; //
								data++;
							}
						}
					}
				}
				curImg = ( curImg + 1 ) % NB_MIP_COLOR;
			}
			SyncDCache( CachedPointer(GetTexel()) ,uint32(data) - uint32 (CachedPointer(GetTexel())));
			
		}
		
		uint32		drawFrameNo;

		virtual	~	Bitmap	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		bool Init()
		{
			#if defined( _NVCOMP_ENABLE_DBG )
			uint32 checkMipMode;
			uint32 checkMipLods;
			core::GetParameter( core::PR_CHECK_MIPMAPPING_MODE, &checkMipMode );
			core::GetParameter( core::PR_CHECK_MIPMAPPING_LODS, &checkMipLods );
			if( checkMipMode ) 
			{
				//TestMipmaping( inDesc, checkMipMode-1, checkMipLods );
				if( hdr()->mipmapCpt == 1 )
					return TRUE;
				uint allLodMask	= (1<<hdr()->mipmapCpt) - 1;
				checkMipLods &= allLodMask;
				if( checkMipLods == 0 )
					return TRUE;
				
				if (GetPixelFormat() == GX_TF_CMPR) 
					WriteS3TCBitmap(checkMipLods);
				else if (GetPixelFormat() == GX_TF_RGBA8) 
					WriteRGBA32Bitmap(checkMipLods);
				else if (GetPixelFormat() == GX_TF_RGB5A3) 
					WriteRGBA16Bitmap(checkMipLods);
			}

			#endif
			return TRUE;
		}
		
		Header*
		hdr				(		)
		{
			return (Header*)inst.dataptr;
		}

		void
		AddRef			(			)
		{
			inst.refCpt++;
		}

		uint
		GetRefCpt		(		)
		{
			return inst.refCpt;
		}

		void
		Release			(		)
		{
			NV_ASSERT( inst.refCpt );
			if( inst.refCpt > 1 )
			{
				inst.refCpt--;
			}
			else
			{
				// Safe to delete ?
				if( IsDrawing() )
				{
					DpyManager::AddToGarbager( this );
					return;
				}
				else
				{
					NV_ASSERT( bitmapFactory );
					bitmapFactory->FreeInstance( GetInterface() );
				}
			}
		}

		uint32
		GetRscType	(			)
		{
			return NvBitmap::TYPE;
		}
	
		uint32
		GetRscUID		(		)
		{
			return inst.uid;
		}
	
		uint32
		GetWidth		(		)
		{
			return hdr()->width;
		}
		
	
		uint32
		GetHeight		(		)
		{
			return hdr()->height;
		}
		
		uint32
		GetWPad		(		)
		{
			return hdr()->wpad;
		}
	
		uint32
		GetHPad		(		)
		{
			return hdr()->hpad;
		}
		
		GXTexFmt
		GetPixelFormat ()
		{
			return GXTexFmt(hdr()->pfmt);
		}

		NvBitmap::AlphaStatus
		GetAlphaStatus	(		)
		{
			return NvBitmap::AlphaStatus( hdr()->aStatus );
		}
		
		uint32
		GetMipmapCpt 	(	)
		{
			return hdr()->mipmapCpt;
		}

		void
		UpdateBeforeDraw	(		)
		{
			drawFrameNo = DpyManager::GetFrameNo();
		}

		void	*	
		GetTexel	(	)
		{
			return hdr()->iData;
		}
		
		bool
		IsDrawing	(		)
		{
			return DpyManager::IsDrawing( drawFrameNo );
		}

		bool
		IsBanner	(	)
		{
			return (hdr()->pfmt == 0xFFFE);
		}

		bool IsMask ( )
		{
			return (hdr()->pfmt == 0xFFFF);
		}

		bool Pick ( uint x, uint y)
		{
			if( IsMask() && x<hdr()->width && y<hdr()->height )
			{
				uint8* mbits = (uint8*)CachedPointer( GetTexel() );
				uint i = y * hdr()->width + x;
				return ( mbits[i>>3] & (1<<(i&7)) ) != 0;
			}
			return FALSE;
		}
	};




	//
	// FACTORY

	struct BitmapFactory : public RscFactory_SHR
	{
		uint32
		GetType		(							)
		{
			return NvBitmap::TYPE;
		}

		pvoid
		AllocMemRsc	(	uint32		inBSize		)
		{
			#ifdef _RVL
				return nv::mem::NativeMalloc( inBSize, 32, nv::mem::MEM_NGC_MEM2 );
			#else 
				return RscMallocA( inBSize, 32 );
			#endif 			
		}

		void
		FreeMemRsc		(	pvoid		inPtr,
							uint32	/*	inBSize	*/	)
		{	
			#ifdef _RVL
				nv::mem::NativeFree( inPtr, nv::mem::MEM_NGC_MEM2 );
			#else 
				RscFree( inPtr );
			#endif
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			Bitmap::Header* hdr = (Bitmap::Header*) inRscData;
			return (	inBSize >= sizeof(Bitmap::Header)
					&&	hdr->ver == NV_BITMAP_CVER	);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			Bitmap::Header * hdr = (Bitmap::Header *) inRscData;
			hdr->iData += uint(hdr);				
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_FULL;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			return ((Bitmap*)inInst)->GetInterface();
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			Bitmap* inst = NvEngineNew( Bitmap );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );
			inst->drawFrameNo = ~0U;
			
			Bitmap::Header * hdr = inst->hdr();
			SyncDCache( hdr->iData, inRscBSize );
			hdr->iData = UncachedPointer(hdr->iData);
			inst->Init();
			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			Bitmap* inst = (Bitmap*) inInst;			
			NvEngineDelete( inst );
		}
	};
}





bool
rscfactory_NvBitmap_Reg()
{
	bitmapFactory = NvEngineNew( BitmapFactory );
	return RscManager::RegisterFactory( bitmapFactory );
}



NvBitmap *
NvBitmap::Create	(	uint32	inUID	)
{
	if( !bitmapFactory )
		return NULL;
	else
		return (NvBitmap*) bitmapFactory->CreateInstance( inUID );
}
