/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#include <Kernel/NGC/NvkSound[NGC].h>
#include <Kernel/Common/NvRscFactory_SHR.h>

#include <dolphin/os.h>
#include <dolphin/ax.h>

namespace
{
	RscFactory* soundFactory = NULL;
}




//
// BASE


struct NvSoundBase : public RscFactory_SHR::Instance
{

	#include <Projects\Plugin NovaNGC\comp_NvSound_format.h>
	
	NvSound				itf;
	NvkSound			kitf;
	
	uint32				dataBOffset;
	uint32				dataBSize;
	uint32				perChannelLoopCtxSize;	
	DSPADPCM * 			adpcm;

				

	virtual	~	NvSoundBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

	Header *
	hdr		(		)
	{
		return (Header*)inst.dataptr;
	}


	void
	Init	(		)
	{
	
	}
	
	void
	AddRef			(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release			(		)
	{
		NV_ASSERT( inst.refCpt );
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			soundFactory->FreeInstance( &itf );
		}
	}

	uint32
	GetRscType		(		)
	{
		return NvSound::TYPE;
	}

	uint32
	GetRscUID		(		)
	{
		return inst.uid;
	}

	NvkSound*
	GetKInterface	(		)
	{
		return &kitf;
	}

	NvSound*
	GetInterface	(		)
	{
		return &itf;
	}

	float
	GetDuration		(		)
	{
		return hdr()->duration;
	}
	
	//MD_SPOT_FX	= 0,					// Mono spot FX (for short duration)
	//MD_STREAM_FX	= 1,					// Mono Stream (for long duration)
	//MD_MUSIC		= 2,					// Ambient n-channels music (HW limited !)
	//MD_SPEAKER	= 3						// Speaker

	bool
	IsLooped	(		)
	{
		return ( hdr()->loop != 0 );
	}

	bool
	IsStreamed			(		)
	{
		return ( hdr()->mode != 0 && hdr()->mode != 3 );
	}

	bool
	IsMusic				(		)
	{
		return ( hdr()->mode == 2 );
	}
	
	bool
	IsWMSpeaker	()
	{
		return ( hdr()->mode == 3 );
	}

	uint
	GetFreq			(			)
	{
		return hdr()->freq;
	}

	uint
	GetMarkerCpt	(			)
	{
		return hdr()->markerCpt;
	}

	uint32
	GetMarkerNameCRC	(	uint		inMarkerNo		)
	{
		if( inMarkerNo >= hdr()->markerCpt )
			return 0;
		// Markers follow the header
		Marker* markers = (Marker*)( hdr()+1 );
		return markers[inMarkerNo].nameCRC;
	}

	float
	GetMarkerTime		(	uint		inMarkerNo		)
	{
		if( inMarkerNo >= hdr()->markerCpt )
			return -1.0f;
		// Markers follow the header
		Marker* markers = (Marker*)( hdr()+1 );
		return markers[inMarkerNo].t;
	}

	int
	FindMarker			(	uint32		inMarkerName	)
	{
		if( inMarkerName == 0 )
			return -1;
		uint nb = GetMarkerCpt();
		for( uint i = 0 ; i < nb ; i++ )
			if( GetMarkerNameCRC(i) == inMarkerName )
				return i;
		return -1;
	}

	uint
	GetMode		(							)
	{
		return hdr()->mode;
	}

	bool					
	GetBFSoundData		(	uint32	&		outBOffset		,
							uint32	&		outBSize		)
	{
		outBOffset	= dataBOffset	;
		outBSize	= dataBSize		;
		return TRUE;
	} 

	void*					
	GetDSPADPCMHeader	(	uint32			inChannel = 0	)
	{
		if ( !IsMusic() && inChannel >0 )
			return NULL;
		if (inChannel > 1)
			return NULL;
				
		return (adpcm + inChannel);
	}

	bool					
	GetBFLoopData		(	uint32	&		outBOffset		,
							uint32	&		outBSize		,
							uint32			inChannel = 0	)
	{
		if ( !IsMusic() && inChannel > 0 )
			return FALSE;
		if (inChannel > 1)
			return FALSE;
		if (!IsStreamed())
			return FALSE;
		outBOffset	= dataBOffset + dataBSize +(perChannelLoopCtxSize * inChannel);
		outBSize	= perChannelLoopCtxSize;
		return TRUE;
	}
};





//
// FACTORY


namespace
{
	struct NvSoundFactory : public RscFactory_SHR
	{
		uint32	GetType	(	)
		{
			return NvSound::TYPE;
		}

		uint32
		GetToPrefetchRscBSize	(	uint32		inUID,
									uint32		inBSize		)
		{
			if( !inUID )
				return 0;
			// Read header + up to 256 markers + ADPCM Header!
			uint32 pbsize  = sizeof(NvSoundBase::Header);
				   pbsize += sizeof(NvSoundBase::Marker) * 256;
				   pbsize += sizeof(DSPADPCM) * 2 ; //(can be stereo)
			return Min( pbsize, inBSize );
		}

		bool
		PrefetchRsc		(	uint32		inUID,
							pvoid		inPtr,
							uint32		inBSize		)
		{
			// Overrides PrefetchRsc() to remove unused markers from memory
			// before the first instanciation !
			if( !RscFactory_SHR::PrefetchRsc(inUID,inPtr,inBSize) )
				return FALSE;	// failed !
			// Modify the prefeteched data ...
			Desc* desc = FindDesc( inUID );
			if( desc ) {
				NvSoundBase::Header* hdr = (NvSoundBase::Header*) desc->ptr;
				uint32 pbsize  = sizeof(NvSoundBase::Header);
				       pbsize += sizeof(NvSoundBase::Marker) * hdr->markerCpt;
				       pbsize += (hdr->mode != 3)?sizeof(DSPADPCM):0;
				       pbsize += (hdr->mode == 2)?sizeof(DSPADPCM):0;
				if( desc->bsize > pbsize ) {
					desc->bsize = pbsize;
					desc->ptr   = EngineRealloc( desc->ptr, desc->bsize );
					NV_ASSERT( desc->ptr );
				}
			}
			return TRUE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
								uint32		inBSize		)
		{
			NvSoundBase::Header * hdr = (NvSoundBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvSoundBase::Header)
					&&	hdr->ver == NV_SOUND_CVER					);
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_FULL;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NvSoundBase::Header * hdr = (NvSoundBase::Header*) inRscData;
			NvSoundBase * inst = NvEngineNew( NvSoundBase );
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			bool stereo   = (inst->hdr()->mode == 2);
			bool speaker  = (inst->hdr()->mode == 3);
			bool streamed = (inst->hdr()->mode >= 1 && inst->hdr()->mode != 3);
			// Get dataBOffset & dataBSize in the BigFile (minus the header bsize & markers)
			RscManager::RscInfo rsc;
			bool done = RscManager::GetRscInfo( inst->inst.uid, &rsc );
			if( !done || rsc.bsize < sizeof(NvSoundBase::Header) ) {
				// Bigfile close ?!
				NvEngineDelete( inst );
				return NULL;
			}
			
			uint nbSample 				= inst->hdr()->sampleLen;
			uint nbAdpcmFrame 			= (nbSample / 14)
			  							+ ((nbSample % 14)?1:0);
			uint hdrAndMarkers			=	sizeof(NvSoundBase::Header)  +
											sizeof(NvSoundBase::Marker) * inst->hdr()->markerCpt;
		
			inst->dataBSize    			= rsc.bsize;
			inst->dataBOffset  			= rsc.boffset[0];
			uint otherThanDataSize  	= hdrAndMarkers	 +
										  ( speaker ? 0 : (sizeof(DSPADPCM) << stereo));
										
			inst->dataBSize   			-= otherThanDataSize;
			inst->dataBOffset			+= otherThanDataSize;
			inst->perChannelLoopCtxSize	= streamed?(sizeof(NvSoundBase::LoopCtx) * nbAdpcmFrame):0;
			inst->dataBSize				-= inst->perChannelLoopCtxSize << stereo; 
			inst->adpcm 	 			= (DSPADPCM*)(uint32(hdr) + hdrAndMarkers);

			inst->Init();
			return inst;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvSoundBase* inst = (NvSoundBase*) inInst;
			return &inst->itf;
		}

		void
		ReleaseInstanceObject	(	Desc*		inDesc,
									Instance*	inInst		)
		{
			NvSoundBase* inst = (NvSoundBase*) inInst;
			NvEngineDelete( inst );
		}
	};
}



bool
rscfactory_NvSound_Reg()
{
	soundFactory = NvEngineNew( NvSoundFactory );
	return RscManager::RegisterFactory( soundFactory );
}



NvSound *
NvSound::Create	(	uint32	inUID	)
{
	if( !soundFactory )
		return NULL;
	return (NvSound*) soundFactory->CreateInstance( inUID );
}

//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvSound::TYPE = 0x9A77CC99;	// CRC("NvSound")

NVITF_MTH0(		Sound,	NvInterface *,	GetBase												)
NVITF_CAL0(		Sound,	void,			AddRef												)
NVITF_MTH0(		Sound,	uint,			GetRefCpt											)
NVITF_CAL0(		Sound,	void,			Release												)
NVITF_MTH0(		Sound,	uint32,			GetRscType											)
NVITF_MTH0(		Sound,	uint32,			GetRscUID											)
NVITF_MTH0(		Sound,	NvkSound*,		GetKInterface										)
NVITF_MTH0(		Sound,	float,			GetDuration											)
NVITF_MTH0(		Sound,	bool,			IsStreamed											)
NVITF_MTH0(		Sound,	bool,			IsLooped											)
NVITF_MTH0(		Sound,	bool,			IsMusic												)
NVITF_MTH0(		Sound,	uint,			GetMarkerCpt										)
NVITF_MTH1(		Sound,	uint32,			GetMarkerNameCRC,	uint							)
NVITF_MTH1(		Sound,	float,			GetMarkerTime,		uint							)
NVITF_MTH1(		Sound,	int,			FindMarker,			uint32							)

NVKITF_MTH0(	Sound,	NvInterface *,	GetBase												)
NVKITF_CAL0(	Sound,	void,			AddRef												)
NVKITF_MTH0(	Sound,	uint,			GetRefCpt											)
NVKITF_CAL0(	Sound,	void,			Release												)
NVKITF_MTH0(	Sound,	uint32,			GetRscType											)
NVKITF_MTH0(	Sound,	uint32,			GetRscUID											)
NVKITF_MTH0(	Sound,	NvSound*,		GetInterface										)
NVKITF_MTH0(	Sound,	uint,			GetFreq												)
NVKITF_MTH0(	Sound,	uint,			GetMode												)
NVKITF_MTH0(	Sound,	float,			GetDuration											)
NVKITF_MTH0(	Sound,	bool,			IsStreamed											)
NVKITF_MTH0(	Sound,	bool,			IsLooped											)
NVKITF_MTH0(	Sound,	bool,			IsMusic												)
NVKITF_MTH0(	Sound,	bool,			IsWMSpeaker											)
NVKITF_MTH2(	Sound,	bool,			GetBFSoundData,		uint32&,	uint32&				)
NVKITF_MTH3(	Sound,	bool,			GetBFLoopData ,		uint32&,	uint32&,	uint32	)
NVKITF_MTH1(	Sound,	void*,			GetDSPADPCMHeader,	uint32							)

