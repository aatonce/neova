/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvSurface.h>
#include <Kernel/NGC/NvkSurface[NGC].h>
#include <Kernel/Common/NvRscFactory_SHR.h>
#include <Kernel/NGC/GEK/GEK.h>

#include <Kernel/NGC/NvDpyManager[NGC].h>

//
// SURFACE BASE
struct NvSurfaceBase : NvInterface
{
	enum {
		FLG_EN_PRESENT		= NvkSurface::EN_PRESENT,
		FLG_EN_BEGIN_END	= NvkSurface::EN_BEGIN_END,
		FLG_EN_GENERATE_IDX	= NvkSurface::EN_GENERATE_IDX,
		//--
		FLG_EN_DBF			= (NvkSurface::EN_ALL+1),
		FLG_NEED_PRESENT	= (FLG_EN_DBF<<1),
		FLG_COPY_PRESENT	= (FLG_NEED_PRESENT<<1),		// otherwise, swap present
		FLG_BUILDING		= (FLG_COPY_PRESENT<<1)
	};

	uint				refCpt;
	NvSurface			itf;
	NvkSurface			kitf;
	
	uint16				maxSize;
	uint8				components;	
	uint8				flags;					// FLG_* bit-mask
	
	uint8*				backData;
	uint8*				frontData;
	
	uint16				buildPos;
	uint				buildLocStride;
	uint				buildTexStride;	
	uint				buildColStride;
	uint				buildNrmStride;
	uint8*				buildLocP;
	uint8*				buildTexP;
	uint8*				buildColP;
	uint8*				buildNrmP;			
	
	uint32*				buildKickP;
	
	uint32				drawFrameNo;

	uint				vtxFmtId;
	
	uint 				version;
		
	virtual	~	NvSurfaceBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)


	NvInterface*
	GetBase		(		)
	{
		return this;
	}

	void
	AddRef		(		)
	{
		refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return refCpt;
	}

	void
	Release		(		)
	{
		NV_ASSERT( refCpt );
		if( refCpt > 1 )
		{
			refCpt--;
		}
		else
		{
			NV_ASSERT( !IsDrawing() );		// Referenced in shader !!!

			// Safe to delete ?
			if( IsDrawing() )
			{
				DpyManager::AddToGarbager( this );
				return;
			}
			else
			{
				SafeFree( backData );
				SafeFree( frontData );
				NvEngineDelete( this );
			}
		}
	}

	uint32
	GetRscType		(		)
	{
		return NvSurface::TYPE;
	}

	uint32
	GetRscUID		(		)
	{
		return 0;
	}

	NvkSurface*
	GetKInterface	(		)
	{
		return &kitf;
	}

	NvSurface*
	GetInterface	(		)
	{
		return &itf;
	}

	uint
	GetComponents	(		)
	{
		return components;
	}

	bool
	HasComponent	(	NvSurface::Component	inComponent		)
	{
		return (components & uint(inComponent));
	}

	NvSurface::PackMode
	GetPackingMode	(		)
	{
		return NvSurface::PackMode( 0/*packMode */);
	}

	uint
	GetMaxSize		(		)
	{
		return maxSize;
	}

	bool
	PresentBuffer	(			)
	{
		flags |= FLG_NEED_PRESENT;
		return TRUE;
	}

	void
	DoPresentBuffer	(		)
	{
		if( !(flags&FLG_NEED_PRESENT) )		return;
		if( !(flags&FLG_EN_PRESENT) )		return;

		version ++;
		
		if( flags & FLG_EN_GENERATE_IDX )
			GenerateIdxFromKick( frontData ? NvkSurface::BF_FRONT : NvkSurface::BF_BACK );

		NV_ASSERT( backData );
		if( frontData ) {
			if( flags & FLG_COPY_PRESENT )
				Memcpy( backData, frontData, GetBufferBSize() );
			else
				Swap( backData, frontData );
		}

		flags &= ~(FLG_NEED_PRESENT|FLG_BUILDING);
	}

	uint8*
	AllocBuffer		(			)
	{
		uint bs = GetBufferBSize();
		if( !bs )
			return NULL;
		return (uint8*) EngineMallocA( bs, 32 );	// D$ aligned
	}

	void
	FreeBuffer		(	uint8*		inBuffer	)
	{
		SafeFree( inBuffer );
	}

	uint GetVtxBSize()
	{
		uint posBSize = vtxfmt::GetStride (	vtxfmt::Component(vtxFmtId & vtxfmt::CO_LOC_MASK) ) * maxSize;
		uint texBSize = vtxfmt::GetStride (	vtxfmt::Component(vtxFmtId & vtxfmt::CO_TEX_MASK) ) * maxSize;
		uint colBSize = vtxfmt::GetStride (	vtxfmt::Component(vtxFmtId & vtxfmt::CO_COL_MASK) ) * maxSize;
		uint nrmBSize = vtxfmt::GetStride (	vtxfmt::Component(vtxFmtId & vtxfmt::CO_NRM_MASK) ) * maxSize;
		
		return 		Round32(posBSize) 
				+ 	Round32(texBSize) 
				+ 	Round32(colBSize) 
				+ 	Round32(nrmBSize);
	}
	
	uint
	GetBufferBSize	(		)
	{
		uint vtxBSize  =  GetVtxBSize();
		bool haveKick  = (components & NvSurface::CO_KICK)!=0;		
		uint kickBSize = haveKick ? Round32(maxSize)/8 : 0;	// one bit per vertex
		uint idxBSize  = haveKick ? maxSize * 2 * 2 : 0;	// 16bits index, max 2 swap per vertex
		uint iltBSize  = haveKick ? maxSize * 2 : 0;		// 16bits index count per vertex
		uint bs = Round4(vtxBSize) + Round4(kickBSize) + Round4(idxBSize) + iltBSize;
		return Round32( bs );	// D$ aligned
	}

	uint8*
	GetBufferBase		(	NvkSurface::Buffer		inBuffer	)
	{
		return (inBuffer == NvkSurface::BF_BACK) ? backData : frontData;
	}

	uint8*
	GetSubBufferBase	(	NvkSurface::Buffer		inBuffer,
							NvkSurface::SubBuffer	inSubBuffer	)
	{
		uint8* base  	= GetBufferBase( inBuffer );
		if( !base )		return NULL;
		if( inSubBuffer == NvkSurface::SBF_VTX )	return base;
		if( !HasComponent(NvSurface::CO_KICK) )		return NULL;
		uint vtxBSize  = GetVtxBSize();
		uint kickBSize = Round32(maxSize)/8;
		uint idxBSize  = maxSize * 2 * 2;
		if( inSubBuffer == NvkSurface::SBF_KICK )	return base + Round4(vtxBSize);
		if( inSubBuffer == NvkSurface::SBF_IDX )	return base + Round4(vtxBSize) + Round4(kickBSize);
		if( inSubBuffer == NvkSurface::SBF_ILT )	return base + Round4(vtxBSize) + Round4(kickBSize) + Round4(idxBSize);
		return NULL;
	}

	uint8*
	GetComponentBase	(	NvkSurface::Buffer		inBuffer,
							NvSurface::Component	inComponent,
							uint*					outStride	= NULL,
							uint*					outPsm		= NULL		)
	{
		if (outPsm) 	*outPsm 	= 0;
		if (outStride) 	*outStride	= 0;
		
		if( !HasComponent(inComponent) )
			return NULL;

		if( inComponent == NvSurface::CO_KICK ) {
			if( outStride )		*outStride = 4;
			if( outPsm )		*outPsm    = 0;
			return GetSubBufferBase( inBuffer, NvkSurface::SBF_KICK );
		}
							
		uint8* base = GetSubBufferBase( inBuffer, NvkSurface::SBF_VTX );

		if( !base )		return NULL;
		
		if (outPsm) 
			*outPsm	= PSM_RGBA32;

		uint 	ptr = uint(base);
	
		uint posStride = vtxfmt::GetStride( vtxfmt::Component(vtxFmtId & vtxfmt::CO_LOC_MASK) );
		if (inComponent == NvSurface::CO_LOC) {
			if (outStride) *outStride = posStride;
			return (uint8*) ptr;
		}
		else 
			ptr += Round32(posStride * maxSize);
			
		uint texStride = vtxfmt::GetStride( vtxfmt::Component(vtxFmtId & vtxfmt::CO_TEX_MASK));
		if (inComponent == NvSurface::CO_TEX) {
			if (outStride) *outStride = texStride;
			return (uint8*) ptr;
		}
		else 
			ptr += Round32(texStride * maxSize);

		uint colStride = vtxfmt::GetStride( vtxfmt::Component(vtxFmtId & vtxfmt::CO_COL_MASK));
		if (inComponent == NvSurface::CO_COLOR) {
			if (outStride) *outStride = colStride ;
			return (uint8*) ptr;
		}
		else 
			ptr += Round32(colStride * maxSize);

		uint nrmStride = vtxfmt::GetStride( vtxfmt::Component(vtxFmtId & vtxfmt::CO_NRM_MASK));
		if (inComponent == NvSurface::CO_NORMAL) {
			if (outStride) *outStride = nrmStride;
			return (uint8*) ptr;
		}
			
		return NULL;
	}

	bool
	IsUptoDate	( uint * ioVersion )
	{
		if (!ioVersion ) return FALSE;
		bool ret = (version == *ioVersion);
		*ioVersion = version;
		return ret;
	}
	
	uint
	GetDisplayListBSize	( 	NvkSurface::Buffer		inBuffer	,
							uint					inOffset	,
							uint					inSize		)
	{
		if ( (inOffset+inSize) > maxSize || inSize < 3) 
			return 0;
		
		uint8 * vtxBase = (uint8*)  GetSubBufferBase( inBuffer, NvkSurface::SBF_VTX );
		uint16* idxBase = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_IDX );
		uint16* iltBase = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_ILT );
		
		if (!vtxBase) return 0;

		uint size = 0;

		size += 1; // uint8  : GX_DRAW_TRIANGLE_STRIP; // must be complet with | GX_VTXFMTX
		size += 2; // uint16 : nb vertex to draw
		
		uint16 nbVertexToRender = 0;
		
		uint nbComponent 	= 1;	// 1 for Location !		
		if (components & NvSurface::CO_TEX)
			nbComponent++;
		if (components & NvSurface::CO_COLOR)
			nbComponent++;
		if (components & NvSurface::CO_NORMAL)		
			nbComponent++;
		
		if (idxBase) {		// With Index Buffer
			uint idxBegin = iltBase[inOffset];
			uint idxEnd = iltBase[inOffset+inSize-1];
			nbVertexToRender = idxEnd - idxBegin + 1;
		}
		else {				// Without Index Buffer
			nbVertexToRender = inSize;
		}
		// Nb Vertex * NbVertex Component
		size += nbVertexToRender * nbComponent * 2;
		
		// the size must be a multiple of 32 bytes => padding 
		size = Round32(size);
			  		  	
		return size;
	}
	
	uint
	GetBufferDrawing	(	NvkSurface::Buffer		inBuffer	,
							uint					inOffset	,
							uint					inSize		,
							uint16  **				outDL		,
							int		*				outVtxFmtId	)
	{
		if (! outDL || (uint(*outDL) & 0x1F) || (inOffset+inSize) > maxSize || inSize < 3) 
			return 0;
		if (! outVtxFmtId ) 
			return 0;
		
		uint8 * vtxBase = (uint8*)  GetSubBufferBase( inBuffer, NvkSurface::SBF_VTX );
		uint16* idxBase = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_IDX );
		uint16* iltBase = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_ILT );
		
		if (!vtxBase) return 0;
		
		uint8  * dl8  	= (uint8 *) *outDL;
		uint16 * dl16 	= (uint16 *)(dl8+1);
		
		// Init DL Header
		*dl8 = GX_DRAW_TRIANGLE_STRIP; // must be complet with | GX_VTXFMTX
		uint16 * nbVertexToRender = dl16;
		dl16 ++ ;
		
		uint nbComponent 	= 1;	// 1 for Location !		
		if (components & NvSurface::CO_TEX)
			nbComponent++;
		if (components & NvSurface::CO_COLOR)
			nbComponent++;
		if (components & NvSurface::CO_NORMAL)		
			nbComponent++;
		
		if (idxBase) {		// With Index Buffer
			uint idxBegin = iltBase[inOffset];
			uint idxEnd = iltBase[inOffset+inSize-1];
			*nbVertexToRender = idxEnd - idxBegin + 1;
			for (uint idx = idxBegin ; idx <= idxEnd ; ++idx ) {				
				for (uint i = 0 ; i < nbComponent ; ++i ) {
					*dl16 = idxBase[idx];
					dl16++;
				}
			}
		}
		else {				// Without Index Buffer
			*nbVertexToRender = inSize;
			for (uint16 vtx = inOffset; vtx < inSize ; ++vtx ) {
				for (uint i = 0 ; i < nbComponent ; ++i ) {
					*dl16 = vtx;
					dl16++;		
				}
			}
		}
		// the size must be a multiple of 32 bytes => padding 
		dl8 = (uint8*)dl16;
		while (uint(dl8) & 0x1F) {
			*dl8 = GX_NOP;
			dl8++;
		}		
		*outVtxFmtId  	= vtxFmtId;				
	  	uint size = uint(dl8) - uint(*outDL);
	  	DCFlushRange(*outDL,size);
	  	DCFlushRange(vtxBase,GetBufferBSize() );
	  	
		return size;
	}

	void
	GenerateIdxFromKick	(	NvkSurface::Buffer		inBuffer	)
	{
		uint32* kickBase = (uint32*) GetSubBufferBase( inBuffer, NvkSurface::SBF_KICK );
		uint16* idxBase  = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_IDX );
		uint16* iltBase  = (uint16*) GetSubBufferBase( inBuffer, NvkSurface::SBF_ILT );
		if( !kickBase )
			return;
		uint idxCount = 0;
		for( uint i = 0 ; i < maxSize ; i++ ) {
			uint kick = kickBase[i>>5] & (1U<<(i&31));
			if( !kick && i>1 )
				idxBase[idxCount++] = i-1;
			iltBase[i] = idxCount;
			idxBase[idxCount++] = i;
		}
	}

	bool
	Resize			(	uint		inMaxSize		)
	{
		if( IsDrawing() || inMaxSize < 3 )
			return FALSE;
		bool hasBack  = (backData  != NULL);
		bool hasFront = (frontData != NULL);
		SafeFree( backData );
		SafeFree( frontData );
		maxSize		= inMaxSize;
		backData	= hasBack  ? AllocBuffer() : NULL;
		frontData	= hasFront ? AllocBuffer() : NULL;
		flags	   &= ~(FLG_NEED_PRESENT|FLG_BUILDING);
		NV_ASSERT( !hasBack  || backData  );
		NV_ASSERT( !hasFront || frontData );
		return TRUE;
	}

	void
	EnableDBF		(	NvSurface::PresentMode	inPresentMode,
						bool					inCreateNow		)
	{
		if( inCreateNow && !frontData ) {
			frontData	= AllocBuffer();
			NV_ASSERT( frontData );
		}
		flags |=  FLG_EN_DBF;
		if( inPresentMode==NvSurface::SM_COPY )		flags |=  FLG_COPY_PRESENT;
		else										flags &= ~FLG_COPY_PRESENT;
	}

	void
	DisableDBF		(	bool		inReleaseNow	)
	{
		if( inReleaseNow ) {
			DoPresentBuffer();
			SafeFree( frontData );
		}
		flags &= ~FLG_EN_DBF;
	}

	bool
	IsEnabledDBF	(		)
	{
		return (flags & FLG_EN_DBF);
	}

	bool
	HasDBF			(		)
	{
		return (backData && frontData);
	}

	void
	Enable		(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		flags |= inEnableFlags;
	}

	void
	Disable		(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		flags &= ~inEnableFlags;
		if( !(flags&FLG_EN_BEGIN_END) )
			flags &= ~FLG_BUILDING;
	}

	bool
	IsEnabled	(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		return ((flags & inEnableFlags) == inEnableFlags );
	}

	bool
	Begin			(	uint		inStartOffset			)
	{
		if( !(flags&FLG_EN_BEGIN_END) )		return FALSE;
		if( (flags&FLG_BUILDING) )			return FALSE;
		if( inStartOffset>=maxSize )		return FALSE;

		// Choose the buffer to write : front or back ?
		NvkSurface::Buffer buildBuffer;
		if( !frontData && !IsDrawing() ) {
			// Use back
			NV_ASSERT( backData );
			buildBuffer = NvkSurface::BF_BACK;
		} else {
			// Use front
			if( !(flags&FLG_EN_DBF) )	return FALSE;
			if( !frontData )
				frontData = AllocBuffer();
			NV_ASSERT( frontData );
			if( !frontData )			return FALSE;
			buildBuffer = NvkSurface::BF_FRONT;
		}

		buildKickP	= (uint32*) GetSubBufferBase( buildBuffer, NvkSurface::SBF_KICK ) + (0);
		buildPos	= inStartOffset;
		
		buildLocP =	GetComponentBase	(buildBuffer,NvSurface::CO_LOC,&buildLocStride);
		buildLocP += inStartOffset*	buildLocStride;
		buildTexP =	GetComponentBase	(buildBuffer,NvSurface::CO_TEX,&buildTexStride);
		buildTexP += inStartOffset*	buildTexStride;
		buildColP =	GetComponentBase	(buildBuffer,NvSurface::CO_COLOR,&buildColStride);
		buildColP += inStartOffset*	buildColStride;
		buildNrmP =	GetComponentBase	(buildBuffer,NvSurface::CO_NORMAL,&buildNrmStride);
		buildNrmP += inStartOffset*	buildNrmStride ;
		
		flags |= FLG_BUILDING;
		return TRUE;
	}

	void
	TexCoord		(	const Vec2&		inST		)
	{
		if( !(flags&FLG_BUILDING) )					return;
		if( !HasComponent(NvSurface::CO_TEX) )		return;
		if( buildPos>=maxSize )						return;
		Vec2Copy( (Vec2*)(buildTexP), &inST );
	}

	void
	Color			(	uint32			inColor,
						Psm				inPSM	)
	{
		if( !(flags&FLG_BUILDING) )					return;
		if( !HasComponent(NvSurface::CO_COLOR) )	return;
		if( buildPos>=maxSize )						return;
		if( inPSM != PSM_RGBA32 )
			inColor = ConvertPSM( PSM_RGBA32, inPSM, inColor );
		*((uint32*)(buildColP)) = inColor;
	}

	void
	Normal			(	const Vec3&		inNXYZ		)
	{
		if( !(flags&FLG_BUILDING) )					return;
		if( !HasComponent(NvSurface::CO_NORMAL) )	return;
		if( buildPos>=maxSize )						return;
		Vec3Copy( (Vec3*)(buildNrmP), &inNXYZ );
	}

	void
	Vertex			(	const Vec3&		inXYZ,
						bool			inKick		)
	{
		if( !(flags&FLG_BUILDING) )					return;
		if( !HasComponent(NvSurface::CO_LOC) )		return;
		if( buildPos>=maxSize )						return;
		Vec3Copy( (Vec3*)(buildLocP), &inXYZ );
		if( buildKickP ) {
			uint i32 = buildPos>>5;
			uint m32 = 1U << (buildPos&31);
			if( inKick )	buildKickP[i32] |=  m32;
			else			buildKickP[i32] &= ~m32;
		}
		buildPos++;
		buildLocP += buildLocStride	;
		buildTexP += buildTexStride	;
		buildColP += buildColStride	;
		buildNrmP += buildNrmStride	;
		
	}

	void
	End				(			)
	{
		if( !(flags&FLG_BUILDING) )		return;
		flags &= ~FLG_BUILDING;
		PresentBuffer();
	}

	void
	UpdateBeforeDraw	(		)
	{
		NV_ASSERT( refCpt > 0 );
		drawFrameNo = DpyManager::GetFrameNo();
		DoPresentBuffer();
		// release front ?
		if( frontData && !(flags&FLG_EN_DBF) )
			DisableDBF( TRUE );
	}

	bool
	IsDrawing	(		)
	{
		return DpyManager::IsDrawing( drawFrameNo );
		return FALSE;
	}
};






NvSurface*
NvSurface::Create	(	uint			inMaxSize,
						uint			inComponents,
						PackMode		inPackMode	)
{
	if( inMaxSize<3 || (inComponents&CO_LOC)==0 )
		return NULL;

	// Force not packed !
	inPackMode = PM_NONE;


	NvSurfaceBase* inst = NvEngineNew( NvSurfaceBase );
	if( !inst )
		return NULL;

	uint posType = (uint)(vtxfmt::CO_LOC_F32);
	uint texType = (uint)((inComponents & NvSurface::CO_TEX    )? vtxfmt::CO_TEX_F32 : 0);
	uint colType = (uint)((inComponents & NvSurface::CO_COLOR  )? vtxfmt::CO_COLOR	: 0);
	uint nrmType = (uint)((inComponents & NvSurface::CO_NORMAL )? vtxfmt::CO_NORMAL_F32 : 0);
	
	inst->refCpt		= 1;
	inst->maxSize		= inMaxSize;
	inst->components	= inComponents;
	inst->flags			= NvkSurface::EN_DEFAULT;
	inst->backData		= NULL;
	inst->frontData		= NULL;
	inst->buildPos		= 0;
	inst->buildLocStride= 0;
	inst->buildTexStride= 0;	
	inst->buildColStride= 0;
	inst->buildNrmStride= 0;
	inst->buildLocP		= NULL;
	inst->buildTexP		= NULL;
	inst->buildColP		= NULL;
	inst->buildNrmP		= NULL;			
	inst->buildKickP	= NULL;
	inst->drawFrameNo	= ~0U;
	inst->vtxFmtId 		= posType | texType  | colType | nrmType;
	inst->version 		= 0;
	
	inst->backData		= inst->AllocBuffer();
	if( !inst->backData ) {
		NvEngineDelete( inst );
		return NULL;
	}
	

		
	return &(inst->itf);
}




//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvSurface::TYPE = 0x7D788970;	// CRC("NvSurface")


NVITF_MTH0(		Surface,	NvInterface*,			GetBase										)
NVITF_CAL0(		Surface,	void,					AddRef										)
NVITF_MTH0(		Surface,	uint,					GetRefCpt									)
NVITF_CAL0(		Surface,	void,					Release										)
NVITF_MTH0(		Surface,	uint32,					GetRscType									)
NVITF_MTH0(		Surface,	uint32,					GetRscUID									)
NVITF_MTH0(		Surface,	NvkSurface*,			GetKInterface								)
NVITF_MTH0(		Surface,	uint,					GetComponents								)
NVITF_MTH1(		Surface,	bool,					HasComponent,		NvSurface::Component	)
NVITF_MTH0(		Surface,	NvSurface::PackMode,	GetPackingMode								)
NVITF_MTH0(		Surface,	uint,					GetMaxSize									)
NVITF_MTH1(		Surface,	bool,					Resize,				uint					)
NVITF_CAL2(		Surface,	void,					EnableDBF,			NvSurface::PresentMode,
																		bool					)
NVITF_CAL1(		Surface,	void,					DisableDBF,			bool					)
NVITF_MTH0(		Surface,	bool,					IsEnabledDBF								)
NVITF_MTH0(		Surface,	bool,					HasDBF										)
NVITF_MTH1(		Surface,	bool,					Begin,				uint					)
NVITF_CAL1(		Surface,	void,					TexCoord,			const Vec2&				)
NVITF_CAL2(		Surface,	void,					Color,				uint32, Psm				)
NVITF_CAL1(		Surface,	void,					Normal,				const Vec3&				)
NVITF_CAL2(		Surface,	void,					Vertex,				const Vec3&, bool		)
NVITF_CAL0(		Surface,	void,					End											)

NVKITF_MTH0(	Surface,	NvInterface*,			GetBase										)
NVKITF_CAL0(	Surface,	void,					AddRef										)
NVKITF_MTH0(	Surface,	uint,					GetRefCpt									)
NVKITF_CAL0(	Surface,	void,					Release										)
NVKITF_MTH0(	Surface,	uint32,					GetRscType									)
NVKITF_MTH0(	Surface,	uint32,					GetRscUID									)
NVKITF_MTH0(	Surface,	NvSurface*,				GetInterface								)
NVKITF_MTH0(	Surface,	uint,					GetComponents								)
NVKITF_MTH1(	Surface,	bool,					HasComponent,		NvSurface::Component	)
NVKITF_MTH0(	Surface,	NvSurface::PackMode,	GetPackingMode								)
NVKITF_MTH0(	Surface,	uint,					GetMaxSize									)
NVKITF_MTH1(	Surface,	bool,					Resize,				uint					)
NVKITF_CAL2(	Surface,	void,					EnableDBF,			NvSurface::PresentMode,
																		bool					)
NVKITF_CAL1(	Surface,	void,					DisableDBF,			bool					)
NVKITF_MTH0(	Surface,	bool,					IsEnabledDBF								)
NVKITF_MTH0(	Surface,	bool,					HasDBF										)
NVKITF_CAL1(	Surface,	void,					Enable,				uint					)
NVKITF_CAL1(	Surface,	void,					Disable,			uint					)
NVKITF_MTH1(	Surface,	bool,					IsEnabled,			uint					)
NVKITF_MTH0(	Surface,	bool,					PresentBuffer								)
NVKITF_MTH0(	Surface,	uint8*,					AllocBuffer									)
NVKITF_CAL1(	Surface,	void,					FreeBuffer,			uint8*					)
NVKITF_MTH0(	Surface,	uint,					GetBufferBSize								)
NVKITF_MTH1(	Surface,	uint8*,					GetBufferBase,		NvkSurface::Buffer		)
NVKITF_MTH2(	Surface,	uint8*,					GetSubBufferBase,	NvkSurface::Buffer,
																		NvkSurface::SubBuffer	)
NVKITF_MTH4(	Surface,	uint8*,					GetComponentBase, 	NvkSurface::Buffer,
																		NvSurface::Component,
																		uint*, uint*			)
NVKITF_MTH5(	Surface,	uint,					GetBufferDrawing, 	NvkSurface::Buffer,
																		uint,uint,uint16  **	,
																		int		*				)																		
NVKITF_MTH1(	Surface,	bool,					IsUptoDate,			uint *					)
NVKITF_MTH3(	Surface,	uint,					GetDisplayListBSize,NvkSurface::Buffer,
																		uint,uint				)																		
NVKITF_CAL0(	Surface,	void,					UpdateBeforeDraw							)
NVKITF_MTH0(	Surface,	bool,					IsDrawing									)


Psm
NvSurface::GetNativeColorPSM	(		)
{
	return PSM_RGBA32;
}


Psm
NvkSurface::GetNativeColorPSM	(		)
{
	return PSM_RGBA32;
}



