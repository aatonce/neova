/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/NGC/NvDpyManager[NGC].h>
#include <Kernel/NGC/NvkMesh[NGC].h>
#include <Kernel/Common/NvRscFactory_SHR.h>
#include <NvBitmap.h>
#include <NvMorphTarget.h>
using namespace nv;



//#define		DISABLE_LOCSKINNING
//#define		DISABLE_NRMSKINNING
//#define		DISABLE_SHDSKINNING
//#define		DISABLE_MORPHING
//#define		FORCE_NRMSKINNING
//#define		ENABLE_APPLY_BENCHING





namespace
{

	RscFactory*		meshFactory = NULL;

}



//
// BASE


struct NvMeshBase : public RscFactory_SHR::Instance
{

	#include <Projects\Plugin NovaNGC\comp_NvMesh_format.h>


	struct MorphSlot {
		NvMorphTarget*		target;
		float				weight;
		float				cur_weight;
	};
	
	NvMesh					itf;
	NvkMesh					kitf;

	nv::vector<MorphSlot>	morphSlotA;
	Vec3A					morphCacheA;
	bool					morphCacheValidity;
	uint					morphUnprocFrameCpt;
	

	NvkMesh::Vtx			vtx;
	NvBitmap**				lockBitmapA;		// NvBitmap* for each surface
	Matrix**				locBlendMap;		// loc blend mapping
	Matrix**				nrmBlendMap;		// nrm blend mapping
	uint					enflags;			// enabled flags
	uint					validflags;			// valid flags
	uint					updflags;			// toupdate flags
	uint32					drawFrameNo;


	virtual	~	NvMeshBase	(	) {}			// needed by nv::DestructInPlace<T>(ptr)


	Header*
	hdr			(		)
	{
		return (Header*)inst.dataptr;
	}


	void
	Init		(		)
	{
		enflags	 = NvMesh::EN_DEFAULT;
		#ifdef FORCE_NRMSKINNING
		enflags |= NvMesh::EN_NRM_SKINNING;
		#endif

		validflags	= 0;
		updflags	= 0;
		drawFrameNo = ~0U;
		
		morphCacheValidity	= FALSE;
	}


	void
	Shut		(		)
	{
		UnrefBitmaps();
		UnmapSkeleton();
		SetMorphSlotCpt(0);
	}


	void
	AddRef		(		)
	{
		inst.refCpt++;
	}


	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}


	void
	Release			(		)
	{
		NV_ASSERT( inst.refCpt );
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			NV_ASSERT( !IsDrawing() );		// Referenced in shader !!!
			NV_ASSERT( meshFactory );
			meshFactory->FreeInstance( &itf );
		}
	}


	uint32
	GetRscType	(		)
	{
		return NvMesh::TYPE;
	}


	uint32
	GetRscUID	(		)
	{
		return inst.uid;
	}


	NvMesh*
	GetInterface		(		)
	{
		return &itf;
	}


	NvkMesh*
	GetKInterface		(		)
	{
		return &kitf;
	}


	Box3*
	GetBBox				(		)
	{
		return &hdr()->bbox;
	}


	uint32
	GetNameCRC			(		)
	{
		return hdr()->nameCRC;
	}


	bool
	IsSkinnable			(		)
	{
		return hdr()->skinnable;
	}


	bool
	IsNrmSkinnable			(		)
	{
		return hdr()->nrmskinnable;
	}


	bool
	IsMorphable			(		)
	{
		return hdr()->morphable;
	}


	bool
	IsLightable			(		)
	{
		return hdr()->lightable;
	}


	bool
	IsShadowing		(	)
	{
		return hdr()->shadowing;
	}


	bool
	IsFullOpaque		(		)
	{
		return hdr()->fullopaque;
	}


	void
	Enable		(	uint32		inEnableFlags	)
	{
		enflags |= inEnableFlags;
	}


	void
	Disable		(	uint32		inEnableFlags	)
	{
		enflags &= ~inEnableFlags;
	}


	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
		enflags = inEnableFlags;
	}


	uint32
	GetEnabled		(		)
	{
		return enflags;
	}


	bool
	MapSkeleton			(	uint			inBoneCount,
							uint32*			inBoneNameA,
							Matrix*			inBlend16A		)
	{	
		UnmapSkeleton();

		if(		!IsSkinnable()
			||	!inBoneNameA
			||	!inBoneCount
			||	!inBlend16A		)
			return FALSE;

		// location's blend mapping
		if( hdr()->skinnable )
		{
			NV_ASSERT( hdr()->locProcess );
			NV_ASSERT( locBlendMap );
			for( uint i=0 ; i<hdr()->locProcess->boneCpt ; i++ ) {
				uint32 bone_crc   = hdr()->locProcess->boneList[i];
				bool   bone_found = FALSE;
				for( uint j=0 ; j<inBoneCount ; j++ ) {
					if( inBoneNameA[j]==bone_crc ) {
						locBlendMap[i] = inBlend16A + j;
						bone_found = TRUE;
						break;
					}
				}
				if( !bone_found )
					return FALSE;
			}
		}

		// normal's blend mapping
		if( hdr()->nrmskinnable )
		{
			NV_ASSERT( hdr()->nrmProcess );
			NV_ASSERT( nrmBlendMap );
			for( uint i=0 ; i<hdr()->nrmProcess->boneCpt ; i++ ) {
				uint32 bone_crc   = hdr()->nrmProcess->boneList[i];
				bool   bone_found = FALSE;
				for( uint j=0 ; j<inBoneCount ; j++ ) {
					if( inBoneNameA[j]==bone_crc ) {
						nrmBlendMap[i] = inBlend16A + j;
						bone_found = TRUE;
						break;
					}
				}
				if( !bone_found )
					return FALSE;
			}
		}

		// enables skinning
		validflags |= NvMesh::EN_LOC_SKINNING;
		validflags |= hdr()->nrmskinnable ? NvMesh::EN_NRM_SKINNING : 0;
		return TRUE;
	}


	void
	UnmapSkeleton		(									)
	{
		validflags &= ~( NvMesh::EN_LOC_SKINNING | NvMesh::EN_NRM_SKINNING | NvMesh::EN_SHD_SKINNING );	// disables all skinning
	}


	uint
	GetMorphSlotCpt		(									)
	{
		return morphSlotA.size();
	}


	void
	SetMorphSlotCpt		(	uint			inSize			)
	{
		if( !IsMorphable() )
			return;
	
		uint N = morphSlotA.size();
	
		if( N > inSize )
		{
			// Release erased slots
			for( uint i = inSize ; i < N ; i++ )
				SafeRelease( morphSlotA[i].target );
			morphCacheValidity = FALSE;
			if( inSize == 0 ) {
				morphCacheA.free();
				validflags &= ~(NvMesh::EN_MORPHING);		// disables morphing
			}
		}
		else
		{
			MorphSlot defSlot;
			defSlot.target		= NULL;
			defSlot.weight		= 0.0f;
			defSlot.cur_weight	= 0.0f;
			morphSlotA.resize( inSize, defSlot );
		}
	}


	NvMorphTarget*
	GetMorphSlotTarget	(	uint			inSlotNo		)
	{
		if( !IsMorphable() || inSlotNo >= morphSlotA.size() )
			return NULL;
		else
			return morphSlotA[inSlotNo].target;
	}


	void
	SetMorphSlotTarget	(	uint			inSlotNo,
							NvMorphTarget*	inTarget		)
	{
		#ifdef DISABLE_MORPHING
			return;
		#endif
		if(	!IsMorphable() || inSlotNo >= morphSlotA.size() )
			return;

		if( morphSlotA[inSlotNo].target == inTarget )
			return;
	
		if( morphSlotA[inSlotNo].target ) {
			SafeRelease( morphSlotA[inSlotNo].target );
			if( morphSlotA[inSlotNo].cur_weight != 0.0f )
				morphCacheValidity = FALSE;
		}

		if( inTarget ) {
			inTarget->AddRef();
			morphSlotA[inSlotNo].target		= inTarget;
			morphSlotA[inSlotNo].weight		= 0.0f;
			morphSlotA[inSlotNo].cur_weight	= 0.0f;
			validflags |= NvMesh::EN_MORPHING;
		}
	}


	void
	SetMorphSlotWeight	(	uint			inSlotNo,
							float			inWeight		)
	{
		if(		!IsMorphable()
			||	inSlotNo >= morphSlotA.size() )
			return;
		morphSlotA[inSlotNo].weight = inWeight;
	}


	uint
	GetSurfaceCpt		(			) 
	{
		return hdr()->surfCpt;
	}


	NvkMesh::Shading*
	GetSurfaceShadingA	(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(Shading) == sizeof(NvkMesh::Shading) );
		return (NvkMesh::Shading*) hdr()->shadA;
	}


	uint32
	GetSurfaceNameCRC	(	uint		inNo		)
	{
		if( inNo >= hdr()->surfCpt )
			return 0;
		Shading* shad = hdr()->shadA + inNo;
		return shad->nameCRC;
	}


	uint32
	GetSurfaceBitmapUID	(	uint		inNo		)
	{
		if( inNo >= hdr()->surfCpt )
			return 0;
		Shading* shad = hdr()->shadA + inNo;
		return shad->bitmapUID;
	}


	uint
	GetBunchCpt			(			)
	{
		return hdr()->surfCpt;
	}


	NvkMesh::Bunch*
	GetBunchA			(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(Bunch) == sizeof(NvkMesh::Bunch) );
		return (NvkMesh::Bunch*) hdr()->bunchA;
	}


	NvkMesh::Vtx*
	GetVtx				(			)
	{
		return &vtx;
	}


	void
	RefBitmaps	(			)
	{
		if( !lockBitmapA )
			return;
		Shading* shadA   = hdr()->shadA;
		for( uint i = 0 ; i < hdr()->surfCpt ; i++ ) {
			if( !lockBitmapA[i] )
				lockBitmapA[i] = NvBitmap::Create( shadA[i].bitmapUID );
		}
	}

	void
	UnrefBitmaps	(			)
	{
		if( !lockBitmapA )
			return;
		for( uint i = 0 ; i < hdr()->surfCpt ; i++ )
			SafeRelease( lockBitmapA[i] );
	}


	void
	Update				(									)
	{
		// save flags to update before drawing ...
		updflags = enflags;
	}

	bool
	_ProcessMorphing	(				)
	{
	#ifdef DISABLE_MORPHING
		return false;
	#endif
		uint locCpt = hdr()->locProcess->srcCptMorph;
		NV_ASSERT( locCpt );
	
		// No morphing to do
		uint morphCpt = morphSlotA.size();
		if( morphCpt == 0 )
		{
			if( morphCacheA.size() )
				morphCacheA.free();
			morphCacheValidity = FALSE;
			return FALSE;
		}
	
		if( !morphCacheValidity )
		{
			if( morphCacheA.size() != locCpt )
			{
				morphCacheA.reserve( locCpt );
				morphCacheA.resize( locCpt );
			}
	
			NV_ASSERT( hdr()->locProcess && hdr()->locProcess->srcListMorph );
			Vec3* locA = hdr()->locProcess->srcListMorph;
			Memcpy( &morphCacheA[0], locA, sizeof(Vec3)*locCpt );
	
			for( uint i = 0 ; i < morphCpt ; i++ )
				morphSlotA[i].cur_weight = 0.0f;
	
			morphCacheValidity = TRUE;
		}
	
		bool				oneDone = FALSE;
		Matrix			  &	M0		= hdr()->M0;
		Matrix			   wM0;
		float				w, cur_w;
		NvMorphTarget*		targetP;
		Vec3*				morphCacheP = morphCacheA.data();
		Vec3				v, * d;
	
		for( uint i = 0 ; i < morphCpt ; i++ )
		{
			targetP = morphSlotA[i].target;
			w		= morphSlotA[i].weight;
			cur_w	= morphSlotA[i].cur_weight;
	
			if(	!targetP )
				continue;
	
			// Need delta-update ?
			if( w != cur_w )
			{
				float dw = w - cur_w;
				morphSlotA[i].cur_weight = w;
	
				// Weighted TR0
				wM0 = M0 * dw;
	
				NvMorphTarget::VtxTarget * vtxTargetP;
				NvMorphTarget::VtxTarget * vtxTarget_endP;
				vtxTargetP		= targetP->GetVtxTargets();
				vtxTarget_endP	= vtxTargetP + targetP->GetVtxCpt();
				NV_ASSERT( vtxTargetP );

				while( vtxTargetP != vtxTarget_endP )
				{
					d = morphCacheP + vtxTargetP->index;
					Vec3BlendVector( d, &vtxTargetP->delta, &wM0 );
					vtxTargetP++;
				}
			}
	
			if( w != 0.0f )
				oneDone = TRUE;
		}

		morphUnprocFrameCpt = 0;

		return oneDone;
		return FALSE;
	}
	
	bool
	_ProcessLocSkinning	( int16 * srcLoc , uint srcLocBStride , uint8* srcW , uint srcWBStride	)
	{
		NV_ASSERT( hdr()->locFrac );
		float		packf = (1 << hdr()->locFrac);
		float		upackf= 1.f / packf;
		float		upackw= 1.f / 255.f;

		int16 *		src0 		= srcLoc;
		uint		src0Stride 	= (srcLocBStride>>1);
		uint8 *		w0	 		= srcW;
		
		uint16*		cmd0 = hdr()->locProcess->cmdList;
		uint16*		cmd1 = cmd0 + hdr()->locProcess->cmdCpt;
		uint16*		dst0 = (uint16*)vtx.compAddr[0];
		Matrix**	mat0 = locBlendMap;
		Matrix*		mc[4];

		while( cmd0 != cmd1 )
		{
			uint cmd = *cmd0++;

			uint toload_cpt = (cmd>>12) & 15;
			while( toload_cpt-- ) {
				mc[3] = mc[2];
				mc[2] = mc[1];
				mc[1] = mc[0];
				mc[0] = *mat0++;
			}

			uint cfg = (cmd>>8) & 15;
			uint nb  = (cmd & 255);
			Vec3  s, d;
			float w;
			uint  wi;
			while( nb-- ) {
				if( cfg ) {
					s.x = float(src0[0])    * upackf;
					s.y = float(src0[1])    * upackf;
					s.z = float(src0[2])    * upackf;
					wi  = 0;
					NV_ASSERT( cfg&1 );

					w = float(w0[wi++]) * upackw;
					Vec3Apply( &d, &s, mc[0], w );

					if( cfg&2 ) {
						w = float(w0[wi++]) * upackw;
						Vec3Blend( &d, &s, mc[1], w );
					}

					if( cfg&4 ) {
						w = float(w0[wi++]) * upackw;
						Vec3Blend( &d, &s, mc[2], w );
					}

					if( cfg&8 ) {
						w = float(w0[wi++]) * upackw;
						Vec3Blend( &d, &s, mc[3], w );
					}

					dst0[0] = d.x * packf;
					dst0[1] = d.y * packf;
					dst0[2] = d.z * packf;
				}
				w0 	 += srcWBStride;
				src0 += src0Stride;
				dst0 += 3;
			}
		}
		NV_ASSERT( mat0 == (locBlendMap+hdr()->locProcess->boneCpt) );
		SyncDCache( vtx.compAddr[0], hdr()->compBSize[0] );
		return TRUE;
	}
	
	bool
	_ProcessNrmSkinning	(				)
	{
		float		packf = 255.f;
		float		upackf= 1.f / packf;
		float		upackw= 1.f / 255.f;

		NrmSrc*		src0 = hdr()->nrmProcess->srcList;
		uint16*		cmd0 = hdr()->nrmProcess->cmdList;
		uint16*		cmd1 = cmd0 + hdr()->nrmProcess->cmdCpt;
		uint8*		dst0 = (uint8*)vtx.compAddr[3];
		Matrix**	mapP = nrmBlendMap;
		Matrix*		mat  = NULL;

		while( cmd0 != cmd1 )
		{
			uint cmd = *cmd0++;

			uint toload_cpt = (cmd>>12) & 15;
			if( toload_cpt ) {
				NV_ASSERT( toload_cpt == 1 );
				mat = *mapP++;
			}

			uint cfg = (cmd>>8) & 15;
			uint nb  = (cmd & 255);
			Vec3  s, d;
			while( nb-- ) {
				if( cfg ) {
					NV_ASSERT( cfg == 1 );
					s.x = float(src0->x) * upackf;
					s.y = float(src0->y) * upackf;
					s.z = float(src0->z) * upackf;
					Vec3ApplyVector( &d, &s, mat );
					dst0[0] = d.x * packf;
					dst0[1] = d.y * packf;
					dst0[2] = d.z * packf;
				}
				src0 += 1;
				dst0 += 3;
			}
		}
		SyncDCache( vtx.compAddr[3], hdr()->compBSize[3] );	
		return TRUE;
	}

	void
	UpdateBeforeDraw	(		)
	{
		// update todo flags with current state of the NvMesh ...
		if (inst.uid==681)
		{
			
			int aaaa = 0;
		}
		updflags &= validflags;
		if( updflags == 0 )
			return;
	
		if( !(updflags & NvMesh::EN_MORPHING) && morphCacheValidity ) {
			if( morphSlotA.size()==0 || morphUnprocFrameCpt>120 ) {
				morphCacheA.free();
				morphCacheValidity = FALSE;			
				validflags &= ~(NvMesh::EN_MORPHING);		// disables morphing
				updflags &= validflags;
			}
			morphUnprocFrameCpt++;
		}
			
		if( updflags == 0 )
			return;
		
		
		bool	morphed = FALSE;
		if( updflags & NvMesh::EN_MORPHING )
		{
			if( _ProcessMorphing() ) {
				NV_ASSERT( morphCacheA.size() == hdr()->locProcess->srcCptMorph );
				NV_ASSERT( morphCacheValidity == TRUE );
				morphed = TRUE;
			}
		}


		if ( hdr()->morphable )		// update VB buffer for drawing or skinning
		{
			float 	packf 	= (1 << hdr()->locFrac);
			int16 *	dst 	= (int16*)vtx.compAddr[0];
			Vec3  * src 	= NULL;
			uint 	cpt		= 0;
				
			if (morphed) 
				src = morphCacheA.data();
			else
				src = hdr()->locProcess->srcListMorph;
			
			if ( hdr()->skinnable ) 
			{
				cpt = hdr()->locProcess->locTrCpt;
				NV_ASSERT(hdr()->compBSize[0] == cpt * 6);
				int32 * locTr = hdr()->locProcess->locTr;
				for (uint i = 0 ; i < cpt ; ++i)
				{
					dst[0] = src[locTr[i]].x * packf;
					dst[1] = src[locTr[i]].y * packf;
					dst[2] = src[locTr[i]].z * packf;
					
					dst+=3;
				}
			}
			else
			{
				cpt = hdr()->locProcess->srcCptMorph;
				NV_ASSERT(hdr()->compBSize[0] == cpt * 6);
				for (uint i = 0 ; i < cpt ; ++i)
				{
					dst[0] = src[i].x * packf;
					dst[1] = src[i].y * packf;
					dst[2] = src[i].z * packf;
					
					dst+=3;
				}
			}
			SyncDCache( vtx.compAddr[0], (cpt * 6 ));
		}
	

		//
		// Loc Process ?

		if( updflags & NvMesh::EN_LOC_SKINNING )
		{
			int16 * src;
			uint8 *	w;
			uint	srcbStride;
			uint	wbStride;
			
			if ( hdr()->morphable )
			{
				src 		= (int16*)vtx.compAddr[0];
				srcbStride	= 3*sizeof(int16);
				w			= (uint8*)hdr()->locProcess->weights;
				wbStride	= 4*sizeof(uint8);
			}
			else
			{
				src 		= &(hdr()->locProcess->srcList->x);
				srcbStride	= 3*sizeof(int16) + 4*sizeof(uint8);
				w			= hdr()->locProcess->srcList[0].w;
				wbStride	= srcbStride;
			}
			_ProcessLocSkinning(src,srcbStride,w,wbStride);
		}

		//
		// Nrm process ?

		if( updflags & NvMesh::EN_NRM_SKINNING )
		{
			_ProcessNrmSkinning();
		}

		updflags = 0;
		drawFrameNo = DpyManager::GetFrameNo();
	}


	bool
	IsDrawing	(		)
	{
		return DpyManager::IsDrawing( drawFrameNo );
	}
	
};





//
// FACTORY


namespace
{

	struct NvMeshFactory : public RscFactory_SHR
	{
		uint32
		GetType		(			)
		{
			return NvMesh::TYPE;
		}


		pvoid
		AllocMemRsc	(	uint32		inBSize		)
		{
			// 32B alignement
			pvoid addr = EngineMallocA( inBSize, 32 );
			NV_ASSERT_ALIGNED( addr, 32 );
			return addr;
		}


		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvMeshBase::Header)
					&&	hdr->ver == NV_MESH_CVER					);
		}


		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvMeshBase::Header * hdr = (NvMeshBase::Header*) inRscData;
			TranslatePointer( &hdr->shadA,		inBOffset );
			TranslatePointer( &hdr->bunchA,		inBOffset );

			TranslatePointer( &hdr->compAddr[0], inBOffset );
			TranslatePointer( &hdr->compAddr[1], inBOffset );
			TranslatePointer( &hdr->compAddr[2], inBOffset );
			TranslatePointer( &hdr->compAddr[3], inBOffset );

			TranslatePointer( &hdr->locProcess, inBOffset );
			TranslatePointer( &hdr->nrmProcess, inBOffset );
			if( hdr->locProcess ) {
				TranslatePointer( &hdr->locProcess->srcListMorph, inBOffset );
				TranslatePointer( &hdr->locProcess->locTr		, inBOffset );
				TranslatePointer( &hdr->locProcess->weights		, inBOffset );
				TranslatePointer( &hdr->locProcess->srcList		, inBOffset );
				TranslatePointer( &hdr->locProcess->boneList	, inBOffset );
				TranslatePointer( &hdr->locProcess->cmdList		, inBOffset );
			}
			if( hdr->nrmProcess ) {
				TranslatePointer( &hdr->nrmProcess->srcList,  inBOffset );
				TranslatePointer( &hdr->nrmProcess->boneList, inBOffset );
				TranslatePointer( &hdr->nrmProcess->cmdList,  inBOffset );
			}

			GXVtxFmt vtxfmt = vtxfmt::Probe( hdr->vtxFmt );
			for( uint i = 0 ; i < hdr->surfCpt ; i++ ) {
				TranslatePointer( &hdr->bunchA[i].dlistAddr, inBOffset );
				// Setup vtxfmt in display-lists
				uint8* dl = (uint8*) hdr->bunchA[i].dlistAddr;
				dl[0] |= uint(vtxfmt);
				SyncDCache( dl, 32 );
			}
		}


		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			// Skinning or not, morphing or not => always shared
			// The CUSTOM is the same as SHR. So we use SHR ...
			return SHRM_DATA_SHR;
		}


		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvMeshBase * inst = (NvMeshBase*) inInst;
			return &inst->itf;
		}


		bool
		IsRscCompIsUsed	(	Desc*		inDesc,
							uint		inCompNo	)
		{
			// Find if an instance is using the original component array
			NV_ASSERT( inDesc->ptr );
			NvMeshBase::Header* hdr  = (NvMeshBase::Header*) inDesc->ptr;
			void*				addr = hdr->compAddr[ inCompNo ];
			// For all instances ...
			NvMeshBase* inst = (NvMeshBase*) inDesc->head;
			while( inst ) {
				if( inst->vtx.compAddr[inCompNo] == addr )
					return TRUE;
				inst = (NvMeshBase*) inst->inst.next;
			}
			return FALSE;
		}


		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{			
			NV_COMPILE_TIME_ASSERT( sizeof(NvMeshBase::LocSrc) == 10 );
			NV_COMPILE_TIME_ASSERT( sizeof(NvMeshBase::NrmSrc) == 3  );

			NV_ASSERT( inRscData );
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inRscData;
			uint supplyBSize = 0;

			if( hdr->surfCpt )
				supplyBSize += sizeof( NvBitmap* ) * hdr->surfCpt;

			bool myLocComp = FALSE;
			if( (hdr->skinnable || hdr->morphable) && IsRscCompIsUsed(inDesc,0) ) {
				NV_ASSERT( hdr->compBSize[0] );
				supplyBSize += (hdr->compBSize[0] + 32);
				myLocComp    = TRUE;
			}

			bool myNrmComp = FALSE;
			if( hdr->nrmskinnable && IsRscCompIsUsed(inDesc,3) ) {
				NV_ASSERT( hdr->compBSize[3] );
				supplyBSize += (hdr->compBSize[3] + 32);
				myNrmComp = TRUE;
			}

			if( hdr->skinnable ) {
				NV_ASSERT( hdr->locProcess );
				supplyBSize += (hdr->locProcess->boneCpt*4 + 4);
			}

			if( hdr->nrmskinnable ) {
				NV_ASSERT( hdr->nrmProcess );
				supplyBSize += (hdr->nrmProcess->boneCpt*4 + 4);
			}

			NvMeshBase* inst = NvEngineNewAS( NvMeshBase, 32, supplyBSize );
			if( !inst )		return NULL;
			NV_ASSERT_ALIGNED( inst, 32 );
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			inst->locBlendMap  = NULL;
			inst->nrmBlendMap  = NULL;
			inst->lockBitmapA  = NULL;
			inst->vtx.vtxFmt   = hdr->vtxFmt;
			inst->vtx.locFrac  = hdr->locFrac;
			for( uint i = 0 ; i < 4 ; i++ ) {
				inst->vtx.compAddr[i]    = hdr->compAddr[i];
				inst->vtx.compBStride[i] = hdr->compBStride[i];
			}

			uint32 supplyPtr0 = uint32( inst + 1 );
			uint32 supplyPtr  = supplyPtr0;

			if( hdr->surfCpt ) {
				uint bsize = sizeof( NvBitmap* ) * hdr->surfCpt;
				inst->lockBitmapA = (NvBitmap**) supplyPtr;
				supplyPtr += bsize;
				Memset( inst->lockBitmapA, 0, bsize );
			}

			if( myLocComp ) {
				supplyPtr  = Round32( supplyPtr );
				void* myAddr  = (void*) supplyPtr;
				void* rscAddr = hdr->compAddr[0];
				uint  bsize   = hdr->compBSize[0];
				Memcpy( myAddr, rscAddr, bsize );
				inst->vtx.compAddr[0] = myAddr;
				supplyPtr += bsize;
				SyncDCache( myAddr, bsize );
				NV_ASSERT_ALIGNED( myAddr, 32 );
			}

			if( myNrmComp ) {
				supplyPtr  = Round32( supplyPtr );
				void* myAddr  = (void*) supplyPtr;
				void* rscAddr = hdr->compAddr[3];
				uint  bsize   = hdr->compBSize[3];
				Memcpy( myAddr, rscAddr, bsize );
				inst->vtx.compAddr[3] = myAddr;
				supplyPtr += bsize;
				SyncDCache( myAddr, bsize );
				NV_ASSERT_ALIGNED( myAddr, 32 );
			}

			if( hdr->skinnable ) {
				supplyPtr = Round4( supplyPtr );
				inst->locBlendMap = (Matrix**) Round4( supplyPtr );
				supplyPtr += (hdr->locProcess->boneCpt * 4);
			}

			if( hdr->nrmskinnable ) {
				supplyPtr = Round4( supplyPtr );
				inst->nrmBlendMap = (Matrix**) Round4( supplyPtr );
				supplyPtr += (hdr->nrmProcess->boneCpt * 4);
			}

			SyncDCache( inRscData, inRscBSize );
			NV_ASSERT( (supplyPtr-supplyPtr0) <= supplyBSize );
			inst->Init();

			return inst;
		}


		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvMeshBase * inst = (NvMeshBase*) inInst;
			inst->Shut();
			NvEngineDelete( inst );
		}
	};

}



bool
rscfactory_NvMesh_Reg()
{
	meshFactory = NvEngineNew( NvMeshFactory );
	return RscManager::RegisterFactory( meshFactory );
}



NvMesh *
NvMesh::Create	(	uint32	inUID	)
{
	if( !meshFactory )
		return NULL;
	return (NvMesh*) meshFactory->CreateInstance( inUID );
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvMesh::TYPE = 0xC57DF82A;	// CRC("NvMesh")

NVITF_MTH0(		Mesh,	NvInterface*,			GetBase												)
NVITF_CAL0(		Mesh,	void,					AddRef												)
NVITF_MTH0(		Mesh,	uint,					GetRefCpt											)
NVITF_CAL0(		Mesh,	void,					Release												)
NVITF_MTH0(		Mesh,	uint32,					GetRscType											)
NVITF_MTH0(		Mesh,	uint32,					GetRscUID											)
NVITF_MTH0(		Mesh,	NvkMesh*,				GetKInterface										)
NVITF_MTH0(		Mesh,	uint32,					GetNameCRC											)
NVITF_MTH0(		Mesh,	Box3*,					GetBBox												)
NVITF_MTH0(		Mesh,	uint,					GetSurfaceCpt										)
NVITF_MTH1(		Mesh,	uint32,					GetSurfaceNameCRC,		uint						)
NVITF_MTH1(		Mesh,	uint32,					GetSurfaceBitmapUID,	uint						)
NVITF_CAL0(		Mesh,	void,					RefBitmaps											)
NVITF_CAL0(		Mesh,	void,					UnrefBitmaps										)
NVITF_MTH0(		Mesh,	bool,					IsSkinnable											)
NVITF_MTH0(		Mesh,	bool,					IsNrmSkinnable										)
NVITF_MTH0(		Mesh,	bool,					IsMorphable											)
NVITF_MTH0(		Mesh,	bool,					IsLightable											)
NVITF_MTH0(		Mesh,	bool,					IsShadowing											)
NVITF_MTH0(		Mesh,	bool,					IsFullOpaque										)
NVITF_CAL1(		Mesh,	void,					Enable,					uint32						)
NVITF_CAL1(		Mesh,	void,					Disable,				uint32						)
NVITF_CAL1(		Mesh,	void,					SetEnabled,				uint32						)
NVITF_MTH0(		Mesh,	uint32,					GetEnabled											)
NVITF_MTH3(		Mesh,	bool,					MapSkeleton,			uint, uint32*, Matrix*		)
NVITF_CAL0(		Mesh,	void,					UnmapSkeleton										)
NVITF_MTH0(		Mesh,	uint,					GetMorphSlotCpt										)
NVITF_CAL1(		Mesh,	void,					SetMorphSlotCpt,		uint						)
NVITF_MTH1(		Mesh,	NvMorphTarget*,			GetMorphSlotTarget,		uint						)
NVITF_CAL2(		Mesh,	void,					SetMorphSlotTarget,		uint, NvMorphTarget*		)
NVITF_CAL2(		Mesh,	void,					SetMorphSlotWeight,		uint, float					)
NVITF_CAL0(		Mesh,	void,					Update												)

NVKITF_MTH0(	Mesh,	NvInterface*,			GetBase												)
NVKITF_CAL0(	Mesh,	void,					AddRef												)
NVKITF_MTH0(	Mesh,	uint,					GetRefCpt											)
NVKITF_CAL0(	Mesh,	void,					Release												)
NVKITF_MTH0(	Mesh,	uint32,					GetRscType											)
NVKITF_MTH0(	Mesh,	uint32,					GetRscUID											)
NVKITF_MTH0(	Mesh,	NvMesh*,				GetInterface										)
NVKITF_MTH0(	Mesh,	Box3*,					GetBBox												)
NVKITF_MTH0(	Mesh,	uint32,					GetNameCRC											)
NVKITF_MTH0(	Mesh,	bool,					IsSkinnable											)
NVKITF_MTH0(	Mesh,	bool,					IsNrmSkinnable										)
NVKITF_MTH0(	Mesh,	bool,					IsMorphable											)
NVKITF_MTH0(	Mesh,	bool,					IsLightable											)
NVKITF_MTH0(	Mesh,	bool,					IsShadowing											)
NVKITF_MTH0(	Mesh,	bool,					IsFullOpaque										)
NVKITF_MTH0(	Mesh,	uint,					GetSurfaceCpt										)
NVKITF_MTH0(	Mesh,	NvkMesh::Shading*,		GetSurfaceShadingA									)
NVKITF_MTH0(	Mesh,	NvkMesh::Bunch*,		GetBunchA											)
NVKITF_MTH0(	Mesh,	NvkMesh::Vtx*,			GetVtx												)
NVKITF_CAL0(	Mesh,	void,					RefBitmaps											)
NVKITF_CAL0(	Mesh,	void,					UnrefBitmaps										)
NVKITF_CAL0(	Mesh,	void,					UpdateBeforeDraw									)
NVKITF_MTH0(	Mesh,	bool,					IsDrawing											)





