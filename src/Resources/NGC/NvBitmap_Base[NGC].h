/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef	_NvBitmapBase_NGC_H_
#define	_NvBitmapBase_NGC_H_


#include <NvBitmap.h>
#include <Kernel/NGC/NvkBitmap[NGC].h>
#include <Kernel/Common/NvRscFactory_SHR.h>



//
// BASE implementation

struct NvBitmapBase : public RscFactory_SHR::Instance
{
private:
	friend struct NvBitmap;
	friend struct NvkBitmap;
	NvBitmap	itf;
	NvkBitmap	kitf;
public:
			NvBitmap*				GetInterface		(			) { return &itf;  }
			NvkBitmap*				GetKInterface		(			) { return &kitf; }
	virtual 					~	NvBitmapBase		(			) {}
	uint32							GetRscType			(				) { return NvBitmap::TYPE; }
	virtual	void					AddRef				(			) = 0;
	virtual	uint					GetRefCpt			(			) = 0;
	virtual	void					Release				(			) = 0;
	virtual uint32					GetRscUID			(			) = 0;
	virtual	uint32					GetWidth			(			) = 0;
	virtual uint32					GetHeight			(			) = 0;
	virtual	uint32					GetWPad				(			) = 0;
	virtual uint32					GetHPad				(			) = 0;
	virtual GXTexFmt				GetPixelFormat 		(			) = 0;
	virtual	NvBitmap::AlphaStatus	GetAlphaStatus		(			) = 0;
	virtual bool					IsBanner				(			) = 0;
	virtual bool					IsMask				(			) = 0;
	virtual bool					Pick				( uint,uint	) = 0;
	virtual uint32					GetMipmapCpt		(			) = 0;
	virtual	void					UpdateBeforeDraw	(			) = 0;
	virtual	bool					IsDrawing			(			) = 0;
	virtual void	*				GetTexel			(			) = 0;	
};



#endif // _NvBitmapBase_NGC_H_


