/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/Common/NvRscFactory_SHR.h>
#include <Kernel/NGC/NvkMovie[NGC].h>
#include <Kernel/NGC/NvkImage[NGC].h>
using namespace nv;

struct NvMovieBase;




#define	NV_MOVIE_CVER	MK_UINT64( '____', 'MV01' )

#define DEF_CAPACITY_BSIZE	(512U<<10U)
#define DEF_MAX_LOAD_BSIZE	(256U<<10U)




namespace
{

	RscFactory*		movieFactory = NULL;


	struct BFstream
	{
		struct Page
		{
			byte*			addr;
			uint			reserve;
			uint			capacity;

			uint			fetched;
			uint			reserved;
			uint			loaded;
			uint			loading;
		};

		uint				is_bstart;
		uint				is_bcur;
		uint				is_bend;

		file::ReadRequest	rr;

		Page				front;
		Page				back;


		void init ()
		{
			is_bstart = 0;
			is_bcur   = 0;
			is_bend   = 0;
		}

		void shut ()
		{
			unlock();
		}

		bool islocked ()
		{
			return (is_bstart != is_bend);
		}

		bool isloading ()
		{
			if( islocked() )
			{
				return back.loading > 0;
			}
			else
			{
				return !rr.IsReady();
			}
		}

		bool lock (	uint in_boffset, uint in_bsize, uint in_reserve, uint in_capacity )
		{
			if( islocked() )
				return FALSE;

			if( in_bsize==0 || in_capacity==0 )
				return FALSE;
			
			// check reserve bsize: at least NVHW_FILE_BALIGN bytes to manage loading byte-offset alignement !
			if( in_reserve<NVHW_FILE_BALIGN )
				return FALSE;
			

			void* p0 = NvMallocA( in_reserve+in_capacity, NVHW_FILE_BALIGN );
			void* p1 = NvMallocA( in_reserve+in_capacity, NVHW_FILE_BALIGN );
			if( !p0 || !p1 ) {
				SafeFree( p0 );
				SafeFree( p1 );
				return FALSE;
			}

			is_bstart = in_boffset;
			is_bcur	  = in_boffset;
			is_bend   = in_boffset + in_bsize;

			Zero( front );
			Zero( back );
			front.addr  = (byte*) p0;
			back.addr   = (byte*) p1;
			front.capacity = back.capacity = in_capacity;
			front.reserve  = back.reserve  = in_reserve;

			return TRUE;
		}

		void unlock ( )
		{
			while( isloading() )
			{
				core::Update();
				update();
			}

			if( islocked() )
			{
				is_bstart = 0;
				is_bcur   = 0;
				is_bend   = 0;
				SafeFree( front.addr );
				SafeFree( back.addr );
			}
		}

		bool getavailable ( uint& out_bs, byte*& out_addr )
		{
			if( !islocked() )
				return FALSE;

			NV_ASSERT( front.loading == 0 );
			NV_ASSERT( front.reserved <= front.reserve );
			NV_ASSERT( front.loaded <= front.capacity );
			NV_ASSERT( front.fetched <= (front.reserved+front.loaded) );

			uint all_bs = front.reserved + front.loaded;
			out_bs   = all_bs - front.fetched;
			out_addr = front.addr + front.reserve - front.reserved + front.fetched;

			return (out_bs>0);
		}

		bool fetch ( uint& out_bs, byte*& out_addr, uint in_minbs, uint in_maxbs )
		{
			if( in_maxbs==0 || in_maxbs<in_minbs )
				return FALSE;

			uint  avail_bs;
			byte* avail_p;
			if( getavailable(avail_bs,avail_p) && (avail_bs>=in_minbs) )
			{
				// fully available in front page
				out_bs   = Min( avail_bs, in_maxbs );
				out_addr = avail_p;
				front.fetched += out_bs;
				return TRUE;
			}

			return FALSE;
		}

		void update ( )
		{
			for( ;; )
			{
				if( !islocked() )
					return;

				// update loading
				if( back.loading )
				{
					// still loading ?
					if( !rr.IsReady() )
						return;

					// loading error ?
					if( !rr.IsCompleted() )
					{
						unlock();
						return;
					}

					back.loaded += back.loading;
					back.loading = 0;
				}

				// start loading ?
				if( (is_bcur < is_bend) && (back.loaded < back.capacity) )
				{
					uint  remain = is_bend - is_bcur;
					uint  cap    = back.capacity - back.loaded;
					uint  lalign = (is_bcur % NVHW_FILE_BALIGN);

					if( lalign>0 )
					{
						NV_ASSERT( back.loaded == 0 );
						NV_ASSERT( back.reserved == 0 );
						NV_ASSERT( remain >= NVHW_FILE_BALIGN );
						back.reserved = NVHW_FILE_BALIGN - lalign;
						back.loading  = Min( remain-back.reserved, cap );
						uint  lbsize = RoundX( NVHW_FILE_BALIGN+back.loading, NVHW_FILE_BALIGN );
						byte* laddr  = back.addr + back.reserve - NVHW_FILE_BALIGN;
						NV_ASSERT_RESULT( rr.Setup(laddr,lbsize,is_bcur-lalign) );
						NV_ASSERT_RESULT( file::AddReadRequest(&rr) );
						is_bcur += back.loading + back.reserved;
						return;
					}
					else
					{
						back.loading = Min( remain, cap );
						uint  lbsize = RoundX( back.loading, NVHW_FILE_BALIGN );
						byte* laddr  = back.addr + back.reserve + back.loaded;
						NV_ASSERT_RESULT( rr.Setup(laddr,lbsize,is_bcur) );
						NV_ASSERT_RESULT( file::AddReadRequest(&rr) );
						is_bcur += back.loading;
						return;
					}
				}

				// available bytes in front page ?
				uint  avail_bs;
				byte* avail_p;
				if( !getavailable(avail_bs,avail_p) )
					avail_bs = 0;

				// back page has bytes ?
				// front page has few bytes, fitting to back's reserve ?
				// => move bytes to reserve and switch back to front !
				if( back.loaded>0 && avail_bs<=back.reserve )
				{
					if( avail_bs ) {
						Memcpy( back.addr+back.reserve-avail_bs, avail_p, avail_bs );
						back.reserved = avail_bs;
					}

					// switch
					front.fetched  = 0;
					front.reserved = 0;
					front.loaded   = 0;
					front.loading  = 0;
					Swap( back, front );
				}
				else
				{
					// nothing to do ...
					return;
				}
			}
		}
		
		bool load ( byte* in_tobuffer, uint in_boffset, uint in_bsize )
		{
			if( lock(in_boffset,in_bsize,NVHW_FILE_BALIGN,32*1024) )
			{
				byte* p     = in_tobuffer;
				byte* p_end = in_tobuffer + in_bsize;
				while( p < p_end )
				{
					if( !islocked() )
						return FALSE;

					core::Update();
					update();

					uint  l_bs;
					byte* l_addr;
					if( fetch(l_bs,l_addr,1,in_bsize) )
					{
						Memcpy( p, l_addr, l_bs );
						p += l_bs;
					}
				}

				unlock();
				return TRUE;
			}
			return FALSE;
		}
	};

}




//
// BASE

struct NvMovieBase : public RscFactory_SHR::Instance
{
	struct Frame
	{
		float			time;
		uint32			boffset;
	};

	struct Header
	{
		uint64			ver;
		uint32			flags;
		uint16			width;
		uint16			height;
		uint32			fcount;
		uint32			fmaxbsize;
		float			frate;
		float			duration;
	//	Frame			frames[ fcount ];
	};

	NvMovie				itf;
	NvkMovie			kitf;

	uint32				rsc_bsize;
	uint32				rsc_boffset;

	NvMovie::State		state;

	Frame*				frames;

	BFstream			bfstream;

	int					curpos;
	int					skeekpos;

	NvkImage*			cur_fi;
	NvkImage*			dec_fi;
	bool				valid_cur_fi;




	virtual	~	NvMovieBase	(	) {}			// needed by nv::DestructInPlace<T>(ptr)




	Header* hdr ( )
	{
		return (Header*)inst.dataptr;
	}


	void convert_endian_header ( )
	{
		Header* h = hdr();

		ConvertLSB( h->ver );
		ConvertLSB( h->flags );
		ConvertLSB( h->width );
		ConvertLSB( h->height );
		ConvertLSB( h->fcount );
		ConvertLSB( h->fmaxbsize );
		ConvertLSB( h->frate );
		ConvertLSB( h->duration );
	}

	
	void convert_endian_frames ( )
	{
		Header* h = hdr();

		Frame* f     = frames;
		Frame* f_end = f + h->fcount;
		while( f < f_end )
		{
			ConvertLSB( f->boffset );
			ConvertLSB( f->time );
			f++;
		}
	}
	
	Frame* load_frame_infos ( )
	{
		uint fcount = hdr()->fcount;
		uint toload = sizeof(Frame) * fcount;

		frames = (Frame*) NvMalloc( toload );
		NV_ASSERT( frames );

		if( frames )
		{
			BFstream bs;
			bs.init();
			bs.load( (byte*)frames, rsc_boffset+sizeof(Header), toload );
			bs.shut();
			return frames;
		}

		SafeFree( frames );
		return NULL;
	}


	uint get_frame_bsize ( int in_frame_no )
	{
		NV_ASSERT( frames );
		NV_ASSERT( in_frame_no >= 0 );
		NV_ASSERT( in_frame_no <  hdr()->fcount );

		uint f_count   = hdr()->fcount;
		uint f_boffset = frames[in_frame_no].boffset;
		uint n_boffset = ( in_frame_no == f_count-1 ) ? rsc_bsize : frames[in_frame_no+1].boffset;
		uint f_bsize   = n_boffset - f_boffset;

		return f_bsize;
	}


	bool stream_lock ( int in_frame_no )
	{
		NV_ASSERT( frames );
		NV_ASSERT( in_frame_no >= 0 );
		NV_ASSERT( in_frame_no <  hdr()->fcount );

		uint f_boffset = frames[ in_frame_no ].boffset;
		NV_ASSERT( f_boffset < rsc_bsize );

		uint l_boffset  = rsc_boffset + f_boffset;
		uint l_bsize    = rsc_bsize   - f_boffset;
		uint l_reserve  = Round1024( hdr()->fmaxbsize );
		uint l_capacity = DEF_CAPACITY_BSIZE;

		return bfstream.lock( l_boffset, l_bsize, l_reserve, l_capacity );
	}


	bool fetch_cur_frame ( bool do_decode )
	{
		NV_ASSERT( IsLocked() );

		// fetch the current jpeg
		uint fbsize = get_frame_bsize( curpos );
		uint fboffs = frames[curpos].boffset;

		uint  f_bs;
		byte* f_addr;
		if( bfstream.fetch(f_bs,f_addr,fbsize,fbsize) )
		{
			NV_ASSERT( f_bs == fbsize );
			curpos++;

			if( do_decode )
			{
				int errcode = dec_fi->UpdateJpeg( f_addr, f_bs );
				NV_ASSERT( errcode == 0 );
				Swap( dec_fi, cur_fi );
				valid_cur_fi = TRUE;
			}

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


	bool Init ( uint32 in_rsc_boffset, uint32 in_rsc_bsize )
	{
		rsc_bsize   = in_rsc_bsize;
		rsc_boffset = in_rsc_boffset;

		convert_endian_header();

		uint wp2 = 1 << GetCeilPow2( uint32(hdr()->width) );
		uint hp2 = 1 << GetCeilPow2( uint32(hdr()->height) );

		NvImage* i0 = NvImage::Create( wp2, hp2 );
		NvImage* i1 = NvImage::Create( wp2, hp2 );
		dec_fi = i0 ? i0->GetKInterface() : NULL;
		cur_fi = i1 ? i1->GetKInterface() : NULL;
		valid_cur_fi = FALSE;

		frames = load_frame_infos();
		convert_endian_frames();

		if( dec_fi && cur_fi && frames )
		{
			bfstream.init();
			state = NvMovie::MS_UNLOCKED;
			return TRUE;
		}
		else
		{
			SafeFree( frames );
			SafeRelease( cur_fi );
			SafeRelease( dec_fi );
			return FALSE;			
		}
	}


	void Shut ( )
	{
		Unlock();

		SafeFree( frames );
		SafeRelease( cur_fi );
		SafeRelease( dec_fi );

		bfstream.shut();
	}


	void AddRef	( )
	{
		inst.refCpt++;
	}


	uint GetRefCpt ( )
	{
		return inst.refCpt;
	}


	void Release (		)
	{
		NV_ASSERT( inst.refCpt );
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			NV_ASSERT( movieFactory );
			movieFactory->FreeInstance( &itf );
		}
	}


	uint32 GetRscType ( )
	{
		return NvMovie::TYPE;
	}


	uint32 GetRscUID ( )
	{
		return inst.uid;
	}


	NvkMovie* GetKInterface	( )
	{
		return &kitf;
	}
	

	NvMovie* GetInterface ( )
	{
		return &itf;
	}


	uint GetWidth ( )
	{
		return hdr()->width;
	}


	uint GetHeight ( )
	{
		return hdr()->height;
	}


	uint CountFrames	(		)
	{
		return hdr()->fcount;
	}


	float GetFrameRate	(		)
	{
		return hdr()->frate;
	}


	float GetFrameTime ( int inFno )
	{
		if( frames && inFno<hdr()->fcount )
		{
			return frames[ inFno ].time;
		}
		else
		{
			return -1.f;
		}
	}


	float GetDuration	(		)
	{
		return hdr()->duration;
	}


	NvMovie::State GetState ( )
	{
		return state;
	}


	bool IsBusy ( )
	{
		if( state == NvMovie::MS_UNLOCKED )	return FALSE;
		if( state == NvMovie::MS_LOCKED )	return FALSE;
		return TRUE;
	}


	bool Lock (	int inFno )
	{
		if( state != NvMovie::MS_UNLOCKED )
			return FALSE;

		curpos = Clamp( inFno, 0, int(hdr()->fcount)-1 );

		if( stream_lock(curpos) )
		{
			state = NvMovie::MS_LOCKING;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


	bool Unlock ( )
	{
		if( IsLocked() )
		{
			bfstream.unlock();
			state = NvMovie::MS_UNLOCKED;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


	bool IsLocked ( )
	{
		return ( state >= NvMovie::MS_LOCKED );
	}


	int GetPosition ( )
	{
		if( IsLocked() )
		{
			return curpos;
		}
		else
		{
			return -1;
		}
	}


	bool DecodeFrame (	)
	{
		if( state == NvMovie::MS_LOCKED )
		{
			NV_ASSERT( curpos >= 0 );
			if( curpos < hdr()->fcount )
			{
				state = NvMovie::MS_DECODING;
				return TRUE;
			}
		}

		return FALSE;
	}


	bool SkipFrame ( int inFrameCount )
	{
		if( inFrameCount>0 && state == NvMovie::MS_LOCKED )
		{
			skeekpos = Min( curpos+inFrameCount, int(hdr()->fcount)-1 );
			if( skeekpos > curpos )
			{
				state = NvMovie::MS_SEEKING;
				return TRUE;
			}
		}

		return FALSE;
	}


	NvBitmap* GetBitmap ( )
	{
		if( IsLocked() && valid_cur_fi )
		{
			return cur_fi->GetBitmap();
		}
		else
		{
			return NULL;
		}
	}


	void Update	( )
	{
		if( state == NvMovie::MS_UNLOCKED )
			return;

		bfstream.update();

		if( !bfstream.islocked() )
		{
			// error !
			bfstream.unlock();
			state = NvMovie::MS_UNLOCKED;
			return;
		}

		if( state == NvMovie::MS_LOCKING )
		{
			if( bfstream.isloading() )
				return;
			state = NvMovie::MS_LOCKED;
		}

		else if( state == NvMovie::MS_UNLOCKING )
		{
			//
		}

		else if( state == NvMovie::MS_DECODING )
		{
			if( fetch_cur_frame(TRUE) )
			{
				state = NvMovie::MS_LOCKED;
			}
		}

		else if( state == NvMovie::MS_SEEKING )
		{
			while( skeekpos > curpos )
			{
				if( !fetch_cur_frame(FALSE) )
					return;
			}
			state = NvMovie::MS_LOCKED;
		}

		if( IsLocked() && curpos >= hdr()->fcount )
			Unlock();
	}

};


//
// FACTORY


namespace
{

	struct NvMovieFactory : public RscFactory_SHR
	{
		uint32
		GetType	(	)
		{
			return NvMovie::TYPE;
		}

		uint32
		GetToPrefetchRscBSize	(	uint32		inUID,
									uint32		inBSize		)
		{
			if( !inUID )
				return 0;
			return sizeof(NvMovieBase::Header);
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvMovieBase::Header* hdr = (NvMovieBase::Header*) inRscData;
			uint64 ver = LSBToNative( hdr->ver );
			return (inBSize>=sizeof(NvMovieBase::Header)) && (ver==NV_MOVIE_CVER);
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_DATA_DUP0;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NvMovieBase* inst = NvEngineNew( NvMovieBase );
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			// rsc boffset & bsize in the BF
			RscManager::RscInfo rsc;
			bool done = RscManager::GetRscInfo( inst->inst.uid, &rsc );
			NV_ASSERT( done );
			NV_ASSERT( rsc.bsize >= sizeof(NvMovieBase::Header) );
			if( !done || rsc.bsize < sizeof(NvMovieBase::Header) ) {
				// Bigfile close ?!
				NvEngineDelete( inst );
				return NULL;
			}

			done = inst->Init( rsc.boffset[0], rsc.bsize );
			if( !done ) {
				NvEngineDelete( inst );
				return NULL;
			}

			return inst;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvMovieBase* inst = (NvMovieBase*) inInst;
			return &inst->itf;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvMovieBase* inst = (NvMovieBase*) inInst;
			inst->Shut();
			NvEngineDelete( inst );
		}
	};
}



bool
rscfactory_NvMovie_Reg()
{
	movieFactory = NvEngineNew( NvMovieFactory );
	return RscManager::RegisterFactory( movieFactory );
}



NvMovie *
NvMovie::Create	(	uint32		inUID	)
{
	if( !movieFactory )
		return NULL;
	return (NvMovie*) movieFactory->CreateInstance( inUID );
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvMovie::TYPE = 0x7FA7FD72;	// CRC("NvMovie")

NVITF_MTH0(		Movie,	NvInterface*,	GetBase										)
NVITF_CAL0(		Movie,	void,			AddRef										)
NVITF_MTH0(		Movie,	uint,			GetRefCpt									)
NVITF_CAL0(		Movie,	void,			Release										)
NVITF_MTH0(		Movie,	uint32,			GetRscType									)
NVITF_MTH0(		Movie,	uint32,			GetRscUID									)
NVITF_MTH0(		Movie,	NvkMovie*,		GetKInterface								)
NVITF_MTH0(		Movie,	uint,			GetWidth									)
NVITF_MTH0(		Movie,	uint,			GetHeight									)
NVITF_MTH0(		Movie,	NvBitmap*,		GetBitmap									)
NVITF_MTH0(		Movie,	uint,			CountFrames									)
NVITF_MTH0(		Movie,	float,			GetFrameRate								)
NVITF_MTH1(		Movie,	float,			GetFrameTime,			int					)
NVITF_MTH0(		Movie,	float,			GetDuration									)
NVITF_MTH0(		Movie,	NvMovie::State,	GetState									)
NVITF_MTH0(		Movie,	bool,			IsBusy										)
NVITF_MTH1(		Movie,	bool,			Lock,					int					)
NVITF_MTH0(		Movie,	bool,			Unlock										)
NVITF_MTH0(		Movie,	bool,			IsLocked									)
NVITF_MTH0(		Movie,	int,			GetPosition									)
NVITF_MTH1(		Movie,	bool,			SkipFrame,				int					)
NVITF_MTH0(		Movie,	bool,			DecodeFrame									)
NVITF_CAL0(		Movie,	void,			Update										)
	

NVKITF_MTH0(	Movie,	NvInterface*,	GetBase										)
NVKITF_CAL0(	Movie,	void,			AddRef										)
NVKITF_MTH0(	Movie,	uint,			GetRefCpt									)
NVKITF_CAL0(	Movie,	void,			Release										)
NVKITF_MTH0(	Movie,	uint32,			GetRscType									)
NVKITF_MTH0(	Movie,	uint32,			GetRscUID									)
NVKITF_MTH0(	Movie,	NvMovie*,		GetInterface								)



