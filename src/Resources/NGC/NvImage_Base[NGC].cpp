/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/NvDpyManager[NGC].h>
#include <Kernel/NGC/NvkImage[NGC].h>
#include <Kernel/NGC/GEK/GEK.h>

#include <NvImage.h>
#include "NvBitmap_Base[NGC].h"

using namespace nv;


struct NvImageBase : public NvBitmapBase
{
private:
	friend struct NvImage;
	friend struct NvkImage;
	NvImage		itf;
	NvkImage	kitf;

public :	

	bool					autoFreePixels;	// Auto free back externals buffers ?
	pvoid					accessPixel;
	NvImage::Region			accessRegion;
	NvImage::Region			validRegion;
	uint32					drawFrameNo;
	bool					isJpeg16;
	
	uint16					width;
	uint16					height;
	pvoid					idata;
		
	virtual	~	NvImageBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)
	
	void
	AddRef					(			)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release	(			)
	{
		NV_ASSERT( inst.refCpt );
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			// Safe to delete ?
			if( IsDrawing() )
			{
				DpyManager::AddToGarbager( this );
				return;
			}
			else
			{
				if( autoFreePixels ) {
					SafeFree( idata );
				}
				SafeFree( accessPixel );
				NvEngineDelete( this );
			}
		}
	}

	uint32
	GetRscType	(			)
	{
		return NvImage::TYPE;
	}
	
	uint32
	GetRscUID	(			)
	{
		return 0;
	}

	NvImage*
	GetInterface		(			)
	{
		return &itf;
	}

	NvkImage*
	GetKInterface		(			)
	{
		return &kitf;
	}
	
	NvBitmap*				GetBitmap			(										)
	{
		return NvBitmapBase::GetInterface();
	}
	
	uint32
	GetWidth		(			)
	{
		return width;
	}

	uint32
	GetHeight		(			)
	{
		return height;
	}

	uint32
	GetWPad		(		)
	{
		return 0.0f;
	}

	uint32
	GetHPad		(		)
	{
		return 0.0f;
	}

	bool
	IsBanner	(	)
	{
		return FALSE;
	}
	bool IsMask ()
	{
		return FALSE;
	}

	bool Pick ( uint , uint )
	{
		return FALSE;
	}

	GXTexFmt
	GetPixelFormat ()
	{
		if (!isJpeg16)
			return GXTexFmt(GX_TF_RGBA8);
		else
			return GXTexFmt(GX_TF_RGB565);
	}
		
	NvBitmap::AlphaStatus
	GetAlphaStatus	(			)
	{
		return NvBitmap::AS_OPAQUE;
	}
	uint32	
	GetMipmapCpt	()	
	{
		return 1;
	}
	void
	SetAutoFreePixels	(	bool	inOnOff		)
	{
		autoFreePixels = inOnOff;
	}


	bool					CreateAccess		(	NvImage::Region &		inRegion			)
	{
		if(		accessPixel
			||	inRegion.x<0
			||	inRegion.y<0
			||	inRegion.w==0
			||	inRegion.h==0
			||	(inRegion.x+inRegion.w) > GetWidth()
			||	(inRegion.y+inRegion.h) > GetHeight()	)
			return FALSE;
		accessRegion = inRegion;
		uint bsize;
		bsize = inRegion.w * inRegion.h * 4;
		accessPixel = EngineMallocA( bsize, 4 );
		NV_ASSERT( accessPixel );
		Zero( validRegion );

		if (!accessPixel)
			return FALSE;			
		return TRUE;
	}
	
	pvoid					GetPixelAccess		(	uint&		outLineStride,
													Psm&		outPSM					)
	{
		if( !accessPixel )
			return NULL;
		outLineStride = accessRegion.w * 4;
		outPSM		  = PSM_ARGB32;
		return accessPixel;
	}
													
	uint32*					GetClutAccess		(	Psm&		outPSM					)
	{
		return NULL;
	}
	
	void					ValidatePixelAccess	(	NvImage::Region&		inSubRegion				)
	{
		if( !accessPixel )
			return;

		// Clip sub-region with access-region
		if(		inSubRegion.x > accessRegion.w
			||	inSubRegion.y > accessRegion.h
			||	inSubRegion.w == 0
			||	inSubRegion.h == 0	)
			return;
		inSubRegion.w = Min( inSubRegion.w, accessRegion.w-inSubRegion.x );
		inSubRegion.h = Min( inSubRegion.h, accessRegion.h-inSubRegion.y );
		NV_ASSERT( inSubRegion.w && inSubRegion.h );

		if( validRegion.w == 0 )
		{
			// Init invalid-region
			validRegion = inSubRegion;
		}
		else
		{
			// Merge regions
			uint ix1 = validRegion.x + validRegion.w;
			uint iy1 = validRegion.y + validRegion.h;
			uint sx1 = inSubRegion.x + inSubRegion.w;
			uint sy1 = inSubRegion.y + inSubRegion.h;
			uint mx0 = Min( validRegion.x, inSubRegion.x );
			uint my0 = Min( validRegion.y, inSubRegion.y );
			NV_ASSERT( mx0 < ix1 && mx0 < sx1 );
			NV_ASSERT( my0 < iy1 && my0 < sy1 );
			uint mw  = Max( ix1-mx0, sx1-mx0 );
			uint mh  = Max( iy1-my0, sy1-my0 );
			validRegion.x = mx0;
			validRegion.y = my0;
			validRegion.w = mw;
			validRegion.h = mh;
		}
	}
	
	void
	ValidateClutAccess	(										)
	{
	
	}
	
	void
	ReleaseAccess		(										)
	{
		if( !accessPixel )
			return;
		UpdateBeforeDraw();
		EngineFree( accessPixel );
		accessPixel = NULL;
	}
	
	void
	UpdateBeforeDraw			(				)
	{
		// store the flushing frame
		drawFrameNo = DpyManager::GetFrameNo();
				
		if( !accessPixel )
			return;

		// Update texels
		if( validRegion.w )
		{	
			uint top 			= accessRegion.y + validRegion.y;
			uint bottom 		= accessRegion.y + validRegion.h;
			uint left			= accessRegion.x + validRegion.x;
			uint right 			= accessRegion.x + validRegion.w;
			
			uint32 *src			= (uint32 * )accessPixel;
			uint16 *dst			= (uint16 * )idata;
			
			uint 	nbTileW		= width >> 2;
			uint	X 			= left  >> 2;
			uint	Y 			= top   >> 2;
			uint 	subLineDec  = ((right - left) << 3) - 4;
			uint	lineDec		= (nbTileW <<5) - 16;
			uint 	dstInd 		= (((Y * nbTileW) + X) << 5);
			
			for ( uint y = top 	; y < bottom ; y+=4){
				for( uint j = 0 ; j < 4 ; ++j ) {			
					for ( uint x = left	; x < right ; x+=4){
						for (uint i = 0 ; i < 4 ; ++i) {
							*(dst + dstInd) = uint16( ((*src) >> 16)   );
							*(dst + dstInd+16) = uint16(  (*src) & 0xFFFF	);
							src++;
							dstInd ++;
						}
						dstInd += 28;
					}
					dstInd -= subLineDec;
				}
				dstInd += lineDec;
			}
		}
		
		SyncDCache( idata, width*height*4 );
		Zero( validRegion );
	}

	bool
	IsDrawing	(			)
	{
		return DpyManager::IsDrawing( drawFrameNo );
	}
	
	void	*	GetTexel (			) {
		return idata;
	}
	
	int UpdateJpeg (	byte*		inJpegAddr,
						uint		inJpegBSize		)
	{
		if( !inJpegAddr || !inJpegBSize )
			return -1;

		// begin jpeg decoding
		jpeg::ImageDesc jdesc;
		int				errcode;

		jpeg::DecodePSM jpsm;
		jpsm = jpeg::OPSM_5650;
//		jpsm = jpeg::OPSM_888X;

		errcode = jpeg::DecodeBegin( jdesc, (byte*)inJpegAddr, inJpegBSize , jpsm );
		NV_ASSERT( errcode==0 );

		if( errcode == 0 )
		{
			if( jdesc.width>width || jdesc.height>height ){
				return -1;
				jpeg::DecodeEnd();
			}

			if (jdesc.numco==3 && jdesc.bpp==4) 
			{
				uint top 			= 0;
				uint bottom 		= jdesc.height;
				uint left			= 0;
				uint right 			= jdesc.width;

				uint16 *dst			= (uint16 * )idata;
				uint32 *src			= NULL;
				byte   *srcb		= NULL;
				
				uint 	nbTileW		= width >> 2;
				uint	X 			= left  >> 2;
				uint	Y 			= top   >> 2;
				uint 	subLineDec  = ((right - left) << 3) - 4;
				uint	lineDec		= (nbTileW <<5) - 16;
				uint 	dstInd 		= (((Y * nbTileW) + X) << 5);
				
				for ( uint y = top 	; y < bottom ; y+=4){
					for( uint j = 0 ; j < 4 ; ++j ) {
						if( !jpeg::DecodeLine(srcb) ){
							SyncDCache( idata, width*height*4 );
							jpeg::DecodeEnd();
							return -1;
						}
						src = (uint32 *) srcb;
						for ( uint x = left	; x < right ; x+=4){
							for (uint i = 0 ; i < 4 ; ++i) {
								*(dst + dstInd) = uint16( ((*src) >> 16)   );
								*(dst + dstInd+16) = uint16(  (*src) & 0xFFFF	);
								src++;
								dstInd ++;
							}
							dstInd += 28;
						}
						dstInd -= subLineDec;
					}
					dstInd += lineDec;
				}
				SyncDCache( idata, width*height*4 );
			}
			else if ( jdesc.numco==3 && jdesc.bpp==2)
			{
				isJpeg16 = TRUE;
				uint16 *dst			= (uint16 * )idata;
				byte   *srcb		= NULL;
				Memset(dst,0xFF,width*height);
				uint   pad 		= (width - jdesc.width)*4;
				for ( uint y = 0 ; y < jdesc.height ; y++){
					if( !jpeg::DecodeLine(srcb) ){
						SyncDCache( idata, width*height*2 );
						jpeg::DecodeEnd();
						return -1;
					}
					Memcpy(dst,srcb,jdesc.width * 2);	
					dst += jdesc.width;
					dst += pad * ( (y&0x3) == 3 ); // 1 line on 4
				}
				SyncDCache( idata, width*height*2 );
			}
			else
			{
				return -1;
			}
		}
		jpeg::DecodeEnd();
		return errcode;
	}
};



NvkImage*
NvkImage::Create	(	uint				inPSM,
						uint				inWidth,
						uint				inHeight,
						pvoid				inTexelPtr,
						NvImage::CrStatus*	outStatus	)
{
	NV_ASSERT_RETURN_MTH	(NULL, inTexelPtr 			);
	NV_ASSERT_A256			( inTexelPtr 				);	
	NV_ASSERT_RETURN_MTH	(NULL, inPSM==GX_TF_RGBA8 	);

	if( inWidth<8 || inHeight<8 ) {
		if( outStatus )
			*outStatus = NvImage::CS_TOO_LOW;
		return NULL;
	}

	if( inWidth>1024 || inHeight>1024 ) {
		if( outStatus )
			*outStatus = NvImage::CS_TOO_HIGH;
		return NULL;
	}

	NvImageBase* base = NvEngineNew( NvImageBase );
	
	// allocation for internal image data;	
	uint   backBSize = (inWidth * inHeight * 4)	;
	void * idata = inTexelPtr;
	if (!idata) 
		idata = NvMallocA( backBSize, 32);
		
	if( !base || !idata) {
		if( outStatus )
			*outStatus = NvImage::CS_NOMORE_MEMORY;
		SafeFree(idata);
		SafeFree(base);
		return NULL;
	}

	base->inst.refCpt		= 1;	
	base->accessPixel		= NULL;
	base->autoFreePixels	= TRUE;
	base->drawFrameNo		= ~0U;
	base->width				= inWidth  ;
	base->height			= inHeight ;	
	base->idata				= idata;	
	base->isJpeg16			= FALSE;
	if( outStatus )
		*outStatus = NvImage::CS_SUCCESS;		
	return base->GetKInterface();
}



NvImage*
NvImage::Create	(	uint		inWidth,
					uint		inHeight,
					bool		inUsingCLUT,
					CrStatus*	outStatus				)
{
	if (inUsingCLUT) {
		if( outStatus )
			*outStatus = CS_CLUT_UNSUPPORTED;
		return NULL;		
	}

	if( !IsPow2(inWidth) || !IsPow2(inHeight)) {
		if( outStatus )
			*outStatus = NvImage::CS_POW2_ONLY;
		return NULL;
	}
		
	uint  backBSize = (inWidth * inHeight * 4);
	pvoid backPixel = NvMallocA( backBSize, 32);
	if( !backPixel ) {
		if( outStatus )
			*outStatus = CS_NOMORE_MEMORY;
		return NULL;
	}

	NvkImage* kimg = NvkImage::Create(	GX_TF_RGBA8	,
										inWidth		,
										inHeight	,
										backPixel	,
										outStatus	);
	if( !kimg ) {
		NvFree( backPixel );
		return NULL;
	}

	return kimg->GetInterface();
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvImage::TYPE = 0xA7C40B42;	// CRC("NvImage")

NVITF_MTH0(		Image,	NvInterface*,			GetBase											)
NVITF_CAL0(		Image,	void,					AddRef											)
NVITF_MTH0(		Image,	uint,					GetRefCpt										)
NVITF_CAL0(		Image,	void,					Release											)
NVITF_MTH0(		Image,	uint32,					GetRscType										)
NVITF_MTH0(		Image,	uint32,					GetRscUID										)
NVITF_MTH0(		Image,	NvBitmap*,				GetBitmap										)
NVITF_MTH0(		Image,	NvkImage*,				GetKInterface									)
NVITF_MTH0(		Image,	uint32,					GetWidth										)
NVITF_MTH0(		Image,	uint32,					GetHeight										)
NVITF_MTH0(		Image,	NvBitmap::AlphaStatus,	GetAlphaStatus									)
NVITF_MTH1(		Image,	bool,					CreateAccess,		NvImage::Region&			)
NVITF_MTH2(		Image,	pvoid,					GetPixelAccess,		uint&,	Psm&				)
NVITF_MTH1(		Image,	uint32*,				GetClutAccess,		Psm&						)
NVITF_CAL1(		Image,	void,					ValidatePixelAccess, NvImage::Region&			)
NVITF_CAL0(		Image,	void,					ValidateClutAccess								)
NVITF_CAL0(		Image,	void,					ReleaseAccess									)

NVKITF_MTH0(	Image,	NvInterface*,			GetBase											)
NVKITF_CAL0(	Image,	void,					AddRef											)
NVKITF_MTH0(	Image,	uint,					GetRefCpt										)
NVKITF_CAL0(	Image,	void,					Release											)
NVKITF_MTH0(	Image,	uint32,					GetRscType										)
NVKITF_MTH0(	Image,	uint32,					GetRscUID										)
NVKITF_MTH0(	Image,	NvImage*,				GetInterface									)
NVKITF_MTH0(	Image,	NvBitmap*,				GetBitmap										)
NVKITF_MTH0(	Image,	uint32,					GetWidth										)
NVKITF_MTH0(	Image,	uint32,					GetHeight										)
NVKITF_MTH0(	Image,	NvBitmap::AlphaStatus,	GetAlphaStatus									)
NVKITF_CAL0(	Image,	void,					UpdateBeforeDraw								)
NVKITF_MTH2(	Image,	int,					UpdateJpeg,		byte*,	uint					)
NVKITF_MTH0(	Image,	bool,					IsDrawing										)
NVKITF_CAL1(	Image,	void,					SetAutoFreePixels,		bool					)





