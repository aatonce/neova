/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkMesh[PC].h>
#include <Kernel/Common/NvRscFactory_SHR.h>
#include <NvMorphTarget.h>
#include <NvDpyManager.h>
#include <NvBitmap.h>



//#define		DISABLE_LOCSKINNING
//#define		DISABLE_NRMSKINNING
//#define		DISABLE_MORPHING
//#define		DISABLE_DOUBLE_BUFFER_SKN_MOR
//#define		DISABLE_SHDSKINNING
//#define		FORCE_NRMSKINNING


namespace
{

	RscFactory*	meshFactory = NULL;


	sysvector<Vec3>		locTmpA;
	sysvector<Vec3>		nrmTmpA;


	int
	_FindBoneByName	(	uint32*			inBoneNameA,
						uint			inCount,
						uint32			inBoneName	)
	{
		for( uint i = 0 ; i < inCount ; i ++ ) {
			if( inBoneNameA[i] == inBoneName )
				return i;
		}
		return -1;
	}


	Hrdw::IndexBuffer
	_InitIBData	(	uint32		inBSize,
					pvoid		inData,
					bool		inI16FMT		)
	{
				
		Hrdw::IndexBuffer		ib;
		BYTE *					pib;
		if(!inBSize || !inData )
			return NULL;

		ib = Hrdw::CreateIndexBuffer(	inBSize,( inI16FMT ? Hrdw::NV_INDEX16 : Hrdw::NV_INDEX32 ),Hrdw::NV_STATIC);

		uint8 * ibData = (uint8*)inData;
		pib = (BYTE *) Hrdw::LockIndexBuffer(ib,0,0,0);

		Memcpy( pib, ibData, inBSize );
		Hrdw::UnlockIndexBuffer(ib);
		return ib;
	}


	Hrdw::VertexBuffer
	_InitVBData	(	uint32		inBSize,
					pvoid		inData,
					bool		inDynamic = FALSE	)
	{
		Hrdw::VertexBuffer		vb;
		BYTE *					pvb;
		if(!inBSize || !inData )
			return NULL;

		vb = Hrdw::CreateVertexBuffer(inBSize,
									  (inDynamic ? Hrdw::NV_DYNAMIC : Hrdw::NV_STATIC));

		uint8 * vbData = (uint8*) inData;
		pvb = (BYTE *)Hrdw::LockVertexBuffer(vb,0,0,0);
		Memcpy( pvb, vbData, inBSize );
		Hrdw::UnlockVertexBuffer(vb);
		
		return vb;
	}

}





//
// BASE


struct NvMeshBase : public RscFactory_SHR::Instance
{
	#include <Projects\Plugin NovaPC\comp_NvMesh_format.h>

	struct MorphSlot {
		NvMorphTarget*			target;
		float					weight;
		float					cur_weight;
	};

	NvMesh						itf;
	NvkMesh						kitf;

	NvkMesh::BunchHrdwData		hrdwData;

	Matrix*						skinBlendA;
	nv::vector<MorphSlot>		morphSlotA;
	Vec3A						morphCacheA;
	bool						morphCacheValidity;
	uint						morphUnprocFrameCpt;
	
	ShdVtx*						shadowingSkin;

	uint						enflags;		// enabled flags
	uint						validflags;		// valid flags
	uint						updflags;		// toupdate flags

	NvBitmap**				lockBitmapA;		// NvBitmap* for each surface

	virtual	~	NvMeshBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)


	void _ReleaseD3DData(){
		if (hrdwData.vbCol)		Hrdw::ReleaseVertexBuffer(hrdwData.vbCol)	;
		if (hrdwData.vbNrm)		Hrdw::ReleaseVertexBuffer(hrdwData.vbNrm)	;
		if (hrdwData.vbTex)		Hrdw::ReleaseVertexBuffer(hrdwData.vbTex)	;
		if (hrdwData.vbLoc)		Hrdw::ReleaseVertexBuffer(hrdwData.vbLoc)	;
		if (hrdwData.ib)		Hrdw::ReleaseIndexBuffer (hrdwData.ib)		;
		if (hrdwData.vbLocDB)	Hrdw::ReleaseVertexBuffer(hrdwData.vbLocDB)	;
		if (hrdwData.vbNrmDB)	Hrdw::ReleaseVertexBuffer(hrdwData.vbNrmDB)	;
		hrdwData.vbCol		= 0;
		hrdwData.vbNrm		= 0;
		hrdwData.vbTex		= 0;
		hrdwData.vbLoc		= 0;
		hrdwData.ib			= 0;
		hrdwData.vbLocDB	= 0;
		hrdwData.vbNrmDB	= 0;
	}

	void _InitD3DData(){

		bool dynamicLoc =	(hdr()->locProcess != 0 && hdr()->locProcess->src !=0);
		bool dynamicNrm =	(hdr()->nrmProcess != 0 && hdr()->nrmProcess->src !=0);

		hrdwData.vbLoc  =	_InitVBData( hdr()->vtxDataSectionSize, (uint8*)inst.dataptr + hdr()->vtxDataSectionBOffset, dynamicLoc );
		hrdwData.vbNrm  =	(hdr()->nrmDataSectionSize!=0)? 
							_InitVBData( hdr()->nrmDataSectionSize, (uint8*)inst.dataptr + hdr()->nrmDataSectionBOffset, dynamicNrm ) 
							: 0;
		hrdwData.vbTex  =	(hdr()->texDataSectionSize!=0)? 
							_InitVBData( hdr()->texDataSectionSize, (uint8*)inst.dataptr + hdr()->texDataSectionBOffset) 
							: 0;
		hrdwData.vbCol  =	(hdr()->colDataSectionSize!=0)? 
							_InitVBData( hdr()->colDataSectionSize, (uint8*)inst.dataptr + hdr()->colDataSectionBOffset) 
							: 0;
		
		hrdwData.ib     =	_InitIBData( hdr()->idxDataSectionSize , (uint8*)inst.dataptr + hdr()->idxDataSectionBOffset,
										(hdr()->isIdx16format!=0) );	
#ifndef DISABLE_DOUBLE_BUFFER_SKN_MOR
		if (dynamicLoc)
			hrdwData.vbLocDB = _InitVBData( hdr()->vtxDataSectionSize, (uint8*)inst.dataptr + hdr()->vtxDataSectionBOffset, dynamicLoc );
		else
#endif
			hrdwData.vbLocDB = 0;
#ifndef DISABLE_DOUBLE_BUFFER_SKN_MOR
		if ( (hdr()->nrmDataSectionSize!=0) && dynamicNrm)
			hrdwData.vbNrmDB = _InitVBData( hdr()->nrmDataSectionSize, (uint8*)inst.dataptr + hdr()->nrmDataSectionBOffset, dynamicNrm );
		else 
#endif
			hrdwData.vbNrmDB = 0;

	}

	Header*
	hdr			(		)
	{
		return (Header*)inst.dataptr;
	}

	void
	_Init		(		)
	{
		skinBlendA			= NULL;
		morphCacheValidity	= FALSE;
		validflags	= 0;
		updflags	= 0;
		
		enflags	 = uint(NvMesh::EN_DEFAULT);
		#ifdef FORCE_NRMSKINNING
		enflags |= NvMesh::EN_NRM_SKINNING;
		#endif

		_InitD3DData();
	}

	void
	_Shut		(		)
	{
		UnrefBitmaps();
		UnmapSkeleton();
		SetMorphSlotCpt( 0 );
		_ReleaseD3DData();

	}

	void
	AddRef		(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release		(		)
	{
		if( inst.refCpt > 1 )
			inst.refCpt--;
		else
			meshFactory->FreeInstance( &itf );
	}

	uint32
	GetRscType	(		)
	{
		return NvMesh::TYPE;
	}

	uint32
	GetRscUID	(		)
	{
		return inst.uid;
	}

	NvMesh*
	GetInterface	(		)
	{
		return &itf;
	}

	NvkMesh*
	GetKInterface	(		)
	{
		return &kitf;
	}

	uint
	GetSurfaceCpt	(		)
	{
		return hdr()->surfCpt;
	}

	bool
	GetSurfaceShading	(	NvkMesh::Shading&	outShading,
							uint				inSurfaceNo		)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(Surface) == sizeof(NvkMesh::Shading) );
		if( inSurfaceNo >= hdr()->surfCpt )
			return FALSE;
		outShading = *((NvkMesh::Shading*) (hdr()->surfA+ inSurfaceNo));
		return TRUE;
	}

	uint32
	GetSurfaceNameCRC	(	uint				inSurfaceNo		)
	{
		if( inSurfaceNo >= hdr()->surfCpt )
			return 0;
		Surface* surf = hdr()->surfA + inSurfaceNo;
		return surf->nameCRC;
	}

	uint32
	GetSurfaceBitmapUID	(	uint				inSurfaceNo		)
	{
		if( inSurfaceNo >= hdr()->surfCpt )
			return 0;
		Surface* surf = hdr()->surfA + inSurfaceNo;
		return surf->bitmapUID;
	}

	Box3*
	GetBBox		(		)
	{
		return &hdr()->bbox;
	}

	uint32
	GetNameCRC	(		)
	{
		return hdr()->nameCRC;
	}

	bool
	IsSkinnable		(	)
	{
		return hdr()->skinnable;
	}

	bool
	IsNrmSkinnable		(	)
	{
		return hdr()->nrmskinnable;
	}

	bool
	IsMorphable		(	)
	{
		return hdr()->morphable;
	}

	bool
	IsLightable		(	)
	{
		return hdr()->lightable;
	}

	bool
	IsShadowing		(	)
	{
		return hdr()->shadowing;
	}

	bool
	IsFullOpaque	(	)
	{
		return hdr()->fullopaque;
	}

	void
	Enable		(	uint32		inEnableFlags	)
	{
		enflags |= inEnableFlags;
	}

	void
	Disable		(	uint32		inEnableFlags	)
	{
		enflags &= ~inEnableFlags;
	}

	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
		enflags = inEnableFlags;
	}

	uint32
	GetEnabled		(		)
	{
		return enflags;
	}

	bool
	_DoMapping	(	uint32*					inBoneNameA,
					uint					inCount,
					NvMeshBase::Blend0 *	blend0P,
					uint32			   *	blendMapP,
					uint16			   *	blendCptA	)
	{
		NV_ASSERT( inBoneNameA		);
		NV_ASSERT( inCount			);
		NV_ASSERT( blend0P			);
		NV_ASSERT( blendMapP		);
		NV_ASSERT( blendCptA		);

		uint N0 = blendCptA[0];
		uint N1 = blendCptA[1];
		uint N2 = blendCptA[2];
		uint N3 = blendCptA[3];
		uint N4 = blendCptA[4];

		// Map blend0
		blend0P += N0;

		// Map blend1
		NvMeshBase::Blend1 * blend1P = (NvMeshBase::Blend1*) blend0P;
		for( uint i = 0 ; i < N1 ; i++ )
		{
			int idx = _FindBoneByName( inBoneNameA, inCount, blendMapP[0] );
			if( idx < 0 )
				return FALSE;
			blend1P->midx[0] = idx;
			blendMapP++;
			blend1P++;
		}

		// Map blend2
		NvMeshBase::Blend2 * blend2P = (NvMeshBase::Blend2*) blend1P;
		for( uint i = 0 ; i < N2 ; i++ )
		{
			int idx0 = _FindBoneByName( inBoneNameA, inCount, blendMapP[0] );
			int idx1 = _FindBoneByName( inBoneNameA, inCount, blendMapP[1] );
			if( idx0 < 0 || idx1 < 0 )
				return FALSE;
			blend2P->midx[0] = idx0;
			blend2P->midx[1] = idx1;
			blendMapP += 2;
			blend2P++;
		}

		// Map blend3
		NvMeshBase::Blend3 * blend3P = (NvMeshBase::Blend3*) blend2P;
		for( uint i = 0 ; i < N3 ; i++ )
		{
			int idx0 = _FindBoneByName( inBoneNameA, inCount, blendMapP[0] );
			int idx1 = _FindBoneByName( inBoneNameA, inCount, blendMapP[1] );
			int idx2 = _FindBoneByName( inBoneNameA, inCount, blendMapP[2] );
			if( idx0 < 0 || idx1 < 0 || idx2 < 0 )
				return FALSE;
			blend3P->midx[0] = idx0;
			blend3P->midx[1] = idx1;
			blend3P->midx[2] = idx2;
			blendMapP += 3;
			blend3P++;
		}

		// Map blend4
		NvMeshBase::Blend4 * blend4P = (NvMeshBase::Blend4*) blend3P;
		for( uint i = 0 ; i < N4 ; i++ )
		{
			int idx0 = _FindBoneByName( inBoneNameA, inCount, blendMapP[0] );
			int idx1 = _FindBoneByName( inBoneNameA, inCount, blendMapP[1] );
			int idx2 = _FindBoneByName( inBoneNameA, inCount, blendMapP[2] );
			int idx3 = _FindBoneByName( inBoneNameA, inCount, blendMapP[3] );
			if( idx0 < 0 || idx1 < 0 || idx2 < 0 || idx3 < 0 )
				return FALSE;
			blend4P->midx[0] = idx0;
			blend4P->midx[1] = idx1;
			blend4P->midx[2] = idx2;
			blend4P->midx[3] = idx3;
			blendMapP += 4;
			blend4P++;
		}

		return TRUE;
	}

	bool
	MapSkeleton		(	uint			inBoneCount,
						uint32*			inBoneNameA,
						Matrix*			inBlend16A		)
	{
		UnmapSkeleton();
	
		if(		!IsSkinnable()
			||	!inBoneNameA
			||	!inBoneCount
			||	!inBlend16A	)
			return 0;
	
		// 16 bytes aligned ?
		if( (uint32(inBlend16A)&0xF) != 0 ) {
			NV_DEBUG_WARNING( "NvMesh::MapSkeleton() : inBlend16A must be 16bytes aligned !" );
			return FALSE;
		}
	
		bool res;
	
		// location's blend mapping
		if( hdr()->skinnable && hdr()->locProcess )
		{
			NV_ASSERT( hdr()->locProcess->blend );
			NV_ASSERT( hdr()->locProcess->map   );
			res = _DoMapping(	inBoneNameA,
								inBoneCount,
								hdr()->locProcess->blend,
								hdr()->locProcess->map,
								hdr()->locProcess->blendCpt	);
			if( !res )
				return FALSE;
		}
	
		// normal's blend mapping
		if( hdr()->nrmskinnable && hdr()->nrmProcess )
		{
			NV_ASSERT( hdr()->nrmProcess->blend );
			NV_ASSERT( hdr()->nrmProcess->map   );
			res = _DoMapping(	inBoneNameA,
								inBoneCount,
								hdr()->nrmProcess->blend,
								hdr()->nrmProcess->map,
								hdr()->nrmProcess->blendCpt	);
			if( !res )
				return FALSE;
		}
		// shadowing location mapping ?
		if( shadowingSkin )
		{
			NV_ASSERT( hdr()->shdProcess );
			ShdProcess* shdProc	  = hdr()->shdProcess;
			ShdVtx*		shdVtx	  = shdProc->vtxA;
			ShdVtx*		shdVtx_end= shdVtx + shdProc->vtxCpt;
			ShdVtx*		shdSkin	  = shadowingSkin;
			while( shdVtx != shdVtx_end ) {
				shdSkin->boneCRC = ~0U;
				if( shdVtx->boneCRC ) {
					for( uint i = 0 ; i < inBoneCount ; i++ ) {
						if( inBoneNameA[i] == shdVtx->boneCRC ) {
							shdSkin->boneCRC = i;
							break;
						}
					}
				}
				shdSkin++;
				shdVtx++;
			}
		}

		skinBlendA = inBlend16A;

		// enables skinning
		validflags |= NvMesh::EN_LOC_SKINNING;
		if( hdr()->nrmskinnable )
			validflags |= NvMesh::EN_NRM_SKINNING;
		if( shadowingSkin )
			validflags |= NvMesh::EN_SHD_SKINNING;
		return TRUE;
	}

	void
	UnmapSkeleton	(				)
	{
		skinBlendA = NULL;
		validflags &=  ~( NvMesh::EN_LOC_SKINNING | NvMesh::EN_NRM_SKINNING );	// disables all skinning
	}

	uint
	GetMorphSlotCpt		(			)
	{
		return morphSlotA.size();
	}

	void
	SetMorphSlotCpt		(	uint	inSize		)
	{
		if( !IsMorphable() )
			return;
	
		uint N = morphSlotA.size();
	
		if( N > inSize )
		{
			// Release erased slots
			for( uint i = inSize ; i < N ; i++ )
				SafeRelease( morphSlotA[i].target );
			morphCacheValidity = FALSE;
			if( inSize == 0 ) {
				morphCacheA.free();
				validflags &= ~(NvMesh::EN_MORPHING);		// disables morphing
			}
		}
		else
		{
			MorphSlot defSlot;
			defSlot.target		= NULL;
			defSlot.weight		= 0.0f;
			defSlot.cur_weight	= 0.0f;
			morphSlotA.resize( inSize, defSlot );
		}
	}

	NvMorphTarget*
	GetMorphSlotTarget	(	uint	inSlotNo		)
	{
		if( !IsMorphable() || inSlotNo >= morphSlotA.size() )
			return NULL;
		else
			return morphSlotA[inSlotNo].target;
	}

	void
	SetMorphSlotTarget	(	uint			inSlotNo,
							NvMorphTarget*	inTarget	)
	{
		#ifdef DISABLE_MORPHING
			return;
		#endif
		if(	!IsMorphable() || inSlotNo >= morphSlotA.size() )
			return;

		if( morphSlotA[inSlotNo].target == inTarget )
			return;
	
		if( morphSlotA[inSlotNo].target ) {
			SafeRelease( morphSlotA[inSlotNo].target );
			if( morphSlotA[inSlotNo].cur_weight != 0.0f )
				morphCacheValidity = FALSE;
		}

		if( inTarget ) {
			inTarget->AddRef();
			morphSlotA[inSlotNo].target		= inTarget;
			morphSlotA[inSlotNo].weight		= 0.0f;
			morphSlotA[inSlotNo].cur_weight	= 0.0f;
			validflags |= NvMesh::EN_MORPHING;
		}
	}

	void
	SetMorphSlotWeight	(	uint			inSlotNo,
							float			inWeight	)
	{
		if(		!IsMorphable()
			||	inSlotNo >= morphSlotA.size() )
			return;
		morphSlotA[inSlotNo].weight = inWeight;
	}

	void
	Update				(				)
	{
		// save flags to update before drawing ...
		updflags = enflags & validflags;
	}

	void
	UpdateBeforeDraw		(				)
	{
		// Morphing cache auto-release
		if( !(updflags & NvMesh::EN_MORPHING) && morphCacheValidity ) {
			if( morphSlotA.size()==0 || morphUnprocFrameCpt>120 ) {
				morphCacheA.free();
				morphCacheValidity = FALSE;			
				validflags &= ~(NvMesh::EN_MORPHING);		// disables morphing
				updflags &= validflags;
			}
			morphUnprocFrameCpt++;
		}
	
		if( updflags == 0 )
			return;
	
	
		//
		// Flags filtering
	
		Vec3* locA = NULL;
		Vec3* nrmA = NULL;
	
	
		//
		// Do loc Morphing ?
	
		if( updflags & NvMesh::EN_MORPHING ) {
			if( _ProcessMorphing() ) {
				NV_ASSERT( morphCacheA.size() == hdr()->locCpt );
				NV_ASSERT( morphCacheValidity == TRUE );
				locA = morphCacheA.data();
			}
		}
	
	
		//
		// Do loc skinning ?
	
		if( updflags & NvMesh::EN_LOC_SKINNING ) {
			if( !locA ) {
				NV_ASSERT( hdr()->locProcess && hdr()->locProcess->src );
				locA = hdr()->locProcess->src;
			}
			if( locTmpA.size() < hdr()->locCpt )
				locTmpA.resize( hdr()->locCpt );
#ifndef DISABLE_LOCSKINNING
			_ProcessLocSkinning( locTmpA.data(), locA );
			locA = locTmpA.data();
#endif //DISABLE_LOCSKINNING
		}
	
	
		//
		// Do nrm skinning ?
	
		if( updflags & NvMesh::EN_NRM_SKINNING ) {
			if( !nrmA ) {
				NV_ASSERT( hdr()->nrmProcess && hdr()->nrmProcess->src );
				nrmA = hdr()->nrmProcess->src;
			}
			if( nrmTmpA.size() < hdr()->nrmCpt )
				nrmTmpA.resize( hdr()->nrmCpt );
#ifndef DISABLE_NRMSKINNING
			_ProcessNrmSkinning( nrmTmpA.data(), nrmA );
			nrmA = nrmTmpA.data();
#endif // DISABLE_NRMSKINNING
		}
	
	
		// Apply to	vb !
		if( locA || nrmA ) {
			Vec3* pvb;
#ifndef DISABLE_DOUBLE_BUFFER_SKN_MOR
			Hrdw::VertexBuffer tmpVB; 
#endif
			if( locA )
			{
#ifndef DISABLE_DOUBLE_BUFFER_SKN_MOR
				tmpVB				= hrdwData.vbLocDB;
				hrdwData.vbLocDB	= hrdwData.vbLoc;
				hrdwData.vbLoc		= tmpVB;
#endif
				pvb = (Vec3*) Hrdw::LockVertexBuffer(hrdwData.vbLoc,0,0,Hrdw::NV_DISCARD);
				for( uint j = 0 ; j < hdr()->locIdxDataSectionSize / 2; j++ )
					*pvb++ = locA[*((uint16*)((uint8*)(inst.dataptr) + hdr()->locIdxDataSectionBOffset) + j)];
				Hrdw::UnlockVertexBuffer(hrdwData.vbLoc);
			}

			if( nrmA )
			{
#ifndef DISABLE_DOUBLE_BUFFER_SKN_MOR
				tmpVB				= hrdwData.vbNrmDB;
				hrdwData.vbNrmDB	= hrdwData.vbNrm;
				hrdwData.vbNrm		= tmpVB;
#endif
				//uint16 * nrmIdxP = hdr()->surfA[i].nrmIdx;
				pvb = (Vec3*) Hrdw::LockVertexBuffer(hrdwData.vbNrm,0,0,Hrdw::NV_DISCARD);
				for( uint j = 0 ; j < hdr()->nrmPIdxDataSectionSize / 2 ; j++ )
					*pvb++ = nrmA[*((uint16*)((uint8*)(inst.dataptr) + hdr()->nrmPIdxDataSectionBOffset) + j)];
					//*pvb++ = nrmA[ *nrmIdxP++ ];
				Hrdw::UnlockVertexBuffer(hrdwData.vbNrm);
			}
		}
	
		if( updflags & NvMesh::EN_SHD_SKINNING )
		{
			ProcessShdSkinning();
		}

		updflags = 0;
	}

	bool
	_ProcessMorphing	(				)
	{
	#ifdef DISABLE_MORPHING
		return false;
	#endif
		uint locCpt = hdr()->locCpt;
		NV_ASSERT( locCpt );
	
		// No morphing to do
		uint morphCpt = morphSlotA.size();
		if( morphCpt == 0 )
		{
			if( morphCacheA.size() )
				morphCacheA.free();
			morphCacheValidity = FALSE;
			return FALSE;
		}
	
		if( !morphCacheValidity )
		{
			if( morphCacheA.size() != locCpt )
			{
				morphCacheA.reserve( locCpt );
				morphCacheA.resize( locCpt );
			}
	
			NV_ASSERT( hdr()->locProcess && hdr()->locProcess->src );
			Vec3* locA = hdr()->locProcess->src;
			Memcpy( &morphCacheA[0], locA, sizeof(Vec3)*locCpt );
	
			for( uint i = 0 ; i < morphCpt ; i++ )
				morphSlotA[i].cur_weight = 0.0f;
	
			morphCacheValidity = TRUE;
		}
	
		bool				oneDone = FALSE;
		Matrix			  &	M0		= hdr()->M0;
		Matrix			   wM0;
		float				w, cur_w;
		NvMorphTarget*		targetP;
		Vec3*				morphCacheP = morphCacheA.data();
		Vec3				v, * d;
	
		for( uint i = 0 ; i < morphCpt ; i++ )
		{
			targetP = morphSlotA[i].target;
			w		= morphSlotA[i].weight;
			cur_w	= morphSlotA[i].cur_weight;
	
			if(	!targetP )
				continue;
	
			// Need delta-update ?
			if( w != cur_w )
			{
				float dw = w - cur_w;
				morphSlotA[i].cur_weight = w;
	
				// Weighted TR0
				wM0 = M0 * dw;
	
				NvMorphTarget::VtxTarget * vtxTargetP;
				NvMorphTarget::VtxTarget * vtxTarget_endP;
				vtxTargetP		= targetP->GetVtxTargets();
				vtxTarget_endP	= vtxTargetP + targetP->GetVtxCpt();
				NV_ASSERT( vtxTargetP );

				while( vtxTargetP != vtxTarget_endP )
				{
					d = morphCacheP + vtxTargetP->index;
					Vec3BlendVector( d, &vtxTargetP->delta, &wM0 );
					vtxTargetP++;
				}
			}
	
			if( w != 0.0f )
				oneDone = TRUE;
		}

		morphUnprocFrameCpt = 0;

		return oneDone;
		//return FALSE;
	}

	void
	_ProcessLocSkinning	(	Vec3*		inDstVtxA,
							Vec3*		inSrcVtxA		)
	{
		#ifdef DISABLE_LOCSKINNING
			return ;
		#endif
		NV_ASSERT( IsSkinnable()			);
		NV_ASSERT( skinBlendA				);
		NV_ASSERT( inDstVtxA && inSrcVtxA	);
		NV_ASSERT( hdr()->locProcess && hdr()->locProcess->blend	);
	
		uint N0 = hdr()->locProcess->blendCpt[0];
		uint N1 = hdr()->locProcess->blendCpt[1];
		uint N2 = hdr()->locProcess->blendCpt[2];
		uint N3 = hdr()->locProcess->blendCpt[3];
		uint N4 = hdr()->locProcess->blendCpt[4];
		Vec3 * s, * d;

		// blend0 loc
		Blend0 * blend0P = hdr()->locProcess->blend;
		for( uint i = 0 ; i < N0 ; i++ )
		{
			s = inSrcVtxA + blend0P->idx;
			d = inDstVtxA + blend0P->idx;
			*d = *s;
			blend0P++;
			Vec3Copy(d,s);
		}

		// blend1 loc
		Blend1 * blend1P = (Blend1*) blend0P;
		for( uint i = 0 ; i < N1 ; i++ )
		{
			s = inSrcVtxA + blend1P->idx;
			d = inDstVtxA + blend1P->idx;
			Vec3Apply( d, s, skinBlendA+blend1P->midx[0] );			
			blend1P++;
		}
	
		// blend2 loc
		Blend2 * blend2P = (Blend2*) blend1P;
		for( uint i = 0 ; i < N2 ; i++ )
		{
			s = inSrcVtxA + blend2P->idx;
			d = inDstVtxA + blend2P->idx;
			Vec3Apply( d, s, skinBlendA+blend2P->midx[0], blend2P->w[0] );
			Vec3Blend( d, s, skinBlendA+blend2P->midx[1], blend2P->w[1] );
			blend2P++;
		}
	
		// blend3 loc
		Blend3 * blend3P = (Blend3*) blend2P;
		for( uint i = 0 ; i < N3 ; i++ )
		{
			s = inSrcVtxA + blend3P->idx;
			d = inDstVtxA + blend3P->idx;
			Vec3Apply( d, s, skinBlendA+blend3P->midx[0], blend3P->w[0] );
			Vec3Blend( d, s, skinBlendA+blend3P->midx[1], blend3P->w[1] );
			Vec3Blend( d, s, skinBlendA+blend3P->midx[2], blend3P->w[2] );
			blend3P++;
		}
	
		// blend4 loc
		Blend4 * blend4P = (Blend4*) blend3P;
		for( uint i = 0 ; i < N4 ; i++ )
		{
			s = inSrcVtxA + blend4P->idx;
			d = inDstVtxA + blend4P->idx;
			Vec3Apply( d, s, skinBlendA+blend4P->midx[0], blend4P->w[0] );
			Vec3Blend( d, s, skinBlendA+blend4P->midx[1], blend4P->w[1] );
			Vec3Blend( d, s, skinBlendA+blend4P->midx[2], blend4P->w[2] );
			Vec3Blend( d, s, skinBlendA+blend4P->midx[3], blend4P->w[3] );
			blend4P++;
		}
	}

	void
	_ProcessNrmSkinning	(	Vec3*		inDstNrmA,
							Vec3*		inSrcNrmA		)
	{
		#ifdef DISABLE_NRMSKINNING
			return ;
		#endif

		NV_ASSERT( IsSkinnable()			);
		NV_ASSERT( skinBlendA				);
		NV_ASSERT( inDstNrmA && inSrcNrmA	);
		NV_ASSERT( hdr()->nrmProcess && hdr()->nrmProcess->blend );
	
		uint N0 = hdr()->nrmProcess->blendCpt[0];
		uint N1 = hdr()->nrmProcess->blendCpt[1];
		uint N2 = hdr()->nrmProcess->blendCpt[2];
		uint N3 = hdr()->nrmProcess->blendCpt[3];
		uint N4 = hdr()->nrmProcess->blendCpt[4];
		Vec3 * s, * d;
	
		// blend0 nrm
		Blend0 * blend0P = hdr()->nrmProcess->blend;
		for( uint i = 0 ; i < N0 ; i++ )
		{
			s = inSrcNrmA + blend0P->idx;
			d = inDstNrmA + blend0P->idx;
			*d = *s;
			blend0P++;
		}
	
		// blend1 loc
		Blend1 * blend1P = (Blend1*) blend0P;
		for( uint i = 0 ; i < N1 ; i++ )
		{
			s = inSrcNrmA + blend1P->idx;
			d = inDstNrmA + blend1P->idx;
			Vec3ApplyVector( d, s, skinBlendA+blend1P->midx[0] );
			blend1P++;
		}
	
		// blend2 loc
		Blend2 * blend2P = (Blend2*) blend1P;
		for( uint i = 0 ; i < N2 ; i++ )
		{
			s = inSrcNrmA + blend2P->idx;
			d = inDstNrmA + blend2P->idx;
			Vec3ApplyVector( d, s, skinBlendA+blend2P->midx[0], blend2P->w[0] );
			Vec3BlendVector( d, s, skinBlendA+blend2P->midx[1], blend2P->w[1] );
			blend2P++;
		}
	
		// blend3 loc
		Blend3 * blend3P = (Blend3*) blend2P;
		for( uint i = 0 ; i < N3 ; i++ )
		{
			s = inSrcNrmA + blend3P->idx;
			d = inDstNrmA + blend3P->idx;
			Vec3ApplyVector( d, s, skinBlendA+blend3P->midx[0], blend3P->w[0] );
			Vec3BlendVector( d, s, skinBlendA+blend3P->midx[1], blend3P->w[1] );
			Vec3BlendVector( d, s, skinBlendA+blend3P->midx[2], blend3P->w[2] );
			blend3P++;
		}
	
		// blend4 loc
		Blend4 * blend4P = (Blend4*) blend3P;
		for( uint i = 0 ; i < N4 ; i++ )
		{
			s = inSrcNrmA + blend4P->idx;
			d = inDstNrmA + blend4P->idx;
			Vec3ApplyVector( d, s, skinBlendA+blend4P->midx[0], blend4P->w[0] );
			Vec3BlendVector( d, s, skinBlendA+blend4P->midx[1], blend4P->w[1] );
			Vec3BlendVector( d, s, skinBlendA+blend4P->midx[2], blend4P->w[2] );
			Vec3BlendVector( d, s, skinBlendA+blend4P->midx[3], blend4P->w[3] );
			blend4P++;
		}
	}

	void
	ProcessShdSkinning	(	)
	{
	#ifdef DISABLE_SHDSKINNING
		return ;
	#endif 
		if( !shadowingSkin )
			return;

		NV_ASSERT( IsSkinnable()   );
		NV_ASSERT( IsShadowing()   );
		NV_ASSERT( skinBlendA	   );
		NV_ASSERT( hdr()->shdProcess );
		NV_ASSERT( sizeof(NvkMesh::ShadowingVtx) == sizeof(ShdVtx) );

		ShdProcess* shdProc	  = hdr()->shdProcess;
		ShdVtx*		shdVtx	  = shdProc->vtxA;
		ShdVtx*		shdVtx_end= shdVtx + shdProc->vtxCpt;
		ShdVtx*		shdSkin	  = shadowingSkin;

		while( shdVtx != shdVtx_end ) {
			if( shdSkin->boneCRC != ~0U ) {
				Vec3Apply( &shdSkin->location, &shdVtx->location, skinBlendA + shdSkin->boneCRC );
				Vec3ApplyVector( &shdSkin->normal, &shdVtx->normal, skinBlendA + shdSkin->boneCRC );
			}
			shdSkin++;
			shdVtx++;
		}
	}

	uint
	GetBunchCpt			(			)
	{
		return hdr()->bunchCpt;
	}

	uint
	GetBunchDataCpt		(			)
	{
		return hdr()->bunchDataCpt;
	}

	NvkMesh::Bunch*
	GetBunchA			(			)
	{
		return (NvkMesh::Bunch*) hdr()->bunchA;
	}

	Box3*
	GetBunchBBoxA		(			)
	{
		return (Box3*) hdr()->bunchBBoxA;
	}

	NvkMesh::BunchData*
	GetBunchDataA		(			)
	{
		return (NvkMesh::BunchData*) hdr()->bunchDataA;
	}

	NvkMesh::BunchList*
	GetBunchByBunchA	(			)
	{
		return (NvkMesh::BunchList*) hdr()->bunchByBunchA;
	}

	NvkMesh::BunchList*
	GetBunchBySurfA		(			)
	{
		return (NvkMesh::BunchList*) hdr()->bunchBySurfA;
	}

	NvkMesh::BunchHrdwData *
	GetHrdwData	(			){
		if (hrdwData.vbLoc && !Hrdw::VBValidData(hrdwData.vbLoc)){
			BYTE * pvb;				
			BYTE * pib;
			bool dynamicLoc =	(hdr()->locProcess != 0 && hdr()->locProcess->src !=0);
			bool dynamicNrm =	(hdr()->nrmProcess != 0 && hdr()->nrmProcess->src !=0);

			uint8 * vbData = (uint8*)inst.dataptr + hdr()->vtxDataSectionBOffset;
			pvb = (BYTE *)Hrdw::LockVertexBuffer(hrdwData.vbLoc,0,0,(dynamicLoc?Hrdw::NV_DISCARD:0));
			Memcpy( pvb, vbData, hdr()->vtxDataSectionSize );
			Hrdw::UnlockVertexBuffer(hrdwData.vbLoc);
			Hrdw::VBSetValidData(hrdwData.vbLoc);

			if (hrdwData.ib && !Hrdw::IBValidData(hrdwData.ib)){
				uint8 * ibData = (uint8*)inst.dataptr + hdr()->idxDataSectionBOffset;
				pib = (BYTE *) Hrdw::LockIndexBuffer(hrdwData.ib,0,0,0);
				Memcpy( pib, ibData, hdr()->idxDataSectionSize );
				Hrdw::UnlockIndexBuffer(hrdwData.ib);
				Hrdw::IBSetValidData(hrdwData.ib);
			}
			if (hrdwData.vbCol && !Hrdw::VBValidData(hrdwData.vbCol)){
				uint8 * vbData = (uint8*) inst.dataptr + hdr()->colDataSectionBOffset;
				pvb = (BYTE *)Hrdw::LockVertexBuffer(hrdwData.vbCol,0,0,0);
				Memcpy( pvb, vbData, hdr()->colDataSectionSize );
				Hrdw::UnlockVertexBuffer(hrdwData.vbCol);
				Hrdw::VBSetValidData(hrdwData.vbCol);
			}
			if (hrdwData.vbLocDB && Hrdw::VBValidData(hrdwData.vbLocDB)){
				uint8 * vbData = (uint8*) inst.dataptr + hdr()->vtxDataSectionBOffset;
				pvb = (BYTE *)Hrdw::LockVertexBuffer(hrdwData.vbLocDB,0,0,(dynamicLoc?Hrdw::NV_DISCARD:0));
				Memcpy( pvb, vbData, hdr()->vtxDataSectionSize );
				Hrdw::UnlockVertexBuffer(hrdwData.vbLocDB);
				Hrdw::VBSetValidData(hrdwData.vbLocDB);
			}
			if (hrdwData.vbNrm && !Hrdw::VBValidData(hrdwData.vbNrm)){
				uint8 * vbData = (uint8*) inst.dataptr + hdr()->nrmDataSectionBOffset;
				pvb = (BYTE *)Hrdw::LockVertexBuffer(hrdwData.vbNrm,0,0,(dynamicNrm?Hrdw::NV_DISCARD:0));
				Memcpy( pvb, vbData, hdr()->nrmDataSectionSize );
				Hrdw::UnlockVertexBuffer(hrdwData.vbNrm);
				Hrdw::VBSetValidData(hrdwData.vbNrm);
			}
			if (hrdwData.vbNrmDB && !Hrdw::VBValidData(hrdwData.vbNrmDB)){
				uint8 * vbData = (uint8*) inst.dataptr + hdr()->nrmDataSectionBOffset;
				pvb = (BYTE *)Hrdw::LockVertexBuffer(hrdwData.vbNrmDB,0,0,(dynamicNrm?Hrdw::NV_DISCARD:0));
				Memcpy( pvb, vbData, hdr()->nrmDataSectionSize );
				Hrdw::UnlockVertexBuffer(hrdwData.vbNrmDB);
				Hrdw::VBSetValidData(hrdwData.vbNrmDB);
			}
			if (hrdwData.vbTex && !Hrdw::VBValidData(hrdwData.vbTex)){
				uint8 * vbData = (uint8*) inst.dataptr + hdr()->texDataSectionBOffset;
				pvb = (BYTE *)Hrdw::LockVertexBuffer(hrdwData.vbTex,0,0,0);
				Memcpy( pvb, vbData, hdr()->texDataSectionSize );
				Hrdw::UnlockVertexBuffer(hrdwData.vbTex);
				Hrdw::VBSetValidData(hrdwData.vbTex);
			}
		}
		NV_ASSERT(!hrdwData.ib		|| Hrdw::IBValidData(hrdwData.ib) );
		NV_ASSERT(!hrdwData.vbLoc	|| Hrdw::VBValidData(hrdwData.vbLoc) );
		NV_ASSERT(!hrdwData.vbCol	|| Hrdw::VBValidData(hrdwData.vbCol) );
		NV_ASSERT(!hrdwData.vbLocDB || Hrdw::VBValidData(hrdwData.vbLocDB) );
		NV_ASSERT(!hrdwData.vbNrm	|| Hrdw::VBValidData(hrdwData.vbNrm) );
		NV_ASSERT(!hrdwData.vbNrmDB || Hrdw::VBValidData(hrdwData.vbNrmDB) );
		NV_ASSERT(!hrdwData.vbTex	|| Hrdw::VBValidData(hrdwData.vbTex) );
		return &hrdwData;
	}
	
	void
	RefBitmaps	(			)
	{
		if( !lockBitmapA )
			return;
		Surface* surfA   = hdr()->surfA;
		uint	 surfCpt = hdr()->surfCpt;
		for( uint i = 0 ; i < surfCpt ; i++ ) {
			if( !lockBitmapA[i] )
				lockBitmapA[i] = NvBitmap::Create( surfA[i].bitmapUID );
		}
	}

	void
	UnrefBitmaps	(			)
	{
		if( !lockBitmapA )
			return;
		uint surfCpt = hdr()->surfCpt;
		for( uint i = 0 ; i < surfCpt ; i++ )
			SafeRelease( lockBitmapA[i] );
	}

	bool
	GetShadowing		(	uint&						outVtxCpt,
							uint&						outFaceCpt,
							NvkMesh::ShadowingVtx*&		outVtxA,
							NvkMesh::ShadowingFace*&	outFaceA		)
	{
		if( !hdr()->shadowing )
			return FALSE;
		NV_ASSERT( hdr()->shdProcess );
		NV_ASSERT( sizeof(NvkMesh::ShadowingVtx) == sizeof(ShdVtx) );
		NV_ASSERT( sizeof(NvkMesh::ShadowingFace) == sizeof(ShdFace) );
		ShdProcess* shdProc = hdr()->shdProcess;
		outVtxCpt  = shdProc->vtxCpt;
		outFaceCpt = shdProc->faceCpt;
		outVtxA    = (NvkMesh::ShadowingVtx*)  ( shadowingSkin ? shadowingSkin : shdProc->vtxA );
		outFaceA   = (NvkMesh::ShadowingFace*) shdProc->faceA;
		return TRUE;
	}

};




//
// FACTORY

namespace
{

	struct NvMeshFactory : public RscFactory_SHR
	{
		void	RegisterInit	(		)
		{
			locTmpA.Init();
			nrmTmpA.Init();
		}

		void	RegisterShut	(		)
		{
			locTmpA.Shut();
			nrmTmpA.Shut();
		}

		uint32	GetType		(			)
		{
			return NvMesh::TYPE;
		}

		bool	CheckRsc	(	pvoid		inRscData,
								uint32		inBSize		)
		{
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvMeshBase::Header)
					&&	hdr->ver == NV_MESH_CVER					);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvMeshBase::Header * hdr = (NvMeshBase::Header*) inRscData;
			if (hdr->locProcess) {
				TranslatePointer(&hdr->locProcess,			inBOffset);
				TranslatePointer(&hdr->locProcess->src,		inBOffset);
				TranslatePointer(&hdr->locProcess->blend,	inBOffset);
				TranslatePointer(&hdr->locProcess->map,		inBOffset);
			}
			if (hdr->nrmProcess) {
				TranslatePointer(&hdr->nrmProcess,			inBOffset);
				TranslatePointer(&hdr->nrmProcess->src,		inBOffset);
				TranslatePointer(&hdr->nrmProcess->blend,	inBOffset);
				TranslatePointer(&hdr->nrmProcess->map,		inBOffset);
			}
			if (hdr->shdProcess) {
				TranslatePointer(&hdr->shdProcess,			inBOffset);
				TranslatePointer(&hdr->shdProcess->vtxA,	inBOffset);
				TranslatePointer(&hdr->shdProcess->faceA,	inBOffset);
			}
			TranslatePointer(&hdr->surfA,				inBOffset);
			TranslatePointer(&hdr->bunchA,				inBOffset);
			TranslatePointer(&hdr->bunchBBoxA,			inBOffset);
			TranslatePointer(&hdr->bunchDataA,			inBOffset);
			TranslatePointer(&hdr->bunchBySurfA,		inBOffset);
			TranslatePointer(&hdr->bunchByBunchA,		inBOffset);
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inRscData;
			// static mesh ?
			return ( hdr->skinnable || hdr->morphable ) ? SHRM_DATA_DUP : SHRM_FULL;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize		)
		{
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inRscData;
			uint supplyBSize = 0;
			if( hdr->surfCpt )
				supplyBSize += sizeof( NvBitmap* ) * hdr->surfCpt;
			if( hdr->shadowing )
				supplyBSize += sizeof(NvMeshBase::ShdVtx) * hdr->shdProcess->vtxCpt;

			NvMeshBase* inst	= NvEngineNewS(NvMeshBase,supplyBSize);
			InitInstanceObject	( inst, inDesc, inRscData, inRscBSize );
			
			uint32 supplyPtr0 = uint32( inst + 1 );
			uint32 supplyPtr  = supplyPtr0;

			inst->lockBitmapA = NULL;
			if( hdr->surfCpt ) {
				uint bsize = sizeof( NvBitmap* ) * hdr->surfCpt;
				inst->lockBitmapA = (NvBitmap**) supplyPtr;
				supplyPtr += bsize;
				Memset( inst->lockBitmapA, 0, bsize );
			}

			inst->shadowingSkin = NULL;
			if( hdr->shadowing ) {
				inst->shadowingSkin = (NvMeshBase::ShdVtx*) supplyPtr;
				uint bsize = sizeof(NvMeshBase::ShdVtx) * hdr->shdProcess->vtxCpt;
				Memcpy( inst->shadowingSkin, hdr->shdProcess->vtxA, bsize );
				supplyPtr += bsize;
			}

			inst->_Init();
			return inst;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvMeshBase* inst = (NvMeshBase*) inInst;
			return &inst->itf;
		}

		void
		ReleaseInstanceObject( 	Desc*		inDesc,
								Instance*	inInst	)
		{
			NvMeshBase* inst = (NvMeshBase*) inInst;
			inst->_Shut();
			NvEngineDelete( inst );
		}
		
	};

}


bool
rscfactory_NvMesh_Reg()
{
	meshFactory = NvEngineNew( NvMeshFactory );
	return RscManager::RegisterFactory( meshFactory );
}



NvMesh *
NvMesh::Create	(	uint32	inUID	)
{
	if( !meshFactory )
		return NULL;
	return (NvMesh*) meshFactory->CreateInstance( inUID );
}




//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvMesh::TYPE = 0xC57DF82A;	// CRC("NvMesh")

NVITF_MTH0(		Mesh,	NvInterface *,			GetBase												)
NVITF_CAL0(		Mesh,	void,					AddRef												)
NVITF_MTH0(		Mesh,	uint,					GetRefCpt											)
NVITF_CAL0(		Mesh,	void,					Release												)
NVITF_MTH0(		Mesh,	uint32,					GetRscType											)
NVITF_MTH0(		Mesh,	uint32,					GetRscUID											)
NVITF_MTH0(		Mesh,	NvkMesh*,				GetKInterface										)
NVITF_MTH0(		Mesh,	uint32,					GetNameCRC											)
NVITF_MTH0(		Mesh,	Box3*,					GetBBox												)
NVITF_MTH0(		Mesh,	uint,					GetSurfaceCpt										)
NVITF_MTH1(		Mesh,	uint32,					GetSurfaceNameCRC,		uint						)
NVITF_MTH1(		Mesh,	uint32,					GetSurfaceBitmapUID,	uint						)
NVITF_CAL0(		Mesh,	void,					RefBitmaps											)
NVITF_CAL0(		Mesh,	void,					UnrefBitmaps										)
NVITF_MTH0(		Mesh,	bool,					IsSkinnable											)
NVITF_MTH0(		Mesh,	bool,					IsNrmSkinnable										)
NVITF_MTH0(		Mesh,	bool,					IsMorphable											)
NVITF_MTH0(		Mesh,	bool,					IsLightable											)
NVITF_MTH0(		Mesh,	bool,					IsShadowing											)
NVITF_MTH0(		Mesh,	bool,					IsFullOpaque										)
NVITF_CAL1(		Mesh,	void,					Enable,					uint32						)
NVITF_CAL1(		Mesh,	void,					Disable,				uint32						)
NVITF_CAL1(		Mesh,	void,					SetEnabled,				uint32						)
NVITF_MTH0(		Mesh,	uint32,					GetEnabled											)
NVITF_MTH3(		Mesh,	bool,					MapSkeleton,			uint, uint32*, Matrix*		)
NVITF_CAL0(		Mesh,	void,					UnmapSkeleton										)
NVITF_MTH0(		Mesh,	uint,					GetMorphSlotCpt										)
NVITF_CAL1(		Mesh,	void,					SetMorphSlotCpt,		uint						)
NVITF_MTH1(		Mesh,	NvMorphTarget*,			GetMorphSlotTarget,		uint						)
NVITF_CAL2(		Mesh,	void,					SetMorphSlotTarget,		uint, NvMorphTarget*		)
NVITF_CAL2(		Mesh,	void,					SetMorphSlotWeight,		uint, float					)
NVITF_CAL0(		Mesh,	void,					Update												)

NVKITF_MTH0(	Mesh,	NvInterface *,			GetBase												)
NVKITF_CAL0(	Mesh,	void,					AddRef												)
NVKITF_MTH0(	Mesh,	uint,					GetRefCpt											)
NVKITF_CAL0(	Mesh,	void,					Release												)
NVKITF_MTH0(	Mesh,	uint32,					GetRscType											)
NVKITF_MTH0(	Mesh,	uint32,					GetRscUID											)
NVKITF_MTH0(	Mesh,	NvMesh*,				GetInterface										)
NVKITF_MTH0(	Mesh,	Box3*,					GetBBox												)
NVKITF_MTH0(	Mesh,	uint32,					GetNameCRC											)
NVKITF_MTH0(	Mesh,	bool,					IsSkinnable											)
NVKITF_MTH0(	Mesh,	bool,					IsNrmSkinnable										)
NVKITF_MTH0(	Mesh,	bool,					IsMorphable											)
NVKITF_MTH0(	Mesh,	bool,					IsLightable											)
NVKITF_MTH0(	Mesh,	bool,					IsFullOpaque										)
NVKITF_MTH0(	Mesh,	bool,					IsShadowing											)
NVKITF_MTH0(	Mesh,	uint,					GetSurfaceCpt										)
NVKITF_MTH2(	Mesh,	bool,					GetSurfaceShading,		NvkMesh::Shading&,	uint	)
NVKITF_MTH0(	Mesh,	uint,					GetBunchCpt											)
NVKITF_MTH0(	Mesh,	uint,					GetBunchDataCpt										)
NVKITF_MTH0(	Mesh,	NvkMesh::Bunch*,		GetBunchA											)
NVKITF_MTH0(	Mesh,	Box3*,					GetBunchBBoxA										)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchData*,	GetBunchDataA										)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchList*,	GetBunchByBunchA									)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchList*,	GetBunchBySurfA										)
NVKITF_CAL0(	Mesh,	void,					RefBitmaps											)
NVKITF_CAL0(	Mesh,	void,					UnrefBitmaps										)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchHrdwData*,GetHrdwData											)
NVKITF_MTH4(	Mesh,	bool,					GetShadowing,			uint&, uint&,
																		NvkMesh::ShadowingVtx*&,
																		NvkMesh::ShadowingFace*&	)
NVKITF_MTH0(	Mesh,	void,					UpdateBeforeDraw									)




