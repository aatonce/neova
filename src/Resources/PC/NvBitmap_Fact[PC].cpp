/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvDpyManager[PC].h>
#include "NvBitmap_Base[PC].h"



namespace
{

	RscFactory*  bitmapFactory = NULL;


	struct Bitmap : public NvBitmapBase
	{
		#include <Projects\Plugin NovaPC\comp_NvBitmap_format.h>

		Hrdw::Texture		tex;
		int					texPaletteIdx;		// <0 => none

		virtual	~	Bitmap	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		Header* hdr ( )
		{
			return (Header*) inst.dataptr;
		}

		void AddRef (			)
		{
			inst.refCpt++;
		}

		uint GetRefCpt		(		)
		{
			return inst.refCpt;
		}
	
		void Release	(			)
		{
			if( inst.refCpt > 1 )
			{
				inst.refCpt--;
			}
			else
			{
				NV_ASSERT( bitmapFactory );
				bitmapFactory->FreeInstance( GetInterface() );
			}
		}
	
		uint32 GetRscType	(			)
		{
			return NvBitmap::TYPE;
		}
	
		uint32 	GetRscUID	(			)
		{
			return inst.uid;
		}
	
		uint32	GetWidth		(			)
		{
			return hdr()->width;
		}

		uint32	GetHeight		(			)
		{
			return hdr()->height;
		}

		uint32	GetWPad ()
		{
			return hdr()->wpad;
		}
		uint32	GetHPad ()
		{
			return hdr()->hpad;
		}

		NvBitmap::AlphaStatus GetAlphaStatus	(			)
		{
			return NvBitmap::AlphaStatus( hdr()->astatus );
		}

		bool
		IsBanner	(	)
		{
			return FALSE;
		}
	
		bool IsMask ( )
		{
			return hdr()->ismask;
		}

		bool Pick ( uint x, uint y )
		{
			if( IsMask() && x<hdr()->width && y<hdr()->height )
			{
				uint8* mbits = (uint8*)( hdr() + 1 );
				uint i = y * hdr()->width + x;
				return ( mbits[i>>3] & (1<<(i&7)) ) != 0;
			}
			return FALSE;
		}

		Hrdw::Texture
		GetTexture		(			)
		{
			return tex;
		}

		int
		GetTextureClutIdx	(		)
		{
			return texPaletteIdx;
		}
	};



	//
	// FACTORY

	struct BitmapFactory : public RscFactory_SHR
	{
		uint32
		GetType	(	)
		{
			return NvBitmap::TYPE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			Bitmap::Header* hdr = (Bitmap::Header*) inRscData;
			return (	inBSize >= sizeof(Bitmap::Header)
					&&	hdr->ver == NV_BITMAP_CVER	);
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_FULL;
		}

		Instance*
		CreateInstanceObject	( Desc*		inDesc,
								  pvoid		inRscData,
								  uint32	inRscBSize	)
		{
			Bitmap* inst = NvEngineNew( Bitmap );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			Hrdw::Texture tex = NULL;

			inst->tex		    = NULL;
			inst->texPaletteIdx = -1;

			if( !inst->hdr()->ismask )
			{
				bool autogenSupported = FALSE;
				if( Hrdw::GetDriverCapability(Hrdw::DRV_AUTOGENMIPMAP) )
				{
					// check for auto gen. mip maps support
						autogenSupported = Hrdw::CheckDeviceFormatForMipMap(Hrdw::TextureFormat(inst->hdr()->pfmt));
				}			

				tex = Hrdw::CreateTexture(	inst->hdr()->width,
											inst->hdr()->height,
											inst->hdr()->mipmaped && autogenSupported ? Hrdw::NV_MipMapMax : Hrdw::NV_MipMapLevel1,
											Hrdw::TextureFormat( inst->hdr()->pfmt ) );
		

				if(tex )
				{
					int pitch;
					uint32 * tptr = (uint32 * ) Hrdw::LockTexture(tex,0,pitch);

					uint8 * bmpData = (uint8*) ( inst->hdr() + 1);
					uint32  bmpSize = inst->hdr()->width * inst->hdr()->height * 4;
					Memcpy( tptr, bmpData, bmpSize );

					Hrdw::UnlockTexture(tex);
					inst->tex = tex;
				}
			}
			
			return inst;
		}

		NvResource*
		GetInstanceITF			(	Instance*	inInst	)
		{
			return ((Bitmap*)inInst)->GetInterface();
		}

		void
		ReleaseInstanceObject	(	Desc*		inDesc,
									Instance*	inInst	)
		{
			Bitmap* inst = (Bitmap*) inInst;
			Hrdw::ReleaseTexture(inst->tex);
			NvEngineDelete( inst );
		}
	};

}



bool
rscfactory_NvBitmap_Reg()
{
	bitmapFactory = NvEngineNew( BitmapFactory );
	return RscManager::RegisterFactory( bitmapFactory );
}



NvBitmap *
NvBitmap::Create	(	uint32	inUID	)
{
	if( !bitmapFactory )
		return NULL;
	else
		return (NvBitmap*) bitmapFactory->CreateInstance( inUID );
}


