/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkSurface[PC].h>
#include <Kernel/Common/NvRscFactory_SHR.h>

#define SIZE_LOC					12
#define	SIZE_TEX					8	
#define SIZE_COL					4
#define SIZE_NOR					12
#define SIZE_KIC					2

#define COEFF_ALLOC_INDEX_BUFFER	2


struct NvSurfaceBase : NvInterface
{
	uint					refCpt;
	NvSurface				itf;
	NvkSurface				kitf;

	uint16					maxSize;
	Hrdw::DeclIndex			vs_decl;
	uint8					components;					// NvSurface::Component bit-mask
	uint8					packMode;
	uint					vbStride;

	bool					needPresent;
	bool					enDBF;
	uint8					enableMask;
	uint8					presentMode;


	
	//Back Buffer
	Hrdw::IndexBuffer		indexBack;					// D3DFMT_INDEX16
	Hrdw::VertexBuffer		vertexBack;	
	uint16	*				idcBack;
	uint16	*				kickBufferBack;
	// Front Buffer
	Hrdw::IndexBuffer		indexFront;					// D3DFMT_INDEX16
	Hrdw::VertexBuffer		vertexFront;
	uint16	*				idcFront;
	uint16	*				kickBufferFront;

	uint16					vertexSizeBack;
	uint16					indexSizeBack;
	uint16					vertexSizeFront;
	uint16					indexSizeFront;

	uint8  *				lockedVBBuffer;
	uint16 *				lockedIBBuffer;
	uint16					curVertexSize;
	uint16 *				lockedKickBuffer;
	uint8					lockedBuffer;				// 0 : No Buffer, 1 : Front, 2 : Back;

	uint32					drawFrameNo;


	virtual				~NvSurfaceBase		(										) {}		// needed by nv::DestructInPlace<T>(ptr)

	NvInterface *		GetBase				(										)
	{
		return this;
	}

	void				AddRef				(										)
	{
		refCpt++;
	}

	uint				GetRefCpt			(										)
	{
		return refCpt;
	}

	void				Release				(										)
	{
		if( refCpt > 1 ){
			refCpt--;
		}
		else if (!IsDrawing()){
			DestroyBuffer(NvkSurface::BF_FRONT);
			DestroyBuffer(NvkSurface::BF_BACK);
			NvEngineDelete( this );
		}
		else {
			DpyManager::AddToGarbager( this );
			return;
		}
	}

	uint32				GetRscType			(										)
	{
		return NvSurface::TYPE;
	}

	uint32				GetRscUID			(										)
	{
		return 0;
	}

	NvSurface*			GetInterface		(										)
	{
		return &itf;
	}

	NvkSurface*			GetKInterface		(										)
	{
		return &kitf;
	}

	uint				GetComponents		(										)
	{
		return components;
	}

	bool				HasComponent		(	NvSurface::Component	inComponent	)
	{
		return (components & inComponent) != 0 ;
	}

	NvSurface::PackMode	GetPackingMode		(										)
	{
		return (NvSurface::PackMode)packMode;
	}

	uint				GetMaxSize			(										)
	{
		return maxSize;
	}

	bool				Reset				(	uint			inMaxSize			)
	{
		if (IsDrawing() || inMaxSize < 3)
			return FALSE;
		DestroyBuffer(NvkSurface::BF_FRONT);
		DestroyBuffer(NvkSurface::BF_BACK);
		lockedBuffer = 0;		
		vertexSizeFront= vertexSizeBack = 0;
		indexSizeFront = indexSizeBack  = 0;
		maxSize		   = inMaxSize;
		return TRUE;
	}

	void				EnableDBF			(	NvSurface::PresentMode	inPresentMode,
												bool					inCreateNow			)
	{
		enDBF		= TRUE;
		presentMode	= inPresentMode;
		if (inCreateNow && !vertexFront) {
			AllocBuffer(NvkSurface::BF_FRONT);
		}
	}

	void				DisableDBF			(	bool			inReleaseNow= FALSE	)
	{
		enDBF = FALSE;
		if (inReleaseNow && vertexFront){
			DestroyBuffer(NvkSurface::BF_FRONT);
		}
	}

	bool				IsEnabledDBF		(										)
	{
		return enDBF;
	}

	bool				HasDBF				(										)
	{
		return vertexFront>0;
	}

	bool				Begin				(	uint			inStartOffset		)
	{
		NV_ASSERT(!lockedBuffer);
		if (!(enableMask & NvkSurface::EN_BEGIN_END)) return FALSE;
		if (!vertexBack) {
			AllocBuffer(NvkSurface::BF_BACK);
		}
		if (vertexFront || IsDrawing()){
			if (!vertexFront){
				if (enDBF) {
					AllocBuffer(NvkSurface::BF_FRONT);
				}
				else 
					return FALSE;
			}

			lockedVBBuffer =(BYTE *)  Hrdw::LockVertexBuffer (vertexFront,0,0,Hrdw::NV_DISCARD);
			if (!lockedVBBuffer) 
				return FALSE;
			/*if (HasComponent(NvSurface::CO_KICK)) {
				lockedIBBuffer =(uint16*) Hrdw::LockIndexBuffer  (indexFront,0,0,Hrdw::NV_DISCARD);
				if (!lockedIBBuffer) {
					Hrdw::UnlockVertexBuffer(vertexFront);
					return FALSE;
				}
			}*/
			lockedKickBuffer = kickBufferFront;
			lockedBuffer = 1;
		}
		else {
			lockedVBBuffer =(BYTE *)  Hrdw::LockVertexBuffer (vertexBack,0,0,Hrdw::NV_DISCARD);
			if (!lockedVBBuffer) 
				return FALSE;
			/*if (HasComponent(NvSurface::CO_KICK)) {
				lockedIBBuffer =(uint16*) Hrdw::LockIndexBuffer  (indexBack,0,0,Hrdw::NV_DISCARD );
				if (!lockedIBBuffer) {
					Hrdw::UnlockVertexBuffer(vertexBack);
					return FALSE;
				}
			}*/
			lockedKickBuffer = kickBufferBack;
			lockedBuffer = 2;
		}
		curVertexSize = inStartOffset;
		return TRUE;
	}

	void				TexCoord			(	const Vec2&		inST				)
	{
		NV_ASSERT( curVertexSize<maxSize );
		NV_ASSERT( lockedVBBuffer   );
		if (!(enableMask & NvkSurface::EN_BEGIN_END)) return ;
		if(curVertexSize>=maxSize || !lockedVBBuffer )
			return;
		Vec2* co  = (Vec2 *) ((lockedVBBuffer + curVertexSize * vbStride ) + GetComponentOffset( NvSurface::CO_TEX ));
		Vec2Copy( co, &inST );
	}

	void				Color				(	uint32			inColor,
												Psm				inFromPSM			)
	{
		NV_ASSERT( curVertexSize<maxSize );
		NV_ASSERT( lockedVBBuffer   );
		if (!(enableMask & NvkSurface::EN_BEGIN_END)) return ;
		if(curVertexSize>=maxSize || !lockedVBBuffer )
			return;		
		uint32* co  = (uint32*) ((lockedVBBuffer + curVertexSize * vbStride ) + GetComponentOffset( NvSurface::CO_COLOR ));
		if( inFromPSM != PSM_ARGB32 )
			inColor = ConvertPSM( PSM_ARGB32, inFromPSM, inColor );
		*co = inColor;
	}

	void				Normal				(	const Vec3&		inNXYZ				)
	{
		NV_ASSERT( curVertexSize<maxSize );
		NV_ASSERT( lockedVBBuffer   );
		if (!(enableMask & NvkSurface::EN_BEGIN_END)) return ;
		if(curVertexSize>=maxSize || !lockedVBBuffer )
			return;		
		Vec3* co  = (Vec3*) ((lockedVBBuffer + curVertexSize * vbStride ) + GetComponentOffset( NvSurface::CO_NORMAL ));			
		Vec3Copy( co, &inNXYZ );
	}

	void				Vertex				(	const Vec3&		inXYZ,
												bool			inKick				)
	{
		NV_ASSERT( curVertexSize<maxSize );
		NV_ASSERT( lockedVBBuffer   );
		if (!(enableMask & NvkSurface::EN_BEGIN_END)) return ;
		if(curVertexSize>=maxSize || !lockedVBBuffer )
			return;		

		lockedKickBuffer[curVertexSize] = ( !inKick  && (curVertexSize > 1) && HasComponent(NvSurface::CO_KICK))?  0 : 1;

		Vec3* co  = (Vec3*) ((lockedVBBuffer + curVertexSize * vbStride ) + GetComponentOffset( NvSurface::CO_LOC));			
		Vec3Copy( (Vec3*)co, &inXYZ );
		curVertexSize++;		
	}

	void				End					(							)
	{
		NV_ASSERT(lockedBuffer);
		if (!(enableMask & NvkSurface::EN_BEGIN_END)) return ;

		if (lockedBuffer == 1){
			/*if (HasComponent(NvSurface::CO_KICK))
				Hrdw::UnlockIndexBuffer (indexFront);*/
			Hrdw::UnlockVertexBuffer(vertexFront);

			if (curVertexSize > vertexSizeFront)
				vertexSizeFront = curVertexSize;
			Hrdw::VBSetValidData(vertexFront);
		}
		else {
			/*if (HasComponent(NvSurface::CO_KICK))
				Hrdw::UnlockIndexBuffer (indexBack);*/
			Hrdw::UnlockVertexBuffer(vertexBack);
			if (curVertexSize > vertexSizeBack)
				vertexSizeBack = curVertexSize;
			Hrdw::VBSetValidData(vertexBack);
		}
		lockedIBBuffer = NULL;
		lockedVBBuffer = NULL;
		lockedBuffer   = 0;
		
		PresentBuffer();
	}
/// Kernel .............................................................................
	void				Enable				(	NvkSurface::EnableFlag	inFlag		)
	{
		enableMask |= inFlag;
	}

	void				Disable				(	NvkSurface::EnableFlag	inFlag		)
	{
		enableMask &= (!inFlag);
	}

	bool				IsEnabled			(	NvkSurface::EnableFlag	inFlag		)
	{
		return (enableMask & inFlag) != 0;
	}

	void				DestroyBuffer		(	NvkSurface::Buffer		inB			)
	{
		if (inB == NvkSurface::BF_FRONT) {
			if (lockedBuffer == 1 && vertexFront){
				Hrdw::UnlockVertexBuffer(vertexFront);
				/*if (HasComponent(NvSurface::CO_KICK))
					Hrdw::UnlockIndexBuffer(indexFront);*/
				lockedBuffer = 0;
			}
			if (vertexFront) {
				Hrdw::ReleaseVertexBuffer( vertexFront );
				if (HasComponent(NvSurface::CO_KICK))
					Hrdw::ReleaseIndexBuffer ( indexFront );
				EngineFree(idcFront);
				EngineFree(kickBufferFront);
			}
			vertexFront			= 0;
			indexFront			= 0;
			idcFront			= NULL;
			kickBufferFront		= NULL;
		}
		else {
			if (lockedBuffer == 2 && vertexBack){
				Hrdw::UnlockVertexBuffer(vertexBack);
				/*if (HasComponent(NvSurface::CO_KICK))
					Hrdw::UnlockIndexBuffer (indexBack);*/
				lockedBuffer = 0;
			}
			if (vertexBack) {
				Hrdw::ReleaseVertexBuffer( vertexBack );
				if (HasComponent(NvSurface::CO_KICK))
					Hrdw::ReleaseIndexBuffer ( indexBack );
				EngineFree(idcBack);
				EngineFree(kickBufferBack);
			}
			vertexBack			= 0;
			indexBack			= 0;
			idcBack				= NULL;
			kickBufferBack		= NULL;
		}
	}

	void				AllocBuffer			(	NvkSurface::Buffer		inB			)
	{
		if (inB == NvkSurface::BF_FRONT) {
			vertexFront = Hrdw::CreateVertexBuffer(maxSize * vbStride,Hrdw::NV_DYNAMIC);
			if (HasComponent(NvSurface::CO_KICK))
				indexFront = Hrdw::CreateIndexBuffer (maxSize*COEFF_ALLOC_INDEX_BUFFER * 2,Hrdw::NV_INDEX16,Hrdw::NV_DYNAMIC); 	
			idcFront		= (uint16 *)EngineMalloc(maxSize * 2);
			kickBufferFront = (uint16 *)EngineMalloc(maxSize * 2);
		}
		else {
			vertexBack = Hrdw::CreateVertexBuffer(maxSize * vbStride, Hrdw::NV_DYNAMIC);
			if (HasComponent(NvSurface::CO_KICK))
				indexBack = Hrdw::CreateIndexBuffer (maxSize*COEFF_ALLOC_INDEX_BUFFER * 2,Hrdw::NV_INDEX16,Hrdw::NV_DYNAMIC); 	
			idcBack			= (uint16 *)EngineMalloc(maxSize * 2);
			kickBufferBack	= (uint16 *)EngineMalloc(maxSize * 2);
		}
	}

	uint				GetComponentOffset	(	NvSurface::Component	inCo		)
	{
		if( inCo == NvSurface::CO_KICK )
			return 0;
		uint co_before = (uint(inCo)-1) & components;
		uint bo = 0;
		if( co_before & NvSurface::CO_LOC )		bo += SIZE_LOC;
		if( co_before & NvSurface::CO_TEX )		bo += SIZE_TEX;
		if( co_before & NvSurface::CO_COLOR )	bo += SIZE_COL;
		if( co_before & NvSurface::CO_NORMAL )	bo += SIZE_NOR;
		return bo;
	}

	uint				GetComponentSize	(	NvSurface::Component	inCo		)
	{
		if( inCo == NvSurface::CO_LOC )		return SIZE_LOC;
		if( inCo == NvSurface::CO_TEX )		return SIZE_TEX;
		if( inCo == NvSurface::CO_COLOR )	return SIZE_COL;
		if( inCo == NvSurface::CO_NORMAL )	return SIZE_NOR;
		if( inCo == NvSurface::CO_KICK )	return SIZE_KIC;
		return 0;
	}

	uint				GetVertexStride		(										)
	{
		return vbStride;
	}

	Hrdw::ElementType 	GetPSM				(										) 
	{
		return Hrdw::NV_COLORT;
	}

	Hrdw::DeclIndex		GetVSDecl			(										)
	{
		return vs_decl;
	}

	Hrdw::VertexBuffer	GetVtxBufferBase	(	NvkSurface::Buffer		inBuffer	)
	{
		if (inBuffer == NvkSurface::BF_BACK){
			return vertexBack;
		}
		else {
			return vertexFront;
		}
	}

	Hrdw::IndexBuffer	GetIdxBufferBase	(	NvkSurface::Buffer		inBuffer	)
	{
		if (inBuffer == NvkSurface::BF_BACK){
			return indexBack;
		}
		else {
			return indexFront;
		}
	}

	uint16*				GetIdcBufferBase	(	NvkSurface::Buffer		inBuffer	)
	{
		if (inBuffer == NvkSurface::BF_BACK){
			return idcBack;
		}
		else {
			return idcFront;
		}
	}

	uint				GetDrawingSize		(	NvkSurface::Buffer		inBuffer,
												uint					inVertexOffset,
												uint					inVertexSize)

	{
		
		if (! HasComponent(NvSurface::CO_KICK) ){
			if ( (inBuffer == NvkSurface::BF_BACK) && 
				 ((inVertexOffset+inVertexSize) > vertexSizeBack || inVertexSize < 3))
				return 0;
			if ( (inBuffer == NvkSurface::BF_FRONT) && 
				 ((inVertexOffset+inVertexSize) > vertexSizeFront || inVertexSize < 3))
				return 0;

			return inVertexSize ;
		}
		else {
			if (inBuffer == NvkSurface::BF_BACK){
				if ((inVertexOffset+inVertexSize) > vertexSizeBack || inVertexSize < 3)	return 0;
				return idcBack[ (inVertexOffset + inVertexSize) -1] - idcBack[inVertexOffset] +1;
			}
			else {
				if ((inVertexOffset+inVertexSize) > vertexSizeFront || inVertexSize < 3)	return 0;
				return idcFront[ (inVertexOffset + inVertexSize) -1] - idcFront[inVertexOffset] +1;
			}
		}
	}

	uint				GetDrawingOffset	(   NvkSurface::Buffer		inBuffer,
												uint					inVertexOffset)
	{
		if (! HasComponent(NvSurface::CO_KICK) ){
			if ( (inBuffer == NvkSurface::BF_BACK) && 
				 (int(inVertexOffset) > vertexSizeBack - 3) )
				return 0;
			if ( (inBuffer == NvkSurface::BF_FRONT) && 
				 (int(inVertexOffset) > vertexSizeFront - 3 ))
				return 0;

			return inVertexOffset ;
		}
		else {
			if (inBuffer == NvkSurface::BF_BACK){
				if (int(inVertexOffset) > vertexSizeBack - 3)	return 0;
				return idcBack[inVertexOffset];
			}
			else {
				if (int(inVertexOffset) > vertexSizeFront - 3 )	return 0;
				return idcFront[inVertexOffset];
			}
		}
	}

	uint				GetVertexSize		(	NvkSurface::Buffer		inBuffer	)
	{
		if (inBuffer == NvkSurface::BF_BACK)	return vertexSizeBack;
		else									return vertexSizeFront;
	}

	void				GenerateIdxFromKick	(	NvkSurface::Buffer		inBuffer	)
	{		
		if (!HasComponent(NvSurface::CO_KICK)) return ;
		
		if (vertexFront){
			indexSizeFront = 0;
			lockedIBBuffer =(uint16*) Hrdw::LockIndexBuffer  (indexFront,0,0,Hrdw::NV_DISCARD );
			NV_ASSERT(lockedIBBuffer);
			for (uint i = 0 ; i < maxSize ; ++i){
				if ( !kickBufferFront[i] && i > 1)  {
					lockedIBBuffer[indexSizeFront] = i-1;
					indexSizeFront++;
				}
				lockedIBBuffer[indexSizeFront] = i;
				idcFront[i] = indexSizeFront;
				indexSizeFront++;
			}
			Hrdw::UnlockIndexBuffer(indexFront);
			Hrdw::IBSetValidData(indexFront);
			NV_ASSERT(indexSizeFront < maxSize * COEFF_ALLOC_INDEX_BUFFER);
		}
		else {
			indexSizeBack = 0;
			lockedIBBuffer =(uint16*) Hrdw::LockIndexBuffer  (indexBack,0,0,Hrdw::NV_DISCARD );
			for (uint i = 0 ; i <  maxSize ; ++i){
				if ( !kickBufferBack[i] && i > 1) {
					lockedIBBuffer[indexSizeBack] = i-1;
					indexSizeBack++;
				}
				lockedIBBuffer[indexSizeBack] = i;
				idcBack[i] = indexSizeBack;
				indexSizeBack++;
			}
			Hrdw::UnlockIndexBuffer(indexBack);
			Hrdw::IBSetValidData(indexBack);
			NV_ASSERT(indexSizeBack < maxSize * COEFF_ALLOC_INDEX_BUFFER);
		}
	}

	void	UpdateBeforeDraw	(										)
	{
		NV_ASSERT( refCpt > 0 );
		if ((enableMask & NvkSurface::EN_PRESENT) && needPresent && vertexFront ) {
			if (presentMode == NvSurface::SM_SWAP) {
				if (enableMask & NvkSurface::EN_GENERATE_IDX) 
					GenerateIdxFromKick(NvkSurface::BF_FRONT);
				Swap(	vertexBack,		vertexFront );	
				Swap(	indexBack,		indexFront);
				Swap(	vertexSizeBack,	vertexSizeFront);
				Swap(	indexSizeBack,	indexSizeFront );
				Swap(	idcBack,		idcFront);
				Swap(	kickBufferBack, kickBufferFront);
			}
			else {
				vertexSizeBack = vertexSizeFront;
				indexSizeBack  = indexSizeFront;
				uint8 *  ptrF = (uint8*) Hrdw::LockVertexBuffer  (vertexFront,0,0,Hrdw::NV_READONLY);
				uint8 *  ptrB = (uint8*) Hrdw::LockVertexBuffer  (vertexBack,0,0,Hrdw::NV_DISCARD );
				NV_ASSERT(ptrF && ptrB);
				Memcpy(ptrB,			ptrF,	vertexSizeBack * vbStride );
				Hrdw::UnlockVertexBuffer(vertexFront);
				Hrdw::UnlockVertexBuffer(vertexBack);
				Memcpy(idcBack,			idcFront,		vertexSizeBack * 2 );
				Memcpy(kickBufferBack,	kickBufferFront,vertexSizeBack * 2);
				if (enableMask & NvkSurface::EN_GENERATE_IDX) 
					GenerateIdxFromKick(NvkSurface::BF_BACK);
				if (Hrdw::VBValidData(vertexFront))
					Hrdw::VBSetValidData(vertexBack);

			}
			needPresent = FALSE;
		}
		else if (enableMask & NvkSurface::EN_GENERATE_IDX){
			GenerateIdxFromKick(NvkSurface::BF_BACK);
		}
		drawFrameNo = DpyManager::GetFrameNo();
	}

	bool				IsDrawing			(										)
	{
		return DpyManager::IsDrawing( drawFrameNo );
	}

	bool				PresentBuffer		(										)
	{
		if( !vertexFront )
			return FALSE;
		needPresent = TRUE;
		return TRUE;		
	}
};


NvSurface*				NvSurface::Create	(	uint			inSize,
												uint			inComponents,
												PackMode		inPackMode			)
{
	if( inSize<3 || (inComponents&CO_LOC)==0 )
		return NULL;

	NvSurfaceBase* inst = NvEngineNew( NvSurfaceBase );
	if( !inst )
		return NULL;
	inst->refCpt			= 1;

	inst->maxSize		=inSize;
	inst->components	=inComponents;					// NvSurface::Component bit-mask
	inst->packMode		=PM_NONE;

	inst->needPresent	=FALSE;
	inst->enDBF			=FALSE;
	inst->enableMask	=NvkSurface::EN_DEFAULT;
	inst->presentMode	=NvSurface::SM_DEFAULT;

	inst->indexBack		=NULL;		
	inst->vertexBack	=NULL;	
	inst->idcBack		=NULL;
	inst->kickBufferBack=NULL;	
	inst->indexFront	=NULL;
	inst->vertexFront	=NULL;
	inst->idcFront		=NULL;
	inst->kickBufferFront=NULL;

	inst->vertexSizeBack=0;
	inst->indexSizeBack	=0;
	inst->vertexSizeFront=0;
	inst->indexSizeFront=0;

	inst->lockedVBBuffer=NULL;
	inst->lockedIBBuffer=NULL;
	inst->curVertexSize	=0;
	inst->lockedBuffer	=0;			// 0 : No Buffer, 1 : Front, 2 : Back;

	inst->drawFrameNo	=~0U;

	{
		inst->vs_decl		= 0;
		inst->vbStride	= 0;
		Hrdw::BeginVertexDeclaration();
		if( inst->components & NvSurface::CO_LOC )	{
			Hrdw::AddToVertexDeclaration(0,inst->vbStride,Hrdw::NV_FLOAT3,Hrdw::NV_POSITION);
			inst->vbStride += SIZE_LOC;
		}
		if( inst->components & NvSurface::CO_TEX )	{
			Hrdw::AddToVertexDeclaration(0,inst->vbStride,Hrdw::NV_FLOAT2,Hrdw::NV_TEXCOORD);
			inst->vbStride += SIZE_TEX;
		}
		if( inst->components & NvSurface::CO_COLOR ) {
			Hrdw::AddToVertexDeclaration(0,inst->vbStride,Hrdw::NV_COLORT,Hrdw::NV_COLOR);				
			inst->vbStride += SIZE_COL;
		}
		if( inst->components & NvSurface::CO_NORMAL ) {
			Hrdw::AddToVertexDeclaration(0,inst->vbStride,Hrdw::NV_FLOAT3,Hrdw::NV_NORMAL);
			inst->vbStride += SIZE_NOR;
		}
		inst->vs_decl = Hrdw::FinalizeVertexDeclaration();
	}
	return &(inst->itf);
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvSurface::TYPE = 0x7D788970;	// CRC("NvSurface")

NVITF_MTH0(		Surface,	NvInterface*,			GetBase										)
NVITF_CAL0(		Surface,	void,					AddRef										)
NVITF_MTH0(		Surface,	uint,					GetRefCpt									)
NVITF_CAL0(		Surface,	void,					Release										)
NVITF_MTH0(		Surface,	uint32,					GetRscType									)
NVITF_MTH0(		Surface,	uint32,					GetRscUID									)
NVITF_MTH0(		Surface,	NvkSurface*,			GetKInterface								)
NVITF_MTH0(		Surface,	uint,					GetComponents								)
NVITF_MTH1(		Surface,	bool,					HasComponent,		NvSurface::Component	)
NVITF_MTH0(		Surface,	NvSurface::PackMode,	GetPackingMode								)
NVITF_MTH0(		Surface,	uint,					GetMaxSize									)
NVITF_CAL2(		Surface,	void,					EnableDBF,			NvSurface::PresentMode, bool)
NVITF_CAL1(		Surface,	void,					DisableDBF,			bool					)
NVITF_MTH0(		Surface,	bool,					IsEnabledDBF								)
NVITF_MTH0(		Surface,	bool,					HasDBF										)
NVITF_MTH1(		Surface,	bool,					Begin,				uint					)
NVITF_CAL1(		Surface,	void,					TexCoord,			const Vec2&				)
NVITF_CAL2(		Surface,	void,					Color,				uint32, Psm				)
NVITF_CAL1(		Surface,	void,					Normal,				const Vec3&				)
NVITF_CAL2(		Surface,	void,					Vertex,				const Vec3&, bool		)
NVITF_CAL0(		Surface,	void,					End											)

NVKITF_MTH0(	Surface,	NvInterface*,			GetBase										)
NVKITF_CAL0(	Surface,	void,					AddRef										)
NVKITF_MTH0(	Surface,	uint,					GetRefCpt									)
NVKITF_CAL0(	Surface,	void,					Release										)
NVKITF_MTH0(	Surface,	uint32,					GetRscType									)
NVKITF_MTH0(	Surface,	uint32,					GetRscUID									)
NVKITF_MTH0(	Surface,	NvSurface*,				GetInterface								)
NVKITF_MTH0(	Surface,	Hrdw::DeclIndex,		GetVSDecl									)

NVKITF_MTH0(	Surface,	uint,					GetComponents								)
NVKITF_MTH1(	Surface,	bool,					HasComponent,		NvSurface::Component	)
NVKITF_MTH0(	Surface,	NvSurface::PackMode,	GetPackingMode								)
NVKITF_MTH0(	Surface,	uint,					GetMaxSize									)
NVKITF_MTH1(	Surface,	bool,					Reset,				uint					)
NVKITF_CAL2(	Surface,	void,					EnableDBF,			NvSurface::PresentMode,bool	)
NVKITF_CAL1(	Surface,	void,					DisableDBF,			bool					)
NVKITF_MTH0(	Surface,	bool,					IsEnabledDBF								)
NVKITF_MTH0(	Surface,	bool,					HasDBF										)

NVKITF_MTH1(	Surface,	uint,					GetComponentSize,	NvSurface::Component	)
NVKITF_MTH0(	Surface,	uint,					GetVertexStride								)
NVKITF_CAL0(	Surface,	void,					UpdateBeforeDraw							)
NVKITF_MTH0(	Surface,	bool,					IsDrawing									)

NVKITF_CAL1(	Surface,	void,					Enable,				NvkSurface::EnableFlag	)
NVKITF_CAL1(	Surface,	void,					Disable,			NvkSurface::EnableFlag	)
NVKITF_MTH1(	Surface,	bool,					IsEnabled,			NvkSurface::EnableFlag	)
NVKITF_CAL1(	Surface,	void,					DestroyBuffer,		NvkSurface::Buffer		)
NVKITF_CAL1(	Surface,	void,					AllocBuffer,		NvkSurface::Buffer		)	
NVKITF_MTH1(	Surface,	Hrdw::VertexBuffer,		GetVtxBufferBase,	NvkSurface::Buffer		)
NVKITF_MTH1(	Surface,	Hrdw::IndexBuffer,		GetIdxBufferBase,	NvkSurface::Buffer		)
NVKITF_MTH1(	Surface,	uint16*,				GetIdcBufferBase,	NvkSurface::Buffer		)
NVKITF_CAL1(	Surface,	void,					GenerateIdxFromKick	,	NvkSurface::Buffer	)
NVKITF_MTH0(	Surface,	bool,					PresentBuffer								)
NVKITF_MTH3(	Surface,	uint,					GetDrawingSize,		NvkSurface::Buffer,uint,uint)
NVKITF_MTH2(	Surface,	uint,					GetDrawingOffset,	NvkSurface::Buffer,uint		)
NVKITF_MTH1(	Surface,	uint,					GetVertexSize,		NvkSurface::Buffer		)



Psm
NvSurface::GetNativeColorPSM	(		)
{
	return PSM_ARGB32;
}


Psm
NvkSurface::GetNativeColorPSM	(		)
{
	return PSM_ARGB32;
}



