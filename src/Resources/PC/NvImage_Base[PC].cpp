/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvBitmap_Base[PC].h"
#include <NvImage.h>
#include <Kernel/PC/NvkImage[PC].h>
#include <Kernel/PC/NvDpyManager[PC].h>

using namespace nv;


#define	JPEG_AS_RGB16




struct NvImageBase : public NvBitmapBase
{
	friend struct NvImage;
	friend struct NvkImage;
	NvImage					itf;
	NvkImage				kitf;

	Hrdw::Texture			tex;
	int						texPaletteIdx;	// <0 => none
	uint					width;
	uint					height;
	Hrdw::TextureFormat		fmt;
	bool					hasCLUT;
	pvoid					accessPixel;
	NvImage::Region			accessRegion;
	NvImage::Region			validRegion;
	uint					validCLUT;


	virtual	~ NvImageBase ( ) {}		// needed by nv::DestructInPlace<T>(ptr)

	void
	AddRef ( )
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt ( )
	{
		return inst.refCpt;
	}

	void
	Release	( )
	{
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			ReleaseAccess();
			Hrdw::ReleaseTexture(tex);
			if( texPaletteIdx >= 0 )
				DpyManager::ReleaseDevPalette( texPaletteIdx );
			NvEngineDelete( this );
		}
	}

	uint32
	GetRscType ( )
	{
		return NvImage::TYPE;
	}

	uint32
	GetRscUID ( )
	{
		return 0;
	}

	NvImage*
	GetInterface ( )
	{
		return &itf;
	}

	NvkImage*
	GetKInterface		(			)
	{
		return &kitf;
	}

	NvBitmap*
	GetBitmap			(			)
	{
		return NvBitmapBase::GetInterface();
	}

	uint32
	GetWidth		(			)
	{
		return width;
	}

	uint32
	GetHeight		(			)
	{
		return height;
	}

	uint32
	GetWPad			(			)
	{
		return 0;
	}
	uint32
	GetHPad		(			)
	{
		return 0;
	}

	NvBitmap::AlphaStatus
	GetAlphaStatus	(			)
	{
		return NvBitmap::AS_OPAQUE;
	}
	
	bool
	IsBanner	(	)
	{
		return FALSE;
	}
	
	bool IsMask ()
	{
		return FALSE;
	}

	bool Pick ( uint , uint )
	{
		return FALSE;
	}

	Hrdw::Texture
	GetTexture		(			)
	{
		return tex;
	}

	int
	GetTextureClutIdx	(		)
	{
		return texPaletteIdx;
	}

	bool
	CreateAccess	(	NvImage::Region&	inRegion	)
	{
		if(		accessPixel
			||	inRegion.x<0
			||	inRegion.y<0
			||	inRegion.w==0
			||	inRegion.h==0
			||	(inRegion.x+inRegion.w) > width
			||	(inRegion.y+inRegion.h) > height	)
			return FALSE;
		accessRegion = inRegion;
		uint bsize;
		// clut ?
		if( hasCLUT )
			bsize = 256*4 + inRegion.w * inRegion.h;
		else
			bsize = inRegion.w * inRegion.h * 4;
		accessPixel = EngineMallocA( bsize, 4 );
		NV_ASSERT( accessPixel );
		Zero( validRegion );
		validCLUT = FALSE;
		return TRUE;
	}

	pvoid
	GetPixelAccess	(	uint&				outLineStride,
						Psm&				outPSM			)
	{
		if( !accessPixel )
			return NULL;
		// clut ?
		if( hasCLUT ) {
			outLineStride = accessRegion.w;
			outPSM		  = PSM_CLUT8;
			return ((uint8*)accessPixel)+256*4;
		} else {
			outLineStride = accessRegion.w * 4;
			outPSM		  = PSM_ARGB32;
			return accessPixel;
		}
	}

	uint32*
	GetClutAccess	(	Psm&		outPSM	)
	{
		if( !accessPixel || !hasCLUT )
			return NULL;
		// Check PALETTEENTRY PSM
		outPSM = PSM_ARGB32;
		return (uint32*)accessPixel;
	}

	void
	ValidatePixelAccess	(	NvImage::Region&		inSubRegion		)
	{
		if( !accessPixel )
			return;

		// Clip sub-region with access-region
		if(		inSubRegion.x > accessRegion.w
			||	inSubRegion.y > accessRegion.h
			||	inSubRegion.w == 0
			||	inSubRegion.h == 0	)
			return;
		inSubRegion.w = Min( inSubRegion.w, accessRegion.w-inSubRegion.x );
		inSubRegion.h = Min( inSubRegion.h, accessRegion.h-inSubRegion.y );
		NV_ASSERT( inSubRegion.w && inSubRegion.h );

		if( validRegion.w == 0 )
		{
			// Init invalid-region
			validRegion = inSubRegion;
		}
		else
		{
			// Merge regions
			uint ix1 = validRegion.x + validRegion.w;
			uint iy1 = validRegion.y + validRegion.h;
			uint sx1 = inSubRegion.x + inSubRegion.w;
			uint sy1 = inSubRegion.y + inSubRegion.h;
			uint mx0 = Min( validRegion.x, inSubRegion.x );
			uint my0 = Min( validRegion.y, inSubRegion.y );
			NV_ASSERT( mx0 < ix1 && mx0 < sx1 );
			NV_ASSERT( my0 < iy1 && my0 < sy1 );
			uint mw  = Max( ix1-mx0, sx1-mx0 );
			uint mh  = Max( iy1-my0, sy1-my0 );
			validRegion.x = mx0;
			validRegion.y = my0;
			validRegion.w = mw;
			validRegion.h = mh;
		}
	}

	void
	ValidateClutAccess	(		)
	{
		if( accessPixel && hasCLUT )
			validCLUT = TRUE;
	}

	void
	ReleaseAccess		(			)
	{
		if( !accessPixel )
			return;
		_Update();
		EngineFree( accessPixel );
		accessPixel = NULL;
	}

	void
	UpdateBeforeDraw	(			)
	{
		_Update();
	}


	void
	_Update			(				)
	{
		if( !accessPixel )
			return;

		// Update texels
		if( validRegion.w )
		{
			int pitch;			
			uint	left	= accessRegion.x + validRegion.x						,
					right	= accessRegion.x + validRegion.x + validRegion.w - 1	,
					top		= accessRegion.y + validRegion.y						,
					bottom	= accessRegion.y + validRegion.y + validRegion.h - 1	;
			uint lockFlags = 0;

			if ( left == 0 && top == 0 && right+1 == width && bottom+1 == height)
			{
				// lock all data in texture;
			//	lockFlags	= Hrdw::NV_DISCARD;
				bottom		= top;
				right       = left;
			}

			uint8* dst = (uint8*)Hrdw::LockTexture(	tex,lockFlags,pitch,
					    		 					left,
													top,
													right,
   													bottom);
			uint lstride;
			Psm  psm;
			uint8* src = (uint8*) GetPixelAccess(lstride,psm);
			NV_ASSERT( src && dst );
			src += validRegion.x * (hasCLUT?1:4);
			for( uint y = 0 ; y < validRegion.h ; y++ ) {
				Memcpy( dst, src, validRegion.w*(hasCLUT?1:4) );
				src += lstride;
				dst += pitch;
			}
			Hrdw::UnlockTexture(tex);
			Hrdw::TexSetValidData(tex);
		}

		// Update CLUT ?
		if( hasCLUT && validCLUT )
		{
			Psm psm;
			uint32 * clut =  GetClutAccess( psm );
			NV_ASSERT( clut );
			Hrdw::SetPaletteEntries(texPaletteIdx,clut);
		}

		// Reset invalid regions
		Zero( validRegion );
		validCLUT = FALSE;
	}



	int UpdateJpeg (	byte*		inJpegAddr,
						uint		inJpegBSize		)
	{
		if( hasCLUT )
			return -1;


		if( !inJpegAddr || !inJpegBSize )
			return -1;


		#if defined(JPEG_AS_RGB16)
		// always switch to 16BPP for jpeg !!
		if( fmt != Hrdw::NV_R5G6B5 )
		{
			ReleaseAccess();
			Hrdw::ReleaseTexture( tex );

			fmt = Hrdw::NV_R5G6B5;

			tex = Hrdw::CreateTexture( width, height, Hrdw::NV_MipMapLevel1, fmt, Hrdw::NV_DYNAMIC );
		}
		#endif


		// begin jpeg decoding
		jpeg::ImageDesc jdesc;
		jpeg::DecodePSM decpsm;

		int				errcode;

		decpsm = (fmt==Hrdw::NV_R5G6B5) ? jpeg::OPSM_5650 : jpeg::OPSM_888X;

		errcode = jpeg::DecodeBegin( jdesc, (byte*)inJpegAddr, inJpegBSize, decpsm );
		NV_ASSERT( errcode==0 );

		if( errcode == 0 )
		{
			if( jdesc.width>width || jdesc.height>height )
				return -1;

			NV_ASSERT( (fmt==Hrdw::NV_R5G6B5 && jdesc.bpp==2) || (fmt==Hrdw::NV_A8R8G8B8 && jdesc.bpp==4) );
			NV_ASSERT( jdesc.numco == 3 );

			int   dxpitch;			
			byte* dxpix = (byte*) Hrdw::LockTexture( tex, 0, dxpitch );
			if( !dxpix )
			{
				jpeg::DecodeEnd();
				return -1;
			}

			byte* jpix;
			int   jpitch = jdesc.bpsl;
			for( uint y=0 ; y<jdesc.height ; y++ )
			{
				if( !jpeg::DecodeLine(jpix) )
				{
					errcode = -1;
					break;
				}
				else
				{
					Memcpy( dxpix, jpix, jdesc.width*jdesc.bpp );
					dxpix += dxpitch;
					jpix  += jpitch;
				}
			}

			Hrdw::UnlockTexture( tex );
			Hrdw::TexSetValidData( tex );
		}

		return errcode;
	}

};



NvImage*
NvImage::Create	(	uint		inWidth,
					uint		inHeight,
					bool		inUsingCLUT,
					CrStatus*	outStatus		)
{
	if( !inWidth || !inHeight ) {
		if( outStatus )
			*outStatus = CS_TOO_LOW;
		return NULL;
	}

	if( int(inWidth) > Hrdw::GetMaxTextureWidth() ) {
		if( outStatus )
			*outStatus = CS_TOO_HIGH;
		return NULL;
	}

	if( int(inHeight) > Hrdw::GetMaxTextureHeight() ) {
		if( outStatus )
			*outStatus = CS_TOO_HIGH;
		return NULL;
	}

	if( Hrdw::GetTextureCapability(Hrdw::TEX_SQUAREONLY) ) {
		//Texture have to be square
    	if( inWidth != inHeight ) {
			if( outStatus )
				*outStatus = CS_SQUARE_ONLY;
			return NULL;
		}
	}

	// no conditions (D3DPTEXTURECAPS_NONPOW2CONDITIONAL)
	if( Hrdw::GetTextureCapability(Hrdw::TEX_POW2) ) {
		// Texture must be a power of 2 in size
		if( !IsPow2(inWidth) || !IsPow2(inHeight) ) {
			if( outStatus )
				*outStatus = CS_POW2_ONLY;
			return NULL;
		}
	}

	// Check 8-indexed format support
	if( !Hrdw::CheckDeviceFormatForDynamic(	Hrdw::NV_P8 ) && inUsingCLUT ) {
		if( outStatus )
			*outStatus = CS_CLUT_UNSUPPORTED;
		return NULL;
	}

	NvImageBase* base   = NvEngineNew( NvImageBase );
	Zero( base->inst );
	base->inst.refCpt	= 1;
	base->width			= inWidth;
	base->height		= inHeight;
	base->fmt			= inUsingCLUT ? Hrdw::NV_P8 : Hrdw::NV_A8R8G8B8;
	base->hasCLUT		= inUsingCLUT;
	base->texPaletteIdx = inUsingCLUT ? DpyManager::AllocDevPalette() : -1;
	base->accessPixel	= NULL;

	base->tex = Hrdw::CreateTexture( base->width, base->height, Hrdw::NV_MipMapLevel1, base->fmt, Hrdw::NV_DYNAMIC );

	if( outStatus )
		*outStatus = CS_SUCCESS;

	return &base->itf;
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvImage::TYPE = 0xA7C40B42;	// CRC("NvImage")

NVITF_MTH0(		Image,	NvInterface *,			GetBase											)
NVITF_CAL0(		Image,	void,					AddRef											)
NVITF_MTH0(		Image,	uint,					GetRefCpt										)
NVITF_CAL0(		Image,	void,					Release											)
NVITF_MTH0(		Image,	uint32,					GetRscType										)
NVITF_MTH0(		Image,	uint32,					GetRscUID										)
NVITF_MTH0(		Image,	NvBitmap*,				GetBitmap										)
NVITF_MTH0(		Image,	NvkImage*,				GetKInterface									)
NVITF_MTH0(		Image,	uint32,					GetWidth										)
NVITF_MTH0(		Image,	uint32,					GetHeight										)
NVITF_MTH0(		Image,	NvBitmap::AlphaStatus,	GetAlphaStatus									)
NVITF_MTH1(		Image,	bool,					CreateAccess,		NvImage::Region&			)
NVITF_MTH2(		Image,	pvoid,					GetPixelAccess,		uint&,	Psm&				)
NVITF_MTH1(		Image,	uint32*,				GetClutAccess,		Psm&						)
NVITF_CAL1(		Image,	void,					ValidatePixelAccess, NvImage::Region&			)
NVITF_CAL0(		Image,	void,					ValidateClutAccess								)
NVITF_CAL0(		Image,	void,					ReleaseAccess									)

NVKITF_MTH0(	Image,	NvInterface *,			GetBase											)
NVKITF_CAL0(	Image,	void,					AddRef											)
NVKITF_MTH0(	Image,	uint,					GetRefCpt										)
NVKITF_CAL0(	Image,	void,					Release											)
NVKITF_MTH0(	Image,	uint32,					GetRscType										)
NVKITF_MTH0(	Image,	uint32,					GetRscUID										)
NVKITF_MTH0(	Image,	NvBitmap*,				GetBitmap										)
NVKITF_MTH0(	Image,	NvImage*,				GetInterface									)
NVKITF_MTH0(	Image,	uint32,					GetWidth										)
NVKITF_MTH0(	Image,	uint32,					GetHeight										)
NVKITF_MTH0(	Image,	NvBitmap::AlphaStatus,	GetAlphaStatus									)
NVKITF_MTH0(	Image,	Hrdw::Texture,			GetTexture										)
NVKITF_MTH0(	Image,	int,					GetTextureClutIdx								)
NVKITF_CAL0(	Image,	void,					UpdateBeforeDraw								)
NVKITF_MTH2(	Image,	int,					UpdateJpeg,		byte*,	uint					)





