/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvAnimCtrl.h"
#include <NvMorphAnim.h>
#include <Kernel/Common/NvRscFactory_SHR.h>



namespace
{
	RscFactory * manimFactory = NULL;
}



//
// BASE




struct NvMorphAnimBase : public RscFactory_SHR::Instance
{
	#include <Projects\Plugin Nova\comp_NvMorphAnim_format.h>

	NvMorphAnim					itf;
	nv::anim::CtrlContext *		ctxtA;

	NvMorphAnimBase		(	)	{}
	~NvMorphAnimBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)

	Header*
	hdr			(		)
	{
		return (Header*)inst.dataptr;
	}

	uint32
	GetRscType	(		)
	{
		return NvMorphAnim::TYPE;
	}

	uint32
	GetRscUID	(		)
	{
		return inst.uid;
	}

	void
	AddRef		(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release		(		)
	{
		if( inst.refCpt > 1 )
			inst.refCpt--;
		else
			manimFactory->FreeInstance( &itf );
	}

	float
	GetBeginTime	(		)
	{
		return hdr()->beginTime;
	}

	float
	GetEndTime		(		)
	{
		return hdr()->endTime;
	}

	float
	GetDuration		(		)
	{
		return GetEndTime()-GetBeginTime();
	}

	uint
	GetTargetCpt	(		)
	{
		return hdr()->targetCpt;
	}

	uint32
	GetTargetName	(	uint	inTargetNo	)
	{
		if( inTargetNo >= GetTargetCpt() )
			return 0;
		else
			return hdr()->targetA[inTargetNo].name;
	}

	bool
	GetTargetWeight		(	float	inT,
							float*	outWeightA,
							uint	inFirstNo,
							uint	inLastNo	)
	{
		if(		!outWeightA
			||	inFirstNo >= GetTargetCpt()
			||	inLastNo < inFirstNo		)
			return FALSE;
	
		if( inLastNo >= GetTargetCpt() )
			inLastNo  = GetTargetCpt() - 1;
	
		nv::anim::Ctrl        * ctrl;
		nv::anim::CtrlContext * ctxt	   = ctxtA + inFirstNo;
		float				  *	out		   = outWeightA;
		Target				  * target     = hdr()->targetA + inFirstNo;
		Target				  * target_end = hdr()->targetA + inLastNo + 1;
	
		while( target != target_end )
		{
			ctrl = (nv::anim::Ctrl*) target->ctrl;
			NV_ASSERT_A32( ctrl );
	
			nv::anim::GetFpCtrlValue( ctrl, ctxt, inT, out );
	
			// limit clamping ?
			if( target->useLimit )
				out[0] = Clamp( out[0], target->minLimit, target->maxLimit );
	
			ctxt++;
			target++;
			out++;
	
			inFirstNo++;
		}
	
		return TRUE;
	}

};





//
// FACTORY



namespace
{

	struct NvMorphAnimFactory : public RscFactory_SHR
	{
		uint32
		GetType		(	)
		{
			return NvMorphAnim::TYPE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvMorphAnimBase::Header * hdr = (NvMorphAnimBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvMorphAnimBase::Header)
					&&	hdr->ver == NV_MORPHANIM_CVER					);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvMorphAnimBase::Header * hdr = (NvMorphAnimBase::Header*) inRscData;
			TranslatePointer( &hdr->targetA, inBOffset );
			for( uint i = 0 ; i < hdr->targetCpt ; i++ )
				TranslatePointer( &hdr->targetA[i].ctrl, inBOffset );
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_DATA_SHR;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvMorphAnimBase * inst = (NvMorphAnimBase*) inInst;
			return &inst->itf;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NvMorphAnimBase::Header* hdr = (NvMorphAnimBase::Header*) inRscData;
			uint supplyBSize = sizeof(nv::anim::CtrlContext) * hdr->targetCpt;

			NvMorphAnimBase* inst = NvEngineNewAS(	NvMorphAnimBase, 16, supplyBSize );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			inst->ctxtA = (nv::anim::CtrlContext*) ( inst+1 );
			Memset( inst->ctxtA, 0, sizeof(nv::anim::CtrlContext)*inst->hdr()->targetCpt );
			
			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvMorphAnimBase * inst = (NvMorphAnimBase*) inInst;
			NvEngineDelete( inst );
		}
	};

}


bool
rscfactory_NvMorphAnim_Reg()
{
	manimFactory = NvEngineNew( NvMorphAnimFactory );
	return RscManager::RegisterFactory( manimFactory );
}



NvMorphAnim *
NvMorphAnim::Create	(	uint32	inUID	)
{
	if( !manimFactory )
		return NULL;
	return (NvMorphAnim*) manimFactory->CreateInstance( inUID );
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvMorphAnim::TYPE = 0x24EEF409;	// CRC("NvMorphAnim")

NVITF_MTH0( MorphAnim, NvInterface*,	GetBase												)
NVITF_CAL0( MorphAnim, void,			AddRef												)
NVITF_MTH0( MorphAnim, uint,			GetRefCpt											)
NVITF_CAL0( MorphAnim, void,			Release												)
NVITF_MTH0( MorphAnim, uint32,			GetRscType											)
NVITF_MTH0( MorphAnim, uint32,			GetRscUID											)
NVITF_MTH0( MorphAnim, float,			GetBeginTime										)
NVITF_MTH0( MorphAnim, float,			GetEndTime											)
NVITF_MTH0( MorphAnim, float,			GetDuration											)
NVITF_MTH0( MorphAnim, uint,			GetTargetCpt										)
NVITF_MTH1( MorphAnim, uint32,			GetTargetName,		uint							)
NVITF_MTH4( MorphAnim, bool,			GetTargetWeight,	float, float*, uint, uint		)



