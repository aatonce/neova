/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvMorphTarget.h>
#include <Kernel/Common/NvRscFactory_SHR.h>


namespace
{
	RscFactory* mtargetFactory = NULL;
}






//
// BASE


struct NvMorphTargetBase : public RscFactory_SHR::Instance
{
	#include <Projects\Plugin Nova\comp_NvMorphTarget_format.h>


	NvMorphTarget	itf;


	NvMorphTargetBase	(	)	{}
	~NvMorphTargetBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)

	Header*
	hdr		(		)
	{
		return (Header*)inst.dataptr;
	}

	uint32
	GetRscType	(		)
	{
		return NvMorphTarget::TYPE;
	}

	uint32
	GetRscUID	(		)
	{
		return inst.uid;
	}

	void
	AddRef		(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release		(		)
	{
		if( inst.refCpt > 1 )
			inst.refCpt--;
		else
			mtargetFactory->FreeInstance( &itf );
	}

	uint32
	GetName		(		)
	{
		return hdr()->name;
	}

	uint32
	GetVtxCpt		(		)
	{
		return hdr()->vtxCpt;
	}

	NvMorphTarget::VtxTarget*
	GetVtxTargets	(		)
	{
		return (NvMorphTarget::VtxTarget*) hdr()->vtxTargetA;
	}

};




namespace
{

	struct NvMorphTargetFactory : public RscFactory_SHR
	{
		uint32
		GetType	(	)
		{
			return NvMorphTarget::TYPE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvMorphTargetBase::Header * hdr = (NvMorphTargetBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvMorphTargetBase::Header)
					&&	hdr->ver == NV_MORPHTARGET_CVER					);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvMorphTargetBase::Header * hdr = (NvMorphTargetBase::Header*) inRscData;
			TranslatePointer( &hdr->vtxTargetA, inBOffset );
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_FULL;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvMorphTargetBase * inst = (NvMorphTargetBase*) inInst;
			return &inst->itf;
		}

		Instance*
		CreateInstanceObject	 (	Desc*		inDesc,
									pvoid		inRscData,
									uint32		inRscBSize	)
		{
			NvMorphTargetBase* inst = NvEngineNew( NvMorphTargetBase );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );
			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvMorphTargetBase* inst = (NvMorphTargetBase*) inInst;
			NvEngineDelete( inst );
		}
	};

}


bool
rscfactory_NvMorphTarget_Reg()
{
	mtargetFactory = NvEngineNew( NvMorphTargetFactory );
	return RscManager::RegisterFactory( mtargetFactory );
}




NvMorphTarget *
NvMorphTarget::Create	(	uint32	inUID	)
{
	if( !mtargetFactory )
		return NULL;
	return (NvMorphTarget*) mtargetFactory->CreateInstance( inUID );
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvMorphTarget::TYPE = 0x9007C441;	// CRC("NvMorphTarget")

NVITF_MTH0(		MorphTarget,	NvInterface*,				GetBase					)
NVITF_CAL0(		MorphTarget,	void,						AddRef					)
NVITF_MTH0(		MorphTarget,	uint,						GetRefCpt				)
NVITF_CAL0(		MorphTarget,	void,						Release					)
NVITF_MTH0(		MorphTarget,	uint32,						GetRscType				)
NVITF_MTH0(		MorphTarget,	uint32,						GetRscUID				)
NVITF_MTH0(		MorphTarget,	uint32,						GetName					)
NVITF_MTH0(		MorphTarget,	uint32,						GetVtxCpt				)
NVITF_MTH0(		MorphTarget,	NvMorphTarget::VtxTarget*,	GetVtxTargets			)



