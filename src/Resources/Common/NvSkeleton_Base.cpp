/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvSkeleton.h>
#include <Kernel/Common/NvRscFactory_SHR.h>


namespace
{
	RscFactory* skelFactory = NULL;
}




//
// BASE


struct NvSkeletonBase : public RscFactory_SHR::Instance
{
	#include <Projects\Plugin Nova\comp_NvSkeleton_format.h>

	NvSkeleton		itf;


	NvSkeletonBase	(	)	{}
	~NvSkeletonBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)

	Header*
	hdr			(		)
	{
		return (Header*)inst.dataptr;
	}

	uint32
	GetRscType	(		)
	{
		return NvSkeleton::TYPE;
	}

	uint32
	GetRscUID	(		)
	{
		return inst.uid;
	}

	void
	AddRef		(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release		(		)
	{
		if( inst.refCpt > 1 )
			inst.refCpt--;
		else
			skelFactory->FreeInstance( &itf );
	}

	Box3*
	GetBBox		(		)
	{
		return &hdr()->bbox;
	}

	uint
	GetBoneCpt		(		)
	{
		return hdr()->boneCpt;
	}

	int16*
	GetBoneParentIdxA		(		)
	{
		return hdr()->parentIdxA;
	}

	int16*
	GetBoneSiblingIdxA		(		)
	{
		return hdr()->siblingIdxA;
	}

	int16*
	GetBoneChildIdxA		(		)
	{
		return hdr()->childIdxA;
	}

	uint32
	GetBoneName		(	uint		inBoneNo		)
	{
		uint N = GetBoneCpt();
		if( inBoneNo > N )
			return 0;
	
		uint32* boneNameP = GetBoneNameA();
		return boneNameP[ inBoneNo ];
	}

	uint32*
	GetBoneNameA	(		)
	{
		return hdr()->boneNameA;
	}

	int
	FindBoneByName		(	uint32			inBoneNameCRC	)
	{
		int N = GetBoneCpt();
		if( !N ) return -1;
		uint32* boneNameP = GetBoneNameA();
		for( int i = 0 ; i < N ; i++ ) {
			if( boneNameP[i] == inBoneNameCRC )
				return i;
		}
		return -1;
	}

	pcstr*
	GetBoneNoteA	(		)
	{
		return (pcstr*)hdr()->boneNoteA;
	}

	TRS*
	GetBoneInitialTRSA		(		)
	{
		return hdr()->boneTRS0A;
	}

	Matrix*
	GetToBoneInitialTRA		(		)
	{
		return hdr()->toBoneTR0A;
	}

	void
	InitState		(	NvSkeleton::State*	inState		)
	{
		if( !inState )
			return;
	
		uint N = GetBoneCpt();
		NV_ASSERT( N > 0 );
	
		if( inState->trsA.size() < N )
			inState->trsA.resize( N );
	
		Memcpy( inState->trsA.data(), GetBoneInitialTRSA(), sizeof(TRS)*N );
	}

	void
	InitTarget		(	NvSkeleton::Target*	inTarget	)
	{
		if( !inTarget )
			return;
	
		uint N = GetBoneCpt();
		NV_ASSERT( N > 0 );
	
		if( inTarget->blendA.size() < N )
			inTarget->blendA.resize( N );
	
		if( inTarget->trsA.size() < N )
			inTarget->trsA.resize( N );
	}

	bool
	ComputeTarget	(	NvSkeleton::Target*	outTarget,
						NvSkeleton::State*	inState		)
	{
		uint N = GetBoneCpt();
		NV_ASSERT( N > 0 );

		if(		!outTarget
			||	!inState
			||	inState->trsA.size()	 < N
			||	outTarget->blendA.size() < N
			||	outTarget->trsA.size()	 < N )
			return FALSE;

		Matrix*		toBoneTR0_A = GetToBoneInitialTRA();
		TRS*		trsA        = inState->trsA.data();
		Matrix*		mA			= outTarget->trsA.data();
		Matrix*		blendA      = outTarget->blendA.data();

		trsA[0].parentIdx = -1;
		MatrixArraySetTRS	( mA,		trsA,				N );
		MatrixArrayMul		( blendA,	toBoneTR0_A,	mA, N );

		return TRUE;
	}

};





//
// FACTORY


namespace
{

	struct NvSkeletonFactory : public RscFactory_SHR
	{
		uint32
		GetType	(	)
		{
			return NvSkeleton::TYPE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvSkeletonBase::Header * hdr = (NvSkeletonBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvSkeletonBase::Header)
					&&	hdr->ver == NV_SKELETON_CVER					);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvSkeletonBase::Header * hdr = (NvSkeletonBase::Header*) inRscData;
			TranslatePointer( &hdr->boneNameA,	 inBOffset );
			TranslatePointer( &hdr->boneNoteA,	 inBOffset );
			TranslatePointer( &hdr->parentIdxA,	 inBOffset );
			TranslatePointer( &hdr->siblingIdxA, inBOffset );
			TranslatePointer( &hdr->childIdxA,   inBOffset );
			TranslatePointer( &hdr->boneTRS0A,   inBOffset );
			TranslatePointer( &hdr->toBoneTR0A,  inBOffset );

			// notes translation
			uint N = hdr->boneCpt;
			uint8** boneNoteA = hdr->boneNoteA;
			while( boneNoteA && N-- ) {
				if( *boneNoteA ) {
					*boneNoteA += inBOffset;
					//pcstr note = (pcstr) *boneNoteA;
				}
				boneNoteA++;
			}
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_FULL;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvSkeletonBase * inst = (NvSkeletonBase*) inInst;
			return &inst->itf;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NvSkeletonBase * inst = NvEngineNew( NvSkeletonBase );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );
			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvSkeletonBase * inst = (NvSkeletonBase*) inInst;
			NvEngineDelete( inst );
		}
	};

}


bool
rscfactory_NvSkeleton_Reg()
{
	skelFactory = NvEngineNew( NvSkeletonFactory );
	return RscManager::RegisterFactory( skelFactory );
}






NvSkeleton *
NvSkeleton::Create	(	uint32	inUID	)
{
	if( !skelFactory )
		return NULL;
	return (NvSkeleton*) skelFactory->CreateInstance( inUID );
}




//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvSkeleton::TYPE = 0x1C788D96;	// CRC("NvSkeleton")

NVITF_MTH0( Skeleton, NvInterface*,				GetBase												)
NVITF_CAL0( Skeleton, void,						AddRef												)
NVITF_MTH0( Skeleton, uint,						GetRefCpt											)
NVITF_CAL0( Skeleton, void,						Release												)
NVITF_MTH0( Skeleton, uint32,					GetRscType											)
NVITF_MTH0( Skeleton, uint32,					GetRscUID											)
NVITF_MTH0( Skeleton, Box3*,					GetBBox												)
NVITF_MTH0( Skeleton, uint,						GetBoneCpt											)
NVITF_MTH0( Skeleton, int16*,					GetBoneParentIdxA									)
NVITF_MTH0( Skeleton, int16*,					GetBoneSiblingIdxA									)
NVITF_MTH0( Skeleton, int16*,					GetBoneChildIdxA									)
NVITF_MTH1( Skeleton, uint32,					GetBoneName,			uint						)
NVITF_MTH0( Skeleton, uint32*,					GetBoneNameA										)
NVITF_MTH1( Skeleton, int,						FindBoneByName,			uint32						)
NVITF_MTH0( Skeleton, pcstr*,					GetBoneNoteA										)
NVITF_MTH0( Skeleton, TRS*,						GetBoneInitialTRSA									)
NVITF_MTH0( Skeleton, Matrix*,					GetToBoneInitialTRA									)
NVITF_CAL1( Skeleton, void,						InitState,				State*						)
NVITF_CAL1( Skeleton, void,						InitTarget,				Target*						)
NVITF_MTH2( Skeleton, bool,						ComputeTarget,			Target*,	State*			)




