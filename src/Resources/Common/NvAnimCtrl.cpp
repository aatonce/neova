/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvAnimCtrl.h"
using namespace nv::anim;



namespace nv { namespace anim {

	#include <Projects\Plugin Nova\comp_kctrl_format.h>

} }



namespace
{

	void	FindKey (	Ctrl*			inCtrl,
						CtrlContext*	ioCtxt,
						float&			outU,
						float			inT		)
	{
		NV_ASSERT( inCtrl );
		NV_ASSERT( ioCtxt );

		int   keySize	= inCtrl->keyFpSize;
		int   keyCpt	= inCtrl->keyCpt;
		float bT		= inCtrl->beginTime;
		float eT		= inCtrl->endTime;
		NV_ASSERT( eT >= bT );
		NV_ASSERT( keyCpt>1 );

		float* keyA		= (float*)( inCtrl+1 );
		#define	KEY_T(N)	keyA[N*keySize]		// first field of key is t !


		//
		// Check ORT

		float dt = eT - bT;
		if( keyCpt==1 || dt <= 0.0f ) {
			ioCtxt->i0 = ioCtxt->i1 = 0;
			outU = 0.0f;
		}

		if( inT < bT )
		{
			// Before
			if( inCtrl->beforeORT == ORT_CYCLE ) {
				while( inT < bT )
					inT += dt;
			} else if( inCtrl->beforeORT == ORT_OSCILLATE ) {
				int cpt = 0;
				while( inT < bT ) {
					inT += dt;
					cpt++;
				}
				if( cpt&1 ) // Flip
					inT = bT + eT - inT;
			} else {
				ioCtxt->i0 = ioCtxt->i1 = 0;
				outU = 0.0f;
				return;
			}
		}
		else if( inT >= eT )
		{
			// After ?
			if( inCtrl->beforeORT == ORT_CYCLE ) {
				while( inT >= eT )
					inT -= dt;
			} else if( inCtrl->beforeORT == ORT_OSCILLATE ) {
				int cpt = 0;
				while( inT >= eT ) {
					inT -= dt;
					cpt++;
				}
				if( cpt&1 )	// Flip
					inT = bT + eT - inT;
			} else {
				ioCtxt->i0 = ioCtxt->i1 = keyCpt-1;
				outU = 1.0f;
				return;
			}
		}
		NV_ASSERT( inT >= bT );
		NV_ASSERT( inT <  eT );


		//
		// Find key range [i0,i1]

		int   i0	= ioCtxt->i0;
		int   i1	= ioCtxt->i1;
		float t0	= KEY_T(i0);
		float t1	= KEY_T(i1);
		// dicho find ?
		if( inT<t0 || inT>t1 || i0==i1 ) {
			i0	= 0;
			i1	= keyCpt-1;
			t0	= KEY_T(i0);
			t1	= KEY_T(i1);
			while( i1 > (i0+1) ) {
				NV_ASSERT( t1 > t0 );
				int   i = (i1+i0)>>1;
				float t = KEY_T(i);
				if( inT < t ) {
					t1 = t;
					i1 = i;
				} else {
					t0 = t;
					i0 = i;
				}
			}
		}

		NV_ASSERT( i0<keyCpt && i1<keyCpt );
		NV_ASSERT( t0<=inT   && t1>=inT );
		NV_ASSERT( t1 > t0 );
		NV_ASSERT( i1 == i0+1 );

		ioCtxt->i0 = i0;
		ioCtxt->i1 = i1;
		outU = (inT-t0)/(t1-t0);
		NV_ASSERT( outU >= 0.0f );
		NV_ASSERT( outU <= 1.0f );
	}


	template <typename KT> inline
	KT*				Key				(	Ctrl*	inCtrl,
										uint	inIdx,
										Quat*			= NULL	)
	{
		NV_ASSERT( inIdx < inCtrl->keyCpt );
		KT* k0 = (KT*)(inCtrl+1);
		return k0+inIdx;
	}

	template <> inline
	RotTCBKey*		Key<RotTCBKey>	(	Ctrl*	inCtrl,
										uint	inIdx,
										Quat*	outQ0			)
	{
		NV_ASSERT( inIdx < inCtrl->keyCpt );
		NV_ASSERT( outQ0 );
		RotTCBKey* k0 = (RotTCBKey*)(inCtrl+1);
		Quat*	   q0 = (Quat*)(k0+inCtrl->keyCpt);
		*outQ0 = q0[inIdx];
		return k0+inIdx;
	}

}



bool
nv::anim::GetLocCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								Vec3*			outLoc,
								Vec3*			outVelocity,
								Vec3*			outSpeedUp		)
{
	NV_ASSERT_A32( inCtrl );
	NV_ASSERT_A32( ioCtxt );

	float u;
	FindKey( inCtrl, ioCtxt, u, inT );

	if( inCtrl->type == TP_LOC_LIN )
	{
		LocLinKey* key = Key<LocLinKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outLoc = key[0].v;
		}
		else
		{
			Vec3Lerp( outLoc, &key[0].v, &key[1].v, u );
		}
	}
	else
	{
		LocBezKey* key = Key<LocBezKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outLoc = key[0].v;
		}
		else
		{
			Vec3 a =   key[0].v*2.0f - key[1].v*2.0f + key[0].outtan      - key[1].intan;
			Vec3 b = - key[0].v*3.0f + key[1].v*3.0f - key[0].outtan*2.0f + key[1].intan;
			Vec3 c =   key[0].outtan;
			Vec3 d =   key[0].v;
			*outLoc = d+u*(c+u*(b+a*u));
		}
	}

	return TRUE;
}


bool
nv::anim::GetRotCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								Quat*			outRot,
								Quat*			outVelocity,
								Quat*			outSpeedUp	)
{
	NV_ASSERT_A32( inCtrl );
	NV_ASSERT_A32( ioCtxt );

	float u;
	FindKey( inCtrl, ioCtxt, u, inT );

	if( inCtrl->type == TP_ROT_LIN )
	{
		RotLinKey* key = Key<RotLinKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outRot = key[0].v;
		}
		else
		{
			QuatSlerp( outRot, &key[0].v, &key[1].v, u );
		}
	}
	else
	{
		Quat k0_q;
		RotTCBKey* key = Key<RotTCBKey>( inCtrl, ioCtxt->i0, &k0_q );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outRot = k0_q;
		}
		else
		{
			Quat q0 = Quat::UNIT;
			Quat q1 = Quat( key[1].angle, key[1].axys );
			QuatSlerp( outRot, &q0, &q1, u );
			QuatMul( outRot, outRot, &k0_q );
		}
	}

	return TRUE;
}


bool
nv::anim::GetSclCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								Vec3*			outScl,
								Vec3*			outVelocity,
								Vec3*			outSpeedUp		)
{
	NV_ASSERT_A32( inCtrl );
	NV_ASSERT_A32( ioCtxt );

	float u;
	FindKey( inCtrl, ioCtxt, u, inT );

	if( inCtrl->type == TP_SCL_LIN )
	{
		SclLinKey* key = Key<SclLinKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outScl = key[0].v;
		}
		else
		{
			Vec3Lerp( outScl, &key[0].v, &key[1].v, u );
		}
	}
	else
	{
		SclBezKey* key = Key<SclBezKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outScl = key[0].v;
		}
		else
		{
			Vec3 a =   key[0].v*2.0f - key[1].v*2.0f + key[0].outtan      - key[1].intan;
			Vec3 b = - key[0].v*3.0f + key[1].v*3.0f - key[0].outtan*2.0f + key[1].intan;
			Vec3 c =   key[0].outtan;
			Vec3 d =   key[0].v;
			*outScl = d+u*(c+u*(b+a*u));
		}
	}

	return TRUE;
}


bool
nv::anim::GetFpCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								float*			outF,
								float*			outVelocity,
								float*			outSpeedUp		)
{
	NV_ASSERT_A32( inCtrl );
	NV_ASSERT_A32( ioCtxt );

	float u;
	FindKey( inCtrl, ioCtxt, u, inT );

	if( inCtrl->type == TP_FLT_LIN )
	{
		FltLinKey* key = Key<FltLinKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outF = key[0].v;
		}
		else
		{
			float dv = key[1].v - key[0].v;
			*outF = key[0].v + u*dv;
		}
	}
	else
	{
		FltBezKey* key = Key<FltBezKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outF = key[0].v;
		}
		else
		{
			float a =   key[0].v*2.0f - key[1].v*2.0f + key[0].outtan      - key[1].intan;
			float b = - key[0].v*3.0f + key[1].v*3.0f - key[0].outtan*2.0f + key[1].intan;
			float c =   key[0].outtan;
			float d =   key[0].v;
			*outF = d+u*(c+u*(b+a*u));
		}
	}

	return TRUE;
}


bool
nv::anim::GetRGBCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								Vec3*			outRGB,
								Vec3*			outVelocity,
								Vec3*			outSpeedUp		)
{
	NV_ASSERT_A32( inCtrl );
	NV_ASSERT_A32( ioCtxt );

	float u;
	FindKey( inCtrl, ioCtxt, u, inT );

	if( inCtrl->type == TP_COL_LIN )
	{
		ColLinKey* key = Key<ColLinKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outRGB = key[0].v;
		}
		else
		{
			Vec3Lerp( outRGB, &key[0].v, &key[1].v, u );
		}
	}
	else
	{
		ColBezKey* key = Key<ColBezKey>( inCtrl, ioCtxt->i0 );
		if( ioCtxt->i0 == ioCtxt->i1 )
		{
			*outRGB = key[0].v;
		}
		else
		{
			Vec3 a =   key[0].v*2.0f - key[1].v*2.0f + key[0].outtan      - key[1].intan;
			Vec3 b = - key[0].v*3.0f + key[1].v*3.0f - key[0].outtan*2.0f + key[1].intan;
			Vec3 c =   key[0].outtan;
			Vec3 d =   key[0].v;
			*outRGB = d+u*(c+u*(b+a*u));
		}
	}

	return TRUE;
}



void
nv::anim::PrintCtrlKeys	(	Ctrl		*	inCtrl	)
{
	NV_ASSERT_A32( inCtrl );
/*
	if( inCtrl->type == TP_ROT_LIN )
	{
		RotLinKey* key = KEY( RotLinKey, inCtrl, 0 );
		for( uint i = 0 ; i < inCtrl->keyCpt ; i++ ) {
			Printf( "[t:%f] %08x\n", key[i].t, uint(key+i) );
			Printf( "    q.x:%f q.y:%f q.z:%f q.w:%f\n",
				key[i].v.x, key[i].v.y, key[i].v.z, key[i].v.w );
		}
	}
	else if( inCtrl->type == TP_ROT_TCB )
	{
		RotTCBKey* key = KEY( RotTCBKey, inCtrl, 0 );
		for( uint i = 0 ; i < inCtrl->keyCpt ; i++ ) {
			Printf( "[t:%f] %08x\n", key[i].t, uint(key+i) );
			Printf( "    tens:%f cont:%f bias:%f\n",
				key[i].tens, key[i].cont, key[i].bias );
			Printf( "    axys.x:%f axys.y:%f axys.z:%f angle:%f\n",
				key[i].axys.x, key[i].axys.y, key[i].axys.z, key[i].angle );
			Printf( "    q.x:%f q.y:%f q.z:%f q.w:%f\n",
				key[i].q.x, key[i].q.y, key[i].q.z, key[i].q.w );
		}
	}
*/
}


