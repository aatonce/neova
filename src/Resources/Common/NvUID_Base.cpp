/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvUID.h>
#include <Kernel/Common/NvRscFactory_SHR.h>
using namespace nv;


#define	NV_UID_CVER		MK_UINT64( 'XXXX', 'NU02' )


namespace
{

	RscFactory*	uidFactory = NULL;

}



//
// BASE


struct NvUIDBase : public RscFactory_SHR::Instance
{

	struct Entry
	{
		uint32		uid;			// entry UID
		uint32		type;			// entry type
		char*		filename;		// c-string name offset
	};


	struct Header
	{
		uint64		ver;
		uint32		flags;
		uint32		count;			// counter
		Entry*		entries;		// array
	};



	NvUID		itf;


			NvUIDBase	(	)	{}
	virtual	~NvUIDBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)


	Header*
	hdr			(		)
	{
		return (Header*)inst.dataptr;
	}


	uint32
	GetRscType	(	)
	{
		return NvUID::TYPE;
	}


	uint32
	GetRscUID	(	)
	{
		return inst.uid;
	}


	void
	AddRef	(	)
	{
		inst.refCpt++;
	}


	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}


	void
	Release	(	)
	{
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else if( uidFactory )
		{
			uidFactory->FreeInstance( &itf );
		}
	}


	uint
	GetRscCpt	(		)
	{
		return hdr()->count;
	}


	bool
	GetRscInfo		(	uint		inNo,
						pcstr*		outName,
						uint32*		outUID,
						uint32*		outTYPE		)
	{
		if( inNo >= hdr()->count )
		{
			return FALSE;
		}
		else
		{
			Entry* e = &hdr()->entries[ inNo ];
			if( outName )		*outName = e->filename;
			if( outUID )		*outUID  = e->uid;
			if( outTYPE )		*outTYPE = e->type;
			return TRUE;
		}
	}


	void
	FindUIDByName	(	UInt32A	&	outUIDA,
						pcstr		inPattern,
						uint		inFlags		)
	{
		outUIDA.clear();
		if( !inPattern || inPattern[0]==0 )
			return;

		uint nb = hdr()->count;
		for( uint i=0 ; i<nb ; i++ )
		{
			Entry* e = &hdr()->entries[ i ];
			NV_ASSERT( e->filename );
			//Printf( "%s: %08x: <%s>\n", inPattern, e->uid, e->filename );

			// Matching ?
			if( libc::Fnmatch(inPattern,e->filename,inFlags) )
				outUIDA.push_back( e->uid );
		}
	}


	pcstr
	FindNameByUID	(	uint32		inUID		)
	{
		if( !inUID )
			return NULL;

		uint nb = hdr()->count;
		for( uint i=0 ; i<nb ; i++ )
		{
			Entry* e = & hdr()->entries[ i ];
			if( e->uid == inUID )
				return e->filename;
		}

		return NULL;
	}


	bool
	FindPath	(	pstr		outPath,
					pcstr		inDirname	)
	{
		int dlen = inDirname ? Strlen(inDirname) : 0;
		if( dlen==0 )
			return FALSE;

		for( uint i=0 ; i < GetRscCpt() ; i++ )
		{
			pcstr fn;
			if( GetRscInfo(i,&fn,NULL,NULL) )
			{
				int flen = fn ? Strlen(fn) : 0;
				for( int j=0 ; j<=(flen-dlen) ; j++ )
				{
					pcstr fnj = fn+j;
					if( Strnicmp(fnj,inDirname,dlen)==0 )
					{
						if( outPath )
						{
							Strcpy( outPath, fn );
							outPath[j+dlen] = 0;
						}
						return TRUE;
					}
				}
			}
		}

		return FALSE;
	}


};




//
// FACTORY


namespace
{

	struct NvUIDFactory : public RscFactory_SHR
	{
		uint32
		GetType	(	)
		{
			return NvUID::TYPE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvUIDBase::Header * hdr = (NvUIDBase::Header*) inRscData;
			bool needEndianCvt = (hdr->flags == 0);

			if( inBSize < sizeof(NvUIDBase::Header) )
				return FALSE;

			uint64 ver = LSBToNative( hdr->ver );
			if( ver != NV_UID_CVER )
				return FALSE;

			return TRUE;
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvUIDBase::Header * hdr = (NvUIDBase::Header*) inRscData;
			bool needEndianCvt = (hdr->flags == 0);
			hdr->flags = 1;

			if( needEndianCvt )
			{
				ConvertLSB( hdr->ver   );
				ConvertLSB( hdr->flags );
				ConvertLSB( hdr->count );
				ConvertLSB( hdr->entries );
			}

			TranslatePointer( &hdr->entries, inBOffset );

			for( uint i=0 ; i<hdr->count ; i++ )
			{
				NvUIDBase::Entry* e = &hdr->entries[i];
				ConvertLSB( e->uid );
				ConvertLSB( e->type );
				ConvertLSB( e->filename );
				TranslatePointer( &e->filename, inBOffset );
			}
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_FULL;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvUIDBase * inst = (NvUIDBase*) inInst;
			return &inst->itf;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NvUIDBase* inst = NvEngineNewA( NvUIDBase, 16 );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );
			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvUIDBase * inst = (NvUIDBase*) inInst;
			NvEngineDelete( inst );
		}
	};

}


bool
rscfactory_NvUID_Reg()
{
	uidFactory = NvEngineNew( NvUIDFactory );
	return RscManager::RegisterFactory( uidFactory );
}




NvUID*
NvUID::Create	(	uint32	inUID	)
{
	if( !uidFactory )
		return NULL;
	return (NvUID*) uidFactory->CreateInstance( inUID );
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvUID::TYPE = 0xF5FADCD5;	// CRC("NvUID")

NVITF_MTH0( UID,	NvInterface*,	GetBase											)
NVITF_CAL0( UID,	void,			AddRef											)
NVITF_MTH0( UID,	uint,			GetRefCpt										)
NVITF_CAL0( UID,	void,			Release											)
NVITF_MTH0( UID,	uint32,			GetRscType										)
NVITF_MTH0( UID,	uint32,			GetRscUID										)
NVITF_MTH0( UID,	uint,			GetRscCpt										)
NVITF_MTH4( UID,	bool,			GetRscInfo,		uint, pcstr*, uint32*, uint32*	)
NVITF_CAL3( UID,	void,			FindUIDByName,	UInt32A&, pcstr, uint			)
NVITF_MTH1( UID,	pcstr,			FindNameByUID,	uint32							)
NVITF_MTH2( UID,	bool,			FindPath,		pstr, pcstr						)




