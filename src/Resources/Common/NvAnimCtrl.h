/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvAnimCtrl_H_
#define	_NvAnimCtrl_H_


#include <Nova.h>


namespace nv { namespace anim
{

	struct Ctrl;

	struct CtrlContext {
		int	i0, i1;
	};


	bool	GetLocCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								Vec3*			outLoc		= NULL,
								Vec3*			outVelocity	= NULL,
								Vec3*			outSpeedUp	= NULL	);

	bool	GetRotCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								Quat*			outRot		= NULL,
								Quat*			outVelocity	= NULL,
								Quat*			outSpeedUp	= NULL	);

	bool	GetSclCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								Vec3*			outScl		= NULL,
								Vec3*			outVelocity	= NULL,
								Vec3*			outSpeedUp	= NULL	);

	bool	GetFpCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								float*			outF		= NULL,
								float*			outVelocity	= NULL,
								float*			outSpeedUp	= NULL	);

	bool	GetRGBCtrlValue	(	Ctrl		*	inCtrl,
								CtrlContext	*	ioCtxt,
								float			inT,
								Vec3*			outRGB		= NULL,
								Vec3*			outVelocity	= NULL,
								Vec3*			outSpeedUp	= NULL	);

	void	PrintCtrlKeys	(	Ctrl		*	inCtrl				);

} }




#endif // _NvAnimCtrl_H_


