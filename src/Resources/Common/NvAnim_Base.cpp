/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "NvAnimCtrl.h"
#include <NvAnim.h>
#include <Kernel/Common/NvRscFactory_SHR.h>



namespace
{
	RscFactory* animFactory = NULL;
}




//
// BASE


struct NvAnimBase : public RscFactory_SHR::Instance
{
	#include <Projects\Plugin Nova\comp_NvAnim_format.h>

	NvAnim						itf;
	nv::anim::CtrlContext *		ctxtA;

	NvAnimBase	(	)	{}
	~NvAnimBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)

	Header*
	hdr				(		)
	{
		return (Header*)inst.dataptr;
	}

	uint32
	GetRscType		(		)
	{
		return NvAnim::TYPE;
	}

	uint32
	GetRscUID		(		)
	{
		return inst.uid;
	}

	void
	AddRef			(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release			(		)
	{
		if( inst.refCpt > 1 )
			inst.refCpt--;
		else
			animFactory->FreeInstance( &itf );
	}

	Box3*
	GetBBox			(		)
	{
		return & hdr()->bbox;
	}

	uint
	GetCtrlMask		(		)
	{
		return hdr()->ctrlMask;
	}

	float
	GetBeginTime	(		)
	{
		return hdr()->beginTime;
	}

	float
	GetEndTime		(		)
	{
		return hdr()->endTime;
	}

	float
	GetDuration		(		)
	{
		return GetEndTime() - GetBeginTime();
	}

	bool
	GetLocValue		(	float		inT,
						Vec3*		outLoc		=NULL,
						Vec3*		outVelocity	=NULL,
						Vec3*		outSpeedUp	=NULL	)
	{
		nv::anim::Ctrl * ctrl = (nv::anim::Ctrl*) hdr()->ctrl[0];
		if( !ctrl )
			return FALSE;
		NV_ASSERT_A32( ctrl );
		return nv::anim::GetLocCtrlValue( ctrl, ctxtA+0, inT, outLoc, outVelocity, outSpeedUp );
	}

	bool
	GetRotValue		(	float		inT,
						Quat*		outRot		=NULL,
						Quat*		outVelocity	=NULL,
						Quat*		outSpeedUp	=NULL	)
	{
		nv::anim::Ctrl * ctrl = (nv::anim::Ctrl*) hdr()->ctrl[1];
		if( !ctrl )
			return FALSE;
		NV_ASSERT_A32( ctrl );
		return nv::anim::GetRotCtrlValue( ctrl, ctxtA+1, inT, outRot, outVelocity, outSpeedUp );
	}

	bool
	GetSclValue		(	float		inT,
						Vec3*		outScl		=NULL,
						Vec3*		outVelocity	=NULL,
						Vec3*		outSpeedUp	=NULL	)
	{
		nv::anim::Ctrl * ctrl = (nv::anim::Ctrl*) hdr()->ctrl[2];
		if( !ctrl )
			return FALSE;
		NV_ASSERT_A32( ctrl );
		return nv::anim::GetSclCtrlValue( ctrl, ctxtA+2, inT, outScl, outVelocity, outSpeedUp );
	}

	bool
	GetFloatValue	(	float		inT,
						float*		outF		=NULL,
						float*		outVelocity	=NULL,
						float*		outSpeedUp	=NULL	)
	{
		nv::anim::Ctrl * ctrl = (nv::anim::Ctrl*) hdr()->ctrl[3];
		if( !ctrl )
			return FALSE;
		NV_ASSERT_A32( ctrl );
		return nv::anim::GetFpCtrlValue( ctrl, ctxtA+3, inT, outF, outVelocity, outSpeedUp );
	}

	bool
	GetRGBValue		(	float		inT,
						Vec3*		outRGB		=NULL,
						Vec3*		outVelocity	=NULL,
						Vec3*		outSpeedUp	=NULL	)
	{
		nv::anim::Ctrl * ctrl = (nv::anim::Ctrl*) hdr()->ctrl[4];
		if( !ctrl )
			return FALSE;
		NV_ASSERT_A32( ctrl );
		return nv::anim::GetRGBCtrlValue( ctrl, ctxtA+4, inT, outRGB, outVelocity, outSpeedUp );
	}

	void
	GetTRValue		(	float		inT,
						Matrix*		outTR,
						Vec3*		inDefLoc	=NULL,
						Quat*		inDefRot	=NULL,
						Vec3*		inDefScl	=NULL	)
	{
		Vec3 loc;
		Quat rot;
		Vec3 scl;
		nv::anim::Ctrl* loc_ctrl = (nv::anim::Ctrl*) hdr()->ctrl[0];
		nv::anim::Ctrl* rot_ctrl = (nv::anim::Ctrl*) hdr()->ctrl[1];
		nv::anim::Ctrl* scl_ctrl = (nv::anim::Ctrl*) hdr()->ctrl[2];
		if( loc_ctrl ) {
			nv::anim::GetLocCtrlValue( loc_ctrl, ctxtA+0, inT, &loc );
			inDefLoc = &loc;
		}
		if( rot_ctrl ) {
			nv::anim::GetRotCtrlValue( rot_ctrl, ctxtA+1, inT, &rot );
			inDefRot = &rot;
		}
		if( scl_ctrl ) {
			nv::anim::GetSclCtrlValue( scl_ctrl, ctxtA+2, inT, &scl );
			inDefScl = &scl;
		}
		MatrixBuildTR( outTR, inDefLoc, inDefRot, inDefScl );
	}

	void
	GetTRValue		(	float		inT,
						Matrix*		outTR,
						Matrix*		inDefTR		)
	{
		Vec3 loc, *locP = NULL;
		Quat rot, *rotP = NULL;
		Vec3 scl, *sclP = NULL;
		nv::anim::Ctrl* loc_ctrl = (nv::anim::Ctrl*) hdr()->ctrl[0];
		nv::anim::Ctrl* rot_ctrl = (nv::anim::Ctrl*) hdr()->ctrl[1];
		nv::anim::Ctrl* scl_ctrl = (nv::anim::Ctrl*) hdr()->ctrl[2];
		if( loc_ctrl ) {
			nv::anim::GetLocCtrlValue( loc_ctrl, ctxtA+0, inT, &loc );
			locP = &loc;
		}
		if( rot_ctrl ) {
			nv::anim::GetRotCtrlValue( rot_ctrl, ctxtA+1, inT, &rot );
			rotP = &rot;
		}
		if( scl_ctrl ) {
			nv::anim::GetSclCtrlValue( scl_ctrl, ctxtA+2, inT, &scl );
			sclP = &scl;
		}
		MatrixSetTR( outTR, locP, rotP, sclP, inDefTR );
	}

	uint
	CountNotes		(		)
	{
		return hdr()->noteCpt;
	}

	NvAnim::Note*
	EnumNotes		(		)
	{
		return (NvAnim::Note*) hdr()->notes;
	}

};





//
// FACTORY



namespace
{

	struct NvAnimFactory : public RscFactory_SHR
	{
		uint32
		GetType	(	)
		{
			return NvAnim::TYPE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvAnimBase::Header * hdr = (NvAnimBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvAnimBase::Header)
					&&	hdr->ver == NV_ANIM_CVER					);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvAnimBase::Header * hdr = (NvAnimBase::Header*) inRscData;

			// Translate controllers
			for( uint i = 0 ; i < 5 ; i++ )
				TranslatePointer( &hdr->ctrl[i], inBOffset );

			TranslatePointer( &hdr->notes, inBOffset );

			// Translate all note text pointers
			for( uint i = 0 ; i < hdr->noteCpt ; i++ )
				TranslatePointer( &hdr->notes[i].text, inBOffset );
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_DATA_SHR;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvAnimBase * inst = (NvAnimBase*) inInst;
			return &inst->itf;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			uint supplyBSize = sizeof(nv::anim::CtrlContext) * 5;

			NvAnimBase* inst = NvEngineNewAS( NvAnimBase, 16, supplyBSize );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			inst->ctxtA = (nv::anim::CtrlContext*)( inst+1 );
			Memset( inst->ctxtA, 0, sizeof(nv::anim::CtrlContext)*5 );

			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvAnimBase * inst = (NvAnimBase*) inInst;
			NvEngineDelete( inst );
		}
	};

}


bool
rscfactory_NvAnim_Reg()
{
	animFactory = NvEngineNew( NvAnimFactory );
	return RscManager::RegisterFactory( animFactory );
}




NvAnim*
NvAnim::Create	(	uint32	inUID	)
{
	if( !animFactory )
		return NULL;
	return (NvAnim*) animFactory->CreateInstance( inUID );
}




//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvAnim::TYPE = 0x43B9A627;	// CRC("NvAnim")

NVITF_MTH0( Anim,	NvInterface*,	GetBase												)
NVITF_CAL0( Anim,	void,			AddRef												)
NVITF_MTH0( Anim,	uint,			GetRefCpt											)
NVITF_CAL0( Anim,	void,			Release												)
NVITF_MTH0( Anim,	uint32,			GetRscType											)
NVITF_MTH0( Anim,	uint32,			GetRscUID											)
NVITF_MTH0( Anim,	uint,			GetCtrlMask											)
NVITF_MTH0( Anim,	float,			GetBeginTime										)
NVITF_MTH0( Anim,	float,			GetEndTime											)
NVITF_MTH0( Anim,	float,			GetDuration											)
NVITF_MTH0( Anim,	Box3*,			GetBBox												)
NVITF_MTH4( Anim,	bool,			GetLocValue,	float, Vec3*, Vec3*, Vec3*			)
NVITF_MTH4( Anim,	bool,			GetRotValue,	float, Quat*, Quat*, Quat*			)
NVITF_MTH4( Anim,	bool,			GetSclValue,	float, Vec3*, Vec3*, Vec3*			)
NVITF_MTH4( Anim,	bool,			GetFloatValue,	float, float*, float*, float*		)
NVITF_MTH4( Anim,	bool,			GetRGBValue,	float, Vec3*, Vec3*, Vec3*			)
NVITF_CAL5( Anim,	void,			GetTRValue,		float, Matrix*, Vec3*, Quat*, Vec3*	)
NVITF_CAL3( Anim,	void,			GetTRValue,		float, Matrix*, Matrix*				)
NVITF_MTH0( Anim,	uint,			CountNotes											)
NVITF_MTH0( Anim,	NvAnim::Note*,	EnumNotes											)


