/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "NvAnimCtrl.h"
#include <NvSkeleton.h>
#include <NvSkelAnim.h>
#include <Kernel/Common/NvRscFactory_SHR.h>


namespace
{
	RscFactory* skelAnimFactory = NULL;

	int	_FindBoneByName	(	uint32*			inBoneNameA,
							uint			inCount,
							uint32			inBoneName	)
	{
		for( uint i = 0 ; i < inCount ; i ++ ) {
			if( inBoneNameA[i] == inBoneName )
				return i;
		}
		return -1;
	}

	inline
	float
	_Unpack		(	uint8		inP,
					uint		inLSL,
					float		inA,
					float		inB		)
	{
		union {
			float  _f;
			uint32 _i;
		};

		// P = E(7..1) | S(1)    8bits

		_i   = inP;
		_i <<= 23-8+inLSL;
		_i  |= (128<<23);
		_f = _f * inA + inB;
		if( inP&1 )
			_f = -_f;
		return _f;
	}

}




//
// BASE



struct NvSkelAnimBase : public RscFactory_SHR::Instance
{
	#include <Projects\Plugin Nova\comp_NvSkelAnim_format.h>

	struct CtrlMapping {
		uint16		ctrlIdx;
		uint16		boneIdx;
		uint16		cpt;
	};
	typedef nv::vector<CtrlMapping>		CtrlMappingA;

	NvSkelAnim			itf;
	CtrlMappingA		mappingA;
	FloatA				locFrame;
	FloatA				rotFrame;
	uint				frameNo;


	NvSkelAnimBase	(	)	{}
	~NvSkelAnimBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)

	Header*
	hdr			(			)
	{
		return (Header*)inst.dataptr;
	}

	uint32
	GetRscType	(			)
	{
		return NvSkelAnim::TYPE;
	}

	uint32
	GetRscUID	(			)
	{
		return inst.uid;
	}

	void
	AddRef		(			)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release		(			)
	{
		if( inst.refCpt > 1 )
			inst.refCpt--;
		else
			skelAnimFactory->FreeInstance( &itf );
	}

	Box3*
	GetBBox		(			)
	{
		return &hdr()->bbox;
	}

	float
	GetBeginTime	(	)
	{
		return hdr()->beginTime;
	}

	float
	GetEndTime		(		)
	{
		return hdr()->endTime;
	}

	float
	GetDuration		(		)
	{
		return GetEndTime()-GetBeginTime();
	}

	uint
	GetCtrlCpt		(		)
	{
		return hdr()->ctrlCpt;
	}

	uint
	GetCtrlMask		(	uint			inCtrlNo	)
	{
		if( inCtrlNo > GetCtrlCpt() )
			return 0;
		else
			return hdr()->boneMaskA[ inCtrlNo ];
	}

	uint32
	GetCtrlBoneName		(	uint			inCtrlNo	)
	{
		if( inCtrlNo > GetCtrlCpt() )
			return 0;
		else
			return hdr()->boneNameA[ inCtrlNo ];
	}

	uint32*
	GetCtrlBoneNameA	(			)
	{
		return hdr()->boneNameA;
	}

	int
	FindCtrlByBoneName	(	uint32		inBoneName	)
	{
		uint N = GetCtrlCpt();
		for( uint i = 0 ; i < N ; i++ ) {
			if( hdr()->boneNameA[i] == inBoneName )
				return i;
		}
		return -1;
	}

	uint
	CountNotes		(		)
	{
		return hdr()->noteCpt;
	}

	NvSkelAnim::Note*
	EnumNotes		(		)
	{
		return (NvSkelAnim::Note*) hdr()->notes;
	}

	int
	GetMapping		(	uint		inCtrlNo	)
	{
		uint N = mappingA.size();
		for( uint i = 0 ; i < N ; i++ )
		{
			if( inCtrlNo >= mappingA[i].ctrlIdx )
			{
				uint d = inCtrlNo - mappingA[i].ctrlIdx;
				if( d < mappingA[i].cpt )
					return (mappingA[i].boneIdx + d);
			}
		}
		return -1;
	}

	void
	Unmap		(		)
	{
		mappingA.clear();
	}

	uint
	Map			(	uint32*			inBoneNameA,
					uint			inCount		)
	{
		Unmap();
	
		if( !inBoneNameA || !inCount )
			return 0;
	
		uint N = GetCtrlCpt();
		uint32* ctrlBoneNameA = GetCtrlBoneNameA();
	
		CtrlMapping ctrlmap = { 0 };
		uint		ctrlmapCpt	= 0;
	
		for( uint i = 0 ; i < N ; i++ )
		{
			int idx = _FindBoneByName( inBoneNameA, inCount, ctrlBoneNameA[i] );
			if( idx < 0 )
				continue;
	
			if( ctrlmapCpt == 0 )
			{
				ctrlmap.boneIdx	= idx;
				ctrlmap.ctrlIdx	= i;
				ctrlmap.cpt		= 1;
			}
			else
			{
				if(		int(ctrlmap.boneIdx+ctrlmap.cpt) == idx
					&&	(ctrlmap.ctrlIdx+ctrlmap.cpt) == int(i)	)
				{
					ctrlmap.cpt++;
				}
				else
				{
					mappingA.push_back( ctrlmap );
					ctrlmap.boneIdx	= idx;
					ctrlmap.ctrlIdx	= i;
					ctrlmap.cpt		= 1;
				}
			}
	
			ctrlmapCpt++;
		}
	
		if( ctrlmapCpt > 0 )
			mappingA.push_back( ctrlmap );
	
		return ctrlmapCpt;
	}

	uint
	Map			(	NvSkeleton*		inSkel		)
	{
		Unmap();
		if( !inSkel )
			return 0;
		return Map( inSkel->GetBoneNameA(), inSkel->GetBoneCpt() );
	}

	void
	_InitFrame0	(			)
	{
		// init with frame 0
		Memcpy( locFrame.data(), hdr()->locFrame0, locFrame.size()*4 );
		Memcpy( rotFrame.data(), hdr()->rotFrame0, rotFrame.size()*4 );
		frameNo = 0;
	}

	void
	_ApplyFrameForward(	AnimFrame	*	inFrame,
						float*			outFA,
						uint			inFASize	)
	{
		uint n = (inFASize+31)>>5;
		uint32* maskP = (uint32*)(inFrame+1);
		uint8*  dataP = (uint8*)(maskP+n);
		for( uint i = 0 ; i < n ; i++ )
		{
			uint32 m = maskP[i];
			for( uint j = 0 ; j < 32 ; j++ )
			{
				if( m & (1<<j) )
				{
					float d = _Unpack( *dataP++, inFrame->lsl, inFrame->a, inFrame->b );
					*outFA += d;
				}
				outFA++;
			}
		}
	}

	void
	_ApplyFrameBackward	(	AnimFrame	*	inFrame,
							float*			outFA,
							uint			inFASize	)
	{
		uint n = (inFASize+31)>>5;
		uint32* maskP = (uint32*)(inFrame+1);
		uint8*  dataP = (uint8*)(maskP+n);
		for( uint i = 0 ; i < n ; i++ )
		{
			uint32 m = maskP[i];
			for( uint j = 0 ; j < 32 ; j++ )
			{
				if( m & (1<<j) )
				{
					float d = _Unpack( *dataP++, inFrame->lsl, inFrame->a, inFrame->b );
					*outFA -= d;
				}
				outFA++;
			}
		}
	}

	bool
	ComputeState	(	float		inT,
						TRS *		outTRS16A	)
	{
		if( !outTRS16A )
			return FALSE;

		if( (uint32(outTRS16A)&0xF) != 0 ) {
			NV_DEBUG_WARNING( "NvSkelAnim::ComputeState() : outTRS16A must be 16bytes aligned !" );
			return FALSE;
		}

		uint N = mappingA.size();
		if( N == 0 )
			return FALSE;

		// wanted frame no
		uint toFrameNo = 0;
		if( inT > GetBeginTime() && hdr()->frameCpt )
		{
			toFrameNo = ( inT - GetBeginTime() ) * hdr()->freq;
			if( toFrameNo >= hdr()->frameCpt )
				toFrameNo = hdr()->frameCpt - 1;
		}

		if( toFrameNo != frameNo )
		{
			if(		toFrameNo==0
				 ||	( (toFrameNo<frameNo) && (toFrameNo<=(frameNo-toFrameNo)) ))	// rewind is faster ?
				_InitFrame0();

			// Forward ?
			if( toFrameNo > frameNo ) 
			{
				for( uint f = frameNo ; f < toFrameNo ; f++ )
				{
					if( hdr()->locFrameA )
						_ApplyFrameForward( hdr()->locFrameA[f], locFrame.data(), locFrame.size() );
					if( hdr()->rotFrameA )
						_ApplyFrameForward( hdr()->rotFrameA[f], rotFrame.data(), rotFrame.size() );
				}
			}
			// Backward ?
			else if( toFrameNo < frameNo )
			{
				for( uint f = frameNo-1 ; f >= toFrameNo ; f-- )
				{
					if( hdr()->locFrameA )
						_ApplyFrameBackward( hdr()->locFrameA[f], locFrame.data(), locFrame.size() );
					if( hdr()->rotFrameA )
						_ApplyFrameBackward( hdr()->rotFrameA[f], rotFrame.data(), rotFrame.size() );
				}
			}

			frameNo = toFrameNo;
		}

		for( uint i = 0 ; i < N ; i++ )
		{
			uint fb = mappingA[i].boneIdx;
			uint fc = mappingA[i].ctrlIdx;
			uint  n = mappingA[i].cpt;
	
			Vec3*  locSrc = (Vec3*)&locFrame[fc*3];
			Quat*  rotSrc = (Quat*)&rotFrame[fc*4];
			TRS *  dst    = &outTRS16A[fb];
			uint8* maskP  = &hdr()->boneMaskA[fc];
	
			for( uint j = 0 ; j < n ; j++ )
			{
				if( *maskP & NvSkelAnim::CTRL_LOC )
					Vec3Copy( &dst->t, locSrc );
				if( *maskP & NvSkelAnim::CTRL_ROT )
					QuatCopy( &dst->r, rotSrc );
				dst++;
				locSrc++;
				rotSrc++;
				maskP++;
			}
		}
	
		return TRUE;
	}

};





//
// FACTORY


namespace
{

	struct NvSkelAnimFactory : public RscFactory_SHR
	{
		uint32
		GetType	(	)
		{
			return NvSkelAnim::TYPE;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvSkelAnimBase::Header * hdr = (NvSkelAnimBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvSkelAnimBase::Header)
						&&	hdr->ver == NV_SKELANIM_CVER	);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvSkelAnimBase::Header * hdr = (NvSkelAnimBase::Header*) inRscData;
			TranslatePointer( &hdr->boneNameA,	 inBOffset );
			TranslatePointer( &hdr->boneMaskA,	 inBOffset );
			TranslatePointer( &hdr->locFrame0,	 inBOffset );
			TranslatePointer( &hdr->rotFrame0,	 inBOffset );
			TranslatePointer( &hdr->locFrameA,	 inBOffset );
			TranslatePointer( &hdr->rotFrameA,	 inBOffset );
			TranslatePointer( &hdr->notes,		 inBOffset );

			// Translate all frame pointers
			for( uint i = 0 ; i < hdr->frameCpt ; i++ ) {
				if( hdr->locFrameA )
					TranslatePointer( &hdr->locFrameA[i], inBOffset );
				if( hdr->rotFrameA )
					TranslatePointer( &hdr->rotFrameA[i], inBOffset );
			}

			// Translate all note text pointers
			for( uint i = 0 ; i < hdr->noteCpt ; i++ )
				TranslatePointer( &hdr->notes[i].text, inBOffset );
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_DATA_SHR;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvSkelAnimBase * inst = (NvSkelAnimBase*) inInst;
			return &inst->itf;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NvSkelAnimBase* inst = NvEngineNewA( NvSkelAnimBase, 16 );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			inst->locFrame.resize( inst->hdr()->ctrlCpt*3 );
			inst->rotFrame.resize( inst->hdr()->ctrlCpt*4 );
			inst->_InitFrame0();

			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvSkelAnimBase * inst = (NvSkelAnimBase*) inInst;
			inst->Unmap();
			NvEngineDelete( inst );
		}
	};

}

bool
rscfactory_NvSkelAnim_Reg()
{
	skelAnimFactory = NvEngineNew( NvSkelAnimFactory );
	return RscManager::RegisterFactory( skelAnimFactory );
}




NvSkelAnim *
NvSkelAnim::Create	(	uint32	inUID	)
{
	if( !skelAnimFactory )
		return NULL;
	return (NvSkelAnim*) skelAnimFactory->CreateInstance( inUID );
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvSkelAnim::TYPE = 0x4DF8CA5B;	// CRC("NvSkelAnim")

NVITF_MTH0( SkelAnim, NvInterface*,			GetBase												)
NVITF_CAL0( SkelAnim, void,					AddRef												)
NVITF_MTH0( SkelAnim, uint,					GetRefCpt											)
NVITF_CAL0( SkelAnim, void,					Release												)
NVITF_MTH0( SkelAnim, uint32,				GetRscType											)
NVITF_MTH0( SkelAnim, uint32,				GetRscUID											)
NVITF_MTH0( SkelAnim, float,				GetBeginTime										)
NVITF_MTH0( SkelAnim, float,				GetEndTime											)
NVITF_MTH0( SkelAnim, float,				GetDuration											)
NVITF_MTH0( SkelAnim, Box3*,				GetBBox												)
NVITF_MTH0( SkelAnim, uint,					GetCtrlCpt											)
NVITF_MTH1( SkelAnim, uint,					GetCtrlMask,			uint						)
NVITF_MTH1( SkelAnim, uint32,				GetCtrlBoneName,		uint						)
NVITF_MTH0( SkelAnim, uint32*,				GetCtrlBoneNameA									)
NVITF_MTH1( SkelAnim, int,					FindCtrlByBoneName,		uint32						)
NVITF_MTH0( SkelAnim, uint,					CountNotes											)
NVITF_MTH0( SkelAnim, NvSkelAnim::Note*,	EnumNotes											)
NVITF_MTH2( SkelAnim, uint,					Map,					uint32*, uint				)
NVITF_MTH1( SkelAnim, uint,					Map,					NvSkeleton*					)
NVITF_CAL0( SkelAnim, void,					Unmap												)
NVITF_MTH1( SkelAnim, int,					GetMapping,				uint						)
NVITF_MTH2( SkelAnim, bool,					ComputeState,			float,	TRS*				)




