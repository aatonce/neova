/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvRawData.h>
#include <Kernel/Common/NvRscFactory.h>
using namespace nv;



namespace
{
	sysvector<RscFactory*>	rawFactories;
}




//
// BASE


struct NvRawDataBase : public NvInterface
{
	NvRawData		itf;
	pvoid			dataptr;				// instance data pointer
	uint32			databsize;				// instance data size in bytes
	uint32			uid;					// instance UID
	uint32			type;					// instance TYPE
	uint			refCpt;					// instance ref counter

	NvRawDataBase	(	)	{}
	~NvRawDataBase	(	)	{}		// needed by nv::DestructInPlace<T>(ptr)

	NvInterface*
	GetBase		(	)
	{
		return this;
	}

	uint32
	GetRscType	(	)
	{
		return type;
	}

	uint32
	GetRscUID	(	)
	{
		return uid;
	}

	void
	AddRef		(	)
	{
		refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return refCpt;
	}

	void
	Release		(	)
	{
		if( refCpt > 1 )
		{
			refCpt--;
		}
		else
		{
			for( uint i = 0 ; i < rawFactories.size() ; i++ ) {
				RscFactory* fac = rawFactories[i];
				if( fac->GetType() == type ) {
					fac->FreeInstance( &itf );
					break;
				}
			}
		}
	}

	pvoid
	GetDataPtr		(		)
	{
		return dataptr;
	}

	uint32
	GetDataBSize	(		)
	{
		return databsize;
	}
};




//
// FACTORY


namespace
{

	struct RscFactory_RawData : public RscFactory
	{
		// descriptor

		struct Desc {
			pvoid				ptr;					// rsc data pointer
			uint32				bsize;					// rsc data size in bytes
			uint32				uid;					// rsc UID
			NvRawDataBase*		inst;					// instance
		};
		nv::vector<Desc>		descA;
		uint32					type;


		virtual		~RscFactory_RawData	() {}


		Desc*
		FindDesc	(	uint32		inUID,
						uint32*		outIndex = NULL	)
		{
			uint N = descA.size();
			for( uint i = 0 ; i < N ; i++ ) {
				if( descA[i].uid == inUID ) {
					if( outIndex )
						*outIndex = i;
					return &descA[i];
				}
			}
			return NULL;
		}

		void	RegisterInit	(		)
		{
			static bool done = FALSE;
			if( !done ) {
				rawFactories.Init();
				done = TRUE;
			}
		}

		void	RegisterShut	(		)
		{
			static bool done = FALSE;
			if( !done ) {
				rawFactories.Shut();
				done = TRUE;
			}
		}

		uint32
		GetType		(		)
		{
			return type;
		}

		bool
		IsAvailableRsc	(	uint32		inUID	)
		{
			if( !inUID )
				return FALSE;
			else
				return ( FindDesc(inUID) != NULL );
		}

		bool
		EnumAvailableRsc (	uint		inIdx,
							uint*		outUID,
							uint*		outNbInstance,
							uint*		outNbReference,
							uint*		outMemUBSize	)
		{
			if( outUID )			*outUID = 0;
			if( outNbInstance )		*outNbInstance = 0;
			if( outNbReference )	*outNbReference = 0;
			if( outMemUBSize )		*outMemUBSize = 0;

			if( inIdx < descA.size() )
			{
				uint32 uid = descA[inIdx].uid;
				uint mem_bs = descA[inIdx].bsize;
				uint nb_inst = 0;
				uint nb_ref = 0;

				if( descA[inIdx].inst )
				{
					mem_bs += descA[inIdx].inst->databsize;
					nb_inst = 1;
					nb_ref = descA[inIdx].inst->refCpt;
				}

				if( outUID )			*outUID = uid;
				if( outNbInstance )		*outNbInstance = nb_inst;
				if( outNbReference )	*outNbReference = nb_ref;
				if( outMemUBSize )		*outMemUBSize = mem_bs;
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}

		pvoid
		AllocMemRsc			(	uint32		inBSize		)
		{
			return EngineMallocA( inBSize, 16 );
		}

		void
		FreeMemRsc			(	pvoid		inPtr,
								uint32		inBSize		)
		{
			EngineFree( inPtr );
		}

		uint32
		GetToPrefetchRscBSize	(	uint32		inUID,
									uint32		inBSize		)
		{
			return inUID ? inBSize : 0;
		}

		bool
		PrefetchRsc			(	uint32		inUID,
								pvoid		inPtr,
								uint32		inBSize		)
		{
			if( !inUID )
				return FALSE;

			if(		IsAvailableRsc(inUID)
				||	!inPtr
				||	!inBSize	)
				return FALSE;

			Desc desc;
			desc.uid		= inUID;
			desc.ptr		= inPtr;
			desc.bsize		= inBSize;
			desc.inst		= NULL;
			descA.push_back( desc );

			return TRUE;
		}

		bool
		UnloadRsc	(	uint32		inUID	)
		{
			if( !inUID )
				return FALSE;

			uint32 descIndex;
			Desc * desc = FindDesc(	inUID, &descIndex );
			if( !desc )
				return TRUE;	// not available <=> unloaded ok !

			// In USED !
			if( desc->inst )
				return FALSE;

			// prefetched but not instancied yet !
			if( desc->ptr )
				FreeMemRsc( desc->ptr, desc->bsize );
			descA.erase( descA.begin()+descIndex );
			return TRUE;
		}

		NvResource*
		CreateInstance		(	uint32		inUID		)
		{
			if( !inUID )
				return NULL;
		
			Desc * desc = FindDesc( inUID );
			if( !desc )
				return NULL;

			// Instance exists ?
			if( desc->inst )
			{
				desc->inst->refCpt++;
			}
			else
			{
				// Create instance
				NvRawDataBase* inst = NvEngineNewA( NvRawDataBase, 16 );
				if( !inst )
					return NULL;
				inst->dataptr	= desc->ptr;
				inst->databsize = desc->bsize;
				inst->uid		= inUID;
				inst->type		= type;
				inst->refCpt	= 1;
				desc->inst		= inst;
			}
			NV_ASSERT( desc->inst );
			return &desc->inst->itf;
		}

		void
		FreeInstance		(	NvResource*	inRsc		)
		{
			if( !inRsc )
				return;

			uint32 descIndex;
			Desc* desc = FindDesc( inRsc->GetRscUID(), &descIndex );
			if( !desc )
				return;

			// ITF to base - instance
			NvRawDataBase* inst = (NvRawDataBase*) inRsc->GetBase();

			NvEngineDelete( inst );
			// Unload
			if( desc->ptr )
				FreeMemRsc( desc->ptr, desc->bsize );
			descA.erase( descA.begin()+descIndex );
		}
	};

}



bool
rscfactory_NvRawData_Reg()
{
	rawFactories.Init();
	return TRUE;
}






bool
NvRawData::Register		(	uint32	inType	)
{
	// Already registered ?
	for( uint i = 0 ; i < rawFactories.size() ; i++ ) {
		RscFactory* fac = rawFactories[i];
		if( fac->GetType() == inType )
			return TRUE;
	}

	RscFactory_RawData* fac = NvEngineNew( RscFactory_RawData );
	if( !fac )
		return FALSE;
	fac->type = inType;
	RscManager::RegisterFactory( fac );
	rawFactories.push_back( fac );
	return TRUE;
}


uint32
NvRawData::Register		(	pcstr	inExtension		)
{
	if( !inExtension )
		return 0;
	uint32 type = crc::Get( inExtension );
	return Register(type) ? type : 0;
}


bool
NvRawData::GetTypeList	(	UInt32A	&	outTypeList		)
{
	outTypeList.clear();
	for( uint i = 0 ; i < rawFactories.size() ; i++ ) {
		RscFactory* fac = rawFactories[i];
		outTypeList.push_back( fac->GetType() );
	}
	return TRUE;
}


NvRawData*
NvRawData::Create	(	uint32		inUID	)
{
	for( uint i = 0 ; i < rawFactories.size() ; i++ ) {
		RscFactory* fac = rawFactories[i];
		if( fac->IsAvailableRsc(inUID) )
			return (NvRawData*) fac->CreateInstance( inUID );
	}
	return NULL;
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>

NVITF_MTH0( RawData,	NvInterface*,	GetBase									)
NVITF_CAL0( RawData,	void,			AddRef									)
NVITF_MTH0( RawData,	uint,			GetRefCpt								)
NVITF_CAL0( RawData,	void,			Release									)
NVITF_MTH0( RawData,	uint32,			GetRscType								)
NVITF_MTH0( RawData,	uint32,			GetRscUID								)
NVITF_MTH0( RawData,	pvoid,			GetDataPtr								)
NVITF_MTH0( RawData,	uint32,			GetDataBSize							)


