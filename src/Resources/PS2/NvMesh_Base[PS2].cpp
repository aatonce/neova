/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PS2/NvDpyManager[PS2].h>
#include <Kernel/PS2/NvkMesh[PS2].h>
#include <Kernel/Common/NvRscFactory_SHR.h>
#include <NvBitmap.h>
#include <NvMorphTarget.h>
using namespace nv;





//#define		DISABLE_LOCSKINNING
//#define		DISABLE_NRMSKINNING
//#define		DISABLE_SHDSKINNING
//#define		DISABLE_MORPHING
//#define		FORCE_NRMSKINNING
//#define		ENABLE_APPLY_BENCHING




OPEN_C_INTERFACE

// Location blending #0 -> #4
void	LocBlend_Init			(	uint		ipackRow,		uint		ixyzUnpack		);
uint	LocBlend_GetIBSize		(															);
uint32	LocBlend_Process		(	pvoid		inSprLocP,		uint8*		inBlendCpt		);	// #0,...,#4,	return #cycle
uint32	LocApply_Process		(	void*		outTagBase,		void*		inSrcBase,
									uint32		inSrcCpt,		uint32*		inRefTags,
									uint32*		ioBaseAddr,		uint		inMode			);	// mode:0/1,	return #cycle

// Normal blending #0 -> #2
uint	NrmBlend_GetIBSize		(															);
uint32	NrmBlend_Process		(	pvoid		inSprNrmP,		uint8*		inBlendCpt		);	// #0,...,#2,	returns #cycle
uint32	NrmApply_Process		(	void*		outTagBase,		void*		inSrcBase,
									uint32		inSrcCpt,		uint32*		inRefTags,
									uint32*		ioBaseAddr,		uint		inUnused		);	// return #cycle

typedef uint32	BlendFct		(	pvoid		inSprLocP,		uint8*		inBlendCpt		);
typedef uint32	ApplyFct		(	void*		outTagBase,		void*		inSrcBase,
									uint32		inSrcCpt,		uint32*		inRefTags,
									uint32*		ioBaseAddr,		uint		inMode			);

CLOSE_C_INTERFACE






IMPORT_VU_CODE_NAME( VU0_LOCBLEND )
IMPORT_VU_CODE_NAME( VU0_NRMBLEND )




namespace
{

	bool	DoVif0Mapping	(	uint32*			inBoneNameA,
								uint			inBoneCount,
								uint			inMapCpt,
								uint32*			inMapNameA,
								uint32*			inDmaVIF0,
								Matrix*			inSkelMatrixA	)
	{
		NV_ASSERT( inBoneNameA	);
		NV_ASSERT( inBoneCount	);
		NV_ASSERT( inMapCpt		);
		NV_ASSERT( inMapNameA	);
		NV_ASSERT( inDmaVIF0	);
		NV_ASSERT_A128( inSkelMatrixA	);

		for( uint i = 0 ; i < inMapCpt ; i++ ) {
			// No skip value ?
			if( inMapNameA[i] != 0 )
			{
				// Find mapping idx in skeleton bones
				int idx = -1;
				for( uint j = 0 ; j < inBoneCount ; j++ ) {
					if( inBoneNameA[j] == inMapNameA[i] ) {
						idx = j;
						break;
					}
				}
				// Found ?
				if( idx < 0 )
					return FALSE;	// Partial skinning is not permitted !

				// Setup the DMARefTag Addr
				inDmaVIF0[1] = DMA_ADDR( inSkelMatrixA + idx );
			}

			inDmaVIF0 += 4;
		}

		return TRUE;
	}


	RscFactory*		meshFactory = NULL;

	// VU0 micro-code

	int	locBlendMcId = -1;	// VU0
	int	nrmBlendMcId = -1;	// VU0

}





//
// BASE


struct NvMeshBase : public RscFactory_SHR::Instance
{

	#include <Projects\Plugin NovaPS2\comp_NvMesh_format.h>


	struct Morph {
		struct Slot {
			NvMorphTarget*		target;
			float				weight;
			float				cur_weight;
		};
		sysvector<Slot>			slotA;
		Vec3*					cacheA;
		LocSrc*					srcCacheA;
		bool					cacheValidity;
		uint					unprocFrameCpt;
	};


	NvMesh					itf;
	NvkMesh					kitf;
	NvBitmap**				lockBitmapA;		// NvBitmap* for each surface
	Morph*					morph;
	ShdVtx*					shadowingSkin;
	Matrix*					skelBlendA;
	pvoid					dmaData;			// instance dma-data
	uint					enflags;			// enabled flags
	uint					validflags;			// valid flags
	uint					updflags;			// toupdate flags
	uint32					drawFrameNo;


	virtual	~	NvMeshBase	(	) {}			// needed by nv::DestructInPlace<T>(ptr)

	Header*
	hdr			(		)
	{
		return (Header*)inst.dataptr;
	}

	void
	_Init		(		)
	{
		if( morph ) {
			morph->cacheValidity	= FALSE;
			morph->cacheA			= NULL;
			morph->srcCacheA		= NULL;
			morph->slotA.Init();
		}

		skelBlendA	= NULL;
		NV_ASSERT_A128( dmaData );

		enflags	 = NvMesh::EN_DEFAULT;
		#ifdef FORCE_NRMSKINNING
		enflags |= NvMesh::EN_NRM_SKINNING;
		#endif

		validflags	= 0;
		updflags	= 0;
		drawFrameNo = ~0U;
	}

	void
	_Shut		(		)
	{
		UnrefBitmaps();
		UnmapSkeleton();
		SetMorphSlotCpt(0);
		if( morph )
			morph->slotA.Shut();
	}

	void
	AddRef		(		)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release		(		)
	{
		NV_ASSERT( inst.refCpt );
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			NV_ASSERT( !IsDrawing() );		// Referenced in shader !!!
			NV_ASSERT( meshFactory );
			meshFactory->FreeInstance( &itf );
		}
	}

	uint32
	GetRscType	(		)
	{
		return NvMesh::TYPE;
	}

	uint32
	GetRscUID	(		)
	{
		return inst.uid;
	}

	NvMesh*
	GetInterface		(		)
	{
		return &itf;
	}

	NvkMesh*
	GetKInterface		(		)
	{
		return &kitf;
	}

	Box3*
	GetBBox				(		)
	{
		return &hdr()->bbox;
	}

	uint32
	GetNameCRC			(		)
	{
		return hdr()->nameCRC;
	}

	bool
	IsSkinnable			(		)
	{
		return hdr()->skinnable;
	}

	bool
	IsNrmSkinnable			(		)
	{
		return hdr()->nrmskinnable;
	}

	bool
	IsMorphable			(		)
	{
		return hdr()->morphable;
	}

	bool
	IsLightable			(		)
	{
		return hdr()->lightable;
	}

	bool
	IsShadowing		(	)
	{
		return hdr()->shadowing;
	}

	bool
	IsFullOpaque		(		)
	{
		return hdr()->fullopaque;
	}

	void
	Enable		(	uint32		inEnableFlags	)
	{
		enflags |= inEnableFlags;
	}

	void
	Disable		(	uint32		inEnableFlags	)
	{
		enflags &= ~inEnableFlags;
	}

	void
	SetEnabled		(	uint32		inEnableFlags	)
	{
		enflags = inEnableFlags;
	}

	uint32
	GetEnabled		(		)
	{
		return enflags;
	}

	bool
	MapSkeleton			(	uint			inBoneCount,
							uint32*			inBoneNameA,
							Matrix*			inBlend16A		)
	{
		UnmapSkeleton();

		if(		!IsSkinnable()
			||	!inBoneNameA
			||	!inBoneCount
			||	!inBlend16A		)
			return FALSE;

		// 16 bytes aligned ?
		if( (uint32(inBlend16A)&0xF) != 0 ) {
			NV_DEBUG_WARNING( "NvMesh::Map() : inBlend16A must be 16bytes aligned !" );
			return FALSE;
		}

		// location's blend mapping
		if( hdr()->skinnable )
		{
			NV_ASSERT( hdr()->locProcess );
			NV_ASSERT( hdr()->locProcess->vif0_mapCpt );
			NV_ASSERT_A128( dmaData );
			bool res = DoVif0Mapping(	inBoneNameA,
										inBoneCount,
										hdr()->locProcess->vif0_mapCpt,
										hdr()->locProcess->vif0_mapName,
										(uint32*)( uint32(dmaData) + hdr()->locProcess->vif0_dmaBOffset ),
										inBlend16A						);
			if( !res )
				return FALSE;
		}

		// normal's blend mapping
		if( hdr()->nrmskinnable )
		{
			NV_ASSERT( hdr()->nrmProcess );
			NV_ASSERT( hdr()->nrmProcess->vif0_mapCpt );
			NV_ASSERT_A128( dmaData );
			bool res = DoVif0Mapping(	inBoneNameA,
										inBoneCount,
										hdr()->nrmProcess->vif0_mapCpt,
										hdr()->nrmProcess->vif0_mapName,
										(uint32*)( uint32(dmaData) + hdr()->nrmProcess->vif0_dmaBOffset ),
										inBlend16A						);
			if( !res )
				return FALSE;
		}

		// shadowing location mapping ?
		if( shadowingSkin )
		{
			NV_ASSERT( hdr()->shdProcess );
			ShdProcess* shdProc	  = hdr()->shdProcess;
			ShdVtx*		shdVtx	  = shdProc->vtxA;
			ShdVtx*		shdVtx_end= shdVtx + shdProc->vtxCpt;
			ShdVtx*		shdSkin	  = shadowingSkin;
			while( shdVtx != shdVtx_end ) {
				shdSkin->boneCRC = ~0U;
				if( shdVtx->boneCRC ) {
					for( uint i = 0 ; i < inBoneCount ; i++ ) {
						if( inBoneNameA[i] == shdVtx->boneCRC ) {
							shdSkin->boneCRC = i;
							break;
						}
					}
				}
				shdSkin++;
				shdVtx++;
			}
		}

		skelBlendA  = inBlend16A;

		// enables skinning
		validflags |= NvMesh::EN_LOC_SKINNING;
		if( hdr()->nrmskinnable )
			validflags |= NvMesh::EN_NRM_SKINNING;
		if( shadowingSkin )
			validflags |= NvMesh::EN_SHD_SKINNING;
		return TRUE;
	}

	void
	UnmapSkeleton		(									)
	{
		skelBlendA  = NULL;
		validflags &= ~( NvMesh::EN_LOC_SKINNING | NvMesh::EN_NRM_SKINNING | NvMesh::EN_SHD_SKINNING );	// disables all skinning
	}

	uint
	GetMorphSlotCpt		(									)
	{
		return morph ? morph->slotA.size() : 0;
	}

	void
	SetMorphSlotCpt		(	uint			inSize			)
	{
		if( !morph )
			return;

		uint N = morph->slotA.size();

		if( N > inSize )
		{
			// Release erased slots
			for( uint i = inSize ; i < N ; i++ )
				SafeRelease( morph->slotA[i].target );
			morph->cacheValidity = FALSE;
			if( inSize == 0 ) {
				SafeFree( morph->cacheA		);
				SafeFree( morph->srcCacheA	);
				validflags &= ~(NvMesh::EN_MORPHING);		// disables morphing
			} else {
				morph->slotA.resize( inSize );
			}
		}
		else
		{
			Morph::Slot defSlot;
			defSlot.target		= NULL;
			defSlot.weight		= 0.0f;
			defSlot.cur_weight	= 0.0f;
			morph->slotA.resize( inSize, defSlot );
		}
	}

	NvMorphTarget*
	GetMorphSlotTarget	(	uint			inSlotNo		)
	{
		if( !morph || inSlotNo >= morph->slotA.size() )
			return NULL;
		else
			return morph->slotA[inSlotNo].target;
	}

	void
	SetMorphSlotTarget	(	uint			inSlotNo,
							NvMorphTarget*	inTarget		)
	{
	#ifndef DISABLE_MORPHING
		if(	!morph || inSlotNo >= morph->slotA.size() )
			return;

		if( morph->slotA[inSlotNo].target == inTarget )
			return;

		if( morph->slotA[inSlotNo].target ) {
			SafeRelease( morph->slotA[inSlotNo].target );
			if( morph->slotA[inSlotNo].cur_weight != 0.0f )
				morph->cacheValidity = FALSE;
		}

		if( inTarget ) {
			inTarget->AddRef();
			morph->slotA[inSlotNo].target		= inTarget;
			morph->slotA[inSlotNo].weight		= 0.0f;
			morph->slotA[inSlotNo].cur_weight	= 0.0f;
			validflags |= NvMesh::EN_MORPHING;		// enables morphing
		}
	#endif
	}

	void
	SetMorphSlotWeight	(	uint			inSlotNo,
							float			inWeight		)
	{
		if(	!morph || inSlotNo >= morph->slotA.size() )
			return;
		morph->slotA[inSlotNo].weight = inWeight;
	}

	uint
	GetSurfaceCpt		(			)
	{
		return hdr()->surfCpt;
	}

	NvkMesh::Shading*
	GetSurfaceShadingA	(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(Surface) == sizeof(NvkMesh::Shading) );
		return (NvkMesh::Shading*) hdr()->surfA;
	}

	uint32
	GetSurfaceNameCRC	(	uint		inSurfaceNo		)
	{
		if( inSurfaceNo >= hdr()->surfCpt )
			return 0;
		Surface* surf = hdr()->surfA + inSurfaceNo;
		return surf->nameCRC;
	}

	uint32
	GetSurfaceBitmapUID	(	uint		inSurfaceNo		)
	{
		if( inSurfaceNo >= hdr()->surfCpt )
			return 0;
		Surface* surf = hdr()->surfA + inSurfaceNo;
		return surf->bitmapUID;
	}

	uint
	GetBunchCpt			(			)
	{
		return hdr()->bunchCpt;
	}

	uint
	GetBunchDataCpt			(			)
	{
		return hdr()->bunchDataCpt;
	}

	uint32
	GetBunchDataBase	(			)
	{
		return uint32(dmaData);
	}

	NvkMesh::Bunch*
	GetBunchA			(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(Bunch) == sizeof(NvkMesh::Bunch) );
		return (NvkMesh::Bunch*) hdr()->bunchA;
	}

	Box3*
	GetBunchBBoxA			(			)
	{
		return hdr()->bunchBBoxA;
	}

	NvkMesh::BunchData*
	GetBunchDataA		(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(BunchData) == sizeof(NvkMesh::BunchData) );
		return (NvkMesh::BunchData*) hdr()->bunchDataA;
	}

	NvkMesh::BunchList*
	GetBunchByBunchA		(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(BunchList) == sizeof(NvkMesh::BunchList) );
		return (NvkMesh::BunchList*) hdr()->bunchByBunchA;
	}

	NvkMesh::BunchList*
	GetBunchBySurfA		(			)
	{
		NV_COMPILE_TIME_ASSERT( sizeof(BunchList) == sizeof(NvkMesh::BunchList) );
		return (NvkMesh::BunchList*) hdr()->bunchBySurfA;
	}

	void
	RefBitmaps	(			)
	{
		if( !lockBitmapA )
			return;
		Surface* surfA   = hdr()->surfA;
		uint	 surfCpt = hdr()->surfCpt;
		for( uint i = 0 ; i < surfCpt ; i++ ) {
			if( !lockBitmapA[i] )
				lockBitmapA[i] = NvBitmap::Create( surfA[i].bitmapUID );
		}
	}

	void
	UnrefBitmaps	(			)
	{
		if( !lockBitmapA )
			return;
		uint surfCpt = hdr()->surfCpt;
		for( uint i = 0 ; i < surfCpt ; i++ )
			SafeRelease( lockBitmapA[i] );
	}

	int32
	GetPackIPartLen		(				)
	{
		return hdr()->packIPartLen;
	}

	bool
	GetShadowing		(	uint&						outVtxCpt,
							uint&						outFaceCpt,
							NvkMesh::ShadowingVtx*&		outVtxA,
							NvkMesh::ShadowingFace*&	outFaceA		)
	{
		if( !hdr()->shadowing )
			return FALSE;
		NV_ASSERT( hdr()->shdProcess );
		NV_ASSERT( sizeof(NvkMesh::ShadowingVtx) == sizeof(ShdVtx) );
		NV_ASSERT( sizeof(NvkMesh::ShadowingFace) == sizeof(ShdFace) );
		ShdProcess* shdProc = hdr()->shdProcess;
		outVtxCpt  = shdProc->vtxCpt;
		outFaceCpt = shdProc->faceCpt;
		outVtxA    = (NvkMesh::ShadowingVtx*)  ( shadowingSkin ? shadowingSkin : shdProc->vtxA );
		outFaceA   = (NvkMesh::ShadowingFace*) shdProc->faceA;
		return TRUE;
	}

	void
	Update				(									)
	{
		// save flags to update before drawing ...
		updflags = enflags;
	}

	void
	UpdateBeforeDraw	(		)
	{
		// update todo flags with current state of the NvMesh ...
		updflags &= validflags;

		// Morphing cache auto-release ?
		if( morph && !(updflags & NvMesh::EN_MORPHING) && morph->cacheValidity ) {
			if( morph->slotA.size()==0 || morph->unprocFrameCpt>120 ) {
				SafeFree( morph->cacheA    );
				SafeFree( morph->srcCacheA );
				morph->cacheValidity = FALSE;
				validflags &= ~(NvMesh::EN_MORPHING);		// disables morphing
				updflags &= validflags;
			}
			morph->unprocFrameCpt++;
		}

		if( updflags == 0 )
			return;
		uint32 dbgBgColor = gs::crtc::GetBGColor();
		gs::crtc::SetBGColor( 0xFF0000 );


		//
		// Loc Process ?

		uint todo = updflags & (NvMesh::EN_MORPHING | NvMesh::EN_LOC_SKINNING);
		// morphing -> skinning
		if( todo == (NvMesh::EN_MORPHING|NvMesh::EN_LOC_SKINNING) )
		{
			NV_ASSERT( morph );
			ProcessMorphing();
			ProcessLocSkinning( morph->srcCacheA );
		}
		// Only skinning
		else if( todo == NvMesh::EN_LOC_SKINNING )
		{
			ProcessLocSkinning( hdr()->locProcess->src );
		}
		// Only morphing
		else if( todo == NvMesh::EN_MORPHING )
		{
			NV_ASSERT( morph );
			ProcessMorphing();
			ApplyLoc( morph->srcCacheA );
		}


		//
		// Nrm process ?

		if( updflags & NvMesh::EN_NRM_SKINNING )
		{
			ProcessNrmSkinning( hdr()->nrmProcess->src );
		}


		//
		// Shadowing process ?

		if( updflags & NvMesh::EN_SHD_SKINNING )
		{
			ProcessShdSkinning();
		}


		updflags = 0;
		gs::crtc::SetBGColor( dbgBgColor );

		drawFrameNo = DpyManager::GetFrameNo();
	}


	bool
	IsDrawing	(		)
	{
		return DpyManager::IsDrawing( drawFrameNo );
	}


	void
	ProcessMorphing		(									)
	{
	#ifndef DISABLE_MORPHING
		NV_ASSERT( morph );
		if( !morph )	return;

		uint		morphCpt = morph->slotA.size();
		uint		refCpt	  = hdr()->refCpt;
		uint		locCpt	  = hdr()->locProcess->srcCpt;
		uint16*		idxTrlA	  = hdr()->locProcess->srcTrl;
		NV_ASSERT( morphCpt );
		NV_ASSERT( locCpt );
		NV_ASSERT( refCpt );
		NV_ASSERT( idxTrlA );

		// Packing/Unpacking cte
		const float  packRange	= float( 1<<(hdr()->packIPartLen) );
		const float  packRow	= float( 1<<(23-(15-(hdr()->packIPartLen))) );
		const uint32 ipackRow	= AS_UINT32( packRow );
		const float  xyzUnpack	= packRow + packRange;
		MType mt;

		// Setup cache from invalid state -> valid state
		if( !morph->cacheValidity )
		{
			if( !morph->cacheA ) {
				uint bsize = locCpt * sizeof(Vec3);
				morph->cacheA = (Vec3*) EngineMallocA( bsize, 16 );
			}
			if( !morph->srcCacheA )	{
				// 64bytes aligned & size to prevent D-cache & uncached accesses overlap !
				uint bsize = ( refCpt * sizeof(LocSrc) + 63 ) & ~63;
				morph->srcCacheA = (LocSrc*) EngineMallocA( bsize, 64 );
			}

			NV_ASSERT( hdr()->locProcess->src );
			Memcpy(	morph->srcCacheA, hdr()->locProcess->src, sizeof(LocSrc)*refCpt );

			// Unpack to accumulate buffer
			mt.i32[0] = ipackRow;
			mt.i32[1] = ipackRow;
			mt.i32[2] = ipackRow;
			for( uint i = 0 ; i < refCpt ; i++ )	{
				int16 locIdx = idxTrlA[i];
				mt.i16[0] = morph->srcCacheA[i].x;
				mt.i16[2] = morph->srcCacheA[i].y;
				mt.i16[4] = morph->srcCacheA[i].z;
				morph->cacheA[ locIdx ].x = mt.f32[0] - xyzUnpack;
				morph->cacheA[ locIdx ].y = mt.f32[1] - xyzUnpack;
				morph->cacheA[ locIdx ].z = mt.f32[2] - xyzUnpack;
			}

			// Reset cur weight for all slots
			for( uint i = 0 ; i < morphCpt ; i++ )
				morph->slotA[i].cur_weight = 0.0f;

			morph->cacheValidity = TRUE;
		}

		Matrix			  &	M0		= hdr()->M0;
		Matrix			   wM0;
		float				w, cur_w;
		NvMorphTarget*		targetP;
		Vec3   *			cacheP	  = morph->cacheA;
		bool				need_repack = FALSE;

		for( uint i = 0 ; i < morphCpt ; i++ )
		{
			targetP = morph->slotA[i].target;
			w		= morph->slotA[i].weight;
			cur_w	= morph->slotA[i].cur_weight;
	
			if(	!targetP )
				continue;

			// Need delta-update ?
			if( w == cur_w )
				continue;

			float dw = w - cur_w;
			morph->slotA[i].cur_weight = w;

			// Weighted TR0
			wM0 = M0 * dw;

			NvMorphTarget::VtxTarget * vtxTargetP;
			NvMorphTarget::VtxTarget * vtxTarget_endP;
			vtxTargetP		= targetP->GetVtxTargets();
			vtxTarget_endP	= vtxTargetP + targetP->GetVtxCpt();
			NV_ASSERT( vtxTargetP );

			while( vtxTargetP != vtxTarget_endP ) {
				Vec3BlendVector( &cacheP[vtxTargetP->index], &vtxTargetP->delta, &wM0 );
				vtxTargetP++;
			}

			need_repack = TRUE;
		}

		// Repack to src
		if( need_repack ) {
			LocSrc*	srcCacheP = FastUncachedPointer( morph->srcCacheA );
			for( uint i = 0 ; i < refCpt ; i++ ) {
				int16 locIdx = idxTrlA[i];
				mt.f32[0] = cacheP[locIdx].x + xyzUnpack;
				mt.f32[1] = cacheP[locIdx].y + xyzUnpack;
				mt.f32[2] = cacheP[locIdx].z + xyzUnpack;
				srcCacheP[i].x = mt.i16[0];
				srcCacheP[i].y = mt.i16[2];
				srcCacheP[i].z = mt.i16[4];
			}
		}

		// reset last update frame cpt
		morph->unprocFrameCpt = 0;
	#endif
	}

	inline
	void	ApplyProcess	(	BaseProcess*	inProcess,
								uint32			inSrc,
								uint32			inSrcStrideP2,
								int				inBlendMcId,
								BlendFct		inBlendFct,
								uint			inBlendFctBSize,
								ApplyFct		inApplyFct,
								uint			inApplyMode,
								uint*			outT_icyc,
								uint*			outS_icyc		)
	{
		NV_ASSERT_A128( inSrc );

		bool doBlend = (inBlendMcId >= 0);
		NV_ASSERT( !doBlend || (inBlendMcId >= 0) );
		NV_ASSERT( !doBlend || inBlendFct );
		NV_ASSERT( !doBlend || inBlendFctBSize );

		// request VU0 mc
		uint32* mcTadr = 0;
		uint32* mcLink = 0;
		if( doBlend ) {
			dmac::Cursor* DC = dmac::GetFrontHeapCursor();
			NV_ASSERT_DC( DC );
			mcTadr = DC->i32;
			mcLink = vu::RequestMC( DC, inBlendMcId );
			SyncDCache( mcTadr, DC->i32 );
			DC->i32 = mcTadr;	// rewind DC
		}

		uint32		curRefPtr	= uint32(dmaData);
		uint32		vif0Tadr	= uint32(dmaData) + inProcess->vif0_dmaBOffset;
		CmdProcess*	cmd			= inProcess->cmdList;
		uint32		toSprBf		= 0x70000000;	// double buffer 0x70000000 / 0x70002000
		uint32		fromSprBf	= 0;
		uint32		pcr			= 0;

		dmac::Sync( dmac::CH0_VIF0_M | dmac::CH9_TO_SPR_M | dmac::CH8_FROM_SPR_M );
		int intr = DI();
		*outS_icyc = 0;
		*outT_icyc = GetCycleCpt();

		// Preloading buff0
		pcr = dmac::CH9_TO_SPR_M;
		if( doBlend ) {
			SET_DMATAG_NEXT( mcLink, vif0Tadr );
			dmac::Start_CH0( (void*)mcTadr );
			pcr |= dmac::CH0_VIF0_M;
		}
		dmac::CopyToSPR( (void*)0x70000000, (void*)inSrc, (cmd->bc<<inSrcStrideP2)>>4 );
		*D_PCR = pcr;

		// Prefetch all
		if( doBlend )	PrefetchICache( (void*)inBlendFct, inBlendFctBSize );
		PrefetchDCache( (void*)cmd, sizeof(CmdProcess)*hdr()->locProcess->cmdCpt );

		for( ;; )
		{
			// REFILL
			{
				*outS_icyc += dmac::SyncLoop();
				uint enr = dmac::Pause();		// Turn off DMAC to ensure both transfers occur consecutively
				pcr = 0;
				if( fromSprBf ) {
					dmac::Start_CH8( (void*)(fromSprBf&0xFFFF) );
					pcr |= dmac::CH8_FROM_SPR_M;
				}
				if( !cmd->eocl ) {
					NV_ASSERT_A128( cmd->bc << inSrcStrideP2 );
					if( doBlend ) {
						vif0Tadr += ( cmd->vu0_tagc << 4 );
						dmac::Start_CH0( (void*)vif0Tadr );
						pcr |= dmac::CH0_VIF0_M;
					}
					inSrc += ( cmd->bc << inSrcStrideP2 );
					dmac::CopyToSPR( (void*)(toSprBf^0x2000), (void*)inSrc, ((cmd+1)->bc<<inSrcStrideP2)>>4 );
					pcr |= dmac::CH9_TO_SPR_M;
				}
				*D_PCR = pcr;
				dmac::Restore( enr );
			}

			// blending
			if( doBlend )
				(*inBlendFct)( (void*)toSprBf, &cmd->b0c );

			// fromSpr packet'ing
			NV_ASSERT_A32( cmd->toref_tag );
			fromSprBf = toSprBf | 0x1000;
			(*inApplyFct)( (void*)fromSprBf, (void*)toSprBf, cmd->bc, cmd->toref_tag, &curRefPtr, inApplyMode );

			if( cmd->eocl )		break;
			toSprBf ^= 0x2000;
			cmd++;
		}

		// Last strips update
		if( fromSprBf ) {
			*outS_icyc += dmac::SyncLoop();
			dmac::Start_CH8( (void*)(fromSprBf&0xFFFF) );
			*D_PCR = dmac::CH8_FROM_SPR_M;
		//	S_icyc += dmac::SyncLoop();
		}

		*outT_icyc = GetCycleCpt() - *outT_icyc;
		if( intr )	EI();
	}

	void
	ProcessLocSkinning	(	LocSrc  *		inLocA		)
	{
	#ifndef DISABLE_LOCSKINNING
		NV_ASSERT( IsSkinnable()  );
		NV_ASSERT( hdr()->locProcess );
		NV_COMPILE_TIME_ASSERT( sizeof(LocSrc) == 16 );

		float  packRange	  = float( 1<<(hdr()->packIPartLen) );
		float  packRow	  	  = float( 1<<(23-(15-(hdr()->packIPartLen))) );
		float  xyzUnpack	  = packRow + packRange;
		LocBlend_Init( AS_UINT32(packRow), AS_UINT32(xyzUnpack) );

		uint S_icyc, T_icyc;
		ApplyProcess(	hdr()->locProcess,
						uint32(inLocA), 4,
						locBlendMcId,
						LocBlend_Process,
						LocBlend_GetIBSize(),
						LocApply_Process, 0,		// mode 0 !
						&T_icyc, &S_icyc	);

		#ifdef ENABLE_APPLY_BENCHING
		if( (DpyManager::GetFrameNo()%1200) == 600 ) {
			uint sCpt = hdr()->hdr()->locProcess->srcCpt;
			uint rCpt = hdr()->refCpt;
			Printf( "[loc blend] #icyc:%d #dmasync-icyc:%d #ref:%d(%d cyc/ref) #loc:%d(%d cyc/loc)\n", T_icyc, S_icyc, rCpt, T_icyc/rCpt, sCpt, T_icyc/sCpt );
		}
		#endif
	#endif
	}

	void
	ProcessNrmSkinning	(	NrmSrc  *		inNrmA			)
	{
	#ifndef DISABLE_NRMSKINNING
		NV_ASSERT( IsNrmSkinnable()  );
		NV_ASSERT( hdr()->nrmProcess );
		NV_COMPILE_TIME_ASSERT( sizeof(NrmSrc) == 8 );

		uint S_icyc, T_icyc;
		ApplyProcess(	hdr()->nrmProcess,
						uint32(inNrmA), 3,
						nrmBlendMcId,
						NrmBlend_Process,
						NrmBlend_GetIBSize(),
						NrmApply_Process, 0,
						&T_icyc, &S_icyc	);

		#ifdef ENABLE_APPLY_BENCHING
		if( (DpyManager::GetFrameNo()%1200) == 600 ) {
			uint sCpt = hdr()->hdr()->nrmProcess->srcCpt;
			uint rCpt = hdr()->refCpt;
			Printf( "[nrm blend] #icyc:%d #dmasync-icyc:%d #ref:%d(%d cyc/ref) #nrm:%d(%d cyc/nrm)\n", T_icyc, S_icyc, rCpt, T_icyc/rCpt, sCpt, T_icyc/sCpt );
		}
		#endif
	#endif
	}

	void
	ApplyLoc			(	LocSrc  *		inLocA			)
	{
		return;	// BUG in ApplyLoc -> crash with freak !!!!
		NV_ASSERT( IsMorphable() || IsSkinnable() );
		NV_ASSERT( hdr()->locProcess );
		NV_COMPILE_TIME_ASSERT( sizeof(LocSrc) == 16 );

		uint S_icyc, T_icyc;
		ApplyProcess(	hdr()->locProcess,
						uint32(inLocA), 4,
						-1, NULL, 0,
						LocApply_Process, 1,		// mode 1 !
						&T_icyc, &S_icyc	);

		#ifdef ENABLE_APPLY_BENCHING
		if( (DpyManager::GetFrameNo()%1200) == 600 ) {
			uint sCpt = hdr()->locProcess->srcCpt;
			uint rCpt = hdr()->refCpt;
			Printf( "[loc apply] #icyc:%d #dmasync-icyc:%d #ref:%d(%d cyc/ref) #loc:%d(%d cyc/loc)\n", T_icyc, S_icyc, rCpt, T_icyc/rCpt, sCpt, T_icyc/sCpt );
		}
		#endif
	}

	void
	ProcessShdSkinning	(	)
	{
	#ifndef DISABLE_SHDSKINNING
		if( !shadowingSkin )
			return;

		NV_ASSERT( IsSkinnable()   );
		NV_ASSERT( IsShadowing()   );
		NV_ASSERT( skelBlendA	   );
		NV_ASSERT( hdr()->shdProcess );
		NV_ASSERT( sizeof(NvkMesh::ShadowingVtx) == sizeof(ShdVtx) );

		ShdProcess* shdProc	  = hdr()->shdProcess;
		ShdVtx*		shdVtx	  = shdProc->vtxA;
		ShdVtx*		shdVtx_end= shdVtx + shdProc->vtxCpt;
		ShdVtx*		shdSkin	  = shadowingSkin;

		while( shdVtx != shdVtx_end ) {
			if( shdSkin->boneCRC != ~0U )
				Vec3Apply( &shdSkin->location, &shdVtx->location, skelBlendA + shdSkin->boneCRC );
			shdSkin++;
			shdVtx++;
		}
	#endif
	}
};





//
// FACTORY


namespace
{

	struct NvMeshFactory : public RscFactory_SHR
	{
		void
		RegisterInit	(			)
		{
			if( locBlendMcId == -1 )
			{
				// Register VU0 micro-codes
				locBlendMcId = vu::RegisterMC(	0,
												VU0_LOCBLEND_CodeStart,
												VU0_LOCBLEND_CodeEnd	);
	
				nrmBlendMcId = vu::RegisterMC(	0,
												VU0_NRMBLEND_CodeStart,
												VU0_NRMBLEND_CodeEnd	);
			}
		}

		uint32
		GetType		(			)
		{
			return NvMesh::TYPE;
		}

		pvoid
		AllocMemRsc	(	uint32		inBSize		)
		{
			// 8QW for TTE=1 sync
			pvoid addr = EngineMallocA( inBSize, 128 );
			NV_ASSERT_ALIGNED( addr, 128 );
			return addr;
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			NvMeshBase::Header * hdr = (NvMeshBase::Header*) inRscData;
			return (	inBSize >= sizeof(NvMeshBase::Header)
					&&	hdr->ver == NV_MESH_CVER					);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			NvMeshBase::Header * hdr = (NvMeshBase::Header*) inRscData;
			TranslatePointer( &hdr->surfA,			inBOffset );
			TranslatePointer( &hdr->bunchA,			inBOffset );
			TranslatePointer( &hdr->bunchBBoxA,		inBOffset );
			TranslatePointer( &hdr->bunchDataA,		inBOffset );
			TranslatePointer( &hdr->bunchBySurfA,	inBOffset );
			TranslatePointer( &hdr->bunchByBunchA,	inBOffset );
			TranslatePointer( &hdr->locProcess,		inBOffset );
			TranslatePointer( &hdr->nrmProcess,		inBOffset );
			TranslatePointer( &hdr->shdProcess,		inBOffset );
			if( hdr->locProcess ) {
				TranslatePointer( &hdr->locProcess->vif0_mapName,	inBOffset );
				TranslatePointer( &hdr->locProcess->cmdList,		inBOffset );
				TranslatePointer( &hdr->locProcess->src,			inBOffset );
				TranslatePointer( &hdr->locProcess->srcTrl,			inBOffset );
			}
			if( hdr->nrmProcess ) {
				TranslatePointer( &hdr->nrmProcess->vif0_mapName,	inBOffset );
				TranslatePointer( &hdr->nrmProcess->cmdList,		inBOffset );
				TranslatePointer( &hdr->nrmProcess->src,			inBOffset );
			}
			if( hdr->shdProcess ) {
				TranslatePointer( &hdr->shdProcess->vtxA,			inBOffset );
				TranslatePointer( &hdr->shdProcess->faceA,			inBOffset );
			}
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			// Skinning or not, morphing or not => always shared
			// The CUSTOM is the same as SHR. So using SHR ...
			return SHRM_DATA_SHR;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			NvMeshBase * inst = (NvMeshBase*) inInst;
			return &inst->itf;
		}

		bool
		IsRscDmaDataIsUsed	(	Desc*		inDesc		)
		{
			// Find if an instance is using the original rsc DMAData section
			NV_ASSERT( inDesc->ptr );
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inDesc->ptr;
			pvoid rscDmaData = (void*)( uint32(inDesc->ptr) + hdr->dmaSectionBOffset );
			// For all instances ...
			NvMeshBase* inst = (NvMeshBase*) inDesc->head;
			while( inst ) {
				if( inst->dmaData == rscDmaData )
					return TRUE;
				inst = (NvMeshBase*) inst->inst.next;
			}
			return FALSE;
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			NV_COMPILE_TIME_ASSERT( sizeof(NvMeshBase::LocSrc) == 16 );
			NV_COMPILE_TIME_ASSERT( sizeof(NvMeshBase::NrmSrc) == 8 );

			NV_ASSERT( inRscData );
			NvMeshBase::Header* hdr = (NvMeshBase::Header*) inRscData;
			NV_ASSERT_IF( hdr->morphable,    hdr->locProcess );
			NV_ASSERT_IF( hdr->skinnable,    hdr->locProcess );
			NV_ASSERT_IF( hdr->nrmskinnable, hdr->nrmProcess );
			NV_ASSERT_IF( hdr->shadowing,	 hdr->shdProcess );
			uint supplyBSize = 0;

			if( hdr->surfCpt )
				supplyBSize += sizeof( NvBitmap* ) * hdr->surfCpt;

			if(	hdr->morphable )
				supplyBSize += sizeof( NvMeshBase::Morph );

			if( hdr->shadowing )
				supplyBSize += sizeof(NvMeshBase::ShdVtx) * hdr->shdProcess->vtxCpt;

			pvoid rscDmaData	= (void*)(uint32(inRscData)+hdr->dmaSectionBOffset);	// DMAData of the original & shared resource
			pvoid instDmaData	= rscDmaData;											// DMAData of the instance
			if( (hdr->skinnable || hdr->morphable) && IsRscDmaDataIsUsed(inDesc) ) {
				// The instance must owns its DMAData section !
				supplyBSize += hdr->dmaSectionQSize*16 + 64*2;		// D$ aligned before & after / 8QW+1 alignment for TTE=1
				instDmaData = NULL;
			}

			NvMeshBase* inst = NvEngineNewAS( NvMeshBase, 128, supplyBSize );
			if( !inst )		return NULL;
			NV_ASSERT_ALIGNED( inst, 128 );
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			uint32 supplyPtr0 = uint32( inst + 1 );
			uint32 supplyPtr  = supplyPtr0;

			inst->lockBitmapA = NULL;
			if( hdr->surfCpt ) {
				uint bsize = sizeof( NvBitmap* ) * hdr->surfCpt;
				inst->lockBitmapA = (NvBitmap**) supplyPtr;
				supplyPtr += bsize;
				Memset( inst->lockBitmapA, 0, bsize );
			}

			inst->morph = NULL;
			if(	hdr->morphable ) {
				inst->morph	= (NvMeshBase::Morph*) supplyPtr;
				supplyPtr   = uint32( inst->morph + 1 );
			}

			inst->shadowingSkin = NULL;
			if( hdr->shadowing ) {
				inst->shadowingSkin = (NvMeshBase::ShdVtx*) supplyPtr;
				uint bsize = sizeof(NvMeshBase::ShdVtx) * hdr->shdProcess->vtxCpt;
				Memcpy( inst->shadowingSkin, hdr->shdProcess->vtxA, bsize );
				supplyPtr += bsize;
			}

			if( !instDmaData ) {
				// DUP the resource whole DMAData section
				uint bsize = hdr->dmaSectionQSize*16;
				instDmaData = (void*) Round64( supplyPtr );				// D$ align before dma section
				Memcpy( instDmaData, rscDmaData, bsize );
				supplyPtr = Round64( uint32(instDmaData) + bsize );		// D$ align after dma section
			//	Printf( "[UID:%x] NvMesh shared %dKo/%dKo\n", inDesc->uid, bsize>>10, inRscBSize>>10 );
			}
			inst->dmaData = instDmaData;

			NV_ASSERT( (supplyPtr-supplyPtr0) <= supplyBSize );
			inst->_Init();

			#ifdef _DEBUG
			for( uint i = 0 ; i < hdr->bunchDataCpt ; i++ ) {
				uint32 bunch_dmaQSize = hdr->bunchDataA[i].dmaQSize;
				uint32 bunch_dmaData  = hdr->bunchDataA[i].dmaBOffset + uint32(instDmaData);
				NV_ASSERT( bunch_dmaQSize < 65535 );
				NV_ASSERT( (bunch_dmaData&0x7F)==16 );	// 8+1 QW aligned !
			}
			#endif

			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			NvMeshBase * inst = (NvMeshBase*) inInst;
			inst->_Shut();
			NvEngineDelete( inst );
		}
	};

}


bool
rscfactory_NvMesh_Reg()
{
	meshFactory = NvEngineNew( NvMeshFactory );
	return RscManager::RegisterFactory( meshFactory );
}



NvMesh *
NvMesh::Create	(	uint32	inUID	)
{
	if( !meshFactory )
		return NULL;
	return (NvMesh*) meshFactory->CreateInstance( inUID );
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvMesh::TYPE = 0xC57DF82A;	// CRC("NvMesh")

NVITF_MTH0(		Mesh,	NvInterface*,			GetBase												)
NVITF_CAL0(		Mesh,	void,					AddRef												)
NVITF_MTH0(		Mesh,	uint,					GetRefCpt											)
NVITF_CAL0(		Mesh,	void,					Release												)
NVITF_MTH0(		Mesh,	uint32,					GetRscType											)
NVITF_MTH0(		Mesh,	uint32,					GetRscUID											)
NVITF_MTH0(		Mesh,	NvkMesh*,				GetKInterface										)
NVITF_MTH0(		Mesh,	uint32,					GetNameCRC											)
NVITF_MTH0(		Mesh,	Box3*,					GetBBox												)
NVITF_MTH0(		Mesh,	uint,					GetSurfaceCpt										)
NVITF_MTH1(		Mesh,	uint32,					GetSurfaceNameCRC,		uint						)
NVITF_MTH1(		Mesh,	uint32,					GetSurfaceBitmapUID,	uint						)
NVITF_CAL0(		Mesh,	void,					RefBitmaps											)
NVITF_CAL0(		Mesh,	void,					UnrefBitmaps										)
NVITF_MTH0(		Mesh,	bool,					IsSkinnable											)
NVITF_MTH0(		Mesh,	bool,					IsNrmSkinnable										)
NVITF_MTH0(		Mesh,	bool,					IsMorphable											)
NVITF_MTH0(		Mesh,	bool,					IsLightable											)
NVITF_MTH0(		Mesh,	bool,					IsShadowing											)
NVITF_MTH0(		Mesh,	bool,					IsFullOpaque										)
NVITF_CAL1(		Mesh,	void,					Enable,					uint32						)
NVITF_CAL1(		Mesh,	void,					Disable,				uint32						)
NVITF_CAL1(		Mesh,	void,					SetEnabled,				uint32						)
NVITF_MTH0(		Mesh,	uint32,					GetEnabled											)
NVITF_MTH3(		Mesh,	bool,					MapSkeleton,			uint, uint32*, Matrix*		)
NVITF_CAL0(		Mesh,	void,					UnmapSkeleton										)
NVITF_MTH0(		Mesh,	uint,					GetMorphSlotCpt										)
NVITF_CAL1(		Mesh,	void,					SetMorphSlotCpt,		uint						)
NVITF_MTH1(		Mesh,	NvMorphTarget*,			GetMorphSlotTarget,		uint						)
NVITF_CAL2(		Mesh,	void,					SetMorphSlotTarget,		uint, NvMorphTarget*		)
NVITF_CAL2(		Mesh,	void,					SetMorphSlotWeight,		uint, float					)
NVITF_CAL0(		Mesh,	void,					Update												)

NVKITF_MTH0(	Mesh,	NvInterface*,			GetBase												)
NVKITF_CAL0(	Mesh,	void,					AddRef												)
NVKITF_MTH0(	Mesh,	uint,					GetRefCpt											)
NVKITF_CAL0(	Mesh,	void,					Release												)
NVKITF_MTH0(	Mesh,	uint32,					GetRscType											)
NVKITF_MTH0(	Mesh,	uint32,					GetRscUID											)
NVKITF_MTH0(	Mesh,	NvMesh*,				GetInterface										)
NVKITF_MTH0(	Mesh,	Box3*,					GetBBox												)
NVKITF_MTH0(	Mesh,	uint32,					GetNameCRC											)
NVKITF_MTH0(	Mesh,	bool,					IsSkinnable											)
NVKITF_MTH0(	Mesh,	bool,					IsNrmSkinnable										)
NVKITF_MTH0(	Mesh,	bool,					IsMorphable											)
NVKITF_MTH0(	Mesh,	bool,					IsLightable											)
NVKITF_MTH0(	Mesh,	bool,					IsFullOpaque										)
NVKITF_MTH0(	Mesh,	bool,					IsShadowing											)
NVKITF_MTH0(	Mesh,	int32,					GetPackIPartLen										)
NVKITF_MTH0(	Mesh,	uint,					GetBunchCpt											)
NVKITF_MTH0(	Mesh,	uint,					GetBunchDataCpt										)
NVKITF_MTH0(	Mesh,	uint32,					GetBunchDataBase									)
NVKITF_MTH0(	Mesh,	NvkMesh::Bunch*,		GetBunchA											)
NVKITF_MTH0(	Mesh,	Box3*,					GetBunchBBoxA										)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchData*,	GetBunchDataA										)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchList*,	GetBunchByBunchA									)
NVKITF_MTH0(	Mesh,	NvkMesh::BunchList*,	GetBunchBySurfA										)
NVKITF_CAL0(	Mesh,	void,					RefBitmaps											)
NVKITF_CAL0(	Mesh,	void,					UnrefBitmaps										)
NVKITF_MTH0(	Mesh,	uint,					GetSurfaceCpt										)
NVKITF_MTH0(	Mesh,	NvkMesh::Shading*,		GetSurfaceShadingA									)
NVKITF_MTH4(	Mesh,	bool,					GetShadowing,			uint&, uint&,
																		NvkMesh::ShadowingVtx*&,
																		NvkMesh::ShadowingFace*&	)
NVKITF_CAL0(	Mesh,	void,					UpdateBeforeDraw									)
NVKITF_MTH0(	Mesh,	bool,					IsDrawing											)


