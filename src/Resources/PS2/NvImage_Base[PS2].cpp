/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvImage.h>
#include <Kernel/PS2/NvkImage[PS2].h>
#include <Kernel/PS2/NvDpyManager[PS2].h>
#include "NvBitmap_Base[PS2].h"
using namespace nv;





struct NvImageBase : public NvBitmapBase
{
private:
	friend struct NvImage;
	friend struct NvkImage;
	NvImage		itf;
	NvkImage	kitf;
public:

	int						tram_id;
	tram::TexDesc			tdesc;
	pvoid					dmaData;			// tdesc dma data
	pvoid					backPixel;			// back pixels
	pvoid					clutPixel;			// clut pixels
	bool					freeClutAtRelease;
	bool					freeTexelAtRelease;
	pvoid					accessPixel;
	NvImage::Region			accessRegion;
	NvImage::Region			validRegion;
	bool					validCLUT;
	uint32					drawFrameNo;

	virtual	~	NvImageBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

	void
	AddRef					(			)
	{
		inst.refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return inst.refCpt;
	}

	void
	Release	(			)
	{
		NV_ASSERT( inst.refCpt );
		if( inst.refCpt > 1 )
		{
			inst.refCpt--;
		}
		else
		{
			// Safe to delete ?
			if( IsDrawing() )
			{
				DpyManager::AddToGarbager( this );
				return;
			}
			else
			{
				if( freeClutAtRelease )
					SafeFree( clutPixel );
				if( freeTexelAtRelease )
					SafeFree( backPixel );
				SafeFree( dmaData );
				SafeFree( accessPixel );
				tram::ReleaseDesc( tram_id );
				NvEngineDelete( this );
			}
		}
	}

	uint32
	GetRscType	(			)
	{
		return NvImage::TYPE;
	}

	uint32
	GetRscUID	(			)
	{
		return 0;
	}

	NvImage*
	GetInterface		(			)
	{
		return &itf;
	}

	NvkImage*
	GetKInterface		(			)
	{
		return &kitf;
	}

	NvBitmap*
	GetBitmap			(			)
	{
		return NvBitmapBase::GetInterface();
	}

	uint32
	GetWidth		(			)
	{
		return (1 << tdesc.L2_W);
	}

	uint32
	GetHeight		(			)
	{
		return (1 << tdesc.L2_H);
	}

	uint32
	GetWPad		(			)
	{
		return 0;
	}

	uint32
	GetHPad		(			)
	{
		return 0;
	}
	
	NvBitmap::AlphaStatus
	GetAlphaStatus	(			)
	{
		return NvBitmap::AS_OPAQUE;
	}

	bool
	IsBanner	(	)
	{
		return FALSE;
	}
	
	bool IsMask ()
	{
		return FALSE;
	}

	bool Pick ( uint , uint )
	{
		return FALSE;
	}

	int
	GetTRamId		(			)
	{
		return tram_id;
	}

	void
	FreeClutAtRelease	(	bool	inOnOff		)
	{
		freeClutAtRelease = inOnOff;
	}

	void
	FreeTexelAtRelease	(	bool	inOnOff		)
	{
		freeTexelAtRelease = inOnOff;
	}

	bool
	CreateAccess	(	NvImage::Region&	inRegion	)
	{
		if(		accessPixel
			||	inRegion.x<0
			||	inRegion.y<0
			||	inRegion.w==0
			||	inRegion.h==0
			||	(inRegion.x+inRegion.w) > GetWidth()
			||	(inRegion.y+inRegion.h) > GetHeight()	)
			return FALSE;
		accessRegion = inRegion;
		uint bsize;
		// clut ?
		if( tdesc.psm == SCE_GS_PSMT8 )
			bsize = 256*4 + inRegion.w * inRegion.h;
		else
			bsize = inRegion.w * inRegion.h * 4;
		accessPixel = EngineMallocA( bsize, 4 );
		NV_ASSERT( accessPixel );
		Zero( validRegion );
		validCLUT = FALSE;
		return TRUE;
	}

	pvoid
	GetPixelAccess	(	uint&				outLineStride,
						Psm&				outPSM			)
	{
		if( !accessPixel )
			return NULL;
		// clut ?
		if( tdesc.psm == SCE_GS_PSMT8 ) {
			outLineStride = accessRegion.w;
			outPSM		  = PSM_CLUT8;
			return ((uint8*)accessPixel)+256*4;
		} else {
			outLineStride = accessRegion.w * 4;
			outPSM		  = PSM_ABGR32;
			return accessPixel;
		}
		return NULL;
	}

	uint32*
	GetClutAccess	(	Psm&		outPSM	)
	{
		if( !accessPixel || tdesc.psm!=SCE_GS_PSMT8 )
			return NULL;
		outPSM = PSM_ABGR32;
		return (uint32*)accessPixel;
	}

	void
	ValidatePixelAccess	(	NvImage::Region&		inSubRegion		)
	{
		if( !accessPixel )
			return;

		// Clip sub-region with access-region
		if(		inSubRegion.x > accessRegion.w
			||	inSubRegion.y > accessRegion.h
			||	inSubRegion.w == 0
			||	inSubRegion.h == 0	)
			return;
		inSubRegion.w = Min( inSubRegion.w, accessRegion.w-inSubRegion.x );
		inSubRegion.h = Min( inSubRegion.h, accessRegion.h-inSubRegion.y );
		NV_ASSERT( inSubRegion.w && inSubRegion.h );

		if( validRegion.w == 0 )
		{
			// Init invalid-region
			validRegion = inSubRegion;
		}
		else
		{
			// Merge regions
			uint ix1 = validRegion.x + validRegion.w;
			uint iy1 = validRegion.y + validRegion.h;
			uint sx1 = inSubRegion.x + inSubRegion.w;
			uint sy1 = inSubRegion.y + inSubRegion.h;
			uint mx0 = Min( validRegion.x, inSubRegion.x );
			uint my0 = Min( validRegion.y, inSubRegion.y );
			NV_ASSERT( mx0 < ix1 && mx0 < sx1 );
			NV_ASSERT( my0 < iy1 && my0 < sy1 );
			uint mw  = Max( ix1-mx0, sx1-mx0 );
			uint mh  = Max( iy1-my0, sy1-my0 );
			validRegion.x = mx0;
			validRegion.y = my0;
			validRegion.w = mw;
			validRegion.h = mh;
		}
	}

	void
	ValidateClutAccess	(	)
	{
		if( accessPixel && tdesc.psm==SCE_GS_PSMT8 )
			validCLUT = TRUE;
	}

	void
	ReleaseAccess		(			)
	{
		if( !accessPixel )
			return;
		UpdateBeforeDraw();
		EngineFree( accessPixel );
		accessPixel = NULL;
	}

	void
	UpdateBeforeDraw			(				)
	{
		// store the flushing frame
		drawFrameNo = DpyManager::GetFrameNo();

		if( !accessPixel )
			return;

		// Update texels
		if( validRegion.w )
		{
			uint bytespp = tdesc.psm==SCE_GS_PSMT8 ? 1 : 4;

			// src
			uint src_lstride;
			Psm  psm;
			uint8* src = (uint8*) GetPixelAccess( src_lstride, psm );
			NV_ASSERT( src );
			src += validRegion.x * bytespp;

			// dst
			uint8* dst = tdesc.trx[1].bitmapData;
			NV_ASSERT( dst );
			uint dst_lstride = GetWidth() * bytespp;
			uint dst_x0 = accessRegion.x + validRegion.x;
			uint dst_y0 = accessRegion.y + validRegion.y;
			dst += dst_y0 * dst_lstride + dst_x0 * bytespp;
			for( uint y = 0 ; y < validRegion.h ; y++ ) {
				Memcpy( dst, src, validRegion.w*bytespp );
				src += src_lstride;
				dst += dst_lstride;
			}
		}

		// Update CLUT ?
		if( tdesc.psm==SCE_GS_PSMT8 && validCLUT )
		{
			Psm psm;
			uint32* src = GetClutAccess( psm );
			NV_ASSERT( src );
			uint32* dst = (uint32*) tdesc.trx[0].bitmapData;
			for( int k = 0 ; k < 8 ; k++ ) {
				// CSM1 arrangement
				Memcpy( &dst[k*32+ 0], &src[k*32+ 0], 32 );
				Memcpy( &dst[k*32+16], &src[k*32+ 8], 32 );
				Memcpy( &dst[k*32+ 8], &src[k*32+16], 32 );
				Memcpy( &dst[k*32+24], &src[k*32+24], 32 );
			}
		}

		// Reset invalid regions
		Zero( validRegion );
		validCLUT = FALSE;
	}

	bool
	IsDrawing	(			)
	{
		return DpyManager::IsDrawing( drawFrameNo );
	}

	int UpdateJpeg (	byte*		inJpegAddr,
						uint		inJpegBSize		)
	{
		return -1;
	}
};





NvkImage*
NvkImage::Create	(	uint				inPSM,
						uint				inWidth,
						uint				inHeight,
						pvoid				inClutPtr,
						pvoid				inTexelPtr,
						uint				inBlockW,
						uint				inBlockH,
						NvImage::CrStatus*	outStatus	)
{
	NV_ASSERT_NA128( inTexelPtr );
	NV_ASSERT_NA128( inClutPtr );

	if( !inBlockW )		inBlockW = inWidth;
	if( !inBlockH )		inBlockH = inHeight;


	//
	// Some compliant checks !

	if( inPSM!=SCE_GS_PSMT8 && inClutPtr ) {
		if( outStatus )
			*outStatus = NvImage::CS_ERROR;
		return NULL;
	}

	if( !IsPow2(inWidth) || !IsPow2(inHeight) || !IsPow2(inBlockW) || !IsPow2(inBlockH) ) {
		if( outStatus )
			*outStatus = NvImage::CS_POW2_ONLY;
		return NULL;
	}

	if( inWidth<8 || inHeight<8 ) {
		if( outStatus )
			*outStatus = NvImage::CS_TOO_LOW;
		return NULL;
	}

	if( inPSM==SCE_GS_PSMT8 && inWidth<inHeight ) {
		//	Note that when specifying cluted textures, the texture width must be a multiple of 128 texels (i.e. TBW must be a multiple of 2).
		//	For widths less than 64, TBW must be set to 1 (it�s the lowest possible value).
		//	for example, TBW must be set to 2 for 64x128 PSMT8 texture !
		//	see GS-HOWTO technical note on PSMT4/8 texel formats.
		//	To prevent this problem, inWidth must be > inHeight !
		if( outStatus )
			*outStatus = NvImage::CS_WIDTH_GREATER;
		return NULL;
	}

	uint psize = (inPSM==SCE_GS_PSMT8?1:0) + gs::tools::GetPageSize( inPSM, inWidth , inHeight );
	if( psize > tram::GetPSize() || inWidth>512 || inHeight>512 ) {
		if( outStatus )
			*outStatus = NvImage::CS_TOO_HIGH;
		return NULL;
	}

	// block QSIZE limited by the 15bits NLOOP field of GS primitive
	uint blockQSIZE = (inBlockW * inBlockH * (inPSM==SCE_GS_PSMT8?1:4)) >> 4;
	if( blockQSIZE > 32760U ) {		// GIFTAG.NLOOP 15bits limitation !
		if( outStatus )
			*outStatus = NvImage::CS_TOO_HIGH;
		return NULL;
	}


	//
	// Clut / Texel allocation

	bool freeClutAtRelease  = FALSE;
	bool freeTexelAtRelease = FALSE;

	if( (inPSM==SCE_GS_PSMT8) && !inClutPtr ) {
		inClutPtr = AllocDMABuffer( 1024 );
		if( !inClutPtr ) {
			if( outStatus )
				*outStatus = NvImage::CS_NOMORE_MEMORY;
			return NULL;
		}
		freeClutAtRelease = TRUE;
	}

	if( !inTexelPtr ) {
		uint texbsize = inWidth * inHeight * (inPSM==SCE_GS_PSMT8?1:4);
		inTexelPtr = AllocDMABuffer( texbsize );
		if( !inTexelPtr ) {
			if( freeClutAtRelease )
				SafeFree( inClutPtr );
			if( outStatus )
				*outStatus = NvImage::CS_NOMORE_MEMORY;
			return NULL;
		}
		freeTexelAtRelease = TRUE;
	}


	//
	// Init image rsc

	NvImage::CrStatus cr_error;

	NvImageBase* base = NvEngineNew( NvImageBase );
	if( !base )
	{
		cr_error = NvImage::CS_NOMORE_MEMORY;
	}
	else
	{
		base->inst.refCpt			= 1;
		base->tram_id				= -1;
		base->accessPixel			= NULL;
		base->clutPixel				= inClutPtr;
		base->backPixel				= inTexelPtr;
		base->dmaData				= NULL;
		base->freeClutAtRelease		= freeClutAtRelease;
		base->freeTexelAtRelease	= freeTexelAtRelease;
		base->drawFrameNo			= ~0U;

		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		pvoid dma0 = DC->vd;
		bool done = tram::BuildDesc(	&base->tdesc,
										DC,
										inPSM,
										GetFloorPow2(inWidth),
										GetFloorPow2(inHeight),
										inClutPtr,
										inTexelPtr,
										GetFloorPow2(inBlockW),
										GetFloorPow2(inBlockH)	);
		if( !done )
		{
			cr_error = NvImage::CS_NOMORE_MEMORY;
			DC->vd = dma0;
		}
		else
		{
			// dma relocation
			base->dmaData = DC->Dup( dma0 );
			uint32 dp = uint32(base->dmaData) - uint32(dma0);
			tram::DMATranslateDesc( &base->tdesc, dp );
			DC->vd = dma0;

			// register texture
			base->tram_id = tram::RegisterDesc( &base->tdesc );
			if( base->tram_id < 0 )
			{
				cr_error = NvImage::CS_TOO_MANY;
			}
			else
			{
				if( outStatus )
					*outStatus = NvImage::CS_SUCCESS;
				return base->GetKInterface();
			}
		}
	}


	//
	// Error handling

	if( base ) {
		SafeFree( base->dmaData );
		NvEngineDelete( base );
	}
	if( freeClutAtRelease )
		SafeFree( inClutPtr );
	if( freeTexelAtRelease )
		SafeFree( inTexelPtr );
	if( outStatus )
		*outStatus = cr_error;
	return NULL;
}




NvImage*
NvImage::Create	(	uint		inWidth,
					uint		inHeight,
					bool		inUsingCLUT,
					CrStatus*	outStatus		)
{
	NvkImage* kimg = NvkImage::Create(	inUsingCLUT ? SCE_GS_PSMT8 : SCE_GS_PSMCT32,
										inWidth,
										inHeight,
										NULL,
										NULL,
										0,
										0,
										outStatus	);
	return kimg ? kimg->GetInterface() : NULL;
}





//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvImage::TYPE = 0xA7C40B42;	// CRC("NvImage")

NVITF_MTH0(		Image,	NvInterface*,			GetBase											)
NVITF_CAL0(		Image,	void,					AddRef											)
NVITF_MTH0(		Image,	uint,					GetRefCpt										)
NVITF_CAL0(		Image,	void,					Release											)
NVITF_MTH0(		Image,	uint32,					GetRscType										)
NVITF_MTH0(		Image,	uint32,					GetRscUID										)
NVITF_MTH0(		Image,	NvBitmap*,				GetBitmap										)
NVITF_MTH0(		Image,	NvkImage*,				GetKInterface									)
NVITF_MTH0(		Image,	uint32,					GetWidth										)
NVITF_MTH0(		Image,	uint32,					GetHeight										)
NVITF_MTH0(		Image,	NvBitmap::AlphaStatus,	GetAlphaStatus									)
NVITF_MTH1(		Image,	bool,					CreateAccess,		NvImage::Region&			)
NVITF_MTH2(		Image,	pvoid,					GetPixelAccess,		uint&,	Psm&				)
NVITF_MTH1(		Image,	uint32*,				GetClutAccess,		Psm&						)
NVITF_CAL1(		Image,	void,					ValidatePixelAccess, NvImage::Region&			)
NVITF_CAL0(		Image,	void,					ValidateClutAccess								)
NVITF_CAL0(		Image,	void,					ReleaseAccess									)

NVKITF_MTH0(	Image,	NvInterface*,			GetBase											)
NVKITF_CAL0(	Image,	void,					AddRef											)
NVKITF_MTH0(	Image,	uint,					GetRefCpt										)
NVKITF_CAL0(	Image,	void,					Release											)
NVKITF_MTH0(	Image,	uint32,					GetRscType										)
NVKITF_MTH0(	Image,	uint32,					GetRscUID										)
NVKITF_MTH0(	Image,	NvImage*,				GetInterface									)
NVKITF_MTH0(	Image,	NvBitmap*,				GetBitmap										)
NVKITF_MTH0(	Image,	uint32,					GetWidth										)
NVKITF_MTH0(	Image,	uint32,					GetHeight										)
NVKITF_MTH0(	Image,	NvBitmap::AlphaStatus,	GetAlphaStatus									)
NVKITF_MTH0(	Image,	int,					GetTRamId										)
NVKITF_CAL0(	Image,	void,					UpdateBeforeDraw								)
NVKITF_MTH0(	Image,	bool,					IsDrawing										)
NVKITF_CAL1(	Image,	void,					FreeClutAtRelease,		bool					)
NVKITF_CAL1(	Image,	void,					FreeTexelAtRelease,		bool					)
NVKITF_MTH2(	Image,	int,					UpdateJpeg,		byte*,	uint					)


