
#include <Kernel/PS2/EE/asm.h>



		.global LocBlend_Init
		.global LocBlend_GetIBSize
		.global LocBlend_Process



		.data

ipackRow:		.int	0
ixyzUnpack:		.int	0



		.text

LocBlend_Init:
		// a0 : uint32 ipackRow
		// a1 : uint32 ixyzUnpack
		// Save parameters
		LoadImm		t0, ipackRow
		LoadImm		t1, ixyzUnpack
		sw			a0, 0(t0)
		jr			ra
		sw			a1, 0(t1)


LocBlend_GetIBSize:
		LoadImm		t0, LocBlend_Process
		LoadImm		t1, LocBlend_Process_End
		jr			ra
		sub			v0, t1, t0


		.p2align 6	// I$ line (64-bytes) alignment

LocBlend_Process:
		// a0 : pvoid	inSprLocP
		// a1 : uint8*	inBlendCpt
		addiu		sp, sp, -16
		sd			ra,0(sp)
		di
		sync.p

		// blend #0 (just a copy)
		lbu			t0, 0(a1)
		beq			t0, zr, 0f
		dsll		t0, t0, 4
		addu		v0, a0, t0
	1:	lw			t0, 4(a0)
		sw			t0, 0(a0)
		addiu		a0, a0, 16
		beq			a0, v0, 0f
		lw			t0, 4(a0)
		sw			t0, 0(a0)
		addiu		a0, a0, 16
		bne			a0, v0, 1b
		nop

		// vf30	: [ #4 | #3 | #2 | #1 ]
	0:	lbu			t1, 1(a1)
		lbu			t2, 2(a1)
		lbu			t3, 3(a1)
		lbu			t4, 4(a1)
		pextlw		t0, t3, t1
		pextlw		t1, t4, t2
		pextlw		t0, t1, t0
		qmtc2		t0, vf30

		// a1 : [ #4*16 | #3*16 | #2*16 | #1*16 ]
		psllw		a1, t0, 4

		// vf31 : < ixyzUnpack, iweightUnpack, -, - >
		LoadWord	t0, ixyzUnpack
		lui			t1, 0x4700				// 32768.0 = 0x47000000
		pextlw		t0, t1, t0
		qmtc2		t0, vf31

		// a2 : uint16 ipackRowHI[8]
		LoadWord	a2, ipackRow
		dsrl		a2, 16
		pcpyh		a2, a2
		pcpyld		a2, a2

		// a3 : uint16 iweightRowHI[8]
		addiu		a3, zr, 0x4700			// (127+(23-8))<<23 = 32768.0 = 0x47000000
		pcpyh		a3, a3
		pcpyld		a3, a3

		// Prefetch I$
		addu		t9, a0, zr
		addiu		a0, a0, 0x1000			 //	assert inSprLocP+0x1000 is free !
		jal			DO_Blend1
		addiu		v0, a0, 8*2
		jal			DO_Blend234
		addiu		v0, a0, 8*2
		addu		a0, t9, zr

		// t9 : #cycle accumulator
		GetCycleCpt t9

		// blend #1
		andi		t0, a1, 0xFFFF			// #1 * 16
		beq			t0, zr, 0f
		nop
		jal			DO_Blend1
		addu		v0, a0, t0				// end locP

		// blend #2
	0:	prot3w		a1, a1
		andi		t0, a1, 0xFFFF			// #2 * 16
		beq			t0, zr, 0f
		nop
		jal			DO_Blend234
		addu		v0, a0, t0				// end locP

		// blend #3
	0:	prot3w		a1, a1
		andi		t0, a1, 0xFFFF			// #3 * 16
		beq			t0, zr, 0f
		nop
		jal			DO_Blend234
		addu		v0, a0, t0				// end locP

		// blend #4
		pcpyud		a1, a1, zr
		prot3w		a1, a1
		andi		t0, a1, 0xFFFF			// #4 * 16
		beq			t0, zr, 0f
		nop
		jal			DO_Blend234
		addu		v0, a0, t0				// end locP

	0:	GetCycleCpt t0
		subu		t0, t0, t9				// returns #cycle acc
		ei
		sync.p
		ld			ra, 0(sp)
		jr			ra
		addiu		sp, sp, 16


//
// DO Blend 1 function
//
// -> a0    = start locP
// -> v0    = end   locP

		.p2align 3	// instruction pairing alignment

DO_Blend1:
		// Preloading
		lq			t1, 0*16(a0)
		lq			t2, 1*16(a0)
		lq			t3, 2*16(a0)
		lq			t0, 3*16(a0)

		// v
		pextuh		t4, a2, t1
		ctc2.i		t1, vi01			// -> idx0		(and waits vu0 completion E=1)
		qmtc2		t4, vf01			// -> locIn
		I_NOP

		VU0_CNT		v1

		// v@
		pextuh		t4, a2, t2
		ctc2.i		t2, vi02			// -> idx1
		qmtc2		t4, vf04			// -> locIn@
		I_NOP

		// v
		pextuh		t4, a2, t3
		ctc2.i		t3, vi01			// -> idx0
		qmtc2		t4, vf01			// -> locIn
		I_NOP

		// v@
	0:	pextuh		t1, a2, t0
		ctc2.i		t0, vi02			// -> idx1
		add			t2, zr, a0
		qmfc2		t3, vf08			// <- locOut
		addiu		a0, a0, 16
		qmtc2		t1, vf04			// -> locIn@
		ppach		t3, zr, t3
		lq			t0, 3*16(a0)
		beq			a0, v0, 0f
		sd			t3, 0(t2)

		// v
		pextuh		t1, a2, t0
		ctc2.i		t0, vi01			// -> idx0
		add			t2, zr, a0
		qmfc2		t3, vf11			// <- locOut@
		addiu		a0, a0, 16
		qmtc2		t1, vf01			// -> locIn
		ppach		t3, zr, t3
		lq			t0, 3*16(a0)
		bne			a0, v0, 0b
		sd			t3, 0(t2)

	0:	jr			ra
		nop


//
// DO Blend 2,3,4 function (same MIPS code !)
//
// -> a0    = start locP
// -> v0    = end   locP

		.p2align 3	// instruction pairing alignment

DO_Blend234:
		// Preloading
		lq			t1, 0*16(a0)
		lq			t2, 1*16(a0)
		lq			t3, 2*16(a0)
		lq			t0, 3*16(a0)

		// v
		pextuh		t4, a2, t1
		pextlb		t1, zr, t1
		qmtc2.i		t4, vf01			// -> locIn		(and waits vu0 completion E=1)
		pextlh		t5, zr, t1
		pextuh		t4, a3, t1
		qmtc2		t5, vf02			// -> idxIn
		qmtc2		t4, vf03			// -> wghtIn
		I_NOP

		VU0_CNT		v1

		// v@
		pextuh		t4, a2, t2
		pextlb		t2, zr, t2
		qmtc2.i		t4, vf04			// -> locIn@
		pextlh		t5, zr, t2
		pextuh		t4, a3, t2
		qmtc2		t5, vf05			// -> idxIn@
		qmtc2		t4, vf06			// -> wghtIn@
		I_NOP

		// v
		pextuh		t4, a2, t3
		pextlb		t3, zr, t3
		qmtc2.i		t4, vf01			// -> locIn
		pextlh		t5, zr, t3
		pextuh		t4, a3, t3
		qmtc2		t5, vf02			// -> idxIn
		qmtc2		t4, vf03			// -> wghtIn
		I_NOP

		// locIn@ pre-unpacking
		pextuh		t1, a2, t0
		LS_NOP

		// v@
	0:	pextlb		t0, zr, t0
		qmtc2.i		t1, vf04			// -> locIn@
		pextlh		t1, zr, t0
		qmfc2		t2, vf08			// <- locOut
		pextuh		t3, a3, t0
		qmtc2		t1, vf05			// -> idxIn@
		ppach		t2, zr, t2
		qmtc2		t3, vf06			// -> wghtIn@
		add			t3, zr, a0
		lq			t0, 4*16(a0)
		addiu		a0, a0, 16
		sd			t2, 0(t3)
		beq			a0, v0, 0f
		pextuh		t1, a2, t0

		// v
		pextlb		t0, zr, t0
		qmtc2.i		t1, vf01			// -> locIn
		pextlh		t1, zr, t0
		qmfc2		t2, vf11			// <- locOut@
		pextuh		t3, a3, t0
		qmtc2		t1, vf02			// -> idxIn
		ppach		t2, zr, t2
		qmtc2		t3, vf03			// -> wghtIn
		add			t3, zr, a0
		lq			t0, 4*16(a0)
		addiu		a0, a0, 16
		sd			t2, 0(t3)
		bne			a0, v0, 0b
		pextuh		t1, a2, t0

	0:	jr			ra
		nop


		.p2align 6	// I$ line (64-bytes) alignment
LocBlend_Process_End:




