

#include <Kernel/PS2/EE/asm.h>




		.global NrmApply_Process
		.text

		.p2align 6	// I$ alignement


NrmApply_Process:
		// a0 : void*		outTagBase
		// a1 : void*		inSrcBase
		// a2 : uint		inSrcCpt
		// a3 : uint32*		inRefTags
		// t0 : uint32*		ioBaseAddr
		// t1 : uint		inMode (unused !)
		// v0: returns #cycle to apply !

		di
		sync.p
		GetCycleCpt t9						// t9 : cycle
		addu		t8, zr, t0				// t8 : ioBaseAddr


		//
		// Sort sources from srcbase to tagbase

		dsll		v1, a2, 3
		addu		v0, zr, a1				// v0 : cur src
		addu		v1, v1, a1				// v1 : last src
		lbu			v1, -1(v1)
		dsll		v1, v1, 2
		addu		v1, v1, a0				// v1 : last destination addr

		.p2align 3							// start i-pairing
	0:	addiu		a1, v0, 0
		lbu			t0, 7+0*8(v0)			// load addrs
		addiu		v0, v0, 4*8
		lbu			t2, 7+1*8(a1)
		dsll		t0, t0, 2
		lbu			t4, 7+2*8(a1)
		dsll		t2, t2, 2
		lbu			t6, 7+3*8(a1)
		dsll		t4, t4, 2
		dsll		t6, t6, 2

		addu		t0, t0, a0				// destination ptr
		lw			t1, 0*8(a1)				// load xyz0
		addu		t2, t2, a0
		lw			t3, 1*8(a1)
		addu		t4, t4, a0
		lw			t5, 2*8(a1)
		addu		t6, t6, a0
		lw			t7, 3*8(a1)

		beq			t0, v1, 1f
		sw			t1, 0(t0)				// store xyz0
		beq			t2, v1, 1f
		sw			t3, 0(t2)
		beq			t4, v1, 1f
		sw			t5, 0(t4)
		bne			t6, v1, 0b
		sw			t7, 0(t6)
	1:


		//
		// Build fromSpr tags

		lw			v0, 0(t8)
		lui			v1, 0x1000
		pextlw		v0, v0, v1				// v0 : [ base-addr | DMA_TAG_CNT ]
		lui			v1, 0xFFFF				// v1 : qwc-mask [ FFFFFFFF | FFFF0000 ]

	0:	lw			t5, 0(a3)				// t5 : tag [ r-addr | qwc ] as 2 x uint16
		addiu		a3, a3, 4				// next tag parameters ...
		beq			t5, zr, 1f				// null tag => break
		pextlh		t5, zr, t5				//		tag [ r-addr | qwc ] as 2 x uint32
		paddw		v0, v0, t5				//      tag [ base-addr + r-addr | DMA_TAG_CNT | qwc ]
		sd			v0, 0(a0)
		pand		v0, v0, v1				//		tag [ base-addr + r-addr | DMA_TAG_CNT       ] (qwc cleared)
		andi		t5, t5, 0xFF			// qwc
		addiu		a0, a0, 16
		dsll		t5, t5, 4
		b			0b
		addu		a0, a0, t5				// tag += (qwc+1)<<4
	1:

		lui			t0, 0x7000				// DMA_TAG_END
		sw			t0, 0(a0)
		dsrl32		t0, v0, 0
		sw			t0, 0(t8)				// write back base-addr


		GetCycleCpt v0
		ei
		sync.p
		jr			ra
		subu		v0, v0, t9





