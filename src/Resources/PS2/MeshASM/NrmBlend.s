
#include <Kernel/PS2/EE/asm.h>


		.global NrmBlend_GetIBSize
		.global NrmBlend_Process



		.text

NrmBlend_GetIBSize:
		LoadImm		t0, NrmBlend_Process
		LoadImm		t1, NrmBlend_Process_End
		sub			v0, t1, t0
		jr			ra
		nop


		.p2align 6	// I$ line (64-bytes) alignment

NrmBlend_Process:
		// a0 : pvoid	inSprLocP
		// a1 : uint8*	inBlendCpt
		addiu		sp, sp, -16
		sd			ra,0(sp)
		di
		sync.p

		// blend #0 (just a copy)
		lbu			t0, 0(a1)
		beq			t0, zr, 0f
		dsll		t0, t0, 3
		addu		v0, a0, t0
	1:	lw			t0, 4(a0)
		sw			t0, 0(a0)
		addiu		a0, a0, 8
		beq			a0, v0, 0f
		lw			t0, 4(a0)
		sw			t0, 0(a0)
		addiu		a0, a0, 8
		bne			a0, v0, 1b
		nop

		// vf30	: [ #4 | #3 | #2 | #1 ]
	0:	lbu			t1, 1(a1)
		lbu			t2, 2(a1)
		lbu			t3, 3(a1)
		lbu			t4, 4(a1)
		pextlw		t0, t3, t1
		pextlw		t1, t4, t2
		pextlw		t0, t1, t0
		qmtc2		t0, vf30

		// a1 : [ #4*8 | #3*8 | #2*8 | #1*8 ]
		psllw		a1, t0, 3

		// a2 : uint16 ipackRowHI[8]
		addiu		a2, zr, 0x4780			// (127+(23-7))<<23 = 0x47800000
		pcpyh		a2, a2
		pcpyld		a2, a2

		// a3 : uint16 iweightRowHI[8]
		addiu		a3, zr, 0x4700			// (127+(23-8))<<23 = 0x47000000
		pcpyh		a3, a3
		pcpyld		a3, a3

		// t7 : nxyz min
		lui			t7, 0x4780				// 65536.00 = 0x47800000
		pextlw		t7, t7
		pextlw		t7, t7

		// t8 : nxyz max
		lui			t8, 0x4780
		addiu		t8, t8, 0x00FF			// 65537.99 = 0x478000FF
		pextlw		t8, t8
		pextlw		t8, t8

		// Prefetch I$
		addu		t9, a0, zr
		addiu		a0, a0, 0x1000			 //	assert inSprLocP+0x1000 is free !
		jal			DO_Blend1
		addiu		v0, a0, 8*2
		jal			DO_Blend2
		addiu		v0, a0, 8*2
		addu		a0, t9, zr

		// t9 : #cycle accumulator
		GetCycleCpt t9

		// blend #1
		andi		t0, a1, 0xFFFF			// #1 * 8
		beq			t0, zr, 0f
		nop
		jal			DO_Blend1
		addu		v0, a0, t0				// end nrmP

		// blend #2
	0:	prot3w		a1, a1
		andi		t0, a1, 0xFFFF			// #2 * 8
		beq			t0, zr, 0f
		nop
		jal			DO_Blend2
		addu		v0, a0, t0				// end nrmP

	0:	GetCycleCpt t0
		subu		t0, t0, t9				// returns #cycle acc
		ei
		sync.p
		ld			ra, 0(sp)
		jr			ra
		addiu		sp, sp, 16


//
// DO Blend 1 function
//
// -> a0    = start nrmP
// -> v0    = end   nrmP

		.p2align 3	// instruction pairing alignment

DO_Blend1:
		// Preloading
		ld			t1, 0*8(a0)
		ld			t2, 1*8(a0)
		ld			t3, 2*8(a0)
		ld			t0, 3*8(a0)

		// v
		pextlb		t4, zr, t1
		ctc2.i		t1, vi01			// -> idx0		(and waits vu0 [E] completion)
		pextuh		t4, a2, t4
		qmtc2		t4, vf01			// -> nrmIn

		VU0_CNT		v1

		// v@
		pextlb		t4, zr, t2
		ctc2.i		t2, vi02			// -> idx1
		pextuh		t4, a2, t4
		qmtc2		t4, vf04			// -> nrmIn@

		// v
		pextlb		t4, zr, t3
		ctc2.i		t3, vi01			// -> idx0
		pextuh		t4, a2, t4
		qmtc2		t4, vf01			// -> nrmIn

		// v@
	0:	pextlb		t1, zr, t0
		ctc2.i		t0, vi02			// -> idx1
		pextuh		t1, a2, t1
		qmfc2		t3, vf08			// <- nrmOut
		add			t2, zr, a0
		qmtc2		t1, vf04			// -> nrmIn@
		pmaxw		t3, t3, t7			// nxyz underflow
		addiu		a0, a0, 8
		pminw		t3, t3, t8			// nxyz overflow
		ppach		t3, zr, t3
		ppacb		t3, zr, t3
		ld			t0, 3*8(a0)
		beq			a0, v0, 0f
		sw			t3, 0(t2)

		// v
		pextlb		t1, zr, t0
		ctc2.i		t0, vi01			// -> idx0
		pextuh		t1, a2, t1
		qmfc2		t3, vf11			// <- nrmOut@
		add			t2, zr, a0
		qmtc2		t1, vf01			// -> nrmIn
		pmaxw		t3, t3, t7			// nxyz underflow
		addiu		a0, a0, 8
		pminw		t3, t3, t8			// nxyz overflow
		ppach		t3, zr, t3
		ppacb		t3, zr, t3
		ld			t0, 3*8(a0)
		bne			a0, v0, 0b
		sw			t3, 0(t2)

	0:	jr			ra
		nop


//
// DO Blend 2 function
//
// -> a0    = start nrmP
// -> v0    = end   nrmP

		.p2align 3	// instruction pairing alignment

DO_Blend2:
		// Preloading
		ld			t1, 0*8(a0)
		ld			t2, 1*8(a0)
		ld			t3, 2*8(a0)
		ld			t0, 3*8(a0)

		// v
		pextlb		t1, zr, t1
		pextuh		t4, a2, t1
		pextlh		t5, a3, t1
		qmtc2.i		t4, vf01			// -> nrmIn		(and waits vu0 [E] completion)
		qmtc2		t5, vf02			// -> iwIn
		I_NOP

		VU0_CNT		v1

		// v@
		pextlb		t2, zr, t2
		pextuh		t4, a2, t2
		pextlh		t5, a3, t2
		qmtc2.i		t4, vf04			// -> nrmIn@
		qmtc2		t5, vf05			// -> iwIn@
		I_NOP

		// v
		pextlb		t3, zr, t3
		pextuh		t4, a2, t3
		pextlh		t5, a3, t3
		qmtc2.i		t4, vf01			// -> nrmIn
		qmtc2		t5, vf02			// -> iwIn
		I_NOP

		// nrmIn@ pre-unpacking
		pextlb		t0, zr, t0
		pextuh		t1, a2, t0

		// v@
	0:	pextlh		t2, a3, t0
		qmtc2.i		t1, vf04			// -> nrmIn@
		qmtc2		t2, vf05			// -> iwIn@
		add			t3, zr, a0
		qmfc2		t1, vf08			// <- nrmOut
		addiu		a0, a0, 8
		pmaxw		t1, t1, t7			// nxyz underflow
		ld			t0, 3*8(a0)
		pminw		t1, t1, t8			// nxyz overflow
		ppach		t1, zr, t1
		ppacb		t1, zr, t1
		pextlb		t0, zr, t0
		sw			t1, 0(t3)
		beq			a0, v0, 0f
		pextuh		t1, a2, t0

		// v
		pextlh		t2, a3, t0
		qmtc2.i		t1, vf01			// -> nrmIn
		qmtc2		t2, vf02			// -> iwIn
		add			t3, zr, a0
		qmfc2		t1, vf11			// <- nrmOut@
		addiu		a0, a0, 8
		pmaxw		t1, t1, t7			// nxyz underflow
		ld			t0, 3*8(a0)
		pminw		t1, t1, t8			// nxyz overflow
		ppach		t1, zr, t1
		ppacb		t1, zr, t1
		pextlb		t0, zr, t0
		sw			t1, 0(t3)
		bne			a0, v0, 0b
		pextuh		t1, a2, t0

	0:	jr			ra
		nop


		.p2align 6	// I$ line (64-bytes) alignment
NrmBlend_Process_End:



