/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "NvBitmap_Base[PS2].h"
#include <Kernel/PS2/NvDpyManager[PS2].h>




namespace
{

	RscFactory*  bitmapFactory = NULL;


	struct Bitmap : public NvBitmapBase
	{
		#include <Projects\Plugin NovaPS2\comp_NvBitmap_format.h>

		int			tram_id;
		uint32		drawFrameNo;

		virtual	~	Bitmap	(	) {}		// needed by nv::DestructInPlace<T>(ptr)

		Header*
		hdr				(		)
		{
			return (Header*)inst.dataptr;
		}

		void
		AddRef			(			)
		{
			inst.refCpt++;
		}

		uint
		GetRefCpt		(		)
		{
			return inst.refCpt;
		}
	
		void
		Release			(		)
		{
			NV_ASSERT( inst.refCpt );
			if( inst.refCpt > 1 )
			{
				inst.refCpt--;
			}
			else
			{
				// Safe to delete ?
				if( IsDrawing() )
				{
					DpyManager::AddToGarbager( this );
					return;
				}
				else
				{
					NV_ASSERT( bitmapFactory );
					bitmapFactory->FreeInstance( GetInterface() );
				}
			}
		}

		uint32
		GetRscType	(			)
		{
			return NvBitmap::TYPE;
		}
	
		uint32
		GetRscUID		(		)
		{
			return inst.uid;
		}
	
		uint32
		GetWidth		(		)
		{
			return (1 << (hdr()->desc.L2_W) );
		}
	
		uint32
		GetHeight		(		)
		{
			return 0;
		}

		uint32
		GetWPad		(		)
		{
			return 0;
		}
	
		uint32
		GetHPad		(		)
		{
			return (1 << (hdr()->desc.L2_H) );
		}
		
		NvBitmap::AlphaStatus
		GetAlphaStatus	(		)
		{
			return NvBitmap::AlphaStatus( hdr()->aStatus );
		}

		int
		GetTRamId		(		)
		{
			return tram_id;
		}

		void
		UpdateBeforeDraw	(		)
		{
			drawFrameNo = DpyManager::GetFrameNo();
		}

		bool
		IsDrawing	(		)
		{
			return DpyManager::IsDrawing( drawFrameNo );
		}

		bool
		IsBanner	(	)
		{
			return FALSE;
		}
		
		bool IsMask ( )
		{
			return FALSE;
		}

		bool Pick ( uint, uint )
		{
			return FALSE;
		}
	};




	//
	// FACTORY

	struct BitmapFactory : public RscFactory_SHR
	{
		uint32
		GetType		(							)
		{
			return NvBitmap::TYPE;
		}

		pvoid
		AllocMemRsc	(	uint32		inBSize		)
		{
			// 8QW for TTE=0 sync
			return EngineMallocA( inBSize, 128 );
		}

		bool
		CheckRsc	(	pvoid		inRscData,
						uint32		inBSize		)
		{
			Bitmap::Header* hdr = (Bitmap::Header*) inRscData;
			return (	inBSize >= sizeof(Bitmap::Header)
					&&	hdr->ver == NV_BITMAP_CVER		);
		}

		void
		TranslateRsc	(	pvoid		inRscData,
							uint32		inBOffset		)
		{
			Bitmap::Header* hdr = (Bitmap::Header*) inRscData;
			for( uint i = 0 ; i < 8 ; i++ ) {
				TranslatePointer( &hdr->desc.trx[i].dmaData,    inBOffset );
				TranslatePointer( &hdr->desc.trx[i].bitmapData, inBOffset );
			}
		}

		SHRM
		GetRscSHRMode	 (	pvoid		inRscData	)
		{
			return SHRM_FULL;
		}

		NvResource*
		GetInstanceITF		(	Instance*	inInst	)
		{
			return ((Bitmap*)inInst)->GetInterface();
		}

		Instance*
		CreateInstanceObject (	Desc*		inDesc,
								pvoid		inRscData,
								uint32		inRscBSize	)
		{
			Bitmap* inst = NvEngineNew( Bitmap );
			if( !inst )		return NULL;
			InitInstanceObject( inst, inDesc, inRscData, inRscBSize );

			inst->tram_id = tram::RegisterDesc( &inst->hdr()->desc );
			if( inst->tram_id < 0 ) {
				NV_WARNING( "NvBitmap::Factory: Too many textures registered ..." );
				NvEngineDelete( inst );
				return NULL;
			}

			inst->drawFrameNo = ~0U;
			return inst;
		}

		void
		ReleaseInstanceObject	( 	Desc*		inDesc,
									Instance*	inInst	)
		{
			Bitmap* inst = (Bitmap*) inInst;
			tram::ReleaseDesc( inst->tram_id );
			NvEngineDelete( inst );
		}
	};

}



bool
rscfactory_NvBitmap_Reg()
{
	bitmapFactory = NvEngineNew( BitmapFactory );
	return RscManager::RegisterFactory( bitmapFactory );
}



NvBitmap *
NvBitmap::Create	(	uint32	inUID	)
{
	if( !bitmapFactory )
		return NULL;
	else
		return (NvBitmap*) bitmapFactory->CreateInstance( inUID );
}



