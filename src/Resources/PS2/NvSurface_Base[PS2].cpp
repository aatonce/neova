/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/NvkSurface[PS2].h>
#include <Kernel/PS2/VU/vu1_Mapping.h>
#include <Kernel/Common/NvRscFactory_SHR.h>




//
// SURFACE BASE


struct NvSurfaceBase : NvInterface
{
	enum {
		FLG_EN_PRESENT		= NvkSurface::EN_PRESENT,
		FLG_EN_BEGIN_END	= NvkSurface::EN_BEGIN_END,
		//--
		FLG_EN_DBF			= (NvkSurface::EN_ALL+1),
		FLG_NEED_PRESENT	= (FLG_EN_DBF<<1),
		FLG_COPY_PRESENT	= (FLG_NEED_PRESENT<<1),		// otherwise, swap present
		FLG_BUILDING		= (FLG_COPY_PRESENT<<1)
	};

	uint				refCpt;
	NvSurface			itf;
	NvkSurface			kitf;
	uint16				maxSize;
	NvSurface::PackMode	packMode;
	uint8				components;
	uint8				flags;					// FLG_* bit-mask
	uint8*				backData;
	uint8*				frontData;
	uint16				buildPos;
	uint8*				buildLocP;
	uint8*				buildTexP;
	uint8*				buildColP;
	uint8*				buildNrmP;
	uint32				drawFrameNo;


	virtual	~	NvSurfaceBase	(	) {}		// needed by nv::DestructInPlace<T>(ptr)


	NvInterface*
	GetBase		(		)
	{
		return this;
	}

	void
	AddRef		(		)
	{
		refCpt++;
	}

	uint
	GetRefCpt		(		)
	{
		return refCpt;
	}

	void
	Release		(		)
	{
		NV_ASSERT( refCpt );
		if( refCpt > 1 )
		{
			refCpt--;
		}
		else
		{
			NV_ASSERT( !IsDrawing() );		// Referenced in shader !!!

			// Safe to delete ?
			if( IsDrawing() )
			{
				DpyManager::AddToGarbager( this );
				return;
			}
			else
			{
				SafeFree( backData );
				SafeFree( frontData );
				NvEngineDelete( this );
			}
		}
	}

	uint32
	GetRscType		(		)
	{
		return NvSurface::TYPE;
	}

	uint32
	GetRscUID		(		)
	{
		return 0;
	}

	NvkSurface*
	GetKInterface	(		)
	{
		return &kitf;
	}

	NvSurface*
	GetInterface	(		)
	{
		return &itf;
	}

	uint
	GetComponents	(		)
	{
		return components;
	}

	bool
	HasComponent	(	NvSurface::Component	inComponent		)
	{
		return (components & uint(inComponent));
	}

	NvSurface::PackMode
	GetPackingMode	(		)
	{
		return packMode;
	}

	uint
	GetMaxSize		(		)
	{
		return maxSize;
	}

	void
	Enable		(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		flags |= inEnableFlags;
	}

	void
	Disable		(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		flags &= ~inEnableFlags;
		if( !(flags&FLG_EN_BEGIN_END) )
			flags &= ~FLG_BUILDING;
	}

	bool
	IsEnabled	(	uint	inEnableFlags	)
	{
		inEnableFlags &= NvkSurface::EN_ALL;
		return (flags & inEnableFlags);
	}

	bool
	PresentBuffer	(			)
	{
		flags |= FLG_NEED_PRESENT;
		return TRUE;
	}

	void
	DoPresentBuffer	(		)
	{
		if( !(flags&FLG_NEED_PRESENT) )		return;
		if( !(flags&FLG_EN_PRESENT) )		return;

		NV_ASSERT( backData );
		if( frontData ) {
			if( flags & FLG_COPY_PRESENT )
				Memcpy( backData, frontData, GetBufferBSize() );
			else
				Swap( backData, frontData );
		}

		flags &= ~(FLG_NEED_PRESENT|FLG_BUILDING);
	}

	uint
	GetBufferBSize	(		)
	{
		uint bs = 0;
		if( components & NvSurface::CO_LOC )		bs += Round16( maxSize*16 );
		if( components & NvSurface::CO_TEX )		bs += Round16( maxSize*8  );
		if( components & NvSurface::CO_COLOR )		bs += Round16( maxSize*4  );
		if( components & NvSurface::CO_NORMAL )		bs += Round16( maxSize*12 );
		return Round64( bs );
	}

	uint8*
	AllocBuffer		(			)
	{
		uint bs = GetBufferBSize();
		if( !bs )
			return NULL;
		return (uint8*) EngineMallocA( bs, 64 );		// D$ aligned
	}

	void
	FreeBuffer		(	uint8*		inBuffer	)
	{
		SafeFree( inBuffer );
	}

	uint8*
	GetBufferBase		(	NvkSurface::Buffer		inBuffer	)
	{
		return (inBuffer == NvkSurface::BF_BACK) ? backData : frontData;
	}

	uint8*
	GetComponentBase	(	NvkSurface::Buffer		inBuffer,
							NvSurface::Component	inComponent,
							uint*					outStride	= NULL,
							Psm*					outPsm		= NULL		)
	{
		if( !HasComponent(inComponent) )
			return NULL;

		uint8* base = GetBufferBase( inBuffer );
		if( !base )
			return NULL;

		uint bo = 0;
		uint bs = 0;
		if( inComponent == NvSurface::CO_KICK ) {
			bo = 12;
			bs = 16;
		} else {
			uint co_before = (uint(inComponent)-1) & components;
			if( co_before & NvSurface::CO_LOC )		bo += Round16( maxSize*16 );
			if( co_before & NvSurface::CO_TEX )		bo += Round16( maxSize*8  );
			if( co_before & NvSurface::CO_COLOR )	bo += Round16( maxSize*4  );
			if( co_before & NvSurface::CO_NORMAL )	bo += Round16( maxSize*12 );
			if( inComponent == NvSurface::CO_LOC )			bs = 16;
			else if( inComponent == NvSurface::CO_TEX )		bs = 8;
			else if( inComponent == NvSurface::CO_COLOR )	bs = 4;
			else if( inComponent == NvSurface::CO_NORMAL )	bs = 12;
		}
		if( outStride )	*outStride = bs;
		if( outPsm )	*outPsm = PSM_ABGR32;
		return base + bo;
	}

	bool
	Resize			(	uint		inMaxSize		)
	{
		if( IsDrawing() || inMaxSize < 3 )
			return FALSE;
		bool hasBack  = (backData  != NULL);
		bool hasFront = (frontData != NULL);
		SafeFree( backData );
		SafeFree( frontData );
		maxSize		= inMaxSize;
		backData	= hasBack  ? AllocBuffer() : NULL;
		frontData	= hasFront ? AllocBuffer() : NULL;
		flags	   &= ~(FLG_NEED_PRESENT|FLG_BUILDING);
		NV_ASSERT( !hasBack  || backData  );
		NV_ASSERT( !hasFront || frontData );
		return TRUE;
	}

	void
	EnableDBF		(	NvSurface::PresentMode	inPresentMode,
						bool					inCreateNow		)
	{
		if( inCreateNow && !frontData ) {
			frontData	= AllocBuffer();
			NV_ASSERT( frontData );
		}
		flags |=  FLG_EN_DBF;
		if( inPresentMode==NvSurface::SM_COPY )		flags |=  FLG_COPY_PRESENT;
		else										flags &= ~FLG_COPY_PRESENT;
	}

	void
	DisableDBF		(	bool		inReleaseNow	)
	{
		if( inReleaseNow ) {
			DoPresentBuffer();
			SafeFree( frontData );
		}
		flags &= ~FLG_EN_DBF;
	}

	bool
	IsEnabledDBF	(		)
	{
		return (flags & FLG_EN_DBF);
	}

	bool
	HasDBF			(		)
	{
		return (backData && frontData);
	}

	bool
	Begin			(	uint	inStartOffset		)
	{
		if( !(flags&FLG_EN_BEGIN_END) )		return FALSE;
		if( (flags&FLG_BUILDING) )			return FALSE;
		if( inStartOffset>=maxSize )		return FALSE;

		// Choose the buffer to write : front or back ?
		NvkSurface::Buffer buildBuffer;
		if( !frontData && !IsDrawing() ) {
			// Use back
			NV_ASSERT( backData );
			buildBuffer = NvkSurface::BF_BACK;
		} else {
			// Use front
			if( !(flags&FLG_EN_DBF) )	return FALSE;
			if( !frontData )
				frontData = AllocBuffer();
			NV_ASSERT( frontData );
			if( !frontData )			return FALSE;
			buildBuffer = NvkSurface::BF_FRONT;
		}

		buildLocP	= GetComponentBase( buildBuffer, NvSurface::CO_LOC   );
		buildTexP	= GetComponentBase( buildBuffer, NvSurface::CO_TEX   );
		buildColP	= GetComponentBase( buildBuffer, NvSurface::CO_COLOR );
		buildNrmP	= GetComponentBase( buildBuffer, NvSurface::CO_NORMAL);
		buildPos	= inStartOffset;
		if( buildLocP )		buildLocP += inStartOffset * 16;
		if( buildTexP )		buildTexP += inStartOffset * 8;
		if( buildColP )		buildColP += inStartOffset * 4;
		if( buildNrmP )		buildNrmP += inStartOffset * 12;
		flags |= FLG_BUILDING;
		return TRUE;
	}

	void
	TexCoord		(	const Vec2&		inST		)
	{
		if( !(flags&FLG_BUILDING) )		return;
		if( !buildTexP )				return;
		if( buildPos>=maxSize )			return;
		Vec2Copy( (Vec2*)buildTexP, &inST );
	}

	void
	Color			(	uint32			inColor,
						Psm				inPSM	)
	{
		if( !(flags&FLG_BUILDING) )		return;
		if( !buildColP )				return;
		if( buildPos>=maxSize )			return;
		if( inPSM != PSM_ABGR32 )
			inColor = ConvertPSM( PSM_ABGR32, inPSM, inColor );
		*((uint32*)buildColP) = inColor;
	}

	void
	Normal			(	const Vec3&		inNXYZ		)
	{
		if( !(flags&FLG_BUILDING) )		return;
		if( !buildNrmP )				return;
		if( buildPos>=maxSize )			return;
		Vec3Copy( (Vec3*)buildNrmP, &inNXYZ );
	}

	void
	Vertex			(	const Vec3&		inXYZ,
						bool			inKick		)
	{
		if( !(flags&FLG_BUILDING) )		return;
		if( !buildLocP )				return;
		if( buildPos>=maxSize )			return;
		Vec4Copy( (Vec4*)buildLocP, (Vec4*)&inXYZ );
		*((uint32*)(buildLocP+12)) = (inKick || !HasComponent(NvSurface::CO_KICK)) ? NvkSurface::ADC_OFF : NvkSurface::ADC_ON;
		if( buildLocP )		buildLocP += 16;
		if( buildTexP )		buildTexP += 8;
		if( buildColP )		buildColP += 4;
		if( buildNrmP )		buildNrmP += 12;
		buildPos++;
	}

	void
	End				(			)
	{
		if( !(flags&FLG_BUILDING) )		return;
		flags &= ~FLG_BUILDING;
		PresentBuffer();
	}

	void
	UpdateBeforeDraw	(		)
	{
		NV_ASSERT( refCpt > 0 );
		drawFrameNo = DpyManager::GetFrameNo();
		DoPresentBuffer();
		// release front ?
		if( frontData && !(flags&FLG_EN_DBF) )
			DisableDBF( TRUE );
	}

	bool
	IsDrawing	(		)
	{
		return DpyManager::IsDrawing( drawFrameNo );
	}
};




NvSurface*
NvSurface::Create	(	uint			inMaxSize,
						uint			inComponents,
						PackMode	/*	inPackMode	*/	)
{
	if( inMaxSize<3 || (inComponents&CO_LOC)==0 )
		return NULL;

	NvSurfaceBase* inst = NvEngineNew( NvSurfaceBase );
	if( !inst )
		return NULL;

	inst->refCpt		= 1;
	inst->maxSize		= inMaxSize;
	inst->components	= inComponents;
	inst->packMode		= PM_NONE;
	inst->flags			= NvkSurface::EN_DEFAULT;
	inst->backData		= NULL;
	inst->frontData		= NULL;
	inst->buildPos		= 0;
	inst->buildLocP		= NULL;
	inst->buildTexP		= NULL;
	inst->buildColP		= NULL;
	inst->buildNrmP		= NULL;
	inst->drawFrameNo	= ~0U;

	inst->backData		= inst->AllocBuffer();
	if( !inst->backData ) {
		NvEngineDelete( inst );
		return NULL;
	}

	return &(inst->itf);
}



//
// INTERFACES

#include <Kernel/Common/NvInterface.h>
const uint32 NvSurface::TYPE = 0x7D788970;	// CRC("NvSurface")

NVITF_MTH0(		Surface,	NvInterface*,			GetBase										)
NVITF_CAL0(		Surface,	void,					AddRef										)
NVITF_MTH0(		Surface,	uint,					GetRefCpt									)
NVITF_CAL0(		Surface,	void,					Release										)
NVITF_MTH0(		Surface,	uint32,					GetRscType									)
NVITF_MTH0(		Surface,	uint32,					GetRscUID									)
NVITF_MTH0(		Surface,	NvkSurface*,			GetKInterface								)
NVITF_MTH0(		Surface,	uint,					GetComponents								)
NVITF_MTH1(		Surface,	bool,					HasComponent,		NvSurface::Component	)
NVITF_MTH0(		Surface,	NvSurface::PackMode,	GetPackingMode								)
NVITF_MTH0(		Surface,	uint,					GetMaxSize									)
NVITF_MTH1(		Surface,	bool,					Resize,				uint					)
NVITF_CAL2(		Surface,	void,					EnableDBF,			NvSurface::PresentMode,
																		bool					)
NVITF_CAL1(		Surface,	void,					DisableDBF,			bool					)
NVITF_MTH0(		Surface,	bool,					IsEnabledDBF								)
NVITF_MTH0(		Surface,	bool,					HasDBF										)
NVITF_MTH1(		Surface,	bool,					Begin,				uint					)
NVITF_CAL1(		Surface,	void,					TexCoord,			const Vec2&				)
NVITF_CAL2(		Surface,	void,					Color,				uint32, Psm				)
NVITF_CAL1(		Surface,	void,					Normal,				const Vec3&				)
NVITF_CAL2(		Surface,	void,					Vertex,				const Vec3&, bool		)
NVITF_CAL0(		Surface,	void,					End											)

NVKITF_MTH0(	Surface,	NvInterface*,			GetBase										)
NVKITF_CAL0(	Surface,	void,					AddRef										)
NVKITF_MTH0(	Surface,	uint,					GetRefCpt									)
NVKITF_CAL0(	Surface,	void,					Release										)
NVKITF_MTH0(	Surface,	uint32,					GetRscType									)
NVKITF_MTH0(	Surface,	uint32,					GetRscUID									)
NVKITF_MTH0(	Surface,	NvSurface*,				GetInterface								)
NVKITF_MTH0(	Surface,	uint,					GetComponents								)
NVKITF_MTH1(	Surface,	bool,					HasComponent,		NvSurface::Component	)
NVKITF_MTH0(	Surface,	NvSurface::PackMode,	GetPackingMode								)
NVKITF_MTH0(	Surface,	uint,					GetMaxSize									)
NVKITF_MTH1(	Surface,	bool,					Resize,				uint					)
NVKITF_CAL2(	Surface,	void,					EnableDBF,			NvSurface::PresentMode,
																		bool					)
NVKITF_CAL1(	Surface,	void,					DisableDBF,			bool					)
NVKITF_MTH0(	Surface,	bool,					IsEnabledDBF								)
NVKITF_MTH0(	Surface,	bool,					HasDBF										)
NVKITF_MTH0(	Surface,	bool,					PresentBuffer								)
NVKITF_CAL1(	Surface,	void,					Enable,				uint					)
NVKITF_CAL1(	Surface,	void,					Disable,			uint					)
NVKITF_MTH1(	Surface,	bool,					IsEnabled,			uint					)
NVKITF_MTH0(	Surface,	uint8*,					AllocBuffer									)
NVKITF_CAL1(	Surface,	void,					FreeBuffer,			uint8*					)
NVKITF_MTH0(	Surface,	uint,					GetBufferBSize								)
NVKITF_MTH1(	Surface,	uint8*,					GetBufferBase,		NvkSurface::Buffer		)
NVKITF_MTH4(	Surface,	uint8*,					GetComponentBase, 	NvkSurface::Buffer,
																		NvSurface::Component,
																		uint*, Psm*				)
NVKITF_CAL0(	Surface,	void,					UpdateBeforeDraw							)
NVKITF_MTH0(	Surface,	bool,					IsDrawing									)


Psm
NvSurface::GetNativeColorPSM	(		)
{
	return PSM_ABGR32;
}


Psm
NvkSurface::GetNativeColorPSM	(		)
{
	return PSM_ABGR32;
}



