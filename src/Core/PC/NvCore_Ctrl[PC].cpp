/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PC/NvWindows.h>
#include <Kernel/PC/NvHrdw.h>
#include <Kernel/PC/NvkCore_Ctrl[PC].h>
#include <Nova.h>
using namespace nv;
using namespace nv::ctrl;


#define		COOPLEVEL_WINDOWED		(DISCL_NONEXCLUSIVE | DISCL_FOREGROUND)
#define		COOPLEVEL_FULLSCREEN	(DISCL_EXCLUSIVE    | DISCL_FOREGROUND)
#define		KEYBOARD_SAMPLE_BUFFER_SIZE	16

#ifdef _DEBUG
//define	DBG_OUTPUT_STATUS
#endif


namespace
{

	NV_DINPUT							pDI;
	bool								opened;

	uint32								devStatusMask;
	nv::ctrl::Status					devStatusA[32];
	nv::ctrl::Mapper*					devMapper;
	DevInfo								devKey;
	DevInfo								devMouse;
	bool								hasMousePos;
	POINT								devMousePos;
	POINT								devMouseHwndPos;
	nv::sysvector<DevInfo>				devJoyA;

	float								deadzA[ nv::ctrl::TP_MAX ][ nv::ctrl::BT_MAX ][ 2 ];	// lo/hi

	BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance,
										 VOID* pContext )
	{
		HRESULT hr;
		NV_DINPUTDEVICE devJoy;
		hr = pDI->CreateDevice( pdidInstance->guidInstance, &devJoy, NULL );
		if( SUCCEEDED(hr) && devJoy )
		{
			DevInfo dinf;
			dinf.dev = devJoy;
			devJoyA.push_back( dinf );
		}
		return DIENUM_CONTINUE;
	}


	struct DefMapper : public nv::ctrl::Mapper
	{
		void	Init		(	) {}
		void	Shut		(	) {}

		bool	SetStatus	(		nv::ctrl::Status*	outStatus,
									uint				inCtrlNo,
									nv::ctrl::Type		inType,
									DevInfo*			inData		)
		{
			Zero( *outStatus );
			outStatus->type = inType;

			// USER mapper ?
			if( devMapper && devMapper->SetStatus(outStatus,inCtrlNo,inType,inData) )
				return TRUE;

			HRESULT hr;


			//
			// Keyboard

			if( inType == nv::ctrl::TP_KEYBOARD )
			{

				//
				// Test 
				Zero(inData->di_ascii);
				DIDEVICEOBJECTDATA didod[ KEYBOARD_SAMPLE_BUFFER_SIZE ];  // Receives buffered data 
				DWORD dwElements = KEYBOARD_SAMPLE_BUFFER_SIZE;
				hr = inData->dev->GetDeviceData( sizeof(DIDEVICEOBJECTDATA), didod, &dwElements, 0 );
				if( SUCCEEDED(hr) ) {
					for(DWORD i = 0; i < dwElements; i++ ) {
						BOOL  bButtonState = (didod[i].dwData==0x80) ? TRUE : FALSE;
						if (bButtonState) { // process only key press (not key release)
							//(int) didod[i].dwOfs, (int) &didod[i]
							DWORD scanCode = (DWORD) didod[i].dwOfs;


							// translate scancode to ascii char
							uchar AsciiUChar = (uchar) 0;
							static HKL layout = GetKeyboardLayout(0);
							static unsigned char State[256];
							if (GetKeyboardState(State) != FALSE)
							{
								unsigned short result;
								UINT vk = MapVirtualKeyEx(scanCode, 1, layout);
								int ascii_res = ToAsciiEx(vk, scanCode, State, &result, 0, layout);
								if (!ascii_res)
								{
									switch (scanCode)
									{
									case DIK_NUMPAD0 : result = '0'; break;
									case DIK_NUMPAD1 : result = '1'; break;
									case DIK_NUMPAD2 : result = '2'; break;
									case DIK_NUMPAD3 : result = '3'; break;
									case DIK_NUMPAD4 : result = '4'; break;
									case DIK_NUMPAD5 : result = '5'; break;
									case DIK_NUMPAD6 : result = '6'; break;
									case DIK_NUMPAD7 : result = '7'; break;
									case DIK_NUMPAD8 : result = '8'; break;
									case DIK_NUMPAD9 : result = '9'; break;
									default : result = 0; break;
									}
								}
								AsciiUChar = (uchar) (result & 0xff);
								if (AsciiUChar >= 0 && AsciiUChar < 256){
									inData->di_ascii[AsciiUChar] = 1;
								}
							}
						}
					}
				}
				//Test end
				Zero(inData->di_kb);
			    hr = inData->dev->GetDeviceState( 256, inData->di_kb );
				if( FAILED(hr) )
					return FALSE;

				float right	= (inData->di_kb[DIK_RIGHT] & 0x80) ? 1.0F : 0.0F;
				float left	= (inData->di_kb[DIK_LEFT]  & 0x80) ? 1.0F : 0.0F;
				float up	= (inData->di_kb[DIK_UP]    & 0x80) ? 1.0F : 0.0F;
				float down	= (inData->di_kb[DIK_DOWN]  & 0x80) ? 1.0F : 0.0F;
	
				outStatus->type								= nv::ctrl::TP_KEYBOARD;
				outStatus->button[ nv::ctrl::BT_DIR1X ]		= right - left;
				outStatus->button[ nv::ctrl::BT_DIR1Y ]		= down - up;
				outStatus->button[ nv::ctrl::BT_DIR3right ]	= right;
				outStatus->button[ nv::ctrl::BT_DIR3left ]	= left;
				outStatus->button[ nv::ctrl::BT_DIR3up ]	= up;
				outStatus->button[ nv::ctrl::BT_DIR3down ]	= down;
				outStatus->button[ nv::ctrl::BT_BUTTON0 ]	= (inData->di_kb[DIK_RETURN] & 0x80) ? 1.0F : 0.0F;
				outStatus->button[ nv::ctrl::BT_BUTTON1 ]	= (inData->di_kb[DIK_SPACE]  & 0x80) ? 1.0F : 0.0F;
				outStatus->button[ nv::ctrl::BT_BUTTON2 ]	= (inData->di_kb[DIK_ESCAPE] & 0x80) ? 1.0F : 0.0F;
				//DebugPrintf( "#0 : KEYBOARD\n" );
			}


			//
			// Mouse

			else if( inType == nv::ctrl::TP_MOUSE )
			{
			    hr = inData->dev->GetDeviceState( sizeof(DIMOUSESTATE2), &inData->di_ms );
				if( FAILED(hr) )
					return FALSE;

				outStatus->type								= nv::ctrl::TP_MOUSE;
				outStatus->button[ nv::ctrl::BT_DIR1X ]		= float( inData->di_ms.lX );
				outStatus->button[ nv::ctrl::BT_DIR1Y ]		= float( inData->di_ms.lY );
				outStatus->button[ nv::ctrl::BT_BUTTON0 ]	= (inData->di_ms.rgbButtons[0] & 0x80) ? 1.0F : 0.0F;
				outStatus->button[ nv::ctrl::BT_BUTTON1 ]	= (inData->di_ms.rgbButtons[1] & 0x80) ? 1.0F : 0.0F;
				outStatus->button[ nv::ctrl::BT_BUTTON2 ]	= (inData->di_ms.rgbButtons[2] & 0x80) ? 1.0F : 0.0F;
				//DebugPrintf( "#1 : MOUSE\n" );
			}
 
 
 			//
 			// Paddle
 
 			else if( inType == nv::ctrl::TP_PADDLE )
 			{
			    hr = inData->dev->GetDeviceState( sizeof(DIJOYSTATE2), &inData->di_joy );
				if( FAILED(hr) )
					return FALSE;

				DIJOYSTATE2& dijs2 = inData->di_joy;

				outStatus->type	= nv::ctrl::TP_PADDLE;

				// ============================================================

				if( inData->IsProductID(DevInfo::SuperJoyBox3) )
				{
					outStatus->button[ nv::ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
					DWORD pov = dijs2.rgdwPOV[0];
					if( pov != -1 ) {
						if( pov >= 31000 || pov < 5000 )
							outStatus->button[ nv::ctrl::BT_DIR3up ] = 1.0f;
						if( pov >= 4500 && pov < 14000 )
							outStatus->button[ nv::ctrl::BT_DIR3right ] = 1.0f;
						if( pov >= 13500 && pov < 23000 )
							outStatus->button[ nv::ctrl::BT_DIR3down ] = 1.0f;
						if( pov >= 22500 && pov < 32000 )
							outStatus->button[ nv::ctrl::BT_DIR3left ] = 1.0f;
					}
					for( uint i = nv::ctrl::BT_BUTTON0 ; i < nv::ctrl::BT_MAX ; i++ )
						if( dijs2.rgbButtons[i-nv::ctrl::BT_BUTTON0] & 0x80 )
							outStatus->button[i] = 1.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON8 ] = dijs2.rgbButtons[9] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON9 ] = dijs2.rgbButtons[8] & 0x80 ? 1.0f : 0.0f;
				}

				// ============================================================

				else if( inData->IsProductID(DevInfo::EMS_SW2) )
				{
					outStatus->button[ nv::ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
					for( uint i = nv::ctrl::BT_BUTTON0 ; i < nv::ctrl::BT_MAX ; i++ )
						if( dijs2.rgbButtons[i-nv::ctrl::BT_BUTTON0] & 0x80 )
							outStatus->button[i] = 1.0f;
					outStatus->button[ nv::ctrl::BT_DIR3up ]    = dijs2.rgbButtons[12] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3right ] = dijs2.rgbButtons[13] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3down ]  = dijs2.rgbButtons[14] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3left ]  = dijs2.rgbButtons[15] & 0x80 ? 1.0f : 0.0f;
				}

				// ============================================================

				else if( inData->IsProductID(DevInfo::SmartJoyDeluxe) )
				{
					outStatus->button[ nv::ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
					for( uint i = nv::ctrl::BT_BUTTON0 ; i < nv::ctrl::BT_MAX ; i++ )
						if( dijs2.rgbButtons[i-nv::ctrl::BT_BUTTON0] & 0x80 )
							outStatus->button[i] = 1.0f;
					outStatus->button[ nv::ctrl::BT_DIR3up ]    = dijs2.rgbButtons[12] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3right ] = dijs2.rgbButtons[13] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3down ]  = dijs2.rgbButtons[14] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3left ]  = dijs2.rgbButtons[15] & 0x80 ? 1.0f : 0.0f;
				}

				// ============================================================

				else if( inData->IsProductID(DevInfo::KooInteractive) )
				{
					outStatus->button[ nv::ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
					DWORD pov = dijs2.rgdwPOV[0];
					if( pov != -1 ) {
						if( pov >= 31000 || pov < 5000 )
							outStatus->button[ nv::ctrl::BT_DIR3up ] = 1.0f;
						if( pov >= 4500 && pov < 14000 )
							outStatus->button[ nv::ctrl::BT_DIR3right ] = 1.0f;
						if( pov >= 13500 && pov < 23000 )
							outStatus->button[ nv::ctrl::BT_DIR3down ] = 1.0f;
						if( pov >= 22500 && pov < 32000 )
							outStatus->button[ nv::ctrl::BT_DIR3left ] = 1.0f;
					}
					for( unsigned int j = nv::ctrl::BT_BUTTON0 ; j < nv::ctrl::BT_MAX ; j++ )
						if( dijs2.rgbButtons[j-nv::ctrl::BT_BUTTON0] & 0x80 )
							outStatus->button[j] = 1.0f;

					outStatus->button[ nv::ctrl::BT_BUTTON8] = dijs2.rgbButtons[8] & 0x80 ? 1.0f : 0.0f;	// select
					outStatus->button[ nv::ctrl::BT_BUTTON9] = dijs2.rgbButtons[11] & 0x80 ? 1.0f : 0.0f;	// start
					outStatus->button[ nv::ctrl::BT_BUTTON10] = dijs2.rgbButtons[9] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON11] = dijs2.rgbButtons[10] & 0x80 ? 1.0f : 0.0f;
				}

				// ============================================================

/*				else if( inData->IsProductID(DevInfo::PS3Sixaxis) )
				{
					outStatus->button[ nv::ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR3right ] = dijs2.rgbButtons[5] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3left  ] = dijs2.rgbButtons[7] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3up    ] = dijs2.rgbButtons[4] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_DIR3down  ] = dijs2.rgbButtons[6] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON0   ] = dijs2.rgbButtons[12]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON1   ] = dijs2.rgbButtons[13]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON2   ] = dijs2.rgbButtons[14]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON3   ] = dijs2.rgbButtons[15]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON4   ] = dijs2.rgbButtons[8]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON5   ] = dijs2.rgbButtons[9]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON6   ] = dijs2.rgbButtons[10]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON7   ] = dijs2.rgbButtons[11]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON8   ] = dijs2.rgbButtons[0]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON9   ] = dijs2.rgbButtons[3]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON10  ] = dijs2.rgbButtons[1]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON11  ] = dijs2.rgbButtons[2]& 0x80 ? 1.0f : 0.0f;
				}
*/
				// GameSaike driver
				else if( inData->IsProductID(DevInfo::PS3Sixaxis) )
				{
					outStatus->button[ nv::ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
					DWORD pov = dijs2.rgdwPOV[0];
					if( pov != -1 ) {
						if( pov >= 31000 || pov < 5000 )
							outStatus->button[ nv::ctrl::BT_DIR3up ] = 1.0f;
						if( pov >= 4500 && pov < 14000 )
							outStatus->button[ nv::ctrl::BT_DIR3right ] = 1.0f;
						if( pov >= 13500 && pov < 23000 )
							outStatus->button[ nv::ctrl::BT_DIR3down ] = 1.0f;
						if( pov >= 22500 && pov < 32000 )
							outStatus->button[ nv::ctrl::BT_DIR3left ] = 1.0f;
					}
					outStatus->button[ nv::ctrl::BT_BUTTON0   ] = dijs2.rgbButtons[0]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON1   ] = dijs2.rgbButtons[1]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON2   ] = dijs2.rgbButtons[2]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON3   ] = dijs2.rgbButtons[3]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON4   ] = dijs2.rgbButtons[4]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON5   ] = dijs2.rgbButtons[5]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON6   ] = dijs2.rgbButtons[6]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON7   ] = dijs2.rgbButtons[7]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON8   ] = dijs2.rgbButtons[8]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON9   ] = dijs2.rgbButtons[11]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON10  ] = dijs2.rgbButtons[9]& 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON11  ] = dijs2.rgbButtons[10]& 0x80 ? 1.0f : 0.0f;
				}

				// ============================================================

				else if( inData->IsProductID(DevInfo::XBox360ctrl) )
				{
					outStatus->button[ nv::ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lRx-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRy-32768)/32767.0F, -1.0f, +1.0f );
					DWORD pov = dijs2.rgdwPOV[0];
					if( pov != -1 )
					{
						if( pov >= 31000 || pov < 5000 )
							outStatus->button[ nv::ctrl::BT_DIR3up ] = 1.0f;
						if( pov >= 4500 && pov < 14000 )
							outStatus->button[ nv::ctrl::BT_DIR3right ] = 1.0f;
						if( pov >= 13500 && pov < 23000 )
							outStatus->button[ nv::ctrl::BT_DIR3down ] = 1.0f;
						if( pov >= 22500 && pov < 32000 )
							outStatus->button[ nv::ctrl::BT_DIR3left ] = 1.0f;
					}
					outStatus->button[ nv::ctrl::BT_BUTTON0]  = dijs2.rgbButtons[3] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON1]  = dijs2.rgbButtons[1] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON2]  = dijs2.rgbButtons[0] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON3]  = dijs2.rgbButtons[2] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON4]  = Clamp( float(dijs2.lZ-32768)/32767.0F, 0.f, 1.f );
					outStatus->button[ nv::ctrl::BT_BUTTON5]  = Clamp( float(32768-dijs2.lZ)/32767.0F, 0.f, 1.f );
					outStatus->button[ nv::ctrl::BT_BUTTON6]  = dijs2.rgbButtons[4] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON7]  = dijs2.rgbButtons[5] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON8]  = dijs2.rgbButtons[6] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON9]  = dijs2.rgbButtons[7] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON10] = dijs2.rgbButtons[8] & 0x80 ? 1.0f : 0.0f;
					outStatus->button[ nv::ctrl::BT_BUTTON11] = dijs2.rgbButtons[9] & 0x80 ? 1.0f : 0.0f;
				}

				// ============================================================

				else
				{
					outStatus->button[ nv::ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
					outStatus->button[ nv::ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
					for( uint i = nv::ctrl::BT_BUTTON0 ; i < nv::ctrl::BT_MAX ; i++ )
						if( dijs2.rgbButtons[i-nv::ctrl::BT_BUTTON0] & 0x80 )
							outStatus->button[i] = 1.0f;
				}

				//DebugPrintf( "#2 : JOY\n" );
			}

			return TRUE;
		}
	} defMapper;



	bool IsCursorInClientRect ( POINT& outLocalCursor )
	{
		HWND targetHwnd, focusHwnd;
		nv::core::GetParameter( nv::core::PR_W32_HWND_FOCUS, (uint32*)&focusHwnd  );
		nv::core::GetParameter( nv::core::PR_W32_HWND,		 (uint32*)&targetHwnd );
		HWND hwnd = focusHwnd ? focusHwnd : targetHwnd;

		POINT cursorPos;
		GetCursorPos( &cursorPos );

		HWND underHwnd = WindowFromPoint( cursorPos );
		if( underHwnd == hwnd )
		{
			RECT clientRect;
			GetClientRect( hwnd, &clientRect );

			POINT clientPos = cursorPos;
			ScreenToClient( hwnd, &clientPos );

			outLocalCursor = clientPos;

			return (	clientPos.x >= 0
					&&	clientPos.y >= 0
					&&	clientPos.x < clientRect.right
					&&	clientPos.y < clientRect.bottom );
		}

		return FALSE;
	}



	bool SetCursorInClientRect ( POINT& outLocalCursor )
	{
		HWND targetHwnd, focusHwnd;
		nv::core::GetParameter( nv::core::PR_W32_HWND_FOCUS, (uint32*)&focusHwnd  );
		nv::core::GetParameter( nv::core::PR_W32_HWND,		 (uint32*)&targetHwnd );
		HWND hwnd = focusHwnd ? focusHwnd : targetHwnd;

		POINT cursorPos;
		GetCursorPos( &cursorPos );

		HWND foreHwnd = GetForegroundWindow();
		if( foreHwnd == hwnd )
		{
			RECT clientRect;
			GetClientRect( hwnd, &clientRect );

			POINT clientPos = cursorPos;
			ScreenToClient( hwnd, &clientPos );

			clientPos.x = Clamp( clientPos.x, 0L, clientRect.right-1L );
			clientPos.y = Clamp( clientPos.y, 0L, clientRect.bottom-1L );
			outLocalCursor = clientPos;

			ClientToScreen( hwnd, &clientPos );
			SetCursorPos( clientPos.x, clientPos.y );
			return TRUE;
		}

		return FALSE;
	}



	void UpdateDevMouse ( )
	{
		hasMousePos = IsCursorInClientRect( devMouseHwndPos );

		if( !hasMousePos )
		{
			bool fullscreen, constcursor;
			nv::core::GetParameter( nv::core::PR_W32_HWND_CONSTRAINTCURSOR, (bool*)&constcursor );
			nv::core::GetParameter( nv::core::PR_DX_FULLSCREEN, (bool*)&fullscreen  );
			if( fullscreen && constcursor )
			{
				hasMousePos = SetCursorInClientRect( devMouseHwndPos );
			}
		}

		if( hasMousePos )
		{
			// apply virtual / physical scale
			uint back_w, back_h, front_w, front_h;
			core::GetParameter( core::PR_BACK_FRAME_W, &back_w );
			core::GetParameter( core::PR_BACK_FRAME_H, &back_h );
			core::GetParameter( core::PR_FRONT_FRAME_W, &front_w );
			core::GetParameter( core::PR_FRONT_FRAME_H, &front_h );
			float sw = float(back_w) / float(front_w);
			float sh = float(back_h) / float(front_h);
			devMousePos.x = float(devMouseHwndPos.x) * sw;
			devMousePos.y = float(devMouseHwndPos.y) * sh;
		}
	}


}



void
nv::ctrl::DevInfo::Init( NV_DINPUTDEVICE inDev )
{
	HRESULT hr;
	dev = inDev;

	caps.dwSize = sizeof(caps);
	hr = dev->GetCapabilities( &caps );
	NV_ASSERT( SUCCEEDED(hr) );

	inst.dwSize = sizeof(inst);
	hr = dev->GetDeviceInfo( &inst );
	NV_ASSERT( SUCCEEDED(hr) );
}


void
nv::ctrl::DevInfo::Shut(  )
{
	if( dev )
		dev->Release();
	dev = NULL;
}


ulong
nv::ctrl::DevInfo::GetProductID(  )
{
	if( dev )
	{
		return inst.guidProduct.Data1;
	}
	else
	{
		return 0U;
	}
}


bool
nv::ctrl::DevInfo::IsProductID ( ulong inPID )
{
	return (GetProductID() == inPID);
}


bool
nvcore_Ctrl_Init()
{
    HRESULT hr;
	hr = NV_DINPUT_CREATE(	GetModuleHandle(NULL),
							DIRECTINPUT_VERSION, 
							NV_DINPUT_REFIID,
							(VOID**)&pDI,
							NULL	);

	if( !SUCCEEDED(hr) || !pDI )
	{
		return FALSE;
	}

	devJoyA.Init();
	devStatusMask	= 0;
	devMapper		= NULL;
	devKey.dev		= NULL;
	devMouse.dev	= NULL;
	opened			= FALSE;

	for( int i = 0 ; i < nv::ctrl::TP_MAX ; i++ ) {
		for( int j = 0 ; j < nv::ctrl::BT_MAX ; j++ ) {
			if( i==nv::ctrl::TP_PADDLE ) {
				deadzA[i][j][0] = 0.1f;
				deadzA[i][j][1] = 0.8f;
			} else {
				deadzA[i][j][0] = 0.f;
				deadzA[i][j][1] = 1.f;
			}
		}
	}

	hasMousePos = FALSE;

	return TRUE;
}


void
nvcore_Ctrl_Update()
{
	if( !opened )
		return;

	devStatusMask = 0;

	// Update KeyBoard
	if( devKey.dev )
	{
		HRESULT hr = devKey.dev->Acquire();
		if(	SUCCEEDED(hr) )
			if( defMapper.SetStatus(&devStatusA[0],0,nv::ctrl::TP_KEYBOARD,&devKey) )
				devStatusMask |= 1<<0;
	}

	// Update Mouse
	hasMousePos = FALSE;
	if( devMouse.dev )
	{
		HRESULT hr = devMouse.dev->Acquire();
		if(	SUCCEEDED(hr) )
		{
			if( defMapper.SetStatus(&devStatusA[1],1,nv::ctrl::TP_MOUSE,&devMouse) )
				devStatusMask |= 1<<1;

			UpdateDevMouse();
		}
	}

	// Update Joystick
	for( uint i = 0 ; i < devJoyA.size() ; i++ )
	{
		HRESULT hr;

		if( !devJoyA[i].dev )
			continue;

		// Get access to the input device. 
		hr = devJoyA[i].dev->Acquire(); 
		if( FAILED(hr) ) {
			continue;
		}

		// Poll the device to read the current state
		hr = devJoyA[i].dev->Poll();
		if( FAILED(hr) ) {
			hr = devJoyA[i].dev->Acquire();
			while( hr == DIERR_INPUTLOST )
				hr = devJoyA[i].dev->Acquire();
			// hr may be DIERR_OTHERAPPHASPRIO or other errors.  This
			// may occur when the app is minimized or in the process of 
			// switching, so just try again later 
			continue;
		}

		if( defMapper.SetStatus(&devStatusA[2+i],2+i,nv::ctrl::TP_PADDLE,&devJoyA[i]) )
			devStatusMask |= 1<<(2+i);
	}
}


void
nvcore_Ctrl_Shut()
{
	if( pDI ) {
		nv::ctrl::Close();
		pDI->Release();
		devJoyA.Shut();
	}
}


bool
nv::ctrl::SetMapper	(	Mapper*		inMapper	)
{
	devMapper = inMapper;
	return TRUE;
}


void
nv::ctrl::Open(		)
{
	if( opened )
		return;

	HRESULT hr;
	bool    fullscreen;
	HWND	targetHwnd, focusHwnd, hwnd;
	uint32	diCoopLevel;
	nv::core::GetParameter( nv::core::PR_W32_HWND, (uint32*)&targetHwnd );
	nv::core::GetParameter( nv::core::PR_W32_HWND, (uint32*)&focusHwnd  );
	nv::core::GetParameter( nv::core::PR_DX_FULLSCREEN, &fullscreen );
	nv::core::GetParameter( nv::core::PR_DI_COOPLEVEL, &diCoopLevel );
	hwnd = focusHwnd ? focusHwnd : targetHwnd;

	uint coopLevel;
	if( diCoopLevel )	coopLevel = diCoopLevel;
	else				coopLevel = fullscreen ? COOPLEVEL_FULLSCREEN : COOPLEVEL_WINDOWED;

	// Look for a keyboard
	{
		hr = pDI->CreateDevice(	GUID_SysKeyboard, &devKey.dev, NULL );
		if( !SUCCEEDED(hr) ) {
			devKey.dev = NULL;
		}
		else {
			// Set the default data format
			hr = devKey.dev->SetDataFormat( &c_dfDIKeyboard );
			if( !SUCCEEDED(hr) ) {
				devKey.Shut();
			}
			else {
		        // Set the cooperativity level
				hr = devKey.dev->SetCooperativeLevel( hwnd, diCoopLevel?diCoopLevel:COOPLEVEL_WINDOWED );		// to allow KEYDOWN window message !

				///////
				{
					DIPROPDWORD dipdw;
					dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
					dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
					dipdw.diph.dwObj        = 0;
					dipdw.diph.dwHow        = DIPH_DEVICE;
					dipdw.dwData            = 16;
					hr = devKey.dev->SetProperty( DIPROP_BUFFERSIZE, &dipdw.diph );
				}	
				///////
				if( !SUCCEEDED(hr) ) {
					devKey.Shut();
				} else {
					devKey.Init( devKey.dev );
				//	DebugPrintf( "DX keyboard device found !\n" );
				}
			}
		}
	}

	// Look for a mouse
	{
		hr = pDI->CreateDevice(	GUID_SysMouse, &devMouse.dev, NULL );
		if( !SUCCEEDED(hr) ) {
			devMouse.dev = NULL;
		}
		else {
			// Set the default data format
			hr = devMouse.dev->SetDataFormat( &c_dfDIMouse2 );
			if( !SUCCEEDED(hr) ) {
				devMouse.Shut();
			}
			else {
		        // Set the cooperativity level
				hr = devMouse.dev->SetCooperativeLevel(	hwnd, coopLevel );
				if( !SUCCEEDED(hr) ) {
					devMouse.Shut();
				}
				else {
					// Relative axis data
			        DIPROPDWORD dipdw;
					dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
					dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
					dipdw.diph.dwObj        = 0;
					dipdw.diph.dwHow        = DIPH_DEVICE;
					dipdw.dwData            = DIPROPAXISMODE_REL;
					hr = devMouse.dev->SetProperty( DIPROP_AXISMODE, &dipdw.diph );
					if( !SUCCEEDED(hr) ) {
						devMouse.Shut();
					} else {
						devMouse.Init( devMouse.dev );
						hasMousePos = FALSE;
					//	DebugPrintf( "DX mouse device found !\n" );
					}
				}
			}
		}
	}

	// Look for several joysticks
	{
		devJoyA.clear();
		hr = pDI->EnumDevices(	DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, NULL, DIEDFL_ATTACHEDONLY );
		if( !SUCCEEDED(hr) ) {
			devJoyA.clear();
		}
		else {
			// Up to 16 paddles !
			if( devJoyA.size() > 16 ) {
				for( uint i = 16 ; i < devJoyA.size() ; i++ )
					devJoyA[i].Shut();
				devJoyA.resize( 16 );
			}

			for( uint i = 0 ; i < devJoyA.size() ; i++ )
			{
				// Set the default data format
				NV_ASSERT( devJoyA[i].dev );
				hr = devJoyA[i].dev->SetDataFormat( &c_dfDIJoystick2 );
				if( !SUCCEEDED(hr) ) {
					devJoyA[i].Shut();
				}
				else {
					// Set the cooperative level
					hr = devJoyA[i].dev->SetCooperativeLevel( hwnd, coopLevel );
					if( !SUCCEEDED(hr) ) {
						devJoyA[i].Shut();
					} else {
						devJoyA[i].Init( devJoyA[i].dev );
					//	DebugPrintf( "DX joy device found %x !\n", uint32(devJoyA[i].dev) );
					}
				}
			}
		}
	}

	devStatusMask	= 0;

	// Init USER mapper
	if( devMapper )
		devMapper->Init();

	opened = TRUE;
}


uint
nv::ctrl::GetNbMax(	)
{
	return 32;
}




nv::ctrl::Status*
nv::ctrl::GetStatus(	uint	inCtrlNo		)
{
	Status* stt = NULL;
	if( opened && ((1<<inCtrlNo)&devStatusMask) )
	{
		stt = &devStatusA[ inCtrlNo ];
	}
	#ifdef DBG_OUTPUT_STATUS
	if( stt )
	{
		static int fno = 0;
		if( Fabs(stt->button[BT_DIR1X])>0.5f )		DebugPrintf( "<%d, ctrl %d, DIR1X:%f>\n", fno, inCtrlNo, stt->button[BT_DIR1X] );
		if( Fabs(stt->button[BT_DIR1Y])>0.5f )		DebugPrintf( "<%d, ctrl %d, DIR1Y:%f>\n", fno, inCtrlNo, stt->button[BT_DIR1Y] );
		if( Fabs(stt->button[BT_DIR2X])>0.5f )		DebugPrintf( "<%d, ctrl %d, DIR2X:%f>\n", fno, inCtrlNo, stt->button[BT_DIR2X] );
		if( Fabs(stt->button[BT_DIR2Y])>0.5f )		DebugPrintf( "<%d, ctrl %d, DIR2Y:%f>\n", fno, inCtrlNo, stt->button[BT_DIR2Y] );
		if( Fabs(stt->button[BT_DIR3right])>0.5f )	DebugPrintf( "<%d, ctrl %d, DIR3right:%f>\n", fno, inCtrlNo, stt->button[BT_DIR3right] );
		if( Fabs(stt->button[BT_DIR3left])>0.5f )	DebugPrintf( "<%d, ctrl %d, DIR3left:%f>\n",  fno, inCtrlNo, stt->button[BT_DIR3left] );
		if( Fabs(stt->button[BT_DIR3up])>0.5f )		DebugPrintf( "<%d, ctrl %d, DIR3up:%f>\n",	  fno, inCtrlNo, stt->button[BT_DIR3up] );
		if( Fabs(stt->button[BT_DIR3down])>0.5f )	DebugPrintf( "<%d, ctrl %d, DIR3down:%f>\n",  fno, inCtrlNo, stt->button[BT_DIR3down] );
		for( int i = BT_BUTTON0 ; i <= BT_BUTTON15 ; i++ ) {
			if( Fabs(stt->button[i])>0.5f )		DebugPrintf( "<%d, ctrl %d, BUTTON%d:%f>\n", fno, inCtrlNo, i-BT_BUTTON0, stt->button[i] );
		}
		fno++;
	}
	#endif
	return stt;
}


bool
nv::ctrl::GetKeyboard	(		uint			inCtrlNo,
								Keyboard*		outK		)
{
	if( GetStatus(inCtrlNo) && GetStatus(inCtrlNo)->type == TP_KEYBOARD )
	{
		if( outK )
		{
			Zero( *outK );
			for( int k = 0 ; k < 256 ; k++ ){
				uint pressed = (devKey.di_kb[k] != 0)?1:0;
				outK->keys [k>>5] |= (pressed) << (k&31);
				outK->ascii[k>>5] |= (devKey.di_ascii[k]) << (k&31);
			}
		}
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}





bool
nv::ctrl::GetMouse		(		uint			inCtrlNo,
								Mouse*			outM		)
{
	if( GetStatus(inCtrlNo) && GetStatus(inCtrlNo)->type == TP_MOUSE )
	{
		if( outM )
		{
			Zero( *outM );

			outM->has_xy = hasMousePos;
			outM->x = float( devMousePos.x );
			outM->y = float( devMousePos.y );

			outM->has_dxy = TRUE;
			outM->dx = float( devMouse.di_ms.lX );
			outM->dy = float( devMouse.di_ms.lY );

			for( uint i=0 ; i<8 ; i++ )
			{
				if( devMouse.di_ms.rgbButtons[i] & 0x80 )
					outM->buttons |= (1<<i);
			}
		}

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


nv::ctrl::DevInfo*
nv::ctrl::GetDevInfo	(		uint			inCtrlNo	)
{
	if( inCtrlNo == 0 )
		return &devKey;

	if( inCtrlNo == 1 )
		return &devMouse;

	int joyno = int(inCtrlNo) - 2;
	if( joyno>=0 && joyno<devJoyA.size() )
		return &devJoyA[joyno];

	return NULL;
}



void
nv::ctrl::Close()
{
	if( !opened )
		return;

	// Shut USER mapper
	if( devMapper )
		devMapper->Shut();
	devMapper = NULL;

	devKey.Shut();
	devMouse.Shut();
	for( uint i=0 ; i<devJoyA.size() ; i++ )
		devJoyA[i].Shut();
	devJoyA.clear();
	devStatusMask = 0;
	opened = FALSE;
}


bool
nv::ctrl::SetActuators	(	uint		inCtrlNo,
							float		inLowSpeed,
							float		inHighSpeed	)
{
	return FALSE;
}


bool
nv::ctrl::GetDefDeadZone	(		Type			inType,
									Button			inButton,
									float*			outLo,
									float*			outHi			)
{
	int t = int(inType);
	int b = int(inButton);
	if( t<0 || t>=TP_MAX )	return FALSE;
	if( b<0 || b>=BT_MAX )	return FALSE;
	if( outLo )		*outLo = deadzA[t][b][0];
	if( outHi )		*outHi = deadzA[t][b][1];
	return TRUE;
}


bool
nv::ctrl::SetDefDeadZone	(		Type			inType,
									Button			inButton,
									float			inLo,
									float			inHi			)
{
	float lo, hi;
	if( !GetDefDeadZone(inType,inButton,&lo,&hi) )
		return FALSE;
	if( inLo >= 0.f )	lo = Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return FALSE;
	int t = int(inType);
	int b = int(inButton);
	deadzA[t][b][0] = lo;
	deadzA[t][b][1] = hi;
	return TRUE;
}

float
nv::ctrl::Status::Filtered	(		Button			inButton,
									float			inLo,
									float			inHi	)
{
	if( !opened )
		return 0.f;

	if( int(inButton)<0 || int(inButton)>=BT_MAX )
		return 0.f;

	float lo, hi;
	if( !GetDefDeadZone(type,inButton,&lo,&hi) )
		return 0.f;

	if( inLo >= 0.f )	lo = Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return 0.f;

	float b  = Clamp( button[int(inButton)], -1.f, +1.f );
	float ab = Absf( b );
	float sb = ( b>=0.f ? 1.f : -1.f );

	if( ab <= lo )	return 0.f;
	if( ab >= hi )	return sb;
	return sb * (ab-lo)/(hi-lo);
}



