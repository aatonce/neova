/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <kernel/pc/nvwindows.h>
#include <direct.h>


#define IDI_ICON1                       108
#define IDI_ICON2                       109
#define IDI_ICON3                       110
#define IDI_ICON4                       111
#define IDI_ICON5                       112
#define IDI_ICON6                       113
#define IDS_STRING116                   116


#include <nova.h>
#include <kernel/Common/nvkcore_mem.h>
#include <kernel/pc/nvhrdw.h>




#define	NVCORE_DECL( ID )					\
	bool	nvcore_##ID##_Init		();		\
	void	nvcore_##ID##_Update	();		\
	void	nvcore_##ID##_Shut		();

NVCORE_DECL( Math	)
NVCORE_DECL( Jpeg	)
NVCORE_DECL( Libc	)
NVCORE_DECL( Report )
NVCORE_DECL( Mem	)
NVCORE_DECL( Clock	)
NVCORE_DECL( File	)
NVCORE_DECL( NVR	)
NVCORE_DECL( Net	)
NVCORE_DECL( Ctrl	)
NVCORE_DECL( Param	)
NVCORE_DECL( Ext	)





namespace
{

	bool		coreInitialized = FALSE;
	bool		mainCalled		= FALSE;
	TCHAR		szWindowClass[]	= "NEOVA";
	uint32		wndProcMsg		= 0;
	bool		visState		= TRUE;
	uint32		main_sp;
	bool		exitAsked		= FALSE;


	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		switch( message )
		{
			case WM_COMMAND :
				break;
			case WM_PAINT :
				break;
			case WM_QUIT :
			case WM_DESTROY :
				wndProcMsg |= nv::core::UPD_EXITED;
				return 0;
			case WM_CLOSE :
				wndProcMsg |= nv::core::UPD_EXIT_ASKED;
				exitAsked	= TRUE;
				return 0;
			case WM_SETFOCUS :
				wndProcMsg |= nv::core::UPD_SET_FOCUS;
				break;
			case WM_KILLFOCUS :
				wndProcMsg |= nv::core::UPD_LOST_FOCUS;
				break;
			case WM_KEYDOWN :
				if( wParam == VK_ESCAPE ){
					//wndProcMsg |= nv::core::UPD_EXIT_ASKED;
					//exitAsked	= TRUE;
				}
				break;
			case WM_ACTIVATE :
				{
					bool fullscreen;
					nv::core::GetParameter( nv::core::PR_DX_FULLSCREEN, &fullscreen );
					if( fullscreen ) {
						if( LOWORD(wParam) == WA_INACTIVE )
							ShowWindow( hWnd, SW_HIDE|SW_MINIMIZE );
						else
							ShowWindow( hWnd, SW_SHOWNORMAL );
					}
				}
				break;
			case WM_SYSCOMMAND :
				if( wParam == SC_SCREENSAVE ) {
					bool screensaver;
					nv::core::GetParameter( nv::core::PR_W32_SCREENSAVER, &screensaver );
					if( !screensaver )	return 1;
					break;
				}
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	bool IsHWNDVisible	(			)
	{
		typedef nv::vector<RECT>	VisRectA;
		VisRectA					visRectA;
		RECT						visRect;
		RECT						topRect;

		HWND targetHwnd, focusHwnd;
		nv::core::GetParameter( nv::core::PR_W32_HWND_FOCUS, (uint32*)&focusHwnd  );
		nv::core::GetParameter( nv::core::PR_W32_HWND,		 (uint32*)&targetHwnd );
		HWND hwnd = focusHwnd ? focusHwnd : targetHwnd;

		if(		GetTopWindow(NULL) == hwnd
			||	GetForegroundWindow() == hwnd	)
			return TRUE;

		if( !IsWindowVisible(hwnd) )
			return FALSE;

		if( !GetWindowRect(hwnd,&visRect) )
			return FALSE;
		visRectA.push_back( visRect );

		for( ;; )
		{
			hwnd = GetNextWindow( hwnd, GW_HWNDPREV );

			if( !hwnd || visRectA.size() == 0 )
				break;

			if( !IsWindowVisible(hwnd) )
				continue;

			if( !GetWindowRect(hwnd,&topRect) )
				continue;

			// rect clipping
			for( uint i = 0 ; i < visRectA.size() ; )
			{
				visRect = visRectA[i];

				// no intersection ?
				if(		visRect.left > topRect.right
					||	topRect.left > visRect.right
					||	visRect.top  > topRect.bottom
					||	topRect.top  > visRect.bottom	)
				{
					i++;
					continue;
				}

				visRectA.erase( visRectA.begin()+i );

				// masked ?
				if(		topRect.left   <= visRect.left
					&&	topRect.top    <= visRect.top
					&&	topRect.right  >= visRect.right
					&&	topRect.bottom >= visRect.bottom )
					continue;

				// left clipping
				if( visRect.left < topRect.left )
				{
					RECT cvisRect = visRect;
					cvisRect.right = topRect.left - 1;
					visRectA.push_back( cvisRect );
				}

				// right clipping
				if( visRect.right > topRect.right )
				{
					RECT cvisRect = visRect;
					cvisRect.left = topRect.right + 1;
					visRectA.push_back( cvisRect );
				}

				// top clipping
				if( visRect.top < topRect.top )
				{
					RECT cvisRect = visRect;
					cvisRect.bottom = topRect.top - 1;
					visRectA.push_back( cvisRect );
				}

				// bottom clipping
				if( visRect.bottom > topRect.bottom )
				{
					RECT cvisRect = visRect;
					cvisRect.top = topRect.bottom + 1;
					visRectA.push_back( cvisRect );
				}
			}
		}

		return ( visRectA.size() > 0 );
	}


	bool IsCursorInClientRect ()
	{
		HWND targetHwnd, focusHwnd;
		nv::core::GetParameter( nv::core::PR_W32_HWND_FOCUS, (uint32*)&focusHwnd  );
		nv::core::GetParameter( nv::core::PR_W32_HWND,		 (uint32*)&targetHwnd );
		HWND hwnd = focusHwnd ? focusHwnd : targetHwnd;

		POINT cursorPos;
		GetCursorPos( &cursorPos );

		HWND underHwnd = WindowFromPoint( cursorPos );
		if( underHwnd==hwnd )
		{
			RECT clientRect;
			GetClientRect( hwnd, &clientRect );

			POINT clientPos = cursorPos;
			ScreenToClient( hwnd, &clientPos );

			return (	clientPos.x >= 0
					&&	clientPos.y >= 0
					&&	clientPos.x < clientRect.right
					&&	clientPos.y < clientRect.bottom );
		}

		return FALSE;
	}


	bool my_show_cursor = true;

	void MyShowCursor ( bool inShow )
	{
		if( inShow )
		{
			if( !my_show_cursor )
			{
				::ShowCursor( TRUE );
				my_show_cursor = TRUE;
			}
		}
		else
		{
			if( my_show_cursor )
			{
				::ShowCursor( FALSE );
				my_show_cursor = FALSE;
			}
		}
	}


}




bool
nv::core::Startup__	(	void*		inHInstance,
						char*		inStrCmdLine,
						uint32		inBaseSP		)
{
	NV_COMPILE_TIME_CHECK_TYPES();

	main_sp = inBaseSP;

	static char cwd[512];
	_getcwd( cwd, sizeof(cwd) );

	nvcore_Param_Init();
	nv::core::SetParameter( nv::core::PR_SYS_CMDLINE,      pcstr(inStrCmdLine) );
	nv::core::SetParameter( nv::core::PR_SYS_CWD,		   pcstr(cwd) );
	nv::core::SetParameter( nv::core::PR_W32_HINSTANCE,    uint32(inHInstance) );
	nv::core::SetParameter( nv::core::PR_W32_HWND_DEFPROC, uint32(WndProc)   );

	mainCalled = TRUE;
	return TRUE;
}



bool
nv::core::Init		(		)
{
	if( coreInitialized )
		return TRUE;

	mem::NativeInit();

	Printf( ">> Neova Core-Engine is running\n" );
	Printf( ">> (c) 2002-2008 AtOnce Technologies\n" );
	Printf( "\n" );

	if( !mainCalled ) {
		NV_ERROR( "\n\n\n** Overload the main() function is not allowed !\n** Use the function [void GameMain()] as your application entrypoint." );
		return FALSE;
	}

	// CoreEngine revision
	
	Printf( "<Neova> CoreEngine %d.%d-%s\n", GetReleaseNumber(), GetReleaseRevision(), GetDebugLevel() );
	#ifdef TIME_LIMITED_EVALUATION
	Printf( "<Neova> Time limited version\n" );
	#endif

	// Link areas
	Printf( "<Neova> Exe   @%x, %dKo\n", 0, 0 );
	Printf( "<Neova> Stack @%x, %dKo\n", main_sp, 0 );
	Printf( "<Neova> Heap  @%x, %dKo\n", 0, 0 );
	SetParameter( PR_MEM_APP_ADDR,			0U			);
	SetParameter( PR_MEM_APP_BSIZE,			0U			);
	SetParameter( PR_MEM_APP_HEAP_ADDR,		0U			);
	SetParameter( PR_MEM_APP_HEAP_BSIZE,	0U			);
	SetParameter( PR_MEM_APP_STACK_ADDR,	main_sp		);
	SetParameter( PR_MEM_APP_STACK_BSIZE,	0U			);

	// Create main window ?
	HWND hwnd;
	GetParameter( PR_W32_HWND, (uint32*)&hwnd );
	if( hwnd == 0 )
	{
		HINSTANCE hInstance;
		bool	  fullscreen;
		uint32	  frontW, frontH;
		GetParameter( PR_W32_HINSTANCE, (uint32*)&hInstance );
		GetParameter( PR_FRONT_FRAME_W,	&frontW	);
		GetParameter( PR_FRONT_FRAME_H,	&frontH	);
		GetParameter( PR_DX_FULLSCREEN, &fullscreen	);
		NV_ASSERT( hInstance );

		// Register the window class
		WNDCLASS wndClass = { CS_HREDRAW | CS_VREDRAW | CS_OWNDC/* | CS_SAVEBITS*/,
							  (WNDPROC)WndProc, 
							  0, 0,
							  hInstance,
							  NULL, NULL,
							  HBRUSH(COLOR_BACKGROUND),
							  NULL,
							  szWindowClass		};
		RegisterClass(&wndClass);

	    MSG msg;
		PeekMessage( &msg, NULL, 0U, 0U, PM_NOREMOVE );

		// Main Window
		HWND hwnd;
		hwnd  = CreateWindowEx(	WS_EX_APPWINDOW,
								szWindowClass,
								"",
								WS_OVERLAPPEDWINDOW,
								0, 0,
								16, 16,
								0L, 0L, hInstance, 0L );

		if( !hwnd )
		{
			NV_ERROR( "nv::core::Init: Error: Failed to create the main window !" );
			return FALSE;
		}

		SetParameter( PR_W32_HWND, uint32(hwnd) );
		SetParameter( PR_W32_HWND_MANAGED, TRUE );

		MoveWindow( hwnd, 0, 0, frontW, frontH, FALSE );
		RECT r;
		GetClientRect( hwnd, &r );
		int wx = int(frontW) + ( int(frontW) - int(r.right)  );
		int wy = int(frontH) + ( int(frontH) - int(r.bottom) );
		int sx = GetSystemMetrics( SM_CXSCREEN );
		int sy = GetSystemMetrics( SM_CYSCREEN );
		int px = (sx-wx)/2;
		int py = (sy-wy)/2;

		uint defpos = 0;
		GetParameter( PR_W32_HWND_DEFPOS, &defpos );
		if( defpos != ~0U )
		{
			px = uint(defpos>>16) & 0xFFFF;
			py = uint(defpos>>0 ) & 0xFFFF;
			px = Clamp( px, 0, sx-wx );
			py = Clamp( py, 0, sy-wy );
		}
		MoveWindow( hwnd, px, py, wx, wy, FALSE );

		HICON big_icon=0, small_icon=0;
		pcstr hwnd_title = NULL;
		GetParameter( PR_W32_HWND_BIGICON, (uint32*)&big_icon );
		GetParameter( PR_W32_HWND_SMALLICON, (uint32*)&small_icon );
		GetParameter( PR_W32_HWND_TITLE, &hwnd_title );

		if( small_icon )
			SendMessage( hwnd, (UINT)WM_SETICON, ICON_SMALL, (LPARAM)small_icon );

		if( big_icon )
			SendMessage( hwnd, (UINT)WM_SETICON, ICON_BIG, (LPARAM)big_icon );

		if( hwnd_title )
			::SetWindowText( hwnd, hwnd_title );

		ShowWindow( hwnd, SW_SHOWNORMAL );
	}

	Printf( "<Neova> Components initialization ...\n" );
	nvcore_Report_Init();
	nvcore_Mem_Init();
	nvcore_Libc_Init();
	nvcore_Math_Init();
	nvcore_File_Init();
	nvcore_Net_Init();
	nvcore_Ctrl_Init();
	nvcore_NVR_Init();
	nvcore_Clock_Init();
	nvcore_Jpeg_Init();
	nvcore_Ext_Init();
	Printf( "<Neova> Components initialized.\n" );

	coreInitialized = TRUE;
	mem::LogOpen();
	return TRUE;
}



void
nv::core::Shut		(		)
{
	if( !coreInitialized )
		return;

	nvcore_Ctrl_Shut();
	nvcore_Jpeg_Shut();
	nvcore_Net_Shut();
	nvcore_NVR_Shut();
	nvcore_File_Shut();
	nvcore_Clock_Shut();
	nvcore_Math_Shut();
	nvcore_Libc_Shut();
	nvcore_Mem_Shut();
	nvcore_Report_Shut();
	nvcore_Param_Shut();
	nvcore_Ext_Shut();
	PostQuitMessage(0);  

	mem::NativeShut();

	coreInitialized = FALSE;
}


void
nv::core::Exit()
{
	NV_MESSAGE( "\n\n\n** SYSTEM ABORTED **\n\n\n" );
	Shut();
	exit(0);
}

bool
nv::core::ExitAsked	 ( )
{
	return exitAsked;
}




uint
nv::core::Update	(	)
{
	// Manage message & window ?
	wndProcMsg = 0;
	HWND targetHwnd, focusHwnd, hwnd;
	bool hwndManaged = FALSE;
	core::GetParameter( core::PR_W32_HWND_FOCUS,   (uint32*)&focusHwnd  );
	core::GetParameter( core::PR_W32_HWND,		   (uint32*)&targetHwnd );
	core::GetParameter( core::PR_W32_HWND_MANAGED, &hwndManaged );
	hwnd = focusHwnd ? focusHwnd : targetHwnd;
	if( hwnd && hwndManaged )
	{
		// Alertable state for io access !
		SleepEx( 0, TRUE );

		// Window Proc
		MSG msg;
		while( PeekMessage(&msg,NULL,0,0,PM_REMOVE) )
			DispatchMessage(&msg);

		// Visibility state
		if( IsWindow(hwnd) )
		{
			bool _visState = IsHWNDVisible();
			if( visState && !_visState )
				wndProcMsg |= UPD_LOST_VISIBILITY;
			if( !visState && _visState )
				wndProcMsg |= UPD_SET_VISIBILITY;
			visState = _visState;
		}
	}

	Hrdw::UpdateDeviceState();
	Hrdw::DeviceState devS = Hrdw::GetDeviceState();
	if( devS == Hrdw::NV_DEVICE_LOST )
	{
		wndProcMsg |= UPD_LOST_DEVICE;
	}
	else if ( devS == Hrdw::NV_DEVICE_NEED_RESET )
	{
		Hrdw::ResetDevice();
		wndProcMsg |= UPD_DATA_REBUILDING_NEEDED;
	}

	// Update components
	nvcore_Param_Update();
	nvcore_Jpeg_Update();
	nvcore_Report_Update();
	nvcore_Mem_Update();
	nvcore_Libc_Update();
	nvcore_Math_Update();
	nvcore_Clock_Update();
	nvcore_File_Update();
	nvcore_Net_Update();
	nvcore_Ctrl_Update();
	nvcore_NVR_Update();
	nvcore_Ext_Update();

	bool hide_cursor;
	core::GetParameter( core::PR_HIDE_CURSOR, &hide_cursor );
	if( hide_cursor )
	{
		if( IsCursorInClientRect() )
			MyShowCursor( FALSE );
		else
			MyShowCursor( TRUE );
	}

	return wndProcMsg;
}



nv::core::SystemID
nv::core::GetSystemID		(		)
{
	return MSWindows;
}




nv::core::Aspect
nv::core::GetAspect ( )
{
	return ASPECT_FULL;
}

nv::core::VideoMode		
nv::core::GetVideoMode		(	)
{
	return VM_Other;
}

nv::core::SoundMode		
nv::core::GetSoundMode		(	)
{
	return nv::core::SoundMode(nv::core::SM_Stereo);	
}

nv::core::DateNotation
nv::core::GetDateNotation (	)
{
	return DATE_DDMMYYYY;
}


nv::core::Language
nv::core::GetLanguage ( )
{
	return LANG_ENGLISH;
}


nv::core::SummerTime
nv::core::GetSummerTime ( )
{
	return SUMT_OFF;
}


nv::core::TimeNotation
nv::core::GetTimeNotation ( )
{
	return TIME_24H;
}



