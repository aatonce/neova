/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvWindows.h>
#include <Nova.h>


//#define	USING_ASYNC




namespace
{
	HANDLE		fHd;
	OVERLAPPED	fOverlap;
	uint32		fbOffset;
	bool		ioPending;

	void WINAPI ioCompletedCB	(	DWORD			/*dwErrorCode*/,
									DWORD			/*dwNumberOfBytesTransfered*/,
									LPOVERLAPPED	/*lpOverlapped*/				)
	{
		NV_ASSERT( ioPending );
		ioPending = FALSE;
	}

	char* subDev[] = {
		// name					prefix				suffix
		"WinFS",				NULL,				NULL,
		"WinFS (cwd)",			".\\",				NULL,
	};

}




bool
nvcore_File_io_Init	(	)
{
	fHd = NULL;
	ioPending = FALSE;
	return TRUE;
}



nv::file::Status
nvcore_File_io_GetStatus	(	)
{
	return nv::file::FS_OK;
}


bool
nvcore_File_io_IsOpened	(	)
{
	return ( fHd != NULL );
}



bool
nvcore_File_io_IsReady	(	)
{
	// Not in use !
	if( !nvcore_File_io_IsOpened() )
		return TRUE;
#ifdef USING_ASYNC
	return !ioPending;
#else
	return TRUE;
#endif
}


bool
nvcore_File_io_IsSuccess	(		)
{
	// Not in use !
	if( !nvcore_File_io_IsOpened() )
		return FALSE;
	return TRUE;
}


void
nvcore_File_io_Sync	(	)
{
	while( !nvcore_File_io_IsReady() ) {}
}

static bool GetSystemVersion(OSVERSIONINFOEX &osvi)
{
	BOOL bOsVersionInfoEx;

	Zero(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

	if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) ){
	  osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
	  if (! GetVersionEx ( (OSVERSIONINFO *) &osvi) ) 
		 return FALSE;
	}
	return TRUE;
}

bool
nvcore_File_io_Open(	pcstr		inFilename,
						uint32	&	outBSize	)
{
	// In use !
	if( nvcore_File_io_IsOpened() )
		return FALSE;

	// Getting system information : On Win98 GetLastError do not work !
	OSVERSIONINFOEX osVersion;
	bool validGetLastError = TRUE;
	if ( GetSystemVersion(osVersion) ) {
		if (osVersion.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS ) 
			validGetLastError = FALSE;
	}

	// Open existing file in ASync mode
	fHd = CreateFile(	inFilename,
						GENERIC_READ,
						FILE_SHARE_READ,
						NULL,
						OPEN_EXISTING,
					#ifdef USING_ASYNC
						FILE_FLAG_OVERLAPPED,
					#else
						FILE_ATTRIBUTE_NORMAL,
					#endif
						NULL	);
	DWORD lastErr = GetLastError();
	if( fHd == INVALID_HANDLE_VALUE ) {
		fHd = NULL;
		return FALSE;
	}

	// Get file size
	DWORD shigh, slow;
	slow = GetFileSize( fHd, &shigh );
	lastErr = validGetLastError? GetLastError() : NO_ERROR;

	if(		shigh!=0
		||	slow <= 0
		||	lastErr!=NO_ERROR ) {
		CloseHandle( fHd );
		fHd = NULL;
		return FALSE;
	}

	outBSize = uint32( slow );

	// Rewind
	SetFilePointer( fHd, 0, NULL, FILE_BEGIN );
	lastErr = validGetLastError? GetLastError() : NO_ERROR;
	NV_ASSERT( lastErr == NO_ERROR );
	fbOffset = 0;

	return TRUE;
}


bool
nvcore_File_io_Read		(	uint		inSysFlags,
							pvoid		inBufferPtr,
							uint32		inBSize,
							uint32		inBOffset0,
							uint32		inBOffset1,
							uint32		inBOffset2,
							uint32		inBOffset3	)
{
	// Not in use or invalid call !
	if(		!nvcore_File_io_IsOpened()
		||	!inBufferPtr	)
		return FALSE;

	if( inBSize == 0 )
		return TRUE;

	// Prevent sync
	nvcore_File_io_Sync();

	// Select nearest offset
	uint32	d0		= fbOffset > inBOffset0 ? fbOffset-inBOffset0 : inBOffset0-fbOffset;
	uint32	d1		= fbOffset > inBOffset1 ? fbOffset-inBOffset1 : inBOffset1-fbOffset;
	uint32	d2		= fbOffset > inBOffset2 ? fbOffset-inBOffset2 : inBOffset2-fbOffset;
	uint32	d3		= fbOffset > inBOffset3 ? fbOffset-inBOffset3 : inBOffset3-fbOffset;
	uint32  nearest	= inBOffset0;
	uint32	dmin	= d0;
	if( d1 < dmin )		{	dmin = d1;	nearest = inBOffset1;	}
	if( d2 < dmin )		{	dmin = d2;	nearest = inBOffset2;	}
	if( d3 < dmin )		{	dmin = d3;	nearest = inBOffset3;	}

#ifdef USING_ASYNC
	fOverlap.hEvent		= NULL;
	fOverlap.OffsetHigh	= 0;
	fOverlap.Offset		= nearest;

	ioPending = TRUE;
	BOOL res = ReadFileEx( fHd, inBufferPtr, inBSize, &fOverlap, ioCompletedCB );

	if( !res )
	{
		// failed !?
		ioPending = FALSE;
		return FALSE;
	}
	else
	{
		DWORD lastErr = GetLastError();
		if( lastErr == ERROR_SUCCESS ) {
			fbOffset = nearest + inBSize;
			return TRUE;
		} else {
			// incorrect async io is successfully pending !
			nvcore_File_io_Sync();
			fbOffset = nearest /*+ ??*/;
			return FALSE;
		}
	}
#else
	LARGE_INTEGER li;
	li.QuadPart = nearest;
	li.LowPart = SetFilePointer( fHd, li.LowPart, &li.HighPart, FILE_BEGIN );
	if( li.LowPart==INVALID_SET_FILE_POINTER && GetLastError()!=NO_ERROR )
		return FALSE;

	DWORD readen;
	BOOL res = ReadFile( fHd, inBufferPtr, inBSize, &readen, NULL );
	fbOffset = nearest + readen;
	return (res!=0);
#endif
}


bool
nvcore_File_io_DumpFrom	(	pcstr	inFilename,
							pvoid&	outBuffer,
							uint&	outBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;
	FILE* f = fopen( inFilename, "rb" );
	if( !f )	return FALSE;
	fseek( f, 0, SEEK_END );
	outBSize = ftell( f );
	if( outBSize==0 ) {
		outBuffer = NULL;
		return TRUE;
	}
	outBuffer = NvMallocA( outBSize, 64 );
	if( !outBuffer ) {
		fclose( f );
		return FALSE;
	}
	fseek( f, 0, SEEK_SET );
	uint readen = fread( outBuffer, 1, outBSize, f );
	if( readen != outBSize ) {
		NvFree( outBuffer );
		fclose( f );
		return FALSE;
	}
	fclose( f );
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_DumpTo	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;
	if( !inBuffer )		return FALSE;
	FILE* f = fopen( inFilename, "wb" );
	if( !f )	return FALSE;
	fwrite( inBuffer, inBSize, 1, f );
	fclose( f );
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_Append	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;
	if( !inBuffer )		return FALSE;
	FILE* f = fopen( inFilename, "a+b" );
	if( !f )	return FALSE;
	fwrite( inBuffer, inBSize, 1, f );
	fclose( f );
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_Close	(	)
{
	if( nvcore_File_io_IsOpened() )	{
		nvcore_File_io_Sync();
		CloseHandle( fHd );
		fHd = NULL;
	}
	return TRUE;
}


void
nvcore_File_io_Shut	(	)
{
	nvcore_File_io_Close();
}



uint
nvcore_File_io_GetNbSubDevice	(		)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	return nb_subdev;
}


pcstr
nvcore_File_io_GetSubDeviceName		(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+0];
}


pcstr
nvcore_File_io_GetSubDevicePrefix	(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+1];
}


pcstr
nvcore_File_io_GetSubDeviceSuffix	(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+2];
}




