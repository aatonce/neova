/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvWindows.h>
#include <Nova.h>



bool
nvcore_Report_Init()
{
	return TRUE;
}


void
nvcore_Report_Update()
{
	//
}


void
nvcore_Report_Shut()
{
	//
}


void
nv::report::Output	(	Type		inReportT,
						pcstr		inMessage,
						pcstr		inFilename,
						pcstr		inFctName,
						int			inLineNo	)
{
	char tmp[ 16*1024 ];
	if( inFilename || inFctName )
		Sprintf( tmp, "%s [%s, %s(), L%d]\n", inMessage, inFilename, inFctName, inLineNo );
	else
		Sprintf( tmp, "%s\n", inMessage );

	char tmp2[ 256 ];
	Sprintf( tmp2, "<Neova %d.%d-%s>",
		core::GetReleaseNumber(),
		core::GetReleaseRevision(),
		core::GetDebugLevel()	);
		
	if( inReportT == T_MESSAGE )
	{
		Strcat( tmp2, " Message : " );
		OutputDebugString( tmp2 );
		OutputDebugString( tmp );
	}
	else if( inReportT == T_WARNING )
	{
		Strcat( tmp2, " Warning : " );
		OutputDebugString( tmp2 );
		OutputDebugString( tmp );
		MessageBox(NULL,inMessage,"Neova WARNING",MB_OK|MB_ICONWARNING|MB_APPLMODAL|MB_TOPMOST);
	}
	else if( inReportT == T_ERROR )
	{
		Strcat( tmp2, " Error : " );
		OutputDebugString( tmp2 );
		OutputDebugString( tmp );

		// Output call-stack
		nv::stack::Frame sf;
		if( stack::GetFrame(sf) && stack::GetBackFrame(sf,sf) )
			stack::OutputCallstack( sf );

		MessageBox(NULL,inMessage,"Neova ERROR",MB_OK|MB_ICONERROR|MB_APPLMODAL|MB_TOPMOST);
	}
}


nv::report::Action
nv::report::AskUserAction	(	)
{
	int act = MessageBox(NULL,"Please select an action to do :","Neova User Action",MB_ABORTRETRYIGNORE|MB_ICONQUESTION|MB_APPLMODAL|MB_TOPMOST);
	if( act == IDABORT )	return A_BREAK;
	if( act == IDRETRY )	return A_CONTINUE;
	if( act == IDIGNORE )	return A_DONTCARE;
	return A_EXIT;
}


