/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvWindows.h>

//#define NO_WIN32_LEAN_AND_MEAN
#include <shlobj.h>

#include <Nova.h>
using namespace nv;



#define	MAX_SLOT		(16)
#define	MAX_SLOT_BSIZE	(8*1024*1024)



namespace
{

	enum StateID
	{
		S_IDLE	= 0,

		SCR_OPENING,
		SCR_WRITING,
		SCR_CLOSING,

		SWR_OPENING,
		SWR_SEEKING,
		SWR_WRITING,
		SWR_CLOSING,

		SRD_OPENING,
		SRD_SEEKING,
		SRD_READING,
		SRD_CLOSING,
	};

	struct DateTime
	{
		uint8		s_time;		// second
		uint8		m_time;		// minute
		uint8		h_time;		// hour
		uint8		d_date;		// day
		uint8		m_date;		// month
		uint16		y_date;		// year
	};


	char				productCode[32+1];

	int					state;
	HANDLE				state_hd;
	uint32				state_params[4];
	nv::nvram::Result	state_res;

	HANDLE				firstnext_hd;
	WIN32_FIND_DATA		firstnext_finfo;
	char				firstnext_path[ MAX_PATH ];


	uint32	GetFileInfo(	pcstr		inFilename,
							DateTime*	outCreateTime = NULL,
							DateTime*	outModifyTime = NULL	)
	{
		HANDLE hd = CreateFile(	inFilename,
								GENERIC_READ,
								FILE_SHARE_READ,
								NULL,
								OPEN_EXISTING,
								FILE_ATTRIBUTE_NORMAL,
								NULL	);
		if( hd == INVALID_HANDLE_VALUE )
			return 0;

		DWORD shigh, slow;
		slow = GetFileSize( hd, &shigh );
		uint32 bsize = 0;
		if( slow != -1 )
			bsize = slow;

		FILETIME   _cTime, _mTime;
		SYSTEMTIME cTime, mTime;
		memset( &_cTime, 0, sizeof(FILETIME) );
		memset( &_mTime, 0, sizeof(FILETIME) );
		GetFileTime( hd, &_cTime, NULL, &_mTime );
		FileTimeToLocalFileTime( &_cTime, &_cTime );
		FileTimeToLocalFileTime( &_mTime, &_mTime );
		FileTimeToSystemTime( &_cTime, &cTime );
		FileTimeToSystemTime( &_mTime, &mTime );

		if( outCreateTime )
		{
			outCreateTime->s_time = cTime.wSecond;
			outCreateTime->m_time = cTime.wMinute;
			outCreateTime->h_time = cTime.wHour;
			outCreateTime->d_date = cTime.wDay;
			outCreateTime->m_date = cTime.wMonth;
			outCreateTime->y_date = cTime.wYear;
		}

		if( outModifyTime )
		{
			outModifyTime->s_time = mTime.wSecond;
			outModifyTime->m_time = mTime.wMinute;
			outModifyTime->h_time = mTime.wHour;
			outModifyTime->d_date = mTime.wDay;
			outModifyTime->m_date = mTime.wMonth;
			outModifyTime->y_date = mTime.wYear;
		}

		CloseHandle( hd );
		return bsize;
	}


	bool GetRecUsedBSize	(	pcstr	inPath,	uint32 & outBSize	)
	{
		outBSize = 0;

		HANDLE				hd;
		WIN32_FIND_DATA		finfo;

		char tmp[MAX_PATH];
		Sprintf( tmp, "%s\\*", inPath );

		hd = FindFirstFile( tmp, &finfo );
		if( hd == INVALID_HANDLE_VALUE )
			return FALSE;

		do {
			if(		Strcmp(finfo.cFileName,"." ) == 0
				||	Strcmp(finfo.cFileName,"..") == 0  )
				continue;

			Sprintf( tmp, "%s\\%s", inPath, finfo.cFileName );
			DWORD res = GetFileAttributes( tmp );
			if( (res!=-1) && ((res&FILE_ATTRIBUTE_DIRECTORY)!=0) )
			{
				uint bsize = 0;
				if( GetRecUsedBSize(tmp,bsize) )
					outBSize += bsize;
			}
			else
			{
				outBSize += GetFileInfo( tmp );
			}
		} while( FindNextFile(hd,&finfo) );

		FindClose( hd );

		return TRUE;
	}


	bool remove_directory_rec( pcstr inPath )
	{
		HANDLE				hd = 0;
		WIN32_FIND_DATA		finfo;

		bool again = TRUE;
		bool res   = TRUE;

		while( again ) {

			hd = FindFirstFile( inPath, &finfo );
			if( hd == INVALID_HANDLE_VALUE )
				return FALSE;

			char tmp[128];
			again = FALSE;

			do {
				if(		Strcmp(finfo.cFileName,"." ) == 0
					||	Strcmp(finfo.cFileName,"..") == 0  )
					continue;

				FindClose( hd );

				Sprintf( tmp, "%s\\%s", inPath, finfo.cFileName );
				DWORD attr = GetFileAttributes( tmp );
				if( (attr!=-1) && ((attr&FILE_ATTRIBUTE_DIRECTORY)!=0) ) {
					if( !remove_directory_rec(tmp) )	res = FALSE;
					else						again = TRUE;
				} else {
					if( DeleteFile(tmp)==0 )	res = FALSE;
					else						again = TRUE;
				}
				break;
			} while( FindNextFile(hd,&finfo) );

		}

		FindClose( hd );
		return ( res && RemoveDirectory(inPath)!=0 );
	}


	bool GetSlotPath ( char* outPath, pcstr inProductCode=NULL, int inSlotNo=-1, pcstr inFilename=NULL )
	{
		// CSIDL_APPDATA			The file system directory that serves as a common repository for application-specific data. A typical path is C:\Documents and Settings\username\Application Data. This CSIDL is supported by the redistributable Shfolder.dll for systems that do not have the Microsoft Internet Explorer 4.0 integrated Shell installed.
		// CSIDL_LOCAL_APPDATA		The file system directory that serves as a data repository for local (nonroaming) applications. A typical path is C:\Documents and Settings\username\Local Settings\Application Data.
		// CSIDL_COMMON_APPDATA		The file system directory that contains application data for all users. A typical path is C:\Documents and Settings\All Users\Application Data. This folder is used for application data that is not user specific. For example, an application can store a spell-check dictionary, a database of clip art, or a log file in the CSIDL_COMMON_APPDATA folder. This information will not roam and is available to anyone using the computer.
		// CSIDL_MYDOCUMENTS
		// CSIDL_PERSONAL

		char mydoc_path[MAX_PATH];
		LPITEMIDLIST pidl;
		SHGetSpecialFolderLocation( 0, CSIDL_LOCAL_APPDATA, &pidl );
		SHGetPathFromIDList( pidl, mydoc_path );

		if( outPath && inSlotNo<MAX_SLOT )
		{
			if( inProductCode )
			{
				if( inSlotNo>=0 )
				{
					if( inFilename )
					{
						Sprintf( outPath, "%s\\%s\\%d\\%s", mydoc_path, inProductCode, inSlotNo, inFilename );
						return TRUE;
					}
					else
					{
						Sprintf( outPath, "%s\\%s\\%d", mydoc_path, inProductCode, inSlotNo );
						return TRUE;
					}
				}
				else
				{
					Sprintf( outPath, "%s\\%s", mydoc_path, inProductCode );
					return TRUE;
				}
			}
			else
			{
				Sprintf( outPath, "%s", mydoc_path );
				return TRUE;
			}
		}

		return FALSE;
	}



	bool is_exists(	LPCSTR	inPathname	)
	{
		if( !inPathname || strlen(inPathname)==0 )
			return FALSE;

		DWORD res = GetFileAttributes( inPathname );
		return ( res != INVALID_FILE_ATTRIBUTES );
	}


	bool is_directory ( LPCSTR	inPathname	)
	{
		if( !inPathname || strlen(inPathname)==0 )
			return FALSE;

		DWORD res = GetFileAttributes( inPathname );
		return ( (res!=INVALID_FILE_ATTRIBUTES) && ((res&FILE_ATTRIBUTE_DIRECTORY)!=0) );
	}


	bool create_directory_rec ( LPCSTR inDirname, bool inRecCreate )
	{
		BOOL res = CreateDirectory(	inDirname, NULL );
		if( (res==0) && inRecCreate && !is_directory(inDirname) )
		{
			char tmp[ MAX_PATH ], tmp2[ MAX_PATH ];

			strcpy( tmp, inDirname );
			tmp2[0] = 0;
			char * p = tmp, * next = tmp;
			while( next[0] )
			{
				p = next;

				// extract dir
				{
					char * dir0 = strchr( p, '\\' );
					char * dir1 = strchr( p, '/' );
					if( dir0 && dir1 && dir1<dir0 )		dir0 = dir1;
					if( !dir0 )							dir0 = dir1;
					if( dir0 )
					{
						next = dir0 + 1;
						dir0[0] = 0;
					}
					else
					{
						next = p + strlen(p);
					}
				}
				if( strchr(p,':') )
				{
					strcat( tmp2, p );
				}
				else
				{
					strcat( tmp2, p );
					if( is_exists(tmp2) )
					{
						if( !is_directory(tmp2) )
						{
							return FALSE;
						}
					}
					else
					{
						if( CreateDirectory(tmp2,NULL) == 0 )
						{
							return FALSE;
						}
					}
				}
				strcat( tmp2, "/" );
			}
			return TRUE;
		}
		return ( res != 0 );
	}

}



bool
nvcore_NVR_Init()
{
	state = S_IDLE;
	state_res = nv::nvram::Result(0);
	productCode[0] = 0;
	firstnext_hd = NULL;
	return TRUE;
}


void
nvcore_NVR_Shut()
{
	//
}



void
nvcore_NVR_Update()
{
	switch( state )
	{
		//
		// Idle
		//

		case S_IDLE :
			return;


		//
		// Create Record
		//

		case SCR_OPENING :
			{
				// Fill new record with dummy data
				DWORD written;
				BOOL  res = WriteFile(	state_hd,
										(pvoid)state_params[0],
										state_params[1],
										&written,
										NULL	);
				NvFree( (pvoid)state_params[0] );
				if( res == 0 )
				{
					//DWORD lr = GetLastError();
					CloseHandle( state_hd );
					state_res = nvram::NVR_ISFULL;
					state     = S_IDLE;
				}
				else
				{
					NV_ASSERT( written == state_params[1] );
					state = SCR_WRITING;
				}
			}
			break;

		case SCR_WRITING :
			{
				CloseHandle( state_hd );
				state = SCR_CLOSING;
			}
			break;

		case SCR_CLOSING :
			{
				state_res = nvram::NVR_SUCCESS;
				state     = S_IDLE;
			}
			break;


		//
		// Write Record
		//

		case SWR_OPENING :
			{
				DWORD res = SetFilePointer( state_hd,
											state_params[2],
											NULL,
											FILE_BEGIN );
				if( res == -1 )
				{
					//DWORD lr = GetLastError();
					CloseHandle( state_hd );
					state_res = nvram::NVR_ERROR;
					state     = S_IDLE;
				}
				else
				{
					state = SWR_SEEKING;
				}
			}
			break;

		case SWR_SEEKING :
			{
				DWORD written;
				BOOL  res = WriteFile(	state_hd,
										(pvoid)state_params[0],
										state_params[1],
										&written,
										NULL	);
				if( res == 0 )
				{
					//DWORD lr = GetLastError();
					CloseHandle( state_hd );
					state_res = nvram::NVR_ISFULL;
					state     = S_IDLE;
				}
				else
				{
					NV_ASSERT( written == state_params[1] );
					state = SWR_WRITING;
				}
			}
			break;

		case SWR_WRITING :
			{
				CloseHandle( state_hd );
				state     = SWR_CLOSING;
			}
			break;

		case SWR_CLOSING :
			{
				state_res = nvram::NVR_SUCCESS;
				state     = S_IDLE;
			}
			break;


		//
		// Read Record
		//

		case SRD_OPENING :
			{
				DWORD res = SetFilePointer( state_hd,
											state_params[2],
											NULL,
											FILE_BEGIN );
				if( res == -1 )
				{
					//DWORD lr = GetLastError();
					CloseHandle( state_hd );
					state_res = nvram::NVR_ERROR;
					state     = S_IDLE;
				}
				else
				{
					state = SRD_SEEKING;
				}
			}
			break;

		case SRD_SEEKING :
			{
				DWORD readen = 0;
				BOOL  res = ReadFile(	state_hd,
										(pvoid)state_params[0],
										state_params[1],
										&readen,
										NULL	);
				if( res == 0 || readen < state_params[1] )
				{
					//DWORD lr = GetLastError();
					CloseHandle( state_hd );
					state_res = nvram::NVR_ERROR;
					state     = S_IDLE;
				}
				else
				{
					state = SRD_READING;
				}
			}
			break;

		case SRD_READING :
			{
				CloseHandle( state_hd );
				state     = SRD_CLOSING;
			}
			break;

		case SRD_CLOSING :
			{
				state_res = nvram::NVR_SUCCESS;
				state     = S_IDLE;
			}
			break;
	}
}




bool
nv::nvram::SetProductCode	(	pcstr		inProductCode		)
{
	// Checks if an op. is currently in progress ...
	if( state != S_IDLE ) {
		NV_WARNING( "nvram: An async operation is still in progress !" );
		return FALSE;
	}
	if( Strlen(inProductCode) >= 32 ) {
		NV_WARNING( "nvram: The specified product code is too long !" );
		return FALSE;
	}

	Strcpy( productCode, inProductCode );
	return TRUE;
}


pcstr
nv::nvram::GetProductCode	(									)
{
	return productCode;
}


uint
nv::nvram::GetNbMax			(									)
{
	return MAX_SLOT;
}


bool
nv::nvram::IsReady			(	Result		*		outLastResult	)
{
	if( outLastResult )
		*outLastResult = state_res;
	return ( state == S_IDLE );
}


bool
nv::nvram::GetStatus		(	uint			inNVRNo,
								uint32	*		outFullBSize,
								uint32	*		outFreeBSize,
								bool	*		outSwitched		)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;

	char path[MAX_PATH];
	GetSlotPath( path, productCode, inNVRNo );

	if( outSwitched )
		*outSwitched = FALSE;

	DWORD res = GetFileAttributes( path );
	if( (res!=-1) && ((res&FILE_ATTRIBUTE_DIRECTORY)!=0) )
	{
		if( outFullBSize )
			*outFullBSize = MAX_SLOT_BSIZE;
		if( outFreeBSize )
		{
			uint32 ubsize;
			if( GetRecUsedBSize(path,ubsize) )
				*outFreeBSize = MAX_SLOT_BSIZE - ubsize;
			else
				*outFreeBSize = 0;
		}
		state_res = NVR_STATUS_READY;
	}
	else
	{
		state_res = NVR_STATUS_UNFORMATTED;
	}

	return TRUE;
}


bool
nv::nvram::Format			(	uint		inNVRNo			)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;

	char app_path[MAX_PATH];
	GetSlotPath( app_path, productCode, inNVRNo );
	
	remove_directory_rec( app_path );

	if( create_directory_rec(app_path,TRUE) )
	{
		state_res = NVR_SUCCESS;
		return TRUE;
	}
	else
	{
		state_res = NVR_ERROR;
		return FALSE;
	}
}


bool
nv::nvram::GetFirstRecord	(	uint			inNVRNo,
								pstr			outName,
								uint32		*	outBSize	)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;

	if( firstnext_hd )
	{
		FindClose( firstnext_hd );
		firstnext_hd = NULL;
	}

	GetSlotPath( firstnext_path, productCode, inNVRNo );

	char tmp[MAX_PATH];
	Sprintf( tmp, "%s\\*", firstnext_path );

	firstnext_hd = FindFirstFile( tmp, &firstnext_finfo );
	if( firstnext_hd == INVALID_HANDLE_VALUE )
	{
		state_res = NVR_NOTFOUND;
		return TRUE;
	}

	// "."
	if( !FindNextFile(firstnext_hd,&firstnext_finfo) )
	{
		FindClose( firstnext_hd );
		firstnext_hd = NULL;
		state_res = NVR_NOTFOUND;
		return TRUE;
	}

	// ".."
	if( !FindNextFile(firstnext_hd,&firstnext_finfo) )
	{
		FindClose( firstnext_hd );
		firstnext_hd = NULL;
		state_res = NVR_NOTFOUND;
		return TRUE;
	}

	if( outName )
		Strcpy( outName, (pcstr)firstnext_finfo.cFileName );

	Sprintf( tmp, "%s\\%s", firstnext_path, firstnext_finfo.cFileName );
	uint32 bsize = GetFileInfo( tmp );

	if( outBSize )
		*outBSize = bsize;

	state_res = NVR_SUCCESS;
	return TRUE;
}


bool
nv::nvram::GetNextRecord	(	pstr			outName,
								uint32		*	outBSize	)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( !firstnext_hd )
		return FALSE;

	if( !FindNextFile(firstnext_hd,&firstnext_finfo) )
	{
		FindClose( firstnext_hd );
		firstnext_hd = NULL;
		state_res = NVR_NOTFOUND;
		return TRUE;
	}

	if( outName )
		Strcpy( outName, (pcstr)firstnext_finfo.cFileName );

	char tmp[128];
	Sprintf( tmp, "%s\\%s", firstnext_path, firstnext_finfo.cFileName );
	uint32 bsize = GetFileInfo( tmp );

	if( outBSize )
		*outBSize = bsize;

	state_res = NVR_SUCCESS;
	return TRUE;
}


bool
nv::nvram::CreateRecord		(	uint			inNVRNo,
								pcstr			inName,
								uint32			inBSize,
								pvoid			inBuffer		)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;
	if( !inName || Strlen(inName)==0 )
		return FALSE;
	if( inBSize == 0 )
		return FALSE;

	// Create slot directory

	char path[MAX_PATH];
	GetSlotPath( path, productCode, inNVRNo );

	// Check slot is full ?

	uint ubsize = 0;
	if( !GetRecUsedBSize(path,ubsize) )
	{
		state_res = NVR_ERROR;
		return TRUE;
	}
	if( (ubsize+inBSize) >= MAX_SLOT_BSIZE )
	{
		state_res = NVR_ISFULL;
		return TRUE;
	}

	// Alloc dummy data block

	pvoid buffer = NvMallocA( inBSize, 128 );
	if( !buffer )
	{
		state_res = NVR_ERROR;
		return TRUE;
	}
	if( inBuffer )	Memcpy( buffer, inBuffer, inBSize );
	else			Memset( buffer, 0xCD,     inBSize );

	// Open record

	GetSlotPath( path, productCode, inNVRNo, inName );

	HANDLE hd = CreateFile(	path,
							GENERIC_WRITE,
							FILE_SHARE_READ,
							NULL,
							CREATE_NEW,
							FILE_ATTRIBUTE_NORMAL,
							NULL	);
	if( hd == INVALID_HANDLE_VALUE )
	{
		NvFree( buffer );
		state_res = NVR_ALREADYEXIST;
	}
	else
	{
		state			= SCR_OPENING;
		state_hd        = hd;
		state_params[0]	= (uint32) buffer;
		state_params[1] = inBSize;
	}

	return TRUE;
}


bool
nv::nvram::WriteRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset	)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;
	if( !inName || Strlen(inName)==0 )
		return FALSE;
	if( !inBuffer )
		return FALSE;
	if( !inBSize )
		return FALSE;

	// Open record

	char path[MAX_PATH];
	GetSlotPath( path, productCode, inNVRNo, inName );

	HANDLE hd = CreateFile(	path,
							GENERIC_WRITE,
							FILE_SHARE_READ,
							NULL,
							OPEN_EXISTING,
							FILE_ATTRIBUTE_NORMAL,
							NULL	);
	if( hd == INVALID_HANDLE_VALUE )
	{
		state_res = NVR_NOTFOUND;
		return TRUE;
	}
	else
	{
		state			= SWR_OPENING;
		state_hd        = hd;
		state_params[0] = (uint32)inBuffer;
		state_params[1] = inBSize;
		state_params[2] = inBOffset;
	}

	return TRUE;
}


bool
nv::nvram::ReadRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset		)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;
	if( !inName || Strlen(inName)==0 )
		return FALSE;
	if( !inBuffer )
		return FALSE;
	if( !inBSize )
		return FALSE;

	// Open record

	char path[MAX_PATH];
	GetSlotPath( path, productCode, inNVRNo, inName );

	HANDLE hd = CreateFile(	path,
							GENERIC_READ,
							FILE_SHARE_READ,
							NULL,
							OPEN_EXISTING,
							FILE_ATTRIBUTE_NORMAL,
							NULL	);
	if( hd == INVALID_HANDLE_VALUE )
	{
		state_res = NVR_NOTFOUND;
		return TRUE;
	}
	else
	{
		state			= SRD_OPENING;
		state_hd        = hd;
		state_params[0] = (uint32)inBuffer;
		state_params[1] = inBSize;
		state_params[2] = inBOffset;
	}

	return TRUE;
}


bool
nv::nvram::DeleteRecord		(	uint			inNVRNo,
								pcstr			inName			)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;
	if( !inName || Strlen(inName)==0 )
		return FALSE;

	char path[MAX_PATH];
	GetSlotPath( path, productCode, inNVRNo, inName );

	BOOL res = DeleteFile( path );
	if( res != 0 )
	{
		state_res = NVR_SUCCESS;
	}
	else
	{
		DWORD lr = GetLastError();
		if( lr == ERROR_FILE_NOT_FOUND )
			state_res = NVR_NOTFOUND;
		else
			state_res = NVR_ERROR;
	}

	return TRUE;
}


