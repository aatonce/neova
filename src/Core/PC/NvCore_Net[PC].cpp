/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PC/NvWindows.h>
#include <Nova.h>
using namespace nv;


#define		DEF_WAIT_TIMEOUT		3000


namespace
{

	struct Sock
	{
		SOCKET			hdl;
		net::State		state;
		net::Type		type;
		int				next;
	};

	nv::sysvector<Sock>			sockA;
	int							sockFreeIdx;

	inline bool MakeASync(SOCKET	hdl)
	{
		unsigned long tmp = 1; //nonblocking mode should be enabled
		return ioctlsocket(hdl,FIONBIO,(unsigned long*)&tmp) == 0;
	}
}

bool
nvcore_Net_Init()
{
	sockA.Init();
	sockA.clear();
	sockFreeIdx = -1;

	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD(2, 2);
	int ret = WSAStartup( wVersionRequested, &wsaData );
	return ( ret == 0 );
}


void
nvcore_Net_Update()
{
	//
}


void
nvcore_Net_Shut()
{
	sockA.Shut();
	sockFreeIdx = -1;
	WSACleanup();
}




uint
nv::net::GetHostAddrCpt		(	pcstr		inName		)
{
	char hostName[128];
	if( !inName || Strcmp(inName,"127.0.0.1")==0 || Stricmp(inName,"localhost")==0 )
	{
		if (!gethostname(hostName, sizeof(hostName)))
		{
			hostName[sizeof(hostName)-1] = '\0';
			inName = hostName;
		}
		else
		{
			return 0;
		}
	}

	hostent* ht = NULL;
	if (inet_addr(inName) == INADDR_NONE)
		ht = gethostbyname(inName);
	else
	{
		uint16 a = 0;
		uint16 b = 0;
		uint16 c = 0;
		uint16 d = 0;
		if( sscanf(inName,"%hu.%hu.%hu.%hu",&a,&b,&c,&d) != 4 )
			return 0;

		char addr[4];
		addr[0] = (uint8)a;
		addr[1] = (uint8)b;
		addr[2] = (uint8)c;
		addr[3] = (uint8)d;
		ht = gethostbyaddr(addr, 4, AF_INET);
	}

	if (!ht || (ht->h_addrtype != AF_INET) || (ht->h_length != 4))
		return 0;

	inName = ht->h_name;

	uint32 nb = 0;
	while (ht->h_addr_list[nb])
		nb++;

	return nb;
}


bool
nv::net::GetHostAddr		(	pcstr				inName,
								uint				inAddrNo,
								pstr				outAddr			)
{
	char hostName[128];
	if( !inName || Strcmp(inName,"127.0.0.1")==0 || Stricmp(inName,"localhost")==0 )
	{
		if (!gethostname(hostName, sizeof(hostName)))
		{
			hostName[sizeof(hostName)-1] = '\0';
			inName = hostName;
		}
		else
		{
			return FALSE;
		}
	}

	hostent* ht = NULL;
	if (inet_addr(inName) == INADDR_NONE)
		ht = gethostbyname(inName);
	else
	{
		uint16 a = 0;
		uint16 b = 0;
		uint16 c = 0;
		uint16 d = 0;
		if( sscanf(inName,"%hu.%hu.%hu.%hu",&a,&b,&c,&d) != 4 )
			return FALSE;

		char addr[4];
		addr[0] = (uint8)a;
		addr[1] = (uint8)b;
		addr[2] = (uint8)c;
		addr[3] = (uint8)d;
		ht = gethostbyaddr(addr, 4, AF_INET);
	}

	if (!ht || (ht->h_addrtype != AF_INET) || (ht->h_length != 4))
		return FALSE;

	inName = ht->h_name;

	uint32 nb = 0;
	while (ht->h_addr_list[nb])
		nb++;

	if( inAddrNo >= nb )
		return FALSE;

	if( outAddr )
	{
		Sprintf( outAddr, "%hu.%hu.%hu.%hu",  (uint8)ht->h_addr_list[inAddrNo][0],
											  (uint8)ht->h_addr_list[inAddrNo][1],
											  (uint8)ht->h_addr_list[inAddrNo][2],
											  (uint8)ht->h_addr_list[inAddrNo][3]	);
	}

	return TRUE;
}






int
nv::net::Create	(	Type		inType		)
{
	if( inType != T_TCP )
		return -1;

	if( sockFreeIdx == -1 )
	{
		// Resize
		int N = sockA.size();
		int P = N + 4;
		sockA.resize( P );
		sockFreeIdx = N;
		for( int i = N ; i < P-1 ; i++ ) {
			sockA[i].type = T_UNDEF;
			sockA[i].next = i+1;
		}
		sockA[P-1].next = -1;
		sockA[P-1].type = T_UNDEF;
	}

	// Alloc sockId
	int sid				= sockFreeIdx;
	sockFreeIdx			= sockA[sid].next;

	// Init sock
	sockA[sid].hdl		= INVALID_SOCKET;
	sockA[sid].state	= S_CLOSED;
	sockA[sid].type		= inType;
	return sid;
}


void
nv::net::Release	(	int			inSockId	)
{
	if (inSockId < 0 || inSockId >= int(sockA.size())) return;
	if (sockA[inSockId].type != T_UNDEF) {
		Close( inSockId );

		// Free sockId
		sockA[inSockId].type	= T_UNDEF;
		sockA[inSockId].next	= sockFreeIdx;
		sockFreeIdx				= inSockId;
	}
}


bool
nv::net::Open		(	int			inSockId,
						uint16		inPort,
						pcstr		inAddr		)
{
	State state = GetState(inSockId);
	if (state != S_CLOSED ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	sockP->hdl = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockP->hdl == INVALID_SOCKET)
		return FALSE;

	//Set socket options
	const int32 optEnable = 1;
	const int32 optDisable = 0;
	const linger ling = {0};
	if (MakeASync(sockP->hdl) &&
		!setsockopt(sockP->hdl, SOL_SOCKET, SO_LINGER, (const char*)&ling, sizeof(ling)) &&
		!setsockopt(sockP->hdl, SOL_SOCKET, SO_DEBUG, (const char*)&optDisable, sizeof(optDisable)) &&
		!setsockopt(sockP->hdl, SOL_SOCKET, SO_REUSEADDR, (const char*)&optDisable, sizeof(optDisable)) &&
		!setsockopt(sockP->hdl, SOL_SOCKET, SO_KEEPALIVE, (const char*)&optEnable, sizeof(optEnable)) &&
		!setsockopt(sockP->hdl, SOL_SOCKET, SO_DONTROUTE, (const char*)&optDisable, sizeof(optDisable)) &&
		!setsockopt(sockP->hdl, SOL_SOCKET, SO_OOBINLINE, (const char*)&optDisable, sizeof(optDisable)) 	)
	{
		//Bind socket
		sockaddr_in addr = {0};
		addr.sin_family = AF_INET;
		addr.sin_port = htons( inPort );
		addr.sin_addr.S_un.S_addr = inAddr ? inet_addr(inAddr) : INADDR_ANY;
		if(! bind(sockP->hdl, (const sockaddr*)&addr, sizeof(addr)) ){
			sockP->state = S_OPENED;
			return TRUE;
		}
	}
	Close(inSockId);
	return FALSE;
}


void
nv::net::Close		(	int			inSockId	)
{
	if (inSockId < 0 )				return ;
	if (inSockId >= int(sockA.size()))	return ;
	Sock * sockP = & sockA[inSockId];
	if (sockP->type != net::T_TCP )	return ;
	if (sockP->state <= S_CLOSED ) 	return ;

	if( sockP->hdl != INVALID_SOCKET )
	{
		int32 errCode = WSAGetLastError();
		closesocket(sockP->hdl);
		WSASetLastError(errCode);
		sockP->hdl = INVALID_SOCKET;
	}
	sockP->state = S_CLOSED;
}


bool
nv::net::Listen	(	int			inSockId,
					uint32		inPendingClients	)
{
	State state = GetState(inSockId);
	if (state != S_OPENED ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	if (!listen(sockP->hdl, inPendingClients)){
		sockP->state = S_LISTENING;
		return TRUE;
	}
	return false;
}


bool
nv::net::Accept	(	int			inSockId,
					int	&		outSockId	)
{
	State state = GetState(inSockId);
	if (state != S_LISTENING ) return FALSE;

	Sock * sockP = & sockA[inSockId];


	outSockId = -1;
	SOCKET peerSock = accept(sockP->hdl, NULL, NULL);	
	if (peerSock != INVALID_SOCKET)
	{
		int asockId = Create( sockP->type );
		if( asockId < 0 )
			return FALSE;
		Sock * asockP	= & sockA[asockId];
		asockP->hdl		= peerSock;
		asockP->state	= S_CONNECTED;
		outSockId		= asockId;
		if (!MakeASync(asockP->hdl)) {
			Close(asockP->hdl);
			return FALSE;	
		}
		return TRUE;
	}
	//WSAGetLastError() == WSAEWOULDBLOCK => R_EMPTY_QUEUE	
	return FALSE;
}


bool
nv::net::Connect	(	int			inSockId,
						pcstr		inAddr,
						uint16		inPort		)
{
	State state = GetState(inSockId);
	if (state != S_CLOSED ) return FALSE;

	Sock * sockP = & sockA[inSockId];
	
	sockP->hdl = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockP->hdl == INVALID_SOCKET)
		return FALSE;
	if (!MakeASync(sockP->hdl)){
		Close( inSockId );
		return FALSE;
	}

	sockaddr_in addr = {0};
	addr.sin_family = AF_INET;
	addr.sin_port = htons( inPort );

	if (!inAddr || (inAddr[0] == '\0') || Stricmp(inAddr,"localhost")==0 )
		inAddr = "127.0.0.1";

	uint32 ipAddr = inet_addr(inAddr);
	if (ipAddr == INADDR_NONE)
	{
		hostent* ht = gethostbyname(inAddr);
		if (!ht ||
			(ht->h_addrtype != AF_INET) ||
			(ht->h_length != 4) ||
			!ht->h_addr_list[0])
			return FALSE;
		addr.sin_addr.S_un.S_un_b.s_b1 = (uint8)ht->h_addr_list[0][0];
		addr.sin_addr.S_un.S_un_b.s_b2 = (uint8)ht->h_addr_list[0][1];
		addr.sin_addr.S_un.S_un_b.s_b3 = (uint8)ht->h_addr_list[0][2];
		addr.sin_addr.S_un.S_un_b.s_b4 = (uint8)ht->h_addr_list[0][3];
	}
	else
	{
		addr.sin_addr.S_un.S_addr = ipAddr;
	}

	if (!connect(sockP->hdl, (const sockaddr*)&addr, sizeof(addr)) ||
		(WSAGetLastError() == WSAEWOULDBLOCK))
	{	
		sockP->state = S_CONNECTING;
		return TRUE;
	}	
	return FALSE;	
}

bool
nv::net::ConnectWait	(	int					inSockId,
							uint				inMsTimeout,
							pcstr				inAddr,
							uint16				inPort		)
{
	if( !Connect(inSockId,inAddr,inPort) )
		return FALSE;
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	for( ;; ) {
		if( GetState(inSockId) != S_CONNECTING )
			break;
		clock::GetTime( &t1 );
		if( (t1-t0)*1000.f > msTimeout ) {
			Close( inSockId );
			break;
		}
		Sleep( 1 );
	}
	return ( GetState(inSockId) == S_CONNECTED );
}

bool
nv::net::Receive	(	int					inSockId,
						uint32		&		outRcvSize,
						pvoid				inBuff,
						uint32				inBuffSize,
						bool				peek			)
{
	outRcvSize = 0;

	State state = GetState(inSockId);
	if (state != S_CONNECTED ) return FALSE;

	Sock * sockP = & sockA[inSockId];
	
	if (!inBuffSize)	return TRUE;
	if ( !inBuff )		return FALSE;

	int32 ret = 0;
	ret = recv(sockP->hdl, (char*)inBuff, inBuffSize, peek ? MSG_PEEK : 0);

	if( ret > 0 )
	{
		outRcvSize = ret;
		return TRUE;
	}
	else if( ret == 0 )
	{
		//Connection has been gracefully closed by the peer socket
		Close(inSockId);
		WSASetLastError(WSAECONNRESET);
	}
	else // ( ret < 0 )
	{
		if( WSAGetLastError() == WSAEWOULDBLOCK )
			return TRUE;
	}
	return FALSE;
}


bool
nv::net::Send		(	int					inSockId,
						uint32		&		outSndSize,
						pvoid				inBuff,
						uint32				inSize			)
{
	outSndSize = 0;

	State state = GetState(inSockId);
	if (state != S_CONNECTED )	return FALSE;
	if ( !inSize )				return TRUE;
	if ( !inBuff )				return FALSE;

	Sock * sockP = & sockA[inSockId];


	int32 MTUBSize = GetMTUBSize(inSockId);
	if( MTUBSize>0 && int(inSize)>MTUBSize )
		inSize = MTUBSize;

	int ret = send( sockP->hdl, (pcstr)inBuff, inSize, 0 );
	if( ret >= 0 ){
		outSndSize = ret;
		return TRUE;
	}
	else
	{
		if( WSAGetLastError() == WSAEWOULDBLOCK ){
			outSndSize = 0;
			return TRUE;
		}
	}
	return FALSE;
}

bool
nv::net::ReceiveWait	(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize		)
{
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	uint8* buffer   = (uint8*) inBuffer;
	while( inBSize ) {
		uint32 rbs = 0;
		if( !Receive(inSockId,rbs,buffer,inBSize) )
			return FALSE;
		if( rbs==0 ) {
			clock::GetTime( &t1 );
			if( (t1-t0)*1000.f > msTimeout )
				return FALSE;
			Sleep( 1 );
		} else {
			buffer   += rbs;
			inBSize  -= rbs;
			clock::GetTime( &t0 );
		}
	}
	return ( GetState(inSockId) == S_CONNECTED );
}

bool
nv::net::SendWait		(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize			)
{
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	uint8* buffer   = (uint8*) inBuffer;
	while( inBSize ) {
		uint32 sbs = 0;
		if( !Send(inSockId,sbs,buffer,inBSize) )
			return FALSE;
		if( sbs==0 ) {
			clock::GetTime( &t1 );
			if( (t1-t0)*1000.f > msTimeout )
				return FALSE;
			Sleep( 1 );
		} else {
			buffer   += sbs;
			inBSize  -= sbs;
			clock::GetTime( &t0 );
		}
	}
	return ( GetState(inSockId) == S_CONNECTED );
}

bool
nv::net::GetPeerAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort		)
{
	State state = GetState(inSockId);
	if (state != S_CONNECTED ) return FALSE;

	Sock * sockP = & sockA[inSockId];


	sockaddr_in addr = {0};
	int size = sizeof(addr);
	if (!getpeername(sockP->hdl, (sockaddr*)&addr, &size))
	{
		NV_ASSERT(addr.sin_family == AF_INET);
		if( outAddr )	*outAddr = inet_ntoa(addr.sin_addr);
		if( outPort )	*outPort = ntohs( addr.sin_port );
		return TRUE;
	}
	return FALSE;
}


bool
nv::net::GetSockAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort			)
{
	State state = GetState(inSockId);
	if (state < S_CLOSED ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	sockaddr_in addr = {0};
	int size = sizeof(addr);
	if (!getsockname(sockP->hdl,(sockaddr*)&addr, &size))
	{
		NV_ASSERT(addr.sin_family == AF_INET);
		if( outAddr )	*outAddr = inet_ntoa(addr.sin_addr);
		if( outPort )	*outPort = ntohs( addr.sin_port );
		return TRUE;
	}
	return FALSE;
}


bool
nv::net::IsRcvPending	(	int			inSockId,
							uint		inBSize		)
{
	if (GetState(inSockId) != S_CONNECTED )
		return FALSE;

	Sock * sockP = & sockA[inSockId];
	if( inBSize > 1024 )
		return FALSE;
	char tmp[1024];
	int ret = recv( sockP->hdl, tmp, inBSize, MSG_PEEK );

	return ( ret>0 && ret>=(int)inBSize );
}


bool
nv::net::IsClientPending		(	int			inSockId		)
{
	State state = GetState(inSockId);
	if (state != S_LISTENING ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	fd_set readSet;
	FD_ZERO(&readSet);
	FD_SET(sockP->hdl, &readSet);
	timeval timeout = {0};
	if( select(sockP->hdl+1, &readSet, NULL, NULL, &timeout) <= 0 )
		return FALSE;
	else
		return TRUE;
}


int32
nv::net::GetMTUBSize	(	int			inSockId		)
{
	State state = GetState(inSockId);
	if (state != S_LISTENING ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	if( sockP->hdl == INVALID_SOCKET )
		return -1;

	int rcvMTU = 0;
	int sndMTU = 0;
	int rcvLength = sizeof(rcvMTU);
	int sndLength = sizeof(sndMTU);
	if( getsockopt(sockP->hdl, SOL_SOCKET, SO_RCVBUF, (char*)&rcvMTU, &rcvLength) )
		rcvMTU = 0;
	if( getsockopt(sockP->hdl, SOL_SOCKET, SO_SNDBUF, (char*)&sndMTU, &sndLength) )
		sndMTU = 0;

	if( rcvMTU && sndMTU )
		return Min( rcvMTU, sndMTU );
	else if( rcvMTU )
		return rcvMTU;
	else if( sndMTU )
		return sndMTU;
	else
		return -1;
}


nv::net::State
nv::net::GetState		(	int			inSockId		)
{
	if (inSockId < 0 )				return S_INVALID;
	if (inSockId >= int(sockA.size()))	return S_INVALID;

	Sock * sockP = & sockA[inSockId];

	if( sockP->state == S_CONNECTING) {
		fd_set successSet;
		FD_ZERO(&successSet);
		FD_SET(sockP->hdl, &successSet);

		fd_set failureSet;
		FD_ZERO(&failureSet);
		FD_SET(sockP->hdl, &failureSet);

		timeval timeout = {0};

		if (select(sockP->hdl+1, NULL, &successSet, &failureSet, &timeout) < 0)
		{
			net::Close( inSockId );
			return sockP->state;
		}

		if (FD_ISSET(sockP->hdl, &successSet))
		{
			sockP->state = S_CONNECTED;
			NV_ASSERT(!FD_ISSET(sockP->hdl, &failureSet));
		}
		else if (FD_ISSET(sockP->hdl, &failureSet))
		{
			net::Close( inSockId );
		}
	}
	else if (sockP->state == S_CONNECTED)
	{
		// Check the connection validity
		char tmp [16];
		int ret = recv( sockP->hdl, tmp, 1, MSG_PEEK );
		if( ret == 0 ) {
			net::Close( inSockId );
		} else if( ret < 0 ) {
			int err = WSAGetLastError();
			if( err!=WSAEINPROGRESS && err!=WSAEWOULDBLOCK )
				Close( inSockId );
		}
	}
	return sockP->state;
}


nv::net::Type
nv::net::GetType		(	int			inSockId		)
{
	State state = GetState(inSockId);
	if (state < S_CLOSED ) return T_UNDEF;

	Sock * sockP = & sockA[inSockId];
	return sockP->type;
}



