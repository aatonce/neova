/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Kernel/PC/NvWindows.h>
#include <Nova.h>


//
// EBP -> parent stack frame EBP --------o
//		  return address for this call   |
//		  data					         |
//		  parent stack frame EBP <-------o
//		  ...



#if defined( _NVCOMP_ENABLE_DBG )


#pragma inline_depth( 0 )


bool
nv::stack::GetFrame	(	Frame	&	outF		)
{
	DWORD * _EBP;
	__asm mov _EBP, EBP

	Frame fs;		
	fs.ptr     = _EBP;
	fs.bsize   = _EBP[0] - DWORD(_EBP);
	fs.retAddr = (pvoid) _EBP[1];
	fs.locAddr = GetLocalAddr();

	// Returns the caller frame !
	return GetBackFrame( outF, fs );
}


bool
nv::stack::GetBackFrame	(	Frame	&	outF,
							Frame	&	inF			)
{
	if(		!inF.retAddr
	 	||	!inF.locAddr
	 	||	!inF.ptr
	 	||	!inF.bsize	)
		return FALSE;

	// this is top frame ?
	uint32 sp_top;
	core::GetParameter( core::PR_MEM_APP_STACK_ADDR, &sp_top );
	if(	uint(inF.ptr) >= sp_top )
		return FALSE;

	DWORD *       _EBP = (DWORD*) inF.ptr;
	DWORD * caller_EBP = (DWORD*) _EBP[0];

	outF.ptr     = caller_EBP;
	outF.bsize   = caller_EBP[0] - DWORD(caller_EBP);
	outF.retAddr = (pvoid) caller_EBP[1];
	outF.locAddr = (pvoid) _EBP[1];

	return TRUE;
}


pvoid
nv::stack::GetReturnAddr	(		)
{
	Frame fs, caller_fs;

	if( !GetFrame(fs) )
		return NULL;

	if( !GetBackFrame(caller_fs,fs) )
		return NULL;

	return caller_fs.retAddr;
}


pvoid
nv::stack::GetLocalAddr		(		)
{
	DWORD * _EBP;
	__asm mov _EBP, EBP
	return (pvoid) _EBP[1];
}


uint
nv::stack::OutputCallstack	(	Frame&		inFromF,
								pcstr		inLabel		)
{
	// Output call-stack
	uint32 appStartAddr;
	core::GetParameter( core::PR_MEM_APP_ADDR, &appStartAddr );

	Printf( "<Neova> call-stack :\n" );
	Frame sf    = inFromF;
	uint  depth = 0;
	for( ;; ) {
		Printf( "%s[%d] %08X .t=%08X sp=%08X\n",
			inLabel ? inLabel : "",
			depth++,
			uint32(sf.locAddr),
			uint32(sf.locAddr)-appStartAddr,
			uint32(sf.ptr)	);
		if( !GetBackFrame(sf,sf) )
			break;
	}

	return depth;
}





#else	// _NVCOMP_ENABLE_DBG






bool
nv::stack::GetFrame		(	Frame	&	outF		)
{
	return FALSE;
}

bool
nv::stack::GetBackFrame	(	Frame	&	outBackF,
							Frame	&	inF			)
{
	return FALSE;
}

pvoid
nv::stack::GetReturnAddr	(		)
{
	return NULL;
}

pvoid
nv::stack::GetLocalAddr		(		)
{
	return NULL;
}

uint
nv::stack::OutputCallstack	(	Frame&		inFromF,
								pcstr		inLabel		)
{
	return 0;
}





#endif // _NVCOMP_ENABLE_DBG





