/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvWindows.h>
#include <Nova.h>


namespace
{
/*
	// http://hazelware.luggle.com/tutorials/cpuinfo.html

	#define MAX_ULONG			4294967295
	#define TIME_DELAY			500		// intervale de calibration du timer en ms

	float		time_coef1;
	float		time_coef2;
	float		base_time; 
	int			base_time_init;
	float		frame_speed;
	int			CPU_Frequency;  // en MHz
	bool		fixCpuFreq;		// freq fixed ?

	void CalibrateTimer(void)
	{
		unsigned long time_ref,time;
		unsigned long value0,value1;
		unsigned long value2,value3;
		
		time_coef1		= 65536.0f / (133.0f * 1000.0f * 1000.0f);
		time_coef2		= 1.0f / (133.0f * 1000.0f * 1000.0f);
		base_time		= 0.0f; 
		base_time_init	= 0;
		frame_speed		= 30.0f;
		CPU_Frequency	= 0;

		__asm 
		{
			_emit 0x0f
			_emit 0x31
			mov	value0,eax
			mov	value1,edx
		}
		
		time_ref=GetTickCount() + TIME_DELAY;
		do
		{
			time=GetTickCount();

		} while (time < time_ref); 

		__asm
		{
			_emit 0x0f
			_emit 0x31
			mov	value2,eax
			mov	value3,edx
		}

		value0 >>=16 ;
		value1 =  (value1 & 0x0000FFFF) << 16;
		value1 |= value0;

		value2 >>=16 ;
		value3 =  (value3 & 0x0000FFFF) << 16;
		value3 |= value2;

		unsigned long diff = value3 - value1;
		float f = ((float)diff) * 65536.f;

		f /= (float)(TIME_DELAY) / 1000.0f; 

		CPU_Frequency = (int)(f / 1000000.0f);

		time_coef1 =  65536.0f / f;
		time_coef2 =  1.0f / f;
	}


	float GetTimeCPU()
	{
		unsigned long value0,value1;
		float f;

		__asm 
		{
			_emit 0x0f
			_emit 0x31
			mov	value0,eax
			mov	value1,edx
		}

		value0 >>=16 ;
		value1 =  (value1 & 0x0000FFFF) << 16;
		value1 |= value0;

		f=(float)(value1) * time_coef1;

		if (base_time_init)
			return f - base_time;

		base_time_init=1;
		base_time=f;

		return 0.0f;
	}

	uint StartBench(void)
	{
		uint value0,value1;
		__asm 
		{
			_emit 0x0f
			_emit 0x31
			mov	value0,eax
			mov	value1,edx
		}
		return value0;
	}

	float EndBench(uint _BaseTime)
	{
		uint value0,value1;
		float f;

		__asm 
		{
			_emit 0x0f
			_emit 0x31
			mov	value0,eax
			mov	value1,edx
		}
		
		if (value1 < _BaseTime)
			value1 = value0 + (MAX_ULONG - _BaseTime);
		else
			value1 = value0 - _BaseTime;
		
		f=(float)(value1) * time_coef2;

		return f;
	}
*/
	float GetTimePerformanceCounter()
	{
		static bool lidone = FALSE;
		static double based;
		static double freqd;
		if( !lidone ) {
			lidone = TRUE;
			__int64 baseli;
			__int64 freqli;
			QueryPerformanceCounter( (LARGE_INTEGER*)&baseli );
			QueryPerformanceFrequency( (LARGE_INTEGER*)&freqli );
			if( !freqli )	freqli = 1;
			based = double( baseli );
			freqd = double( freqli );
		}
		__int64 li;
		QueryPerformanceCounter( (LARGE_INTEGER*)&li );
		return (double(li)-based)/freqd;
	}

}




bool
nvcore_Clock_Init()
{
   // be sure we only use 1 processor!
	DWORD_PTR pam;
	DWORD_PTR sam;
	GetProcessAffinityMask( GetCurrentProcess(), &pam, &sam );
    SetProcessAffinityMask( GetCurrentProcess(), 1 );

	GetTimePerformanceCounter();
	return TRUE;
}


void
nvcore_Clock_Update()
{
	//
}


void
nvcore_Clock_Shut()
{
	//
}


void
nv::clock::GetTime(	Time *		outTime		)
{
	if( !outTime )		return;
	float t = GetTimePerformanceCounter();
	int	  ti = (int)t;
	outTime->ipart = (float)(ti);
	outTime->fpart = t - (outTime->ipart);
}



