/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <stdio.h>
#include <stdlib.h>
#include <Kernel/NGC/GEK/GEK.h>
#include <Kernel/Common/NvkCore_Mem.h>
#include <NvSndManager.h>
#ifdef RVL
#include <revolution/sc.h>
#endif 
using namespace nv;



#define	NVCORE_DECL( ID )					\
	bool	nvcore_##ID##_Init		();		\
	void	nvcore_##ID##_Update	();		\
	void	nvcore_##ID##_Shut		();

NVCORE_DECL( Math	)
NVCORE_DECL( Libc	)
NVCORE_DECL( Report )
NVCORE_DECL( Mem	)
NVCORE_DECL( Clock	)
NVCORE_DECL( File	)
NVCORE_DECL( NVR	)
NVCORE_DECL( Net	)
NVCORE_DECL( Ctrl	)
NVCORE_DECL( Param	)
NVCORE_DECL( Ext	)


// linker symbols
//#include <__ppc_eabi_linker.h>
__declspec(section ".init") extern char	__start[];
__declspec(section ".init") extern char	_stack_addr[];
__declspec(section ".init") extern char	_stack_end[];


namespace
{

	void InitCoreParam( int argc, char ** argv )
	{
		// Cmd line
		static char cmdLine[ 128 ];
		int    cmdLineRemain = sizeof(cmdLine) - 1;
		char * cmdLineP      = cmdLine;
		for( int i = 0 ; i < argc ; i++ ) {
			if( !argv[i] )
				break;
			int argvLen = Strlen(argv[i]);
			if( argvLen > cmdLineRemain )
				break;
			Strcpy( cmdLineP, argv[i] );
			cmdLineRemain -= argvLen;
			cmdLineP      += argvLen;
			if( cmdLineRemain == 0 )
				break;
			*cmdLineP++ = ' ';
			cmdLineRemain--;
		}
		*cmdLineP = 0;
		core::SetParameter( core::PR_SYS_CMDLINE, (pcstr)cmdLine );
	}


	volatile bool	resetPressed = FALSE;
	volatile bool	powerPressed = FALSE;

	bool			coreBooted		= FALSE;
	bool			coreInitialized = FALSE;
	bool			startupCalled	= FALSE;


	void ResetCallback()
	{
	#if defined(_RVL)

		//OSReport( "reset pressed !!\n" );
		resetPressed = TRUE;
		VISetBlack(true);
		VIFlush();

	#else

		if( OSGetResetButtonState() )
			resetPressed = TRUE;

	#endif
	}


	void PowerCallback()
	{
	#if defined(_RVL)

		//OSReport( "power pressed !!\n" );
		powerPressed = TRUE;
		VISetBlack(true);
		VIFlush();

	#else

		if( OSGetResetButtonState() )
			powerPressed = TRUE;

	#endif
	}

}





bool
nv::core::Startup__	(	int			argc,
						char**		argv	)
{
	NV_COMPILE_TIME_CHECK_TYPES();

	nvcore_Param_Init();
	InitCoreParam( argc, argv );

	startupCalled = TRUE;
	return TRUE;
}



bool
nv::core::Init		()
{
	if( coreInitialized )
		return TRUE;

	// Link areas
	uint stack_lo	 = uint(_stack_end);
	uint stack_hi	 = uint(_stack_addr);
	uint elf_lo		 = uint(__start);
	uint elf_hi		 = stack_lo;
	uint heap_lo	 = (uint32) OSGetArenaLo();
	uint heap_hi	 = (uint32) OSGetArenaHi();

	mem::NativeInit();
	gx::Init();

	#ifdef _NVCOMP_ENABLE_CONSOLE	
	devcons::Init();
	#endif

	Printf( ">> Neova CoreEngine is running\n" );
	Printf( ">> (c) 2002-2008 AtOnce Technologies\n" );
	Printf( "\n" );

	if( !startupCalled ) {
		NV_ERROR( "\n\n\n** Overload the main() function is not allowed !\n** Use the function [void GameMain()] as your application entrypoint." );
		return FALSE;
	}

	// CoreEngine revision
	Printf( "<Neova> CoreEngine %d.%d-%s\n", GetReleaseNumber(), GetReleaseRevision(), GetDebugLevel() );
	#ifdef TIME_LIMITED_EVALUATION
	Printf( "<Neova> Time limited version\n" );
	#endif

	Printf( "<Neova> CPU freq: %d Mhz\n", OS_CORE_CLOCK/(1000*1000) );
	Printf( "<Neova> Bus freq: %d Mhz\n", OS_BUS_CLOCK/(1000*1000) );	
	Printf( "<Neova> SDK Ver.: %s-%s\n", __SDKVER__ , __SDKPATCH__ );
	Printf( "<Neova> Elf   @%x, %dKo\n",			elf_lo, (elf_hi-elf_lo)>>10 );
	Printf( "<Neova> Stack [@%x->@%x], %dKo\n",		stack_hi, stack_lo, (stack_hi-stack_lo)>>10 );
	Printf( "<Neova> Heap  @%x, %dMo\n",			heap_lo, (heap_hi-heap_lo)>>20 );
	SetParameter( PR_MEM_APP_ADDR,			elf_lo				);
	SetParameter( PR_MEM_APP_BSIZE,			(elf_hi-elf_lo)		);
	SetParameter( PR_MEM_APP_HEAP_ADDR,		heap_lo				);
	SetParameter( PR_MEM_APP_HEAP_BSIZE,	(heap_hi-heap_lo)	);
	SetParameter( PR_MEM_APP_STACK_ADDR,	stack_hi			);
	SetParameter( PR_MEM_APP_STACK_BSIZE,	(stack_hi-stack_lo)	);

	// Check thread stack
	OSThread* curThread  = OSGetCurrentThread();
	NV_ASSERT( uint(curThread->stackBase) == stack_hi );
	NV_ASSERT( uint(curThread->stackEnd)  == stack_lo );

	Printf( "<Neova> Components initialization ...\n" );
	nvcore_Report_Init();
	nvcore_Mem_Init();
	nvcore_Libc_Init();
	nvcore_Clock_Init();
	nvcore_Math_Init();
	nvcore_Net_Init();
	nvcore_File_Init();
	nvcore_Ctrl_Init();	
	nvcore_NVR_Init();
	nvcore_Ext_Init();
	dmac::Init();
	vtxfmt::Init();
	Printf( "<Neova> Components initialized.\n" );

	#ifdef _NVCOMP_ENABLE_CONSOLE	
	devcons::Shut();
	#endif

	coreBooted	    = TRUE;
	coreInitialized = TRUE;
	resetPressed  = FALSE;
	powerPressed 	= FALSE;

	OSSetResetCallback( &ResetCallback );

	#ifdef _RVL
	OSSetPowerCallback( &PowerCallback );
	#endif 

	mem::LogOpen();
	return TRUE;
}



void
nv::core::Shut		(			)
{
	if( !coreInitialized )
		return;

	while ( !SndManager::Shut() )
	{
		core::Update();
		SndManager::Update();
	}


	Printf( "\n\n\n<Neova> ** EXITING **\n" );
	libc::FlushPrintf();

	vtxfmt::Shut();
	dmac::Shut();
	nvcore_Ext_Shut();
	nvcore_NVR_Shut();
	nvcore_Ctrl_Shut();
	nvcore_File_Shut();
	nvcore_Net_Shut();
	nvcore_Math_Shut();
	nvcore_Libc_Shut();
	nvcore_Clock_Shut();
	nvcore_Mem_Shut();
	nvcore_Report_Shut();
	nvcore_Param_Shut();

	mem::NativeShut();

	Printf( "\n\n\n<Neova> ** EXITED **\n" );
	libc::FlushPrintf();

	coreInitialized = FALSE;
}


#ifdef _RVL
void 
nv_core_rvl_ReturnToMenu()
{
	nv::core::Shut();
	VISetBlack( TRUE );
	VIFlush();
	OSReturnToMenu();
}

void 
nv_core_rvl_Restart()
{
	nv::core::Shut();
	VISetBlack( TRUE );
	VIFlush();
	
	OSRestart(0);
}


void 
nv_core_rvl_Shutdown()
{
	nv::core::Shut();
	VISetBlack( TRUE );
	VIFlush();
	OSShutdownSystem();
}
#endif


void
nv::core::Exit()
{
	bool reset, power;
	GetParameter( PR_NGC_POWER_PRESSED,	&power	);
	GetParameter( PR_NGC_RESET_PRESSED,	&reset	);

	OSReport( "exiting ...\n" );

#ifdef _RVL

	if( power )
	{
		nv_core_rvl_Shutdown();
	}
	else if( reset )
	{
		nv_core_rvl_Restart();
	}
	else
	{
		nv_core_rvl_ReturnToMenu();
	}

#else

	exit(0);

#endif
}


bool
nv::core::ExitAsked	 ( )
{
	if( resetPressed )
	{
		OSSetResetCallback( &ResetCallback );
		SetParameter( PR_NGC_RESET_PRESSED, TRUE );
	}

#if defined(_RVL)
	if( powerPressed )
	{
		OSSetPowerCallback( &PowerCallback );
		SetParameter( PR_NGC_POWER_PRESSED, TRUE );
	}
#endif

	bool reset, power;
	GetParameter( PR_NGC_POWER_PRESSED,	&reset	);
	GetParameter( PR_NGC_RESET_PRESSED,	&power	);

	return reset || power;
}


uint
nv::core::Update	(	)
{
	nvcore_Param_Update();
	nvcore_Report_Update();
	nvcore_Mem_Update();
	nvcore_Libc_Update();
	nvcore_Clock_Update();
	nvcore_Math_Update();
	nvcore_Net_Update();
	nvcore_File_Update();
	nvcore_Ctrl_Update();
	nvcore_NVR_Update();
	nvcore_Ext_Update();

	uint returnFlags = 0;

	if( ExitAsked() )
		returnFlags |= nv::core::UPD_EXIT_ASKED;

	return returnFlags;
}



nv::core::SystemID
nv::core::GetSystemID		(		)
{
#ifdef _RVL
	return NintendoWii;
#else
	return NintendoGC;
#endif
}


nv::core::Aspect
nv::core::GetAspect		(			)
{
#if defined(_RVL)
	uint8 asp = SCGetAspectRatio();
	if (asp == SC_ASPECT_RATIO_4x3)
		return ASPECT_43;
	if (asp == SC_ASPECT_RATIO_16x9)
		return ASPECT_169;
#endif
	
	return ASPECT_FULL;
}

nv::core::VideoMode		
nv::core::GetVideoMode		(	)
{
#if defined(_RVL)
	uint8 eurgb  = SCGetEuRgb60Mode();
	uint8 prog	 = SCGetProgressiveMode();
	if (prog == SC_PROGRESSIVE_MODE_ON)
		return VM_Progressive;
	if (eurgb == SC_PROGRESSIVE_MODE_ON)
		return VM_EURGB60;
#endif

	return VM_Other;
}

nv::core::SoundMode		
nv::core::GetSoundMode		(	)
{
#if defined(_RVL)
	NV_COMPILE_TIME_ASSERT(nv::core::SM_Mono 		== SC_SOUND_MODE_MONO);
	NV_COMPILE_TIME_ASSERT(nv::core::SM_Stereo 	== SC_SOUND_MODE_STEREO);
	NV_COMPILE_TIME_ASSERT(nv::core::SM_Surround 	== SC_SOUND_MODE_SURROUND);
	return nv::core::SoundMode(SCGetSoundMode());	
#else
	return SM_Stereo;
#endif
}

nv::core::DateNotation
nv::core::GetDateNotation		(			)
{				
	return DATE_DDMMYYYY;
}


nv::core::Language
nv::core::GetLanguage	(				)
{
#ifndef _RVL
	int v;
	u8 l = OSGetLanguage( );
	
	if		( l == OS_LANG_ENGLISH )	return LANG_ENGLISH;
	else if	( l == OS_LANG_GERMAN )		return LANG_GERMAN;
	else if	( l == OS_LANG_FRENCH )		return LANG_FRENCH;
	else if	( l == OS_LANG_SPANISH)		return LANG_SPANISH;
	else if	( l == OS_LANG_ITALIAN )	return LANG_ITALIAN;
	else if	( l == OS_LANG_DUTCH )		return LANG_DUTCH;
	else return LANG_ENGLISH;
#else // _RVL 
	u8 l = SCGetLanguage();
	
	if 		(l == SC_LANG_JAPANESE) 	return LANG_JAPANESE;
	else if (l == SC_LANG_ENGLISH) 		return LANG_ENGLISH;
	else if (l == SC_LANG_GERMAN) 		return LANG_GERMAN;
	else if (l == SC_LANG_FRENCH) 		return LANG_FRENCH;
	else if (l == SC_LANG_SPANISH) 		return LANG_SPANISH;
	else if (l == SC_LANG_ITALIAN)		return LANG_ITALIAN;
	else if (l == SC_LANG_DUTCH) 		return LANG_DUTCH;
	else if (l == SC_LANG_SIMP_CHINESE) return LANG_CHINESE_S;
	else if (l == SC_LANG_TRAD_CHINESE) return LANG_CHINESE_T;
	
	else return LANG_ENGLISH;	
#endif 
}

	
nv::core::SummerTime
nv::core::GetSummerTime	(				)
{
	return SUMT_OFF;
}


nv::core::TimeNotation
nv::core::GetTimeNotation	(					)
{
	return TIME_12H;
}



