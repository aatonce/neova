/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
using namespace nv;


#ifdef _RVL 
#include <Kernel/Common/NvkCore_Mem.h>
#include <kernel/NGC/NvkBitmap[NGC].h>
#include <kernel/NGC/NvkCore_NVR[NGC].h>
#include <revolution/nand.h>
#include <NvBitmap.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// RVL Version //////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Limatations :
#define MAX_HOME_SIZE 				(16 * 1024 * 1024)	//Maximum file storage capacity of the home directory 							16 MB
#define MAX_HOME_FILE 				32					//Number of files/directories that can be created in the home directory 		32
#define MAX_TMP_SIZE				(40 * 1024 * 1024)	//Maximum storage capacity of file that can be created in /tmp 					40 MB
#define MAX_TMP_FILE				64					//Number of files/directories that can be created in /tmp 						64

#define MAX_PRODUCTIONCODE_LENGHT	32
#define FSBLOCK_BSIZE				(16 * 1024)
#define MEM_ALIGNMENT				64
#define NAME_MAX_CHAR				(NAND_MAX_NAME)
namespace {
	uint8			umask = 		(NAND_PERM_OWNER_READ | NAND_PERM_OWNER_WRITE | NAND_PERM_GROUP_READ);
	char  			gameSaveDirPath	[NAND_MAX_PATH] 				ATTRIBUTE_ALIGN(32);
	char 			productCode		[MAX_PRODUCTIONCODE_LENGHT +1 ] ATTRIBUTE_ALIGN(32); // Add 1 for '\0' Caractere
	
	volatile int32 	callBackResult;
	volatile bool	callBackPass;
	
	void * 			fillFileBuffer;
	char * 			recordListBuffer;
	uint32			recordListBSize;
	NANDBanner * 	banner;
	struct NVRBanner 
	{
		NvBitmap * 		banner;
		NvBitmap *		icons [8];
		uint	  		speed [8];
		uint32			flag;
		char 			name[32];
		char	 		comment[32];
	} nvrBanner ;

	enum
	{
		MaxIcons 		= 8		,		
		IconsW 			= 48	,
		IconsH 			= 48	,
		BannerW			= 192	,
		BannerH			= 64	,
		NameLength		= 32	,
		CommentLength	= 32	,
	};
	
	void * nvramAlloc(uint32 inBsize , uint32 inAlign) {
		return mem::NativeMalloc ( inBsize , inAlign , mem::MEM_NGC_MEM2 );
	}
	
	void nvramFree	( void * inPtr) {
		NV_ASSERT (inPtr);
		mem::NativeFree(inPtr,mem::MEM_NGC_MEM2);
	}
	
#ifdef _NVCOMP_ENABLE_DBG
	#define OUTPUT_MESSAGE(res) DebugOutputMessage(res);
	#define OUTPUT_ERROR(res) if (res != NAND_RESULT_OK && res < 0) DebugOutputMessage(res);
	
	void DebugOutputMessage (int32 inRes) 
	{
		switch (inRes) {
			case NAND_RESULT_OK:
				DebugPrintf ("Nand Result : NAND_RESULT_OK\n");
				break;
			case NAND_RESULT_ACCESS:
				DebugPrintf ("Nand Result : NAND_RESULT_ACCESS\n");
				break;
			case NAND_RESULT_ALLOC_FAILED:
				DebugPrintf ("Nand Result : NAND_RESULT_ALLOC_FAILED\n");
				break;
			case NAND_RESULT_AUTHENTICATION:
				DebugPrintf ("Nand Result : NAND_RESULT_AUTHENTICATION\n");
				break;
			case NAND_RESULT_BUSY:
				DebugPrintf ("Nand Result : NAND_RESULT_BUSY\n");			
				break;
			case NAND_RESULT_CORRUPT:
				DebugPrintf ("Nand Result : NAND_RESULT_CORRUPT\n");
				break;
			case NAND_RESULT_ECC_CRIT:
				DebugPrintf ("Nand Result : NAND_RESULT_ECC_CRIT\n");
				break;
			case NAND_RESULT_EXISTS:
				DebugPrintf ("Nand Result : NAND_RESULT_EXISTS\n");
				break;
			case NAND_RESULT_INVALID:
				DebugPrintf ("Nand Result : NAND_RESULT_INVALID\n");
				break;
			case NAND_RESULT_MAXBLOCKS:
				DebugPrintf ("Nand Result : NAND_RESULT_MAXBLOCKS\n");
				break;
			case NAND_RESULT_MAXFD:
				DebugPrintf ("Nand Result : NAND_RESULT_MAXFD\n");
				break;
			case NAND_RESULT_MAXFILES:
				DebugPrintf ("Nand Result : NAND_RESULT_MAXFILES\n");
				break;
			case NAND_RESULT_NOEXISTS:
				DebugPrintf ("Nand Result : NAND_RESULT_NOEXISTS\n");
				break;
			case NAND_RESULT_NOTEMPTY:
				DebugPrintf ("Nand Result : NAND_RESULT_NOTEMPTY\n");
				break;
			case NAND_RESULT_OPENFD:
				DebugPrintf ("Nand Result : NAND_RESULT_OPENFD\n");
				break;
			case NAND_RESULT_UNKNOWN:
				DebugPrintf ("Nand Result : NAND_RESULT_UNKNOWN\n");
				break;
			case NAND_RESULT_FATAL_ERROR:
				DebugPrintf ("Nand Result : NAND_RESULT_FATAL_ERROR\n");
				break;
			default : 
				DebugPrintf ("Nand Size Result : %d \n",inRes);
		}
	}
#else 	//_NVCOMP_ENABLE_DBG 
	#define OUTPUT_MESSAGE(err)
	#define OUTPUT_ERROR(res) 
#endif 	//_NVCOMP_ENABLE_DBG

	enum RequestT {
		RT_None			=	 0,
		RT_Create,
		RT_Delete,
		RT_FirstRecord,
		RT_NextRecord,
		RT_Status,
		RT_Read,
		RT_Write
	};
	
	enum StateT {
		S_None		=	0x0,
		S_Create	=	0x1,
		S_Delete	=	0x2,
		S_Open		=	0x3,
		S_Close		=	0x4,
		S_Read		=	0x5,
		S_Write		=	0x6,
		S_Seek		=	0x7,		
		S_Status	=	0x8,
		S_NbRecord	=	0x9,
		S_Record	=	0xa,
		S_Info		=	0xb,
		S_Type		= 	0xc,
		S_Check		= 	0xd,
		S_Error		= 	0xe,
	};
	
	enum FctRet {
		FR_Ok	,
		FR_Error,
		FR_Pass	,
	};
	
#define STATE_SHIFT 4
#define STATE_MASK  0xF

	struct Request {
		RequestT			reqType					;
		StateT				state					;
		uint64				operationsList			;
		uint64				operationsListDone		;
		nvram::	Result		result					;
		
		//char				name[NAME_MAX_CHAR+1]	;
		char				name[NAND_MAX_PATH + NAME_MAX_CHAR+1]	;
		uint32				bSize		 			;
		void *				buffer		 			;
		uint32  			bOffset		 			;

		u32					freeBlock	 			;
		u32					freeINode	 			;
		u32					numRecord				;
		uint8				type					;
		uint32				checkAnswer				;
		uint32 				fileSize				;
		
				
		uint32			*	outFreeBSize 			;
		uint32			* 	outFullBSize			;
		char			*	outName					;
		uint32			*	outBSize	 			;
		uint32				curReadRecord			;
		char 			* 	curReadName				;
		
		NANDFileInfo 		info					;
		NANDCommandBlock 	block					;
		
	} request ;
	
	
	void nandCallback ( s32 inResult	, NANDCommandBlock *  )
	{
		NV_ASSERT( ! callBackPass );
		callBackResult 	= inResult;
		callBackPass 	= TRUE;
	}

	nvram::	Result NANDErrorToResult (Request * req ,  int32 inErr )
	{
		NV_ASSERT(req);
		
		if (inErr > 0 ) {
			if (req->state == S_Seek 	&& inErr == req->bOffset	) {
				return nvram::NVR_SUCCESS;
			}
			if (req->state == S_Write 	&& inErr == req->bSize		) {
				return nvram::NVR_SUCCESS;
			}
			if (req->state == S_Read 	&& inErr == req->bSize		) {
				return nvram::NVR_SUCCESS;
			}
			return nvram::NVR_ERROR;
		}
			
		
		switch (inErr) {
			case NAND_RESULT_OK :				//Function completes successfully.
				return nvram::NVR_SUCCESS;
				
			case NAND_RESULT_ACCESS :			//The are no permission/access privileges for accessing files or directories.
				return nvram::NVR_PROTECTED;
				
			case NAND_RESULT_ALLOC_FAILED :		//The library failed internally to secure the memory needed for receiving a large volume of requests at one time. However, if you wait some time and call the NAND function again, the process might succeed.
				return nvram::NVR_ALLOCATION_FAILED;
				
			case NAND_RESULT_AUTHENTICATION :	//Data authentication failed. The data in Wii console NAND memory might have been altered illegally.
				return nvram::NVR_AUTHENTICATION;
				
			case NAND_RESULT_BUSY :				//Busy state (the queue holding requests is full). If you wait some time and call the NAND function again, the process might succeed.
				return nvram::NVR_BUSY;
				
			case NAND_RESULT_CORRUPT :			//Wear in the FAT region has caused irreparable damage to Wii console NAND memory.
				return nvram::NVR_CORRUPT;
				
			case NAND_RESULT_ECC_CRIT :			//Indicates that a fatal ECC error has been detected (that is, data error correction is not possible). This code might be returned when the Wii console NAND memory is severely worn because the number of write/delete cycles has exceeded the device guaranteed performance.
				return nvram::NVR_ECC_CRIT;
				
			case NAND_RESULT_EXISTS :			//File or directory with the same name already exists.
				return nvram::NVR_ALREADYEXIST;
				
			case NAND_RESULT_MAXBLOCKS :		//All of the FS blocks in the file system have been used, so there is no free space. This code is also returned when, due to device wear, the number of bad blocks has reached the upper limit.
				return nvram::NVR_MAXBLOCKS;
				
			case NAND_RESULT_MAXFILES :			//No more files/directories can be created as the file system's inodes are used up.
				return nvram::NVR_MAXFILES;
				
			case NAND_RESULT_NOEXISTS :			//The specified file or directory does not exist.
				return nvram::NVR_NOTFOUND;

			case NAND_RESULT_INVALID :			//Input parameter or the function call is invalid. This code is returned when an incorrect argument has been passed to a NAND function, an attempt is made to close a file twice, or an attempt is made to use NANDSafe[Close] to close a file that was opened by NANDOpen[Async] (or visa-versa).
				return nvram::NVR_ERROR;
				
			case NAND_RESULT_UNKNOWN :			//Unknown error. When this code is returned, there is a high probability that there is some problem with the SDK. If possible, report the issue to Nintendo.
				return nvram::NVR_ERROR;
				
			case NAND_RESULT_FATAL_ERROR :
				return nvram::NVR_ERROR;

			// Errors should not arrived :
			case NAND_RESULT_OPENFD :			//The file descriptor has been left open. This code is returned when an attempt is made to delete a file that has not yet been closed.
				NV_ASSERT(FALSE);
				return nvram::NVR_ERROR;
				
			case NAND_RESULT_NOTEMPTY :			//File is not empty.
				NV_ASSERT(FALSE);
				return nvram::NVR_ERROR;
							 
			case NAND_RESULT_MAXFD : 			//No more files can be opened because all of the file descriptor entries have been used. Reduce the number of files you open at one time.
				NV_ASSERT(FALSE);
				return nvram::NVR_ERROR;	
			default :
				NV_ASSERT(FALSE);
		}
		return nvram::NVR_ERROR;
	}
	
	
	void FixError (Request * req ,  nvram::Result inErr ) {
		NV_ASSERT (req);
		if ( req->result == nvram::NVR_SUCCESS ) {		// result keeps only the first occured error!
			req->result = inErr;
		}
	}
	
	FctRet Create(Request  * req)
	{
		NV_ASSERT( req );
		NV_ASSERT( req->name[0] != '\0'  );
		NV_ASSERT( req->bSize );
		req->state = S_Create	;
		
		int ret = NANDCreateAsync( req->name, umask , uint8(0) , nandCallback , &req->block );
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Suppr (Request  * req) 
	{
		NV_ASSERT( req );
		NV_ASSERT( req->name[0] != '\0'  );	
		req->state = S_Delete	;
		
		int ret = NANDDeleteAsync( req->name, nandCallback , &req->block );
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Open(Request  * req)  
	{
		NV_ASSERT( req );
		NV_ASSERT( req->name[0] != '\0'  );
		req->state = S_Open	;
		
		bool r =	req->reqType == RT_Read			||
					req->reqType == RT_FirstRecord  ||
					req->reqType == RT_Write  		||
					req->reqType == RT_NextRecord   ;
					
		bool w = 	req->reqType == RT_Write  	||
					req->reqType == RT_Create 	;
		
		
		uint8 access = (r?NAND_ACCESS_READ:0) + (w?NAND_ACCESS_WRITE:0);
		NV_ASSERT(access);
		
		int ret = NANDOpenAsync( req->name , &req->info, access  , nandCallback , &req->block );
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Close (Request  * req)
	{
		NV_ASSERT( req );
		req->state = S_Close	;	
		
		int ret = NANDCloseAsync( &req->info, nandCallback , &req->block );
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Read (Request  * req) 
	{
		NV_ASSERT( req 			);
		NV_ASSERT( req->buffer 	);
		NV_ASSERT( req->bSize 	);
		req->state = S_Read	;	
		
		int ret = NANDReadAsync( &req->info,req->buffer,req->bSize, nandCallback , &req->block );
		
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Write (Request  * req) 
	{
		NV_ASSERT( req 			);
		NV_ASSERT( req->bSize 	);
		req->state = S_Write	;	
		
		void * buf;
		
		if ( !req->buffer ) {
			NV_ASSERT ( !fillFileBuffer );
			fillFileBuffer = nvramAlloc(req->bSize , MEM_ALIGNMENT);
			Memset(fillFileBuffer,0,req->bSize );
			if (!fillFileBuffer){
				FixError(req,nvram::NVR_ERROR );
				return FR_Error;
			}
			buf = fillFileBuffer;
		}
		else
			buf = req->buffer;

		int ret = NANDWriteAsync( &req->info,buf,req->bSize, nandCallback , &req->block );
		
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Seek (Request  * req)
	{
		NV_ASSERT( req 			);
		req->state = S_Seek	;	
		
		if (req->bOffset == 0) return FR_Pass;
		
		
		int ret = NANDSeekAsync( &req->info, req->bOffset, NAND_SEEK_SET, nandCallback , &req->block );
		
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Status (Request  * req)
	{
		NV_ASSERT( req 			);
		req->state = S_Status	;	

		int ret = NANDGetAvailableAreaAsync( &req->freeBlock, &req->freeINode ,  nandCallback , &req->block )	;
		
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet NbRecord (Request  * req)
	{
		NV_ASSERT( req 			);
		req->state = S_NbRecord	;	
		
 		int ret = NANDReadDirAsync( ".", NULL, &req->numRecord , nandCallback , &req->block );

		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Record (Request  * req)
	{
		NV_ASSERT( req 			  );
		NV_ASSERT( req->numRecord );
		req->state = S_Record	 ;	
		
		uint32 safeSize = Round32( req->numRecord * (NAND_MAX_NAME+1) );
		if ( (recordListBSize < safeSize) && recordListBuffer) {
			nvramFree(recordListBuffer);
			recordListBuffer 	= NULL;
			recordListBSize 	= 0;
		}
		
		if ( ! recordListBuffer  ) {
			recordListBuffer = (char*)nvramAlloc(safeSize , 32);
			NV_ASSERT (recordListBuffer);
			recordListBSize  = safeSize;
		}
		
		int ret = NANDReadDirAsync( ".", recordListBuffer , &req->numRecord , nandCallback , &req->block );

		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Info (Request  * req)		// GetSize 
	{
		NV_ASSERT( req 			);
		req->state = S_Info ;	

		int ret = NANDGetLengthAsync( &req->info , (u32*)(&req->fileSize) , nandCallback , &req->block );
		
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet TypeFD (Request  * req)		// File or directory ?
	{
		NV_ASSERT( req 	);
		NV_ASSERT( req->name[0] != '\0');
		req->state = S_Type ;	

		int ret = NANDGetTypeAsync( req->name, &req->type,  nandCallback , &req->block );
		
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	FctRet Check (Request  * req)		// check if a new file can be created ! (TRC)
	{
		NV_ASSERT( req 			);
		NV_ASSERT( req->bSize	);
		
		req->state = S_Check ;	

		uint32 	needFsBlock = (req->bSize / FSBLOCK_BSIZE) + ( (req->bSize%FSBLOCK_BSIZE) !=0 ); // (req->bSize / FSBLOCK_BSIZE) + 1  or (req->bSize / FSBLOCK_BSIZE) 
		uint32	needINode	= 1;
		int ret = NANDCheckAsync	( needFsBlock , needINode , (u32*)(&req->checkAnswer), nandCallback , &req->block );
		
		OUTPUT_ERROR(ret);
		FixError(req,NANDErrorToResult ( req , ret ) );
		return ((ret == NAND_RESULT_OK)?FR_Ok:FR_Error);
	}
	
	void PostOperation ( Request * req) 
	{
		if (req->reqType == RT_Create && req->state == S_Check ){
			if (req->checkAnswer !=0 && req->result == nvram::NVR_SUCCESS) {
				req->operationsList = 0;
				if (req->checkAnswer == NAND_CHECK_SYS_INSSPACE){
					FixError(req, nvram::NVR_SYSMAXBLOCKS);
				}
				else if (req->checkAnswer == NAND_CHECK_SYS_INSINODE)
				{
					FixError(req, nvram::NVR_SYSMAXFILES);
				}
				else 
				{
					FixError(req, nvram::NVR_ISFULL);
				}
			}
		}
		
		if ( fillFileBuffer ) { // if last step is write with buffer==NULL fillFileBuffer have been allocated, and now it must be free
			nvramFree(fillFileBuffer);
			fillFileBuffer = NULL;
		}
		
		//if ( (req->state == S_Write && req->reqType == RT_Write ) || (req->state == S_Read && req->reqType == RT_Read )) {
		if ( (req->state == S_Write && req->reqType == RT_Write ) ) {
			NvFree (req->buffer) ;
		}
		
		if ( req->state == S_Status){
			if (req->outFreeBSize ) 
				*req->outFreeBSize  = req->freeBlock * FSBLOCK_BSIZE;
			if (req->outFullBSize ) 				
				*req->outFullBSize	= MAX_HOME_SIZE - (req->freeBlock * FSBLOCK_BSIZE);
		}


		if ( req->state == S_NbRecord && req->numRecord == 0 ) {
			req->operationsList = 0; 		// Reset opereation list;
			FixError(req, nvram::NVR_NOTFOUND);
		}
		
		if ( req->state == S_Record ) {
			req->curReadName= recordListBuffer;
			NV_ASSERT( Strlen(req->curReadName) <= NAME_MAX_CHAR );
			Strcpy(req->name , req->curReadName);
		}

		if ( req->state == S_Info && ( req->reqType == RT_FirstRecord || req->reqType == RT_NextRecord )) {
			if ( req->outName ) 
				Strcpy( req->outName , req->name);
			if ( req->outBSize )
				*req->outBSize = req->fileSize;
		}

		if ( req->state == S_Info && ( req->reqType == RT_Write )) {
			if ( req->bOffset + req->bSize > req->fileSize ) {
		//		req->operationsList = S_Close ;
				FixError(req, nvram::NVR_ISFULL);
			}
		}
		
		if ( req->state == S_Type && (req->reqType == RT_FirstRecord || req->reqType == RT_NextRecord)) {
			if (req->type == NAND_TYPE_DIR)
				req->operationsList = 0;	// Reset opereation list, can do open/info/close on a directory
		}
		
		if ( req->result != nvram::NVR_SUCCESS ) {
			req->operationsList = 0; // If error then reset operations list, task is finished.
			uint64 copyListDone = req->operationsListDone;
			while (copyListDone != 0 ) {
				if ( (copyListDone & STATE_MASK) == S_Open ) {
					req->operationsList = S_Close;
				}
				copyListDone  = copyListDone >> STATE_SHIFT;
			}
		}
	}

	// do next operation according to the operationsList
	FctRet NextStep (Request * req) 
	{
		NV_ASSERT (req);
		
		StateT step = StateT(req->operationsList & STATE_MASK);
		req->operationsList = req->operationsList >> STATE_SHIFT;
		NV_ASSERT ( step > S_None && step < S_Error )
		req->operationsListDone = (req->operationsListDone << STATE_SHIFT) | step;
		
		FctRet isStart = FR_Error;
		switch (step) {
			case S_Create	:
				isStart = Create(req);
				break;
			case S_Delete	:
				isStart = Suppr(req);
				break;			
			case S_Open		:
				isStart = Open(req);
				break;			
			case S_Close 	:
				isStart = Close(req);
				break;			
			case S_Read		:
				isStart = Read(req);
				break;			
			case S_Write	:
				isStart = Write(req);
				break;
			case S_Seek		:
				isStart = Seek(req);
				break;
			case S_Status	:
				isStart = Status(req);
				break;			
			case S_NbRecord	:
				isStart = NbRecord(req);
				break;			
			case S_Record	:
				isStart = Record(req);
				break;
			case S_Info		:
				isStart = Info(req);
				break;				
			case S_Type		:
				isStart = TypeFD(req);
				break;						
			case S_Check	:
				isStart = Check(req);
				break;
			default 		:
				NV_ASSERT(FALSE);			
		}
		NV_ASSERT(req->state == step)
		
		return isStart;
	}


};

bool
nvcore_NVR_Init()
{
	NV_COMPILE_TIME_ASSERT(NVHW_IO_BALIGN == 32 ); // Warning, if NVHW_IO_BALIGN change, it must be a power of 2, otherwise change assertions and tests in ReadRecord, WriteRecord, CreateRecord.
	
	Memset ( gameSaveDirPath 	, '\0' 	, NAND_MAX_PATH 					* sizeof ( char ) 	);
	Memset ( productCode		, '\0' 	, (MAX_PRODUCTIONCODE_LENGHT + 1) 	* sizeof ( char ) 	);

	Memset ( &request			,	0	, sizeof(Request)										);
	
	fillFileBuffer 	= NULL;
	recordListBuffer= NULL;
	recordListBSize = 0;
		
	request.reqType 		= RT_None			;
	request.result			= nvram::NVR_SUCCESS;
	request.state			= S_None			;
	
	int32 retInit 	= NANDInit();
	OUTPUT_ERROR(retInit);
	if (retInit != NAND_RESULT_OK) return FALSE;

	int32 retCurDir = NANDGetCurrentDir(gameSaveDirPath);
	OUTPUT_ERROR(retCurDir);
	if (retCurDir != NAND_RESULT_OK) return FALSE;
	
	banner = (NANDBanner *)NvMalloc(64*1024);
	
	Zero(nvrBanner);
	return TRUE;
}

void
nvcore_NVR_Update()
{

	bool processEnd = FALSE;
	
	if ( request.reqType != RT_None ) {
		NV_ASSERT ( request.state != S_None || request.operationsList != 0);
		
		if ( (request.state != S_None && callBackPass) || request.state == S_None) {
		
			if ( callBackPass )		{ 		// last step is done, so check Error				
				OUTPUT_ERROR     ( callBackResult ) ;
				FixError(&request, NANDErrorToResult ( &request , callBackResult ));
				PostOperation(&request);
				
				callBackPass   = FALSE			;
				callBackResult = NAND_RESULT_OK	;
			}
			
			while ( 1 ) {
			
/*				if ( request.result != nvram::NVR_SUCCESS ) {
					processEnd = TRUE;
					break;
				}
*/				
				if ( !request.operationsList ){
					processEnd = TRUE;
					break;
				}
				
				FctRet isStart = NextStep (& request );
				
				if (isStart == FR_Ok)
					break;
				if (isStart == FR_Error){
					processEnd = TRUE;
					break;
				}
			}		
		}
		
	}
	
	if (processEnd) {	// End of process, reset request state.
		request.reqType 		= RT_None	;
		request.state 			= S_None	;
		request.operationsList  = 0 		;
	}
}

void
nvcore_NVR_Shut()
{
	while ( request.reqType != RT_None )  {
		nvcore_NVR_Update();
	}
	if ( recordListBuffer ){
		nvramFree(recordListBuffer);
		recordListBuffer = NULL;
	}
	recordListBSize = 0;
	NV_ASSERT ( !fillFileBuffer );
	NvFree(banner);
}


bool
nv::nvram::SetProductCode	(	pcstr		inProductCode		)
{
	uint32 pcL 	= Strlen(inProductCode);
	if   (!pcL) return FALSE;
	
	uint32 copyL= Min ( pcL , uint32 ( MAX_PRODUCTIONCODE_LENGHT+1 ));

	Strncpy(productCode , inProductCode , copyL);

	return TRUE;
}


pcstr
nv::nvram::GetProductCode	(									)
{
	return productCode;
}


uint
nv::nvram::GetNbMax			(									)
{
	return 1;
}


bool
nv::nvram::IsReady			(	Result	*		outLastResult	)
{
	if ( request.reqType != RT_None ) return FALSE;
	
	NV_ASSERT ( request.state == S_None);
	
	if ( outLastResult )
		*outLastResult = request.result;

	// reset Name !
	request.name[0] = '\0';
	
	return TRUE;
}


bool
nv::nvram::GetStatus		(	uint			inNVRNo,
								uint32	*		outFullBSize,
								uint32	*		outFreeBSize,
								bool	*		outSwitched		)
{
	if ( request.reqType != RT_None ) return FALSE;
	NV_ASSERT ( request.state == S_None);
	
	request.reqType 		= RT_Status 		;
	request.result  		= nvram::NVR_SUCCESS;
	request.outFreeBSize	= outFreeBSize		;
	request.outFullBSize	= outFullBSize		;
	
	if (outFullBSize) *outFullBSize = 0		;
	if (outFreeBSize) *outFreeBSize = 0		;
	if (outSwitched ) *outSwitched  = FALSE	;
		
	request.operationsList 		= (S_Status<< (STATE_SHIFT  * 0 ))	;
	request.operationsListDone	= 0;	
	return TRUE;
}

bool		
nv::nvram::SetBannerFlag	(	uint	inFlags		)
{
	nvrBanner.flag = inFlags;
	return true;
}

bool		
nv::nvram::SetBannerImage	(	NvBitmap*	inBitmap	)
{
	if (inBitmap and ( !inBitmap->IsBanner() 				|| 
						inBitmap->GetWidth () != BannerW 	|| 
						inBitmap->GetHeight() != BannerH	)) return false;
	return SafeGetRef(nvrBanner.banner,inBitmap);
}


bool		
nv::nvram::SetBannerIcon	(	uint		inIdx		,		// [0,7]
								NvBitmap*	inBitmap	,
								int	inSpeed				)
{

	if (inBitmap and ( 	!inBitmap->IsBanner() 			|| 
						inBitmap->GetWidth ()  != IconsW 	||
	 					inBitmap->GetHeight() != IconsH 	)) return false;

	
	if (inIdx >= 8) return false;
	bool ret = SafeGetRef( nvrBanner.icons[inIdx] , inBitmap);
	nvrBanner.speed[inIdx]  = inSpeed;
	return true;	
}


bool		
nv::nvram::SetBannerName	(	pcstr	inName		)
{
	if (!inName || Strlen(inName) == 0 ) return false;
	ASSERT(Strlen(inName) <= 32);
	Memcpy((void*)nvrBanner.name, (void*)inName , Min(uint (32),uint(Strlen(inName))));
	return true;
}


bool		
nv::nvram::SetBannerComment	(	pcstr	inComment	)
{
	if (!inComment || Strlen(inComment) == 0 ) return false;
	ASSERT(Strlen(inComment) <= 32);
	Memcpy((void*)nvrBanner.comment,(void*) inComment , Min(uint (32),uint (Strlen(inComment))));
	return true;
}


bool		
nv::nvram::ReleaseBannerBitmaps	(	)
{
	SafeRelease(nvrBanner.banner);
	for (int i = 0 ; i < 8 ; ++i )
		SafeRelease(nvrBanner.icons[i]);
	return true;
}

bool
nv::nvram::Format			(	uint	inNVRNo			)
{
	enum IconSpeed 
	{
		IS_End 		= 0x0,	//NAND_BANNER_ICON_ANIM_SPEED_END    0
		IS_Fast 	= 0x1,	//NAND_BANNER_ICON_ANIM_SPEED_FAST   1
		IS_Normal 	= 0x2,	//NAND_BANNER_ICON_ANIM_SPEED_NORMAL 2
		IS_Slow 	= 0x3,	//NAND_BANNER_ICON_ANIM_SPEED_SLOW   3
		IS_MASK 	= 0x3,	//NAND_BANNER_ICON_ANIM_SPEED_MASK   3	
	};
	
	enum IconAnim
	{
		IA_Bounce	= 0x10,			//NAND_BANNER_FLAG_ANIM_BOUNCE 0x00000010
		IA_Loop		= 0x00,			//AND_BANNER_FLAG_ANIM_LOOP   0x00000000
		IA_Mask		= 0x10,
	};
	uint16			name	[NameLength 	];
	uint16			comment	[CommentLength ];
	
	if ( inNVRNo >= GetNbMax()				) return FALSE;
	if ( request.reqType != RT_None			) return FALSE;
	NV_ASSERT ( request.state == S_None		);

	if ( !nvrBanner.banner				) return false;
	if ( !nvrBanner.icons[0]	 		) return false;
	if ( nvrBanner.name[0] == '\0'  	|| 
	 	 nvrBanner.comment[0] == '\0'	) return false;

	NV_COMPILE_TIME_ASSERT(NAND_BANNER_TEXTURE_SIZE == BannerW * BannerH * 2)
	NV_COMPILE_TIME_ASSERT(NAND_BANNER_ICON_SIZE == IconsW * IconsH * 2)

	uint i = 0;
	for ( i = 0; i < NameLength -1 ; ++i ){
		if (nvrBanner.name[i] == '\0') break;
		name[i] = uint16(nvrBanner.name[i]);
	}
	name[i] = uint16('\0');
	
	for ( i = 0; i < CommentLength-1 ; ++i ){
		if ( nvrBanner.comment[i] == '\0') break;
		comment[i] = uint16( nvrBanner.comment[i]);
	}
	comment[i] = uint16('\0');
	
	NANDInitBanner( banner, nvrBanner.flag , name , comment);
 	
	void * dataPtr = CachedPointer((nvrBanner.banner->GetKInterface())->GetTexel());
	Memcpy(banner->bannerTexture,dataPtr,NAND_BANNER_TEXTURE_SIZE);
	uint nbIcons = 0;
	for ( uint i=0; i<MaxIcons ; ++i ){
		if ( nvrBanner.icons[i] ) {
			void * dataPtr = CachedPointer((nvrBanner.icons[i]->GetKInterface())->GetTexel());
			Memcpy(banner->iconTexture[i], dataPtr,NAND_BANNER_ICON_SIZE);
			nbIcons++;
		}
		else
			break;
	}
	for ( uint i=0; i<MaxIcons ; ++i ){
		NANDSetIconSpeed(banner,i, nvrBanner.speed[i]);
	}

    bool ret = nvram::CreateRecord(0,"banner.bin",NAND_BANNER_SIZE(nbIcons),(void*)banner);
    
    if (!ret) return false;
    
    Result res; 
    while (!IsReady(&res))
    {
    	nvcore_NVR_Update();
    }
    
    return res == NVR_SUCCESS || res == NVR_ALREADYEXIST;
}


bool
nv::nvram::GetFirstRecord	(	uint			inNVRNo,
								pstr			outName,
								uint32		*	outBSize	)	
{	
	if ( inNVRNo >= GetNbMax()					) return FALSE;
	if ( request.reqType != RT_None 			) return FALSE;
	NV_ASSERT ( request.state == S_None);
	
	request.reqType 		= RT_FirstRecord	;
	request.result  		= nvram::NVR_SUCCESS;
	request.outName  		= outName 			;
	request.outBSize	 	= outBSize			;		
	request.curReadRecord	= 0					;
	
	if (outName)		outName[0]		= '\0'	;
	if (outBSize) 		* outBSize 		= 0		;
	//if (outCreateTime) 	Memset(outCreateTime, 0 , sizeof(DateTime) );
	//if (outModifyTime) 	Memset(outModifyTime, 0 , sizeof(DateTime) );
	
	request.operationsList 	= 	(S_NbRecord<< (STATE_SHIFT  * 0 ))	|
								(S_Record  << (STATE_SHIFT  * 1 ))	|
								(S_Type    << (STATE_SHIFT  * 2 ))  |
								(S_Open    << (STATE_SHIFT  * 3 ))	|
								(S_Info    << (STATE_SHIFT  * 4 ))	|
								(S_Close   << (STATE_SHIFT  * 5 ))	;
	request.operationsListDone	= 0;								
	return TRUE;
}

bool
nv::nvram::GetNextRecord	(	pstr			outName,
								uint32		*	outBSize)
{

	if ( request.reqType != RT_None 			) return FALSE;
	NV_ASSERT ( request.state == S_None);

	if (outName)		outName[0]		= '\0'	;
	if (outBSize) 		* outBSize 		= 0		;
	//if (outCreateTime) 	Memset(outCreateTime, 0 , sizeof(DateTime) );
	//if (outModifyTime) 	Memset(outModifyTime, 0 , sizeof(DateTime) );
	
	if (int32(request.curReadRecord) >= int32(int32(request.numRecord)-1) ) {
		request.result = NVR_NOTFOUND;
	 	return TRUE;
	}
	
	NV_ASSERT( recordListBuffer && recordListBSize );
	NV_ASSERT( request.curReadName >= recordListBuffer && request.curReadName < (recordListBuffer + recordListBSize) );
	
	while ( *request.curReadName != '\0' ) request.curReadName ++; 	// go to the end of the current Name;
	request.curReadName ++;											// place pointer on the first character
	request.curReadRecord++;										// currently reading (request.curReadRecord)record ( total : request.numRecord ).
	
	NV_ASSERT( Strlen ( request.curReadName ) <= NAME_MAX_CHAR  );
	Strcpy(request.name , request.curReadName)	;
	request.reqType 		= RT_NextRecord		;
	request.result  		= nvram::NVR_SUCCESS;
	request.outName  		= outName 			;
	request.outBSize	 	= outBSize			;

	request.operationsList 	= 	(S_Type    << (STATE_SHIFT  * 0 ))  |
								(S_Open    << (STATE_SHIFT  * 1 ))	|
								(S_Info    << (STATE_SHIFT  * 2 ))	|
								(S_Close   << (STATE_SHIFT  * 3 ))	;
	request.operationsListDone	= 0;
	return TRUE;
}


bool
nv::nvram::CreateRecord		(	uint			inNVRNo,
								pcstr			inName,
								uint32			inBSize,
								pvoid			inBuffer	)
{
	if ( inNVRNo >= GetNbMax()					) return FALSE;
	if ( !inName								) return FALSE;
	if ( Strlen(inName) > (NAME_MAX_CHAR) 		) return FALSE;	
	if (!inBSize 								) return FALSE;
	NV_ASSERTC( (uint32(inBuffer) & (NVHW_IO_BALIGN-1)) == 0, "Buffer must be NVHW_IO_BALIGN Bytes aligned\n" );
	if ( request.reqType != RT_None 			) return FALSE;
	NV_ASSERT ( request.state == S_None);

	if ( inBSize & (NVHW_IO_BALIGN-1) ) {
		inBSize = RoundX(inBSize,NVHW_IO_BALIGN);
	 	DebugPrintf ( "Warning, File size must be a multiple of NVHW_IO_BALIGN Bytes\n" );
	 	DebugPrintf ( "file %s is creating with %d Bytes\n",inName,inBSize);
	}
	
	request.reqType = RT_Create 		;
	request.result  = nvram::NVR_SUCCESS;
	Strcpy(request.name , inName );
	
	request.bSize 	= inBSize			;
	request.buffer	= inBuffer			;
	
	request.operationsList 	= 	(S_Check << (STATE_SHIFT  * 0 ))	|
								(S_Create<< (STATE_SHIFT  * 1 ))	|
								(S_Open  << (STATE_SHIFT  * 2 ))	|
								(S_Write << (STATE_SHIFT  * 3 ))	|
								(S_Close << (STATE_SHIFT  * 4 ))	;
	request.operationsListDone	= 0;
	
	return TRUE;
}


bool
nv::nvram::WriteRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset	)
{
	if ( inNVRNo >= GetNbMax()					) return FALSE;
	if ( !inName								) return FALSE;
	if ( Strlen(inName) > (NAME_MAX_CHAR) 		) return FALSE;	
	if (!inBuffer								) return FALSE;
	//NV_ASSERTC( (uint32(inBuffer) & (NVHW_IO_BALIGN-1)) == 0, "Buffer must be NVHW_IO_BALIGN Bytes aligned\n" );
	if (!inBSize 								) return FALSE;	
	if ( request.reqType != RT_None 			) return FALSE;
	
	//NV_ASSERTC( (inBSize & (NVHW_IO_BALIGN-1)) == 0, "Write size must be a multiple of NVHW_IO_BALIGN Bytes\n" );
	//if ( inBSize & (NVHW_IO_BALIGN-1)			) return FALSE;
	NV_ASSERTC( (inBOffset & (NVHW_IO_BALIGN-1)) == 0, "Write offset must be a multiple of NVHW_IO_BALIGN Bytes\n" );
	if ( inBOffset & (NVHW_IO_BALIGN-1)			) return FALSE;
	
	NV_ASSERT ( request.state == S_None);

	inBSize = Round32(inBSize);

	request.reqType = RT_Write 				;
	request.result  = nvram::NVR_SUCCESS	;
	Strcpy(request.name, inName)			;
	request.bSize 	= inBSize				;
	//request.buffer	= inBuffer				;
	request.buffer = NvMalloc(inBSize)		;
	Memcpy(request.buffer,inBuffer,inBSize)	;
	request.bOffset	= inBOffset				;
	
	request.operationsList 	= 	(S_Open  << (STATE_SHIFT * 0 ))	|
								(S_Info	 << (STATE_SHIFT * 1 ))	|
								(S_Seek  << (STATE_SHIFT * 2 ))	|	
								(S_Write << (STATE_SHIFT * 3 ))	|
								(S_Close << (STATE_SHIFT * 4 ))	;
	request.operationsListDone	= 0;								
	return TRUE;
}


bool
nv::nvram::ReadRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset		)
{
	if ( inNVRNo >= GetNbMax()					) return FALSE;
	if ( !inName								) return FALSE;
	if ( Strlen(inName) > (NAME_MAX_CHAR) 		) return FALSE;	
	if (!inBuffer								) return FALSE;
	if (!inBSize 								) return FALSE;
	//NV_ASSERTC( (inBSize  & (NVHW_IO_BALIGN-1)) == 0, "Read size must be a multiple of NVHW_IO_BALIGN Bytes\n" );
	//NV_ASSERTC( (uint32(inBuffer) & (NVHW_IO_BALIGN-1)) == 0, "Buffer must be NVHW_IO_BALIGN Bytes aligned\n" );
	//if ( inBSize & (NVHW_IO_BALIGN-1)   	) return FALSE;
	
	if ( request.reqType != RT_None 			) return FALSE;
	NV_ASSERT ( request.state == S_None);

	inBSize = Round32(inBSize);
	
	request.reqType = RT_Read 				;
	request.result  = nvram::NVR_SUCCESS	;	
	Strcpy(request.name, inName )			;
	request.bSize 	= inBSize				;
		request.buffer	= inBuffer			;
	//request.buffer = NvMalloc(inBSize)		;
	//Memcpy(request.buffer,inBuffer,inBSize)	;
	request.bOffset	= inBOffset				;
	
	request.operationsList 	= 	(S_Open  << (STATE_SHIFT  * 0 ))	|
								(S_Seek  << (STATE_SHIFT  * 1 ))	|		
								(S_Read  << (STATE_SHIFT  * 2 ))	|
								(S_Close << (STATE_SHIFT  * 3 ))	;
	request.operationsListDone	= 0;								
	return TRUE;
}


bool
nv::nvram::DeleteRecord		(	uint			inNVRNo,
								pcstr			inName			)
{
	if ( inNVRNo >= GetNbMax()					) return FALSE;
	if ( !inName								) return FALSE;
	if ( Strlen(inName) > (NAME_MAX_CHAR) 		) return FALSE;
	if ( request.reqType != RT_None 			) return FALSE;
	NV_ASSERT ( request.state == S_None );
	
	request.reqType = RT_Delete 			;
	request.result  = nvram::NVR_SUCCESS	;
	Strcpy( request.name , inName );

	request.operationsList 		= S_Delete ;
	request.operationsListDone	= 0;
	return TRUE;
}


#else //_RVL
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// NGC Version //////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool
nvcore_NVR_Init()
{
	return FALSE;
}


void
nvcore_NVR_Shut()
{
	
}


void
nvcore_NVR_Update()
{
	
}


bool
nv::nvram::SetProductCode	(	pcstr		inProductCode		)
{
	
	return TRUE;
}


pcstr
nv::nvram::GetProductCode	(									)
{
	return "";
}


uint
nv::nvram::GetNbMax			(									)
{
	return 0;
}


bool
nv::nvram::IsReady			(	Result	*		outLastResult	)
{
	return FALSE;
}


bool
nv::nvram::GetStatus		(	uint			inNVRNo,
								uint32	*		outFullBSize,
								uint32	*		outFreeBSize,
								bool	*		outSwitched		)
{
	return FALSE;
}


bool
nv::nvram::Format			(	uint			inNVRNo			)
{
	return FALSE;
}


bool
nv::nvram::GetFirstRecord	(	uint			inNVRNo,
								pstr			outName,
								uint32		*	outBSize	)
{
	return FALSE;
}


bool
nv::nvram::GetNextRecord	(	pstr			outName,
								uint32		*	outBSize	)
{
	return FALSE;
}


bool
nv::nvram::CreateRecord		(	uint			inNVRNo,
								pcstr			inName,
								uint32			inBSize,
								pvoid			inBuffer	)
{
	return FALSE;
}


bool
nv::nvram::WriteRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset	)
{
	return FALSE;
}


bool
nv::nvram::ReadRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset		)
{
	return FALSE;
}


bool
nv::nvram::DeleteRecord		(	uint			inNVRNo,
								pcstr			inName			)
{
	return FALSE;
}

#endif // _RVL 



