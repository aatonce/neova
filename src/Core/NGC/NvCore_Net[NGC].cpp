/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/
#ifndef RVL

#include <dolphin/socket.h>

#include <Nova.h>
using namespace nv;


//#define		NO_NETWORK
#define		DEF_WAIT_TIMEOUT		3000


namespace
{

	struct Sock
	{
		int				sockDesc;
		net::State		state;
		net::Type		type;
		int				next;
	};

	nv::sysvector<Sock>			sockA;
	int							sockFreeIdx;


	SOHostEnt*	ResolveHostAddr		(	pcstr	inName	)
	{		
		// loopback
		if( !inName || inName[0]==0 || Strcmp(inName,"127.0.0.1")==0 || Stricmp(inName,"localhost")==0 )
			inName = "127.0.0.1";
		return SOGetHostByName( inName );
	}


	bool	GetHostInAddr	(	SOInAddr&	outInAddr,
								pcstr		inName		)
	{
		// loopback
		if( !inName || inName[0]==0 || Strcmp(inName,"127.0.0.1")==0 || Stricmp(inName,"localhost")==0 ) {
			outInAddr.addr = SO_INADDR_LOOPBACK;
			return TRUE;
		}

		// IPv4 dotted addr ?
		if( SOInetAtoN(inName,&outInAddr) )
			return TRUE;

		// Try to resolve addr
		SOHostEnt* ht = ResolveHostAddr( inName );
		if( !ht || ht->length!=4 || !ht->addrList[0] )
			return FALSE;
		Memcpy( &outInAddr.addr, ht->addrList[0], 4 );	// first addr
		return TRUE;
	}						

	
	bool	MakeASync(int	sockDesc)
	{
		int val = SOFcntl(sockDesc, SO_F_GETFL, 0);
		SOFcntl(sockDesc, SO_F_SETFL, val | SO_O_NONBLOCK);
		return TRUE;
	}


	void*    NetAlloc (u32 name, s32 size) 
	{
		return OSAlloc(size);
	}


    void     NetFree  (u32 name, void* ptr, s32 size)
    {
    	OSFree(ptr);
    }

}



bool
nvcore_Net_Init()
{
	sockA.Init();
	sockA.clear();
	sockFreeIdx = -1;

	// must init network
	SOInit();
	
	SOConfig config = {
		SO_VENDOR_NINTENDO,
		SO_VERSION,
		// vendor specific section
		&NetAlloc,
		&NetFree,
		0,
		0xC0A80099,		//addr		//192.168.0.153
	    0xFFFFFF00,		//netmask	//255.255.255.0
	    0xC0A80001,		//router	//192.168.0.1
		0xC0A80001,		//dns1		//192.168.0.1
		0xC0A80001,		//dns2		//192.168.0.1
		4096,			//timeWaitBuffer 	// time wait buffer size
		4096,			//reassemblyBuffer 	// reassembly buffer size
		1500,      		//mtu             	// maximum transmission unit size
		17520,         	//rwin            	// default TCP receive window size
		OS_TIMER_CLOCK*4,//r2               // default TCP total retransmit timeout value			
		// PPP
		NULL, 			//peerid
		NULL, 			//passwd    		
		// PPPoE
    	NULL,			//serviceName       // UTF-8 string
	    // DHCP
	    NULL, 			//hostName          // DHCP host name
	    4,         		//rdhcp             // TCP total retransmit times (default 4)
	    // UDP
	    1472,         	//udpSendBuff       // default UDP send buffer size (default 1472)
	    4416         	//udpRecvBuff       // defualt UDP receive buffer size (default 4416)			
	};
	
	int ret = SOStartup ( &config );
	
	if (ret) return FALSE;
	
	return TRUE;
}



void
nvcore_Net_Update()
{
	//
}


void
nvcore_Net_Shut()
{
	sockA.Shut();
	sockFreeIdx = -1;
	SOCleanup();
}



uint
nv::net::GetHostAddrCpt		(	pcstr		inName		)
{
	SOHostEnt* ht = ResolveHostAddr( inName ) ;
	if( !ht )
		return 0;

	uint32 nb = 0;
	while( ht->addrList[nb] )
		nb++;

	return nb;
}


bool
nv::net::GetHostAddr		(	pcstr				inName,
								uint				inAddrNo,
								pstr				outAddr			)
{
	SOInAddr inaddr;

	if( inAddrNo==0 )
	{
		if( !GetHostInAddr(inaddr,inName) )
			return FALSE;
	}
	else
	{
		SOHostEnt* ht = ResolveHostAddr( inName ) ;
		if( !ht || ht->length!=4 || !ht->addrList[0] )
			return FALSE;
		for( int i = 0 ; i <= inAddrNo ; i++ )
			if( !ht->addrList[i] )
				return FALSE;
		u8* addr = ht->addrList[ inAddrNo ];
		Memcpy( &inaddr.addr, addr, 4 );
	}

	Sprintf( outAddr, "%d.%d.%d.%d",	(inaddr.addr>>24)&0xFF,
										(inaddr.addr>>16)&0xFF,
										(inaddr.addr>>8)&0xFF,
										(inaddr.addr>>0)&0xFF	);

	return TRUE;
}


int
nv::net::Create	(	Type		inType		)
{
	if( inType != T_TCP )
		return -1;

	if( sockFreeIdx == -1 )
	{
		// Resize
		int N = sockA.size();
		int P = N + 4;
		sockA.resize( P );
		sockFreeIdx = N;
		for( int i = N ; i < P-1 ; i++ ) {
			sockA[i].type = T_UNDEF;
			sockA[i].next = i+1;
		}
		sockA[P-1].next = -1;
		sockA[P-1].type = T_UNDEF;
	}

	// Alloc sockId
	int sid				= sockFreeIdx;
	sockFreeIdx			= sockA[sid].next;

	// Init sock
	sockA[sid].sockDesc	= -1;
	sockA[sid].state	= S_CLOSED;
	sockA[sid].type		= inType;
	return sid;
}


void
nv::net::Release	(	int			inSockId	)
{
	if (inSockId < 0 || inSockId >= int(sockA.size())) return;
	if (sockA[inSockId].type != T_UNDEF) {
		SOClose( inSockId );
		// Free sockId
		sockA[inSockId].type	= T_UNDEF;
		sockA[inSockId].next	= sockFreeIdx;
		sockFreeIdx				= inSockId;
	}
}


bool
nv::net::Open		(	int			inSockId,
						uint16		inPort,
						pcstr		inAddr		)
{
	State state = GetState(inSockId);
	if (state != S_CLOSED ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	sockP->sockDesc = SOSocket(SO_PF_INET,  SO_SOCK_STREAM, 0);
	if (sockP->sockDesc < 0 )
		return FALSE;

	//Bind socket	
	SOSockAddrIn so;
	so.len 		= sizeof (SOSockAddrIn);
	so.family 	= SO_PF_INET;
	so.port		= SOHtoNs ( inPort );
	//so.addr	
	if (MakeASync(sockP->sockDesc)) {
		if (!inAddr)
			so.addr.addr = SO_INADDR_ANY;
		else {
		 	int ret = SOInetPtoN(SO_PF_INET,inAddr,&so.addr);
			if (ret != 1)
				so.addr.addr = SO_INADDR_ANY;
		}
		if(! SOBind(sockP->sockDesc, &so) ){
			sockP->state = S_OPENED;
			return TRUE;
		}
	}
	return FALSE;
}


void
nv::net::Close		(	int			inSockId	)
{
	if (inSockId < 0 )				return ;
	if (inSockId >= int(sockA.size()))	return ;
	Sock * sockP = & sockA[inSockId];
	if (sockP->type != net::T_TCP )	return ;
	if (sockP->state <= S_CLOSED ) 	return ;

	if( sockP->sockDesc >= 0 )
	{
		SOClose(sockP->sockDesc);
		sockP->sockDesc = -1;
	}
	sockP->state = S_CLOSED;
}


bool
nv::net::Listen	(	int			inSockId,
					uint32		inPendingClients	)
{
	State state = GetState(inSockId);
	if (state != S_OPENED ) return FALSE;

	Sock * sockP = & sockA[inSockId];
		
	if (!SOListen(sockP->sockDesc, inPendingClients)){
		sockP->state = S_LISTENING;
		return TRUE;
	}
	return false;
}


bool
nv::net::Accept	(	int			inSockId,
					int	&		outSockId	)
{
	State state = GetState(inSockId);
	if (state != S_LISTENING ) return FALSE;

	Sock * sockP = & sockA[inSockId];


	outSockId = -1;
	int peerSock = SOAccept(sockP->sockDesc, NULL);	
	if (peerSock >= 0)
	{
		int asockId = Create( sockP->type );
		if( asockId < 0 )
			return FALSE;
		Sock * asockP	= & sockA[asockId];
		asockP->sockDesc= peerSock;
		asockP->state	= S_CONNECTED;
		outSockId		= asockId;
		if (!MakeASync(asockP->sockDesc)) {
			Close(asockP->sockDesc);
			return FALSE;	
		}
		return TRUE;
	}
	return FALSE;
}


bool
nv::net::Connect	(	int			inSockId,
						pcstr		inAddr,
						uint16		inPort		)
{
	State state = GetState(inSockId);
	if (state != S_CLOSED ) return FALSE;

	Sock * sockP = & sockA[inSockId];
	
	sockP->sockDesc = SOSocket(SO_PF_INET,  SO_SOCK_STREAM, 0);
	if (sockP->sockDesc < 0)
		return FALSE;
		
	if (!MakeASync(sockP->sockDesc)){
		Close( inSockId );
		return FALSE;
	}

	SOSockAddrIn so;
	so.len		= sizeof(SOSockAddrIn);
	so.family  	= SO_PF_INET;
	so.port 	= SOHtoNs( inPort );
	if( !GetHostInAddr(so.addr,inAddr) )
		return FALSE;

	int connecting = SOConnect(sockP->sockDesc, &so);
	
	if (connecting  == 0 || connecting == SO_EINPROGRESS)
	{	
		sockP->state = S_CONNECTING;
		return TRUE;
	}
	
	return FALSE;	
}

bool
nv::net::ConnectWait	(	int					inSockId,
							uint				inMsTimeout,
							pcstr				inAddr,
							uint16				inPort		)
{
	if( !Connect(inSockId,inAddr,inPort) )
		return FALSE;
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	for( ;; ) {
		if( GetState(inSockId) != S_CONNECTING )
			break;
		clock::GetTime( &t1 );
		if( (t1-t0)*1000.f > msTimeout ) {
			Close( inSockId );
			break;
		}
		//Sleep( 1 ); Wath I can do ...
	}
	return ( GetState(inSockId) == S_CONNECTED );
}

bool
nv::net::Receive	(	int					inSockId,
						uint32		&		outRcvSize,
						pvoid				inBuff,
						uint32				inBuffSize,
						bool				peek		)
{
	outRcvSize = 0;

	State state = GetState(inSockId);
	if (state != S_CONNECTED ) return FALSE;

	Sock * sockP = & sockA[inSockId];
	
	if (!inBuffSize)	return TRUE;
	if ( !inBuff )		return FALSE;

	int32 ret = 0;
	ret = SORecv(sockP->sockDesc, (char*)inBuff, inBuffSize, peek ? SO_MSG_PEEK : 0);

	if( ret > 0 )
	{
		outRcvSize = ret;
		return TRUE;
	}
	else if (ret == SO_EWOULDBLOCK || ret == SO_EAGAIN) {
		return TRUE;
	}
	else // ( ret < 0 )
	{
		return FALSE;
	}
}


bool
nv::net::Send		(	int					inSockId,
						uint32		&		outSndSize,
						pvoid				inBuff,
						uint32				inSize			)
{
	outSndSize = 0;

	State state = GetState(inSockId);
	if (state != S_CONNECTED )	return FALSE;
	if ( !inSize )				return TRUE;
	if ( !inBuff )				return FALSE;

	Sock * sockP = & sockA[inSockId];


	int32 MTUBSize = GetMTUBSize(inSockId);
	if( MTUBSize>0 && int(inSize)>MTUBSize )
		inSize = MTUBSize;

	int ret = SOSend( sockP->sockDesc, (pcstr)inBuff, inSize, 0 );
	if( ret >= 0 ){
		outSndSize = ret;
		return TRUE;
	}
	else if (ret == SO_EWOULDBLOCK || ret == SO_EAGAIN) {
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

bool
nv::net::ReceiveWait	(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize		)
{
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	uint8* buffer   = (uint8*) inBuffer;
	while( inBSize ) {
		uint32 rbs = 0;
		if( !Receive(inSockId,rbs,buffer,inBSize) )
			return FALSE;
		if( rbs==0 ) {
			clock::GetTime( &t1 );
			if( (t1-t0)*1000.f > msTimeout )
				return FALSE;
//			Sleep( 1 );
		} else {
			buffer   += rbs;
			inBSize  -= rbs;
			clock::GetTime( &t0 );
		}
	}
	return ( GetState(inSockId) == S_CONNECTED );
}

bool
nv::net::SendWait		(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize			)
{
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	uint8* buffer   = (uint8*) inBuffer;
	while( inBSize ) {
		uint32 sbs = 0;
		if( !Send(inSockId,sbs,buffer,inBSize) )
			return FALSE;
		if( sbs==0 ) {
			clock::GetTime( &t1 );
			if( (t1-t0)*1000.f > msTimeout )
				return FALSE;
//			Sleep( 1 );
		} else {
			buffer   += sbs;
			inBSize  -= sbs;
			clock::GetTime( &t0 );
		}
	}
	return ( GetState(inSockId) == S_CONNECTED );
}

bool
nv::net::GetPeerAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort		)
{
	State state = GetState(inSockId);
	if (state != S_CONNECTED ) return FALSE;

	Sock * sockP = & sockA[inSockId];


	SOSockAddrIn addr;
	Memset(&addr,0,sizeof (SOSockAddrIn));
	addr.len = sizeof(SOSockAddrIn);
	if (!SOGetPeerName(sockP->sockDesc, &addr))
	{
		NV_ASSERT(addr.family == SO_PF_INET);
		static char buff[16];
		if( outAddr )	*outAddr = (pstr) SOInetNtoP( SO_PF_INET, &addr.addr, buff, 16 );
		if( outPort )	*outPort = SONtoHs( addr.port );
		return TRUE;
	}
	return FALSE;
}


bool
nv::net::GetSockAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort			)
{
	State state = GetState(inSockId);
	if (state < S_CLOSED ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	SOSockAddrIn addr;
	Memset(&addr,0,sizeof (SOSockAddrIn));
	addr.len = sizeof(SOSockAddrIn);
	
	if (!SOGetSockName(sockP->sockDesc, &addr))
	{
		NV_ASSERT(addr.family == SO_PF_INET);
		static char buff[16];
		if( outAddr )	*outAddr = (pstr) SOInetNtoP( SO_PF_INET, &addr.addr, buff, 16 );
		if( outPort )	*outPort = SONtoHs( addr.port );
		return TRUE;
	}
	return FALSE;
}


bool
nv::net::IsRcvPending	(	int			inSockId,
							uint		inBSize		)
{
	if (GetState(inSockId) != S_CONNECTED )
		return FALSE;

	Sock * sockP = & sockA[inSockId];
	if( inBSize > 1024 )
		return FALSE;
	char tmp[1024];
	int ret = SORecv( sockP->sockDesc, tmp, inBSize, SO_MSG_PEEK );
	return ( ret>0 && ret>=(int)inBSize );
}


bool
nv::net::IsClientPending		(	int			inSockId		)
{
	State state = GetState(inSockId);
	if (state != S_LISTENING ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	SOPollFD poll;
	poll.fd 		= sockP->sockDesc;
	poll.events 	= SO_POLLIN | SO_POLLOUT;
	poll.revents 	= 0;

	if ( SOPoll(&poll,1,0) > 0 )
		return TRUE;
	return FALSE;
}


int32
nv::net::GetMTUBSize	(	int			inSockId		)
{
	State state = GetState(inSockId);
	if (state != S_LISTENING ) return FALSE;

	Sock * sockP = & sockA[inSockId];

	if( sockP->sockDesc < 0 )
		return -1;
	
	long mtu ;
	IPGetMtu (NULL , &mtu );
	return int32(mtu);
}

nv::net::State
nv::net::GetState		(	int			inSockId		)
{
	if (inSockId < 0 )					return S_INVALID;
	if (inSockId >= int(sockA.size()))	return S_INVALID;

	Sock * sockP = & sockA[inSockId];	

	SOPollFD poll;
	poll.fd 		= sockP->sockDesc;
	poll.events 	= SO_POLLIN | SO_POLLOUT;
	poll.revents 	= 0;
		
	if( sockP->state == S_CONNECTING) {
		if ( SOPoll(&poll,1,0) > 0 ) {
			if ((poll.revents & SO_POLLHUP) ||
				(poll.revents & SO_POLLERR) ) {
				net::Close( inSockId );
				return sockP->state;
			}
			if ((poll.revents & SO_POLLWRNORM) 	||
				(poll.revents & SO_POLLWRBAND)	) {
				sockP->state = S_CONNECTED;
			}
		}
		else {
			net::Close( inSockId );
			return sockP->state;
		}
		
		char tmp[8];
		int ret = SORecv( sockP->sockDesc, tmp,8, SO_MSG_PEEK );
		if (ret == SO_ENOTCONN 	||
			ret == SO_EINVAL 	||
			ret == SO_EOPNOTSUPP||
			ret == SO_ETIMEDOUT	||
			ret == SO_EBADF		){
			
			net::Close( inSockId );
			return sockP->state;
		}		
	}
	else if (sockP->state == S_CONNECTED)
	{
		if ( SOPoll(&poll,1,0) > 0 ) {
			if ((poll.revents & SO_POLLHUP) ||
				(poll.revents & SO_POLLERR) ) {
				net::Close( inSockId );
				return sockP->state;
			}
		}
	}
	return sockP->state;
}

nv::net::Type
nv::net::GetType		(	int			inSockId		)
{
	State state = GetState(inSockId);
	if (state < S_CLOSED ) return T_UNDEF;

	Sock * sockP = & sockA[inSockId];
	return sockP->type;
}



#else 


#include <Nova.h>
using namespace nv;


//#define		NO_NETWORK
#define		DEF_WAIT_TIMEOUT		3000


namespace
{



}



bool
nvcore_Net_Init()
{
return FALSE;
}



void
nvcore_Net_Update()
{
	//
}


void
nvcore_Net_Shut()
{

}



uint
nv::net::GetHostAddrCpt		(	pcstr		inName		)
{

	return 0;
}


bool
nv::net::GetHostAddr		(	pcstr				inName,
								uint				inAddrNo,
								pstr				outAddr			)
{

			return FALSE;

}


int
nv::net::Create	(	Type		inType		)
{

		return -1;

}


void
nv::net::Release	(	int			inSockId	)
{

}


bool
nv::net::Open		(	int			inSockId,
						uint16		inPort,
						pcstr		inAddr		)
{

	return FALSE;
}


void
nv::net::Close		(	int			inSockId	)
{

}


bool
nv::net::Listen	(	int			inSockId,
					uint32		inPendingClients	)
{

	return false;
}


bool
nv::net::Accept	(	int			inSockId,
					int	&		outSockId	)
{

	return FALSE;
}


bool
nv::net::Connect	(	int			inSockId,
						pcstr		inAddr,
						uint16		inPort		)
{

	return FALSE;	
}

bool
nv::net::ConnectWait	(	int					inSockId,
							uint				inMsTimeout,
							pcstr				inAddr,
							uint16				inPort		)
{
return FALSE;
}

bool
nv::net::Receive	(	int					inSockId,
						uint32		&		outRcvSize,
						pvoid				inBuff,
						uint32				inBuffSize,
						bool				inPeek		)
{

		return FALSE;

}


bool
nv::net::Send		(	int					inSockId,
						uint32		&		outSndSize,
						pvoid				inBuff,
						uint32				inSize			)
{

		return FALSE;
}

bool
nv::net::ReceiveWait	(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize		)
{

	return FALSE;
}

bool
nv::net::SendWait		(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize			)
{

	return FALSE;
}

bool
nv::net::GetPeerAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort		)
{

	return FALSE;
}


bool
nv::net::GetSockAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort			)
{
	return FALSE;

}


bool
nv::net::IsRcvPending	(	int			inSockId,
							uint		inBSize		)
{

		return FALSE;

}


bool
nv::net::IsClientPending		(	int			inSockId		)
{
	return FALSE;
}


int32
nv::net::GetMTUBSize	(	int			inSockId		)
{
	return 0;
}

nv::net::State
nv::net::GetState		(	int			inSockId		)
{
	return S_INVALID;
}

nv::net::Type
nv::net::GetType		(	int			inSockId		)
{
	return T_UNDEF;
}

#endif 
