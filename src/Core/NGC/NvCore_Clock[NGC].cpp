/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <dolphin/os.h>
#include <Nova.h>



namespace
{

	OSTime	base_tm;
	float	base_ts;

}


bool
nvcore_Clock_Init()
{
	base_ts = 0.f;
	base_tm = OSGetTime();
	return TRUE;
}


void
nvcore_Clock_Update()
{

}


void
nvcore_Clock_Shut()
{
	//
}


void
nv::clock::GetTime(	Time *		outTime		)
{
	if (!outTime)	return;

	OSTime tm = OSGetTime();
	NV_ASSERT( tm >= base_tm );

	OSTime dtm = tm - base_tm;
	while( dtm >= OS_TIMER_CLOCK ) {
		base_ts += 1.f;
		base_tm += OS_TIMER_CLOCK;
		dtm     -= OS_TIMER_CLOCK;
	}

	const float ootc = 1.f / float(OS_TIMER_CLOCK);
	int idtm = int( tm - base_tm );
	NV_ASSERT( idtm < OS_TIMER_CLOCK );
	NV_ASSERT( OSTime(idtm) == (tm-base_tm) );

	outTime->ipart = base_ts;
	outTime->fpart = float(idtm) * ootc;
}



