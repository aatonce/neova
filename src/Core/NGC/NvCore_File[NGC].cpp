/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <dolphin/os.h>
#include <stdio.h>
using namespace nv;



#if defined( _NVCOMP_DEVKIT ) && !defined(RVL)
	#define DVDETH
	#include <dolphin/dvdeth.h>	
#else 
	#include <dolphin/dvd.h>
#endif



//#define DBG_SYNC_READ

#define DVD_MAX_SEEK_TIME			(500.f/1000.f)		// 500 ms
#define DVD_BYTE_PER_SEC			(150*1024*20)		// 20x
#define DVD_MAX_LOAD_TIME(inBSize)	( float(DVD_MAX_SEEK_TIME) + float(int(inBSize)) / float(DVD_BYTE_PER_SEC) )





namespace
{

	enum ReadState
	{
		RS_IDLE 		= 0,
		RS_READING		= 1,
		RS_STALLING		= 2,
		RS_SUCCESS		= 3,
		RS_FAILED		= 4,
		RS_STALLED		= 5,
	};


	enum CheckState
	{
		CS_IDLE			= 0,
		CS_BUSY			= 1,
		CS_CANCELING	= 2,
		CS_CANCELED		= 3,
		CS_IDENTIFIED	= 4,
		CS_UNIDENTIFIED	= 5,
	};



	bool						fileIsInit	=	FALSE;

	DVDFileInfo					fileInf;			
	bool						fileIsOpened;

	int32						lastDVDError;

	volatile ReadState 			async_state;
	volatile uint				async_bsize;
	clock::Time					async_stime;

	volatile CheckState			check_state;
	clock::Time					check_stime;
	ALIGNED_IO( byte, check_buffer[1024] );




	void check_callback( long result, DVDFileInfo* fileInfo )
	{
		if( result == DVD_RESULT_CANCELED )
		{
			check_state = CS_CANCELED;
		}
		else
		{
			check_state = (result==32) ? CS_IDENTIFIED : CS_UNIDENTIFIED;
		}
	}



	void read_callback( long result, DVDFileInfo* fileInfo )
	{
		if( result == DVD_RESULT_CANCELED )
		{
			async_state = RS_STALLED;
		}
		else
		{
			async_state = (result==async_bsize) ? RS_SUCCESS : RS_FAILED;
		}
	}



	nv::file::Status DVDtoNvError()
	{
		switch (lastDVDError )
		{
		case DVD_STATE_MOTOR_STOPPED:
		case DVD_STATE_FATAL_ERROR	:
		 	return nv::file::FS_FATAL;
		
		case DVD_STATE_CANCELED:
		case DVD_STATE_END:
		 	return nv::file::FS_OK;

		case DVD_STATE_BUSY:
		case DVD_STATE_WAITING:
		 	return nv::file::FS_BUSY;
		
		case DVD_STATE_NO_DISK: 	
		 	return nv::file::FS_NO_DISK;	 			 			 			 	
		
		case DVD_STATE_WRONG_DISK:
		 	return nv::file::FS_WRONG_DISK;

		case DVD_STATE_RETRY:
		 	return nv::file::FS_RETRY;
		}
		return nv::file::FS_OK;
	}




#ifdef DVDETH

	char* 	dvdeth_fstBuffer	= 	NULL;


	template < typename T >
	bool GetScanf(	int		inNbArg,
					pcstr	inStr,
					pcstr	inFmt,
					T*		outA	)
	{
		int va[16];
		int nb = sscanf(	inStr, inFmt,
							va+0, va+1, va+2, va+3,
							va+4, va+5, va+6, va+7,
							va+8, va+9, va+10,va+11,
							va+12,va+13,va+14,va+15	);
		if( nb != inNbArg )
			return FALSE;
		for( int i = 0 ; i < nb ; i++ )
			outA[i] = (T) va[i];
		return TRUE;
	}


	// returns :
	// -1 : dvdeth available but fails to init !
	//  0 : no dvdeth available
	//  1 : dvdeth success

	int DVDEth_Init		(	)
	{
		dvdeth_fstBuffer = NULL;

		bool dvdeth = FALSE;
		if( !core::GetParameter(core::PR_NGC_DVDETH,&dvdeth) || !dvdeth )
			return 0;

		uint	fstSize;
		pcstr 	serverInf, clientInf;
		if( !core::GetParameter(core::PR_NGC_DVDETH_SERVER,&serverInf) || !serverInf )		return 0;
		if( !core::GetParameter(core::PR_NGC_DVDETH_CLIENT,&clientInf) || !clientInf )		return 0;
		if( !core::GetParameter(core::PR_NGC_DVDETH_FSFMAXBSIZE,&fstSize) || !fstSize )		return 0;

		uint 	serverAddr[5];		// addr,port
		uchar 	serverAddr8[5];		// addr,port
		uchar 	clientAddr8[12];	// addr,netmask,gateway

		if( !GetScanf<uint>(5,serverInf,"%d.%d.%d.%d:%d",serverAddr) )							return FALSE;
		if( !GetScanf<uchar>(5,serverInf,"%d.%d.%d.%d:%d",serverAddr8) )						return FALSE;
		if( !GetScanf<uchar>(12,clientInf,"%d.%d.%d.%d,%d.%d.%d.%d,%d.%d.%d.%d",clientAddr8) )	return FALSE;

	    dvdeth_fstBuffer = (char *) NvMalloc(fstSize);
	    if( !dvdeth_fstBuffer )
	    	return -1;

		DVDEthInit( clientAddr8, clientAddr8+4, clientAddr8+8 );
		DVDLowInit( serverAddr8, serverAddr[4] );

	    if(!DVDFstInit(dvdeth_fstBuffer,fstSize) )
	    	return -1;			    

	    if( !DVDFstRefresh() )
	    	return -1;

	    return 1;
	}


	void DVDEth_Shut		(	)
	{
		if( dvdeth_fstBuffer ) {
			DVDEthShutdown();
			NvFree( dvdeth_fstBuffer );
			dvdeth_fstBuffer = NULL;
		}
	}


#endif


	bool DVD_Init()
	{
	#ifdef DVDETH
		
		int res = DVDEth_Init();
		if( res < 0 )	return FALSE;	// dvdeth fails to init !
		if( res == 1 )	return TRUE;	// dvdeth success
		
	#endif

		DVDInit();

	return TRUE;
	}


	void DVD_Shut()
	{
	#ifdef DVDETH

		DVDEth_Shut();

	#endif
	}


	bool DVD_IsEth()
	{
	#ifdef DVDETH

		return ( dvdeth_fstBuffer != NULL );

	#else

		return FALSE;

	#endif
	}


	struct dirs_t
	{
	    struct dirs_t*  next;       // must be first
	    DVDDirEntry     dirEntry;
	};
	typedef struct dirs_t       dirs;

}





void nvcore_File_io_Wii_States (	int&	out_read_state,
									int&	out_check_state,
									int&	out_dvd_error	)
{
	out_read_state  = int( async_state );
	out_check_state = int( check_state );
	out_dvd_error   = int( lastDVDError );
}




bool
nvcore_File_io_Init()
{
	if( fileIsInit )
		return TRUE;

	if( !DVD_Init() )
		return FALSE;

//	DVDSetAutoFatalMessaging(TRUE);

	fileIsInit 		= TRUE;
	fileIsOpened	= FALSE;
	async_state 	= RS_IDLE;
	async_bsize		= 0;
	check_state		= CS_IDLE;
	lastDVDError	= DVD_STATE_END;

	return TRUE;	
}





void
nvcore_File_io_Update ( )
{
	if( fileIsInit && fileIsOpened )
	{

		int errcode = DVDGetFileInfoStatus( &fileInf );

		bool inprogress = (errcode==DVD_STATE_END || errcode==DVD_STATE_BUSY || errcode==DVD_STATE_WAITING);
		bool inerror    = (!inprogress) && (errcode!=DVD_STATE_CANCELED);


		if( async_state == RS_READING )
		{
			// elapsed time
			clock::Time t;
			clock::GetTime( &t );
			float etime = t - async_stime;
			float maxtime = DVD_MAX_LOAD_TIME( async_bsize );

			if( errcode == DVD_STATE_BUSY )
			{
				if( etime > maxtime )
				{
					// stalling (souvent � cause d'un wrong disk) !
					lastDVDError = DVD_STATE_WRONG_DISK;
					async_state = RS_STALLING;
					check_state = CS_IDLE;
					DVDCancelAsync( &fileInf.cb, NULL );
				}
				else
				{
					// load in progress ...
					lastDVDError = errcode;
				}
			}
			else if( inerror )
			{
				// error in loading since 0.2s ?
				if( float(etime) > 0.2f )
				{
					lastDVDError = errcode;
					async_state = RS_STALLING;
					check_state = CS_IDLE;
					DVDCancelAsync( &fileInf.cb, NULL );
				}
			}
		}


		else if( async_state == RS_SUCCESS )
		{
			lastDVDError = errcode;
		}



		else if( async_state == RS_FAILED )
		{
			if( inerror )
				lastDVDError = errcode;		// don't store DVD_STATE_CANCELED
		}


		else if( async_state == RS_STALLED )
		{
			if( check_state == CS_IDLE )
			{
				check_state = CS_BUSY;
				DVDReadAsync( &fileInf, check_buffer, 32, 0, (DVDCallback)&check_callback );
				clock::GetTime( &check_stime );
			}

			else if( check_state == CS_BUSY )
			{
				// elapsed time
				clock::Time t;
				clock::GetTime( &t );
				float etime = t - check_stime;

				// error in checking since 0.2s ?
				if( inerror && float(etime)>0.2 )
				{
					lastDVDError = errcode;
					check_state = CS_CANCELING;
					DVDCancelAsync( &fileInf.cb, NULL );
				}
			}

			else if( check_state == CS_IDENTIFIED )
			{
				// exit from the stalled state !
				lastDVDError = errcode;
				async_state = RS_IDLE;
			}

			else // CS_UNIDENTIFIED or CS_CANCELED
			{
				// restart a check cycle
				check_state = CS_IDLE;
			}
		}

	}
}





bool
nvcore_File_io_IsOpened		(		)
{
	NV_ASSERT_RETURN_MTH(FALSE,fileIsInit);	
	return fileIsOpened;
}


bool
nvcore_File_io_IsReady		(		)
{
	nvcore_File_io_Update();

	if( fileIsInit && fileIsOpened )
	{
		if( async_state == RS_READING )
			return FALSE;

		if( async_state == RS_STALLING )
			return FALSE;
	}

	return TRUE;
}


void
nvcore_File_io_Sync	(	)
{
	while( !nvcore_File_io_IsReady() )
	{
		nvcore_File_io_Update();
	}
}





bool
nvcore_File_io_Close	(	)
{
	nvcore_File_io_Sync();

	if( fileIsInit && fileIsOpened )
	{
		if( DVDClose(&fileInf) )
		{
			fileIsOpened = FALSE;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
}



bool
nvcore_File_io_IsSuccess	(		)
{
	nvcore_File_io_Update();

	if( fileIsInit && fileIsOpened )
	{
		return (async_state == RS_SUCCESS);
	}
	else
	{
		return TRUE;
	}
}



nv::file::Status
nvcore_File_io_GetStatus	(		)
{
	nvcore_File_io_Update();

	if( fileIsInit && fileIsOpened )
	{
		return DVDtoNvError();
	}
	else
	{
		return nv::file::FS_NOT_AVAILABLE;
	}
}



bool
nvcore_File_io_Open	(	pcstr	inFilename,	 uint32&	outBSize	)
{
	nvcore_File_io_Update();

	if( !fileIsInit || fileIsOpened )
		return FALSE;

	if( !inFilename || Strlen(inFilename)==0 )
		return FALSE;

	// safe sync
	nvcore_File_io_Sync();

	// Check if file exist, because DVDOpen print message if it doesn't exit !
	if( DVDConvertPathToEntrynum(inFilename) < 0)
		return FALSE;

	if( !DVDOpen(inFilename,&fileInf) )
		return FALSE;

	outBSize = DVDGetLength(&fileInf) ;
	
	if ( outBSize <= 0 )
	{
		nvcore_File_io_Close();
		return FALSE ;
	}

	fileIsOpened	= TRUE;
	async_state 	= RS_IDLE;
	async_bsize		= 0;

	return TRUE;
}


bool
nvcore_File_io_Read		(	uint		inSysFlags,
							pvoid		inBufferPtr,
							uint32		inBSize,
							uint32		inBOffset0,
							uint32		inBOffset1,
							uint32		inBOffset2,
							uint32		inBOffset3	)
{
	nvcore_File_io_Update();


	if( !fileIsInit || !fileIsOpened )
		return FALSE;


	// busy ?
	if( async_state==RS_READING || async_state==RS_STALLING )
		return FALSE;


	// stalled ?
	if( async_state == RS_STALLED )
		return FALSE;


	// Not in use or invalid call !
	NV_ASSERT( (inBSize&0x1F) == 0 );
	if(		!inBufferPtr
		||	(uint(inBufferPtr)&0x1F)!=0		// must be 32-bytes aligned !
		||	(inBSize&0x1F)			!=0 	// must be 32-bytes aligned !
		||	(inBOffset0&0x3)		!=0)	// must be a multiple of 4
		return FALSE;

	if( inBSize == 0 )
		return TRUE;


#ifdef DBG_SYNC_READ 

	{
		async_state = RS_IDLE;
		int32 readen = DVDRead( &fileInf, inBufferPtr, inBSize, inBOffset0 );
		return ( readen==inBSize );
	}

#else

	{
		async_bsize = inBSize;
		async_state = RS_READING;

		BOOL load_started = DVDReadAsync( &fileInf, inBufferPtr, inBSize, inBOffset0, (DVDCallback)&read_callback );
		if( load_started )
		{
			clock::GetTime( &async_stime );
			return TRUE;
		}
		else
		{
			async_state = RS_IDLE;
			return FALSE;
		}
	}

#endif
}



bool
nvcore_File_io_DumpFrom	(	pcstr	inFilename,
							pvoid&	outBuffer,
							uint&	outBSize		)
{	
#if defined( _NVCOMP_DEVKIT )

	NV_ASSERT_RETURN_MTH(FALSE,fileIsInit);
	if( !inFilename )	return FALSE;

	DVDFileInfo fi;
	if (! DVDOpen(inFilename,&fi))
		return FALSE;
	
	uint realBSize;
	outBSize 	= DVDGetLength(&fi) ;
		
	realBSize 	= OSRoundUp32B(outBSize);	
	outBuffer 	= NvMallocA( realBSize, 32 );
	
	if( !outBuffer ) {
		DVDClose(&fi);
		return FALSE;
	}
	
	uint readen = DVDRead(&fi,outBuffer,realBSize,0);
	nvcore_File_io_Update();
	DVDClose( &fi );
	if( readen != realBSize ) {
		NvFree( outBuffer );
		outBuffer = NULL;
		return FALSE;
	}
	return TRUE;

#else

	return FALSE;

#endif
}



bool
nvcore_File_io_DumpTo	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{
#if defined( DVDETH )

	NV_ASSERT_RETURN_MTH(FALSE,fileIsInit);

	if( !DVD_IsEth() )
		return FALSE;

	if( !inFilename || ! inBuffer )
		return FALSE;

	DVDFileInfo fi;
	if (! DVDCreate(inFilename,&fi))
		return FALSE;
	uint writenen = DVDWrite(&fi,inBuffer,inBSize,0);
	nvcore_File_io_Update();
	DVDClose( &fi );
	
	return writenen == inBSize;

#else

	return FALSE;

#endif
}


bool
nvcore_File_io_Append	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{
#if defined( DVDETH )

	NV_ASSERT_RETURN_MTH(FALSE,fileIsInit);

	if( !DVD_IsEth() )
		return FALSE;

	if( !inFilename || ! inBuffer )
		return FALSE;
	
	DVDFileInfo fi;
	if (! DVDOpen(inFilename,&fi)){
		nvcore_File_io_Update();
		return FALSE;
	}
	
	uint boffset = DVDGetLength( &fi );
	uint writenen = DVDWrite(&fi,inBuffer,inBSize,boffset);
	nvcore_File_io_Update();
	DVDClose( &fi );
	
	return (writenen == inBSize);

#else

	return FALSE;

#endif
}


void
nvcore_File_io_Shut		(			)
{
	NV_ASSERT_RETURN_CAL(fileIsInit);

	DVD_Shut();
		
	fileIsInit = FALSE;
}


uint
nvcore_File_io_GetNbSubDevice	(		)
{
	return 1;
}


pcstr
nvcore_File_io_GetSubDeviceName		(	uint	inSubDevNo			)
{
	if( inSubDevNo != 0 )
		return NULL;

#if defined( DVDETH )

	if( DVD_IsEth() )
		return "DVD (eth)";

#endif

	return "DVD";
}


pcstr
nvcore_File_io_GetSubDevicePrefix	(	uint	inSubDevNo			)
{
	return NULL;
}


pcstr
nvcore_File_io_GetSubDeviceSuffix	(	uint	inSubDevNo			)
{
	return NULL;
}








