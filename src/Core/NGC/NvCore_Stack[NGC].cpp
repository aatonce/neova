/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>



#if defined( _NVCOMP_ENABLE_DBG )



#if defined(__MWERKS__)
#pragma dont_inline on
#endif




namespace
{

	// (eabi_app.pdf)
	//
	//	The PowerPC architecture defines 32 general purpose registers (GPRs) and 32 floating-point registers
	//	(FPRs). The EABI classifies registers as volatile, nonvolatile, and dedicated. Nonvolatile registers must
	//	have their original values preserved, therefore, functions modifying nonvolatile registers must restore the
	//	original values before returning to the calling function. Volatile registers do not have to be preserved
	//	across function calls.
	//	Three nonvolatile GPR's are dedicated for a specific usage, R1, R2, and R13. R1 is dedicated as the
	//	stack frame pointer (SP). R2 is dedicated for use as a base pointer (anchor) for the read-only small data
	//	area. R13 is dedicated for use as an anchor for addressing the read-write small data area. Dedicated
	//	registers should never be used for any other purpose, not even temporarily, because they may be
	//	needed by an exception handler at any time. All the PowerPC registers and their usage are described in
	//	Table 3.
	//
	//	Table 3 - PowerPC EABI register usage
	//
	//	Register			Type			Used for:
	//	R0					Volatile		Language Specific
	//	R1					Dedicated		Stack Pointer (SP)
	//	R2					Dedicated		Read-only small data area anchor
	//	R3 - R4				Volatile		Parameter passing / return values
	//	R5 - R10			Volatile		Parameter passing
	//	R11 - R12			Volatile
	//	R13					Dedicated		Read-write small data area anchor
	//	R14 - R31			Nonvolatile
	//	F0					Volatile		Language specific
	//	F1					Volatile		Parameter passing / return values
	//	F2 - F8				Volatile		Parameter passing
	//	F9 - F13			Volatile
	//	F14 - F31			Nonvolatile
	//	Fields CR2 - CR4	Nonvolatile
	//	Other CR fields		Volatile
	//	Other registers		Volatile

}



bool
nv::stack::GetFrame( Frame & outFS )
{
	Frame fs;

	asm volatile {
		stw		r1,  fs.ptr;
		lwz		r11, 0(r1);
		sub		r0,  r11, r1;
		stw		r0,  fs.bsize;
		lwz		r0,  4(r11);
		stw		r0,  fs.retAddr;
	}

	// Local-addr
	fs.locAddr = GetLocalAddr();

	// Returns the caller frame !
	return GetBackFrame( outFS, fs );
}


bool
nv::stack::GetBackFrame(	Frame &	outFS,
							Frame &	inFS	)
{
	if(		!inFS.retAddr
	 	||	!inFS.locAddr
	 	||	!inFS.ptr
	 	||	!inFS.bsize	)
		return FALSE;

	uint32 sp_top;
	core::GetParameter( core::PR_MEM_APP_STACK_ADDR, &sp_top );
	if(	uint(inFS.ptr) >= sp_top )
		return FALSE;

	uint32* call_sp_lo = (uint32*)( uint32(inFS.ptr) + inFS.bsize );
	uint32* call_sp_hi = (uint32*) call_sp_lo[0];
	if(	uint(call_sp_lo) >= sp_top || uint32(call_sp_hi) >= sp_top )
		return FALSE;

	// Build the back frame
	outFS.locAddr = inFS.retAddr;
	outFS.ptr     = (void*) call_sp_lo;
	outFS.bsize   = uint32(call_sp_hi) - uint32(call_sp_lo);
	outFS.retAddr = (void*) call_sp_hi[1];

	return TRUE;
}


pvoid
nv::stack::GetReturnAddr	(			)
{
	Frame fs, caller_fs;

	if( !GetFrame(fs) )
		return NULL;

	if( !GetBackFrame(caller_fs,fs) )
		return NULL;

	return caller_fs.retAddr;
}


pvoid
nv::stack::GetLocalAddr		(						)
{
	register uint32 res;
	asm volatile {	mflr res; }
	return (void*)res;
}


uint
nv::stack::OutputCallstack	(	Frame&		inFromF,
								pcstr		inLabel		)
{
	// Output call-stack
	uint32 appStartAddr;
	core::GetParameter( core::PR_MEM_APP_ADDR, &appStartAddr );

	Printf( "<Neova> call-stack :\n" );
	Frame sf    = inFromF;
	uint  depth = 0;
	for( ;; ) {
		Printf( "%s[%d] %08X .t=%08X sp=%08X\n",
			inLabel ? inLabel : "",
			depth++,
			uint32(sf.locAddr),
			uint32(sf.locAddr)-appStartAddr,
			uint32(sf.ptr)	);
		if( !GetBackFrame(sf,sf) )
			break;
	}

	return depth;
}





#else	// _NVCOMP_ENABLE_DBG





bool
nv::stack::GetFrame		(	Frame	&	outF		)
{
	return FALSE;
}

bool
nv::stack::GetBackFrame	(	Frame	&	outBackF,
							Frame	&	inF			)
{
	return FALSE;
}

pvoid
nv::stack::GetReturnAddr	(		)
{
	return NULL;
}

pvoid
nv::stack::GetLocalAddr		(		)
{
	return NULL;
}

uint
nv::stack::OutputCallstack	(	Frame&		inFromF,
								pcstr		inLabel		)
{
	return 0;
}


#endif // _NVCOMP_ENABLE_DBG



