/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/
#define _NvCore_Math_NOT_GLOBAL_
#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>

using namespace nv;

#include <dolphin/pad.h>

#ifdef RVL
#include <revolution/kpad.h>
#include <revolution/mem.h>
#endif
										  		
#ifdef RVL
	// extern variable used in KernelCtrl
	KPADStatus			kpads		[ WPAD_MAX_CONTROLLERS ][ KPAD_MAX_READ_BUFS ] 	;
	int32				kpad_reads 	[ WPAD_MAX_CONTROLLERS ]						;

	void ctrl_InitSpeaker	();
	void ctrl_ShutSpeaker	();
	void ctrl_UpdateSpeaker	();
#else 
	void ctrl_InitSpeaker	(){}
	void ctrl_ShutSpeaker	(){}
	void ctrl_UpdateSpeaker	(){}
#endif 	// RVL	


namespace {

// General ---------------------------------------------------------------------------------------------
	
	bool 				isOpened 	= FALSE;	
	float				deadzA		[ ctrl::TP_MAX ][ ctrl::BT_MAX ][ 2 ];	// lo/hi	

// -----------------------------------------------------------------------------------------------------	
// -----------------------------------------------------------------------------------------------------												
// NGC data	--------------------------------------------------------------------------------------------
	nv::ctrl::Status	statusNGC	[PAD_MAX_CONTROLLERS];
	bool				padOKNGC	[PAD_MAX_CONTROLLERS];
// NGC functions ---------------------------------------------------------------------------------------

	bool
	_InitNgc()
	{
	
		NV_COMPILE_TIME_ASSERT( 	PAD_CHAN0_BIT == (0x80000000 >> 0) && PAD_CHAN1_BIT == (0x80000000 >> 1) 	&& 
									PAD_CHAN2_BIT == (0x80000000 >> 2) && PAD_CHAN3_BIT == (0x80000000 >> 3) 	);
		uint32 analogMode;	
		core::GetParameter(core::PR_NGC_PAD_ANALOGMODE,&analogMode);

		PADSetAnalogMode(analogMode);
		PADInit();

		return TRUE;
	}

	bool 
	_ShutNgc()
	{
		PADReset(PAD_CHAN0_BIT | PAD_CHAN1_BIT | PAD_CHAN2_BIT | PAD_CHAN3_BIT );
		return TRUE;
	}


	void
	_UpdateNGC()
	{
		if (!isOpened) return ;
					
		PADStatus ngcStatus[PAD_MAX_CONTROLLERS];
		PADRead(ngcStatus);
		
		const float factorStickPos 	= 1.0f/127.0f;
		const float factorStickNeg 	= 1.0f/128.0f;	
		const float factorLR_AB		= 1.0f/255.0f;
				
		uint32 resetPadMask = 0;	
		for (uint i = 0 ; i < PAD_MAX_CONTROLLERS ; ++i ) {
			Zero(statusNGC[i]);
			if (ngcStatus[i].err == PAD_ERR_NONE) {			
						
				padOKNGC[i] = TRUE;
				statusNGC[i].type = PADIsBarrel(i)?ctrl::TP_TOMTOM : ctrl::TP_PADDLE;

				float stickX 		= ngcStatus[i].stickX 		* ((ngcStatus[i].stickX<0)?factorStickNeg:factorStickPos);
				float stickY 		= ngcStatus[i].stickY 		* ((ngcStatus[i].stickY<0)?factorStickNeg:factorStickPos);
				float substickX 	= ngcStatus[i].substickX 	* ((ngcStatus[i].substickX<0)?factorStickNeg:factorStickPos);
				float substickY 	= ngcStatus[i].substickY	* ((ngcStatus[i].substickY<0)?factorStickNeg:factorStickPos);
				float anL			= ngcStatus[i].triggerLeft  * factorLR_AB;
				float anR			= ngcStatus[i].triggerRight * factorLR_AB;
				float anM			= (ngcStatus[i].button & PAD_BUTTON_A)?1.0f:0.0f;

				statusNGC[i].button[ctrl::BT_DIR1X]		=  stickX;											 // Analog stick DirX
				statusNGC[i].button[ctrl::BT_DIR1Y]		= -stickY;											 // Analog stick DirY
				statusNGC[i].button[ctrl::BT_DIR2X]		=  substickX;					 					 // Analog substick C DirX
				statusNGC[i].button[ctrl::BT_DIR2Y]		= -substickY;										 // Analog substick C DirY
				statusNGC[i].button[ctrl::BT_DIR3right]= (ngcStatus[i].button & PAD_BUTTON_RIGHT)?1.0f:0.0f;// Digital right
				statusNGC[i].button[ctrl::BT_DIR3left]	= (ngcStatus[i].button & PAD_BUTTON_LEFT )?1.0f:0.0f;// Digital left
				statusNGC[i].button[ctrl::BT_DIR3up]	= (ngcStatus[i].button & PAD_BUTTON_UP   )?1.0f:0.0f;// Digital up
				statusNGC[i].button[ctrl::BT_DIR3down]	= (ngcStatus[i].button & PAD_BUTTON_DOWN )?1.0f:0.0f;// Digital down
				statusNGC[i].button[ctrl::BT_BUTTON0]	= (ngcStatus[i].button & PAD_BUTTON_Y    )?1.0f:0.0f;// Y
				statusNGC[i].button[ctrl::BT_BUTTON1]	= (ngcStatus[i].button & PAD_BUTTON_X    )?1.0f:0.0f;// X
				statusNGC[i].button[ctrl::BT_BUTTON2]	= (ngcStatus[i].button & PAD_BUTTON_A    )?1.0f:0.0f;// A
				statusNGC[i].button[ctrl::BT_BUTTON3]	= (ngcStatus[i].button & PAD_BUTTON_B    )?1.0f:0.0f;// B
				statusNGC[i].button[ctrl::BT_BUTTON4]	= anL;												 // L
				statusNGC[i].button[ctrl::BT_BUTTON5]	= anR;												 // R
				statusNGC[i].button[ctrl::BT_BUTTON6]	= (anM>0.8f) ? anL : 0.f;							 // A+L
				statusNGC[i].button[ctrl::BT_BUTTON7]	= (anM>0.8f) ? anR : 0.f;							 // A+R
				statusNGC[i].button[ctrl::BT_BUTTON8]	= (ngcStatus[i].button & PAD_TRIGGER_Z)?1.0f:0.0f;	 // Z
				statusNGC[i].button[ctrl::BT_BUTTON9]	= (ngcStatus[i].button & PAD_BUTTON_START)?1.0f:0.0f;// Start/Pause
				statusNGC[i].button[ctrl::BT_BUTTON10]	= (ngcStatus[i].button & PAD_TRIGGER_L)?1.0f:0.0f;	 // L trigger
				statusNGC[i].button[ctrl::BT_BUTTON11]	= (ngcStatus[i].button & PAD_TRIGGER_R)?1.0f:0.0f;	 // R trigger
			}
			else if (ngcStatus[i].err == PAD_ERR_NO_CONTROLLER) {
				resetPadMask |= 0x80000000 >> i;			
				padOKNGC[i] = FALSE;
			}
		}
		if (resetPadMask)	
			PADReset(resetPadMask);
	}

	void				
	_OpenNGC	()
	{
		PADReset(PAD_CHAN0_BIT | PAD_CHAN1_BIT | PAD_CHAN2_BIT | PAD_CHAN3_BIT );
	
		for (uint i = 0 ; i < PAD_MAX_CONTROLLERS ; ++i ) {
			Zero(statusNGC[i]);		
			padOKNGC[i]			= FALSE;
		}
	}
	void
	_CloseNGC 	(	)
	{
	
	}

	uint
	_GetNbMaxNGC	(	)
	{
		return PAD_MAX_CONTROLLERS;
	}
	
	nv::ctrl::Status*
	_GetStatusNGC ( uint inCtrlNo	)
	{
		if (!isOpened) return NULL;	
	
		if ( inCtrlNo >= PAD_MAX_CONTROLLERS ) return NULL;
		if ( ! padOKNGC[inCtrlNo] ) return NULL;
		
		return & (statusNGC[inCtrlNo]);		
	}

	bool
	_SetActuatorsNGC	(uint inCtrlNo, float inLowSpeed, float	inHighSpeed	)
	{
		return FALSE;		
	}
	
// -----------------------------------------------------------------------------------------------------	
// -----------------------------------------------------------------------------------------------------												
// Wii data --------------------------------------------------------------------------------------------
#ifdef RVL
	nv::ctrl::Status	statusWii	[ WPAD_MAX_CONTROLLERS ]						;	
	bool				padOKWii	[ WPAD_MAX_CONTROLLERS ] 						;
	
// Wii functions ---------------------------------------------------------------------------------------
	
	static void 
	dpd_callback( s32 chan, s32 reason ) 
	{
		
	}

	static void 
	sampling_callback( s32 chan )
	{
	
	}

	static void* PadAlloc32(u32 size)
	{
		return nv::mem::NativeMalloc( size, 32, nv::mem::MEM_NGC_MEM2 );
	}

	static u8 PadFree32(void *addr)
	{
		nv::mem::NativeFree( addr, nv::mem::MEM_NGC_MEM2 );
		return 1;
	}
	
	void ExtractWiimoteClassicInformation(KPADStatus * inKpadStatus , ctrl::Status * outStatus)
	{
		if (!inKpadStatus || !outStatus) return ;	
		
		outStatus->button[ctrl::BT_DIR1X]		=  inKpadStatus->ex_status.cl.lstick.x;										// Analog stick DirX
		outStatus->button[ctrl::BT_DIR1Y]		=  -inKpadStatus->ex_status.cl.lstick.y;										// Analog stick DirY
		outStatus->button[ctrl::BT_DIR2X]		=  inKpadStatus->ex_status.cl.rstick.x;					 					// Analog substick C DirX
		outStatus->button[ctrl::BT_DIR2Y]		=  -inKpadStatus->ex_status.cl.rstick.y;										// Analog substick C DirY
		outStatus->button[ctrl::BT_DIR3right] 	= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_RIGHT	) ||
												   (inKpadStatus->hold & KPAD_BUTTON_RIGHT	))?1.0f:0.0f;					// Digital right
		outStatus->button[ctrl::BT_DIR3left]	= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_LEFT 	) ||
												   (inKpadStatus->hold & KPAD_BUTTON_RIGHT	))?1.0f:0.0f;					// Digital left
		outStatus->button[ctrl::BT_DIR3up]		= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_UP   	) ||
												   (inKpadStatus->hold & KPAD_BUTTON_RIGHT	))?1.0f:0.0f;					// Digital up
		outStatus->button[ctrl::BT_DIR3down]	= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_DOWN 	) ||
		      									   (inKpadStatus->hold & KPAD_BUTTON_RIGHT	))?1.0f:0.0f;					// Digital down

		outStatus->button[ctrl::BT_BUTTON0]		= (inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_X    	)?1.0f:0.0f;	// X
		outStatus->button[ctrl::BT_BUTTON1]		= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_A 	)	||
												   (inKpadStatus->hold & KPAD_BUTTON_A  					))?1.0f:0.0f;	// A
		outStatus->button[ctrl::BT_BUTTON2]		= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_B 	) 	||
												   (inKpadStatus->hold & KPAD_BUTTON_B						))?1.0f:0.0f;	// B
		outStatus->button[ctrl::BT_BUTTON3]		= (inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_Y 		)?1.0f:0.0f;	// Y

		outStatus->button[ctrl::BT_BUTTON4]		=  inKpadStatus->ex_status.cl.ltrigger;										// analog L
		outStatus->button[ctrl::BT_BUTTON5]		=  inKpadStatus->ex_status.cl.rtrigger;										// analog R
		outStatus->button[ctrl::BT_BUTTON6]		= (inKpadStatus->ex_status.cl.hold & KPAD_CL_TRIGGER_ZL 	)?1.0f:0.0f;	// ZL
		outStatus->button[ctrl::BT_BUTTON7]		= (inKpadStatus->ex_status.cl.hold & KPAD_CL_TRIGGER_ZR 	)?1.0f:0.0f;	// ZR

		outStatus->button[ctrl::BT_BUTTON8]		= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_MINUS ) || 
												  (inKpadStatus->hold & KPAD_BUTTON_MINUS					 ))?1.0f:0.0f;	// -
		outStatus->button[ctrl::BT_BUTTON9]		= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_PLUS  ) || 
												  (inKpadStatus->hold & KPAD_BUTTON_MINUS					 ))?1.0f:0.0f;	// +
		outStatus->button[ctrl::BT_BUTTON10]	= ((inKpadStatus->ex_status.cl.hold & KPAD_CL_BUTTON_HOME  ) ||
												  (inKpadStatus->hold & KPAD_BUTTON_MINUS  				 	 ))?1.0f:0.0f;	// Home

		outStatus->button[ctrl::BT_BUTTON11]	= (inKpadStatus->ex_status.cl.hold & KPAD_CL_TRIGGER_L 		)?1.0f:0.0f;	// digital L
		outStatus->button[ctrl::BT_BUTTON12]	= (inKpadStatus->ex_status.cl.hold & KPAD_CL_TRIGGER_R 		)?1.0f:0.0f;		// digital R
		
		outStatus->button[ctrl::BT_BUTTON13]	= (inKpadStatus->hold & KPAD_BUTTON_1						)?1.0f:0.0f;		// wiimote 1
		outStatus->button[ctrl::BT_BUTTON14]	= (inKpadStatus->hold & KPAD_BUTTON_1						)?1.0f:0.0f;		// wiimote 2
	}
	
	void ExtractWiimoteFreeStyleInformation(KPADStatus * inKpadStatus , ctrl::Status * outStatus)
	{
		if (!inKpadStatus || !outStatus) return ;
		outStatus->button[ctrl::BT_DIR1X]		=  inKpadStatus->ex_status.fs.stick.x;									// Analog stick DirX
		outStatus->button[ctrl::BT_DIR1Y]		=  inKpadStatus->ex_status.fs.stick.y;									// Analog stick DirY
		outStatus->button[ctrl::BT_DIR2X]		=  inKpadStatus->acc.x;					 								// Analog substick C DirX
		outStatus->button[ctrl::BT_DIR2Y]		=  inKpadStatus->acc.y;													// Analog substick C DirY
		outStatus->button[ctrl::BT_DIR3right] 	= (inKpadStatus->hold & KPAD_BUTTON_RIGHT		)?1.0f:0.0f;				// Digital right
		outStatus->button[ctrl::BT_DIR3left]	= (inKpadStatus->hold & KPAD_BUTTON_LEFT 		)?1.0f:0.0f;				// Digital left
		outStatus->button[ctrl::BT_DIR3up]		= (inKpadStatus->hold & KPAD_BUTTON_UP   		)?1.0f:0.0f;				// Digital up
		outStatus->button[ctrl::BT_DIR3down]	= (inKpadStatus->hold & KPAD_BUTTON_DOWN 		)?1.0f:0.0f;				// Digital down

		outStatus->button[ctrl::BT_BUTTON0]		= (inKpadStatus->hold & KPAD_BUTTON_1   		)?1.0f:0.0f;				// 1 (cl X)
		outStatus->button[ctrl::BT_BUTTON1]		= (inKpadStatus->hold & KPAD_BUTTON_A 		)?1.0f:0.0f;				// A (cl A)
		outStatus->button[ctrl::BT_BUTTON2]		= (inKpadStatus->hold & KPAD_BUTTON_B 		)?1.0f:0.0f;				// B (cl B)
		outStatus->button[ctrl::BT_BUTTON3]		= (inKpadStatus->hold & KPAD_BUTTON_2 		)?1.0f:0.0f;				// 2 (cl Y)

		float fsAcc = inKpadStatus->ex_status.fs.acc.x;
		outStatus->button[ctrl::BT_BUTTON4]		= (fsAcc >  0.0f) ? fsAcc  : 0.0f ;										// analog L
		outStatus->button[ctrl::BT_BUTTON5]		= (fsAcc <  0.0f) ? -fsAcc : 0.0f ;										// analog R
		outStatus->button[ctrl::BT_BUTTON6]		= (inKpadStatus->hold & KPAD_BUTTON_C 	)?1.0f:0.0f;					// C (cl ZL)
		outStatus->button[ctrl::BT_BUTTON7]		= (inKpadStatus->hold & KPAD_BUTTON_Z 	)?1.0f:0.0f;					// Z (cl ZR)

		outStatus->button[ctrl::BT_BUTTON8]		= (inKpadStatus->hold & KPAD_BUTTON_MINUS )?1.0f:0.0f;					// -
		outStatus->button[ctrl::BT_BUTTON9]		= (inKpadStatus->hold & KPAD_BUTTON_PLUS 	)?1.0f:0.0f;					// +
		outStatus->button[ctrl::BT_BUTTON10]	= (inKpadStatus->hold & KPAD_BUTTON_HOME 	)?1.0f:0.0f;					// Home
	}
	
	void ExtractWiimoteCoreInformation (KPADStatus * inKpadStatus , ctrl::Status * outStatus)
	{
		if (!inKpadStatus || !outStatus) return ;
		outStatus->button[ctrl::BT_DIR1X]		=  inKpadStatus->pos.x;													// Analog stick DirX
		outStatus->button[ctrl::BT_DIR1Y]		=  inKpadStatus->pos.y;													// Analog stick DirY
		outStatus->button[ctrl::BT_DIR2X]		=  inKpadStatus->acc.x;					 								// Analog substick C DirX
		outStatus->button[ctrl::BT_DIR2Y]		=  inKpadStatus->acc.y;													// Analog substick C DirY
		outStatus->button[ctrl::BT_DIR3right] 	= (inKpadStatus->hold & KPAD_BUTTON_RIGHT		)?1.0f:0.0f;				// Digital right
		outStatus->button[ctrl::BT_DIR3left]	= (inKpadStatus->hold & KPAD_BUTTON_LEFT 		)?1.0f:0.0f;				// Digital left
		outStatus->button[ctrl::BT_DIR3up]		= (inKpadStatus->hold & KPAD_BUTTON_UP   		)?1.0f:0.0f;				// Digital up
		outStatus->button[ctrl::BT_DIR3down]	= (inKpadStatus->hold & KPAD_BUTTON_DOWN 		)?1.0f:0.0f;				// Digital down

		outStatus->button[ctrl::BT_BUTTON0]		= (inKpadStatus->hold & KPAD_BUTTON_1   		)?1.0f:0.0f;				// 1 
		outStatus->button[ctrl::BT_BUTTON1]		= (inKpadStatus->hold & KPAD_BUTTON_A 		)?1.0f:0.0f;				// A 
		outStatus->button[ctrl::BT_BUTTON2]		= (inKpadStatus->hold & KPAD_BUTTON_B 		)?1.0f:0.0f;				// B 
		outStatus->button[ctrl::BT_BUTTON3]		= (inKpadStatus->hold & KPAD_BUTTON_2 		)?1.0f:0.0f;				// 2 

		outStatus->button[ctrl::BT_BUTTON8]		= (inKpadStatus->hold & KPAD_BUTTON_MINUS )?1.0f:0.0f;					// -
		outStatus->button[ctrl::BT_BUTTON9]		= (inKpadStatus->hold & KPAD_BUTTON_PLUS 	)?1.0f:0.0f;					// +
		outStatus->button[ctrl::BT_BUTTON10]	= (inKpadStatus->hold & KPAD_BUTTON_HOME 	)?1.0f:0.0f;					// Home
	}
	
	bool 
	_InitWii()
	{
	    WPADRegisterAllocator(PadAlloc32, PadFree32);
	    KPADInit() ;		// Controller
	    KPADSetControlDpdCallback(0, dpd_callback);
    	KPADSetSamplingCallback(0, sampling_callback);
		return TRUE;
	}
	
	bool 
	_ShutWii()
	{
		return TRUE;
	}
	
	void
	_UpdateWii()
	{
		if (!isOpened) return ;
					
		for (uint i = 0 ; i < WPAD_MAX_CONTROLLERS ; ++i ) {
			kpad_reads[i] = KPADRead( i, &kpads[i][0], KPAD_MAX_READ_BUFS ) ;
			Zero(statusWii[i]);
			statusWii[i].type = ctrl::TP_COMBI;
			padOKWii[i] = FALSE;

			if ( kpad_reads[i] > 0 && kpads[0][0].wpad_err == WPAD_ERR_NONE && kpads[i][0].dev_type !=WPAD_DEV_NOT_FOUND) {
				if (kpads[i][0].dev_type == WPAD_DEV_CLASSIC) 
					ExtractWiimoteClassicInformation (&kpads[i][0] , &statusWii[i]);
				else if (kpads[i][0].dev_type == WPAD_DEV_FREESTYLE)
					ExtractWiimoteFreeStyleInformation (&kpads[i][0] , &statusWii[i]);
				else 
					ExtractWiimoteCoreInformation (&kpads[i][0] , &statusWii[i]);
				padOKWii[i] = TRUE;
			}
		}
	}
	
	void				
	_OpenWii	()
	{
	   	KPADReset();
		for (uint i = 0 ; i < WPAD_MAX_CONTROLLERS ; ++i ) {
			Zero(statusWii[i]);
			padOKNGC[i]= FALSE;
		}		   
	}
	
	void
	_CloseWii 	(	)
	{

	}

	uint
	_GetNbMaxWii	(	)
	{
		return WPAD_MAX_CONTROLLERS;
	}
	
	nv::ctrl::Status*
	_GetStatusWii
	( uint		inCtrlNo	)
	{
	
		if (!isOpened) return NULL;	
	
		if ( inCtrlNo >= WPAD_MAX_CONTROLLERS ) return NULL;
		if ( ! padOKWii[inCtrlNo] ) return NULL;
		
		return & statusWii[inCtrlNo];		
	}

	bool
	_SetActuatorsWii	(uint inCtrlNo, float inLowSpeed, float	inHighSpeed	)
	{
		return FALSE;
	}
#else	// RVL
	
	bool 
	_InitWii()
	{
		return TRUE;
	}
	
	bool 
	_ShutWii()
	{
		return TRUE;
	}
	
	void
	_UpdateWii()
	{
	
	}
	
	void				
	_OpenWii	()
	{
	
	}
	
	void
	_CloseWii 	(	)
	{

	}

	uint
	_GetNbMaxWii	(	)
	{
		return 0;
	}
	
	nv::ctrl::Status*
	_GetStatusWii
	( uint		inCtrlNo	)
	{	
		return NULL;
	}

	bool
	_SetActuatorsWii	(uint inCtrlNo, float inLowSpeed, float	inHighSpeed	)
	{
		return FALSE;
	}
#endif 	// RVL
};


bool
nvcore_Ctrl_Init()
{
	// Init default dead zone 
	for( int i = 0 ; i < ctrl::TP_MAX ; i++ ) {
		for( int j = 0 ; j < ctrl::BT_MAX ; j++ ) {
			float lo=0, hi=0;
			if( i==ctrl::TP_PADDLE )
			{
				if( j==ctrl::BT_DIR1X || j==ctrl::BT_DIR1Y ) {
					lo = 0.1171875f;
					hi = 0.6796875f;
				} else if( j==ctrl::BT_DIR2X || j==ctrl::BT_DIR2Y ) {
					lo = 0.1171875f;
					hi = 0.578125f;
				} else {
					lo = 0.0588235f;
					hi = 0.7058824f;
				}
			}
			deadzA[i][j][0] = lo;
			deadzA[i][j][1] = hi;
		}
	}
	bool initNgc = _InitNgc();
	bool initWii = _InitWii();
	
	ctrl_InitSpeaker	();	
	return  initNgc && initWii;
}


void
nvcore_Ctrl_Shut()
{
	ctrl_ShutSpeaker	();
	_ShutNgc();
	_ShutWii();
}


void
nv::ctrl::Open(		)
{
	_OpenNGC();
	_OpenWii();
	isOpened = TRUE;
}


void
nv::ctrl::Close()
{
	_CloseNGC();
	_CloseWii();
	isOpened = FALSE;
}


uint
nv::ctrl::GetNbMax	(		)
{
	return _GetNbMaxNGC( ) + _GetNbMaxWii();
}


nv::ctrl::Status*
nv::ctrl::GetStatus(	uint	inCtrlNo		)
{
	if (inCtrlNo >= GetNbMax() ) return NULL;
	
	if ( inCtrlNo < _GetNbMaxNGC() )
		return _GetStatusNGC(inCtrlNo);
	else 
		return _GetStatusWii(inCtrlNo-_GetNbMaxNGC());
}


bool
nv::ctrl::SetActuators	(	uint		inCtrlNo,
							float		inLowSpeed,
							float		inHighSpeed	)
{
	if (inCtrlNo >= GetNbMax() ) return NULL;

	if ( inCtrlNo < _GetNbMaxNGC() )
		return _SetActuatorsNGC(inCtrlNo,inLowSpeed,inHighSpeed);
	else 
		return _SetActuatorsWii(inCtrlNo-_GetNbMaxNGC(),inLowSpeed,inHighSpeed);
}

void
nvcore_Ctrl_Update()
{
	_UpdateWii();
	_UpdateNGC();
	ctrl_UpdateSpeaker	();
}

bool
nv::ctrl::GetDefDeadZone	(	Type			inType		,
								Button			inButton	,
								float*			outLo		,
								float*			outHi		)
{
	int t = int(inType);
	int b = int(inButton);
	if( t<0 || t>=TP_MAX )	return FALSE;
	if( b<0 || b>=BT_MAX )	return FALSE;
	if( outLo )		*outLo = deadzA[t][b][0];
	if( outHi )		*outHi = deadzA[t][b][1];
	return TRUE;
}

bool
nv::ctrl::SetDefDeadZone	(		Type			inType,
									Button			inButton,
									float			inLo,
									float			inHi			)
{
	float lo, hi;
	if( !GetDefDeadZone(inType,inButton,&lo,&hi) )
		return FALSE;
	if( inLo >= 0.f )	lo = nv::math::Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = nv::math::Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return FALSE;
	int t = int(inType);
	int b = int(inButton);
	deadzA[t][b][0] = lo;
	deadzA[t][b][1] = hi;
	return TRUE;
}

float
nv::ctrl::Status::Filtered	(		Button			inButton,
									float			inLo,
									float			inHi	)
{
	if( int(inButton)<0 || int(inButton)>=BT_MAX )
		return 0.f;

	float lo, hi;
	if( !GetDefDeadZone(type,inButton,&lo,&hi) )
		return 0.f;

	if( inLo >= 0.f )	lo = nv::math::Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = nv::math::Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return 0.f;

	float b  = nv::math::Clamp( button[int(inButton)], -1.f, +1.f );
	float ab = nv::math::Absf( b );
	float sb = ( b>=0.f ? 1.f : -1.f );

	if( ab <= lo )	return 0.f;
	if( ab >= hi )	return sb;
	return sb * (ab-lo)/(hi-lo);
}


