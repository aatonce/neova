/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>
using namespace nv;




namespace
{

	nv::mem::Allocator*		allocator = NULL;

}






bool
nvcore_Mem_Init(		)
{
	// Custom allocator ?
	uint32 customAllocator;
	nv::core::GetParameter( nv::core::PR_MEM_ALLOCATOR, &customAllocator );

	// Default allocator ?
	if( customAllocator )
	{
		DebugPrintf( "<Neova> Using custom DMM allocator : %08xh\n", uint32(allocator) );
		allocator = (nv::mem::Allocator*) customAllocator;
		allocator->AddRef();
	}
	else
	{
		allocator = nv::mem::CreateCoreEngineDefAllocator();
		if( !allocator ) {
			DebugPrintf( "<Neova> Default DMM allocator has failed !\n" );
			return FALSE;
		}

		DebugPrintf( "<Neova> Using default DMM allocator : %08xh\n", uint32(allocator) );
	}

	return TRUE;
}



void
nvcore_Mem_Update()
{
	NV_ASSERT( allocator );
	if( allocator )
		allocator->Update();
}



void
nvcore_Mem_Shut()
{
	NV_ASSERT( allocator );
	if( allocator ) {
		allocator->Release();
		allocator = NULL;
	}

	mem::LogClose();
}






void*
nv::mem::AllocInPlace(	void*			inPointer,
						uint32			inBSize		)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->AllocInPlace(inPointer,inBSize) : NULL;
}


void*
nv::mem::Alloc		(	uint32			inBSize,
						uint			inFlags		)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->Alloc(inBSize,inFlags) : NULL;
}


void*
nv::mem::AllocA		(	uint32			inBSize,
						uint			inBAlignment,
						uint			inFlags		)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->AllocA(inBSize,inBAlignment,inFlags) : NULL;
}


void*
nv::mem::Realloc	(	void*			inPointer,
						uint32			inBSize,
						uint			inFlags		)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->Realloc(inPointer,inBSize,inFlags) : NULL;
}




void
nv::mem::Free		(	void*			inPointer	)
{
	NV_ASSERT( allocator );
	if( allocator )
		allocator->Free( inPointer );
}



void
nv::mem::EnableProfiling		(				)
{
	NV_ASSERT( allocator );
	if( allocator )
		allocator->EnableProfiling();
}


void
nv::mem::DisableProfiling		(			)
{
	NV_ASSERT( allocator );
	if( allocator )
		allocator->DisableProfiling();
}


bool
nv::mem::IsProfiling			(			)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->IsProfiling() : FALSE;
}


uint32
nv::mem::GetTotBSize	(			)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->GetTotBSize() : 0;
}


uint32
nv::mem::GetAllocCpt		(			)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->GetAllocCpt() : 0;
}


uint32
nv::mem::GetAllocTotBSize		(			)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->GetAllocTotBSize() : 0;
}


uint32
nv::mem::GetAllocTotBSizePeak	(			)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->GetAllocTotBSizePeak() : 0;
}


uint32
nv::mem::GetAllocNb			(			)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->GetAllocNb() : 0;
}


uint32
nv::mem::GetAllocNbPeak		(			)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->GetAllocNbPeak() : 0;
}


nv::mem::RangeStatus
nv::mem::CheckRange			(	void*			inPointer,
								uint32			inBSize		)
{
	NV_ASSERT( allocator );
	return allocator ? allocator->CheckRange(inPointer,inBSize) : RS_NOT_AVAILABLE;
}




void*
nv::EngineMalloc	(	uint32				inBSize			)
{
	return mem::Alloc( inBSize, mem::AFC_ENGINE_DEFAULT );
}

void*
nv::EngineMallocA		(	uint32				inBSize,
							uint				inBAlignment	)
{
	return mem::AllocA( inBSize, inBAlignment, mem::AFC_ENGINE_DEFAULT );
}

void*
nv::EngineCalloc		(	uint				inN,
							uint32				inBSize			)
{
	void* ptr = mem::Alloc( inBSize*inN, mem::AFC_ENGINE_DEFAULT );
	if( ptr )
		Memset( ptr, 0, inBSize*inN );
	return ptr;
}

void*
nv::EngineRealloc		(	void*				inPtr,
							uint32				inBSize			)
{
	return mem::Realloc( inPtr, inBSize, mem::AFC_ENGINE_DEFAULT );
}

void
nv::EngineFree			(	void*				inPtr			)
{
	mem::Free( inPtr );
}





void*
nv::GameMalloc	(	uint32				inBSize			)
{
	return mem::Alloc( inBSize, mem::AFC_GAME_DEFAULT );
}

void*
nv::GameMallocA		(	uint32				inBSize,
						uint				inBAlignment	)
{
	return mem::AllocA( inBSize, inBAlignment, mem::AFC_GAME_DEFAULT );
}

void*
nv::GameCalloc		(	uint				inN,
						uint32				inBSize			)
{
	void* ptr = mem::Alloc( inBSize*inN, mem::AFC_GAME_DEFAULT );
	if( ptr )
		Memset( ptr, 0, inBSize*inN );
	return ptr;
}

void*
nv::GameRealloc		(	void*				inPtr,
						uint32				inBSize			)
{
	return mem::Realloc( inPtr, inBSize, mem::AFC_GAME_DEFAULT );
}

void
nv::GameFree			(	void*				inPtr			)
{
	mem::Free( inPtr );
}







void*
nv::RscMalloc	(	uint32				inBSize			)
{
	return mem::Alloc( inBSize, mem::AFC_RSC_DEFAULT );
}

void*
nv::RscMallocA		(	uint32				inBSize,
						uint				inBAlignment	)
{
	return mem::AllocA( inBSize, inBAlignment, mem::AFC_RSC_DEFAULT );
}

void*
nv::RscCalloc		(	uint				inN,
						uint32				inBSize			)
{
	void* ptr = mem::Alloc( inBSize*inN, mem::AFC_RSC_DEFAULT );
	if( ptr )
		Memset( ptr, 0, inBSize*inN );
	return ptr;
}

void*
nv::RscRealloc		(	void*				inPtr,
						uint32				inBSize			)
{
	return mem::Realloc( inPtr, inBSize, mem::AFC_RSC_DEFAULT );
}

void
nv::RscFree			(	void*				inPtr			)
{
	mem::Free( inPtr );
}



