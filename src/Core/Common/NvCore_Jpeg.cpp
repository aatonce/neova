/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include "jpegd/jpegdecoder.h"
using namespace nv;


namespace
{

	struct jpeg_decoder_mem_stream : public jpeg_decoder_stream
	{
		byte*	beg_addr;
		byte*	cur_addr;
		byte*	end_addr;

		jpeg_decoder_mem_stream()
		{
			setup( NULL, 0 );
		}

		void setup ( byte* addr, uint bsize )
		{
			if( addr && bsize )
			{
				beg_addr = addr;
				cur_addr = addr;
				end_addr = addr + bsize;
			}
			else
			{
				beg_addr = NULL;
				cur_addr = NULL;
				end_addr = NULL;
			}
		}

		int read( uchar *Pbuf, int max_bytes_to_read, bool *Peof_flag )
		{
			if( !cur_addr || !end_addr )
				return -1;
	
			if( cur_addr >= end_addr )
			{
				if( Peof_flag )
					*Peof_flag = TRUE;
				return 0;
			}

			int nb = Min( max_bytes_to_read, int(end_addr-cur_addr) );

			Memcpy( Pbuf, cur_addr, nb );
			cur_addr += nb;

			return nb;
		}

		bool iseos ( )
		{
			return (cur_addr >= end_addr);
		}
	};


	struct jpeg_decoder_mem_heap : public jpeg_decoder_heap
	{
		byte*	beg_addr;
		byte*	cur_addr;
		byte*	end_addr;

		jpeg_decoder_mem_heap ( )
		{
			init();
		}

		void init ( )
		{
			beg_addr = NULL;
			cur_addr = NULL;
			end_addr = NULL;
		}

		void shut ( )
		{
			freereserve();
		}

		void freereserve( )
		{
			if( beg_addr )
			{
				NvFree( beg_addr );
				beg_addr = NULL;
				cur_addr = NULL;
				end_addr = NULL;
			}
		}

		uint getreserve	( )
		{
			if( beg_addr && end_addr )
			{
				return end_addr - beg_addr;
			}
			else
			{
				return 0;
			}
		}

		uint getfilling ( )
		{
			return (cur_addr - beg_addr);
		}

		uint getremaining ( )
		{
			return (end_addr - cur_addr);
		}

		bool setreserve	( uint inbsize )
		{
			if( !inbsize )
				return FALSE;

			freereserve();

			void* p = NvMalloc( inbsize );
			if( !p )
				return FALSE;

			beg_addr = (byte*) p;
			cur_addr = beg_addr;
			end_addr = beg_addr + inbsize;
			return TRUE;
		}

		void clear ( )
		{
			cur_addr = beg_addr;
		}

		void* alloc ( uint inbsize )
		{
			if( cur_addr && end_addr )
			{
				if( inbsize > getremaining() )
					return NULL;
				byte* p = cur_addr;
				cur_addr += inbsize;
				return (void*)p;
			}
			else
			{
				return NULL;
			}
		}
	};


	jpeg_decoder_mem_stream		mem_stream;
	jpeg_decoder_mem_heap		mem_heap;
	jpeg_decoder				decoder;

}



bool
nvcore_Jpeg_Init()
{
	mem_heap.init();
	return TRUE;
}


void
nvcore_Jpeg_Update()
{
	//
}


void
nvcore_Jpeg_Shut()
{
	mem_heap.shut();
}




uint
nv::jpeg::GetReserve	(	)
{
	return mem_heap.getreserve();
}


uint
nv::jpeg::GetReserveFilling	(	)
{
	return mem_heap.getfilling();
}


bool
nv::jpeg::SetReserve	(	uint	inBSize		)
{
	return mem_heap.setreserve( inBSize );
}


void
nv::jpeg::FreeReserve (	)
{
	mem_heap.freereserve();
}


int
nv::jpeg::DecodeBegin	(	ImageDesc&	outDesc,
							byte*		inJpegAddr,
							uint		inJpegBSize,
							DecodePSM	inDecPSM		)
{
	if( !inJpegAddr || !inJpegBSize )
		return JPGD_NOT_JPEG;

	mem_heap.clear();
	mem_stream.setup( inJpegAddr, inJpegBSize );
	decoder.decode_init( &mem_stream, &mem_heap, TRUE, (inDecPSM==OPSM_888X) );

	int errcode = decoder.get_error_code();

	if( errcode==0 )
	{
		Zero( outDesc );
		outDesc.width  = decoder.get_width();
		outDesc.height = decoder.get_height();
		outDesc.numco  = decoder.get_num_components();

		errcode = decoder.begin();
		if( errcode==0 )
		{
			outDesc.bpp	 = decoder.get_bytes_per_pixel();
			outDesc.bpsl = decoder.get_bytes_per_scan_line();
		}
	}

	return errcode;
}


bool
nv::jpeg::DecodeLine (	byte*&	outPixels	)
{
	void* pix = NULL;
	uint  len = 0;
	int   errcode;
	errcode = decoder.decode( &pix, &len );

	if( errcode==0 )
	{
		outPixels = (byte*) pix;
		return TRUE;
	}
	else
	{
		DecodeEnd();
		return FALSE;
	}
}


void
nv::jpeg::DecodeEnd	( )
{
	mem_stream.setup( NULL, 0 );
}


