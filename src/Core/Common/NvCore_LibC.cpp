/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#if defined(WIN32)

#include <Kernel/PC/NvWindows.h>
#include <Nova.h>

#else

#include <Nova.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#if defined(_EE)
	#include <Kernel/PS2/EE/EE.h>
	#include <sifdev.h>
#endif
#if defined(_PSP)
	#include <Kernel/PSP/ALG/ALG.h>
#endif
#if defined(_NGC)
	#include <dolphin.h>
	#include <Kernel/NGC/GEK/GEK_devcons.h>	
#endif
#endif


#include "minilzo_108/minilzo.h"



bool
nvcore_Libc_Init 	(		)
{
	int r = lzo_init();
	NV_ASSERTC( r == LZO_E_OK, "nv::libc packer init error !" );
	return ( r == LZO_E_OK );
}


void
nvcore_Libc_Update	(		)
{
	//
}


void
nvcore_Libc_Shut	(		)
{
	//
}



void*
nv::libc::Memset(	void* inPtr, int inValue, uint32 inSize )
{
	if( inPtr && inSize )
		::memset( inPtr, inValue, inSize );
	return inPtr;
}


void*
nv::libc::Memcpy(	void* inDst, void* inSrc, uint32 inSize )
{
	if( inDst && inSrc && inSize )
		::memcpy( inDst, inSrc, inSize );
	return inDst;
}


void*
nv::libc::Memncpy(	void* inDst, void* inSrc, uint32 inSize, uint inCpt	)
{
	if( inDst && inSrc && inCpt && inSize ) {
		uint8* dst = (uint8*) inDst;
		uint8* end = dst + (inCpt*inSize);
		while( dst != end ) {
			Memcpy( dst, inSrc, inSize );
			dst += inSize;
		}
	}
	return inDst;
}


void*
nv::libc::Memmove(	void * inDst, void * inSrc, uint32 inSize )
{
	if( inDst && inSrc && inSize )
		::memmove( inDst, inSrc, inSize );
	return inDst;
}


int
nv::libc::Memcmp(	void* inP0,  void* inP1,  uint32 inSize		)
{
	NV_ASSERT( inP0 );
	NV_ASSERT( inP1 );
	return ::memcmp( inP0, inP1, inSize );
}


int
nv::libc::Memcmp (	void* inPtr, int inValue, uint32 inSize )
{
	if( inSize && inPtr )
	{
		byte* cur = (byte*) inPtr;
		byte* end = cur + inSize;
		while( cur < end )
		{
			if( *cur < inValue )	return -1;
			if( *cur > inValue )	return +1;
			cur++;
		}
	}
	return 0;
}


bool
nv::libc::IsPacked	(	void* inSrc, uint inLen		)
{
	if( !inSrc || inLen <= 8 )
		return FALSE;
	uint32 hdr[2];
	Memcpy( hdr, inSrc, 8 );
	return ( LSBToNative(hdr[0])=='PAKD' && LSBToNative(hdr[1])>inLen );
}


bool
nv::libc::GetUnpackBSize	(	void* inSrc, uint inLen, uint& outLen		)
{
	if( !IsPacked(inSrc,inLen) )
		return FALSE;
	uint32 hdr[2];
	Memcpy( hdr, inSrc, 8 );
	outLen = LSBToNative( hdr[1] );
	return TRUE;
}


bool
nv::libc::Pack	(	void* inSrc, uint inLen, void* inDst, uint& outLen	)
{
	if( !inSrc || !inLen || !inDst )
		return FALSE;

	void* out    = EngineMallocA( 8 + inLen + inLen / 64 + 16 + 3, 16 );
	void* wrkmem = EngineMallocA( LZO1X_1_MEM_COMPRESS, 16 );
	if( !out || !wrkmem ) {
		SafeFree( out );
		SafeFree( wrkmem );
		return FALSE;
	}

	lzo_uint out_len;
	int r = lzo1x_1_compress(	(lzo_byte*)inSrc, inLen,
								((lzo_byte*)out)+8, &out_len,
								(lzo_voidp)wrkmem );

	NV_ASSERTC( r == LZO_E_OK, "nv::libc::Pack failed !" );
	EngineFree( wrkmem );

	if( r != LZO_E_OK || (out_len+8) >= inLen ) {
		EngineFree( out );
		return FALSE;
	}

	uint32 hdr[2];
	hdr[0] = NativeToLSB( 'PAKD' );
	hdr[1] = NativeToLSB( inLen );
	Memcpy( out, hdr, 8 );
	Memcpy( inDst, out, out_len+8 );
	EngineFree( out );
	outLen = out_len+8;

	return TRUE;
}


bool
nv::libc::Unpack (	void* inSrc, uint inLen, void* inDst	)
{
	if( !inDst )
		return FALSE;
	uint32 ubsize;
	if( !GetUnpackBSize(inSrc,inLen,ubsize) )
		return FALSE;

	lzo_uint out_len = 0;
	int r = lzo1x_decompress(	((lzo_byte*)inSrc)+8, inLen-8,
								(lzo_byte*)inDst, &out_len,
								NULL	);

	// Check unpack error
	if( r != LZO_E_OK )
	{
		if( r == LZO_E_INPUT_NOT_CONSUMED )
		{
			// The inLen input-size is no correct (too big) !
			// But the data are correctly outputed but LZO, so display only a warning ...
			NV_WARNING_ONCE( "libc::Unpack: Input not consumed !" );
		}
		else
		{
			// Major error handling ...
		#if defined( _NVCOMP_ENABLE_DBG )
			char* err_msg = "libc::Unpack: Failed !";
			if( r == LZO_E_OUT_OF_MEMORY )				err_msg = "libc::Unpack: Out of memory !";
			else if( r == LZO_E_NOT_COMPRESSIBLE )		err_msg = "libc::Unpack: Not compressible !";
			else if( r == LZO_E_INPUT_OVERRUN )			err_msg = "libc::Unpack: Input overrun !";
			else if( r == LZO_E_OUTPUT_OVERRUN )		err_msg = "libc::Unpack: Output overrun !";
			else if( r == LZO_E_LOOKBEHIND_OVERRUN )	err_msg = "libc::Unpack: Lookbehind overrun !";
			else if( r == LZO_E_EOF_NOT_FOUND )			err_msg = "libc::Unpack: EOF not found !";
			NV_ERROR( err_msg );
		#else
			NV_ERROR( "libc::Unpack: Failed (use DBGO version for more information) !" );
		#endif
			return FALSE;
		}
	}

	// Check ouput error
	NV_ASSERTC( out_len == ubsize, "libc::Unpack: Incorrect outlen !" );
	return ( out_len == ubsize );
}


int
nv::libc::Toupper(	int   c		)
{
	return toupper( c );
}


int
nv::libc::Tolower(	int	  c		)
{
	return tolower( c );
}


void*
nv::libc::Zero( void * inDst, uint32 inSize )
{
	return Memset( inDst, 0, inSize );
}


bool
nv::libc::CheckZero ( void* inDst, uint32 inSize )
{
	return Memcmp( inDst, 0, inSize ) == 0;
}


pstr
nv::libc::Strcpy(	pstr  inDest,  pcstr inSrc	)
{
	return ::strcpy( inDest, inSrc );
}


pstr
nv::libc::Strncpy(	pstr  inDest,  pcstr inSrc, uint count	)
{
	return ::strncpy( inDest, inSrc, count );
}


pstr
nv::libc::Strcat(	pstr  inDest,  pcstr inSrc	)
{
	return ::strcat( inDest, inSrc );
}


uint
nv::libc::Strlen	(	pcstr inString	)
{
	return ::strlen( inString );
}


int
nv::libc::Strcmp	(	pcstr string1, pcstr string2	)
{
	return ::strcmp( string1, string2 );
}


int
nv::libc::Strncmp	(	pcstr string1, pcstr string2, uint32 count	)
{
	return ::strncmp( string1, string2, count );
}


int
nv::libc::Strnicmp	(	pcstr string1, pcstr string2, uint32 count		)
{
#if defined(WIN32)
	return ::_strnicmp( string1, string2, count );
#elif defined(_PS3)
	return ::strncasecmp(string1, string2, count);
#else
	return ::strnicmp( string1, string2, count );
#endif
}


int
nv::libc::Stricmp	(	pcstr string1, pcstr string2	)
{
#if defined(WIN32)
	return ::_stricmp( string1, string2 );
#elif defined(_PS3)
	return ::strcasecmp(string1, string2);
#else
	return ::stricmp( string1, string2 );
#endif
}


pstr
nv::libc::Strchr	(	pcstr inString, int inC		)
{
	return (pstr)(::strchr( inString, inC ));
}


pstr
nv::libc::Strrchr	(	pcstr inString, int inC		)
{
	return (pstr)(::strrchr( inString, inC ));
}


uint
nv::libc::Strcspn	(	pcstr inString, pcstr inCharSet		)
{
	return ::strcspn( inString, inCharSet );
}


uint
nv::libc::Strspn	(	pcstr inString, pcstr inCharSet		)
{
	return ::strspn( inString, inCharSet );
}


pcstr
nv::libc::Strpbrk	(	pcstr inString, pcstr inCharSet 	)
{
	return ::strpbrk( inString, inCharSet );
}


pstr
nv::libc::Strlwr	(	pstr  inString	)
{
#if defined(WIN32)
	return ::_strlwr( inString );
#elif defined(_PS3)
	if ( !inString ) return inString;
	pstr ptr = inString;
	while ( *ptr != '\0' )
		*ptr = char ( tolower(*ptr) );
	return inString;
#else
	return ::strlwr( inString );
#endif
}


pstr
nv::libc::Strupr(	pstr  	inString	)
{
#if defined(WIN32)
	return ::_strupr( inString );
#elif defined(_PS3)
	if ( !inString ) return inString;
	pstr ptr = inString;
	while ( *ptr != '\0' )
		*ptr = char ( toupper(*ptr) );
	return inString;
#else
	return ::strupr( inString );
#endif
}



static int maxExponent = 38;	/* Largest possible base 10 exponent.  Any
								 * exponent larger than this will already
								 * produce underflow or overflow, so there's
								 * no need to worry about additional digits.
								 */
static float powersOf10[] = {	/* Table giving binary powers of 10.  Entry */
		10.f,					/* is 10^2^i.  Used to convert decimal */
		100.f,					/* exponents into floating-point numbers. */
		1.0e4f,
		1.0e8f,
		1.0e16f,
		1.0e32f
};


float
nv::libc::Strtof(	pcstr	string,		pstr *	endPtr	)
{
    int sign, expSign = FALSE;
    float fraction, dblExp, *d;
    register pcstr p;
    register int c;
    int exp = 0;			/* Exponent read from "EX" field. */
    int fracExp = 0;		/* Exponent that derives from the fractional
							 * part.  Under normal circumstatnces, it is
							 * the negative of the number of digits in F.
							 * However, if I is very long, the last digits
							 * of I get dropped (otherwise a long I with a
							 * large negative exponent could cause an
							 * unnecessary overflow on I alone).  In this
							 * case, fracExp is incremented one for each
							 * dropped digit. */
    int mantSize;			/* Number of digits in mantissa. */
    int decPt;				/* Number of mantissa digits BEFORE decimal
							 * point. */
    pcstr pExp;				/* Temporarily holds location of exponent
							 * in string. */

    /*
     * Strip off leading blanks and check for a sign.
     */

    p = string;
    while (isspace(uchar(*p)))
		p += 1;
    if (*p == '-') {
		sign = TRUE;
		p += 1;
	} else {
		if (*p == '+')
			p += 1;
		sign = FALSE;
    }

    /*
     * Count the number of digits in the mantissa (including the decimal
     * point), and also locate the decimal point.
     */

    decPt = -1;
    for (mantSize = 0; ; mantSize += 1)
    {
		c = *p;
		if (!isdigit(c)) {
			if ((c != '.') || (decPt >= 0))
				break;
			decPt = mantSize;
		}
		p += 1;
    }

    /*
     * Now suck up the digits in the mantissa.  Use two integers to
     * collect 9 digits each (this is faster than using floating-point).
     * If the mantissa has more than 18 digits, ignore the extras, since
     * they can't affect the value anyway.
     */
    
    pExp  = p;
    p -= mantSize;
    if (decPt < 0) {
		decPt = mantSize;
    } else {
		mantSize -= 1;			/* One of the digits was the point. */
    }
    if (mantSize > 18) {
		fracExp = decPt - 18;
		mantSize = 18;
    } else {
		fracExp = decPt - mantSize;
    }
    if (mantSize == 0) {
		fraction = 0.0;
		p = string;
		goto Strtof_done;
    } else {
		int frac1, frac2;
		frac1 = 0;
		for ( ; mantSize > 9; mantSize -= 1)
		{
			c = *p;
			p += 1;
			if (c == '.') {
				c = *p;
				p += 1;
			}
			frac1 = 10*frac1 + (c - '0');
		}
		frac2 = 0;
		for (; mantSize > 0; mantSize -= 1)
		{
			c = *p;
			p += 1;
			if (c == '.') {
				c = *p;
				p += 1;
			}
			frac2 = 10*frac2 + (c - '0');
		}
		fraction = (1.0e9 * frac1) + frac2;
    }

    /*
     * Skim off the exponent.
     */

    p = pExp;
    if ((*p == 'E') || (*p == 'e')) {
		p += 1;
		if (*p == '-') {
			expSign = TRUE;
			p += 1;
		} else {
			if (*p == '+') {
				p += 1;
			}
			expSign = FALSE;
		}
		if (!isdigit(uchar(*p))) {
			p = pExp;
			goto Strtof_done;
		}
		while (isdigit(uchar(*p))) {
			exp = exp * 10 + (*p - '0');
			p += 1;
		}
	}
    if (expSign) {
		exp = fracExp - exp;
    } else {
		exp = fracExp + exp;
    }

    /*
     * Generate a floating-point number that represents the exponent.
     * Do this by processing the exponent one bit at a time to combine
     * many powers of 2 of 10. Then combine the exponent with the
     * fraction.
     */

    if (exp < 0) {
		expSign = TRUE;
		exp = -exp;
    } else {
		expSign = FALSE;
    }
    if (exp > maxExponent) {
		exp = maxExponent;
//		errno = ERANGE;
    }
    dblExp = 1.0;
    for (d = powersOf10; exp != 0; exp >>= 1, d += 1) {
		if (exp & 01) {
			dblExp *= *d;
		}
    }
    if (expSign) {
		fraction /= dblExp;
    } else {
		fraction *= dblExp;
    }

Strtof_done:
    if (endPtr != NULL) {
		*endPtr = (char *) p;
    }

    if( sign )	return -fraction;
	else		return fraction;
}


long
nv::libc::Strtol( pcstr nptr, pstr* endptr, int base )
{
	return ::strtol( nptr, endptr, base );
}


ulong
nv::libc::Strtoul( pcstr nptr, pstr* endptr, int base )
{
	return ::strtoul( nptr, endptr, base );
}


void
nv::libc::Printf( pcstr inFmt, ... )
{
	#if defined(_NVCOMP_ENABLE_CONSOLE)
	static bool en_console = TRUE;
	#else
	static bool en_console = FALSE;
	#endif

	if( inFmt )
	{
		if( en_console )
		{
			if( Strcmp(inFmt,"nv-debug-dis-cons")==0 )
				en_console = FALSE;
		}
		else
		{
			if( Strcmp(inFmt,"nv-debug-en-cons")==0 )
				en_console = TRUE;
		}
	}

//	if( en_console )
	{
		char str[4096];
		va_list	arg;
		va_start( arg, inFmt );
		vsprintf( str, inFmt, arg );
		va_end( arg );

		// change '%' to '@'
		for( ;; ) {
			char* p = strchr( str, '%' );
			if( !p )	break;
			*p = '@';
		}

	#if defined(_EE)

		#if defined( _ATMON )
			static int atLogFd = -1;
			pcstr atLogFile = nv::file::GetFullname( "mwAtLog.txt" );
			NV_ASSERT( atLogFile );
			if( atLogFd < 0 )
				atLogFd = sceOpen( atLogFile, SCE_CREAT|SCE_TRUNC|SCE_RDWR|SCE_NOBUF );
			if( atLogFd >= 0 )
				sceWrite( atLogFd, str, Strlen(str)+1 );
		#else
			scePrintf( str );
		#endif
		devcons::Printf( str );

	#elif defined(_PSP)

		printf( str );
		devcons::Printf( str );
		#if defined(_ATMON)
		sceKernelRotateThreadReadyQueue( 0 );
		#endif

	#elif defined(WIN32)

		OutputDebugString( str );
		printf( str );

	#elif defined(_NGC)	

		OSReport( str );
		devcons::Printf( str );

	#elif defined(_PS3)

		printf( str );
		//devcons::Printf( str );

	#else

		#error "Missing target !"

	#endif
	}
}


void
nv::libc::FlushPrintf	(			)
{
#if defined(_NVCOMP_ENABLE_CONSOLE)
#if defined(_PSP)
	sceKernelDelayThread( 100000 );
#endif // _PSP
#endif // _NVCOMP_ENABLE_CONSOLE
}


int
nv::libc::Sprintf( 	pstr  inDst,   pcstr inFmt, ...	)
{
	va_list argptr;
	va_start( argptr, inFmt);
	int res = vsprintf( inDst, inFmt, argptr );
	va_end( argptr );
	return res;
}


pcstr
nv::libc::Tsprintf( pcstr inFmt, ... )
{
	static char _msg[ 256 ];
	va_list argptr;
	va_start( argptr, inFmt);
	vsprintf( _msg, inFmt, argptr );
	va_end( argptr );
	return _msg;
};


void
nv::libc::DumpData( uint32 inUID, void* inPtr, uint inSize )
{
	Printf( "\nDump: uid=0x%x, ptr=0x%x, bsize=%d\n", inUID, uint32(inPtr), inSize );
	for( uint io = 0 ; io < inSize ; io+=16 ) {
		uint8 v[16], a[16];
		for( uint j = 0 ; j < 16 ; j++ ) {
			v[j] = ((uint8*)inPtr)[io+j];
			a[j] = (v[j] < 32 || v[j] >= 128) ? '.' : v[j];
		}
		char tmp[256];
		Sprintf( tmp, "%04x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %02x  %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n",
			io,
			v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8], v[9], v[10], v[11], v[12], v[13], v[14], v[15],
			a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]	);
		Printf( tmp );
	}
	Printf( "\n" );
}



namespace
{
	uint64 _x[5] = { 4002576854,3733863077,3750272681,3762340723,1728061494 };  // History buffer (init value is equal to _RandomInit(nv::libc::RANDSEED) )

	uint32 _BRandom()
	{
		uint64 sum;
		sum = uint64(2111111111ul)*_x[3] + uint64(1492)*_x[2] + uint64(1776)*_x[1] + uint64(5115)*_x[0] + uint64(_x[4]);
		_x[3] = _x[2];
		_x[2] = _x[1];
		_x[1] = _x[0];
		_x[4] = uint32(sum >> 32);          // Carry
		_x[0] = uint32(sum & 0xFFFFFFFFU);	// Low 32 bits of sum
		return _x[0];
	}

	float _FRandom()
	{
		return (float)_BRandom() * (1.f/(65536.f*65536.f));
	}

	int _IRandom(int min, int max)
	{
		if (max <= min)
		{
			if (max == min) return min; else return 0x80000000;
		}
		int r = int((max - min + 1) * _FRandom()) + min; 
		if (r > max) r = max;
		return r;
	}
	
	void _RandomInit (uint32 *	inState) /* [5] */
	{
		for (int i = 0; i < 5; i++)
		{
			_x[i] = inState[i];
		}
	}
/*
	void _RandomInit (uint32 seed) {
		int i;
		uint32 s = seed;
		for (i = 0; i < 5; i++) {
			s = s * 29943829 - 1;
			_x[i] = s;
		}
		for (i=0; i<19; i++) _BRandom();
	}
*/
}

bool
nv::libc::RandRestore( pcstr inRandState )	/* 256 char */
{
	if ( !inRandState || inRandState[0] == '\0' ) return false;

	for (int i = 0; i < 5; i++) {
		_x[i] = 0;
	}

	pcstr	ptr		= inRandState;
	int		i		= 0;

	while ( i < 5 ) {
		if (*ptr == '\0') {
			_x[i] = i;
		}
		else {
			pstr end;
			_x[i] = Strtoul(ptr,&end,10);
			ptr = (pcstr)end;
		}
		i++;
	}
	return true;
}

bool
nv::libc::RandSave( pstr outRandStatea )	/* 256 char */
{
	if (!outRandStatea) return false;
	Sprintf(outRandStatea,"%u %u %u %u %u",_x[0],_x[1],_x[2],_x[3],_x[4],_x[5]);
	return true;
}


uint32
nv::libc::Rand	(	uint32 inM0, uint inM1	)
{
	return _IRandom( inM0, inM1 );
}


float
nv::libc::Randf	(					)
{
	return _FRandom();
}


float
nv::libc::Randsf	(				)
{
	return (_FRandom() * 2.0f) -1.0f;
}



namespace
{
	#define	FNM_NOMATCH		1
	#define MAXNEST    		20
	#define EOS    			'\0'
	#define RANGE_MATCH     1
	#define RANGE_NOMATCH   0
	#define RANGE_ERROR     (-1)

	char * index(const char *p, int ch)
	{
         union {
                 const char *cp;
                 char *p;
         } u;
 
         u.cp = p;
         for (;; ++u.p) {
                 if (*u.p == ch)
                         return(u.p);
                 if (!*u.p)
                         return(NULL);
         }
         /* NOTREACHED */
	}

	int rangematch( const char * pattern, char test, int flags, char ** newp)
	{
         int negate, ok;
         char c, c2;
 
         /*
          * A bracket expression starting with an unquoted circumflex
          * character produces unspecified results (IEEE 1003.2-1992,
          * 3.13.2).  This implementation treats it like '!', for
          * consistency with the regular expression syntax.
          * J.T. Conklin (conklin@ngai.kaleida.com)
          */
         if ( (negate = (*pattern == '!' || *pattern == '^')) )
                 ++pattern;
 
         if (flags & FNM_CASEFOLD)
                 test = tolower((unsigned char)test);
 
         /*
          * A right bracket shall lose its special meaning and represent
          * itself in a bracket expression if it occurs first in the list.
          * -- POSIX.2 2.8.3.2
          */
         ok = 0;
         c = *pattern++;
         do {
                 if (c == '\\' && !(flags & FNM_NOESCAPE))
                         c = *pattern++;
                 if (c == EOS)
                         return (RANGE_ERROR);
 
                 if (c == '/' && (flags & FNM_PATHNAME))
                         return (RANGE_NOMATCH);
 
                 if (flags & FNM_CASEFOLD)
                         c = tolower((unsigned char)c);
 
                 if (*pattern == '-'
                     && (c2 = *(pattern+1)) != EOS && c2 != ']') {
                         pattern += 2;
                         if (c2 == '\\' && !(flags & FNM_NOESCAPE))
                                 c2 = *pattern++;
                         if (c2 == EOS)
                                 return (RANGE_ERROR);
 
                         if (flags & FNM_CASEFOLD)
                                 c2 = tolower((unsigned char)c2);
 
                         if (c <= test && test <= c2)
                                 ok = 1;
                 } else if (c == test)
                         ok = 1;
         } while ((c = *pattern++) != ']');
 
         *newp = (char*)pattern;
         return (ok == negate ? RANGE_NOMATCH : RANGE_MATCH);
	}

	int	_fnmatch(const char *pattern, const char *string, int flags, int nesting)
	{
         const char *stringstart;
         char *newp;
         char c, test;
 
         if (nesting == MAXNEST)
                 return (FNM_NOMATCH);
 
         for (stringstart = string;;) {
                 switch (c = *pattern++) {
                 case EOS:
                         if ((flags & FNM_LEADING_DIR) && *string == '/')
                                 return (0);
                         return (*string == EOS ? 0 : FNM_NOMATCH);
                 case '?':
                         if (*string == EOS)
                                 return (FNM_NOMATCH);
                         if (*string == '/' && (flags & FNM_PATHNAME))
                                 return (FNM_NOMATCH);
                         if (*string == '.' && (flags & FNM_PERIOD) &&
                             (string == stringstart ||
                             ((flags & FNM_PATHNAME) && *(string - 1) == '/'))) {
                                 return (FNM_NOMATCH);
                         }
                         ++string;
                         break;
                 case '*':
                         c = *pattern;
                         /* Collapse multiple stars. */
                         while (c == '*')
                                 c = *++pattern;
 
                         if (*string == '.' && (flags & FNM_PERIOD) &&
                             (string == stringstart ||
                             ((flags & FNM_PATHNAME) && *(string - 1) == '/'))) {
                                 return (FNM_NOMATCH);
                         }
 
                         /* Optimize for pattern with * at end or before /. */
                         if (c == EOS) {
                                 if (flags & FNM_PATHNAME) {
                                         return ((flags & FNM_LEADING_DIR) ||
                                             index(string, '/') == NULL ?
                                             0 : FNM_NOMATCH);
                                 } else {
                                         return (0);
                                 }
                         } else if (c == '/' && flags & FNM_PATHNAME) {
                                 if ((string = index(string, '/')) == NULL)
                                         return (FNM_NOMATCH);
                                 break;
                         }
 
                         /* General case, use recursion. */
                         while ((test = *string) != EOS) {
                                 if (!_fnmatch(pattern, string, flags & ~FNM_PERIOD, nesting + 1))
                                         return (0);
                                 if (test == '/' && flags & FNM_PATHNAME)
                                         break;
                                 ++string;
                         }
                         return (FNM_NOMATCH);
                 case '[':
                         if (*string == EOS)
                                 return (FNM_NOMATCH);
                         if (*string == '/' && (flags & FNM_PATHNAME))
                                 return (FNM_NOMATCH);
                         if (*string == '.' && (flags & FNM_PERIOD) &&
                             (string == stringstart ||
                             ((flags & FNM_PATHNAME) && *(string - 1) == '/')))
                                 return (FNM_NOMATCH);
 
                         switch (rangematch(pattern, *string, flags, &newp)) {
                         case RANGE_ERROR:
                                 goto norm;
                         case RANGE_MATCH:
                                 pattern = newp;
                                 break;
                         case RANGE_NOMATCH:
                                 return (FNM_NOMATCH);
                         }
                         ++string;
                         break;
                 case '\\':
                         if (!(flags & FNM_NOESCAPE)) {
                                 if ((c = *pattern++) == EOS) {
                                         c = '\\';
                                         --pattern;
                                 }
                         }
                         /* FALLTHROUGH */
                 default:
                 norm:
                         if (c == *string) {
                                 ;
                         } else if ((flags & FNM_CASEFOLD) &&
                                  (tolower((unsigned char)c) ==
                                   tolower((unsigned char)*string))) {
                                 ;
                         } else {
                                 return (FNM_NOMATCH);
                         }
                         string++;
                         break;
                 }
         }
         /* NOTREACHED */
	}
}


bool
nv::libc::Fnmatch	(	pcstr pattern, pcstr string, uint flags	)
{
	return _fnmatch( pattern, string, flags, 0 ) != FNM_NOMATCH;
}



