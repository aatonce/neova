/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>

#if defined(_EE)

#include <Kernel/PS2/EE/EE.h>

#elif defined(_PSP)

#include <Kernel/PSP/ALG/ALG.h>

#elif defined( _WIN32 )

#include <Kernel/PC/NvHrdw.h>
const static uint32 DEFAULT_DS_DESC_FLAGS = 0 
										/* DSBCAPS_CTRL3D */
											| DSBCAPS_CTRLFREQUENCY 
											| DSBCAPS_CTRLVOLUME 
											| DSBCAPS_GETCURRENTPOSITION2
										//	| DSBCAPS_LOCHARDWARE
										//	| DSBCAPS_LOCSOFTWARE
											| DSBCAPS_LOCDEFER
										//	| DSBCAPS_GLOBALFOCUS 
										//  | DSBCAPS_STICKYFOCUS
										;
#endif



#include <Build/nv_custom.h>
using namespace nv; 


namespace
{
 	struct Param
	{
		enum Type
		{
			T_UNDEF=0,
			T_BOOL,
			T_UINT32,
			T_PCSTR
		};

		Type type;

		union
		{
			bool		bo;
			uint32		ui;
			pcstr		cs;
		};
	} paramA[ int(nv::core::PR_LAST) ];


	inline
	void InitBoolParam( nv::core::Parameter inP, bool inV )
	{
		NV_ASSERT( inP < nv::core::PR_LAST );
		if( inP < nv::core::PR_LAST ) {
			paramA[int(inP)].type = Param::T_BOOL;
			paramA[int(inP)].bo   = inV;
		}
	}

	inline
	void InitUInt32Param( nv::core::Parameter inP, uint32 inV )
	{
		NV_ASSERT( inP < nv::core::PR_LAST );
		if( inP < nv::core::PR_LAST ) {
			paramA[int(inP)].type = Param::T_UINT32;
			paramA[int(inP)].ui   = inV;
		}
	}

	inline
	void InitStringParam( nv::core::Parameter inP, pcstr inV )
	{
		NV_ASSERT( inP < nv::core::PR_LAST );
		if( inP < nv::core::PR_LAST ) {
			paramA[uint(inP)].type = Param::T_PCSTR;
			paramA[uint(inP)].cs   = inV;
		}
	}


#if defined( _EE )
	char* irxList[] = {
		// PATH						ARGS
		"IRX\\sio2man.irx",			"",
		"IRX\\mtapman.irx",			"",
		"IRX\\padman.irx",			"",
		"IRX\\libsd.irx",			"",
		"IRX\\mcman.irx",			"",
		"IRX\\mcserv.irx",			"",
		"IRX\\inet.irx",			"",
		"IRX\\netcnf.irx",			"icon=SYS_NET.ICO iconsys=ICON.SYS",
		"IRX\\inetctl.irx",			"-no_auto -no_decode",
//		"IRX\\usbd.irx",			"",
//		"IRX\\an986.irx",			"",
		"IRX\\novaIOP.irx",			"",
		NULL
	};
#endif
}



bool
nvcore_Param_Init	(	)
{
	for( uint i = 0 ; i < uint(nv::core::PR_LAST) ; i++ )
		paramA[i].type = Param::T_UNDEF;


#if defined( _EE )
	InitStringParam( 	core::PR_SYS_OS,					"Sony Playstation 2"		);
#elif defined( _PSP )
	InitStringParam( 	core::PR_SYS_OS,					"Sony Playstation Portable"	);
#elif defined( WIN32 )
	InitStringParam( 	core::PR_SYS_OS,					"Microsoft Windows"			);
#elif defined( _NGC )
	#if defined(_RVL)
	InitStringParam( 	core::PR_SYS_OS,					"Nintendo Revolution Wii"	);	
	#else
	InitStringParam( 	core::PR_SYS_OS,					"Nintendo GameCube"			);	
	#endif
#elif defined( _PS3 )
	InitStringParam( 	core::PR_SYS_OS,					"Sony Playstation 3"		);
#else
	#error "missing definitions !"
#endif

	InitStringParam(	core::PR_SYS_CMDLINE,				""					);
	InitStringParam(	core::PR_SYS_CWD,					""					);

	// Memory
	InitUInt32Param(	core::PR_MEM_ALLOCATOR,				0					);
	InitUInt32Param(	core::PR_MEM_BALIGN,				16					);
	InitStringParam(	core::PR_EVAL_KEY,					NULL				);
#if defined( _NVCOMP_ENABLE_DBG )
	InitUInt32Param(	core::PR_MEM_RESET_VALUE,			0xCD				);
	InitUInt32Param(	core::PR_MEM_LOG_MAXBSIZE,			0					);		// Disabled !
	InitStringParam(	core::PR_MEM_LOG_FILENAME,			"nvmem.log"			);
#endif
	InitUInt32Param(	core::PR_MEM_APP_ADDR,				0					);
	InitUInt32Param(	core::PR_MEM_APP_BSIZE,				0					);
	InitUInt32Param(	core::PR_MEM_APP_HEAP_ADDR,			0					);
	InitUInt32Param(	core::PR_MEM_APP_HEAP_BSIZE,		0xFFFFFFFF			);
	InitUInt32Param(	core::PR_MEM_APP_STACK_ADDR,		0					);
	InitUInt32Param(	core::PR_MEM_APP_STACK_BSIZE,		0xFFFFFFFF			);

	// I/O
#if defined( _WIN32 )
	InitStringParam(	core::PR_FILE_PREFIX,				""					);
	InitStringParam(	core::PR_FILE_SUFFIX,				""					);
#elif defined( _EE )
	#if defined( _NVCOMP_DEVKIT )
	// host0
	InitStringParam(	core::PR_FILE_PREFIX,				"host0:.\\"			);
	InitStringParam(	core::PR_FILE_SUFFIX,				""					);
	#else
	// cdrom0
	InitStringParam(	core::PR_FILE_PREFIX,				"cdrom0:\\"			);
	InitStringParam(	core::PR_FILE_SUFFIX,				";1"				);
	#endif // _NVCOMP_DEVKIT
#elif defined( _PSP )
	#if defined( _NVCOMP_DEVKIT )
	// host0
	InitStringParam(	core::PR_FILE_PREFIX,				"host0:.\\"			);
	InitStringParam(	core::PR_FILE_SUFFIX,				""					);
	#else
	// UMD
	InitStringParam(	core::PR_FILE_PREFIX,				"disc0:\\"			);
	InitStringParam(	core::PR_FILE_SUFFIX,				""					);
	#endif // _NVCOMP_DEVKIT
#elif defined( _NGC )
	InitStringParam(	core::PR_FILE_PREFIX,				""					);
	InitStringParam(	core::PR_FILE_SUFFIX,				""					);
#elif defined( _PS3 )
	InitStringParam(	core::PR_FILE_PREFIX,				""					);
	InitStringParam(	core::PR_FILE_SUFFIX,				""					);
#else
	#error "missing definitions !"
#endif
	InitStringParam(	core::PR_MEDIA_TYPE,				""					);
	InitUInt32Param(	core::PR_MEDIA_STATUS_ERROR,		0					);
	InitStringParam(	core::PR_MEDIA_STATUS_FILE,			NULL				);

	// RscManager
	InitUInt32Param(	core::PR_RSCMAN_CACHEBSIZE,			64*1024				);
	InitUInt32Param(	core::PR_RSCMAN_JUMP_MAXBSIZE,		4*1024				);
	InitUInt32Param(	core::PR_RSCMAN_READ_BSTART_ALIGN,	2048				);
	InitUInt32Param(	core::PR_RSCMAN_READ_BSIZE_ALIGN,	512					);
	InitUInt32Param(	core::PR_RSCMAN_LOG_MAXBSIZE,		4*1024				);
	InitUInt32Param(	core::PR_RSCMAN_RRPRIORITY,			8					);
	InitBoolParam(		core::PR_RSCMAN_RETRY,				TRUE				);

	// DpyManager
#if defined( _WIN32 )
	InitUInt32Param(	core::PR_FRONT_FRAME_W,				640					);
	InitUInt32Param(	core::PR_FRONT_FRAME_H,				480					);
	InitUInt32Param(	core::PR_BACK_FRAME_W,				640					);
	InitUInt32Param(	core::PR_BACK_FRAME_H,				480					);
	InitUInt32Param(	core::PR_OFF_FRAME_W,				128					);
	InitUInt32Param(	core::PR_OFF_FRAME_H,				128					);
#elif defined( _EE )
	InitUInt32Param(	core::PR_FRONT_FRAME_W,				640					);
	InitUInt32Param(	core::PR_FRONT_FRAME_H,				448					);
	InitUInt32Param(	core::PR_BACK_FRAME_W,				640					);
	InitUInt32Param(	core::PR_BACK_FRAME_H,				448					);
	InitUInt32Param(	core::PR_OFF_FRAME_W,				64					);
	InitUInt32Param(	core::PR_OFF_FRAME_H,				64					);
#elif defined( _PSP )
	InitUInt32Param(	core::PR_FRONT_FRAME_W,				480					);
	InitUInt32Param(	core::PR_FRONT_FRAME_H,				272					);
	InitUInt32Param(	core::PR_BACK_FRAME_W,				480					);
	InitUInt32Param(	core::PR_BACK_FRAME_H,				272					);
	InitUInt32Param(	core::PR_OFF_FRAME_W,				256					);
	InitUInt32Param(	core::PR_OFF_FRAME_H,				256					);
#elif defined( _NGC )
	InitUInt32Param(	core::PR_FRONT_FRAME_W,				640					);
	InitUInt32Param(	core::PR_FRONT_FRAME_H,				448					);
	InitUInt32Param(	core::PR_BACK_FRAME_W,				640					);
	InitUInt32Param(	core::PR_BACK_FRAME_H,				448					);
	InitUInt32Param(	core::PR_OFF_FRAME_W,				256					);
	InitUInt32Param(	core::PR_OFF_FRAME_H,				256					);	
#elif defined( _PS3 )
	InitUInt32Param(	core::PR_FRONT_FRAME_W,				640					);
	InitUInt32Param(	core::PR_FRONT_FRAME_H,				448					);
	InitUInt32Param(	core::PR_BACK_FRAME_W,				640					);
	InitUInt32Param(	core::PR_BACK_FRAME_H,				448					);
	InitUInt32Param(	core::PR_OFF_FRAME_W,				64					);
	InitUInt32Param(	core::PR_OFF_FRAME_H,				64					);
#else
	#error "missing definitions !"
#endif
#if defined( _NVCOMP_ENABLE_DBG )
	InitUInt32Param(	core::PR_CHECK_MIPMAPPING_MODE,		0					);
	InitUInt32Param(	core::PR_CHECK_MIPMAPPING_LODS,		~0U					);
	InitBoolParam(		core::PR_ASYNC_LOG,					FALSE				);
	InitBoolParam(		core::PR_BGCOLOR_RW,				TRUE				);
#endif // _NVCOMP_ENABLE_DBG
	InitUInt32Param(	core::PR_RESET_ALLVRAM,				1					);
	InitUInt32Param(	core::PR_RESET_ALLVRAM_RGBA,		0U					);
	InitBoolParam(		core::PR_ENABLE_GPU_DRAWING,		TRUE				);
	InitBoolParam(		core::PR_CONSOLE_ONLY,				FALSE				);
	InitBoolParam(		core::PR_VSYNC_DRAW,				TRUE				);
	InitBoolParam(		core::PR_ASYNC_DRAW,				TRUE				);
	InitBoolParam(		core::PR_ASYNC_BREAK,				FALSE				);
	InitBoolParam(		core::PR_ASYNC_CHECK,				FALSE				);
	InitBoolParam(		core::PR_HIDE_CURSOR,				FALSE				);

	// SndManager
	InitUInt32Param(	core::PR_SNDMAN_STREAM_CACHEMST,	2000				);

	// ProfManager
	InitUInt32Param(	core::PR_PROFMAN_DATA_MAXBSIZE,		64*1024				);

	// Win32/DirectX
#if defined( _WIN32 )
	InitUInt32Param	(	core::PR_DX_D3D,					0						);	
	InitUInt32Param	(	core::PR_DX_D3DDEV,					0						);	
	InitUInt32Param	(	core::PR_DX_D3DADAPTER,				0						);	
	InitUInt32Param	(	core::PR_DX_RUNNING_MODE,			uint32(Hrdw::RM_Unknown) );
	InitBoolParam	(	core::PR_DX_FULLSCREEN,				FALSE					);
	InitBoolParam	(	core::PR_DX_ENABLE_RESIZING,		TRUE					);
	InitUInt32Param	(	core::PR_DI_COOPLEVEL,				0						);
	InitUInt32Param	(	core::PR_DS_BUFFERDESC_FLAGS,		DEFAULT_DS_DESC_FLAGS);
	InitBoolParam	(	core::PR_W32_SCREENSAVER,			FALSE					);
	InitUInt32Param	(	core::PR_W32_HINSTANCE,				0						);
	InitUInt32Param	(	core::PR_W32_HWND,					0						);
	InitUInt32Param	(	core::PR_W32_HWND_DEFPOS,			~0U						);
	InitUInt32Param	(	core::PR_W32_HWND_FOCUS,			0						);
	InitUInt32Param	(	core::PR_W32_HWND_DEFPROC,			0						);
	InitBoolParam	(	core::PR_W32_HWND_MANAGED,			FALSE					);
	InitStringParam	(	core::PR_W32_HWND_TITLE,			NULL					);
	InitUInt32Param	(	core::PR_W32_HWND_SMALLICON,		0						);
	InitUInt32Param	(	core::PR_W32_HWND_BIGICON,			0						);
	InitBoolParam	(	core::PR_W32_HWND_CONSTRAINTCURSOR,	TRUE					);
#endif

	// PS2
#if defined( _EE )
	InitUInt32Param(	core::PR_EE_DMAC_HEAPS_BSIZE,		2<<20					);
	InitBoolParam(		core::PR_EE_DMAC_HEAPS_UNCACHE,		TRUE					);
	#if defined( _NVCOMP_ENABLE_DBG )
	InitUInt32Param(	core::PR_EE_DMAC_BREAK_ON_PKT,		0U						);
	#endif
	InitUInt32Param(	core::PR_EE_VMODE,					gs::VM_PSM16|gs::VM_FIELD|gs::VM_AFLICKER );	// < 60FPS !
	InitUInt32Param(	core::PR_EE_BACKSTACK_BSIZE,		8*1024					);
	InitStringParam(	core::PR_IOP_IMGFILE,				IOP_IMAGE_FILE			);
	InitStringParam(	core::PR_IOP_IRXZIP,				"IRX.ZIP"				);
	InitUInt32Param(	core::PR_IOP_IRXLIST,				uint32(irxList)			);
	InitUInt32Param(	core::PR_IOP_RPC_THREADPRI,			50						);
	InitUInt32Param(	core::PR_IOP_UPD_THREADPRI,			50						);
	InitUInt32Param(	core::PR_IOP_UPD_TIMERFREQ,			4*60					);						// 4 IOP updates per frame !
	InitUInt32Param(	core::PR_IOP_NET_THREADPRI,			50						);
	InitUInt32Param(	core::PR_IOP_STREAM_CACHEBSIZE,		256*1024				);
	InitUInt32Param(	core::PR_IOP_EELOAD_REQMAXBSIZE,	128*1024				);
	InitUInt32Param(	core::PR_IOP_EELOAD_TRYCOUNTER,		16						);
	InitUInt32Param(	core::PR_IOP_VOICE_REQMAXBSIZE,		64*1024					);
	InitUInt32Param(	core::PR_IOP_CMDBUF_FILLING,		0						);
	InitUInt32Param(	core::PR_IOP_CMDBUF_OVERFLOW,		0						);
	InitBoolParam(		core::PR_IPU_COLUMNMODE_MOVIE,		FALSE					);
#endif

	// PSP
#if defined( _PSP )
	InitUInt32Param(	core::PR_ALG_BACKSTACK_BSIZE,		8*1024					);
	InitUInt32Param(	core::PR_ALG_DMAC_HEAPS_BSIZE,		2<<20					);
	InitBoolParam(		core::PR_ALG_DMAC_HEAPS_UNCACHE,	TRUE					);
	#if defined( _NVCOMP_ENABLE_DBG )
	InitUInt32Param(	core::PR_ALG_DMAC_BREAK_ON_PKT,		0U						);
	#endif
	InitUInt32Param(	core::PR_ALG_VMODE,					ge::VM_SWAPPING|ge::VM_PSM16|ge::VM_LCD	 );
	InitUInt32Param(	core::PR_ALG_VSYNC_INTR_IDX,		0										 );
	InitUInt32Param(	core::PR_ALG_TH_PRIORITY,			SCE_KERNEL_USER_LOWEST_PRIORITY			 );
#endif

	// NGC
#if defined( _NGC )
	InitUInt32Param(	core::PR_NGC_DMAC_HEAPS_BSIZE,		1<<20								);
	InitBoolParam(		core::PR_NGC_DMAC_HEAPS_UNCACHE,	TRUE								);
	#if defined( _NVCOMP_DEVKIT )
	InitBoolParam(		core::PR_NGC_DVDETH,				NV_CUSTOM_PR_NGC_DVDETH				);
	InitStringParam(	core::PR_NGC_DVDETH_SERVER,			NV_CUSTOM_PR_NGC_DVDETH_SERVER		);
	InitStringParam(	core::PR_NGC_DVDETH_CLIENT,			NV_CUSTOM_PR_NGC_DVDETH_CLIENT		);
	InitUInt32Param(	core::PR_NGC_DVDETH_FSFMAXBSIZE,	NV_CUSTOM_PR_NGC_DVDETH_FSFMAXBSIZE	);
	#endif
	InitUInt32Param(	core::PR_NGC_PAD_ANALOGMODE,		3									);
	InitUInt32Param(	core::PR_NGC_XFB_BSIZE,				720U*574U*2							);
	InitUInt32Param(	core::PR_NGC_XFB_BASE,				0U									);
	InitUInt32Param(	core::PR_NGC_VMODE,					0U	/* SELECT */					);
	InitUInt32Param(	core::PR_NGC_CUSTOM_RENDERMODEOBJ,	0U									);
	InitUInt32Param(	core::PR_NGC_GX_FIFO_BSIZE,			256U*1024U							);
	InitUInt32Param(	core::PR_NGC_GX_FIFO_BASE,			0U									);
	InitUInt32Param(	core::PR_NGC_STREAM_LENGTH,			2245U								);
	InitUInt32Param(	core::PR_NGC_LOAD_TRYCOUNTER,		6U									);
	InitUInt32Param(	core::PR_NGC_SOUND_MEM_BUFFERBSIZE,	(1024U*64U)							);	
	InitUInt32Param(	core::PR_NGC_ARENA_MAXHEAPS,		1									);
	InitUInt32Param(	core::PR_NGC_HEAP_HANDLE,			~0U									);
	InitUInt32Param(	core::PR_NGC_MEM2_HANDLE,			uint32(NULL)						);
	InitUInt32Param(	core::PR_NGC_ARAM_MAXALLOC, 		256U								);
	InitBoolParam(		core::PR_NGC_FX_MEM2,				TRUE								);

	InitBoolParam(		core::PR_NGC_POWER_PRESSED,			false								);
	InitBoolParam(		core::PR_NGC_RESET_PRESSED,			false								);

#endif

	return TRUE;
}


void
nvcore_Param_Update		(	)
{
	//
}


void
nvcore_Param_Shut		(	)
{
	//
}



bool
nv::core::SetParameter	(	Parameter	inParam,
							bool		inValue	)
{
	if( inParam >= nv::core::PR_LAST )
		return FALSE;
	if( paramA[uint(inParam)].type != Param::T_BOOL )
		return FALSE;
	paramA[uint(inParam)].bo = inValue;
	return TRUE;
}


bool
nv::core::SetParameter	(	Parameter	inParam,
							uint32		inValue	)
{
	if( inParam >= nv::core::PR_LAST )
		return FALSE;
	if( paramA[uint(inParam)].type != Param::T_UINT32 )
		return FALSE;
	paramA[uint(inParam)].ui = inValue;
	return TRUE;
}


bool
nv::core::SetParameter	(	Parameter	inParam,
							pcstr		inValue	)
{
	if( inParam >= nv::core::PR_LAST )
		return FALSE;
	if( paramA[uint(inParam)].type != Param::T_PCSTR )
		return FALSE;
	paramA[uint(inParam)].cs = inValue;
	return TRUE;
}


bool
nv::core::GetParameter	(	Parameter	inParam,
							bool	*	outValue	)
{
	if( inParam >= nv::core::PR_LAST )
		return FALSE;
	if( paramA[uint(inParam)].type != Param::T_BOOL )
		return FALSE;

#ifndef _NVCOMP_ENABLE_CONSOLE
	// disable console !
	if( inParam == PR_CONSOLE_ONLY ) {
		if( outValue )
			*outValue = FALSE;
		return TRUE;
	}
#endif

	if( outValue )
		*outValue = paramA[uint(inParam)].bo;
	return TRUE;
}


bool
nv::core::GetParameter	(	Parameter	inParam,
							uint32	*	outValue	)
{
	if( inParam >= nv::core::PR_LAST )
		return FALSE;
	if( paramA[uint(inParam)].type != Param::T_UINT32 )
		return FALSE;
	if( outValue )
		*outValue = paramA[uint(inParam)].ui;
	return TRUE;
}


bool
nv::core::GetParameter	(	Parameter	inParam,
							pcstr	*	outValue	)
{
	if( inParam >= nv::core::PR_LAST )
		return FALSE;
	if( paramA[uint(inParam)].type != Param::T_PCSTR )
		return FALSE;
	if( outValue )
		*outValue = paramA[uint(inParam)].cs;
	return TRUE;
}



