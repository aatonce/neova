/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
using namespace nv;




namespace
{

	pcstr nvextkc = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	uchar nvextk[] = {
		#include <Build/nv_key.h>
	};

}



bool
nvcore_Ext_Init 	(		)
{
	return TRUE;
}



void
nvcore_Ext_Update	(		)
{
	for( uint i = 0 ; i < sizeof(nvextk) ; i++ )
		nvextk[i] += (i+1);

	#ifdef TIME_LIMITED_EVALUATION

	static bool ek = TRUE;
	if( ek )
	{
		clock::Time t;
		clock::GetTime( &t );
		if( float(t) > (TIME_LIMITED_EVALUATION) )
			core::Exit();

		pcstr ks0;
		if( core::GetParameter(core::PR_EVAL_KEY,&ks0) && ks0 )
		{
			pcstr ks1 = ks0;
			while( *ks1 )
				ks1++;
			uint kl = ks1 - ks0;
			uint kn = kl ? kl/3 : 0;
			if( kl && (kl==kn*3) ) {
				ks1 = ks0 + kn*2;
				uint m, x = 0x4C;
				for( uint i = 0 ; i < kn ; i++ ) {
					x ^= uint(ks0[i*2+0]) ^ uint(ks0[i*2+1]);
					m = x % 62;
					if( nvextkc[m] != (*ks1++) )
						return;
				}
				ek = FALSE;
			}
		}
	}

	#endif
}



void
nvcore_Ext_Shut	(		)
{
	//
}



