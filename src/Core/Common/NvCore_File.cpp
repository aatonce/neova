/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <Kernel/Common/NvkCore_File.h>
using namespace nv;



#define		AFS_MAX				16
#define		AFS_DEV_LEN			16

#define 	CHECK_STATUS_TTL	999.f



namespace
{

	struct RegAbstractFS
	{
		file::AbstractFS*	afs;
		char				devname[AFS_DEV_LEN+1];
		bool				enabled;
	};

	int						reg_afs_nb;
	RegAbstractFS			reg_afs[AFS_MAX];
	file::FullnameHook		fullname_hook;


	pcstr	_ExtractDevice		(	pcstr	inPath	)
	{
		static char s_devname[AFS_DEV_LEN+1];
		if( !inPath )	return NULL;
		pcstr sep = libc::Strchr( inPath, ':' );
		if( !sep )	return NULL;
		NV_ASSERT( sep >= inPath );
		int dev_len = sep - inPath;
		if( dev_len > AFS_DEV_LEN )	return NULL;
		libc::Zero( s_devname );
		libc::Strncpy( s_devname, inPath, dev_len );
		return s_devname;
	}

	pcstr	_ExtractFilename	(	pcstr	inPath	)
	{
		if( !inPath )	return NULL;
		pcstr sep = libc::Strchr( inPath, ':' );
		if( !sep )		return inPath;
		NV_ASSERT( sep >= inPath );
		int dev_len = sep - inPath;
		if( dev_len > AFS_DEV_LEN )	return inPath;
		return sep+1;
	}

	bool	_ExtractFullAFS		(	pcstr				inPath,
									file::AbstractFS*&	outAFS,
									pcstr&				outFilename		)
	{
		pcstr fullname = file::GetFullname( inPath );
		if( !fullname )	return FALSE;
		pcstr devname  = _ExtractDevice( fullname );
		int   afsIdx   = file::FindAbstractFS( devname );
		if( afsIdx>0 ) {
			// remove device for identified afs !
			outFilename = _ExtractFilename( fullname );
		} else {
			// keep whole fullname for the default system io !
			afsIdx      = 0;
			outFilename	= fullname;
		}
		outAFS		   = file::GetAbstractFS( afsIdx );
		return (outAFS && outFilename);
	}



	// read process states
	enum RRState
	{	
		RR_NONE,
		RR_READING,
	};

	// current state
	file::AbstractFS*		curAFS;
	uint32					curFBSize;
	uint					curLockCpt;
	uint					qSize;
	uint16					qPrlMask;
	file::ReadRequest*		qPrlHead[ 16 ];
	file::ReadRequest*		qPrlTail[ 16 ];
	file::ReadRequest*		curRR;
	RRState					curRRState;
	uint					RRCompletedCpt;

}



bool
nv::file::ReadRequest::Setup(	pvoid		inBufferPtr,
								uint32		inBSize,
								uint32		inBOffset0,
								uint32		inBOffset1,
								uint32		inBOffset2,
								uint32		inBOffset3	)
{
	if( !IsReady() ) {
		DebugPrintf( "ReadRequest:Setup : Not ready !\n" );
		return FALSE;
	}

	if( uint32(inBufferPtr) & (NVHW_FILE_BALIGN-1) ) {
		DebugPrintf( "ReadRequest:Setup : Not aligned pointer 0x%08x !\n", uint(inBufferPtr) );
		return FALSE;
	}

	if( inBSize & (NVHW_FILE_BALIGN-1) ) {
		DebugPrintf( "ReadRequest:Setup : Not aligned bsize 0x%x !\n", uint(inBSize) );
		return FALSE;
	}

	if( inBOffset1 == ~0U )		inBOffset1 = inBOffset0;
	if( inBOffset2 == ~0U )		inBOffset2 = inBOffset0;
	if( inBOffset3 == ~0U )		inBOffset3 = inBOffset0;

	bSize		= inBSize;
	bOffset[0]	= inBOffset0;
	bOffset[1]	= inBOffset1;
	bOffset[2]	= inBOffset2;
	bOffset[3]	= inBOffset3;
	bufferPtr	= inBufferPtr;
	sysFlags	= 0;
	stt			= IDLE;

	return TRUE;
}



bool
nvcore_File_Init()
{
	reg_afs_nb		= 0;
	fullname_hook	= NULL;
	curAFS			= NULL;
	qSize			= 0;
	RRCompletedCpt	= 0;
	qPrlMask		= 0;
	curRR			= NULL;
	curRRState		= RR_NONE;
	for( uint i = 0 ; i < 16 ; i++ )
		qPrlHead[i] = qPrlTail[i] = NULL;

	file::AbstractFS* systemIO = nv::file::GetSystemIOAbstractFS();
	if( !systemIO )				return FALSE;
	if( !systemIO->Init() )		return FALSE;

	file::RegisterAbstractFS( systemIO, NULL );

	return TRUE;
}



void
nvcore_File_Shut()
{
	if( curAFS ) {
		curAFS->Sync();
		curAFS->Close();
		curAFS = NULL;
	}

	for( int i = 0 ; i < reg_afs_nb ; i++ ) {
		file::AbstractFS* afs = reg_afs[i].afs;
		NV_ASSERT( afs );
		afs->Shut();
	}

	fullname_hook = NULL;
	reg_afs_nb = 0;
}



uint
nvcore_File_StatusError ( )
{
	uint mediaStatus = -1;
	core::GetParameter( core::PR_MEDIA_STATUS_ERROR, &mediaStatus );
	return mediaStatus;
}




nv::file::FullnameHook
nv::file::SetFullnameHook	(	FullnameHook	inHook		)
{
	if( !inHook )
		return NULL;
	FullnameHook old = fullname_hook;
	fullname_hook = inHook;
	return old;
}


bool
nv::file::RegisterAbstractFS	(	AbstractFS*		inAbstractFS,
									pcstr			inDevice			)
{
	if( !inAbstractFS )
		return FALSE;

	if( reg_afs_nb == AFS_MAX )
		return FALSE;

	if( inDevice ) {
		if( libc::Strlen(inDevice) > AFS_DEV_LEN )	return FALSE;
		if( libc::Strchr(inDevice,':') )			return FALSE;
	}

	RegAbstractFS* regAFS = & reg_afs[ reg_afs_nb ];
	regAFS->afs     = inAbstractFS;
	regAFS->enabled = TRUE;
	if( inDevice )	libc::Strcpy( regAFS->devname, inDevice );
	else			regAFS->devname[0] = 0;

	reg_afs_nb++;
	return TRUE;
}


int
nv::file::GetNbAbstractFS		(				)
{
	return reg_afs_nb;
}


int
nv::file::FindAbstractFS		(	pcstr		inDevice	)
{
	for( int i = 0 ; i < reg_afs_nb ; i++ ) {
		char* afs_devname = reg_afs[i].devname;
		if( !inDevice && *afs_devname==0 )							return i;
		if( inDevice && libc::Stricmp(inDevice,afs_devname)==0 )	return i;
	}
	// not found !
	return -1;
}


nv::file::AbstractFS*
nv::file::GetAbstractFS		(	int				inIndex		)
{
	if( inIndex<0 || inIndex>=reg_afs_nb )
		return NULL;
	return reg_afs[ inIndex ].afs;
}


pcstr
nv::file::GetAbstractFSDevice	(	int			inIndex		)
{
	if( inIndex<0 || inIndex>=reg_afs_nb )
		return NULL;
	return reg_afs[ inIndex ].devname;
}


bool
nv::file::EnableAbstractFS		(	int			inIndex		)
{
	if( inIndex<0 || inIndex>=reg_afs_nb )
		return FALSE;
	reg_afs[ inIndex ].enabled = TRUE;
	return TRUE;
}


bool
nv::file::DisableAbstractFS		(	int			inIndex		)
{
	if( inIndex<0 || inIndex>=reg_afs_nb )
		return FALSE;
	reg_afs[ inIndex ].enabled = FALSE;
	return TRUE;
}


bool
nv::file::IsEnabledAbstractFS	(	int			inIndex		)
{
	if( inIndex<0 || inIndex>=reg_afs_nb )
		return FALSE;
	return reg_afs[ inIndex ].enabled;
}



pcstr
nv::file::GetFullname(	pcstr			inFilename		)
{
	static char fullname[ 256 ];

	if( !inFilename || inFilename[0]==0	)
		return NULL;

	// Has device ?
	if( libc::Strchr(inFilename,':') )
	{
		Strcpy( fullname, inFilename );
	}
	else
	{
		pcstr filePrefix, fileSuffix;
		core::GetParameter( core::PR_FILE_PREFIX, &filePrefix );
		core::GetParameter( core::PR_FILE_SUFFIX, &fileSuffix );
		Strcpy( fullname, filePrefix );
		Strcat( fullname, inFilename );
		Strcat( fullname, fileSuffix );
	}

	// Hook ?
	return fullname_hook ? (*fullname_hook)(fullname) : fullname;
}


nv::file::Status
nv::file::GetStatus		(	)
{
	// Not in use ?
	if( !curAFS )
		return FS_NOT_AVAILABLE;

	uint mediaStatus = nvcore_File_StatusError();
	if( mediaStatus )
		return Status( mediaStatus );

	return curAFS->GetStatus();
}



bool
nv::file::Open	(	pcstr	inFilename	)
{
	// In use ?
	if( curAFS )
	{
		if( curAFS->IsOpened() )	return FALSE;
		curAFS = NULL;
	}

	AbstractFS* afs;
	pcstr		fn;
	if( !_ExtractFullAFS(inFilename,afs,fn) )
		return FALSE;

	if( nvcore_File_StatusError() )
		return FALSE;

	if( !afs->Open(fn,curFBSize) )
		return FALSE;

	curLockCpt		= 0;
	curAFS			= afs;
	qPrlMask		= 0;
	curRR			= NULL;
	curRRState		= RR_NONE;
	RRCompletedCpt	= 0;
	return TRUE;
}


uint32
nv::file::GetSize	(			)
{
	// Not in use ?
	if( !curAFS || !curAFS->IsOpened() )
	{
		curAFS = NULL;
		return 0;
	}
	return curFBSize;
}


bool
nvcore_File_Lock		(			)
{
	// Not in use ?
	if( !curAFS || !curAFS->IsOpened() )
	{
		curAFS = NULL;
		return FALSE;
	}
	curLockCpt++;
	return TRUE;
}


bool
nvcore_File_Unlock	(			)
{
	// Not in use ?
	if( !curAFS || !curAFS->IsOpened() )
	{
		curAFS = NULL;
		return FALSE;
	}
	if( curLockCpt )
		curLockCpt--;
	return TRUE;
}


bool
nv::file::Close	(			)
{
	// Not in use ?
	if( !curAFS || !curAFS->IsOpened() ) {
		curAFS = NULL;
		return TRUE;
	}

	// Busy (queue is not empty) !
	if(	qSize > 0 )
		return FALSE;

	if( curLockCpt > 0 ) {
		DebugPrintf( "<Neova> The current file is locked !\n" );
		return FALSE;
	}

	curAFS->Sync();
	if( !curAFS->Close() )
		return FALSE;

	curAFS		= NULL;
	curRR		= NULL;
	curRRState	= RR_NONE;

	return TRUE;
}


bool
nv::file::AddReadRequest	(	ReadRequest*	inRequest,
								uint8			inPriorityLevel	)
{
	// valid file
	if( GetSize()==0 )
		return FALSE;

	// not pending ?
	if( !inRequest || !inRequest->IsReady() ) {
		DebugPrintf( "AddReadRequest error: request is not ready !\n" );
		return FALSE;
	}

	// RR is valid ?
	// offset+bsize > fileBSize is allowed for 16-bytes bsize alignement !
	if( !inRequest->bufferPtr || !inRequest->bSize ) {
		DebugPrintf( "AddReadRequest error: invalid request size (%d) or buffer ptr (0x%08x)!\n", inRequest->bSize, uint(inRequest->bufferPtr) );
		return FALSE;
	}

	if( uint32(inRequest->bufferPtr) & (NVHW_FILE_BALIGN-1) ) {
		DebugPrintf( "file::AddReadRequest : Not aligned pointer 0x%08x. Request rejected !\n", uint(inRequest->bufferPtr) );
		return FALSE;
	}

	if( inRequest->bSize & (NVHW_FILE_BALIGN-1) ) {
		DebugPrintf( "file::AddReadRequest : Not aligned bsize 0x%x. Request rejected !\n", uint(inRequest->bSize) );
		return FALSE;
	}

	// Greatest priority is 0xF
	if( inPriorityLevel > 0xF )
		inPriorityLevel = 0xF;

	// Queue priority is empty ?
	inRequest->next = NULL;
	if( !qPrlHead[ inPriorityLevel ] )
	{
		qPrlHead[ inPriorityLevel ] 		= inRequest;
		qPrlTail[ inPriorityLevel ]			= inRequest;
	}
	else
	{
		NV_ASSERT( qPrlTail[inPriorityLevel] );
		qPrlTail[ inPriorityLevel ]->next	= inRequest;
		qPrlTail[ inPriorityLevel ]			= inRequest;
	}

	qSize++;
	inRequest->stt = ReadRequest::PENDING;
	return TRUE;
}


bool
nv::file::AbortReadRequest	(	ReadRequest*	inRequest	)
{
	// pending ?
	if( !inRequest || inRequest->IsReady() )
		return FALSE;

	// Queue is empty ?
	if(	qSize==0 )
		return FALSE;

	// Reading in progress => can't abort !
	if( inRequest == curRR )
		return FALSE;

	for( uint i = 0 ; i < 16 ; i++ ) {
		ReadRequest* rr_prev, *rr;
		rr_prev = NULL;
		rr = qPrlHead[i];
		while( rr ) {
			if( rr == inRequest ) {
				if( rr_prev )
					rr_prev->next = rr->next;
				if( qPrlHead[i] == rr )
					qPrlHead[i] = rr->next;
				if( qPrlTail[i] == rr )
					qPrlTail[i] = rr_prev;
				rr->stt = ReadRequest::ABORTED;
				qSize--;
				return TRUE;
			}
			rr_prev = rr;
			rr = rr->next;
		}
	}

	return FALSE;
}


uint
nv::file::GetRequestQueueSize	(		)
{
	return qSize;
}


uint
nv::file::GetRequestCompltdCpt		(		)
{
	return RRCompletedCpt;
}


uint16
nv::file::GetPriorityMask	(		)
{
	// valid file
	if( GetSize()==0 )
		return 0;
	return qPrlMask;
}


void
nv::file::SetPriorityMask	(	uint16	inMask	)
{
	// valid file
	if( GetSize()==0 )
		return;
	qPrlMask = inMask;
}






bool
nv::file::DumpFromFile		(	pcstr			inFilename,
								pvoid&			outBuffer,
								uint&			outBSize		)
{
	AbstractFS* afs;
	pcstr		fn;
	if( !_ExtractFullAFS(inFilename,afs,fn) )	return FALSE;
	return afs->DumpFromFile( fn, outBuffer, outBSize );
}


bool
nv::file::DumpToFile		(	pcstr			inFilename,
								pvoid			inBuffer,
								uint			inBSize			)
{
	if( !inBuffer || !inBSize || !inFilename )
		return FALSE;
	AbstractFS* afs;
	pcstr		fn;
	if( !_ExtractFullAFS(inFilename,afs,fn) )	return FALSE;
	return afs->DumpToFile( fn, inBuffer, inBSize );
}


bool
nv::file::AppendToFile		(	pcstr			inFilename,
								pvoid			inBuffer,
								uint			inBSize			)
{
	if( !inBuffer || !inBSize || !inFilename )
		return FALSE;
	AbstractFS* afs;
	pcstr		fn;
	if( !_ExtractFullAFS(inFilename,afs,fn) )	return FALSE;
	return afs->AppendToFile( fn, inBuffer, inBSize );
}





void
nvcore_File_Update()
{
	// valid file ?
	if( file::GetSize() == 0 )
		return;

	#ifndef _NVCOMP_FAST_CODE
	static clock::Time t0, t1;
	#endif

	for( ;; ) {

		switch( curRRState )
		{

		//
		// Start a new RR ?

		case RR_NONE :
			{
				// Queue is empty ?
				if( qSize == 0 )
					return;

				// Find the next RR
				curRR = NULL;
				for( int i = 15 ; i >= 0 ; i-- ) {
					if( qPrlMask & (1<<i) )
						continue;
					if( qPrlHead[i] ) {
						curRR = qPrlHead[i];
						qPrlHead[i] = curRR->next;
						qSize--;
						break;
					}
				}

				// Found ?
				if( !curRR )
					return;
				curRR->next = NULL;

				uint mediaError = nvcore_File_StatusError();

				// start * READING *
				curRRState = RR_READING;
				bool res =		(mediaError==0)
							&&	curAFS->Read(	curRR->sysFlags,
												curRR->bufferPtr,
												curRR->bSize,
												curRR->bOffset[0],
												curRR->bOffset[1],
												curRR->bOffset[2],
												curRR->bOffset[3]	);

				// Error ?
				if( !res )
				{
					curRR->stt = file::ReadRequest::FAILED;
					curRRState = RR_NONE;
					break;	// loop in for()
				}
				else
				{
					#ifndef _NVCOMP_FAST_CODE
					clock::GetTime( &t0 );
					#endif
					return;
				}
			}

		//
		// Reading ?

		case RR_READING :
			{
				// Finished ?
				if( !curAFS->IsReady() )
				{
				//	Printf( "nv_file_io: read waiting\n" );
					return;
				}

				// * RR is ready *
				NV_ASSERT( curRR );
				if( curAFS->IsSuccess() )
				{
					curRR->stt = file::ReadRequest::COMPLETED;
					RRCompletedCpt++;
					#ifndef _NVCOMP_FAST_CODE	// >= DBGOPT only
				//	clock::GetTime( &t1 );
				//	float rd_rate = float(int(curRR->bSize)) / (t1-t0);
				//	Printf( "<Neova> file: %dKo @ %dKo/s\n", curRR->bSize>>10, uint(rd_rate/1024.f) );
					#endif
				}
				else
				{
					curRR->stt = file::ReadRequest::FAILED;
				}

				// Start a new RR ...
				curRRState = RR_NONE;
				break;	// loop in for()
			}

		} // switch()

	} // for()
}





namespace
{


	void check_exit ( )
	{
		if( core::ExitAsked() )
			core::Exit();
	}


	void delay ( float s )
	{
		clock::Time t0, t1;
		clock::GetTime( &t0 );
		for( ;; )
		{
			clock::GetTime( &t1 );
			float dt = t1 - t0;
			if( dt > s )
				break;
		}
	}



	file::Status async_read_status (		)
	{
		// try to load first bytes !
		NV_ASSERT( file::GetSize() >= NVHW_FILE_BALIGN );

		static file::ReadRequest	rr;
		static clock::Time			rr_stime;
		static uchar				rr_bytes[1024];

		check_exit();
		nvcore_File_Update();

		if( rr.IsReady() )
		{
			clock::Time t;
			clock::GetTime( &t );

			if( rr.IsIdle() || (t-rr_stime)>CHECK_STATUS_TTL )
			{
				// start an async check request ...
				uchar* rr_b0 = RoundX( rr_bytes, NVHW_FILE_BALIGN );
				NV_ASSERT_RESULT( rr.Setup(rr_b0,NVHW_FILE_BALIGN,0) );
				NV_ASSERT_RESULT( file::AddReadRequest(&rr,15) );
				rr_stime = t;
			}
			else
			{
				rr.stt = file::ReadRequest::IDLE;
				return file::GetStatus();
			}
		}

		return file::FS_TIMEOUT;
	}



	file::Status sync_read_status (		)
	{
		// try to load first bytes !
		NV_ASSERT( file::GetSize() >= NVHW_FILE_BALIGN );

		static file::ReadRequest	rr;
		static clock::Time			rr_stime;
		static uchar				rr_bytes[1024];

		while( !rr.IsReady() )
		{
			check_exit();
			nvcore_File_Update();
			delay( 0.1f );
		}

		// start an async check request ...
		uchar* rr_b0 = RoundX( rr_bytes, NVHW_FILE_BALIGN );
		NV_ASSERT_RESULT( rr.Setup(rr_b0,NVHW_FILE_BALIGN,0) );
		NV_ASSERT_RESULT( file::AddReadRequest(&rr) );

		while( !rr.IsReady() )
		{
			check_exit();
			nvcore_File_Update();
			delay( 0.1f );
		}

		return file::GetStatus();
	}
}





nv::file::Status
nv::file::CheckStatus	(	)
{
	if( file::GetSize() )
	{

		return sync_read_status();

	}
	else
	{

		pcstr status_file;
		core::GetParameter( core::PR_MEDIA_STATUS_FILE, &status_file );

		if( status_file && file::Open(status_file) )
		{
			file::Status stt = sync_read_status();
			file::Close();
			return stt;
		}

		return FS_NOT_AVAILABLE;
	}
}




