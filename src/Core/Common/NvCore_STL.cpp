/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>



//
///// base_vector ///////////////////////////////////////////////////



uint32
nv::base_vector::_max_size	(	) const
{
	NV_ASSERT( vtbsize );
	return ( 0x7FFFFFFF / vtbsize );
}


void
nv::base_vector::_mem_reserve(	uint32	inBSize	)
{
	NV_ASSERT( (inBSize%vtbsize) == 0 );
	NV_ASSERT( (inBSize/vtbsize) <= _max_size() );

	uint32 bsize = uint32(vend - vfirst);
	if( bsize == inBSize )
		return;

	if( inBSize == 0)
	{
		_free();
	}
	else
	{
		uint32 bpos = uint32(vlast-vfirst);
		if( vfirst )	vfirst = (uint8*) mem::Realloc( vfirst, inBSize, nv::mem::AllocFlags(vmflags) );
		else			vfirst = (uint8*) mem::Alloc( inBSize, nv::mem::AllocFlags(vmflags) );
		NV_ASSERT( vfirst );
		vlast  = vfirst + Min( bpos, inBSize );
		vend   = vfirst + inBSize;
	}

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_reserve(	uint	inSize	)
{
	_mem_reserve( inSize * vtbsize );
}


void
nv::base_vector::_init	(	uint		inTBSize,
							uint		inAllocFlags	)
{
	NV_ASSERT( inTBSize );
	NV_ASSERT( inAllocFlags <= 65535 );

	vtbsize = inTBSize;
	vmflags	= inAllocFlags;
	vfirst  = vlast = vend = NULL;

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_init	(	uint		inTBSize,
							uint		inAllocFlags,
							uint		inSize		)
{
	NV_ASSERT( inTBSize );

	_init( inTBSize, inAllocFlags );

	if( inSize > 0 ) {
		_reserve( inSize );
		vlast = vend;
	}

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_init	(	uint		inTBSize,
							uint		inAllocFlags,
							uint		inSize,
							uint8*		inT		)
{
	NV_ASSERT( inT );

	_init( inTBSize, inAllocFlags, inSize );
	if( inSize )
		Memncpy( vfirst, inT, vtbsize, inSize );

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_init	(	uint		inTBSize,
							uint		inAllocFlags,
							uint8*		inFirst,
							uint8*		inLast		)
{
	NV_ASSERT( inLast>=inFirst );

	_init( inTBSize, inAllocFlags );

	if( inFirst && inLast && inLast>inFirst ) {
		uint bsize = uint32(inLast-inFirst);
		_mem_reserve( bsize );
		Memcpy( vfirst, inFirst, bsize );
		vlast = vend;
	}

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_init	(	uint inAllocFlags, base_vector* inRef )
{
	NV_ASSERT( inRef );

	_init( inRef->vtbsize, inAllocFlags, inRef->vfirst, inRef->vlast );

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_free	(		)
{
	if( vfirst ) {
		mem::Free( vfirst );
		vfirst = vlast = vend = NULL;
	}
	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_push_back	(	uint8*	inT	)
{
	NV_ASSERT( inT );

	_insert( vlast, 1, inT );

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


uint8*
nv::base_vector::_alloc_back	(	uint	inSize	)
{
	uint8* pos = _mem_insert( vlast, inSize*vtbsize );
	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
	return pos;
}


void
nv::base_vector::_pop_back	(				)
{
	if( vlast > vfirst )
		vlast -= vtbsize;

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_swap	(	base_vector * inRef	)
{
	Swap( vfirst,	inRef->vfirst	);
	Swap( vlast,	inRef->vlast	);
	Swap( vend,		inRef->vend		);
	Swap( vtbsize,	inRef->vtbsize	);
	Swap( vmflags,	inRef->vmflags	);
	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


uint8*
nv::base_vector::_mem_insert	(	uint8*		inPos,
									uint32		inBSize		)
{
	NV_ASSERT( inBSize>0 );
	NV_ASSERT( (inBSize%vtbsize) == 0 );
	NV_ASSERT( (inPos>=vfirst) );
	NV_ASSERT( (inPos<=vlast) );
	NV_ASSERT( (uint32(inPos-vfirst)%vtbsize) == 0 );

	uint32 bremain = uint32(vend-vlast);
	if( bremain < inBSize )
	{
		uint32 max_bsize = _max_size() * vtbsize;
		uint32 new_bsize = uint32(vend-vfirst);
		uint32 req_bsize = uint32(vend-vfirst) + inBSize;
		NV_ASSERT( req_bsize <= max_bsize );
		if( new_bsize == 0 )
			new_bsize = vtbsize;
		while( req_bsize > new_bsize )
			if( new_bsize < max_bsize/2 )
				new_bsize *= 2;
			else
				new_bsize = max_bsize;

		uint32 bpos = uint32(inPos-vfirst);
		_mem_reserve( new_bsize );
		inPos = vfirst + bpos;
	}
	NV_ASSERT( uint32(vend-vlast) >= inBSize );

	uint32 btomove = uint32(vlast-inPos);
	if( btomove )
		Memmove( inPos+inBSize, inPos, btomove );

	vlast += inBSize;

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
	return inPos;
}


uint8*
nv::base_vector::_insert(	uint8*		inPos,
							uint8*		inT		)
{
	NV_ASSERT( inT );
	NV_ASSERT( inPos>=vfirst );
	NV_ASSERT( inPos<=vlast );

	if( inPos>=vfirst && inPos<=vlast ) {
		inPos = _mem_insert( inPos, vtbsize );
		Memcpy( inPos, inT, vtbsize );
	}

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
	return inPos;
}


void
nv::base_vector::_insert(	uint8*		inPos,
							uint8*		inFirst,
							uint8*		inLast	)
{
	NV_ASSERT( inPos>=vfirst );
	NV_ASSERT( inPos<=vlast );
	NV_ASSERT( inLast>=inFirst );

	if(		inPos>=vfirst && inPos<=vlast
		&&	inFirst && inLast && inLast>inFirst )
	{
		uint bsize = uint32(inLast-inFirst);
		inPos = _mem_insert( inPos, bsize );
		Memcpy( inPos, inFirst, bsize );
	}

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


void
nv::base_vector::_insert(	uint8*		inPos,
							uint		inSize,
							uint8*		inT		)
{
	NV_ASSERT( inT );
	NV_ASSERT( inPos>=vfirst );
	NV_ASSERT( inPos<=vlast );

	if(	inPos>=vfirst && inPos<=vlast && inSize )
	{
		inPos = _mem_insert( inPos, inSize*vtbsize );
		Memncpy( inPos, inT, vtbsize, inSize );
	}

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}


uint8*
nv::base_vector::_mem_erase	(	uint8*		inPos,
								uint32		inBSize	)
{
	NV_ASSERT( inBSize>0 );
	NV_ASSERT( (inBSize%vtbsize) == 0 );
	NV_ASSERT( (inPos>=vfirst) );
	NV_ASSERT( (inPos<=vlast) );
	NV_ASSERT( (uint32(inPos-vfirst)%vtbsize) == 0 );

	uint32 btomove = uint32( vlast - inPos );
	inBSize  = Min( inBSize, btomove );
	btomove -= inBSize;

	if( btomove )
	{
		NV_ASSERT( inBSize > 0 );
		Memmove( inPos, inPos+inBSize, btomove );
	}
	vlast -= inBSize;

	NV_ASSERT( vlast>=inPos );
	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
	return inPos;
}


uint8*
nv::base_vector::_erase	(	uint8*		inPos	)
{
	NV_ASSERT( inPos>=vfirst );
	NV_ASSERT( inPos<=vlast );

	if(	inPos>=vfirst && inPos<=vlast )
		inPos = _mem_erase( inPos, vtbsize );

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
	return inPos;
}


uint8*
nv::base_vector::_erase	(	uint8*		inFirst,
							uint8*		inLast	)
{
	NV_ASSERT( inFirst>=vfirst );
	NV_ASSERT( inFirst<=vlast  );
	NV_ASSERT( inLast>=vfirst  );
	NV_ASSERT( inLast<=vlast   );

	if(		inFirst>=vfirst && inFirst<=vlast
		&&	inLast>=vfirst  && inLast<=vlast
		&&	inLast>inFirst					)
	{
		uint32 bsize = uint32(inLast-inFirst);
		inFirst = _mem_erase( inFirst, bsize );
	}

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
	return inFirst;
}


void
nv::base_vector::_resize(	uint		inSize,
							uint8*		inT		)
{
	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
	uint32 cur_bsize = uint32(vlast-vfirst);
	uint32 req_bsize = inSize * vtbsize;

	if( cur_bsize >= req_bsize )
	{
		vlast = vfirst + req_bsize;
	}
	else
	{
		uint32 size = (req_bsize - cur_bsize) / vtbsize;
		_insert( vlast, size, inT );
	}

	NV_ASSERT( vfirst <= vlast );
	NV_ASSERT( vlast <= vend );
}




//
/// base_tree ///////////////////////////////////////////////////


void
nv::base_tree::_init	(	 )
{
	_initNode( &_nil );
	_nil._color = NC_BLACK;
	_clear();
}


void
nv::base_tree::_shut	(	)
{
	//
}


void
nv::base_tree::_clear()
{
	_first = NULL;
	_last  = NULL;
	_root  = &_nil;
	_size  = 0;
}


nv::base_tree::_Node*
nv::base_tree::_insert(	_Node*	node	)
{
	if( !node )	return NULL;
	_initNode( node );
	node = _doInsert( node, TRUE );
	if( node )	_size++;
	return node;
}


nv::base_tree::_Node*
nv::base_tree::_insertUnique(	_Node*	node	)
{
	if( !node )	return NULL;
	_initNode( node );
	node = _doInsert( node, FALSE );
	if( node )	_size++;
	return node;
}


void
nv::base_tree::_erase(	_Node*	node	)
{
	if( !node )	return;
	_doErase ( node );
	NV_ASSERT( _size );
	_size--;
}


nv::base_tree::_Node*
nv::base_tree::_find( _Node* ref )
{
	_Node* y = _root;
	int c;
	while( y != &_nil ) {
		c = _compareNode( ref, y );
		if( c == 0 ) return y;
		if( c < 0 )	y = y->_left;
		else        y = y->_right;
	}
	return NULL;
}


nv::base_tree::_Node*
nv::base_tree::_nextUnique( _Node* node )
{
	if( !node )	return NULL;
	if( node->_color == NC_DUP )	node = _find( node );
	if( node )						node = _treeNext( node );
	return node;
}

	
nv::base_tree::_Node*
nv::base_tree::_prevUnique( _Node* node )
{
	if( !node )	return NULL;
	if( node->_color == NC_DUP )	node = _find( node );
	if( node )						node = _treePrev( node );
	return node;
}


void
nv::base_tree::_rotateRight( _Node* x )
{
	NV_ASSERT( x );
	_Node* y = x->_left;
	x->_left = y->_right;
	if( y->_right != &_nil )	y->_right->_parent = x;
	if( y != &_nil )			y->_parent = x->_parent;
	if( x->_parent ) {
		if( x == x->_parent->_right )	x->_parent->_right = y;
		else                	        x->_parent->_left = y;
	} else {
		_root = y;
	}
	y->_right = x;
	if( x != &_nil) x->_parent = y;
}

	
void
nv::base_tree::_rotateLeft( _Node* x )
{
	NV_ASSERT( x );
	_Node* y = x->_right;
	x->_right = y->_left;
	if( y->_left != &_nil )	y->_left->_parent = x;
	if( y != &_nil )		y->_parent = x->_parent;
	if( x->_parent )	{
		if( x == x->_parent->_left )  x->_parent->_left = y;
		else                       	  x->_parent->_right = y;
	} else {
		_root = y;
	}
	y->_left = x;
	if( x != &_nil ) x->_parent = y;
}


void
nv::base_tree::_fixInsert( _Node* x )
{
	NV_ASSERT( x );
	while( x != _root && x->_parent->_color == NC_RED )
	{
		if( x->_parent == x->_parent->_parent->_left )
		{
			_Node* y = x->_parent->_parent->_right;
			if( y->_color == NC_RED ) {
				x->_parent->_color = NC_BLACK;
				y->_color = NC_BLACK;
				x->_parent->_parent->_color = NC_RED;
				x = x->_parent->_parent;
			} else {
				if( x == x->_parent->_right ) {
					x = x->_parent;
					_rotateLeft( x );
				}
				x->_parent->_color = NC_BLACK;
				x->_parent->_parent->_color = NC_RED;
				_rotateRight( x->_parent->_parent );
			}
		}
		else
		{
			_Node* y = x->_parent->_parent->_left;
			if( y->_color == NC_RED )
			{
				x->_parent->_color = NC_BLACK;
				y->_color = NC_BLACK;
				x->_parent->_parent->_color = NC_RED;
				x = x->_parent->_parent;
			} else {
				if( x == x->_parent->_left ) {
					x = x->_parent;
					_rotateRight( x );
				}
				x->_parent->_color = NC_BLACK;
				x->_parent->_parent->_color = NC_RED;
				_rotateLeft( x->_parent->_parent );
			}
		}
	}
	_root->_color = NC_BLACK;
}


nv::base_tree::_Node*
nv::base_tree::_doInsert( _Node* x, bool allowMulti )
{
	NV_ASSERT( x );
	_Node *y=_root, *p=NULL;
	int c = 0;

	while( y != &_nil ) {
		c = _compareNode( x, y );
		if( c == 0 ) {
			if( !allowMulti )	return NULL;
			x->_color = NC_DUP;
			y = _treeNext( y );
			if( y ) _dllInsertBefore( y, x );
			else	_dllAddAfter( _last, x );
			return x;
		}
		p = y;
		y = (c < 0) ? y->_left : y->_right;
	}

	if( p ) {
		x->_parent = p;
		if( c < 0 ) {
			_dllInsertBefore( p, x );
			p->_left = x;
		} else {
			y = _treeNext(p);
			if( y ) _dllInsertBefore( y, x );
			else    _dllAddAfter( _last, x );
			p->_right = x;
		}
	} else {
		_root = x;
		x->_color = NC_BLACK;
		_first = x;
		_last  = x;
	}

	_fixInsert( x );
	return x;
}


void
nv::base_tree::_fixErase( _Node* x )
{
	NV_ASSERT( x );
	while( x != _root && x->_color == NC_BLACK) {
		if( x == x->_parent->_left )
		{
			_Node* w = x->_parent->_right;
			if( w->_color == NC_RED ) {
				w->_color = NC_BLACK;
				x->_parent->_color = NC_RED;
				_rotateLeft( x->_parent );
				w = x->_parent->_right;
			}
			if( w->_left->_color == NC_BLACK && w->_right->_color == NC_BLACK ) {
				w->_color = NC_RED;
				x = x->_parent;
			}
			else {
				if( w->_right->_color == NC_BLACK ) {
					w->_left->_color = NC_BLACK;
					w->_color = NC_RED;
					_rotateRight( w );
					w = x->_parent->_right;
				}
				w->_color = x->_parent->_color;
				x->_parent->_color = NC_BLACK;
				w->_right->_color = NC_BLACK;
				_rotateLeft( x->_parent );
				x = _root;
			}
		}
		else
		{
			_Node* w = x->_parent->_left;
			if( w->_color == NC_RED ) {
				w->_color = NC_BLACK;
				x->_parent->_color = NC_RED;
				_rotateRight( x->_parent );
				w = x->_parent->_left;
			}
			if( w->_right->_color == NC_BLACK && w->_left->_color == NC_BLACK )	{
				w->_color = NC_RED;
				x = x->_parent;
			}
			else {
				if( w->_left->_color == NC_BLACK ) {
					w->_right->_color = NC_BLACK;
					w->_color = NC_RED;
					_rotateLeft( w );
					w = x->_parent->_left;
				}
				w->_color = x->_parent->_color;
				x->_parent->_color = NC_BLACK;
				w->_left->_color = NC_BLACK;
				_rotateRight( x->_parent );
				x = _root;
			}
		}
	}
	x->_color = NC_BLACK;
}


void
nv::base_tree::_doErase( _Node* x )
{
	NV_ASSERT( x );
	if( x->_color == NC_DUP ) {
		_dllErase( x );
		return;
	}

	if( x->_next && x->_next->_color == NC_DUP ) {
		_replaceTreeNode( x, x->_next );
		_dllErase( x );
		return;
	}

	_dllErase( x );

	_Node *y, *z;
	bool balance;

	if( x->_left == &_nil || x->_right == &_nil )	y = x;
	else										y = _findMin( x->_right );

	if( y->_left != &_nil )	z = y->_left;
	else					z = y->_right;

	z->_parent = y->_parent;
	if( y->_parent ) {
		if( y == y->_parent->_left )  y->_parent->_left  = z;
		else                        y->_parent->_right = z;
	} else {
		_root = z;
	}

	balance = ( y->_color == NC_BLACK );
	if( y != x ) _replaceTreeNode( x, y );
	if( balance ) _fixErase( z );
}


void
nv::base_tree::_dllErase( _Node* x )
{
	NV_ASSERT( x );
	if( x->_prev )   x->_prev->_next = x->_next;
	else            _first = x->_next;
	if( x->_next )   x->_next->_prev = x->_prev;
	else            _last = x->_prev;
}

	
void
nv::base_tree::_dllInsertBefore( _Node* x, _Node* y )
{
	NV_ASSERT( x );
	NV_ASSERT( y );
	y->_prev = x->_prev;
	if( y->_prev )   y->_prev->_next = y;
	else            _first = y;
	y->_next = x;
	x->_prev = y;
}


void
nv::base_tree::_dllAddAfter( _Node* x, _Node* y )
{
	NV_ASSERT( x );
	NV_ASSERT( y );
	y->_next = x->_next;
	if( y->_next )   y->_next->_prev = y;
	else            _last = y;
	y->_prev = x;
	x->_next = y;
}


void
nv::base_tree::_initNode( _Node* x )
{
	NV_ASSERT( x );
	x->_color  = NC_RED;
	x->_parent = NULL;
	x->_left   = &_nil;
	x->_right  = &_nil;
	x->_next   = NULL;
	x->_prev   = NULL;
}


nv::base_tree::_Node*
nv::base_tree::_treeNext( _Node* x )
{
	NV_ASSERT( x );
	if( x->_right != &_nil )		return _findMin( x->_right );
	while( x->_parent ) {
		if( x == x->_parent->_left )	break;
		x = x->_parent;
	}
	return x->_parent;
}


nv::base_tree::_Node*
nv::base_tree::_treePrev( _Node* x )
{
	NV_ASSERT( x );
	if( x->_left != &_nil )		return _findMax( x->_left );
	while( x->_parent ) {
		if( x == x->_parent->_right )	break;
		x = x->_parent;
	}
	return x->_parent;
}


nv::base_tree::_Node*
nv::base_tree::_findMin( _Node* x )
{
	NV_ASSERT( x );
	while( x->_left != &_nil )	x = x->_left;
	return x;
}


nv::base_tree::_Node*
nv::base_tree::_findMax( _Node* x )
{
	NV_ASSERT( x );
	while( x->_right != &_nil )	x = x->_right;
	return x;
}


void
nv::base_tree::_replaceTreeNode( _Node* x, _Node* y )
{
	NV_ASSERT( x );
	NV_ASSERT( y );
	y->_color  = x->_color;
	y->_left   = x->_left;
	y->_right  = x->_right;
	y->_parent = x->_parent;
	if( y->_parent ) {
		if (y->_parent->_left == x )  y->_parent->_left  = y;
		else                        y->_parent->_right = y;
	} else {
		_root = y;
	}
	y->_left->_parent  = y;
	y->_right->_parent = y;
}


