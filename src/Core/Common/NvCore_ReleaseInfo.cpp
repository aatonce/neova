/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Nova.h>
#include <Build/nv_release_version.h>




uint
nv::core::GetReleaseNumber		(	)
{
#if defined(_EE)
	return NVPS2_RELEASE_NUMBER;
#elif defined(_PSP) 
	return NVPSP_RELEASE_NUMBER;
#elif defined(_MSC_VER) || defined(_WIN32) || defined(_DX)
	return NVDX_RELEASE_NUMBER;
#elif defined(_NGC)
	return NVNGC_RELEASE_NUMBER;	
#endif
}


uint
nv::core::GetReleaseRevision	(	)
{
	return NV_RELEASE_REVISION;
}


uint
nv::core::GetReleaseVersion		(	)
{
	return ( GetReleaseNumber()<<16 ) | GetReleaseRevision();
}


pcstr
nv::core::GetDebugLevel	(	)
{
	#if defined( _FINAL )
		return "final";
	#elif defined( _MASTER )
		return "master";
	#elif defined( _RELEASE )
		return "release";
	#elif defined( _DEBUG_OPT )
		return "debugopt";
	#elif defined( _DEBUG )
		return "debug";
	#else
		#error "missing definitions !"
		return NULL;
	#endif
}


