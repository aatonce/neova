/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>




//
// PSM


uint32
GetPSM( Psm inToPSM, uint8 inR, uint8 inG, uint8 inB, uint8 inA )
{
	if( inToPSM==PSM_RGBA32 )	return (uint32(inR)<<24) | (uint32(inG)<<16) | (uint32(inB)<<8) | (uint32(inA)<<0);
	if( inToPSM==PSM_ABGR32 )	return (uint32(inA)<<24) | (uint32(inB)<<16) | (uint32(inG)<<8) | (uint32(inR)<<0);
	if( inToPSM==PSM_ARGB32 )	return (uint32(inA)<<24) | (uint32(inR)<<16) | (uint32(inG)<<8) | (uint32(inB)<<0);
	if( inToPSM==PSM_BGRA32 )	return (uint32(inB)<<24) | (uint32(inG)<<16) | (uint32(inR)<<8) | (uint32(inA)<<0);
	return 0U;
}

uint32
GetPSM(	Psm inToPSM, const Vec3& inColorFactor	)
{
	uint8 r8 = uint( Clamp(inColorFactor.x,0.f,1.f) * 255.f );
	uint8 g8 = uint( Clamp(inColorFactor.y,0.f,1.f) * 255.f );
	uint8 b8 = uint( Clamp(inColorFactor.z,0.f,1.f) * 255.f );
	return GetPSM( inToPSM, r8, g8, b8, 255 );
}

uint32
GetPSM(	Psm inToPSM, const Vec4& inColorFactor	)
{
	uint8 r8 = uint( Clamp(inColorFactor.x,0.f,1.f) * 255.f );
	uint8 g8 = uint( Clamp(inColorFactor.y,0.f,1.f) * 255.f );
	uint8 b8 = uint( Clamp(inColorFactor.z,0.f,1.f) * 255.f );
	uint8 a8 = uint( Clamp(inColorFactor.w,0.f,1.f) * 255.f );
	return GetPSM( inToPSM, r8, g8, b8, a8 );
}

Vec3*
ExtractRGB( Vec3& outColorFactor, Psm inFromPSM, uint32 inColor	)
{
	uint32 rgba = ConvertPSM( PSM_RGBA32, inFromPSM, inColor );  
	uint8  r8 = (rgba>>24) & 0xFF;
	uint8  g8 = (rgba>>16) & 0xFF;
	uint8  b8 = (rgba>> 8) & 0xFF;
	const float oo255 = 1.f / 255.f;
	outColorFactor.x = float(int(r8)) * oo255;
	outColorFactor.y = float(int(g8)) * oo255;
	outColorFactor.z = float(int(b8)) * oo255;
	return &outColorFactor;
}

Vec4*
ExtractRGBA( Vec4& outColorFactor, Psm inFromPSM, uint32 inColor )
{
	uint32 rgba = ConvertPSM( PSM_RGBA32, inFromPSM, inColor );  
	uint8  r8 = (rgba>>24) & 0xFF;
	uint8  g8 = (rgba>>16) & 0xFF;
	uint8  b8 = (rgba>> 8) & 0xFF;
	uint8  a8 = (rgba>> 0) & 0xFF;
	const float oo255 = 1.f / 255.f;
	outColorFactor.x = float(int(r8)) * oo255;
	outColorFactor.y = float(int(g8)) * oo255;
	outColorFactor.z = float(int(b8)) * oo255;
	outColorFactor.w = float(int(a8)) * oo255;
	return &outColorFactor;
}

Vec3*
ExtractRGB( Psm inFromPSM, uint32 inColor )
{
	static Vec3 vc;
	return ExtractRGB( vc, inFromPSM, inColor );
}

Vec4*
ExtractRGBA( Psm inFromPSM, uint32 inColor )
{
	static Vec4 vc;
	return ExtractRGBA( vc, inFromPSM, inColor );
}

uint32
ConvertPSM( Psm inToPSM, Psm inFromPSM, uint32 inColor )
{
	if( inFromPSM == inToPSM )
		return inColor;
	uint8 c0 = (inColor>>24) & 0xFF;
	uint8 c1 = (inColor>>16) & 0xFF;
	uint8 c2 = (inColor>> 8) & 0xFF;
	uint8 c3 = (inColor>> 0) & 0xFF;
	if( inFromPSM==PSM_RGBA32 )	return GetPSM( inToPSM, c0, c1, c2, c3 );
	if( inFromPSM==PSM_ABGR32 )	return GetPSM( inToPSM, c3, c2, c1, c0 );
	if( inFromPSM==PSM_ARGB32 )	return GetPSM( inToPSM, c1, c2, c3, c0 );
	if( inFromPSM==PSM_BGRA32 )	return GetPSM( inToPSM, c2, c1, c0, c3 );
	return 0U;
}


uint32
ConvertRGBA	( Psm inToPSM,	uint32 inRGBA	)
{
	return ConvertPSM( inToPSM, PSM_RGBA32, inRGBA );
}


uint32
ConvertABGR	( Psm inToPSM,	uint32 inABGR	)
{
	return ConvertPSM( inToPSM, PSM_ABGR32, inABGR );
}


uint32
ConvertARGB	( Psm inToPSM,	uint32 inARGB	)
{
	return ConvertPSM( inToPSM, PSM_ARGB32, inARGB );
}


uint32
ConvertBGRA	( Psm inToPSM,	uint32 inBGRA	)
{
	return ConvertPSM( inToPSM, PSM_BGRA32, inBGRA );
}





//
// Dpy color



uint32
GetDpyColor( Psm inToPSM, uint8 inR, uint8 inG, uint8 inB, uint8 inA )
{
#if defined(_EE)
	return GetPSM(	inToPSM,
					inR, inG, inB,
					(uint(inA)+1)>>1	);
#else
	return GetPSM(	inToPSM,
					inR, inG, inB,
					inA	);
#endif
}


uint32
GetDpyColorRGBA	( Psm inToPSM,	uint32 inRGBA	)
{
	return GetDpyColor( inToPSM, (inRGBA>>24)&0xFF, (inRGBA>>16)&0xFF, (inRGBA>>8)&0xFF, (inRGBA>>0)&0xFF );
}


uint32
GetDpyColorABGR	( Psm inToPSM,	uint32 inABGR	)
{
	return GetDpyColor( inToPSM, (inABGR>>0)&0xFF, (inABGR>>8)&0xFF, (inABGR>>16)&0xFF, (inABGR>>24)&0xFF );
}


uint32
GetDpyColorARGB	( Psm inToPSM,	uint32 inARGB	)
{
	return GetDpyColor( inToPSM, (inARGB>>16)&0xFF, (inARGB>>8)&0xFF, (inARGB>>0)&0xFF, (inARGB>>24)&0xFF );
}


uint32
GetDpyColorBGRA	( Psm inToPSM,	uint32 inBGRA	)
{
	return GetDpyColor( inToPSM, (inBGRA>>8)&0xFF, (inBGRA>>16)&0xFF, (inBGRA>>24)&0xFF, (inBGRA>>0)&0xFF );
}


uint32
GetDpyColorRGBA	(	uint32 inRGBA	)
{
	return GetDpyColorRGBA( PSM_RGBA32, inRGBA );
}


uint32
GetDpyColorABGR	(	uint32 inABGR	)
{
	return GetDpyColorABGR( PSM_ABGR32, inABGR );
}


uint32
GetDpyColorARGB	(	uint32 inARGB	)
{
	return GetDpyColorARGB( PSM_ARGB32, inARGB );
}


uint32
GetDpyColorBGRA	(	uint32 inBGRA	)
{
	return GetDpyColorBGRA( PSM_BGRA32, inBGRA );
}




//
// Dpy Gouraud


uint32
GetDpyGouraud( Psm inToPSM, int8 inSaturation, uint8 inR, uint8 inG, uint8 inB, uint8 inA )
{
#if defined(_MSC_VER) || defined(_WIN32)
	// DX RGB-range=[0,255]^3, A-range=[0,255]
	return GetPSM(	inToPSM,
					((uint(inR)+1)*(int(inSaturation)+128))>>7,
					((uint(inG)+1)*(int(inSaturation)+128))>>7,
					((uint(inB)+1)*(int(inSaturation)+128))>>7,
					inA		);
#elif defined(_EE)
	// RGB-range=[0,128]^3, A-range=[0,128]
	return GetPSM(	inToPSM,
					((uint(inR)+1)*(int(inSaturation)+128))>>8,
					((uint(inG)+1)*(int(inSaturation)+128))>>8,
					((uint(inB)+1)*(int(inSaturation)+128))>>8,
					(uint(inA)+1)>>1	);
#elif defined(_PSP)
	// RGB-range=[0,255]^3, A-range=[0,255]
	return GetPSM(	inToPSM,
					((uint(inR)+1)*(int(inSaturation)+128))>>7,
					((uint(inG)+1)*(int(inSaturation)+128))>>7,
					((uint(inB)+1)*(int(inSaturation)+128))>>7,
					inA		);
#elif defined(_NGC)
	// RGB-range=[0,255]^3, A-range=[0,255]
	return GetPSM(	inToPSM,
					((uint(inR)+1)*(int(inSaturation)+128))>>7,
					((uint(inG)+1)*(int(inSaturation)+128))>>7,
					((uint(inB)+1)*(int(inSaturation)+128))>>7,
					inA		);
#elif defined(_PS3)
	// RGB-range=[0,255]^3, A-range=[0,255]
	return GetPSM(	inToPSM,
		((uint(inR)+1)*(int(inSaturation)+128))>>7,
		((uint(inG)+1)*(int(inSaturation)+128))>>7,
		((uint(inB)+1)*(int(inSaturation)+128))>>7,
		inA		);
#else
	#error "Unsupported platform !"
#endif
}


uint32
GetDpyGouraud( Psm inToPSM, uint8 inR, uint8 inG, uint8 inB, uint8 inA )
{
#if defined(_MSC_VER) || defined(_WIN32)
	// DX RGB-range=[0,255]^3, A-range=[0,255]
	return GetPSM(	inToPSM,
					inR,
					inG,
					inB,
					inA		);
#elif defined(_EE)
	// RGB-range=[0,128]^3, A-range=[0,128]
	return GetPSM(	inToPSM,
					(uint(inR)+1)>>1,
					(uint(inG)+1)>>1,
					(uint(inB)+1)>>1,
					(uint(inA)+1)>>1	);
#elif defined(_PSP)
	// RGB-range=[0,255]^3, A-range=[0,255]
	return GetPSM(	inToPSM,
					inR,
					inG,
					inB,
					inA		);
#elif defined(_NGC)
	// RGB-range=[0,255]^3, A-range=[0,255]
	return GetPSM(	inToPSM,
					inR,
					inG,
					inB,
					inA		);
#elif defined(_PS3)
	// RGB-range=[0,255]^3, A-range=[0,255]
	return GetPSM(	inToPSM,
		inR,
		inG,
		inB,
		inA		);
#else
	#error "Unsupported platform !"
#endif
}


uint32
GetDpyGouraudRGBA	( Psm inToPSM,	uint32 inRGBA	)
{
	return GetDpyGouraud( inToPSM, (inRGBA>>24)&0xFF, (inRGBA>>16)&0xFF, (inRGBA>>8)&0xFF, (inRGBA>>0)&0xFF );
}


uint32
GetDpyGouraudABGR	( Psm inToPSM,	uint32 inABGR	)
{
	return GetDpyGouraud( inToPSM, (inABGR>>0)&0xFF, (inABGR>>8)&0xFF, (inABGR>>16)&0xFF, (inABGR>>24)&0xFF );
}


uint32
GetDpyGouraudARGB	( Psm inToPSM,	uint32 inARGB	)
{
	return GetDpyGouraud( inToPSM, (inARGB>>16)&0xFF, (inARGB>>8)&0xFF, (inARGB>>0)&0xFF, (inARGB>>24)&0xFF );
}


uint32
GetDpyGouraudBGRA	( Psm inToPSM,	uint32 inBGRA	)
{
	return GetDpyGouraud( inToPSM, (inBGRA>>8)&0xFF, (inBGRA>>16)&0xFF, (inBGRA>>24)&0xFF, (inBGRA>>0)&0xFF );
}


uint32
GetDpyGouraudRGBA	(	uint32 inRGBA	)
{
	return GetDpyGouraudRGBA( PSM_RGBA32, inRGBA );
}


uint32
GetDpyGouraudABGR	(	uint32 inABGR	)
{
	return GetDpyGouraudABGR( PSM_ABGR32, inABGR );
}


uint32
GetDpyGouraudARGB	(	uint32 inARGB	)
{
	return GetDpyGouraudARGB( PSM_ARGB32, inARGB );
}


uint32
GetDpyGouraudBGRA	(	uint32 inBGRA	)
{
	return GetDpyGouraudBGRA( PSM_BGRA32, inBGRA );
}


uint32
GetDpyGouraudRGBA	( Psm inToPSM,	int8 inSaturation,	uint32 inRGBA )
{
	return GetDpyGouraud( inToPSM, inSaturation, (inRGBA>>24)&0xFF, (inRGBA>>16)&0xFF, (inRGBA>>8)&0xFF, (inRGBA>>0)&0xFF );
}


uint32
GetDpyGouraudABGR	( Psm inToPSM,	int8 inSaturation, uint32 inABGR )
{
	return GetDpyGouraud( inToPSM, inSaturation, (inABGR>>0)&0xFF, (inABGR>>8)&0xFF, (inABGR>>16)&0xFF, (inABGR>>24)&0xFF );
}


uint32
GetDpyGouraudARGB	( Psm inToPSM,	int8 inSaturation, uint32 inARGB )
{
	return GetDpyGouraud( inToPSM, inSaturation, (inARGB>>16)&0xFF, (inARGB>>8)&0xFF, (inARGB>>0)&0xFF, (inARGB>>24)&0xFF );
}


uint32
GetDpyGouraudBGRA	( Psm inToPSM,	int8 inSaturation, uint32 inBGRA )
{
	return GetDpyGouraud( inToPSM, inSaturation, (inBGRA>>8)&0xFF, (inBGRA>>16)&0xFF, (inBGRA>>24)&0xFF, (inBGRA>>0)&0xFF );
}


uint32
GetDpyGouraudRGBA	(	int8 inSaturation, uint32 inRGBA )
{
	return GetDpyGouraudRGBA( PSM_RGBA32, inSaturation, inRGBA );
}


uint32
GetDpyGouraudABGR	(	int8 inSaturation, uint32 inABGR )
{
	return GetDpyGouraudABGR( PSM_ABGR32, inSaturation, inABGR );
}


uint32
GetDpyGouraudARGB	(	int8 inSaturation, uint32 inARGB )
{
	return GetDpyGouraudARGB( PSM_ARGB32, inSaturation, inARGB );
}


uint32
GetDpyGouraudBGRA	(	int8 inSaturation, uint32 inBGRA )
{
	return GetDpyGouraudBGRA( PSM_BGRA32, inSaturation, inBGRA );
}




