/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#define USE_APPROX


#if defined(_DX)

	#define	MATH_USE_DX
	#include <Kernel/PC/NvWindows.h>
	#include <d3dx9.h>
	#include <d3d9.h>
	#define	DXV2( PTR )							((D3DXVECTOR2*)(PTR))
	#define	DXV3( PTR )							((D3DXVECTOR3*)(PTR))
	#define	DXV4( PTR )							((D3DXVECTOR4*)(PTR))
	#define	DXQ(  PTR )							((D3DXQUATERNION*)(PTR))
	#define	DXM(  PTR )							((D3DXMATRIX*)(PTR))

#elif defined(_EE)

	#define	MATH_USE_VU0
	#define	IS_VU0ABLE( PTR )						((uint(PTR)&15)==0)
	#define	IS_VU0ABLE_2( PTR0, PTR1 )				(((uint(PTR0)|uint(PTR1))&15)==0)
	#define	IS_VU0ABLE_3( PTR0, PTR1, PTR2 )		(((uint(PTR0)|uint(PTR1)|uint(PTR2))&15)==0)
	#define	IS_VU0ABLE_4( PTR0, PTR1, PTR2, PTR3 )	(((uint(PTR0)|uint(PTR1)|uint(PTR2)|uint(PTR3))&15)==0)

#elif defined(_PSP)

	#include <libvfpu.h>
	//#define	MATH_USE_VFPU
	#define	IS_VFPUABLE( PTR )						((uint(PTR)&15)==0)
	#define	IS_VFPUABLE_2( PTR0, PTR1 )				(((uint(PTR0)|uint(PTR1))&15)==0)
	#define	IS_VFPUABLE_3( PTR0, PTR1, PTR2 )		(((uint(PTR0)|uint(PTR1)|uint(PTR2))&15)==0)
	#define	IS_VFPUABLE_4( PTR0, PTR1, PTR2, PTR3 )	(((uint(PTR0)|uint(PTR1)|uint(PTR2)|uint(PTR3))&15)==0)
	#define	PSPV2( PTR )							((ScePspFVector2*)(PTR))
	#define	PSPV3( PTR )							((ScePspFVector3*)(PTR))
	#define	PSPV4( PTR )							((ScePspFVector4*)(PTR))
	#define	PSPQ(  PTR )							((ScePspFQuaternion*)(PTR))
	#define	PSPM(  PTR )							((ScePspFMatrix4*)(PTR))

#endif


#include <math.h>
#include <Nova.h>
using namespace nv;





//--------------------------
// Math Fct
//--------------------------

namespace
{

	#define K_BITSCOUNT_INVSQRT		7
	#define K_BITSCOUNT_ACOS		6
	#define K_TABLESIZE_COS			32
	#define K_TABLESIZE_INVSQRT		(1 << K_BITSCOUNT_INVSQRT)
	#define K_TABLESIZE_ACOS		(1 << K_BITSCOUNT_ACOS)

	float							gTable_Acos[K_TABLESIZE_ACOS];
	float							gTable_Cos[K_TABLESIZE_COS+1];
	unsigned char					gTable1_InvSqrt[K_TABLESIZE_INVSQRT];
	float							gTable2_InvSqrt[2] = { 1.F, 1.414213562F };

	/*
	 *	Approximations_Initialize
	 *	--------------------------------------------------------------------------------------- */

	void Approximations_Initialize()
	{
		int			index;

		for (index = 0; index <= K_TABLESIZE_COS; ++ index )
		{
			float						angle;
			
			angle = index * (nv::math::HalfPi / K_TABLESIZE_COS);
			gTable_Cos[index] = (float) ::cosf (angle );
		}

		for (index = 0; index < K_TABLESIZE_INVSQRT; ++ index )
		{
			uint32						ieeeNumber;
			
			ieeeNumber = (127UL << 23) + (index << (23 - K_BITSCOUNT_INVSQRT));
			
			float						realNumber;
			
			realNumber = * (float *) &ieeeNumber;
			realNumber = (float) (1.0 / ::sqrtf (realNumber ) );
			ieeeNumber = AS_UINT32( realNumber );
			
			gTable1_InvSqrt[index] = (unsigned char) ((ieeeNumber >> (23 - 8)) & 0xffUL);
		}

		for (index = 0; index < K_TABLESIZE_ACOS; ++ index )
		{
			uint32						ieeeNumber;
			
			ieeeNumber = (0x7UL << 27) | (index << (27 - K_BITSCOUNT_ACOS));
			
			float						realNumber;
			
			realNumber = AS_FLOAT( ieeeNumber );
			gTable_Acos[index] = (float) ::acosf (1.F - realNumber );
		}
	}

}


bool
nvcore_Math_Init 	(		)
{
	Approximations_Initialize();

	NV_COMPILE_TIME_ASSERT( sizeof(Vec2)	== 8  );
	NV_COMPILE_TIME_ASSERT( sizeof(Vec3)   == 12 );
	NV_COMPILE_TIME_ASSERT( sizeof(Vec4)   == 16 );
	NV_COMPILE_TIME_ASSERT( sizeof(Matrix) == 64 );
	NV_COMPILE_TIME_ASSERT( sizeof(Quat)   == 16 );

	return TRUE;
}


void
nvcore_Math_Update	(		)
{
	//
}


void
nvcore_Math_Shut	(		)
{
	//
}




const float	nv::math::Pi			= 3.141592654f;
const float	nv::math::HalfPi		= 1.570796326f;
const float	nv::math::TwoPi			= 6.283185307f;
const float	nv::math::OOPi			= 0.318309886f;
const float	nv::math::FpEpsilon		= 1.192092896e-07F;
const float	nv::math::FpMin			= 1.175494351e-38F;
const float	nv::math::FpMax			= 3.402823466e+38F;
const float	nv::math::DegToRad		= 0.01745329252f;
const float	nv::math::RadToDeg		= 57.29577951308f;




//
//	Accepte des valeurs en dehors du domaine de d�finition
//	Pr�cision : erreur moyenne  0.057%
//				erreur maximale 0.536%	0.24 degree

float
nv::math::Acosf( float x )
{
#ifndef USE_APPROX

	return ::acosf( x );

#else

	float							cte_0_00;
	float							cte_1_00;
	float							usedx;

	cte_0_00 = 0.F;
	cte_1_00 = 1.F;

	uint32							value_int;

	usedx = cte_1_00 - math::Absf(x);
	value_int = (AS_UINT32(usedx)) & ~0x80000000UL;

	float							approx;
	float							value;
	float							derivate;
	float							parameter;
	
	parameter = usedx + usedx - usedx * usedx;
	derivate = math::Isqrtf(parameter);

	if (value_int < 0x38000000 )
	{
		approx = cte_0_00;
		value = cte_0_00;
		
		derivate += 0.5F * derivate * (cte_1_00 - parameter * derivate * derivate);
	}
	else
	{
		uint32						index;
		
		index = (value_int >> (27 - K_BITSCOUNT_ACOS)) & (K_TABLESIZE_ACOS - 1);
		value_int = (0x7UL << 27) | (index << (27 - K_BITSCOUNT_ACOS));
		
		value = AS_FLOAT( value_int );
		approx = gTable_Acos[index];
	}

	approx += (usedx - value) * derivate;
	
	if (x < cte_0_00 )
	{
		approx = (float) Pi - approx;
	}
	
	return approx;

#endif
}


float
nv::math::Asinf( float a )
{
#ifndef USE_APPROX

	return ::asinf( a );

#else

	return ( HalfPi - Acosf(a) );

#endif
}


//
//	Dans le cas ou (x * x + y * y) = 1, il n'y pas de normalisation
//	et la pr�cision des calculs est tr�s sup�rieure.
//	Pr�cision : erreur moyenne	0.183%
//				erreur maximale 0.24 degr�s

float
nv::math::Atan2f( float x, float y )
{
#ifndef USE_APPROX

	return ::atan2f( x, y );
	
#else

	float					cte_0_00;

	cte_0_00 = 0.F;
	if (x == cte_0_00 ) return (float) Pi;

	float					scale;					
	float					norm2;
	
	norm2 = x * x + y * y;
	scale = math::Isqrtf( norm2 );
	scale += 0.5F * (1.F - norm2 * scale * scale) * scale;
	
	x *= scale;
	y *= scale;
	
	float					value;

	value = Acosf( y );
	return (x < cte_0_00) ? -value : value;

#endif
}


float
nv::math::Atanf( float a )
{
#ifndef USE_APPROX

	return ::atanf( a );

#else

	return Atan2f( a, 1.0 );

#endif
}


//	
//	Bon comportement avec les cas particuliers
//		Inf -> Nan
//	Toutefois limit� dans l'�tendue des valeurs support�es (autour de 10e6)
//	Pr�cision :	erreur moyenne  0.009%
//				erreur maximale 0.117%

float
nv::math::Cosf( float a )
{
#ifndef USE_APPROX

	return ::cosf( a );

#else
	
	float						ap;
	
	ap = math::Absf( a * (float) (K_TABLESIZE_COS / HalfPi) );

	int32						masterIndex;
	
	masterIndex = (int) ap;
	
	float						ratio1, ratio2;
	
	ratio2 = ap - masterIndex;
	ratio1 = 1.F - ratio2;
	
	int							index;
	
	index = (masterIndex & (4 * K_TABLESIZE_COS - 1)) & (K_TABLESIZE_COS - 1);
	
	float						value;
	
	if (masterIndex & K_TABLESIZE_COS )
	{
		index = K_TABLESIZE_COS - index;
		value = ratio1 * gTable_Cos[index] + ratio2 * gTable_Cos[index - 1];
	}
	else
	{
		value = ratio1 * gTable_Cos[index] + ratio2 * gTable_Cos[index + 1];
	}
	
	if ((masterIndex & K_TABLESIZE_COS) ^ ((masterIndex >> 1) & K_TABLESIZE_COS) )
	{
		value = -value;
	}

	return value;

#endif
}


float
nv::math::Sinf( float a )
{
#ifndef USE_APPROX

	return ::sinf( a );

#else

	return Cosf( a - HalfPi );

#endif
}


void
nv::math::Sincosf( float inA, float * outSin, float * outCos )
{
	if( outSin )	*outSin = Sinf( inA );
	if( outCos )	*outCos = Cosf( inA );
}


float
nv::math::Tanf( float a )
{
#ifndef USE_APPROX

	return ::tanf( a );

#else

	float s1, c1;
	Sincosf( a, &s1, &c1 );
	return ( s1 / c1 );

#endif
}


//
// Erreur maximale de 0.007
// Lissage par approximation polynomiale de la mantisse au troisi�me ordre

float
nv::math::Log2f( float v )
{
#ifndef USE_APPROX

	return ::logf(v) * 1.44269504F;		// 1/ln(2)

#else

   int32          x = AS_INT32( v );
   const int      logm2 = ((x >> 23) & 255) - 128;
   x &= ~(255 << 23);
   x +=   127 << 23;
   v  = AS_FLOAT( x );
   v  = ((-1.0f/3.0f) * v + 2.0f) * v - (2.0f/3.0f);
   return (logm2 + v);

#endif
}


float
nv::math::Logf( float v )
{
#ifndef USE_APPROX

	return ::logf(v);

#else

	return Log2f(v) * 0.69314718F;		// ln(2)

#endif
}


float
nv::math::Log10f( float v )
{
#ifndef USE_APPROX

	return ::logf(v) * 0.4342944819F;				// 1/ln(10)

#else

	return Log2f(v) * 0.301029995664F;	// ln(2)/ln(10)

#endif
}


//
//	Bon comportement avec les cas particuliers
//		Inf -> valeur proche de 0
//		0 ou d�normalis� -> valeur tr�s grande
//	Non test� avec valeurs n�gatives.
//	Pr�cision : erreur moyenne 0.095%
//				erreur maximale 0.389% 

float
nv::math::Isqrtf( float a )
{
#ifndef USE_APPROX

	return 1.0f / ::sqrtf( a );

#else

	uint32								ieeeNumber;
	
	ieeeNumber = AS_UINT32( a );
	
	int32								exponent;
	
	exponent = (int32) ((ieeeNumber >> 23) & 0xffUL) - 127;
	
	uint32								index;
	
	index = (ieeeNumber >> (23 - K_BITSCOUNT_INVSQRT)) & (K_TABLESIZE_INVSQRT - 1);

	uint32								resultExponent;
	
	resultExponent = (uint32) (((-exponent) >> 1) + 127);	
	if (index != 0 ) resultExponent -= 1;
		
	ieeeNumber = (resultExponent << 23) | ((uint32) gTable1_InvSqrt[index] << (23 - 8));
	a = * (float *) &ieeeNumber;
	
	return a * gTable2_InvSqrt[exponent & 1];

#endif
}



float
nv::math::Powf( float inV, float inP )
{
	return ::powf( inV, inP );
}


float
nv::math::Sqrtf( float inV )
{
	return ::sqrtf( inV );
}


float
nv::math::Expf( float inA )
{
	return ::expf( inA );
}


int
nv::math::Ftoi( float inV )
{
	return (int)inV;
}


float
nv::math::Itof( int inV )
{
	return (float)inV;
}


float
nv::math::Absf( float inV )
{
	*((unsigned int *)&inV) &= 0x7FFFFFFF;
	return inV;
}


float
nv::math::Fmod(	float x, float y	)
{
	return ( x - float((int)(x/y))*y );
}


float
nv::math::Ceil(	float x		)
{
	int    i = int(x);
	float fi = float(i);
	if( x < 0.0f || x == fi )
		return fi;
	else
		return (fi+1.0f);
}


float
nv::math::Floor( float x	)
{
	int    i = int(x);
	float fi = float(i);
	if( x > 0.0f || x == fi )
		return fi;
	else
		return (fi-1.0f);
}


float
nv::math::Round( float x )
{
	return int(x+0.5f);
}


float nv::math::Asin	(	float x								)	{ return nv::math::Asinf(x);			}
float nv::math::Acos	(	float x								)	{ return nv::math::Acosf(x);			}
float nv::math::Atan	(	float x								)	{ return nv::math::Atanf(x);			}
float nv::math::Atan2	(	float x, float y					)	{ return nv::math::Atan2f(x,y);			}
float nv::math::Sin		(	float a								)	{ return nv::math::Sinf(a);				}
float nv::math::Cos		(	float a								)	{ return nv::math::Cosf(a);				}
void  nv::math::Sincos	(	float a, float* outS, float* outC	)	{        nv::math::Sincosf(a,outS,outC);}
float nv::math::Tan		(	float a								)	{ return nv::math::Tanf(a);				}
float nv::math::Exp		(	float x								)	{ return nv::math::Expf(x);				}
float nv::math::Log		(	float x								)	{ return nv::math::Logf(x);				}
float nv::math::Log2	(	float x								)	{ return nv::math::Log2f(x);			}
float nv::math::Log10	(	float x								)	{ return nv::math::Log2f(x);			}
float nv::math::Pow		(	float x, float y					)	{ return nv::math::Powf(x,y);			}
float nv::math::Isqrt	(	float x								)	{ return nv::math::Isqrtf(x);			}
float nv::math::Sqrt	(	float x								)	{ return nv::math::Sqrtf(x);			}
float nv::math::Abs		(	float x								)	{ return nv::math::Absf(x);				}
float nv::math::Fabs	(	float x								)	{ return nv::math::Absf(x);				}
float nv::math::Fabsf	(	float x								)	{ return nv::math::Absf(x);				}



//--------------------------
// 2D Vector
//--------------------------


const nv::math::Vec2	nv::math::Vec2::ZERO(0.0f,0.0f);
const nv::math::Vec2	nv::math::Vec2::ONE(1.0f,1.0f);
const nv::math::Vec2	nv::math::Vec2::UNIT_X(1.0f,0.0f);
const nv::math::Vec2	nv::math::Vec2::UNIT_Y(0.0f,1.0f);


nv::math::Vec2*
nv::math::Vec2Copy		(       Vec2* outV, const Vec2* inV									)
{
	if( outV == inV )
		return outV;

	outV->x = inV->x;
	outV->y = inV->y;
	return outV;
}

nv::math::Vec2*
nv::math::Vec2Zero			(       Vec2* outV													)
{
	outV->x = outV->y = 0;
	return outV;
}

float
nv::math::Vec2Norm			( const Vec2* inV													)
{
#if defined(MATH_USE_DX)

	return D3DXVec2Length( DXV2(inV) );

#else

	return Sqrtf( inV->x * inV->x + inV->y * inV->y );

#endif
}

float
nv::math::Vec2Norm2			( const Vec2* inV													)
{
#if defined(MATH_USE_DX)

	return D3DXVec2LengthSq( DXV2(inV) );

#else

	return ( inV->x * inV->x + inV->y * inV->y );

#endif
}

nv::math::Vec2*
nv::math::Vec2Normalize		( Vec2* outV, const Vec2* inV									)
{
#if defined(MATH_USE_DX)

	return (Vec2*) D3DXVec2Normalize( DXV2(outV), DXV2(inV) );

#else

	float n = Vec2Norm( inV );
	NV_ASSERT( n != 0.f );
	float oon = 1.0f/n;
	outV->x = inV->x * oon;
	outV->y = inV->y * oon;
	return outV;

#endif
}

float
nv::math::Vec2Cross			( const Vec2* inV0, const Vec2* inV1								)
{
#if defined(MATH_USE_DX)

	return D3DXVec2CCW( DXV2(inV0), DXV2(inV1) );

#else

	return ( inV0->x * inV1->y - inV0->y * inV1->x );

#endif
}

float
nv::math::Vec2Dot				( const Vec2* inV0, const Vec2* inV1								)
{
#if defined(MATH_USE_DX)

	return D3DXVec2Dot( DXV2(inV0), DXV2(inV1) );

#else

	return ( inV0->x * inV1->x + inV0->y * inV1->y );

#endif
}

nv::math::Vec2*
nv::math::Vec2Add				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec2*) D3DXVec2Add( DXV2(outV), DXV2(inV0), DXV2(inV1) );

#else

	outV->x = inV0->x + inV1->x;
	outV->y = inV0->y + inV1->y;
	return outV;

#endif
}

nv::math::Vec2*
nv::math::Vec2Sub				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec2*) D3DXVec2Subtract( DXV2(outV), DXV2(inV0), DXV2(inV1) );

#else

	outV->x = inV0->x - inV1->x;
	outV->y = inV0->y - inV1->y;
	return outV;

#endif
}

nv::math::Vec2*
nv::math::Vec2Mul				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				)
{
	outV->x = inV0->x * inV1->x;
	outV->y = inV0->y * inV1->y;
	return outV;
}

nv::math::Vec2*
nv::math::Vec2Min				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec2*) D3DXVec2Minimize( DXV2(outV), DXV2(inV0), DXV2(inV1) );

#else

	outV->x = math::Min( inV0->x , inV1->x );
	outV->y = math::Min( inV0->y , inV1->y );
	return outV;

#endif
}

nv::math::Vec2*
nv::math::Vec2Max				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec2*) D3DXVec2Maximize( DXV2(outV), DXV2(inV0), DXV2(inV1) );

#else

	outV->x = math::Max( inV0->x , inV1->x );
	outV->y = math::Max( inV0->y , inV1->y );
	return outV;

#endif
}

nv::math::Vec2*
nv::math::Vec2Scale			(       Vec2* outV, const Vec2* inV,  float inF						)
{
#if defined(MATH_USE_DX)

	return (Vec2*) D3DXVec2Scale( DXV2(outV), DXV2(inV), inF );

#else

	outV->x = inV->x * inF;
	outV->y = inV->y * inF;
	return outV;

#endif
}

nv::math::Vec2*
nv::math::Vec2Lerp			(       Vec2* outV, const Vec2* inV0, const Vec2* inV1, float inF	)
{
#if defined(MATH_USE_DX)

	return (Vec2*) D3DXVec2Lerp( DXV2(outV), DXV2(inV0), DXV2(inV1), inF );

#else

	outV->x = inV0->x + inF * ( inV1->x - inV0->x );
	outV->y = inV0->y + inF * ( inV1->y - inV0->y );
	return outV;

#endif
}

nv::math::Vec2*
nv::math::Vec2Apply			(       Vec2* outV, const Vec2* inV,  const Matrix* inM		)
{
	ALIGNED128( Vec4, v );
	v.x = inV->x;
	v.y = inV->y;
	v.z = 0.f;
	v.w = 1.f;
	Vec4Apply( &v, &v, inM );
	outV->x = v.x;
	outV->y = v.y;
	return outV;
}

nv::math::Vec2*
nv::math::Vec2ApplyVector	(       Vec2* outV, const Vec2* inV,  const Matrix* inM		)
{
	Vec4 v( inV->x, inV->y, 0.f, 0.f );
	Vec4Apply( &v, &v, inM );
	outV->x = v.x;
	outV->y = v.y;
	return outV;
}




//--------------------------
// 3D Vector
//--------------------------


const nv::math::Vec3	nv::math::Vec3::ZERO(0.0f,0.0f,0.0f);
const nv::math::Vec3	nv::math::Vec3::ONE(1.0f,1.0f,1.0f);
const nv::math::Vec3	nv::math::Vec3::UNIT_X(1.0f,0.0f,0.0f);
const nv::math::Vec3	nv::math::Vec3::UNIT_Y(0.0f,1.0f,0.0f);
const nv::math::Vec3	nv::math::Vec3::UNIT_Z(0.0f,0.0f,1.0f);


nv::math::Vec3*
nv::math::Vec3Copy			(       Vec3* outV, const Vec3* inV			)
{
	if( outV == inV )
		return outV;
	outV->x = inV->x;
	outV->y = inV->y;
	outV->z = inV->z;
	return outV;
}

nv::math::Vec3*
nv::math::Vec3Zero			(       Vec3* outV				)
{
	outV->x = outV->y = outV->z = 0;
	return outV;
}

float
nv::math::Vec3Norm			( const Vec3* inV			)
{
#if defined(MATH_USE_DX)

	return D3DXVec3Length( DXV3(inV) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE(inV) ) {
		ALIGNED128( Vec4, outV );
	    __asm__ __volatile__(
        "lqc2    vf4,0x0(%1)\n"
        "vmul	vf5,vf4,vf4\n"
        "vaddy.x vf5,vf5,vf5\n"
        "vaddz.x vf5,vf5,vf5\n"
        "vsqrt Q,vf5x\n"
        "vwaitq\n"
        "vaddq.x vf5,vf0,Q\n"
        "sqc2    vf5,0x0(%0)\n"
        : : "r" (&outV) , "r" (inV) : "memory");
		return outV.x;
	}
#endif

#if defined(MATH_USE_VFPU)
	if( IS_VFPUABLE(inV)){
		float	norm;
		__asm__ __volatile__ (
			".set			noreorder		\n"			// suppress reordering
			"lv.q			c000, 0(%1)		\n"			// c000 = (inV.x,inV.y,inV.z)
			"vdot.t			s010, c000, c000\n"			// s010 = s000*s000 + s001*s001 + s002*s002
			"vsqrt.s		s010, s010		\n"			// s010 = sqrt(s010)
			"sv.s			s010, 0(%0)		\n"			// norm = s000
			".set			reorder			\n"			// reordering			
			: "=m"(norm)
			: "r"(inV)
		);
		return norm;
	}
#endif
	return Sqrtf( Vec3Dot(inV,inV) );
#endif
}

float
nv::math::Vec3Norm2			( const Vec3* inV													)
{
#if defined(MATH_USE_DX)

	return D3DXVec3LengthSq( DXV3(inV) );

#else

	return Vec3Dot(inV,inV);

#endif
}

nv::math::Vec3*
nv::math::Vec3Normalize		(       Vec3* outV, const Vec3* inV									)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3Normalize( DXV3(outV), DXV3(inV) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outV,inV) ) {
	    __asm__ __volatile__(
        "lqc2    vf4,0x0(%1)\n"
        "vmul	vf5,vf4,vf4\n"
        "vaddy.x vf5,vf5,vf5\n"
        "vaddz.x vf5,vf5,vf5\n"
        "vsqrt	Q,vf5x\n"
        "vwaitq\n"
        "vaddq.x vf5x,vf0x,Q\n"
        "vnop\n"
        "vnop\n"
        "vdiv    Q,vf0w,vf5x\n"
        "vsub 	vf6,vf0,vf0\n"	// vf6.xyzw=0
        "vwaitq\n"
        "vmulq   vf6,vf4,Q\n"
		"qmfc2	$6, vf6\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
        : : "r" (outV) , "r" (inV) : "$6", "memory");
		return outV;
	}
#endif

	float n = Vec3Norm( inV );
	NV_ASSERT( n != 0.f );
	float oon = 1.f / n;
	return Vec3Scale( outV, inV, oon );

#endif
}

nv::math::Vec3*
nv::math::Vec3Cross			(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3Cross( DXV3(outV), DXV3(inV0), DXV3(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vopmula.xyz	ACC,vf4,vf5\n"
		"vopmsub.xyz	vf6,vf5,vf4\n"
		"qmfc2	$6, vf6\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inV0) ,"r" (inV1) : "$6", "memory");
		return outV;
	}
#endif

	Vec3 tmp;
	tmp.x = inV0->y*inV1->z - inV0->z*inV1->y;
	tmp.y = inV0->z*inV1->x - inV0->x*inV1->z;
	tmp.z = inV0->x*inV1->y - inV0->y*inV1->x;
	return Vec3Copy( outV, &tmp );

#endif
}

float
nv::math::Vec3Dot				( const Vec3* inV0, const Vec3* inV1								)
{
#if defined(MATH_USE_DX)

	return D3DXVec3Dot( DXV3(inV0), DXV3(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(inV0,inV1) ) {
		ALIGNED128( Vec4, outV );
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
        "vmul	vf5,vf4,vf5\n"
        "vaddy.x vf5,vf5,vf5\n"
        "vaddz.x vf5,vf5,vf5\n"
 		"sqc2    vf5,0x0(%0)\n"
		: : "r" (&outV) , "r" (inV0), "r" (inV1) : "memory");
		return outV.x;
	}
#endif

	return ( inV0->x * inV1->x + inV0->y * inV1->y + inV0->z * inV1->z );

#endif
}

nv::math::Vec3*
nv::math::Vec3Add				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3Add( DXV3(outV), DXV3(inV0), DXV3(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vadd	vf6,vf4,vf5\n"
		"qmfc2	$6, vf6\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "$6", "memory");
		return outV;
	}
#endif

	outV->x = inV0->x + inV1->x;
	outV->y = inV0->y + inV1->y;
	outV->z = inV0->z + inV1->z;
	return outV;

#endif
}

nv::math::Vec3*
nv::math::Vec3Sub				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3Subtract( DXV3(outV), DXV3(inV0), DXV3(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vsub	vf6,vf4,vf5\n"
		"qmfc2	$6, vf6\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "$6", "memory");
		return outV;
	}
#endif

	outV->x = inV0->x - inV1->x;
	outV->y = inV0->y - inV1->y;
	outV->z = inV0->z - inV1->z;
	return outV;

#endif
}

nv::math::Vec3*
nv::math::Vec3Mul				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vmul	vf6,vf4,vf5\n"
		"qmfc2	$6, vf6\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "$6", "memory");
		return outV;
	}
#endif

	outV->x = inV0->x * inV1->x;
	outV->y = inV0->y * inV1->y;
	outV->z = inV0->z * inV1->z;
	return outV;
}

nv::math::Vec3*
nv::math::Vec3Min				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3Minimize( DXV3(outV), DXV3(inV0), DXV3(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vmini	vf6,vf4,vf5\n"
		"qmfc2	$6, vf6\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "$6", "memory");
		return outV;
	}
#endif

	outV->x = math::Min( inV0->x , inV1->x );
	outV->y = math::Min( inV0->y , inV1->y );
	outV->z = math::Min( inV0->z , inV1->z );
	return outV;

#endif
}

nv::math::Vec3*
nv::math::Vec3Max				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3Maximize( DXV3(outV), DXV3(inV0), DXV3(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vmax	vf6,vf4,vf5\n"
		"qmfc2	$6, vf6\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "$6", "memory");
		return outV;
	}
#endif

	outV->x = math::Max( inV0->x , inV1->x );
	outV->y = math::Max( inV0->y , inV1->y );
	outV->z = math::Max( inV0->z , inV1->z );
	return outV;

#endif
}

nv::math::Vec3*
nv::math::Vec3Scale			(       Vec3* outV, const Vec3* inV,  float inF						)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3Scale( DXV3(outV), DXV3(inV), inF );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outV,inV) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"mfc1    $6,%2\n"
		"qmtc2   $6,vf5\n"	// vf5.x=t
		"vmulx	vf4,vf4,vf5\n"
		"qmfc2	$6, vf4\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inV), "f" (inF) : "$6", "memory");
		return outV;
	}
#endif

	outV->x = inV->x * inF;
	outV->y = inV->y * inF;
	outV->z = inV->z * inF;
	return outV;

#endif
}


nv::math::Vec3*
nv::math::Vec3Lerp			(       Vec3* outV, const Vec3* inV0, const Vec3* inV1, float inF	)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3Lerp( DXV3(outV), DXV3(inV0), DXV3(inV1), inF );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"mfc1    $8,%3\n"
		"qmtc2   $8,vf6\n"		// vf6.x=t
		"vsubx	vf7,vf0,vf6\n"	// vf7.w=1-t
		"vmulax	ACC,vf5,vf6\n"
		"vmaddw	vf4,vf4,vf7\n"	// v0*(1-t) + v1*t
		"qmfc2	$6, vf4\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1), "f" (inF) : "$6", "memory");
		return outV;
	}
#endif

	outV->x = inV0->x + inF * ( inV1->x - inV0->x );
	outV->y = inV0->y + inF * ( inV1->y - inV0->y );
	outV->z = inV0->z + inF * ( inV1->z - inV0->z );
	return outV;

#endif
}


nv::math::Vec3*
nv::math::Vec3Apply			(       Vec3* outV, const Vec3* inV, const Quat* inQ				)
{
	// V' = Q * Quat(V,0) * Q^-1
	Quat r, v( inV->x, inV->y, inV->z, 0.0f );
	QuatMul( &r, inQ, QuatMul( &r, &v, QuatFastInverse( &r, inQ ) ) );
	outV->x = r.x;
	outV->y = r.y;
	outV->z = r.z;
	return outV;
}


nv::math::Vec3*
nv::math::Vec3Apply			(       Vec3* outV, const Vec3* inV, const Matrix* inM				)
{
#if defined(MATH_USE_DX)

	Vec4 v4;
	D3DXVec3Transform( DXV4(&v4), DXV3(inV), DXM(inM) );
	outV->x = v4.x;
	outV->y = v4.y;
	outV->z = v4.z;
	return outV;

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"vmulax	ACC, vf4,vf8\n"
		"vmadday	ACC, vf5,vf8\n"
		"vmaddaz	ACC, vf6,vf8\n"
		"vmaddw	vf9,vf7,vf00\n"		// .w=1
		"qmfc2	$6, vf9\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV): "$6", "memory");
		return outV;
	}
#endif

	Vec3 tmp;
	tmp.x = inV->x * inM->m11 + inV->y * inM->m21 + inV->z * inM->m31 + inM->m41;
	tmp.y = inV->x * inM->m12 + inV->y * inM->m22 + inV->z * inM->m32 + inM->m42;
	tmp.z = inV->x * inM->m13 + inV->y * inM->m23 + inV->z * inM->m33 + inM->m43;
	return Vec3Copy( outV, &tmp );

#endif
}


nv::math::Vec3*
nv::math::Vec3Apply			(       Vec3* outV, const Vec3* inV,  const Matrix* inM, float inF	)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"vmulax	ACC, vf4,vf8\n"
		"vmadday	ACC, vf5,vf8\n"
		"vmaddaz	ACC, vf6,vf8\n"
		"vmaddw	vf9, vf7,vf00\n"		// .w=1
		"mfc1    $6,%3\n"
		"qmtc2   $6,vf10\n"			// vf10.x=inF
		"vmulx	vf9, vf9, vf10\n"
		"qmfc2	$6, vf9\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV), "f" (inF) : "$6", "memory");
		return outV;
	}
#endif

	Vec3 tmp;
	tmp.x = ( inV->x * inM->m11 + inV->y * inM->m21 + inV->z * inM->m31 + inM->m41 ) * inF;
	tmp.y = ( inV->x * inM->m12 + inV->y * inM->m22 + inV->z * inM->m32 + inM->m42 ) * inF;
	tmp.z = ( inV->x * inM->m13 + inV->y * inM->m23 + inV->z * inM->m33 + inM->m43 ) * inF;
	return Vec3Copy( outV, &tmp );
}


nv::math::Vec3*
nv::math::Vec3ApplyVector		(       Vec3* outV, const Vec3* inV, const Matrix* inM				)
{
#if defined(MATH_USE_DX)

	return (Vec3*) D3DXVec3TransformNormal( DXV3(outV), DXV3(inV), DXM(inM) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"vmulax	ACC, vf4,vf8\n"
		"vmadday	ACC, vf5,vf8\n"
		"vmaddz	vf9, vf6,vf8\n"
		"qmfc2	$6, vf9\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV): "$6", "memory");
		return outV;
	}
#endif

	Vec3 tmp;
	tmp.x = inV->x * inM->m11 + inV->y * inM->m21 + inV->z * inM->m31;
	tmp.y = inV->x * inM->m12 + inV->y * inM->m22 + inV->z * inM->m32;
	tmp.z = inV->x * inM->m13 + inV->y * inM->m23 + inV->z * inM->m33;
	return Vec3Copy( outV, (const Vec3*)&tmp );

#endif
}


nv::math::Vec3*
nv::math::Vec3ApplyVector	(       Vec3* outV, const Vec3* inV,  const Matrix* inM, float inF	)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"vmulax	ACC, vf4,vf8\n"
		"vmadday	ACC, vf5,vf8\n"
		"vmaddz	vf9, vf6,vf8\n"
		"mfc1    $6,%3\n"
		"qmtc2   $6,vf10\n"		// vf10.x=inF
		"vmulx	vf9, vf9, vf10\n"
		"qmfc2	$6, vf9\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV), "f" (inF) : "$6", "memory");
		return outV;
	}
#endif

	Vec3 tmp;
	tmp.x = ( inV->x * inM->m11 + inV->y * inM->m21 + inV->z * inM->m31 ) * inF;
	tmp.y = ( inV->x * inM->m12 + inV->y * inM->m22 + inV->z * inM->m32 ) * inF;
	tmp.z = ( inV->x * inM->m13 + inV->y * inM->m23 + inV->z * inM->m33 ) * inF;
	return Vec3Copy( outV, &tmp );
}


nv::math::Vec3*
nv::math::Vec3Blend			(       Vec3* outV, const Vec3* inV,  const Matrix* inM				)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf3,0x0(%0)\n"
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"vmulaw	ACC,vf3,vf0\n"
		"vmaddax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddaz	ACC,vf6,vf8\n"
		"vmaddw	vf9,vf7,vf00\n"	// .w=1
		"qmfc2	$6, vf9\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV): "$6", "memory");
		return outV;
	}
#endif

	ALIGNED128( Vec3, tmp );
	return Vec3Add( outV, outV, Vec3Apply(&tmp,inV,inM) );
}


nv::math::Vec3*
nv::math::Vec3Blend			(       Vec3* outV, const Vec3* inV,  const Matrix* inM, float inF	)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf3,0x0(%0)\n"
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"vmulax	ACC, vf4,vf8\n"
		"vmadday	ACC, vf5,vf8\n"
		"vmaddaz	ACC, vf6,vf8\n"
		"vmaddw	vf9, vf7,vf00\n"	// .w=1
		"mfc1    $6,%3\n"
		"qmtc2   $6,vf10\n"		// vf10.x=inF
		"vmulaw	ACC,vf3,vf0\n"
		"vmaddx	vf9,vf9,vf10\n"
		"qmfc2	$6, vf9\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV), "f" (inF) : "$6", "memory");
		return outV;
	}
#endif

	ALIGNED128( Vec3, tmp );
	return Vec3Add( outV, outV, Vec3Apply(&tmp,inV,inM,inF) );
}


nv::math::Vec3*
nv::math::Vec3BlendVector	(       Vec3* outV, const Vec3* inV,  const Matrix* inM				)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf3,0x0(%0)\n"
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"vmulaw	ACC,vf3,vf0\n"
		"vmaddax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddz	vf9,vf6,vf8\n"
		"qmfc2	$6, vf9\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV): "$6", "memory");
		return outV;
	}
#endif

	ALIGNED128( Vec3, tmp );
	return Vec3Add( outV, outV, Vec3ApplyVector(&tmp,inV,inM) );
}


nv::math::Vec3*
nv::math::Vec3BlendVector	(       Vec3* outV, const Vec3* inV,  const Matrix* inM, float inF	)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf3,0x0(%0)\n"
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"vmulax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddz	vf9,vf6,vf8\n"
		"mfc1    $6,%3\n"
		"qmtc2   $6,vf10\n"		// vf10.x=inF
		"vmulaw	ACC,vf3,vf0\n"
		"vmaddx	vf9,vf9,vf10\n"
		"qmfc2	$6, vf9\n"
		"sd		$6,0x0(%0)\n"
		"pexew	$6, $6\n"
		"sw		$6,0x8(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV), "f" (inF) : "$6", "memory");
		return outV;
	}
#endif

	ALIGNED128( Vec3, tmp );
	return Vec3Add( outV, outV, Vec3ApplyVector(&tmp,inV,inM,inF) );
}




void
nv::math::Vec3GetBaseFromBZ	(		Vec3* outBX, Vec3* outBY, Vec3* outBZ, const Vec3* inBZ		)
{
	ALIGNED128( Vec3, BX );
	ALIGNED128( Vec3, BY );
	ALIGNED128( Vec3, BZ );

	Vec3Normalize( &BZ, inBZ );

	if( Absf(BZ.y) < 0.8f ) {
		BX = Vec3(0,1,0) ^ BZ;
		BY = BZ ^ BX;
	} else {
		BY = BZ ^ Vec3(1,0,0);
		BX = BY ^ BZ;
	}

	if( outBX )
		Vec3Normalize( outBX, &BX );
	if( outBY )
		Vec3Normalize( outBY, &BY );
	if( outBZ )
		*outBZ = BZ;
}

void
nv::math::Vec3GetBaseFromBX	(		Vec3* outBX, Vec3* outBY, Vec3* outBZ, const Vec3* inBX		)
{
	Vec3GetBaseFromBZ( outBY, outBZ, outBX, inBX );
}

void
nv::math::Vec3GetBaseFromBY	(		Vec3* outBX, Vec3* outBY, Vec3* outBZ, const Vec3* inBY		)
{
	Vec3GetBaseFromBZ( outBX, outBZ, outBY, inBY );
	if( outBZ )
		*outBZ = - *outBZ;
}











//--------------------------
// 4D Vector
//--------------------------



const nv::math::Vec4	nv::math::Vec4::ZERO(0.0f,0.0f,0.0f,0.0f);
const nv::math::Vec4	nv::math::Vec4::ONE(1.0f,1.0f,1.0f,1.0f);
const nv::math::Vec4	nv::math::Vec4::UNIT_X(1.0f,0.0f,0.0f,0.0f);
const nv::math::Vec4	nv::math::Vec4::UNIT_Y(0.0f,1.0f,0.0f,0.0f);
const nv::math::Vec4	nv::math::Vec4::UNIT_Z(0.0f,0.0f,1.0f,0.0f);
const nv::math::Vec4	nv::math::Vec4::UNIT_W(0.0f,0.0f,0.0f,1.0f);


nv::math::Vec4*
nv::math::Vec4Copy			(       Vec4* outV, const Vec4* inV									)
{
	if( outV == inV )
		return outV;

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outV,inV) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"sqc2    vf4,0x0(%0)\n"
		: : "r" (outV) , "r" (inV): "memory");
		return outV;
	}
#endif

	outV->x = inV->x;
	outV->y = inV->y;
	outV->z = inV->z;
	outV->w = inV->w;
	return outV;
}

nv::math::Vec4*
nv::math::Vec4Zero			(       Vec4* outV													)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE(outV) ) {
	    __asm__ __volatile__(
	    "sq    $0,0x0(%0)\n"
		: : "r" (outV) : "memory");
		return outV;
	}
#endif

	outV->x = outV->y = outV->z = outV->w = 0;
	return outV;
}

float
nv::math::Vec4Norm			( const Vec4* inV													)
{
#if defined(MATH_USE_DX)

	return D3DXVec4Length( DXV4(inV) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE(inV) ) {
		ALIGNED128( Vec4, outV );
	    __asm__ __volatile__(
        "lqc2    vf4,0x0(%1)\n"
        "vmul	vf5,vf4,vf4\n"
        "vaddy.x vf5,vf5,vf5\n"
        "vaddz.x vf5,vf5,vf5\n"
        "vaddw.x vf5,vf5,vf5\n"
        "vsqrt Q,vf5x\n"
        "vwaitq\n"
        "vaddq.x vf5,vf0,Q\n"
        "sqc2    vf5,0x0(%0)\n"
        : : "r" (&outV) , "r" (inV) : "memory");
		return outV.x;
	}
#endif

	return Sqrtf( Vec4Dot(inV,inV) );

#endif
}

float
nv::math::Vec4Norm2			( const Vec4* inV													)
{
#if defined(MATH_USE_DX)

	return D3DXVec4LengthSq( DXV4(inV) );

#else

	return Vec4Dot( inV, inV );

#endif
}

nv::math::Vec4*
nv::math::Vec4Normalize		(       Vec4* outV, const Vec4* inV									)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Normalize( DXV4(outV), DXV4(inV) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outV,inV) ) {
	    __asm__ __volatile__(
        "lqc2    vf4,0x0(%1)\n"
        "vmul.xyzw vf5,vf4,vf4\n"
        "vaddy.x vf5,vf5,vf5\n"
        "vaddz.x vf5,vf5,vf5\n"
        "vaddw.x vf5,vf5,vf5\n"
        "vsqrt Q,vf5x\n"
        "vwaitq\n"
        "vaddq.x vf5x,vf0x,Q\n"
        "vnop\n"
        "vnop\n"
        "vdiv    Q,vf0w,vf5x\n"
        "vsub.xyzw vf6,vf0,vf0\n"		// vf6.xyzw=0;
        "vwaitq\n"
        "vmulq.xyzw vf6,vf4,Q\n"
        "sqc2    vf6,0x0(%0)\n"
        : : "r" (outV) , "r" (inV) : "memory");
		return outV;
	}
#endif

	float n	= Vec4Norm( inV );
	NV_ASSERT( n != 0.f );
	float oon = 1.f / n;
	return Vec4Scale( outV, inV, oon );

#endif
}

nv::math::Vec4*
nv::math::Vec4Cross			(       Vec4* outV, const Vec4* inV0, const Vec4* inV1, const Vec4* inV2	)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Cross( DXV4(outV), DXV4(inV0), DXV4(inV1), DXV4(inV2) );

#else

	// "http://research.microsoft.com/~hollasch/thesis/chapter2.html"
	float A, B, C, D, E, F;
	A = (inV1->x * inV2->y) - (inV1->y * inV2->x);
	B = (inV1->x * inV2->z) - (inV1->z * inV2->x);
	C = (inV1->x * inV2->w) - (inV1->w * inV2->x);
	D = (inV1->y * inV2->z) - (inV1->z * inV2->y);
	E = (inV1->y * inV2->w) - (inV1->w * inV2->y);
	F = (inV1->z * inV2->w) - (inV1->w * inV2->z);

	Vec4 tmp;
	tmp.x =   (inV0->y * F) - (inV0->z * E) + (inV0->w * D);
	tmp.y = - (inV0->x * F) + (inV0->z * C) - (inV0->w * B);
	tmp.z =   (inV0->x * E) - (inV0->y * C) + (inV0->w * A);
	tmp.w = - (inV0->x * D) + (inV0->y * B) - (inV0->z * A);
	return Vec4Copy( outV, &tmp );

#endif
}

float
nv::math::Vec4Dot			( const Vec4* inV0, const Vec4* inV1								)
{
#if defined(MATH_USE_DX)

	return D3DXVec4Dot( DXV4(inV0), DXV4(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(inV0,inV1) ) {
		ALIGNED128( Vec4, outV );
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
        "vmul.xyzw vf5,vf4,vf5\n"
        "vaddy.x vf5,vf5,vf5\n"
        "vaddz.x vf5,vf5,vf5\n"
        "vaddw.x vf5,vf5,vf5\n"
		"sqc2    vf5,0x0(%0)\n"
		: : "r" (&outV) , "r" (inV0), "r" (inV1) : "memory");
		return outV.x;
	}
#endif

	return (	inV0->x * inV1->x
			+	inV0->y * inV1->y
			+	inV0->z * inV1->z
			+	inV0->w * inV1->w	);

#endif
}

nv::math::Vec4*
nv::math::Vec4Add				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Add( DXV4(outV), DXV4(inV0), DXV4(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vadd.xyzw	vf6,vf4,vf5\n"
		"sqc2    vf6,0x0(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "memory");
		return outV;
	}
#endif

	outV->x = inV0->x + inV1->x;
	outV->y = inV0->y + inV1->y;
	outV->z = inV0->z + inV1->z;
	outV->w = inV0->w + inV1->w;
	return outV;

#endif
}

nv::math::Vec4*
nv::math::Vec4Sub				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Subtract( DXV4(outV), DXV4(inV0), DXV4(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vsub.xyzw	vf6,vf4,vf5\n"
		"sqc2    vf6,0x0(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "memory");
		return outV;
	}
#endif

	outV->x = inV0->x - inV1->x;
	outV->y = inV0->y - inV1->y;
	outV->z = inV0->z - inV1->z;
	outV->w = inV0->w - inV1->w;
	return outV;

#endif
}

nv::math::Vec4*
nv::math::Vec4Mul				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"vmul.xyzw	vf6,vf4,vf5\n"
		"sqc2    vf6,0x0(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "memory");
		return outV;
	}
#endif

	outV->x = inV0->x * inV1->x;
	outV->y = inV0->y * inV1->y;
	outV->z = inV0->z * inV1->z;
	outV->w = inV0->w * inV1->w;
	return outV;
}

nv::math::Vec4*
nv::math::Vec4Min				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Minimize( DXV4(outV), DXV4(inV0), DXV4(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2		vf4,0x0(%1)\n"
		"lqc2		vf5,0x0(%2)\n"
		"vmini.xyzw	vf6,vf4,vf5\n"
		"sqc2    	vf6,0x0(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "memory");
		return outV;
	}
#endif

	outV->x = math::Min( inV0->x , inV1->x );
	outV->y = math::Min( inV0->y , inV1->y );
	outV->z = math::Min( inV0->z , inV1->z );
	outV->w = math::Min( inV0->w , inV1->w );
	return outV;

#endif
}

nv::math::Vec4*
nv::math::Vec4Max				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Maximize( DXV4(outV), DXV4(inV0), DXV4(inV1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2  	  	vf4,0x0(%1)\n"
		"lqc2  		vf5,0x0(%2)\n"
		"vmax.xyzw	vf6,vf4,vf5\n"
		"sqc2    	vf6,0x0(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1) : "memory");
		return outV;
	}
#endif

	outV->x = math::Max( inV0->x , inV1->x );
	outV->y = math::Max( inV0->y , inV1->y );
	outV->z = math::Max( inV0->z , inV1->z );
	outV->w = math::Max( inV0->w , inV1->w );
	return outV;

#endif
}

nv::math::Vec4*
nv::math::Vec4Scale			(       Vec4* outV, const Vec4* inV,  float inF						)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Scale( DXV4(outV), DXV4(inV), inF );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outV,inV) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"mfc1    $8,%2\n"
		"qmtc2    $8,vf5\n"
		"vmulx.xyzw	vf6,vf4,vf5\n"
		"sqc2    vf6,0x0(%0)\n"
		: : "r" (outV) , "r" (inV), "f" (inF):"$8", "memory");
		return outV;
	}
#endif

	outV->x = inV->x * inF;
	outV->y = inV->y * inF;
	outV->z = inV->z * inF;
	outV->w = inV->w * inF;
	return outV;

#endif
}


nv::math::Vec4*
nv::math::Vec4Lerp			(       Vec4* outV, const Vec4* inV0, const Vec4* inV1, float inF	)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Lerp( DXV4(outV), DXV4(inV0), DXV4(inV1), inF );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV0,inV1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x0(%2)\n"
		"mfc1    $8,%3\n"
		"qmtc2   $8,vf6\n"		// vf6.x=t
		"vsubx	vf7,vf0,vf6\n"	// vf7.w=1-t
		"vmulax	ACC,vf5,vf6\n"
		"vmaddw	vf4,vf4,vf7\n"	// v0*(1-t) + v1*t
		"sqc2    vf4,0x0(%0)\n"
		: : "r" (outV) , "r" (inV0), "r" (inV1), "f" (inF) : "$8", "memory");
		return outV;
	}
#endif

	outV->x = inV0->x + inF * ( inV1->x - inV0->x );
	outV->y = inV0->y + inF * ( inV1->y - inV0->y );
	outV->z = inV0->z + inF * ( inV1->z - inV0->z );
	outV->w = inV0->w + inF * ( inV1->w - inV0->w );
	return outV;

#endif
}


nv::math::Vec4*
nv::math::Vec4Apply			(       Vec4* outV, const Vec4* inV, const Matrix* inM				)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Transform( DXV4(outV), DXV4(inV), DXM(inM) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"vmulax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddaz	ACC,vf6,vf8\n"
		"vmaddw	vf9,vf7,vf8\n"
		"sqc2    vf9,0x0(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV): "memory");
		return outV;
	}
#endif

	Vec4 tmp;
	tmp.x = inV->x * inM->m11 + inV->y * inM->m21 + inV->z * inM->m31 + inV->w * inM->m41;
	tmp.y = inV->x * inM->m12 + inV->y * inM->m22 + inV->z * inM->m32 + inV->w * inM->m42;
	tmp.z = inV->x * inM->m13 + inV->y * inM->m23 + inV->z * inM->m33 + inV->w * inM->m43;
	tmp.w = inV->x * inM->m14 + inV->y * inM->m24 + inV->z * inM->m34 + inV->w * inM->m44;
	return Vec4Copy( outV, &tmp );

#endif
}


nv::math::Vec4*
nv::math::Vec4Apply			(       Vec4* outV, const Vec4* inV, const Matrix* inM, float inF	)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"vmulax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddaz	ACC,vf6,vf8\n"
		"vmaddw	vf9,vf7,vf8\n"
		"mfc1    $8,%3\n"
		"qmtc2   $8,vf10\n"		// vf10.x=inF
		"vmulx	vf9,vf9,vf10\n"
		"sqc2    vf9,0x0(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV), "f" (inF) : "$8", "memory");
		return outV;
	}
#endif

	Vec4 tmp;
	tmp.x = ( inV->x * inM->m11 + inV->y * inM->m21 + inV->z * inM->m31 + inV->w * inM->m41 ) * inF;
	tmp.y = ( inV->x * inM->m12 + inV->y * inM->m22 + inV->z * inM->m32 + inV->w * inM->m42 ) * inF;
	tmp.z = ( inV->x * inM->m13 + inV->y * inM->m23 + inV->z * inM->m33 + inV->w * inM->m43 ) * inF;
	tmp.w = ( inV->x * inM->m14 + inV->y * inM->m24 + inV->z * inM->m34 + inV->w * inM->m44 ) * inF;
	return Vec4Copy( outV, &tmp );
}


nv::math::Vec4*
nv::math::Vec4Apply			(       Vec4* outV, const Vec4* inV, const Quat* inQ				)
{
	// V' = Q * Quat(V) * Q^-1
	Quat r, v( inV->x, inV->y, inV->z, inV->w );
	QuatMul( &r, inQ, QuatMul( &r, &v, QuatFastInverse( &r, inQ ) ) );
	return Vec4Copy( outV, (Vec4*)&r );
}


nv::math::Vec4*
nv::math::Vec4ApplyVector		(       Vec4* outV, const Vec4* inV, const Matrix* inM				)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Transform( DXV4(outV), DXV4(inV), DXM(inM) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2   	vf8,0x0(%2)\n"
		"lqc2	    vf4,0x0(%1)\n"
		"lqc2	    vf5,0x10(%1)\n"
		"lqc2	    vf6,0x20(%1)\n"
		"vmul.w		vf9,vf0,vf0\n"		// .w=1
		"vmulax		ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddz.xyz	vf9,vf6,vf8\n"		// .xyz
		"sqc2    	vf9,0x0(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV): "memory");
		return outV;
	}
#endif

	Vec4 tmp;
	tmp.x = inV->x * inM->m11 + inV->y * inM->m21 + inV->z * inM->m31;
	tmp.y = inV->x * inM->m12 + inV->y * inM->m22 + inV->z * inM->m32;
	tmp.z = inV->x * inM->m13 + inV->y * inM->m23 + inV->z * inM->m33;
	tmp.w = 1.f;
	return Vec4Copy( outV, &tmp );

#endif
}


nv::math::Vec4*
nv::math::Vec4ApplyVector	(       Vec4* outV, const Vec4* inV, const Matrix* inM, float inF	)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2		vf8,0x0(%2)\n"
		"lqc2		vf4,0x0(%1)\n"
		"lqc2		vf5,0x10(%1)\n"
		"lqc2		vf6,0x20(%1)\n"
		"vmul.w		vf9,vf0,vf0\n"	// .w=1
		"vmulax		ACC,vf4,vf8\n"
		"vmadday		ACC,vf5,vf8\n"
		"vmaddz.xyz	vf9,vf6,vf8\n"	// .xyz
		"mfc1		$8,%3\n"
		"qmtc2		$8,vf10\n"		// vf10.x=inF
		"vmulx.xyz	vf9,vf9,vf10\n"
		"sqc2    	vf9,0x0(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV), "f" (inF) : "$8", "memory");
		return outV;
	}
#endif

	Vec4 tmp;
	tmp.x = ( inV->x * inM->m11 + inV->y * inM->m21 + inV->z * inM->m31 + inV->w ) * inF;
	tmp.y = ( inV->x * inM->m12 + inV->y * inM->m22 + inV->z * inM->m32 + inV->w ) * inF;
	tmp.z = ( inV->x * inM->m13 + inV->y * inM->m23 + inV->z * inM->m33 + inV->w ) * inF;
	tmp.w = 1.f;
	return Vec4Copy( outV, &tmp );
}


nv::math::Vec4*
nv::math::Vec4Blend			(       Vec4* outV, const Vec4* inV, const Matrix* inM				)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Transform( DXV4(outV), DXV4(inV), DXM(inM) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf3,0x0(%0)\n"
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"vmulaw	ACC,vf3,vf0\n"
		"vmaddax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddaz	ACC,vf6,vf8\n"
		"vmaddw	vf9,vf7,vf8\n"
		"sqc2    vf9,0x0(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV): "memory");
		return outV;
	}
#endif

	ALIGNED128( Vec4, tmp );
	return Vec4Add( outV, outV, Vec4Apply(&tmp,inV,inM) );

#endif
}


nv::math::Vec4*
nv::math::Vec4Blend			(       Vec4* outV, const Vec4* inV, const Matrix* inM, float inF	)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf3,0x0(%0)\n"
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"vmulax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddaz	ACC,vf6,vf8\n"
		"vmaddw	vf9,vf7,vf8\n"
		"mfc1    $8,%3\n"
		"qmtc2   $8,vf10\n"			// vf10.x=inF
		"vmulaw	ACC,vf3,vf0\n"
		"vmaddx	vf9,vf9,vf10\n"
		"sqc2    vf9,0x0(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV), "f" (inF) : "$8", "memory");
		return outV;
	}
#endif

	ALIGNED128( Vec4, tmp );
	return Vec4Add( outV, outV, Vec4Apply(&tmp,inV,inM,inF) );
}


nv::math::Vec4*
nv::math::Vec4BlendVector		(       Vec4* outV, const Vec4* inV, const Matrix* inM				)
{
#if defined(MATH_USE_DX)

	return (Vec4*) D3DXVec4Transform( DXV4(outV), DXV4(inV), DXM(inM) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2	    vf3,0x0(%0)\n"
		"lqc2   	vf8,0x0(%2)\n"
		"lqc2	    vf4,0x0(%1)\n"
		"lqc2	    vf5,0x10(%1)\n"
		"lqc2	    vf6,0x20(%1)\n"
		"vmul.w		vf9,vf0,vf0\n"	// .w=1
		"vmulaw		ACC,vf3,vf0\n"
		"vmaddax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddz.xyz	vf9,vf6,vf8\n"	// .xyz
		"sqc2    	vf9,0x0(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV): "memory");
		return outV;
	}
#endif

	ALIGNED128( Vec4, tmp );
	return Vec4Add( outV, outV, Vec4ApplyVector(&tmp,inV,inM) );

#endif
}


nv::math::Vec4*
nv::math::Vec4BlendVector	(       Vec4* outV, const Vec4* inV, const Matrix* inM, float inF	)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outV,inV,inM) ) {
	    __asm__ __volatile__(
		"lqc2  		vf3,0x0(%0)\n"
		"lqc2		vf8,0x0(%2)\n"
		"lqc2		vf4,0x0(%1)\n"
		"lqc2		vf5,0x10(%1)\n"
		"lqc2		vf6,0x20(%1)\n"
		"vmul.w		vf9,vf0,vf0\n"	// .w=1
		"vmulax		ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddz.xyz	vf9,vf6,vf8\n"	// .xyz
		"mfc1		$8,%3\n"
		"qmtc2		$8,vf10\n"		// vf10.x=inF
		"vmulaw		ACC,vf3,vf0\n"
		"vmaddx.xyz	vf9,vf9,vf10\n"
		"sqc2    	vf9,0x0(%0)\n"
		: : "r" (outV) , "r" (inM) ,"r" (inV), "f" (inF) : "$8", "memory");
		return outV;
	}
#endif

	ALIGNED128( Vec4, tmp );
	return Vec4Add( outV, outV, Vec4ApplyVector(&tmp,inV,inM,inF) );
}


uint
nv::math::Vec4Clip			(		const Vec4* inV			)
{
#if defined( MATH_USE_VU0 )

	register uint32 cf, tmp0;

	asm volatile (
	"	.set noreorder\n"
	"	andi	%0,  %2, 15\n"
	"	bgtz	%0,  0f\n"			// aligned ?
	"	nop\n"
	"	lqc2	vf1, 0(%2)\n"		// load 1 x QW
	"	b		1f\n"
	"0:	lw		%0, 12(%2)\n"		// load 4 x W
	"	pcpyld	%0, %0, $0\n"
	"	lw		%0, 8(%2)\n"
	"	lw		%1, 4(%2)\n"
	"	pcpyld	%1, %1, $0\n"
	"	lw		%1, 0(%2)\n"
	"	ppacw	%0, %0, %1\n"
	"	qmtc2	%0, vf1\n"
	"1:	vclipw	vf1, vf1\n"
	"	vnop\n"
	"	vnop\n"
	"	vnop\n"
	"	vnop\n"
	"	vnop\n"
	"	cfc2	%0, $vi18\n"
	"	andi	%0, %0, 0x3F\n"
	"	.set reorder\n"
	  : "=&r"(cf), "=&r"(tmp0)
	  : "r"(inV) );

	return cf;

/*
#elif defined( MATH_USE_VFPU )

	asm volatile  (
	"	.set		noreorder\n"			// suppress reordering
	"	lv.q 		c010, %1\n"				// c010 = *pv1
	"	lv.q		c020, %2\n"				// c020 = *pv2
	"	vadd.q		c000, c010, c020\n"		// c000 = c010 + c020
	"	sv.q		c000, %0\n"				// *pv0 = c000
	"	.set reorder\n"
	: "=m"(*pv0)
	: "m"(*pv1), "m"(*pv2)
	);
*/
#else

	uint cf = 0;
	float w = Fabs( inV->w );
	if( inV->x >  w )		cf |= (1<<0);
	if( inV->x < -w )		cf |= (1<<1);
	if( inV->y >  w )		cf |= (1<<2);
	if( inV->y < -w )		cf |= (1<<3);
	if( inV->z >  w )		cf |= (1<<4);
	if( inV->z < -w )		cf |= (1<<5);
	return cf;

#endif
}











//--------------------------
// Matrix
//--------------------------




const nv::math::Matrix	nv::math::Matrix::ZERO(	0.0f,0.0f,0.0f,0.0f,
												0.0f,0.0f,0.0f,0.0f,
												0.0f,0.0f,0.0f,0.0f,
												0.0f,0.0f,0.0f,0.0f		);

const nv::math::Matrix	nv::math::Matrix::UNIT(	1.0f,0.0f,0.0f,0.0f,
												0.0f,1.0f,0.0f,0.0f,
												0.0f,0.0f,1.0f,0.0f,
												0.0f,0.0f,0.0f,1.0f		);


nv::math::Matrix*
nv::math::MatrixCopy			(       Matrix* outM, const Matrix* inM			)
{
	if( outM == inM )
		return outM;

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outM,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"sqc2    vf4,0x0(%0)\n"
		"sqc2    vf5,0x10(%0)\n"
		"sqc2    vf6,0x20(%0)\n"
		"sqc2    vf7,0x30(%0)\n"
		: : "r" (outM), "r" (inM) : "memory");
		return outM;
		return outM;
	}
#endif

	outM->m11 = inM->m11; outM->m12 = inM->m12; outM->m13 = inM->m13; outM->m14 = inM->m14;
	outM->m21 = inM->m21; outM->m22 = inM->m22; outM->m23 = inM->m23; outM->m24 = inM->m24;
	outM->m31 = inM->m31; outM->m32 = inM->m32; outM->m33 = inM->m33; outM->m34 = inM->m34;
	outM->m41 = inM->m41; outM->m42 = inM->m42; outM->m43 = inM->m43; outM->m44 = inM->m44;
	return outM;
}


nv::math::Matrix*
nv::math::MatrixZero			(       Matrix* outM						)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE(outM) ) {
	   __asm__ __volatile__(
		"sq    $0,0x0(%0)\n"
		"sq    $0,0x10(%0)\n"
		"sq    $0,0x20(%0)\n"
		"sq    $0,0x30(%0)\n"
		: : "r" (outM) : "memory" );
		return outM;
	}
#endif

	outM->m11 = outM->m12 = outM->m13 = outM->m14 = 0;
	outM->m21 = outM->m22 = outM->m23 = outM->m24 = 0;
	outM->m31 = outM->m32 = outM->m33 = outM->m34 = 0;
	outM->m41 = outM->m42 = outM->m43 = outM->m44 = 0;
	return outM;
}


nv::math::Matrix*
nv::math::MatrixIdentity		(       Matrix* outM		)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixIdentity( DXM(outM) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE(outM) ) {
	    __asm__ __volatile__(
		"vsub.xyzw	vf4,vf0,vf0\n"	// vf4.xyzw=0;
		"vadd.w	vf4,vf4,vf0\n"
		"vmr32.xyzw	vf5,vf4\n"
		"vmr32.xyzw	vf6,vf5\n"
		"vmr32.xyzw	vf7,vf6\n"
		"sqc2    vf4,0x30(%0)\n"
		"sqc2    vf5,0x20(%0)\n"
		"sqc2    vf6,0x10(%0)\n"
		"sqc2    vf7,0x0(%0)\n"
		: : "r" (outM) : "memory");
		return outM;
	}
#endif

	outM->m11=1.0f; outM->m12=0.0f; outM->m13=0.0f; outM->m14=0.0f;
	outM->m21=0.0f; outM->m22=1.0f; outM->m23=0.0f; outM->m24=0.0f;
	outM->m31=0.0f; outM->m32=0.0f; outM->m33=1.0f; outM->m34=0.0f;
	outM->m41=0.0f; outM->m42=0.0f; outM->m43=0.0f; outM->m44=1.0f;
	return outM;

#endif
}


bool
nv::math::MatrixIsIdentity	( const Matrix*	inM		)
{
#if defined(MATH_USE_DX)

	return D3DXMatrixIsIdentity( DXM(inM) ) != 0;

#else

	return (   inM->m11==1.0f && inM->m12==0.0f && inM->m13==0.0f && inM->m14==0.0f
			&& inM->m21==0.0f && inM->m22==1.0f && inM->m23==0.0f && inM->m24==0.0f
			&& inM->m31==0.0f && inM->m32==0.0f && inM->m33==1.0f && inM->m34==0.0f
			&& inM->m41==0.0f && inM->m42==0.0f && inM->m43==0.0f && inM->m44==1.0f	);

#endif
}


float
nv::math::MatrixDeterminant	( const Matrix* inM		)
{
#if defined(MATH_USE_DX)

	return D3DXMatrixDeterminant( DXM(inM) );

#else

	float det0 = inM->m33 * inM->m44 - inM->m43 * inM->m34;
	float det1 = inM->m32 * inM->m44 - inM->m42 * inM->m34;
	float det2 = inM->m32 * inM->m43 - inM->m42 * inM->m33;
	float det3 = inM->m31 * inM->m44 - inM->m41 * inM->m34;
	float det4 = inM->m31 * inM->m43 - inM->m41 * inM->m33;
	float det5 = inM->m31 * inM->m42 - inM->m41 * inM->m32;

	float det;
	det	=  inM->m11 * ( inM->m22 * det0 - inM->m23 * det1 + inM->m24 * det2 )
		 - inM->m12 * ( inM->m21 * det0 - inM->m23 * det3 + inM->m24 * det4 )
		 + inM->m13 * ( inM->m21 * det1 - inM->m22 * det3 + inM->m24 * det5 )
		 - inM->m14 * ( inM->m21 * det2 - inM->m22 * det4 + inM->m23 * det5 );
	return det;

#endif
}


nv::math::Matrix*
nv::math::MatrixAdd			(       Matrix* outM, const Matrix* inM0, const Matrix* inM1		)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outM,inM0,inM1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf9,0x10(%2)\n"
		"lqc2    vf10,0x20(%2)\n"
		"lqc2    vf11,0x30(%2)\n"
		"vadd	vf4, vf4, vf8\n"
		"vadd	vf5, vf5, vf9\n"
		"vadd	vf6, vf6, vf10\n"
		"vadd	vf7, vf7, vf11\n"
		"sqc2    vf4,0x0(%0)\n"
		"sqc2    vf5,0x10(%0)\n"
		"sqc2    vf6,0x20(%0)\n"
		"sqc2    vf7,0x30(%0)\n"
		: : "r" (outM), "r" (inM0), "r" (inM1) : "memory");
		return outM;
	}
#endif

	outM->m11 = inM0->m11 + inM1->m11; outM->m12 = inM0->m12 + inM1->m12; outM->m13 = inM0->m13 + inM1->m13; outM->m14 = inM0->m14 + inM1->m14;
	outM->m21 = inM0->m21 + inM1->m21; outM->m22 = inM0->m22 + inM1->m22; outM->m23 = inM0->m23 + inM1->m23; outM->m24 = inM0->m24 + inM1->m24;
	outM->m31 = inM0->m31 + inM1->m31; outM->m32 = inM0->m32 + inM1->m32; outM->m33 = inM0->m33 + inM1->m33; outM->m34 = inM0->m34 + inM1->m34;
	outM->m41 = inM0->m41 + inM1->m41; outM->m42 = inM0->m42 + inM1->m42; outM->m43 = inM0->m43 + inM1->m43; outM->m44 = inM0->m44 + inM1->m44;
	return outM;
}


nv::math::Matrix*
nv::math::MatrixSub			(       Matrix* outM, const Matrix* inM0, const Matrix* inM1		)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outM,inM0,inM1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"lqc2    vf8,0x0(%2)\n"
		"lqc2    vf9,0x10(%2)\n"
		"lqc2    vf10,0x20(%2)\n"
		"lqc2    vf11,0x30(%2)\n"
		"vsub	vf4, vf4, vf8\n"
		"vsub	vf5, vf5, vf9\n"
		"vsub	vf6, vf6, vf10\n"
		"vsub	vf7, vf7, vf11\n"
		"sqc2    vf4,0x0(%0)\n"
		"sqc2    vf5,0x10(%0)\n"
		"sqc2    vf6,0x20(%0)\n"
		"sqc2    vf7,0x30(%0)\n"
		: : "r" (outM), "r" (inM0), "r" (inM1) : "memory");
		return outM;
	}
#endif

	outM->m11 = inM0->m11 - inM1->m11; outM->m12 = inM0->m12 - inM1->m12; outM->m13 = inM0->m13 - inM1->m13; outM->m14 = inM0->m14 - inM1->m14;
	outM->m21 = inM0->m21 - inM1->m21; outM->m22 = inM0->m22 - inM1->m22; outM->m23 = inM0->m23 - inM1->m23; outM->m24 = inM0->m24 - inM1->m24;
	outM->m31 = inM0->m31 - inM1->m31; outM->m32 = inM0->m32 - inM1->m32; outM->m33 = inM0->m33 - inM1->m33; outM->m34 = inM0->m34 - inM1->m34;
	outM->m41 = inM0->m41 - inM1->m41; outM->m42 = inM0->m42 - inM1->m42; outM->m43 = inM0->m43 - inM1->m43; outM->m44 = inM0->m44 - inM1->m44;
	return outM;
}


nv::math::Matrix*
nv::math::MatrixScalar		(       Matrix* outM, const Matrix* inM, float inF			)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outM,inM) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%1)\n"
		"lqc2    vf5,0x10(%1)\n"
		"lqc2    vf6,0x20(%1)\n"
		"lqc2    vf7,0x30(%1)\n"
		"mfc1    $8,%2\n"
		"qmtc2   $8,vf8\n"		// vf8.x=f
		"vmulx	vf4, vf4, vf8\n"
		"vmulx	vf5, vf5, vf8\n"
		"vmulx	vf6, vf6, vf8\n"
		"vmulx	vf7, vf7, vf8\n"
		"sqc2    vf4,0x0(%0)\n"
		"sqc2    vf5,0x10(%0)\n"
		"sqc2    vf6,0x20(%0)\n"
		"sqc2    vf7,0x30(%0)\n"
		: : "r" (outM), "r" (inM), "f" (inF) : "$8", "memory");
		return outM;
	}
#endif

	outM->m11 = inM->m11 * inF; outM->m12 = inM->m12 * inF; outM->m13 = inM->m13 * inF; outM->m14 = inM->m14 * inF;
	outM->m21 = inM->m21 * inF; outM->m22 = inM->m22 * inF; outM->m23 = inM->m23 * inF; outM->m24 = inM->m24 * inF;
	outM->m31 = inM->m31 * inF; outM->m32 = inM->m32 * inF; outM->m33 = inM->m33 * inF; outM->m34 = inM->m34 * inF;
	outM->m41 = inM->m41 * inF; outM->m42 = inM->m42 * inF; outM->m43 = inM->m43 * inF; outM->m44 = inM->m44 * inF;
	return outM;
}


nv::math::Matrix*
nv::math::MatrixMul			(       Matrix* outM, const Matrix* inM0, const Matrix* inM1		)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixMultiply( DXM(outM), DXM(inM0), DXM(inM1) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(outM,inM0,inM1) ) {
	    __asm__ __volatile__(
		"lqc2    vf4,0x0(%2)\n"
		"lqc2    vf5,0x10(%2)\n"
		"lqc2    vf6,0x20(%2)\n"
		"lqc2    vf7,0x30(%2)\n"
		"lqc2    vf8,0x0(%1)\n"
		"lqc2    vf9,0x10(%1)\n"
		"lqc2    vf10,0x20(%1)\n"
		"lqc2    vf11,0x30(%1)\n"
		"vmulax	ACC,vf4,vf8\n"
		"vmadday	ACC,vf5,vf8\n"
		"vmaddaz	ACC,vf6,vf8\n"
		"vmaddw	vf8,vf7,vf8\n"
		"vmulax	ACC,vf4,vf9\n"
		"vmadday	ACC,vf5,vf9\n"
		"vmaddaz	ACC,vf6,vf9\n"
		"vmaddw	vf9,vf7,vf9\n"
		"vmulax	ACC,vf4,vf10\n"
		"vmadday	ACC,vf5,vf10\n"
		"vmaddaz	ACC,vf6,vf10\n"
		"vmaddw	vf10,vf7,vf10\n"
		"vmulax	ACC,vf4,vf11\n"
		"vmadday	ACC,vf5,vf11\n"
		"vmaddaz	ACC,vf6,vf11\n"
		"vmaddw	vf11,vf7,vf11\n"
		"sqc2    vf8,0x0(%0)\n"
		"sqc2    vf9,0x10(%0)\n"
		"sqc2    vf10,0x20(%0)\n"
		"sqc2    vf11,0x30(%0)\n"
		:: "r" (outM), "r" (inM0), "r" (inM1) : "memory");
		return outM;
	}
#endif

#if defined(MATH_USE_VFPU)
	if( IS_VFPUABLE_3(outM,inM0,inM1) ) {
		__asm__ (
			".set			noreorder\n"			// suppress reordering
			"lv.q			c100,  0(%1)\n"			// c100 = pm1->x
			"lv.q			c110, 16(%1)\n"			// c110 = pm1->y
			"lv.q			c120, 32(%1)\n"			// c120 = pm1->z
			"lv.q			c130, 48(%1)\n"			// c130 = pm1->w
			"lv.q			c200,  0(%2)\n"			// c200 = pm2->x
			"lv.q			c210, 16(%2)\n"			// c210 = pm2->y
			"lv.q			c220, 32(%2)\n"			// c220 = pm2->z
			"lv.q			c230, 48(%2)\n"			// c230 = pm2->w
			"vmmul.q		e000, e200, e100\n"		// e000 = e100 * e200
			"sv.q			c000,  0(%0)\n"			// pm0->x = c000
			"sv.q			c010, 16(%0)\n"			// pm0->y = c010
			"sv.q			c020, 32(%0)\n"			// pm0->z = c020
			"sv.q			c030, 48(%0)\n"			// pm0->w = c030
			".set			reorder\n"				// restore reordering
		:: "r" (outM), "r" (inM1), "r" (inM0) : "memory" );
		return outM;
	}
#endif

	Matrix tmp;
	for( uint r = 0 ; r < 4 ; r++ ) {
		for( uint c = 0 ; c < 4 ; c++ ) {
			tmp.m[r][c] = inM0->m[r][0] * inM1->m[0][c]
						+ inM0->m[r][1] * inM1->m[1][c]
						+ inM0->m[r][2] * inM1->m[2][c]
						+ inM0->m[r][3] * inM1->m[3][c];
		}
	}
	return MatrixCopy( outM, &tmp );

#endif
}

nv::math::Matrix*
nv::math::MatrixTranspose		(       Matrix* outM, const Matrix* inM								)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixTranspose( DXM(outM), DXM(inM) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outM,inM) ) {
	    __asm__ __volatile__(
		"lq $8,0x0000(%1)\n"
		"lq $9,0x0010(%1)\n"
		"lq $10,0x0020(%1)\n"
		"lq $11,0x0030(%1)\n"
		"pextlw     $12,$9,$8\n"
		"pextuw     $13,$9,$8\n"
		"pextlw     $14,$11,$10\n"
		"pextuw     $15,$11,$10\n"
		"pcpyld     $8,$14,$12\n"
		"pcpyud     $9,$12,$14\n"
		"pcpyld     $10,$15,$13\n"
		"pcpyud     $11,$13,$15\n"
		"sq $8,0x0000(%0)\n"
		"sq $9,0x0010(%0)\n"
		"sq $10,0x0020(%0)\n"
		"sq $11,0x0030(%0)\n"
		: : "r" (outM) , "r" (inM) : "memory");
		return outM;
	}
#endif

	Matrix tmp;
	tmp.m11 = inM->m11; tmp.m12 = inM->m21; tmp.m13 = inM->m31; tmp.m14 = inM->m41;
	tmp.m21 = inM->m12; tmp.m22 = inM->m22; tmp.m23 = inM->m32; tmp.m24 = inM->m42;
	tmp.m31 = inM->m13; tmp.m32 = inM->m23; tmp.m33 = inM->m33; tmp.m34 = inM->m43;
	tmp.m41 = inM->m14; tmp.m42 = inM->m24; tmp.m43 = inM->m34; tmp.m44 = inM->m44;
	return MatrixCopy( outM, &tmp );

#endif
}


nv::math::Matrix*
nv::math::MatrixInverse		(		Matrix* outM, float * outDeterminant, const Matrix* inM		)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixInverse( DXM(outM), outDeterminant, DXM(inM) );

#else

	Matrix tmp;
	// Calculate the determinant of the 3x3 rotation matrix
	float det=0.f;
	det += inM->m11 * inM->m22 * inM->m33;
	det += inM->m21 * inM->m32 * inM->m13;
	det += inM->m31 * inM->m12 * inM->m23;
	det -= inM->m31 * inM->m22 * inM->m13;
	det -= inM->m21 * inM->m12 * inM->m33;
	det -= inM->m11 * inM->m32 * inM->m23;
	if( outDeterminant )
		*outDeterminant = det;

	// Invert the 3x3 rotation matrix
	NV_ASSERT( det != 0.f );
	float oodet = 1.f / det;
	tmp.m11 =  (inM->m22 * inM->m33 - inM->m32 * inM->m23) * oodet;
	tmp.m12 = -(inM->m12 * inM->m33 - inM->m32 * inM->m13) * oodet;
	tmp.m13 =  (inM->m12 * inM->m23 - inM->m22 * inM->m13) * oodet;
	tmp.m21 = -(inM->m21 * inM->m33 - inM->m31 * inM->m23) * oodet;
	tmp.m22 =  (inM->m11 * inM->m33 - inM->m31 * inM->m13) * oodet;
	tmp.m23 = -(inM->m11 * inM->m23 - inM->m21 * inM->m13) * oodet;
	tmp.m31 =  (inM->m21 * inM->m32 - inM->m31 * inM->m22) * oodet;
	tmp.m32 = -(inM->m11 * inM->m32 - inM->m31 * inM->m12) * oodet;
	tmp.m33 =  (inM->m11 * inM->m22 - inM->m21 * inM->m12) * oodet;

	// Translation
	tmp.m41 = -(tmp.m11 * inM->m41 + tmp.m21 * inM->m42 + tmp.m31 * inM->m43);
	tmp.m42 = -(tmp.m12 * inM->m41 + tmp.m22 * inM->m42 + tmp.m32 * inM->m43);
	tmp.m43 = -(tmp.m13 * inM->m41 + tmp.m23 * inM->m42 + tmp.m33 * inM->m43);

	tmp.m14 = 0.f;
	tmp.m24 = 0.f;
	tmp.m34 = 0.f;
	tmp.m44 = 1.f;
	return MatrixCopy( outM, &tmp );

#endif
}

nv::math::Matrix*
nv::math::MatrixFastInverse	(		Matrix* outM, const Matrix* inM		)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outM,inM) ) {
	    __asm__ __volatile__(
		"lq $8,0x0000(%1)\n"
		"lq $9,0x0010(%1)\n"
		"lq $10,0x0020(%1)\n"
		"lqc2 vf4,0x0030(%1)\n"
		"vmove.xyzw vf5,vf4\n"
		"vsub.xyz vf4,vf4,vf4\n"		// vf4.xyz=0;
		"vmove.xyzw vf9,vf4\n"
		"qmfc2    $11,vf4\n"
		"pextlw     $12,$9,$8\n"
		"pextuw     $13,$9,$8\n"
		"pextlw     $14,$11,$10\n"
		"pextuw     $15,$11,$10\n"
		"pcpyld     $8,$14,$12\n"
		"pcpyud     $9,$12,$14\n"
		"pcpyld     $10,$15,$13\n"
		"qmtc2    $8,vf6\n"
		"qmtc2    $9,vf7\n"
		"qmtc2    $10,vf8\n"
		"vmulax.xyz	ACC,   vf6,vf5\n"
		"vmadday.xyz	ACC,   vf7,vf5\n"
		"vmaddz.xyz	vf4,vf8,vf5\n"
		"vsub.xyz	vf4,vf9,vf4\n"
		"sq $8,0x0000(%0)\n"
		"sq $9,0x0010(%0)\n"
		"sq $10,0x0020(%0)\n"
		"sqc2 vf4,0x0030(%0)\n"
		: : "r" (outM) , "r" (inM):"$8","$9","$10","$11","$12","$13","$14","$15", "memory");
		return outM;
	}
#endif

	// R^-1
	MatrixTranspose( outM, inM );
	// T o R
	if( outM->m14 || outM->m24 || outM->m34 ) {
		outM->m41 = - ( outM->m14*outM->m11 + outM->m24*outM->m21 + outM->m34*outM->m31 );
		outM->m42 = - ( outM->m14*outM->m12 + outM->m24*outM->m22 + outM->m34*outM->m32 );
		outM->m43 = - ( outM->m14*outM->m13 + outM->m24*outM->m23 + outM->m34*outM->m33 );
		outM->m14 = outM->m24 = outM->m34 = 0.0f;
	}
	return outM;
}

nv::math::Matrix*
nv::math::MatrixOrthoNormalize		(	Matrix* outM, const Matrix* inM		)
{
	Vec3Cross( outM->right(), inM->up(), inM->front() );
	Vec3Cross( outM->front(), outM->right(), inM->up() );
	if( outM != inM )
		Vec3Copy( outM->up(), inM->up() );
	return MatrixNormalize( outM, inM );
}

nv::math::Matrix*
nv::math::MatrixNormalize	(	Matrix* outM, const Matrix* inM		)
{
	Vec3Normalize( outM->right(), inM->right() );
	Vec3Normalize( outM->up(),    inM->up()    );
	Vec3Normalize( outM->front(), inM->front() );
	if( outM != inM ) {
		outM->m14 = inM->m14;
		outM->m24 = inM->m24;
		outM->m34 = inM->m34;
		outM->m41 = inM->m41;
		outM->m42 = inM->m42;
		outM->m43 = inM->m43;
		outM->m44 = inM->m44;
	}
	return outM;
}

nv::math::Matrix*
nv::math::MatrixScale			(		Matrix* outM, const Matrix* inM, float inSX, float inSY, float inSZ	)
{
	Matrix m;
	MatrixScaling( &m, inSX, inSY, inSZ );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixScale			(		Matrix* outM, const Matrix* inM, const Vec3* inV					)
{
	return MatrixScale( outM, inM, inV->x, inV->y, inV->z );
}

nv::math::Matrix*
nv::math::MatrixScale			(		Matrix* outM, const Matrix* inM, const Matrix* inS					)
{
	Matrix m;
	MatrixScaling( &m, inS );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixPreScale		(		Matrix* outM, const Matrix* inM, float inSX, float inSY, float inSZ	)
{
	Matrix m;
	MatrixScaling( &m, inSX, inSY, inSZ );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixPreScale		(		Matrix* outM, const Matrix* inM, const Vec3* inV					)
{
	return MatrixPreScale( outM, inM, inV->x, inV->y, inV->z );
}

nv::math::Matrix*
nv::math::MatrixPreScale		(		Matrix* outM, const Matrix* inM, const Matrix* inS					)
{
	Matrix m;
	MatrixScaling( &m, inS );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixTranslate		(		Matrix* outM, const Matrix* inM, float inTX, float inTY, float inTZ	)
{
	Matrix m;
	MatrixTranslation( &m, inTX, inTY, inTZ );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixTranslate		(		Matrix* outM, const Matrix* inM, const Vec3* inV					)
{
	return MatrixTranslate( outM, inM, inV->x, inV->y, inV->z );
}

nv::math::Matrix*
nv::math::MatrixTranslate		(		Matrix* outM, const Matrix* inM, const Matrix* inT					)
{
	Matrix m;
	MatrixTranslation( &m, inT );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixPreTranslate	(		Matrix* outM, const Matrix* inM, float inTX, float inTY, float inTZ	)
{
	Matrix m;
	MatrixTranslation( &m, inTX, inTY, inTZ );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixPreTranslate	(		Matrix* outM, const Matrix* inM, const Vec3* inV					)
{
	return MatrixPreTranslate( outM, inM, inV->x, inV->y, inV->z );
}

nv::math::Matrix*
nv::math::MatrixPreTranslate		(		Matrix* outM, const Matrix* inM, const Matrix* inT				)
{
	Matrix m;
	MatrixTranslation( &m, inT );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixRotate		(		Matrix* outM, const Matrix* inM, const Quat* inQ						)
{
	Matrix m;
	MatrixRotation( &m, inQ );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixRotate		(		Matrix* outM, const Matrix* inM, const Vec3* inAxis, float inAngle		)
{
	Matrix m;
	MatrixRotation( &m, inAxis, inAngle );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixRotate		(		Matrix* outM, const Matrix* inM, const Matrix* inR						)
{
	Matrix m;
	MatrixRotation( &m, inR );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixPreRotate	(		Matrix* outM, const Matrix* inM, const Quat* inQ						)
{
	Matrix m;
	MatrixRotation( &m, inQ );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixPreRotate	(		Matrix* outM, const Matrix* inM, const Vec3* inAxis, float inAngle		)
{
	Matrix m;
	MatrixRotation( &m, inAxis, inAngle );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixPreRotate	(		Matrix* outM, const Matrix* inM, const Matrix* inR						)
{
	Matrix m;
	MatrixRotation( &m, inR );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixRotateX		(		Matrix* outM, const Matrix* inM, float inAngle							)
{
	Matrix m;
	MatrixRotationX( &m, inAngle );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixPreRotateX	(		Matrix* outM, const Matrix* inM, float inAngle							)
{
	Matrix m;
	MatrixRotationX( &m, inAngle );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixRotateY		(		Matrix* outM, const Matrix* inM, float inAngle							)
{
	Matrix m;
	MatrixRotationY( &m, inAngle );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixPreRotateY	(		Matrix* outM, const Matrix* inM, float inAngle							)
{
	Matrix m;
	MatrixRotationY( &m, inAngle );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixRotateZ		(		Matrix* outM, const Matrix* inM, float inAngle							)
{
	Matrix m;
	MatrixRotationZ( &m, inAngle );
	return MatrixMul( outM, inM, &m );
}

nv::math::Matrix*
nv::math::MatrixPreRotateZ	(		Matrix* outM, const Matrix* inM, float inAngle							)
{
	Matrix m;
	MatrixRotationZ( &m, inAngle );
	return MatrixMul( outM, &m, inM );
}

nv::math::Matrix*
nv::math::MatrixScaling			(		Matrix* outM, float inSX, float inSY, float inSZ			)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixScaling( DXM(outM), inSX, inSY, inSZ );

#else

	MatrixZero( outM );
	outM->m11 = inSX;
	outM->m22 = inSY;
	outM->m33 = inSZ;
	outM->m44 = 1.0f;
	return outM;

#endif
}

nv::math::Matrix*
nv::math::MatrixScaling			(		Matrix* outM, const Vec3* inV								)
{
	return MatrixScaling( outM, inV->x, inV->y, inV->z );
}

nv::math::Matrix*
nv::math::MatrixScaling			(		Matrix* outM, const Matrix* inM	 							)
{
	Vec3 s;
	MatrixGetTR( NULL, NULL, &s, inM );
	return MatrixScaling( outM, &s );
}

nv::math::Matrix*
nv::math::MatrixTranslation		(		Matrix* outM, float inTX, float inTY, float inTZ			)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixTranslation( DXM(outM), inTX, inTY, inTZ );

#else

	MatrixIdentity( outM );
	outM->m41 = inTX;
	outM->m42 = inTY;
	outM->m43 = inTZ;
	return outM;

#endif
}

nv::math::Matrix*
nv::math::MatrixTranslation		(		Matrix* outM, const Vec3* inV								)
{
	return MatrixTranslation( outM, inV->x, inV->y, inV->z );
}

nv::math::Matrix*
nv::math::MatrixTranslation		(		Matrix* outM, const Matrix* inM	 							)
{
	return MatrixTranslation( outM, inM->m41, inM->m42, inM->m43 );
}

nv::math::Matrix*
nv::math::MatrixRotationX		(		Matrix* outM, float inAngle									)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixRotationX( DXM(outM), inAngle );

#else

	float s, c;
	Sincosf( inAngle, &s, &c );
	MatrixIdentity( outM );
	outM->m22 =  c;
	outM->m23 =  s;
	outM->m32 = -s;
	outM->m33 =  c;
	return outM;

#endif
}

nv::math::Matrix*
nv::math::MatrixRotationY		(		Matrix* outM, float inAngle									)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixRotationY( DXM(outM), inAngle );

#else

	float s, c;
	Sincosf( inAngle, &s, &c );
	MatrixIdentity( outM );
	outM->m11 =  c;
	outM->m13 = -s;
	outM->m31 =  s;
	outM->m33 =  c;
	return outM;

#endif
}

nv::math::Matrix*
nv::math::MatrixRotationZ		(		Matrix* outM, float inAngle									)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixRotationZ( DXM(outM), inAngle );

#else

	float s, c;
	Sincosf( inAngle, &s, &c );
	MatrixIdentity( outM );
	outM->m11 =  c;
	outM->m12 =  s;
	outM->m21 = -s;
	outM->m22 =  c;
	return outM;

#endif
}

nv::math::Matrix*
nv::math::MatrixRotation		(		Matrix* outM, const Vec3* inAxis, float inAngle				)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixRotationAxis( DXM(outM), DXV3(inAxis), inAngle );

#else

	Quat q( inAngle, *inAxis );
	return MatrixRotation( outM, &q );

#endif
}

nv::math::Matrix*
nv::math::MatrixRotation		(		Matrix* outM, const Quat* inQ			)
{
#if defined(MATH_USE_DX)

	return (Matrix*) D3DXMatrixRotationQuaternion( DXM(outM), DXQ(inQ) );

#else

	float s  = 2.f;
	float xs = inQ->x*s;
	float ys = inQ->y*s;
	float zs = inQ->z*s;
	float wx = inQ->w*xs;
	float wy = inQ->w*ys;
	float wz = inQ->w*zs;
	float xx = inQ->x*xs;
	float xy = inQ->x*ys;
	float xz = inQ->x*zs;
	float yy = inQ->y*ys;
	float yz = inQ->y*zs;
	float zz = inQ->z*zs;

	outM->m11 = (1.0f-(yy+zz));
	outM->m12 = (xy+wz);
	outM->m13 = (xz-wy);
	outM->m21 = (xy-wz);
	outM->m22 = (1.0f-(xx+zz));
	outM->m23 = (yz+wx);
	outM->m31 = (xz+wy);
	outM->m32 = (yz-wx);
	outM->m33 = (1.0f-(xx+yy));
	outM->m41 = outM->m42 = outM->m43 = 0.0f;
	outM->m14 = outM->m24 = outM->m34 = 0.0f;
	outM->m44 = 1.0f;
	return outM;

#endif
}

nv::math::Matrix*
nv::math::MatrixRotation		(		Matrix* outM, const Matrix* inM			)
{
	if( outM != inM )
		MatrixCopy( outM, inM );
	outM->m41 = outM->m42 = outM->m43 = 0.0f;
	return outM;
}

nv::math::Matrix*
nv::math::MatrixBuildTR		(		Matrix*	outM, const Vec3* inT, const Quat* inR, const Vec3* inS	)
{
	if( inR )	MatrixRotation( outM, inR );
	else		MatrixIdentity( outM );
	if( inT ) {
		outM->m41 = inT->x;
		outM->m42 = inT->y;
		outM->m43 = inT->z;
	}
	if( inS )
		MatrixPreScale( outM, outM, inS );
	return outM;
}

void
nv::math::MatrixGetTR			(		Vec3*	outT, Quat* outR, Vec3*	outS, const Matrix*	inM					)
{
	if( outT ) {
		outT->x = inM->m41;
		outT->y = inM->m42;
		outT->z = inM->m43;
	}
	if( outR || outS ) {
		Matrix m;
		float sx = Sqrtf( inM->m11*inM->m11 + inM->m12*inM->m12 + inM->m13*inM->m13 );
		float sy = Sqrtf( inM->m21*inM->m21 + inM->m22*inM->m22 + inM->m23*inM->m23 );
		float sz = Sqrtf( inM->m31*inM->m31 + inM->m32*inM->m32 + inM->m33*inM->m33 );
		if( outR ) {
			float oosx = (sx==0.f) ? 0.f : 1.f/sx;
			float oosy = (sy==0.f) ? 0.f : 1.f/sy;
			float oosz = (sz==0.f) ? 0.f : 1.f/sz;
			m.m11 = inM->m11 * oosx;	m.m12 = inM->m12 * oosx;	m.m13 = inM->m13 * oosx;
			m.m21 = inM->m21 * oosy;	m.m22 = inM->m22 * oosy;	m.m23 = inM->m23 * oosy;
			m.m31 = inM->m31 * oosz;	m.m32 = inM->m32 * oosz;	m.m33 = inM->m33 * oosz;
			QuatFromMatrix( outR, &m );
		}
		if( outS ) {
			outS->x = sx;
			outS->y = sy;
			outS->z = sz;
		}
	}
}

nv::math::Matrix*
nv::math::MatrixSetTR			(		Matrix*	outM, const Vec3* inT, const Quat* inR, const Vec3* inS, const Matrix* inM	)
{
	if( !inM )
		return MatrixBuildTR( outM, inT, inR, inS );

	// t ?
	if( inT ) {
		outM->m41 = inT->x;
		outM->m42 = inT->y;
		outM->m43 = inT->z;
	} else if( inM != outM ) {
		outM->m41 = inM->m41;
		outM->m42 = inM->m42;
		outM->m43 = inM->m43;
	}

	// r ?
	if( inR ) {
		float s  = 2.f;
		float xs = inR->x*s;
		float ys = inR->y*s;
		float zs = inR->z*s;
		float wx = inR->w*xs;
		float wy = inR->w*ys;
		float wz = inR->w*zs;
		float xx = inR->x*xs;
		float xy = inR->x*ys;
		float xz = inR->x*zs;
		float yy = inR->y*ys;
		float yz = inR->y*zs;
		float zz = inR->z*zs;
		if( inS ) {
			outM->m11 = inS->x * (1.0f-(yy+zz));
			outM->m12 = inS->x * (xy+wz);
			outM->m13 = inS->x * (xz-wy);
			outM->m21 = inS->y * (xy-wz);
			outM->m22 = inS->y * (1.0f-(xx+zz));
			outM->m23 = inS->y * (yz+wx);
			outM->m31 = inS->z * (xz+wy);
			outM->m32 = inS->z * (yz-wx);
			outM->m33 = inS->z * (1.0f-(xx+yy));
		} else {
			outM->m11 = (1.0f-(yy+zz));
			outM->m12 = (xy+wz);
			outM->m13 = (xz-wy);
			outM->m21 = (xy-wz);
			outM->m22 = (1.0f-(xx+zz));
			outM->m23 = (yz+wx);
			outM->m31 = (xz+wy);
			outM->m32 = (yz-wx);
			outM->m33 = (1.0f-(xx+yy));
		}
		return outM;
	}

	// no-r & s ?
	if( inS ) {
		// get scale in inM
		float sx2 = inM->m11*inM->m11 + inM->m12*inM->m12 + inM->m13*inM->m13;
		float sy2 = inM->m21*inM->m21 + inM->m22*inM->m22 + inM->m23*inM->m23;
		float sz2 = inM->m31*inM->m31 + inM->m32*inM->m32 + inM->m33*inM->m33;
		if( sx2>0.f ) {
			float sx = Abs(1.f-sx2)>0.1f ? inS->x/Sqrtf(sx2) : inS->x;
			outM->m11 = sx * inM->m11;
			outM->m12 = sx * inM->m12;
			outM->m13 = sx * inM->m13;
		} else if( inM != outM ) {
			outM->m11 = inM->m11;
			outM->m12 = inM->m12;
			outM->m13 = inM->m13;
		}
		if( sy2>0.f ) {
			float sy = Abs(1.f-sy2)>0.1f ? inS->y/Sqrtf(sy2) : inS->y;
			outM->m21 = sy * inM->m21;
			outM->m22 = sy * inM->m22;
			outM->m23 = sy * inM->m23;
		} else if( inM != outM ) {
			outM->m21 = inM->m21;
			outM->m22 = inM->m22;
			outM->m23 = inM->m23;
		}
		if( sz2>0.f ) {
			float sz = Abs(1.f-sz2)>0.1f ? inS->z/Sqrtf(sz2) : inS->z;
			outM->m31 = sz * inM->m31;
			outM->m32 = sz * inM->m32;
			outM->m33 = sz * inM->m33;
		} else if( inM != outM ) {
			outM->m31 = inM->m31;
			outM->m32 = inM->m32;
			outM->m33 = inM->m33;
		}
		return outM;
	}

	// no-r & no-s
	if( inM != outM ) {
		outM->m11 = inM->m11;
		outM->m12 = inM->m12;
		outM->m13 = inM->m13;
		outM->m21 = inM->m21;
		outM->m22 = inM->m22;
		outM->m23 = inM->m23;
		outM->m31 = inM->m31;
		outM->m32 = inM->m32;
		outM->m33 = inM->m33;
	}

	return outM;
}

void
nv::math::MatrixGetAxis		(		Vec3*	outRight, Vec3* outUp, Vec3* outFront, const Matrix* inM	)
{
	if( outRight ) {
		outRight->x = inM->m11;
		outRight->y = inM->m12;
		outRight->z = inM->m13;
	}
	if( outUp ) {
		outUp->x = inM->m21;
		outUp->y = inM->m22;
		outUp->z = inM->m23;
	}
	if( outFront ) {
		outFront->x = inM->m31;
		outFront->y = inM->m32;
		outFront->z = inM->m33;
	}
}

nv::math::Matrix*
nv::math::MatrixSetAxis		(		Matrix*	outM, const Vec3* inRight, const Vec3* inUp, const Vec3* inFront	)
{
	if( inRight ) {
		outM->m11 = inRight->x;
		outM->m12 = inRight->y;
		outM->m13 = inRight->z;
	}
	if( inUp ) {
		outM->m21 = inUp->x;
		outM->m22 = inUp->y;
		outM->m23 = inUp->z;
	}
	if( inFront ) {
		outM->m31 = inFront->x;
		outM->m32 = inFront->y;
		outM->m33 = inFront->z;
	}
	return outM;
}


nv::math::Matrix*
nv::math::MatrixOrthoRH		(	Matrix*	outM, float inW, float inH, float inZNear, float inZFar			)
{
	NV_ASSERT( inZFar > inZNear );
	MatrixZero( outM );
	float p = 1.0f / ( inZFar - inZNear );
	float q = inZNear * p;
	outM->m11 = 2.0f/inW;
	outM->m22 = 2.0f/inH;
	outM->m33 = -(2.0f*p);
	outM->m43 = -2.0f*q-1.0f;
	outM->m44 = 1.0f;
	return outM;
}

nv::math::Matrix*
nv::math::MatrixOrthoLH		(	Matrix*	outM, float inW, float inH, float inZNear, float inZFar			)
{
	NV_ASSERT( inZFar > inZNear );
	MatrixZero( outM );
	float p = 1.0f / ( inZFar - inZNear );
	float q = inZNear * p;
	outM->m11 = 2.0f/inW;
	outM->m22 = 2.0f/inH;
	outM->m33 = (2.0f*p);
	outM->m43 = -2.0f*q-1.0f;
	outM->m44 = 1.0f;
	return outM;
}

nv::math::Matrix*
nv::math::MatrixPerspectiveRH	(	Matrix*	outM, float inFovx, float inAspect, float inZNear, float inZFar	)
{
	NV_ASSERT( inZFar > inZNear );
	MatrixZero( outM );
	float s = 1.0f / Tanf( inFovx * 0.5f );
	float p = inZFar / ( inZFar - inZNear );
	float q = inZNear * p;
	outM->m11 = s;
	outM->m22 = s*inAspect;
	outM->m33 = -(2.0f*p-1.0f);
	outM->m34 = -1.0f;
	outM->m43 = -2.0f*q;
	return outM;
}

nv::math::Matrix*
nv::math::MatrixPerspectiveLH	(	Matrix*	outM, float inFovx, float inAspect, float inZNear, float inZFar	)
{
	NV_ASSERT( inZFar > inZNear );
	MatrixZero( outM );
	float s = 1.0f / Tanf( inFovx * 0.5f );
	float p = inZFar / ( inZFar - inZNear );
	float q = inZNear * p;
	outM->m11 = s;
	outM->m22 = s*inAspect;
	outM->m33 = (2.0f*p-1.0f);
	outM->m34 = +1.0f;
	outM->m43 = -2.0f*q;
	return outM;
}

bool
nv::math::MatrixIsProjRH		(		Matrix*	inM			)
{
	if( inM->m34 < 0 )		return TRUE;	// Checks MatrixPerspectiveRH
	if( inM->m34 > 0 )		return FALSE;	// Checks MatrixPerspectiveLH
	if( inM->m33 < 0 )		return TRUE;	// Checks MatrixOrthoRH
	else					return FALSE;	// Else   MatrixOrthoLH
}

bool
nv::math::MatrixIsProjLH		(		Matrix*	inM			)
{
	if( inM->m34 > 0 )		return TRUE;	// Checks MatrixPerspectiveLH
	if( inM->m34 < 0 )		return FALSE;	// Checks MatrixPerspectiveRH
	if( inM->m33 > 0 )		return TRUE;	// Checks MatrixOrthoLH
	else					return FALSE;	// Else   MatrixOrthoRH
}


void
nv::math::MatrixArraySetTR	(		Matrix*		outMArray,
									const TR*	inTRArray,
									uint		inCount	)
{
	Matrix* out = outMArray, *outParent;
	TR *    cur = (TR*)inTRArray;
	TR *    end = (TR*)inTRArray + inCount;

	while( cur != end )
	{
		MatrixBuildTR( out, &cur->t, &cur->r, NULL );
		if( cur->parentIdx >= 0 ) {
			outParent = outMArray + cur->parentIdx;
			NV_ASSERT( outParent < out );
			MatrixMul( out, out, outParent );
		}
		cur++;
		out++;
	}
}


void
nv::math::MatrixArraySetTR		(		Matrix*		outMArray,
										const TRS*	inTRSArray,
										uint		inCount		)
{
	Matrix* out = outMArray, *outParent;
	TRS *   cur = (TRS*)inTRSArray;
	TRS *   end = (TRS*)inTRSArray + inCount;

	while( cur != end )
	{
		MatrixBuildTR( out, &cur->t, &cur->r, NULL );
		if( cur->parentIdx >= 0 ) {
			outParent = outMArray + cur->parentIdx;
			NV_ASSERT( outParent < out );
			MatrixMul( out, out, outParent );
		}
		cur++;
		out++;
	}
}



void
nv::math::MatrixArraySetTRS		(		Matrix*		outMArray,
										const TRS*	inTRSArray,
										uint		inCount		)
{
	Matrix* out = outMArray, *outParent;
	TRS *   cur = (TRS*)inTRSArray;
	TRS *   end = (TRS*)inTRSArray + inCount;

	while( cur != end )
	{
		MatrixBuildTR( out, &cur->t, &cur->r, &cur->s );
		if( cur->parentIdx >= 0 ) {
			outParent = outMArray + cur->parentIdx;
			NV_ASSERT( outParent < out );
			MatrixMul( out, out, outParent );
		}
		cur++;
		out++;
	}
}


void
nv::math::MatrixArrayMul	(		Matrix*		outMArray,
									Matrix*		inM0Array,
									Matrix*		inM1Array,
									uint		inCount	)
{
	Matrix* out  = outMArray;
	Matrix* end  = outMArray + inCount;
	Matrix* cur1 = inM1Array;
	Matrix* cur0 = inM0Array;

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_3(out,cur0,cur1) ) {
	    __asm__ __volatile__(
	"_loop_array_mul:\n"
	"	lqc2    vf4,0x0(%2)\n"
	"	lqc2    vf5,0x10(%2)\n"
	"	lqc2    vf6,0x20(%2)\n"
	"	lqc2    vf7,0x30(%2)\n"
	"	lqc2    vf8,0x0(%1)\n"
	"	lqc2    vf9,0x10(%1)\n"
	"	lqc2    vf10,0x20(%1)\n"
	"	lqc2    vf11,0x30(%1)\n"
	"	vmulax	ACC,vf4,vf8\n"
	"	vmadday	ACC,vf5,vf8\n"
	"	vmaddaz	ACC,vf6,vf8\n"
	"	vmaddw	vf8,vf7,vf8\n"
	"	vmulax	ACC,vf4,vf9\n"
	"	vmadday	ACC,vf5,vf9\n"
	"	vmaddaz	ACC,vf6,vf9\n"
	"	vmaddw	vf9,vf7,vf9\n"
	"	vmulax	ACC,vf4,vf10\n"
	"	vmadday	ACC,vf5,vf10\n"
	"	vmaddaz	ACC,vf6,vf10\n"
	"	vmaddw	vf10,vf7,vf10\n"
	"	vmulax	ACC,vf4,vf11\n"
	"	vmadday	ACC,vf5,vf11\n"
	"	vmaddaz	ACC,vf6,vf11\n"
	"	vmaddw	vf11,vf7,vf11\n"
	"	sqc2    vf8,0x0(%0)\n"
	"	sqc2    vf9,0x10(%0)\n"
	"	sqc2    vf10,0x20(%0)\n"
	"	sqc2    vf11,0x30(%0)\n"
	"	addi    %0,0x40\n"
	"	addi    %1,0x40\n"
	"	addi    %2,0x40\n"
	"	bne    %0,%3,_loop_array_mul\n"
		:: "r" (out), "r" (cur0), "r" (cur1), "r" (end) : "memory");
		return;
	}
#endif

	while( out != end ) {
		MatrixMul( out, cur0, cur1 );
		cur0++;
		cur1++;
		out++;
	}
}





//--------------------------
// Quaternion
//--------------------------



const nv::math::Quat	nv::math::Quat::ZERO(	0.0f,0.0f,0.0f,0.0f );
const nv::math::Quat	nv::math::Quat::ONE(	1.0f,1.0f,1.0f,1.0f );
const nv::math::Quat	nv::math::Quat::UNIT(	0.0f,0.0f,0.0f,1.0f );


nv::math::Quat*
nv::math::QuatCopy			(       Quat* outQ, const Quat* inQ		)
{
	if( outQ == inQ )
		return outQ;

	outQ->x		= inQ->x;
	outQ->y		= inQ->y;
	outQ->z		= inQ->z;
	outQ->w		= inQ->w;
	return outQ;
}


nv::math::Quat*
nv::math::QuatZero			(       Quat* outQ													)
{
	outQ->x	= 0.0f;
	outQ->y	= 0.0f;
	outQ->z	= 0.0f;
	outQ->w	= 0.0f;
	return outQ;
}


nv::math::Quat*
nv::math::QuatIdentity		(       Quat* outQ													)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionIdentity( DXQ(outQ) );

#else

	outQ->x	= 0.0f;
	outQ->y	= 0.0f;
	outQ->z	= 0.0f;
	outQ->w	= 1.0f;
	return outQ;

#endif
}

bool
nv::math::QuatIsIdentity		( const Quat* inQ													)
{
#if defined(MATH_USE_DX)

	return D3DXQuaternionIsIdentity( DXQ(inQ) ) != 0;

#else

	return	(	inQ->x == 0.0f
			&&	inQ->y == 0.0f
			&&	inQ->z == 0.0f
			&&	inQ->w == 1.0f	);

#endif
}

float
nv::math::QuatNorm			( const Quat* inQ													)
{
#if defined(MATH_USE_DX)

	return D3DXQuaternionLength( DXQ(inQ) );

#else

	return Vec4Norm( (const Vec4*)inQ );

#endif
}

float
nv::math::QuatNorm2			( const Quat* inQ													)
{
#if defined(MATH_USE_DX)

	return D3DXQuaternionLengthSq( DXQ(inQ) );

#else

	return Vec4Norm2( (const Vec4*)inQ );

#endif
}

nv::math::Quat*
nv::math::QuatNormalize		(       Quat* outQ, const Quat* inQ									)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionNormalize( DXQ(outQ), DXQ(inQ) );

#else

	return (Quat*) Vec4Normalize( (Vec4*)outQ, (const Vec4*)inQ );

#endif
}

float
nv::math::QuatDot				( const Quat* inQ0, const Quat* inQ1								)
{
#if defined(MATH_USE_DX)

	return D3DXQuaternionDot( DXQ(inQ0), DXQ(inQ1) );

#else

	return Vec4Dot( (const Vec4*)inQ0, (const Vec4*)inQ1 );

#endif
}

nv::math::Quat*
nv::math::QuatConjugate		(       Quat* outQ, const Quat* inQ									)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionConjugate( DXQ(outQ), DXQ(inQ) );

#else

#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outQ,inQ) ) {
	    __asm__ __volatile__(
		"lqc2    	vf4,0x0(%1)\n"
		"vsub.xyz	vf4,vf0,vf4\n"
		"sqc2    	vf4,0x0(%0)\n"
		: : "r" (outQ) , "r" (inQ) : "memory");
		return outQ;
	}
#endif

	outQ->x = - inQ->x;
	outQ->y = - inQ->y;
	outQ->z = - inQ->z;
	outQ->w =   inQ->w;
	return outQ;

#endif
}


nv::math::Quat*
nv::math::QuatFastInverse		(       Quat* outQ, const Quat* inQ									)
{
#if defined(MATH_USE_VU0)
	if( IS_VU0ABLE_2(outQ,inQ) ) {
	    __asm__ __volatile__(
		"lqc2    	vf4,0x0(%1)\n"
		"vsub.xyz	vf4,vf0,vf4\n"
		"sqc2    	vf4,0x0(%0)\n"
		: : "r" (outQ) , "r" (inQ) : "memory");
		return outQ;
	}
#endif

	return QuatConjugate( outQ, inQ );
}


nv::math::Quat*
nv::math::QuatInverse			(       Quat* outQ, const Quat* inQ									)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionInverse( DXQ(outQ), DXQ(inQ) );

#else

	// q^-1 = q' / (q'*q) = q' / ||q||^2
	float n2 = (	inQ->x * inQ->x
				+	inQ->y * inQ->y
				+	inQ->z * inQ->z
				+	inQ->w * inQ->w	);
	float oon2 = (n2==0.0f) ? 1.0f : 1.0f/n2;

	outQ->x = - inQ->x * oon2;
	outQ->y = - inQ->y * oon2;
	outQ->z = - inQ->z * oon2;
	outQ->w =   inQ->w;
	return outQ;

#endif
}


void
nv::math::QuatToAxisAngle		(       Vec3* outV, float * outAngle, const Quat* inQ				)
{
#if defined(MATH_USE_DX)

	D3DXQuaternionToAxisAngle( DXQ(inQ), DXV3(outV), outAngle );

#else

	float n = Sqrtf( inQ->x*inQ->x+inQ->y*inQ->y+inQ->z*inQ->z );
	float a = Atan2f( n, inQ->w );

	if( outAngle )
		*outAngle = a * 2.0f;

	if( outV )
	{
		float f = Sinf( a );
		float oof = (Absf(f)>FpEpsilon) ? 1.0f/f : 1.0f;
		outV->x = inQ->x * oof;
		outV->y = inQ->y * oof;
		outV->z = inQ->z * oof;
	}

#endif
}


nv::math::Quat*
nv::math::QuatFromAxisAngle	(       Quat* outQ, const Vec3* inV, float inAngle					)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionRotationAxis( DXQ(outQ), DXV3(inV), inAngle );

#else

	float c, s;
	Sincosf( inAngle*0.5f, &s, &c );
	outQ->x = inV->x * s;
	outQ->y = inV->y * s;
	outQ->z = inV->z * s;
	outQ->w = c;
	return outQ;

#endif
}


nv::math::Quat*
nv::math::QuatFromMatrix		(       Quat* outQ, const Matrix* inM								)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionRotationMatrix( DXQ(outQ), DXM(inM) );

#else

	float trace = inM->m[0][0] + inM->m[1][1] + inM->m[2][2];

	if( trace > 0.0f)
	{
		float s = Sqrt( trace + 1.0f );
		outQ->w = s * 0.5f;
		s = 0.5f / s;
		outQ->x = -(inM->m[2][1] - inM->m[1][2]) * s;
		outQ->y = -(inM->m[0][2] - inM->m[2][0]) * s;
		outQ->z = -(inM->m[1][0] - inM->m[0][1]) * s;
	}
	else 
	{
		int i=0, j=1, k=2;
		if (inM->m[1][1] > inM->m[0][0]) { i=1; j=2; k=0; }
		if (inM->m[2][2] > inM->m[i][i]) { i=2; j=0; k=1; }

		float s = Sqrt( inM->m[i][i] - (inM->m[j][j] + inM->m[k][k]) + 1.0f);
		float*q = & outQ->x;
		q[i] = - s * 0.5f;
		if( s != 0.0f )
			s = 0.5f / s;
		q[j] = - (inM->m[j][i] + inM->m[i][j]) * s;
		q[k] = - (inM->m[k][i] + inM->m[i][k]) * s;
		q[3] = + (inM->m[k][j] - inM->m[j][k]) * s;
	}
	return outQ;

#endif
}


nv::math::Quat*
nv::math::QuatMul				(		Quat* outQ, const Quat* inQ0, const Quat* inQ1				)
{
#if defined(MATH_USE_DX)

	// fake DX, i'm want Q1 * Q2 !!!!!
	return (Quat*) D3DXQuaternionMultiply( DXQ(outQ), DXQ(inQ1), DXQ(inQ0) );

#else

	Quat tmp;
	tmp.w = inQ0->w * inQ1->w - inQ0->x * inQ1->x - inQ0->y * inQ1->y - inQ0->z * inQ1->z;
	tmp.x = inQ0->w * inQ1->x + inQ0->x * inQ1->w + inQ0->y * inQ1->z - inQ0->z * inQ1->y;
	tmp.y = inQ0->w * inQ1->y + inQ0->y * inQ1->w + inQ0->z * inQ1->x - inQ0->x * inQ1->z;
	tmp.z = inQ0->w * inQ1->z + inQ0->z * inQ1->w + inQ0->x * inQ1->y - inQ0->y * inQ1->x;
	return QuatCopy( outQ, &tmp );

#endif
}


nv::math::Quat*
nv::math::QuatLn				(		Quat* outQ, const Quat* inQ			)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionLn( DXQ(outQ), DXQ(inQ) );

#else

	Quat tmp;
	tmp.w = 0.0f;

	if( math::Absf(inQ->w) < 1.0f )
	{
		float n = Sqrtf( inQ->x*inQ->x+inQ->y*inQ->y+inQ->z*inQ->z );
		float a = Atan2f( n, inQ->w );
		float s = Sinf( a );
		if( Absf(s) >= FpEpsilon )
		{
			float f = a/s;
			tmp.x = inQ->x * f;
			tmp.y = inQ->y * f;
			tmp.z = inQ->z * f;
		}
	}
	else
	{
		tmp.x = inQ->x;
		tmp.y = inQ->y;
		tmp.z = inQ->z;
	}

	return QuatCopy( outQ, &tmp );

#endif
}


nv::math::Quat*
nv::math::QuatExp				(		Quat* outQ, const Quat* inQ									)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionExp( DXQ(outQ), DXQ(inQ) );

#else

	Quat tmp;

	float a = Sqrt( inQ->x*inQ->x+inQ->y*inQ->y+inQ->z*inQ->z );
	float s, c;
	math::Sincosf( a, &s, &c );
	tmp.w = c;

	if( math::Absf(s) >= FpEpsilon )
	{
		float f = s/a;
		tmp.x = inQ->x * f;
		tmp.y = inQ->y * f;
		tmp.z = inQ->z * f;
	}
	else
	{
		tmp.x = inQ->x;
		tmp.y = inQ->y;
		tmp.z = inQ->z;
	}

	return QuatCopy( outQ, &tmp );

#endif
}


nv::math::Quat*
nv::math::QuatSlerp			(		Quat* outQ, const Quat* inQ0, const Quat* inQ1, float inT	)
{
	NV_ASSERT( inT >= 0.0f );
	NV_ASSERT( inT <= 1.0f );

	if( inT == 0.0f ) {
		*outQ = *inQ0;
		return outQ;
	}
	if( inT == 1.0f ) {
		*outQ = *inQ1;
		return outQ;
	}

#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionSlerp( DXQ(outQ), DXQ(inQ0), DXQ(inQ1), inT );

#else

	float omega, cosom, sinom, scale0, scale1, flip;

	// calc cosine
	cosom =		inQ0->x * inQ1->x
			+	inQ0->y * inQ1->y
			+	inQ0->z * inQ1->z
			+	inQ0->w * inQ1->w;

	// adjust signs
	if( cosom < 0.0f ) {
		cosom  = -cosom;
		flip = -1.0f;
	} else {
		flip = 1.0f;
	}

	// calculate coefficients
	if( (1.0f - cosom) > FpEpsilon ) {
		// standard case (slerp)
		omega = Acosf(cosom);
		sinom = 1.0f/Sinf(omega);
		scale0 = Sinf((1.0f - inT) * omega) * sinom;
		scale1 = flip * Sinf(inT * omega) * sinom;
	} else {        
		// "p" and "q" quaternions are very close 
		//  ... so we can do a linear interpolation
		scale0 = 1.0f - inT;
		scale1 = flip * inT;
	}

	// calculate final values
	outQ->x = scale0 * inQ0->x + scale1 * inQ1->x;
	outQ->y = scale0 * inQ0->y + scale1 * inQ1->y;
	outQ->z = scale0 * inQ0->z + scale1 * inQ1->z;
	outQ->w = scale0 * inQ0->w + scale1 * inQ1->w;

	return outQ;

#endif
}


nv::math::Quat*
nv::math::QuatEuler			(		Quat* outQ, float inEx, float inEy, float inEz		)
{
#if defined(MATH_USE_DX)

	return (Quat*) D3DXQuaternionRotationYawPitchRoll( DXQ(outQ), inEy, inEx, inEz );

#else

	float cx, sx;
	float cy, sy;
	float cz, sz;
	Sincos( inEx * 0.5f, &sx, &cx );
	Sincos( inEy * 0.5f, &sy, &cy );
	Sincos( inEz * 0.5f, &sz, &cz );

	float cycz, sysz, sycz, cysz;
	cycz = cy*cz;   
	sysz = sy*sz;
	sycz = sy*cz;
	cysz = cy*sz;
	outQ->w = cycz*cx + sysz*sx;
	outQ->x = cycz*sx + sysz*cx;
	outQ->y = sycz*cx - cysz*sx;
	outQ->z = cysz*cx - sycz*sx;
	return outQ;

#endif
}



nv::math::Quat*
nv::math::QuatPow		(		Quat* outQ, const Quat* inQ, float inScale		)
{
	float n = Sqrtf( inQ->x*inQ->x+inQ->y*inQ->y+inQ->z*inQ->z );
	float a = Atan2f( n, inQ->w );

	float s0 = Sinf( a );

	float s, c;
	math::Sincosf( a*inScale, &s, &c );

	float f = Absf(s0)>=FpEpsilon ? s/s0 : s;

	outQ->x = inQ->x * f;
	outQ->y = inQ->y * f;
	outQ->z = inQ->z * f;
	outQ->w = c;
	return outQ;
}







//--------------------------
// Box3
//--------------------------


nv::math::Box3*
nv::math::Box3Copy			(       Box3* outB, const Box3* inB			)
{
	if( outB == inB )
		return outB;

	outB->center   = inB->center;
	outB->rotation = inB->rotation;
	outB->length   = inB->length;
	return outB;
}


nv::math::Box3*
nv::math::Box3Concat			(       Box3* outB, const Box3* inB0, const Box3* inB1				)
{
	// Rotation resultante q = moyenne des rotations
	Quat q0, q1, q, iq;
	QuatNormalize( &q0, &inB0->rotation );
	QuatNormalize( &q1, &inB1->rotation );
	QuatSlerp( &q, &q0, &q1, 0.5f );
	QuatNormalize( &q, &q );
	QuatFastInverse( &iq, &q );

	// 16 coins ds l'espace de rotation
	Vec3  v, vmin, vmax;
	float W, H, D;
	uint i;

	// Box0
	W	= inB0->length.x * 0.5f;
	H	= inB0->length.y * 0.5f;
	D	= inB0->length.z * 0.5f;
	for( i = 0 ; i < 8 ; i++ ) {
		v.x = i&1 ? W : -W;
		v.y = i&2 ? H : -H;
		v.z = i&4 ? D : -D;
		v   = ( (v*q0) + inB0->center ) * iq;
		if( i== 0 )	{
			vmin = vmax = v;
		}
		else {
			Vec3Min( &vmin, &vmin, &v );
			Vec3Max( &vmax, &vmax, &v );
		}
	}

	// Box1
	W	= inB1->length.x * 0.5f;
	H	= inB1->length.y * 0.5f;
	D	= inB1->length.z * 0.5f;
	for( i = 0 ; i < 8 ; i++ ) {
		v.x = i&1 ? W : -W;
		v.y = i&2 ? H : -H;
		v.z = i&4 ? D : -D;
		v   = ( (v*q1) + inB1->center ) * iq;
		Vec3Min( &vmin, &vmin, &v );
		Vec3Max( &vmax, &vmax, &v );
	}

	outB->rotation = q;
	outB->length   = ( vmax - vmin );
	outB->center   = ( vmax + vmin ) * 0.5f;
	outB->center  *= q;
	return outB;
}


nv::math::Box3*
nv::math::Box3AxysAlign		(       Box3* outB, const Box3* inB		)
{
	// 8 coins ds le world
	Vec3  v, vmin, vmax;

	float W	= inB->length.x * 0.5f;
	float H	= inB->length.y * 0.5f;
	float D	= inB->length.z * 0.5f;
	for( uint i = 0 ; i < 8 ; i++ ) {
		v.x = i&1 ? W : -W;
		v.y = i&2 ? H : -H;
		v.z = i&4 ? D : -D;
		v   = (v*inB->rotation) + inB->center;
		if( i== 0 )	{
			vmin = vmax = v;
		}
		else {
			Vec3Min( &vmin, &vmin, &v );
			Vec3Max( &vmax, &vmax, &v );
		}
	}

	outB->rotation = Quat::UNIT;
	outB->length   = ( vmax - vmin );
	outB->center   = ( vmax + vmin ) * 0.5f;
	return outB;
}


nv::math::Box3*
nv::math::Box3Apply			(       Box3* outB, const Box3* inB,  const Matrix* inM				)
{
	Matrix TR;
	MatrixBuildTR( &TR, &inB->center, &inB->rotation, NULL );
	MatrixMul( &TR, &TR, inM );
	MatrixGetTR( &outB->center, &outB->rotation, NULL, &TR );
	outB->length = inB->length;
	return outB;
}












//--------------------------
// Sph3
//--------------------------




nv::math::Sph3*
nv::math::Sph3Copy			(       Sph3* outS, const Sph3* inS			)
{
	if( outS == inS )
		return outS;

	outS->center = inS->center;
	outS->radius = inS->radius;
	return outS;
}


nv::math::Sph3*
nv::math::Sph3Concat			(       Sph3* outS, const Sph3* inB0, const Sph3* inB1				)
{
	Vec3 c0 = inB0->center;
	Vec3 c1 = inB1->center;
	Vec3 c  = ( c0 + c1 ) * 0.5f;
	Vec3 dc = ( c0 - c1 );
	outS->center = c;
	outS->radius = Vec3Norm(&dc) + inB0->radius + inB1->radius;
	return outS;
}


nv::math::Sph3*
nv::math::Sph3Apply			(       Sph3* outS, const Sph3* inS,  const Matrix* inM				)
{
	Vec3Apply( &outS->center, &inS->center, inM );
	outS->radius = inS->radius;
	return outS;
}







//--------------------------
// Frustum3
//--------------------------

nv::math::Frustum3*
nv::math::Frustum3Copy		(       Frustum3* outF, const Frustum3* inF				)
{
	if( outF != inF )
		Memcpy( (void*)outF->plane, (void*)inF->plane, sizeof(outF->plane) );
	return outF;
}

nv::math::Frustum3*
nv::math::Frustum3Build		(       Frustum3* outF, const Box3* inB					)
{
	float hw = inB->length.x * 0.5f;
	float hh = inB->length.y * 0.5f;
	float hd = inB->length.z * 0.5f;
	Vec3 l[8];
	l[0] = Vec3( -hw, -hh, -hd ) * inB->rotation + inB->center;
	l[1] = Vec3( +hw, -hh, -hd ) * inB->rotation + inB->center;
	l[2] = Vec3( +hw, -hh, +hd ) * inB->rotation + inB->center;
	l[3] = Vec3( -hw, -hh, +hd ) * inB->rotation + inB->center;
	l[4] = Vec3( -hw, +hh, -hd ) * inB->rotation + inB->center;
	l[5] = Vec3( +hw, +hh, -hd ) * inB->rotation + inB->center;
	l[6] = Vec3( +hw, +hh, +hd ) * inB->rotation + inB->center;
	l[7] = Vec3( -hw, +hh, +hd ) * inB->rotation + inB->center;
	Vec3 n[6], p[6];
	n[0] = (l[0]-l[1]) ^ (l[2]-l[1]);	p[0] = l[1];
	n[1] = (l[1]-l[5]) ^ (l[6]-l[5]);	p[1] = l[5];
	n[2] = (l[2]-l[6]) ^ (l[7]-l[6]);	p[2] = l[6];
	n[3] = (l[3]-l[7]) ^ (l[4]-l[7]);	p[3] = l[7];
	n[4] = (l[0]-l[4]) ^ (l[5]-l[4]);	p[4] = l[4];
	n[5] = (l[5]-l[4]) ^ (l[7]-l[4]);	p[5] = l[4];
	for( int i = 0 ; i < 6 ; i++ ) {
		Vec3Normalize( &n[i], &n[i] );
		outF->plane[i].x =   n[i].x;
		outF->plane[i].y =   n[i].y;
		outF->plane[i].z =   n[i].z;
		outF->plane[i].w = - n[i] * p[i];
	}
	NV_ASSERT( outF->IsInside(inB->center) );
	return outF;
}

nv::math::Frustum3*
nv::math::Frustum3Build		(       Frustum3* outF, const Sph3* inS					)
{
	Box3 b( *inS );
	return Frustum3Build( outF, &b );
}

nv::math::Frustum3*
nv::math::Frustum3Apply		(      Frustum3* outF, const Frustum3* inF,  const Matrix* inM		)
{
	for( int i = 0 ; i < 6 ; i++ ) {
		Vec3 n, p;
		n.x		= inF->plane[i].x;
		n.y		= inF->plane[i].y;
		n.z		= inF->plane[i].z;
		float d	= inF->plane[i].w;
		p.x = p.y = p.z = 0;
		if( n.x != 0.f )	p.x = -d / n.x;
		if( n.y != 0.f )	p.y = -d / n.y;
		if( n.z != 0.f )	p.z = -d / n.z;
		Vec3ApplyVector( &n, &n, inM );
		Vec3Apply( &p, &p, inM );
		outF->plane[i].x =   n.x;
		outF->plane[i].y =   n.y;
		outF->plane[i].z =   n.z;
		outF->plane[i].w = - n * p;
	}
	return outF;
}

nv::math::Frustum3*
nv::math::Frustum3Build	(       Frustum3* outF, const Matrix* inM		)
{
	// planes extraction
	outF->plane[0].x = (inM->m14 + inM->m11);
	outF->plane[0].y = (inM->m24 + inM->m21);
	outF->plane[0].z = (inM->m34 + inM->m31);
	outF->plane[0].w = (inM->m44 + inM->m41);
	outF->plane[1].x = (inM->m14 - inM->m11);
	outF->plane[1].y = (inM->m24 - inM->m21);
	outF->plane[1].z = (inM->m34 - inM->m31);
	outF->plane[1].w = (inM->m44 - inM->m41);
	outF->plane[2].x = (inM->m14 + inM->m12);
	outF->plane[2].y = (inM->m24 + inM->m22);
	outF->plane[2].z = (inM->m34 + inM->m32);
	outF->plane[2].w = (inM->m44 + inM->m42);
	outF->plane[3].x = (inM->m14 - inM->m12);
	outF->plane[3].y = (inM->m24 - inM->m22);
	outF->plane[3].z = (inM->m34 - inM->m32);
	outF->plane[3].w = (inM->m44 - inM->m42);
	outF->plane[4].x = (inM->m14 + inM->m13);
	outF->plane[4].y = (inM->m24 + inM->m23);
	outF->plane[4].z = (inM->m34 + inM->m33);
	outF->plane[4].w = (inM->m44 + inM->m43);
	outF->plane[5].x = (inM->m14 - inM->m13);
	outF->plane[5].y = (inM->m24 - inM->m23);
	outF->plane[5].z = (inM->m34 - inM->m33);
	outF->plane[5].w = (inM->m44 - inM->m43);
	// normalisation
	for( int i = 0 ; i < 6 ; i++ ) {
		float n   = Vec3Norm( (Vec3*)&outF->plane[i] );
		float oon = (n==0.f) ? 1.f : 1.f/n;
		Vec4Scale( &outF->plane[i], &outF->plane[i], oon );
	}
	return outF;
}

uint
nv::math::Frustum3Clip	(	const Frustum3* inF, const Vec3* inV, float inRadius	)
{
	Vec4 v( *inV );
	return Frustum3Clip( inF, &v, inRadius );
}

uint
nv::math::Frustum3Clip	(	const Frustum3* inF, const Vec4* inV, float inRadius	)
{
	uint cf = 0;

#if defined(MATH_USE_VFPU)

    __asm__ __volatile__(
	"	.set		noreorder\n"			
	"	lv.s		s400,  0 (%1)\n"			// Load inRadius		

	"	or			  $8,  %2, %3\n"			// if ((inV | inF) % 16 == 0)
	"	andi		  $8,  $8, 0xf\n"
	"	bnez		  $8,  0f\n"

    "	lv.q		r200,  0 (%3)\n"			// Load inV				
	"	lv.q		r000,  0 (%2)\n"			// Load inF[0]
	"	lv.q		r001, 16 (%2)\n"			// Load inF[1]
	"	lv.q		r002, 32 (%2)\n"			// Load inF[2]
	"	lv.q		r003, 48 (%2)\n"			// Load inF[3]
	"	lv.q		r100, 64 (%2)\n"			// Load inF[4]			
	"	lv.q		r101, 80 (%2)\n"			// Load inF[5]
	"	b 			1f\n"
	"	nop\n"
		
	"0:\n"
	"	lv.s		s200,  0 (%3)\n"			// Load inV[0]
	"	lv.s		s210,  4 (%3)\n"			// Load inV[1]				
	"	lv.s		s220,  8 (%3)\n"			// Load inV[2]				
	"	lv.s		s230, 12 (%3)\n"			// Load inV[3]				
	"	lv.s		s000,  0 (%2)\n"			// Load inF[0][0]
	"	lv.s		s010,  4 (%2)\n"			// Load inF[0][1]
	"	lv.s		s020,  8 (%2)\n"			// Load inF[0][2]
	"	lv.s		s030, 12 (%2)\n"			// Load inF[0][3]
	"	lv.s		s001, 16 (%2)\n"			// Load inF[1][0]
	"	lv.s		s011, 20 (%2)\n"			// Load inF[1][1]
	"	lv.s		s021, 24 (%2)\n"			// Load inF[1][2]
	"	lv.s		s031, 28 (%2)\n"			// Load inF[1][3]
	"	lv.s		s002, 32 (%2)\n"			// Load inF[2][0]
	"	lv.s		s012, 36 (%2)\n"			// Load inF[2][1]
	"	lv.s		s022, 40 (%2)\n"			// Load inF[2][2]
	"	lv.s		s032, 44 (%2)\n"			// Load inF[2][3]
	"	lv.s		s003, 48 (%2)\n"			// Load inF[3][0]
	"	lv.s		s013, 52 (%2)\n"			// Load inF[3][1]
	"	lv.s		s023, 56 (%2)\n"			// Load inF[3][2]
	"	lv.s		s033, 60 (%2)\n"			// Load inF[3][3]
	"	lv.s		s100, 64 (%2)\n"			// Load inF[4][0]
	"	lv.s		s110, 68 (%2)\n"			// Load inF[4][1]
	"	lv.s		s120, 72 (%2)\n"			// Load inF[4][2]
	"	lv.s		s130, 76 (%2)\n"			// Load inF[4][3]
	"	lv.s		s101, 80 (%2)\n"			// Load inF[5][0]
	"	lv.s		s111, 84 (%2)\n"			// Load inF[5][1]
	"	lv.s		s121, 88 (%2)\n"			// Load inF[5][2]
	"	lv.s		s131, 92 (%2)\n"			// Load inF[5][3]								
	
	"1:\n"
	"	vabs.s     s401, s400\n"				// absRadius = Abs(inRadius)
														
	"	vdot.q		s300, r000, r200\n"			// s0 = inF[0].inV
	"	vdot.q		s310, r001, r200\n"			// s1 = inF[1].inV	
	"	vdot.q		s320, r002, r200\n"			// s2 = inF[2].inV
	"	vdot.q		s330, r003, r200\n"			// s3 = inF[3].inV

	"	vmov.q 		r402, r401[-X,-X,-X,-X]\n"	// radius = (-absRadius,-absRadius,-absRadius,-absRadius)
									
	"	vcmp.q	  	  LT, r300, r402\n"			// CC = (s0,s1,s2,s3) < radius
	
	"	vdot.q		s301, r100, r200\n"			// s4 = inF[4].inV
	"	vdot.q		s311, r101, r200\n"			// s5 = inF[5].inV
	
	"	mfvc		 $10, $131\n"				// tmp1 = CC
	"	and		      $8,$10,0x0000000F\n"		// tmp2 = tmp1 & 0x0000000F (CC 4 first bits)
				
	"	vcmp.p	  	  LT, r301, r402\n"			// CC = (s4,s5) < radius
	"	vnop\n"								// Sync
	"	vnop\n"								// Sync
		
	"	mfvc		 $10, $131\n"				// tmp1 = CC
	"	and		  $9, $10,0x00000003\n"		// tmp3 = tmp1 & 0x00000003 (CC 2 first bits)
	"	sll		 $10, $9,4 \n"				// tmp1 = tmp3 << 4			
	"	or			  %0, $8,$10\n"				// cf = tmp2 & tmp1

		".set		reorder\n"
		: "=&r" (cf)
		: "r" (&inRadius),"r" (inF) , "r" (inV)
		: "$8","$9","$10", "memory"
	);							

#else

	inRadius = Fabs( inRadius );
	if( Vec4Dot(&inF->plane[0],inV) < -inRadius )	cf |= (1<<0);
	if( Vec4Dot(&inF->plane[1],inV) < -inRadius )	cf |= (1<<1);
	if( Vec4Dot(&inF->plane[2],inV) < -inRadius )	cf |= (1<<2);
	if( Vec4Dot(&inF->plane[3],inV) < -inRadius )	cf |= (1<<3);
	if( Vec4Dot(&inF->plane[4],inV) < -inRadius )	cf |= (1<<4);
	if( Vec4Dot(&inF->plane[5],inV) < -inRadius )	cf |= (1<<5);

#endif	

	return cf;
}

bool
nv::math::Frustum3Inside	(	const Frustum3* inF, const Vec3* inV, float inRadius	)
{
	Vec4 v( *inV );
	return Frustum3Inside( inF, &v, inRadius );
}

bool
nv::math::Frustum3Inside	(	const Frustum3* inF, const Vec4* inV, float inRadius	)
{
#if defined(MATH_USE_VFPU)
	uint8 isClip;
	 __asm__ __volatile__(
	"	.set		noreorder				\n"			
	"	lv.s		s400,  0 (%1)			\n"			// Load inRadius		

	"	or			  $8,  %2, %3			\n"			// if ((inV | inF) % 16 == 0)
	"	andi		  $8,  $8, 0xf			\n"
	"	bnez		  $8,  0f				\n"

    "	lv.q		r200,  0 (%3)			\n"			// Load inV				
	"	lv.q		r000,  0 (%2)			\n"			// Load inF[0]
	"	lv.q		r001, 16 (%2)			\n"			// Load inF[1]
	"	lv.q		r002, 32 (%2)			\n"			// Load inF[2]
	"	vabs.s      s401, s400				\n"			// absRadius = Abs(inRadius)			
	"	lv.q		r003, 48 (%2)			\n"			// Load inF[3]
	"	lv.q		r100, 64 (%2)			\n"			// Load inF[4]			
	"	vmov.s 		s402, s401[-X]			\n"			// radius = (-absRadius)						
	"	lv.q		r101, 80 (%2)			\n"			// Load inF[5]
	"	b 			1f\n"
	"	nop\n"

	"0:\n"
	"	lv.s		s200,  0 (%3)			\n"			// Load inV[0]
	"	lv.s		s210,  4 (%3)			\n"			// Load inV[1]				
	"	lv.s		s220,  8 (%3)			\n"			// Load inV[2]				
	"	lv.s		s230, 12 (%3)			\n"			// Load inV[3]				
	"	vabs.s      s401, s400				\n"			// absRadius = Abs(inRadius)				
	"	lv.s		s000,  0 (%2)			\n"			// Load inF[0][0]
	"	lv.s		s010,  4 (%2)			\n"			// Load inF[0][1]
	"	lv.s		s020,  8 (%2)			\n"			// Load inF[0][2]
	"	lv.s		s030, 12 (%2)			\n"			// Load inF[0][3]
	"	lv.s		s001, 16 (%2)			\n"			// Load inF[1][0]
	"	lv.s		s011, 20 (%2)			\n"			// Load inF[1][1]
	"	lv.s		s021, 24 (%2)			\n"			// Load inF[1][2]
	"	lv.s		s031, 28 (%2)			\n"			// Load inF[1][3]
	"	vmov.s 		s402, s401[-X]			\n"			// radius = (-absRadius)					
	"	lv.s		s002, 32 (%2)			\n"			// Load inF[2][0]
	"	lv.s		s012, 36 (%2)			\n"			// Load inF[2][1]
	"	lv.s		s022, 40 (%2)			\n"			// Load inF[2][2]
	"	lv.s		s032, 44 (%2)			\n"			// Load inF[2][3]
	"	lv.s		s003, 48 (%2)			\n"			// Load inF[3][0]
	"	lv.s		s013, 52 (%2)			\n"			// Load inF[3][1]
	"	lv.s		s023, 56 (%2)			\n"			// Load inF[3][2]
	"	lv.s		s033, 60 (%2)			\n"			// Load inF[3][3]
	"	lv.s		s100, 64 (%2)			\n"			// Load inF[4][0]
	"	lv.s		s110, 68 (%2)			\n"			// Load inF[4][1]
	"	lv.s		s120, 72 (%2)			\n"			// Load inF[4][2]
	"	lv.s		s130, 76 (%2)			\n"			// Load inF[4][3]
	"	lv.s		s101, 80 (%2)			\n"			// Load inF[5][0]
	"	lv.s		s111, 84 (%2)			\n"			// Load inF[5][1]
	"	lv.s		s121, 88 (%2)			\n"			// Load inF[5][2]
	"	lv.s		s131, 92 (%2)			\n"			// Load inF[5][3]								
	
	"1:\n"			
	"	vdot.q		s301, r100, r200		\n"			// s4 = inF[4].inV
	"	vcmp.s	  	  LT, s301, s402		\n"			// CC = (s4) < radius
	"	bvt			   0,	0f				\n"					    		
	"	li			  %0, 0x0				\n"			
			
	"	vdot.q		s311, r101, r200		\n"			// s5 = inF[5].inV
	"	vcmp.s	  	  LT, s311, s402		\n"			// CC = (s5) < radius
	"	bvt			   0,	0f				\n"					    		
	"	li			  %0, 0x0				\n"			
	
	"	vdot.q		s300, r000, r200		\n"			// s0 = inF[0].inV
	"	vcmp.s	  	  LT, s300, s402		\n"			// CC = (s0) < radius
	"	bvt			   0,	0f				\n"					    	
	"	li			  %0, 0x0				\n"			
		
	"	vdot.q		s310, r001, r200		\n"			// s1 = inF[1].inV	
	"	vcmp.s	  	  LT, s310, s402		\n"			// CC = (s1) < radius
	"	bvt			   0,	0f				\n"					    	
	"	li			  %0, 0x0				\n"			
			
	"	vdot.q		s320, r002, r200		\n"			// s2 = inF[2].inV
	"	vcmp.s	  	  LT, s320, s402		\n"			// CC = (s2) < radius
	"	bvt			   0,	0f				\n"					    	
	"	li			  %0, 0x0				\n"			
			
	"	vdot.q		s330, r003, r200		\n"			// s3 = inF[3].inV
	"	vcmp.s	  	  LT, s330, s402		\n"			// CC = (s3) < radius
	"	bvt			   0,	0f				\n"					    	
	"	li			  %0, 0x0				\n"			

	"	li			  %0, 0x1				\n"			
	"0:										\n"																	
		".set		reorder\n"
		: "=&r" (isClip)
		: "r" (&inRadius),"r" (inF) , "r" (inV)
		: "$8", "memory"
	);		
	return isClip;
#else
	inRadius = Fabs( inRadius );
	if( Vec4Dot(&inF->plane[4],inV) < -inRadius )	return FALSE;
	if( Vec4Dot(&inF->plane[5],inV) < -inRadius )	return FALSE;
	if( Vec4Dot(&inF->plane[0],inV) < -inRadius )	return FALSE;
	if( Vec4Dot(&inF->plane[1],inV) < -inRadius )	return FALSE;
	if( Vec4Dot(&inF->plane[2],inV) < -inRadius )	return FALSE;
	if( Vec4Dot(&inF->plane[3],inV) < -inRadius )	return FALSE;
	return TRUE;
#endif //defined(MATH_USE_VFPU)
}




//--------------------------
// Curve
//--------------------------


void
nv::math::ComputeHermiteBasis	( float * outB, float inU	)
{
	float u,u2,u3;
	u  = inU;
	u2 = u*u;
	u3 = u2*u;
	outB[0] =  2.0f*u3 - 3.0f*u2     + 1.0f	;
	outB[1] = -2.0f*u3 + 3.0f*u2			;
	outB[2] =       u3 - 2.0f*u2 + u		;
	outB[3] =       u3 -      u2			;
}


void
nv::math::ComputeBezierBasis	( float * outB, float inU	)
{

	float u,u2,u3;
	u  = inU;
	u2 = u*u;
	u3 = u2*u;
	outB[0] = -     u3 + 3.0f*u2 - 3.0f*u + 1.0f;
	outB[1] =  3.0f*u3 - 6.0f*u2 + 3.0f*u		;
	outB[2] = -3.0f*u3 + 3.0f*u2				;
	outB[3] =       u3							;
}


void
nv::math::ComputeSplineInterp			(	float &		outP,
											float *		inB/*[4]*/,
											float 		K0, float 	K1, float	K2,	float	K3		)
{
	outP  = K0 * inB[0];
	outP += K1 * inB[1];
	outP += K2 * inB[2];
	outP += K3 * inB[3];
}

void
nv::math::ComputeSplineInterp			(	Vec2 &		outP,
											float *		inB/*[4]*/,
											Vec2 &		K0, Vec2 &	K1, Vec2 &	K2,	Vec2 &	K3		)
{
	outP  = K0 * inB[0];
	outP += K1 * inB[1];
	outP += K2 * inB[2];
	outP += K3 * inB[3];
}


void
nv::math::ComputeSplineInterp			(	Vec3 &		outP,
											float *		inB/*[4]*/,
											Vec3 &		K0, Vec3 &	K1, Vec3 &	K2,	Vec3 &	K3		)
{
	outP  = K0 * inB[0];
	outP += K1 * inB[1];
	outP += K2 * inB[2];
	outP += K3 * inB[3];
}



// Smooth step function with hermite interpolation
float
nv::math::ComputeHermiteSmoothStep ( float x0, float x1, float v )
{
   if (v<=x0) return(0.0f);
   else if (v>=x1) return(1.0f);
   else {
      float u = (v-x0)/(x1-x0);
      return(u*u*(3-2*u));
   }
}


/*
void
nv::math::ComputeSplineInterp( Vec3 & outP, float u, Vec3 * knot, float m1, float m2 )
{
	float v[4];
	ComputeHermiteBasis(v,u);
	float c[4];
	c[0] = -v[2]*m2;
	c[1] = v[0] + v[2]*(m2-m1) - v[3]*m1;
	c[2] = v[1] + v[2]*m1 + v[3]*(m1-m2);
	c[3] = v[3]*m2;
	outP =  knot[0]*c[0] + knot[1]*c[1] + knot[2]*c[2] + knot[3]*c[3];
}



void CubicMorphCont::CalcFirstCoef(float *v,float *c)
	{
	float G, H, J;

	H = .5f*(1.0f-keys[0].tens)*v[2];
	J = 3.0f*H;
	G = v[3] - H;
	c[0] = v[0] - J - G * keys[1].k;
	c[1] = v[1] + J + G*(keys[1].k-keys[1].l);
	c[2] = G*keys[1].l;
	}

void CubicMorphCont::CalcLastCoef(float *v,float *c)
	{
	int nkeys = keys.Count();
	float G, H, J;

	H = .5f*(1.0f-keys[nkeys-1].tens)*v[3];
	J = 3.0f*H;
	G = v[2]-H;
	c[0] = -G*keys[nkeys-2].m;
	c[1] = v[0] - J + G*(keys[nkeys-2].m-keys[nkeys-2].n);
	c[2] = v[1] + J + G*keys[nkeys-2].n;
	}

void CubicMorphCont::CalcMiddleCoef(float *v,int *knum,float *c)
	{
	c[0] = -v[2]*keys[knum[1]].m;
	c[1] = v[0] + v[2]*(keys[knum[1]].m-keys[knum[1]].n) - v[3]*keys[knum[2]].k;
	c[2] = v[1] + v[2]*keys[knum[1]].n + v[3]*(keys[knum[2]].k-keys[knum[2]].l);
	c[3] = v[3]*keys[knum[2]].l;
	}


*/


float
nv::math::GaussianUnit			(	float		inDeviation,
									float		inDistance				)
{
	NV_ASSERT( inDeviation > 0.0f );
	return Exp( -(inDistance*inDistance) / (2.0f*inDeviation*inDeviation) );
}


float
nv::math::Gaussian				(	float		inDeviation,
									float		inDistance				)
{
	NV_ASSERT( inDeviation > 0.0f );
	float scalar = inDeviation * Sqrt(TwoPi);
	return GaussianUnit(inDeviation,inDistance) / scalar;
}


float
nv::math::GaussianDeviation		(	float		inRadius				)
{
	// Correct gaussian sampling with width = 4 * deviation
	return ( inRadius + 1.0f ) * 0.25f;
}


int
nv::math::GaussianWidth			(	float		inDeviation				)
{
	// Correct gaussian sampling with width = 4 * deviation
	if( inDeviation <= 0.0f )
		return 0;
	return int( Ceil(inDeviation*4.0f) );
}


int
nv::math::GaussianNormSequence	(	float*		outG,
									float		inDeviation,
									bool		inNormalize				)
{
	int width = GaussianWidth( inDeviation );
	if( width <= 0 )
		return 0;

	// Gaussian factors
	float sum = 0;
	for( int i = 0 ; i < width ; i++ ) {
		float g = GaussianUnit( inDeviation, i );
		outG[i] = g;
		sum    += g;
	}

	if( inNormalize ) {
		// Gaussian factors -> normalised weights
		float oo_sum = 1.0f / sum;
		for( int i = 0 ; i < width ; i++ )
			outG[i] *= oo_sum;
	}

	return width;
}



