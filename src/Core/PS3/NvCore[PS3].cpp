/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Nova.h>
#include <Kernel/Common/NvkCore_Mem.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysutil/sysutil_common.h>
#include <sysutil/sysutil_sysparam.h>
#include <cell/sysmodule.h>

using namespace nv;


#define	NVCORE_DECL( ID )					\
	bool	nvcore_##ID##_Init		();		\
	void	nvcore_##ID##_Update	();		\
	void	nvcore_##ID##_Shut		();

NVCORE_DECL( Math	)
NVCORE_DECL( Libc	)
NVCORE_DECL( Report )
NVCORE_DECL( Mem	)
NVCORE_DECL( Clock	)
NVCORE_DECL( File	)
NVCORE_DECL( NVR	)
NVCORE_DECL( Net	)
NVCORE_DECL( Ctrl	)
NVCORE_DECL( Param	)
NVCORE_DECL( Ext	)


// externs for linker variables
typedef int __attribute__ ((mode (TI))) elink_type __attribute__((aligned(16)));
extern elink_type _start;
extern elink_type _end;



namespace
{
	bool	coreBooted		= FALSE;
	bool	coreInitialized = FALSE;
	bool	mainCalled		= FALSE;
	uint32	main_sp			= 0;
	bool	exitAsked		= FALSE;

	static void ExitGameCallback(uint64_t status, uint64_t param, void * userdata)
	{
		if (status == CELL_SYSUTIL_REQUEST_EXITGAME)
			exitAsked = TRUE;
	}

	uint16 ModulesToLoad[] = {
		CELL_SYSMODULE_RTC		,
		CELL_SYSMODULE_FS		,
		CELL_SYSMODULE_IO		,
		CELL_SYSMODULE_SYSUTIL	,
	};

}




bool
nv::core::Startup__	(	int			argc,
						char**		argv)
{
	NV_COMPILE_TIME_CHECK_TYPES();

	// Load Module
	int ret = 0;
	for ( uint i = 0 ; i < sizeof (ModulesToLoad) / sizeof(uint16) ; ++i) {
		ret = cellSysmoduleLoadModule( ModulesToLoad[i] );
		if (ret != CELL_OK) return FALSE;
	}

	nvcore_Param_Init();

	mainCalled = TRUE;
	return TRUE;
}



bool
nv::core::Init		()
{
	if ( coreInitialized )
		return TRUE;

	// register "EXITGAME" callback.
	int ret = cellSysutilRegisterCallback(	0,  ExitGameCallback, NULL );
	if (ret != 0) return FALSE;


	mem::NativeInit();
//	devcons::Init();
	Printf( ">> Neova CoreEngine is running\n" );
	Printf( ">> (c) 2002-2008 AtOnce Technologies\n" );
	Printf( "\n" );

	if( !mainCalled ) {
		NV_ERROR( "\n\n\n** Overload the main() function is not allowed !\n** Use the function [void GameMain()] as your application entrypoint." );
		return FALSE;
	}

	// CoreEngine revision
	Printf( "<Neova> CoreEngine %d.%d-%s\n", GetReleaseNumber(), GetReleaseRevision(), GetDebugLevel() );
	#ifdef TIME_LIMITED_EVALUATION
	Printf( "<Neova> Time limited version\n" );
	#endif

/*
	// Power frequencies
	Printf( "<Neova> CPU freq: %d Mhz\n", scePowerGetCpuClockFrequencyInt() );
	Printf( "<Neova> Bus freq: %d Mhz\n", scePowerGetBusClockFrequencyInt() );
	Printf( "<Neova> Pll freq: %d Mhz\n", scePowerGetPllClockFrequencyInt() );
	Printf( "<Neova> SDK Ver.: %d.%d.%d\n", (SCE_DEVKIT_VERSION>>24)&0xFF, (SCE_DEVKIT_VERSION>>16)&0xFF, (SCE_DEVKIT_VERSION)&0xFFFF );

	// Thread priority
	uint32 thPri;
	GetParameter( PR_ALG_TH_PRIORITY, &thPri );
	if( thPri>=SCE_KERNEL_USER_HIGHEST_PRIORITY && thPri<=SCE_KERNEL_USER_LOWEST_PRIORITY ) {
		sceKernelChangeThreadPriority( 0, thPri );
		Printf( "<Neova> Thread priority changed to %d [low:%d->hi:%d]\n", thPri, SCE_KERNEL_USER_LOWEST_PRIORITY, SCE_KERNEL_USER_HIGHEST_PRIORITY );
	} else {
		Printf( "<Neova> Thread priority is %d [low:%d->hi:%d]\n", sceKernelGetThreadCurrentPriority(), SCE_KERNEL_USER_LOWEST_PRIORITY, SCE_KERNEL_USER_HIGHEST_PRIORITY );
	}

	// Link areas
	uint elf_addr	 = uint(&_start) - 296U;
	uint elf_bsize   = uint(&_end) - elf_addr;
	uint stack_bsize = sce_newlib_stack_kb_size << 10;
	uint heap_bsize  = sce_newlib_heap_kb_size << 10;
	uint app_bsize   = elf_bsize + stack_bsize + heap_bsize;
	uint free_bsize  = sceKernelTotalFreeMemSize();
	Printf( "<Neova> Kmem  tot:%dKo, app:%dKo, free:%dKo\n", (app_bsize+free_bsize)>>10, app_bsize>>10, free_bsize>>10 );
	Printf( "<Neova> Elf   @%x, %dKo\n", elf_addr, elf_bsize>>10 );
	Printf( "<Neova> Stack @%x, %dKo, remaining %dKo\n", main_sp, stack_bsize>>10, sceKernelCheckThreadStack()>>10 );
	Printf( "<Neova> Heap  @%x, %dKo\n", uint(&_end), heap_bsize>>10 );
	SetParameter( PR_MEM_APP_ADDR,			elf_addr    );
	SetParameter( PR_MEM_APP_BSIZE,			elf_bsize   );
	SetParameter( PR_MEM_APP_HEAP_ADDR,		uint(&_end) );
	SetParameter( PR_MEM_APP_HEAP_BSIZE,	heap_bsize  );
	SetParameter( PR_MEM_APP_STACK_ADDR,	main_sp     );
	SetParameter( PR_MEM_APP_STACK_BSIZE,	stack_bsize );
*/
	Printf( "<Neova> Components initialization ...\n" );
	nvcore_Report_Init();
	nvcore_Mem_Init();
	nvcore_Libc_Init();
	nvcore_Clock_Init();
	nvcore_Math_Init();
	nvcore_Net_Init();
	nvcore_File_Init();
	nvcore_Ctrl_Init();	
	nvcore_NVR_Init();
	nvcore_Ext_Init();

	Printf( "<Neova> Components initialized.\n" );

//	devcons::Shut();

	coreBooted	    = TRUE;
	coreInitialized = TRUE;
	mem::LogOpen();


	return TRUE;
}



void
nv::core::Shut		(				)
{
	if( !coreInitialized )
		return;

	Printf( "\n\n\n<Neova> ** EXITING **\n" );
	libc::FlushPrintf();

	nvcore_Ext_Shut();
	nvcore_NVR_Shut();
	nvcore_Ctrl_Shut();
	nvcore_File_Shut();
	nvcore_Net_Shut();
	nvcore_Math_Shut();
	nvcore_Libc_Shut();
	nvcore_Clock_Shut();
	nvcore_Mem_Shut();
	nvcore_Report_Shut();
	nvcore_Param_Shut();

	Printf( "\n\n\n<Neova> ** EXITED **\n" );
	libc::FlushPrintf();
	mem::NativeShut();

	coreInitialized = FALSE;
}


void
nv::core::Exit()
{
	Shut();
	exit(0);
}


bool
nv::core::ExitAsked	 ( )
{
	return FALSE;
}

uint
nv::core::Update	(	)
{
	nvcore_Param_Update();
	nvcore_Report_Update();
	nvcore_Mem_Update();
	nvcore_Libc_Update();
	nvcore_Clock_Update();
	nvcore_Math_Update();
	nvcore_Net_Update();
	nvcore_File_Update();
	nvcore_Ctrl_Update();
	nvcore_NVR_Update();
	nvcore_Ext_Update();

	cellSysutilCheckCallback();

	uint returnFlags = 0 ;

	if (exitAsked){
		exitAsked = FALSE;
		returnFlags = nv::core::UPD_EXIT_ASKED;	
	}
	return returnFlags;
}



nv::core::SystemID
nv::core::GetSystemID		(		)
{
	return SCEPlaystation3;
}



nv::core::Aspect
nv::core::GetAspect		(			)
{
	return ASPECT_FULL;
}

nv::core::VideoMode		
nv::core::GetVideoMode		(	)
{
	return VM_Other;
}

nv::core::SoundMode		
nv::core::GetSoundMode		(	)
{
	return nv::core::SoundMode(nv::core::SM_Stereo);	
}

nv::core::DateNotation
nv::core::GetDateNotation		(			)
{
	int v;
	cellSysutilGetSystemParamInt ( CELL_SYSUTIL_SYSTEMPARAM_ID_DATE_FORMAT, &v );

	if( v == CELL_SYSUTIL_DATE_FMT_YYYYMMDD  )		
		return DATE_YYYYMMDD;
	else if( v ==  CELL_SYSUTIL_DATE_FMT_MMDDYYYY  )	
		return DATE_MMDDYYYY;
	else
		return DATE_DDMMYYYY;
}


nv::core::Language
nv::core::GetLanguage	(				)
{
	int value;
	cellSysutilGetSystemParamInt(CELL_SYSUTIL_SYSTEMPARAM_ID_LANG,&value);

	switch (value){
		case CELL_SYSUTIL_LANG_JAPANESE		: return LANG_JAPANESE;
		case CELL_SYSUTIL_LANG_ENGLISH		: return LANG_ENGLISH;
		case CELL_SYSUTIL_LANG_FRENCH		: return LANG_FRENCH;
		case CELL_SYSUTIL_LANG_SPANISH		: return LANG_SPANISH;
		case CELL_SYSUTIL_LANG_GERMAN		: return LANG_GERMAN;
		case CELL_SYSUTIL_LANG_ITALIAN		: return LANG_ITALIAN;
		case CELL_SYSUTIL_LANG_DUTCH		: return LANG_DUTCH;
		case CELL_SYSUTIL_LANG_PORTUGUESE	: return LANG_PORTUGUESE;
		case CELL_SYSUTIL_LANG_RUSSIAN		: return LANG_RUSSIAN;
		case CELL_SYSUTIL_LANG_CHINESE_T	: return LANG_CHINESE_T;
		case CELL_SYSUTIL_LANG_CHINESE_S	: return LANG_CHINESE_S;
 // CELL_SYSUTIL_LANG_KOREAN,CELL_SYSUTIL_LANG_FINNISH,CELL_SYSUTIL_LANG_SWEDISH,CELL_SYSUTIL_LANG_DANISH,CELL_SYSUTIL_LANG_NORWEGIAN
		default								: return LANG_ENGLISH; 
	}
}


nv::core::SummerTime
nv::core::GetSummerTime	(				)
{
	int value;
	cellSysutilGetSystemParamInt ( CELL_SYSUTIL_SYSTEMPARAM_ID_SUMMERTIME, &value );
	if ( value == 0 )
		return SUMT_OFF;
	else
		return SUMT_ON;
}


nv::core::TimeNotation
nv::core::GetTimeNotation	(					)
{
	int value;
	cellSysutilGetSystemParamInt ( CELL_SYSUTIL_SYSTEMPARAM_ID_TIME_FORMAT, &value );
	if ( value == CELL_SYSUTIL_TIME_FMT_CLOCK24  )
		return TIME_24H;
	else
		return TIME_12H;
}



