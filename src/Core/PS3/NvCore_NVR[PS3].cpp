/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
using namespace nv;

namespace
{


}



bool
nvcore_NVR_Init()
{
	
	return TRUE;
}


void
nvcore_NVR_Shut()
{
	
}


void
nvcore_NVR_Update()
{
	
}




bool
nv::nvram::SetProductCode	(	pcstr		inProductCode		)
{
	
	return TRUE;
}


pcstr
nv::nvram::GetProductCode	(									)
{
	return NULL;
}


uint
nv::nvram::GetNbMax			(									)
{
	return 0;
}


bool
nv::nvram::IsReady			(	Result	*		outLastResult	)
{
	return FALSE;
}


bool
nv::nvram::GetStatus		(	uint			inNVRNo,
								uint32	*		outFullBSize,
								uint32	*		outFreeBSize,
								bool	*		outSwitched		)
{
	return FALSE;
}


bool
nv::nvram::Format			(	uint			inNVRNo			)
{
	return FALSE;
}


bool
nv::nvram::GetFirstRecord	(	uint			inNVRNo,
								pstr			outName,
								uint32		*	outBSize,
								DateTime	*	outCreateTime,
								DateTime	*	outModifyTime	)
{
	return FALSE;
}


bool
nv::nvram::GetNextRecord	(	pstr			outName,
								uint32		*	outBSize,
								DateTime	*	outCreateTime,
								DateTime	*	outModifyTime	)
{
	return FALSE;
}


bool
nv::nvram::CreateRecord		(	uint			inNVRNo,
								pcstr			inName,
								uint32			inBSize,
								pvoid			inBuffer	)
{
	return FALSE;
}


bool
nv::nvram::WriteRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset	)
{
	return FALSE;
}


bool
nv::nvram::ReadRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset		)
{
	return FALSE;
}


bool
nv::nvram::DeleteRecord		(	uint			inNVRNo,
								pcstr			inName			)
{
	return FALSE;
}


