/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <cell/rtc.h>


namespace
{
	uint64	tick0;
	uint64	tick_per_s;
	uint64	timeTck;
	float	timeI;
	float	timeF;
}



bool
nvcore_Clock_Init()
{
	CellRtcTick tick;
	cellRtcGetCurrentTick(&tick);			// Get UTC time
	timeTck = tick0 = tick.tick;
	timeI   = 0;
	timeF   = 0;

	cellRtcTickAddSeconds( &tick, &tick, 1 );
	tick_per_s = tick.tick - tick0;
	return TRUE;
}


void
nvcore_Clock_Update()
{
	CellRtcTick tick;
	cellRtcGetCurrentTick(&tick);			// Get UTC time
	uint64 d_tck = tick.tick - timeTck;
	while( d_tck >= tick_per_s ) {
		timeI   += 1.f;
		timeTck += tick_per_s;
		d_tck   -= tick_per_s;
	}
	timeF = float(int(d_tck)) / float(int(tick_per_s));
}


void
nvcore_Clock_Shut()
{
	//
}


void
nv::clock::GetTime(	Time *		outTime		)
{
	nvcore_Clock_Update();
	if( outTime ) {
		outTime->ipart	= timeI;
		outTime->fpart	= timeF;
	}
}



