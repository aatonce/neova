/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>


#define PadThreshold (0.4f)

bool
nvcore_Report_Init()
{
	return TRUE;
}


void
nvcore_Report_Update()
{
	//
}


void
nvcore_Report_Shut()
{
	//
}


void
nv::report::Output	(	Type		inReportT,
						pcstr		inMessage,
						pcstr		inFilename,
						pcstr		inFctName,
						int			inLineNo	)
{
	char tmp[ 256 ];
	if( inFilename || inFctName )
		Sprintf( tmp, "%s [%s, %s(), L%d]", inMessage, inFilename, inFctName, inLineNo );
	else
		Sprintf( tmp, "%s", inMessage );

	char tmp2[256];
	Sprintf( tmp2, "%d.%d-%s",
		core::GetReleaseNumber(),
		core::GetReleaseRevision(),
		core::GetDebugLevel()	);

	if( inReportT == T_MESSAGE )
	{
		Printf( "<Neova %s> Message : %s\n", tmp2, tmp );
	}
	else if( inReportT == T_WARNING )
	{
		Printf( "<Neova %s> Warning : %s\n", tmp2, tmp );
	}
	else if( inReportT == T_ERROR )
	{
		Printf( "<Neova %s> Error : %s\n", tmp2, tmp );

		// Output call-stack
		//nv::stack::Frame sf;
		//if( stack::GetFrame(sf) && stack::GetBackFrame(sf,sf) )
		//	stack::OutputCallstack( sf );
	}
}



nv::report::Action
nv::report::AskUserAction	(	)
{
	Printf( "\n\n## Please select an action to do :\n" );

	// Reset paddles
where_s_my_paddle:
	Printf( "   (getting the paddle ...)\n" );
	ctrl::Close();
	ctrl::Open();
	uint      N = ctrl::GetNbMax();
	uint ctrlNo = uint(-1);
	while( ctrlNo == uint(-1) ) {
		for( uint i = 0 ; i < N ; i++ ) {
			ctrl::Status* c = ctrl::GetStatus( i );
			if( c ) {
				ctrlNo = i;
				break;
			}
		}
		core::Update();
		libc::FlushPrintf();
	}

	Printf( ">> Press L+R to start the selection.\n" );
	for( ;; ) {
		ctrl::Status* c = ctrl::GetStatus( ctrlNo );
		if(	!c )
			goto where_s_my_paddle;
		if( 	c->button[ctrl::BT_BUTTON4] > PadThreshold
			&&	c->button[ctrl::BT_BUTTON5] > PadThreshold	)
			break;
		core::Update();
		libc::FlushPrintf();
	}

	Printf( ">> Press Triangle : EXIT\n" );
	Printf( "      or Square   : DON'T CARE\n" );
	Printf( "      or Circle   : CONTINUE\n" );
	Printf( "      or Cross    : BREAK\n" );
	for( ;; ) {
		ctrl::Status* c = ctrl::GetStatus( ctrlNo );
		if(	!c )
			goto where_s_my_paddle;
		if( c->button[ctrl::BT_BUTTON0] > PadThreshold ) {
			Printf( ">> EXIT selected.\n\n" );
			return A_EXIT;
		}
		if( c->button[ctrl::BT_BUTTON3] > PadThreshold ) {
			Printf( ">> DON'T CARE selected.\n\n" );
			return A_DONTCARE;
		}
		if( c->button[ctrl::BT_BUTTON1] > PadThreshold ) {
			Printf( ">> CONTINUE selected.\n\n" );
			return A_CONTINUE;
		}
		if( c->button[ctrl::BT_BUTTON2] > PadThreshold ) {
			Printf( ">> BREAK selected.\n\n" );
			return A_BREAK;
		}
		core::Update();
		libc::FlushPrintf();
	}
}


