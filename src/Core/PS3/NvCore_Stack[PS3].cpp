/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>


#if defined( _NVCOMP_ENABLE_DBG )



#if defined(__MWERKS__)
#pragma dont_inline on
#endif

namespace
{
}



bool
nv::stack::GetFrame( Frame & outFS )
{
	return FALSE;
}


bool
nv::stack::GetBackFrame(	Frame &	outFS,
							Frame &	inFS	)
{
	return FALSE;
}


pvoid
nv::stack::GetReturnAddr	(			)
{
	return NULL;
}


pvoid
nv::stack::GetLocalAddr		(						)
{
	return NULL;
}


uint
nv::stack::OutputCallstack	(	Frame&		inFromF,
								pcstr		inLabel		)
{
	return 0;
}


#else	// _NVCOMP_ENABLE_DBG





bool
nv::stack::GetFrame		(	Frame	&	outF		)
{
	return FALSE;
}

bool
nv::stack::GetBackFrame	(	Frame	&	outBackF,
							Frame	&	inF			)
{
	return FALSE;
}

pvoid
nv::stack::GetReturnAddr	(		)
{
	return NULL;
}

pvoid
nv::stack::GetLocalAddr		(		)
{
	return NULL;
}

uint
nv::stack::OutputCallstack	(	Frame&		inFromF,
								pcstr		inLabel		)
{
	return 0;
}


#endif // _NVCOMP_ENABLE_DBG



