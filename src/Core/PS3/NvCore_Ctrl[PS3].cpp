/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <cell/pad.h>

using namespace nv;


#define				MAX_CONTROLER		7

#define _ATMON

namespace
{

	bool				opened;
	bool				initOk = FALSE;
	float				deadzA[ ctrl::TP_MAX ][ ctrl::BT_MAX ][ 2 ];	// lo/hi

	nv::ctrl::Status	ctrlStat[MAX_CONTROLER];
	bool				connected[MAX_CONTROLER];

#if defined(_ATMON)

	int 				sockServerID;
	int 				sockClientID;
	bool				sockDataPending;
	uint32				sockDataRcvCpt;
	bool				sockConnected;

	void	net_ctrl_Init	()
	{
		// Open network server
		sockConnected	 = FALSE;
		sockServerID	 = -1;
		sockClientID	 = -1;
		sockDataPending	 = FALSE;
		sockDataRcvCpt   = 0;

		char addr[16];
		if( net::GetHostAddr("localhost",0,addr)) {		
			sockServerID = net::Create(nv::net::T_TCP);
			if( sockServerID >= 0 ) {
				net::Open(sockServerID,5555,addr);
				net::Listen(sockServerID,2);
			}
		}
	}

	void	net_ctrl_Shut	()
	{
		net::Release(sockClientID);
		net::Release(sockServerID);	
		sockClientID = -1;
		sockServerID = -1;	
	}

	bool	net_ctrl_Connected()
	{
		return sockConnected;
	}

	bool	net_ctrl_Available()
	{
		return sockDataRcvCpt;
	}

	bool 	net_ctrl_Update()
	{
		if( sockServerID < 0 )		return FALSE;

		if( net::IsClientPending(sockServerID) )
		{
			if( sockClientID < 0 )
			{
				if( !net::Accept(sockServerID, sockClientID) )
					return FALSE;
			}
			else
			{
				int sockTmp;	
				net::Accept(sockServerID, sockTmp);
				net::Close(sockTmp);
				net::Release(sockTmp);
			}			
		}

		if( sockClientID < 0 )
			return FALSE;

		net::State cliStt = net::GetState( sockClientID );
		if( cliStt != nv::net::S_CONNECTED ) {
			net::Release(sockClientID);
			sockClientID = -1 ;
			sockDataPending = FALSE;
			sockConnected = FALSE;
			return FALSE;
		}

		sockConnected = TRUE;

		if( sockDataPending ) {
			if( net::IsRcvPending(sockClientID,sizeof(ctrlStat[0].button))) {
				uint32 dataSizeRcv;
				net::Receive(sockClientID,dataSizeRcv,(pvoid)ctrlStat[0].button,sizeof(ctrlStat[0].button));
				NV_ASSERT( dataSizeRcv == sizeof(ctrlStat[0].button) );
				sockDataPending = FALSE;
				sockDataRcvCpt++;
			}
		}

		if( !sockDataPending ) {
			static const char query[]={"update\n"};
			sockDataPending = net::SendWait( sockClientID, 0, (void*)query, 7 );
		}

		return TRUE;				
	}

#else

#define	net_ctrl_Init()			{					}
#define	net_ctrl_Shut()			{					}
#define	net_ctrl_Connected()	FALSE
#define	net_ctrl_Available()	FALSE
#define	net_ctrl_Update()		FALSE

#endif

	float ConvertPS3PadStickTF (uint16 c){ // char 0x0, 0xFF to float[-1,1]
		return float ((c << 1) / 255.0f) - 1.0f;
		//return 0.0F;
	}

	float ConvertPS3PadButtonTF(uint16 c){ // char 0x0, 0xFF to float[0,1]
//		if ( c > 0xFF ) return 0.0f;
		float f = float ( (c) / 255.0f );
		return f;
	}

	void ClearStates()
	{
		for (uint i = 0 ; i < MAX_CONTROLER ; ++i)
			for (uint b = 0 ; b < ctrl::BT_MAX ; ++b)
				ctrlStat[i].button[b] = 0.0f;
	}

}




bool
nvcore_Ctrl_Init()
{
	net_ctrl_Init();

	for( int i = 0 ; i < ctrl::TP_MAX ; i++ ) {
		for( int j = 0 ; j < ctrl::BT_MAX ; j++ ) {
			if( i==ctrl::TP_PADDLE ) {
				deadzA[i][j][0] = 0.3f;
				deadzA[i][j][1] = 1.0f;
			} else {
				deadzA[i][j][0] = 0.f;
				deadzA[i][j][1] = 0.f;
			}
		}
	}

	opened = FALSE;

	int err = cellPadInit(MAX_CONTROLER);
	if (err != CELL_PAD_OK)
		return FALSE;

	initOk = TRUE;
	return TRUE;
}


void
nvcore_Ctrl_Shut()
{
	nv::ctrl::Close();
	net_ctrl_Shut();
	cellPadEnd();
	initOk = FALSE;
}


void
nv::ctrl::Open(		)
{
	if (!initOk) return;
	Memset(connected,0,MAX_CONTROLER * sizeof(bool) );
	opened = TRUE;
	ClearStates();
}


void
nv::ctrl::Close()
{
	opened = FALSE;
}


uint
nv::ctrl::GetNbMax	(		)
{
	return MAX_CONTROLER;
}

void
nvcore_Ctrl_Update()
{
	if (!opened)								return;
	CellPadInfo info;
	if ( cellPadGetInfo(&info) != CELL_PAD_OK ) {
		ClearStates();
		return;
	}

	for (uint i = 0 ; i < MAX_CONTROLER ; ++i){

		if ( (info.status[i] == 1) && !connected[i]) {
			connected[i] = TRUE;
			cellPadSetPressMode	(i,CELL_PAD_PRESS_MODE_ON);
			cellPadSetSensorMode(i,CELL_PAD_SENSOR_MODE_ON);
			cellPadClearBuf		(i);

			Printf ("Found pad %d, "   ,i);
			Printf ("Vendor ID : %u, " ,info.vendor_id[i]);
			Printf ("Product ID : %u\n",info.vendor_id[i]);

			ctrlStat[i].type = ctrl::TP_PADDLE;
		}
		else if ( info.status[0] == 0){
			connected[i] = FALSE;
		}
	}

	for (uint i = 0 ; i < MAX_CONTROLER ; ++i){

		if ( !connected[i] ) continue;

		CellPadData padData;
		if ( cellPadGetData( i,&padData ) != CELL_PAD_OK) continue;

		if (padData.len ==0) continue;

		ctrlStat[i].button[ctrl::BT_BUTTON8]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL1] & CELL_PAD_CTRL_SELECT)?1.0f:0.0f;	
		ctrlStat[i].button[ctrl::BT_BUTTON9]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL1] & CELL_PAD_CTRL_START)?1.0f:0.0f;	
		ctrlStat[i].button[ctrl::BT_BUTTON10]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL1] & CELL_PAD_CTRL_L3)?1.0f:0.0f;	
		ctrlStat[i].button[ctrl::BT_BUTTON11]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL1] & CELL_PAD_CTRL_R3)?1.0f:0.0f;	

		ctrlStat[i].button[ctrl::BT_DIR1X]		= ConvertPS3PadStickTF(padData.button[CELL_PAD_BTN_OFFSET_ANALOG_LEFT_X]);	
		ctrlStat[i].button[ctrl::BT_DIR1Y]		= -ConvertPS3PadStickTF(padData.button[CELL_PAD_BTN_OFFSET_ANALOG_LEFT_Y]);		
		ctrlStat[i].button[ctrl::BT_DIR2X]		= ConvertPS3PadStickTF(padData.button[CELL_PAD_BTN_OFFSET_ANALOG_RIGHT_X]);	
		ctrlStat[i].button[ctrl::BT_DIR2Y]		= -ConvertPS3PadStickTF(padData.button[CELL_PAD_BTN_OFFSET_ANALOG_RIGHT_Y]);	

		if (cellPadInfoPressMode(i) == CELL_PAD_INFO_SUPPORTED_PRESS_MODE ) {
			ctrlStat[i].button[ctrl::BT_DIR3right]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_RIGHT]);
			ctrlStat[i].button[ctrl::BT_DIR3left]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_LEFT]);
			ctrlStat[i].button[ctrl::BT_DIR3up]		= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_UP]);
			ctrlStat[i].button[ctrl::BT_DIR3down]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_DOWN]);

			ctrlStat[i].button[ctrl::BT_BUTTON0]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_TRIANGLE]);
			ctrlStat[i].button[ctrl::BT_BUTTON1]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_CIRCLE]);
			ctrlStat[i].button[ctrl::BT_BUTTON2]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_CROSS]);
			ctrlStat[i].button[ctrl::BT_BUTTON3]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_SQUARE]);

			ctrlStat[i].button[ctrl::BT_BUTTON4]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_L2]);
			ctrlStat[i].button[ctrl::BT_BUTTON5]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_R2]);
			ctrlStat[i].button[ctrl::BT_BUTTON6]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_L1]);
			ctrlStat[i].button[ctrl::BT_BUTTON7]	= ConvertPS3PadButtonTF(padData.button[CELL_PAD_BTN_OFFSET_PRESS_R1]);
		}
		else {
			ctrlStat[i].button[ctrl::BT_DIR3right]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL1] & CELL_PAD_CTRL_RIGHT)?1.0f:0.0f;
			ctrlStat[i].button[ctrl::BT_DIR3left]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL1] & CELL_PAD_CTRL_LEFT)?1.0f:0.0f;	
			ctrlStat[i].button[ctrl::BT_DIR3up]		= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL1] & CELL_PAD_CTRL_UP)?1.0f:0.0f;
			ctrlStat[i].button[ctrl::BT_DIR3down]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL1] & CELL_PAD_CTRL_DOWN)?1.0f:0.0f;

			ctrlStat[i].button[ctrl::BT_BUTTON0]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL2] & CELL_PAD_CTRL_TRIANGLE)?1.0f:0.0f;
			ctrlStat[i].button[ctrl::BT_BUTTON1]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL2] & CELL_PAD_CTRL_CIRCLE)?1.0f:0.0f;
			ctrlStat[i].button[ctrl::BT_BUTTON2]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL2] & CELL_PAD_CTRL_CROSS)?1.0f:0.0f;
			ctrlStat[i].button[ctrl::BT_BUTTON3]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL2] & CELL_PAD_CTRL_SQUARE)?1.0f:0.0f;

			ctrlStat[i].button[ctrl::BT_BUTTON4]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL2] & CELL_PAD_CTRL_L2)?1.0f:0.0f;
			ctrlStat[i].button[ctrl::BT_BUTTON5]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL2] & CELL_PAD_CTRL_R2)?1.0f:0.0f;
			ctrlStat[i].button[ctrl::BT_BUTTON6]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL2] & CELL_PAD_CTRL_L1)?1.0f:0.0f;
			ctrlStat[i].button[ctrl::BT_BUTTON7]	= (padData.button[CELL_PAD_BTN_OFFSET_DIGITAL2] & CELL_PAD_CTRL_R1)?1.0f:0.0f;
		}

		ctrlStat[i].button[ctrl::BT_BUTTON12]	=0.0f;	
		ctrlStat[i].button[ctrl::BT_BUTTON13]	=0.0f;	
		ctrlStat[i].button[ctrl::BT_BUTTON14]	=0.0f;	
		ctrlStat[i].button[ctrl::BT_BUTTON15]	=0.0f;	
	}

}



nv::ctrl::Status*
nv::ctrl::GetStatus(	uint	inCtrlNo		)
{
	if( !opened )				return NULL;
	if( inCtrlNo>=GetNbMax())	return NULL;

	// remote ctrl ?
	if( net_ctrl_Connected() )
		return net_ctrl_Available() ? &ctrlStat[0] : NULL;

	// onboard ctrl
	return connected[inCtrlNo] ? (ctrlStat+inCtrlNo) : NULL ;
}


bool
nv::ctrl::SetActuators	(	uint		inCtrlNo,
							float		inLowSpeed,
							float		inHighSpeed	)
{
	return FALSE;
}


bool
nv::ctrl::GetDefDeadZone	(		Type			inType,
									Button			inButton,
									float*			outLo,
									float*			outHi			)
{
	int t = int(inType);
	int b = int(inButton);
	if( t<0 || t>=TP_MAX )	return FALSE;
	if( b<0 || b>=BT_MAX )	return FALSE;
	if( outLo )		*outLo = deadzA[t][b][0];
	if( outHi )		*outHi = deadzA[t][b][1];
	return TRUE;
}


bool
nv::ctrl::SetDefDeadZone	(		Type			inType,
									Button			inButton,
									float			inLo,
									float			inHi			)
{
	float lo, hi;
	if( !GetDefDeadZone(inType,inButton,&lo,&hi) )
		return FALSE;
	if( inLo >= 0.f )	lo = Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return FALSE;
	int t = int(inType);
	int b = int(inButton);
	deadzA[t][b][0] = lo;
	deadzA[t][b][1] = hi;
	return TRUE;
}


float
nv::ctrl::Status::Filtered	(		Button			inButton,
									float			inLo,
									float			inHi	)
{
	if( int(inButton)<0 || int(inButton)>=BT_MAX )
		return 0.f;

	float lo, hi;
	if( !GetDefDeadZone(type,inButton,&lo,&hi) )
		return 0.f;

	if( inLo >= 0.f )	lo = Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return 0.f;

	float b  = Clamp( button[int(inButton)], -1.f, +1.f );
	float ab = Absf( b );
	float sb = ( b>=0.f ? 1.f : -1.f );

	if( ab <= lo )	return 0.f;
	if( ab >= hi )	return sb;
	return sb * (ab-lo)/(hi-lo);
}





