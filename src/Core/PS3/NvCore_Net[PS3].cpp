/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Nova.h>
using namespace nv;

namespace
{

}


bool
nvcore_Net_Init()
{
	return FALSE;
}


void
nvcore_Net_Update()
{
	//
}


void
nvcore_Net_Shut()
{

}


bool
nv::net::GetHostAddr		(	pcstr				inName,
								uint				inAddrNo,
								pstr				outAddr			)
{
	return FALSE;
}


int
nv::net::Create	(	Type		inType		)
{			

	return -1;
}


void
nv::net::Release	(	int			inSockId	)
{

}


void
nv::net::Close		(	int			inSockId	)
{

}


bool
nv::net::Open		(	int			inSockId,
						uint16		inPort,
						pcstr		inAddr		)
{
	return FALSE;
}


bool
nv::net::Listen	(	int			inSockId,
					uint32		inPendingClients	)
{
	return FALSE;
}


bool
nv::net::Accept	(	int			inSockId,
					int	&		outSockId	)
{
	return FALSE;
}


bool
nv::net::Connect	(	int			inSockId,
						pcstr		inAddr,
						uint16		inPort		)
{
	return FALSE;
}


bool
nv::net::ConnectWait	(	int					inSockId,
							uint				inMsTimeout,
							pcstr				inAddr,
							uint16				inPort		)
{
	return FALSE;
}


bool
nv::net::Receive	(	int					inSockId,
						uint32		&		outRcvSize,
						pvoid				inBuff,
						uint32				inSize,
						bool				peek		)
{
	return FALSE;
}


bool
nv::net::Send		(	int					inSockId,
						uint32		&		outSndSize,
						pvoid				inBuff,
						uint32				inSize			)
{
	return FALSE;
}


bool
nv::net::ReceiveWait	(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize		)
{
	return FALSE;
}


bool
nv::net::SendWait		(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize			)
{
	return FALSE;
}


bool
nv::net::GetPeerAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort		)
{
	return FALSE;
}


bool
nv::net::GetSockAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort			)
{
	return FALSE;
}


bool
nv::net::IsRcvPending	(	int			inSockId,
							uint		inBSize		)
{
	return FALSE;
}


bool
nv::net::IsClientPending		(	int			inSockId		)
{
	return FALSE;
}


int32
nv::net::GetMTUBSize	(	int			inSockId		)
{
	return 0;
}


nv::net::State
nv::net::GetState		(	int			inSockId		)
{
	return S_INVALID;
}


nv::net::Type
nv::net::GetType		(	int			inSockId		)
{
	return T_UNDEF;
}


