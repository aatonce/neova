/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <stdio.h>

#include <sys/paths.h>
#include <cell/cell_fs.h>
#include <sys/timer.h>

using namespace nv;


//#define	DBG_SYNC_READ
#define OUTPUT_ERROR
#define SYNC_WAIT_MICRO_SECOND (50)

namespace
{
	enum State {
		IDLE 		= 		0,
		SEEKING		=		1,
		READING		=		2
	};

	int			fileID;
	

	// used for async seek/read
	volatile State		async_state;
	volatile bool 		async_success;
	pvoid				async_bufferPtr;
	uint				async_bsize;
	uint				async_boffset;

	CellFsAio	asyncIO;
	volatile int		asyncID;

	struct Device {
		char *	name;
		char *	prefix;
		bool	isInit;
	};

	Device subDev[] = {
		// name					prefix				isInit
		{"Home",				SYS_APP_HOME,	FALSE},
		{"Devkit root",			SYS_HOST_ROOT,	NULL},
		{"BDVD",				SYS_DEV_BDVD,	NULL},
		{"Hdd Game data",		SYS_DEV_HDD0,	NULL},
		{"Memory stick",		SYS_DEV_MS,		NULL}
	};

	int GetSubDevId ( const char * path ) {
		if ( !path )
			return -1;
		
		for (uint i = 0 ; i < sizeof(subDev) / sizeof(Device) ; ++i ) {
			if (Strncmp(subDev[i].prefix , path , Strlen(subDev[i].prefix)) == 0){
#ifdef OUTPUT_ERROR
				Printf("path %s match with prefix %s\n",path,subDev[i].prefix);
#endif 
				return i;
			}
		}
		return -1;
	}

	void callBackReadASync(CellFsAio *xaio, CellFsErrno error, int xid, uint64_t size){
		NV_ASSERT(asyncID != -1);
		NV_ASSERT(xid == asyncID);
		
		if (error != CELL_FS_SUCCEEDED || size != async_bsize) {
			async_success = FALSE;
		}
		asyncID = -1;
		async_state = IDLE;
	}

#ifdef OUTPUT_ERROR
	struct FileError {
		int  err;
		char * msg;
	};
	FileError openErr[] = {
		{CELL_FS_SUCCEEDED,
		"Normal termination"},
		{CELL_FS_ENOTMOUNTED,
		"File system corresponding to path is not mounted"},
		{CELL_FS_ENOENT,
		"The file specified by path does not exist The specified file does not exist and CELL_FS_O_CREAT is not specified in flags"},
		{CELL_FS_EINVAL,
		"The specified path is invalid Extended argument is invalid"},
		{CELL_FS_EMFILE,
		"The number of file descriptors that can be opened at the same time has reached the maximum"},
		{CELL_FS_EISDIR,
		"The specified path is a directory"},
		{CELL_FS_EIO,
		"I/O error has occurred"},
		{CELL_FS_ENOMEM,
		"Memory is insufficient"},
		{CELL_FS_ENOTDIR,
		"Components in path contain something other than a directory"},
		{CELL_FS_ENAMETOOLONG,
		"path or components in path exceed the maximum length"},
		{CELL_FS_EFSSPECIFIC,
		"File system specific internal error has occurred"},
		{CELL_FS_EFAULT,
		"When size is not 0, a NULL pointer is specified in arg path or fd is NULL"},
		{CELL_FS_EPERM,
		"Permission is invalid"},
		{CELL_FS_EACCES,
		"Permission is invalid (Only CFS, FAT and HOSTFS)"},
		{CELL_FS_EEXIST,
		"When the specified file exists, CELL_FS_O_CREAT and CELL_FS_O_EXCL are specified"},
		{CELL_FS_ENOSPC,
		"When CELL_FS_O_CREAT is specified in flags and the file does not exist, there is no area to create a new file"},
		{CELL_FS_EROFS,
		"When CELL_FS_O_CREAT is specified in flags and the file does not exist, the directory in which it is to be created does not permit writing (Only CFS (SYS_DEV_HDD1) and FAT)"},
		{CELL_FS_ENOTMSELF,
		"A file other than the MSELF file is specified in path, and either CELL_FS_O_MSELF or CELL_FS_O_RDONLY|CELL_FS_O_MSELF is specified in flags"},
	};

	FileError readErr[] = {
		{CELL_FS_SUCCEEDED,
		"Normal termination"},
		{CELL_FS_EBADF,
		"fd is invalid fd is not opened for read"},
		{CELL_FS_EIO,
		"I/O error has occurred"},
		{CELL_FS_ENOMEM,
		"Memory is insufficient"},
		{CELL_FS_EFSSPECIFIC,
		"File system specific internal error has occurred"},
		{CELL_FS_EPERM,
		"Read permission does not exist in file"},
		{CELL_FS_EFAULT,
		"buf is NULL"},
	};

	FileError seekErr[] = {
		{CELL_FS_SUCCEEDED,
		"Normal termination"},
		{CELL_FS_EBADF,
		"fd is invalid"},
		{CELL_FS_EINVAL,
		"Argument is invalid Position to seek to is a negative value"},
		{CELL_FS_EIO,
		"I/O error has occurred"},
		{CELL_FS_ENOMEM,
		"Memory is insufficient"},
		{CELL_FS_EFSSPECIFIC,
		"File system specific internal error has occurred"},
		{CELL_FS_EFAULT,
		"pos is NULL"},
	};

	FileError initASyncErr[] = {
		{CELL_FS_SUCCEEDED,	"Normal termination"},
		{CELL_FS_EFAULT, 	"mount_point is NULL"},
		{CELL_FS_EINVAL, 	"mount_point is invalid"},
		{CELL_FS_EBUSY ,	"Maximum number of times for initialization (CELL_FS_AIO_MAX_FS) has been exceeded"},
		{CELL_FS_ENOMEM, 	"Memory is insufficient"},
	};

	FileError asyncReadErr[] = {
		{CELL_FS_SUCCEEDED, "Normal termination"},
		{CELL_FS_EFAULT, 	"aio, id or func is NULL"},
		{CELL_FS_EBADF, 	"fd specified by aio is invalid"},
		{CELL_FS_EBUSY, 	"Maximum number of times for asynchronous I/O (CELL_FS_AIO_MAX_REQUEST) has been exceeded"},
		{CELL_FS_ENXIO, 	"The file system corresponding to fd specified in aio is not initialized by cellFsAioInit()"},
	};

#define OutputError(__err,__fctErr) for (uint i = 0 ; i < sizeof (__fctErr) / sizeof(FileError) ; ++i)  \
								if (__err == __fctErr[i].err)											\
									printf("%s\n",__fctErr[i].msg);								

#else
	#define OutputError(__err,__fctErr)
#endif //OUTPUT_ERROR
}



bool
nvcore_File_io_Init()
{
	fileID 			= -1;
	asyncID			= -1;
	async_state		= IDLE;
	async_success	= TRUE;
	return TRUE;
}


bool
nvcore_File_io_IsOpened		(		)
{
	return ( fileID >= 0 ) ;
}


bool
nvcore_File_io_IsReady		(		)
{
	// no file opened
	if( !nvcore_File_io_IsOpened() )
		return TRUE;

	if( async_state == IDLE )
		return TRUE;

	return FALSE;
}

bool
nvcore_File_io_IsSuccess	(		)
{
	if( !nvcore_File_io_IsOpened() )
		return FALSE;
	return async_success;
}

void
nvcore_File_io_Sync	(	)
{
	while ( ! nvcore_File_io_IsReady(	) ){
		sys_timer_usleep(SYNC_WAIT_MICRO_SECOND);
	}
}

nv::file::Status
nvcore_File_io_GetStatus	(	)
{
	if( async_state != IDLE )
		return nv::file::FS_BUSY;
	else
		return nv::file::FS_OK;
}

bool
nvcore_File_io_Open	(	pcstr	inFilename,	 uint32&	outBSize	)
{
	if( !inFilename || Strlen(inFilename)==0 )
		return FALSE;

	if( nvcore_File_io_IsOpened() )
		return FALSE;

	// safe sync
	nvcore_File_io_Sync();
	async_state = IDLE;

	CellFsErrno error;
	error = cellFsOpen( inFilename, CELL_FS_O_RDONLY , &fileID ,NULL, 0 );
	if ( error != CELL_FS_SUCCEEDED ){
		fileID = -1;
		OutputError(error,openErr);
		return FALSE;
	}
	
	CellFsStat stat;

	error = cellFsFstat(fileID,&stat);
	if ( error != CELL_FS_SUCCEEDED ){
		fileID = -1;
		cellFsClose(fileID);
		return FALSE;
	}

	outBSize = stat.st_size;

#ifndef DBG_SYNC_READ
	uint devId = GetSubDevId(inFilename);
	if (devId < 0 ) {
		cellFsClose(fileID);
		return FALSE;
	}
	if (!subDev[devId].isInit){
		subDev[devId].isInit = TRUE;
		CellFsErrno error;
		error = cellFsAioInit(subDev[devId].prefix);
		if (error != CELL_FS_SUCCEEDED){
			cellFsClose(fileID);

			OutputError(error,initASyncErr);
			return FALSE;
		}
	}
#endif 
	return TRUE;
}


bool
nvcore_File_io_Read		(	uint		inSysFlags,
							pvoid		inBufferPtr,
							uint32		inBSize,
							uint32		inBOffset0,
							uint32		inBOffset1,
							uint32		inBOffset2,
							uint32		inBOffset3	)
{
	// Not in use or invalid call !
	if(		!nvcore_File_io_IsOpened()
		||	!inBufferPtr )
		return FALSE;

	if( inBSize == 0 )
		return TRUE;

	// safe sync
	nvcore_File_io_Sync();

	// async parameters
	async_bufferPtr  = inBufferPtr;
	async_boffset	 = inBOffset0;
	async_bsize 	 = inBSize;

#ifdef DBG_SYNC_READ
	CellFsErrno error;
	error = cellFsLseek(fileID,async_boffset,CELL_FS_SEEK_SET,NULL);

	if( error != CELL_FS_SUCCEEDED && error != CELL_FS_EFAULT) {
		DebugPrintf( "cellFsLseek() error %x !\n", uint(async_boffset), async_boffset );
		OutputError(error,seekErr);
		return FALSE;
	}
	
	uint64 readBSize;
	error = cellFsRead(fileID,async_bufferPtr,async_bsize,&readBSize);
	if( error != CELL_FS_SUCCEEDED || readBSize != async_bsize) {
		DebugPrintf( "sceIoRead() error %x %x !\n", uint(readBSize), async_bsize );
		OutputError(error,readErr);
		return FALSE;
	}
	async_success = TRUE;
	return TRUE;

#else
	CellFsErrno error;

	asyncIO.buf			= inBufferPtr;
	asyncIO.fd			= fileID;
	asyncIO.offset		= inBOffset0;
	asyncIO.size		= inBSize;
	asyncIO.user_data	= NULL;

	int tmpId;
	error = cellFsAioRead(	&asyncIO,
							&tmpId,
							&callBackReadASync );
	asyncID = tmpId;

	if (error != CELL_FS_SUCCEEDED){
		asyncID = -1;
		OutputError(error,asyncReadErr);
		return FALSE;
	}

	async_state		= READING;
	async_success	= TRUE;

	return TRUE;
#endif
}


bool
nvcore_File_io_DumpFrom	(	pcstr	inFilename,
							pvoid&	outBuffer,
							uint&	outBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;

	CellFsErrno error;
	int fid;
	error = cellFsOpen( inFilename, CELL_FS_O_RDONLY , &fid ,NULL, 0 );
	if ( error != CELL_FS_SUCCEEDED ) return FALSE;
	
	CellFsStat stat;
	error = cellFsFstat(fid,&stat);
	if ( error != CELL_FS_SUCCEEDED ){
		cellFsClose(fid);
		return FALSE;
	}

	outBSize = stat.st_size;

	outBuffer = NvMalloc( outBSize );
	if( !outBuffer ) {
		cellFsClose(fid);
		return FALSE;
	}

	uint64 readBSize;
	error = cellFsRead(fid,outBuffer,outBSize,&readBSize);
	if( error != CELL_FS_SUCCEEDED || readBSize != outBSize) {
		cellFsClose(fid);
		return FALSE;
	}

	cellFsClose(fid);
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_DumpTo	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{

#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;

	CellFsErrno error;
	int fid;
	error = cellFsOpen( inFilename, CELL_FS_O_WRONLY | CELL_FS_O_CREAT | CELL_FS_O_TRUNC , &fid ,NULL, 0 );
	if ( error != CELL_FS_SUCCEEDED ){
		return FALSE;
	}

	uint64 writeBSize;
	error = cellFsWrite(fid,inBuffer,inBSize,&writeBSize);
	if( error != CELL_FS_SUCCEEDED || writeBSize != inBSize) {
		cellFsClose(fid);
		return FALSE;
	}

	cellFsClose(fid);
	return TRUE;
#else
	return FALSE;
#endif

}


bool
nvcore_File_io_Append	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{

#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;

	CellFsErrno error;
	int fid;
	error = cellFsOpen( inFilename, CELL_FS_O_WRONLY | CELL_FS_O_CREAT | CELL_FS_O_APPEND, &fid ,NULL, 0 );
	if ( error != CELL_FS_SUCCEEDED ) return FALSE;

	uint64 writeBSize;
	error = cellFsWrite(fid,inBuffer,inBSize,&writeBSize);
	if( error != CELL_FS_SUCCEEDED || writeBSize != inBSize) {
		cellFsClose(fid);
		return FALSE;
	}

	cellFsClose(fid);
	return TRUE;
#else
	return FALSE;
#endif

}


bool
nvcore_File_io_Close	(			)
{
	if( nvcore_File_io_IsOpened() ) {
		nvcore_File_io_Sync();
		cellFsClose(fileID);
		fileID = -1;
	}
	return TRUE;
}


void
nvcore_File_io_Shut		(			)
{
	nvcore_File_io_Close();
}



uint
nvcore_File_io_GetNbSubDevice	(		)
{
	uint nb_subdev = sizeof(subDev)/sizeof(Device);
	return nb_subdev;
}


pcstr
nvcore_File_io_GetSubDeviceName		(	uint	inSubDevNo			)
{
	uint nb_subdev = nvcore_File_io_GetNbSubDevice();
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo].name;
}


pcstr
nvcore_File_io_GetSubDevicePrefix	(	uint	inSubDevNo			)
{
	uint nb_subdev = nvcore_File_io_GetNbSubDevice();
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo].prefix;
}


pcstr
nvcore_File_io_GetSubDeviceSuffix	(	uint	inSubDevNo			)
{
	return NULL;
}


