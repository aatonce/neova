/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <kernel.h>
#include <pspnet.h>
#include <errno.h>
#include <pspnet_error.h>
#include <pspnet_inet.h>
#include <pspnet_resolver.h>
#include <pspnet/sys/socket.h>
#include <pspnet/sys/select.h>
#include <pspnet/sys/time.h>
#include <pspnet/netinet/in.h>
#include <pspnet_apctl.h>
#include <pspnet_ap_dialog_dummy.h>
#include <wlan.h>

#include <Nova.h>
using namespace nv;



//#define		NO_NETWORK
#define			DEF_WAIT_TIMEOUT	3000

#if defined(_ATMON)
#define		NO_NET_INIT
#endif


#define PSPNET_POOLSIZE				0x20000
#define CALLOUT_SPL					0x20
#define CALLOUT_STACKSIZE			0x1000
#define NETINTR_SPL					0x20
#define NETINTR_STACKSIZE			0x1000
#define SCE_APCTL_STACKSIZE			(1024 * 4)
#define SCE_APCTL_PRIO				48
#define AP_DIALOG_DUMMY_WAIT_TIME	(1000 * 1000)


#define RESOLVER_BUFFER_SIZE		1024
#define RESOLVER_TIMEOUT			(5*1000*1000)
#define RESOLVER_RETRY				5
#define	DEFAULT_WAIT_TO				(6*1000*1000)



namespace
{

#ifndef NO_NETWORK

	void psp_net_shut	(	)
	{
		#ifndef NO_NET_INIT
		sceNetApctlTerm();
		sceNetResolverTerm();
		sceNetInetTerm();
		sceNetTerm();
		#endif
	}
	
	bool psp_net_init	(	)
	{
		#ifndef NO_NET_INIT
		Printf( "<Neova> Init PSP INET\n" );
		struct SceNetApDialogDummyStateInfo ap_dialog_dummy_state;
		static struct SceNetApDialogDummyParam ap_dialog_dummy_param;
		int ret;
		ret = sceNetInit		( PSPNET_POOLSIZE, CALLOUT_SPL, CALLOUT_STACKSIZE, NETINTR_SPL, NETINTR_STACKSIZE );
		ret = sceNetInetInit	(				);
		ret = sceNetApctlInit	( SCE_APCTL_STACKSIZE, SCE_APCTL_PRIO );
		ret = sceNetApctlConnect(1				);
		uint cpt = 0;
		while (1) {
			int state;
			ret = sceNetApctlGetState(&state);
			if( ret != 0 || cpt > 80) {
				break;
				psp_net_shut();
				return FALSE;
			}
			if (state == 4)
				break;  // connected with static IP
			// wait a little before polling again
			sceKernelDelayThread(50*1000); // 50ms
			cpt++;
		}
		Printf( "<Neova> PSP INET done.\n" );
		#endif
		return TRUE;
	}

	struct Sock {
		int				hdl;		// sce sock id
		net::State		state;		
		net::Type		type;		// TCP & UDP
		int				next;		// next free
	};

	nv::sysvector<Sock>		sockA;
	int						sockFreeIdx;

	char 					resolverBuffer[RESOLVER_BUFFER_SIZE];
	int 					resolverId;

#endif // NO_NETWORK

}


bool
nvcore_Net_Init()
{
#ifdef NO_NETWORK
	Printf( "<Neova> Network disabled !\n" );
	return FALSE;
#else
	sockA.Init();
	sockA.clear();
	sockFreeIdx = -1;

	if( !psp_net_init() )
		return FALSE;

	if( sceNetResolverCreate(&resolverId,resolverBuffer,sizeof(resolverBuffer)) != 0 )
		return FALSE;

	char myaddrs[SCE_UTILITY_NET_PARAM_IPV4_ADDR_STR_LEN];
	Zero( myaddrs );
	if( net::GetHostAddr("localhost",0,myaddrs) )
		Printf( "<Neova> Network initialized [ip:%s]\n", myaddrs );
	return TRUE;
#endif
}


void
nvcore_Net_Update()
{
	//
}


void
nvcore_Net_Shut()
{
#ifndef NO_NETWORK
	sockA.Shut();
	sockFreeIdx = -1;

	if( (uint)sceNetResolverDelete(resolverId) == SCE_ERROR_NET_RESOLVER_CTX_BUSY)
		sceNetResolverStop( resolverId );
	sceNetResolverDelete( resolverId );

	psp_net_shut();
#endif // NO_NETWORK
}


uint
nv::net::GetHostAddrCpt		(	pcstr		inName		)
{
#ifdef NO_NETWORK
	return 0;
#else
	char buff[SCE_UTILITY_NET_PARAM_IPV4_ADDR_STR_LEN];
	if (GetHostAddr(inName,0,buff))
		return 1;
	return 0;
#endif
}


bool
nv::net::GetHostAddr		(	pcstr				inName,
								uint				inAddrNo,
								pstr				outAddr			)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	//outAddr must be a valid adress with a minimum size of 'sizeof("XXX.XXX.XXX.XXX")'.
	if( !outAddr )
		return FALSE;

	if(!inName || libc::Stricmp(inName,"localhost")==0 ){
		union SceNetApctlInfo info;
		uint ret = sceNetApctlGetInfo(SCE_NET_APCTL_INFO_IP_ADDRESS,	&info	);
		if (ret != 0) 		
			return FALSE;		
		Memcpy(outAddr,info.ip_address,SCE_UTILITY_NET_PARAM_IPV4_ADDR_STR_LEN);		
		return TRUE;
	}

	if (sceNetInetInetAddr(inName) == SCE_NET_INET_INADDR_NONE) {							
		struct SceNetInetInAddr addr;		
		if (sceNetResolverStartNtoA(resolverId, inName, &addr, RESOLVER_TIMEOUT, RESOLVER_RETRY) == 0){			
			const char * str = sceNetInetInetNtop(SCE_NET_INET_AF_INET, &addr, outAddr, SCE_UTILITY_NET_PARAM_IPV4_ADDR_STR_LEN);			
			return (str==NULL)?FALSE:TRUE;
		}	
	}

	return FALSE;
#endif
}


int
nv::net::Create	(	Type		inType		)
{			
#ifdef NO_NETWORK
	return -1;
#else
	// UPD not supported yet !
	if( inType != T_TCP )
		return -1;

	if( sockFreeIdx == -1 ) {
		// Resize
		int N = sockA.size();
		int P = Max( 4, N*2 );
		sockA.resize( P );
		for( int i = N ; i < P ; i++ ) {
			sockA[i].hdl   = NULL;
			sockA[i].state = S_INVALID;
			sockA[i].type  = T_UNDEF;
			sockA[i].next  = (i==P-1) ? -1 : i+1;
		}
		sockFreeIdx = N;
	}

	// alloc sockId
	int sid		= sockFreeIdx;
	sockFreeIdx	= sockA[sid].next;

	// init sock
	sockA[sid].hdl   = NULL;
	sockA[sid].state = S_CLOSED;
	sockA[sid].type	 = inType;
	return sid;
#endif
}


void
nv::net::Release	(	int			inSockId	)
{
#ifndef NO_NETWORK
	State stt = GetState( inSockId );
	if( stt < 0 )	return;
	Close( inSockId );
	// free sockId
	sockA[inSockId].hdl   = NULL;
	sockA[inSockId].state = S_INVALID;
	sockA[inSockId].type  = T_UNDEF;
	sockA[inSockId].next  = sockFreeIdx;
	sockFreeIdx = inSockId;
#endif
}


void
nv::net::Close		(	int			inSockId	)
{
#ifndef NO_NETWORK
	if( !IsClosed(inSockId) ) {
		if( sockA[inSockId].type == T_TCP )
			sceNetInetShutdown( sockA[inSockId].hdl, SCE_NET_INET_SHUT_RDWR );
		sceNetInetClose( sockA[inSockId].hdl );
		sockA[inSockId].hdl   = NULL;
		sockA[inSockId].state = S_CLOSED;
	}
#endif
}


bool
nv::net::Open		(	int			inSockId,
						uint16		inPort,
						pcstr		inAddr		)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	if( GetState(inSockId) != S_CLOSED )
		return FALSE;

	if( sockA[inSockId].type != T_TCP )
		return FALSE;

	int sceSockId = sceNetInetSocket( SCE_NET_INET_AF_INET, SCE_NET_INET_SOCK_STREAM, 0 );
	if( sceSockId <= 0 )
		return FALSE;

	struct SceNetInetSockaddrIn sin;
	Zero( sin );
	sin.sin_len 		= sizeof(sin);
	sin.sin_family 		= SCE_NET_INET_AF_INET;
	sin.sin_addr.s_addr = inAddr? sceNetInetInetAddr(inAddr) : SCE_NET_INET_INADDR_ANY;
	sin.sin_port 		= sceNetHtons(inPort);
	if( sin.sin_addr.s_addr == SCE_NET_INET_INADDR_NONE )
		return FALSE;

	int	opt_nbio  = 1;
	int	opt_reuse = 1;
	sceNetInetSetsockopt( sceSockId, SCE_NET_INET_SOL_SOCKET, SCE_NET_INET_SO_REUSEADDR, &opt_reuse, sizeof(opt_reuse) );
	sceNetInetSetsockopt( sceSockId, SCE_NET_INET_SOL_SOCKET, SCE_NET_INET_SO_REUSEPORT, &opt_reuse, sizeof(opt_reuse) );
	sceNetInetSetsockopt( sceSockId, SCE_NET_INET_SOL_SOCKET, SCE_NET_INET_SO_NBIO, &opt_nbio, sizeof(opt_nbio) );

	if(	sceNetInetBind(sceSockId,(struct SceNetInetSockaddr*)&sin,sizeof(sin)) < 0 )
		return FALSE;

	sockA[inSockId].hdl	  = sceSockId;
	sockA[inSockId].state = S_OPENED;	
	return TRUE;
#endif	
}


bool
nv::net::Listen	(	int			inSockId,
					uint32		inPendingClients	)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	if( GetState(inSockId) != S_OPENED )
		return FALSE;
	if( sceNetInetListen(sockA[inSockId].hdl,1) <0 )
		return FALSE;
	sockA[inSockId].state = S_LISTENING;
	return TRUE;
#endif
}


bool
nv::net::Accept	(	int			inSockId,
					int	&		outSockId	)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	outSockId = -1;
	if( GetState(inSockId) != S_LISTENING )
		return FALSE;

	struct SceNetInetSockaddrIn sin;
	Zero( sin );
	uint sockaddr_len = sizeof(sin);
	int newSock = sceNetInetAccept(	sockA[inSockId].hdl, (struct SceNetInetSockaddr*)&sin, &sockaddr_len );
	if( newSock <= 0 )
		return FALSE;

	// non-blocking mode
	int	opt_nbio = 1;
	sceNetInetSetsockopt( newSock, SCE_NET_INET_SOL_SOCKET, SCE_NET_INET_SO_NBIO, &opt_nbio, sizeof(opt_nbio) );						 	

	outSockId = Create( GetType(inSockId) );
	NV_ASSERT( outSockId >= 0 );
	sockA[outSockId].hdl 	= newSock;
	sockA[outSockId].state 	= S_CONNECTED;
	return TRUE;
#endif
}


bool
nv::net::Connect	(	int			inSockId,
						pcstr		inAddr,
						uint16		inPort		)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	if( GetState(inSockId)!=S_CLOSED )
		return FALSE;

	int sceSockId = sceNetInetSocket( SCE_NET_INET_AF_INET, SCE_NET_INET_SOCK_STREAM, 0 );
	if( sceSockId <= 0 )
		return FALSE;

	struct SceNetInetSockaddrIn sin;
	Zero( sin );
	sin.sin_len 			= sizeof(sin);				
	sin.sin_family 			= SCE_NET_INET_AF_INET;
	sin.sin_addr.s_addr		= sceNetInetInetAddr(inAddr);
	sin.sin_port 			= sceNetHtons(inPort);
	if( sin.sin_addr.s_addr == SCE_NET_INET_INADDR_NONE )
		return FALSE;

	// non-blocking mode
	int	opt_nbio = 1;
	sceNetInetSetsockopt( sceSockId, SCE_NET_INET_SOL_SOCKET, SCE_NET_INET_SO_NBIO, &opt_nbio, sizeof(opt_nbio) );						 	

	int ret = sceNetInetConnect( sceSockId, (struct SceNetInetSockaddr*)&sin, sizeof(sin) );
	if( ret==0 || sceNetInetGetErrno()==EINPROGRESS ) {
		sockA[inSockId].hdl   = sceSockId;
		sockA[inSockId].state = S_CONNECTING;
		return TRUE;
	}

	return FALSE;
#endif
}


bool
nv::net::ConnectWait	(	int					inSockId,
							uint				inMsTimeout,
							pcstr				inAddr,
							uint16				inPort		)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	if( !Connect(inSockId,inAddr,inPort) )
		return FALSE;
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	for( ;; ) {
		if( GetState(inSockId) != S_CONNECTING )
			break;
		clock::GetTime( &t1 );
		if( (t1-t0)*1000.f > msTimeout ) {
			Close( inSockId );
			break;
		}
		sceKernelDelayThread( 1000 );		// 1ms
	}
	return ( GetState(inSockId) == S_CONNECTED );
#endif
}


bool
nv::net::Receive	(	int					inSockId,
						uint32		&		outRcvSize,
						pvoid				inBuff,
						uint32				inSize,
						bool				peek		)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	outRcvSize = 0;

	if( !IsConnected(inSockId) )	return FALSE;
	if( !inSize )					return TRUE;
	if( !inBuff )					return FALSE;

	int rcvBSize = sceNetInetRecv( sockA[inSockId].hdl, inBuff, inSize, SCE_NET_INET_MSG_DONTWAIT | (peek ? SCE_NET_INET_MSG_PEEK : 0) );

	if( rcvBSize > 0 ) {
		outRcvSize = rcvBSize;
		return TRUE;
	}

	int err = sceNetInetGetErrno();
	return (err == EAGAIN) ? TRUE : FALSE;
#endif
}


bool
nv::net::Send		(	int					inSockId,
						uint32		&		outSndSize,
						pvoid				inBuff,
						uint32				inSize			)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	outSndSize = 0;

	if( !IsConnected(inSockId) )		return FALSE;
	if( !inSize )						return TRUE;
	if( !inBuff )						return FALSE;

	int sndBSize = sceNetInetSend( sockA[inSockId].hdl, inBuff, inSize, SCE_NET_INET_MSG_DONTWAIT );

	if( sndBSize > 0 ) {
		outSndSize = sndBSize;
		return TRUE;
	}

	int err = sceNetInetGetErrno();
	return (err == EAGAIN) ? TRUE : FALSE;
#endif
}


bool
nv::net::ReceiveWait	(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize		)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	uint8* buffer   = (uint8*) inBuffer;
	while( inBSize ) {
		uint32 rbs = 0;
		if( !Receive(inSockId,rbs,buffer,inBSize) )
			return FALSE;
		if( rbs==0 ) {
			clock::GetTime( &t1 );
			if( (t1-t0)*1000.f > msTimeout )
				return FALSE;
			sceKernelDelayThread( 1000 );		// 1ms
		} else {
			buffer   += rbs;
			inBSize  -= rbs;
			clock::GetTime( &t0 );
		}
	}
	return ( GetState(inSockId) == S_CONNECTED );
#endif
}


bool
nv::net::SendWait		(	int				inSockId,
							uint			inMsTimeout,
							pvoid			inBuffer,
							uint32			inBSize			)
{
#ifdef NO_NETWORK
	return FALSE;
#else	
	clock::Time t0, t1;
	clock::GetTime( &t0 );
	float msTimeout = inMsTimeout ? float(int(inMsTimeout)) : DEF_WAIT_TIMEOUT;
	uint8* buffer   = (uint8*) inBuffer;
	while( inBSize ) {
		uint32 sbs = 0;
		if( !Send(inSockId,sbs,buffer,inBSize) )
			return FALSE;
		if( sbs==0 ) {
			clock::GetTime( &t1 );
			if( (t1-t0)*1000.f > msTimeout )
				return FALSE;
			sceKernelDelayThread( 1000 );		// 1ms
		} else {
			buffer   += sbs;
			inBSize  -= sbs;
			clock::GetTime( &t0 );
		}
	}
	return ( GetState(inSockId) == S_CONNECTED );
#endif
}


bool
nv::net::GetPeerAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort		)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	if( !IsConnected(inSockId) )		return FALSE;

	struct SceNetInetSockaddrIn sin;
	SceNetInetSocklen_t sockaddr_len;
	static char buff[SCE_UTILITY_NET_PARAM_IPV4_ADDR_STR_LEN];

	if(sceNetInetGetpeername(sockA[inSockId].hdl, (struct SceNetInetSockaddr *)&sin, &sockaddr_len) < 0)
		return FALSE;

	sceNetInetInetNtop(SCE_NET_INET_AF_INET, &sin.sin_addr,buff, sizeof(buff));
	if( outPort )	*outPort = sceNetNtohs(sin.sin_port);
	if( outAddr )	*outAddr = buff;

	return TRUE;
#endif
}


bool
nv::net::GetSockAddr	(	int					inSockId,
							pstr		*		outAddr,
							uint16		*		outPort			)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	if( GetState(inSockId) < S_OPENED )
		return FALSE;

	struct SceNetInetSockaddrIn sin;
	SceNetInetSocklen_t sockaddr_len;
	static char buff[SCE_UTILITY_NET_PARAM_IPV4_ADDR_STR_LEN];

	if(sceNetInetGetsockname(sockA[inSockId].hdl, (struct SceNetInetSockaddr *)&sin, &sockaddr_len) < 0)
		return FALSE;

	sceNetInetInetNtop(SCE_NET_INET_AF_INET, &sin.sin_addr,buff, sizeof(buff));
	if( outAddr )	*outAddr = buff;
	if( outPort )	*outPort = sceNetNtohs(sin.sin_port);

	return TRUE;
#endif
}


bool
nv::net::IsRcvPending	(	int			inSockId,
							uint		inBSize		)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	if( !IsConnected(inSockId) )		return FALSE;

	if( inBSize > 1024 )
		return FALSE;
	char* buff = (char*) alloca( inBSize+16 );

	int ret = sceNetInetRecv( sockA[inSockId].hdl, buff, inBSize, SCE_NET_INET_MSG_PEEK|SCE_NET_INET_MSG_DONTWAIT );
	return ( ret>0 && uint(ret)==inBSize );
#endif
}


bool
nv::net::IsClientPending		(	int			inSockId		)
{
#ifdef NO_NETWORK
	return FALSE;
#else
	if( GetState(inSockId) != S_LISTENING )
		return FALSE;

	SceNetInetFdSet	  readSet;
	Zero( readSet );
	SceNetInetFD_SET( sockA[inSockId].hdl, &readSet	);

	SceNetInetTimeval timeout;
	Zero( timeout );

	if( sceNetInetSelect(sockA[inSockId].hdl+1, &readSet, NULL, NULL, &timeout) <= 0 )
		return FALSE;
	else if(SceNetInetFD_ISSET(sockA[inSockId].hdl, &readSet))
		return TRUE;

	return FALSE;
#endif
}


int32
nv::net::GetMTUBSize	(	int			inSockId		)
{
	return 0;
}


nv::net::State
nv::net::GetState		(	int			inSockId		)
{
#ifdef NO_NETWORK
	return S_INVALID;
#else
	if( inSockId<0 || inSockId>=int(sockA.size()) )
		return S_INVALID;

	if( sockA[inSockId].state == S_CONNECTING )
	{
		SceNetInetFdSet 	 successSet;
		Zero( successSet );
		SceNetInetFD_SET( sockA[inSockId].hdl, &successSet );

		SceNetInetFdSet 	 failureSet;
		Zero( failureSet );
		SceNetInetFD_SET( sockA[inSockId].hdl, &failureSet );

		SceNetInetTimeval timeout;
		Zero( timeout );

		if (sceNetInetSelect(sockA[inSockId].hdl+1, NULL, &successSet, &failureSet, &timeout) < 0)
		{
			int err = sceNetInetGetErrno();
			if( err != EINVAL && err != EINTR ) {
				Release( inSockId );
				return S_INVALID;
			}
		}
		else if (SceNetInetFD_ISSET(sockA[inSockId].hdl, &successSet))
		{
			NV_ASSERT(!SceNetInetFD_ISSET(sockA[inSockId].hdl, &failureSet));
			sockA[inSockId].state = S_CONNECTED;
			return S_CONNECTED;
		}
	}
	else if( sockA[inSockId].state == S_CONNECTED )
	{
		// Check the connection validity
		char buff[2];
		int ret = sceNetInetRecv( sockA[inSockId].hdl, buff, 2, SCE_NET_INET_MSG_PEEK|SCE_NET_INET_MSG_DONTWAIT );
		if( ret <= 0 ) {
			int err = sceNetInetGetErrno();
			if( err == EBADF ) {
				Release( inSockId );
				return S_INVALID;
			}
			if( err!=EAGAIN && err!=EINTR ) {
				sockA[inSockId].state = S_CLOSED;
				return S_CLOSED;
			}
		}
	}
	
	return sockA[inSockId].state;
#endif
}


nv::net::Type
nv::net::GetType		(	int			inSockId		)
{
#ifdef NO_NETWORK
	return T_UNDEF;
#else
	if( GetState(inSockId) == S_INVALID )	return T_UNDEF;
	else									return sockA[inSockId].type;
#endif
}


