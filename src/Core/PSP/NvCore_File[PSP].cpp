/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <kernel.h>
#include <stdio.h>
using namespace nv;



//#define	DBG_SYNC_READ




namespace
{

	enum State {
		IDLE 		= 		0,
		SEEKING		=		1,
		READING		=		2
	};

	SceUID 		fileID;

	// used for async seek/read
	State		async_state;
	bool 		async_success;
	pvoid		async_bufferPtr;
	SceSSize	async_bsize;
	SceOff		async_boffset;

	char* subDev[] = {
		// name					prefix				suffix
		"Devkit",				"host0:",			NULL,
		"Devkit (cwd)",			"host0:.\\",		NULL,
		"UMD",					"disc0:",			NULL,
		"MS",					"ms0:\\",			NULL,
		"TTY",					"tty0:",			NULL,
	};

}



bool
nvcore_File_io_Init()
{
	fileID 	= -1;
	async_state	= IDLE;
	async_success = TRUE;
	return TRUE;
}


bool
nvcore_File_io_IsOpened		(		)
{
	return ( fileID >= 0 ) ;
}


bool
nvcore_File_io_IsReady		(		)
{
	// no file opened
	if( !nvcore_File_io_IsOpened() )
		return TRUE;

	// idle ?
	if( async_state == IDLE )
		return TRUE;

	// seeking ?
	if( async_state == SEEKING ) {
		SceIores iores;
		int res = sceIoPollAsync( fileID, &iores );
		if( res == 1 )	return FALSE;
		async_state = IDLE;
		SceOff sres = (res==SCE_KERNEL_ERROR_OK)
					? static_cast<SceOff>(iores)
					: static_cast<SceOff>(res);
		if( sres != async_boffset ) {
			DebugPrintf( "sceIoPollAsync() error on seeking %d %x %x !\n", res, uint(sres), uint(async_boffset) );
			async_success = FALSE;
			return TRUE;
		}

		// now read
		res = sceIoReadAsync( fileID, async_bufferPtr, async_bsize );
		if( res != SCE_KERNEL_ERROR_OK   ) {
			DebugPrintf( "sceIoReadAsync() error !\n" );
			async_success = FALSE;
			return TRUE;
		}

		// continue ...
		async_state = READING;
		return FALSE;
	}

	// reading ?
	if( async_state == READING ) {
		SceIores iores;
		int res = sceIoPollAsync( fileID, &iores );
		if( res == 1 )	return FALSE;
		async_state = IDLE;
		SceSSize sres = (res==SCE_KERNEL_ERROR_OK)
					  ? static_cast<SceSSize>(iores)
					  : static_cast<SceSSize>(res);
		if( sres != async_bsize ) {
			DebugPrintf( "sceIoPollAsync() error on reading %d %x %x !\n", res, uint(sres), uint(async_bsize) );
			async_success = FALSE;
			return TRUE;
		}

		async_success = TRUE;
		return TRUE;
	}
	
	return TRUE;
}



bool
nvcore_File_io_IsSuccess	(		)
{
	if( !nvcore_File_io_IsOpened() )
		return FALSE;
	return async_success;
}


void
nvcore_File_io_Sync	(	)
{
	SceIores iores;
	while( !nvcore_File_io_IsReady() )
		sceIoWaitAsync( fileID, &iores );
}


nv::file::Status
nvcore_File_io_GetStatus	(	)
{
	if( async_state != IDLE )
		return nv::file::FS_BUSY;
	else
		return nv::file::FS_OK;
}


bool
nvcore_File_io_Open	(	pcstr	inFilename,	 uint32&	outBSize	)
{
	if( !inFilename || Strlen(inFilename)==0 )
		return FALSE;

	if( nvcore_File_io_IsOpened() )
		return FALSE;

	// safe sync
	nvcore_File_io_Sync();
	async_state = IDLE;

	fileID = sceIoOpen(	inFilename, SCE_O_RDONLY, 0 );
	if ( fileID < 0 )
		return FALSE;

	outBSize = sceIoLseek( fileID, 0, SCE_SEEK_END ) ;
	if ( outBSize <= 0 ) {
		sceIoClose( fileID ) ;
		return FALSE ;
	}

	sceIoLseek( fileID, 0, SCE_SEEK_SET ) ;
//	DebugPrintf( "<Neova> file <%s> opened, type:%xh\n", inFilename, sceIoGetDevType(fileID) );
	return TRUE;
}


bool
nvcore_File_io_Read		(	uint		inSysFlags,
							pvoid		inBufferPtr,
							uint32		inBSize,
							uint32		inBOffset0,
							uint32		inBOffset1,
							uint32		inBOffset2,
							uint32		inBOffset3	)
{
	// Not in use or invalid call !
	NV_ASSERT( (inBSize&0xF) == 0 );
	if(		!nvcore_File_io_IsOpened()
		||	!inBufferPtr
		||	(uint(inBufferPtr)&0x3)!=0		// must be 4-bytes aligned !
		||	(inBSize&0xF)!=0	)			// must be 16-bytes aligned !
		return FALSE;

	if( inBSize == 0 )
		return TRUE;

	// safe sync
	nvcore_File_io_Sync();

	// async parameters
	async_bufferPtr  = inBufferPtr;
	async_boffset	 = inBOffset0;
	async_bsize 	 = inBSize;

#ifdef DBG_SYNC_READ
	SceOff soff = sceIoLseek( fileID, async_boffset, SCE_SEEK_SET );
	if( soff != async_boffset ) {
		DebugPrintf( "sceIoLseek() error %x %x !\n", uint(soff), async_boffset );
		return FALSE;
	}
	SceSSize siz = sceIoRead( fileID, async_bufferPtr, async_bsize );
	if( siz != async_bsize ) {
		DebugPrintf( "sceIoRead() error %x %x !\n", uint(siz), async_bsize );
		return FALSE;
	}
	return TRUE;

#else
	// seek
	int res = sceIoLseekAsync( fileID, async_boffset, SCE_SEEK_SET );
	if( res < 0 ) {
		DebugPrintf( "sceIoLseekAsync() error %d!\n", res );
		return FALSE;
	}
	async_state = SEEKING;
	return TRUE;
#endif
}


bool
nvcore_File_io_DumpFrom	(	pcstr	inFilename,
							pvoid&	outBuffer,
							uint&	outBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;
	int fd = sceIoOpen( inFilename, SCE_O_RDONLY , 0 );
	if( fd < 0 )
		return FALSE;
	outBSize = sceIoLseek( fd, 0, SCE_SEEK_END ) ;
	sceIoLseek( fd, 0, SCE_SEEK_SET );
	outBuffer = NvMallocA( outBSize, 64 );
	if( !outBuffer ) {
		sceIoClose( fd );
		return FALSE;
	}
	uint readen = sceIoRead( fd, outBuffer, outBSize );
	if( readen != outBSize ) {
		NvFree( outBuffer );
		sceIoClose( fd );
		return FALSE;
	}
	sceIoClose( fd );
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_DumpTo	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;
	if( !inBuffer )		return FALSE;
	if ( (uint32(inBuffer) & 3) != 0 ){
		NV_WARNING("Data is not 4-byte-aligned, writing may become slower due to system restrictions !");
	}
	int fd = sceIoOpen( inFilename, SCE_O_CREAT|SCE_O_TRUNC|SCE_O_WRONLY , 666 );
	if( fd < 0 )
		return FALSE;
	sceIoWrite( fd, inBuffer, inBSize );
	sceIoClose( fd );
	return TRUE;
#else
	return FALSE;
#endif

}


bool
nvcore_File_io_Append	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )	return FALSE;
	int fd = sceIoOpen( inFilename, SCE_O_APPEND|SCE_O_WRONLY, 0 );
	if( fd < 0 )
		return FALSE;
	sceIoLseek( fd, 0, SCE_SEEK_END ) ;
	sceIoWrite( fd, inBuffer, inBSize );
	sceIoClose( fd );
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_Close	(			)
{
	if( nvcore_File_io_IsOpened() ) {
		nvcore_File_io_Sync();
		sceIoClose(fileID);
		fileID = -1;
	}
	return TRUE;
}


void
nvcore_File_io_Shut		(			)
{
	nvcore_File_io_Close();
}



uint
nvcore_File_io_GetNbSubDevice	(		)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	return nb_subdev;
}


pcstr
nvcore_File_io_GetSubDeviceName		(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+0];
}


pcstr
nvcore_File_io_GetSubDevicePrefix	(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+1];
}


pcstr
nvcore_File_io_GetSubDeviceSuffix	(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+2];
}


