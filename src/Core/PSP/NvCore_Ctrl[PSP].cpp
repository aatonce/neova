/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <ctrlsvc.h>
using namespace nv;


namespace
{

	bool				opened;
	nv::ctrl::Status	ctrlStat;
	float				deadzA[ ctrl::TP_MAX ][ ctrl::BT_MAX ][ 2 ];	// lo/hi


#if defined(_ATMON)

	int 				sockServerID;
	int 				sockClientID;
	bool				sockDataPending;
	uint32				sockDataRcvCpt;
	bool				sockConnected;

	void	net_ctrl_Init	()
	{
		// Open network server
		sockConnected	 = FALSE;
		sockServerID	 = -1;
		sockClientID	 = -1;
		sockDataPending	 = FALSE;
		sockDataRcvCpt   = 0;
	
		char addr[16];
		if( net::GetHostAddr("localhost",0,addr)) {		
			sockServerID = net::Create(nv::net::T_TCP);
			if( sockServerID >= 0 ) {
				net::Open(sockServerID,5555,addr);
				net::Listen(sockServerID,2);
			}
		}
	}

	void	net_ctrl_Shut	()
	{
		net::Release(sockClientID);
		net::Release(sockServerID);	
		sockClientID = -1;
		sockServerID = -1;	
	}

	bool	net_ctrl_Connected()
	{
		return sockConnected;
	}

	bool	net_ctrl_Available()
	{
		return sockDataRcvCpt;
	}

	bool 	net_ctrl_Update()
	{
		if( sockServerID < 0 )		return FALSE;

		if( net::IsClientPending(sockServerID) )
		{
			if( sockClientID < 0 )
			{
				if( !net::Accept(sockServerID, sockClientID) )
					return FALSE;
			}
			else
			{
				int sockTmp;	
				net::Accept(sockServerID, sockTmp);
				net::Close(sockTmp);
				net::Release(sockTmp);
			}			
		}

		if( sockClientID < 0 )
			return FALSE;

		net::State cliStt = net::GetState( sockClientID );
		if( cliStt != nv::net::S_CONNECTED ) {
			net::Release(sockClientID);
			sockClientID = -1 ;
			sockDataPending = FALSE;
			sockConnected = FALSE;
			return FALSE;
		}

		sockConnected = TRUE;

		if( sockDataPending ) {
			if( net::IsRcvPending(sockClientID,sizeof(ctrlStat.button))) {
				uint32 dataSizeRcv;
				net::Receive(sockClientID,dataSizeRcv,(pvoid)ctrlStat.button,sizeof(ctrlStat.button));
				NV_ASSERT( dataSizeRcv == sizeof(ctrlStat.button) );
				sockDataPending = FALSE;
				sockDataRcvCpt++;
			}
		}

		if( !sockDataPending ) {
			static const char query[]={"update\n"};
			sockDataPending = net::SendWait( sockClientID, 0, (void*)query, 7 );
		}

		return TRUE;				
	}

#else

	#define	net_ctrl_Init()			{					}
	#define	net_ctrl_Shut()			{					}
	#define	net_ctrl_Connected()	FALSE
	#define	net_ctrl_Available()	FALSE
	#define	net_ctrl_Update()		FALSE

#endif

}




bool
nvcore_Ctrl_Init()
{
	sceCtrlSetSamplingMode( SCE_CTRL_MODE_DIGITALANALOG );
	sceCtrlSetSamplingCycle( 0 );
	net_ctrl_Init();

	for( int i = 0 ; i < ctrl::TP_MAX ; i++ ) {
		for( int j = 0 ; j < ctrl::BT_MAX ; j++ ) {
			if( i==ctrl::TP_PADDLE ) {
				deadzA[i][j][0] = 0.3f;
				deadzA[i][j][1] = 1.0f;
			} else {
				deadzA[i][j][0] = 0.f;
				deadzA[i][j][1] = 0.f;
			}
		}
	}

	opened = FALSE;
	return TRUE;
}


void
nvcore_Ctrl_Shut()
{
	nv::ctrl::Close();
	net_ctrl_Shut();
}


void
nv::ctrl::Open(		)
{
	opened = TRUE;
}


void
nv::ctrl::Close()
{
	opened = FALSE;
}


uint
nv::ctrl::GetNbMax	(		)
{
	return 1;
}


void
nvcore_Ctrl_Update()
{
	if( !opened )		return;

	if( net_ctrl_Update() )
		return;

	SceCtrlData bufCtrl;
	if( sceCtrlPeekBufferPositive(&bufCtrl,1) <= 0 )
		return;

	Zero( ctrlStat );

	const float oo128 = 1.f / 128.f;
	const float oo127 = 1.f / 127.f;
	float dirx  = float(int(bufCtrl.Lx)) - 128.f;
		  dirx  = (dirx > 0) ? dirx*oo127 : dirx*oo128;
	float diry  = float(int(bufCtrl.Ly)) - 128.f;
		  diry  = (diry > 0) ? diry*oo127 : diry*oo128;
	float L     = (bufCtrl.Buttons & SCE_CTRL_L     ) ? 1.0F : 0.0F;
	float R     = (bufCtrl.Buttons & SCE_CTRL_R     ) ? 1.0F : 0.0F;
	float CROSS = (bufCtrl.Buttons & SCE_CTRL_CROSS ) ? 1.0F : 0.0F;

	ctrlStat.button[ nv::ctrl::BT_DIR1X ] 		= dirx;
	ctrlStat.button[ nv::ctrl::BT_DIR1Y ] 		= diry;
	ctrlStat.button[ nv::ctrl::BT_DIR2X ] 		= 0;
	ctrlStat.button[ nv::ctrl::BT_DIR2Y ] 		= 0;
	ctrlStat.button[ nv::ctrl::BT_DIR3right ] 	= (bufCtrl.Buttons & SCE_CTRL_RIGHT 		) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_DIR3left ] 	= (bufCtrl.Buttons & SCE_CTRL_LEFT 			) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_DIR3up ] 		= (bufCtrl.Buttons & SCE_CTRL_UP 			) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_DIR3down ] 	= (bufCtrl.Buttons & SCE_CTRL_DOWN 			) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_BUTTON0 ] 	= (bufCtrl.Buttons & SCE_CTRL_TRIANGLE 		) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_BUTTON1 ] 	= (bufCtrl.Buttons & SCE_CTRL_CIRCLE		) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_BUTTON2 ] 	= CROSS;
	ctrlStat.button[ nv::ctrl::BT_BUTTON3 ] 	= (bufCtrl.Buttons & SCE_CTRL_SQUARE 		) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_BUTTON4 ] 	= L;
	ctrlStat.button[ nv::ctrl::BT_BUTTON5 ] 	= R;
	ctrlStat.button[ nv::ctrl::BT_BUTTON6 ] 	= (CROSS>0.8f) ? L : 0.f;
	ctrlStat.button[ nv::ctrl::BT_BUTTON7 ] 	= (CROSS>0.8f) ? R : 0.f;
	ctrlStat.button[ nv::ctrl::BT_BUTTON8 ] 	= (bufCtrl.Buttons & SCE_CTRL_SELECT 		) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_BUTTON9 ] 	= (bufCtrl.Buttons & SCE_CTRL_START 		) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_BUTTON14 ] 	= (bufCtrl.Buttons & SCE_CTRL_INTERCEPTED	) ? 1.0F : 0.0F;
	ctrlStat.button[ nv::ctrl::BT_BUTTON15 ] 	= (bufCtrl.Buttons & SCE_CTRL_HOLD			) ? 1.0F : 0.0F;
	ctrlStat.type = nv::ctrl::TP_PADDLE;
}



nv::ctrl::Status*
nv::ctrl::GetStatus(	uint	inCtrlNo		)
{
	if( !opened )		return NULL;
	if( inCtrlNo!=0 )	return NULL;

	// remote ctrl ?
	if( net_ctrl_Connected() )
		return net_ctrl_Available() ? &ctrlStat : NULL;

	// onboard ctrl
	return &ctrlStat;
}


bool
nv::ctrl::SetActuators	(	uint		inCtrlNo,
							float		inLowSpeed,
							float		inHighSpeed	)
{
	return FALSE;
}


bool
nv::ctrl::GetDefDeadZone	(		Type			inType,
									Button			inButton,
									float*			outLo,
									float*			outHi			)
{
	int t = int(inType);
	int b = int(inButton);
	if( t<0 || t>=TP_MAX )	return FALSE;
	if( b<0 || b>=BT_MAX )	return FALSE;
	if( outLo )		*outLo = deadzA[t][b][0];
	if( outHi )		*outHi = deadzA[t][b][1];
	return TRUE;
}


bool
nv::ctrl::SetDefDeadZone	(		Type			inType,
									Button			inButton,
									float			inLo,
									float			inHi			)
{
	float lo, hi;
	if( !GetDefDeadZone(inType,inButton,&lo,&hi) )
		return FALSE;
	if( inLo >= 0.f )	lo = Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return FALSE;
	int t = int(inType);
	int b = int(inButton);
	deadzA[t][b][0] = lo;
	deadzA[t][b][1] = hi;
	return TRUE;
}


float
nv::ctrl::Status::Filtered	(		Button			inButton,
									float			inLo,
									float			inHi	)
{
	if( int(inButton)<0 || int(inButton)>=BT_MAX )
		return 0.f;

	float lo, hi;
	if( !GetDefDeadZone(type,inButton,&lo,&hi) )
		return 0.f;

	if( inLo >= 0.f )	lo = Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return 0.f;

	float b  = Clamp( button[int(inButton)], -1.f, +1.f );
	float ab = Absf( b );
	float sb = ( b>=0.f ? 1.f : -1.f );

	if( ab <= lo )	return 0.f;
	if( ab >= hi )	return sb;
	return sb * (ab-lo)/(hi-lo);
}





