/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>



//
// Stack backtracing used some predicted heuritics.
//
// The compiler must respect the ABI rules etablished in "SYSTEM V ABI Mips RISC processor" :
//
//		Standard Called Function rules
//
//		By convention, there is a set of rules that must be followed by every function that
//		allocates a stack frame. Following this set of rules ensures that, given an arbitrary
//		program counter, return address register $31, and stack pointer, there is a deterministic
//		way of performing stack backtracing. These rules also make possible programs
//		that translate already compiled absolute code into position-independent
//		code. See Coding Examples in this chapter.
//
//		Within a function that allocates a stack frame, the following rules must be observed:
//		*	In position-independent code that calculates a new value for the gp register,
//			the calculation must occur in the first three instructions of the function.
//			One possible optimization is the total elimination of this calculation; a local
//			function called from within a position-independent module guarantees
//			that the context pointer gp already points to the global offset table.
//			The calculation must occur in the first basic block of the function.
//
//		*	The stack pointer must be adjusted to allocate the stack frame before any
//			other use of the stack pointer register.
//
//		*	At most, one frame pointer can be used in the function. Use of a frame
//			pointer is identified if the stack pointer value is moved into another register,
//			after the stack pointer has been adjusted to allocate the stack frame.
//			This use of a frame pointer must occur within the first basic block of the
//			function before any branch or jump instructions, or in the delay slot of the
//			first branch or jump instruction in the function.
//
//		*	There is only one exit from a function that contains a stack adjustment: a
//			jump register instruction that transfers control to the location in the return
//			address register $31. This instruction, including the contents of its branch
//			delay slot, mark the end of function.
//
//		*	The deallocation of the stack frame, which is done by adjusting the stack
//			pointer value, must occur once and in the last basic block of the function.
//			The last basic block of a function includes all of the non control-transfer instructions
//			immediately prior to the function exit, including the branch delay
//			slot.
//		
// More efficient predicted heuristics can be found in the gdb source code, mips-tdep.c file.
//
// alloca() is not compliant with the ABI defined rules !
//




#if defined( _NVCOMP_ENABLE_DBG )



#if defined(__MWERKS__)
#pragma dont_inline on
#endif

namespace
{

	// Looks backward at the function entry prolog for stack adjustment offset

	pvoid	Find_SP_OpCode(	pvoid		inPC,
							uint32	&	outBSize	)
	{
		NV_ASSERT_A32( inPC );

		uint32 backBSize = 0;
		nv::core::GetParameter(	nv::core::PR_ALG_BACKSTACK_BSIZE, &backBSize );
		if( backBSize == 0 )
			backBSize = 16*1024;

		uint32 * sp_limit = (uint32*) (uint32(inPC) - backBSize);
		uint32 * sp_paddr = (uint32*) inPC;
		for( ;; )
		{
			sp_paddr--;
			if( sp_paddr <= sp_limit )
				return NULL;			// not found !

			uint16 hi = sp_paddr[0] >> 16;
			uint16 lo = sp_paddr[0] & 0xFFFF;
			if( lo & 0x8000 )
			{
				if(		hi == 0x27BD		// addiu	sp,sp,-N
					||	hi == 0x67BD		// daddiu	sp,sp,-N
					||	hi == 0x23BD		// addi		sp,sp,-N
					||	hi == 0x63BD	)	// daddi	sp,sp,-N
				{
					outBSize = 65536 - uint(lo);
					if(	outBSize==0 || (outBSize&3) )	return NULL;
					else								return sp_paddr;
				}
			}
		}
	}


	// Looks forward in range for ra offset in stack

	pvoid	Find_RA_OpCode(	pvoid		inPC0,
							pvoid		inPC1,
							uint32	&	outOfst	)
	{
		NV_ASSERT_A32( inPC0 );
		NV_ASSERT_A32( inPC1 );

		uint32 * ra_paddr = (uint32*) inPC0;
		uint32 * ra_limit = (uint32*) inPC1;
		for( ;; )
		{
			if( ra_paddr >= ra_limit )
				return NULL;			// not found !

			uint16 hi = ra_paddr[0] >> 16;
			if(		hi == 0xAFBF		// sw	ra,N(sp)
				||	hi == 0xFFBF )		// sd	ra,N(sp)
			{
				outOfst = ra_paddr[0]  & 0xFFFF;
				if(	(outOfst&3)!=0 )
					return NULL;
				else
					return ra_paddr;
			}

			ra_paddr++;
		}
	}

}



bool
nv::stack::GetFrame( Frame & outFS )
{
	Frame fs;

	// Get ptr (SP) & retAddr (RA) from registers
	NV_COMPILE_TIME_ASSERT( MOFFSET(nv::stack::Frame,ptr)     == 0 );
	NV_COMPILE_TIME_ASSERT( MOFFSET(nv::stack::Frame,retAddr) == 8 );
	asm __volatile__ (
	"	sw $29, 0(%0)\n"
	"	sw $31, 8(%0)\n"
	:: "r"(&fs) );

	// Local-addr
	fs.locAddr = GetLocalAddr();

	// Frame bsize using local forward parsing
	if( !Find_SP_OpCode(fs.locAddr,fs.bsize) )
		return FALSE;

	// Returns the caller frame !
	return GetBackFrame( outFS, fs );
}


bool
nv::stack::GetBackFrame(	Frame &	outFS,
							Frame &	inFS	)
{
	if(		!inFS.retAddr
	 	||	!inFS.locAddr
	 	||	!inFS.ptr
	 	||	!inFS.bsize	)
		return FALSE;

	// this is top frame ?
	uint32 sp_top;
	core::GetParameter( core::PR_MEM_APP_STACK_ADDR, &sp_top );
	if(	uint(inFS.ptr) >= sp_top )
		return FALSE;

	// caller op-codes parsing to find sp adjustment
	uint32 caller_frame_bsize;
	pvoid caller_sp_opc = Find_SP_OpCode( inFS.retAddr, caller_frame_bsize );
	if(	!caller_sp_opc )
		return FALSE;

	// caller op-codes parsing to find the ra save offset in the stack-frame.
	// save occurs in range [caller_sp_opc, inFS->retAddr] (ABI rules !)
	uint32 caller_ra_ofst;
	pvoid caller_ra_opc = Find_RA_OpCode( caller_sp_opc, inFS.retAddr, caller_ra_ofst );
	if(	!caller_ra_opc || caller_ra_ofst>(caller_frame_bsize-4) )
		return FALSE;

	// caller stack frame
	uint32 * caller_sp = (uint32*) (uint32(inFS.ptr) + inFS.bsize);
	NV_ASSERT_A32( caller_sp );
	if( uint32(caller_sp)&3 )
		return FALSE;

	// Build the back frame
	outFS.ptr     = (pvoid) caller_sp;
	outFS.bsize   = caller_frame_bsize;
	outFS.locAddr = inFS.retAddr;
	outFS.retAddr = (pvoid) caller_sp[ caller_ra_ofst>>2 ];

	return TRUE;
}


pvoid
nv::stack::GetReturnAddr	(			)
{
	Frame fs, caller_fs;

	if( !GetFrame(fs) )
		return NULL;

	if( !GetBackFrame(caller_fs,fs) )
		return NULL;

	return caller_fs.retAddr;
}


pvoid
nv::stack::GetLocalAddr		(						)
{
	register pvoid res;
	asm volatile( "add %0, $0, $31" : "=r"(res) );
	return res;
}


uint
nv::stack::OutputCallstack	(	Frame&		inFromF,
								pcstr		inLabel		)
{
	// Output call-stack
	uint32 appStartAddr;
	core::GetParameter( core::PR_MEM_APP_ADDR, &appStartAddr );

	Printf( "<Neova> call-stack :\n" );
	Frame sf    = inFromF;
	uint  depth = 0;
	for( ;; ) {
		Printf( "%s[%d] %08X .t=%08X sp=%08X\n",
			inLabel ? inLabel : "",
			depth++,
			uint32(sf.locAddr),
			uint32(sf.locAddr)-appStartAddr,
			uint32(sf.ptr)	);
		if( !GetBackFrame(sf,sf) )
			break;
	}

	return depth;
}




#else	// _NVCOMP_ENABLE_DBG





bool
nv::stack::GetFrame		(	Frame	&	outF		)
{
	return FALSE;
}

bool
nv::stack::GetBackFrame	(	Frame	&	outBackF,
							Frame	&	inF			)
{
	return FALSE;
}

pvoid
nv::stack::GetReturnAddr	(		)
{
	return NULL;
}

pvoid
nv::stack::GetLocalAddr		(		)
{
	return NULL;
}

uint
nv::stack::OutputCallstack	(	Frame&		inFromF,
								pcstr		inLabel		)
{
	return 0;
}


#endif // _NVCOMP_ENABLE_DBG



