/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
using namespace nv;


#define	NB_PORT			2
#define NB_SLOT			4
#define	MAX_DIR			(18+2)		// TRC limits + "." + ".." !



namespace
{

	enum StateID
	{
		S_IDLE	= 0,

		SGS_SYNCING,

		SFM_SYNCING,

		SGF_SYNCING,

		SCR_MKDIR,
		SCR_CHECKING,
		SCR_OPENING,
		SCR_WRITING,
		SCR_CLOSING,

		SWR_OPENING,
		SWR_SEEKING,
		SWR_WRITING,
		SWR_CLOSING,

		SRD_OPENING,
		SRD_SEEKING,
		SRD_READING,
		SRD_CLOSING,

		SDR_SYNCING,
	};

	char				productCode[32+1];

	int					state;
	int					state_hd;
	uint32				state_params[8];
	nv::nvram::Result	state_res;
	char				state_name[32];

	int					firstnext_idx;
	int					firstnext_cpt;
	//sceMcTblGetDir		firstnext_dir[MAX_DIR];


	// Convert in local time

	inline uint fromBCD( uchar v )
	{
		uchar vh = v>>4;
		uchar vl = v&15;
		return vh*10+vl;
	}

	inline uint toBCD( uchar v )
	{
		uchar vh = v/10;
		uchar vl = v - vh*10;
		return (vh<<4)|(vl);
	}

	/*void Convert( nvram::DateTime * out, sceMcStDateTime * in )
	{
		
	}*/

}



bool
nvcore_NVR_Init()
{
	
	return TRUE;
}


void
nvcore_NVR_Shut()
{
	
}


//
// ASYNC ( sceMcSync(1,...) ) failed !!!!
//

/*#define	MC_SYNC()						\
	int res;							\
	if( sceMcSync(0,NULL,&res) == 0 )	\
		return;
*/


void
nvcore_NVR_Update()
{
	
}




bool
nv::nvram::SetProductCode	(	pcstr		inProductCode		)
{
	
	return TRUE;
}


pcstr
nv::nvram::GetProductCode	(									)
{
	return productCode;
}


uint
nv::nvram::GetNbMax			(									)
{
	return NB_PORT*NB_SLOT;
}


bool
nv::nvram::IsReady			(	Result	*		outLastResult	)
{
	return FALSE;
}


bool
nv::nvram::GetStatus		(	uint			inNVRNo,
								uint32	*		outFullBSize,
								uint32	*		outFreeBSize,
								bool	*		outSwitched		)
{
	return FALSE;
}


bool
nv::nvram::Format			(	uint			inNVRNo			)
{
	return FALSE;
}


bool
nv::nvram::GetFirstRecord	(	uint			inNVRNo,
								pstr			outName,
								uint32		*	outBSize	)
{
	return FALSE;
}


bool
nv::nvram::GetNextRecord	(	pstr			outName,
								uint32		*	outBSize	)
{
	return FALSE;
}


bool
nv::nvram::CreateRecord		(	uint			inNVRNo,
								pcstr			inName,
								uint32			inBSize,
								pvoid			inBuffer	)
{
	return FALSE;
}


bool
nv::nvram::WriteRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset	)
{
	return FALSE;
}


bool
nv::nvram::ReadRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset		)
{
	return FALSE;
}


bool
nv::nvram::DeleteRecord		(	uint			inNVRNo,
								pcstr			inName			)
{
	return FALSE;
}


