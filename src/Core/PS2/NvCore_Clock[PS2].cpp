/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <libcdvd.h>



namespace
{

	sceCdCLOCK				bootClock;
	volatile	uint32		lastCycCpt32;
	volatile	uint64		lastCycCpt64;
				uint64		lastCycCpt64I;
	float					timeI;

}



bool
nvcore_Clock_Init()
{
	int res = sceCdReadClock( &bootClock );
	NV_ASSERT( res == 1 );

	asm volatile ("mtc0 $0, $9; sync.p" );
	lastCycCpt32	= 0;
	lastCycCpt64	= 0;
	lastCycCpt64I	= 0;
	timeI			= 0;
	return TRUE;
}


void
nvcore_Clock_iUpdate()
{
	uint32 cycCpt32	=	GetCycleCpt();
	uint32 d32		=	cycCpt32 - lastCycCpt32;
	lastCycCpt32	=	cycCpt32;
	lastCycCpt64   +=	uint64(d32);
}


void
nvcore_Clock_Update()
{
	DI();		// prevents VSyncHandler conflit in nvcore_Clock_iUpdate() !
	nvcore_Clock_iUpdate();
	EI();
}


void
nvcore_Clock_Shut()
{
	//
}


void
nv::clock::GetTime(	Time *		outTime		)
{
	nvcore_Clock_Update();

	register const uint64 per64	= 294912000UL;
	uint64 d64 = lastCycCpt64 - lastCycCpt64I;
	while( d64 >= per64 ) {
		timeI			+=	1.0F;
		lastCycCpt64I	+=	per64;
		d64				-=	per64;
	}

	if( outTime ) {
		outTime->ipart	= timeI;
		outTime->fpart	= GetCycleCptTime( uint32(d64) );
	}
}



