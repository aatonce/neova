/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <sifdev.h>
#include <Kernel/PS2/IOP/IOP.h>
#include <Kernel/Common/NvkCore_File.h>
using namespace nv;



namespace
{

	bool	fileOpened;


	pcstr	ps2_FullnameHook(	pcstr		inFullname	)
	{
		if( !inFullname )		return NULL;
		if( !inFullname[0] )	return NULL;

		static char ps2path[ 256 ];
		Strcpy( ps2path, inFullname );

		char* sptr = ps2path;
		while( sptr ) {
			sptr = Strchr( sptr, '/' );
			if( sptr ) {
				*sptr = '\\';
				sptr++;
			}
		}

		// Upper case for cdvdrom
		if(	Strncmp("cdrom",ps2path,5)==0 )
			Strupr( ps2path+5 );

		return ps2path;
	}


	char* subDev[] = {
		// name				prefix				suffix
		"T10K",				"host0:",			NULL,
		"T10K (cwd)",		"host0:.\\",		NULL,
		"CDVD",				"cdrom0:\\",		";1",
		"PFS",				"pfs0:",			NULL,
		"HDD",				"hdd0:",			NULL,
	};


}




bool
nvcore_File_io_Init()
{
	fileOpened = FALSE;
	file::SetFullnameHook( ps2_FullnameHook );
	return TRUE;
}


bool
nvcore_File_io_IsOpened		(		)
{
	return fileOpened;
}


bool
nvcore_File_io_IsReady		(		)
{
	// Not in use !
	if( !nvcore_File_io_IsOpened() )
		return TRUE;
	return iop::IsLoadReady();
}


bool
nvcore_File_io_IsSuccess	(		)
{
	// Not in use !
	if( !nvcore_File_io_IsOpened() )
		return FALSE;
	return iop::IsLoadSuccess();
}


void
nvcore_File_io_Sync	(	)
{
	while( !nvcore_File_io_IsReady() )
		iop::ForceFlush();
}


nv::file::Status
nvcore_File_io_GetStatus	(	)
{
	if( iop::IsLoadReady() )
		return nv::file::FS_OK;
	else
		return nv::file::FS_BUSY;
}


bool
nvcore_File_io_Open	(	pcstr	inFilename,	 uint32	&	outBSize	)
{
	// In use !
	if( nvcore_File_io_IsOpened() )
		return FALSE;

	fileOpened = iop::OpenFile( inFilename, &outBSize );

	return fileOpened;
}



bool
nvcore_File_io_Read		(	uint		inSysFlags,
							pvoid		inBufferPtr,
							uint32		inBSize,
							uint32		inBOffset0,
							uint32		inBOffset1,
							uint32		inBOffset2,
							uint32		inBOffset3	)
{
	NV_ASSERT( (inBSize&0xF) == 0 );

	// Not in use or invalid call !
	if(		!nvcore_File_io_IsOpened()
		||	!inBufferPtr
		||	(inBSize&0xF)!=0	)		// bsize must be 16-bytes aligned !
		return FALSE;

	if( inBSize == 0 )
		return TRUE;

	// Prevent sync
	nvcore_File_io_Sync();

	bool done = iop::LoadData(	iop::TargetRam(inSysFlags),
								inBufferPtr ,
								inBSize,
								inBOffset0,
								inBOffset1,
								inBOffset2,
								inBOffset3		);

	return done;
}


bool
nvcore_File_io_DumpFrom	(	pcstr	inFilename,
							pvoid&	outBuffer,
							uint&	outBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )		return FALSE;
	int fd = sceOpen( inFilename, SCE_RDONLY, 0 );
	if( fd < 0 )
		return FALSE;
	outBSize = sceLseek( fd, 0, SCE_SEEK_END );
	sceLseek( fd, 0, SCE_SEEK_SET );
	outBuffer = NvMallocA( outBSize, 64 );
	if( !outBuffer ) {
		sceClose( fd );
		return FALSE;
	}
	FlushCache( WRITEBACK_DCACHE );
	int readen = sceRead( fd, outBuffer, outBSize );
	if( uint(readen) != outBSize ) {
		NvFree( outBuffer );
		sceClose( fd );
		return FALSE;
	}
	sceClose( fd );
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_DumpTo	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )		return FALSE;
	if( !inBuffer )			return FALSE;
	int fd = sceOpen( inFilename, SCE_CREAT|SCE_TRUNC|SCE_WRONLY|SCE_NOBUF );
	if( fd < 0 )
		return FALSE;
	FlushCache( WRITEBACK_DCACHE );
	sceWrite( fd, inBuffer, inBSize );
	sceClose( fd );
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_Append	(	pcstr	inFilename,
							pvoid	inBuffer,
							uint	inBSize		)
{
#if defined( _NVCOMP_DEVKIT )
	if( !inFilename )		return FALSE;
	int fd = sceOpen( inFilename, SCE_APPEND|SCE_WRONLY|SCE_NOBUF );
	if( fd < 0 )
		return FALSE;
	sceLseek( fd, 0, SCE_SEEK_END );
	FlushCache( WRITEBACK_DCACHE );
	sceWrite( fd, inBuffer, inBSize );
	sceClose( fd );
	return TRUE;
#else
	return FALSE;
#endif
}


bool
nvcore_File_io_Close	(			)
{
	if( nvcore_File_io_IsOpened() ) {
		nvcore_File_io_Sync();
		if( !iop::CloseFile() )
			return FALSE;
		fileOpened = FALSE;
	}
	return TRUE;
}


void
nvcore_File_io_Shut		(			)
{
	nvcore_File_io_Close();
}


uint
nvcore_File_io_GetNbSubDevice	(		)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	return nb_subdev;
}


pcstr
nvcore_File_io_GetSubDeviceName		(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+0];
}


pcstr
nvcore_File_io_GetSubDevicePrefix	(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+1];
}


pcstr
nvcore_File_io_GetSubDeviceSuffix	(	uint	inSubDevNo			)
{
	uint nb_subdev = sizeof(subDev)/sizeof(char*)/3;
	if( inSubDevNo >= nb_subdev )
		return NULL;
	return subDev[inSubDevNo*3+2];
}



