/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <Kernel/PS2/IOP/IOP.h>
#include <Kernel/Common/NvkCore_Mem.h>
#include <sif.h>
#include <sifrpc.h>
#include <sifdev.h>
#include <sifcmd.h>
#include <libcdvd.h>
#include <libscf.h>
#include <stdlib.h>
#include <malloc.h>
using namespace nv;



#define	INETCTL_ARGS			"-no_auto" "\0" "-no_decode"
#define	NETCNF_ARGS				"icon=SYS_NET.ICO" "\0" "iconsys=ICON.SYS"




#define	NVCORE_DECL( ID )					\
	bool	nvcore_##ID##_Init		();		\
	void	nvcore_##ID##_Update	();		\
	void	nvcore_##ID##_Shut		();

NVCORE_DECL( Math	)
NVCORE_DECL( Libc	)
NVCORE_DECL( Report )
NVCORE_DECL( Mem	)
NVCORE_DECL( Clock	)
NVCORE_DECL( File	)
NVCORE_DECL( NVR	)
NVCORE_DECL( Net	)
NVCORE_DECL( Ctrl	)
NVCORE_DECL( Param	)
NVCORE_DECL( Ext	)



// externs for linker variables
typedef int __attribute__ ((mode (TI))) elink_type __attribute__((aligned(16)));
extern elink_type _start;
extern elink_type _end;
extern elink_type _stack_size;
extern elink_type _heap_size;	// -1
extern elink_type _stack;		// -1
extern elink_type _fbss;



namespace
{

	bool	atlaunched		= FALSE;
	bool	coreBooted		= FALSE;
	bool	coreInitialized = FALSE;
	bool	mainCalled		= FALSE;
	uint32	main_sp			= 0;




	// http://www.pkware.com/products/enterprise/white_papers/appnote.html#2

	// Zip Local file header, for each file
	// +0  : local file header signature 4 bytes   (0x04034b50)
	//       central file header signature (0x02014b50)
	// +4  : version needed to extract 2 bytes 
	// +6  : general purpose bit flag 2 bytes 
	// +8  : compression method 2 bytes
	// +10 : last mod file time 2 bytes
	// +12 : last mod file date 2 bytes 
	// +14 : crc-32 4 bytes 
	// +18 : compressed size 4 bytes
	// +22 : uncompressed size 4 bytes
	// +26 : file name length 2 bytes
	// +28 : extra field length 2 bytes
	// +30 : file name (variable size) ...
	//     : extra field (variable size) ...

	// Zip Data Descriptor
	// This descriptor exists only if bit 3 of the general purpose bit flag is set.
	// crc-32 4 bytes 
	// compressed size 4 bytes
	// uncompressed size 4 bytes

	uint8 * ReadZipEntry(	uint8	*	inAddr,
							pstr		outName,
							uint32	&	outBSize,
							uint8*	&	outData		)
	{
		uint32 hdrSign;
		Memcpy( &hdrSign, inAddr, 4 );
		if( hdrSign != 0x04034b50 )
			return NULL;
		uint16 gflags, fnameLen, extraLen;
		Memcpy( &gflags,   inAddr+6,  2 );
		Memcpy( &fnameLen, inAddr+26, 2 );
		Memcpy( &extraLen, inAddr+28, 2 );
		Strncpy( outName, (pcstr)(inAddr+30), fnameLen );
		outName[fnameLen] = 0;
		Memcpy( &outBSize, inAddr+22, 4 );
		outData = inAddr + 30 + fnameLen + extraLen;
		uint8 * next = outData + outBSize;
		if( gflags & (1<<3) )
			next += 12;
		return next;
	}


	bool LoadIrxArchive (	pcstr		inFilename	)
	{
		#undef	memalign
		#undef	free
		#define	memalign	memalign
		#define	free		free

		int fd = sceOpen( inFilename, SCE_RDONLY );
		if( fd < 0 ) {
			Printf( "<Neova> IRX archive named \"%s\" not found !\n", inFilename );
			return FALSE;
		} else {
			Printf( "<Neova> Using IRX archive \"%s\" ...\n", inFilename );
		}

		uint irxBSize = sceLseek( fd, 0, SCE_SEEK_END );
		sceLseek( fd, 0, SCE_SEEK_SET );
		uint8* irxBAddr = (uint8*) memalign( 64, irxBSize+1024 );
		sceRead( fd, irxBAddr, ((irxBSize+512)>>9)<<9 );
		sceClose( fd );

		// for each entries
		uint		  sd_maxSize = 128*1024;	// starts with 128Ko
		sceSifDmaData sd;
		sd.data = (uint) memalign( 256, sd_maxSize );
		sd.addr = (uint) sceSifAllocIopHeap( sd_maxSize );
		sd.mode = 0;

		pcstr filePrefix;
		nv::core::GetParameter( nv::core::PR_FILE_PREFIX, &filePrefix );

		uint8*  zipP = irxBAddr;
		char    fname[ 128 ];
		uint32  fbsize;
		uint8*	fdata;
		for( ;; )
		{
			zipP = ReadZipEntry( zipP, fname, fbsize, fdata );
			if( !zipP )
				break;

			// trx to iop
			sd.size = ((fbsize+15)>>4)<<4;
			if( sd.size > sd_maxSize ) {
				// resizes
				free( (pvoid)sd.data );
				sceSifFreeIopHeap( (pvoid)sd.addr );
				sd_maxSize = ((sd.size+1023)>>10)<<10;
				sd.data = (uint)memalign( 256, sd_maxSize );
				sd.addr = (uint)sceSifAllocIopHeap( sd_maxSize );
			}
			Memcpy( (pvoid)sd.data, (pvoid)fdata, fbsize );
			FlushCache( WRITEBACK_DCACHE );
			int qid = sceSifSetDma( &sd, 1 );
			while( sceSifDmaStat(qid) >= 0 ) {}

			// irx args
			pcstr irxArgs = NULL;
			if( Stricmp("netcnf.irx",fname) == 0 )			irxArgs = NETCNF_ARGS;
			else if( Stricmp("inetctl.irx",fname) == 0 )	irxArgs = INETCTL_ARGS;
			else											irxArgs = (pcstr) filePrefix;

			// load & start irx
			// sceSifLoadModuleBuffer inserts argv[0] = "LDByEE"
 			int res = sceSifLoadModuleBuffer( (pvoid)sd.addr, Strlen(irxArgs), irxArgs );
			Printf( "<Neova> LoadIrxArchive(): IRX \"%s\" %s\n", fname, res<0 ? "failed !":"loaded." );
		}

		free( (pvoid)sd.data );
		sceSifFreeIopHeap( (pvoid)sd.addr );
		free( irxBAddr );
		return TRUE;

		#undef	memalign
		#undef	free
		#define	memalign	$0xdeadf00d$
		#define	free		$0xdeadf00d$
	}


	void LoadIrx	( pcstr inFilename, uint inArgs = 0, pcstr inArgp = NULL )
	{
		Printf( "<Neova> Loading IRX \"%s\" ...\n", inFilename );
		while( sceSifLoadModule(inFilename,inArgs,inArgp) < 0 ) {}
	}


	void LoadIOPModules	(		)
	{
		// From archive ...
		pcstr irxZip = NULL;
		core::GetParameter( core::PR_IOP_IRXZIP, &irxZip );
		if( irxZip && LoadIrxArchive( file::GetFullname(irxZip) ) )
			return;

		// From files ...
		char	path[256];
		char	argp[256];
		char**	irxList = NULL;
		core::GetParameter( core::PR_IOP_IRXLIST, (uint32*)&irxList );
		if( irxList ) {
			while( irxList[0] ) {
				Strcpy( path, irxList[0] );
				Strcpy( argp, irxList[1] );
				irxList += 2;
				uint args = Strlen( argp );
				for( uint i = 0 ; i < args ; i++ )
					if( argp[i]==' ' )	argp[i] = 0;
				LoadIrx( file::GetFullname(path), args, args?argp:NULL );
			}
		}
	}


	void InitCoreParam( int argc, char ** argv )
	{
		// Cmd line
		static char cmdLine[ 512 ];
		int    cmdLineRemain = sizeof(cmdLine) - 1;
		char * cmdLineP      = cmdLine;
		for( int i = 0 ; i < argc ; i++ ) {
			if( !argv[i] )
				break;
			int argvLen = Strlen(argv[i]);
			if( argvLen > cmdLineRemain )
				break;
			Strcpy( cmdLineP, argv[i] );
			cmdLineRemain -= argvLen;
			cmdLineP      += argvLen;
			if( cmdLineRemain == 0 )
				break;
			*cmdLineP++ = ' ';
			cmdLineRemain--;
		}
		*cmdLineP = 0;
		core::SetParameter( core::PR_SYS_CMDLINE, (pcstr)cmdLine );

		// Setup filePrefix & fileSuffix for atmon
		#if defined(_ATMON)
		if( argc && argv[0] && Strnicmp(argv[0],"atfile",6)==0 )
		{
			char* strptr = argv[0];
			while( *strptr != ',' )
				strptr++;
			strptr++;
			*strptr = 0;
			static char atFilePrefix[ 32 ];
			Strcpy( atFilePrefix, argv[0] );
			core::SetParameter( core::PR_FILE_PREFIX, atFilePrefix );
			core::SetParameter( core::PR_FILE_SUFFIX, "" );
			atlaunched = TRUE;
		}
		#endif // _ATMON
	}


	void BootIOP	(	bool	reboot,
						int		mediamode	)
	{
		NV_ASSERT( (mediamode==SCECdCD) || (mediamode==SCECdDVD) );

		if( reboot )
		{
			pcstr iopimg = NULL;
			core::GetParameter( core::PR_IOP_IMGFILE, &iopimg );
			NV_ASSERT( iopimg );

			pcstr iopimgPath = file::GetFullname( iopimg );
			NV_ASSERT( iopimgPath );

			sceSifInitRpc( 0 );
			sceCdInit( SCECdINIT );
			sceCdMmode( mediamode );
			while( sceSifRebootIop(iopimgPath)==0 ) {}
			while( sceSifSyncIop()==0 ) {}
			sceSifInitRpc( 0 );
			sceCdInit( SCECdINIT );
			sceCdMmode( mediamode );
			sceSifLoadFileReset();
			sceFsReset();
			sceSifInitIopHeap();
		}
		else
		{
			sceSifInitRpc( 0 );
			sceSifInitIopHeap();
			sceCdInit( SCECdINIT );
			sceCdMmode( mediamode );
		}
	}

}




bool
nv::core::Startup__	(	int			argc,
						char**		argv,
						uint32		inBaseSP	)
{
	NV_COMPILE_TIME_CHECK_TYPES();

	*GS_BGCOLOR = 0L;
	main_sp = inBaseSP;

	nvcore_Param_Init();
	InitCoreParam( argc, argv );

	mainCalled = TRUE;
	return TRUE;
}



bool
nv::core::Init		()
{
	if( coreInitialized )
		return TRUE;

	pcstr fileprefix = NULL;
	pcstr mediaType  = NULL;
	core::GetParameter( core::PR_FILE_PREFIX, &fileprefix );
	core::GetParameter( core::PR_MEDIA_TYPE,  &mediaType  );

	int  mediamode = SCECdCD;
	if( mediaType && Strnicmp(mediaType,"dvd",3)==0 )
		mediamode = SCECdDVD;

	bool rebootiop = TRUE;
	#if defined(_ATMON)
	if( fileprefix && Strnicmp(fileprefix,"atfile",6)==0 )
		rebootiop = FALSE;
	#endif

	if( !coreBooted )
		BootIOP( rebootiop, mediamode );

	sceResetEE();
	mem::NativeInit();
	devcons::Init();
	Printf( ">> Neova CoreEngine is running\n" );
	Printf( ">> (c) 2002-2008 AtOnce Technologies\n" );
	Printf( "\n" );

	if( !mainCalled ) {
		NV_ERROR( "\n\n\n** Overload the main() function is not allowed !\n** Use the function [void GameMain()] as your application entrypoint." );
		return FALSE;
	}

	// CoreEngine revision
	Printf( "<Neova> CoreEngine %d.%d-%s\n", GetReleaseNumber(), GetReleaseRevision(), GetDebugLevel() );
	#ifdef TIME_LIMITED_EVALUATION
	Printf( "<Neova> Time limited version\n" );
	#endif

	// file media
	if( atlaunched )			Printf( "<Neova> Launched from ATMON\n" );
	if( rebootiop )				Printf( "<Neova> IOP rebooted\n" );
	if( mediamode==SCECdCD )	Printf( "<Neova> cdrom media selected\n" );
	if( mediamode==SCECdDVD )	Printf( "<Neova> dvdrom media mode selected\n" );

	// EE revision
	uint32 prID;
	asm volatile ("mfc0 %0, $15": "=r"(prID));
	Printf( "<Neova> EE revision impl:%d rev:%d\n", (prID>>8)&0xF, prID&0xF );

	// EE configuration
	uint32 prConf;
	asm volatile ("mfc0 %0, $16": "=r"(prConf));
	Printf( "<Neova> EE die:%c ice:%c dce:%c nbe:%c bpe:%c\n",
		(prConf&(1<<18))?'Y':'N',
		(prConf&(1<<17))?'Y':'N',
		(prConf&(1<<16))?'Y':'N',
		(prConf&(1<<13))?'Y':'N',
		(prConf&(1<<12))?'Y':'N'	);

	// GS revision
	uint64 gsCSR = *GS_CSR;
	Printf( "<Neova> GS revision no:%d id:%d\n", uint((gsCSR>>16)&0xF), uint((gsCSR>>24)&0xF) );

	// Link areas
	Printf( "<Neova> Elf   @%x, %dKo\n", uint(&_start), (uint(&_end)-uint(&_start))>>10 );
	Printf( "<Neova> Stack @%x, %dKo\n", main_sp, uint(&_stack_size)>>10 );
	Printf( "<Neova> Heap  @%x, %dKo\n", uint(&_end), (main_sp - uint(&_end))>>10 );
	SetParameter( PR_MEM_APP_ADDR,			uint(&_start)             );
	SetParameter( PR_MEM_APP_BSIZE,			uint(&_end)-uint(&_start) );
	SetParameter( PR_MEM_APP_HEAP_ADDR,		uint(&_end)               );
	SetParameter( PR_MEM_APP_HEAP_BSIZE,	main_sp-uint(&_end)       );
	SetParameter( PR_MEM_APP_STACK_ADDR,	main_sp                   );
	SetParameter( PR_MEM_APP_STACK_BSIZE,	uint(&_stack_size)        );

	// IOP modules
	if( !coreBooted )
		LoadIOPModules();

	// Init all core components
	Printf( "<Neova> NeovaIRX initialization ...\n" );
	if( !coreBooted )	iop::Init( mediamode );
	else				iop::Reset();
	Printf( "<Neova> NeovaIRX initialized.\n" );

	Printf( "<Neova> Components initialization ...\n" );
	nvcore_Report_Init();
	nvcore_Mem_Init();
	nvcore_Libc_Init();
	nvcore_Math_Init();
	nvcore_Net_Init();
	nvcore_File_Init();
	nvcore_Ctrl_Init();
	nvcore_NVR_Init();
	nvcore_Clock_Init();
	nvcore_Ext_Init();
	dmac::Init();
	vu::Init();
	Printf( "<Neova> Components initialized.\n" );

	devcons::Shut();

	coreBooted	    = TRUE;
	coreInitialized = TRUE;
	mem::LogOpen();
	return TRUE;
}



void
nv::core::Shut		(				)
{
	if( !coreInitialized )
		return;

	Printf( "\n\n\n<Neova> ** EXITING **\n" );
	libc::FlushPrintf();

	vu::Shut();
	dmac::Shut();
	iop::Shut();

	nvcore_Ext_Shut();
	nvcore_NVR_Shut();
	nvcore_Ctrl_Shut();
	nvcore_File_Shut();
	nvcore_Net_Shut();
	nvcore_Clock_Shut();
	nvcore_Math_Shut();
	nvcore_Libc_Shut();
	nvcore_Mem_Shut();
	nvcore_Report_Shut();
	nvcore_Param_Shut();

	Printf( "\n\n\n<Neova> ** EXITED **\n" );
	libc::FlushPrintf();
	mem::NativeShut();

	coreInitialized = FALSE;
}


void
nv::core::Exit()
{
	Shut();

	exit(0);
}


bool
nv::core::ExitAsked	 ( )
{
	return FALSE;
}


uint
nv::core::Update	(	)
{
	iop::Update();
	iop::Flush();

	nvcore_Param_Update();
	nvcore_Report_Update();
	nvcore_Mem_Update();
	nvcore_Libc_Update();
	nvcore_Math_Update();
	nvcore_Clock_Update();
	nvcore_Net_Update();
	nvcore_File_Update();
	nvcore_Ctrl_Update();
	nvcore_NVR_Update();
	nvcore_Ext_Update();

	return 0;
}



nv::core::SystemID
nv::core::GetSystemID		(		)
{
	return SCEPlaystation2;
}



nv::core::Aspect
nv::core::GetAspect		(			)
{
	int v = sceScfGetAspect();
	if( v == SCE_ASPECT_43 )			return ASPECT_43;
	else if( v == SCE_ASPECT_FULL )		return ASPECT_FULL;
	else 								return ASPECT_169;
}

nv::core::VideoMode		
nv::core::GetVideoMode		(	)
{
	return VM_Other;
}

nv::core::SoundMode		
nv::core::GetSoundMode		(	)
{
	return nv::core::SoundMode(nv::core::SM_Stereo);	
}

nv::core::DateNotation
nv::core::GetDateNotation		(			)
{
	int v = sceScfGetDateNotation();
	if( v == SCE_DATE_YYYYMMDD )		return DATE_YYYYMMDD;
	else if( v == SCE_DATE_MMDDYYYY )	return DATE_MMDDYYYY;
	else								return DATE_DDMMYYYY;
}


nv::core::Language
nv::core::GetLanguage	(				)
{
	int v = sceScfGetLanguage();
	if( v == SCE_JAPANESE_LANGUAGE )		return LANG_JAPANESE;
	else if( v == SCE_ENGLISH_LANGUAGE )	return LANG_ENGLISH;
	else if( v == SCE_FRENCH_LANGUAGE )		return LANG_FRENCH;
	else if( v == SCE_GERMAN_LANGUAGE )		return LANG_GERMAN;
	else if( v == SCE_ITALIAN_LANGUAGE )	return LANG_ITALIAN;
	else if( v == SCE_SPANISH_LANGUAGE )	return LANG_SPANISH;
	else if( v == SCE_DUTCH_LANGUAGE )		return LANG_DUTCH;
	else 									return LANG_PORTUGUESE;
}


nv::core::SummerTime
nv::core::GetSummerTime	(				)
{
	int v = sceScfGetSummerTime();
	if( v == SCE_SUMMERTIME_ON )			return SUMT_ON;
	else									return SUMT_OFF;
}


nv::core::TimeNotation
nv::core::GetTimeNotation	(					)
{
	int v = sceScfGetTimeNotation();
	if( v == SCE_TIME_24HOUR )				return TIME_24H;
	else									return TIME_12H;
}






