/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>

//#define	EXCEPTION_HANDLER



namespace
{
/*
	const char * NonNullString( const char * inString )
	{
		if( !inString )				return (const char *)"-";
		if( Strlen(inString) == 0 )	return (const char *)"-";
		return inString;
	}
*/
#ifdef EXCEPTION_HANDLER

	typedef void (*DbgHandler)(u_int, u_int, u_int, u_int, u_int, u_long128 *);
	DbgHandler	defDbgHandler[14];

	void	myDbgHandler( u_int code, u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )
	{
		devcons::Init();
		devcons::Printf( "Exception occured !\n" );

/*			Printf( "<Neova> Error : %s", tmp );

			// Output call-stack
			nv::stack::Frame sf;
			if( GetFrame(sf) && GetBackFrame(sf,sf) )
			{
				Printf( "<Neova> call-stack :\n" );
				uint depth = 0;
				for( ;; )
				{
					Printf( "[%d] : %08X (sp=%08X)\n", depth++, uint32(sf.locAddr), uint32(sf.ptr) );
					if( !GetBackFrame(sf,sf) )
						break;
				}
			}

			Printf( "<Neova> Aborted !" );
*/

//		if( defDbgHandler[code] )
//			(*defDbgHandler[code])( stat, cause, epc, bva, bpa, gpr );
		for( ;; ) {}
		ExitHandler();
	}

	void myDbgHandler_1( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(1,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_2( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(2,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_3( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(3,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_4( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(4,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_5( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(5,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_6( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(6,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_7( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(7,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_8( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(8,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_9( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )		{ myDbgHandler(9,stat,cause,epc,bva,bpa,gpr);  }
	void myDbgHandler_10( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )	{ myDbgHandler(10,stat,cause,epc,bva,bpa,gpr); }
	void myDbgHandler_11( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )	{ myDbgHandler(11,stat,cause,epc,bva,bpa,gpr); }
	void myDbgHandler_12( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )	{ myDbgHandler(12,stat,cause,epc,bva,bpa,gpr); }
	void myDbgHandler_13( u_int stat, u_int cause, u_int epc, u_int bva, u_int bpa, u_long128* gpr )	{ myDbgHandler(13,stat,cause,epc,bva,bpa,gpr); }

#endif // EXCEPTION_HANDLER

}



bool
nvcore_Report_Init()
{
#ifdef EXCEPTION_HANDLER
	defDbgHandler[1]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_1 );
	defDbgHandler[2]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_2 );
	defDbgHandler[3]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_3 );
	defDbgHandler[4]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_4 );
	defDbgHandler[5]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_5 );
	defDbgHandler[6]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_6 );
	defDbgHandler[7]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_7 );
	defDbgHandler[8]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_8 );
	defDbgHandler[9]  = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_9 );
	defDbgHandler[10] = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_10 );
	defDbgHandler[11] = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_11 );
	defDbgHandler[12] = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_12 );
	defDbgHandler[13] = (DbgHandler) SetDebugHandler( 1,	myDbgHandler_13 );
#endif
	return TRUE;
}


void
nvcore_Report_Update()
{
	//
}


void
nvcore_Report_Shut()
{
	//
}


void
nv::report::Output	(	Type		inReportT,
						pcstr		inMessage,
						pcstr		inFilename,
						pcstr		inFctName,
						int			inLineNo	)
{
	char tmp[ 256 ];
	if( inFilename || inFctName )
		Sprintf( tmp, "%s [%s, %s(), L%d]", inMessage, inFilename, inFctName, inLineNo );
	else
		Sprintf( tmp, "%s", inMessage );

	char tmp2[256];
	Sprintf( tmp2, "%d.%d-%s",
		core::GetReleaseNumber(),
		core::GetReleaseRevision(),
		core::GetDebugLevel()	);

	if( inReportT == T_MESSAGE )
	{
		Printf( "<Neova %s> Message : %s\n", tmp2, tmp );
	}
	else if( inReportT == T_WARNING )
	{
		Printf( "<Neova %s> Warning : %s\n", tmp2, tmp );
	}
	else if( inReportT == T_ERROR )
	{
		Printf( "<Neova %s> Error : %s\n", tmp2, tmp );

		// Output call-stack
		nv::stack::Frame sf;
		if( stack::GetFrame(sf) && stack::GetBackFrame(sf,sf) )
			stack::OutputCallstack( sf );
	}
}



nv::report::Action
nv::report::AskUserAction	(	)
{
	Printf( "\n\n## Please select an action to do :\n" );

	// Reset paddles
where_s_my_paddle:
	Printf( "   (searching a paddle ...)\n" );
	ctrl::Close();
	ctrl::Open();
	uint      N = ctrl::GetNbMax();
	uint ctrlNo = uint(-1);
	while( ctrlNo == uint(-1) ) {
		for( uint i = 0 ; i < N ; i++ ) {
			ctrl::Status* c = ctrl::GetStatus( i );
			if( c ) {
				ctrlNo = i;
				break;
			}
		}
		core::Update();
	}

	Printf( ">> Press L1+R1+L2+R2 to start the selection.\n" );
	for( ;; ) {
		ctrl::Status* c = ctrl::GetStatus( ctrlNo );
		if(	!c )
			goto where_s_my_paddle;
		if( 	c->button[ctrl::BT_BUTTON4] > 0.5f
			&&	c->button[ctrl::BT_BUTTON5] > 0.5f
			&&	c->button[ctrl::BT_BUTTON6] > 0.5f
			&&	c->button[ctrl::BT_BUTTON7] > 0.5f	)
			break;
		core::Update();
	}

	Printf( ">> Press Triangle : EXIT\n" );
	Printf( "      or Square   : DON'T CARE\n" );
	Printf( "      or Circle   : CONTINUE\n" );
	Printf( "      or Cross    : BREAK\n" );
	for( ;; ) {
		ctrl::Status* c = ctrl::GetStatus( ctrlNo );
		if(	!c )
			goto where_s_my_paddle;
		if( c->button[ctrl::BT_BUTTON0] > 0.5f ) {
			Printf( ">> EXIT selected.\n\n" );
			return A_EXIT;
		}
		if( c->button[ctrl::BT_BUTTON3] > 0.5f ) {
			Printf( ">> DON'T CARE selected.\n\n" );
			return A_DONTCARE;
		}
		if( c->button[ctrl::BT_BUTTON1] > 0.5f ) {
			Printf( ">> CONTINUE selected.\n\n" );
			return A_CONTINUE;
		}
		if( c->button[ctrl::BT_BUTTON2] > 0.5f ) {
			Printf( ">> BREAK selected.\n\n" );
			vu::Break_PATH12();
			return A_BREAK;
		}
		core::Update();
	}
}


