/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <Kernel/PS2/NvkCore_Ctrl[PS2].h>
#include <libpad.h>
#include <libmtap.h>
using namespace nv;


#define	NB_PORT			2
#define NB_SLOT			4
//#define	DEBUG_PAD


#if defined(_NVCOMP_ENABLE_DBG) && defined(DEBUG_PAD)
#define		PADPrintf		Printf
#else
inline void	NULL_PADPrintf( pcstr, ... )		{	}
#define		PADPrintf		NULL_PADPrintf
#endif


namespace
{

	enum CtrlPhase {
		S_CLOSED		= 0,
		S_UNPLUGGED,
		S_INIT_ANALOG0,
		S_INIT_ANALOG1,
		S_INIT_PRESSM0,
		S_INIT_PRESSM1,
		S_INIT_VIB0,
		S_INIT_VIB1,
		S_READY,
		S_ACQUIRING,
	};


	struct CtrlState {
		CtrlPhase				phase;
		bool					actuator;
		bool					analog;
		bool					pressMode;
		int						termId;
		ALIGNED512( uint128,	pad_dma_buf[scePadDmaBufferMax] );
		uchar					pad_act[6];
		nv::ctrl::Status		status;
	};


	bool				opened;
	CtrlState			stateA[ NB_PORT ][ NB_SLOT ];
	float				deadzA[ ctrl::TP_MAX ][ ctrl::BT_MAX ][ 2 ];	// lo/hi


	void OpenPort ( int P )
	{
		sceMtapPortOpen( P );

		// Open slots
		for( int S = 0 ; S < NB_SLOT ; S++ ) {
			NV_ASSERT_A512( stateA[P][S].pad_dma_buf );
			if( scePadPortOpen(P,S,stateA[P][S].pad_dma_buf)==1 ) {
				stateA[P][S].phase = S_UNPLUGGED;
			}
		}
	}


	void ClosePort ( int P )
	{
		for( int S = 0 ; S < NB_SLOT ; S++ ) {
			if( stateA[P][S].phase != S_CLOSED ) {
				scePadPortClose( P, S );
				stateA[P][S].phase = S_CLOSED;
			}
		}
		sceMtapPortClose( P );
	}


	void UpdatePort ( int P )
	{
		//
	}


	#define	CTOF1( inV )	float(int(inV)&0xFF)*(1.0F/255.0F)
	#define BTOF1( inM )	( (1<<(inM))&paddata ? 1.0F : 0.0F )

	void ReadPressModeCtrl( nv::ctrl::Status * inCtrl, uchar * rdata )
	{
		uint paddata = 0xffff ^ ((uint(rdata[2])<<8)|uint(rdata[3]));
		inCtrl->button[ nv::ctrl::BT_DIR1X ]		= CTOF1( rdata[6] ) * 2.0F - 1.0F;
		inCtrl->button[ nv::ctrl::BT_DIR1Y ]		= CTOF1( rdata[7] ) * 2.0F - 1.0F;
		inCtrl->button[ nv::ctrl::BT_DIR2X ]		= CTOF1( rdata[4] ) * 2.0F - 1.0F;
		inCtrl->button[ nv::ctrl::BT_DIR2Y ]		= CTOF1( rdata[5] ) * 2.0F - 1.0F;
		inCtrl->button[ nv::ctrl::BT_DIR3right ]	= CTOF1( rdata[8] );
		inCtrl->button[ nv::ctrl::BT_DIR3left ]		= CTOF1( rdata[9] );
		inCtrl->button[ nv::ctrl::BT_DIR3up ]		= CTOF1( rdata[10] );
		inCtrl->button[ nv::ctrl::BT_DIR3down ]		= CTOF1( rdata[11] );
		inCtrl->button[ nv::ctrl::BT_BUTTON0 ]		= CTOF1( rdata[12] );
		inCtrl->button[ nv::ctrl::BT_BUTTON1 ]		= CTOF1( rdata[13] );
		inCtrl->button[ nv::ctrl::BT_BUTTON2 ]		= CTOF1( rdata[14] );
		inCtrl->button[ nv::ctrl::BT_BUTTON3 ]		= CTOF1( rdata[15] );
		inCtrl->button[ nv::ctrl::BT_BUTTON4 ]		= CTOF1( rdata[18] );
		inCtrl->button[ nv::ctrl::BT_BUTTON5 ]		= CTOF1( rdata[19] );
		inCtrl->button[ nv::ctrl::BT_BUTTON6 ]		= CTOF1( rdata[16] );
		inCtrl->button[ nv::ctrl::BT_BUTTON7 ]		= CTOF1( rdata[17] );
		inCtrl->button[ nv::ctrl::BT_BUTTON8 ]		= BTOF1( 8 );
		inCtrl->button[ nv::ctrl::BT_BUTTON9 ]		= BTOF1( 11 );
		inCtrl->button[ nv::ctrl::BT_BUTTON10 ]		= BTOF1( 9 );
		inCtrl->button[ nv::ctrl::BT_BUTTON11 ]		= BTOF1( 10 );
		inCtrl->button[ nv::ctrl::BT_BUTTON12 ]		= 0.0F;
		inCtrl->button[ nv::ctrl::BT_BUTTON13 ]		= 0.0F;
		inCtrl->button[ nv::ctrl::BT_BUTTON14 ]		= 0.0F;
		inCtrl->button[ nv::ctrl::BT_BUTTON15 ]		= 0.0F;
	}

	void ReadNoPressModeCtrl( nv::ctrl::Status * inCtrl, uchar * rdata )
	{
		uint paddata = 0xffff ^ ((uint(rdata[2])<<8)|uint(rdata[3]));
		inCtrl->button[ nv::ctrl::BT_DIR1X ]		= CTOF1( rdata[6] ) * 2.0F - 1.0F;
		inCtrl->button[ nv::ctrl::BT_DIR1Y ]		= CTOF1( rdata[7] ) * 2.0F - 1.0F;
		inCtrl->button[ nv::ctrl::BT_DIR2X ]		= CTOF1( rdata[4] ) * 2.0F - 1.0F;
		inCtrl->button[ nv::ctrl::BT_DIR2Y ]		= CTOF1( rdata[5] ) * 2.0F - 1.0F;
		inCtrl->button[ nv::ctrl::BT_DIR3right ]	= BTOF1( 13 );
		inCtrl->button[ nv::ctrl::BT_DIR3left ]		= BTOF1( 15 );
		inCtrl->button[ nv::ctrl::BT_DIR3up ]		= BTOF1( 12 );
		inCtrl->button[ nv::ctrl::BT_DIR3down ]		= BTOF1( 14 );
		inCtrl->button[ nv::ctrl::BT_BUTTON0 ]		= BTOF1( 4 );
		inCtrl->button[ nv::ctrl::BT_BUTTON1 ]		= BTOF1( 5 );
		inCtrl->button[ nv::ctrl::BT_BUTTON2 ]		= BTOF1( 6 );
		inCtrl->button[ nv::ctrl::BT_BUTTON3 ]		= BTOF1( 7 );
		inCtrl->button[ nv::ctrl::BT_BUTTON4 ]		= BTOF1( 0 );
		inCtrl->button[ nv::ctrl::BT_BUTTON5 ]		= BTOF1( 1 );
		inCtrl->button[ nv::ctrl::BT_BUTTON6 ]		= BTOF1( 2 );
		inCtrl->button[ nv::ctrl::BT_BUTTON7 ]		= BTOF1( 3 );
		inCtrl->button[ nv::ctrl::BT_BUTTON8 ]		= BTOF1( 8 );
		inCtrl->button[ nv::ctrl::BT_BUTTON9 ]		= BTOF1( 11 );
		inCtrl->button[ nv::ctrl::BT_BUTTON10 ]		= BTOF1( 9 );
		inCtrl->button[ nv::ctrl::BT_BUTTON11 ]		= BTOF1( 10 );
		inCtrl->button[ nv::ctrl::BT_BUTTON12 ]		= 0.0F;
		inCtrl->button[ nv::ctrl::BT_BUTTON13 ]		= 0.0F;
		inCtrl->button[ nv::ctrl::BT_BUTTON14 ]		= 0.0F;
		inCtrl->button[ nv::ctrl::BT_BUTTON15 ]		= 0.0F;
	}


	//
	// id table :
	// 2 (NeGi-CON), 3 (GunCON(Konami)), 4 (STANDARD), 5 (JOYSTICK),
	// 6 (GunCON(NAMCO)), 7 (ANALOG DUALSHOCK/DUALSHOCK 2), 0x100 (TSURI-CON), 0x300 (JOG-CON)

	int GetCtrlTermId( int P, int S )
	{
		int id, exid;
		id = scePadInfoMode( P, S, InfoModeCurID, 0 );
		if( id == 0 )	return 0;
		exid = scePadInfoMode( P, S, InfoModeCurExID, 0 );
		if( exid > 0 )	id = exid;
		return id;
	}

	bool IsCtrlAnalogable( int P, int S )
	{
		// analog can be turn-on ?
		return scePadInfoMode(P,S,InfoModeCurExID,0) != 0;
	}

	void UpdateCtrl( int P, int S )
	{
		int					r, termId;
		ALIGNED512( uchar, 	rdata[32] );

		CtrlState* st = & stateA[P][S];
		if( st->phase == S_CLOSED )
			return;

		// Check state
		// Note that scePadStateFindCTP1 could be returned for in-built DVD remote controller on PORT 1 !
		//
		// scePadStateFindCTP1 (controller protocol v1) :
		// - Controllers which do not have vibration function
		// - Digital controller (0x41)
		// - Analog joystick (0x53)
		// - DVD remote controller (0x4?)
		//
		// scePadStateStable (controller protocol v2) :
		// - DUALSHOCK   (0x73)
		// - DUALSHOCK 2 (0x79)
		r = scePadGetState( P, S );
		if( r != scePadStateFindCTP1 && r != scePadStateStable ) {
			if( r == scePadStateDiscon ) {
				st->phase = S_UNPLUGGED;
			}
			return;
		}

		switch( st->phase )
		{
		case S_UNPLUGGED :
			st->termId		= 0;
			st->actuator	= FALSE;
			st->analog		= FALSE;
			st->pressMode	= FALSE;
			termId = GetCtrlTermId(P,S);
			PADPrintf( "[%d,%d] S_UNPLUGGED (termId=%d)\n", P, S, termId );
			if( termId == 4 || termId == 7 )
				st->phase = S_INIT_ANALOG0;
			break;

		case S_INIT_ANALOG0 :
			PADPrintf( "[%d,%d] S_INIT_ANALOG0\n", P, S );
			if( !IsCtrlAnalogable(P,S) ) {
				st->analog = FALSE;
				st->phase  = S_INIT_PRESSM0;
			}
			else if( scePadSetMainMode(P,S,1,3) == 1 ) {
				st->phase = S_INIT_ANALOG1;
			}
			break;

		case S_INIT_ANALOG1 :
			PADPrintf( "[%d,%d] S_INIT_ANALOG1\n", P, S );
			r = scePadGetReqState( P, S );
			if( r != scePadReqStateBusy ) {
				st->analog = ( r == scePadReqStateComplete );
				st->phase  = S_INIT_PRESSM0;
			}
			break;

		case S_INIT_PRESSM0 :
			PADPrintf( "[%d,%d] S_INIT_PRESSM0\n", P, S );
			if(	r!=scePadStateStable || scePadInfoPressMode(P,S)!=1 )
				st->phase = S_INIT_VIB0;
			else if( scePadEnterPressMode(P,S) == 1 )
				st->phase = S_INIT_PRESSM1;
			break;

		case S_INIT_PRESSM1 :
			PADPrintf( "[%d,%d] S_INIT_PRESSM1\n", P, S );
			r = scePadGetReqState( P, S );
			if( r == scePadReqStateBusy )
				break;
			if( r != scePadReqStateComplete )
				st->phase = S_INIT_PRESSM0;
			else {
				st->pressMode = TRUE;
				st->phase     = S_INIT_VIB0;
			}
			break;

		case S_INIT_VIB0 :
			PADPrintf( "[%d,%d] S_INIT_VIB0\n", P, S );
			if( r!=scePadStateStable || scePadInfoAct(P,S,-1,0)!=2 ) {
				st->phase = S_READY;
			}
			else  {
				Memset( &stateA[P][S].pad_act, 0xff, 6 );
				stateA[P][S].pad_act[0] = 0;
				stateA[P][S].pad_act[1] = 1;
				if( scePadSetActAlign(P,S,stateA[P][S].pad_act) == 1 )
					st->phase = S_INIT_VIB1;
			}
			break;

		case S_INIT_VIB1 :
			PADPrintf( "[%d,%d] S_INIT_VIB1\n", P, S );
			r = scePadGetReqState( P, S );
			if( r == scePadReqStateBusy )
				break;
			else if( r != scePadReqStateComplete )
				st->phase = S_INIT_VIB0;
			else {
				st->actuator = TRUE;
				st->phase    = S_READY;
			}
			break;

		case S_READY :
			if( st->termId == 0 ) {
				// Backup the final terminal ID to check controller changement compatibility !
				st->termId = GetCtrlTermId( P, S );
			}
			PADPrintf( "[%d,%d] S_READY, termid=%d, analog=%d, actuator=%d, pressMode=%d \n", P, S, st->termId, int(st->analog), int(st->actuator), int(st->pressMode) );

		case S_ACQUIRING :
			r = scePadRead( P, S, rdata );
			if( r == 0 || rdata[0] != 0 ) {
				st->phase = S_READY;
				break;
			}

			// Check termid
			termId = (rdata[1]>>4);
			if( termId != st->termId ) {
				PADPrintf( "<Neova> ctrl [%d,%d]: changed from termid %d to termid %d !\n", P, S, st->termId, termId );
				st->phase = S_UNPLUGGED;
				break;
			}

			if( st->pressMode )		ReadPressModeCtrl( &st->status, rdata );
			else					ReadNoPressModeCtrl( &st->status, rdata );

			if( st->termId==0x4 && !st->actuator && !st->analog && !st->pressMode )		st->status.type = nv::ctrl::TP_DVDREMOTE;
			else																		st->status.type = nv::ctrl::TP_PADDLE;

			//PADPrintf( "[%d,%d] S_ACQUIRING (termId=%d) !\n", P, S, st->termId );
			st->phase = S_ACQUIRING;
			break;
		}
	}

}




bool
nvcore_Ctrl_Init()
{
	sceMtapInit();
	scePadInit(0);

	for( int P = 0 ; P < NB_PORT ; P++ ) {
		for( int S = 0 ; S < NB_SLOT ; S++ )
			stateA[P][S].phase = S_CLOSED;
	}

	for( int i = 0 ; i < ctrl::TP_MAX ; i++ ) {
		for( int j = 0 ; j < ctrl::BT_MAX ; j++ ) {
			if( i==ctrl::TP_PADDLE || i==ctrl::TP_GUN ) {
				deadzA[i][j][0] = 0.3f;
				deadzA[i][j][1] = 1.0f;
			} else {
				deadzA[i][j][0] = 0.f;
				deadzA[i][j][1] = 0.f;
			}
		}
	}

	opened = FALSE;
	return TRUE;
}


void
nvcore_Ctrl_Shut()
{
	nv::ctrl::Close();
	scePadEnd();
	sceMtapEnd();
}


void
nv::ctrl::Open(		)
{
	if( opened )
		return;

	for( int P = 0 ; P < NB_PORT ; P++ )
		OpenPort( P );

	opened = TRUE;
}


void
nv::ctrl::Close()
{
	if( !opened )
		return;

	for( int P = 0 ; P < NB_PORT ; P++ )
		ClosePort( P );

	opened = FALSE;
}


uint
nv::ctrl::GetNbMax(	)
{
	return NB_PORT*NB_SLOT;
}



void
nvcore_Ctrl_Update()
{
	if( !opened )
		return;

	//PADPrintf( "----\n" );
	for( int P = 0 ; P < NB_PORT ; P++ ) {
		UpdatePort( P );
		for( int S = 0 ; S < NB_SLOT ; S++ )
			UpdateCtrl( P, S );
	}
}



nv::ctrl::Status*
nv::ctrl::GetStatus(	uint	inCtrlNo		)
{
	if( !opened )
		return NULL;
	uint P = inCtrlNo/NB_SLOT;
	uint S = inCtrlNo%NB_SLOT;
	if( P>=NB_PORT || S>=NB_SLOT )			return NULL;
	if( stateA[P][S].phase != S_ACQUIRING )	return NULL;
	return &stateA[P][S].status;
}


int
nv::ctrl::GetCtrlTermId		(	uint 		inCtrlNo	)
{
	if( !opened )
		return 0;
	uint P = inCtrlNo/NB_SLOT;
	uint S = inCtrlNo%NB_SLOT;
	if( P>=NB_PORT || S>=NB_SLOT )			return 0;
	if( stateA[P][S].phase != S_ACQUIRING )	return 0;
	return stateA[P][S].termId;
}


bool
nv::ctrl::IsCtrlAnalog		(	uint 		inCtrlNo	)
{
	if( !opened )
		return 0;
	uint P = inCtrlNo/NB_SLOT;
	uint S = inCtrlNo%NB_SLOT;
	if( P>=NB_PORT || S>=NB_SLOT )			return 0;
	if( stateA[P][S].phase != S_ACQUIRING )	return 0;
	return stateA[P][S].analog;
}


bool
nv::ctrl::IsCtrlPressMode		(	uint		inCtrlNo	)
{
	if( !opened )
		return 0;
	uint P = inCtrlNo/NB_SLOT;
	uint S = inCtrlNo%NB_SLOT;
	if( P>=NB_PORT || S>=NB_SLOT )			return 0;
	if( stateA[P][S].phase != S_ACQUIRING )	return 0;
	return stateA[P][S].pressMode;
}


bool
nv::ctrl::HasCtrlActuators		(	uint		inCtrlNo	)
{
	if( !opened )
		return 0;
	uint P = inCtrlNo/NB_SLOT;
	uint S = inCtrlNo%NB_SLOT;
	if( P>=NB_PORT || S>=NB_SLOT )			return 0;
	if( stateA[P][S].phase != S_ACQUIRING )	return 0;
	return stateA[P][S].actuator;
}


bool
nv::ctrl::SetActuators	(	uint		inCtrlNo,
							float		inLowSpeed,
							float		inHighSpeed	)
{
	if( !opened )
		return FALSE;
	uint P = inCtrlNo/NB_SLOT;
	uint S = inCtrlNo%NB_SLOT;
	if( P>=NB_PORT || S>=NB_SLOT )				return FALSE;
	if( stateA[P][S].phase != S_ACQUIRING ) 	return FALSE;
	if( !stateA[P][S].actuator )				return FALSE;
	stateA[P][S].pad_act[0] = uchar( Clamp(inHighSpeed,0.0f,1.0f) >= 0.5f ? 1 : 0 );
	stateA[P][S].pad_act[1] = uchar( Clamp(inLowSpeed,0.0f,1.0f) * 255.0f );
	return scePadSetActDirect( P, S, stateA[P][S].pad_act ) == 1;
}


bool
nv::ctrl::GetDefDeadZone	(		Type			inType,
									Button			inButton,
									float*			outLo,
									float*			outHi			)
{
	int t = int(inType);
	int b = int(inButton);
	if( t<0 || t>=TP_MAX )	return FALSE;
	if( b<0 || b>=BT_MAX )	return FALSE;
	if( outLo )		*outLo = deadzA[t][b][0];
	if( outHi )		*outHi = deadzA[t][b][1];
	return TRUE;
}


bool
nv::ctrl::SetDefDeadZone	(		Type			inType,
									Button			inButton,
									float			inLo,
									float			inHi			)
{
	float lo, hi;
	if( !GetDefDeadZone(inType,inButton,&lo,&hi) )
		return FALSE;
	if( inLo >= 0.f )	lo = Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return FALSE;
	int t = int(inType);
	int b = int(inButton);
	deadzA[t][b][0] = lo;
	deadzA[t][b][1] = hi;
	return TRUE;
}


float
nv::ctrl::Status::Filtered	(		Button			inButton,
									float			inLo,
									float			inHi	)
{
	if( int(inButton)<0 || int(inButton)>=BT_MAX )
		return 0.f;

	float lo, hi;
	if( !GetDefDeadZone(type,inButton,&lo,&hi) )
		return 0.f;

	if( inLo >= 0.f )	lo = Clamp( inLo, 0.f, 1.f );
	if( inHi >= 0.f )	hi = Clamp( inHi, 0.f, 1.f );
	if( lo >= hi )
		return 0.f;

	float b  = Clamp( button[int(inButton)], -1.f, +1.f );
	float ab = Absf( b );
	float sb = ( b>=0.f ? 1.f : -1.f );

	if( ab <= lo )	return 0.f;
	if( ab >= hi )	return sb;
	return sb * (ab-lo)/(hi-lo);
}



