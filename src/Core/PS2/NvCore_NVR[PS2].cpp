/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PS2/EE/EE.h>
#include <libmc.h>
#include <libcdvd.h>
#include <libscf.h>
using namespace nv;


#define	NB_PORT			2
#define NB_SLOT			4
#define	MAX_DIR			(18+2)		// TRC limits + "." + ".." !



namespace
{

	enum StateID
	{
		S_IDLE	= 0,

		SGS_SYNCING,

		SFM_SYNCING,

		SGF_SYNCING,

		SCR_MKDIR,
		SCR_CHECKING,
		SCR_OPENING,
		SCR_WRITING,
		SCR_CLOSING,

		SWR_OPENING,
		SWR_SEEKING,
		SWR_WRITING,
		SWR_CLOSING,

		SRD_OPENING,
		SRD_SEEKING,
		SRD_READING,
		SRD_CLOSING,

		SDR_SYNCING,
	};

	char				productCode[32+1];

	int					state;
	int					state_hd;
	uint32				state_params[8];
	nv::nvram::Result	state_res;
	char				state_name[32];

	int					firstnext_idx;
	int					firstnext_cpt;
	sceMcTblGetDir		firstnext_dir[MAX_DIR];


	// Convert in local time

	inline uint fromBCD( uchar v )
	{
		uchar vh = v>>4;
		uchar vl = v&15;
		return vh*10+vl;
	}

	inline uint toBCD( uchar v )
	{
		uchar vh = v/10;
		uchar vl = v - vh*10;
		return (vh<<4)|(vl);
	}

/*	void Convert( nvram::DateTime * out, sceMcStDateTime * in )
	{
		sceCdCLOCK cdclock;
		cdclock.second = toBCD( in->Sec );
		cdclock.minute = toBCD( in->Min );
		cdclock.hour   = toBCD( in->Hour );
		cdclock.day    = toBCD( in->Day );
		cdclock.month  = toBCD( in->Month );
		cdclock.year   = toBCD( in->Year - 2000 );

		sceScfGetLocalTimefromRTC( &cdclock );

		out->s_time = fromBCD( cdclock.second );
		out->m_time = fromBCD( cdclock.minute );
		out->h_time = fromBCD( cdclock.hour );
		out->d_date = fromBCD( cdclock.day );
		out->m_date = fromBCD( cdclock.month );
		out->y_date = fromBCD( cdclock.year ) + 2000;
	}
*/
}



bool
nvcore_NVR_Init()
{
	if( !sceMcInit() < 0 )
		return FALSE;
	state = S_IDLE;
	state_res = nv::nvram::Result(0);
	productCode[0] = 0;
	firstnext_idx = 0;
	firstnext_cpt = 0;
	return TRUE;
}


void
nvcore_NVR_Shut()
{
	sceMcEnd();
}


//
// ASYNC ( sceMcSync(1,...) ) failed !!!!
//

#define	MC_SYNC()						\
	int res;							\
	if( sceMcSync(0,NULL,&res) == 0 )	\
		return;



void
nvcore_NVR_Update()
{
	switch( state )
	{
		//
		// Idle
		//

		case S_IDLE :
			return;


		//
		// Get Status
		//

		case SGS_SYNCING :
			{
				MC_SYNC();

				uint32* out_freeBSize	= (uint32*)	 state_params[0];
				bool* 	out_switched	= (bool*)	 state_params[1];
				int * gi_type			= (int*)	&state_params[2];
				int * gi_free			= (int*)	&state_params[3];
				int * gi_format			= (int*)	&state_params[4];

				if( out_freeBSize )
					*out_freeBSize = 0;
				if( out_switched )
					*out_switched = FALSE;

				if( res == sceMcResDeniedPermit )
				{
					state_res = nvram::NVR_STATUS_INVALID;
				}
				else if(	res == sceMcResSucceed
						||	res == sceMcResNoFormat
						||	res == sceMcResChangedCard	)
				{
					if( out_switched )
						*out_switched = (res != sceMcResSucceed);
					if( *gi_type == sceMcTypeNoCard )
					{
						state_res = nvram::NVR_STATUS_NOTFOUND;
					}
					else if( *gi_type == sceMcTypePS2 )
					{
						if( res == sceMcResNoFormat || *gi_format == 0 )
						{
							state_res = nvram::NVR_STATUS_UNFORMATTED;
						}
						else
						{
							if( out_freeBSize )
								*out_freeBSize = (*gi_free) << 10;
							state_res = nvram::NVR_STATUS_READY;
						}
					}
					else
					{
						state_res = nvram::NVR_STATUS_INVALID;
					}
				}
				else
				{
					state_res = nvram::NVR_STATUS_NOTFOUND;
				}

				state = S_IDLE;
			}
			break;


		//
		// Format
		//

		case SFM_SYNCING :
			{
				MC_SYNC();

				if( res == sceMcResSucceed )
					state_res = nvram::NVR_SUCCESS;
				else if( res == sceMcResDeniedPermit )
					state_res = nvram::NVR_PROTECTED;
				else
					state_res = nvram::NVR_NOTFOUND;

				state = S_IDLE;
			}
			break;


		//
		// Get First
		//

		case SGF_SYNCING :
			{
				MC_SYNC();

				if( res >= 0 )
				{
					state = S_IDLE;		// unlock GetNextRecord()
					firstnext_idx = 0;
					firstnext_cpt = res;
					nvram::GetNextRecord( (pstr)state_params[0], (uint32*)state_params[1] );
				}
				else if( res == sceMcResNoEntry )
					state_res = nvram::NVR_NOTFOUND;
				else
					state_res = nvram::NVR_ERROR;

				state = S_IDLE;
			}
			break;


		//
		// Create Record
		//

		case SCR_MKDIR :
			{
				MC_SYNC();

				if( res == sceMcResSucceed || res == sceMcResNoEntry )
				{
					uint P = state_params[3]/NB_SLOT;
					uint S = state_params[3]%NB_SLOT;
					char path[128];
					Sprintf( path, "/%s/%s", productCode, (pcstr)state_params[0] );
					res = sceMcGetDir( P, S, path, 0, 1, firstnext_dir );
					if( res != 0 )
					{
						// free the initial data buffer
						NvFree( (void*)state_params[2] );
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SCR_CHECKING;
					}
				}
				else
				{
					if( res == sceMcResFullDevice )
						state_res = nvram::NVR_ISFULL;
					else if( res == sceMcResDeniedPermit )
						state_res = nvram::NVR_PROTECTED;
					else
						state_res = nvram::NVR_ERROR;
					NvFree( (void*)state_params[2] );
					state = S_IDLE;
				}
			}
			break;

		case SCR_CHECKING :
			{
				MC_SYNC();

				if( res == 0 )	// no file exists !
				{
					uint P = state_params[3]/NB_SLOT;
					uint S = state_params[3]%NB_SLOT;
					char path[128];
					Sprintf( path, "/%s/%s", productCode, (pcstr)state_params[0] );
					res = sceMcOpen( P, S, path, SCE_WRONLY|SCE_CREAT );
					if( res != 0 )
					{
						NvFree( (pvoid)state_params[2] );
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SCR_OPENING;
					}
				}
				else
				{
					if( res > 0 )
						state_res = nvram::NVR_ALREADYEXIST;
					else if( res == sceMcResFullDevice )
						state_res = nvram::NVR_ISFULL;
					else if( res == sceMcResDeniedPermit )
						state_res = nvram::NVR_PROTECTED;
					else
						state_res = nvram::NVR_ERROR;
					NvFree( (pvoid)state_params[2] );
					state = S_IDLE;
				}
			}
			break;

		case SCR_OPENING :
			{
				MC_SYNC();

				if( res >= 0 )
				{
					state_hd = res;
					// Fill new record with dummy data
					res = sceMcWrite( state_hd, (pvoid)state_params[2], state_params[1] );
					if( res != 0 )
					{
						NvFree( (pvoid)state_params[2] );
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SCR_WRITING;
					}
				}
				else
				{
					if( res == sceMcResFullDevice )
						state_res = nvram::NVR_ISFULL;
					else if( res == sceMcResNoEntry )
						state_res = nvram::NVR_NOTFOUND;
					else if( res == sceMcResDeniedPermit )
						state_res = nvram::NVR_PROTECTED;
					else
						state_res = nvram::NVR_ERROR;
					NvFree( (pvoid)state_params[2] );
					state = S_IDLE;
				}
			}
			break;

		case SCR_WRITING :
			{
				MC_SYNC();

				NvFree( (pvoid)state_params[2] );

				if( res >= 0 )
				{
					res = sceMcClose( state_hd );
					if( res != 0 )
					{
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SCR_CLOSING;
					}
				}
				else
				{
					if( res == sceMcResFullDevice )
						state_res = nvram::NVR_ISFULL;
					else if( res == sceMcResDeniedPermit )
						state_res = nvram::NVR_PROTECTED;
					else
						state_res = nvram::NVR_ERROR;
					state = S_IDLE;
				}
			}
			break;

		case SCR_CLOSING :
			{
				MC_SYNC();
				
				if( res == 0 )
					state_res = nvram::NVR_SUCCESS;
				else
					state_res = nvram::NVR_ERROR;

				state = S_IDLE;
			}
			break;


		//
		// Write Record
		//

		case SWR_OPENING :
			{
				MC_SYNC();

				if( res >= 0 )
				{
					state_hd = res;
					res = sceMcSeek( state_hd, state_params[2], 0 );
					if( res != 0 )
					{
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SWR_SEEKING;
					}
				}
				else
				{
					if( res == sceMcResNoEntry )
						state_res = nvram::NVR_NOTFOUND;
					else if( res == sceMcResDeniedPermit )
						state_res = nvram::NVR_PROTECTED;
					else
						state_res = nvram::NVR_ERROR;
					state = S_IDLE;
				}
			}
			break;

		case SWR_SEEKING :
			{
				MC_SYNC();

				if( res == state_params[2] )
				{
					res = sceMcWrite( state_hd, (pvoid)state_params[0], state_params[1] );
					if( res != 0 )
					{
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SWR_WRITING;
					}
				}
				else
				{
					state_res = nvram::NVR_ERROR;
					state = S_IDLE;
				}
			}
			break;

		case SWR_WRITING :
			{
				MC_SYNC();

				if( res == state_params[1] )
				{
					res = sceMcClose( state_hd );
					if( res != 0 )
					{
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SWR_CLOSING;
					}
				}
				else
				{
					if( res == sceMcResFullDevice )
						state_res = nvram::NVR_ISFULL;
					else if( res == sceMcResDeniedPermit )
						state_res = nvram::NVR_PROTECTED;
					else
						state_res = nvram::NVR_ERROR;
					state = S_IDLE;
				}
			}
			break;

		case SWR_CLOSING :
			{
				MC_SYNC();

				if( res == 0 )
					state_res = nvram::NVR_SUCCESS;
				else
					state_res = nvram::NVR_ERROR;

				state = S_IDLE;
			}
			break;


		//
		// Read Record
		//

		case SRD_OPENING :
			{
				MC_SYNC();

				if( res >= 0 )
				{
					state_hd = res;
					res = sceMcSeek( state_hd, state_params[2], 0 );
					if( res != 0 )
					{
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SRD_SEEKING;
					}
				}
				else
				{
					if( res == sceMcResNoEntry )
						state_res = nvram::NVR_NOTFOUND;
					else if( res == sceMcResDeniedPermit )
						state_res = nvram::NVR_PROTECTED;
					else
						state_res = nvram::NVR_ERROR;
					state = S_IDLE;
				}
			}
			break;

		case SRD_SEEKING :
			{
				MC_SYNC();

				if( res == state_params[2] )
				{
					res = sceMcRead( state_hd, (pvoid)state_params[0], state_params[1] );
					if( res != 0 )
					{
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SRD_READING;
					}
				}
				else
				{
					state_res = nvram::NVR_ERROR;
					state = S_IDLE;
				}
			}
			break;

		case SRD_READING :
			{
				MC_SYNC();

				if( res == state_params[1] )
				{
					res = sceMcClose( state_hd );
					if( res != 0 )
					{
						state = S_IDLE;
						state_res = nvram::NVR_ERROR;
					}
					else
					{
						state = SRD_CLOSING;
					}
				}
				else
				{
					if( res == sceMcResDeniedPermit )
						state_res = nvram::NVR_PROTECTED;
					else
						state_res = nvram::NVR_ERROR;
					state = S_IDLE;
				}
			}
			break;

		case SRD_CLOSING :
			{
				MC_SYNC();

				if( res == 0 )
					state_res = nvram::NVR_SUCCESS;
				else
					state_res = nvram::NVR_ERROR;

				state = S_IDLE;
			}
			break;


		//
		// Delete Record
		//

		case SDR_SYNCING :
			{
				MC_SYNC();

				if( res == 0 )
					state_res = nvram::NVR_SUCCESS;
				else if( res == sceMcResNoEntry )
					state_res = nvram::NVR_NOTFOUND;
				else if( res == sceMcResDeniedPermit )
					state_res = nvram::NVR_PROTECTED;
				else
					state_res = nvram::NVR_ERROR;

				state = S_IDLE;
			}
			break;

	}
}




bool
nv::nvram::SetProductCode	(	pcstr		inProductCode		)
{
	// Checks if an op. is currently in progress ...
	if( state != S_IDLE ) {
		NV_WARNING( "nvram: An async operation is still in progress !" );
		return FALSE;
	}
	if( Strlen(inProductCode) >= 32 ) {
		NV_WARNING( "nvram: The specified product code is too long !" );
		return FALSE;
	}

	Strcpy( productCode, inProductCode );
	return TRUE;
}


pcstr
nv::nvram::GetProductCode	(									)
{
	return productCode;
}


uint
nv::nvram::GetNbMax			(									)
{
	return NB_PORT*NB_SLOT;
}


bool
nv::nvram::IsReady			(	Result	*		outLastResult	)
{
	if( outLastResult )
		*outLastResult = state_res;
	return ( state == S_IDLE );
}


bool
nv::nvram::GetStatus		(	uint			inNVRNo,
								uint32	*		outFullBSize,
								uint32	*		outFreeBSize,
								bool	*		outSwitched		)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;

	uint P = inNVRNo/NB_SLOT;
	uint S = inNVRNo%NB_SLOT;

	int nbmax = sceMcGetSlotMax(P);
	if( nbmax == 0 || S >= nbmax )
	{
		state_res = nvram::NVR_STATUS_NOTFOUND;
		return TRUE;
	}

	if( sceMcGetInfo(P,S,(int*)&state_params[2],(int*)&state_params[3],(int*)&state_params[4]) != 0 )
		return FALSE;

	state			= SGS_SYNCING;
	state_params[0] = (uint32) outFreeBSize;
	state_params[1] = (uint32) outSwitched;
	if( outFullBSize )
		*outFullBSize = 8000<<10;

	return TRUE;
}


bool
nv::nvram::Format			(	uint			inNVRNo			)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;

	uint P = inNVRNo/NB_SLOT;
	uint S = inNVRNo%NB_SLOT;

	if( sceMcFormat(P,S) != 0 )
		return FALSE;

	state = SFM_SYNCING;

	return TRUE;
}


bool
nv::nvram::GetFirstRecord	(	uint			inNVRNo,
								pstr			outName,
								uint32		*	outBSize	)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;

	uint P = inNVRNo/NB_SLOT;
	uint S = inNVRNo%NB_SLOT;
	char tmp[64];
	Sprintf( tmp, "/%s/*", productCode );

	if( sceMcGetDir(P,S,tmp,0,MAX_DIR,firstnext_dir) != 0 )
		return FALSE;

	state			= SGF_SYNCING;
	state_params[0] = (uint32) outName;
	state_params[1] = (uint32) outBSize;
	state_params[2] = 0;
	state_params[3] = 0;

	return TRUE;
}


bool
nv::nvram::GetNextRecord	(	pstr			outName,
								uint32		*	outBSize	)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;

	for( ;; )
	{
		if( firstnext_idx >= firstnext_cpt )
		{
			state_res = NVR_NOTFOUND;
			break;
		}

		sceMcTblGetDir * ent = & firstnext_dir[ firstnext_idx++ ];
		if( ent->EntryName[0] == '.' )	// "." & ".."
			continue;

		if( outName )
			Strcpy( outName, (const char*)&ent->EntryName[0] );

		if( outBSize )
			*outBSize = ent->FileSizeByte;

//		if( outCreateTime )
//			Convert( outCreateTime, &ent->_Create );

//		if( outModifyTime )
//			Convert( outModifyTime, &ent->_Modify );

		state_res = nvram::NVR_SUCCESS;
		break;
	}

	return TRUE;
}


bool
nv::nvram::CreateRecord		(	uint			inNVRNo,
								pcstr			inName,
								uint32			inBSize,
								pvoid			inBuffer	)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;
	if( !inName || Strlen(inName)==0 )
		return FALSE;
	if( inBSize == 0 )
		return FALSE;

	// Alloc dummy data block
	pvoid buffer = NvMallocA( inBSize, 128 );
	if( !buffer )
		return FALSE;
	if( inBuffer )	Memcpy( buffer, inBuffer, inBSize );
	else			Memset( buffer, 0xCD,     inBSize );

	uint P = inNVRNo/NB_SLOT;
	uint S = inNVRNo%NB_SLOT;

	char path[64];
	libc::Sprintf( path, "/%s", productCode );
	if( sceMcMkdir(P,S,path) != 0 )
	{
		NvFree( buffer );
		return FALSE;
	}

	Strcpy( state_name, inName );

	state			= SCR_MKDIR;
	state_params[0]	= (uint32) state_name;
	state_params[1] = inBSize;
	state_params[2] = (uint32) buffer;
	state_params[3] = inNVRNo;

	return TRUE;
}


bool
nv::nvram::WriteRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset	)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;
	if( !inName || Strlen(inName)==0 )
		return FALSE;
	if( !inBuffer )
		return FALSE;
	if( !inBSize )
		return FALSE;

	// Open record

	uint P = inNVRNo/NB_SLOT;
	uint S = inNVRNo%NB_SLOT;
	char path[128];
	libc::Sprintf( path, "/%s/%s", productCode, inName );
	int res = sceMcOpen( P, S, path, SCE_WRONLY );
	if( res != 0 )
		return FALSE;

	state			= SWR_OPENING;
	state_params[0]	= (uint32) inBuffer;
	state_params[1] = inBSize;
	state_params[2] = inBOffset;

	return TRUE;
}


bool
nv::nvram::ReadRecord		(	uint			inNVRNo,
								pcstr			inName,
								pvoid			inBuffer,
								uint32			inBSize,
								uint32			inBOffset		)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;
	if( !inName || Strlen(inName)==0 )
		return FALSE;
	if( !inBuffer )
		return FALSE;
	if( !inBSize )
		return FALSE;

	// Open record

	uint P = inNVRNo/NB_SLOT;
	uint S = inNVRNo%NB_SLOT;
	char path[128];
	libc::Sprintf( path, "/%s/%s", productCode, inName );
	int res = sceMcOpen( P, S, path, SCE_RDONLY );
	if( res != 0 )
		return FALSE;

	state			= SRD_OPENING;
	state_params[0]	= (uint32) inBuffer;
	state_params[1] = inBSize;
	state_params[2] = inBOffset;

	return TRUE;
}


bool
nv::nvram::DeleteRecord		(	uint			inNVRNo,
								pcstr			inName			)
{
	if( productCode[0] == 0 )
		return FALSE;
	if( state != S_IDLE )
		return FALSE;
	if( inNVRNo >= GetNbMax() )
		return FALSE;
	if( !inName || Strlen(inName)==0 )
		return FALSE;

	// Delete record

	uint P = inNVRNo/NB_SLOT;
	uint S = inNVRNo%NB_SLOT;
	char path[128];
	libc::Sprintf( path, "/%s/%s", productCode, inName );
	int res = sceMcDelete( P, S, path );
	if( res != 0 )
		return FALSE;

	state = SDR_SYNCING;

	return TRUE;
}


