/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifdef _PSP


inline
void
DBG_TestCube_Draw(	uint32*		outBASE,
					uint32*		outVADR,
					uint32*		outIADR,
					uint32*		outVTYPE,
					uint32*		outPRIM		)
{
	struct CubeVertex {
		uint32 Color;
		float X,Y,Z;
	};

	static ALIGNED32(CubeVertex,CubeVB[]) = {
		{0xFF0000FF,+1,+1,+1},		//	  2--------1
		{0xFF00FF00,+1,+1,-1},		//  3/-------0/|
		{0xFFFF0000,-1,+1,-1},		//  | �      | |
		{0xFF00FFFF,-1,+1,+1},		//  | �      | |
		{0xFF0000FF,+1,-1,+1},		//  | �      | |
		{0xFF00FF00,+1,-1,-1},		//  | �      | |
		{0xFFFF0000,-1,-1,-1},		//	| 6------|-5
		{0xFF00FFFF,-1,-1,+1}		//	7/-------4/
	};

	static ALIGNED32(uint16,CubeIB[]) = {
		0, 1, 5,
		0, 5, 4,
		0, 4, 7,
		0, 7, 3,
		3, 7, 6,
		3, 6, 2,
		2, 6, 5,
		2, 5, 1,
		1, 0, 3,
		1, 3, 2,
		7, 4, 5,
		7, 5, 6
	};

	if( outBASE )	*outBASE  = SCE_GE_SET_BASE ( (uint(CubeVB)) >> 24 );
	if( outVADR )	*outVADR  = SCE_GE_SET_VADR ( (uint)CubeVB);
	if( outIADR )	*outIADR  = SCE_GE_SET_IADR ( (uint)CubeIB );
	if( outVTYPE )	*outVTYPE = SCE_GE_SET_VTYPE( 0,7,0,3,0,2,0,0,0);
	if( outPRIM )	*outPRIM  = SCE_GE_SET_PRIM ( 36,3);	
}


#endif

