/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/
#ifndef _NvSndManager_Tools_h
#define _NvSndManager_Tools_h

#include <Nova.h>
#include <NvSndManager.h>
#include <Kernel/NGC/NvkSound[NGC].h>
#include <Kernel/Common/NvkCore_Mem.h>

#ifdef _RVL
#include <revolution.h>
#else 
#include <dolphin/os.h>
#include <dolphin/arq.h>
#include <dolphin/ax.h>
#include <dolphin/mix.h>
#endif

#define GetDSPADPCMDataSize32B(a) 		( RoundX(((DSPADPCM*)a)->num_adpcm_nibbles,64) >> 1)
#define Bytes2Nibbles(n) 				( n << 1 )

#define VOICE_PRIO_HIGH 				31
#define VOICE_PRIO_MED  				15
#define VOICE_PRIO_LOW  				1

#define VOICE_SFX_PRIO					VOICE_PRIO_MED
#define VOICE_STRM_PRIO					VOICE_PRIO_HIGH

#define SEGMENT_REQUEST_FILE_PRIORITY 	16
#define	STREAM_FILE_PRIORITY			15
#define	NORMAL_FILE_PRIORITY			8

#define MAX_SEGMENT_PER_QUEUE 			4	// < 256 
#define MAX_PENDING_SEGMENT_REQUEST		32	// < 256
#define LOOP_READ_BUFFER_SIZE			64  // should not change //64 => Round32(sizeof(NvkSound::LoopCtx) + 31) : Add 31 reserved Bytes for 32B aligned read resquest

#define FIFO_NRM_LENGHT					24
#define FIFO_STRM_LENGHT				24

#define EPSILON_TIME 					0.01

#define BEGIN_MUTE_CYCLE				15
					
#ifdef _RVL	
#define TrxObj 							SdTransfert 
#define MEM_SOUND						nv::mem::MEM_NGC_MEM2
#define MIX_INIT_PARAMETER 				0,0,0,0,0,0,0,0
#define ARQ_INIT
#else
#define TrxObj 							NGCSdTransfert
#define MEM_SOUND						nv::mem::MEM_NGC_ARAM
#define MIX_INIT_PARAMETER 				0,0,0,0,0,0,0
#define ARQ_INIT						ARQInit();
#endif	


namespace snd_tools
{

template <typename T, int nbV>
struct Queue
{
	T queue[nbV];
	uint8 	head;
	uint8 	end;
	bool 	empty;
public :	
	Queue				(			);
	void	Reset		(			);
	bool 	Empty		(			);
	bool 	Full		(			);
	T *		Add			(			);
	bool	Add			( T &  inT	);
	bool	GetFirst	( T *  outT	);
	T * 	AccesFirst	(			);
	void 	RemoveLast	(			);
	void	RemoveFirst	(			);
};
	

struct Segment;
struct SegmentRequest {
	Segment * 				owner ;
	ALIGNED_X( uint8, 		loopCtxBuffer[LOOP_READ_BUFFER_SIZE], 32 ); // buffer for file request, contain loop ctx
	NvkSound::LoopCtx	*	lpctx;
	nv::file::ReadRequest	rr;
};

struct Segment {
	SegmentRequest 	* 	reqL;
	SegmentRequest 	* 	reqR;	

	uint				beginAddr	;
	uint				endAddr		;
	NvkSound::LoopCtx	lctxL		;
	NvkSound::LoopCtx	lctxR		;	
	bool				available	;
	bool 				reqFailed	;
};

struct SegmentRequestQ : public Queue<SegmentRequest,MAX_PENDING_SEGMENT_REQUEST>
{
	void Update ( );
};
//typedef Queue<SegmentRequest,MAX_PENDING_SEGMENT_REQUEST> SegmentRequestQ;

	
struct AbstractSdTransfert {
	enum TrxState {
		TS_Error		= 0,
		TS_Invalid		= 1,
		TS_DVD			= 3,
		TS_MRamLoading	= 4,
		TS_MRam			= 5,
		TS_ARamLoading	= 6,
		TS_ARam			= 7,		
	};
	
	enum TrxType {
		TT_Normal 		= 1,
		TT_Stream 		= 2,
		TT_Music 		= 3, // music is stereo streaming
	};

	volatile TrxState  		trxState;
	TrxType					trxType; 
		
	virtual bool 	Start 				( void * inMRamAddr	) = 0;
	virtual void 	Update				(					) = 0;
	virtual bool 	IsFinish 			(					) = 0;
	virtual bool 	IsValid				(					) = 0;
	virtual bool	IsError				(					) = 0;
	virtual bool	IsInTransfert		(					) = 0;
	
	virtual void * 	GetSoundAddr		(					) = 0;
	virtual bool  	SetSoundSoundAddr	(	void * inAddr1	,
	 										void * inAddr2	) = 0;
	virtual uint32	GetTrxBSize			(					) = 0;
	virtual uint32	GetNeedAuxBSize		(					) = 0;
};

struct SdTransfert : public AbstractSdTransfert {
	uint8					firstByte1;
	uint8					firstByte2;
	
	uint32					bsize;
	uint32					paddingBSize;
	nv::file::ReadRequest	rr;
	
	void * 					dataMAddr	; // The real Data addr during DVD transfert du to the 4 byte alignement for DVDRead !
	void * 					auxMAddr	; // an aux buffer in Mem2 ( use to store mPtr when deinterleaving stereo stream)		
	void * 					mPtr1		; // main Memory Addr
	void * 					mPtr2		; // main Memory Addr	

	SdTransfert							(									);
	SdTransfert							(	uint32 		inBSize 			,
											uint32		inPaddingBSize		, 
											uint32 		dvdDataOffset		, 
											TrxType 	inType 				,
											void * 		inMAddr1 			,
											void * 		inMAddr2 = NULL		);	// inMaddr2 => For stereo Only !

	~SdTransfert						(									);
	
	virtual bool 	Start 				( 	void * 		inMRamAddr			);	
	virtual void 	Update				(									);
	virtual bool 	IsFinish 			(									);	
	virtual bool 	IsValid				(									);
	virtual bool	IsError				(									);	
		
	bool			IsInMRam			(									);
	virtual bool 	IsInTransfert		(									);
	bool 			LoadToRam			( 									);
	
	virtual void * 	GetSoundAddr		(									);
	virtual bool 	SetSoundSoundAddr 	(	void * 		inAddr1				,
											void * 		inAddr2	 = NULL		);
	virtual uint32	GetTrxBSize			(									);
	virtual uint32	GetNeedAuxBSize		(									);
};

#ifndef _RVL
struct NGCSdTransfert : public SdTransfert
{	
	void *					aramAddr1;
	void *					aramAddr2;
	uint32					nbAramTrx;	
	uint32					waitingNbAramTrx;
	ARQRequest				taskM;
	
	NGCSdTransfert						(									);
	NGCSdTransfert						(	uint32 		inBSize 			,
											uint32		inPaddingBSize		,
											uint32 		dvdDataOffset		,
											TrxType 	inType 				,
											void * 		inMAddr1 			,
											void * 		inMAddr2 = NULL		);	// inMaddr2 => For stereo Only !
																						
	NGCSdTransfert 						(	void * 		inTmpBuffer			, 
											void * 		inAramData			, 
											uint32 		inBSize				,	 
											TrxType 	inType = TT_Normal	); // Data is already in MRam
	
	virtual bool 	Start 				( 	void * 		inMRamAddr			);
	virtual void 	Update				(									);
	virtual bool 	IsFinish 			(									);	
	
	bool 			IsInARam			(									);										
	bool 			LoadToARam			(  									);
	 
	static void 	TRXCallBack			(	u32 		task				);
	virtual void * 	GetSoundAddr		(									);
	virtual bool 	SetSoundSoundAddr 	(	void * 		inAddr1				,
											void * 		inAddr2	 = NULL		);	
	virtual uint32	GetNeedAuxBSize		(									);											
};
#endif 

	struct TrxFifo 
	{
		enum TrxError{
			TRX_None	= 0,
			TRX_Failed  = 1,
		};
		
		struct TrxQueue 
		{
			AbstractSdTransfert ** 	trxQueue	;
			uint8  					trxQHead 	;
			uint8  					trxQEnd  	;
			bool					trxQEmpty 	;
			uint8					lenght		;

			TrxQueue							(	uint8 					inLenght);
			TrxQueue							(									);
			bool 					AddTrx		(	AbstractSdTransfert * 	trx		);
			AbstractSdTransfert * 	GetTrx		(									);
		};

		TrxQueue				qNrm			;
		TrxQueue				qStrm			;

		AbstractSdTransfert *	curTrx		[2]	;
		void *					buffers		[2]	;
		uint32					buffersSize	[2]	;

		uint 					stolenTurnCpt	;
		uint 					oldReqCplt		;
		TrxError				lastError		;
			
		
		TrxFifo									(									);
		~TrxFifo								(									);
		bool 						AddTrx		(	AbstractSdTransfert * 	trx		);
		AbstractSdTransfert *		GetTrx		(									);
		void						Update		(									);
		TrxError					GetLastError(									);
	};
//------------------------------------------- Sound --------------------------
	typedef	uint8 SoundID;
	
	enum SdStateInternal {
		SDI_UNALLOCATED	= 0,
		SDI_ALLOCATED	= 1,
		SDI_TORELEASE	= 4,
	};
	
	enum StreamState	{
		SS_Normal 		 		= 0,	
		SS_EndStream 	 		= 1,
		SS_EndStreamNewSeg 		= 2,
		SS_StartNewSeg0 		= 3,	// The first new block for the new segment is in buffer0 
		SS_StartNewSeg1 		= 4,	// The first new block for the new segment is in buffer1
		SS_ResetLoopParameter 	= 5,	// Change Loop addr, (ps,yn1,yn2) and SoundType to return in the normal state
		SS_WaitNewSegmentStart	= 6,
		SS_Invalid				= 7,
	};

	enum BufferPosition		{
		B_First 	= 0,
		B_Second 	= 1,
		B_Invalid 	= 2,
	};
	
	struct SoundMemoryInfo
	{
	    void *		 sAddr;	// The Sound Address, NGC => Aram, RVL => Memory2
	    uint32       bSize;
	};

	struct SoundData 	
	{
		DSPADPCM	adpcmHeader[2]	; // Mono / Stereo (0 => L / 1 => R)
		uint32		dataBOffset		; // samples file offset;
		uint32		dataBSize		; // samples file size;
		uint32		loopBOffset[2]	; // Mono / Stereo
		uint32		loopBSize		; // looBSize channel Left == looBSize channel Right
	};
	
	struct SoundProperty 
	{ 
		uint 		freq		;
		float		duration	;
		bool		streamed	;
		bool		music		;		
	};
	
	struct SoundState 	// use for streaming sound and music
	{
		uint32				bOffset		;	//	currently used BigFile offset
		uint32				bSize		;	// 	currently used Bigfile size.

		Segment 			newSeg		;
		
		TrxObj				trx			; 	// RVL => SdTransfert, NGC => NGCSdTransfert

		SndManager::SdState	state		;	// User side state
		SdStateInternal		stateI		;	// Internal side state
	};
	
	struct SoundChain 
	{
		SoundMemoryInfo		sndMem		;
		SoundData			sndData		;
		SoundProperty		sndProp		;
		SoundState			sndState	;
		
		SoundID				nextFree	;
		SoundID				prevFree	;
		volatile uint64		usedVoice	;
	};

//------------------------------------------- Voices --------------------------------------------------

	struct StreamVoice 
	{	
		bool				callCallback		;
		bool				needUpdateData		;	//startPlayingBlock	;
		bool				needUpdateLoopParam	;
		StreamState			sstate				; 	// streaming state machine variable.
		
		uint32				currentPlayedAddr	;
		uint32				currentFilledAddr	;
		BufferPosition		currentPosition		;

		uint32 				lastPosition		;
		uint32				beginPosition		;
		uint32 				samplePosition		;
		
		uint				streamEnd			;	// last sample Addr in the ax sound buffer
	};
	
	struct Voice 
	{
		int16				sndOwnerId			;
		bool				stereo				;
		bool 				streamed			;

		AXVPB*				axVoiceL			; 	// in Mono only Left is played
		AXVPB*				axVoiceR			;
		void*				sAddrL				;
		void*				sAddrR				;
		uint32				bufferSize			;
		
		float 				vol					;
		float				panAngle			;
		float				pitchFactor			;

		uint				intMute				;
		StreamVoice			stream				;
	};
	

	namespace SoundMemory 
	{	
		inline 	void * 		SoundAlloc			( 	uint32 			inBSize 			);	// depend on target (NGC/RVL) => MEM2 / ARAM
		inline 	void 		SoundFree			( 	void * 			inPtr 				);	// depend on target (NGC/RVL) => MEM2 / ARAM
		inline	void * 		AllocAuxBuffer		(	uint32 			inBSize 			);	// depend on target (NGC/RVL)
		inline	void 		FreeAuxBuffer 		( 	void * 			inPtr				);	// depend on target (NGC/RVL)
	}

	/// Addr in Aram (Byte format), End is the first non played Byte
	void				SetLoopAddr			(	AXVPB * 		inAxV				, 
												uint 			inBeginAddr			, 
												uint 			inEndAddr			, 
												bool 			inLoopOn			, 
												uint32 			inCurrentAddr = 0	);

	inline void 		SetLoopParam		(	AXVPB * 		inAxV				, 
												uint16 			inPs 				, 
												uint16 			inYN1 = 0 			, 
												uint16 			inYN2 = 0			);


//------------------------------------------- Inline & Template ----------------------------------------------

void 	SetLoopParam		(		AXVPB * 		inAxV				, 
									uint16 			inPs 				, 
									uint16 			inYN1  				, 
									uint16 			inYN2 				)
{
	NV_ASSERT			(inAxV);
	AXPBADPCMLOOP   	loop={inPs,inYN1,inYN2};
	AXSetVoiceAdpcmLoop (inAxV, &loop);
}

void * 
SoundMemory::SoundAlloc( uint32 inBSize )
{
	uint rbs = inBSize + 32; // 32 for aligment
	return nv::mem::NativeMalloc( rbs , 32, MEM_SOUND );
}

void 
SoundMemory::SoundFree( void * inPtr )
{
	nv::mem::NativeFree( inPtr, MEM_SOUND );
}
			
void * 
SoundMemory::AllocAuxBuffer (uint32 inBSize )
{
#ifdef _RVL
	return NativeMalloc(inBSize,32,nv::mem::MEM_NGC_MEM2);
#else 
	return NvMalloc(inBSize);
#endif
}

void 
SoundMemory::FreeAuxBuffer ( void * inPtr) 
{
	if (inPtr) {
#ifdef _RVL		
		NativeFree(inPtr,nv::mem::MEM_NGC_MEM2);
#else 
		NvFree(inPtr);
#endif			
	}
}

template <typename T, int nbV>
Queue<T,nbV>::Queue()
{
	NV_COMPILE_TIME_ASSERT( nbV < 256);
	Reset();
}

template <typename T, int nbV>
void
Queue<T,nbV>::Reset()
{
	NV_COMPILE_TIME_ASSERT( nbV < 256);
	head  = 
	end   = 0;
	empty = TRUE;					
}

template <typename T, int nbV>
bool 
Queue<T,nbV>::Empty() 
{
	NV_ASSERT( (!empty) || ( empty && head == end));
	return empty;
}

template <typename T, int nbV>
bool 
Queue<T,nbV>::Full() 
{
	return ( head == end ) && ( !empty );
}

template <typename T, int nbV>
T *
Queue<T,nbV>::Add		()
{
	if (Full()) return NULL;
	T * ret 	= queue + end;
	end 		= (end + 1) % nbV;
	empty 		= FALSE;
	return ret	;
}

template <typename T, int nbV>
bool
Queue<T,nbV>::Add		(T & inT)
{
	if (Full()) return FALSE;
	queue[end] 	= intT;
	end 		= (end + 1) % nbV;
	empty 		= FALSE;
	return TRUE	;
}

template <typename T, int nbV>
bool
Queue<T,nbV>::GetFirst(T * outT)
{
	if (! outT ) return FALSE;
	
	T * e = AccesFirst();
	if (e) {
		*outT = *e; // Copy !!
		RemoveFirst( );
		return TRUE	;	
	}
	else 
		return FALSE;
}

template <typename T, int nbV>
T * 
Queue<T,nbV>::AccesFirst()
{
	if (Empty()) return NULL;
	return 	queue+head;	
}

template <typename T, int nbV>
void 
Queue<T,nbV>::RemoveLast()
{
	if (!empty) {
		end--;
		if (end < 0)
			end = nbV-1;
		if (end == head)
			empty = TRUE;
	}
}

template <typename T, int nbV>
void 
Queue<T,nbV>::RemoveFirst()
{
	if (!empty) {
		head = (head + 1) % nbV;
		if (head == end) 
			empty = TRUE;
	}
}


extern SegmentRequestQ		segReqQ;

}



#endif //_NvSndManager_Tools_h