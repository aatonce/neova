/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/NGC/GEK/GEK.h>
#include <dolphin/mix.h>
using namespace nv;

#include "NvSndManager_Tools.h"


namespace snd_tools
{


SegmentRequestQ		segReqQ;



void		/// Addr in Aram (Byte format), End is the first non played Byte
SetLoopAddr(AXVPB * inAxV, uint inBeginAddr, uint inEndAddr, bool inLoopOn, uint32 inCurrentAddr)  
{
	NV_ASSERT(inAxV);
	
	AXPBADDR	addr;

	addr.loopFlag 			= inLoopOn?AXPBADDR_LOOP_ON:AXPBADDR_LOOP_OFF;
	addr.format				= inAxV->pb.addr.format;
	
	uint32 aramAddress  ;
	if (inCurrentAddr) {
		aramAddress         	= Bytes2Nibbles(inCurrentAddr) +2;
		addr.currentAddressHi	= (u16)(aramAddress >> 16);
		addr.currentAddressLo	= (u16)(aramAddress & 0xFFFF);
	}
	else {
		addr.currentAddressHi	= inAxV->pb.addr.currentAddressHi;
		addr.currentAddressLo	= inAxV->pb.addr.currentAddressLo;
	}
	
	// Adding 2 to pass adcp header
	aramAddress  			= Bytes2Nibbles(inBeginAddr) + 2;
	addr.loopAddressHi  	= (u16)(aramAddress >> 16);              
	addr.loopAddressLo  	= (u16)(aramAddress & 0xFFFF);
	
	// sub 1 to have the last played nibble.
	if ( (inEndAddr % 8) == 0) inEndAddr -= 1;
	aramAddress         	= Bytes2Nibbles(inEndAddr);
	addr.endAddressHi		= (u16)(aramAddress >> 16);
	addr.endAddressLo		= (u16)(aramAddress & 0xFFFF);

	AXSetVoiceAddr(inAxV, &addr);
}

//-------------------------------------------------------------------------------------------------
//------------------------------------------- SdTransfert -----------------------------------------
//-------------------------------------------------------------------------------------------------
SdTransfert::SdTransfert(){
	trxState 		= TS_Invalid ;
	trxType 		= TT_Normal ;
	bsize			= 0;
	firstByte1		= 0;
	firstByte2		= 0;	
	mPtr1			= NULL;
	mPtr2			= NULL;	
	dataMAddr		= NULL;
	auxMAddr		= NULL;
}

SdTransfert::SdTransfert(uint32 inBSize, uint32 inPaddingBSize, uint32 dvdDataOffset, TrxType inType, void * inMAddr1, void * inMAddr2)
{
	trxState 		= TS_DVD;
	trxType 		= inType;
	
	bsize			= inBSize;
	paddingBSize	= inPaddingBSize;
	
	firstByte1		= 0;
	firstByte2		= 0;
	mPtr1			= inMAddr1;
	mPtr2			= inMAddr2;	
	dataMAddr		= NULL;
	auxMAddr		= NULL;
				
	rr.bSize		= Round32(bsize);
	rr.bOffset[0]	= dvdDataOffset;
	rr.bOffset[1]	= dvdDataOffset;
	rr.bOffset[2]	= dvdDataOffset;
	rr.bOffset[3]	= dvdDataOffset;

	if (!bsize) 
		trxState 	= TS_Invalid;
}

SdTransfert::~SdTransfert(){
	NV_ASSERT(!IsInTransfert());
}

void 
SdTransfert::Update(){
	// Update state 
	if (trxState == TS_MRamLoading && rr.IsReady()){
		if ((rr.GetState() == file::ReadRequest::COMPLETED)) {
			if ( trxType == TT_Music) {
				NV_ASSERT(mPtr2);
				NV_ASSERT( (bsize & 0xF) == 0 );
				uint8 * p1 = (uint8*)mPtr1;
				uint8 * p2 = (uint8*)mPtr2;
				uint8 * d  = (uint8*)dataMAddr;
				uint byteCpt = 0;
				while ( uint(d) < uint(dataMAddr) + bsize ) { 
					Memcpy(p1,d,8);
					Memcpy(p2,d+8,8);
					d  +=16;
					p1 +=8;
					p2 +=8;
					byteCpt +=16;
				}
				NV_ASSERT (bsize - byteCpt == 0);
				
				// pad with 0 if necessary
				/*uint8 * paddingStartAddr  = d ;
				while ( uint(d) < uint(paddingStartAddr) + paddingBSize ) { 
					Zero(p1,8);
					Zero(p2,8);
					d  +=16;
					p1 +=8;
					p2 +=8;
					byteCpt +=16;
				}*/
				if (paddingBSize) {
					NV_ASSERT( (paddingBSize & 0xF) == 0 );
					uint32 chanPadBSize = paddingBSize >> 1;
					Zero(p1,chanPadBSize);
					Zero(p2,chanPadBSize);
				}
				
				DCFlushRange(mPtr1,bsize+paddingBSize/2);
				DCFlushRange(mPtr2,bsize+paddingBSize/2);
				firstByte1 	= *((uint8*)mPtr1);
				firstByte2 	= *((uint8*)mPtr2);
			}
			else {
				Memcpy(((uint8*)mPtr1),dataMAddr,bsize);
				Zero(((uint8*)mPtr1) + bsize,paddingBSize);
				DCFlushRange(mPtr1,bsize+paddingBSize);
				firstByte1 	= *((uint8*)mPtr1);
			}
			trxState  	= TS_MRam;
			dataMAddr	= NULL ;
			auxMAddr	= NULL ;
		}
		else {
			trxState  	= TS_Error;
			firstByte1 	= 0;
			firstByte2 	= 0;
			dataMAddr	= NULL;
			mPtr1 		= NULL;
			mPtr2 		= NULL;
			auxMAddr	= NULL;
		}
	}
}

bool
SdTransfert::IsInMRam() 
{ 
	return trxState >= TS_MRam ;
}

bool 
SdTransfert::IsInTransfert(){
	return (trxState == TS_MRamLoading) || (trxState == TS_ARamLoading);
}

bool 
SdTransfert::LoadToRam	( )
{
	if ( trxState != TS_DVD || !mPtr1 || (trxType==TT_Music && !mPtr2) || (uint32(mPtr1) & 0x1F) || (uint32(mPtr2) & 0x1F) || !IsValid() ) {
		trxState = TS_Invalid;
		return FALSE;
	}
	
	rr.bufferPtr = auxMAddr;
	
	uint byteAlign  = rr.bOffset[0] & 0x3;
	if (byteAlign) {
		rr.bSize += 32 ;
		rr.bOffset[0]	-= byteAlign;
		rr.bOffset[1]	-= byteAlign;
		rr.bOffset[2]	-= byteAlign;
		rr.bOffset[3]	-= byteAlign;
		dataMAddr		= ((uint8*)rr.bufferPtr) + byteAlign;
	}
	else {
		dataMAddr		 = rr.bufferPtr;
	}
	
	if( !file::AddReadRequest(&rr,(trxType == TT_Normal)? NORMAL_FILE_PRIORITY : STREAM_FILE_PRIORITY ) )
		return FALSE;

	trxState = TS_MRamLoading;

	return TRUE;
}

bool
SdTransfert::Start ( void * inMRamAddr) 
{
	if (inMRamAddr) {
		auxMAddr = inMRamAddr;
		return LoadToRam	();
	}	
	NV_ASSERT(FALSE);
	return FALSE;
}

bool 
SdTransfert::IsFinish () 
{
	return trxState == TS_MRam || trxState == TS_Invalid || trxState == TS_Error;
}

bool 
SdTransfert::IsValid() 
{
	return trxState != TS_Invalid;
}

bool
SdTransfert::IsError ( )
{
	return trxState == TS_Error;
}

void * 	
SdTransfert::GetSoundAddr	(	)
{
	if ( IsFinish () )
		return mPtr1;
	return NULL;
}

bool 
SdTransfert::SetSoundSoundAddr ( void * inAddr1,void * inAddr2 ) 
{
	if (trxState == TS_DVD) 
	{
		mPtr1 = inAddr1;
		mPtr2 = inAddr2;
		return TRUE;
	}
	return FALSE;
}

uint32	
SdTransfert::GetTrxBSize		(					)
{
	return bsize;
}

uint32
SdTransfert::GetNeedAuxBSize	(	)
{
	return bsize + 64;
}

//-------------------------------------------------------------------------------------------------
//------------------------------------------- NGCSdTransfert --------------------------------------
//-------------------------------------------------------------------------------------------------

#ifndef RVL

NGCSdTransfert::NGCSdTransfert()  : SdTransfert()
{
	aramAddr1		= NULL;	
}

NGCSdTransfert::NGCSdTransfert		(	uint32 		inBSize 			,
										uint32		inPaddingBSize		,
										uint32 		dvdDataOffset		,
										TrxType 	inType 				,
										void * 		inMAddr1 			,
										void * 		inMAddr2 			):
			 	SdTransfert(inBSize,inPaddingBSize,dvdDataOffset,inType,NULL,NULL)
{
	aramAddr1 = inMAddr1;
	aramAddr2 = inMAddr2;
}

NGCSdTransfert::NGCSdTransfert (void * inTmpBuffer, void * inAramData, uint32 inBSize, TrxType inType) // Data is already in MRam
{
	trxState		= TS_MRam;
	trxType 		= inType;

	aramAddr1		= inAramData;
	aramAddr2		= NULL;
	bsize			= inBSize;
	mPtr1			= inTmpBuffer;	
	
	firstByte1		= 0;
	firstByte2		= 0;
	
	NV_ASSERT(trxType != TT_Music);
	if (!aramAddr1 || !bsize || !mPtr1)
		trxState 	= TS_Invalid;
}

bool 
NGCSdTransfert::LoadToARam	(  )
{
	if ( (trxState != TS_MRam && trxState != TS_ARamLoading) || !mPtr1 ) {
		trxState = TS_Invalid;
		return FALSE;
	}
	
	waitingNbAramTrx ++;
	trxState = TS_ARamLoading;
	
	if (trxType != TT_Music) {
		ARQPostRequest(	&taskM,					(uint32)this			,
			            ARQ_TYPE_MRAM_TO_ARAM,	(trxType == TT_Normal)?ARQ_PRIORITY_LOW : ARQ_PRIORITY_HIGH ,
			            (u32)mPtr1,		(u32)aramAddr1							,
			            Round32(bsize),					&TRXCallBack			);
	}
	else {
		if (nbAramTrx == 0 ) {
			ARQPostRequest(	&taskM,					(uint32)this			,
			            	ARQ_TYPE_MRAM_TO_ARAM,	(trxType == TT_Normal)?ARQ_PRIORITY_LOW : ARQ_PRIORITY_HIGH ,
			            	(u32)mPtr1,		(u32)aramAddr1							,
			            	Round32(bsize/2),					&TRXCallBack			);
		
		}
		else if (nbAramTrx == 1 ) {
			ARQPostRequest(	&taskM,					(uint32)this			,
			            	ARQ_TYPE_MRAM_TO_ARAM,	(trxType == TT_Normal)?ARQ_PRIORITY_LOW : ARQ_PRIORITY_HIGH ,
			            	(u32)mPtr2,		(u32)aramAddr2							,
			            	Round32(bsize/2),					&TRXCallBack			);
		}
	}
	return TRUE;
}

bool 
NGCSdTransfert::IsInARam() 
{ 
	return trxState == TS_ARam ;
}	

void 
NGCSdTransfert::TRXCallBack(u32 task) 
{
	ARQRequest  * req 		= (ARQRequest * )  task;
	NGCSdTransfert * trx 	= (NGCSdTransfert * ) req->owner;
	trx->nbAramTrx ++;
}

bool 
NGCSdTransfert::Start ( void * inMRamAddr) 
{
	if (inMRamAddr) {
		auxMAddr 	= inMRamAddr   ;
		mPtr1 		= (void*)Round32((uint32(inMRamAddr) + bsize + 64));
		if (trxType == TT_Music)
			mPtr2 	= (void*)Round32(uint32(mPtr1) + (bsize / 2));
		else 
			mPtr2 	= mPtr1;
		return LoadToRam	  (  );
	}	
	NV_ASSERT(FALSE);
	return FALSE;
}
	
void 
NGCSdTransfert::Update ( ) 
{
	if ( !IsFinish() ) {
		if (trxState < TS_MRam)
			SdTransfert::Update();
		
		if (trxState == TS_MRam) {
			nbAramTrx		= 0;
			waitingNbAramTrx= 0;		
			LoadToARam();
		}		
		else if (trxState == TS_ARamLoading) {
			if ( waitingNbAramTrx == nbAramTrx ) {
				if ( 	(trxType == TT_Music && nbAramTrx == 2) 	||
						(trxType != TT_Music && nbAramTrx == 1)  	){
					trxState = TS_ARam;
				}
				else 
					LoadToARam();
			}
		}
	}
}

bool 
NGCSdTransfert::IsFinish () 
{
	return trxState == TS_ARam || trxState == TS_Invalid || trxState == TS_Error;
}

void * 	
NGCSdTransfert::GetSoundAddr	(	)
{
	if (IsFinish ())
		return aramAddr1;
	return NULL;
}

bool 
NGCSdTransfert::SetSoundSoundAddr (void * inAddr1,void * inAddr2) 
{
	if (trxState <= TS_MRam) 
	{
		aramAddr1 = inAddr1;
		aramAddr2 = inAddr2;
		return TRUE;
	}
	return FALSE;
}

uint32
NGCSdTransfert::GetNeedAuxBSize	(	)
{
	return (bsize * 2) + 128;
}

#endif //ifndef RVL


//-------------------------------------------------------------------------------------------------
//------------------------------------------- TrxFifo::TrxQueue -----------------------------------
//-------------------------------------------------------------------------------------------------

TrxFifo::TrxQueue::TrxQueue(uint8 inLenght):trxQHead(0),trxQEnd(0),trxQEmpty(TRUE) {
	lenght = inLenght;
	trxQueue = (AbstractSdTransfert **) NvMalloc( lenght * sizeof(AbstractSdTransfert *) );
	Memset( trxQueue, 0 , lenght * sizeof(AbstractSdTransfert *) );
}

TrxFifo::TrxQueue::TrxQueue(){
	NvFree(trxQueue);
}

bool 
TrxFifo::TrxQueue::AddTrx(AbstractSdTransfert * trx)
{
	if (! trx ) return FALSE;
	if (trxQHead == trxQEnd && !trxQEmpty) return FALSE;
	trxQueue[trxQEnd] 	= trx;
	trxQEnd   			= (trxQEnd+1)%lenght;
	trxQEmpty 			= FALSE;
	return TRUE;
}

AbstractSdTransfert * 
TrxFifo::TrxQueue::GetTrx()
{
	if (trxQEmpty) return NULL;	
	AbstractSdTransfert * ret 	= trxQueue[trxQHead];
	trxQueue[trxQHead] 	= NULL;
	NV_ASSERT(ret);
	trxQHead = (trxQHead+1) % lenght;
	if (trxQHead == trxQEnd) trxQEmpty = TRUE;
	NV_ASSERT(  (trxQEmpty && trxQHead == trxQEnd) || (!trxQEmpty && trxQHead != trxQEnd) );
	return ret;
}

//-------------------------------------------------------------------------------------------------
//------------------------------------------- SegmentRequestQ -----------------------------------
//-------------------------------------------------------------------------------------------------
void 
SegmentRequestQ::Update ( )
{
	SegmentRequest * srq = AccesFirst();
	while (srq && srq->rr.IsReady()) {
		Segment * sg 	= srq->owner;
		
		if ( sg ){
			bool reqOk = srq->rr.GetState() == file::ReadRequest::COMPLETED;
			if (!reqOk)
				sg->reqFailed = TRUE;

			if (srq == sg->reqL) {
				sg->reqL = NULL;
				if (reqOk)
					sg->lctxL = * ((NvkSound::LoopCtx *) srq->lpctx );
			}
			else {
				NV_ASSERT(srq == sg->reqR);
				sg->reqR = NULL;
				if (reqOk)
					sg->lctxR = * ((NvkSound::LoopCtx *) srq->lpctx );				
			}
			
			if ( !sg->reqR && !sg->reqL ) {
				// All request have been done.
				if (sg->reqFailed) {	// one or more request have failed
					sg->beginAddr 	= sg->endAddr = 0;
					sg->available	= FALSE;
				}
				else {
					sg->available	= TRUE;
				}
			}
		}
		
		srq->owner = NULL;
		RemoveFirst();	
		srq = segReqQ.AccesFirst();
	}
}
	
//-------------------------------------------------------------------------------------------------
//------------------------------------------------ TrxFifo ----------------------------------------
//-------------------------------------------------------------------------------------------------

TrxFifo::TrxFifo():qNrm( FIFO_NRM_LENGHT	),qStrm( FIFO_STRM_LENGHT ) {
	uint stolenTurn;
	core::GetParameter(core::PR_NGC_LOAD_TRYCOUNTER , &stolenTurn);
	NV_ASSERT(stolenTurn);
	
	stolenTurnCpt	= stolenTurn;
	oldReqCplt		= file::GetRequestCompltdCpt()-2;
	
	curTrx[0] 		= NULL;
	curTrx[1] 		= NULL;
	buffers[0] 		= NULL;
	buffers[1] 		= NULL;
	buffersSize[0]  = 0;
	buffersSize[1]  = 0;
		
	uint bufferBSize;
	core::GetParameter(core::PR_NGC_SOUND_MEM_BUFFERBSIZE,&bufferBSize);
	NV_ASSERT(bufferBSize);			
}

TrxFifo::~TrxFifo(){

	SoundMemory::FreeAuxBuffer(buffers[0]);
	SoundMemory::FreeAuxBuffer(buffers[1]);
	buffersSize[0]  = 0;
	buffersSize[1]  = 0;			
}

bool 
TrxFifo::AddTrx(AbstractSdTransfert * trx)
{
	if (!trx) return FALSE;
	if (trx->trxType == AbstractSdTransfert::TT_Normal)
	{
		//Printf ( "add in qNrm (head,end): %d , %d\n",qNrm.trxQHead,qNrm.trxQEnd);
		return qNrm.AddTrx(trx);
	}
	else 
	{
		//Printf ( "add in qStrm (head,end): %d , %d\n",qStrm.trxQHead,qStrm.trxQEnd);
		return qStrm.AddTrx(trx);
	}
}

AbstractSdTransfert * 
TrxFifo::GetTrx()
{
	AbstractSdTransfert * ret = qStrm.GetTrx();
	if ( !ret )
		ret = qNrm.GetTrx();
	return ret;
}

void
TrxFifo::Update(){
	for (uint i = 0 ; i < 2 ; ++i ) {
		if ( !curTrx[i] ) {
			if ( file::GetRequestQueueSize() && file::GetRequestCompltdCpt() == oldReqCplt + 1)
				stolenTurnCpt --;
			if (!stolenTurnCpt) {
				uint stolenTurn;
				core::GetParameter(core::PR_NGC_LOAD_TRYCOUNTER ,&stolenTurn);
				NV_ASSERT(stolenTurn);
				stolenTurnCpt = stolenTurn;
			}
			else {
				curTrx[i] = GetTrx();
				if (curTrx[i] && curTrx[i]->IsValid()){
					if ( buffersSize[i] < ( curTrx[i]->GetNeedAuxBSize()) ){
						SoundMemory::FreeAuxBuffer(buffers[i]);
						buffers[i] 		= SoundMemory::AllocAuxBuffer(curTrx[i]->GetNeedAuxBSize());
						buffersSize[i] 	= curTrx[i]->GetNeedAuxBSize();
					}
					bool start = curTrx[i]->Start(buffers[i]);
					if (!start) {
						if  (curTrx[i]->IsValid() ) {
							// AddReadRequest failed, try again!
							AddTrx(curTrx[i]);
						}
						curTrx[i] = NULL;
					}
				}
			}
		}
		else {
			if ( !curTrx[i]->IsFinish() ){
				curTrx[i]->Update();
				if ( ! curTrx[i]->IsValid() )
					curTrx[i] = NULL;				
				if ( curTrx[i]->IsError() ) {
					lastError = TRX_Failed;
					curTrx[i] = NULL;
				}
			}
			else{
				if (curTrx[i]->IsError()){
					lastError = TRX_Failed;
				}
				curTrx[i] = NULL;
			}
		}
	}
}

TrxFifo::TrxError 
TrxFifo::GetLastError( )
{
	TrxError  ret 	= lastError;
	lastError 		= TRX_None;
	return ret;
}





}



