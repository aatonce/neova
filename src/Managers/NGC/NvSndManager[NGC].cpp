/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvSndManager.h>
#include <Kernel/NGC/NvkSound[NGC].h>
#include <Kernel/NGC/GEK/GEK.h>
#include <Kernel/Common/NvkCore_Mem.h>
#include <Kernel/NGC/NvSndManager[NGC].h>
#include <dolphin/mix.h>
#include "NvSndManager_Tools.h"

using namespace nv;
using namespace snd_tools;


bool	nvcore_File_Lock	();
bool	nvcore_File_Unlock	();

namespace
{
	using namespace SndManager;
//------------------------------------------ Constant --------------------------
	enum {
		NB_SOUND_MAX		= 128			,
		MAX_CHANNEL 		= 64			,		
		NB_CHANNEL			= MAX_CHANNEL-1	,
		BGM_VOICE			= MAX_CHANNEL-1	,		
		ZERO_BUFFER_SIZE	= 256			,
		InvalidSoundID 		= 255			,
	};
//------------------------------------------- Default Callback  ---------------------------		
	bool DefaultStreamCallBack (	int					inCh,						// channel id or -1 for bgm
									float&				outFromTime,				// next segment start time
									float&				outDuration		)
	{
		return FALSE;
	}
//------------------------------------------- Sound ----------------------------
	SoundChain					soundA				[NB_SOUND_MAX]	;
	SoundID						firstFree			= 0 ;
	bool						haveAMusicLocked	= FALSE;
//------------------------------------------- Voices ---------------------------
	Voice						voiceA				[MAX_CHANNEL];
	volatile uint64				voiceBusyMask		= 0;
	volatile uint64				voicePauseMask		= 0;
	volatile uint64				voiceToStartMask	= 0;
	volatile uint64				voiceStoppingMask	= 0;
//------------------------------------------- Global paramaters ----------------
	float						masterVol			= 0;
	SndManager::OutputMode		omode				= SndManager::OM_MONO;
	void  	* 					aramZeroBuffer		= NULL;
	bool						bgmFctCall			= FALSE;
	bool 						soundManagerIsInit 	= FALSE;
	SndManager::StreamHandler	streamLoopCallBack 	= &DefaultStreamCallBack;
	TrxFifo		*				trxFifo				= NULL;
	
//------------------------------------------- Functions ------------------------
	
	void ReleaseVoice(uint inVId)
	{
		NV_ASSERT(inVId < MAX_CHANNEL);

		uint64 chanMask		= uint64(1) << inVId;
	    uint64 notChanMask 	= ~chanMask;
		Voice & vc 			= voiceA[inVId];
		
		NV_ASSERT( voiceBusyMask & chanMask );
		NV_ASSERT( vc.axVoiceL && ((!vc.stereo && !vc.axVoiceR)||(vc.stereo && vc.axVoiceR)));
		NV_ASSERT( vc.sndOwnerId >=0 && vc.sndOwnerId < NB_SOUND_MAX && (soundA[vc.sndOwnerId].usedVoice & chanMask) );
		
		if (vc.axVoiceL){
			MIXReleaseChannel(vc.axVoiceL);
			AXFreeVoice		 (vc.axVoiceL);
			vc.axVoiceL= NULL;
		}

		if (vc.axVoiceR){
			MIXReleaseChannel(vc.axVoiceR);
			AXFreeVoice		 (vc.axVoiceR);
			vc.axVoiceR= NULL;
		}
		
		if (vc.sndOwnerId >=0 && vc.sndOwnerId < NB_SOUND_MAX ) {
			soundA[vc.sndOwnerId].usedVoice  &= notChanMask;
			if (soundA[vc.sndOwnerId].sndProp.streamed) 
				SdUnlock(vc.sndOwnerId);
		}

		vc.sndOwnerId 					= 0;
		vc.stereo	 					= FALSE;
		vc.bufferSize					= 0;				
		vc.sAddrL						= NULL;
		vc.sAddrR						= NULL;
		vc.stream.currentPosition		= B_Invalid;				
		vc.stream.callCallback 			= FALSE;
		vc.stream.sstate 				= SS_Invalid;
		vc.stream.currentPlayedAddr 	= 0;
		vc.stream.currentFilledAddr 	= 0;	 
		vc.stream.needUpdateData	 	= FALSE;
		vc.stream.streamEnd				= 0;
		vc.stream.needUpdateLoopParam	= FALSE;
		
		voicePauseMask	 	&= notChanMask;
		voiceBusyMask	 	&= notChanMask;
		voiceToStartMask	&= notChanMask;
		voiceStoppingMask 	&= notChanMask;
	}
	
	//--------------------------------------- CallBack -------------------------
	static void AudioFrameCallback()
	{
		MIXUpdateSettings();
		//if (voiceA[BGM_VOICE].axVoiceL && voiceA[BGM_VOICE].axVoiceL->pb.ve.currentDelta)
		//	Printf ("BGM ve : %d,%d\n",voiceA[BGM_VOICE].axVoiceL->pb.ve.currentVolume,voiceA[BGM_VOICE].axVoiceL->pb.ve.currentDelta);
		// check voice to start 
		while (voiceToStartMask) {
			uint 	id 		 	= GetLowestBitNo(voiceToStartMask);
			uint64	chanMask 	= uint64(1)<< id ;
			uint64 	notChanMask = ~(chanMask);
			Voice & vc 			= voiceA[id];
			
			NV_ASSERT(	voiceBusyMask & (uint64(1)<<id)	);
			NV_ASSERT(	vc.axVoiceL 					);
	
			// data have been read by libAx and libMix, => start the new voice !
			AXSetVoiceState(vc.axVoiceL,AX_PB_STATE_RUN);
			if (vc.axVoiceR)
				AXSetVoiceState(vc.axVoiceR,AX_PB_STATE_RUN);
	
            voiceToStartMask 	&= notChanMask;
			voicePauseMask 	 	&= notChanMask;
		}

		uint64 copyVoiceStopping = voiceStoppingMask ;
		while (copyVoiceStopping) {
			uint 	id 		 	= GetLowestBitNo(copyVoiceStopping);
			uint64	chanMask 	= uint64(1)<< id 			;
			uint64 	notChanMask = ~(chanMask)				;
			Voice & vc 			= voiceA[id];
			
			NV_ASSERT(	voiceBusyMask & chanMask		);
			NV_ASSERT(	vc.axVoiceL						);
			
			copyVoiceStopping &= notChanMask;
			// check if libAx know the new state.
			if (  !(vc.axVoiceL->sync & AX_SYNC_USER_STATE) && 
				 	((! vc.stereo ) || !(vc.axVoiceR->sync & AX_SYNC_USER_STATE)) )
				// Stop state is valid.
				voiceStoppingMask &= notChanMask;	
		}
		
	/*	for (uint i = 0 ; i < MAX_CHANNEL ; ++i ) {
			uint64 chanMask = uint64(1) << i;	
			Voice & vc = voiceA[i];
			
			if ( (voiceBusyMask & chanMask) &&  !(voiceToStartMask& chanMask)){
				if (vc.intMute) 
				{
					vc.intMute--;
					if (!vc.intMute) 
					{
						float	vol		= Clamp( masterVol, 0.0f, 1.0f ) * Clamp( vc.vol, 0.0f, 1.0f );
						int 	cb		= int(SndManager::LinearToMbVolume(vol) / 10.0f); // centibel
				
						MIXSetInput		(vc.axVoiceL, cb);
						if (vc.axVoiceR)
							MIXSetInput		(vc.axVoiceR, cb);
					}
				}
			}
		}*/
			
	}
	
	static void VoiceCallback(void * voiceIn)
	{
		AXVPB * axv = (AXVPB * ) voiceIn;
		NV_ASSERT( axv->userContext < MAX_CHANNEL );
		
		Voice & 		vc = voiceA [axv->userContext];
		SoundChain & 	sc = soundA[vc.sndOwnerId];
		voiceStoppingMask |= uint64(1) << axv->userContext ;
		
		AXSetVoiceState(vc.axVoiceL,AX_PB_STATE_STOP);
		if (vc.axVoiceR)
			AXSetVoiceState(vc.axVoiceR,AX_PB_STATE_STOP);
	} 	
	
//------------------------------------------- Ax Utilities ----------------------------------------------

	void ApplyVolumeOnly(	Voice  &			inVoice			,
							float				inMasterVolume	)
	{
		// volume
		float	vol		= Clamp( inMasterVolume, 0.0f, 1.0f ) * Clamp( inVoice.vol, 0.0f, 1.0f );
		int 	cb		= int(SndManager::LinearToMbVolume(vol) / 10.0f); // centibel
		
	    MIXSetInput		(inVoice.axVoiceL, cb);
	    if (inVoice.axVoiceR)
	    	MIXSetInput		(inVoice.axVoiceR, cb);
	}
	
	void 
	ApplyPanPitch(	Voice  &			inVoice			)
	{
		// pitch Factor
		DSPADPCM & ps =  soundA[inVoice.sndOwnerId].sndData.adpcmHeader[0];
		AXPBSRC src;
	    u32 	srcBits 		= (u32)(float(0x00010000) * (((f32)(ps.sample_rate) * f32(inVoice.pitchFactor))/ float(AX_IN_SAMPLES_PER_SEC)) );
	    src.ratioHi 			= (u16)(srcBits >> 16);
	    src.ratioLo 			= (u16)(srcBits & 0xFFFF);
	    src.currentAddressFrac 	= 0;
	    src.last_samples[0] 	= 0;
	    src.last_samples[1] 	= 0;
	    src.last_samples[2] 	= 0;
	    src.last_samples[3] 	= 0;

	    AXSetVoiceSrcType	(inVoice.axVoiceL, AX_SRC_TYPE_LINEAR);
	    AXSetVoiceSrc		(inVoice.axVoiceL, &src);
		if (inVoice.axVoiceR) {
			NV_ASSERT(ps.sample_rate == soundA[inVoice.sndOwnerId].sndData.adpcmHeader[1].sample_rate);
			AXSetVoiceSrcType	(inVoice.axVoiceR, AX_SRC_TYPE_LINEAR);
	    	AXSetVoiceSrc		(inVoice.axVoiceR, &src);
		}
		
		// panning in [-Pi, Pi]
		if (!inVoice.stereo) {
			int angle = inVoice.panAngle;
			while( angle < -Pi	)	angle += TwoPi;
			while( angle >  Pi 	)	angle -= TwoPi;

			inVoice.panAngle = angle;
			
			float fpan  = Sin(angle);
			float fspan = Cos(angle);
			
			int pan , span;
			
			pan  = int(  ( fpan  <= 0 )?( fpan  * 64.0f):(fpan  * 63.0f) + 64.0f  );
			span = int(  ( fspan <= 0 )?( fspan * 64.0f):(fspan * 63.0f) + 64.0f  );		
		
			MIXSetPan	(inVoice.axVoiceL, pan);
			MIXSetSPan	(inVoice.axVoiceL, span);
		}
		else {
			MIXSetPan	(inVoice.axVoiceL, 0);
			MIXSetSPan	(inVoice.axVoiceL, 127);
			
			MIXSetPan	(inVoice.axVoiceR, 127);
			MIXSetSPan	(inVoice.axVoiceR, 127);
		}
	}

	AXVPB* 
	AquireVoiceADPCM( DSPADPCM* pDSPADPCMData , void* pSRAMStart, bool inIsStreamed , uint32 inStreamSize)
	{
	    DSPADPCM            *ps = pDSPADPCMData;
	    AXPBADDR            addr;
	    AXPBADPCM           adpcm;
	    AXPBSRC             src;
	    AXPBADPCMLOOP       adpcmLoop;
	    u32                 aramAddress;
	    AXVPB*              axv;
	    u32                 srcBits;

		uint32				nibbleStreamEnd = (inStreamSize << 1) ;//convert Addr to nibble 
		if ( (nibbleStreamEnd % 16) == 0 )  nibbleStreamEnd -= 1;
		
	    axv = AXAcquireVoice(inIsStreamed?VOICE_STRM_PRIO:VOICE_SFX_PRIO,VoiceCallback,0);
	    if (!axv)  return NULL;

	    // Fill AXPBADDR structure
	    // All the folowing addresses are in nibbles

		addr.loopFlag       		= inIsStreamed ? AXPBADDR_LOOP_ON : ps->loop_flag;    
	    addr.format         		= ps->format;

        adpcmLoop.loop_pred_scale 	= ps->lps;
        adpcmLoop.loop_yn1        	= ps->lyn1;
        adpcmLoop.loop_yn2        	= ps->lyn2;
	        
	    // Support for looping
	    if (addr.loopFlag )
	        aramAddress             = (ps->sa + Bytes2Nibbles((u32)pSRAMStart));
	    else
	        aramAddress             = Bytes2Nibbles((u32)aramZeroBuffer) + 2; // + 2 to jump the 2 Header nibbles
	    
	    addr.loopAddressHi      	= (u16)(aramAddress >> 16);              
	    addr.loopAddressLo      	= (u16)(aramAddress & 0xFFFF);       

		if (inIsStreamed)
		    aramAddress             = (nibbleStreamEnd + Bytes2Nibbles((u32)pSRAMStart));		
		else
		    aramAddress             = (ps->ea + Bytes2Nibbles((u32)pSRAMStart));
		    
	    addr.endAddressHi       	= (u16)(aramAddress >> 16);       
	    addr.endAddressLo       	= (u16)(aramAddress & 0xFFFF);       

	    aramAddress             	= (ps->ca + Bytes2Nibbles((u32)pSRAMStart));
	    addr.currentAddressHi  	 	= (u16)(aramAddress >> 16);       
	    addr.currentAddressLo   	= (u16)(aramAddress & 0xFFFF);

	    // Fill AXPBADPCM structure
	    adpcm.a[0][0]           = ps->coef[0];
	    adpcm.a[0][1]           = ps->coef[1];
	    adpcm.a[1][0]           = ps->coef[2];
	    adpcm.a[1][1]           = ps->coef[3];
	    adpcm.a[2][0]           = ps->coef[4];
	    adpcm.a[2][1]           = ps->coef[5];
	    adpcm.a[3][0]           = ps->coef[6];
	    adpcm.a[3][1]           = ps->coef[7];
	    adpcm.a[4][0]           = ps->coef[8];
	    adpcm.a[4][1]           = ps->coef[9];
	    adpcm.a[5][0]           = ps->coef[10];
	    adpcm.a[5][1]           = ps->coef[11];
	    adpcm.a[6][0]           = ps->coef[12];
	    adpcm.a[6][1]           = ps->coef[13];
	    adpcm.a[7][0]           = ps->coef[14];
	    adpcm.a[7][1]           = ps->coef[15];
	    adpcm.gain              = ps->gain; 
	    adpcm.pred_scale        = ps->ps;
	    adpcm.yn1               = ps->yn1;
	    adpcm.yn2               = ps->yn2;

	    // Set Address and ADPCM information
		AXSetVoiceType		(axv , inIsStreamed ? AX_PB_TYPE_STREAM : AX_PB_TYPE_NORMAL);		    
	    AXSetVoiceAddr		(axv, &addr);
	    AXSetVoiceAdpcm		(axv, &adpcm);
	    AXSetVoiceAdpcmLoop	(axv, &adpcmLoop);

		// Init mix parameter 
		MIXInitChannel		(axv,MIX_INIT_PARAMETER);
		MIXResetControls	(axv);
		//MIXSetInput			(axv, 0);

	//	float	vol		= 0.01f;
//		int 	cb		= int(SndManager::LinearToMbVolume(vol) / 10.0f); // centibel
	
//		MIXSetInput		(axv, cb);
		
	    return axv;
	}
	
	bool InitNewSegment(SoundChain & sc , float inFromTime , float inDuration)
	{
		Segment & 	seg 	= sc.sndState.newSeg;
		seg.available 		= FALSE;
		seg.reqL  			= NULL;
		seg.reqR  			= NULL;
		seg.beginAddr 		= 0;
		seg.endAddr 		= 0;
		seg.reqFailed		= FALSE;
		
		float 		toTime 	= inFromTime + inDuration;		
		if ( toTime > (sc.sndProp.duration + EPSILON_TIME)) return FALSE;
		if ( toTime > sc.sndProp.duration) toTime = sc.sndProp.duration;
		
		uint sndFreq 				= sc.sndProp.freq;
		uint fromSample				= uint( inFromTime * sndFreq );
		uint toSample				= uint( toTime  * sndFreq );
		uint adpcmFromFrame 		= fromSample / 14;
		uint adpcmToFrame 			= toSample / 14;
		uint loopInfoFileOffsetL 	= sc.sndData.loopBOffset[0] + (adpcmFromFrame * sizeof(NvkSound::LoopCtx));
		uint loopInfoFileOffsetR 	= sc.sndData.loopBOffset[1] + (adpcmFromFrame * sizeof(NvkSound::LoopCtx));		
		
		if (seg.reqR)	seg.reqR->owner = NULL;
		if (seg.reqL)	seg.reqL->owner = NULL;

		SegmentRequest * 	srqL 	= segReqQ.Add();
		if ( !srqL ) return FALSE;
		NV_ASSERT (! srqL->owner );
		
		srqL->owner 				= &seg;
		seg.reqL 					= srqL;

		uint offsetAlignL			= loopInfoFileOffsetL & 0x3;
		NV_ASSERT 					(Round32(sizeof(NvkSound::LoopCtx) + offsetAlignL) <= LOOP_READ_BUFFER_SIZE);		
		seg.reqL->rr.bSize 	 		= Round32(sizeof(NvkSound::LoopCtx) + offsetAlignL);
		seg.reqL->rr.bOffset[0] 	=
		seg.reqL->rr.bOffset[1] 	=
		seg.reqL->rr.bOffset[2] 	=
		seg.reqL->rr.bOffset[3] 	= loopInfoFileOffsetL - offsetAlignL;
		
		seg.reqL->rr.bufferPtr		= seg.reqL->loopCtxBuffer;
		seg.reqL->lpctx				= (NvkSound::LoopCtx*)(seg.reqL->loopCtxBuffer + offsetAlignL);
		
		if (sc.sndProp.music) {	// Stereo 
			SegmentRequest * 	srqR 	= segReqQ.Add();
			if ( !srqR ) {
				seg.reqL = NULL;
				segReqQ.RemoveLast();
				seg.reqL->owner = NULL;
				return FALSE;
			}
			NV_ASSERT (! srqR->owner );

			srqR->owner 				= &seg;
			seg.reqR 					= srqR;

			uint offsetAlignR			= loopInfoFileOffsetR & 0x3;
			NV_ASSERT 					(Round32(sizeof(NvkSound::LoopCtx) + offsetAlignR) <= LOOP_READ_BUFFER_SIZE);
			seg.reqR->rr.bSize 	 		= Round32(sizeof(NvkSound::LoopCtx) + offsetAlignR);
			seg.reqR->rr.bOffset[0] 	=
			seg.reqR->rr.bOffset[1] 	=
			seg.reqR->rr.bOffset[2] 	=
			seg.reqR->rr.bOffset[3] 	= loopInfoFileOffsetR - offsetAlignR;
			
			seg.reqR->rr.bufferPtr		= seg.reqR->loopCtxBuffer;
			seg.reqR->lpctx				= (NvkSound::LoopCtx*)(seg.reqR->loopCtxBuffer + offsetAlignR);
		}

		bool addOk;
		addOk = file::AddReadRequest(&seg.reqL->rr, SEGMENT_REQUEST_FILE_PRIORITY );
		addOk &= (!seg.reqR) || file::AddReadRequest(&seg.reqR->rr, SEGMENT_REQUEST_FILE_PRIORITY );		

		if(!addOk) {
			segReqQ.RemoveLast();
			seg.reqL 		= NULL;			
			seg.reqL->owner = NULL;
			if (seg.reqR){
				seg.reqR 		= NULL;
				seg.reqR->owner = NULL;
				segReqQ.RemoveLast();
			}
			return FALSE;
		}

		// set Begin and End information.
		if (sc.sndProp.music) {	
			seg.beginAddr 				= (adpcmFromFrame * 2)	* 8;
			seg.endAddr					= ((adpcmToFrame+1) * 2 * 8) -1;
		}
		else {
			seg.beginAddr 				= adpcmFromFrame 	* 8;
			seg.endAddr					= ((adpcmToFrame+1) 	* 8) -1;
		}

		// Set Segment active.
		seg.available = TRUE;
		return TRUE;
	}
	
	bool CallCallBack(int inSd, Voice & inVc , SoundChain & inSc){
		inVc.stream.callCallback 		= FALSE;
		float 	segBegin 	= -1.0f	, 
				segDuration	= -1.0f	;
		bool loopOk = streamLoopCallBack(inSd,segBegin,segDuration);
		if (loopOk) {
			if (segBegin < 0.0f)
				segBegin = 0.0f;
			if (segDuration <= 0.0f)
				segDuration = inSc.sndProp.duration - segBegin;
			loopOk = InitNewSegment(inSc , segBegin ,segDuration) ;
		}
		if (!loopOk) 	inSc.sndState.newSeg.available = FALSE;
		else 			inSc.sndState.newSeg.available = TRUE;
		return loopOk;
	}
	
	void UpdateStreaming (uint inVoiceId ) 
	{
		Voice & 		vc 			= voiceA[inVoiceId];
		SoundChain & 	sc 			= soundA[vc.sndOwnerId];		
		uint64 			chanMsk 	= uint64(1) << inVoiceId ;
		uint64 			notChanMask = ~(chanMsk);
		
		if (vc.stream.callCallback) 
		{
			if ( !CallCallBack(vc.sndOwnerId, vc , sc) ) {
				bgmFctCall = (inVoiceId==BGM_VOICE);
				ChStop(inVoiceId);
				bgmFctCall = FALSE;
			};
		}

		bool nextStep 				= vc.stream.needUpdateData; // Next step : the current playing cursor reach a new block 
		vc.stream.needUpdateData 	= FALSE;

		switch (vc.stream.sstate) {
			case SS_ResetLoopParameter :
				{
					if (nextStep) {
						AXSetVoiceType	(vc.axVoiceL, AX_PB_TYPE_STREAM);
						SetLoopAddr		(vc.axVoiceL, uint(vc.sAddrL), uint(vc.sAddrL) + vc.bufferSize, TRUE);
						vc.stream.sstate = SS_Normal;
						if (vc.stereo) {
							AXSetVoiceType(vc.axVoiceR, AX_PB_TYPE_STREAM);
							SetLoopAddr(vc.axVoiceR, uint(vc.sAddrR), uint(vc.sAddrR) +vc.bufferSize, TRUE);
						}
					}
				}
			//	No Break !!!! Load a new segment too !
			case SS_Normal :
				{
					// New transfer
					if (nextStep && sc.sndState.bSize){
						bool newSegnNow	= sc.sndState.newSeg.available && !sc.sndState.newSeg.reqL && !sc.sndState.newSeg.reqR;
						if (newSegnNow) { // change file information for this segment :
							vc.stream.sstate 		= (vc.stream.currentPosition == B_First)? SS_StartNewSeg1 : SS_StartNewSeg0 ;
							vc.stream.streamEnd		= vc.bufferSize >> 1;
							sc.sndState.bOffset 	= sc.sndData.dataBOffset + sc.sndState.newSeg.beginAddr;
							sc.sndState.bSize 		= sc.sndState.newSeg.endAddr - sc.sndState.newSeg.beginAddr +1;
						}

						uint aramOffset = (vc.stream.currentPosition == B_First)? (vc.bufferSize >> 1) : 0 ;
						
						if ( sc.sndState.trx.IsFinish() && !sc.sndState.trx.IsError()) {
							uint bufferBS = vc.bufferSize >> (!vc.stereo);
							uint trxSize = Min( bufferBS , sc.sndState.bSize );
							NV_ASSERT( (vc.stereo && (trxSize & 0x1) == 0) || !(vc.stereo ));
							sc.sndState.trx = TrxObj(	trxSize, bufferBS-trxSize , sc.sndState.bOffset,
														sc.sndProp.music? SdTransfert::TT_Music : SdTransfert::TT_Stream 	,
														(void*)(uint(vc.sAddrL)+aramOffset)							,
														vc.stereo?(void*)(uint(vc.sAddrR)+aramOffset):NULL			);	
							if (! trxFifo->AddTrx(&sc.sndState.trx) ) {
								DebugPrintf("<Neova> Push new Trx in trxFifo failed ! \n");
								vc.stream.needUpdateData = TRUE; // Redo AddTrx at the next Update
								sc.sndState.trx = TrxObj();
							}
							else {
								sc.sndState.bOffset += trxSize ;
								sc.sndState.bSize   -= trxSize;
							}

							vc.stream.needUpdateLoopParam = (vc.stream.currentPosition == B_First);
							if (sc.sndState.bSize <= 0) {
								vc.stream.callCallback = FALSE;
								CallCallBack(vc.sndOwnerId,vc,sc);
								vc.stream.sstate 	= SS_EndStream;
								NV_ASSERT( (trxSize & 0x1)==0);
								vc.stream.streamEnd = trxSize >> (vc.stereo) ;
								if (vc.stream.streamEnd < (bufferBS / 2)){ // play "zero sound" at the stream's end to give time to change loop parameters
									vc.stream.streamEnd = (bufferBS / 2);
								}
							}
						}
						else {
							DebugPrintf("<Neova> Streaming lag !! \n");
							if ( !sc.sndState.trx.IsInMRam()) 
								sc.sndState.trx.SetSoundSoundAddr( (void*)(uint(vc.sAddrL)+aramOffset) ,
																   vc.stereo?(void*)(uint(vc.sAddrR)+aramOffset): NULL );
						}
					}
					// Set PreScale for the next loop
					if ( vc.stream.needUpdateLoopParam && sc.sndState.trx.IsInMRam()){
						vc.stream.needUpdateLoopParam = FALSE ;
						SetLoopParam(vc.axVoiceL, (u16) sc.sndState.trx.firstByte1 );
						if (vc.axVoiceR)
							SetLoopParam(vc.axVoiceR, (u16) sc.sndState.trx.firstByte2 );
					}
				}
				break;
		
			case SS_EndStream :
				{
					if (nextStep) {
						// The End : loop to the Zero buffer => Stop this stream
						uint32 beginAddr 	= (u32)aramZeroBuffer;
						uint32 endAddrL		= uint32(vc.sAddrL) + vc.stream.streamEnd;
						uint32 endAddrR		= uint32(vc.sAddrR) + vc.stream.streamEnd;
						uint32 addPos		= ((vc.stream.currentPosition==B_Second) ? (vc.bufferSize>>1) : 0U);
					    endAddrL		   += addPos;
						endAddrR		   += addPos;
						
						SetLoopAddr(vc.axVoiceL, beginAddr, endAddrL, FALSE);
						if (vc.axVoiceR)
							SetLoopAddr(vc.axVoiceR, beginAddr, endAddrR, FALSE);
						
						// Need to be restart with new segment ?
						if (sc.sndState.newSeg.available) {
						 	// Pause This voice otherwise it will be released !!!
							voicePauseMask |= chanMsk;

							// change streaming Paramater to start a new segment
							uint32 	bOffset = sc.sndData.dataBOffset;
								
							sc.sndState.bOffset	= bOffset + sc.sndState.newSeg.beginAddr;
							sc.sndState.bSize 	= sc.sndState.newSeg.endAddr - sc.sndState.newSeg.beginAddr + 1;

							//Set a new trx to transfert segment's beginning in Aram
							uint bufferBS 	= vc.bufferSize >> (!vc.stereo);
							uint trxSize 	= Min( bufferBS , sc.sndState.bSize );
							uint aramOffset = (vc.stream.currentPosition == B_First)?  (vc.bufferSize >> 1) : 0;
							NV_ASSERT( (vc.stereo && (trxSize & 0x1) == 0) || !(vc.stereo ));
							sc.sndState.trx = TrxObj(	trxSize,bufferBS-trxSize, sc.sndState.bOffset 								,
														sc.sndProp.music? SdTransfert::TT_Music : SdTransfert::TT_Stream 	,
														(void*)(uint(vc.sAddrL)+aramOffset)							,
														vc.stereo?(void*)(uint(vc.sAddrR)+aramOffset):NULL			);
														
							if (! trxFifo->AddTrx(&sc.sndState.trx) ) {
								vc.stream.needUpdateData = TRUE; // Redo AddTrx at the next Update
								sc.sndState.trx = TrxObj();
							}
							else {
								sc.sndState.bOffset += trxSize ;
								sc.sndState.bSize   -= trxSize;
								if (sc.sndState.bSize <= 0)
									vc.stream.streamEnd = trxSize >> (vc.stereo) ;
									if (vc.stream.streamEnd < (bufferBS / 2)){ // play "zero sound" at the stream's end to give time to change loop parameters
										vc.stream.streamEnd = (bufferBS / 2);
									}
								vc.stream.sstate 	= SS_WaitNewSegmentStart;
							}
						}
					}
				}
				break;
			case SS_StartNewSeg0 :
				{
					NV_ASSERT(sc.sndState.newSeg.available);
					if (vc.stream.currentPosition == B_Second && sc.sndState.trx.IsInMRam() && !sc.sndState.newSeg.reqL && !sc.sndState.newSeg.reqR) {
						// set stream type to normal to update (yn1,yn2) parameters !							
						sc.sndState.newSeg.available	= FALSE;
						AXSetVoiceType(vc.axVoiceL, AX_PB_TYPE_NORMAL);
						SetLoopParam(vc.axVoiceL, sc.sndState.newSeg.lctxL.ps , sc.sndState.newSeg.lctxL.yn1 , sc.sndState.newSeg.lctxL.yn2 );
						if (vc.axVoiceR) {
							AXSetVoiceType(vc.axVoiceR, AX_PB_TYPE_NORMAL);
							SetLoopParam(vc.axVoiceR, sc.sndState.newSeg.lctxR.ps , sc.sndState.newSeg.lctxR.yn1 , sc.sndState.newSeg.lctxR.yn2 );
						}
						vc.stream.sstate = SS_ResetLoopParameter; 
					}
					
				}
				break;

			case SS_StartNewSeg1 :
				{
					NV_ASSERT(sc.sndState.newSeg.available);
					if (vc.stream.currentPosition == B_First && sc.sndState.trx.IsInMRam() && !sc.sndState.newSeg.reqL && !sc.sndState.newSeg.reqR) {
						// New Loop to reset Yn1, Yn2 and ps;
						sc.sndState.newSeg.available	= FALSE;
						AXSetVoiceType(vc.axVoiceL, AX_PB_TYPE_NORMAL);
						SetLoopParam(vc.axVoiceL, sc.sndState.newSeg.lctxL.ps , sc.sndState.newSeg.lctxL.yn1 , sc.sndState.newSeg.lctxL.yn2 );
						
						uint32 beginAddr 	= uint(vc.sAddrL) + (vc.bufferSize >> 1);
						uint32 endAddr		= beginAddr;
						SetLoopAddr	( vc.axVoiceL, beginAddr, endAddr, TRUE );	
						
						if (vc.axVoiceR) {
							AXSetVoiceType(vc.axVoiceR, AX_PB_TYPE_NORMAL);
							SetLoopParam(vc.axVoiceR, sc.sndState.newSeg.lctxR.ps , sc.sndState.newSeg.lctxR.yn1 , sc.sndState.newSeg.lctxR.yn2 );
							beginAddr 	= uint(vc.sAddrR) + (vc.bufferSize >> 1);
							endAddr		= beginAddr;
							SetLoopAddr	( vc.axVoiceR, beginAddr, endAddr, TRUE );
						}
						vc.stream.sstate = SS_ResetLoopParameter; 
					}
				}
				break;
			case SS_WaitNewSegmentStart :
				{
					NV_ASSERT(sc.sndState.newSeg.available);
					if 	( 	vc.axVoiceL->pb.state == AX_PB_STATE_STOP 									&& 
					  		( !((vc.axVoiceR) && (vc.axVoiceR->pb.state == AX_PB_STATE_RUN )) ) 		&&
							sc.sndState.trx.IsFinish() && !sc.sndState.newSeg.reqL && !sc.sndState.newSeg.reqR	){
							
						if (! voicePauseMask &  notChanMask ) break; // voice is stopping.
						
						uint32 beginAddrL 	= uint32(vc.sAddrL);
						uint32 beginAddrR	= uint32(vc.sAddrR);
						uint32 endAddrL		= beginAddrL + vc.bufferSize;
						uint32 endAddrR		= beginAddrR + vc.bufferSize;
						uint32 currentAddrL	= beginAddrL + ((vc.stream.currentPosition == B_First)? (vc.bufferSize>>1) : 0 );
						uint32 currentAddrR	= beginAddrR + ((vc.stream.currentPosition == B_First)? (vc.bufferSize>>1) : 0 );

						sc.sndState.newSeg.available	= FALSE;
						SetLoopAddr	(vc.axVoiceL,beginAddrL, endAddrL , TRUE , currentAddrL);
						SetLoopParam(vc.axVoiceL,sc.sndState.newSeg.lctxL.ps,sc.sndState.newSeg.lctxL.yn1 , sc.sndState.newSeg.lctxL.yn2);
						AXSetVoiceType(vc.axVoiceL, AX_PB_TYPE_STREAM);
					    AXSetVoiceState(vc.axVoiceL, AX_PB_STATE_RUN);
					    
					    if (vc.axVoiceR) {
						    SetLoopAddr	(vc.axVoiceR,beginAddrR, endAddrR , TRUE , currentAddrR);
							SetLoopParam(vc.axVoiceR,sc.sndState.newSeg.lctxR.ps,sc.sndState.newSeg.lctxR.yn1 , sc.sndState.newSeg.lctxR.yn2);
							AXSetVoiceType(vc.axVoiceR, AX_PB_TYPE_STREAM);
						    AXSetVoiceState(vc.axVoiceR, AX_PB_STATE_RUN);
						}
			            voiceToStartMask 	&= notChanMask;
						voicePauseMask 		&= notChanMask;
						
						if (sc.sndState.bSize)
							vc.stream.sstate 			= SS_Normal;
						else {
							vc.stream.sstate 			= SS_EndStream;
							vc.stream.needUpdateData 	= TRUE;
							CallCallBack(vc.sndOwnerId, vc , sc);
							if (vc.stream.currentPosition == B_First)
								vc.stream.currentPosition = B_Second;
							else 
								vc.stream.currentPosition = B_First;
						}
					}
				}
				break;
		}
	}
	
	
	void UpdateVoice ( )
	{
		for (uint i = 0 ; i < MAX_CHANNEL ; ++i ) {
			uint64 chanMask = uint64(1) << i;
			
			Voice & vc = voiceA[i];
			
			if ( (voiceBusyMask & chanMask) &&  !(voiceToStartMask& chanMask)){
				NV_ASSERT(vc.axVoiceL);
				NV_ASSERT(!vc.stereo || (vc.stereo && vc.axVoiceR)) ;
				if ( vc.axVoiceL->pb.state == AX_PB_STATE_STOP &&  !(voiceStoppingMask & chanMask ) && !(voiceToStartMask & chanMask ) && !(voicePauseMask & chanMask )) {
					if (!vc.stereo || (vc.axVoiceR->pb.state == AX_PB_STATE_STOP)) {
						ReleaseVoice(i);
						continue;
					}
				}
				if (vc.streamed) {
				
				/*Update Sound Postion for streamed sound*/
					{
						uint32 addr = uint32(vc.axVoiceL->pb.addr.currentAddressHi);
						addr  	= addr << 16;
						addr   |= uint32(vc.axVoiceL->pb.addr.currentAddressLo);
						addr	= addr >> 1 ; 	// Convert Nibble to Byte	
						
						addr    = addr & 0xFFFFFFF8;	// check only completed ADPCM Frame (8bytes, 1bytes header + 7bytes samples) (14 samples)
						
						NV_ASSERT( (vc.stream.lastPosition & 0x7) == 0)
						
						if ( (addr >= uint32(vc.stream.beginPosition)) && 
						     (addr <= uint32(vc.stream.beginPosition) + vc.bufferSize ) )
						{
							if ( vc.stream.lastPosition <= addr) 
							{
								uint32 nbADPCMFrame 		= (addr - vc.stream.lastPosition) / 8;
								uint32 nbSample				= nbADPCMFrame * 14;
								vc.stream.samplePosition 	+= nbSample;
							}
							else // have looped
							{
								//NV_ASSERT( vc.stream.lastPosition <= uint32(vc.stream.beginPosition) + vc.bufferSize )
								//NV_ASSERT( addr >= uint32(vc.stream.beginPosition ) )
								if ( addr >= uint32(vc.stream.beginPosition) && ( vc.stream.lastPosition <= uint32(vc.stream.beginPosition) + vc.bufferSize )) 
								{
									uint32 nbADPCMFrame 		= ( ( uint32(vc.stream.beginPosition)+vc.bufferSize ) - vc.stream.lastPosition) / 8 + 
																  (addr - uint32(vc.stream.beginPosition)) / 8 ;
									uint32 nbSample				= nbADPCMFrame * 14 ;

									vc.stream.samplePosition 	+= nbSample;
								}
							}

							vc.stream.lastPosition = addr;
						}
					}
					//vc.sAddrL;
				
				
					SoundChain & sc = soundA[vc.sndOwnerId];
					
					uint32 addrL 	= *((u32*)&vc.axVoiceL->pb.addr.currentAddressHi); // in Nibbles.
					addrL 			-= uint(vc.sAddrL) << 1;
					addrL 			= addrL >> 1 ; // Convert Nibble to Byte
					uint32 addrR 	= 0xFFFFFFFF;
/*					if (vc.stereo) {
						addrR 	= *((u32*)&vc.axVoiceR->pb.addr.currentAddressHi); // in Nibbles.
						addrR	-= uint(vc.sAddrR) << 1;
						addrR 	= addrR >> 1 ; // Convert Nibble to Byte
					}
*/
					// it is assumed that current left and right playing sample are synchronous.
					uint32 newAddr = addrL;
					NV_ASSERT(vc.stream.currentPosition != B_Invalid);
					if ( !(voicePauseMask & chanMask )) {
						if (vc.stream.currentPosition == B_First) {
							if ( newAddr > (vc.bufferSize>>1)){
								vc.stream.currentPosition   = B_Second;
								vc.stream.needUpdateData	= TRUE;
							}
						}
						else { // sc.curPlaying == SoundChain::B_Second
							if ( newAddr < (vc.bufferSize>>1)){
								vc.stream.currentPosition   = B_First;
								vc.stream.needUpdateData	= TRUE;
							}
						}
					}
					UpdateStreaming		(i);	
				}
			}

		}
	}
	
	void UpdateTransfert ( ) 
	{
		// update transfert
		trxFifo->Update();
		
		if (trxFifo->GetLastError() == TrxFifo::TRX_Failed ) {
			for (uint i = 0 ; i < NB_SOUND_MAX; ++i ) {
				/*if ( soundA[i].sndState.state == SD_LOCKING  || 
					  soundA[i].sndState.state == SD_LOCKED ){
					if (soundA[i].sndProp.streamed) {
						SdUnlock(i);
					}
				}*/
				
				if ( soundA[i].sndState.state == SD_LOCKING  ){
					SdUnlock(i);
				}
				else if (soundA[i].sndState.state == SD_LOCKED and soundA[i].sndProp.streamed){
					SdUnlock(i);
				}
			}
		}

		segReqQ.Update();
	}
	
	void UpdateSound ( ) 
	{
		for (uint i = 0 ; i < NB_SOUND_MAX ; ++i ){

			SoundChain & sc = soundA[i];
			if ( sc.sndState.state == SD_UNUSED) continue ;
			
			NV_ASSERT(sc.sndState.stateI	!= SDI_UNALLOCATED	);
			if (sc.sndState.state == SD_LOCKING ) {
				if (!sc.sndState.trx.IsValid()) {
					SdFree(i);
					return ;
				}
				if (sc.sndState.trx.IsFinish() && !sc.sndState.newSeg.reqL && !sc.sndState.newSeg.reqR) {
					if ( sc.sndProp.streamed && !sc.sndState.newSeg.available ) {
						sc.sndState.state = SD_UNLOCKING;
					}
					else {
						if (!sc.sndState.trx.IsError() ) 
						{
							NV_ASSERT( (sc.sndProp.streamed && sc.sndState.newSeg.available) || !sc.sndProp.streamed ) ;
							sc.sndState.state = SD_LOCKED;	
						}
						else
						{
							sc.sndState.state = SD_UNLOCKING;
						}
					}
				}
			}
			else if ( sc.sndState.state == SD_UNLOCKING ) {
				if ( sc.sndState.trx.IsFinish() && !sc.usedVoice ) {
					if (sc.sndState.newSeg.reqL)
						sc.sndState.newSeg.reqL->owner = NULL;
					if (sc.sndState.newSeg.reqR)
						sc.sndState.newSeg.reqR->owner = NULL;					
					
					sc.sndState.newSeg.available = FALSE;
					sc.sndState.newSeg.reqL = NULL;
					sc.sndState.newSeg.reqR = NULL;
					sc.sndState.state = SD_UNLOCKED;
					if (sc.sndMem.sAddr)
						SoundMemory::SoundFree(sc.sndMem.sAddr);
					sc.sndMem.sAddr = 0;
					if (sc.sndProp.music) haveAMusicLocked = FALSE;
					if (sc.sndProp.streamed)
						nvcore_File_Unlock();
				}
				else if (sc.usedVoice) {
					uint64 usedV = sc.usedVoice;
					while (usedV) {
						uint bn = GetLowestBitNo(usedV);
						bgmFctCall = (bn == BGM_VOICE);
						ChStop(bn);
						bgmFctCall = FALSE;
						usedV &= ~(uint64(1) << bn);
					}
				}
			}
			else if (sc.sndState.stateI == SDI_TORELEASE) {
				if (sc.sndState.state == SD_UNLOCKED){
					NV_ASSERT( ! sc.usedVoice );
					
				//	SoundMemory::SoundFree(sc.sndMem.sAddr);

					sc.sndState.stateI 			= SDI_UNALLOCATED;					
					sc.sndState.state			= SD_UNUSED;
					soundA[firstFree].prevFree	= i;
					sc.nextFree					= firstFree;
					sc.prevFree					= InvalidSoundID;
					firstFree					= i;
					
					sc.sndState.trx				= TrxObj();
				}
			}
		}
	}
}


bool
SndManager::Init	(			)
{
	if (soundManagerIsInit) return FALSE;
	
	uint8 * zeroBuffer 	= NULL;
	#ifndef RVL	
	NGCSdTransfert	trx;
	#endif 

	NV_COMPILE_TIME_ASSERT(MAX_CHANNEL <= 64);	// Compatibility with uint64 for voiceBusyMask and voicePauseMask.

    ARQ_INIT	// only on NGC
	
	
	if ( AICheckInit() )
	{
		AIReset();
		AIInit	(NULL);
	}
	else 
    	AIInit	(NULL);
	AXInit	(	 );
	MIXInit	(	 );	
	// Register Callback with AX for audio processing
	AXRegisterCallback(&AudioFrameCallback);	
	
   	masterVol						= 1.0f;
	voiceBusyMask					= 0;
	voicePauseMask					= 0;
	voiceToStartMask				= 0;
	voiceStoppingMask				= 0;
	omode 							= OM_STEREO;
	trxFifo							= NvEngineNew(TrxFifo);
	bgmFctCall						= FALSE;
	soundManagerIsInit 				= TRUE;
	streamLoopCallBack 				= &DefaultStreamCallBack;
	haveAMusicLocked				= FALSE;	
	
	if (!trxFifo) goto error;
	
	for ( uint i = 0 ; i < MAX_CHANNEL ; ++i ) {
		voiceA[i].axVoiceL 					= NULL;
		voiceA[i].axVoiceR 					= NULL;
		voiceA[i].sndOwnerId 				= 0;
		voiceA[i].stereo	 				= FALSE;		
		voiceA[i].stream.callCallback 		= FALSE;
		voiceA[i].stream.sstate 			= SS_Invalid;
		voiceA[i].stream.currentPlayedAddr 	= 0;
		voiceA[i].stream.currentFilledAddr 	= 0;
		voiceA[i].stream.currentPosition	= B_Invalid;
		voiceA[i].stream.needUpdateData 	= FALSE;
		voiceA[i].bufferSize				= 0;
		voiceA[i].sAddrL					= 0;		
		voiceA[i].sAddrR					= 0;				
		voiceA[i].stream.streamEnd			= 0;
		voiceA[i].stream.needUpdateLoopParam= FALSE;
	}

	for (uint i = 0 ; i < NB_SOUND_MAX ; ++i ) {
		soundA[i].sndState.trx 		= TrxObj();
		soundA[i].sndState.state	= SD_UNUSED;
		soundA[i].sndState.stateI	= SDI_UNALLOCATED;
		soundA[i].sndState.bOffset	= 0;
		soundA[i].sndState.bSize	= 0;
						
		Memset(&soundA[i].sndMem	,0,sizeof(SoundMemoryInfo));
		Memset(&soundA[i].sndData,0,sizeof(SoundData));
		
		soundA[i].nextFree			= i + 1;
		soundA[i].prevFree			= i - 1;
		soundA[i].usedVoice			= 0;
	}

	soundA[NB_SOUND_MAX-1].nextFree	= InvalidSoundID;
	soundA[0].prevFree				= InvalidSoundID;
	firstFree						= 0;

	// Load the zero Buffer into Aram 	
	aramZeroBuffer 			= SoundMemory::SoundAlloc(ZERO_BUFFER_SIZE); 
	if (! aramZeroBuffer) 	goto error;


#ifndef RVL 	
	zeroBuffer 	= (uint8*) NvMalloc		(ZERO_BUFFER_SIZE);
	if (! zeroBuffer ) goto error;
	
	Zero(zeroBuffer,ZERO_BUFFER_SIZE);
	trx = NGCSdTransfert(zeroBuffer, aramZeroBuffer, ZERO_BUFFER_SIZE );
	// sync load 
	while (!trx.IsFinish()) {
		trx.Update();
	}
	if (!trx.IsValid() || trx.IsError() || !trx.IsInARam()) goto error;
	NvFree(zeroBuffer);
#else 

	Zero(aramZeroBuffer	,ZERO_BUFFER_SIZE);
#endif 
	soundManagerIsInit 				= TRUE;
	
	nv::core::SoundMode	soundM=	nv::core::GetSoundMode();
	switch (soundM)
	{
		case nv::core::SM_Mono 		: omode = OM_MONO	;  	break;
		case nv::core::SM_Stereo 	: omode = OM_STEREO	;	break;
		case nv::core::SM_Surround 	: omode = OM_STEREO	;	break;
	}
	SetOutputMode(omode);
	
	return TRUE;
	
error :
	MIXQuit();
	AXQuit();
	if (trxFifo)		NvEngineDelete(trxFifo);
	if (aramZeroBuffer)	SoundMemory::SoundFree(aramZeroBuffer);
	if (zeroBuffer)		SoundMemory::SoundFree(zeroBuffer);	
	return FALSE;
}


bool
SndManager::Shut	(			)
{	
	if( !soundManagerIsInit )
		return TRUE;
		
	ChStopAll	();
	BgmStop		();
	SdFreeAll	();
	
	for (uint i = 0 ; i < NB_SOUND_MAX ; ++i ) {
		if (soundA[i].sndState.state	!= SD_UNUSED)
			return FALSE;
		NV_ASSERT(soundA[i].sndState.stateI	== SDI_UNALLOCATED);
	}
	
	bool mayUpdate = TRUE;
	uint firstSDUsed = 0;
	while (mayUpdate) {
		mayUpdate = FALSE;
		for ( uint i = firstSDUsed ; i < NB_SOUND_MAX ; ++i ) {
			if (soundA[i].sndState.state	!= SD_UNUSED) {
				mayUpdate = TRUE;
				firstSDUsed = i;
				break;			
			}
		}
		mayUpdate = mayUpdate || (voiceBusyMask != 0);		
		Update();
	}
	

	MIXQuit					(					);
	AXQuit					(					);
	NvEngineDelete			(	trxFifo			);
	SoundMemory::SoundFree	(	aramZeroBuffer	);
	soundManagerIsInit = FALSE;
	return TRUE;
}

SndManager::StreamHandler
SndManager::SetHandler			(	StreamHandler		inHandler				)
{
	if (! inHandler ) inHandler = &DefaultStreamCallBack;

	StreamHandler Old = streamLoopCallBack;
	streamLoopCallBack = inHandler;
	return Old;
}

void
SndManager::Update	(	)
{
	UpdateTransfert 	( 	);
	UpdateSound			(	);	
	UpdateVoice 		(	);
}

bool
SndManager::ProbeOutputMode		(	OutputMode			inOMode		)
{
	return TRUE;
}


void
SndManager::SetOutputMode		(	OutputMode			inOMode		)
{
	switch(inOMode){
		case OM_MONO :
		case OM_MONOPAN :				
			AXSetMode(AX_MODE_STEREO);
			MIXSetSoundMode(MIX_SOUND_MODE_MONO);
			break;
		case OM_STEREO :		
			AXSetMode(AX_MODE_STEREO);
			MIXSetSoundMode(MIX_SOUND_MODE_STEREO);			
			break;
		case OM_DPL2 :		
			AXSetMode(AX_MODE_DPL2);
			MIXSetSoundMode(MIX_SOUND_MODE_DPL2);			
			break;
		default:
			NV_ASSERT(FALSE);
			break;
	}
	omode = inOMode;
}


SndManager::OutputMode
SndManager::GetOutputMode		(							)
{
	return omode;
}


void
SndManager::SetMasterVolume		(	float		inVol		)
{
	masterVol = Clamp( inVol, 0.0f, 1.0f );
	for( int i = 0 ; i < MAX_CHANNEL ; i++ ) {
		if( voiceBusyMask & (uint64(1)<<i) ) {
			ApplyVolumeOnly(voiceA[i],masterVol);
		}
	}
}


int
SndManager::SdGetNbMax			(				)
{
	return NB_SOUND_MAX;
}


int
SndManager::SdAny		(					)
{
	NV_ASSERTC(firstFree != InvalidSoundID, "Not Enouth Sound buffer\n");
	if (firstFree == InvalidSoundID) return -1;

	int ret = firstFree;
	if ( soundA[firstFree].prevFree != InvalidSoundID )
		firstFree = soundA[firstFree].prevFree;
	else if ( soundA[firstFree].nextFree != InvalidSoundID )
		firstFree = soundA[firstFree].nextFree;
	else 
		firstFree = InvalidSoundID;
	return ret;
}


SndManager::SdState
SndManager::SdGetState			(	int		inSoundId	)
{
	NV_ASSERT_RETURN_MTH(SD_UNUSED,inSoundId >= 0 && inSoundId < NB_SOUND_MAX );
	return soundA[inSoundId].sndState.state;
}


int
SndManager::SdLock		(	int				inSdId		,
							NvSound*		inSndObj	,
							float			inFromTime	,	// (for streamed sound only !)
							float			inDuration	)
{
	if (!inSndObj) return -1;
	if  (inDuration < 0.0f && inFromTime < 0.0f) return -1;
	NvkSound * 	ks	= inSndObj->GetKInterface();
	NV_ASSERT(ks);	
	if ( haveAMusicLocked && ks->IsMusic()) return -1;
	
	uint soundId=inSdId;
	uint allocatedID = InvalidSoundID;
	if (inSdId == SD_ANY) {
		soundId=SdAny();
		if (soundId == -1)
			goto error;
		allocatedID = SdAlloc(soundId);
		if (allocatedID != soundId) goto error;
	}

	NV_ASSERT_RETURN_MTH(-1, soundId >=0 && soundId < NB_SOUND_MAX	);
	SoundChain & sc = soundA[soundId];
	if (sc.sndState.stateI != SDI_ALLOCATED) goto error;
	if (sc.sndState.state  != SD_UNLOCKED  ) goto error;

	NV_ASSERT( sc.usedVoice == 0 );
	NV_ASSERT( sc.sndState.trx.IsFinish() );	
	

	bool		stereo = ks->IsMusic();
	uint32 		dataBOffset	;
	uint32 		dataBSize	;		
	uint32		loopBOffset	;
	uint32		loopBSize	;
	
	ks->GetBFSoundData	(sc.sndData.dataBOffset,sc.sndData.dataBSize);
	sc.sndData.adpcmHeader[0] = *(DSPADPCM *)ks->GetDSPADPCMHeader(0);
	ks->GetBFLoopData	(sc.sndData.loopBOffset[0],sc.sndData.loopBSize,0);

	if (stereo) {
		sc.sndData.adpcmHeader[1] = *(DSPADPCM *)ks->GetDSPADPCMHeader(1);
		ks->GetBFLoopData	(sc.sndData.loopBOffset[1],sc.sndData.loopBSize,1);
	}
	else {
		sc.sndData.loopBOffset[1] 	= 0;
		Zero(sc.sndData.adpcmHeader[1]);
	}
	
	sc.sndProp.freq 	= ks->GetFreq();
	sc.sndProp.streamed	= ks->IsStreamed();
	sc.sndProp.music	= ks->IsMusic();
	sc.sndProp.duration	= ks->GetDuration();
	
	if (sc.sndProp.streamed) {
  		uint streamMsTime;
  		core::GetParameter(core::PR_NGC_STREAM_LENGTH , & streamMsTime);

		if (!streamMsTime) streamMsTime = 3000;
		uint needNbSample   = (streamMsTime * ks->GetFreq()) / 1000;
		uint needADPCMFrame = (needNbSample / 14) + ((needNbSample%14 != 0)?1:0);
		uint aramPartBSize 	= Round32(needADPCMFrame * 8);

		if (inFromTime < 0.0f)
			inFromTime = 0.0f;	
		if (inDuration <= 0.0f) 
			inDuration = ks->GetDuration() - inFromTime;
		if (inFromTime > ks->GetDuration()) { SdFree(inSdId); return -1; }

		if ( !InitNewSegment(sc , inFromTime , inDuration) 			) { 
			if ( allocatedID >=0 && allocatedID < NB_SOUND_MAX ) 
				SdFree(allocatedID);
			//SdFree(inSdId);
			return -1;
		}
		//change boffset and bsize for this segment
		uint32 bOffset 	 = 	sc.sndData.dataBOffset + sc.sndState.newSeg.beginAddr;
		uint32 bSize   	 =  sc.sndState.newSeg.endAddr - sc.sndState.newSeg.beginAddr +1;

	    sc.sndMem.bSize 	= aramPartBSize * (2 << stereo);
        sc.sndMem.sAddr 	= SoundMemory::SoundAlloc(sc.sndMem.bSize);
		if (!sc.sndMem.sAddr) goto error;
		sc.sndState.bOffset	= bOffset;
		sc.sndState.bSize	= bSize;
		
		uint32 loadSize 	= Min(sc.sndMem.bSize / 2,sc.sndState.bSize);
		if (sc.sndProp.music)
	   		sc.sndState.trx 	= TrxObj(loadSize, (sc.sndMem.bSize / 2) - loadSize, bOffset,SdTransfert::TT_Music,sc.sndMem.sAddr,((uint8*)sc.sndMem.sAddr)+loadSize);
	   	else 
	   		sc.sndState.trx 	= TrxObj(loadSize, (sc.sndMem.bSize / 2) - loadSize, bOffset,SdTransfert::TT_Stream,sc.sndMem.sAddr);

	   	sc.sndState.bSize 	-= loadSize;
	   	sc.sndState.bOffset	+= loadSize;
	   	
	   	nvcore_File_Lock();

	}

	if(!sc.sndProp.streamed) {
		uint32 dataSize = sc.sndData.adpcmHeader->num_adpcm_nibbles >> 1;
        sc.sndMem.bSize = GetDSPADPCMDataSize32B(sc.sndData.adpcmHeader);
        NV_ASSERT((dataSize) <= sc.sndData.dataBSize);
        sc.sndMem.sAddr 	= SoundMemory::SoundAlloc(sc.sndMem.bSize); 
	    sc.sndState.trx 	= TrxObj(dataSize,0,sc.sndData.dataBOffset,SdTransfert::TT_Normal,sc.sndMem.sAddr);
	}
	
	sc.sndState.state  = SD_LOCKING;
	if ( ! trxFifo->AddTrx(&sc.sndState.trx) ) goto error2;
	
	if (ks->IsMusic()) haveAMusicLocked = TRUE;
   	return soundId;
   	
error2:
	sc.sndState.trx = TrxObj();
	if ( sc.sndMem.sAddr ) 
	{
		SoundMemory::SoundFree(sc.sndMem.sAddr);
		sc.sndMem.sAddr = 0;
	}
error:
	if ( allocatedID >=0 && allocatedID < NB_SOUND_MAX ) 
		SdFree(allocatedID);
	return -1;
}


bool
SndManager::SdUnlock	(	int			inSoundId		)
{
	NV_ASSERT_RETURN_MTH(FALSE,inSoundId >=0 && inSoundId < NB_SOUND_MAX	);
	SoundChain & sc = soundA[inSoundId];
	if ( sc.sndState.state == SD_LOCKING || sc.sndState.state == SD_LOCKED ) { 
		NV_ASSERT( sc.sndState.stateI != SDI_UNALLOCATED);
		sc.sndState.state = SD_UNLOCKING;

		// Stop voice that are playing this sound 
		uint64 usedV = sc.usedVoice;
		while (usedV) {
			uint bn = GetLowestBitNo(usedV);
			if (bn == BGM_VOICE)
				bgmFctCall = TRUE;
			ChStop(bn);
			if (bn == BGM_VOICE)
				bgmFctCall = FALSE;
			usedV &= ~(uint64(1) << bn);
		}
	}
	return TRUE;
}


void
SndManager::SdFree		(	int			inSoundId		)
{
	NV_ASSERT_RETURN_CAL(inSoundId >=0 && inSoundId < NB_SOUND_MAX	);
	if (soundA[inSoundId].sndState.stateI != SDI_UNALLOCATED ) { 
		soundA[inSoundId].sndState.stateI  = SDI_TORELEASE;
		if (soundA[inSoundId].sndState.state != SD_UNLOCKED)
			SdUnlock(inSoundId);
	}
}


void
SndManager::SdFreeAll		(						)
{
	for (uint i = 0 ; i < NB_SOUND_MAX ; ++i ) {
		SdFree(i);
	}
}


int
SndManager::SdAlloc			(		int		inSoundId		)
{
	
	NV_ASSERT_RETURN_MTH(-1,(inSoundId >=0 && inSoundId < NB_SOUND_MAX) || inSoundId == SD_ANY);
	
	if (inSoundId == SD_ANY){
		inSoundId=SdAny();
		if (inSoundId == -1) 
			return -1;
	}

	if ( soundA[inSoundId].sndState.stateI == SDI_UNALLOCATED		) {
		NV_ASSERT(soundA[inSoundId].sndState.state	== SD_UNUSED	);
		soundA[inSoundId].sndState.stateI = SDI_ALLOCATED;
		soundA[inSoundId].sndState.state  = SD_UNLOCKED;
		if ( soundA[inSoundId].prevFree != InvalidSoundID )
			soundA[soundA[inSoundId].prevFree].nextFree = soundA[inSoundId].nextFree;
		if ( soundA[inSoundId].nextFree != InvalidSoundID )
			soundA[soundA[inSoundId].nextFree].prevFree = soundA[inSoundId].prevFree;
		if ( firstFree == inSoundId) {
			if ( soundA[inSoundId].prevFree != InvalidSoundID )
				firstFree = soundA[inSoundId].prevFree;
			else if ( soundA[inSoundId].nextFree != InvalidSoundID )
				firstFree = soundA[inSoundId].nextFree;
			else 
				firstFree = InvalidSoundID;
		}
		return inSoundId;
	}
	return -1;
}

bool
SndManager::SdBreak		(	int		inSoundId	)
{
	NV_ASSERT_RETURN_MTH(FALSE,inSoundId >=0 && inSoundId < NB_SOUND_MAX);

	SoundChain & sc = soundA[inSoundId];
	if (sc.sndProp.streamed && sc.usedVoice) {
		int vcId = GetLowestBitNo(sc.usedVoice);
		NV_ASSERT ( vcId >= 0 && vcId < NB_SOUND_MAX );
		Voice & vc = voiceA[vcId];

		if ( !(voiceBusyMask 	& sc.usedVoice))return FALSE;
		if ( voiceStoppingMask 	& sc.usedVoice) return TRUE	;		
		
		vc.stream.callCallback = TRUE;
		return TRUE;
	}
	else 
		return FALSE;
}


int
SndManager::ChGetNb		(						)
{
	return NB_CHANNEL;
}


int
SndManager::ChAny	(		)
{
	uint notVoiceBusyMask = ~voiceBusyMask;	
	while (notVoiceBusyMask) {
		uint ret = GetLowestBitNo(notVoiceBusyMask);
		if (ret >= NB_CHANNEL)
			break;
		uint64 retMask = uint64(1)<<ret;
		if ( ! (voiceToStartMask & retMask) )
			return ret;
		notVoiceBusyMask &= ~retMask;
	}
	return -1;
}

SndManager::ChState
SndManager::ChGetState	(	int		inChannel		)
{
	uint64 chanMsk = uint64(1) << inChannel ;
	if (inChannel < 0 || inChannel >= MAX_CHANNEL || !(voiceBusyMask & chanMsk) /*|| (voiceToStartMask & chanMsk) */) {
		return CH_IDLE;	
	}
	return CH_PLAYING;
}

int
SndManager::ChPlay		(	int			inChannel,
							int			inSoundId,
							float		inVol,	
							float		inPanAngle,
							float		inPitchFactor	)
{
	int channelId = inChannel;
	if (inChannel == CH_ANY)
		channelId = ChAny();
	if (channelId < 0) return -1;
		
	NV_ASSERT_RETURN_MTH(-1, (bgmFctCall && channelId==BGM_VOICE) || (!bgmFctCall && channelId<NB_CHANNEL && channelId >= 0));
	
	uint64 chanMsk = uint64(1) << channelId ;

	if ( (voiceBusyMask & chanMsk) )
		return -1;
	
	SoundChain 	& sc = soundA[inSoundId] ;
	Voice		& vc = voiceA[channelId];
	
	NV_ASSERT ( !vc.sndOwnerId );
	NV_ASSERT ( !(voicePauseMask & chanMsk) );
	NV_ASSERT ( !(voiceStoppingMask & chanMsk) );
	
	if (	sc.sndState.state != SD_LOCKED 				  	|| 
			(sc.sndProp.music && inChannel != BGM_VOICE)  	|| 
			(inChannel == BGM_VOICE && !sc.sndProp.music) 	||
			(sc.sndProp.streamed && sc.usedVoice )			)
		return -1;


	vc.vol								= inVol;
	vc.panAngle							= inPanAngle;
	vc.pitchFactor						= inPitchFactor;
	vc.sndOwnerId			 			= inSoundId;	
	vc.stereo							= sc.sndProp.music;
	vc.streamed							= sc.sndProp.streamed;

	vc.bufferSize						= vc.stereo?sc.sndMem.bSize/2:sc.sndMem.bSize;
	vc.sAddrL							= (void*)sc.sndMem.sAddr;
	vc.sAddrR							= vc.stereo?(void*) (uint32(sc.sndMem.sAddr)+vc.bufferSize):NULL;

	vc.intMute							= BEGIN_MUTE_CYCLE;
	
	if (vc.streamed) {
		vc.stream.callCallback	 		= FALSE;			
		vc.stream.sstate	 	 		= SS_ResetLoopParameter;		
		vc.stream.currentPlayedAddr		= 0;
		vc.stream.currentFilledAddr		= sc.sndMem.bSize / 2;
		vc.stream.currentPosition		= B_First;
		vc.stream.needUpdateData		= TRUE;
		vc.stream.streamEnd				= 0;
		vc.stream.needUpdateLoopParam	= FALSE;
		
		vc.stream.samplePosition		= 0;
	}

	if (vc.streamed) {
		NV_ASSERT(sc.sndState.newSeg.available); 								// play the first segment ( begin to end segment if no segment was passed to sdLock)
		NV_ASSERT(!sc.sndState.newSeg.reqL && !sc.sndState.newSeg.reqR);		// sound is lock so the segment request for loop context must be finished .

		vc.axVoiceL = AquireVoiceADPCM( sc.sndData.adpcmHeader ,vc.sAddrL, TRUE, vc.bufferSize);
		sc.sndState.newSeg.available = FALSE;
		SetLoopParam(vc.axVoiceL, sc.sndState.newSeg.lctxL.ps , sc.sndState.newSeg.lctxL.yn1 , sc.sndState.newSeg.lctxL.yn2 );
		
		vc.stream.lastPosition	 = (uint32(vc.axVoiceL->pb.addr.currentAddressHi) << 16 ) | uint32(vc.axVoiceL->pb.addr.currentAddressLo);
		vc.stream.lastPosition	 = vc.stream.lastPosition >> 1;
		vc.stream.lastPosition 	&= 0xFFFFFFF8;
		vc.stream.beginPosition	 = vc.stream.lastPosition;
		
		if (vc.stereo) {
			vc.axVoiceR = AquireVoiceADPCM( sc.sndData.adpcmHeader+1 ,vc.sAddrR, TRUE, vc.bufferSize);
			SetLoopParam(vc.axVoiceR, sc.sndState.newSeg.lctxR.ps , sc.sndState.newSeg.lctxR.yn1 , sc.sndState.newSeg.lctxR.yn2 );
		}
		
		if (sc.sndState.bSize <= 0 ) {
			vc.stream.streamEnd = sc.sndState.trx.GetTrxBSize() >> (vc.stereo) ;
			CallCallBack(vc.sndOwnerId, vc , sc);
			vc.stream.sstate 			= SS_EndStream;
			vc.stream.needUpdateData 	= TRUE;
		}
	}
	else
		vc.axVoiceL = AquireVoiceADPCM( (DSPADPCM*) &sc.sndData.adpcmHeader[0] ,sc.sndMem.sAddr,FALSE,0);

	if (!vc.axVoiceL)
	 	return -1;

	vc.axVoiceL->userContext = channelId;
	if (vc.axVoiceR)
		vc.axVoiceR->userContext = channelId;
	voiceBusyMask 			|= chanMsk;
	voiceToStartMask		|= chanMsk;
	sc.usedVoice 			|= chanMsk;
	
	ApplyPanPitch(vc);
	ApplyVolumeOnly(vc, masterVol); 

	return channelId;
}

bool
SndManager::ChStop		(	int			inChannel		)
{
	NV_ASSERT_RETURN_MTH(FALSE, (bgmFctCall && inChannel==BGM_VOICE) || (!bgmFctCall && inChannel<NB_CHANNEL && inChannel >= 0));

	uint64 chanMsk 		= uint64(1U) << inChannel;
	uint64 notChanMask 	= ~chanMsk;
	
	if ( !(voiceBusyMask 	& chanMsk))	return TRUE;
	if ( voiceToStartMask  	& chanMsk) 	return FALSE;
	if ( voiceStoppingMask 	& chanMsk) 	return TRUE;
	
	if ( voiceA[inChannel].axVoiceL->pb.state == AX_PB_STATE_STOP){
		if (!voiceA[inChannel].stereo || (voiceA[inChannel].axVoiceR->pb.state == AX_PB_STATE_STOP)) {
			if (voiceA[inChannel].stream.sstate == SS_WaitNewSegmentStart) {
				voiceA[inChannel].stream.sstate = SS_EndStream;
			}
			voicePauseMask &= notChanMask;
			return TRUE;
		}
	}

	NV_ASSERT(voiceA[inChannel].axVoiceL );
	AXSetVoiceState(voiceA[inChannel].axVoiceL, AX_PB_STATE_STOP);
	if (voiceA[inChannel].axVoiceR)
		AXSetVoiceState(voiceA[inChannel].axVoiceR, AX_PB_STATE_STOP);
	voiceStoppingMask |= chanMsk ;
	voicePauseMask &= notChanMask;
	return TRUE;
}

void
SndManager::ChStopAll	(			)
{
	for (uint i = 0 ; i < NB_CHANNEL ; ++i ) {
		ChStop(i);
	}
}

bool
SndManager::ChPause			(	int			inChannel		)
{
	NV_ASSERT_RETURN_MTH(FALSE, (bgmFctCall && inChannel==BGM_VOICE) || (!bgmFctCall && inChannel<NB_CHANNEL && inChannel >= 0));

	uint64 chanMsk = uint64(1) << inChannel ;

	if ( !(voiceBusyMask 	& chanMsk))return FALSE;
	if ( voiceToStartMask  	& chanMsk) return FALSE;
	if ( voiceStoppingMask 	& chanMsk) return FALSE;
		
	NV_ASSERT(voiceA[inChannel].axVoiceL)
	
	voicePauseMask |= chanMsk;
	AXSetVoiceState(voiceA[inChannel].axVoiceL, AX_PB_STATE_STOP);
	if (voiceA[inChannel].axVoiceR)
		AXSetVoiceState(voiceA[inChannel].axVoiceR, AX_PB_STATE_STOP);
	return TRUE;
}

bool
SndManager::ChResume		(	int			inChannel		)
{
	NV_ASSERT_RETURN_MTH(FALSE, (bgmFctCall && inChannel==BGM_VOICE) || (!bgmFctCall && inChannel<NB_CHANNEL && inChannel >= 0));
	
	uint64 chanMsk = uint64(1) << inChannel ;
	
	if (!(voiceBusyMask 	& chanMsk))return FALSE;
	if ( voiceToStartMask  	& chanMsk) return FALSE;
	if ( voiceStoppingMask 	& chanMsk) return FALSE;
	
	voiceToStartMask |= chanMsk;
	return TRUE;
}

bool
SndManager::ChSetVolume		(	int			inChannel,
								float		inVol			)
{
	NV_ASSERT_RETURN_MTH(FALSE, (bgmFctCall && inChannel==BGM_VOICE) || (!bgmFctCall && inChannel<NB_CHANNEL && inChannel >= 0));

	uint64 chanMsk = uint64(1) << inChannel ;	

	if ( !(voiceBusyMask 	& chanMsk))return FALSE;
	if ( voiceToStartMask  	& chanMsk) return FALSE;
	if ( voiceStoppingMask 	& chanMsk) return FALSE;

	if (voiceA[inChannel].vol != inVol) 
	{
		voiceA[inChannel].vol = inVol;
		ApplyVolumeOnly(voiceA[inChannel],masterVol); 
	}
	return true;
}


bool
SndManager::ChSetPanning	(	int			inChannel,
								float		inAngle			)
{
	NV_ASSERT_RETURN_MTH(FALSE, inChannel<NB_CHANNEL && inChannel); // No SetPanning on BGM
	
	uint64 chanMsk = uint64(1) << inChannel ;	
	
	if ( !(voiceBusyMask 	& chanMsk))return FALSE;
	if ( voiceToStartMask  	& chanMsk) return FALSE;
	if ( voiceStoppingMask 	& chanMsk) return FALSE;
	
	if (inAngle != voiceA[inChannel].panAngle) {
		voiceA[inChannel].panAngle = inAngle;
		ApplyPanPitch(voiceA[inChannel]);	
	}
	return TRUE;
}


bool
SndManager::ChSetPitchFactor	(	int			inChannel,
									float		inFactor	)
{
	NV_ASSERT_RETURN_MTH(FALSE, inChannel<NB_CHANNEL && inChannel); // No SetPitchFactor on BGM
	
	uint64 chanMsk = uint64(1) << inChannel ;
	
	if ( !(voiceBusyMask 	& chanMsk))return FALSE;
	if ( voiceToStartMask  	& chanMsk) return FALSE;
	if ( voiceStoppingMask 	& chanMsk) return FALSE;
	
	if (inFactor != voiceA[inChannel].pitchFactor) 
	{
		voiceA[inChannel].pitchFactor = inFactor;
		ApplyPanPitch(voiceA[inChannel]);	
	}
	return TRUE;
}

bool
SndManager::BgmPlay		(	int			inSoundId,
							float		inVol		)
{
	bgmFctCall = TRUE;
	bool ret   = ChPlay(BGM_VOICE,inSoundId,inVol,0.0f,1.0f);
	bgmFctCall = FALSE;
	return ret;
}

bool
SndManager::BgmStop		(			)
{
	bgmFctCall = TRUE;
	bool ret   = ChStop(BGM_VOICE);
	bgmFctCall = FALSE;
	return ret;	
}

bool
SndManager::BgmPause	(			)
{
	bgmFctCall = TRUE;
	bool ret   = ChPause(BGM_VOICE);
	bgmFctCall = FALSE;
	return ret;	
}

bool
SndManager::BgmResume	(			)
{
	bgmFctCall = TRUE;
	bool ret   = ChResume(BGM_VOICE);
	bgmFctCall = FALSE;
	return ret;	
}

bool
SndManager::BgmSetVolume	(	float		inVol		)
{
	bgmFctCall = TRUE;
	bool ret   = ChSetVolume(BGM_VOICE,inVol);
	bgmFctCall = FALSE;
	return ret;	
}

bool	
SndManager::BgmBreak	(		)
{
	return SdBreak(voiceA[BGM_VOICE].sndOwnerId);	
}

bool
SndManager::BgmIsPlaying		(			)
{
	bgmFctCall = TRUE;
	bool ret = ChGetState(BGM_VOICE)== CH_PLAYING;
	bgmFctCall = FALSE;
	return ret;
}

float
SndManager::MbToLinearVolume	(	float		inMillibel		)
{
	// [-10000,0] -> [0,1]
	float mb = Clamp( inMillibel, -10000.0f, 0.0f );
	if( mb == -10000.0f )
		return 0.0f;
	else {
		float lin = Powf( 10.0f, mb*(1.0f/2000.0f) );
		return Clamp( lin, 0.0f, 1.0f );
	}
}

float
SndManager::LinearToMbVolume	(	float		inLinear		)
{
	// [0,1] -> [-10000,0]
	float lin = Clamp( inLinear, 0.0f, 1.0f );
	if( lin == 0.0f )
		return -10000.0f;
	else {
		int mb = int( Log10f(lin) * 2000.0f );
		return Clamp( mb, -10000, 0 );
	}
}


namespace SndManager
{


float
ChGetPosition(uint channelId)
{
	Voice & vc = voiceA[channelId];
	
	if (!vc.streamed) 
		return 0.0f;
	
	return vc.stream.samplePosition / float (soundA[vc.sndOwnerId].sndProp.freq ) ;
}	


float
BgmGetPosition()
{
	Voice & vc = voiceA[BGM_VOICE];
	
	if (!vc.streamed) 
		return 0.0f;
	
	return vc.stream.samplePosition /  float (soundA[vc.sndOwnerId].sndProp.freq);
}	

}


