/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/NGC/NvDpyManager[NGC].h>
#include <Kernel/NGC/NvDpyObject[NGC].h>
#include <Kernel/Common/NvkCore_Mem.h>
using namespace nv;


#define	SESSION_NB		256


namespace
{
	bool									presentFrame;
	bool									frameOpened;
	bool									frameSynced;
	uint									frameNo = ~0U;
	uint									flushCpt;
	uint									flushingFrameNo;
	sysvector<NvDpyObject_NGC*>				sessionA[SESSION_NB];
	uint									ctxtCurSession;

	NvDpyObject_NGC*						frameBreakBefore;
	NvDpyObject_NGC*						frameBreakAfter;

	sysvector<NvInterface*>					garbage0;
	sysvector<NvInterface*>					garbage1;
	sysvector<NvInterface*>	*				garbager;
	
	
	struct 	RasterTexture 
	{
		// Common
		uint16 		width	;
		uint16		height	;
		
		// Color 
		void *		cData	;
		GXTexFmt	cFmt	;
		uint16		cBSize	;
		
		// Z
		//void * 	zData	;
		//GXTexFmt	zFmt	;
		//uint16	zBSize	;
	};
	
	RasterTexture							rTexA	[DPYR_OFF_FRAME_C32Z32+1];

	void* RasterAlloc ( uint inBSize )
	{
	#ifdef RVL
		return nv::mem::NativeMalloc( inBSize, 32 , nv::mem::MEM_NGC_MEM2 );
	#else
		return RscMallocA( inBSize, 32 );
	#endif
	}

	void RasterFree ( void* inPtr )
	{
	#ifdef RVL
		return nv::mem::NativeFree( inPtr, nv::mem::MEM_NGC_MEM2 );
	#else
		return RscFree( inPtr );
	#endif
	}

	void InitRasterTex () 
	{
		uint efbW = gx::GetWidth();
		uint efbH = gx::GetHeight();
		uint refW  ;
		uint refH  ;
		nv::core::GetParameter( nv::core::PR_OFF_FRAME_W, &refW ) ;
		nv::core::GetParameter( nv::core::PR_OFF_FRAME_H, &refH ) ;
		if ( 2 * refW > efbW ) refW = efbW / 2; 
		if ( 2 * refH > efbH ) refH = efbH / 2;
		
		for (uint i = 0 ; i <= DPYR_OFF_FRAME_C32Z32 ; ++ i ) {

			switch (DpyRaster(i)) {
				case DPYR_NULL :
					rTexA[i].width 	= 0;
					rTexA[i].height = 0;
					rTexA[i].cFmt	= GX_TF_RGBA8;
					break;
				case DPYR_IN_FRAME :
					rTexA[i].width 	= efbW;
					rTexA[i].height = efbH;
					rTexA[i].cFmt	= GX_TF_RGBA8;
					break;
				case DPYR_OFF_FRAME_C16 :
					rTexA[i].width 	= refW * 2;
					rTexA[i].height = refH * 2;
					rTexA[i].cFmt	= GX_TF_RGB5A3;
					break;
				case DPYR_OFF_FRAME_C32H :
					rTexA[i].width 	= refW * 2;
					rTexA[i].height = refH;
					rTexA[i].cFmt	= GX_TF_RGBA8;
					break;
				case DPYR_OFF_FRAME_C32V :
					rTexA[i].width 	= refW;
					rTexA[i].height = refH * 2;
					rTexA[i].cFmt	= GX_TF_RGBA8;
					break;
				case DPYR_OFF_FRAME_C16Z16H :
					rTexA[i].width 	= refW * 2;
					rTexA[i].height = refH;
					rTexA[i].cFmt	= GX_TF_RGB5A3;
					break;
				case DPYR_OFF_FRAME_C16Z16V :
					rTexA[i].width 	= refW;
					rTexA[i].height = refH * 2;
					rTexA[i].cFmt	= GX_TF_RGB5A3;
					break;
				case DPYR_OFF_FRAME_C32Z32 :
					rTexA[i].width 	= refW;
					rTexA[i].height = refH;
					rTexA[i].cFmt	= GX_TF_RGBA8;
					break;
			};
			
			if (rTexA[i].width && rTexA[i].height ) 
			{
				uint bs = GXGetTexBufferSize(rTexA[i].width ,rTexA[i].height , rTexA[i].cFmt ,GX_FALSE,0);
				rTexA[i].cBSize	= bs;
				rTexA[i].cData  = RasterAlloc( bs );
			}
			else 
			{
				rTexA[i].cData 	= NULL;
				rTexA[i].width  = 0;
				rTexA[i].height = 0;
				rTexA[i].cBSize	= 0;
			}

		}
	}
	
	void ShutRasterTex( )
	{
		for (uint i = 0 ; i <= DPYR_OFF_FRAME_C32Z32 ; ++ i ) {
			RasterFree( rTexA[i].cData );
			rTexA[i].cData  = NULL;
			rTexA[i].width  = 0;
			rTexA[i].height = 0;
			rTexA[i].cBSize = 0;
		}
	}
	
	
}

const float DpyManager::LIGHT_SCALE_VALUE = 1.0E+18F;
DpyManager::CoordinateSystem	DpyManager::coordSystem;
NvDpyContext*					DpyManager::context;
Matrix							DpyManager::clipMatrix;

#define OFFFRAME_RASTER_FORMAT GX_TF_RGB565


bool
DpyManager::Init	(	CoordinateSystem	inCoordinateSystem	)
{
	garbage0.Init();
	garbage1.Init();
	garbager = &garbage0;

	context = NvEngineNew( NvDpyContext );
	context->Init();
	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].Init();

	coordSystem		= inCoordinateSystem;
	frameBreakBefore= NULL;
	frameBreakAfter = NULL;
	presentFrame	= FALSE;
	frameNo			= 0;
	frameOpened		= FALSE;
	frameSynced		= TRUE;
	flushingFrameNo = 0;
	flushCpt		= 0;

	MatrixIdentity( &clipMatrix );
	clipMatrix.m33 = 0.5f;
	clipMatrix.m43 = -0.5f;

	gx::Init();
	gx::Setup();
	gx::Reset( FALSE );

	uint32 vmode;
	uint32 xfb_base, xfb_bs;
	uint32 fifo_base, fifo_bsize;
	core::GetParameter( core::PR_NGC_VMODE, &vmode );
	core::GetParameter( core::PR_NGC_XFB_BSIZE,	&xfb_bs   );
	core::GetParameter( core::PR_NGC_XFB_BASE,	&xfb_base );
	core::GetParameter( core::PR_NGC_GX_FIFO_BSIZE,	&fifo_bsize );
	core::GetParameter( core::PR_NGC_GX_FIFO_BASE,	&fifo_base  );

	InitRasterTex ();	
	
#ifndef _MASTER
	Printf( "<Neova> GX rmode   : %d\n", vmode );
	Printf( "<Neova> EFB        : %d x %d\n", gx::GetWidth(), gx::GetHeight() );
	Printf( "<Neova> XFB        : %d x %d\n", gx::GetXFBWidth(), gx::GetXFBHeight() );
	Printf( "<Neova> XFB        : @%xh [%d Ko]\n", xfb_base, xfb_bs>>10 );
	Printf( "<Neova> FIFO       : @%xh [%d Ko]\n", fifo_base, fifo_bsize>>10 );
#endif

	return TRUE;
}


bool
DpyManager::Shut	(				)
{
	SyncFrame();

	while( garbager->size() ) {
		frameNo++;
		FlushGarbager();
	}

	garbage0.Shut();
	garbage1.Shut();
	garbager = NULL;

	context->Shut();
	NvEngineDelete( context );

	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].Shut();

	gx::Shut();
	
	ShutRasterTex();
	
	return TRUE;
}



DpyManager::CoordinateSystem
DpyManager::GetCoordinateSystem	(				)
{
	return coordSystem;
}


uint
DpyManager::GetPhysicalWidth	(				)
{
	return gx::GetXFBWidth();
}


uint
DpyManager::GetPhysicalHeight	(				)
{
	return gx::GetXFBHeight();
}


float
DpyManager::GetPhysicalAspect	(				)
{
	float w = float( GetPhysicalWidth() );
	float h = float( GetPhysicalHeight() );
	return w/h;
}


uint
DpyManager::GetVirtualWidth		(				)
{
	return gx::GetVirtualWidth();
}


uint
DpyManager::GetVirtualHeight	(				)
{
	return gx::GetVirtualHeight();
}


float
DpyManager::GetVirtualAspect	(				)
{
	float w = float( GetVirtualWidth() );
	float h = float( GetVirtualHeight() );
	return w/h;
}


uint
DpyManager::GetRasterWidth	(	DpyRaster	inRaster	)
{
	if ( int (inRaster) >= 0 && int(inRaster) <= DPYR_OFF_FRAME_C32Z32 )
		return rTexA[int(inRaster)].width;
	return 0 ;
}


uint
DpyManager::GetRasterHeight	(	DpyRaster	inRaster	)
{
	if ( int (inRaster) >= 0 && int(inRaster) <= DPYR_OFF_FRAME_C32Z32 )
		return rTexA[int(inRaster)].height;
	return 0 ;
}


uint
DpyManager::GetVSyncRate	(			)
{
	return gx::GetVSyncRate();
}


uint
DpyManager::GetLatency		(			)
{
	return 2;
}


uint
DpyManager::GetViewingLatency	(		)
{
	return 2;
}


bool
DpyManager::TranslateCRTC		(	int		inX0,
									int		inY0	)
{
	gx::TranslateCRTC( inX0, inY0 );
	return TRUE;
}


bool
DpyManager::ReadFrame	(	pvoid			outBuffer	)
{
	// Defined and QW aligned !
	if( !outBuffer || uint32(outBuffer)&15 ) {
		NV_DEBUG_WARNING( "DpyManager::ReadFrame() : outBuffer must be 16bytes aligned !" );
		return FALSE;
	}

	SyncFrame();

	gx::GetBack_Direct( Vec4(0,0,gx::GetWidth(),gx::GetHeight()), (uint8*)outBuffer, gx::GetWidth()*4 );
	return TRUE;
}


bool
DpyManager::WriteFrame	(	pvoid		inBuffer	)
{
	// Defined and QW aligned !
	if( !inBuffer || uint32(inBuffer)&15 ) {
		NV_DEBUG_WARNING( "DpyManager::WriteFrame() : inBuffer must be 16bytes aligned !" );
		return FALSE;
	}

	SyncFrame();

	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	if( !consoleOnly ) {
		gx::PutBack32_Direct( Vec4(0,0,gx::GetWidth(),gx::GetHeight()), (uint8*)inBuffer, gx::GetWidth()*4 );
	}

	return TRUE;
}


bool
DpyManager::SaveFrame	(	pcstr		inFilename		)
{
#ifdef _MASTER
	return FALSE;
#else
	SyncFrame();

	if( !inFilename || (*inFilename)==0 )
		return FALSE;

	uint bsize = gx::GetWidth() * gx::GetHeight() * 4;

	pvoid  mem = EngineMalloc( 256+bsize );
	uint8* bmp = ((uint8*)mem) + 256;
	uint8* hdr = bmp - 18;

	if( !ReadFrame(bmp) ) {
		EngineFree( mem );
		return FALSE;
	}

	uint32* bmp0 = (uint32*) bmp;
	uint32* bmp1 = bmp0 + (bsize/4);
	while( bmp0 < bmp1 ) {
		*bmp0 = InvertByteOrder( *bmp0 );
		bmp0++;
	}

	Zero( hdr, 18 );
	hdr[2] =2;							// Image type code
	hdr[12]=(gx::GetWidth()>>0)&0xFF;	// low width of image
	hdr[13]=(gx::GetWidth()>>8)&0xFF;	// high width of image
	hdr[14]=(gx::GetHeight()>>0)&0xFF;	// low height of image
	hdr[15]=(gx::GetHeight()>>8)&0xFF;	// high height of image
	hdr[16]=32;							// Image pixel size
	hdr[17]=32;							// Image descriptor byte

	bool res = nv::file::DumpToFile( inFilename, hdr, 18+bsize );
	EngineFree( mem );
	return res;

#endif
}


uint
DpyManager::GetFrameNo		(		)
{
	return frameNo;
}


Psm
DpyManager::GetFramePSM		(		)
{
	return PSM_ARGB32;
}


bool
DpyManager::BeginFrame		(		)
{
	NV_ASSERTC( !frameOpened, "Frame is already opened !" );
	if( frameOpened )
		return FALSE;

	// reset session context
	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].clear();
	ctxtCurSession = 0;

	// reset context stacks
	Vec4 lightcolor( 1, 1, 1, 1 );
	Vec2 clipRange( 1.0f, 500.0f );
	Vec4 viewport( 0, 0, gx::GetWidth(), gx::GetHeight() );
	context->Reset();
	context->SetLight( 0, &lightcolor, NULL );
	context->SetLight( 1, &lightcolor, (Vec3*)&Vec3::UNIT_X );
	context->SetLight( 2, &lightcolor, (Vec3*)&Vec3::UNIT_Y );
	context->SetView( NULL, NULL, &clipRange, &viewport );
	context->SetTarget( DpyTarget(DPYR_IN_FRAME) );

	// setup new frame
	frameOpened = TRUE;
	frameNo++;

	return TRUE;
}


bool
DpyManager::EndFrame	(		)
{
	NV_ASSERTC( frameOpened, "Frame is not opened !" );
	if( !frameOpened )
		return FALSE;
	frameOpened = FALSE;
	return TRUE;
}



namespace
{
	float		perf_GPU_FPS	= 0;
	float		perf_CPU_FPS	= 0;
	uint32		perf_drawCpt	= 0;
	uint32		perf_triCpt		= 0;
	uint32		perf_locCpt		= 0;
	uint32		perf_vtxCpt 	= 0;
	uint32		perf_primCpt	= 0;
	uint32		perf_texBSize	= 0;
}


bool
DpyManager::FlushFrame	(	bool	inPresent,
							bool	inGetPerf	)
{
	NV_ASSERTC( !frameOpened, "Frame must not be opened !" );
	NV_ASSERTC( flushingFrameNo != frameNo, "Frame has been already flushed !" );
	if( frameOpened || flushingFrameNo==frameNo )
		return FALSE;

	#ifdef ENABLE_ASYNC_LOG
	{
		static bool async_output = FALSE;
		ctrl::Status* c = ctrl::GetStatus( 0 );
		if( 	c
			&&	c->button[ctrl::BT_BUTTON4] > 0.5f
			&&	c->button[ctrl::BT_BUTTON5] > 0.5f
			&&	c->button[ctrl::BT_BUTTON6] > 0.5f
			&&	c->button[ctrl::BT_BUTTON7] > 0.5f	)
		{
			if( !async_output )
				core::SetParameter( core::PR_ASYNC_LOG, TRUE );
			async_output = TRUE;
		}
		else
		{
			async_output = FALSE;
		}
	}
	#endif

	bool async, vsync;
	core::GetParameter( core::PR_ASYNC_DRAW, &async );
	core::GetParameter( core::PR_VSYNC_DRAW, &vsync );

	// swap frame dmac heaps
	dmac::SwapHeap();
	#ifdef _NVCOMP_ENABLE_DBG
	{
		dmac::Cursor* curC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( curC );
		Memset( curC->vd, 0, curC->GetLeftBSize() );
	}
	#endif

	gx::ResetRegisters();
	vtxfmt::Reset();

	// init frame
	if( presentFrame ) {
		if( vsync ) {
			gx::Sync();
			gx::VSync();
			frameSynced	= TRUE;
		}
		gx::Present( FALSE );
		gx::Flush();
	}
	presentFrame = inPresent;

	// Draw
	for( uint s = 0 ; s < SESSION_NB ; s++ ) {
		uint N = sessionA[s].size();
		if( N == 0 )	continue;
		NvDpyObject_NGC	** dpyObjP		= sessionA[s].data();
		NvDpyObject_NGC	** dpyObj_endP	= dpyObjP + N;
		while( dpyObjP != dpyObj_endP ) {
			NV_ASSERT( *dpyObjP );
			if( *dpyObjP == frameBreakBefore )	goto dpy_frame_break;
			(*dpyObjP)->Draw();
			if( *dpyObjP == frameBreakAfter )	goto dpy_frame_break;
			dpyObjP++;
		}
	}
dpy_frame_break:

	// sync frame
	flushingFrameNo	= frameNo;
	frameSynced	= FALSE;

	if( !async ) {
		gx::Sync();
		frameSynced = TRUE;
	}

	// Get performances ?
	if( inGetPerf ) {
//		frame::WaitDMA();
/*		float dmaTime	= frame::GetPerformance( frame::PERF_DMA );
		float cpuTime	= frame::GetPerformance( frame::PERF_BEGIN_BEGIN )
						- frame::GetPerformance( frame::PERF_SYNC_UPDATE_IDLE )
						- frame::GetPerformance( frame::PERF_VSYNC_IDLE )
						- frame::GetPerformance( frame::PERF_WAIT_DMA );
		perf_GPU_FPS	= dmaTime ? (1.0f/dmaTime) : 0.f;
		perf_CPU_FPS	= cpuTime ? (1.0f/cpuTime) : 0.f;
		perf_texBSize	= int( frame::GetPerformance( frame::PERF_TRXD_TEXBSIZE ) );
*/		perf_drawCpt	= 0;
		perf_triCpt		= 0;
		perf_locCpt		= 0;
		perf_vtxCpt		= 0;
		perf_primCpt	= 0;
		for( uint s = 0 ; s < SESSION_NB ; s++ ) {
			uint N = sessionA[s].size();
			if( N == 0 )	continue;
			NvDpyObject_NGC	** dpyObjP		= sessionA[s].data();
			NvDpyObject_NGC	** dpyObj_endP	= dpyObjP + N;
			while( dpyObjP != dpyObj_endP ) {
				NV_ASSERT( *dpyObjP );
				uint32 objTriCpt=0, objLocCpt=0, objVtxCpt=0, objPrimCpt=0;
				if( (*dpyObjP)->GetPerformances(objTriCpt,objLocCpt,objVtxCpt,objPrimCpt) ) {
					perf_triCpt	 += objTriCpt;
					perf_locCpt	 += objLocCpt;
					perf_vtxCpt  += objVtxCpt;
					perf_primCpt += objPrimCpt;
				}
				perf_drawCpt += (*dpyObjP)->dpyctxt.chainCpt;
				dpyObjP++;
			}
		}
	}

	gx::Flush();

	FlushGarbager();

	if( ++flushCpt == 2 )
		gx::EnableCRTC();

	
	return TRUE;
}


float
DpyManager::GetPerformance		(	Performance			inPerf		)
{
	if( inPerf == PERF_GPU_FPS )	return perf_GPU_FPS;
	if( inPerf == PERF_CPU_FPS )	return perf_CPU_FPS;
	if( inPerf == PERF_DRAW_CPT )	return float(int(perf_drawCpt));
	if( inPerf == PERF_TRI_CPT )	return float(int(perf_triCpt));
	if( inPerf == PERF_LOC_CPT )	return float(int(perf_locCpt));
	if( inPerf == PERF_VTX_CPT )	return float(int(perf_vtxCpt));
	if( inPerf == PERF_PRIM_CPT )	return float(int(perf_primCpt));
	if( inPerf == PERF_TEX_BSIZE )	return float(int(perf_texBSize));
	return 0.f;
}


void
DpyManager::SyncFrame	(			)
{
	NV_ASSERTC( !frameOpened, "Frame must not be opened !" );
	if( frameOpened )	return;

	if( !frameSynced ) {
		gx::Sync();
		frameSynced = TRUE;
	}

	FlushGarbager();
}


bool
DpyManager::IsInFrame	(	NvDpyObject_NGC*	inDpyObj	)
{
	if( !inDpyObj )
		return FALSE;
	return ( inDpyObj->dpyctxt.drawFrameNo == frameNo );
}


bool
DpyManager::IsInFrame	(	NvDpyObject*		inDpyObj	)
{
	return IsInFrame( inDpyObj ? (NvDpyObject_NGC*)inDpyObj->GetBase() : NULL );
}


bool
DpyManager::IsDrawing	(	uint32				inFrameNo	)
{
	// my frame is the current frame and is flushed ?
	if( inFrameNo==flushingFrameNo && inFrameNo==frameNo )
		return !frameSynced;
	else
		return ( inFrameNo==flushingFrameNo || inFrameNo==frameNo );
}


bool
DpyManager::IsDrawing	(	NvDpyObject_NGC*	inDpyObj	)
{
	if( !inDpyObj )
		return FALSE;
	return IsDrawing( inDpyObj->dpyctxt.drawFrameNo );
}


bool
DpyManager::IsDrawing	(	NvDpyObject*		inDpyObj	)
{
	return IsDrawing( inDpyObj ? (NvDpyObject_NGC*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetFrameBreakBefore	(	NvDpyObject_NGC*		inDpyObj	)
{
	frameBreakAfter  = NULL;
	frameBreakBefore = inDpyObj;
}


void
DpyManager::SetFrameBreakBefore	(	NvDpyObject*		inDpyObj	)
{
	SetFrameBreakBefore( inDpyObj ? (NvDpyObject_NGC*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetFrameBreakAfter	(	NvDpyObject_NGC*		inDpyObj	)
{
	frameBreakBefore = NULL;
	frameBreakAfter	 = inDpyObj;
}


void
DpyManager::SetFrameBreakAfter	(	NvDpyObject*		inDpyObj	)
{
	SetFrameBreakAfter( inDpyObj ? (NvDpyObject_NGC*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetTarget	(	DpyTarget		inTarget	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetTarget( inTarget );
}


void
DpyManager::SetView		(	Matrix*			inViewTR,
							Matrix*			inProjTR,
							Vec2*			inClipRange,
							Vec4*			inViewport	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetView( inViewTR, inProjTR, inClipRange, inViewport );
}


void
DpyManager::SetSession	(	uint8			inSessionNo		)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	ctxtCurSession = (inSessionNo < SESSION_NB) ? inSessionNo : SESSION_NB-1;
}


void
DpyManager::SetWorldTR	(	Matrix*		inWorldTR	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetWorldTR( inWorldTR );
}


void
DpyManager::SetLight	(	uint		inLightNo,
							Vec4*		inColor,
							Vec3*		inDirection		)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetLight( inLightNo, inColor, inDirection );
}


bool
DpyManager::Draw		(	NvDpyObject_NGC*	inDpyObj	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return FALSE;

	if( !inDpyObj )
		return FALSE;

	// First draw in the current frame ?
	bool firstDraw = !IsInFrame( inDpyObj );

	int chainNext = (firstDraw ? -1 : inDpyObj->dpyctxt.chainHead );
	int chainHead = context->Push( chainNext );
	if( chainHead < 0 )
		return FALSE;

	inDpyObj->dpyctxt.chainHead = chainHead;
	inDpyObj->dpyctxt.chainCpt += 1;

	// Not already in the current draw queue ?
	if( firstDraw ) {
		NV_ASSERT( ctxtCurSession < SESSION_NB );
		sessionA[ ctxtCurSession ].push_back( inDpyObj );
		inDpyObj->dpyctxt.drawFrameNo = frameNo;
		inDpyObj->dpyctxt.chainCpt = 1;
	}

	return TRUE;
}


bool
DpyManager::Draw	(	NvDpyObject*		inDpyObj	)
{
	return Draw( inDpyObj ? (NvDpyObject_NGC*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::AddToGarbager( NvInterface*		inITF )
{
	NV_ASSERT( garbager );
	if( !inITF )
		return;

#ifdef _NVCOMP_ENABLE_DBG
	if( garbager->size() ) {
		NvInterface** itf	  = garbager->data();
		NvInterface** itf_end = itf + garbager->size();
		while( itf != itf_end ) {
			NV_ASSERT( *itf );
			NV_ASSERTC( (*itf)!=inITF, "Interface 0x%08x is already garbaging !" );
			itf++;
		}
	}
#endif

	garbager->push_back( inITF );
}


void
DpyManager::FlushGarbager	(	)
{
	NV_ASSERT( garbager );

	// Swap garbager before to allow Release() to call AddToGarbager() again !
	sysvector<NvInterface*>* _garbager = (garbager==&garbage0) ? &garbage1 : &garbage0;
	_garbager->clear();
	Swap( _garbager, garbager );

	// Release garbaging objets
	if( _garbager->size() ) {
		NvInterface** itf	  = _garbager->data();
		NvInterface** itf_end = itf + _garbager->size();
		while( itf != itf_end ) {
			NV_ASSERT( *itf );
			(*itf)->Release();		// Note that Release() can call AddToGarbager() again !
			itf++;
		}
	}
}


void
DpyManager::SetupRaster 	(	DpyRaster			inRaster)
{
	static DpyRaster 	lastRaster 			= DPYR_IN_FRAME;
	static uint 		lastChangeFrameNo	= 0xFFFFFFFF;
	
	if ( (inRaster == DPYR_IN_FRAME) && (lastRaster != DPYR_IN_FRAME)&&  (GetFrameNo() == lastChangeFrameNo)) {
	
		if (lastRaster != DPYR_NULL )
			CopyEFBToTex		(lastRaster);

	}
	
	lastRaster 			= inRaster;
	lastChangeFrameNo 	= GetFrameNo();
}

void
DpyManager::SetupDrawing 	(	DpyRaster			inRaster,
								NvDpyContext::View*	inView,
								Matrix*				inWorldTR,
								bool				inThroughMode		)
{		

	DpyManager::SetupRaster 	( inRaster ) ;

    Matrix 	 toView;
   				
	GXSetViewport(inView->viewport.x, inView->viewport.y, inView->viewport.z, inView->viewport.w, 0.0f, 1.0f);
	GXSetScissor (inView->viewport.x, inView->viewport.y, inView->viewport.z, inView->viewport.w 			);				
    
	// TR
	if( !inThroughMode )
	{
		MatrixMul		( &toView,inWorldTR,&inView->viewTR );
		MatrixTranspose	( &toView,&toView);		
		
		Matrix projTR;
		MatrixMul(&projTR,&inView->projTR,&DpyManager::clipMatrix);	
		MatrixTranspose(&projTR,&projTR);
		if ( projTR.m44 == 1.0f )
		{
			NV_ASSERTC( projTR.m43==0.0f, "Projection matrix m34 must be 0 !" );
			GXSetProjection(projTR.m, GX_ORTHOGRAPHIC);
		}
		else
		{
			NV_ASSERTC( projTR.m44 == 0.0f, "Projection matrix m44 must be 0 !" );
			NV_ASSERTC( projTR.m43 == -1.0f, "Projection matrix m34 must be -1 !" );
			GXSetProjection(projTR.m, GX_PERSPECTIVE);
		}
	}
	else
    { 
		MatrixTranspose( &toView,inWorldTR );
		
		Matrix proj2D;
		MatrixZero(&proj2D);
		
		proj2D.m11 =	2.0f  / inView->viewport.z ;
		proj2D.m22 =	-2.0f / inView->viewport.w ;
		proj2D.m33 =	1.0f;
		proj2D.m14 =	-1.0f ;
		proj2D.m24 =	1.0f  ;
		proj2D.m34 =	-1.0f ;
		proj2D.m44 =	1.0f  ;
	    GXSetProjection(proj2D.m, GX_ORTHOGRAPHIC);
	}
	
	GXLoadPosMtxImm	(toView.m, GX_PNMTX0);
	GXSetCurrentMtx( GX_PNMTX0 );
}

// Off Frame Raster 
bool
DpyManager::CopyEFBToTex( 	DpyRaster	inSaveAsRaster	)
{
	if ( int (inSaveAsRaster) >= 0 && int(inSaveAsRaster) <= DPYR_OFF_FRAME_C32Z32 )
	{
		if ( rTexA[int (inSaveAsRaster)].cData ) 
		{
			
			gx::CopyEFBToMem( 	rTexA[int (inSaveAsRaster)].cData 	,
								0, 0 , 
								rTexA[int (inSaveAsRaster)].width  ,
								rTexA[int (inSaveAsRaster)].height	,
								rTexA[int (inSaveAsRaster)].cFmt	);
			DCFlushRange( rTexA[int (inSaveAsRaster)].cData, rTexA[int (inSaveAsRaster)].cBSize );
			return TRUE;
		}
	}
	return FALSE;
}

void
DpyManager::GetRasterTex(	DpyRaster					inSaveAsRaster	,
							GXTexFmt	&				outFmt			,
							uint		&				outWidth		,
							uint		&				outHeight		,
							void		**				outData			)
{
	if ( int (inSaveAsRaster) >= 0 && int(inSaveAsRaster) <= DPYR_OFF_FRAME_C32Z32 )
	{
		outWidth  	= rTexA[int(inSaveAsRaster)].width;
		outHeight 	= rTexA[int(inSaveAsRaster)].height;
		outFmt		= rTexA[int(inSaveAsRaster)].cFmt;
		if (outData)
			*outData 	= rTexA[int(inSaveAsRaster)].cData;
	}
	else 
	{
		outWidth  = 0;
		outHeight  = 0;
		if (outData)
			*outData  = 0;
	}
}

void
DpyManager::DrawImmediate	(	void* 				inTex					,		
								uint				inWidth					,
								uint				inHeight				,
								int					inOffsetX				,		
								int					inOffsetY				)
{
	
	if (!inTex) return ;
	uint 		efbW 	 = gx::GetWidth();
	uint 	 	efbH 	 = gx::GetHeight();

	Vec4 xywh	( (efbW/2) - (inWidth/2) + inOffsetX , (efbH/2) - (inHeight/2) + inOffsetY, inWidth , inHeight );
	
	gx::Sync();
	gx::Flush();

	gx::PutBack32_Direct (xywh,(uint8*)inTex,inWidth*4 );
	gx::Sync();
	gx::VSync();
	gx::Present( FALSE );
	gx::Flush();
}
