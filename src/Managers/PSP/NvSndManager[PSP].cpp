/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <NvSndManager.h>
#include <Kernel/PSP/ALG/ALG.h>
#include <Kernel/PSP/NvkSound[PSP].h>
using namespace nv;


//#define	TEST_FX



namespace
{

	// Dolby Pro-Logic II lookup table
	#include "../Common/DPL2.h"

	void
	GetNativeVolumes(	SndManager::OutputMode	inOMode,
						float					inVol,
						float					inPanAngle,
						int&					outVolL,
						int&					outVolR		)
	{
		// volume in [0,1]
		inVol = Clamp( inVol, 0.0f, 1.0f   );

		// panning in [-Pi, Pi]
		int degAngle = int( inPanAngle * RadToDeg );
		while( degAngle < 0	   )	degAngle += 360;
		while( degAngle >= 360 )	degAngle -= 360;
		NV_ASSERT( degAngle >= 0   );
		NV_ASSERT( degAngle <  360 );
		float Lt = DPLII_CoeffTable[degAngle][0];
		float Rt = DPLII_CoeffTable[degAngle][1];

		float Lv=0, Rv=0;
		switch( inOMode ) {
			case SndManager::OM_MONO :
				Lv = Rv = inVol;
				break;
			case SndManager::OM_MONOPAN :
				Lv = Rv = inVol * Max( Absf(Lt), Absf(Rt) );
				break;
			case SndManager::OM_STEREO :
				Lv = inVol * Absf(Lt);
				Rv = inVol * Absf(Rt);
				break;
			case SndManager::OM_DPL2 :
				Lv = inVol * Lt;
				Rv = inVol * Rt;
				break;
		}
		outVolL = int( 4096.f * Lv );
		outVolR = int( 4096.f * Rv );
	}


	struct Channel {
		uint	pitch;
		float	pan;
		float	vol;
	};

	Channel						chA[ audio::NB_CHANNEL ];
	SndManager::OutputMode		omode;
	SndManager::StreamHandler	str_handler;

};



bool
SndManager::Init	(			)
{
	str_handler = NULL;
	omode = OM_STEREO;
	if( !audio::Init() )
		return FALSE;
#ifdef TEST_FX
	audio::EnableMix( audio::MX_WET );
	audio::SetEffectType( audio::FX_TYPE_SPACE );
#endif
	return TRUE;
}


bool
SndManager::Shut	(			)
{
	audio::Shut();
	return TRUE;
}


void
SndManager::Update	(			)
{
	audio::Update();
}


SndManager::StreamHandler
SndManager::SetHandler		(	StreamHandler		inHandler		)
{
	SndManager::StreamHandler old_handler = str_handler;
	str_handler = inHandler;
	return old_handler;
}


bool
SndManager::ProbeOutputMode		(	OutputMode			inOMode		)
{
	if( inOMode == OM_DPL2 )
		return FALSE;		// DPL2 TODO !!
	return TRUE;
}


void
SndManager::SetOutputMode		(	OutputMode			inOMode		)
{
	if( ProbeOutputMode(inOMode) )
		omode = inOMode;
}


SndManager::OutputMode
SndManager::GetOutputMode		(							)
{
	return omode;
}


void
SndManager::SetMasterVolume		(	float		inVol		)
{
	audio::SetMasterVolume( inVol );
}






int
SndManager::SdGetNbMax			(				)
{
	return audio::NB_SOUND_MAX;
}


int
SndManager::SdAny		(					)
{
	return audio::SdAny();
}


SndManager::SdState
SndManager::SdGetState			(	int		inSoundId	)
{
	return SdState( audio::SdGetState(inSoundId) );
}


int
SndManager::SdLock		(	int				inSdId,
							NvSound*		inSndObj,
							float			inFromTime,
							float			inDuration		)
{
	if( !inSndObj )
		return -1;

	int sid = (inSdId==SD_ANY) ? SdAlloc(SD_ANY) : inSdId;
	if( sid < 0 )
		return -1;

	NvkSound* ksnd = inSndObj->GetKInterface();
	if( !ksnd )
		return -1;

	uint32 snd_boffset, snd_bsize;
	ksnd->GetBFData( snd_boffset, snd_bsize );

	bool done = audio::SdLock( 	sid,
								ksnd->GetMode(),
								ksnd->GetFreq(),
								snd_boffset,
								snd_bsize,
								ksnd->GetDuration(),
								inFromTime,
								inDuration	);

	if( done ) {
	//	DebugPrintf( "SndMan: SdLock: ksnd:%xh sid:%d\n", uint32(ksnd), sid );
		return sid;
	}

	if( inSdId == SD_ANY )
		SdFree( sid );
	return -1;
}


bool
SndManager::SdUnlock	(	int			inSoundId		)
{
	return audio::SdUnlock( inSoundId );
}


void
SndManager::SdFree		(	int			inSoundId		)
{
	audio::SdFree( inSoundId );
}


bool
SndManager::SdBreak		(	int			inSoundId		)
{
	return FALSE;
}


void
SndManager::SdFreeAll		(						)
{
	for( ;; ) {
		bool remain = FALSE;
		for( int i = 0 ; i < audio::NB_SOUND_MAX ; i++ ) {
			audio::SdFree( i );
			if( audio::SdGetState(i) != audio::SD_UNUSED )
				remain = TRUE;
		}
		if( !remain )	break;
		core::Update();
		audio::Update();
	}
}


int
SndManager::SdAlloc			(		int		inSoundId		)
{
	if( inSoundId == SD_ANY )
		inSoundId = SdAny();
	return audio::SdAlloc( inSoundId );
}





int
SndManager::ChGetNb		(						)
{
	return audio::NB_CHANNEL;
}


int
SndManager::ChAny	(		)
{
	return audio::ChAny();
}


SndManager::ChState
SndManager::ChGetState	(	int		inChannel		)
{
	return SndManager::ChState( audio::ChGetState(inChannel) );
}


int
SndManager::ChPlay		(	int			inChannel,
							int			inSoundId,
							float		inVol,
							float		inPanAngle,
							float		inPitchFactor	)
{
	int ch = (inChannel==CH_ANY) ? ChAny() : inChannel;
	if( ch<0 || ch>=audio::NB_CHANNEL )
		return -1;
	int snd_freq  = audio::SdGetBaseFreq( inSoundId );
	int psp_pitch = int( float(int(0x1000*snd_freq)) * inPitchFactor / 44100.f );
	int psp_volL;
	int psp_volR;
	GetNativeVolumes( omode, inVol, inPanAngle, psp_volL, psp_volR );
	if( !audio::ChPlay(ch,inSoundId,psp_pitch,psp_volL,psp_volR) )
		return -1;
	chA[ ch ].pitch = psp_pitch;
	chA[ ch ].pan   = inPanAngle;
	chA[ ch ].vol   = inVol;
//	DebugPrintf( "SndMan: ChPlay: sid:%d cid:%d\n", inSoundId, ch );
	return ch;
}


bool
SndManager::ChStop		(	int			inChannel		)
{
	return audio::ChStop( inChannel );
}


void
SndManager::ChStopAll	(			)
{
	for( int i = 0 ; i < audio::NB_CHANNEL ; i++ )
		ChStop( i );
}


bool
SndManager::ChPause			(	int			inChannel		)
{
	return audio::ChSetPitch( inChannel, 0 );
}


bool
SndManager::ChResume		(	int			inChannel		)
{
	return audio::ChSetPitch( inChannel, chA[inChannel].pitch );
}


bool
SndManager::ChSetVolume		(	int			inChannel,
								float		inVol			)
{
	if( inChannel<0 || inChannel>=audio::NB_CHANNEL )
		return FALSE;
	int psp_volL;
	int psp_volR;
	GetNativeVolumes( omode, inVol, chA[inChannel].pan, psp_volL, psp_volR );
	if( !audio::ChSetVolume(inChannel,psp_volL,psp_volR) )
		return FALSE;
	chA[ inChannel ].vol = inVol;
	return TRUE;
}


bool
SndManager::ChSetPanning	(	int			inChannel,
								float		inAngle			)
{
	if( inChannel<0 || inChannel>=audio::NB_CHANNEL )
		return FALSE;
	int psp_volL;
	int psp_volR;
	GetNativeVolumes( omode, chA[inChannel].vol, inAngle, psp_volL, psp_volR );
	if( !audio::ChSetVolume(inChannel,psp_volL,psp_volR) )
		return FALSE;
	chA[inChannel].pan = inAngle;
	return TRUE;
}


bool
SndManager::ChSetPitchFactor	(	int			inChannel,
									float		inFactor	)
{
	if( inChannel<0 || inChannel>=audio::NB_CHANNEL )
		return FALSE;
	int snd_id    = audio::ChGetSoundId( inChannel );
	if( snd_id < 0 )
		return FALSE;
	int snd_freq  = audio::SdGetBaseFreq( snd_id );
	int psp_pitch = int( float(int(0x1000*snd_freq)) * inFactor / 44100.f );
	if( !audio::ChSetPitch(inChannel,psp_pitch) )
		return FALSE;
	chA[ inChannel ].pitch = psp_pitch;
	return TRUE;
}





bool
SndManager::BgmPlay		(	int			inSoundId,
							float		inVol		)
{
	return FALSE;
}


bool
SndManager::BgmStop		(			)
{
	return FALSE;
}


bool
SndManager::BgmPause	(			)
{
	return FALSE;
}


bool
SndManager::BgmBreak	(			)
{
	return FALSE;
}


bool
SndManager::BgmResume	(			)
{
	return FALSE;
}


bool
SndManager::BgmSetVolume	(	float		inVol		)
{
	return FALSE;
}


bool
SndManager::BgmIsPlaying		(			)
{
	return FALSE;
}


float
SndManager::MbToLinearVolume	(	float		inMillibel		)
{
	// [-10000,0] -> [0,1]
	float mb = Clamp( inMillibel, -10000.0f, 0.0f );
	if( mb == -10000.0f )
		return 0.0f;
	else {
		float lin = Powf( 10.0f, mb*(1.0f/2000.0f) );
		return Clamp( lin, 0.0f, 1.0f );
	}
}


float
SndManager::LinearToMbVolume	(	float		inLinear		)
{
	// [0,1] -> [-10000,0]
	float lin = Clamp( inLinear, 0.0f, 1.0f );
	if( lin == 0.0f )
		return -10000.0f;
	else {
		int mb = int( Log10f(lin) * 2000.0f );
		return Clamp( mb, -10000, 0 );
	}
}



