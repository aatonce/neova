/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PSP/NvDpyManager[PSP].h>
#include <Kernel/PSP/NvDpyObject[PSP].h>
#include <Managers/NvProfManager.h>
using namespace nv;

#include "NvDpyManager[PSP]_Tools.h"



#define	SESSION_NB		256

/*
#ifdef _NVCOMP_ENABLE_DBG
#define	ENABLE_ASYNC_LOG
#endif
*/



namespace
{
	bool									presentFrame;
	bool									frameOpened;
	bool									frameSynced;
	uint									frameNo = ~0U;
	uint									flushCpt;
	uint									flushingFrameNo;
	sysvector<NvDpyObject_PSP*>				sessionA[SESSION_NB];
	uint									ctxtCurSession;

	NvDpyObject_PSP*						frameBreakBefore;
	NvDpyObject_PSP*						frameBreakAfter;

	frame::Packet							initFramePkt;
	uint32*									initFrameFBPCmd;	// for swap mode !
	frame::Packet							presentFramePkt;

	sysvector<NvInterface*>					garbage0;
	sysvector<NvInterface*>					garbage1;
	sysvector<NvInterface*>	*				garbager;
}




DpyManager::Raster				DpyManager::raster[DPYR_OFF_FRAME_C32Z32+1];
float							DpyManager::rasterMaxWidth;
float							DpyManager::rasterMaxHeight;
float							DpyManager::projNormalisedXY;
float							DpyManager::ooProjNormalisedXY;
NvDpyContext*					DpyManager::context;
DpyManager::CoordinateSystem	DpyManager::coordSystem;
Matrix							DpyManager::clipMatrix;




bool
DpyManager::Init	(	CoordinateSystem	inCoordinateSystem	)
{
	garbage0.Init();
	garbage1.Init();
	garbager = &garbage0;

	context = NvEngineNew( NvDpyContext );
	context->Init();
	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].Init();

	coordSystem			= inCoordinateSystem;
	frameBreakBefore	= NULL;
	frameBreakAfter		= NULL;
	presentFrame		= FALSE;
	frameNo				= 0;
	frameOpened			= FALSE;
	frameSynced			= TRUE;
	flushingFrameNo		= 0;
	flushCpt			= 0;

	ge::Init( FALSE );		// disable CRTC !
	uint32 offramBaseAddr = ge::frame::VRamLowerAddr;
	uint32 tramBaseAddr   = InitRasters( offramBaseAddr );
	tram::Init( );
	frame::Init();

#ifndef _MASTER
	Printf( "<Neova> VRAM mapping (block-unit):\n" );
	Printf( "<Neova> in-frame   : %d - %d [%dKo]\n", 0, offramBaseAddr, offramBaseAddr>>10 );
	Printf( "<Neova> off-frame  : %d - %d [%dKo]\n", offramBaseAddr, tramBaseAddr, (tramBaseAddr-offramBaseAddr)>>10 );
	Printf( "<Neova> free-ram   : %d - %d [%dKo]\n", tramBaseAddr, ge::frame::VRamUpperAddr, (ge::frame::VRamUpperAddr-tramBaseAddr)>>10 );
	Printf( "<Neova> raster-max : %d x %d pixels\n", int(rasterMaxWidth), int(rasterMaxHeight) );
	NV_ASSERT( tramBaseAddr <= ge::frame::VRamUpperAddr );
#endif


	//
	//	Frame pkts

	{
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );
		pvoid dc0 = DC->vd;

		// Frame init cmds
		{
			initFramePkt.Init();
			initFramePkt.SetSeparator( TRUE );
			ge::FlushContext_CH( DC, &ge::frame::DefContext, ge::ALL_REGS, &initFrameFBPCmd );
			*DC->i32++ = SCE_GE_SET_RET();
			initFramePkt.cadr = DC->Dup( dc0 );
			NV_ASSERT( initFrameFBPCmd );
			AS_UINT32( initFrameFBPCmd ) += (uint32(initFramePkt.cadr)-uint32(dc0));
			DC->vd = dc0;
		}

		// Frame present cmds
		if( !ge::frame::IsSwapMode() ) {
			presentFramePkt.Init();
			presentFramePkt.SetSeparator( TRUE );
			ge::frame::Present_CH( DC );
			*DC->i32++ = SCE_GE_SET_RET();
			presentFramePkt.cadr = DC->Dup( dc0 );
			DC->vd = dc0;
		} else {
			presentFramePkt.cadr = NULL;
		}
	}

	return TRUE;
}


bool
DpyManager::Shut	(				)
{
	SyncFrame();

	while( garbager->size() ) {
		frameNo++;
		FlushGarbager();
	}

	SafeFree( initFramePkt.cadr );
	SafeFree( presentFramePkt.cadr );

	garbage0.Shut();
	garbage1.Shut();
	garbager = NULL;

	context->Shut();
	NvEngineDelete( context );

	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].Shut();

	frame::Shut();
	ge::Shut();
	return TRUE;
}



DpyManager::CoordinateSystem
DpyManager::GetCoordinateSystem	(				)
{
	return coordSystem;
}


uint
DpyManager::GetPhysicalWidth	(				)
{
	return ge::frame::PhysicalSizeX;
}


uint
DpyManager::GetPhysicalHeight	(				)
{
	return ge::frame::PhysicalSizeY;
}


float
DpyManager::GetPhysicalAspect	(				)
{
	float w = float( int(ge::frame::PhysicalSizeX) );
	float h = float( int(ge::frame::PhysicalSizeY) );
	return w/h;
}


uint
DpyManager::GetVirtualWidth		(				)
{
	return ge::frame::VirtualSizeX;
}


uint
DpyManager::GetVirtualHeight	(				)
{
	return ge::frame::VirtualSizeY;
}


float
DpyManager::GetVirtualAspect	(				)
{
	float w = float( int(ge::frame::VirtualSizeX) );
	float h = float( int(ge::frame::VirtualSizeY) );
	return w/h;
}


uint
DpyManager::GetRasterWidth	(	DpyRaster	inRaster	)
{
	NV_ASSERT( inRaster>=DPYR_NULL && inRaster<=DPYR_OFF_FRAME_C32Z32 );
	return raster[inRaster].width;
}


uint
DpyManager::GetRasterHeight	(	DpyRaster	inRaster	)
{
	NV_ASSERT( inRaster>=DPYR_NULL && inRaster<=DPYR_OFF_FRAME_C32Z32 );
	return raster[inRaster].height;
}


uint
DpyManager::GetVSyncRate	(			)
{
	return ge::crtc::vsyncRate;
}


uint
DpyManager::GetLatency		(			)
{
	return 2;
}


uint
DpyManager::GetViewingLatency	(		)
{
	return 2;
}


bool
DpyManager::TranslateCRTC		(	int		inX0,
									int		inY0	)
{
	return FALSE;
}


bool
DpyManager::ReadFrame	(	pvoid			outBuffer	)
{
	if( !outBuffer || uint32(outBuffer)&3 ) {
		NV_DEBUG_WARNING( "DpyManager::ReadFrame() : outBuffer must be 4bytes aligned !" );
		return FALSE;
	}

	SyncFrame();

	ge::frame::GetBack(	outBuffer );
	return TRUE;
}


bool
DpyManager::WriteFrame	(	pvoid		inBuffer	)
{
	if( !inBuffer || uint32(inBuffer)&3 ) {
		NV_DEBUG_WARNING( "DpyManager::WriteFrame() : inBuffer must be 16bytes aligned !" );
		return FALSE;
	}

	SyncFrame();

	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	if( !consoleOnly )
		ge::frame::PutBack(	inBuffer );

	return TRUE;
}


bool
DpyManager::SaveFrame	(	pcstr		inFilename		)
{
#ifdef _MASTER
	return FALSE;
#else
	if( !inFilename || (*inFilename)==0 )
		return FALSE;

	uint   bsize = ge::frame::SizeX * ge::frame::SizeY * 4;
	uint8* mem   = (uint8*) EngineMallocA( 256+bsize, 16 );

	// TARGA header
	Zero( mem, 18 );
	mem[2]	= 2;						// Image type code
	mem[12]	= ge::frame::SizeX;			// low width of image
	mem[13]	= ge::frame::SizeX>>8;		// high width of image
	mem[14]	= ge::frame::SizeY;			// low height of image
	mem[15]	= ge::frame::SizeY>>8;		// high height of image
	mem[16]	= 32;						// Image pixel size
	mem[17]	= 32;						// Image descriptor byte

	// Read & convert ABGR to ARGB
	uint32* bmp  = (uint32*)( mem+256 );
	uint    bmpN = bsize >> 2;
	if( !ReadFrame(bmp) ) {
		EngineFree( mem );
		return FALSE;
	}
	while( bmpN-- )
		*bmp++ = ConvertABGR( PSM_ARGB32, *bmp );
	libc::Memmove( mem+18, mem+256, bsize );

	bool res = nv::file::DumpToFile( inFilename, mem, 18+bsize );
	EngineFree( mem );
	return res;
#endif
}


uint
DpyManager::GetFrameNo		(		)
{
	return frameNo;
}


Psm
DpyManager::GetFramePSM		(		)
{
	return PSM_ABGR32;
}


bool
DpyManager::BeginFrame		(		)
{
	NV_ASSERTC( !frameOpened, "Frame is already opened !" );
	if( frameOpened )
		return FALSE;

	// reset session context
	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].clear();
	ctxtCurSession = 0;

	// reset context stacks
	Vec4 lightcolor( 1, 1, 1, 1 );
	Vec2 clipRange( 1.0f, 500.0f );
	Vec4 viewport( 0, 0, ge::frame::SizeX, ge::frame::SizeY );
	context->Reset();
	context->SetLight( 0, &lightcolor, NULL );
	context->SetLight( 1, &lightcolor, (Vec3*)&Vec3::UNIT_X );
	context->SetLight( 2, &lightcolor, (Vec3*)&Vec3::UNIT_Y );
	context->SetView( NULL, NULL, &clipRange, &viewport );
	context->SetTarget( DpyTarget(DPYR_IN_FRAME) );

	// setup new frame
	frameOpened = TRUE;
	frameNo++;

	return TRUE;
}


bool
DpyManager::EndFrame	(		)
{
	NV_ASSERTC( frameOpened, "Frame is not opened !" );
	if( !frameOpened )
		return FALSE;
	frameOpened = FALSE;
	return TRUE;
}



namespace
{
	float		perf_GPU_FPS	= 0;
	float		perf_CPU_FPS	= 0;
	uint32		perf_drawCpt	= 0;
	uint32		perf_triCpt		= 0;
	uint32		perf_locCpt		= 0;
	uint32		perf_vtxCpt		= 0;
	uint32		perf_primCpt	= 0;
	uint32		perf_texBSize	= 0;
}


bool
DpyManager::FlushFrame	(	bool	inPresent,
							bool	inGetPerf	)
{
	NV_ASSERTC( !frameOpened, "Frame must not be opened !" );
	NV_ASSERTC( flushingFrameNo != frameNo, "Frame has been already flushed !" );
	if( frameOpened || flushingFrameNo==frameNo )
		return FALSE;

	#ifdef ENABLE_ASYNC_LOG
	{
		static bool async_output = FALSE;
		ctrl::Status* c = ctrl::GetStatus( 0 );
		if( 	c
			&&	c->button[ctrl::BT_BUTTON4] > 0.5f
			&&	c->button[ctrl::BT_BUTTON5] > 0.5f
			&&	c->button[ctrl::BT_BUTTON6] > 0.5f
			&&	c->button[ctrl::BT_BUTTON7] > 0.5f	)
		{
			if( !async_output )
				core::SetParameter( core::PR_ASYNC_LOG, TRUE );
			async_output = TRUE;
		}
		else
		{
			async_output = FALSE;
		}
	}
	#endif

	bool   swapMode = ge::frame::IsSwapMode();
	bool   async, vsync;
	core::GetParameter( core::PR_ASYNC_DRAW, &async );
	core::GetParameter( core::PR_VSYNC_DRAW, &vsync );

	// swap frame dmac heaps
	dmac::SwapHeap();
	#ifdef _NVCOMP_ENABLE_DBG
	{
		dmac::Cursor* curC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( curC );
		uint32* curP	 = curC->i32;
		uint32* curP_end = curP + (curC->GetLeftBSize()>>2);
		while( curP < curP_end )
			*curP++ = SCE_GE_SET_RET();		// ge ret cmd, will crash with DC invalid usage !
	}
	#endif

	// begin frame
	frame::Begin( async );

	// init frame
	frame::Push( &initFramePkt );

	// next frame present ?
	if( !swapMode && presentFrame )
		frame::Push( &presentFramePkt );

	// draw
	for( uint s = 0 ; s < SESSION_NB ; s++ ) {
		uint N = sessionA[s].size();
		if( N == 0 )	continue;
		NvDpyObject_PSP	** dpyObjP		= sessionA[s].data();
		NvDpyObject_PSP	** dpyObj_endP	= dpyObjP + N;
		while( dpyObjP != dpyObj_endP ) {
			NV_ASSERT( *dpyObjP );
			if( *dpyObjP == frameBreakBefore )	goto dpy_frame_break;
			(*dpyObjP)->Draw();
			if( *dpyObjP == frameBreakAfter )	goto dpy_frame_break;
			dpyObjP++;
		}
	}
dpy_frame_break:

	// end frame
	frame::End();

	// sync frame
	frame::Sync();
	frameSynced		= TRUE;
	flushingFrameNo	= frameNo;

	// swap FBP ?
	if( swapMode )
		*initFrameFBPCmd = ge::frame::DefContext.fbp;

	// vsync & execute DMA
	frame::Flush( vsync, (swapMode&&presentFrame) );
	frameSynced	= FALSE;

	if( ++flushCpt == 2 )
		ge::crtc::Enable();

	// Get performances ?
	if( inGetPerf ) {
		frame::WaitDMA();
		float dmaTime	= frame::GetPerformance( frame::PERF_DMA );
		float cpuTime	= frame::GetPerformance( frame::PERF_BEGIN_BEGIN )
						- frame::GetPerformance( frame::PERF_SYNC_UPDATE_IDLE )
						- frame::GetPerformance( frame::PERF_VSYNC_IDLE )
						- frame::GetPerformance( frame::PERF_WAIT_DMA );
		perf_GPU_FPS	= dmaTime ? (1.0f/dmaTime) : 0.f;
		perf_CPU_FPS	= cpuTime ? (1.0f/cpuTime) : 0.f;
		perf_texBSize	= int( frame::GetPerformance( frame::PERF_TRXD_TEXBSIZE ) );
		perf_drawCpt	= 0;
		perf_triCpt		= 0;
		perf_locCpt		= 0;
		perf_vtxCpt		= 0;
		perf_primCpt	= 0;
		for( uint s = 0 ; s < SESSION_NB ; s++ ) {
			uint N = sessionA[s].size();
			if( N == 0 )	continue;
			NvDpyObject_PSP	** dpyObjP		= sessionA[s].data();
			NvDpyObject_PSP	** dpyObj_endP	= dpyObjP + N;
			while( dpyObjP != dpyObj_endP ) {
				NV_ASSERT( *dpyObjP );
				uint32 objTriCpt=0, objLocCpt=0, objVtxCpt=0, objPrimCpt=0;
				if( (*dpyObjP)->GetPerformances(objTriCpt,objLocCpt,objVtxCpt,objPrimCpt) ) {
					perf_triCpt	 += objTriCpt;
					perf_locCpt	 += objLocCpt;
					perf_vtxCpt	 += objVtxCpt;
					perf_primCpt += objPrimCpt;
				}
				perf_drawCpt += (*dpyObjP)->dpyctxt.chainCpt;
				dpyObjP++;
			}
		}
	}

	PROF_SETUP( 0x11223344, frameNo );

	// swap frame mapping ?
	if( swapMode ) {
		ge::frame::Swap();
		InitRaster(	DpyManager::raster[ int(DPYR_IN_FRAME) ],
					ge::frame::SizeX,
					ge::frame::SizeY,
					ge::frame::BackAddr,
					ge::frame::BackPSM,
					TRUE,
					ge::frame::DepthAddr	);
	}

	presentFrame = inPresent;
	FlushGarbager();

	return TRUE;
}


float
DpyManager::GetPerformance		(	Performance			inPerf		)
{
	if( inPerf == PERF_GPU_FPS )	return perf_GPU_FPS;
	if( inPerf == PERF_CPU_FPS )	return perf_CPU_FPS;
	if( inPerf == PERF_DRAW_CPT )	return float(int(perf_drawCpt));
	if( inPerf == PERF_TRI_CPT )	return float(int(perf_triCpt));
	if( inPerf == PERF_LOC_CPT )	return float(int(perf_locCpt));
	if( inPerf == PERF_VTX_CPT )	return float(int(perf_vtxCpt));
	if( inPerf == PERF_PRIM_CPT )	return float(int(perf_primCpt));
	if( inPerf == PERF_TEX_BSIZE )	return float(int(perf_texBSize));
	return 0.f;
}


void
DpyManager::SyncFrame	(			)
{
	NV_ASSERTC( !frameOpened, "Frame must not be opened !" );
	if( frameOpened )	return;

	if( !frameSynced ) {
		frame::Begin();
		frame::End();
		frame::Sync();
		frameSynced = TRUE;
	}

	FlushGarbager();
}


bool
DpyManager::IsInFrame	(	NvDpyObject_PSP*	inDpyObj	)
{
	if( !inDpyObj )
		return FALSE;
	return ( inDpyObj->dpyctxt.drawFrameNo == frameNo );
}


bool
DpyManager::IsInFrame	(	NvDpyObject*		inDpyObj	)
{
	return IsInFrame( inDpyObj ? (NvDpyObject_PSP*)inDpyObj->GetBase() : NULL );
}


bool
DpyManager::IsDrawing	(	uint32				inFrameNo	)
{
	// my frame is the current frame and is flushed ?
	if( inFrameNo==flushingFrameNo && inFrameNo==frameNo )
		return !frameSynced;
	else
		return ( inFrameNo==flushingFrameNo || inFrameNo==frameNo );
}


bool
DpyManager::IsDrawing	(	NvDpyObject_PSP*	inDpyObj	)
{
	if( !inDpyObj )
		return FALSE;
	return IsDrawing( inDpyObj->dpyctxt.drawFrameNo );
}


bool
DpyManager::IsDrawing	(	NvDpyObject*		inDpyObj	)
{
	return IsDrawing( inDpyObj ? (NvDpyObject_PSP*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetFrameBreakBefore	(	NvDpyObject_PSP*		inDpyObj	)
{
	frameBreakAfter  = NULL;
	frameBreakBefore = inDpyObj;
}


void
DpyManager::SetFrameBreakBefore	(	NvDpyObject*		inDpyObj	)
{
	SetFrameBreakBefore( inDpyObj ? (NvDpyObject_PSP*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetFrameBreakAfter	(	NvDpyObject_PSP*		inDpyObj	)
{	
	frameBreakBefore = NULL;
	frameBreakAfter	 = inDpyObj;
}


void
DpyManager::SetFrameBreakAfter	(	NvDpyObject*		inDpyObj	)
{	
	SetFrameBreakAfter( inDpyObj ? (NvDpyObject_PSP*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetTarget	(	DpyTarget		inTarget	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetTarget( inTarget );
}


void
DpyManager::SetView		(	Matrix*			inViewTR,
							Matrix*			inProjTR,
							Vec2*			inClipRange,
							Vec4*			inViewport	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetView( inViewTR, inProjTR, inClipRange, inViewport );
}


void
DpyManager::SetSession	(	uint8			inSessionNo		)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	ctxtCurSession = (inSessionNo < SESSION_NB) ? inSessionNo : SESSION_NB-1;
}


void
DpyManager::SetWorldTR	(	Matrix*		inWorldTR	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetWorldTR( inWorldTR );
}


void
DpyManager::SetLight	(	uint		inLightNo,
							Vec4*		inColor,
							Vec3*		inDirection		)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetLight( inLightNo, inColor, inDirection );
}


bool
DpyManager::Draw		(	NvDpyObject_PSP*	inDpyObj	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return FALSE;

	if( !inDpyObj )
		return FALSE;

	// First draw in the current frame ?
	bool firstDraw = !IsInFrame( inDpyObj );

	int chainNext = (firstDraw ? -1 : inDpyObj->dpyctxt.chainHead );
	int chainHead = context->Push( chainNext );
	if( chainHead < 0 )
		return FALSE;

	inDpyObj->dpyctxt.chainHead = chainHead;
	inDpyObj->dpyctxt.chainCpt += 1;

	// Not already in the current draw queue ?
	if( firstDraw ) {
		NV_ASSERT( ctxtCurSession < SESSION_NB );
		sessionA[ ctxtCurSession ].push_back( inDpyObj );
		inDpyObj->dpyctxt.drawFrameNo = frameNo;
		inDpyObj->dpyctxt.chainCpt = 1;
	}

	return TRUE;
}


bool
DpyManager::Draw	(	NvDpyObject*		inDpyObj	)
{
	return Draw( inDpyObj ? (NvDpyObject_PSP*)inDpyObj->GetBase() : NULL );
}


/*
void
DpyManager::GetProjThroughMode	(	DpyRaster		inRaster,
									Vec4*			inViewport,
									Vec4*			outProjScale,
									Vec4*			outProjTrans	)
{
	NV_ASSERT( inRaster>=DPYR_NULL && inRaster<=DPYR_OFF_FRAME_C32Z32 );
	NV_ASSERT( inViewport );

	// X&Y in viewport (no scale + trans)
	// Z in [Zmax->0] from [0,1]

	if( outProjScale ) {
		outProjScale->x	= 1.f;
		outProjScale->y	= 1.f;
		outProjScale->z = zbuffMax;
		outProjScale->w = 0.0f;
	}

	if( outProjTrans ) {
		// { dX, dY } viewport relative center in the raster frame
		float frameHalfW   = int( raster[inRaster].width  ) >> 1;
		float frameHalfH   = int( raster[inRaster].height ) >> 1;
		outProjTrans->x	   = (2048.0f-frameHalfW) + int(inViewport->x);
		outProjTrans->y	   = (2048.0f-frameHalfH) + int(inViewport->y);
		outProjTrans->z	   = 0.f;
		outProjTrans->w	   = 0.0f;
	}
}


void
DpyManager::GetProjNormalised	(	DpyRaster		inRaster,
									Vec4*			inViewport,
									Vec4*			outProjScale,
									Vec4*			outProjTrans	)
{
	NV_ASSERT( inRaster>=DPYR_NULL && inRaster<=DPYR_OFF_FRAME_C32Z32 );
	NV_ASSERT( inViewport );

	// X&Y in viewport (scale + trans)
	// Z in [Zmax->0] from [-1,+1]
	// voir DpyManager::Init() pour les paramètres de projection

	if( outProjScale ) {
		outProjScale->x	= +ooProjNormalisedXY * (inViewport->z*0.5f);
		outProjScale->y	= -ooProjNormalisedXY * (inViewport->w*0.5f);
		outProjScale->z = -(0.5f*zbuffMax);
		outProjScale->w = 0.0f;
	}

	if( outProjTrans ) {
		// { dX, dY } viewport relative center in the raster frame
		float frameCenterX = float( int(raster[inRaster].width)  ) * 0.5f;
		float frameCenterY = float( int(raster[inRaster].height) ) * 0.5f;
		float vpCenterX    = inViewport->x + inViewport->z * 0.5f;
		float vpCenterY    = inViewport->y + inViewport->w * 0.5f;
		float dX		   = frameCenterX - vpCenterX;
		float dY		   = frameCenterY - vpCenterY;
		outProjTrans->x	   = 2048.0f - dX;
		outProjTrans->y	   = 2048.0f - dY;
		outProjTrans->z	   = 0.5f*zbuffMax;
		outProjTrans->w	   = 0.0f;
	}
}
*/

float
DpyManager::GetZProjNormalised	(	float			inZ,
									Matrix*			inProjTR		)
{
	// Projected Z, in valid range [-1,+1]
	if( coordSystem == CS_RIGHT_HANDED )
		inZ = - inZ;	// LH/RH ?
	Vec4 xyzw( 0, 0, inZ, 1 );
	Vec4Apply( &xyzw, &xyzw, inProjTR );
	return (xyzw.w==0.0f) ? -1.0f : xyzw.z/xyzw.w;
}


uint16
DpyManager::GetZProjBuffValue	(	float			inZ,
									Matrix*			inProjTR		)
{
	// Projection in [-1,+1]
	float pnZ = GetZProjNormalised( inZ, inProjTR );
		  pnZ = Clamp( pnZ, -1.0f, 1.0f );

	// Z in [Zmax->0] from [-1,+1]
	float  fZ = - pnZ * (0.5f*zbuffMax) + (0.5f*zbuffMax);
	uint16 iZ = uint16( int(fZ) );
	return iZ;
}


uint
DpyManager::SetupDrawing	(	dmac::Cursor*			DC,
								DpyRaster				inRaster,
								NvDpyContext::View*		inView,
								Matrix*					inWorldTR,
								bool					inThroughMode,
								bool					inEnableCalls		)
{
	NV_ASSERT( inRaster>=DPYR_NULL && inRaster<=DPYR_OFF_FRAME_C32Z32 );
	NV_ASSERT( inView );
	NV_ASSERT( inWorldTR );
	NV_ASSERT_DC( DC );
	uint32* dc0 = DC->i32;

	Raster* geRaster = &raster[ int(inRaster) ];
	float vpX0		 = inView->viewport.x;
	float vpY0		 = inView->viewport.y;
	float vpX1		 = vpX0 + inView->viewport.z;
	float vpY1		 = vpY0 + inView->viewport.w;
	float rasterHW	 = float( int(geRaster->width >>1) );
	float rasterHH	 = float( int(geRaster->height>>1) );

	DC->i32[0] = geRaster->fbp;
	DC->i32[1] = geRaster->fbw;
	DC->i32[2] = geRaster->fpf;
	DC->i32[3] = geRaster->offsetxy[0];
	DC->i32[4] = geRaster->offsetxy[1];
	DC->i32[5] = geRaster->zbp;
	DC->i32[6] = geRaster->zbw;
	DC->i32[7] = geRaster->zmsk;
	DC->i32[8] = SCE_GE_SET_SCISSOR1( int(vpX0),   int(vpY0)   );
	DC->i32[9] = SCE_GE_SET_SCISSOR2( int(vpX1)-1, int(vpY1)-1 );
	DC->i32   += 10;

	if( inThroughMode )
	{
		// Optimized version of 2D
		// o Wc is fixed to 1024
		// o -Wc <= Xc/Yc/Zc <= +Wc   => The clipping test always pass !
		// o Xs & Ys /= Wc            => To obtain the screen coordinates
		// o Zs in [0,65535]
		DC->i32[0] = SCE_GE_SET_SX( 0x448000 );			// 1024
		DC->i32[1] = SCE_GE_SET_SY( 0x448000 );			// 1024
		DC->i32[2] = SCE_GE_SET_SZ( 0x477FFF );			// 65535
		DC->i32[3] = SCE_GE_SET_TX( ExtFp24( 2048.f-rasterHW+vpX0 ) );
		DC->i32[4] = SCE_GE_SET_TY( ExtFp24( 2048.f-rasterHH+vpY0 ) );
		DC->i32[5] = SCE_GE_SET_TZ( 0 );
		DC->i32   += 6;

		*DC->i32++ = SCE_GE_SET_WORLDN(0);
		BuildGeMatrix12( DC->i32, inWorldTR, SCE_GE_CMD_WORLDD );		DC->i32 += 12;

		const uint32 v0 = SCE_GE_SET_VIEWD( 0 );
		const uint32 v1 = SCE_GE_SET_VIEWD( 0x3F8000 );
		DC->i32[0] = SCE_GE_SET_VIEWN(0);
		DC->i32[1] = v1;	DC->i32[4] = v0;	DC->i32[7] = v0;	DC->i32[10] = v0;
		DC->i32[2] = v0;	DC->i32[5] = v1;	DC->i32[8] = v0;	DC->i32[11] = v0;
		DC->i32[3] = v0;	DC->i32[6] = v0;	DC->i32[9] = v1;	DC->i32[12] = v0;
		DC->i32   += 13;

		const uint32 p0 = SCE_GE_SET_PROJD( 0);
		const uint32 p1 = SCE_GE_SET_PROJD( 0x3F8000 );
		const uint32 p1k= SCE_GE_SET_PROJD( 0x448000 );	// 1024
		DC->i32[0] = SCE_GE_SET_PROJN(0);
		DC->i32[1] = p1;	DC->i32[5] = p0;	DC->i32[9] = p0;	DC->i32[13] = p0;
		DC->i32[2] = p0;	DC->i32[6] = p1;	DC->i32[10]= p0;	DC->i32[14] = p0;
		DC->i32[3] = p0;	DC->i32[7] = p0;	DC->i32[11]= p1;	DC->i32[15] = p0;
		DC->i32[4] = p0;	DC->i32[8] = p0;	DC->i32[12]= p0;	DC->i32[16] = p1k;
		DC->i32   += 17;
	}
	else
	{
		// Optimized version of 3D mode
		float vpHW = inView->viewport.z * 0.5f;
		float vpHH = inView->viewport.w * 0.5f;
		float vpDX = rasterHW - (vpX0+vpHW);
		float vpDY = rasterHH - (vpY0+vpHH);
		DC->i32[0] = SCE_GE_SET_SX( ExtFp24( +ooProjNormalisedXY * vpHW ) );
		DC->i32[1] = SCE_GE_SET_SY( ExtFp24( -ooProjNormalisedXY * vpHH ) );
		DC->i32[2] = SCE_GE_SET_SZ( 0xC6FFFF );		// -0.5f*zbuffMax
		DC->i32[3] = SCE_GE_SET_TX( ExtFp24(2048.f-vpDX) );
		DC->i32[4] = SCE_GE_SET_TY( ExtFp24(2048.f-vpDY) );
		DC->i32[5] = SCE_GE_SET_TZ( 0x46FFFF );		// +0.5f*zbuffMax
		DC->i32   += 6;

		*DC->i32++ = SCE_GE_SET_WORLDN(0);
		BuildGeMatrix12( DC->i32, inWorldTR, SCE_GE_CMD_WORLDD );		DC->i32 += 12;
		*DC->i32++ = SCE_GE_SET_VIEWN(0);
		BuildGeMatrix12( DC->i32, &inView->viewTR, SCE_GE_CMD_VIEWD );	DC->i32 += 12;
		*DC->i32++ = SCE_GE_SET_PROJN(0);
		Matrix toclipTR;
		MatrixMul( &toclipTR, &inView->projTR, &clipMatrix );
		BuildGeMatrix16( DC->i32, &toclipTR, SCE_GE_CMD_PROJD );		DC->i32 += 16;
	}

	NV_ASSERT_DC( DC );
	return (DC->i32 - dc0);
}


void
DpyManager::AddToGarbager( NvInterface*		inITF )
{
	NV_ASSERT( garbager );
	if( !inITF )
		return;

#ifdef _NVCOMP_ENABLE_DBG
	if( garbager->size() ) {
		NvInterface** itf	  = garbager->data();
		NvInterface** itf_end = itf + garbager->size();
		while( itf != itf_end ) {
			NV_ASSERT( *itf );
			NV_ASSERTC( (*itf)!=inITF, "Interface 0x%08x is already garbaging !" );
			itf++;
		}
	}
#endif

	garbager->push_back( inITF );
}


void
DpyManager::FlushGarbager	(	)
{
	NV_ASSERT( garbager );

	// Swap garbager before to allow Release() to call AddToGarbager() again !
	sysvector<NvInterface*>* _garbager = (garbager==&garbage0) ? &garbage1 : &garbage0;
	_garbager->clear();
	Swap( _garbager, garbager );

	// Release garbaging objets
	if( _garbager->size() ) {
		NvInterface** itf	  = _garbager->data();
		NvInterface** itf_end = itf + _garbager->size();
		while( itf != itf_end ) {
			NV_ASSERT( *itf );
			(*itf)->Release();		// Note that Release() can call AddToGarbager() again !
			itf++;
		}
	}
}




void
DpyManager::DrawImmediate	(	void* 				inTex					,		
														uint				inWidth					,
														uint				inHeight				,
														int					inOffsetX				,		
														int					inOffsetY				)
{
	
}