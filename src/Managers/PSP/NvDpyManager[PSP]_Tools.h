/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



namespace
{

	void
	InitRaster	(	DpyManager::Raster&		outRaster,
					uint					inWidth,
					uint					inHeight,
					uint32					inFrameAddr,
					uint					inFramePSM,
					bool					inHasDepth  = FALSE,
					uint32					inDepthAddr = 0		)
	{
		NV_COMPILE_TIME_ASSERT( SCE_GE_TPF_5650 == SCE_GE_FPF_5650 );
		NV_COMPILE_TIME_ASSERT( SCE_GE_TPF_5551 == SCE_GE_FPF_5551 );
		NV_COMPILE_TIME_ASSERT( SCE_GE_TPF_4444 == SCE_GE_FPF_4444 );
		NV_COMPILE_TIME_ASSERT( SCE_GE_TPF_8888 == SCE_GE_FPF_8888 );

		outRaster.width			= inWidth;
		outRaster.height		= inHeight;
		outRaster.rgbaAddr		= inFrameAddr;
		outRaster.depthAddr		= inDepthAddr;
		outRaster.fbp			= SCE_GE_SET_FBP_ADDR24( inFrameAddr );
		outRaster.fbw			= SCE_GE_SET_FBW_BASE8( Round64(inWidth), inFrameAddr );
		outRaster.fpf			= SCE_GE_SET_FPF( inFramePSM );
		outRaster.offsetxy[0]	= SCE_GE_SET_OFFSETX( (2048-inWidth/2)<<4 );
		outRaster.offsetxy[1]	= SCE_GE_SET_OFFSETY( (2048-inHeight/2)<<4 );
		outRaster.zbp			= inHasDepth ? SCE_GE_SET_ZBP_ADDR24( inDepthAddr ) : 0;
		outRaster.zbw			= inHasDepth ? SCE_GE_SET_ZBW_BASE8( Round64(inWidth), inDepthAddr ) : 0;
		outRaster.zmsk			= SCE_GE_SET_ZMSK( inHasDepth?0:1 );
		uint	tw		 = GetCeilPow2( inWidth );
		uint	th		 = GetCeilPow2( inHeight );
		uint32	vramAddr = (uint32)ge::GetVRamAddr( inFrameAddr );
		outRaster.tratio.x		= float(int(inWidth)) / float(int(1<<tw));
		outRaster.tratio.y		= float(int(inHeight)) / float(int(1<<th));
		outRaster.tbp0			= SCE_GE_SET_TBP0_ADDR24( vramAddr );
		outRaster.tbw0			= SCE_GE_SET_TBW0_BASE8( Round64(inWidth), vramAddr );	// Same buffer width as frame !
		outRaster.tsize0		= SCE_GE_SET_TSIZE0( tw, th );
		outRaster.tpf			= SCE_GE_SET_TPF( inFramePSM, 0 );
		outRaster.tmode			= SCE_GE_SET_TMODE( SCE_GE_TMODE_HSM_NORMAL, 0, 0 );
	}


	uint
	InitRasters	(	uint	inBaseBAddr	)
	{
		uint32 offFrameW, offFrameH;
		core::GetParameter( core::PR_OFF_FRAME_W, &offFrameW );
		core::GetParameter( core::PR_OFF_FRAME_H, &offFrameH );
		// vram mapping for DPYR_OFF_FRAME_C32Z32 configuration
		uint offFrameAddr	= RoundX( inBaseBAddr, 8*1024 );
		uint offDepthAddr	= RoundX( offFrameAddr+Round64(offFrameW)*offFrameH*4, 8*1024 );	// FBW multiple of 64 !
		uint offEndAddr		= RoundX( offDepthAddr+Round64(offFrameW)*offFrameH*4, 8*1024 );	// FBW multiple of 64 !

		DpyManager::rasterMaxWidth  = 0;
		DpyManager::rasterMaxHeight = 0;

		Zero( DpyManager::raster );
		for( uint i = DPYR_IN_FRAME ; i <= DPYR_OFF_FRAME_C32Z32 ; i++ )
		{
			switch( DpyRaster(i) )
			{
			case DPYR_IN_FRAME :
				{
					InitRaster(	DpyManager::raster[i],
								ge::frame::SizeX,
								ge::frame::SizeY,
								ge::frame::BackAddr,
								ge::frame::BackPSM,
								TRUE,
								ge::frame::DepthAddr	);
				}
				break;

			case DPYR_OFF_FRAME_C16 :
				if( offFrameW && offFrameH )
				{
					InitRaster(	DpyManager::raster[i],
								offFrameW*2,
								offFrameH*2,
								offFrameAddr,
								SCE_GE_FPF_5551	);
				}
				break;

			case DPYR_OFF_FRAME_C32H :
				if( offFrameW && offFrameH )
				{
					InitRaster(	DpyManager::raster[i],
								offFrameW*2,
								offFrameH,
								offFrameAddr,
								SCE_GE_FPF_8888	);
				}
				break;

			case DPYR_OFF_FRAME_C32V :
				if( offFrameW && offFrameH )
				{
					InitRaster(	DpyManager::raster[i],
								offFrameW,
								offFrameH*2,
								offFrameAddr,
								SCE_GE_FPF_8888	);
				}
				break;

			case DPYR_OFF_FRAME_C16Z16H :
				if( offFrameW && offFrameH )
				{
					InitRaster(	DpyManager::raster[i],
								offFrameW*2,
								offFrameH,
								offFrameAddr,
								SCE_GE_FPF_5551,
								TRUE,
								offDepthAddr	);
				}
				break;

			case DPYR_OFF_FRAME_C16Z16V :
				if( offFrameW && offFrameH )
				{
					InitRaster(	DpyManager::raster[i],
								offFrameW,
								offFrameH*2,
								offFrameAddr,
								SCE_GE_FPF_5551,
								TRUE,
								offDepthAddr	);
				}
				break;

			case DPYR_OFF_FRAME_C32Z32 :
				if( offFrameW && offFrameH )
				{
					InitRaster(	DpyManager::raster[i],
								offFrameW,
								offFrameH,
								offFrameAddr,
								SCE_GE_FPF_8888,
								TRUE,
								offDepthAddr	);
				}
				break;
			}

			DpyManager::rasterMaxWidth  = Max( DpyManager::rasterMaxWidth,  float(DpyManager::raster[i].width)  );
			DpyManager::rasterMaxHeight = Max( DpyManager::rasterMaxHeight, float(DpyManager::raster[i].height) );
		}

		// Init clipping parameters
		// La translation maximale sur X/Y, due au viewport, est de rasterMaxHalfSize pixels.
		// En consid�rant cette translation, le range de clipping valide est
		//		[ - (2040-rasterMaxHalfSize) , + (2040-rasterMaxHalfSize) ]
		// sur X/Y, avec une guardband r�duite de 16 pixels.
		// Les sommets sont scal�s par rapport aux dimensions du viewport.
		// Ce scale doit �tre pris en compte dans le clipping 4D, par les
		// coefficients m11 et m22 de la matrice de projection.
		// Afin de rendre ces coefficients de matrice ind�pendants du viewport,
		// la valeur maximale de viewport est utilis�e, correspondant � rasterMaxHalfSize.
		// Ce scale maximale de viewport est corrig� dans le scale de projection (voir GetProjNormalised()).
		// Donc :
		// - m11 et m22 sont multipli�s par projNormalisedXY (=> clipMatrix)
		// - projScale.x = (viewportWidth/2)  / projNormalisedXY
		//   projScale.y = (viewportHeight/2) / projNormalisedXY

		float viewportMaxHSize			= Max( DpyManager::rasterMaxWidth, DpyManager::rasterMaxHeight ) * 0.5f;
		float guardBandClip				= 1.0f / (2040.0f - viewportMaxHSize);
		DpyManager::projNormalisedXY	= viewportMaxHSize * guardBandClip;
		DpyManager::ooProjNormalisedXY	= 1.0f / DpyManager::projNormalisedXY;

		MatrixIdentity( &DpyManager::clipMatrix );
		DpyManager::clipMatrix.m11 = DpyManager::projNormalisedXY;
		DpyManager::clipMatrix.m22 = DpyManager::projNormalisedXY;

		for( uint i = DPYR_IN_FRAME ; i <= DPYR_OFF_FRAME_C32Z32 ; i++ )
		{
			float rw	= float( int(DpyManager::raster[i].width)  );
			float rh	= float( int(DpyManager::raster[i].height) );
			float rx0	= 2048.0f - rw * 0.5f;
			float ry0	= 2048.0f - rh * 0.5f;

			// 2D
			// Z in [Zmax->0] from [0,1]

			DpyManager::raster[i].scale0[0].x	= 1.f;
			DpyManager::raster[i].scale0[0].y	= 1.f;
			DpyManager::raster[i].scale0[0].z	= DpyManager::zbuffMax;
			DpyManager::raster[i].scale0[0].w	= 0.f;

			DpyManager::raster[i].trans0[0].x	= rx0 /*+ vp_x*/;
			DpyManager::raster[i].trans0[0].y	= ry0 /*+ vp_y*/;
			DpyManager::raster[i].trans0[0].z	= 0.f;
			DpyManager::raster[i].trans0[0].w	= 0.f;

			// 3D
			// Z in [Zmax->0] from [-1,+1]

			DpyManager::raster[i].scale0[1].x	= + DpyManager::ooProjNormalisedXY /*+ vp_w/2*/;
			DpyManager::raster[i].scale0[1].y	= - DpyManager::ooProjNormalisedXY /*+ vp_h/2*/;
			DpyManager::raster[i].scale0[1].z	= - DpyManager::zbuffMax * 0.5f;
			DpyManager::raster[i].scale0[1].w	= 0.f;

			DpyManager::raster[i].trans0[1].x	= rx0 /*+ (vp_x + vp_w/2)*/;
			DpyManager::raster[i].trans0[1].y	= ry0 /*+ (vp_y + vp_h/2)*/;
			DpyManager::raster[i].trans0[1].z	= DpyManager::zbuffMax * 0.5f;
			DpyManager::raster[i].trans0[1].w	= 0.f;
		}

		return offEndAddr;
	}

}

