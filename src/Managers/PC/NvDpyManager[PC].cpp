/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Kernel/PC/NvDpyManager[PC].h>
#include <Kernel/PC/NvDpyObject[PC].h>
#include <NvRscManager.h>
#include <NvMesh.h>
using namespace nv;
using namespace nv::math;
using namespace DpyManager;


#define	SESSION_NB				256


namespace
{

	/*float		perf_GPU_FPS	= 0;
	float		perf_CPU_FPS	= 0;
	uint32		perf_triCpt		= 0;
	uint32		perf_locCpt		= 0;
	uint32		perf_vtxCpt 	= 0;
	uint32		perf_primCpt	= 0;
	uint32		perf_texBSize	= 0;
	*/
	uint32		perf_drawCpt	= 0;

	struct TargetHwnd
	{
		HWND		hwnd;
		int			frontTargetId;
		int			backRasterId;
	};

	DpyManager::CoordinateSystem			coordSystem;

	sysvector <TargetHwnd>					targetHwndA;

	sysvector <uint32>						devPaletteA;

	uint									frameNo = ~0U;
	uint									flushingFrameNo;

	bool									frameOpened;
	bool									frameSynced;

	sysvector<NvDpyObject_PC*>				sessionA[SESSION_NB];
	uint									ctxtCurSession;
	bool									presentFrame;

	NvDpyObject_PC*							frameBreakBefore;
	NvDpyObject_PC*							frameBreakAfter;

	sysvector<NvInterface*>					garbage0;
	sysvector<NvInterface*>					garbage1;
	sysvector<NvInterface*>	*				garbager;

	void InitRasterAndTarget() {
		bool	fullscreen;
		HWND	targetHwnd;
		uint32	backW, backH, offFrameW, offFrameH;
		core::GetParameter( core::PR_DX_FULLSCREEN, 		&fullscreen 			);
		core::GetParameter( core::PR_W32_HWND, 				(uint32*)&targetHwnd	);
		core::GetParameter( core::PR_BACK_FRAME_W,			&backW					);
		core::GetParameter( core::PR_BACK_FRAME_H,			&backH					);
		core::GetParameter( core::PR_OFF_FRAME_W,			&offFrameW				);
		core::GetParameter( core::PR_OFF_FRAME_H,			&offFrameH				);

		// Init rasters
		DpyManager::hwraster[ DPYR_NULL ] = -1;
		// DPYR_IN_FRAME
		DpyManager::hwraster[ DPYR_IN_FRAME ] = -1;
		if( fullscreen ) {
			DpyManager::hwraster[DPYR_IN_FRAME] = Hrdw::CreateRaster( backW, backH, 32, 32, TRUE );
			NV_ASSERT( DpyManager::hwraster[DPYR_IN_FRAME] >= 0 );
		}

		// DPYR_OFF_FRAME_...
		if( offFrameW && offFrameH ) {
			DpyManager::hwraster[DPYR_OFF_FRAME_C16]		= Hrdw::CreateRaster( offFrameW*2, offFrameH*2, 16           );
			DpyManager::hwraster[DPYR_OFF_FRAME_C32H]		= Hrdw::CreateRaster( offFrameW*2, offFrameH,   32           );
			DpyManager::hwraster[DPYR_OFF_FRAME_C32V]		= Hrdw::CreateRaster( offFrameW,   offFrameH*2, 32           );
			DpyManager::hwraster[DPYR_OFF_FRAME_C16Z16H]	= Hrdw::CreateRaster( offFrameW*2, offFrameH,   16, 16, TRUE );
			DpyManager::hwraster[DPYR_OFF_FRAME_C16Z16V]	= Hrdw::CreateRaster( offFrameW,   offFrameH*2, 16, 16, TRUE );
			DpyManager::hwraster[DPYR_OFF_FRAME_C32Z32]		= Hrdw::CreateRaster( offFrameW,   offFrameH,   32, 32, TRUE );
			NV_ASSERT( DpyManager::hwraster[DPYR_OFF_FRAME_C16]		>= 0 );
			NV_ASSERT( DpyManager::hwraster[DPYR_OFF_FRAME_C32H]	>= 0 );
			NV_ASSERT( DpyManager::hwraster[DPYR_OFF_FRAME_C32V]	>= 0 );
			NV_ASSERT( DpyManager::hwraster[DPYR_OFF_FRAME_C16Z16H] >= 0 );
			NV_ASSERT( DpyManager::hwraster[DPYR_OFF_FRAME_C16Z16V] >= 0 );
			NV_ASSERT( DpyManager::hwraster[DPYR_OFF_FRAME_C32Z32]	>= 0 );
		}
		else {
			DpyManager::hwraster[DPYR_OFF_FRAME_C16]		= -1;
			DpyManager::hwraster[DPYR_OFF_FRAME_C32H]		= -1;
			DpyManager::hwraster[DPYR_OFF_FRAME_C32V]		= -1;
			DpyManager::hwraster[DPYR_OFF_FRAME_C16Z16H]	= -1;
			DpyManager::hwraster[DPYR_OFF_FRAME_C16Z16V]	= -1;
			DpyManager::hwraster[DPYR_OFF_FRAME_C32Z32]		= -1;
		}

		if( !fullscreen && targetHwnd ) {
			CreateTargetHwnd( targetHwnd );
			ActiveTargetHwnd( targetHwnd );		// set current hwraster[DPYR_IN_FRAME] ...
		}
	}

	void ResetRasterAndTarget ( ) {
		if (! DpyManager::IsFullscreen()){
			for( uint i = 0 ; i < targetHwndA.size(); i++ ) {
				HWND thwnd = targetHwndA[i].hwnd;
				if( !thwnd )			continue;
				targetHwndA[i].hwnd = NULL;
				Hrdw::FreeTarget( targetHwndA[i].frontTargetId );
				Hrdw::FreeRaster( targetHwndA[i].backRasterId );
			}
		}
		uint begin = DpyManager::IsFullscreen()? DPYR_IN_FRAME : DPYR_OFF_FRAME_C16;
		for (uint i = begin ; i <= DPYR_OFF_FRAME_C32Z32 ; ++i ) {
			if (DpyManager::hwraster[i] >= 0){
				Hrdw::FreeRaster(DpyManager::hwraster[i]);
				DpyManager::hwraster[i] = -1;
			}
		}
		DpyManager::hwraster[DPYR_IN_FRAME] = -1;
	}

	void NotifyLost () {
		RscFactory * mf = RscManager::GetFactory(NvMesh::TYPE );
		if (mf) {

		}
	}

	void NotifyReset () {

	}

	void ResetDisplay() {
		NotifyLost ();
		//ResetRasterAndTarget() ;
		bool resetOK = Hrdw::ResetDevice()  ;
		if (resetOK) {
			//InitRasterAndTarget()  ;
			NotifyReset ();
		}
	}
}


Matrix DpyManager::clipMatrix;

NvDpyContext*	DpyManager::context = NULL;
int				DpyManager::hwraster[DPYR_OFF_FRAME_C32Z32+1];




bool
DpyManager::Init	(	CoordinateSystem	inCoordinateSystem			)
{
	bool	fullscreen;
	HWND	focusHwnd, targetHwnd;
	Hrdw::RunningMode rMode;
	uint32	adapter, frontW, frontH, backW, backH, offFrameW, offFrameH;
	core::GetParameter( core::PR_DX_D3DADAPTER,			&adapter				);
	core::GetParameter( core::PR_DX_FULLSCREEN, 		&fullscreen 			);
	core::GetParameter( core::PR_W32_HWND_FOCUS, 		(uint32*)&focusHwnd		);
	core::GetParameter( core::PR_W32_HWND, 				(uint32*)&targetHwnd	);
	core::GetParameter( core::PR_FRONT_FRAME_W,			&frontW					);
	core::GetParameter( core::PR_FRONT_FRAME_H,			&frontH					);
	core::GetParameter( core::PR_BACK_FRAME_W,			&backW					);
	core::GetParameter( core::PR_BACK_FRAME_H,			&backH					);
	core::GetParameter( core::PR_OFF_FRAME_W,			&offFrameW				);
	core::GetParameter( core::PR_OFF_FRAME_H,			&offFrameH				);
	core::GetParameter(	core::PR_DX_RUNNING_MODE,		(uint32*)(&rMode)		);	

	Hrdw::RunningMode safe_rMode = Hrdw::GetBestRunningMode( adapter );
	if( rMode <= Hrdw::RM_None )
	{
		rMode = safe_rMode;
	}
	else if( rMode > safe_rMode )
	{
		DebugPrintf ( "DpyManager: Can't support this Running Mode !" );
		rMode = safe_rMode;
	}
	core::SetParameter( core::PR_DX_RUNNING_MODE, uint32(rMode) );
	DebugPrintf( "<Neova> Compatible running mode : %s\n", Hrdw::GetRunningModeName(rMode) );

	bool soft_vprocessing = (rMode <= Hrdw::RM_TNL);

	if( !focusHwnd )
		focusHwnd = targetHwnd;

	// Init DX
	if( fullscreen )
	{
		uint best_frontW, best_frontH;
		bool found = Hrdw::FindBestFullscreen( adapter, frontW, frontH, best_frontW, best_frontH );
		NV_ASSERT( found );
		if( found && (frontW!=best_frontW) || (frontH!=best_frontH) ) {
			if( (backW==frontW) && (backH==frontH) ) {
				backW = best_frontW;
				backH = best_frontH;
				core::SetParameter( core::PR_BACK_FRAME_W, backW );
				core::SetParameter( core::PR_BACK_FRAME_H, backH );
			}
			frontW = best_frontW;
			frontH = best_frontH;
			core::SetParameter( core::PR_FRONT_FRAME_W,	frontW );
			core::SetParameter( core::PR_FRONT_FRAME_H, frontH );
		}
	}

	if(	!Hrdw::Init(adapter,focusHwnd,fullscreen,frontW,frontH,soft_vprocessing))
		return FALSE;

	garbage0.Init();
	garbage1.Init();
	garbager = &garbage0;

	targetHwndA.Init();
	devPaletteA.Init();
	context = NvEngineNew( NvDpyContext );
	context->Init();

	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].Init();

	coordSystem		= inCoordinateSystem;
	frameBreakBefore= NULL;
	frameBreakAfter = NULL;
	frameNo			= 0;
	flushingFrameNo	= 0;
	frameOpened		= FALSE;
	frameSynced		= TRUE;
	presentFrame	= FALSE;

	// in proj Matrix [-1,1] to DX [0,1]
	MatrixIdentity(&clipMatrix);
	clipMatrix.m33 = 0.5f;
	clipMatrix.m43 = 0.5f;

	InitRasterAndTarget();

	return TRUE;
}



bool
DpyManager::Shut	(				)
{
	SyncFrame();

	// Flush the garbager
	while( garbager->size() ) {
		frameNo++;			// garbaging can be based on frame# -> increased !
		FlushGarbager();
	}
	garbage0.Shut();
	garbage1.Shut();
	garbager = NULL;

	targetHwndA.Shut();
	devPaletteA.Shut();
	context->Shut();
	NvEngineDelete( context );

	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].Shut();

	Hrdw::Shut();

	return TRUE;
}



DpyManager::CoordinateSystem
DpyManager::GetCoordinateSystem	(				)
{
	return coordSystem;
}


uint
DpyManager::GetPhysicalWidth	(				)
{
	if( IsFullscreen() )
	{
		return Hrdw::GetFullscreenWidth();
	}
	else
	{
		HWND targetHwnd;
		core::GetParameter( core::PR_W32_HWND, (uint32*)&targetHwnd	);
		int targetId;
		if( !GetTargetHwndInfo(targetHwnd,NULL,&targetId) )
			return 0;
		return Hrdw::GetTargetWidth( targetId );
	}
}


uint
DpyManager::GetPhysicalHeight	(				)
{
	if( IsFullscreen() )
	{
		return Hrdw::GetFullscreenHeight();
	}
	else
	{
		HWND targetHwnd;
		core::GetParameter( core::PR_W32_HWND, (uint32*)&targetHwnd	);
		int targetId;
		if( !GetTargetHwndInfo(targetHwnd,NULL,&targetId) )
			return 0;
		return Hrdw::GetTargetHeight( targetId );
	}
}


float
DpyManager::GetPhysicalAspect	(				)
{
	float w = float( int( GetPhysicalWidth()  ) );
	float h = float( int( GetPhysicalHeight() ) );
	return w / h;
}


uint
DpyManager::GetVirtualWidth		(				)
{
	return GetRasterWidth	(	DPYR_IN_FRAME );
}


uint
DpyManager::GetVirtualHeight	(				)
{
	return GetRasterHeight	(	DPYR_IN_FRAME );
}


float
DpyManager::GetVirtualAspect	(				)
{
	float w = float( int( GetVirtualWidth()  ) );
	float h = float( int( GetVirtualHeight() ) );
	return w / h;
}


uint
DpyManager::GetRasterWidth	(	DpyRaster	inRaster	)
{
	NV_ASSERT_RETURN_MTH( 0, int(inRaster) >= 0 );
	NV_ASSERT_RETURN_MTH( 0, inRaster <= DPYR_OFF_FRAME_C32Z32 );
	if( IsFullscreen() || inRaster!=DPYR_IN_FRAME )
	{
		return Hrdw::GetRasterWidth( hwraster[inRaster] );
	}
	else
	{
		HWND targetHwnd;
		core::GetParameter( core::PR_W32_HWND, (uint32*)&targetHwnd	);
		int rasterId;
		if( !GetTargetHwndInfo(targetHwnd,&rasterId,NULL) )
			return 0;
		return Hrdw::GetRasterWidth( rasterId );
	}
}


uint
DpyManager::GetRasterHeight	(	DpyRaster	inRaster	)
{
	NV_ASSERT_RETURN_MTH( 0, int(inRaster) >= 0 );
	NV_ASSERT_RETURN_MTH( 0, inRaster <= DPYR_OFF_FRAME_C32Z32 );
	if( IsFullscreen() || inRaster!=DPYR_IN_FRAME )
	{
		return Hrdw::GetRasterHeight( hwraster[inRaster] );
	}
	else
	{
		HWND targetHwnd;
		core::GetParameter( core::PR_W32_HWND, (uint32*)&targetHwnd	);
		int rasterId;
		if( !GetTargetHwndInfo(targetHwnd,&rasterId,NULL) )
			return 0;
		return Hrdw::GetRasterHeight( rasterId );
	}
}


uint
DpyManager::GetVSyncRate	(			)
{
	return Hrdw::GetVSyncRate();
}


uint
DpyManager::GetLatency		(			)
{
	return 2;
}


uint
DpyManager::GetViewingLatency	(		)
{
	return 2;
}


bool
DpyManager::TranslateCRTC		(	int		inX0,
									int		inY0	)
{
	return FALSE;
}


bool
DpyManager::ReadFrame	(	pvoid	outBuffer	)
{
	int rasterId;
	if( IsFullscreen() )
	{
		rasterId = hwraster[ DPYR_IN_FRAME ];
	}
	else
	{
		HWND targetHwnd;
		core::GetParameter( core::PR_W32_HWND, (uint32*)&targetHwnd	);
		if( !GetTargetHwndInfo(targetHwnd,&rasterId,NULL) )
			return FALSE;
	}	
	return Hrdw::ReadRaster( outBuffer, rasterId );
}


bool
DpyManager::WriteFrame	(	pvoid	inBuffer	)
{
	int rasterId;
	if( IsFullscreen() )
	{
		rasterId = hwraster[ DPYR_IN_FRAME ];
	}
	else
	{
		HWND targetHwnd;
		core::GetParameter( core::PR_W32_HWND, (uint32*)&targetHwnd	);
		if( !GetTargetHwndInfo(targetHwnd,&rasterId,NULL) )
			return FALSE;
	}
	return Hrdw::WriteRaster( inBuffer, rasterId );
}



bool
DpyManager::SaveFrame	(	pcstr		inFilename		)
{
#ifdef _MASTER
	return FALSE;
#else
	if( !inFilename || (*inFilename)==0 )
		return FALSE;

	uint32 backW = GetRasterWidth( DPYR_IN_FRAME );
	uint32 backH = GetRasterHeight( DPYR_IN_FRAME );
	uint   bsize = backW * backH * 4;

	pvoid  mem = EngineMalloc( 256+bsize );
	uint8* bmp = ((uint8*)mem) + 256;
	uint8* hdr = bmp - 18;

	if( !ReadFrame(bmp) ) {
		EngineFree( mem );
		return FALSE;
	}

	Zero( hdr, 18 );
	hdr[2] =2;			// Image type code
	hdr[12]=backW;		// low width of image
	hdr[13]=backW>>8;	// high width of image
	hdr[14]=backH;		// low height of image
	hdr[15]=backH>>8;	// high height of image
	hdr[16]=32;			// Image pixel size
	hdr[17]=32;			// Image descriptor byte

	bool res = nv::file::DumpToFile( inFilename, hdr, 18+bsize );
	EngineFree( mem );
	return res;
#endif
}


Psm
DpyManager::GetFramePSM	(		)
{
	return PSM_ARGB32;
}


uint
DpyManager::GetFrameNo		(		)
{
	return frameNo;
}


bool
DpyManager::BeginFrame	(		)
{
	NV_ASSERTC( !frameOpened, "Frame is already opened !" );
	if( frameOpened )
		return FALSE;

	if( !IsFullscreen() )
	{
		HWND targetHwnd     = 0;
		bool enableResizing = FALSE;
		core::GetParameter( core::PR_W32_HWND,           (uint32*)&targetHwnd );
		core::GetParameter( core::PR_DX_ENABLE_RESIZING, &enableResizing      );
		CheckTargetHwnd();
		if( enableResizing )
			ResizeTargetHwnd( targetHwnd );
		if( !ActiveTargetHwnd(targetHwnd) )
			return FALSE;
	}

	// session context
	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].clear();
	ctxtCurSession = 0;

	// reset context stacks
	context->Reset();
	Vec2 clipRange( 1.0f, 500.0f );
	Vec4 viewport( 0, 0, GetRasterWidth(DPYR_IN_FRAME), GetRasterHeight(DPYR_IN_FRAME) );
	context->SetView( NULL, NULL, &clipRange, &viewport );
	context->SetTarget( DpyTarget(DPYR_IN_FRAME) );

	// inc frameNo
	frameNo++;
	
	frameOpened = TRUE;

	// Flush garbager
	FlushGarbager();

	return TRUE;
}


bool
DpyManager::EndFrame	(		)
{
	NV_ASSERTC( frameOpened, "Frame is not opened !" );
	if( !frameOpened )
		return FALSE;
	frameOpened = FALSE;
	return TRUE;
}


bool
DpyManager::FlushFrame	(	bool	inPresent,
							bool	inGetPerf		)
{
	NV_ASSERTC( !frameOpened, "Frame must not be opened !" );
	NV_ASSERTC( flushingFrameNo != frameNo, "Frame has been already flushed !" );
	if( frameOpened || flushingFrameNo==frameNo )
		return FALSE;

	bool endOk = FALSE;
	Hrdw::DeviceState devS = Hrdw::GetDeviceState();
	if (devS == Hrdw::NV_DEVICE_OK){
		// Begin scene
		Hrdw::BeginScene();
		Hrdw::ResetStates();
		Hrdw::SetRaster( hwraster[DPYR_IN_FRAME] );

		if( presentFrame ) {
			Hrdw::CopyBackToFront();
			Hrdw::ResetStates();
			Hrdw::SetRaster( hwraster[DPYR_IN_FRAME] );
		}
		presentFrame = inPresent;

		// Draw
		for( uint s = 0 ; s < SESSION_NB ; s++ ) {
			uint N = sessionA[s].size();
			if( N == 0 )	continue;
			NvDpyObject_PC	** dpyObjP		= sessionA[s].data();
			NvDpyObject_PC	** dpyObj_endP	= dpyObjP + N;
			while( dpyObjP != dpyObj_endP ) {
				NV_ASSERT( *dpyObjP );
				if( *dpyObjP == frameBreakBefore )	goto dpy_frame_break;
				(*dpyObjP)->Draw();
				if( *dpyObjP == frameBreakAfter )	goto dpy_frame_break;
				dpyObjP++;
			}
		}
dpy_frame_break:
		// End
		endOk = Hrdw::EndScene();
	}

	if( inGetPerf ) {
		perf_drawCpt	= 0;
		for( uint s = 0 ; s < SESSION_NB ; s++ ) {
			uint N = sessionA[s].size();
			if( N == 0 )	continue;
			NvDpyObject_PC	** dpyObjP		= sessionA[s].data();
			NvDpyObject_PC	** dpyObj_endP	= dpyObjP + N;
			while( dpyObjP != dpyObj_endP ) {
				NV_ASSERT( *dpyObjP );
				perf_drawCpt += (*dpyObjP)->dpyctxt.chainCpt;
				dpyObjP++;
			}
		}
	}

	frameSynced		= FALSE;
	flushingFrameNo	= frameNo;
	return endOk;
}


void
DpyManager::SyncFrame	(		)
{
	NV_ASSERTC( !frameOpened, "Frame must not be opened !" );
	if( frameOpened )	return;
	
	Hrdw::DeviceState devS = Hrdw::GetDeviceState();

	if ( !frameSynced && (devS == Hrdw::NV_DEVICE_OK) ){
		Hrdw::BeginScene();
		Hrdw::EndScene();
	}
	frameSynced = TRUE;
	FlushGarbager();
}


bool
DpyManager::IsInFrame	(	NvDpyObject*		inDpyObj	)
{
	if( !inDpyObj )
		return FALSE;

	NvDpyObject_PC* dpyObj = (NvDpyObject_PC*) inDpyObj->GetBase();
	NV_ASSERT( dpyObj );
	return ( dpyObj->dpyctxt.drawFrameNo == frameNo );
}


bool
DpyManager::IsDrawing	(	NvDpyObject*		inDpyObj	)
{
	if( !inDpyObj )
		return FALSE;
	NvDpyObject_PC* dpyObj = (NvDpyObject_PC*) inDpyObj->GetBase();
	NV_ASSERT( dpyObj );
	return IsDrawing( dpyObj->dpyctxt.drawFrameNo );
}

bool
DpyManager::IsDrawing	(	uint32				inFrameNo	)
{
	// my frame is the current frame and is flushed ?
	if( inFrameNo==flushingFrameNo && inFrameNo==frameNo )
		return !frameSynced;
	else
		return ( inFrameNo==flushingFrameNo || inFrameNo==frameNo );
}

void
DpyManager::SetFrameBreakBefore	(	NvDpyObject*		inDpyObj	)
{
	frameBreakAfter  = NULL;
	frameBreakBefore = inDpyObj ? (NvDpyObject_PC*)inDpyObj->GetBase() : NULL;
}


void
DpyManager::SetFrameBreakAfter	(	NvDpyObject*		inDpyObj	)
{
	frameBreakBefore = NULL;
	frameBreakAfter	 = inDpyObj ? (NvDpyObject_PC*)inDpyObj->GetBase() : NULL;
}


void
DpyManager::SetTarget	(	DpyTarget		inTarget	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return ;
	context->SetTarget(inTarget);
}


void
DpyManager::SetView		(	Matrix*			inViewTR,
							Matrix*			inProjTR,
							Vec2*			inClipRange,
							Vec4*			inViewport	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return ;
	context->SetView(inViewTR,inProjTR,inClipRange,inViewport);
}


void
DpyManager::SetSession	(	uint8			inSessionNo		)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return ;
	ctxtCurSession = (inSessionNo < SESSION_NB) ? inSessionNo : SESSION_NB-1;
}


void
DpyManager::SetWorldTR	(	Matrix*		inWorldTR	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return ;
	context->SetWorldTR(inWorldTR);
}


void
DpyManager::SetLight	(	uint		inLightNo,
							Vec4*		inColor,
							Vec3*		inDirection		)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return ;
	context->SetLight(inLightNo,inColor,inDirection);
}


bool
DpyManager::Draw		(	NvDpyObject*	inDpyObj	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return FALSE;

	if( !inDpyObj )
		return FALSE;

	NvDpyObject_PC* dpyObj = (NvDpyObject_PC*) inDpyObj->GetBase();
	NV_ASSERT( dpyObj );
	bool firstDraw = ( dpyObj->dpyctxt.drawFrameNo != frameNo );

	int chainNext = (firstDraw ? -1 : dpyObj->dpyctxt.chainHead );
	int chainHead = context->Push( chainNext );
	if( chainHead < 0 )
		return FALSE;

	dpyObj->dpyctxt.chainHead = chainHead;
	dpyObj->dpyctxt.chainCpt += 1;

	// Not already in the current draw queue ?
	if( firstDraw ) {
		NV_ASSERT( ctxtCurSession < SESSION_NB );
		sessionA[ ctxtCurSession ].push_back( dpyObj );
		dpyObj->dpyctxt.drawFrameNo = frameNo;
		dpyObj->dpyctxt.chainCpt = 1;
	}

	return TRUE;
}



bool
DpyManager::IsFullscreen		(		)
{
	return Hrdw::IsFullscreen();
}



int
DpyManager::CreateTargetHwnd	(	HWND		inHwnd		)
{
	if( IsFullscreen() )
		return -1;

	if( !IsWindow(inHwnd) )
		return -1;

	int foundFree = -1;
	for( uint i = 0 ; i < targetHwndA.size() ; i++ ) {
		if( targetHwndA[i].hwnd == inHwnd )
			return int(i);
		if( !targetHwndA[i].hwnd )
			foundFree = int(i);
	}

	RECT r;
	GetClientRect( inHwnd, &r );
	int hwnd_width  = int(r.right);
	int hwnd_height = int(r.bottom);
	if( hwnd_width<1 || hwnd_height<1 )
		return -1;

	TargetHwnd thwnd;
	thwnd.hwnd			= inHwnd;
	thwnd.frontTargetId	= Hrdw::CreateTarget( inHwnd, hwnd_width, hwnd_height );

	bool enableResizing = FALSE;
	uint32 backW,backH;
	core::GetParameter( core::PR_DX_ENABLE_RESIZING, &enableResizing			);
	core::GetParameter( core::PR_BACK_FRAME_W,			&backW					);
	core::GetParameter( core::PR_BACK_FRAME_H,			&backH					);

	if (enableResizing)
		thwnd.backRasterId	= Hrdw::CreateRaster( hwnd_width, hwnd_height, 32, 32, TRUE );
	else{
		thwnd.backRasterId	= Hrdw::CreateRaster( backW, backH, 32, 32, TRUE );
	}

	if( thwnd.frontTargetId<0 || thwnd.backRasterId<0 ) {
		Hrdw::FreeTarget( thwnd.frontTargetId );
		Hrdw::FreeRaster( thwnd.backRasterId );
	}

	if( foundFree >= 0 ) {
		targetHwndA[ foundFree ] = thwnd;
		return foundFree;
	}
	else {
		targetHwndA.push_back( thwnd );
		return int( targetHwndA.size() - 1 );
	}
}


void
DpyManager::CheckTargetHwnd		(							)
{
	if( IsFullscreen() )
		return;

	for( uint i = 0 ; i < targetHwndA.size(); i++ ) {
		HWND thwnd = targetHwndA[i].hwnd;
		if( !thwnd )			continue;
		if( IsWindow(thwnd) )	continue;
		targetHwndA[i].hwnd = NULL;
		Hrdw::FreeTarget( targetHwndA[i].frontTargetId );
		Hrdw::FreeRaster( targetHwndA[i].backRasterId );
	}
}


bool
DpyManager::ActiveTargetHwnd	(	HWND		inHwnd		)
{
	if( IsFullscreen() )
		return FALSE;

	int tidx = CreateTargetHwnd( inHwnd );

	if( tidx < 0 ) {
		CheckTargetHwnd();
		return FALSE;
	}

	hwraster[ DPYR_IN_FRAME ] = targetHwndA[ tidx ].backRasterId;
	Hrdw::SetRaster( targetHwndA[ tidx ].backRasterId );
	Hrdw::SetTarget( targetHwndA[ tidx ].frontTargetId );

	return TRUE;
}


bool
DpyManager::ResizeTargetHwnd	(	HWND		inHwnd		)
{
	if( IsFullscreen() )
		return FALSE;

	int tidx = CreateTargetHwnd( inHwnd );

	if( tidx < 0 ) {
		CheckTargetHwnd();
		return FALSE;
	}

	RECT r;
	GetClientRect( inHwnd, &r );
	int hwnd_width  = int(r.right);
	int hwnd_height = int(r.bottom);
	if( hwnd_width<1 || hwnd_height<1 )
		return FALSE;

	int rasterId = targetHwndA[tidx].backRasterId;
	int targetId = targetHwndA[tidx].frontTargetId;

	if( !Hrdw::ResizeRaster(rasterId,hwnd_width,hwnd_height) )
		return FALSE;

	if( !Hrdw::ResizeTarget(targetId,hwnd_width,hwnd_height) )
		return FALSE;

	return TRUE;
}


bool
DpyManager::GetTargetHwndInfo	(	HWND		inHwnd,
									int*		outHwRasterId,
									int*		outHwTargetId	)
{
	if( IsFullscreen() )
		return FALSE;

	if( !inHwnd )
		return FALSE;

	for( uint i = 0 ; i < targetHwndA.size(); i++ ) {
		if( targetHwndA[i].hwnd != inHwnd )
			continue;
		if( outHwRasterId )		*outHwRasterId = targetHwndA[i].backRasterId;
		if( outHwTargetId )		*outHwTargetId = targetHwndA[i].frontTargetId;
		return TRUE;
	}

	return FALSE;
}



uint
DpyManager::AllocDevPalette		(		)
{
	uint i = 0;
	for( ; i < devPaletteA.size() ; i++ ) {
		if( devPaletteA[i] != ~0U ) {
			uint idx = GetLowestBitNo( devPaletteA[i] ^ (~0U) );
			devPaletteA[i] |= 1<<idx;
			return idx+(i<<5);
		}
	}
	devPaletteA.push_back( 1 );
	return i<<5;
}


void
DpyManager::ReleaseDevPalette	(	uint	inIdx	)
{
	uint idx32 = inIdx>>5;
	uint idx0  = inIdx-idx32;
	if( idx32 < devPaletteA.size() )
		devPaletteA[ idx32 ] &= ~(1<<idx0);
}


void
DpyManager::AddToGarbager( NvInterface*		inITF )
{
	NV_ASSERT( garbager );
	if( !inITF )
		return;

#ifdef _NVCOMP_ENABLE_DBG
	if( garbager->size() ) {
		NvInterface** itf	  = garbager->data();
		NvInterface** itf_end = itf + garbager->size();
		while( itf != itf_end ) {
			NV_ASSERT( *itf );
			NV_ASSERTC( (*itf)!=inITF, "Interface 0x%08x is already garbaging !" );
			itf++;
		}
	}
#endif

	garbager->push_back( inITF );
}


void
DpyManager::FlushGarbager	(	)
{
	NV_ASSERT( garbager );

	// Swap garbager before to allow Release() to call AddToGarbager() again !
	sysvector<NvInterface*>* _garbager = (garbager==&garbage0) ? &garbage1 : &garbage0;
	_garbager->clear();
	Swap( _garbager, garbager );

	// Release garbaging objets
	if( _garbager->size() ) {
		NvInterface** itf	  = _garbager->data();
		NvInterface** itf_end = itf + _garbager->size();
		while( itf != itf_end ) {
			NV_ASSERT( *itf );
			(*itf)->Release();		// Note that Release() can call AddToGarbager() again !
			itf++;
		}
	}
}

float
DpyManager::GetPerformance		(	Performance			inPerf					)
{
	if( inPerf == PERF_DRAW_CPT )	return float(int(perf_drawCpt));
	return 0.f;
}

void
DpyManager::DrawImmediate	(	void* 				inTex					,		
														uint				inWidth					,
														uint				inHeight				,
														int					inOffsetX				,		
														int					inOffsetY				)
{
	
}

