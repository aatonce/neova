/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <Kernel/PC/NvSndManager[PC].h>
#include <NvCore.h>
#include <NvSound.h>
#include <Kernel/PC/NvkSound[PC].h>

using namespace nv;

#include <dsound.h>

NV_DSOUND		SndManager::sdev = NULL;
NV_DSCAPS		SndManager::sdevCaps;

#define MAX_SEGMENT		8

#define BLOCK_TO_BSIZE(nbBlock) ( (nbBlock) * 16 )
#define SAMPLE_TO_BLOCK(nbSample) ( ((nbSample)/28) + ((nbSample%28)?1:0) )
#define PCMBSIZE_TO_SAMPLE(bsize) (bsize/2)
#define PCMBSIZE_TO_ADPCMBSSIZE(bsize) BLOCK_TO_BSIZE(SAMPLE_TO_BLOCK(PCMBSIZE_TO_SAMPLE(bsize)))
#define ADPCMBSSIZE_TO_PCMBSIZE(bsize) (((bsize)/16)*28*2) // 16 bits/sample

namespace
{
	typedef		 uint8	SoundID;
	static const uint8	InvalidSoundID = 255;

	enum
	{
		NB_SOUND_MAX		= 128,
		NB_CHANNEL			= 48,
		BGM_VOICE			= 48,
	};

	enum SdStateInternal
	{
		SDI_UNALLOCATED	= 0,
		SDI_ALLOCATED	= 1,	
		SDI_TORELEASE	= 4,			
	};

	struct SFX
	{
		bool					loaded	;
		bool					loading	;
		nv::file::ReadRequest	rr		;
		NV_DSSOUNDBUFFER		sbuffer	;
	};

	struct Stream
	{
		uint32					str_buffer;			// stream reading buffer
		uint32					str_bsize;			// stream buffer bsize
		uint32					str_avl_bsize;		// available bsize in buffer
		uint32					str_cur_boffset;	// offset courrant (0 au debut)
		uint32					str_cur_bsize;		// taille qui reste a charger (tout le fichier au debut)
		uint32					str_wbpos;			// write pos in sbuffer
		nv::file::ReadRequest	str_rr;				
		uint32					str_loading_bsize;	// loading bsize in buffer (en train de charger)
		uint32					str_end_bsize;
	};

	struct SoundChain
	{
		bool					looped		;
		bool					streamed	;
		bool					music		;
		SndManager::SdState		state		;
		SdStateInternal			stateI		;
		
		DSBUFFERDESC			sdesc		;
		PCMWAVEFORMAT			pcmwf		;

		uint32					freq		;
		uint32					bOffset		;
		uint32					bSize		;
		float					duration	;

		SoundID					nextFree	;
		SoundID					prevFree	;
		uint64					usedVoice	;

		SFX						sfx	;
		Stream					strm;
	};

	bool DefaultStreamCallBack (	int					inCh,						// channel id or -1 for bgm
									float&				outFromTime,				// next segment start time
									float&				outDuration				)
	{
		return FALSE;
	}

	SoundChain				soundA		[NB_SOUND_MAX]	;
	SoundID					firstFree	;

	struct Voice {
		int					sndOwnerId;
		NV_DSSOUNDBUFFER	sbuffer;
		float				vol;
		float				panAngle;
		float				pitchFactor;
		bool				callCallback;
	};

	float					masterVol;
	SndManager::OutputMode	omode;
	
	SndManager::StreamHandler streamLoopCallBack = &DefaultStreamCallBack;

	uint64					voiceBusyMask;
	uint64					voicePauseMask;
	Voice					voice[NB_CHANNEL+1];	
	bool					haveAMusicLocked	= FALSE;


	/*********************** VAG ***********************************/
	
	static int f[5][2] = {	{ 0,   0 },
	   						{ 60,  0 },
	   						{ 115, -52 },
	   						{ 98,  -55 },
	   						{ 122, -60 } };

	static void DepackVag( int16 *pcm, void *vag, int size, bool inStereo)
	{
		static int s_1 = 0;
		static int s_2 = 0;

		static int s_1L = 0;
		static int s_2L = 0;
		static int s_1R = 0;
		static int s_2R = 0;

		unsigned char	*p;
		short			*q,*startPCM;
		int				predict_nr, shift_factor;
		int				d, s, fa, i, b;	// b => block
		unsigned int	inc;


		b		 = 0 ;
		p		 = (unsigned char *)vag;
		q		 = (int16         *)pcm;
		startPCM = (int16         *)pcm;
		inc		 = (unsigned int (inStereo)) + 1;
		
		NV_ASSERT( (inStereo && ((size%56) == 0)) || !inStereo ) ;
		NV_ASSERT( (size&1) == 0);
		while(size) {
			if (inStereo) {
				if (b&0x1) {
					s_1R = s_1;
					s_2R = s_2;
					s_1  = s_1L;
					s_2  = s_2L;
					q = startPCM +1;
				}
				else { 
					startPCM = ((int16*)pcm) + (b * 28);
					q		 = startPCM;

					s_1L = s_1;
					s_2L = s_2;
					s_1  = s_1R;
					s_2  = s_2R;
				}
			}

			predict_nr = *p++;
			shift_factor = predict_nr & 0xf;
			predict_nr >>= 4;
			p++;
			int loopEnd = Min(28,size);
 
			for (i = 0; i < loopEnd; i += 2) {
				d = *p++;
				s = (d & 0xf) << 12;
				if (s & 0x8000) s |= 0xffff0000;
				fa = s >> shift_factor;
				fa+= ((s_1 * f[predict_nr][0]) >> 6) + 
					 ((s_2 * f[predict_nr][1]) >> 6);
				s_2 = s_1; s_1 = fa;
				(*q) = fa;
				q+=inc;

				s = (d & 0xf0) << 8;
				if (s & 0x8000) s |= 0xffff0000;
				fa = s >> shift_factor;
				fa+= ((s_1 * f[predict_nr][0]) >> 6) + 
					 ((s_2 * f[predict_nr][1]) >> 6);
				s_2 = s_1; s_1 = fa;
				(*q) = fa;
				q+=inc;
			}
			size -= loopEnd;
			b++	;
			NV_ASSERT(size>=0);
		}
	}

	/***************************************************************/

	bool ApplyVolPan (		NV_DSSOUNDBUFFER	inBuffer,
							float				inMasterVolume,
							float				inVolume,
							float				inPanAngle) {
		
		if (! inBuffer ) return FALSE;

		float vol  = Clamp( inMasterVolume, 0.0f, 1.0f ) * Clamp( inVolume, 0.0f, 1.0f );

		HRESULT hr;

		// 3d sound ?
		LPDIRECTSOUND3DBUFFER s3dbuffer;
		hr = inBuffer->QueryInterface( IID_IDirectSound3DBuffer, (LPVOID*)&s3dbuffer );
		if( hr == S_OK ) {
			float y = Cosf( inPanAngle );
			float x = Sinf( inPanAngle );
			hr = s3dbuffer->SetPosition( x, 0.0f,y, DS3D_IMMEDIATE );
			NV_ASSERT( hr == DS_OK );
			SafeRelease( s3dbuffer );
		}

		// stereo panning
		else {
			float x = Sinf( inPanAngle );
			LONG pan;
			if( x > 0.0f ) {
				pan  = x * float(DSBPAN_RIGHT);
				vol *= 1.0f - x;
			}
			else if( x < 0.0f ) {
				pan  = (-x) * float(DSBPAN_LEFT);
				vol *= 1.0f + x;
			}
			else {
				pan = 0;
			}

			hr = inBuffer->SetPan( pan );
		}

		// linear -> db
		LONG lvol = SndManager::LinearToMbVolume( vol );
		hr = inBuffer->SetVolume( lvol );
		NV_ASSERT( hr == DS_OK );
		return hr == DS_OK;
	}


	uint _GetStreamingNeedBSize	(	NV_DSSOUNDBUFFER	inSBuffer,
									uint32				inWriteBPos,
									uint32				inBufferBSize	)
	{
		HRESULT hr;
		DWORD	playC;
		DWORD	writeC;
		hr = inSBuffer->GetCurrentPosition( &playC, &writeC );
		if( hr != DS_OK )
			return 0;

		NV_ASSERT( (playC&1) == 0 );		// Multiple de 2b
		NV_ASSERT( (writeC&1) == 0 );		// Multiple de 2b

		if( playC > writeC )
		{
			// [xxxxW.......Pxxxx]
			if( (inWriteBPos<writeC) || (inWriteBPos>=playC) )
				return 0;	// Sync Failed !
			else
				return ( playC - inWriteBPos );
		}
		else
		{
			// [....PxxxxxxxW....]
			if( inWriteBPos < playC )
				return ( playC - inWriteBPos );
			else if( inWriteBPos >= writeC )
				return ( playC + inBufferBSize-inWriteBPos );
			else
				return 0;	// Sync Failed !
		}
	}


	void _FillStreaming	(	NV_DSSOUNDBUFFER	inSBuffer		,
							uint32&				ioWriteBPos		,	// PCM 
							uint32				inBufferBSize	,	// ADPCM
							uint8*				inFillPtr		,	// ptr opn ADPCM data
							uint				inFillBSize		,	// ADPCM available data size
							bool				inStereo		)	
	{
		HRESULT hr;
		PVOID	pwrite0, pwrite1;
		DWORD	lwrite0, lwrite1;

		uint pcmFillBSize = ADPCMBSSIZE_TO_PCMBSIZE(inFillBSize);

		hr = inSBuffer->Lock(	ioWriteBPos, pcmFillBSize,
								&pwrite0, &lwrite0,
								&pwrite1, &lwrite1, 0 );
		NV_ASSERT( hr == DS_OK );

		if( lwrite0 == pcmFillBSize )
			lwrite1 = 0;
		if (inFillPtr) {
			if( lwrite0 )DepackVag( (int16 *)pwrite0, inFillPtr, PCMBSIZE_TO_SAMPLE(lwrite0),inStereo);
			uint adpcmOffset = PCMBSIZE_TO_ADPCMBSSIZE(lwrite0);
			NV_ASSERT((adpcmOffset % 16) == 0);
			if( lwrite1 )DepackVag( (int16 *)pwrite1, inFillPtr + adpcmOffset, PCMBSIZE_TO_SAMPLE(lwrite1),inStereo);
		}
		else {
			if (lwrite0)Memset( pwrite0, 0, lwrite0 );
			if (lwrite1)Memset( pwrite1, 0, lwrite1 );
		}
		NV_ASSERT( (lwrite0&1) == 0 );		// Multiple de 2b
		NV_ASSERT( (lwrite1&1) == 0 );		// Multiple de 2b
		ioWriteBPos += lwrite0 + lwrite1;
		ioWriteBPos %= ADPCMBSSIZE_TO_PCMBSIZE(inBufferBSize);
		inSBuffer->Unlock( pwrite0, lwrite0, pwrite1, lwrite1 );
	}

	bool StreamSetSegment	(	int					inSoundId		,
								float				inFromTime		,
								float				inToTime		,
								bool				inLoadFullBuffer = FALSE)
	{
		int soundId = inSoundId;
		uint32	bOffset = soundA[soundId].bOffset	,
				bSize	= soundA[soundId].bSize		;
		uint	freq	= soundA[soundId].freq		;
		uint	nbChan	= soundA[soundId].music?2:1	;

		uint32	beginSample			= uint(inFromTime * freq);
		uint32	endSample			= uint(inToTime * freq);
		uint	nbBlockBeginOffset	= SAMPLE_TO_BLOCK(beginSample) * nbChan;
		uint	nbBlockEndOffset	= SAMPLE_TO_BLOCK(endSample  ) * nbChan;

		uint32 beginOffset = BLOCK_TO_BSIZE(nbBlockBeginOffset) + bOffset; 
		uint32 endOffset   = BLOCK_TO_BSIZE(nbBlockEndOffset)   + bOffset;
		if  (( (bOffset&1) && !(beginOffset&1) ) || 
			( !(bOffset&1) && (beginOffset&1)) ){
			//beginOffset+=1;
			NV_ASSERT(FALSE);
		}

		if  (( (bOffset&1) && !(endOffset&1) ) || 
			( !(bOffset&1) && (endOffset&1)) ){
			//endOffset+=1;
			NV_ASSERT(FALSE);
		}
		
		if ((beginOffset >= bOffset+bSize) || (endOffset > bOffset+bSize) || (beginOffset >= endOffset))
			return FALSE;
		NV_ASSERT( ((endOffset - beginOffset) %16) == 0);

		soundA[soundId].strm.str_cur_bsize		= endOffset - beginOffset;
		soundA[soundId].strm.str_avl_bsize		= 0;
		soundA[soundId].strm.str_loading_bsize	= Min( soundA[soundId].strm.str_bsize/(inLoadFullBuffer?1U:2U), soundA[soundId].strm.str_cur_bsize );		
		soundA[soundId].strm.str_cur_boffset	= beginOffset;
		soundA[soundId].strm.str_rr.bSize		= Round16( soundA[soundId].strm.str_loading_bsize );
		soundA[soundId].strm.str_rr.bufferPtr	= (void*) soundA[soundId].strm.str_buffer;
		soundA[soundId].strm.str_rr.bOffset[0]	= soundA[soundId].strm.str_cur_boffset;
		soundA[soundId].strm.str_rr.bOffset[1]	= soundA[soundId].strm.str_cur_boffset;
		soundA[soundId].strm.str_rr.bOffset[2]	= soundA[soundId].strm.str_cur_boffset;
		soundA[soundId].strm.str_rr.bOffset[3]	= soundA[soundId].strm.str_cur_boffset;
		file::AddReadRequest( &soundA[soundId].strm.str_rr, 8 );
		soundA[soundId].strm.str_cur_boffset	+= soundA[soundId].strm.str_loading_bsize;
		soundA[soundId].strm.str_cur_bsize		-= soundA[soundId].strm.str_loading_bsize;

		return TRUE;
	}

	bool SFX_IsLoading		(	SFX	* inS 	)
	{
		return inS && inS->loading;
	}

	bool SFX_IsLoaded		(	SFX	* inS 	)
	{
		return inS && inS->loaded;
	}

	bool SFX_Duplicate ( LPDIRECTSOUNDBUFFER * outB, DSBUFFERDESC & inSDesc, LPDIRECTSOUNDBUFFER * inB )
	{
		//hr = SndManager::sdev->DuplicateSoundBuffer( soundA[inSoundId].sfx.sbuffer, &voice[channelId].sbuffer );
		HRESULT hr;
		hr = SndManager::sdev->CreateSoundBuffer( &inSDesc, outB , NULL );
		if ( hr != DS_OK )
			return FALSE;

		pvoid	ptr1;
		DWORD	ptr_len1;
		hr = (*inB)->Lock( 0, 0, &ptr1, &ptr_len1, NULL, NULL, DSBLOCK_ENTIREBUFFER );
		if( hr!=DS_OK ) {
			SafeRelease( *outB );
			return FALSE;
		}
		
		pvoid	ptr2;
		DWORD	ptr_len2;
		hr = (*outB)->Lock( 0, 0, &ptr2, &ptr_len2, NULL, NULL, DSBLOCK_ENTIREBUFFER );
		if( hr!=DS_OK ) {
			hr = (*inB)->Unlock( ptr1, ptr_len1, NULL, 0 );
			SafeRelease( *outB );
			return FALSE;
		}
		NV_ASSERT(ptr_len2 == ptr_len1);

		Memcpy( ptr2, ptr1, Min(ptr_len1,ptr_len2) );
		hr = (*inB)->Unlock( ptr1, ptr_len1, NULL, 0 );
		NV_ASSERT( hr == DS_OK );
		hr = (*outB)->Unlock( ptr2, ptr_len2, NULL, 0 );
		NV_ASSERT( hr == DS_OK );
		return TRUE;
	}

	bool SFX_Load		(	SFX	* inS , DSBUFFERDESC & inSDesc , uint inDataBOffset )
	{
		if (!inS )
			return FALSE;
		if( SFX_IsLoaded(inS) || SFX_IsLoading(inS) )
			return TRUE;

		// Create buffer
		HRESULT hr;
 		hr = SndManager::sdev->CreateSoundBuffer( &inSDesc, &(inS->sbuffer), NULL );
		if( hr!=DS_OK ) {
			SndManager::OutputDSError( hr );
			return FALSE;
		}
		// Get Vag Size :
		if (inSDesc.dwBufferBytes&0x1) return FALSE;

		uint nbByte	= BLOCK_TO_BSIZE( SAMPLE_TO_BLOCK( PCMBSIZE_TO_SAMPLE(inSDesc.dwBufferBytes)));

		// Async read
		inS->rr.bSize		= Round16( nbByte );
		inS->rr.bufferPtr	= nv::mem::Alloc( inS->rr.bSize, mem::AF_PART_RSC | mem::AF_DATA_SOUND );
		inS->rr.bOffset[0]	= inDataBOffset;
		inS->rr.bOffset[1]	= inDataBOffset;
		inS->rr.bOffset[2]	= inDataBOffset;
		inS->rr.bOffset[3]	= inDataBOffset;
		if( !file::AddReadRequest(&inS->rr,8) ) {
			SafeRelease( inS->sbuffer );
			return FALSE;
		}

		inS->loading = TRUE;
		return TRUE;
	}

	void SFX_LoadUpdate		( SFX	* inS )
	{
		if( !inS || !SFX_IsLoading(inS) || !inS->rr.IsReady() )
			return;

		if( inS->rr.GetState() != file::ReadRequest::COMPLETED ) {
			nv::mem::Free( inS->rr.bufferPtr );
			SafeRelease( inS->sbuffer );
			inS->loading = FALSE;
			return;
		}

		HRESULT hr;

		// Lock buffer
		pvoid	ptr;
		DWORD	ptr_len;
		hr = inS->sbuffer->Lock( 0, 0, &ptr, &ptr_len, NULL, NULL, DSBLOCK_ENTIREBUFFER );
		if( hr!=DS_OK ) {
			nv::mem::Free( inS->rr.bufferPtr );
			inS->rr.bufferPtr = NULL;
			SafeRelease( inS->sbuffer );
			inS->loading = FALSE;
			return;
		}

		uint nbSample	= (ptr_len/2);
		DepackVag( (int16*)ptr, inS->rr.bufferPtr, nbSample,FALSE);
		hr = inS->sbuffer->Unlock( ptr, ptr_len, NULL, 0 );
		NV_ASSERT( hr == DS_OK );

		nv::mem::Free( inS->rr.bufferPtr );
		inS->loading = FALSE;
		inS->loaded  = TRUE;
	}

	bool SFX_Release(SFX	* inS)
	{
		if ( !inS )					return FALSE;
		if ( !SFX_IsLoaded(inS) && 
			 !SFX_IsLoading(inS) )	return FALSE;
		
		if (!inS->rr.IsReady()) {
			file::AbortReadRequest(&inS->rr);
		}

		inS->loaded		= FALSE;
		inS->loading	= FALSE;
		SafeRelease(inS->sbuffer);
		return TRUE;
	}

	bool CallCallBack ( int inSndId , SoundChain & sc , Voice & vc)
	{
		vc.callCallback = FALSE;
		float	begin	= -1.0f,
				duration= -1.0f;

		bool cnt = (*streamLoopCallBack)( inSndId , begin , duration );
		if( cnt )
		{
			if( begin < 0.0f )		begin = 0.0F;
			if( duration <= 0.0f )	duration = sc.duration - begin;
			float end = begin + duration;
			return StreamSetSegment ( inSndId , begin, end, FALSE );
		}
		else
		{
			return FALSE;
		}
	}

	void InitSDesc ( SoundChain & ioSC , NvkSound * inKs )
	{
		uint32 dsflags = 0;
		if ( ! nv::core::GetParameter(nv::core::PR_DS_BUFFERDESC_FLAGS,&dsflags) )
			dsflags = DSBCAPS_CTRL3D | DSBCAPS_CTRLFREQUENCY | DSBCAPS_CTRLVOLUME | DSBCAPS_GETCURRENTPOSITION2	| DSBCAPS_LOCDEFER;

		memset( &ioSC.pcmwf, 0, sizeof(PCMWAVEFORMAT) );
		ioSC.pcmwf.wf.wFormatTag		= WAVE_FORMAT_PCM;
		ioSC.pcmwf.wf.nChannels			= inKs->IsMusic()?2:1;
		ioSC.pcmwf.wf.nSamplesPerSec	= inKs->GetFreq();
		ioSC.pcmwf.wf.nBlockAlign		= 2 * ioSC.pcmwf.wf.nChannels;
		ioSC.pcmwf.wf.nAvgBytesPerSec	= inKs->GetFreq() * 2 * ioSC.pcmwf.wf.nChannels;
		ioSC.pcmwf.wBitsPerSample		= 16;

		memset( &ioSC.sdesc, 0, sizeof(NV_DSBUFFERDESC) );
		ioSC.sdesc.dwSize			= sizeof(NV_DSBUFFERDESC);
		ioSC.sdesc.dwFlags			= dsflags;
 		ioSC.sdesc.dwBufferBytes	= inKs->GetSampleLength() * 2 * ioSC.pcmwf.wf.nChannels;
		ioSC.sdesc.lpwfxFormat		= (LPWAVEFORMATEX)&ioSC.pcmwf;
		ioSC.sdesc.guid3DAlgorithm	= DS3DALG_DEFAULT;	//DS3DALG_HRTF_FULL;
	}
}

namespace SndManager{
	bool IsAvailable();
}

bool
SndManager::IsAvailable	(				)
{
	return sdev!=NULL;
}

bool
SndManager::Init	(				)
{	// Check if type uint8 for soundAMayBe... is enought
	// 255 is the invalid value.
	NV_ASSERT			(NB_SOUND_MAX <= InvalidSoundID);
	NV_ASSERT_RETURN_MTH(FALSE,sdev==NULL);

	HRESULT hr;
	HWND hwnd;
	core::GetParameter( core::PR_W32_HWND_FOCUS, (uint32*)&hwnd );
	if (!hwnd)
		core::GetParameter( core::PR_W32_HWND, (uint32*)&hwnd );

	NV_ASSERT_RETURN_MTH(FALSE,hwnd);
	
	// Open Device
	hr = NV_DS_CREATE( NULL, &sdev, NULL );
	if( hr!=DS_OK || sdev == NULL) {
		return FALSE;
	}

	hr = sdev->SetCooperativeLevel( hwnd, DSSCL_PRIORITY );
	if( hr!=DS_OK ) {
		sdev->Release();
		sdev = NULL;
		return FALSE;
	}


	sdevCaps.dwSize = sizeof(NV_DSCAPS);
	hr = sdev->GetCaps( &sdevCaps );
	if( hr!=DS_OK ) {
		sdev->Release();
		sdev = NULL;
		return FALSE;
	}

	for (uint i = 0 ; i < NB_SOUND_MAX ; ++i ) {
		soundA[i].state		= SD_UNUSED;
		soundA[i].stateI	= SDI_UNALLOCATED;		
		soundA[i].nextFree	= i + 1;
		soundA[i].prevFree	= i - 1;
		soundA[i].usedVoice	= 0;
	}

	soundA[NB_SOUND_MAX-1].nextFree	= InvalidSoundID;
	soundA[0].prevFree				= InvalidSoundID;
	firstFree						= 0;
	haveAMusicLocked				= FALSE;

	masterVol						= 1.0f;
	voiceBusyMask					= 0;
	voicePauseMask					= 0;
	for( omode = OM_DPL2 ; omode >= OM_MONO ; omode = OutputMode(int(omode)-1) ) {
		if( ProbeOutputMode(omode) ) {
			SetOutputMode(omode);
			break;
		}
	}

	return TRUE;
}


bool
SndManager::Shut	(			)
{
	ChStopAll();
	BgmStop();
	SdFreeAll();
	// Can't shut now ?	
	bool mayUpdate = TRUE;
	uint firstSDUsed = 0;
	while (mayUpdate) {
		mayUpdate = FALSE;
		for ( uint i = firstSDUsed ; i < NB_SOUND_MAX ; ++i ) {
			if (soundA[i].state	!= SD_UNUSED) {
				mayUpdate = TRUE;
				firstSDUsed = i;
				break;			
			}
		}
		mayUpdate = mayUpdate || (voiceBusyMask != 0);		
		Update();
	}

	if( sdev )
	{
		sdev->Release();
		sdev = NULL;
	}
	return TRUE;
}

SndManager::StreamHandler
SndManager::SetHandler			(	StreamHandler		inHandler				)
{
	if (! inHandler ) inHandler = &DefaultStreamCallBack;

	StreamHandler Old = streamLoopCallBack;
	streamLoopCallBack = inHandler;
	return Old;
}

bool
SndManager::ProbeOutputMode	(	OutputMode			inOMode			)
{
	HRESULT hr;
	DWORD   config;
	hr = sdev->GetSpeakerConfig( &config );
	if( hr != DS_OK ) {
		return ( inOMode == OM_STEREO );
	}
	else {
		if( DSSPEAKER_CONFIG(config) == DSSPEAKER_MONO )
			return ( inOMode == OM_MONO );
		else if( DSSPEAKER_CONFIG(config) == DSSPEAKER_HEADPHONE )
			return ( inOMode == OM_STEREO );
		else if( DSSPEAKER_CONFIG(config) == DSSPEAKER_STEREO )
			return ( inOMode == OM_STEREO );
		else
			return ( inOMode == OM_DPL2 );
	}
}

void
SndManager::SetOutputMode	(	OutputMode			inOMode			)
{
	if( ProbeOutputMode(inOMode) ) {
		omode = inOMode;
		// nothing for DX

		DWORD   config;
		if( inOMode == OM_MONO)
			config = DSSPEAKER_MONO ;
		else if( inOMode == OM_STEREO ) 
			config = DSSPEAKER_HEADPHONE; 
		else if( inOMode == OM_STEREO ) 
			config = DSSPEAKER_STEREO;
		else // ( inOMode == OM_DPL2 )
			config = DSSPEAKER_5POINT1;

		HRESULT hr;
		hr = sdev->GetSpeakerConfig( &config );
	}
}


SndManager::OutputMode
SndManager::GetOutputMode	(					)
{
	return omode;
}

void
SndManager::SetMasterVolume	(	float		inVol		)
{
	masterVol = Clamp( inVol, 0.0f, 1.0f );
	for( int i = 0 ; i < NB_CHANNEL ; i++ ) {
		if( voiceBusyMask & (uint64(1)<<i) ) {
			NV_ASSERT( voice[i].sbuffer );
			ApplyVolPan(voice[i].sbuffer,masterVol,voice[i].vol,voice[i].panAngle);
		}
	}
}

void
SndManager::Update	(				)
{
	for (uint i = 0 ; i < NB_SOUND_MAX ; ++i ) {
		if ( soundA[i].state != SD_UNUSED) {
			NV_ASSERT(soundA[i].stateI	!= SDI_UNALLOCATED	);

			switch(soundA[i].state) {
				case SD_LOCKING	:
					if( soundA[i].streamed )
					{
						if( !soundA[i].strm.str_rr.IsReady() )
							break ;
						if( soundA[i].strm.str_rr.GetState() != file::ReadRequest::COMPLETED ) {
							if (soundA[i].strm.str_buffer) {
								NvFree((void*)soundA[i].strm.str_buffer );
								soundA[i].strm.str_buffer = 0;
							}
							soundA[i].state = SD_UNLOCKED;
							soundA[i].stateI = SDI_TORELEASE;							
						}
						else {
							soundA[i].strm.str_avl_bsize		+= soundA[i].strm.str_loading_bsize;
							soundA[i].strm.str_loading_bsize	= 0;
							soundA[i].state						= SD_LOCKED;
						}
					}
					else
					{
						SFX_LoadUpdate(& soundA[i].sfx);
						if ( SFX_IsLoaded(&soundA[i].sfx) )
							soundA[i].state = SD_LOCKED;
						if( !SFX_IsLoaded(& soundA[i].sfx ) && !SFX_IsLoading(& soundA[i].sfx) ) {
							soundA[i].state  = SD_UNLOCKED;
							soundA[i].stateI = SDI_TORELEASE ;
						}
					}
					break;

				case SD_UNLOCKING	:
					if (soundA[i].usedVoice == 0) {
						if (!soundA[i].streamed) {
							if ( !SFX_IsLoaded(& soundA[i].sfx) ) {
								SFX_LoadUpdate(& soundA[i].sfx);
							}
							if ( SFX_IsLoaded(& soundA[i].sfx) ) {
								soundA[i].state = SD_UNLOCKED;
								SFX_Release(& soundA[i].sfx);
							}
						}
						else {
							if ( !soundA[i].strm.str_rr.IsReady() ){
								if ( file::AbortReadRequest(&soundA[i].strm.str_rr) ){
									if ( soundA[i].music ) haveAMusicLocked = FALSE;
									soundA[i].state = SD_UNLOCKED;
								}
							}
							else {
								if ( soundA[i].music ) haveAMusicLocked = FALSE;
								soundA[i].state = SD_UNLOCKED;
							}
						}
					}
					else {
						while (soundA[i].usedVoice ) {
							uint chanId = GetLowestBitNo(soundA[i].usedVoice);
							ChStop(chanId);
							NV_ASSERT( (soundA[i].usedVoice & ( (uint64(0x1)) << chanId)) == 0);
						}
					}
					break;

				default :
					break;
			}

			switch (soundA[i].stateI) {
				case SDI_TORELEASE	:
					if (soundA[i].state == SD_UNLOCKED) {
						if (soundA[i].streamed && soundA[i].strm.str_buffer) {
							NvFree((pvoid)soundA[i].strm.str_buffer);
							soundA[i].strm.str_buffer = 0;
						}
						soundA[i].state				= SD_UNUSED;
						soundA[i].stateI			= SDI_UNALLOCATED;
						soundA[firstFree].prevFree	= i;
						soundA[i].nextFree			= firstFree;
						firstFree					= i;
					}
					break;
				default :
					break;
			}

			if( soundA[i].streamed && soundA[i].usedVoice ) {
				if ( !(soundA[i].strm.str_loading_bsize && !soundA[i].strm.str_rr.IsReady()) ) {

					if( soundA[i].strm.str_rr.GetState() != file::ReadRequest::COMPLETED )
					{
						if (soundA[i].strm.str_buffer) {
							NvFree((void*)soundA[i].strm.str_buffer );
							soundA[i].strm.str_buffer = 0;
						}
						soundA[i].state = SD_UNLOCKED;
						soundA[i].stateI = SDI_TORELEASE;							

						uint ch = GetLowestBitNo( soundA[i].usedVoice );					
						ChStop(ch);
					}
					else
					{
						// Loading ?
						if( soundA[i].strm.str_loading_bsize ) {
							soundA[i].strm.str_avl_bsize    += soundA[i].strm.str_loading_bsize;
							soundA[i].strm.str_loading_bsize = 0;
						}

						// check refill sbuffer ?
						uint ch = GetLowestBitNo( soundA[i].usedVoice );					
						uint fillLen		= _GetStreamingNeedBSize( voice[ch].sbuffer, soundA[i].strm.str_wbpos, ADPCMBSSIZE_TO_PCMBSIZE ( soundA[i].strm.str_bsize ) );
						uint fillLenADPCM	= PCMBSIZE_TO_ADPCMBSSIZE(((fillLen>>1) - ((fillLen>>1)%28)) <<1 );
						NV_ASSERT(!(fillLen&0x1));

						// can refill ?
						if( soundA[i].strm.str_avl_bsize ) {
							if( fillLenADPCM >= soundA[i].strm.str_avl_bsize ) {
								// refill
								_FillStreaming(	voice[ch].sbuffer,
												soundA[i].strm.str_wbpos, soundA[i].strm.str_bsize,
												(uint8*)soundA[i].strm.str_buffer, soundA[i].strm.str_avl_bsize, soundA[i].music );
								soundA[i].strm.str_avl_bsize = 0;

								// Start prefetching
								if( soundA[i].strm.str_cur_bsize ) {
									if (voice[ch].callCallback) {
										if ( !CallCallBack ( i , soundA[i] , voice[ch]) )
											ChStop(ch);
									}
									else {	
										soundA[i].strm.str_loading_bsize	= Min( soundA[i].strm.str_bsize/2U, soundA[i].strm.str_cur_bsize );
										soundA[i].strm.str_rr.bSize			= Round16( soundA[i].strm.str_loading_bsize );
										soundA[i].strm.str_rr.bufferPtr		= (void*)soundA[i].strm. str_buffer;
										soundA[i].strm.str_rr.bOffset[0]	= soundA[i].strm.str_cur_boffset;
										soundA[i].strm.str_rr.bOffset[1]	= soundA[i].strm.str_cur_boffset;
										soundA[i].strm.str_rr.bOffset[2]	= soundA[i].strm.str_cur_boffset;
										soundA[i].strm.str_rr.bOffset[3]	= soundA[i].strm.str_cur_boffset;
										file::AddReadRequest( &soundA[i].strm.str_rr, 8 );
										soundA[i].strm.str_cur_boffset    += soundA[i].strm.str_loading_bsize;
										soundA[i].strm.str_cur_bsize      -= soundA[i].strm.str_loading_bsize;
									}
								}
							}
						}
						else { // No more data to stream
							CallCallBack ( i , soundA[i] , voice[ch]);
							if( fillLen ) {
								// refill with 0
								uint fillLenADPCM2Chan	= PCMBSIZE_TO_ADPCMBSSIZE(((fillLen>>1) - ((fillLen>>1)%56)) <<1 );
								if( soundA[i].strm.str_end_bsize + fillLenADPCM2Chan >= soundA[i].strm.str_bsize )
									ChStop(ch);
								else if (fillLenADPCM2Chan) {
									_FillStreaming(	voice[ch].sbuffer,
													soundA[i].strm.str_wbpos, soundA[i].strm.str_bsize,
													 NULL, fillLenADPCM2Chan ,soundA[i].music);
									soundA[i].strm.str_end_bsize += fillLenADPCM2Chan;
								}
							}
						}
					}
				}
			} 
		}
	}
	// Updating Voice 
	for( int i = 0 ; i <= NB_CHANNEL ; i++ ) {
		if( voiceBusyMask & (uint64(1)<<i) ) {
			NV_ASSERT(voice[i].sbuffer );
			if (!voice[i].sbuffer) continue;
			HRESULT hr; 
			DWORD   status;
			hr = voice[i].sbuffer->GetStatus( &status );
			NV_ASSERT(hr == DS_OK);
			if( hr != DS_OK ) continue;			
			if( ! (status & DSBSTATUS_PLAYING) && !(voicePauseMask & (uint64(1)<<i))) {
				voiceBusyMask							&= ~(uint64(1) << i);
				soundA[voice[i].sndOwnerId].usedVoice	&= ~(uint64(1) << i);
				voice[i].sndOwnerId = -1;
				voice[i].sbuffer->Stop();
				SafeRelease(voice[i].sbuffer);
			}
		}
	}
}

int
SndManager::SdGetNbMax			(												)
{
	return NB_SOUND_MAX;
}

SndManager::SdState
SndManager::SdGetState			(	int					inSoundId				)
{
	NV_ASSERT_RETURN_MTH(SD_UNUSED,inSoundId >= 0 && inSoundId < NB_SOUND_MAX );
	return soundA[inSoundId].state;
}

int
SndManager::SdLock				(	int					inSoundId	,				// valid sound or SD_ANY
									NvSound*			inSndObj	,
									float				inFromTime	,
									float				inDuration	)
									
{
	NvkSound * ks	= inSndObj->GetKInterface();
	NV_ASSERT_RETURN_MTH(-1, ks	);

	if ( haveAMusicLocked && ks->IsMusic()) return -1;

	uint soundId;
	if (inSoundId == SD_ANY)
	{
		soundId=SdAny();
		if (soundId == -1)
			return -1;
		uint allocatedID = SdAlloc(soundId);
		if (allocatedID != soundId)
		{
			SdFree(allocatedID);
			return -1;
		}
	}
	else
	{
		soundId = inSoundId;
	}

	NV_ASSERT_RETURN_MTH( -1, soundId >=0 && soundId < NB_SOUND_MAX );
	if (soundA[soundId].stateI	!= SDI_ALLOCATED) return -1;
	if (soundA[soundId].state	!= SD_UNLOCKED	) return -1;

	soundA[soundId].looped		= (!ks->IsStreamed() && ks->IsLooped());
	soundA[soundId].streamed	= ks->IsStreamed();
	soundA[soundId].music		= ks->IsMusic();
	soundA[soundId].freq		= ks->GetFreq();
	soundA[soundId].duration 	= ks->GetDuration();
	ks->GetBFData(soundA[soundId].bOffset,soundA[soundId].bSize);

	InitSDesc( soundA[soundId], ks );

	soundA[soundId].sfx.sbuffer = NV_DSSOUNDBUFFER(0);

	if(soundA[soundId].streamed)
	{
		uint32 streamMST;
		core::GetParameter( core::PR_SNDMAN_STREAM_CACHEMST, &streamMST );
		uint32 nbChan = soundA[soundId].music?2:1;
		uint pcmBSize = (ks->GetFreq() * streamMST * 2 * nbChan) / 1000;
		soundA[soundId].strm.str_bsize = BLOCK_TO_BSIZE(SAMPLE_TO_BLOCK(PCMBSIZE_TO_SAMPLE(pcmBSize)));
		soundA[soundId].strm.str_bsize = Round64(soundA[soundId].strm.str_bsize); // 32 = 2*2*16 => 4 adpcm block => 2(L/R) in each half buffer 

		if( soundA[soundId].strm.str_bsize == 0 ) {
			SdFree(soundId);
			return -1;
		}

		soundA[soundId].strm.str_buffer = (uint32)NvMalloc( soundA[soundId].strm.str_bsize );
		if( !soundA[soundId].strm.str_buffer ) {
			SdFree(soundId);
			return -1;
		}
		
		if (inFromTime < 0.0f ) 
			inFromTime = 0.0f;
		if (inDuration <=0.0f) 
			inDuration = ks->GetDuration() - inFromTime;

		if (! StreamSetSegment (	soundId	, inFromTime , inFromTime + inDuration, TRUE )) {
			SdFree(soundId);
			return -1;
		}

		soundA[soundId].state	= SD_LOCKING;
	} 
	else {
		SFX_Load(&soundA[soundId].sfx,soundA[soundId].sdesc,soundA[soundId].bOffset);
		soundA[soundId].state = SD_LOCKING;
	}
	if ( ks->IsMusic() ) haveAMusicLocked = TRUE;
	return soundId;
}

bool
SndManager::SdUnlock			(	int					inSoundId				)
{
	NV_ASSERT_RETURN_MTH(FALSE, inSoundId >=0 && inSoundId < NB_SOUND_MAX	);
	
	if (soundA[inSoundId].state == SD_LOCKED || soundA[inSoundId].state == SD_LOCKING) {
		NV_ASSERT(soundA[inSoundId].stateI != SDI_UNALLOCATED);
		soundA[inSoundId].state = SD_UNLOCKING;
	}
	return TRUE;
}

void
SndManager::SdFree				(	int					inSoundId				)
{
	NV_ASSERT_RETURN_CAL(inSoundId >=0 && inSoundId < NB_SOUND_MAX	);
	if (soundA[inSoundId].stateI != SDI_UNALLOCATED ) { 
		soundA[inSoundId].stateI = SDI_TORELEASE;
		if (soundA[inSoundId].state != SD_UNLOCKED) 
			soundA[inSoundId].state = SD_UNLOCKING;
	}
}

void
SndManager::SdFreeAll			(			)
{
	for (uint i = 0 ; i < NB_SOUND_MAX ; ++i ) {
		SdFree(i);
	}
}

int
SndManager::SdAlloc				(	int					inSoundId 		)
{
	NV_ASSERT_RETURN_MTH( -1, (inSoundId >=0 && inSoundId < NB_SOUND_MAX) || inSoundId == SD_ANY );
	
	if (inSoundId == SD_ANY){
		inSoundId=SdAny();
		if (inSoundId == -1) 
			return -1;
	}

	if ( soundA[inSoundId].stateI == SDI_UNALLOCATED		) {
		NV_ASSERT(soundA[inSoundId].state	== SD_UNUSED	);
		soundA[inSoundId].stateI = SDI_ALLOCATED;
		soundA[inSoundId].state	= SD_UNLOCKED;
		if ( soundA[inSoundId].prevFree != InvalidSoundID )
			soundA[soundA[inSoundId].prevFree].nextFree = soundA[inSoundId].nextFree;
		if ( soundA[inSoundId].nextFree != InvalidSoundID )
			soundA[soundA[inSoundId].nextFree].prevFree = soundA[inSoundId].prevFree;
		if ( firstFree == inSoundId) {
			if ( soundA[inSoundId].prevFree != InvalidSoundID )
				firstFree = soundA[inSoundId].prevFree;
			else if ( soundA[inSoundId].nextFree != InvalidSoundID )
				firstFree = soundA[inSoundId].nextFree;
			else 
				firstFree = InvalidSoundID;
		}
		return inSoundId;
	}
	return -1;
}

int
SndManager::SdAny				(												)
{
	NV_ASSERTC(firstFree != InvalidSoundID, "Not Enouth Sound buffer\n");
	if (firstFree == InvalidSoundID) return -1;

	int ret = firstFree;
	if ( soundA[firstFree].prevFree != InvalidSoundID )
		firstFree = soundA[firstFree].prevFree;
	else if ( soundA[firstFree].nextFree != InvalidSoundID )
		firstFree = soundA[firstFree].nextFree;
	else 
		firstFree = InvalidSoundID;
	return ret;
}

bool
SndManager::SdBreak		(	int		inSoundId	)
{
	if (inSoundId < 0 || inSoundId >= NB_SOUND_MAX )	return FALSE;
	SoundChain & sc = soundA[inSoundId];
	if (sc.streamed && sc.usedVoice) {
		int vcId = GetLowestBitNo(sc.usedVoice);
		NV_ASSERT ( vcId >= 0 && vcId < (NB_CHANNEL+1) );
		Voice & vc = voice[vcId];

		if ( !(voiceBusyMask 	& sc.usedVoice))return FALSE;
		
		vc.callCallback = TRUE;
		return TRUE;
	}
	else 
		return FALSE;
}
//
// Channel management

int
SndManager::ChGetNb				(												)
{
	return NB_CHANNEL;
}

SndManager::ChState
SndManager::ChGetState			(	int					inChannel				)
{
	uint64 chanMsk = uint64(1) << inChannel ;
	if (inChannel < 0 || inChannel > NB_CHANNEL || !(voiceBusyMask & chanMsk) ) 
		return CH_IDLE;	
	return CH_PLAYING;
}

int
SndManager::ChAny		(	)
{
	uint notVoiceBusyMask  = ~voiceBusyMask;
	if (!notVoiceBusyMask)
		return -1;
	uint ret = GetLowestBitNo(notVoiceBusyMask);
	if (ret >= NB_CHANNEL )
		return -1;

	return ret;
}

int
SndManager::ChPlay				(	int					inChannel			,	// valid channel or CH_ANY
									int					inSoundId			,
									float				inVol				,	// volume in [0,1]
									float				inPanAngle			,	// panning angle in [-Pi,Pi]
									float				inPitchFactor		)
{
	int channelId = inChannel;
	if (inChannel == CH_ANY) {
		channelId = ChAny();
	}
	uint64 chanMsk = uint64(1) << channelId ;
	if ( channelId < 0 || channelId > NB_CHANNEL || (voiceBusyMask & chanMsk) )
		return -1;
	if (SdGetState(inSoundId) !=SD_LOCKED)
		return -1;

	if (soundA[inSoundId].music && inChannel != BGM_VOICE)
		return -1;

	if (!soundA[inSoundId].strm.str_rr.IsReady()){
		if (!file::AbortReadRequest(&soundA[inSoundId].strm.str_rr))
			return -1;
	}

	voice[channelId].vol			= inVol;
	voice[channelId].panAngle		= inPanAngle;
	voice[channelId].pitchFactor	= inPitchFactor;
	voice[channelId].sndOwnerId		= inSoundId;
	voice[channelId].callCallback	= FALSE;
	if (soundA[inSoundId].streamed) {
		if (soundA[inSoundId].usedVoice)
			return -1;

		NV_DSBUFFERDESC sdesc;
		Memcpy( &sdesc, &soundA[inSoundId].sdesc, sizeof(NV_DSBUFFERDESC) );
		sdesc.dwBufferBytes = ADPCMBSSIZE_TO_PCMBSIZE(soundA[inSoundId].strm.str_bsize);
		if (soundA[inSoundId].music) 
			sdesc.dwFlags &= ~DSBCAPS_CTRL3D;

		// Init sound buffer
		HRESULT hr;
		hr = sdev->CreateSoundBuffer( &sdesc, &voice[channelId].sbuffer, NULL );
		if( hr!=DS_OK ) {		
			return -1;
		}

		soundA[inSoundId].usedVoice |= uint64(1) << channelId;
		voiceBusyMask				|= uint64(1) << channelId;
		LPVOID lpvWrite;
		DWORD  dwLength;
		hr = voice[channelId].sbuffer->Lock( 0, 0, &lpvWrite, &dwLength, NULL, NULL, DSBLOCK_ENTIREBUFFER );
		NV_ASSERT( hr == DS_OK );
		Memset( lpvWrite, 0, dwLength );

		uint nbUncomSample	= (soundA[inSoundId].strm.str_avl_bsize/16) * 28 ;
		DepackVag( (int16*)lpvWrite, (void*)soundA[inSoundId].strm.str_buffer, nbUncomSample , soundA[inSoundId].music);
		hr = voice[channelId].sbuffer->Unlock( lpvWrite, dwLength, NULL, 0 );
		NV_ASSERT( hr == DS_OK );

		// Start prefetching
		soundA[inSoundId].strm.str_avl_bsize	= 0;
		soundA[inSoundId].strm.str_wbpos		= 0;
		soundA[inSoundId].strm.str_end_bsize	= 0;
		if( soundA[inSoundId].strm.str_cur_bsize ) {
			soundA[inSoundId].strm.str_loading_bsize	= Min( soundA[inSoundId].strm.str_bsize/2U, soundA[inSoundId].strm.str_cur_bsize );
			soundA[inSoundId].strm.str_rr.bSize			= Round16( soundA[inSoundId].strm.str_loading_bsize );
			soundA[inSoundId].strm.str_rr.bufferPtr		= (void*) soundA[inSoundId].strm.str_buffer;
			soundA[inSoundId].strm.str_rr.bOffset[0]	= soundA[inSoundId].strm.str_cur_boffset;
			soundA[inSoundId].strm.str_rr.bOffset[1]	= soundA[inSoundId].strm.str_cur_boffset;
			soundA[inSoundId].strm.str_rr.bOffset[2]	= soundA[inSoundId].strm.str_cur_boffset;
			soundA[inSoundId].strm.str_rr.bOffset[3]	= soundA[inSoundId].strm.str_cur_boffset;
			file::AddReadRequest( &soundA[inSoundId].strm.str_rr, 8 );
			soundA[inSoundId].strm.str_cur_boffset    += soundA[inSoundId].strm.str_loading_bsize;
			soundA[inSoundId].strm.str_cur_bsize      -= soundA[inSoundId].strm.str_loading_bsize;
		}

		ChSetVolume			(channelId,inVol);		
		ChSetPitchFactor	(channelId,inPitchFactor);

		hr = voice[channelId].sbuffer->Play( 0, 0, DSBPLAY_LOOPING );
		Sleep(1);
		NV_ASSERT( hr == DS_OK );
	}
	else
	{
		bool res = SFX_Duplicate ( &(voice[channelId].sbuffer), soundA[inSoundId].sdesc, & soundA[inSoundId].sfx.sbuffer );		
		if (!res || !voice[channelId].sbuffer)
			return -1;

		voiceBusyMask |= uint64(1) << channelId;

		soundA[inSoundId].usedVoice |= uint64(1) << channelId;

		ApplyVolPan( voice[channelId].sbuffer,	masterVol,voice[channelId].vol,voice[channelId].panAngle);
		ChSetPitchFactor(channelId,inPitchFactor);

		HRESULT hr;
		hr = voice[channelId].sbuffer->Play( 0, 0, soundA[inSoundId].looped?DSBPLAY_LOOPING:0 );
		Sleep(1);
		NV_ASSERT( hr == DS_OK );
	}
	return channelId;
}

bool
SndManager::ChStop				(	int					inChannel				)
{
	uint64 chanMsk = uint64(1) << inChannel ;
	if ( inChannel < 0 || inChannel > NB_CHANNEL || !(voiceBusyMask & chanMsk) )
		return FALSE;

	NV_ASSERT(voice[inChannel].sbuffer);

	voice[inChannel].sbuffer->Stop();

	if ( soundA[voice[inChannel].sndOwnerId].streamed ) {
		SdUnlock(voice[inChannel].sndOwnerId);
	}

	voiceBusyMask									&= ~(uint64(1) << inChannel);
	soundA[voice[inChannel].sndOwnerId].usedVoice	&= ~(uint64(1) << inChannel);
	voice[inChannel].sndOwnerId = -1;
	SafeRelease(voice[inChannel].sbuffer);

	return TRUE;
}

void
SndManager::ChStopAll			(												)
{
	for (uint i = 0 ; i < NB_CHANNEL ; ++i ) {
		ChStop(i);
	}
}

bool
SndManager::ChPause				(	int					inChannel				)
{
	uint64 chanMsk = uint64(1) << inChannel ;
	if ( inChannel < 0 || inChannel > NB_CHANNEL || !(voiceBusyMask & chanMsk) )
		return FALSE;

	NV_ASSERT(voice[inChannel].sbuffer);
	
	voice[inChannel].sbuffer->Stop();
	voicePauseMask |= chanMsk;
	return TRUE;
}

bool
SndManager::ChResume			(	int					inChannel				)
{
	uint64 chanMsk = uint64(1) << inChannel ;
	if ( inChannel < 0 || inChannel > NB_CHANNEL || !(voiceBusyMask & chanMsk) )
		return FALSE;

	NV_ASSERT(voice[inChannel].sbuffer);

	HRESULT hr;	
	if (soundA[voice[inChannel].sndOwnerId].streamed) 
		hr = voice[inChannel].sbuffer->Play( 0, 0, DSBPLAY_LOOPING );
	else 
		hr = voice[inChannel].sbuffer->Play( 0, 0, soundA[voice[inChannel].sndOwnerId].looped?DSBPLAY_LOOPING:0 );

	Sleep(1);

	voicePauseMask &= ~chanMsk;
	NV_ASSERT( hr == DS_OK );

	return TRUE;
}

bool
SndManager::ChSetVolume			(	int					inChannel,
									float				inVol					)
{

	if (inChannel <0 || inChannel > NB_CHANNEL) 
		return FALSE;

	if( voiceBusyMask & (uint64(1)<<inChannel) ) {
		NV_ASSERT(voice[inChannel].sbuffer);
		voice[inChannel].vol = inVol;
		return ApplyVolPan(voice[inChannel].sbuffer,masterVol,voice[inChannel].vol,voice[inChannel].panAngle);
	}
	return FALSE;

}
	// volume in [0,1]
bool
SndManager::ChSetPanning		(	int					inChannel,
									float				inAngle					)
{
	if (inChannel <0 || inChannel >= NB_CHANNEL) 
		return FALSE;

	if( voiceBusyMask & (uint64(1)<<inChannel) ) {
		NV_ASSERT(voice[inChannel].sbuffer);
		voice[inChannel].panAngle = inAngle;
		return ApplyVolPan(voice[inChannel].sbuffer,masterVol,voice[inChannel].vol,voice[inChannel].panAngle);
	}
	return FALSE;

}
	// panning angle in [-Pi,Pi] 
bool
SndManager::ChSetPitchFactor	(	int					inChannel,
									float				inFactor				)
{

	if (inChannel <0 || inChannel >= NB_CHANNEL ) 
		return FALSE;

	if( voiceBusyMask & (uint64(1)<<inChannel) ) {
		NV_ASSERT( voice[inChannel].sbuffer );		
		NV_ASSERT( inFactor > 0.0f );

		LONG freq = soundA[voice[inChannel].sndOwnerId].freq * inFactor;
		voice[inChannel].sbuffer->SetFrequency( freq );
		return TRUE;
	}
	return FALSE;
}

//
// Background music management

bool
SndManager::BgmPlay				(	int					inSoundId,
									float				inVol			)
{
	if (SdGetState(inSoundId) !=SD_LOCKED)
		return FALSE;
	if (! soundA[inSoundId].streamed )
		return FALSE;

	if (! soundA[inSoundId].music )
		return FALSE;

	int ret = ChPlay(BGM_VOICE,inSoundId,inVol);

	return ret == BGM_VOICE;
}

bool
SndManager::BgmStop				(										)
{
	return ChStop(BGM_VOICE);
}

bool
SndManager::BgmPause			(										)
{
	return ChPause(BGM_VOICE);
}

bool
SndManager::BgmResume			(										)
{
	return ChResume(BGM_VOICE);
}

bool
SndManager::BgmSetVolume		(	float				inVol			)
{
	return ChSetVolume(BGM_VOICE,inVol);
}

bool	
SndManager::BgmBreak	(		)
{
	return SdBreak(voice[BGM_VOICE].sndOwnerId );
}

bool
SndManager::BgmIsPlaying		(										)
{
	return (ChGetState(BGM_VOICE) == CH_PLAYING);
}

//
// Tools
void
SndManager::OutputDSError	(	HRESULT	inHr	)
{
	if( inHr == DS_OK )							Printf( "DSound error: DS_OK\n" );
	else if( inHr == DS_NO_VIRTUALIZATION )		Printf( "DSound error: DS_NO_VIRTUALIZATION\n" );
	else if( inHr == DSERR_ALLOCATED )			Printf( "DSound error: DSERR_ALLOCATED\n" );
	else if( inHr == DSERR_CONTROLUNAVAIL )		Printf( "DSound error: DSERR_CONTROLUNAVAIL\n" );
	else if( inHr == DSERR_INVALIDPARAM )		Printf( "DSound error: DSERR_INVALIDPARAM\n" );
	else if( inHr == DSERR_INVALIDCALL )		Printf( "DSound error: DSERR_INVALIDCALL\n" );
	else if( inHr == DSERR_GENERIC )			Printf( "DSound error: DSERR_GENERIC\n" );
	else if( inHr == DSERR_PRIOLEVELNEEDED )	Printf( "DSound error: DSERR_PRIOLEVELNEEDED\n" );
	else if( inHr == DSERR_OUTOFMEMORY )		Printf( "DSound error: DSERR_OUTOFMEMORY\n" );
	else if( inHr == DSERR_BADFORMAT )			Printf( "DSound error: DSERR_BADFORMAT\n" );
	else if( inHr == DSERR_UNSUPPORTED )		Printf( "DSound error: DSERR_UNSUPPORTED\n" );
	else if( inHr == DSERR_NODRIVER )			Printf( "DSound error: DSERR_NODRIVER\n" );
	else if( inHr == DSERR_ALREADYINITIALIZED )	Printf( "DSound error: DSERR_ALREADYINITIALIZED\n" );
	else if( inHr == DSERR_NOAGGREGATION )		Printf( "DSound error: DSERR_NOAGGREGATION\n" );
	else if( inHr == DSERR_BUFFERLOST )			Printf( "DSound error: DSERR_BUFFERLOST\n" );
	else if( inHr == DSERR_OTHERAPPHASPRIO )	Printf( "DSound error: DSERR_OTHERAPPHASPRIO\n" );
	else if( inHr == DSERR_UNINITIALIZED )		Printf( "DSound error: DSERR_UNINITIALIZED\n" );
	else if( inHr == DSERR_NOINTERFACE )		Printf( "DSound error: DSERR_NOINTERFACE\n" );
	else if( inHr == DSERR_ACCESSDENIED )		Printf( "DSound error: DSERR_ACCESSDENIED\n" );
	else if( inHr == DSERR_BUFFERTOOSMALL )		Printf( "DSound error: DSERR_BUFFERTOOSMALL\n" );
	else if( inHr == DSERR_DS8_REQUIRED )		Printf( "DSound error: DSERR_DS8_REQUIRED\n" );
	else if( inHr == DSERR_SENDLOOP )			Printf( "DSound error: DSERR_SENDLOOP\n" );
	else if( inHr == DSERR_BADSENDBUFFERGUID )	Printf( "DSound error: DSERR_BADSENDBUFFERGUID\n" );
	else if( inHr == DSERR_OBJECTNOTFOUND )		Printf( "DSound error: DSERR_OBJECTNOTFOUND\n" );
	else if( inHr == DSERR_FXUNAVAILABLE )		Printf( "DSound error: DSERR_FXUNAVAILABLE\n" );
}

float
SndManager::MbToLinearVolume(	float		inMillibel		)
{
	// [-10000,0] -> [0,1]
	float mb = Clamp( inMillibel, -10000.0f, 0.0f );
	if( mb == -10000.0f )
		return 0.0f;
	else {
		float lin = Powf( 10.0f, mb*(1.0f/2000.0f) );
		return Clamp( lin, 0.0f, 1.0f );
	}
}

float
SndManager::LinearToMbVolume(	float		inLinear		)
{
	// [0,1] -> [-10000,0]
	float lin = Clamp( inLinear, 0.0f, 1.0f );
	if( lin == 0.0f )
		return -10000.0f;
	else {
		int mb = int( Log10f(lin) * 2000.0f );
		return Clamp( mb, -10000, 0 );
	}
}
