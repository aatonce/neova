/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


uint
InitRasters	(	uint	inBaseBAddr	)
{
	uint32 offFrameW, offFrameH, offFrameWL2 = 0, offFrameHL2 = 0;
	nv::core::GetParameter( nv::core::PR_OFF_FRAME_W, &offFrameW );
	nv::core::GetParameter( nv::core::PR_OFF_FRAME_H, &offFrameH );
	while( offFrameW > (1U<<offFrameWL2) )	offFrameWL2++;
	while( offFrameH > (1U<<offFrameHL2) )	offFrameHL2++;

	uint offBAddr = Round32( inBaseBAddr );
	uint offBSize = ((offFrameW+63)/64)
				  * ((offFrameH+31)/32)
				  * 32 * 2;

	uint rgbaBAddr	= offBAddr;
	uint depthBAddr = offBAddr + offBSize/2;

	DpyManager::rasterMaxWidth  = 0;
	DpyManager::rasterMaxHeight = 0;

	Zero( DpyManager::raster );
	for( uint i = DPYR_IN_FRAME ; i <= DPYR_OFF_FRAME_C32Z32 ; i++ )
	{
		switch( DpyRaster(i) )
		{
		case DPYR_IN_FRAME :
			{
				uint tbw = Max( (gs::frame::SizeX+63)/64, 1U );
				uint WL2 = 0, HL2 = 0;
				while( gs::frame::SizeX > (1U<<WL2) )	WL2++;
				while( gs::frame::SizeY > (1U<<HL2) )	HL2++;
				DpyManager::raster[i].width 			= gs::frame::SizeX;
				DpyManager::raster[i].height			= gs::frame::SizeY;
				DpyManager::raster[i].rgbaBAddr			= gs::frame::BackBAddr;
				DpyManager::raster[i].depthBAddr		= gs::frame::DepthBAddr;
				DpyManager::raster[i].rgbaFrameReg		= gs::frame::BackFrameReg;
				DpyManager::raster[i].rgbaXYOffsetReg	= gs::frame::DefContext.xyoffset;
				DpyManager::raster[i].rgbaScissor		= gs::frame::DefContext.scissor;
				DpyManager::raster[i].rgbaTex0Reg		= SCE_GS_SET_TEX0( gs::frame::BackBAddr, tbw, SCE_GS_PSMCT32, WL2, HL2, 0, 0, 0, 0, 0, 0, 0 );
				DpyManager::raster[i].rgbaTex1Reg		= SCE_GS_SET_TEX1( 0, 0, 1, 1, 0, 0, 0 );
				DpyManager::raster[i].depthFrameReg		= gs::frame::DepthFrameReg;
				DpyManager::raster[i].depthZBufReg		= gs::frame::DepthZBufReg;
			}
			break;

		case DPYR_OFF_FRAME_C16 :
			if( offFrameW && offFrameH )
			{
				uint tbw = Max( (offFrameW*2+63)/64, 1U );
				DpyManager::raster[i].width 			= offFrameW*2;
				DpyManager::raster[i].height			= offFrameH*2;
				DpyManager::raster[i].rgbaBAddr			= rgbaBAddr;
				DpyManager::raster[i].rgbaFrameReg		= SCE_GS_SET_FRAME( rgbaBAddr>>5, tbw, SCE_GS_PSMCT16S, 0 );
				DpyManager::raster[i].rgbaXYOffsetReg	= SCE_GS_SET_XYOFFSET( ((2048-DpyManager::raster[i].width/2)<<4), ((2048-DpyManager::raster[i].height/2)<<4) );
				DpyManager::raster[i].rgbaScissor		= SCE_GS_SET_SCISSOR( 0, DpyManager::raster[i].width-1, 0, DpyManager::raster[i].height-1 );
				DpyManager::raster[i].rgbaTex0Reg		= SCE_GS_SET_TEX0( rgbaBAddr, tbw, SCE_GS_PSMCT16S, offFrameWL2+1, offFrameHL2+1, 0, 0, 0, 0, 0, 0, 0 );
				DpyManager::raster[i].rgbaTex1Reg		= SCE_GS_SET_TEX1( 0, 0, 1, 1, 0, 0, 0 );
				DpyManager::raster[i].depthFrameReg		= SCE_GS_SET_FRAME( 0, 1, 0, 0xFFFFFFFF );	// not available !
				DpyManager::raster[i].depthZBufReg		= SCE_GS_SET_ZBUF( 0, 0, 1 );	// not available !
			}
			break;

		case DPYR_OFF_FRAME_C32H :
			if( offFrameW && offFrameH )
			{
				uint tbw = Max( (offFrameW*2+63)/64, 1U );
				DpyManager::raster[i].width 			= offFrameW*2;
				DpyManager::raster[i].height			= offFrameH;
				DpyManager::raster[i].rgbaBAddr			= rgbaBAddr;
				DpyManager::raster[i].rgbaFrameReg		= SCE_GS_SET_FRAME( rgbaBAddr>>5, tbw, SCE_GS_PSMCT32, 0 );
				DpyManager::raster[i].rgbaXYOffsetReg	= SCE_GS_SET_XYOFFSET( ((2048-DpyManager::raster[i].width/2)<<4), ((2048-DpyManager::raster[i].height/2)<<4) );
				DpyManager::raster[i].rgbaScissor		= SCE_GS_SET_SCISSOR( 0, DpyManager::raster[i].width-1, 0, DpyManager::raster[i].height-1 );
				DpyManager::raster[i].rgbaTex0Reg		= SCE_GS_SET_TEX0( rgbaBAddr, tbw, SCE_GS_PSMCT32, offFrameWL2+1, offFrameHL2, 0, 0, 0, 0, 0, 0, 0 );
				DpyManager::raster[i].rgbaTex1Reg		= SCE_GS_SET_TEX1( 0, 0, 1, 1, 0, 0, 0 );
				DpyManager::raster[i].depthFrameReg		= SCE_GS_SET_FRAME( 0, 1, 0, 0xFFFFFFFF );	// not available !
				DpyManager::raster[i].depthZBufReg		= SCE_GS_SET_ZBUF( 0, 0, 1 );	// not available !
			}
			break;

		case DPYR_OFF_FRAME_C32V :
			if( offFrameW && offFrameH )
			{
				uint tbw = Max( (offFrameW+63)/64, 1U );
				DpyManager::raster[i].width 			= offFrameW;
				DpyManager::raster[i].height			= offFrameH*2;
				DpyManager::raster[i].rgbaBAddr			= rgbaBAddr;
				DpyManager::raster[i].rgbaFrameReg		= SCE_GS_SET_FRAME( rgbaBAddr>>5, tbw, SCE_GS_PSMCT32, 0 );
				DpyManager::raster[i].rgbaXYOffsetReg	= SCE_GS_SET_XYOFFSET( ((2048-DpyManager::raster[i].width/2)<<4), ((2048-DpyManager::raster[i].height/2)<<4) );
				DpyManager::raster[i].rgbaScissor		= SCE_GS_SET_SCISSOR( 0, DpyManager::raster[i].width-1, 0, DpyManager::raster[i].height-1 );
				DpyManager::raster[i].rgbaTex0Reg		= SCE_GS_SET_TEX0( rgbaBAddr, tbw, SCE_GS_PSMCT32, offFrameWL2, offFrameHL2+1, 0, 0, 0, 0, 0, 0, 0 );
				DpyManager::raster[i].rgbaTex1Reg		= SCE_GS_SET_TEX1( 0, 0, 1, 1, 0, 0, 0 );
				DpyManager::raster[i].depthFrameReg		= SCE_GS_SET_FRAME( 0, 1, 0, 0xFFFFFFFF );	// not available !
				DpyManager::raster[i].depthZBufReg		= SCE_GS_SET_ZBUF( 0, 0, 1 );	// not available !
			}
			break;

		case DPYR_OFF_FRAME_C16Z16H :
			if( offFrameW && offFrameH )
			{
				uint tbw = Max( (offFrameW*2+63)/64, 1U );
				DpyManager::raster[i].width 			= offFrameW*2;
				DpyManager::raster[i].height			= offFrameH;
				DpyManager::raster[i].rgbaBAddr			= rgbaBAddr;
				DpyManager::raster[i].rgbaFrameReg		= SCE_GS_SET_FRAME( rgbaBAddr>>5, tbw, SCE_GS_PSMCT16S, 0 );
				DpyManager::raster[i].rgbaXYOffsetReg	= SCE_GS_SET_XYOFFSET( ((2048-DpyManager::raster[i].width/2)<<4), ((2048-DpyManager::raster[i].height/2)<<4) );
				DpyManager::raster[i].rgbaScissor		= SCE_GS_SET_SCISSOR( 0, DpyManager::raster[i].width-1, 0, DpyManager::raster[i].height-1 );
				DpyManager::raster[i].rgbaTex0Reg		= SCE_GS_SET_TEX0( rgbaBAddr, tbw, SCE_GS_PSMCT16S, offFrameWL2+1, offFrameHL2, 0, 0, 0, 0, 0, 0, 0 );
				DpyManager::raster[i].rgbaTex1Reg		= SCE_GS_SET_TEX1( 0, 0, 1, 1, 0, 0, 0 );
				DpyManager::raster[i].depthBAddr		= depthBAddr;
				DpyManager::raster[i].depthFrameReg		= SCE_GS_SET_FRAME( depthBAddr>>5, tbw, SCE_GS_PSMCT16S, 0 );
				DpyManager::raster[i].depthZBufReg		= SCE_GS_SET_ZBUF( depthBAddr>>5, (SCE_GS_PSMZ16S-SCE_GS_PSMZ32), 0 );
			}
			break;

		case DPYR_OFF_FRAME_C16Z16V :
			if( offFrameW && offFrameH )
			{
				uint tbw = Max( (offFrameW+63)/64, 1U );
				DpyManager::raster[i].width 			= offFrameW;
				DpyManager::raster[i].height			= offFrameH*2;
				DpyManager::raster[i].rgbaBAddr			= rgbaBAddr;
				DpyManager::raster[i].rgbaFrameReg		= SCE_GS_SET_FRAME( rgbaBAddr>>5, tbw, SCE_GS_PSMCT16S, 0 );
				DpyManager::raster[i].rgbaXYOffsetReg	= SCE_GS_SET_XYOFFSET( ((2048-DpyManager::raster[i].width/2)<<4), ((2048-DpyManager::raster[i].height/2)<<4) );
				DpyManager::raster[i].rgbaScissor		= SCE_GS_SET_SCISSOR( 0, DpyManager::raster[i].width-1, 0, DpyManager::raster[i].height-1 );
				DpyManager::raster[i].rgbaTex0Reg		= SCE_GS_SET_TEX0( rgbaBAddr, tbw, SCE_GS_PSMCT16S, offFrameWL2, offFrameHL2+1, 0, 0, 0, 0, 0, 0, 0 );
				DpyManager::raster[i].rgbaTex1Reg		= SCE_GS_SET_TEX1( 0, 0, 1, 1, 0, 0, 0 );
				DpyManager::raster[i].depthBAddr		= depthBAddr;
				DpyManager::raster[i].depthFrameReg		= SCE_GS_SET_FRAME( depthBAddr>>5, tbw, SCE_GS_PSMCT16S, 0 );
				DpyManager::raster[i].depthZBufReg		= SCE_GS_SET_ZBUF( depthBAddr>>5, (SCE_GS_PSMZ16S-SCE_GS_PSMZ32), 0 );
			}
			break;

		case DPYR_OFF_FRAME_C32Z32 :
			if( offFrameW && offFrameH )
			{
				uint tbw	 = Max( (offFrameW+63)/64, 1U );
				DpyManager::raster[i].width 			= offFrameW;
				DpyManager::raster[i].height			= offFrameH;
				DpyManager::raster[i].rgbaBAddr			= rgbaBAddr;
				DpyManager::raster[i].rgbaFrameReg		= SCE_GS_SET_FRAME( rgbaBAddr>>5, tbw, SCE_GS_PSMCT32, 0 );
				DpyManager::raster[i].rgbaXYOffsetReg	= SCE_GS_SET_XYOFFSET( ((2048-DpyManager::raster[i].width/2)<<4), ((2048-DpyManager::raster[i].height/2)<<4) );
				DpyManager::raster[i].rgbaScissor		= SCE_GS_SET_SCISSOR( 0, DpyManager::raster[i].width-1, 0, DpyManager::raster[i].height-1 );
				DpyManager::raster[i].rgbaTex0Reg		= SCE_GS_SET_TEX0( rgbaBAddr, tbw, SCE_GS_PSMCT32, offFrameWL2, offFrameHL2, 0, 0, 0, 0, 0, 0, 0 );
				DpyManager::raster[i].rgbaTex1Reg		= SCE_GS_SET_TEX1( 0, 0, 1, 1, 0, 0, 0 );
				DpyManager::raster[i].depthBAddr		= depthBAddr;
				DpyManager::raster[i].depthFrameReg		= SCE_GS_SET_FRAME( depthBAddr>>5, tbw, SCE_GS_PSMCT16S, 0 );
				DpyManager::raster[i].depthZBufReg		= SCE_GS_SET_ZBUF( depthBAddr>>5, (SCE_GS_PSMZ16S-SCE_GS_PSMZ32), 0 );
			}
			break;
		}

		DpyManager::rasterMaxWidth  = Max( DpyManager::rasterMaxWidth,  float(DpyManager::raster[i].width)  );
		DpyManager::rasterMaxHeight = Max( DpyManager::rasterMaxHeight, float(DpyManager::raster[i].height) );
	}


	// Init clipping parameters
	// La translation maximale sur X/Y, due au viewport, est de rasterMaxHalfSize pixels.
	// En consid�rant cette translation, le range de clipping valide est
	//		[ - (2040-rasterMaxHalfSize) , + (2040-rasterMaxHalfSize) ]
	// sur X/Y, avec une guardband r�duite de 16 pixels.
	// Les sommets sont scal�s par rapport aux dimensions du viewport.
	// Ce scale doit �tre pris en compte dans le clipping 4D, par les
	// coefficients m11 et m22 de la matrice de projection.
	// Afin de rendre ces coefficients de matrice ind�pendants du viewport,
	// la valeur maximale de viewport est utilis�e, correspondant � rasterMaxHalfSize.
	// Ce scale maximale de viewport est corrig� dans le scale de projection (voir GetProjNormalised()).
	// Donc :
	// - m11 et m22 sont multipli�s par projNormalisedXY (=> clipMatrix)
	// - projScale.x = (viewportWidth/2)  / projNormalisedXY
	//   projScale.y = (viewportHeight/2) / projNormalisedXY

	float viewportMaxHSize			= Max( DpyManager::rasterMaxWidth, DpyManager::rasterMaxHeight ) * 0.5f;
	float guardBandClip				= 1.0f / (2040.0f - viewportMaxHSize);
	DpyManager::projNormalisedXY	= viewportMaxHSize * guardBandClip;
	DpyManager::ooProjNormalisedXY	= 1.0f / DpyManager::projNormalisedXY;

	MatrixIdentity( &DpyManager::clipMatrix );
	DpyManager::clipMatrix.m11 = DpyManager::projNormalisedXY;
	DpyManager::clipMatrix.m22 = DpyManager::projNormalisedXY;

	for( uint i = DPYR_IN_FRAME ; i <= DPYR_OFF_FRAME_C32Z32 ; i++ )
	{
		const float ITOF4_pack = 524288.0F;			// 0x49000000
		float rw	= float( int(DpyManager::raster[i].width)  );
		float rh	= float( int(DpyManager::raster[i].height) );
		float rx0	= 2048.0f - rw * 0.5f;
		float ry0	= 2048.0f - rh * 0.5f;

		// 2D
		// Z in [Zmax->0] from [0,1]

		DpyManager::raster[i].scale0[0].x	= 1.f;
		DpyManager::raster[i].scale0[0].y	= 1.f;
		DpyManager::raster[i].scale0[0].z	= DpyManager::zbuffMax;
		DpyManager::raster[i].scale0[0].w	= 0.f;

		DpyManager::raster[i].trans0[0].x	= ITOF4_pack + rx0 /*+ vp_x*/;
		DpyManager::raster[i].trans0[0].y	= ITOF4_pack + ry0 /*+ vp_y*/;
		DpyManager::raster[i].trans0[0].z	= ITOF4_pack;
		DpyManager::raster[i].trans0[0].w	= 0.f;

		// 3D
		// Z in [Zmax->0] from [-1,+1]

		DpyManager::raster[i].scale0[1].x	= + DpyManager::ooProjNormalisedXY /*+ vp_w/2*/;
		DpyManager::raster[i].scale0[1].y	= - DpyManager::ooProjNormalisedXY /*+ vp_h/2*/;
		DpyManager::raster[i].scale0[1].z	= - DpyManager::zbuffMax * 0.5f;
		DpyManager::raster[i].scale0[1].w	= 0.f;

		DpyManager::raster[i].trans0[1].x	= ITOF4_pack + rx0 /*+ (vp_x + vp_w/2)*/;
		DpyManager::raster[i].trans0[1].y	= ITOF4_pack + ry0 /*+ (vp_y + vp_h/2)*/;
		DpyManager::raster[i].trans0[1].z	= ITOF4_pack + DpyManager::zbuffMax * 0.5f;
		DpyManager::raster[i].trans0[1].w	= 0.f;
	}

	return ( offBAddr + offBSize );
}




