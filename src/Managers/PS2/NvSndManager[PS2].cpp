/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvSndManager.h>
#include <Kernel/PS2/EE/EE.h>
#include <Kernel/PS2/IOP/IOP.h>
#include <Kernel/PS2/NvkSound[PS2].h>
using namespace nv;



//#define	TEST_FX
//#define	DBG_WAIT_PALYING



namespace
{

	// Dolby Pro-Logic II lookup table
	#include "../Common/DPL2.h"

	void
	GetNativeVolumes(	SndManager::OutputMode	inOMode,
						float					inVol,
						float					inPanAngle,
						uint&					outVolL,
						uint&					outVolR		)
	{
		// volume in [0,1]
		inVol = Clamp( inVol, 0.0f, 1.0f );

		// panning in [-Pi, Pi]
		int degAngle = int( inPanAngle * RadToDeg );
		while( degAngle < 0	   )	degAngle += 360;
		while( degAngle >= 360 )	degAngle -= 360;
		NV_ASSERT( degAngle >= 0   );
		NV_ASSERT( degAngle <  360 );
		float Lt = DPLII_CoeffTable[degAngle][0];
		float Rt = DPLII_CoeffTable[degAngle][1];

		float Lv=0, Rv=0;
		switch( inOMode ) {
			case SndManager::OM_MONO :
				Lv = Rv = inVol;
				break;
			case SndManager::OM_MONOPAN :
				Lv = Rv = inVol * Max( Absf(Lt), Absf(Rt) );
				break;
			case SndManager::OM_STEREO :
				Lv = inVol * Absf(Lt);
				Rv = inVol * Absf(Rt);
				break;
			case SndManager::OM_DPL2 :
				Lv = inVol * Lt;
				Rv = inVol * Rt;
				break;
		}
		outVolL = uint( 16383.0f * Absf(Lv) );
		outVolR = uint( 16383.0f * Absf(Rv) );
		if( Lv < 0.0f )
			outVolL = 0x7fff - outVolL;
		if( Rv < 0.0f )
			outVolR = 0x7fff - outVolR;
		//Printf( "VL:%f VR:%f\n", volL, volR );

		// ************ TEST DBPL2
	/*	if( inPanAngle == 0 ) {
			outVolL = 0x2000;
			outVolR = 0x2000;
		} else {
			outVolL = 0x2000;
			outVolR = 0x7fff - 0x2000;
		}
	*/	// **********************
	}


	struct Channel {
		int		sid;
		uint	pitch;
		float	pan;
		float	vol;
	};

	SndManager::StreamHandler	str_handler;
	Channel						chA[ iop::SD_NB_CHANNEL ];
	SndManager::OutputMode		omode;
	int							bgm_sid;


	inline int GetChSID( int inChannel )
	{
		return SndManager::ChIsPlaying(inChannel) ? chA[inChannel].sid : -1;
	}


	bool	iop_str_handler (	int				inSdId,
								float&			outFromTime,
								float&			outDuration		)
	{
		if( !str_handler )
			return FALSE;
		return (*str_handler)( inSdId, outFromTime, outDuration );
	}

}



bool
SndManager::Init	(			)
{
	omode = OM_STEREO;
	str_handler = NULL;
	iop::SetHandler( iop_str_handler );
	return TRUE;
}


bool
SndManager::Shut	(			)
{
	return TRUE;
}


void
SndManager::Update	(			)
{
	//
}


SndManager::StreamHandler
SndManager::SetHandler		(	StreamHandler		inHandler		)
{
	SndManager::StreamHandler old_handler = str_handler;
	str_handler = inHandler;
	return old_handler;
}


bool
SndManager::ProbeOutputMode		(	OutputMode			inOMode		)
{
//	if( inOMode == OM_DPL2 )
//		return FALSE;		// DPL2 TODO !!
	return TRUE;
}


void
SndManager::SetOutputMode		(	OutputMode			inOMode		)
{
	if( ProbeOutputMode(inOMode) )
		omode = inOMode;
}


SndManager::OutputMode
SndManager::GetOutputMode		(							)
{
	return omode;
}


void
SndManager::SetMasterVolume		(	float		inVol		)
{
	uint vol = uint( 16383.0f * Min( Absf(inVol), 1.0f ) );
	iop::SdSet( SD_P_MVOLL,  SD_CORE_0,	vol	);
	iop::SdSet( SD_P_MVOLR,  SD_CORE_0,	vol	);
	iop::SdSet( SD_P_MVOLL,  SD_CORE_1,	vol	);
	iop::SdSet( SD_P_MVOLR,  SD_CORE_1,	vol	);
}



int
SndManager::SdGetNbMax			(				)
{
	return iop::SD_NB_SOUND_MAX;
}


int
SndManager::SdAny		(					)
{
	return iop::SdAny();
}


SndManager::SdState
SndManager::SdGetState			(	int		inSoundId	)
{
	return SdState( iop::SdGetState(inSoundId) );
}


int
SndManager::SdLock		(	int				inSdId,
							NvSound*		inSndObj,
							float			inFromTime,
							float			inDuration	)
{
	if( !inSndObj )
		return -1;

	int sid = (inSdId==SD_ANY) ? SdAlloc(SD_ANY) : inSdId;
	if( sid < 0 )
		return -1;

	NvkSound* ksnd = inSndObj->GetKInterface();
	if( !ksnd )
		return -1;

	uint32 snd_boffset, snd_bsize;
	ksnd->GetBFData( snd_boffset, snd_bsize );

	bool done = iop::SdLock( 	sid,
								ksnd->GetMode(),
								ksnd->GetFreq(),
								snd_boffset,
								snd_bsize,
								ksnd->GetDuration(),
								inFromTime,
								inDuration	);

	if( done ) {
	//	DebugPrintf( "SndMan::SdLock: ksnd:%xh sid:%d\n", uint32(ksnd), sid );
		return sid;
	}

	if( inSdId == SD_ANY )
		SdFree( sid );
	return -1;
}


bool
SndManager::SdUnlock	(	int			inSoundId		)
{
	return iop::SdUnlock( inSoundId );
}


bool
SndManager::SdBreak		(	int			inSoundId		)
{
	return iop::SdBreak( inSoundId );
}


void
SndManager::SdFree		(	int			inSoundId		)
{
	iop::SdFree( inSoundId );
}


void
SndManager::SdFreeAll		(						)
{
	for( ;; ) {
		bool remain = FALSE;
		for( int i = 0 ; i < iop::SD_NB_SOUND_MAX ; i++ ) {
			iop::SdFree( i );
			if( iop::SdGetState(i) != iop::SD_UNUSED )
				remain = TRUE;
		}
		if( !remain )	break;
		core::Update();
	}
}


int
SndManager::SdAlloc			(		int		inSoundId		)
{
	if( inSoundId == SD_ANY )
		inSoundId = SdAny();
	return iop::SdAlloc( inSoundId );
}





int
SndManager::ChGetNb		(						)
{
	return iop::SD_NB_CHANNEL;
}


int
SndManager::ChAny	(		)
{
	uint64 envx = iop::SdGetENVX();
	uint64 chm  = 1UL;
	for( int i = 0 ; i < iop::SD_NB_CHANNEL ; i++ ) {
		if( (chm&envx)==0 )
			return i;
		chm <<= 1;
	}
	return -1;
}


SndManager::ChState
SndManager::ChGetState	(	int		inChannel		)
{
	// check in valid ch range
	if( inChannel<0 || inChannel>=iop::SD_NB_CHANNEL )
		return CH_IDLE;
	return iop::SdIsPlaying( inChannel ) ? CH_PLAYING : CH_IDLE;
}


int
SndManager::ChPlay		(	int			inChannel,
							int			inSoundId,
							float		inVol,
							float		inPanAngle,
							float		inPitchFactor	)
{
	// not music ?
	if( iop::SdGetMode(inSoundId) == 2 )
		return -1;

	int ch = (inChannel==CH_ANY) ? ChAny() : inChannel;
	if( ch<0 || ch>=iop::SD_NB_CHANNEL || ChIsPlaying(ch) )
		return -1;

	int snd_freq  = iop::SdGetBaseFreq( inSoundId );
	int ps2_pitch = int( float(snd_freq) * inPitchFactor * 4096.0f / 48000.0f );
	uint ps2_volL;
	uint ps2_volR;
	GetNativeVolumes( omode, inVol, inPanAngle, ps2_volL, ps2_volR );
	if( !iop::SdStart(inSoundId,ch,ps2_pitch,ps2_volL,ps2_volR) )
		return -1;

	chA[ ch ].sid   = inSoundId;
	chA[ ch ].pitch = ps2_pitch;
	chA[ ch ].pan   = inPanAngle;
	chA[ ch ].vol   = inVol;
	//DebugPrintf( "SndMan::ChPlay: sid:%d cid:%d\n", inSoundId, ch );
	#ifdef DBG_WAIT_PALYING
	ChWaitPlaying( ch );
	#endif
	return ch;
}


bool
SndManager::ChStop		(	int			inChannel		)
{
	int sid = GetChSID( inChannel );
	//DebugPrintf( "SndMan::ChStop: sid:%d cid:%d\n", sid, inChannel );
	if( sid < 0 )
		return FALSE;
	return iop::SdStop( sid, inChannel );
}


void
SndManager::ChStopAll	(			)
{
	for( int i = 0 ; i < iop::SD_NB_CHANNEL ; i++ )
		ChStop( i );
}


bool
SndManager::ChPause			(	int			inChannel		)
{
	int sid = GetChSID( inChannel );
	if( sid < 0 )
		return FALSE;
	return iop::SdSetPitch( sid, inChannel, 0 );
}


bool
SndManager::ChResume		(	int			inChannel		)
{
	int sid = GetChSID( inChannel );
	if( sid < 0 )
		return FALSE;
	return iop::SdSetPitch( sid, inChannel, chA[inChannel].pitch );
}


bool
SndManager::ChSetVolume		(	int			inChannel,
								float		inVol			)
{
	int sid = GetChSID( inChannel );
	if( sid < 0 )
		return FALSE;
	uint ps2_volL;
	uint ps2_volR;
	GetNativeVolumes( omode, inVol, chA[inChannel].pan, ps2_volL, ps2_volR );
	if( !iop::SdSetVolume(sid,inChannel,ps2_volL,ps2_volR) )
		return FALSE;
	chA[ inChannel ].vol = inVol;
	return TRUE;
}


bool
SndManager::ChSetPanning	(	int			inChannel,
								float		inAngle			)
{
	int sid = GetChSID( inChannel );
	if( sid < 0 )
		return FALSE;
	uint ps2_volL;
	uint ps2_volR;
	GetNativeVolumes( omode, chA[inChannel].vol, inAngle, ps2_volL, ps2_volR );
	if( !iop::SdSetVolume(sid,inChannel,ps2_volL,ps2_volR) )
		return FALSE;
	chA[inChannel].pan = inAngle;
	return TRUE;
}


bool
SndManager::ChSetPitchFactor	(	int			inChannel,
									float		inFactor	)
{
	int sid = GetChSID( inChannel );
	if( sid < 0 )
		return FALSE;
	int snd_freq  = iop::SdGetBaseFreq( sid );
	int ps2_pitch = int( float(snd_freq) * inFactor * 4096.0f / 48000.0f );
	if( !iop::SdSetPitch(sid,inChannel,ps2_pitch) )
		return FALSE;
	chA[ inChannel ].pitch = ps2_pitch;
	return TRUE;
}






bool
SndManager::BgmPlay		(	int			inSoundId,
							float		inVol		)
{
	if( BgmIsPlaying() )
		return FALSE;

	// streamed snd as BGM ?
	if( iop::SdGetMode(inSoundId) == 0 )
		return FALSE;

	int snd_freq  = iop::SdGetBaseFreq( inSoundId );
	int ps2_pitch = int( float(snd_freq) * 4096.0f / 48000.0f );
	uint ps2_volL;
	uint ps2_volR;
	GetNativeVolumes( omode, inVol, 0.f, ps2_volL, ps2_volR );
	if( !iop::SdStart(inSoundId,iop::SD_BGM_CHANNEL,ps2_pitch,ps2_volL,ps2_volR) )
		return -1;
	bgm_sid = inSoundId;
	//DebugPrintf( "SndMan::BgmPlay: sid:%d\n", inSoundId );
	#ifdef DBG_WAIT_PALYING
	BgmWaitPlaying();
	#endif
	return TRUE;
}


bool
SndManager::BgmStop		(			)
{
	//DebugPrintf( "SndMan::BgmStop: (enter)\n" );
	if( !BgmIsPlaying() )
		return FALSE;
	//DebugPrintf( "SndMan::BgmStop: Done\n" );
	return iop::SdStop( bgm_sid, iop::SD_BGM_CHANNEL );
}


bool
SndManager::BgmPause	(			)
{
	if( !BgmIsPlaying() )
		return FALSE;
	return iop::SdSetPitch( bgm_sid, iop::SD_BGM_CHANNEL, 0 );
}


bool
SndManager::BgmResume	(			)
{
	if( !BgmIsPlaying() )
		return FALSE;
	int snd_freq  = iop::SdGetBaseFreq( bgm_sid );
	int ps2_pitch = int( float(snd_freq) * 4096.0f / 48000.0f );
	return iop::SdSetPitch( bgm_sid, iop::SD_BGM_CHANNEL, ps2_pitch );
}


bool
SndManager::BgmBreak	(			)
{
	if( !BgmIsPlaying() )
		return FALSE;
	return iop::SdBreak( bgm_sid );
}


bool
SndManager::BgmSetVolume	(	float		inVol		)
{
	if( !BgmIsPlaying() )
		return FALSE;
	uint ps2_volL;
	uint ps2_volR;
	GetNativeVolumes( omode, inVol, 0.f, ps2_volL, ps2_volR );
	return iop::SdSetVolume( bgm_sid, iop::SD_BGM_CHANNEL, ps2_volL, ps2_volR );
}


bool
SndManager::BgmIsPlaying		(			)
{
	// mono-streamed (1 channel used) or music (2 channels used)
	return iop::SdIsPlaying(iop::SD_BGM_LCHANNEL) || iop::SdIsPlaying(iop::SD_BGM_RCHANNEL);
}





float
SndManager::MbToLinearVolume	(	float		inMillibel		)
{
	// [-10000,0] -> [0,1]
	float mb = Clamp( inMillibel, -10000.0f, 0.0f );
	if( mb == -10000.0f )
		return 0.0f;
	else {
		float lin = Powf( 10.0f, mb*(1.0f/2000.0f) );
		return Clamp( lin, 0.0f, 1.0f );
	}
}


float
SndManager::LinearToMbVolume	(	float		inLinear		)
{
	// [0,1] -> [-10000,0]
	float lin = Clamp( inLinear, 0.0f, 1.0f );
	if( lin == 0.0f )
		return -10000.0f;
	else {
		int mb = int( Log10f(lin) * 2000.0f );
		return Clamp( mb, -10000, 0 );
	}
}



