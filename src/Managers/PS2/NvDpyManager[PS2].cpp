/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvShader.h>
#include <Kernel/PS2/NvDpyManager[PS2].h>
#include <Kernel/PS2/NvDpyObject[PS2].h>
#include <Kernel/PS2/NvDpyState[PS2].h>
using namespace nv;


#define	SESSION_NB		256


/*
#ifdef _NVCOMP_ENABLE_DBG
#define	ENABLE_ASYNC_LOG
#endif
*/


namespace
{
	#include "NvDpyManager[PS2]_Tools.h"

	bool									presentFrame;
	bool									frameOpened;
	bool									frameSynced;
	uint									frameNo = ~0U;
	uint									flushCpt;
	uint									flushingFrameNo;
	sysvector<NvDpyObject_PS2*>				sessionA[SESSION_NB];
	uint									ctxtCurSession;

	NvDpyObject_PS2*						frameBreakBefore;
	NvDpyObject_PS2*						frameBreakAfter;

	frame::Packet							initFramePkt;
	frame::Packet							presentFramePkt;
	gs::frame::PresentMod					presentMod;

	sysvector<NvInterface*>					garbage0;
	sysvector<NvInterface*>					garbage1;
	sysvector<NvInterface*>	*				garbager;
}



DpyManager::CoordinateSystem	DpyManager::coordSystem;
DpyManager::Raster				DpyManager::raster[DPYR_OFF_FRAME_C32Z32+1];
float							DpyManager::rasterMaxWidth;
float							DpyManager::rasterMaxHeight;
float							DpyManager::projNormalisedXY;
float							DpyManager::ooProjNormalisedXY;
Matrix							DpyManager::clipMatrix;
NvDpyContext*					DpyManager::context;




bool
DpyManager::Init	(	CoordinateSystem	inCoordinateSystem	)
{
	garbage0.Init();
	garbage1.Init();
	garbager = &garbage0;

	context = NvEngineNew( NvDpyContext );
	context->Init();
	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].Init();

	coordSystem		= inCoordinateSystem;
	frameBreakBefore= NULL;
	frameBreakAfter = NULL;
	presentFrame	= FALSE;
	frameNo			= 0;
	frameOpened		= FALSE;
	frameSynced		= TRUE;
	flushingFrameNo = 0;
	flushCpt		= 0;

	gs::Init( FALSE );		// disable CRTC !
	uint32 offramBaseBAddr = gs::frame::VRamLowerBAddr;
	NV_ASSERTC( offramBaseBAddr<(512*32), "DpyManager: VRAM overflow !" );
	uint32 tramBaseBAddr   = InitRasters( offramBaseBAddr );
	NV_ASSERTC( tramBaseBAddr<(512*32), "DpyManager: VRAM overflow !" );
	tram::Init( (tramBaseBAddr+31)>>5 );
	frame::Init();
	ipu::Init();


#ifndef _MASTER
	Printf( "<Neova> VRAM mapping (block-unit):\n" );
	Printf( "<Neova> in-frame   : %d - %d [%dKo]\n", 0, offramBaseBAddr, offramBaseBAddr/4 );
	Printf( "<Neova> off-frame  : %d - %d [%dKo]\n", offramBaseBAddr, tramBaseBAddr, (tramBaseBAddr-offramBaseBAddr)/4 );
	Printf( "<Neova> tex-ram    : %d - %d [2*%dKo]\n", tramBaseBAddr, gs::frame::VRamUpperBAddr, (gs::frame::VRamUpperBAddr-tramBaseBAddr)/4/2 );
	Printf( "<Neova> raster-max : %d x %d pixels\n", int(rasterMaxWidth), int(rasterMaxHeight) );
#endif


	//
	//	Frame pkts

	{
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( DC );
		pvoid dc0 = DC->vd;

		// Frame init cmds
		{
			initFramePkt.Init();
			initFramePkt.SetSeparator( TRUE );
			initFramePkt.tadr = DC->i32;
			gs::FlushContext_CH1( DC, &(gs::frame::DefContext), gs::ALL_REGS, gs::CONTEXT1|gs::CONTEXT2 );
			initFramePkt.link = DC->i32;
			*(DC->i128++) = DMA_TAG_END;
			pvoid DCDup = DC->Dup( initFramePkt.tadr );
			uint32 dp = uint32(DCDup) - uint32( initFramePkt.tadr );
			initFramePkt.Translate( dp );
			DC->vd = dc0;
		}

		// Frame present cmds
		{
			presentFramePkt.Init();
			presentFramePkt.SetSeparator( TRUE );
			presentFramePkt.tadr = DC->i32;
			gs::frame::Present_CH1( DC, presentMod );
			presentFramePkt.link = DC->i32;
			*(DC->i128++) = DMA_TAG_END;
			pvoid DCDup = DC->Dup( presentFramePkt.tadr );
			uint32 dp = uint32(DCDup) - uint32( presentFramePkt.tadr );
			presentFramePkt.Translate( dp );
			presentMod.Translate( dp );
			DC->vd = dc0;
		}
	}

	return TRUE;
}


bool
DpyManager::Shut	(				)
{
	SyncFrame();

	while( garbager->size() ) {
		frameNo++;
		FlushGarbager();
	}

	SafeFree( initFramePkt.tadr );
	SafeFree( presentFramePkt.tadr );

	garbage0.Shut();
	garbage1.Shut();
	garbager = NULL;

	context->Shut();
	NvEngineDelete( context );

	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].Shut();

	frame::Shut();
	ipu::Shut();
	tram::Shut();
	gs::Shut();
	return TRUE;
}



DpyManager::CoordinateSystem
DpyManager::GetCoordinateSystem	(				)
{
	return coordSystem;
}


uint
DpyManager::GetPhysicalWidth	(				)
{
	return gs::frame::PhysicalSizeX;
}


uint
DpyManager::GetPhysicalHeight	(				)
{
	return gs::frame::PhysicalSizeY;
}


float
DpyManager::GetPhysicalAspect	(				)
{
	float w = float( int(gs::frame::PhysicalSizeX) );
	float h = float( int(gs::frame::PhysicalSizeY) );
	return w/h;
}


uint
DpyManager::GetVirtualWidth		(				)
{
	return gs::frame::VirtualSizeX;
}


uint
DpyManager::GetVirtualHeight	(				)
{
	return gs::frame::VirtualSizeY;
}


float
DpyManager::GetVirtualAspect	(				)
{
	float w = float( int(gs::frame::VirtualSizeX) );
	float h = float( int(gs::frame::VirtualSizeY) );
	return w/h;
}


uint
DpyManager::GetRasterWidth	(	DpyRaster	inRaster	)
{
	return raster[ int(inRaster) ].width;
}


uint
DpyManager::GetRasterHeight	(	DpyRaster	inRaster	)
{
	return raster[ int(inRaster) ].height;
}


uint
DpyManager::GetVSyncRate	(			)
{
	return gs::crtc::vsyncRate;
}


uint
DpyManager::GetLatency		(			)
{
	return 2;
}


uint
DpyManager::GetViewingLatency	(		)
{
	return 2;
}


bool
DpyManager::TranslateCRTC		(	int		inX0,
									int		inY0	)
{
	gs::crtc::Translate( inX0, inY0 );
	return TRUE;
}


bool
DpyManager::ReadFrame	(	pvoid			outBuffer	)
{
	// Defined and QW aligned !
	if( !outBuffer || uint32(outBuffer)&15 ) {
		NV_DEBUG_WARNING( "DpyManager::ReadFrame() : outBuffer must be 16bytes aligned !" );
		return FALSE;
	}

	SyncFrame();

	gs::frame::GetBack_CH1(	(uint128*)outBuffer	);
	return TRUE;
}


bool
DpyManager::WriteFrame	(	pvoid		inBuffer	)
{
	// Defined and QW aligned !
	if( !inBuffer || uint32(inBuffer)&15 ) {
		NV_DEBUG_WARNING( "DpyManager::WriteFrame() : inBuffer must be 16bytes aligned !" );
		return FALSE;
	}

	SyncFrame();

	bool consoleOnly = FALSE;
	core::GetParameter( core::PR_CONSOLE_ONLY, &consoleOnly );
	if( !consoleOnly ) {
		dmac::Cursor* DC = dmac::GetFrontHeapCursor();
		uint8* DCStart	= DC->i8;
		gs::frame::PutBack32_CH1( DC, (uint128*)inBuffer );
		DC->i128[0] = DMA_TAG_END;
		NV_ASSERT_DC( DC );
		DC->i8 = DCStart;
	
		dmac::Sync( dmac::CH1_VIF1_M | dmac::CH2_GIF_M );
		FlushCache( WRITEBACK_DCACHE );
		dmac::Start_CH1( DCStart, FALSE );
		dmac::Sync( dmac::CH1_VIF1_M | dmac::CH2_GIF_M );
	}

	return TRUE;
}


bool
DpyManager::SaveFrame	(	pcstr		inFilename		)
{
#ifdef _MASTER
	return FALSE;
#else
	SyncFrame();

	if( !inFilename || (*inFilename)==0 )
		return FALSE;

	uint bsize = gs::frame::SizeX * gs::frame::SizeY * 4;

	pvoid  mem = EngineMalloc( 256+bsize );
	uint8* bmp = ((uint8*)mem) + 256;
	uint8* hdr = bmp - 18;

	if( !ReadFrame(bmp) ) {
		EngineFree( mem );
		return FALSE;
	}

	// ABGR to ARGB
	uint8* end_bmp = bmp + bsize;
	while( bmp < end_bmp ) {
		uint32 ABGR = *((uint32*)bmp);
		bmp[0] = (ABGR >> 16) & 0xFF;		// B
		bmp[1] = (ABGR >>  8) & 0xFF;		// G
		bmp[2] = (ABGR >>  0) & 0xFF;		// R
		bmp[3] = (ABGR >> 24) & 0xFF;		// A
		bmp += 4;
	}

	Zero( hdr, 18 );
	hdr[2] =2;						// Image type code
	hdr[12]=gs::frame::SizeX;		// low width of image
	hdr[13]=gs::frame::SizeX>>8;	// high width of image
	hdr[14]=gs::frame::SizeY;		// low height of image
	hdr[15]=gs::frame::SizeY>>8;	// high height of image
	hdr[16]=32;						// Image pixel size
	hdr[17]=32;						// Image descriptor byte

	bool res = nv::file::DumpToFile( inFilename, hdr, 18+bsize );
	EngineFree( mem );
	return res;
#endif
}


uint
DpyManager::GetFrameNo		(		)
{
	return frameNo;
}


Psm
DpyManager::GetFramePSM		(		)
{
	return PSM_ABGR32;
}


bool
DpyManager::BeginFrame		(		)
{
	NV_ASSERTC( !frameOpened, "Frame is already opened !" );
	if( frameOpened )
		return FALSE;

	// reset session context
	for( uint i = 0 ; i < SESSION_NB ; i++ )
		sessionA[i].clear();
	ctxtCurSession = 0;

	// reset context stacks
	Vec4 lightcolor( 1, 1, 1, 1 );
	Vec2 clipRange( 1.0f, 500.0f );
	Vec4 viewport( 0, 0, gs::frame::SizeX, gs::frame::SizeY );
	context->Reset();
	context->SetLight( 0, &lightcolor, NULL );
	context->SetLight( 1, &lightcolor, (Vec3*)&Vec3::UNIT_X );
	context->SetLight( 2, &lightcolor, (Vec3*)&Vec3::UNIT_Y );
	context->SetView( NULL, NULL, &clipRange, &viewport );
	context->SetTarget( DpyTarget(DPYR_IN_FRAME) );

	// setup new frame
	frameOpened = TRUE;
	frameNo++;

	return TRUE;
}


bool
DpyManager::EndFrame	(		)
{
	NV_ASSERTC( frameOpened, "Frame is not opened !" );
	if( !frameOpened )
		return FALSE;
	frameOpened = FALSE;
	return TRUE;
}



namespace
{
	float		perf_GPU_FPS	= 0;
	float		perf_CPU_FPS	= 0;
	uint32		perf_drawCpt	= 0;
	uint32		perf_triCpt		= 0;
	uint32		perf_locCpt		= 0;
	uint32		perf_vtxCpt 	= 0;
	uint32		perf_primCpt	= 0;
	uint32		perf_texBSize	= 0;
}


bool
DpyManager::FlushFrame	(	bool	inPresent,
							bool	inGetPerf	)
{
	NV_ASSERTC( !frameOpened, "Frame must not be opened !" );
	NV_ASSERTC( flushingFrameNo != frameNo, "Frame has been already flushed !" );
	if( frameOpened || flushingFrameNo==frameNo )
		return FALSE;

	#ifdef ENABLE_ASYNC_LOG
	{
		static bool async_output = FALSE;
		ctrl::Status* c = ctrl::GetStatus( 0 );
		if( 	c
			&&	c->button[ctrl::BT_BUTTON4] > 0.5f
			&&	c->button[ctrl::BT_BUTTON5] > 0.5f
			&&	c->button[ctrl::BT_BUTTON6] > 0.5f
			&&	c->button[ctrl::BT_BUTTON7] > 0.5f	)
		{
			if( !async_output )
				core::SetParameter( core::PR_ASYNC_LOG, TRUE );
			async_output = TRUE;
		}
		else
		{
			async_output = FALSE;
		}
	}
	#endif


	bool   async, vsync;
	uint32 vmode;
	core::GetParameter( core::PR_EE_VMODE,	 &vmode );
	core::GetParameter( core::PR_ASYNC_DRAW, &async );
	core::GetParameter( core::PR_VSYNC_DRAW, &vsync );

	// swap frame dmac heaps
	dmac::SwapHeap();
	#ifdef _NVCOMP_ENABLE_DBG
	{
		dmac::Cursor* curC = dmac::GetFrontHeapCursor();
		NV_ASSERT_DC( curC );
		uint32* curP	 = curC->i32;
		uint32* curP_end = curP + (curC->GetLeftBSize()>>2);
		while( curP < curP_end )
			*curP++ = 0x6c100000;
	}
	#endif

	// begin frame
	gs::crtc::SetBGColor( 0x0000FF );
	frame::Begin();

	// init frame
	if( presentFrame )
		frame::Push( &presentFramePkt );
	presentFrame = inPresent;
	frame::Push( &initFramePkt );

	// Draw
	for( uint s = 0 ; s < SESSION_NB ; s++ ) {
		uint N = sessionA[s].size();
		if( N == 0 )	continue;
		NvDpyObject_PS2	** dpyObjP		= sessionA[s].data();
		NvDpyObject_PS2	** dpyObj_endP	= dpyObjP + N;
		while( dpyObjP != dpyObj_endP ) {
			NV_ASSERT( *dpyObjP );
			if( *dpyObjP == frameBreakBefore )	goto dpy_frame_break;
			(*dpyObjP)->Draw();
			if( *dpyObjP == frameBreakAfter )	goto dpy_frame_break;
			dpyObjP++;
		}
	}
dpy_frame_break:

	// end frame
	frame::End();

	// sync frame
	gs::crtc::SetBGColor( 0x51381E );
	frame::Sync();
	frameSynced		= TRUE;
	flushingFrameNo	= frameNo;

	// Fix interlace one pixel offset
	if( !(vmode & gs::VM_FIELD) ) {
		// Interlace FRAME mode needs 1 line offset for odd frames !
		// displayed    | EVEN  | ODD
		// [VSYNC]      |       |
		// present      | ODD   | EVEN
		// compute back | EVEN  | ODD
		if( gs::crtc::IsEvenFrame() )	presentMod.SetOddFrame();
		else							presentMod.SetEvenFrame();
	}

	// vsync & execute DMA
	frame::Flush( async, vsync );
	frameSynced	= FALSE;

	// Get performances ?
	if( inGetPerf ) {
		frame::WaitDMA();
		float dmaTime	= frame::GetPerformance( frame::PERF_DMA );
		float cpuTime	= frame::GetPerformance( frame::PERF_BEGIN_BEGIN )
						- frame::GetPerformance( frame::PERF_SYNC_UPDATE_IDLE )
						- frame::GetPerformance( frame::PERF_VSYNC_IDLE )
						- frame::GetPerformance( frame::PERF_WAIT_DMA );
		perf_GPU_FPS	= dmaTime ? (1.0f/dmaTime) : 0.f;
		perf_CPU_FPS	= cpuTime ? (1.0f/cpuTime) : 0.f;
		perf_texBSize	= int( frame::GetPerformance( frame::PERF_TRXD_TEXBSIZE ) );
		perf_drawCpt	= 0;
		perf_triCpt		= 0;
		perf_locCpt		= 0;
		perf_vtxCpt		= 0;
		perf_primCpt	= 0;
		for( uint s = 0 ; s < SESSION_NB ; s++ ) {
			uint N = sessionA[s].size();
			if( N == 0 )	continue;
			NvDpyObject_PS2	** dpyObjP		= sessionA[s].data();
			NvDpyObject_PS2	** dpyObj_endP	= dpyObjP + N;
			while( dpyObjP != dpyObj_endP ) {
				NV_ASSERT( *dpyObjP );
				uint32 objTriCpt=0, objLocCpt=0, objVtxCpt=0, objPrimCpt=0;
				if( (*dpyObjP)->GetPerformances(objTriCpt,objLocCpt,objVtxCpt,objPrimCpt) ) {
					perf_triCpt	 += objTriCpt;
					perf_locCpt	 += objLocCpt;
					perf_vtxCpt  += objVtxCpt;
					perf_primCpt += objPrimCpt;
				}
				perf_drawCpt += (*dpyObjP)->dpyctxt.chainCpt;
				dpyObjP++;
			}
		}
	}

	FlushGarbager();

	if( ++flushCpt == 2 )
		gs::crtc::Enable();

	return TRUE;
}


float
DpyManager::GetPerformance		(	Performance			inPerf		)
{
	if( inPerf == PERF_GPU_FPS )	return perf_GPU_FPS;
	if( inPerf == PERF_CPU_FPS )	return perf_CPU_FPS;
	if( inPerf == PERF_DRAW_CPT )	return float(int(perf_drawCpt));
	if( inPerf == PERF_TRI_CPT )	return float(int(perf_triCpt));
	if( inPerf == PERF_LOC_CPT )	return float(int(perf_locCpt));
	if( inPerf == PERF_VTX_CPT )	return float(int(perf_vtxCpt));
	if( inPerf == PERF_PRIM_CPT )	return float(int(perf_primCpt));
	if( inPerf == PERF_TEX_BSIZE )	return float(int(perf_texBSize));
	return 0.f;
}


void
DpyManager::SyncFrame	(			)
{
	NV_ASSERTC( !frameOpened, "Frame must not be opened !" );
	if( frameOpened )	return;

	if( !frameSynced ) {
		frame::Begin();
		frame::End();
		frame::Sync();
		frameSynced = TRUE;
	}

	FlushGarbager();
}


bool
DpyManager::IsInFrame	(	NvDpyObject_PS2*	inDpyObj	)
{
	if( !inDpyObj )
		return FALSE;
	return ( inDpyObj->dpyctxt.drawFrameNo == frameNo );
}


bool
DpyManager::IsInFrame	(	NvDpyObject*		inDpyObj	)
{
	return IsInFrame( inDpyObj ? (NvDpyObject_PS2*)inDpyObj->GetBase() : NULL );
}


bool
DpyManager::IsDrawing	(	uint32				inFrameNo	)
{
	// my frame is the current frame and is flushed ?
	if( inFrameNo==flushingFrameNo && inFrameNo==frameNo )
		return !frameSynced;
	else
		return ( inFrameNo==flushingFrameNo || inFrameNo==frameNo );
}


bool
DpyManager::IsDrawing	(	NvDpyObject_PS2*	inDpyObj	)
{
	if( !inDpyObj )
		return FALSE;
	return IsDrawing( inDpyObj->dpyctxt.drawFrameNo );
}


bool
DpyManager::IsDrawing	(	NvDpyObject*		inDpyObj	)
{
	return IsDrawing( inDpyObj ? (NvDpyObject_PS2*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetFrameBreakBefore	(	NvDpyObject_PS2*		inDpyObj	)
{
	frameBreakAfter  = NULL;
	frameBreakBefore = inDpyObj;
}


void
DpyManager::SetFrameBreakBefore	(	NvDpyObject*		inDpyObj	)
{
	SetFrameBreakBefore( inDpyObj ? (NvDpyObject_PS2*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetFrameBreakAfter	(	NvDpyObject_PS2*		inDpyObj	)
{
	frameBreakBefore = NULL;
	frameBreakAfter	 = inDpyObj;
}


void
DpyManager::SetFrameBreakAfter	(	NvDpyObject*		inDpyObj	)
{
	SetFrameBreakAfter( inDpyObj ? (NvDpyObject_PS2*)inDpyObj->GetBase() : NULL );
}


void
DpyManager::SetTarget	(	DpyTarget		inTarget	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetTarget( inTarget );
}


void
DpyManager::SetView		(	Matrix*			inViewTR,
							Matrix*			inProjTR,
							Vec2*			inClipRange,
							Vec4*			inViewport	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetView( inViewTR, inProjTR, inClipRange, inViewport );
}


void
DpyManager::SetSession	(	uint8			inSessionNo		)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	ctxtCurSession = (inSessionNo < SESSION_NB) ? inSessionNo : SESSION_NB-1;
}


void
DpyManager::SetWorldTR	(	Matrix*		inWorldTR	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetWorldTR( inWorldTR );
}


void
DpyManager::SetLight	(	uint		inLightNo,
							Vec4*		inColor,
							Vec3*		inDirection		)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return;
	context->SetLight( inLightNo, inColor, inDirection );
}


bool
DpyManager::Draw		(	NvDpyObject_PS2*	inDpyObj	)
{
	NV_ASSERTC( frameOpened, "Frame must be opened !" );
	if( !frameOpened )	return FALSE;

	if( !inDpyObj )
		return FALSE;

	// First draw in the current frame ?
	bool firstDraw = !IsInFrame( inDpyObj );

	int chainNext = (firstDraw ? -1 : inDpyObj->dpyctxt.chainHead );
	int chainHead = context->Push( chainNext );
	if( chainHead < 0 )
		return FALSE;

	inDpyObj->dpyctxt.chainHead = chainHead;
	inDpyObj->dpyctxt.chainCpt += 1;

	// Not already in the current draw queue ?
	if( firstDraw ) {
		NV_ASSERT( ctxtCurSession < SESSION_NB );
		sessionA[ ctxtCurSession ].push_back( inDpyObj );
		inDpyObj->dpyctxt.drawFrameNo = frameNo;
		inDpyObj->dpyctxt.chainCpt = 1;
	}

	return TRUE;
}


bool
DpyManager::Draw	(	NvDpyObject*		inDpyObj	)
{
	return Draw( inDpyObj ? (NvDpyObject_PS2*)inDpyObj->GetBase() : NULL );
}


float
DpyManager::GetZProjNormalised	(	float			inZ,
									Matrix*			inProjTR		)
{
	// Projected Z, in valid range [-1,+1]
	if( coordSystem == CS_RIGHT_HANDED )
		inZ = - inZ;	// LH/RH ?
	Vec4 xyzw( 0, 0, inZ, 1 );
	Vec4Apply( &xyzw, &xyzw, inProjTR );
	return (xyzw.w==0.0f) ? -1.0f : xyzw.z/xyzw.w;
}


uint32
DpyManager::GetZProjBuffValue	(	float			inZ,
									Matrix*			inProjTR		)
{
	// Projection in [-1,+1]
	float pnZ = GetZProjNormalised( inZ, inProjTR );
		  pnZ = Clamp( pnZ, -1.0f, 1.0f );

	// Z in [Zmax->0] from [-1,+1]
	const float ITOF4_pack = 524288.0F;					// 0x49000000
	float  fZ = - pnZ * (0.5f*zbuffMax) + (0.5f*zbuffMax) + ITOF4_pack;
	uint32 iZ = ( AS_UINT32(fZ) & 0x0FFFFFF0 ) >> 4;	// ITOF4_pack mantissa only - 4 bits lost with XYZF2 !
	return iZ;
}


void
DpyManager::GetVRamMapping	(		VRamBuffer		inBuffer,
									uint&			outBAddr,
									uint&			outPSM,
									uint&			outSizeX,
									uint&			outSizeY		)
{
	if( inBuffer == VRM_FRONT ) {
		sceGsFrame* frame = (sceGsFrame*) &gs::frame::FrontFrameReg;
		outBAddr = frame->FBP << 5;
		outPSM   = frame->PSM;
		outSizeX = gs::frame::PhysicalSizeX;
		outSizeY = gs::frame::PhysicalSizeY;
	}
	else if( inBuffer == VRM_FRAME ) {
		sceGsFrame* frame = (sceGsFrame*) &gs::frame::BackFrameReg;
		outBAddr = frame->FBP << 5;
		outPSM   = frame->PSM;
		outSizeX = gs::frame::SizeX;
		outSizeY = gs::frame::SizeY;
	}
	else if( inBuffer == VRM_DEPTH ) {
		sceGsFrame* frame = (sceGsFrame*) &gs::frame::DepthFrameReg;
		outBAddr = frame->FBP << 5;
		outPSM   = frame->PSM;
		outSizeX = gs::frame::SizeX;
		outSizeY = gs::frame::SizeY;
	}
	else if( inBuffer == VRM_OFF ) {
		sceGsFrame* frame = (sceGsFrame*) &raster[DPYR_OFF_FRAME_C32H].rgbaFrameReg;
		outBAddr = frame->FBP << 5;
		outPSM   = frame->PSM;
		outSizeX = raster[DPYR_OFF_FRAME_C32H].width;
		outSizeY = raster[DPYR_OFF_FRAME_C32H].height;
	}
	else {		//	VRM_TEX0
		outBAddr = tram::GetPStart() << 5;
		outPSM   = 0;	// ?
		outSizeX = 0;	// ?
		outSizeY = 0;	// ?
	}
}


bool
DpyManager::SetMipmapping		(	NvShader*			inShader,
									uint				inSurfaceIndex,
									int					inL,
									float				inK					)
{
	DpyState_PS2* dpyps2 = inShader ? (DpyState_PS2*)inShader->GetDisplayState(inSurfaceIndex) : NULL;
	if( !dpyps2 )
		return FALSE;
	dpyps2->SetMipmapping( inL, inK );
	return TRUE;
}


bool
DpyManager::GetPerformances		(	NvShader*			inShader,
									uint32&				outTriCpt,				// # triangles
									uint32&				outLocCpt,				// # original locations
									uint32&				outVtxWeldCpt,			// # welded vertex {components}
									uint32&				outVtxProcCpt		)	// # processed vertex
{
	NvDpyObject_PS2* dpyobj = (NvDpyObject_PS2*)inShader->GetBase();
	if( !dpyobj )
		return FALSE;
	return dpyobj->GetPerformances(	outTriCpt, outLocCpt, outVtxWeldCpt, outVtxProcCpt );
}


void
DpyManager::AddToGarbager( NvInterface*		inITF )
{
	NV_ASSERT( garbager );
	if( !inITF )
		return;

#ifdef _NVCOMP_ENABLE_DBG
	if( garbager->size() ) {
		NvInterface** itf	  = garbager->data();
		NvInterface** itf_end = itf + garbager->size();
		while( itf != itf_end ) {
			NV_ASSERT( *itf );
			NV_ASSERTC( (*itf)!=inITF, "Interface 0x%08x is already garbaging !" );
			itf++;
		}
	}
#endif

	garbager->push_back( inITF );
}


void
DpyManager::FlushGarbager	(	)
{
	NV_ASSERT( garbager );

	// Swap garbager before to allow Release() to call AddToGarbager() again !
	sysvector<NvInterface*>* _garbager = (garbager==&garbage0) ? &garbage1 : &garbage0;
	_garbager->clear();
	Swap( _garbager, garbager );

	// Release garbaging objets
	if( _garbager->size() ) {
		NvInterface** itf	  = _garbager->data();
		NvInterface** itf_end = itf + _garbager->size();
		while( itf != itf_end ) {
			NV_ASSERT( *itf );
			(*itf)->Release();		// Note that Release() can call AddToGarbager() again !
			itf++;
		}
	}
}



void
DpyManager::DrawImmediate	(	void* 				inTex					,		
														uint				inWidth					,
														uint				inHeight				,
														int					inOffsetX				,		
														int					inOffsetY				)
{
	
}