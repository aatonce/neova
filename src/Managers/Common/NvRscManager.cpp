/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvRscManager.h>
#include <Kernel/Common/NvRscFactory.h>
using namespace nv;



#define	NV_FACTORY_DECL( _RSC_CLASS )		bool rscfactory_##_RSC_CLASS##_Reg()
#define	NV_FACTORY_REG( _RSC_CLASS )		rscfactory_##_RSC_CLASS##_Reg()


NV_FACTORY_DECL( NvAnim			);
NV_FACTORY_DECL( NvSkelAnim		);
NV_FACTORY_DECL( NvSkeleton		);
NV_FACTORY_DECL( NvMorphTarget	);
NV_FACTORY_DECL( NvMorphAnim	);
NV_FACTORY_DECL( NvMesh			);
NV_FACTORY_DECL( NvBitmap		);
NV_FACTORY_DECL( NvMovie		);
NV_FACTORY_DECL( NvSound		);
NV_FACTORY_DECL( NvUID			);
NV_FACTORY_DECL( NvRawData		);




namespace
{

	char				bfPath[128];
	uint32				paramCacheBSize;
	uint32				paramReadBStartAlignMask;
	uint32				paramReadBSizeAlign;
	uint32				paramJumpMaxBSize;
	uint32				paramLogMaxSize;


	sysvector<RscManager::RscLog>	logRscA;
	bool							logRscOverflowed;


	struct TOC
	{
		uint32			uid;			// resource file UID
		uint32			type;			// extension CRC
		uint32			datBSsize;
		uint32			datBOffset[4];
		uint32			depCpt;
		uint32			depStart;
	};


	struct FactoryDesc
	{
		RscFactory	*	fac;
		uint32			type;
	};
	sysvector<FactoryDesc>	factoryA;


	struct ToLoadRsc
	{
		TOC*			toc;
		RscFactory *	fac;
		pvoid			prefPtr;
		uint32			prefBSize;
	};


	pvoid				tocData;
	TOC*				toc;
	uint				tocLen;
	uint32*				tocDep;

	file::ReadRequest	fileRR;

	uint				qSize;
	RscPrefetchQ		*qHead,
						*qTail;

	enum RPState		// update process states
	{
		RP_NONE,
		RP_CHECKING_TO_LOAD,
		RP_OPTIMISE_SORTING,
		RP_ALLOC_MEMORY,
		RP_READING_TO_CACHE,
		RP_MEMCPY_FROM_CACHE,
		RP_FACTORY_SETUP
	};

	RscPrefetchQ*			curRP;
	int32					curToLoadBSize;
	int32					curLoadedBSize;
	sysvector<ToLoadRsc>	curToLoadA;
	sysvector<uint32>		curToLoadBitA;
	RPState					curRPState;
	uint32					curRPStateCtx_index;
	file::ReadRequest		curRP_RR;
	pvoid					curRP_RCachePtr;
	uint32					curRP_RCacheBSize;
	uint32					curRP_RCacheBOffset;


	int
	FindUID (	uint32	inUID	)
	{
		if( !tocData || !tocLen )
			return -1;
		uint32 v;
		int i = 0,
			j = tocLen-1,
			k;
		while( j >= i ) {
			k = (i+j)>>1;
			v = toc[k].uid;

			if( v == inUID )
				return k;
			else if( i==j )
				return -1;		// not found

			if( v > inUID )
				j = k;
			else
				i = k+1;
		}
		return -1;
	}


	int
	FindType (	uint32	inType	)
	{
		if( factoryA.size() == 0 )
			return -1;
		uint32 v;
		int	i = 0,
			j = factoryA.size()-1,
			k;
		while( j >= i ) {
			k = (i+j)>>1;
			v = factoryA[k].type;

			if( v == inType )
				return k;
			else if( i==j )
				return -1;		// not found

			if( v > inType )
				j = k;
			else
				i = k+1;
		}
		return -1;
	}


	void
	CheckToLoadUID (	uint32		inUID	)
	{
		// Already checked ?
		uint32 inUID_b32 = inUID>>5;
		uint32 inUID_o32 = 1U << (inUID&0x1F);
		if( inUID_b32 >= curToLoadBitA.size() )
			curToLoadBitA.resize( 2*inUID_b32, 0U );
		if( curToLoadBitA[inUID_b32] & inUID_o32 )
			return;
		curToLoadBitA[ inUID_b32 ] |= inUID_o32;

		int tocIdx = FindUID( inUID	);
		if( tocIdx < 0 )
			return;			// Rsc not found in TOC !!?
		TOC * rscTOC = toc + tocIdx;

		// Get Factory for this rsc
		int facIdx = FindType( rscTOC->type );
		if( facIdx < 0 )
			return;			// Rsc Factory not found !!?
		RscFactory * fac = factoryA[facIdx].fac;

		// Available in factory ?
		if( fac->IsAvailableRsc(inUID) )
			return;

		// Check to load of dependencies UIDs
		uint32* depP   = tocDep + rscTOC->depStart;
		uint32  depCpt = rscTOC->depCpt;
		for( uint i = 0 ; i < depCpt ; i++ )
			CheckToLoadUID( depP[i] );

		ToLoadRsc toLoad;
		toLoad.toc	= rscTOC;
		toLoad.fac	= fac;

		curToLoadA.push_back( toLoad );
	}


	uint8 GetRRPriority	(	)
	{
		uint32 rrPri;
		core::GetParameter( core::PR_RSCMAN_RRPRIORITY, &rrPri );
		return Clamp( rrPri, 0U, 15U );
	}


	void InitTOC	(	)
	{
		// first uint32 is TOC [] bsize
		tocLen = ConvertLSB( *((uint32*)tocData) );
		toc	   = (TOC*)( uint32(tocData) + 4 );
		tocDep = (uint32*)( toc + tocLen );

		TOC* toc0 = toc;
		TOC* toc1 = (TOC*) tocDep;
		while( toc0 < toc1 ) {
			ConvertLSB( toc0->uid );
			ConvertLSB( toc0->type );
			ConvertLSB( toc0->datBSsize );
			ConvertLSB( toc0->datBOffset[0] );
			ConvertLSB( toc0->datBOffset[1] );
			ConvertLSB( toc0->datBOffset[2] );
			ConvertLSB( toc0->datBOffset[3] );
			uint32* dep0 = tocDep + ConvertLSB( toc0->depStart );
			uint32* dep1 = dep0   + ConvertLSB( toc0->depCpt );
			while( dep0 < dep1 )
				ConvertLSB( *dep0++ );
			toc0++;
		}
	}

}



bool
RscManager::Init	(		)
{
	logRscA.Init();
	factoryA.Init();
	curToLoadA.Init();
	curToLoadBitA.Init();

	bfPath[0]	= 0;
	tocData		= NULL;
	toc			= NULL;
	tocLen		= 0;
	tocDep		= NULL;
	qSize		= 0;
	qHead		= NULL;
	qTail		= NULL;
	curRP		= NULL;
	curRPState	= RP_NONE;
	curToLoadA.clear();
	curToLoadBitA.resize( 256>>5 );

	NV_FACTORY_REG( NvAnim			);
	NV_FACTORY_REG( NvSkelAnim		);
	NV_FACTORY_REG( NvSkeleton		);
	NV_FACTORY_REG( NvMorphTarget	);
	NV_FACTORY_REG( NvMorphAnim		);
	NV_FACTORY_REG( NvMesh			);
	NV_FACTORY_REG( NvBitmap		);
	NV_FACTORY_REG( NvMovie			);
	NV_FACTORY_REG( NvSound			);
	NV_FACTORY_REG( NvUID			);
	NV_FACTORY_REG( NvRawData		);

	return TRUE;
}


bool
RscManager::Shut	(		)
{
	CloseBigFile();

	for( uint i = 0 ; i < factoryA.size() ; i++ )
		factoryA[i].fac->RegisterShut();

	factoryA.Shut();
	logRscA.Shut();
	curToLoadA.Shut();
	curToLoadBitA.Shut();

	return TRUE;
}


bool
RscManager::OpenBigFile		(	pcstr		inPath		)
{
	if( !inPath || GetBigFile() )
		return FALSE;

	bool res;

	//
	// Load .TOC

	Strcpy( bfPath, inPath );
	Strcat( bfPath, ".TOC" );
	res = file::Open( bfPath );
	if( !res ) {
		Printf( "<Neova> RscManager: Open .TOC fails [BF=\"%s\"] !\n", inPath );
		bfPath[0] = 0;
		return FALSE;
	}

	uint32 bSize   = file::GetSize();
	uint32 bSize32 = Round32( bSize );
	tocData = NvMallocA( Round128(bSize32), NVHW_IO_BALIGN );
	if( !tocData ) {
		Printf( "<Neova> RscManager: Not enough memory [BF=\"%s\"] !\n", inPath );
		file::Close();
		bfPath[0] = 0;
		return FALSE;
	}
	fileRR.Setup( tocData, bSize32, 0 );
	res = file::AddReadRequest( &fileRR, GetRRPriority() );
	if( res ) {
		while( !fileRR.IsReady() )
			core::Update();
	}
	if( !res || fileRR.GetState() != file::ReadRequest::COMPLETED ) {
		Printf( "<Neova> RscManager: Read .TOC fails [BF=\"%s\"] !\n", inPath );
		if( !res )
			Printf( "<Neova> RscManager: file::AddReadRequest() error !\n" );
		else
			Printf( "<Neova> RscManager: file::RR error !\n" );
		file::Close();
		EngineFree( tocData );
		tocData = NULL;
		bfPath[0] = 0;
		return FALSE;
	}
	file::Close();

	InitTOC();


	//
	// Open .DAT

	Strcpy( bfPath, inPath );
	Strcat( bfPath, ".DAT" );
	res = file::Open( bfPath );
	if( !res ) {
		Printf( "<Neova> RscManager: .DAT is missing [BF=\"%s\"] !\n", inPath );
		file::Close();
		EngineFree( tocData );
		tocData = NULL;
		bfPath[0] = 0;
		return FALSE;
	}


	Strcpy( bfPath, inPath );


	//
	// Get RscMan parameters

	nv::core::GetParameter( nv::core::PR_RSCMAN_CACHEBSIZE, &paramCacheBSize );
	nv::core::GetParameter( nv::core::PR_RSCMAN_JUMP_MAXBSIZE, &paramJumpMaxBSize );
	nv::core::GetParameter( nv::core::PR_RSCMAN_READ_BSTART_ALIGN, &paramReadBStartAlignMask );
	nv::core::GetParameter( nv::core::PR_RSCMAN_READ_BSIZE_ALIGN, &paramReadBSizeAlign );
	if( paramReadBStartAlignMask )
		paramReadBStartAlignMask = ~( paramReadBStartAlignMask - 1);
	else
		paramReadBStartAlignMask = ~0U;

	uint32 logMaxBSize;
	nv::core::GetParameter( nv::core::PR_RSCMAN_LOG_MAXBSIZE, &logMaxBSize );
	paramLogMaxSize = logMaxBSize / sizeof(RscLog);
	logRscA.free();
	logRscOverflowed = FALSE;

	Printf( "<Neova> RscManager: BF Opened [BF=\"%s\"].\n", inPath );
	return TRUE;
}



bool
RscManager::CloseBigFile	(		)
{
	// Not in use ?
	if( !GetBigFile() )
		return FALSE;

	if( qSize )
		return FALSE;

	logRscA.free();

	file::Close();
	EngineFree( tocData );
	tocData = NULL;
	bfPath[0] = 0;
	qSize = 0;
	qHead = NULL;
	qTail = NULL;
	curRP = NULL;
	return TRUE;
}



pcstr
RscManager::GetBigFile	(	)
{
	if( bfPath[0] == 0 )
		return NULL;
	else
		return (pcstr)bfPath;
}


uint32
RscManager::GetRscCpt		(			)
{
	// Not in use ?
	if( !GetBigFile() )
		return 0;
	return tocLen;
}


uint32
RscManager::GetRscUID		(	uint			inNo		)
{
	// Not in use ?
	if( !GetBigFile() )
		return 0;

	if( inNo >= tocLen )
		return 0;
	else
		return toc[ inNo ].uid;
}


bool
RscManager::GetRscInfo		(	uint32			inUID,
								RscInfo	*		outInfo		)
{
	// Not in use ?
	if( !GetBigFile() )
		return FALSE;

	if( inUID == 0 )
		return FALSE;

	int idx = FindUID( inUID );
	if( idx < 0 )
		return FALSE;

	if( outInfo )
	{
		TOC * rscTOC = toc + idx;
		outInfo->uid		= rscTOC->uid;
		outInfo->type		= rscTOC->type;
		outInfo->bsize		= rscTOC->datBSsize;
		outInfo->boffset[0]	= rscTOC->datBOffset[0];
		outInfo->boffset[1] = rscTOC->datBOffset[1];
		outInfo->boffset[2] = rscTOC->datBOffset[2];
		outInfo->boffset[3] = rscTOC->datBOffset[3];
		outInfo->depCpt		= rscTOC->depCpt;
	}

	return TRUE;
}



uint32
RscManager::FindRscByType	(	uint32		inType,
								uint32		inPreviousUID	)
{
	// Not in use ?
	if( !GetBigFile() )
		return 0;

	int tocIdx = 0;

	// Valid previous UID
	if( inPreviousUID ) {
		tocIdx = FindUID( inPreviousUID );
		if( tocIdx < 0 )
			return 0;
		else
			tocIdx++;
	}

	// Find next UID
	TOC * rscTOC;
	while( tocIdx < int(tocLen) ) {
		rscTOC = toc + tocIdx;
		if( rscTOC->type == inType )
			return rscTOC->uid;
		else
			tocIdx++;
	}

	// Not found !
	return 0;
}



NvResource*
RscManager::CreateInstance		(	uint32			inUID		)
{
	RscInfo rsc;
	if( !GetRscInfo(inUID,&rsc) )
		return NULL;

	int idx = FindType( rsc.type );
	if( idx < 0 )
		return NULL;

	return factoryA[idx].fac->CreateInstance( inUID );
}



bool
RscManager::IsAvailableRsc		(	uint32			inUID		)
{
	int tocIdx = FindUID( inUID	);
	if( tocIdx < 0 )
		return FALSE;

	TOC* rscTOC = toc + tocIdx;

	// Get Factory for this rsc
	int facIdx = FindType( rscTOC->type );
	if( facIdx < 0 )
		return FALSE;

	RscFactory* fac = factoryA[facIdx].fac;
	return fac->IsAvailableRsc( inUID );
}



bool
RscManager::RegisterFactory		(	RscFactory*		inFactory	)
{
	if(		!inFactory
		||	inFactory->GetType()==0 )
		return FALSE;

	FactoryDesc desc;
	desc.fac	= inFactory;
	desc.type	= inFactory->GetType();

	// Sorted insert
	uint N = factoryA.size();
	for( uint i = 0 ; i < N ; i++ ) {

		// Factory already registered ?
		if( factoryA[i].fac == inFactory )
			return TRUE;

		// Type already registered -> remplace
		if( factoryA[i].type == desc.type ) {
			factoryA[i].fac->RegisterShut();
			inFactory->RegisterInit();
			factoryA[i] = desc;
			return TRUE;
		}

		// Insert here !
		if( factoryA[i].type > desc.type ) {
			factoryA.insert( factoryA.begin()+i, desc );
			inFactory->RegisterInit();
			return TRUE;
		}
	}

	// The last one !
	factoryA.push_back( desc );
	inFactory->RegisterInit();
	return TRUE;
}



RscFactory*
RscManager::GetFactory		(	uint32				inType			)
{
	if( !inType )
		return NULL;

	int facIdx = FindType( inType );

	if( facIdx < 0 )
		return NULL;
	else
		return factoryA[facIdx].fac;
}


uint
RscManager::CountFactory	(		)
{
	return factoryA.size();
}


RscFactory*
RscManager::EnumFactory		(	uint	inIndex		)
{
	if( inIndex < factoryA.size() )
		return factoryA[inIndex].fac;
	else
		return NULL;
}


bool
RscManager::AddPrefetchQ	(	RscPrefetchQ*		inPrefetchQ		)
{
	// Not in use ?
	if( !GetBigFile() )
		return FALSE;

	if(	!inPrefetchQ )
		return FALSE;

	if( inPrefetchQ->stt == RscPrefetchQ::PENDING )
		return FALSE;

	inPrefetchQ->next = NULL;
	if( !qHead )
	{
		qHead = qTail = inPrefetchQ;
	}
	else
	{
		NV_ASSERT( qTail );
		qTail->next = inPrefetchQ;
		qTail		= inPrefetchQ;
	}

	qSize++;
	inPrefetchQ->stt = RscPrefetchQ::PENDING;
	return TRUE;
}


bool
RscManager::AbortPrefetchQ	(	RscPrefetchQ*	inPrefetchQ	)
{
	if(		!inPrefetchQ
		||	qSize == 0
		||	inPrefetchQ->IsReady()
		||	GetProgress() == inPrefetchQ )
		return FALSE;

	RscPrefetchQ	*prev = NULL,
					*cur  = qHead;

	while( cur )
	{
		if( cur == inPrefetchQ )
		{
			if( cur == qHead && cur == qTail )
			{
				NV_ASSERT( qSize == 1 );
				qTail = qHead = NULL;
			}
			else if( cur == qHead )
			{
				NV_ASSERT( qSize > 0 );
				NV_ASSERT( cur->next );
				qHead = cur->next;
			}
			else if( cur == qTail )
			{
				NV_ASSERT( qSize > 0 );
				NV_ASSERT( prev );
				qTail = prev;
				qTail->next = NULL;
			}
			else
			{
				NV_ASSERT( qSize > 0 );
				NV_ASSERT( prev->next );
				NV_ASSERT( cur->next );
				prev->next = cur->next;
			}

			qSize--;
			cur->stt	= RscPrefetchQ::ABORTED;
			cur->next	= NULL;
			return TRUE;
		}

		prev = cur;
		cur  = cur->next;
	}

	return FALSE;
}


uint
RscManager::Unload	(	uint32	inUID	)
{
	RscInfo rsc;

	if( inUID && GetRscInfo(inUID,&rsc) )
	{
		int idx = FindType( rsc.type );
		if( idx >= 0 )
		{
			if( factoryA[idx].fac->UnloadRsc(inUID) )
			{
				return 1;
			}
		}
	}

	return 0;
}


uint
RscManager::Unload ( UInt32A& inUIDs )
{
	uint cpt = 0;

	for( uint i=0 ; i<inUIDs.size() ; i++ )
		cpt += Unload( inUIDs[i] );

	return cpt;
}


uint
RscManager::Unload ( RscPrefetchQ* inPrefetchQ )
{
	if( !inPrefetchQ )
		return 0;

	if( !inPrefetchQ->IsReady()	)
		return 0;

	uint cpt = Unload( inPrefetchQ->rscUIDA );

//	inPrefetchQ->stt	= RscPrefetchQ::IDLE;
//	inPrefetchQ->next	= NULL;

	return cpt;
}


uint
RscManager::UnloadAll ( )
{
	uint cpt = 0;

	for( uint i=0 ; i<tocLen ; i++ )
	{
		uint32 uid = toc[i].uid;
		cpt += Unload( uid );
	}

	return cpt;
}


RscPrefetchQ*
RscManager::GetProgress	(	float*			outProgress	)
{
	switch( curRPState )
	{
	case RP_NONE :
		// No PrefetchQ pending !
		if( outProgress )
			*outProgress = 0.0f;
		return NULL;

	default :
		// PrefetchQ pending and progress available.
		if( outProgress ) {
			NV_ASSERT( curLoadedBSize <= curToLoadBSize );
			if( curToLoadBSize )
				*outProgress = float(curLoadedBSize) / float(curToLoadBSize);
			else
				*outProgress = 0.0f;
		}
		return curRP;
	}
}


bool
RscManager::GetRscLog(	nv::vector<RscLog> & outLog		)
{
#ifndef _MASTER
	outLog.copy( logRscA.begin(), logRscA.end() );
	logRscA.clear();
	bool res = logRscOverflowed;
	logRscOverflowed = FALSE;
	return res;
#else
	return FALSE;
#endif
}


void
RscManager::Update	(	float	inMaxMsTime		)
{
	// Not in use ?
	if(	!GetBigFile() )
		return;

	// Time slice invalid ?
	if( inMaxMsTime <= 0.0f )
		inMaxMsTime = 1.0f;
	inMaxMsTime *= 0.001f;	// ms -> s

	nv::clock::Time t0, t1;
	nv::clock::GetTime( &t0 );

	RscLog log;
	log.t = float(t0);

	for( ;; )
	{

		switch( curRPState )
		{

		//
		// Start a new RP ?

		case RP_NONE :
			{
				// Queue is empty ?
				if( qSize == 0 )
					return;

				curRP = qHead;
				qHead = curRP->next;
				qSize--;
				curRP->next = NULL;

				// start * CHECKING_TO_LOAD *
				curRPState = RP_CHECKING_TO_LOAD;
				curToLoadA.clear();
				curToLoadBSize = 0;
				curLoadedBSize = 0;
				Memset( curToLoadBitA.data(), 0, curToLoadBitA.size()*4 );
				curRPStateCtx_index = 0;
				break;	// loop in for()
			}

		//
		// Checking UID to load

		case RP_CHECKING_TO_LOAD :
			{
				uint N = curRP->rscUIDA.size();
				uint i = curRPStateCtx_index;
				for( ; i < N ; i++ ) {

					if( (i&0xF)==0 ) {
						// Time overflow ?
						nv::clock::GetTime( &t1 );
						if( (t1-t0) > inMaxMsTime ) {
							curRPStateCtx_index = i;
							return;
						}
					}

					uint32 uid = curRP->rscUIDA[i];
					if( uid )
						CheckToLoadUID( uid );
				}

				// Empty ? -> terminate
				if( curToLoadA.size() == 0 ) {
					curRP->rscUIDA.clear();
					// Call Ready CallBack
					curRP->stt = RscPrefetchQ::COMPLETED;
					// start * RP_NONE *
					curRP	   = NULL;
					curRPState = RP_NONE;
					break;	// loop in for()
				}

				// start * RP_ALLOC_MEMORY *
				curRPState = RP_ALLOC_MEMORY;
				curRPStateCtx_index = 0;
				break;	// loop in for()
			}

		//
		// Allocate all memory

		case RP_ALLOC_MEMORY :
			{
				ToLoadRsc * toLoad     = &curToLoadA[0] + curRPStateCtx_index;
				ToLoadRsc * toLoad_end = &curToLoadA[0] + curToLoadA.size();
				uint32 toprefetch_bsize;

				while( toLoad < toLoad_end ) {

					if( (curRPStateCtx_index&0xF)==0 ) {
						// Time overflow ?
						nv::clock::GetTime( &t1 );
						if( (t1-t0) > inMaxMsTime )
							return;
					}

					NV_ASSERT( toLoad->toc->datBSsize > 0 );

					// Ask the factory to alloc memory
					toprefetch_bsize  = toLoad->fac->GetToPrefetchRscBSize( toLoad->toc->uid, toLoad->toc->datBSsize );
					toLoad->prefPtr   = toprefetch_bsize ? toLoad->fac->AllocMemRsc( toprefetch_bsize ) : NULL;

					// Invalid ?
					if( !toLoad->prefPtr || toprefetch_bsize==0 )
					{
						// Move the last item in place
						*toLoad = *(toLoad_end-1);
						curToLoadA.pop_back();
						toLoad_end--;
					}
					else
					{
						toLoad->prefBSize  = toprefetch_bsize;
						curToLoadBSize	  += toprefetch_bsize;
						curRPStateCtx_index++;
						toLoad++;
					}
				}

				// Empty ? -> terminate
				if( curToLoadA.size() == 0 )
				{
					curRP->rscUIDA.clear();
					// Call Ready CallBack
					curRP->stt = RscPrefetchQ::COMPLETED;
					// start * RP_NONE *
					curRP	   = NULL;
					curRPState = RP_NONE;
					break;	// loop in for()
				}

				// start * RP_OPTIMISE_SORTING *
				curRPState = RP_OPTIMISE_SORTING;
				break;	// loop in for()
			}

		//
		// Optimise the ToLoad array by offset

		case RP_OPTIMISE_SORTING :
			{
				// Sort by offset
				ToLoadRsc * toload_begin = &curToLoadA[0];
				ToLoadRsc * toload_end   = &curToLoadA[0] + curToLoadA.size() - 1;
				ToLoadRsc * toload;
				uint32 off0, off1;

				// Very slow bubble sort ... Radix comes later.

				bool again = TRUE;
				while( again )
				{
					again = FALSE;

					// Time overflow ?
					nv::clock::GetTime( &t1 );
					if( (t1-t0) > inMaxMsTime )
						return;

					toload = toload_begin;
					while( toload < toload_end )
					{
						off0 = toload[0].toc->datBOffset[0];
						off1 = toload[1].toc->datBOffset[0];
						if( off0 > off1 ) {
							Swap( toload[0], toload[1] );
							again = TRUE;
						}
						toload++;
					}
				}

				// start * RP_READING_TO_CACHE *
				curRPState = RP_READING_TO_CACHE;
				curRPStateCtx_index = 0;
				curRP_RCacheBOffset	= 0;
				curRP_RCachePtr		= EngineMallocA( paramCacheBSize, NVHW_IO_BALIGN );
				NV_ASSERT( curRP_RCachePtr );
				break;	// loop in for()
			}

		//
		// Reading data to cache

		case RP_READING_TO_CACHE :
			{
				ToLoadRsc * toLoad		= &curToLoadA[0] + curRPStateCtx_index;
				ToLoadRsc * toLoad_end	= &curToLoadA[0] + curToLoadA.size();
				uint32 off, off0, off1;

				// Compute the Sliding Cache-Window (SCW) offset with the first rsc

				NV_ASSERT( toLoad < toLoad_end );			// toLoad remain !
				off0 = toLoad->toc->datBOffset[0];
				off1 = off0 + toLoad->prefBSize;
				NV_ASSERT( off1 > curRP_RCacheBOffset );	// already done !
				// Move the SCW to the first far offset ?
				if( off0 > curRP_RCacheBOffset )
					curRP_RCacheBOffset = off0 & paramReadBStartAlignMask;
				off = off1;
				toLoad++;

				// Compute the best SCW size by appending next rsc

				while( toLoad < toLoad_end )
				{
					// Rsc start & end offset
					off0 = toLoad->toc->datBOffset[0];
					off1 = off0 + toLoad->prefBSize;
					NV_ASSERT( off0 >= off );	// by the sort

					// rsc offset out of cache range ?
					if( (off0-curRP_RCacheBOffset) >= paramCacheBSize )
						break;

					// jump offset to large ?
					if(	(off0-off) > paramJumpMaxBSize )
						break;

					// try next rsc
					off = off1;
					toLoad++;
				}

				curRP_RCacheBSize = off - curRP_RCacheBOffset;
				curRP_RCacheBSize = RoundX( curRP_RCacheBSize, paramReadBSizeAlign );
				curRP_RCacheBSize = Min( curRP_RCacheBSize, paramCacheBSize );

				// Setup the ReadRequest with this cache window
				curRP_RR.Setup( curRP_RCachePtr, curRP_RCacheBSize, curRP_RCacheBOffset );
				file::AddReadRequest( &curRP_RR, GetRRPriority() );

				// start * RP_MEMCPY_FROM_CACHE *
				curRPState = RP_MEMCPY_FROM_CACHE;
				return;
			}

		//
		// Memcpy data from cache

		case RP_MEMCPY_FROM_CACHE :
			{
				// Data are ready ?
				if( !curRP_RR.IsReady() )
					return;

				// Error ?
				if( curRP_RR.stt != file::ReadRequest::COMPLETED )
				{
					bool retry;
					core::GetParameter( core::PR_RSCMAN_RETRY, &retry );

					if( retry )
					{
						// retry ...
						curRPState = RP_READING_TO_CACHE;
						break;	// loop in for()
					}
					else
					{
						// abort loading ...
						ToLoadRsc* toLoad		= &curToLoadA[0];
						ToLoadRsc* toLoad_end	= toLoad + curToLoadA.size();
						while( toLoad < toLoad_end ) {
							toLoad->fac->FreeMemRsc( toLoad->prefPtr, toLoad->prefBSize );
							toLoad++;
						}

						EngineFree( curRP_RCachePtr );
						curRP_RCachePtr = NULL;
						curRPState = RP_NONE;

						curRP->stt  = RscPrefetchQ::FAILED;

						curRP->next = NULL;
						break;	// loop in for()
					}
				}

				ToLoadRsc * toLoad		= &curToLoadA[0] + curRPStateCtx_index;
				ToLoadRsc * toLoad_end	= &curToLoadA[0] + curToLoadA.size();

				// cache offset range
				uint32 cache_off0 = curRP_RCacheBOffset;
				uint32 cache_off1 = cache_off0 + curRP_RCacheBSize;
				uint32 off0, off1, clip_off0, clip_off1;

				while( toLoad < toLoad_end )
				{
					off0 = toLoad->toc->datBOffset[0];
					off1 = off0 + toLoad->prefBSize;
					NV_ASSERT( off1 > cache_off0 );		// already done !

					// Clip rsc with SCW
					clip_off0 = Max( off0, cache_off0 );
					clip_off1 = Min( off1, cache_off1 );

					// rsc data out of cache ?
					if( clip_off0 >= clip_off1 )
						break;

					// rsc data are (partially or totally) in cache -> memcpy
					pvoid src_ptr = (pvoid) ( uint32(curRP_RCachePtr)+(clip_off0-cache_off0) );
					pvoid dst_ptr = (pvoid) ( uint32(toLoad->prefPtr)+(clip_off0-off0) );
					Memcpy( dst_ptr, src_ptr, (clip_off1-clip_off0) );
					NV_ASSERT( (clip_off1-clip_off0) <= toLoad->prefBSize );
					curLoadedBSize += ( clip_off1 - clip_off0 );

					// rsc data totally loaded ?
					if( cache_off1 >= off1 ) {
						curRPStateCtx_index++;
						#ifndef _MASTER
						if( logRscA.size() < paramLogMaxSize ) {
							log.uid = toLoad->toc->uid;
							logRscA.push_back( log );
						} else {
							logRscOverflowed = TRUE;
						}
						#endif
					}

					// Next
					toLoad++;
				}

				// Continue ?
				if( curRPStateCtx_index < curToLoadA.size() )
				{
					curRPState = RP_READING_TO_CACHE;
					// Translate the SCW by its size
					curRP_RCacheBOffset += curRP_RCacheBSize;
				}
				else
				{
					// free the cache
					EngineFree( curRP_RCachePtr );
					curRP_RCachePtr = NULL;

					// start * RP_FACTORY_SETUP *
					curRPState = RP_FACTORY_SETUP;
					curRPStateCtx_index = 0;
				}
				break;	// loop in for()
			}

		//
		// Prefetch loaded rsc in factory

		case RP_FACTORY_SETUP :
			{
				ToLoadRsc * toLoad		= &curToLoadA[0] + curRPStateCtx_index;
				ToLoadRsc * toLoad_end	= &curToLoadA[0] + curToLoadA.size();

				while( toLoad < toLoad_end )
				{
					if( (curRPStateCtx_index&0xF)==0 )
					{
						// Time overflow ?
						nv::clock::GetTime( &t1 );
						if( (t1-t0) > inMaxMsTime )
						{
							return;
						}
					}

					// Unpack data ?
					bool unpackDone = true;
					uint unpackBSize = 0;
					if( libc::GetUnpackBSize(toLoad->prefPtr,toLoad->prefBSize,unpackBSize) )
					{
						unpackDone = false;
						NV_ASSERTC( unpackBSize, "Invalid packed data. Unpacked size is 0 !" );
						if( unpackBSize )
						{
							pvoid unpackPtr = toLoad->fac->AllocMemRsc( unpackBSize );
							NV_ASSERTC( unpackPtr, "Rsc memory allocation for unpack has failed !" );
							if( unpackPtr )
							{
								bool unpackSuccess = libc::Unpack( toLoad->prefPtr, toLoad->prefBSize, unpackPtr );
								NV_ASSERTC( unpackSuccess, "Rsc unpack has failed !" );
								if( unpackSuccess )
								{
									toLoad->fac->FreeMemRsc( toLoad->prefPtr, toLoad->prefBSize );
									toLoad->prefPtr   = unpackPtr;
									toLoad->prefBSize = unpackBSize;
									unpackDone = true;
								}
							}
						}
					}

					bool prefetchDone = false;
					if( unpackDone )
					{
						prefetchDone = toLoad->fac->PrefetchRsc(	toLoad->toc->uid,
																	toLoad->prefPtr,
																	toLoad->prefBSize	);
					}

					if( !prefetchDone )
					{
						// Invalid rsc -> free from memory
						toLoad->fac->FreeMemRsc( toLoad->prefPtr, toLoad->prefBSize );
						// Move the last item in place
						*toLoad = *(toLoad_end-1);
						curToLoadA.pop_back();
						toLoad_end--;
					}
					else
					{
						toLoad++;
						curRPStateCtx_index++;
					}
				}

				//
				// All is done !

				// Update the prefetch Queue with the prefeched UID
				uint N = curToLoadA.size();
				curRP->rscUIDA.resize( N );
				for( uint i = 0 ; i < N ; i++ )
					curRP->rscUIDA[i] = curToLoadA[i].toc->uid;
				curToLoadA.clear();

				// Ready
				curRP->stt  = RscPrefetchQ::COMPLETED;
				curRP->next = NULL;

				// start * RP_NONE *
				curRP	   = NULL;
				curRPState = RP_NONE;
				break;	// loop in for()
			}

		} // switch()

	} // for()
}





namespace
{

	void check_exit ( )
	{
		if( core::ExitAsked() )
			core::Exit();
	}


	void delay ( float s )
	{
		clock::Time t0, t1;
		clock::GetTime( &t0 );
		for( ;; )
		{
			clock::GetTime( &t1 );
			float dt = t1 - t0;
			if( dt > s )
				break;
			check_exit();
		}
	}


	RscManager::AtomStatus wait_disc ( RscManager::AtomHook* hook )
	{
		for( ;; )
		{
			check_exit();

			file::Status			fs;
			RscManager::AtomStatus	as;

			fs = file::CheckStatus();
			as = hook ? hook->onError(fs) : RscManager::AS_Retry;

			if( as == RscManager::AS_Abort )
				return RscManager::AS_Abort;

			if( fs==file::FS_OK || fs==file::FS_BUSY )
				return RscManager::AS_Retry;

			delay( 0.1f );
		}
	}


}




bool
RscManager::AtomOpenBigFile	(	AtomHook*	inHook,
								pcstr		inBigfile	)
{
	if( !inBigfile )
		return FALSE;

	CloseBigFile();

	for ( ;; )
	{
		check_exit();

		if( OpenBigFile(inBigfile) )
		{
			if( inHook )
				inHook->onProgress( 1.0 );
			return TRUE;
		}

		if( wait_disc(inHook) == AS_Abort )
			return FALSE;
	}
}




bool
RscManager::AtomPrefetch	(	AtomHook*			inHook,
								RscPrefetchQ*		inQ		)
{
	if( !inQ )
		return TRUE;

	// back retry mode
	bool old_retry;
	core::GetParameter( core::PR_RSCMAN_RETRY, &old_retry );

	for( ;; )
	{
		bool added = AddPrefetchQ( inQ );
		NV_ASSERT( added );
		if( !added )
			return FALSE;

		// failed mode
		core::SetParameter( core::PR_RSCMAN_RETRY, FALSE );

		while( !inQ->IsReady() )
		{
			core::Update();
			RscManager::Update( 5 );

			check_exit();

			float p = 0;
			RscPrefetchQ* q = RscManager::GetProgress( &p );
			p = (q==inQ) ? p : 0.f;

			if( inHook )
				inHook->onProgress( p );
		}

		// restore retry
		core::SetParameter( core::PR_RSCMAN_RETRY, old_retry );

		if( inQ->IsCompleted() )
		{
			if( inHook )
				inHook->onProgress( 1.0 );
			return TRUE;
		}

		if( wait_disc(inHook) == AS_Abort )
			return FALSE;
	}
}




