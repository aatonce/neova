/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <NvProfManager.h>
using namespace nv;


#if defined( _NVCOMP_ENABLE_PROFILING )

namespace
{

	enum State {
		SRV_DOWN		= 0,
		SRV_LISTENING	= 1<<0,
		SRV_CONNECTED	= 1<<1,
		SRV_CON_IDLE	= SRV_CONNECTED | (1<<8),		// connect�, en attente de message client
		SRV_CON_AVOID	= SRV_CONNECTED | (2<<8),		// en annulation de r�ception
		SRV_CON_RCV		= SRV_CONNECTED | (3<<8),		// en r�ception {'PUID','LIST'}
		SRV_CON_AQU		= SRV_CONNECTED | (4<<8),		// en aquisition
		SRV_CON_SND		= SRV_CONNECTED | (5<<8)		// en �mission {'PUID','DATA'}
	};

	struct Header {
		uint32	dbsize;			// Data size in byte
		uint32	cmdClass;
		uint32	cmdId;
		uint32	reserved;
	};

	struct Item {
		uint32	PUID;
		uint32	append : 1;		// append ?
		uint32	bsize  : 31;	// size in byte
	};


	// PUID List
	sysvector<uint32>	puidList;

	// Data heap
	sysvector<uint8>	puidData;
	uint32				puidDataMaxBSize;

	uint				srvPort;
	int					srvSockId = -1;
	int					cliSockId = -1;
	State				srvState = SRV_DOWN;
	uint32				stateParam[4];
	Item				curItem;


	void
	SrvShut	(		)
	{
		if( cliSockId >= 0 ) {
			//net::Close(cliSockId);
			net::Release( cliSockId );
			cliSockId = -1;
		}
		if( srvSockId >= 0 ) {
			//net::Close(srvSockId);
			net::Release( srvSockId );
			srvSockId = -1;
		}
		puidList.free();
		puidData.free();
		srvState = SRV_DOWN;
	}

	void
	SrvStartListen		(	)
	{
		if( srvSockId >= 0 )
			net::Release( srvSockId );

		srvState = SRV_DOWN;
		puidList.free();
		puidData.free();

		srvSockId = net::Create( net::T_TCP );
		if( srvSockId < 0 )
			return;

		if( !net::Open(srvSockId,srvPort) )	{
			net::Release( srvSockId );
			srvSockId = -1;
			return;
		}

		if( !net::Listen(srvSockId,1) ) {
			net::Release( srvSockId );
			srvSockId = -1;
			return;
		}
		srvState = SRV_LISTENING;
	}

	void
	SrvCheckConnection	(		)
	{
		NV_ASSERT( srvState == SRV_LISTENING );
		NV_ASSERT( srvSockId >= 0 );

		// Check server listening state
		if( net::GetState(srvSockId) != net::S_LISTENING )
		{
			// Listen failed ?!
			SrvShut();
			return;
		}

		// Check pending client
		if(		net::IsClientPending(srvSockId)
			&&	net::Accept(srvSockId,cliSockId) )
		{
			// Client connection
			// -> close server
			// -> start in connected + idle state
			//net::Close( srvSockId );
			net::Release( srvSockId );
			srvSockId = -1;
			srvState = SRV_CON_IDLE;
			return;
		}
		else
		{
			cliSockId = -1;
		}
	}

	void
	CliCheckConnection	(		)
	{
		NV_ASSERT( (srvState&SRV_CONNECTED) != 0 );
		NV_ASSERT( cliSockId >= 0 );

		if( net::GetState(cliSockId) == net::S_CONNECTED )
			return;

		// Client deconnection
		// -> clear previous state
		// -> start srv in listen
		SrvShut();
		SrvStartListen();
	}

	inline
	bool
	IsAskedUID	(	uint32	inUID	)
	{
		uint32  N = puidList.size();
		uint32* T = &puidList[0];
		uint32  v;
		int i=0, j=N-1, k;
		while( j >= i ) {
			k = (i+j)>>1;
			v = T[k];
			if( v == inUID )
				return TRUE;
			else if( i==j )
				return FALSE;	// not found
			if( v > inUID )
				j = k;
			else
				i = k+1;
		}
		return FALSE;
	}

}

#endif // _NVCOMP_ENABLE_PROFILING





bool
ProfManager::Init	(	uint	inPort		)
{
#if defined( _NVCOMP_ENABLE_PROFILING )

	// Server already opened ?
	if( srvSockId >= 0 )
		return TRUE;

	core::GetParameter( nv::core::PR_PROFMAN_DATA_MAXBSIZE, &puidDataMaxBSize );
	puidList.Init();
	puidData.Init();

	srvPort	  = inPort;
	srvSockId = -1;
	cliSockId = -1;
	srvState  = SRV_DOWN;

	SrvStartListen();

	return ( srvState == SRV_LISTENING );

#else

	return FALSE;

#endif
}



bool
ProfManager::IsRunning	(			)
{
#if defined( _NVCOMP_ENABLE_PROFILING )
	return ( srvState != SRV_DOWN );
#else
	return FALSE;
#endif
}


bool
ProfManager::IsConnected	(		)
{
#if defined( _NVCOMP_ENABLE_PROFILING )
	return ((srvState&SRV_CONNECTED) != 0);
#else
	return FALSE;
#endif
}


bool
ProfManager::IsRecording	(		)
{
#if defined( _NVCOMP_ENABLE_PROFILING )
	return ( srvState == SRV_CON_AQU );
#else
	return FALSE;
#endif
}


void
ProfManager::Shut	(			)
{
#if defined( _NVCOMP_ENABLE_PROFILING )
	SrvShut();
	puidList.Shut();
	puidData.Shut();
#endif
}


void
ProfManager::Update	(	)
{
#if defined( _NVCOMP_ENABLE_PROFILING )

	if( srvState == SRV_DOWN )
		return;

	if( srvState == SRV_LISTENING ) {
		SrvCheckConnection();
		return;
	}

	NV_ASSERT( srvState & SRV_CONNECTED );
	CliCheckConnection();

	for( ;; )
	{
		switch( srvState )
		{
			case SRV_CON_IDLE :
				{
					if( !net::IsRcvPending(cliSockId,sizeof(Header)) )
						return;
					Header hdr;
					uint32 bsize;
					if( !net::Receive(cliSockId,bsize,&hdr,sizeof(Header)) ) {
						CliCheckConnection();
						return;
					}
					if( bsize < sizeof(Header) ) {
						// Protocol error (not reliable client) -> disconnet it
						SrvShut();
						SrvStartListen();
						return;
					}
					// {'PUID','LIST'} ?
					if(		hdr.cmdClass == 'PUID'
						&&	hdr.cmdId    == 'LIST' ) {
						if( hdr.dbsize > 0 && (hdr.dbsize%4) == 0 ) {
							srvState = SRV_CON_RCV;
							stateParam[0] = hdr.dbsize;	// remain
							stateParam[1] = 0;			// pos
							puidList.resize( hdr.dbsize / 4 );
							break;
						}
					}
					// Avoid this message !
					srvState = SRV_CON_AVOID;
					stateParam[0] = hdr.dbsize;	// remain
					break;
				}

			case SRV_CON_AVOID :
				{
					if( !net::IsRcvPending(cliSockId) )
						return;
					char tmp[1024];
					uint32 bsize;
					if( !net::Receive(cliSockId,bsize,&tmp,Max(uint(sizeof(tmp)),stateParam[0])) ) {
						CliCheckConnection();
						return;
					}
					stateParam[0] -= bsize;
					if( stateParam[0] == 0 )
						srvState = SRV_CON_IDLE;
					break;
				}

			case SRV_CON_RCV :
				{
					if( !net::IsRcvPending(cliSockId) )
						return;
					uint32 bsize;
					if( !net::Receive(cliSockId,bsize,((uint8*)puidList.data())+stateParam[1],stateParam[0]) ) {
						CliCheckConnection();
						return;
					}
					stateParam[0] -= bsize;
					stateParam[1] += bsize;
					if( stateParam[0] == 0 ) {
						srvState = SRV_CON_AQU;
						// reserve space for msg header
						puidData.resize( sizeof(Header) );
					}
					return;
				}

			case SRV_CON_AQU :
				{
					Header hdr;
					hdr.dbsize   = puidData.size() - sizeof(Header);
					hdr.cmdClass = 'PUID';
					hdr.cmdId    = 'DATA';
					Memcpy( puidData.data(), &hdr, sizeof(Header) );

					srvState = SRV_CON_SND;
					stateParam[0] = puidData.size();	// remain
					stateParam[1] = 0;					// pos
					break;
				}

			case SRV_CON_SND :
				{
					uint32 bsize;
					if( !net::Send(cliSockId,bsize,puidData.data()+stateParam[1],stateParam[0]) ) {
						CliCheckConnection();
						return;
					}
					stateParam[0] -= bsize;
					stateParam[1] += bsize;
					if( stateParam[0] == 0 )
						srvState = SRV_CON_IDLE;
					// break the loop
					return;
				}

			default :
				{
					//NV_ASSERT( FALSE );
					return;
				}
		}
	}

#endif
}



bool
ProfManager::Setup	(	uint32			inPUID,
						pvoid			inData,
						uint			inBSize		)
{
#if defined( _NVCOMP_ENABLE_PROFILING )

	// Serveur en acquisition ?
	if(	srvState != SRV_CON_AQU )
		return FALSE;

	// Valid ?
	if( (inData && !inBSize) || (!inData && inBSize) )
		return FALSE;

	// Asked PUID ?
	if( !IsAskedUID(inPUID) )
		return FALSE;

	// Heap overflow ?
	uint h0 = puidData.size();
	if( (h0+sizeof(Item)+inBSize) > puidDataMaxBSize )
		return FALSE;
	puidData.resize( h0+sizeof(Item)+inBSize );

	// Copy item
	curItem.PUID   = inPUID;
	curItem.append = 0;
	curItem.bsize  = inBSize;
	Memcpy( &puidData[h0], &curItem, sizeof(Item) );
	if( inBSize )
		Memcpy( &puidData[h0+sizeof(Item)], inData, inBSize );

	return TRUE;

#else

	return FALSE;

#endif
}



bool
ProfManager::Append	(	uint32			inPUID,
						pvoid			inData,
						uint			inBSize		)
{
#if defined( _NVCOMP_ENABLE_PROFILING )

	// Serveur en acquisition ?
	if(	srvState != SRV_CON_AQU )
		return FALSE;

	// Not empty ?
	if( !inData || !inBSize )
		return FALSE;

	// Asked PUID ?
	if( !IsAskedUID(inPUID) )
		return FALSE;

	// No heap overflow control !
	uint h0 = puidData.size();
	puidData.resize( h0+sizeof(Item)+inBSize );

	// Copy item
	curItem.PUID   = inPUID;
	curItem.append = 1;
	curItem.bsize  = inBSize;
	Memcpy( &puidData[h0], &curItem, sizeof(Item) );
	Memcpy( &puidData[h0+sizeof(Item)], inData, inBSize );

	return TRUE;

#else

	return FALSE;

#endif
}



bool
ProfManager::Append	(	pvoid			inData,
						uint			inBSize		)
{
#if defined( _NVCOMP_ENABLE_PROFILING )

	// Serveur en acquisition ?
	if(	srvState != SRV_CON_AQU )
		return FALSE;

	// Not empty ?
	if( !inData || !inBSize )
		return FALSE;

	// No heap overflow control !
	uint h0 = puidData.size();
	if( h0 < sizeof(Item) )
		return FALSE;
	puidData.resize( h0+inBSize );

	// Append data
	uint s0 = curItem.bsize;
	curItem.bsize += inBSize;
	Memcpy( &puidData[h0-s0-sizeof(Item)], &curItem, sizeof(Item) );
	Memcpy( &puidData[h0], inData, inBSize );

	return TRUE;

#else

	return FALSE;

#endif
}



