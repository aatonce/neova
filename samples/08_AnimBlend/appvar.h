/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _APPVAR_H_
#define _APPVAR_H_


#include "appdef.h"


#define		MESH_NAME			"*Tut*NvMesh"
#define		SKEL_NAME			"*Tut*NvSkeleton"
#define		WALK_ANIM_NAME		"*Tut*walk*NvSkelAnim"
#define		RUN_ANIM_NAME		"*Tut*run*NvSkelAnim"



struct AppVar
{
	uint32				mesh_UID;
	uint32				skel_UID;
	uint32				skel_walk_UID;
	uint32				skel_run_UID;
	
	NvFrameClear*		pClear;
	NvMesh*				mesh;
	NvGeomShader*		shd;
	NvSkeleton*			skel;
	NvSkelAnim*			skel_walk;
	NvSkelAnim*			skel_run;
	NvSkeleton::Target	skel_target;
	
	float meshRotAngleY;

	struct AnimSimpleInterpolator
	{
		NvSkeleton::State	state[3];
		NvSkelAnim*			anim0;
		NvSkelAnim*			anim1;
		
		float				animEndTime;
		float				anim0ST;
		float				anim1ST;
		
		
		void				Init(	NvSkeleton*		inSkel,
									NvSkelAnim*		inAnim0,
									NvSkelAnim*		inAnim1		);
		
		void				Shut(								);
		
		NvSkeleton::State*	Blend(	float 			_elapsedTime,
									float 			_blendW 	);
	};
	AnimSimpleInterpolator	animInterp;



	
	void Init();
	void Shut();
	void Load();
	
	
	void UpdateSkinning( float _elapsedTime, float _blendW );
	void Draw();
};


#endif // _APPVAR_H_

