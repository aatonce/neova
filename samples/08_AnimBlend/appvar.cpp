/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"



/* ---------------------------------		AppVar			--------------------------------------------- */
void AppVar::Init()
{
	pClear		= NULL;
	mesh		= NULL;
	shd			= NULL;
	skel		= NULL;
	skel_walk	= NULL;
	skel_run	= NULL;
	
	pClear = NvFrameClear::Create();
	pClear->SetColor( 0x30304000 );
	
	
	mesh_UID		= FindUIDByName( MESH_NAME		);
	skel_UID		= FindUIDByName( SKEL_NAME		);
	skel_walk_UID	= FindUIDByName( WALK_ANIM_NAME	);
	skel_run_UID	= FindUIDByName( RUN_ANIM_NAME	);

	UInt32A toLoadA;
		toLoadA.push_back( mesh_UID		 );
		toLoadA.push_back( skel_UID		 );
		toLoadA.push_back( skel_walk_UID );
		toLoadA.push_back( skel_run_UID	 );
	SyncPrefetch( toLoadA );
	
	meshRotAngleY = Pi/5.0f;
}




void AppVar::Load()
{
	mesh = NvMesh::Create( mesh_UID );
	NV_ASSERT( mesh );
	NV_ASSERT( mesh->IsSkinnable() );

	shd = NvGeomShader::Create( mesh );
	NV_ASSERT( shd );

	skel = NvSkeleton::Create( skel_UID );
	NV_ASSERT( skel );

	skel_walk = NvSkelAnim::Create( skel_walk_UID );
	skel_run  = NvSkelAnim::Create( skel_run_UID );
	NV_ASSERT( skel_walk );
	NV_ASSERT( skel_run );
	
	
	
	// Init skinning & blending
	{
		// skeleton informations
		uint	bone_cpt	= skel->GetBoneCpt();
		uint32*	bone_names	= skel->GetBoneNameA();

		// resizing only !
		skel->InitTarget( &skel_target );

		// map mesh with bone's final matrix array
		mesh->MapSkeleton( bone_cpt, bone_names, skel_target.blendA.data() );

		// init interpolator
		animInterp.Init( skel, skel_walk, skel_run );
	}
}


void AppVar::Shut()
{
	animInterp.Shut();
	
	SafeRelease( pClear    );
	SafeRelease( mesh 	   );
	SafeRelease( shd  	   );
	SafeRelease( skel 	   );
	SafeRelease( skel_walk );
	SafeRelease( skel_run  );
}




void AppVar::UpdateSkinning( float _elapsedTime, float _blendW )
{
	NvSkeleton::State* blendState = animInterp.Blend( _elapsedTime, _blendW );
	
	// convert local bone's animations (state) to world & blend matrices (target)
	skel->ComputeTarget( &skel_target, blendState );
	
	// update the mesh skinning asynchronous process
	mesh->Update();
}



void AppVar::Draw()
{
	Matrix viewMat, projMat, worldMat;
	
	Vec4 viewport ( 0, 0, DpyManager::GetRasterWidth(DPYR_IN_FRAME), DpyManager::GetRasterHeight(DPYR_IN_FRAME) );
	Vec2 clipRange( 1.0f, 100.f );
	
	// Setup matrix
	{
		// view matrix
		Matrix  camMat;
		Vec3	camPos( 0.0f, 0.0f, 5.0f );
		MatrixBuildTR( &camMat, &camPos, NULL, NULL );
		MatrixFastInverse( &viewMat, &camMat );
		
		// proj matrix
		float	fov = Pi * 0.1f;
		MatrixPerspectiveRH( &projMat, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );

		// world matrix
		Vec3	meshPos( 0.0f, -0.5f, 0.0f );
		Quat	meshRotX( Pi/2.0f, Vec3(1,0,0) );
		Quat	meshRotY( meshRotAngleY, Vec3(0,1,0) );
		
		Quat	meshRot = meshRotY * meshRotX;
		MatrixBuildTR( &worldMat, &meshPos, &meshRot, NULL );
	}
	
	// Draw frame
	{
		DpyManager::BeginFrame	();
			DpyManager::SetView		( &viewMat, &projMat, &clipRange, &viewport );		// Set viewProj
			DpyManager::Draw		( pClear 									);		// Clear screen
			DpyManager::SetWorldTR	( &worldMat 								);		// Set world
			DpyManager::Draw		( shd 										);		// Draw mesh
		DpyManager::EndFrame	();
		DpyManager::FlushFrame	();
	}
}










/* ---------------------------------		AnimSimpleInterpolator			--------------------------------------------- */

NvSkeleton::State*
AppVar::AnimSimpleInterpolator::Blend(	float _elapsedTime,
										float _blendW 		)
{
	float currAnimTime = _elapsedTime;
	
	// looping
	while( currAnimTime > animEndTime )
		currAnimTime -= animEndTime;


	float blendW = Clamp( _blendW, 0.0f, 1.0f );
	
	if( blendW == 0.0f )
	{
		anim0->ComputeState( currAnimTime*anim0ST, state[0].trsA.data() );
		return &state[0];
	}
	else
	if( blendW == 1.0f )
	{
		anim1->ComputeState( currAnimTime*anim1ST, state[1].trsA.data() );
		return &state[1];
	}
	else
	{
		anim0->ComputeState( currAnimTime*anim0ST, state[0].trsA.data() );
		anim1->ComputeState( currAnimTime*anim1ST, state[1].trsA.data() );
		
		// do linear interpolation between state0 and state2
		uint nb_bones = state[0].trsA.size();
		for( uint i = 0 ; i < nb_bones ; i++ )
		{
			Vec3Lerp ( &state[2].trsA[i].t, &state[0].trsA[i].t, &state[1].trsA[i].t, blendW );
			QuatSlerp( &state[2].trsA[i].r, &state[0].trsA[i].r, &state[1].trsA[i].r, blendW );
		}
		return &state[2];
	}
};



void	AppVar::AnimSimpleInterpolator::Init(	NvSkeleton*		inSkel,
												NvSkelAnim*		inAnim0,
												NvSkelAnim*		inAnim1		)
{
	// Init states (t, r & parentIdx)
	// resize & init bones with pose/figure mode
	inSkel->InitState( &state[0] );
	inSkel->InitState( &state[1] );
	inSkel->InitState( &state[2] );

	// Map animations to states (unknowed bones are ignored !)
	inAnim0->Map( inSkel );
	inAnim1->Map( inSkel );

	inAnim0->AddRef();
	inAnim1->AddRef();
	anim0	= inAnim0;
	anim1 	= inAnim1;

	// Get loop time & scales
	float anim0EndTime = anim0->GetEndTime();
	float anim1EndTime = anim1->GetEndTime();
	DebugPrintf( "anim0: %f s\n", anim0EndTime );
	DebugPrintf( "anim1: %f s\n", anim1EndTime );
	
	animEndTime   = Max( anim0EndTime, anim1EndTime );
	anim0ST = anim0EndTime / animEndTime;
	anim1ST = anim1EndTime / animEndTime;
};



void	AppVar::AnimSimpleInterpolator::Shut()
{
	anim0->Release();
	anim1->Release();
};







