

#include "appctrl.h"






bool AppCtrl::GetController( AppCtrl::AppInputs* _outAppInput )
{
	static int ctrl_no = -1;
	
	ctrl::Status* ctrl_stat = NULL;
	ctrl_stat = ctrl::GetStatus( ctrl_no );
	if( !ctrl_stat )
	{
		ctrl_no	  = WaitController();
		ctrl_stat = ctrl::GetStatus( ctrl_no );
		if(!ctrl_stat)
			return false;
	}
	
	{
		static bool butt1_flg	= FALSE;
		static bool	butt2_flg	= FALSE;
		
		_outAppInput->dir1 = Vec2( ButtonNormThr( ctrl_stat->button[ ctrl::BT_DIR1X ], 0.35f ),
								   ButtonNormThr( ctrl_stat->button[ ctrl::BT_DIR1Y ], 0.35f ) );
		_outAppInput->dir2 = Vec2( ButtonNormThr( ctrl_stat->button[ ctrl::BT_DIR2X ], 0.1f ),
								   ButtonNormThr( ctrl_stat->button[ ctrl::BT_DIR2Y ], 0.1f ) );
	
		float flt1 = ButtonNormThr( ctrl_stat->button[ ctrl::BT_BUTTON1 ],	0.1f  );
		float flt2 = ButtonNormThr( ctrl_stat->button[ ctrl::BT_BUTTON2 ],	0.1f  );

		_outAppInput->button1_pressed = IsPressed(flt1, butt1_flg);
		_outAppInput->button2_pressed = IsPressed(flt2, butt2_flg);
	}
	
	return true;
}






int AppCtrl::WaitController()
{
	ctrl::Close();
	ctrl::Open();
	Printf( "Waiting for a controller ...\n" );
	int ctrl_no = -1;
	for(uint i=0 ;;++i)
	{
		if( core::Update() & (core::UPD_EXIT_ASKED | core::UPD_EXITED) )
			break;
		
		ctrl_no = FindController();
		
		if( ctrl_no>=0 )	break;
		else				ShowNoPadPlugged(i,FALSE);
	}
	Printf( "Controller #%d acquired !\n", ctrl_no );
	ShowNoPadPlugged(0,FALSE);
	
	return ctrl_no;
}



int AppCtrl::FindController()
{
	for( int i=0; i<int(ctrl::GetNbMax()); i++ )
	{
		ctrl::Status* pStat = ctrl::GetStatus(i);
		//Check the controller type
		if( pStat && (pStat->type==ctrl::TP_PADDLE || pStat->type==ctrl::TP_COMBI))
			return i;
	}
	return -1;
}



float AppCtrl::ButtonNormThr	( float	inButtonV, float inThr	)
{
	if( Abs(inButtonV) <= inThr )
		return 0.0f;
	
	float nrm = (1.0f-inThr);
	
	if( inButtonV > 0 )
		return (inButtonV-inThr) / nrm;
	else
		return (inButtonV+inThr) / nrm;
}


bool AppCtrl::IsPressed			( float inButtonV, bool& ioFlag	)
{
	if( inButtonV==0.0f )
	{
		ioFlag = FALSE;
		return FALSE;
	}
	bool _f = ioFlag;
	ioFlag = TRUE;
	return !_f;
}
