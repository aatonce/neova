/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"
#include "appctrl.h"





void InitNeova();
void ShutNeova();
uint UpdateNeova();

void GameLoop();





void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif

	nv::core::Init();
	DpyManager::Init( DpyManager::CS_RIGHT_HANDED );
	SndManager::Init();
	RscManager::Init();
	RscManager::OpenBigFile( BF_PATH );
}



void ShutNeova()
{
	SndManager::Shut();
	DpyManager::Shut();
	RscManager::CloseBigFile();
	RscManager::Shut();
	nv::core::Shut();
}


uint UpdateNeova()
{
	uint upd_flags = core::Update();
	RscManager::Update( 1.0f );		// 1 ms
	SndManager::Update();
	
	return upd_flags;
}






void GameLoop()
{	

	AppVar appvar;


	appvar.Init();
	appvar.Load();


	float blendW;
	float meshRotY = Pi/5.0f;
	
	float deltaTime, elapsedTime, lElapsedTime = 0;
	clock::Time initTime;
	clock::GetTime( &initTime );

	for( ;; )
	{
		uint upd_flags = UpdateNeova();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;

		// Timing
		{
			clock::Time currTime;			
			clock::GetTime( &currTime );
			
			elapsedTime = currTime 	  - initTime;
			deltaTime 	= elapsedTime - lElapsedTime;
			
			lElapsedTime = elapsedTime;
		}

		// Controller
		{
			AppCtrl::AppInputs input;
			if( !AppCtrl::GetController( &input ) )
				break ;
			
			
			if( input.button1_pressed || input.button2_pressed)
				break;													// Exit
			
			blendW = input.dir1.x;
			blendW = Max( blendW, 0.f );
			
			appvar.meshRotAngleY += input.dir2.x * deltaTime;
		}


		appvar.UpdateSkinning( elapsedTime, blendW );
		appvar.Draw();
	}

	// Sync with async drawing to ensure physical release of drawing items
	DpyManager::SyncFrame();


	appvar.Shut();
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_08"

void GameMain()
{
	InitNeova();

	GameLoop();
	
	ShutNeova();
}
