/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _APPVAR_H_
#define _APPVAR_H_


#include "appdef.h"




#define				ENV_MAP					"*envMap*tga"
#define				MESH_NAME				"*Malo.NvMesh"
#define				PLANE_NAME				"*plane*NvMesh"


#define				OFFSCREEN				DPYR_OFF_FRAME_C16
#define				OFFSCREEN_QUAD_SIZE		100

#define DIST_Z 230



struct AppVar
{
	NvGeomShader		*	pDispMesh;
	NvGeomShader		*	pDispPlane;
	NvFrameClear		*	pClear;
	NvMappingShader		*	pDispMeshOffscreen;
	NvFrameClear		*	pClearOffscreen;
	NvMappingShader		*	pEnvMap;
	NvMappingShader		*	pShadow;
	NvFrameFx			*	pframeFX;
	NvBitmap			*	envMap;
	uint					mechSrfCpt;
	uint					planeSrfCpt;
	Box3				*	meshBBox;
	NvGeomShader		*	pDispOffscreen;
	Matrix					centerTR;


	void Init();
	void Shut();
	uint Load();
	
	
	void RenderShadowMap(Matrix & outMappingTransfo, Vec3 & inLightDir, Matrix & inMeshTransfo, Matrix & inPlaneTransfo);
	void DrawOffscreen ();

};


#endif // _APPVAR_H_

