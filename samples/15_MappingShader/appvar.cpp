/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





/* ---------------------------------		AppVar			--------------------------------------------- */
void AppVar::Init()
{
	pDispMesh			= NULL;
	pDispPlane			= NULL;
	pClear				= NULL;
	pDispMeshOffscreen	= NULL;
	pClearOffscreen		= NULL;
	pEnvMap				= NULL;
	pShadow				= NULL;
	pframeFX			= NULL;
	envMap				= NULL;
	mechSrfCpt			= 0;
	planeSrfCpt			= 0;
	meshBBox			= NULL;
	pDispOffscreen		= NULL;
}




uint AppVar::Load()
{
	NvMesh*	pRscMesh	= NULL;
	NvMesh*	pRscPlane	= NULL;

	uint32	meshID		= AppTools::FindUIDByName( MESH_NAME	);
	uint32	envMapID	= AppTools::FindUIDByName( ENV_MAP		);
	uint32	planeID		= AppTools::FindUIDByName( PLANE_NAME	);

	RscPrefetchQ rscQ;
	rscQ.AddRsc(meshID);
	rscQ.AddRsc(envMapID);
	rscQ.AddRsc(planeID);
	
	RscManager::AddPrefetchQ(&rscQ);
	
	// Load Resource
	if(!AppTools::PreparePrefetch(&rscQ))
	{
		NV_WARNING("Cannot create Mesh");
		Shut();
		core::Exit();
		return 0;
	}

	envMap			= NvBitmap::Create			(envMapID);
	pRscMesh		= NvMesh::Create			(meshID);
	pRscPlane		= NvMesh::Create			(planeID);
	NV_ASSERT( envMap && pRscMesh && pRscPlane);

	pDispMesh			= NvGeomShader::Create		(pRscMesh);
	pDispPlane			= NvGeomShader::Create		(pRscPlane);
	pEnvMap				= NvMappingShader::Create	(pRscMesh);
	pShadow				= NvMappingShader::Create	(pRscPlane);
	pClear				= NvFrameClear::Create		();
	pDispMeshOffscreen	= NvMappingShader::Create	(pRscMesh);
	pClearOffscreen		= NvFrameClear::Create		();
	meshBBox			= pRscMesh->GetBBox();
	NV_ASSERT( pClear && pDispMesh && pEnvMap && pDispPlane && pShadow && pClearOffscreen && pDispMeshOffscreen );

	pDispPlane->Enable( NvShader::EN_CLIPPING );
	pShadow->Enable( NvShader::EN_CLIPPING );

	// Set Mapping shader Options :
	mechSrfCpt = pRscMesh->GetSurfaceCpt();
	pEnvMap->HideAllSurfaces();
	for( uint i=0; i<mechSrfCpt; ++i )
	{
		DpyState *	dpys = pEnvMap->GetDisplayState(i);
		dpys->SetSource		( DpySource(envMap)						);
		dpys->SetMode		( DpyState::BM_ADD | DpyState::BS_CTE	);
		dpys->SetBlendSrcCte( 0.4f									);

		// select envmapped surfaces
		if( pRscMesh->GetSurfaceNameCRC(i)==crc::Get( "veste"	 ) )		pEnvMap->ShowSurface( i );
		if( pRscMesh->GetSurfaceNameCRC(i)==crc::Get( "pantalon" ) )		pEnvMap->ShowSurface( i );
		if( pRscMesh->GetSurfaceNameCRC(i)==crc::Get( "dents"	 ) )		pEnvMap->ShowSurface( i );
	}

	for( uint i=0; i<mechSrfCpt; ++i )
	{
		DpyState * dpys = pDispMeshOffscreen->GetDisplayState(i);
		dpys->Disable( DpyState::EN_TEXTURING | DpyState::EN_RD_DEPTH | DpyState::EN_WR_DEPTH );
		Vec4 white(1.0f,1.0f,1.0f,1.0f);
		pDispMeshOffscreen->SetColorFilter(&white);
	}

	planeSrfCpt = pRscPlane->GetSurfaceCpt();
	for( uint i=0; i<planeSrfCpt; ++i )
	{
		DpyState * dpys = pShadow->GetDisplayState(i);		
		dpys->SetAlphaPass(0.0f);
		dpys->SetSource( DpySource(OFFSCREEN) );
		dpys->SetMode( DpyState::BM_SUB | DpyState::BS_CTE | DpyState::TW_CLAMP_CLAMP | DpyState::TM_LINEAR );
		dpys->SetBlendSrcCte( 0.16f );
	}

	pShadow->SetMappingSrc(NvMappingShader::MS_LOC);
	
	//The NvFrameClear object
	pClear->EnableColor( TRUE );
	pClear->EnableDepth( TRUE );
	pClear->SetColor(0x5555AAFF);

	pClearOffscreen->EnableColor( TRUE );
	pClearOffscreen->EnableDepth( FALSE );
	pClearOffscreen->SetColor(0x000000FF);

	// Center screen normal from [-1,+1] to [0,1]
	MatrixZero( &centerTR );
	centerTR.m11 =  0.5f;
	centerTR.m22 = -0.5f;
	centerTR.m33 =  0.5f;
	centerTR.m41 =  0.5f;
	centerTR.m42 =  0.5f;
	centerTR.m43 =  0.5f;
	centerTR.m44 =  1.f;

	SafeRelease(pRscMesh);

	// Create Surface to draw Offscreen Target
	float offWidth  = float(DpyManager::GetRasterWidth ( OFFSCREEN ));
	float offHeight = float(DpyManager::GetRasterHeight( OFFSCREEN ));
	NvSurface * surf = AppTools::BuildQuad(OFFSCREEN_QUAD_SIZE,uint(OFFSCREEN_QUAD_SIZE * (offHeight/offWidth)),DPYR_IN_FRAME);
	pDispOffscreen = NvGeomShader::Create(surf);

	DpyState * dpys = pDispOffscreen->GetDisplayState(0);
	dpys->SetSource	(DpySource(OFFSCREEN));
	dpys->SetMode	(DpyState::CM_TWOSIDED);
	dpys->SetAlphaPass(0.0f);
	dpys->Disable	(DpyState::EN_RD_DEPTH);

	pDispOffscreen->Enable(NvGeomShader::EN_THROUGHMODE);
	pDispOffscreen->SetDrawStart( 0 );
	pDispOffscreen->SetDrawSize ( 4 );

	pframeFX = NvFrameFx::Create();
	NV_ASSERT(pframeFX);
	pframeFX->GenerateFx(1 << NvFrameFx::FX_DOF);


	// dof gradient
	for (int i = 0 ; i < DIST_Z ; ++i )
		pframeFX->SetDOFSharpness(i,255);

	for (int i = DIST_Z ; i < 256 ; ++i )
	{
		uint8 value = 0;
		pframeFX->SetDOFSharpness(i,value);
	}
	
	/*
	for (int i = DIST_Z ; i < 256 ; ++i )
	{
		uint8 value = Max( int(255 - ( 255 * (i-DIST_Z)/(255.0f-DIST_Z))),0);
		pframeFX->SetDOFSharpness(i,value);
	}
	*/
	/*
	for (int i = 0 ; i < 256 ; ++i )
		pframeFX->SetDOFSharpness(i,255-i);
	*/	

	return 1;
}


void AppVar::Shut()
{
	SafeRelease( pDispMesh			);
	SafeRelease( pClear				);
	SafeRelease( pClearOffscreen	);
	SafeRelease( pDispMeshOffscreen	);
	SafeRelease( pEnvMap			);
	SafeRelease( envMap				);
	SafeRelease( pShadow			);
	SafeRelease( pDispOffscreen		);
}






void AppVar::RenderShadowMap(Matrix& outMappingMat, Vec3& inLightDir, Matrix& inMeshWorldMat, Matrix& inPlaneWorldMat )
{
	float meshRadius	  = Vec3Norm( &meshBBox->length ) * 0.5f;
	Vec3  meshLocalCenter = meshBBox->center;
	Vec3  meshWorldCenter = meshLocalCenter * inMeshWorldMat;

	Vec3 light_Z = inLightDir;
	Vec3Normalize( &light_Z, &light_Z );
	Vec3 light_X = Vec3::UNIT_Y ^ light_Z;
	Vec3Normalize( &light_X, &light_X );
	Vec3 light_Y = light_Z ^ light_X;
	Vec3 light_L = meshWorldCenter + light_Z * meshRadius;

	Matrix lightWTR;
	MatrixIdentity( &lightWTR );
	lightWTR.m11 = light_X.x;	lightWTR.m12 = light_X.y;	lightWTR.m13 = light_X.z;
	lightWTR.m21 = light_Y.x;	lightWTR.m22 = light_Y.y;	lightWTR.m23 = light_Y.z;
	lightWTR.m31 = light_Z.x;	lightWTR.m32 = light_Z.y;	lightWTR.m33 = light_Z.z;
	lightWTR.m41 = light_L.x;	lightWTR.m42 = light_L.y;	lightWTR.m43 = light_L.z;

	Matrix tolightWTR;
	MatrixFastInverse( &tolightWTR, &lightWTR );

	float	off_width  = float( DpyManager::GetRasterWidth(OFFSCREEN) );
	float	off_height = float( DpyManager::GetRasterHeight(OFFSCREEN) );
	Vec4	off_viewport( 0, 0, off_width, off_height );

	Matrix	projMat;
	Vec2	clipRange( 0.001f, meshRadius*2.f );
	float	projSx = meshRadius * 2.0f;
	float	projSy = meshRadius * 2.0f;
	if( off_width <= off_height )	projSy *= off_height/off_width;
	else							projSx *= off_width/off_height;

	MatrixOrthoRH( &projMat, projSx, projSy, clipRange.x , clipRange.y ) ;

	// Offscreen rendering :	
	DpyManager::SetTarget		(DpyTarget(OFFSCREEN));
	DpyManager::SetView			(&tolightWTR, &projMat, &clipRange, &off_viewport );

	DpyManager::SetSession		(0);
		DpyManager::Draw			(pClearOffscreen);

	DpyManager::SetSession		(1);
		Vec4 blackColor				(0.0f,0.0f,0.0f,0.0f);
		DpyManager::SetLight		(0,&blackColor,NULL);
		DpyManager::SetLight		(1,&blackColor,(Vec3*)&Vec3::ZERO);			
		DpyManager::SetLight		(2,&blackColor,(Vec3*)&Vec3::ZERO);

		DpyManager::SetWorldTR		(&inMeshWorldMat);
		DpyManager::Draw			(pDispMeshOffscreen);

	outMappingMat = inPlaneWorldMat * tolightWTR * projMat * centerTR;
}

void AppVar::DrawOffscreen ()
{
	uint width		=		 DpyManager::GetRasterWidth	 ( DPYR_IN_FRAME );
	uint height		=		 DpyManager::GetRasterHeight ( DPYR_IN_FRAME );
	float offWidth	= float( DpyManager::GetRasterWidth	 ( OFFSCREEN	 ) );
	float offHeight	= float( DpyManager::GetRasterHeight ( OFFSCREEN	 ) );

	Matrix world;
	Vec3 trans( - float( width /2.0f -  OFFSCREEN_QUAD_SIZE / 2.0f )						,
				- float( height/2.0f - (OFFSCREEN_QUAD_SIZE * (offHeight/offWidth))/2.0f )	,
				0.0f																		);

	MatrixBuildTR(&world, &trans, NULL, NULL);

		DpyManager::SetWorldTR	( &world		 );
		DpyManager::Draw		( pDispOffscreen );
}

