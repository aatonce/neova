/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/





#include "appvar.h"
#include "appctrl.h"







namespace
{
	AppVar appvar;
	AppTools::Camera camera;
}


void InitNeova();
void ShutNeova();

void GameLoop();









void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif

	#if defined(_DX)
	core::SetParameter( core::PR_OFF_FRAME_H, 256U );
	core::SetParameter( core::PR_OFF_FRAME_W, 256U );
	#elif defined(_PS2)
	core::SetParameter( core::PR_OFF_FRAME_H, 128U );
	core::SetParameter( core::PR_OFF_FRAME_W, 128U );
	#endif
	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		core::Exit();
		return;
	}
	
	if (!RscManager::Init())
	{
		NV_WARNING("Cannot init RscManager");
		core::Shut();
		core::Exit();
		return;
	}
	
	if (!RscManager::OpenBigFile(BF_PATH))
	{
		NV_WARNING("Cannot open BigFile");
		RscManager::Shut();
		core::Shut();
		core::Exit();
		return;
	}
	
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		NV_WARNING("Cannot init DpyManager");
		RscManager::Shut();
		core::Shut();
		core::Exit();
		return;
	}
}



void ShutNeova()
{
	DpyManager::Shut();
	RscManager::Shut();
	
	core::Shut();
}




void GameLoop	(			)
{
	camera.Init();
	
	Vec4 ambient (0.6f, 0.6f, 0.6f, 1.f);		// ambient light
	Vec4 diffuse (0.8f, 0.8f, 0.8f, 1.f);		// diffuse light
	float lightAngle = 0.0f;
	bool showOffscreen = false;
	
	Matrix projMat;
	Vec2 clipRange(0.1f, 100.f);
	float fov = Pi/4;
	MatrixPerspectiveRH( &projMat, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );

	Matrix worldMat;
	MatrixIdentity( &worldMat );
	
	
	
	clock::Time initTime;
	clock::GetTime( &initTime );
	float deltaTime, elapsedTime, lElapsedTime = 0;

	// draw data
	for (;;)
	{
		// core update
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;	
		
		
		// Timing
		{
			clock::Time currTime;			
			clock::GetTime( &currTime );
			
			elapsedTime = currTime 	  - initTime;
			deltaTime 	= elapsedTime - lElapsedTime;
			
			lElapsedTime = elapsedTime;
		}

		
		// Controller
		{
			AppCtrl::AppInputs input;
			if( !AppCtrl::GetController( &input ) )
				break ;

			if( input.dir1.x!=0.0f	   )	camera.Strafe			( input.dir1.x * deltaTime );
			if( input.dir1.y!=0.0f	   )	camera.Translate		( input.dir1.y * deltaTime );
			if( input.dir2.x!=0.0f	   )	camera.HorizontalRotate	( input.dir2.x * deltaTime );
			if( input.dir2.y!=0.0f	   )	camera.VerticalRotate	( input.dir2.y * deltaTime );
			if( input.button1_pressed  )	camera.Init				();
			if( input.button0_pressed  )	showOffscreen = !showOffscreen;
			
			if( input.fltButton4!=0.0f )	lightAngle	+= input.fltButton4 * deltaTime;
			if( input.fltButton5!=0.0f )	lightAngle	-= input.fltButton5 * deltaTime;
		}
		
		
		// Lights
		Vec3 lightDir ( Cos(lightAngle), -0.3f , Sin(lightAngle) );
		Vec3Normalize ( &lightDir, &lightDir );
		
		//Set the viewport dimensions, proj transfo, world transfo, and clipping range
		Vec4 viewport( 0, 0, float(DpyManager::GetRasterWidth(DPYR_IN_FRAME)), float(DpyManager::GetRasterHeight(DPYR_IN_FRAME))	);
		
		Matrix invViewMat;
		camera.GetView( &invViewMat );						// Get camera view matrix 
		MatrixFastInverse( &invViewMat, &invViewMat );


		// Build the mapping Transformation
		// Transform normal mesh space to screen space without translation
		Matrix toscreenTR;
		MatrixMul( &toscreenTR, &worldMat, &invViewMat );
		toscreenTR.m41 = 0.f;
		toscreenTR.m42 = 0.f;
		toscreenTR.m43 = 0.f;
		toscreenTR.m44 = 1.f;

		Matrix mapTR;
		MatrixMul( &mapTR, &toscreenTR, &appvar.centerTR );

		// Set mesh mapping transformation 
		appvar.pEnvMap->SetMappingTR( &mapTR );
		appvar.pEnvMap->SetMappingSrc( NvMappingShader::MS_NORMAL );
		
		
	
		//Begin frame
		DpyManager::BeginFrame();

			Matrix mappingTransfo;
			appvar.RenderShadowMap			( mappingTransfo, lightDir , worldMat ,worldMat);		
			appvar.pShadow->SetMappingTR	( &mappingTransfo );

			DpyManager::SetSession(2);
				DpyManager::SetTarget	( DpyTarget(DPYR_IN_FRAME)						);	// Target
				DpyManager::SetView		( &invViewMat, &projMat, &clipRange, &viewport	);	// Set ViewProj matrix
				DpyManager::Draw		( appvar.pClear									);	// clear buffer

			//Set a new session for drawing
			DpyManager::SetSession(3);
				DpyManager::SetLight	( 0, &ambient,	NULL							);
				DpyManager::SetLight	( 1, &diffuse,	&lightDir						);
				DpyManager::SetLight	( 2, NULL	 ,	(Vec3*)&Vec3::ZERO				);

				DpyManager::SetWorldTR	( &worldMat										);	// Set World matrix
				DpyManager::Draw		( appvar.pDispPlane								);	// Draw Plane
				DpyManager::Draw		( appvar.pShadow								);	// Add Shadow with shadow mapping 
				DpyManager::SetWorldTR	( &worldMat										);	// Set World matrix
				DpyManager::Draw		( appvar.pDispMesh								);	// Draw Mesh with GeomShader
				DpyManager::Draw		( appvar.pEnvMap								);	// Draw mesh with Mapping Shader

				if( showOffscreen )
					appvar.DrawOffscreen(												);	// Draw Offscreen in a quad

				//DpyManager::Draw		( appvar.pframeFX								);	// DOF


		//End and flush frame
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_15"


void	GameMain	(		)
{
	// Init
	InitNeova();
	
	appvar.Init();
	if ( !appvar.Load() )
	{
		appvar.Shut();
		return;
	}

	// GameLoop
	GameLoop();
	
	

	// Shut
	appvar.Shut();
	ShutNeova();
}

