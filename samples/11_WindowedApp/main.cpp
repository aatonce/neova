/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"

#include "tools.h"




// this sample is only available on Win32
#if defined(_WIN32)

#include <Kernel/PC/NvWindows.h>
#define BF_PATH			"BIGFILE_DX"


void DoDrawing	(			)
{
	AppVar appvar;

	appvar.Init();
	appvar.Load();

	// draw data
	for (;;)
	{
		core::Update();
		if( UpdateApplication() )
			break;
		
		for( uint i=0; i<4; ++i )
		{

			if( hwndClosed[i] )
				continue ;

			// Change window
			SetHwnd(i);

			//Begin frame
			DpyManager::BeginFrame();
			
			//Set a first drawing session
			//This session is used to clear the frame buffer
			//and all useful 3D buffers set in the NvClear2D object
			DpyManager::SetSession(0);

			//Set the target buffer
			//Here this is the frame buffer
			DpyManager::SetTarget(DpyTarget(DPYR_IN_FRAME));
			
			//Set the viewport dimensions, proj transfo, world transfo, and clipping range
			Vec4 viewport( 0, 0, DpyManager::GetRasterWidth(DPYR_IN_FRAME), DpyManager::GetRasterHeight(DPYR_IN_FRAME)	);
			Matrix invWorldTransfo;
			MatrixIdentity(&invWorldTransfo);
			Vec2 clipRange(1, 100);
			float fov = Pi/4;
			Matrix projTransfo;
			MatrixPerspectiveRH( &projTransfo, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );
			DpyManager::SetView( &invWorldTransfo, &projTransfo, &clipRange, &viewport );

			//Clear the frame
			DpyManager::Draw(appvar.pClear);
			
			//Set a new session for drawing the mesh
			DpyManager::SetSession(1);
			
			//Set the local transfo of the mesh
			Matrix transfo;
			Vec3 offset;
			static float angleAdd = 0.0f;
			angleAdd += 0.00005f;
			Quat rotAdd  = Quat(angleAdd,Vec3(0.0f,1.f,0.0f));
			Quat rot;
			switch (i) 
			{
				case 0:
					rot=Quat(0.0f,0.0f,0.0f,1.f) * rotAdd;
					offset=Vec3(0.0f, -0.8f, -20.f);
					break;
				case 1:
					rot=Quat(HalfPi,Vec3(0.0f,1.f,0.0f)) * rotAdd;
					offset=Vec3(0.0f, -0.8f, -20.f);
					break;
				case 2:
					rot=Quat(-HalfPi,Vec3(0.0f,1.f,0.0f)) * rotAdd;
					offset=Vec3(0.0f, -0.8f, -20.f);
					break;
				case 3:
					rot=Quat(HalfPi,Vec3(1.f,0.0f,0.0f)) * rotAdd;
					offset=Vec3(0.0f, 0.0f, -20.f);
					break;
			}
							
			MatrixBuildTR(&transfo, &offset,&rot, NULL );
			DpyManager::SetWorldTR(&transfo);
			
			//Draw the mesh
			DpyManager::Draw(appvar.pDispMesh);
			
			//End and flush frame
			DpyManager::EndFrame();
			DpyManager::FlushFrame();
		}
	}

	appvar.Shut();
}


// Placement new operator needed by the CoreEngine !
void*	operator new	( unsigned int, void* ptr )	{ return ptr; }



int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE, char* strCmdLine, int )
{
	NV_COMPILE_TIME_CHECK_TYPES();
	NVAPP_CORE_STARTUP();

	core::SetParameter( core::PR_DX_FULLSCREEN, FALSE );

	if (! InitApp() )
	{
		NV_WARNING("Cannot init application");
		return -1;
	}

	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		return -1;
	}
	
	if (!RscManager::Init())
	{
		core::Shut();
		NV_WARNING("Cannot init RscManager");
		return -1;
	}
	
	if (!RscManager::OpenBigFile(BF_PATH))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot open BigFile");
		return -1;
	}
	
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot init DpyManager");
		return -1;
	}

	DoDrawing();
		
	DpyManager::Shut();
	RscManager::Shut();
	
	ShutApp ();

	core::Shut();
	return 0;
}

#else


#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_11"


void GameMain()
{
}


#endif


