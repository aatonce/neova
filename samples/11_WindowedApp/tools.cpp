/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#if defined(_WIN32)


#include <Kernel/PC/NvWindows.h>
#include <Nova.h>

/*
#define WINDOW_SIZEX	320
#define WINDOW_SIZEY	240
*/

#define WINDOW_SIZEX	600
#define WINDOW_SIZEY	450

using namespace nv;

bool			hwndClosed[4]	= {FALSE,FALSE,FALSE,FALSE};	

namespace
{
	HWND		hwnd[4]			= {0,0,0,0};	
	bool		exitApp			= FALSE;
	TCHAR		szWindowClass[]	= "SAMPLE_11";
	
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		switch( message )
		{
			case WM_QUIT :
			case WM_DESTROY :
			case WM_CLOSE :
				exitApp = TRUE;
				for (uint i = 0 ; i < 4 ; ++i ) {					
					if (hWnd == hwnd[i]){
						hwndClosed[i] = TRUE;
						ShowWindow( hWnd, SW_HIDE);
					}
					if (!hwndClosed[i] )
						exitApp = FALSE;
				}
				return 0;			
			case WM_ACTIVATE :
				{
					bool fullscreen;
					nv::core::GetParameter( nv::core::PR_DX_FULLSCREEN, &fullscreen );
					if( fullscreen ) {
						if( LOWORD(wParam) == WA_INACTIVE )
							ShowWindow( hWnd, SW_HIDE|SW_MINIMIZE );
						else
							ShowWindow( hWnd, SW_SHOWNORMAL );
					}
				}
				break;
			case WM_SYSCOMMAND :
				if( wParam == SC_SCREENSAVE ) {					
					return 1;
					break;
				}
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}


bool InitApp()
{
	HINSTANCE hInstance;
	GetParameter( core::PR_W32_HINSTANCE, (uint32*)&hInstance );
	NV_ASSERT( hInstance );

	// Register the window class
	WNDCLASS wndClass = {	CS_HREDRAW | CS_VREDRAW | CS_OWNDC,
							(WNDPROC)WndProc, 
							0, 0,
							hInstance,
							NULL, NULL,
							HBRUSH(COLOR_BACKGROUND),
							NULL,
							szWindowClass						};

	RegisterClass(&wndClass);

	MSG msg;
	PeekMessage( &msg, NULL, 0U, 0U, PM_NOREMOVE );

	// create window
	for( uint i=0; i<4; ++i )
	{
		hwnd[i]	= CreateWindowEx(	WS_EX_APPWINDOW,
									szWindowClass,
									"",
									WS_OVERLAPPEDWINDOW ,
									0, 0,
									16, 16,
									0L, 0L, hInstance, 0L );
		if( !hwnd[i])
		{
			NV_ERROR( "Error: Failed to create application's windows !" );
			return FALSE;
		}
	}

	//give a first window
	core::SetParameter( core::PR_W32_HWND			, uint32(hwnd[0])	);
	core::SetParameter( core::PR_W32_HWND_MANAGED	, FALSE				);	

	// Set window position and size
	SetWindowPos( hwnd[0], 0, 0				, 0				, WINDOW_SIZEX, WINDOW_SIZEY, 0 );
	SetWindowPos( hwnd[1], 0, WINDOW_SIZEX	, 0				, WINDOW_SIZEX, WINDOW_SIZEY, 0 );
	SetWindowPos( hwnd[2], 0, 0				, WINDOW_SIZEY	, WINDOW_SIZEX, WINDOW_SIZEY, 0 );
	SetWindowPos( hwnd[3], 0, WINDOW_SIZEX	, WINDOW_SIZEY	, WINDOW_SIZEX, WINDOW_SIZEY, 0 );

	for( uint i=0; i<4; ++i )
	{
		ShowWindow( hwnd[i], SW_SHOWNORMAL );
		UpdateWindow( hwnd[i]);
	}

	return TRUE;
}

bool UpdateApplication()
{
	// Window Proc
	for( ;; )
	{
		MSG  msg;
		bool wait = FALSE;
		while( PeekMessage(&msg,NULL,0,0,PM_REMOVE) )
		{
			DispatchMessage(&msg);
		}
		for (uint i=0; i<4; ++i )
		{
			if( IsWindow(hwnd[i]) && IsIconic(hwnd[i]) )
			{
				wait = TRUE;
				WaitMessage();
			}
		}
		if( !wait )
			break;
	}

	return exitApp ;
}

void SetHwnd (	int i	)
{
	NV_ASSERT(i >= 0 && i < 4);
	// set Hwnd[i] as front buffer
	if (!hwndClosed[i])
		core::SetParameter( core::PR_W32_HWND, uint32(hwnd[i]) );
}

void ShutApp (			)
{
	for (uint i = 0 ; i < 4 ; ++i)
	{
		if (hwnd[i])
		{
			DestroyWindow(hwnd[i]);
			hwnd[i] = 0;
		}
	}
}


#endif

