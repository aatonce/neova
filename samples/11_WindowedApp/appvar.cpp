/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





/* ---------------------------------		AppVar			--------------------------------------------- */
void AppVar::Init()
{
	pClear		= NULL;
	pRscMesh	= NULL;
	pDispMesh	= NULL;
}




uint AppVar::Load()
{
	//Get the resource ID of the first NvMesh
	//found in the big file
	uint32 meshID = RscManager::FindRscByType( NvMesh::TYPE );

	RscPrefetchQ rscQ;
	rscQ.AddRsc(meshID);
	RscManager::AddPrefetchQ(&rscQ);
	
	
	
	//The NvFrameClear object
	pClear = NvFrameClear::Create();
	pClear->EnableColor( TRUE );
	pClear->EnableDepth( TRUE );
	pClear->SetColor(0x5555AAFF);


	// Load Resource
	if(!PreparePrefetch(&rscQ))
	{
		NV_WARNING("Cannot create Mesh");
		Shut();
		core::Exit();
		return 0;
	}


	pRscMesh	= NvMesh::Create(meshID);
	pDispMesh	= NvGeomShader::Create(pRscMesh);


	
	return 1;
}


void AppVar::Shut()
{
	SafeRelease( pDispMesh );
	SafeRelease( pRscMesh );
	SafeRelease( pClear );
}




uint PreparePrefetch( RscPrefetchQ* _inPrefetchQ )
{
	//prefetching process is asynchronous and requires several updates of the Core and of the RscManager manager
	while(!_inPrefetchQ->IsReady())
	{
		if (core::Update() & (core::UPD_EXIT_ASKED|core::UPD_EXITED))
			return 0;
		
		RscManager::Update(10);
	}
	
	if (_inPrefetchQ->GetState() != RscPrefetchQ::COMPLETED)
	{
		RscManager::Unload(_inPrefetchQ);
		NV_WARNING("Mesh loading failed");
		return 0;
	}
	
	return 1;
}


