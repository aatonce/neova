/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "appvar.h"



namespace
{
	AppVar appvar;
}





void InitNeova();
void ShutNeova();

void GameLoop();






void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif

	// Initialize the core engine
	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		return;
	}

	// Initialize the display manager
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		NV_WARNING("Cannot init DpyManager");
		return;
	}
}


void ShutNeova()
{
	DpyManager::Shut();
	RscManager::Shut();
	core::Shut();
}



void GameLoop()
{
	// Proj Mat
	Vec2  clipRange( 1.0f, 500.0f );
	Matrix projMat;
	MatrixPerspectiveRH( &projMat, HalfPi, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );
	
	
	// World Mat
	Vec3 	worldPos( 0.0f, -60.0f, 0.0f );
	Quat	worldRot( 2.35f, Vec3(0,1,0) );
	
	Matrix 	worldMat;
	MatrixBuildTR( &worldMat,	&worldPos,
								&worldRot,
								NULL 		);
	
	
	const static float deltaTrs = 2.5f;
	const static float deltaVpt = 0.5f;
		
	for( ;; )
	{
		// core update
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
		
		

		clock::Time t;
		clock::GetTime(&t);
		
		float camD 	 = 3.0f;		
		Vec3  camPos = Vec3( 0.0f, 0.0f, camD );

		for( uint i=0; i<4; ++i )
		{
			appvar.viewportAA = appvar.viewportFull + Vec4(1,1,-2,-2);
			
			if(i==0)		{ appvar.viewportAA += Vec4(-deltaVpt,	0.0f	,	0.0f,	0.0f ); }
			else if(i==1)	{ appvar.viewportAA += Vec4( deltaVpt,	0.0f	,	0.0f,	0.0f ); }
			else if(i==2)	{ appvar.viewportAA += Vec4( 0.0f	 , -deltaVpt,	0.0f,	0.0f ); }
			else if(i==3)	{ appvar.viewportAA += Vec4( 0.0f	 ,	deltaVpt,	0.0f,	0.0f ); }


			Quat  camRot 	= Quat::UNIT;
			Matrix camM, icamM;
			MatrixBuildTR( &camM, &camPos, &camRot, NULL );
			MatrixFastInverse( &icamM, &camM );

			DpyManager::BeginFrame	();
				DpyManager::SetSession		(i*3+0);
					DpyManager::SetTarget 	( DpyTarget(DPYR_IN_FRAME) 							);
					DpyManager::SetView		( NULL, NULL, NULL, &appvar.viewportFull 			);
					DpyManager::Draw		( appvar.clearObject 								);
					
				DpyManager::SetSession		(i*3+1);
					DpyManager::SetView		( &icamM, &projMat, &clipRange, &appvar.viewportAA 	);
					DpyManager::SetWorldTR	( &worldMat										 	);
					DpyManager::Draw		( appvar.fractalShader							 	);
					
				DpyManager::SetSession		(i*3+2);
					DpyManager::SetView		( NULL, NULL, NULL, &appvar.viewportFull 			);
					DpyManager::Draw		( appvar.fetchers[i]								);
			
			DpyManager::EndFrame	();
			DpyManager::FlushFrame	( FALSE );
		}

		DpyManager::BeginFrame	();
			DpyManager::SetSession(0);
				DpyManager::SetTarget	( DpyTarget(DPYR_IN_FRAME) 							);
				DpyManager::SetView		( NULL, NULL, NULL, &appvar.viewportFull			);
				DpyManager::Draw		( appvar.aaShader									);
		DpyManager::EndFrame	();
		DpyManager::FlushFrame	();
		DpyManager::SyncFrame	();
	}
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_18"

void GameMain()
{
	InitNeova();
	
	appvar.Init();
	if ( !appvar.Load() )
	{
		appvar.Shut();
		return;
	}
	
	
	GameLoop();
		
		
	// Shut
	appvar.Shut();
	ShutNeova();
}
