/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





/* ---------------------------------		AppVar			--------------------------------------------- */
void AppVar::Init()
{
	clearObject		= NULL;
	clearObject2	= NULL;
	
	fractalShader	= NULL;
	aaShader		= NULL;
}



uint AppVar::Load()
{
	// Create the surface instance
	fractalShader	= AppTools::CreateFractal();
	aaShader		= NvAAShader::Create();
	

	
	// Setup display states
	DpyState* dpystate = NULL;
	dpystate = fractalShader->GetDisplayState( 0 );
	if (dpystate)
	{
	    dpystate->SetMode( DpyState::CM_TWOSIDED );
	    dpystate->SetEnabled( DpyState::EN_WR_COLOR |
	    					  DpyState::EN_WR_DEPTH |
	    					  DpyState::EN_RD_DEPTH	);
//	 	NvBitmap* bmp = LoadBitmap( ... );
//		dpystate->SetSource( bmp );
//		SafeRelease( bmp );
	}
	
	
	float W = float(int(DpyManager::GetRasterWidth(DPYR_IN_FRAME)));
	float H = float(int(DpyManager::GetRasterHeight(DPYR_IN_FRAME)));
	
	viewportFull = Vec4(0, 0, W, H);
	viewportAA   = Vec4(0, 0, W, H);
	
	for (uint i=0; i<4; ++i)
	{
		fetchers[i] = NvFrameFetch::Create(W,H);
		aaShader->AddFrame( i, fetchers[i]->GetFrameData() );
	}
	
	
	// Set NvFrameClear properties: Buffers to clear on each frame
	clearObject	 = NvFrameClear::Create();
	clearObject2 = NvFrameClear::Create();
	
	clearObject ->SetColor( 0x2222AAFF );
	clearObject2->SetColor( 0x333333FF );
	
	
	return 1;
}


void AppVar::Shut()
{
	SafeRelease( fractalShader	);
	SafeRelease( clearObject	);
	SafeRelease( clearObject2	);
	SafeRelease( aaShader		);
}



