/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "appdef.h"








/* ---------------------------------		Tools Function			--------------------------------------------- */


NvGeomShader* AppTools::CreateFractal()
{
	const int		fwidth  = 100;
	const int		fheight = 100;
	const float		fup     = 80.0f;
	const Vec3		fscale( 500.f, 1.0f, 500.f );

	vector<float> fhA( fwidth*fheight );
	vector<Vec2>  ftA( fwidth*fheight );
	vector<Vec3>  fvA( fwidth*fheight );
	vector<Vec3>  fnA( fwidth*fheight );

	for( int fw = 0 ; fw < fwidth ; fw++ ) {
		for( int fh = 0 ; fh < fheight ; fh++ ) {
			fhA[fw+fh*fwidth] = nv::libc::Randf() * fup;
		}
	}

	for( uint s = 0 ; s < 4 ; s++ ) {
		for( int fw = 0 ; fw < fwidth ; fw++ ) {
			for( int fh = 0 ; fh < fheight ; fh++ ) {
				float avg = 0.0f;
				float fac = 0.0f;
				if( fw > 0 ) {
					avg += fhA[(fw-1)+fh*fwidth];
					fac += 1.0f;
				}
				if( fw < fwidth-1 ) {
					avg += fhA[(fw+1)+fh*fwidth];
					fac += 1.0f;
				}
				if( fh > 0 ) {
					avg += fhA[fw+(fh-1)*fwidth];
					fac += 1.0f;
				}
				if( fh < fheight-1 ) {
					avg += fhA[fw+(fh+1)*fwidth];
					fac += 1.0f;
				}
				fhA[fw+fh*fwidth] = avg / fac;
			}
		}
	}

	for( int fw = 0 ; fw < fwidth ; fw++ ) {
		for( int fh = 0 ; fh < fheight ; fh++ ) {
			int i = fw+fh*fwidth;
			float x = float(fw) / float(fwidth-1);
			float z = float(fh) / float(fheight-1);
			Vec3  l( x, fhA[i], z );
			ftA[i] = Vec2( x, z );
			Vec3Mul( &fvA[i], &l, &fscale );
		}
	}

	for( int fw = 0 ; fw < fwidth ; fw++ ) {
		for( int fh = 0 ; fh < fheight ; fh++ ) {
			int i = fw+fh*fwidth;
			if( fw==0 || fh==0 || fh==fheight-1 || fw==fwidth-1 )
			{
				fnA[i] = Vec3::UNIT_Y;
			}
			else
			{
				Vec3 va = fvA[ i-fwidth ];
				Vec3 vb = fvA[ i+fwidth ];
				Vec3 vc = fvA[ i-1 ];
				Vec3 vd = fvA[ i+1 ];
				Vec3 dx = vd - vc;
				Vec3 dz = va - vb;
				Vec3 n  = dx ^ dz;
				Vec3Normalize( &n, &n );
				fnA[i] = n;
			}
		}
	}

	uint N = (fwidth*2*(fheight-1));

	NvSurface* surf = NvSurface::Create( N, NvSurface::CO_LOC|NvSurface::CO_TEX|NvSurface::CO_COLOR|NvSurface::CO_KICK );
	if( !surf )
		return NULL;

	// No need DBF here !

	surf->Begin();
	uint vi = 0;
	for( int fh = 0 ; fh < fheight-1 ; fh++ ) {
		for( int fw = 0 ; fw < fwidth ; fw++ ) {
			int i0 = fw+fh*fwidth;
			int i1 = fw+(fh+1)*fwidth;
			Vec3 v00 = fvA[i0];
			Vec3 v01 = fvA[i1];
			Vec2   t0 = ftA[i0];
			Vec2   t1 = ftA[i1];
			int    l0  = int( fnA[i0].y * 255.f );
			int    l1  = int( fnA[i1].y * 255.f );
			uint32 c0  = GetDpyColor( PSM_ABGR32, l0, l0, l0, 255 );
			uint32 c1  = GetDpyColor( PSM_ABGR32, l1, l1, l1, 255 );
			surf->Color( c0, PSM_ABGR32 );
			surf->TexCoord( t0 );
			surf->Vertex( v00, (fw>1) );
			surf->Color( c1, PSM_ABGR32 );
			surf->TexCoord( t1 );
			surf->Vertex( v01, (fw>1) );
			vi += 2;
		}
	}
	NV_ASSERT( vi == N );
	surf->End();

	NvGeomShader* shd = NvGeomShader::Create( surf );
	SafeRelease( surf );
	shd->SetDrawSize( N );

	return shd;
}





