/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _APPDEF_H_
#define _APPDEF_H_




#include <Nova.h>
#include <NvDpyManager.h>		// Display manager
#include <NvFrameClear.h>		// NvFrameClear interface needed to reset all 3D buffers
#include <NvMesh.h>				// Mesh resource interface
#include <NvSurface.h>			// Immediate surface resource interface
#include <NvGeomShader.h>		// Drawable mesh interface
#include <NvAAShader.h>
#include <NvFrameFetch.h>

using namespace nv;




namespace AppTools
{
	NvGeomShader*	CreateFractal		(													);
}


#endif // _APPDEF_H_