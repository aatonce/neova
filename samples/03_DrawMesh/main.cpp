/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"


namespace
{
	AppVar appvar;
}


void InitNeova();
void ShutNeova();

void GameLoop();





void InitNeova()
{
	
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif
	
	
	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		core::Exit();
	}
	
	if (!RscManager::Init())
	{
		NV_WARNING("Cannot init RscManager");
		core::Shut();
		core::Exit();
	}
	
	if (!RscManager::OpenBigFile(BF_PATH))
	{
		NV_WARNING("Cannot open BigFile");
		RscManager::Shut();
		core::Shut();
		core::Exit();
	}
	
	//Initialize the display manager
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		NV_WARNING("Cannot init DpyManager");
		RscManager::Shut();
		core::Shut();
		core::Exit();
	}
}

void ShutNeova()
{
	//Shut the display manager
	DpyManager::Shut();

	RscManager::Shut();
	core::Shut();
}



// Compute Matrix and draw the mesh
void GameLoop()
{
	for (;;)
	{
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
		
		
		
		clock::Time curTime;
		clock::GetTime(&curTime);
		

		// Compute the projection matrix
		Matrix projMat;
			
		float fov = Pi/4;
		Vec2 clipRange(1.0f, 100.0f);
		MatrixPerspectiveRH( &projMat, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );
			
			
		// Compute the viewport dimensions
		Vec4 viewport( 0, 0, DpyManager::GetRasterWidth(DPYR_IN_FRAME), DpyManager::GetRasterHeight(DPYR_IN_FRAME)	);
			
			
		// Compute the view matrix
		Matrix invViewMat;
		MatrixIdentity(&invViewMat);
		
		
		// Compute the world matrix of the mesh
		Matrix worldMat;
		Vec3 position( 0, -10.0f, -60.0f );
		float angleRot = float(curTime)/3.0f;
		Quat rotation( angleRot, Vec3(0,1,0) );
		MatrixBuildTR( &worldMat, &position, &rotation, NULL );
		
		
		// Begin frame
		DpyManager::BeginFrame();
	
			// Set the target buffer
			// Here this is the frame buffer
			DpyManager::SetTarget(DpyTarget(DPYR_IN_FRAME));
		

			
			// Send camera matrix to the display manager
			DpyManager::SetView( &invViewMat, &projMat, &clipRange, &viewport );


			// Set a first drawing session
			// This session is used to clear the frame buffer
			// and all useful 3D buffers set in the NvClear2D object
			DpyManager::SetSession(0);
			
			// Clear the frame
			DpyManager::Draw(appvar.pClear);
			
			
			
			// Set a new session for drawing the mesh
			DpyManager::SetSession(1);


			// Send world matrix to the display manager
			DpyManager::SetWorldTR(&worldMat);
			
			// Draw the mesh
			DpyManager::Draw(appvar.pDispMesh);
			
			
		// End and flush frame
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}
}





#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_03"

void GameMain()
{
	// Init
	InitNeova();	
	appvar.Init();
	
	if(!appvar.Load())
		return;
	

	// GameLoop
	GameLoop();
	
	

	// Shut
	appvar.Shut();
	ShutNeova();
}
