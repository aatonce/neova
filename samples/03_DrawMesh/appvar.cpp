/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





void
AppVar::Init()
{
	rscQ 		= NULL;
	pRscMesh 	= NULL;
	pDispMesh 	= NULL;
	
	
	//Get the resource ID of the first NvMesh
	//found in the big file
	meshID = RscManager::FindRscByType( NvMesh::TYPE );

	rscQ = NvGameNew( RscPrefetchQ );
	
	//Add the NvMesh resource to the prefetch queue
	rscQ->AddRsc( meshID );				
	RscManager::AddPrefetchQ( rscQ );
	
	
	//The NvFrameClear object
	pClear = NvFrameClear::Create();
	
	//Set NvFrameClear properties:
	//Buffers to clear on each frame
	pClear->EnableColor( TRUE );
	pClear->EnableDepth( TRUE );
	
	//Background color
	pClear->SetColor( 0x5555AAFF );
}




uint
AppVar::Load()
{
	// Load Resource
	if(!AppTools::PreparePrefetch(rscQ))
	{
		Shut();
		return 0;
	}
	
	//Now that the resource has been prefetched, you can create
	//it using the same resource ID as for the prefetch queue
	pRscMesh = NvMesh::Create( meshID );
	
	//Create the drawable mesh that will use the mesh resource
	pDispMesh = NvGeomShader::Create( pRscMesh );
	
	if( !pRscMesh || !pDispMesh )
	{
		RscManager::Unload( rscQ );
		NV_WARNING( "Cannot create Mesh" );
		
		Shut();
		return 0;
	}
	
	return 1;
}



void
AppVar::Shut()
{
	SafeRelease( pDispMesh );		//Release the drawable mesh
	SafeRelease( pRscMesh );		//Release the Mesh resource
	SafeRelease( pClear );			//Release the NvFrameClear object
	
	NvGameDelete( rscQ );
	rscQ = NULL;
}
