/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _APPVAR_H_
#define _APPVAR_H_


#include "appdef.h"




#define				MESH_NAME				"*Malo.NvMesh"
//#define			MESH_NAME				"*Wolf.NvMesh"
//#define			MESH_NAME				"*l_infant_tolit*NvMesh"
#define				PLANE_NAME				"*plane*NvMesh"
#define				MENU_BITMAP				"*menu_sample_13*tif"




struct AppVar
{
	NvFrameClear	*	pClear;
	NvBitmap		*	rscMenu;
	
	
	NvGeomShader	*	pDispPlane;
	NvCartoonShader	*	pDispMesh;
	NvGeomShader	*	pDispMeshGS;
	NvFrameFx		*	pframeFX;
	
	// Object to draw Shadow :
	NvShadowingShader	*	pShadow1;
	
	// Shadowing flags :
	uint32					shadowFlags;


	void Init();
	void Shut();
	uint Load();

};


#endif // _APPVAR_H_

