/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





/* ---------------------------------		AppVar			--------------------------------------------- */
void AppVar::Init()
{
	pDispPlane	= NULL;
	pDispMesh	= NULL;
	pDispMeshGS	= NULL;
	pframeFX	= NULL;
	pClear		= NULL;
	rscMenu		= NULL;
	pShadow1	= NULL;
	
#ifdef _DX
	shadowFlags	= NvShadowingShader::F_CAP_ALL	 | NvShadowingShader::F_CLEAR_STENCIL | NvShadowingShader::F_DRAW_SHADOWS;
#else
	shadowFlags	= NvShadowingShader::F_CAP_SIDES | NvShadowingShader::F_CLEAR_STENCIL | NvShadowingShader::F_DRAW_SHADOWS;
#endif
}




uint AppVar::Load()
{
	NvMesh*	pRscMesh	= NULL;
	NvMesh*	pRscPlane	= NULL;

	//Get the resource ID of the first NvMesh
	//found in the big file
	uint32	meshID		= AppTools::FindUIDByName( MESH_NAME		);
	uint32	planeID		= AppTools::FindUIDByName( PLANE_NAME		);
	uint32	bmpMenuID	= AppTools::FindUIDByName( MENU_BITMAP	);
	RscPrefetchQ rscQ;
	rscQ.AddRsc( meshID		);
	rscQ.AddRsc( planeID	);
	rscQ.AddRsc( bmpMenuID	);
	RscManager::AddPrefetchQ( &rscQ );
	
	
	
	// Load Resource
	if(!AppTools::PreparePrefetch(&rscQ))
	{
		NV_WARNING("Cannot create Mesh");
		Shut();
		core::Exit();
		return 0;
	}
	


	rscMenu		= NvBitmap::Create			( bmpMenuID	);
	pRscMesh	= NvMesh::Create			( meshID	);
	pRscPlane	= NvMesh::Create			( planeID	);
	NV_ASSERT( rscMenu 		);
	NV_ASSERT( pRscMesh 	);
	NV_ASSERT( pRscPlane 	);

	pDispMesh	= NvCartoonShader::Create	( pRscMesh	);
	pDispMeshGS	= NvGeomShader::Create		( pRscMesh	);
	pDispPlane	= NvGeomShader::Create		( pRscPlane	);
	NV_ASSERT( pDispMesh 	);
	NV_ASSERT( pDispMeshGS 	);
	NV_ASSERT( pDispPlane 	);
	pDispPlane->Enable( NvShader::EN_CLIPPING );

	
	// create the ShadowingShader with a mesh: 
	pShadow1	= NvShadowingShader::Create	(pRscMesh);
	NV_ASSERT( pShadow1 );
	
	// Set Shadow substract color and flags;
	pShadow1->SetFlags(shadowFlags);
	pShadow1->SetColor(0x30303000);
	
	pClear	 = NvFrameClear::Create();
	pframeFX = NvFrameFx::Create();
	NV_ASSERT( pClear 	);
	NV_ASSERT( pframeFX );


/*	// Set Full ambient & diffuse surface filters
	uint surfCpt = pRscMesh->GetSurfaceCpt();
	for (uint i=0; i<surfCpt; ++i )
	{
		pDispMesh->SetAmbient( i, (Vec4*)&Vec4::ONE );
		pDispMesh->SetDiffuse( i, (Vec4*)&Vec4::ONE );
		pDispMeshGS->SetAmbient( i, (Vec4*)&Vec4::ONE );
		pDispMeshGS->SetDiffuse( i, (Vec4*)&Vec4::ONE );
	}
*/
	
	//The NvFrameClear object
	pClear->EnableColor( TRUE );
	pClear->EnableDepth( TRUE );
	pClear->SetColor(0x5555AAFF);

	// Create a frame fx and activate Hard line effect.
	pframeFX->GenerateFx( 1<<NvFrameFx::FX_OUTLINE );


	SafeRelease( pRscMesh  );
	SafeRelease( pRscPlane );
	
	
	return 1;
}


void AppVar::Shut()
{
	SafeRelease( pDispMesh	);
	SafeRelease( pDispPlane	);
	SafeRelease( pDispMeshGS);
	SafeRelease( pClear		);
	SafeRelease( pframeFX	);
	SafeRelease( rscMenu	);
	
	SafeRelease( pShadow1	);
}




