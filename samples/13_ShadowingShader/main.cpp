/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/





#include "appvar.h"
#include "menu.h"
#include "viewContext.h"
#include "appctrl.h"




HUD::MenuRscDesc	menuDesc				= {12,166,21,0,175,350,375,399,420};
#define				MENU_NB_LINE			4
#define				MENU_POS_X				10
#define				MENU_POS_Y				10




namespace
{
	AppVar appvar;
	AppTools::Camera camera;
}


void InitNeova();
void ShutNeova();

void GameLoop();









void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif


	if (!nv::core::Init())
	{
		NV_WARNING("Cannot init core");
		core::Exit();
	}
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot init DpyManager");
		core::Exit();
	}
	if (!RscManager::Init())
	{
		core::Shut();
		NV_WARNING("Cannot init RscManager");
		core::Exit();
	}
	if (!RscManager::OpenBigFile( BF_PATH ))
	{
		core::Shut();
		NV_WARNING("Cannot open OpenBigFile");
		core::Exit();
	}
}



void ShutNeova()
{
	DpyManager::Shut();
	RscManager::CloseBigFile();
	RscManager::Shut();
	nv::core::Shut();
}




void GameLoop	(			)
{
	camera.Init();
	ViewContext viewContext(appvar.rscMenu, menuDesc, MENU_NB_LINE, Vec2(MENU_POS_X,MENU_POS_Y));

	Vec4 ambient (0.6f, 0.6f, 0.6f, 1.f);		// ambient light
	Vec4 diffuse (0.8f, 0.8f, 0.8f, 1.f);		// diffuse light
	float lightAngle = 0.0f;
	
	Matrix projMat;
	Vec2 clipRange(0.1f, 100.f);
	float fov = Pi/4;
	MatrixPerspectiveRH( &projMat, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );

	Matrix worldMat;
	MatrixIdentity( &worldMat );
	
	
	clock::Time initTime;
	clock::GetTime( &initTime );
	float deltaTime, elapsedTime, lElapsedTime = 0;

	// draw data
	for (;;)
	{
		// core update
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;	
		
		
		// Timing
		{
			clock::Time currTime;			
			clock::GetTime( &currTime );
			
			elapsedTime = currTime 	  - initTime;
			deltaTime 	= elapsedTime - lElapsedTime;
			
			lElapsedTime = elapsedTime;
		}

		
		// Controller
		{
			AppCtrl::AppInputs input;
			if( !AppCtrl::GetController( &input ) )
				break ;
			
			
			if( input.dir3U_pressed	   )	viewContext.UpMenu		();
			if( input.dir3D_pressed    )	viewContext.DownMenu	();
			if( input.dir3R_pressed    )	viewContext.ChangeValue	();
			if( input.dir3L_pressed    )	viewContext.ChangeValue	();
			if( input.button9_pressed  )	viewContext.ScreenShot	();
			
			if( input.dir1.x!=0.0f	   )	camera.Strafe			( input.dir1.x * deltaTime );
			if( input.dir1.y!=0.0f	   )	camera.Translate		( input.dir1.y * deltaTime );
			if( input.dir2.x!=0.0f	   )	camera.HorizontalRotate	( input.dir2.x * deltaTime );
			if( input.dir2.y!=0.0f	   )	camera.VerticalRotate	( input.dir2.y * deltaTime );
			if( input.button1_pressed  )	camera.Init				();
			
			
			
			if( input.fltButton4!=0.0f )	lightAngle	+= input.fltButton4 * deltaTime;
			if( input.fltButton5!=0.0f )	lightAngle	-= input.fltButton5 * deltaTime;
		}
		
		
		// Lights
		Vec3 lightDir ( Cos(lightAngle), -0.3f , Sin(lightAngle) );
		Vec3Normalize ( &lightDir, &lightDir );
		
		
		
		// Set Cartoon Shader Options :
		NvDpyObject* drawMesh	;
		NvDpyObject* drawFrameFX;

		if( viewContext.options[ViewContext::OOutLining] )			drawFrameFX = appvar.pframeFX;
		else														drawFrameFX = NULL;
		if( viewContext.options[ViewContext::OCartoonRendering] )	drawMesh = appvar.pDispMesh;
		else														drawMesh = appvar.pDispMeshGS;	


		bool bDrawShadow = viewContext.options[ViewContext::OMakeShadow];
		if( bDrawShadow )
		{
			//Set Shadowing flags
			uint32 flags = (viewContext.options[ViewContext::OShowVolume] ? NvShadowingShader::F_DBG_SHOW_VOLUMES : 0)	| appvar.shadowFlags;
			appvar.pShadow1->SetFlags(flags);
			// Set light for shadowing
			appvar.pShadow1->Setup(NvShadowingShader::T_DIRECTIONAL,lightDir,0.00f,25.0f);
		}
		
		
		//Set the viewport dimensions, proj transfo, world transfo, and clipping range
		Vec4 viewport( 0, 0, float(DpyManager::GetRasterWidth(DPYR_IN_FRAME)), float(DpyManager::GetRasterHeight(DPYR_IN_FRAME))	);
			
		Matrix invViewMat;
		camera.GetView( &invViewMat );						// Get camera view matrix 
		MatrixFastInverse( &invViewMat, &invViewMat );

	
		//Begin frame
		DpyManager::BeginFrame();
		
			DpyManager::SetSession(0);
				DpyManager::SetTarget	( DpyTarget(DPYR_IN_FRAME)						);	// Target
				DpyManager::SetView		( &invViewMat, &projMat, &clipRange, &viewport	);	// Set ViewProj matrix
				DpyManager::Draw		( appvar.pClear									);	// clear buffer
	
	
			DpyManager::SetSession(1);
			// Setup ambient & diffuse lights for cartoon rendering modulation
				DpyManager::SetLight	( 0, &ambient,	NULL							);
				DpyManager::SetLight	( 1, &diffuse,	&lightDir						);
				DpyManager::SetLight	( 2, NULL	 ,	(Vec3*)&Vec3::ZERO				);

				DpyManager::SetWorldTR	( &worldMat										);	// Set World matrix
				DpyManager::Draw		( appvar.pDispPlane								);	// Draw Plane
				DpyManager::SetWorldTR	( &worldMat										);	// Set World matrix
				DpyManager::Draw		( drawMesh										);	// Draw Mesh
				if (bDrawShadow)
				{
					// draw shadow with the same World transformation as GeomShader.
					DpyManager::SetSession(2);			
					DpyManager::Draw(appvar.pShadow1);
				}
				if( drawFrameFX )
				{
					DpyManager::SetSession(3);
						DpyManager::Draw		( drawFrameFX									); 	// Draw FrameFX
				}
			
				
			DpyManager::SetSession(5);
				viewContext.DrawHUD	(													);	// Draw HUD
				
				
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_13"


void	GameMain	(		)
{
	// Init
	InitNeova();
	
	appvar.Init();
	if ( ! appvar.Load() )
	{
		appvar.Shut();
		return;
	}

	// GameLoop
	GameLoop();
	
	

	// Shut
	appvar.Shut();
	ShutNeova();
}

