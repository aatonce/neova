/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include "viewContext.h"

using namespace nv;



ViewContext::ViewContext(NvBitmap * inRscMenu,HUD::MenuRscDesc & inMenuDesc,uint inNbLine , Vec2 inPos)
{
	options[OMakeShadow]		= TRUE;
	options[OCartoonRendering]	= TRUE;
	options[OOutLining]			= FALSE;
	options[OShowVolume]		= FALSE;

	hud.Init(inRscMenu,inMenuDesc,inNbLine,inPos,options);			

	curOption = 0;
}





void ViewContext::ChangeValue()
{
	options[curOption] = !options[curOption];
	hud.Do(HUD::AChangeValue,options[curOption],TRUE);
}

void ViewContext::UpMenu()
{
	bool lastLine = options[curOption];
	curOption = (curOption) ? curOption-1 : ONbOption-1 ;		// Loop

	hud.Do(HUD::AUp,lastLine,options[curOption]);
}

void ViewContext::DownMenu()
{
	bool oldLineValue = options[curOption];
	curOption = ++curOption%ONbOption;							// Loop

	hud.Do(HUD::ADown,oldLineValue,options[curOption]);
}

void ViewContext::ScreenShot()
{
	static int snap_cpt = 0;
	char snap_fn[64];
	Sprintf( snap_fn, "nv_sample_13_%d.tga", snap_cpt++ );
	DpyManager::SaveFrame( snap_fn );
}

void ViewContext::DrawHUD()
{
	if(hud.selectedLine>=0)
	{
		Menu::Draw(hud.menu,hud.position);
	}
}
