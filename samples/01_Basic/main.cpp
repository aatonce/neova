/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


//Include Nova main header
#include <Nova.h>

//Use the Nova's base namespace
using namespace nv;





void ProcessGame()
{
	clock::Time t0, t1;
	clock::GetTime( &t0 );

	//Main game loop
	float t = 0.f, prevt = 0.f;
	int it = -1;
	for (;;)
	{
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;

		//Do your stuff here: controlers, drawing, dynamics, etc...
		clock::GetTime( &t1 );
		float t = t1 - t0;
		float dt = t-prevt;
		prevt = t;
		if( int(t) != it ) {
			it = int(t);
			DebugPrintf( "Elapsed: %ds\n", it );
		}
		if( float(t) > 4.f )
			break;	// exit after 4 s
	}
}


//Program main entry point
#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_01"

void GameMain()
{
	//Initialize the Nova core engine
	if (!core::Init())
	{
		NV_WARNING("Can't initialize Nova kernel");
		return;
	}
	
	ProcessGame();

	//Shut the Nova core engine
	core::Shut();
}
