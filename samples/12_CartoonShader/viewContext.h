/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#ifndef _ViewContext_H_
#define _ViewContext_H_

#include <NvBitmap.h>
#include <NvCore_ctrl.h>
#include "menu.h"

#include "appctrl.h"

struct Value
{
	int		max;
	bool	loop;
};

static const Value	OptionMaxVal[] =
{
	{2	, TRUE	},		//OMVCartoon
	{11	, FALSE	},		//OMVCellShadingLevel
	{2	, TRUE	},		//OMVInking
	{11	, FALSE	},		//OMVInkRampStart
	{11	, FALSE	},		//OMVInkRampEnd
	{2	, TRUE	},		//OMVOutlining
	{11	, FALSE	},		//OMVOutliningThickness
	{11	, FALSE	},		//OMVOutliningQuality
	{2	, TRUE	}		//OMVDepthOutLining
};

struct ViewContext
{
	enum OPTION
	{
		OCartoon			= 0,
		OCellShadingLevel	= 1,
		OInking				= 2,
		OInkRampStart		= 3,
		OInkRampEnd			= 4,
		OOutlining			= 5,
		OOutliningThickness = 6,
		OOutliningQuality	= 7,
		ODepthOutLining		= 8,
		ONbOption			= 9,
	};
	

	int	curOption			;
	int options[ONbOption]	;

	HUD	hud					;		


	
	ViewContext			(	NvBitmap *			inRscMenu	,
							HUD::MenuRscDesc &	inMenuDesc	, 
							Vec2				inPos		);
							
	// Menu navigation
	void UpValue	();
	void DownValue	();
	void UpMenu		();
	void DownMenu	();

	void ScreenShot	();
	
	void DrawHUD	();

};

#endif //_ViewContext_H_
