/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#include "appctrl.h"






bool AppCtrl::GetController( AppCtrl::AppInputs* _outAppInput )
{
	static int ctrl_no = -1;
	
	ctrl::Status* ctrl_stat = NULL;
	ctrl_stat = ctrl::GetStatus( ctrl_no );
	if( !ctrl_stat )
	{
		ctrl_no	  = WaitController();
		ctrl_stat = ctrl::GetStatus( ctrl_no );
		if(!ctrl_stat)
			return false;
	}
	
	{
		static	uint	buttonStatus = 0;
		static	uint	buttonDown	 = 0;
	
	
		uint oldBT		= buttonStatus;
		buttonStatus	= 0;

		for (uint i = 0 ; i < ctrl::BT_MAX ; ++i )
		{
			if (ctrl_stat->Filtered(ctrl::Button(i)))
				buttonStatus |= 1 << i ;
		}
		buttonDown = buttonStatus & (~oldBT);



		_outAppInput->dir3L_pressed 	= (buttonDown & ( 1<<ctrl::BT_DIR3left  ));
		_outAppInput->dir3R_pressed 	= (buttonDown & ( 1<<ctrl::BT_DIR3right ));
		_outAppInput->dir3U_pressed 	= (buttonDown & ( 1<<ctrl::BT_DIR3up	));
		_outAppInput->dir3D_pressed 	= (buttonDown & ( 1<<ctrl::BT_DIR3down  ));
		
		_outAppInput->button1_pressed 	= (buttonDown & ( 1<<ctrl::BT_BUTTON1   ));
		_outAppInput->button2_pressed 	= (buttonDown & ( 1<<ctrl::BT_BUTTON1   ));
		_outAppInput->button9_pressed 	= (buttonDown & ( 1<<ctrl::BT_BUTTON9   ));

		_outAppInput->fltButton4		= ctrl_stat->Filtered( ctrl::BT_BUTTON4 );
		_outAppInput->fltButton5		= ctrl_stat->Filtered( ctrl::BT_BUTTON5 );  	    
		_outAppInput->fltButton6		= ctrl_stat->Filtered( ctrl::BT_BUTTON6 );
		_outAppInput->fltButton7		= ctrl_stat->Filtered( ctrl::BT_BUTTON7 );
		

		
		_outAppInput->dir1 = Vec2( ctrl_stat->Filtered( ctrl::BT_DIR1X ),
								   ctrl_stat->Filtered( ctrl::BT_DIR1Y ));
		_outAppInput->dir2 = Vec2( ctrl_stat->Filtered( ctrl::BT_DIR2X ),
								   ctrl_stat->Filtered( ctrl::BT_DIR2Y ));
	}
	
	return true;
}






int AppCtrl::WaitController()
{
	ctrl::Close();
	ctrl::Open();
	Printf( "Waiting for a controller ...\n" );
	int ctrl_no = -1;
	for(uint i=0 ;;++i)
	{
		if( core::Update() & (core::UPD_EXIT_ASKED | core::UPD_EXITED) )
			break;
		
		ctrl_no = FindController();
		
		if( ctrl_no>=0 )	break;
		else				AppTools::ShowNoPadPlugged(i,FALSE);
	}
	Printf( "Controller #%d acquired !\n", ctrl_no );
	AppTools::ShowNoPadPlugged(0,FALSE);
	
	return ctrl_no;
}



int AppCtrl::FindController()
{
	for( int i=0; i<int(ctrl::GetNbMax()); i++ )
	{
		ctrl::Status* pStat = ctrl::GetStatus(i);
		//Check the controller type
		if( pStat && (pStat->type==ctrl::TP_PADDLE ))
			return i;
	}
	return -1;
}


/*
float AppCtrl::ButtonNormThr	( float	inButtonV, float inThr	)
{
	if( Abs(inButtonV) <= inThr )
		return 0.0f;
	
	float nrm = (1.0f-inThr);
	
	if( inButtonV > 0 )
		return (inButtonV-inThr) / nrm;
	else
		return (inButtonV+inThr) / nrm;
}


bool AppCtrl::IsPressed			( float inButtonV, bool& ioFlag	)
{
	if( inButtonV==0.0f )
	{
		ioFlag = FALSE;
		return FALSE;
	}
	bool _f = ioFlag;
	ioFlag = TRUE;
	return !_f;
}
*/
