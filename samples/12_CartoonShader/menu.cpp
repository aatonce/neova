/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <NvSurface.h>
#include <NvBitmap.h>

#include "menu.h"

#define	VALUE_OFFSET_X	10
#define LINE_OFFSET_Y	2








/* ---------------------------------		Menu			--------------------------------------------- */
NvGeomShader *	Menu::Create	(	NvBitmap *		inMenuRsc	,
									uint			inMaxEntries	) 
{
	if( !inMenuRsc || (inMaxEntries==0) )
		return NULL;
	
	uint nbVertex			= 4 + (inMaxEntries-1) * 6;		// 4 vertex  => the first Quad , 6 vertex for others (kick)
	NvSurface * surface		= NvSurface::Create( nbVertex, NvSurface::CO_LOC | NvSurface::CO_TEX );
	if( !surface )
		return NULL;
	NvGeomShader * shader	= NvGeomShader::Create(surface);
	if( !shader )
	{
		SafeRelease(surface);
		return NULL;
	}

	// set surface and shader properties to draw text in 2d mode
	surface->EnableDBF();

	shader->Enable		( NvGeomShader::EN_THROUGHMODE);
	shader->Disable		( NvShader::EN_RD_DEPTH );
	shader->SetDrawStart( 0 );
	shader->SetDrawSize	( nbVertex );

	DpyState * dpys = shader->GetDisplayState(0);
	NV_ASSERT(dpys);
	DpySource srcBmp(inMenuRsc);

	dpys->SetSource(srcBmp);
	dpys->Enable(DpyState::EN_TEXTURING);
	dpys->SetMode( DpyState::CM_TWOSIDED );
	dpys->SetMode( DpyState::TM_NEAREST | DpyState::TR_NEAREST_MIP0);
	
	return shader;
}

bool Menu::Draw	(	NvGeomShader *	inMenuShader		,	
					Vec2		 &	inPosition			)
{
	if( !inMenuShader )
		return FALSE;

	// set Wtr to translate surface.
	Matrix recTransfo;
	Vec3   recPos( inPosition.x, inPosition.y, 0 );
	MatrixBuildTR( &recTransfo, &recPos, NULL, NULL );

	DpyManager::SetWorldTR(&recTransfo);
	DpyManager::Draw(inMenuShader);
	
	return TRUE;
}

bool Menu::Update	(	NvGeomShader *	inMenuShader	,
						MenuEntryA	 &	inEntries		)
{
	if( !inMenuShader || ( inEntries.size()==0 ) )
		return FALSE;
	
	NvResource* rsc = inMenuShader->GetResource();
	NV_ASSERT_RETURN_MTH( FALSE, rsc->GetRscType()==NvSurface::TYPE );
	NvSurface* surf = ( NvSurface * ) rsc;

	// check surface component 
	NV_ASSERT_RETURN_MTH( FALSE,
						  surf->HasComponent(NvSurface::CO_TEX)		&& 
						  surf->HasComponent(NvSurface::CO_LOC)		);
	NV_ASSERT_RETURN_MTH( FALSE,
						  !surf->HasComponent(NvSurface::CO_COLOR)	&& 
						  !surf->HasComponent(NvSurface::CO_KICK)	&& 
						  !surf->HasComponent(NvSurface::CO_NORMAL)	);

	DpyState *	state = inMenuShader->GetDisplayState(0);
	DpySource	src;
	state->GetSource(src);
	NV_ASSERT_RETURN_MTH(FALSE,src.Bitmap && src.Raster == DPYR_NULL);
	uint32	bmpW = src.Bitmap->GetWidth()	,
			bmpH = src.Bitmap->GetHeight()	;

	// update surface 
	surf->Begin();
	for (uint i=0; i<inEntries.size(); ++i )
	{
		MenuEntry & entry	= inEntries[i];

		Vec2 texTLcorner	= Vec2 ( entry.bmpPos.x / (bmpW) , entry.bmpPos.y / (bmpH) ) ;
		Vec2 texBRcorner	= texTLcorner + Vec2 ( (entry.bmpDim.x) / (bmpW) , (entry.bmpDim.y) / (bmpH) ) ;
		Vec3 posTLCorner	= Vec3	( entry.surfPos );
		Vec3 posBRCorner	= posTLCorner + Vec3 ((entry.bmpDim.x) , (entry.bmpDim.y), 0 ) ;		
		Vec2 TexOffset		  (0.5f / float(bmpW) , 0.5f / float(bmpW));

		Vec2 tex;
		Vec3 pos;
		pos.z = 0;

		if (i != 0 )						// kick vertex 
		{
			surf->TexCoord	(texTLcorner+TexOffset);
			surf->Vertex	(posTLCorner);
		}

		surf->TexCoord	(texTLcorner+TexOffset);
		surf->Vertex	(posTLCorner);

		tex.x = texBRcorner.x;		tex.y = texTLcorner.y;
		pos.x = posBRCorner.x;	pos.y = posTLCorner.y;
		surf->TexCoord	(tex+TexOffset);
		surf->Vertex	(pos);

		tex.x = texTLcorner.x;		tex.y = texBRcorner.y;
		pos.x = posTLCorner.x;		pos.y = posBRCorner.y;
		surf->TexCoord	(tex+TexOffset);
		surf->Vertex	(pos);

		tex.x = texBRcorner.x;		tex.y = texBRcorner.y;
		pos.x = posBRCorner.x;	pos.y = posBRCorner.y;
		surf->TexCoord	(tex+TexOffset);
		surf->Vertex	(pos);

		if (i != inEntries.size() -1 )		// kick vertex 
		{
			tex.x = texBRcorner.x;		tex.y = texBRcorner.y;
			pos.x = posBRCorner.x;	pos.y = posBRCorner.y;
			surf->TexCoord	(tex+TexOffset);
			surf->Vertex	(pos);
		}
	}
	surf->End();

	return TRUE;
}




/* ---------------------------------		HUD				--------------------------------------------- */
HUD::HUD			(										)
{
	selectedLine = -1;
	menu = NULL;
}

bool
HUD::Init			(	NvBitmap	*	inMenuRsc			,										
						MenuRscDesc &	inMenuDesc			,
						uint			inNbEntry			,
						Vec2			inPosition			,
						int			*	inValuesA			)
{
	NV_ASSERT_RETURN_MTH(FALSE,inNbEntry >=0 && inValuesA && inMenuRsc);
	
	uint32	bmpW = inMenuRsc->GetWidth(),
			bmpH = inMenuRsc->GetHeight();
	
	uint32 maxHeight = (inNbEntry * inMenuDesc.inEntryHeight)-1;
	NV_ASSERT_RETURN_MTH(FALSE,maxHeight <= bmpH );

	menu = Menu::Create(inMenuRsc,inNbEntry*2);
	NV_ASSERT_RETURN_MTH(FALSE,menu);

	selectedLine = 0;
	menuRscDesc = inMenuDesc;
	entries.resize(inNbEntry * 2);

	for (uint i = 0 ; i < inNbEntry ; ++i)
		ConstructLine(menuRscDesc,i,(i==selectedLine)?TRUE:FALSE,inValuesA[i],entries[i*2],entries[(i*2)+1]);

	position = inPosition;
	Menu::Update(menu,entries);

	return TRUE;
}

HUD::~HUD		(										)
{
	SafeRelease( menu );
}

/*
void 
HUD::Draw	(										)
{
	NV_ASSERT(  );
}
*/

void 
HUD::ConstructLine	(	MenuRscDesc &	inMenuDesc		,
						uint			inLineNum		,
						bool			inIsSelected	,
						int				inValue			,
						MenuEntry	&	outEntryText	,
						MenuEntry	&   outEntryValue	)
{
	float textHeight	= float(inLineNum * 2       * (inMenuDesc.inEntryHeight ));
	float menuHeight	= float(((inLineNum * 2)+1) * (inMenuDesc.inEntryHeight ));
	float surfHeight	= float(inLineNum * (inMenuDesc.inEntryHeight + LINE_OFFSET_Y));
	float textWidth		= float(inMenuDesc.inTextWidth);
	float lineHeight	= float(inMenuDesc.inEntryHeight);
	float textPos		= float(inIsSelected?	inMenuDesc.inTextHposx : inMenuDesc.inTextposx);
	float valuePos		= float(((inValue*2) * inMenuDesc.inValueWith) + inMenuDesc.inValueStart + ((inIsSelected)?inMenuDesc.inValueWith : 0 ));
	float valueWitdh	= float(inMenuDesc.inValueWith);

	// CreateText
	outEntryText.surfPos	= Vec2(0,surfHeight);	
	outEntryText.bmpPos		= Vec2(textPos,textHeight);
	outEntryText.bmpDim		= Vec2(textWidth,lineHeight);

	// CreateValue
	outEntryValue.surfPos	= Vec2((textWidth) + VALUE_OFFSET_X,surfHeight);
	outEntryValue.bmpPos	= Vec2(valuePos,menuHeight);
	outEntryValue.bmpDim	= Vec2(valueWitdh,lineHeight);
}

void 
HUD::Do		(	Action			inAction			,
				int				inOldLineValue1		,
				int				inNewLineValue1		)
{
	if( selectedLine<0 )
		return ;

	int line1 = selectedLine,
		line2 = -1;

	switch( inAction )
	{
		case HUD::AChangeValue :
			break;
		case HUD::ADown:
			selectedLine++;
			if( selectedLine>=int(entries.size()/2.0f) )
				selectedLine = 0;
			line2 = selectedLine;
			break;
		case HUD::AUp:
			selectedLine--;
			if( selectedLine<0 )
				selectedLine= (entries.size()/2.0f) -1;			
			line2 = selectedLine;
			break;
	}

	ConstructLine(menuRscDesc,line1,(line1==selectedLine)?TRUE:FALSE,inOldLineValue1,entries[line1*2],entries[(line1*2) +1 ]);
	
	if (line2>=0)
		ConstructLine(menuRscDesc,line2,(line2==selectedLine)?TRUE:FALSE,inNewLineValue1,entries[line2*2],entries[(line2*2)+1]);
	
	Menu::Update(menu,entries);
}



