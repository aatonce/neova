/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include "viewContext.h"

using namespace nv;



ViewContext::ViewContext(NvBitmap * inRscMenu,HUD::MenuRscDesc & inMenuDesc, Vec2 inPos)
{
	options[ OCartoon			 ] = 1;
	options[ OCellShadingLevel	 ] = 3;
	options[ OInking			 ] = 1;
	options[ OInkRampStart		 ] = 1;
	options[ OInkRampEnd		 ] = 2;
	options[ OOutlining			 ] = 1;
	options[ OOutliningThickness ] = 5;
	options[ OOutliningQuality	 ] = 0;
	options[ ODepthOutLining	 ] = 0;
	
	
	hud.Init( inRscMenu, inMenuDesc, ONbOption, inPos, options );				

	
	curOption = 0;
}





void ViewContext::UpValue()
{
	options[curOption] ++;
	if( options[curOption]>=OptionMaxVal[curOption].max )
	{
		if (OptionMaxVal[curOption].loop) 
			options[curOption] %= OptionMaxVal[curOption].max;
		else
			options[curOption] = OptionMaxVal[curOption].max - 1;
	}
	if( curOption==OInkRampStart )
	{
		if( options[OInkRampStart]>options[OInkRampEnd] )
			options[OInkRampStart]--;
	}
	hud.Do( HUD::AChangeValue, options[curOption], TRUE );
}


void ViewContext::DownValue()
{
	options[curOption] = (options[curOption] - 1);
	if( options[curOption]<0 )
	{
		if (OptionMaxVal[curOption].loop) 
			options[curOption] = OptionMaxVal[curOption].max-1;
		else 
			options[curOption] = 0;
	}
	if( curOption==OInkRampEnd )
	{
		if( options[OInkRampEnd]<options[OInkRampStart] )
			options[OInkRampEnd]++;
	}
	hud.Do( HUD::AChangeValue, options[curOption], TRUE );
}


void ViewContext::UpMenu()
{
	int oldLineValue = options[curOption];
	curOption = (curOption) ? curOption-1 : ONbOption-1 ;		// Loop
	
	hud.Do( HUD::AUp, oldLineValue, options[curOption] );
}


void ViewContext::DownMenu()
{
	int oldLineValue = options[curOption];
	curOption = ++curOption%ONbOption;							// Loop
	hud.Do( HUD::ADown, oldLineValue, options[curOption] );
}


void ViewContext::ScreenShot()
{
	static int snap_cpt = 0;
	char snap_fn[64];
	Sprintf( snap_fn, "nv_sample_12_%d.tga", snap_cpt++ );
	DpyManager::SaveFrame( snap_fn );
}


void ViewContext::DrawHUD()
{
	if(hud.selectedLine>=0)
	{
		Menu::Draw(hud.menu,hud.position);
	}
}
