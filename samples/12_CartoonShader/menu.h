/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _Menu_H_
#define _Menu_H_

#include <NvGeomShader.h>
#include <NvCore_STL.h>


struct MenuEntry
{
	Vec2	bmpPos;
	Vec2	bmpDim;
	Vec2	surfPos;
};
typedef nv::vector< MenuEntry > MenuEntryA ;


/* ---------------------------------		Menu			--------------------------------------------- */
namespace Menu
{	
	NvGeomShader *	Create	(	NvBitmap	*	inMenuRsc		,
								uint			inMaxEntries	);	
	bool			Draw	(	NvGeomShader *	inMenuShader	,	
								Vec2		 &	inPosition		);
	bool			Update	(	NvGeomShader *	inMenuShader	,
								MenuEntryA	 &	inEntries		);
}




/* ---------------------------------		HUD				--------------------------------------------- */
class HUD
{
public :

	// menu description : elements position and size.
	struct MenuRscDesc
	{
		uint inEntryHeight;
		uint inTextWidth;
		uint inValueWith;
		uint inTextposx;		
		uint inTextHposx;
		uint inValueStart;
	};

	enum Action
	{
		AChangeValue,
		ADown,
		AUp
	};
	
	
	NvGeomShader *		menu;
	int					selectedLine;
	MenuRscDesc			menuRscDesc;
	MenuEntryA			entries;
	Vec2				position;
	

	HUD					(										);

	~HUD				(										);

	bool Init			(	NvBitmap	*	inMenuRsc			,										
							MenuRscDesc &	inMenuDesc			,
							uint			inNbEntry			,
							Vec2			inPosition			,
							int			*	inValuesA			);
	
	//void Draw			(										);	

	void Do				(	Action			inAction			,
							int				inOldLineValue1		,
							int				inNewLineValue1		);
	
protected  :
	void ConstructLine	(	MenuRscDesc &	inMenuDesc		,
							uint			inLineNum		,
							bool			inIsSelected	,
							int				inValue			,
							MenuEntry	&	outEntryText	,
							MenuEntry	&   outEntryValue	);
};

#endif // _Menu_H_



