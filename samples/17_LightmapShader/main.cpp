/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "appvar.h"
#include "appctrl.h"



namespace
{
	AppVar appvar;
	AppTools::Camera camera;
}

void InitNeova();
void ShutNeova();

void GameLoop();





void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif

	if( !core::Init() ) {
		NV_WARNING("Cannot init core");
		return;
	}
	
	if (!RscManager::Init()) {
		core::Shut();
		NV_WARNING("Cannot init RscManager");
		return;
	}
	
	if (!RscManager::OpenBigFile(BF_PATH)) {
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot open BigFile");
		return;
	}

	if( !DpyManager::Init(DpyManager::CS_RIGHT_HANDED) ) {
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot init DpyManager");
		return;
	}
}


void ShutNeova()
{
	DpyManager::Shut();
	RscManager::Shut();
	core::Shut();
}



void GameLoop()
{	
	// Viewport, Clip range and Field of View
	Vec4	viewport	( 0.0f, 0.0f,
						  float(DpyManager::GetRasterWidth(DPYR_IN_FRAME)), 
						  float(DpyManager::GetRasterHeight(DPYR_IN_FRAME))	);
	Vec2	clipRange	(1.0f, 100.f);
	float	fov			= Pi/4.0f;
	
	// Projection Matrix
	Matrix	projMat;
	MatrixPerspectiveRH	(	&projMat ,
							fov			, 
							DpyManager::GetVirtualAspect(), 
							clipRange.x, clipRange.y	  );

	// World Matrix
	Matrix worldMat;
	MatrixIdentity( &worldMat );
	
	// Camera
	camera.Init();
	
	// Time
	clock::Time initTime;
	clock::GetTime( &initTime );
	float deltaTime, elapsedTime, lElapsedTime = 0;



	// Loop
	for (;;)
	{
		// core update
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;		
		
		// Timing
		{
			clock::Time currTime;			
			clock::GetTime( &currTime );
			
			elapsedTime = currTime 	  - initTime;
			deltaTime 	= elapsedTime - lElapsedTime;
			
			lElapsedTime = elapsedTime;
		}
		
		// Controller
		{
			AppCtrl::AppInputs input;
			if( !AppCtrl::GetController( &input ) )
				break ;

			if( input.dir1.x!=0.0f	   )	camera.Strafe			( input.dir1.x * deltaTime * 4.0f );
			if( input.dir1.y!=0.0f	   )	camera.Translate		( input.dir1.y * deltaTime * 4.0f );
			if( input.dir2.x!=0.0f	   )	camera.HorizontalRotate	( input.dir2.x * deltaTime * 2.0f );
			if( input.dir2.y!=0.0f	   )	camera.VerticalRotate	( input.dir2.y * deltaTime * 2.0f );
			if( input.button1_pressed  )	camera.Init				();
		}


		Matrix invViewMat;
		camera.GetView( &invViewMat );						// Get camera view matrix 
		MatrixFastInverse( &invViewMat, &invViewMat );
		

		// Draw the frame
		DpyManager::BeginFrame();
		
			DpyManager::SetSession(0);
				DpyManager::SetTarget	( DpyTarget(DPYR_IN_FRAME) );	// Target
				DpyManager::SetView		( &invViewMat				,	// ViewProj Mat
										  &projMat					,	
										  &clipRange				,
										  &viewport 				);	
				DpyManager::Draw		( appvar.pClear 			);	// Clear
				
			DpyManager::SetSession(1);
				DpyManager::SetWorldTR	( &worldMat 				);	// World Mat
				DpyManager::Draw		( appvar.pLightMap			);	// Lightmap

		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_17"

void GameMain()
{

	InitNeova();
	
	appvar.Init();
	if ( !appvar.Load() )
	{
		appvar.Shut();
		return;
	}
	
	
	GameLoop();
		
		
	// Shut
	appvar.Shut();
	ShutNeova();
}
