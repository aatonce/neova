/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





/* ---------------------------------		AppVar			--------------------------------------------- */
void AppVar::Init()
{
	pClear		= NULL;
	pLightMap	= NULL;
}



uint AppVar::Load()
{
	uint32 meshID = AppTools::FindUIDByName( "*lightmap_final*" );
		
	if( !meshID )
		return 0;

	RscPrefetchQ rscQ;
		rscQ.AddRsc( meshID );
	RscManager::AddPrefetchQ(&rscQ);
	
	
	if(!AppTools::PreparePrefetch(&rscQ))
	{
		NV_WARNING("Cannot create Mesh");
		Shut();
		core::Exit();
		return 0;
	}


	NvMesh * mesh  = NvMesh::Create(meshID);
	if( !mesh )
		return 0;
	
	pLightMap = NvLightmapShader::Create(mesh);
	if( !pLightMap )
		return 0;
	
	
	SafeRelease(mesh);


	pClear = NvFrameClear::Create();
	if( !pClear )
		return 0;
	pClear->SetColor(0x5555AAFF);

	return 1;
}


void AppVar::Shut()
{
	SafeRelease( pLightMap	);
	SafeRelease( pClear 	);
}



