/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "appdef.h"








/* ---------------------------------		Camera			--------------------------------------------- */

void AppTools::Camera::Init()
{
	teta = Pi/2.0f;
	phi  = 0.0f;
	position	= Vec3(0.0f, 0.0f, 15.0f);
	up			= Vec3(0.0f, 1.0f, 0.0f);
	
	SetlookAt();
}

void AppTools::Camera::HorizontalRotate(float _angle)
{
	teta+=_angle;
	SetlookAt();
}

void AppTools::Camera::VerticalRotate(float _angle)
{
	phi+=_angle;
	SetlookAt();
}

void AppTools::Camera::Translate(float _X)
{
	position += lookAt*_X;
}

void AppTools::Camera::Strafe(float _X)
{
	Vec3 cross;
	Vec3Cross( &cross, &up, &lookAt );
	Vec3Normalize(&cross,&cross);

	position += cross*_X;
}

void AppTools::Camera::SetlookAt()
{
	lookAt.x = Cos(phi) * Cos(teta);
	lookAt.y = Sin(phi);
	lookAt.z = Cos(phi) * Sin(teta);
}


void AppTools::Camera::GetView( Matrix* _outM)
{
	Vec3Normalize ( &lookAt, &lookAt );
	
	Vec3 cross;
	Vec3Cross( &cross, &Vec3(0.0f, 1.0f, 0.0f), &lookAt );
	Vec3Normalize ( &cross, &cross );
	
	Vec3Cross( &up, &lookAt, &cross );
	Vec3Normalize ( &up, &up );

	MatrixIdentity( _outM );
	_outM->m41 = position.x;
	_outM->m42 = position.y;
	_outM->m43 = position.z;
	MatrixSetAxis	 ( _outM, &cross, &up, &lookAt);
}






/* ---------------------------------		Tools Function			--------------------------------------------- */





uint AppTools::PreparePrefetch( RscPrefetchQ* _inPrefetchQ )
{
	//prefetching process is asynchronous and requires several updates of the Core and of the RscManager manager
	while(!_inPrefetchQ->IsReady())
	{
		if (core::Update() & (core::UPD_EXIT_ASKED|core::UPD_EXITED))
			return 0;
		
		RscManager::Update(10);
	}
	
	if (_inPrefetchQ->GetState() != RscPrefetchQ::COMPLETED)
	{
		RscManager::Unload(_inPrefetchQ);
		NV_WARNING("Mesh loading failed");
		return 0;
	}
	
	return 1;
}


void AppTools::SyncPrefetch	 ( UInt32A&		inUIDA )
{
	RscPrefetchQ prefQ;
	prefQ.rscUIDA.copy( inUIDA.begin(), inUIDA.end() );
	RscManager::AddPrefetchQ( &prefQ );
	while( !prefQ.IsReady() )
	{
		core::Update();
		RscManager::Update( 1000 );
	}
}


uint32 AppTools::FindUIDByName ( pcstr		inPatternFn	)
{
	if( !inPatternFn )
		return INVALID_RSC_UID;

	static NvUID* rscUID = NULL;
	if( !rscUID )
	{
		uint32 uid = RscManager::FindRscByType( NvUID::TYPE, INVALID_RSC_UID );
		if( uid == INVALID_RSC_UID )
			return INVALID_RSC_UID;
		
		UInt32A uidA;
		uidA.push_back( uid );
		SyncPrefetch( uidA );
		rscUID = NvUID::Create( uid );
		if( !rscUID )
			return INVALID_RSC_UID;
	}

	UInt32A uidA;
	rscUID->FindUIDByName( uidA, inPatternFn, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
	
	return ( uidA.size() == 1U ) ? uidA[0] : INVALID_RSC_UID;
}




void AppTools::ShowNoPadPlugged(	uint		inCpt			,
									bool		inReleaseData	)
{
	static NvGeomShader *	shaderSurf		= NULL	;
	static NvFrameClear *	clear			= NULL	;
	static int				bmpID			= -1	;
	static uint				bmpW			= 0		;
	static uint				bmpH			= 0		;
	static RscPrefetchQ	*	rscQ			= NULL	;

	if( inCpt==10 )
	{
		rscQ = NvGameNew(RscPrefetchQ);
		bmpID = FindUIDByName( NO_CONTROLER_BMP );						
		rscQ->AddRsc(bmpID);	
		RscManager::AddPrefetchQ(rscQ);
	}
	if( bmpID>=0 )
	{
		RscManager::Update(10);
		if( rscQ->IsReady() )
		{
			if( rscQ->GetState()==RscPrefetchQ::COMPLETED)
			{
				NvBitmap *	controlerNotfoundBMP = NvBitmap::Create	(bmpID);
				NV_ASSERT(controlerNotfoundBMP);
				bmpW = controlerNotfoundBMP->GetWidth();
				bmpH = controlerNotfoundBMP->GetHeight();
				NvSurface * surf = NvSurface::Create(4,NvSurface::CO_LOC|NvSurface::CO_TEX);
				shaderSurf  = NvGeomShader::Create(surf);
				clear		= NvFrameClear::Create();
				NV_ASSERT( surf );
				NV_ASSERT( shaderSurf );
				NV_ASSERT( clear );
				Vec3   pos;
				Vec2   tex;
				surf->Begin();
				pos=Vec3(0.0f,0.0f,0.0f);
				tex=Vec2(0.0f,0.0f);
				surf->TexCoord(tex);
				surf->Vertex(pos);
				pos=Vec3(float(bmpW),0.0f,0.0f);
				tex=Vec2(1.0f,0.0f);
				surf->TexCoord(tex);
				surf->Vertex(pos);
				pos=Vec3(0.0f,float(bmpH),0.0f);
				tex=Vec2(0.0f,1.0f);
				surf->TexCoord(tex);
				surf->Vertex(pos);
				pos=Vec3(float(bmpW),float(bmpH),0.0f);
				tex=Vec2(1.0f,1.0f);
				surf->TexCoord(tex);
				surf->Vertex(pos);
				surf->End();					
				shaderSurf->Enable		( NvGeomShader::EN_THROUGHMODE);
				shaderSurf->Disable		( NvShader::EN_RD_DEPTH );
				shaderSurf->SetDrawStart( 0 );
				shaderSurf->SetDrawSize	( 4 );
				DpyState * dpys = shaderSurf->GetDisplayState(0);
				DpySource srcBmp(controlerNotfoundBMP);
				dpys->SetSource(srcBmp);
				dpys->Enable(DpyState::EN_TEXTURING);
				dpys->SetMode( DpyState::CM_TWOSIDED );
				dpys->SetMode( DpyState::TM_LINEAR );
				clear->EnableColor	( TRUE );
				clear->SetColor		(0x5555AAFF);
				SafeRelease(surf);
				SafeRelease(controlerNotfoundBMP);
			}
			bmpID=-1;				
		}
	}
	if( shaderSurf )
	{
		Matrix recTransfo;
		uint rW = DpyManager::GetRasterWidth (DPYR_IN_FRAME);
		uint rH = DpyManager::GetRasterHeight(DPYR_IN_FRAME);
		Vec3 pos(float((rW-bmpW)/2),float((rH-bmpH)/2),0.0f);
		MatrixBuildTR( &recTransfo, &pos, NULL, NULL );

		Vec4 viewport( 0, 0, float(rW), float(rH) );
		Matrix	invWorldTransfo,	projTransfo;
		Vec2	clipRange(1, 100);
		float	fov = Pi/4;
		MatrixIdentity(&invWorldTransfo);
		MatrixPerspectiveRH( &projTransfo, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );
		
		DpyManager::BeginFrame	();
			DpyManager::SetSession	(	0					);
			DpyManager::SetTarget	(	DPYR_IN_FRAME		);
			DpyManager::SetView		(	&invWorldTransfo	, 
										&projTransfo		, 
										&clipRange			, 
										&viewport			);
			DpyManager::Draw		(	clear				);
			
			DpyManager::SetSession	(	1					);			
			DpyManager::SetWorldTR	(	&recTransfo			);
			DpyManager::Draw		(	shaderSurf			);
		DpyManager::EndFrame	();
		DpyManager::FlushFrame	();
	}

	if (inReleaseData)
	{
		if (rscQ)
		{
			NvGameDelete(rscQ);
			rscQ = NULL;
		}
		SafeRelease(shaderSurf);
		SafeRelease(clear);
		bmpID = -1;
	}
}