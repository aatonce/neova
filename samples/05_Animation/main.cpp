/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"


namespace
{
	AppVar appvar;
}


void InitNeova();
void ShutNeova();

void GameLoop();





void InitNeova()
{
	
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif

	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		core::Exit();
	}
	
	if (!RscManager::Init())
	{
		core::Shut();
		NV_WARNING("Cannot init RscManager");
		core::Exit();
	}
	
	if (!RscManager::OpenBigFile(BF_PATH))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot open BigFile");
		core::Exit();
	}
	
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot init DpyManager");
		core::Exit();
	}
}

void ShutNeova()
{
	DpyManager::Shut();
	RscManager::Shut();
	core::Shut();
}



void GameLoop()
{
	//Initial location and orientation of the mesh
	//used as reference values for the animation resource
	Vec3 initLoc(0, 0, 0);
	Quat initRot = Quat::UNIT;
	
	for (;;)
	{
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
		
		
		// Get elapsed time since the beginning of drawing
		clock::Time now;
		clock::GetTime(&now);
		float animTime = now - appvar.startTime;
		
		
		// Compute the projection matrix
		Matrix projMat;
		Vec2 clipRange( 1, 500 );
		float fov = Pi/4;
		MatrixPerspectiveRH( &projMat, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );
		
		// Compute the viewport dimensions
		Vec4 viewport(0, 0, DpyManager::GetRasterWidth(DPYR_IN_FRAME), DpyManager::GetRasterHeight(DPYR_IN_FRAME));
		
		// Compute the view matrix
		Matrix invViewMat;
		Vec3 posCam(0, 0, -30);
		MatrixBuildTR(&invViewMat, &posCam, NULL, NULL);
		
		
		// Compute the world matrix
		// according to the animation resource and the animation time
		Matrix worldMat;
		appvar.pAnim->GetTRValue( animTime, &worldMat, &initLoc, &initRot );
		//	MatrixBuildTR( &worldMat, &initLoc, &initRot, NULL );
			
			
		//Set the mesh's graphic scale if it is available
		if( appvar.pAnim->GetCtrlMask() & NvAnim::CTRL_SCL )
		{
			Vec3 scale;
			appvar.pAnim->GetSclValue( animTime, &scale );
			MatrixPreScale( &worldMat, &worldMat, &scale );
		}
		
		
		DpyManager::BeginFrame();
		
			DpyManager::SetSession	( 0 );

			DpyManager::SetTarget	(	DpyTarget( DPYR_IN_FRAME )	);

			DpyManager::Draw		(	appvar.pClear				);
			
			
			DpyManager::SetSession	( 1 );

			DpyManager::SetView		(	&invViewMat	,
										&projMat	,
										&clipRange	,
										&viewport 					);
			DpyManager::SetWorldTR	(	&worldMat					);

			DpyManager::Draw		(	appvar.pDispMesh			);
		
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}
}





#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_05"

void GameMain()
{
	// Init
	InitNeova();
	
	appvar.Init();
	appvar.Load();
	

	// GameLoop
	GameLoop();
	
	

	// Shut
	appvar.Shut();
	ShutNeova();
}
