/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>
#include <NvRscManager.h>
#include <NvDpyManager.h>
#include <NvFrameClear.h>
#include <NvSurface.h>
#include <NvGeomShader.h>
#include <NvImage.h>

using namespace nv;



#define IMAGE_SIZE 			64 



namespace PerlinNoise
{
	float fade( float t								);
   	float lerp( float t,  float a, float b			);
	float grad( int hash, float x, float y, float z	);
	void  SetZ( float inZ							);
	
	void  InitPerlinNoise();
	float Noise(uint i, uint j );
}




namespace AppTools
{
	void BuildQuad				( NvSurface** outSurf, uint 			inSize	 , DpyRaster inRaster 					 );
	void BuildImage				( NvImage**   outImg , NvImage::Region* outRegion, bool 	 inWithClut, NvShader* inShd );
	void InitCircleImage		( NvImage*    inImg	 , NvImage::Region* inRegion 										 );

	void UpdatePerlinNoiseImage	( NvImage *   inImg	 , NvImage::Region* inRegion , float 	 inT  						 );	// updating Pixel in NvImage
	void UpdateCircleImage		( NvImage *   inImg																		 );	// updating Clut  in NvImage	
}
