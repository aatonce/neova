/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include "appdef.h"



namespace
{
	int  	p[512];
	int 	permutation[] =
			{
				151,160,137,91,90,15,
			   131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
			   190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
			   88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
			   77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
			   102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
			   135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
			   5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
			   223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
			   129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
			   251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
			   49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
			   138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
			};
			
			
	int 	nZ;
	float 	nz;
	float 	nw;
	float 	nx[IMAGE_SIZE];
	int  	nX[IMAGE_SIZE];
	float 	nu[IMAGE_SIZE];
}



float PerlinNoise::fade(float t)
{
	return t * t * t * (t * (t * 6 - 15) + 10); 
}

float PerlinNoise::lerp(float t, float a, float b)
{
	return a + t * (b - a); 
}

float PerlinNoise::grad( int hash, float x, float y, float z )
{
	int 	h = hash & 15;
	float 	u = h<8 ? x : y,
			v = h<4 ? y : h==12||h==14 ? x : z;
	return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

void PerlinNoise::SetZ( float inZ )
{
	nZ  = (int)Floor(inZ) & 255;
	nz  = inZ -Floor(inZ);
	nw  = fade(nz);
}

void PerlinNoise::InitPerlinNoise()
{
	for ( uint i=0; i<256; i++ )
		p[256+i] = p[i] = permutation[i];

	for (uint i=0; i<IMAGE_SIZE; ++i)
	{
		nx[i]  = i/8.0f;
		nX[i]  = (int)Floor(nx[i]) & 255;
		nx[i] -= Floor(nx[i]);
		nu[i]  = fade(nx[i]);
	}
	SetZ(0.0f);
}

float PerlinNoise::Noise( uint i, uint j )
{
	int		X 	= nX[i],
			Y 	= nX[j];
	float 	x 	= nx[i],
			y 	= nx[j];
	
	double 	u = nu[i],
			v = nu[j];
			
	int A = p[X  ]+Y, AA = p[A]+nZ, AB = p[A+1]+nZ,      
		B = p[X+1]+Y, BA = p[B]+nZ, BB = p[B+1]+nZ;      

	return lerp(nw, lerp(v, lerp(u, grad(p[AA  ], x  , y  , nz   ),
								grad(p[BA  ], x-1, y  , nz   )),
							lerp(u, grad(p[AB  ], x  , y-1, nz   ),
								grad(p[BB  ], x-1, y-1, nz   ))),
							lerp(v, lerp(u, grad(p[AA+1], x  , y  , nz-1 ),
								grad(p[BA+1], x-1, y  , nz-1 )),
							lerp(u, grad(p[AB+1], x  , y-1, nz-1 ),
								grad(p[BB+1], x-1, y-1, nz-1 ))));									
}





void AppTools::BuildQuad( NvSurface** outSurf, uint inSize, DpyRaster inRaster )
{
	if( !outSurf )
		return;
	*outSurf = NULL;
	
	// The box geometry
	NvSurface*	boxSurf 	= NvSurface::Create( 4, NvSurface::CO_LOC|NvSurface::CO_TEX );
	NV_ASSERT( boxSurf );

	Vec2  xy0 				= Vec2( -(inSize/2.0f), -(inSize/2.0f) );
	Vec2  xy1 				= Vec2(  (inSize/2.0f),  (inSize/2.0f) );
	
	boxSurf->EnableDBF	();
	boxSurf->Begin		();
	boxSurf->TexCoord	(Vec2(0,0));					// v0 ----- v1								
	boxSurf->Vertex		(Vec3(xy0.x,xy0.y,0.f) );		// |      / |
	boxSurf->TexCoord	(Vec2(1,0));					// |     /  |
	boxSurf->Vertex		(Vec3(xy1.x,xy0.y,0.f) );		// |    /   |
	boxSurf->TexCoord	(Vec2(0,1));					// |   /    |
	boxSurf->Vertex		(Vec3(xy0.x,xy1.y,0.f) );		// |  /     |
	boxSurf->TexCoord	(Vec2(1,1));					// | /      |
	boxSurf->Vertex		(Vec3(xy1.x,xy1.y,0.f) );		// v2 ----- v3
	boxSurf->End		();
	
	*outSurf = boxSurf;
}


void AppTools::BuildImage( NvImage** outImg, NvImage::Region* outRegion, bool inWithClut, NvShader* inShd )
{
	if( !outImg )
		return ;
	*outImg = NULL;
	
	if( !inShd || !outRegion )
		return ;
	
	NvImage * img;
	NvImage::CrStatus outStatus;
	img = NvImage::Create( IMAGE_SIZE,IMAGE_SIZE, inWithClut, &outStatus );
	if( outStatus==NvImage::CS_CLUT_UNSUPPORTED )
		Printf("Clut unsupported\n");
	
	if( !img )
		return;
	
	DpySource source(img->GetBitmap());
	inShd->GetDisplayState(0)->SetSource(source);
	inShd->Enable( NvShader::EN_TEXTURING );
	// Create access to the entire image
	outRegion->x=0;
	outRegion->y=0;
	outRegion->w=IMAGE_SIZE;
	outRegion->h=IMAGE_SIZE;
	if ( !img->CreateAccess(*outRegion) )
		SafeRelease(img);
		
	*outImg = img;
}

void AppTools::InitCircleImage(NvImage * inImg,NvImage::Region * inRegion)
{	
	//init data image : make circles
	if( !inImg || !inRegion )
		return ;
	
	uint16 indShift	= GetFloorPow2(IMAGE_SIZE);
	uint16 indMask 	= IMAGE_SIZE - 1 ;

	uint lineStride;
	Psm  psm;
	// get poiteur to the pixel data
	uint8 * data8	= (uint8 *)inImg->GetPixelAccess(lineStride,psm);
	NV_ASSERT(data8 && psm==PSM_CLUT8);

	uint 	center 	= IMAGE_SIZE / 2;
	uint 	cptPxl 	= 0;
	uint8 * pxl 	= (data8),
		  * ptrEnd 	= (data8+IMAGE_SIZE*IMAGE_SIZE);
	// set pixel data				
	while( pxl<ptrEnd )
	{
		int i 		= cptPxl>>indShift;
		int j 		= cptPxl&indMask;
		int distx 	= center-i;
		int disty 	= center-j;
		uint dist 	= (uint)Sqrt((distx * distx + disty * disty));
		*(pxl)		= (dist*255/46) & 0xFF;			
		pxl++;
		cptPxl++;
	}
	// validate change for the entire image
	inImg->ValidatePixelAccess( *inRegion );

	// get poiteur to the clut data		
	uint32 * data32 = inImg->GetClutAccess( psm );
	if( data32 )
	{
		uint 		i 		= 0 ;
		uint32 *  	ptr 	= data32,
				*	ptrEnd 	= data32 + 256;
		// set clut
		while( ptr<ptrEnd )
		{
			float coeff = Sin( (i*Pi)/255.0f );
			uint8 col 	= uint( 128 + int(coeff*128) ) & 0xFF;
			*ptr= GetPSM( psm, col, col, 255, 255 );
			ptr++;
			i++;
		}
	}
	// validate clut modification
	inImg->ValidateClutAccess();				
}



// updating Pixel in NvImage
void AppTools::UpdatePerlinNoiseImage(NvImage * inImg,NvImage::Region * inRegion,float inT)
{
	if( !inImg || !inRegion )
		return ;
	
	uint lineStride;
	Psm  psm;
	
	uint32 * data32 = (uint32 *)inImg->GetPixelAccess( lineStride, psm );		
	if( data32 )
	{
		PerlinNoise::SetZ( inT );						
		uint 	 j 		= 0;
		uint32 * ptr 	= data32,
				* ptrEnd = (uint32*)(((uint8*)data32) + (lineStride*IMAGE_SIZE) );
		while( ptr<ptrEnd )
		{
			for (uint i=0; i<IMAGE_SIZE; ++i)
			{
				float n = PerlinNoise::Noise(i,j);
				ptr[i] = GetPSM( psm, 0x00, 0x00, uint(128+(n*127)) & 0xFF , 255 );
			}
			ptr = (uint32*)(((uint8*)ptr)+lineStride);	
			j++;
		}
		
		inImg->ValidatePixelAccess(*inRegion);				
	}
}

// updating Clut in NvImage	
void AppTools::UpdateCircleImage(NvImage * inImg)
{
	if( !inImg )
		return ;

	Psm  psm;
	uint32 * data32 = inImg->GetClutAccess(psm);
	if (data32)
	{
		uint32 tmp = *data32;
		Memmove( data32, data32+1, 255*4 );
		data32[255] = tmp;
	}
	inImg->ValidateClutAccess();	
}