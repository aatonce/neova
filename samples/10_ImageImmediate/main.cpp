/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"

#define SAMPLE_TIME_LENGHT 	10.0f // in second










namespace
{
	AppVar appvar;
}


void InitNeova();
void ShutNeova();

void GameLoop();





void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif


	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		core::Exit();
	}
	
	if (!RscManager::Init())
	{
		core::Shut();
		NV_WARNING("Cannot init RscManager");
		core::Exit();
	}
	
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot init DpyManager");
		core::Exit();
	}
}



void ShutNeova()
{
	DpyManager::Shut();
	RscManager::Shut();
	core::Shut();
}





void GameLoop()
{		
	// Build boxes 2D transfo
	float shift = (QUAD_SIZE/2.0f+8.0f);
	Matrix 	worldMatNoise,
			worldMatClut;
			
	int   W  = DpyManager::GetRasterWidth ( appvar.backRaster );
	int   H  = DpyManager::GetRasterHeight( appvar.backRaster );
	
	Vec3   	boxPos1	((W/2.0f)-shift, (H/2.0f), 0.0f ),
			boxPos2	((W/2.0f)+shift, (H/2.0f), 0.0f );
	//Vec3   	boxPos1	( 0.0f, 0.0f, 0.0f ),
	//		boxPos2	( 0.0f, 0.0f, 0.0f );
	MatrixBuildTR( &worldMatNoise, &boxPos1, NULL, NULL );	
	MatrixBuildTR( &worldMatClut , &boxPos2, NULL, NULL );
	

	for (;;)
	{
		// Update neova
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
	
		// Timing
		float fltTime;
		{
			clock::Time currTime;
			clock::GetTime( &currTime );
			fltTime = float( currTime );
			if( fltTime > SAMPLE_TIME_LENGHT )
				break;
		}
		
		
		appvar.Update( fltTime );
		
		// Draw the frame
		DpyManager::BeginFrame();
		DpyManager::SetTarget( DpyTarget(appvar.backRaster) );

		DpyManager::Draw( appvar.clear );

		DpyManager::SetWorldTR( &worldMatNoise );
		DpyManager::Draw( appvar.boxShdNoise );
		
		if (appvar.imageClut)
		{
			DpyManager::SetWorldTR( &worldMatClut );
			DpyManager::Draw( appvar.boxShdClut );
		}

		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_10"

void GameMain()
{
	// Init
	InitNeova();
	
	appvar.Init();
	
	if(!appvar.Load())
	{
		core::Exit();
		return ;
	}
	

	// GameLoop
	GameLoop();
	
	

	// Shut
	appvar.Shut();
	ShutNeova();
}
