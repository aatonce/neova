/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"




void AppVar::Init()
{
	clear		= NULL;
	boxSurf		= NULL;
	boxShdNoise	= NULL;
	boxShdClut	= NULL;
	imageNoise 	= NULL;
	imageClut	= NULL;
	
	PerlinNoise::InitPerlinNoise();
	backRaster = DPYR_IN_FRAME;		
	
	// The clear object
	clear   = NvFrameClear::Create();
	NV_ASSERT( clear   );
	clear->SetColor(0x5555AAFF);
}

uint AppVar::Load()
{
	AppTools::BuildQuad( &boxSurf, QUAD_SIZE, backRaster );
	
	// The first box drawing shader
	boxShdNoise = NvGeomShader::Create( boxSurf );
	NV_ASSERT( boxShdNoise  );
	boxShdNoise->SetDrawStart( 0 );
	boxShdNoise->SetDrawSize ( 4 );
	// The Second box drawing shader
	boxShdClut  = NvGeomShader::Create( boxSurf );
	NV_ASSERT( boxShdClut  );
	boxShdClut->SetDrawStart( 0 );
	boxShdClut->SetDrawSize ( 4 );
	
	// Activate the 2D mode.
	boxShdNoise->Enable ( NvShader::EN_THROUGHMODE );
	boxShdClut ->Enable ( NvShader::EN_THROUGHMODE );
	boxShdNoise->Disable( NvShader::EN_RD_DEPTH );
	boxShdClut ->Disable( NvShader::EN_RD_DEPTH );
	boxShdNoise->GetDisplayState(0)->SetMode( DpyState::CM_TWOSIDED );
	boxShdClut ->GetDisplayState(0)->SetMode( DpyState::CM_TWOSIDED );
		

	
	// Build Image, attribute it to a NvShader, and create an access
	AppTools::BuildImage(&imageNoise,&regionNoise,FALSE,boxShdNoise);		
	AppTools::BuildImage(&imageClut ,&regionClut ,TRUE ,boxShdClut );
	// indexed texture can be not supported by DX9

	if (!imageNoise )
	{
		SafeRelease( boxSurf	 );
		SafeRelease( boxShdClut  );
		SafeRelease( boxShdNoise );
		SafeRelease( clear   	 );
		
		return 0;
	}

	// init indexed pixel and clut
	if (imageClut)
		AppTools::InitCircleImage(imageClut,&regionClut);
	
	
	return 1;
}

void AppVar::Shut()
{
	imageNoise->ReleaseAccess();
	SafeRelease( imageNoise	 );	
	if( imageClut )
	{
		imageClut->ReleaseAccess();	
		SafeRelease( imageClut	);
	}
	
	SafeRelease( boxSurf		);
	SafeRelease( boxShdClut		);
	SafeRelease( boxShdNoise	);
	SafeRelease( clear			);
}



void AppVar::Update( float _deltaT )
{
	// Update Perlin noise image
	AppTools::UpdatePerlinNoiseImage(imageNoise,&regionNoise,_deltaT);
	
	// Update Clut for clut rotation image
	if (imageClut)
		AppTools::UpdateCircleImage(imageClut);
}

/*
void AppVar::Draw()
{}
*/
