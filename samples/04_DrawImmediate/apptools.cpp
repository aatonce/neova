/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appdef.h"






NvGeomShader* AppTools::CreateFractal()
{
	const int		fwidth  = 100;
	const int		fheight = 100;
	const float		fup     = 80.0f;
	const Vec3		fscale( 300.f, 1.0f, 300.f );

	vector<float> fhA( fwidth*fheight );		// Height array
	vector<Vec2>  ftA( fwidth*fheight );		// Vertex texture coordinates
	vector<Vec3>  fvA( fwidth*fheight );		// Vertex position
	vector<Vec3>  fnA( fwidth*fheight );		// Vertex normal


	// Initialize height array with random values
	for( uint fw=0 ; fw<fwidth; fw++ )
	{
		for( uint fh=0; fh<fheight; fh++ )
		{
			fhA[fw+fh*fwidth] = nv::libc::Randf() * fup;
		}
	}

	// Smooth height array
	for( uint s=0; s<4; s++ )
	{
		for( uint fw=0; fw<fwidth; fw++ )
		{
			for( uint fh=0; fh<fheight; fh++ )
			{
				float avg = 0.0f;
				float fac = 0.0f;
				if( fw > 0 )
				{
					avg += fhA[(fw-1)+fh*fwidth];
					fac += 1.0f;
				}
				if( fw < fwidth-1 )
				{
					avg += fhA[(fw+1)+fh*fwidth];
					fac += 1.0f;
				}
				if( fh > 0 )
				{
					avg += fhA[fw+(fh-1)*fwidth];
					fac += 1.0f;
				}
				if( fh < fheight-1 )
				{
					avg += fhA[fw+(fh+1)*fwidth];
					fac += 1.0f;
				}
				fhA[fw+fh*fwidth] = avg / fac;
			}
		}
	}


	// Compute vertex position and vertex texture coordinates
	for( uint fw=0; fw<fwidth; fw++ )
	{
		for( uint fh=0; fh<fheight; fh++ )
		{
			int i = fw+fh*fwidth;
			float x = float(fw) / float(fwidth-1);
			float z = float(fh) / float(fheight-1);
			Vec3  l( x, fhA[i], z );
			ftA[i] = Vec2( x, z );					// texture coordinates
			Vec3Mul( &fvA[i], &l, &fscale );		// position
			fvA[i] -= fscale/2.0f;
		}
	}


	// Compute vertex normal
	for( uint fw=0; fw<fwidth; fw++ )
	{
		for( uint fh=0; fh<fheight; fh++ )
		{
			int i = fw+fh*fwidth;
			if( fw==0 || fh==0 || fh==fheight-1 || fw==fwidth-1 )
			{
				fnA[i] = Vec3::UNIT_Y;
			}
			else
			{
				Vec3 va = fvA[ i-fwidth ];
				Vec3 vb = fvA[ i+fwidth ];
				Vec3 vc = fvA[ i-1 ];
				Vec3 vd = fvA[ i+1 ];
				Vec3 dx = vd - vc;
				Vec3 dz = va - vb;
				Vec3 n  = dx ^ dz;
				Vec3Normalize( &n, &n );
				fnA[i] = n;
			}
		}
	}

	uint nbVertex = (fwidth*2*(fheight-1));

	NvSurface* surf = NvSurface::Create( nbVertex, NvSurface::CO_LOC|NvSurface::CO_TEX|NvSurface::CO_COLOR|NvSurface::CO_KICK );
	if( !surf )
		return NULL;

	// No need DBF here !

	surf->Begin();
	uint cptVertex = 0;
	for( uint fh=0; fh<fheight-1; fh++ )
	{
		for( uint fw=0; fw<fwidth; fw++ )
		{
			int i0 = fw+fh*fwidth;
			int i1 = fw+(fh+1)*fwidth;
			Vec3 v00 = fvA[i0];
			Vec3 v01 = fvA[i1];
			Vec2   t0 = ftA[i0];
			Vec2   t1 = ftA[i1];
			int    l0  = int( fnA[i0].y * 255.f );
			int    l1  = int( fnA[i1].y * 255.f );
			uint32 c0  = GetDpyColor( PSM_ABGR32, l0, l0, l0, 255 );
			uint32 c1  = GetDpyColor( PSM_ABGR32, l1, l1, l1, 255 );
			
			surf->Color( c0, PSM_ABGR32 );
			surf->TexCoord( t0 );
			surf->Vertex( v00, (fw>1) );
			
			surf->Color( c1, PSM_ABGR32 );
			surf->TexCoord( t1 );
			surf->Vertex( v01, (fw>1) );
			
			cptVertex += 2;
		}
	}
	NV_ASSERT( cptVertex == nbVertex );
	surf->End();

	NvGeomShader* shd = NvGeomShader::Create( surf );
	SafeRelease( surf );
	shd->SetDrawSize( nbVertex );

	return shd;
}





/* ---------------------------------		Tools Function			--------------------------------------------- */

uint AppTools::PreparePrefetch( RscPrefetchQ* _inPrefetchQ )
{
	//prefetching process is asynchronous and requires several updates of the Core and of the RscManager manager
	while(!_inPrefetchQ->IsReady())
	{
		if (core::Update() & (core::UPD_EXIT_ASKED|core::UPD_EXITED))
			return 0;
		
		RscManager::Update(10);
	}
	
	if (_inPrefetchQ->GetState() != RscPrefetchQ::COMPLETED)
	{
		RscManager::Unload(_inPrefetchQ);
		NV_WARNING("Mesh loading failed");
		return 0;
	}
	
	return 1;
}


void AppTools::SyncPrefetch	 ( UInt32A&		inUIDA )
{
	RscPrefetchQ prefQ;
	prefQ.rscUIDA.copy( inUIDA.begin(), inUIDA.end() );
	RscManager::AddPrefetchQ( &prefQ );
	while( !prefQ.IsReady() )
	{
		core::Update();
		RscManager::Update( 1000 );
	}
}


uint32 AppTools::FindUIDByName ( pcstr		inPatternFn	)
{
	if( !inPatternFn )
		return INVALID_RSC_UID;

	static NvUID* rscUID = NULL;
	if( !rscUID )
	{
		uint32 uid = RscManager::FindRscByType( NvUID::TYPE, INVALID_RSC_UID );
		if( uid == INVALID_RSC_UID )
			return INVALID_RSC_UID;
		
		UInt32A uidA;
		uidA.push_back( uid );
		SyncPrefetch( uidA );
		rscUID = NvUID::Create( uid );
		if( !rscUID )
			return INVALID_RSC_UID;
	}

	UInt32A uidA;
	rscUID->FindUIDByName( uidA, inPatternFn, libc::FNM_IGNORECASE | libc::FNM_FILE_NAME );
	
	return ( uidA.size() == 1U ) ? uidA[0] : INVALID_RSC_UID;
}