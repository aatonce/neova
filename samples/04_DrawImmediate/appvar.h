/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appdef.h"



struct AppVar
{
	NvFrameClear*	clearObject[2];			// Buffers to clear on each frame
	
	
	NvGeomShader* 	fractalShader;			// 3D Surface instance
	NvGeomShader*	HUDShd;					// 2D Surface instance


	
	// Matrix
	Matrix 			projMat[2];				// Projection 
	Matrix 			invViewMat[2];			// View 
	Vec2  			clipRange;
	Vec4			viewport[2];			// Viewport
	
	void 			Init	(	);			// Initialize structure
	void 			Load	(	);			// Load structure
	void 			Shut	(	);			// Shut structure
};

