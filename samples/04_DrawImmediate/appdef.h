/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include <Nova.h>
#include <NvDpyManager.h>		// Display manager
#include <NvUID.h>
#include <NvRscManager.h>

#include <NvFrameClear.h>		// NvFrameClear interface needed to reset all 3D buffers
#include <NvMesh.h>				// Mesh resource interface
#include <NvBitmap.h>				// Mesh resource interface
#include <NvSurface.h>			// Immediate surface resource interface
#include <NvGeomShader.h>		// Drawable mesh interface


using namespace nv;





#if   defined(_PSP)
#define BF_PATH "BIGFILE_PSP"
#elif defined(_PS2)
#define BF_PATH "BIGFILE_PS2"
#elif defined(_DX)
#define BF_PATH "BIGFILE_DX"
#elif defined(_NGC)
#define BF_PATH "BIGFILE_NGC"
#else
#define BF_PATH "BIGFILE"
#endif




#define HUD_TEXTURE_NAME		"*LogoNeova04*png"
#define HUD_TEXTURE_SIZEX		110
#define HUD_TEXTURE_SIZEY		80




namespace AppTools
{
	NvGeomShader* CreateFractal();

	
	uint	PreparePrefetch	( RscPrefetchQ*		inPrefetchQ );
	void	SyncPrefetch	( UInt32A&			inUIDA		);
	uint32	FindUIDByName	( pcstr				inPatternFn	);
}


