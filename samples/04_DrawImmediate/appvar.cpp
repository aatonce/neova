/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"







void AppVar::Init()
{
	fractalShader = NULL;
	
	uint32 clearColor[2] =
	{
		0x2222AAFF,
		0x8800AAFF
	};

	for( uint i=0; i<2; i++ )
	{
		clearObject[i] = NvFrameClear::Create();
		clearObject[i]->SetColor( clearColor[i] );
	}
	
}




void AppVar::Load()
{
	fractalShader = AppTools::CreateFractal();
	
	// Setup display states
	DpyState* dpystate = fractalShader->GetDisplayState( 0 );
	if( dpystate )
	{
		dpystate->SetMode( DpyState::CM_TWOSIDED );
		
		dpystate->SetEnabled(  DpyState::EN_WR_COLOR
	    					 | DpyState::EN_WR_DEPTH
	    					 | DpyState::EN_RD_DEPTH	);
	}
	


	// Viewport
	uint rasterWidth  = DpyManager::GetRasterWidth  ( DPYR_IN_FRAME );
	uint rasterHeight = DpyManager::GetRasterHeight ( DPYR_IN_FRAME );
	

	viewport[0] = Vec4( 0,	0,					rasterWidth,	rasterHeight/2.0f	);		// Viewport Top
	viewport[1] = Vec4( 0,	rasterHeight/2.0f,	rasterWidth,	rasterHeight/2.0f	);		// Viewport Bottom


	
	
	clipRange = Vec2( 1.0f, 500.0f );														// ( Near clip, Far clip )
	
	
	// View
	Vec3 camPos[2] = 
	{
		Vec3( 0.0f,	-300.0f, 0.0f ),
		Vec3( 0.0f,	-300.0f, 0.0f )
	};
	Vec3 camLookAt[2] = 
	{
		Vec3( 0.0f, -1.0f, 0.0f ),
		Vec3( 0.0f, -1.0f, 0.0f )
	};
	Vec3 camUp[2] = 
	{
		Vec3( 0.0f, 0.0f, 1.0f ),
		Vec3( 0.0f, 0.0f, 1.0f )
	};


	for( uint i=0; i<2; i++ )
	{
		// View
		MatrixTranslation( &invViewMat[i], &camPos[i] );
		Vec3 cross;
		Vec3Cross( &cross, &camUp[i], &camLookAt[i] );
		MatrixSetAxis ( &invViewMat[i], &cross, &camUp[i], &camLookAt[i]);

		MatrixFastInverse( &invViewMat[i], &invViewMat[i] );


		// Projection
		float aspect = viewport[i].z / viewport[i].w;
	//	float aspect = DpyManager::GetVirtualAspect();

		MatrixPerspectiveRH( &projMat[i], HalfPi, aspect , clipRange.x, clipRange.y );
	}


	// 2D
	NvSurface* HUDSurf = NULL;
	HUDShd	= NULL;

	// The quad geometry
	HUDSurf = NvSurface::Create( 4, NvSurface::CO_LOC | /*NvSurface::CO_COLOR |*/ NvSurface::CO_TEX );
	NV_ASSERT( HUDSurf );

	// The 2D drawing shader
	HUDShd  = NvGeomShader::Create( HUDSurf );
	NV_ASSERT( HUDShd  );
	HUDShd->SetDrawStart( 0 );
	HUDShd->SetDrawSize	( 4 );

	// Activate the 2D mode here !
	HUDShd->Enable ( NvShader::EN_THROUGHMODE | NvShader::EN_TEXTURING 	);
	HUDShd->Disable( NvShader::EN_RD_DEPTH );
	HUDShd->GetDisplayState( 0 )->SetMode( DpyState::CM_TWOSIDED );
	


	Vec3 posArray[4];
																				// v0 ----- v1
	posArray[0] = Vec3( 0.0f,				0.0f,				0.0f );			// |     /  |
	posArray[1] = Vec3( HUD_TEXTURE_SIZEX,	0.0f,				0.0f );			// |   /    |
	posArray[2] = Vec3( 0.0f,				HUD_TEXTURE_SIZEY,	0.0f );			// | /      |
	posArray[3] = Vec3( HUD_TEXTURE_SIZEX,	HUD_TEXTURE_SIZEY,	0.0f );			// v2 ----- v3


	Vec2 textCoordArray[4];

	textCoordArray[0] = Vec2( 0.0f, 0.0f );
	textCoordArray[1] = Vec2( 1.0f, 0.0f );
	textCoordArray[2] = Vec2( 0.0f, 1.0f );
	textCoordArray[3] = Vec2( 1.0f, 1.0f );



	//Psm		ColorPSM = NvSurface::GetNativeColorPSM();
	//uint32	Color    = GetPSM( ColorPSM, 255, 0, 0, 255 );
		
	// Set Vertex position and texture coordinate to the 2D shader surface
	HUDSurf->EnableDBF();
	HUDSurf->Begin();
	for( uint i=0; i<4; i++)
	{
		//HUDSurf->Color		( Color, ColorPSM );
		HUDSurf->TexCoord	( textCoordArray[i] );
		HUDSurf->Vertex		( posArray[i] );
	}
	HUDSurf->End();




	
	
	uint32	textID	= AppTools::FindUIDByName( HUD_TEXTURE_NAME	);

	RscPrefetchQ rscQ;
	rscQ.AddRsc( textID	);
	RscManager::AddPrefetchQ( &rscQ );

	// Load Texture
	if(!AppTools::PreparePrefetch(&rscQ))
	{
		NV_WARNING( "Can't create Texture" );
		Shut();
		core::Exit();
		return;
	}


	NvBitmap* texture = NvBitmap::Create( textID );
	HUDShd->GetDisplayState(0)->SetSource( texture );

	SafeRelease( texture );	
	SafeRelease( HUDSurf );
}



void AppVar::Shut()
{
	SafeRelease( fractalShader );
	for( uint i=0; i<2; i++ )
		SafeRelease( clearObject[i] );

	SafeRelease( HUDShd  );
}
