/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"


namespace
{
	AppVar appvar;
}


void InitNeova();
void ShutNeova();

void GameLoop();





void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(640) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(496) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(640) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(496) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif

	// Initialize the core engine
	if (!core::Init())
	{
		NV_WARNING( "Cannot init core" );
		core::Exit();
	}

	// Initialize the display manager
	if (!DpyManager::Init( DpyManager::CS_RIGHT_HANDED ))
	{
		NV_WARNING( "Cannot init DpyManager" );
		core::Exit();
	}

	if (!RscManager::Init())
	{
		core::Shut();
		NV_WARNING( "Cannot init RscManager" );
		core::Exit();
	}

	if (!RscManager::OpenBigFile( BF_PATH ))
	{
		core::Shut();
		NV_WARNING( "Cannot open OpenBigFile" );
		core::Exit();
	}
}

void ShutNeova()
{
	DpyManager::Shut();
	core::Shut();
}



void GameLoop()
{
	float rotationSpeed = 2.0f;


	// Draw Loop
	for (;;)
	{
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
		
		
		clock::Time curTime;
		clock::GetTime(&curTime);
		
		

		// Compute the word matrix
		float rotationAngle = float(curTime) * rotationSpeed;

		Vec3 worldPos( 0.0f, 0.0f, 0.0f );
		Quat worldRot( rotationAngle, Vec3( 0.0f, 1.0f, 0.0f ) );

		Matrix 	worldMat;
		MatrixBuildTR( &worldMat, &worldPos,
								  &worldRot,
								  NULL 		);


		// Draw
		DpyManager::BeginFrame();

			DpyManager::SetTarget( DpyTarget( DPYR_IN_FRAME ) );


			for( uint i=0; i<2; i++ )
			{
				DpyManager::SetSession	( 0 );									// Session 0 : Clear the viewport
				{
					DpyManager::SetView	(	NULL,
											NULL,
											NULL,
											&appvar.viewport[i] 	);

					DpyManager::Draw	(	appvar.clearObject[i] 	);
				}


				
				DpyManager::SetSession	( 1 );									// Session 1 : Draw the fractal surface with basic shader
				{
					DpyManager::SetView		( 	&appvar.invViewMat[i],
												&appvar.projMat[i], 
												&appvar.clipRange, 
												&appvar.viewport[i] 	);

					DpyManager::SetWorldTR	( 	&worldMat 				);

					DpyManager::Draw		( 	appvar.fractalShader 	);
				}


				
				
				DpyManager::SetSession	( 2 );									// Session 2 : Draw 2D HUD
				{
					uint viewportWidth  = appvar.viewport[i].z;
					uint viewportHeight = appvar.viewport[i].w;

					Vec3 HUDPosition;
					HUDPosition.x = viewportWidth  - HUD_TEXTURE_SIZEX;
					HUDPosition.y = viewportHeight - HUD_TEXTURE_SIZEY;
					HUDPosition.z = 0.0f;

					Matrix HUDMat;
					MatrixBuildTR( 	&HUDMat,
									&HUDPosition,
									NULL,
									NULL 		);

					
					DpyManager::SetWorldTR	(	&HUDMat 		);
					DpyManager::Draw		(	appvar.HUDShd 	);
				}
			}

		DpyManager::EndFrame   (	);
		DpyManager::FlushFrame (	);
	}
}





#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_04"

void GameMain()
{
	// Init
	InitNeova();
	
	appvar.Init();
	appvar.Load();
	

	// GameLoop
	GameLoop();
	
	

	// Shut
	appvar.Shut();
	ShutNeova();
}
