/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





/* ---------------------------------		AppVar			--------------------------------------------- */
void AppVar::Init()
{
	pWater			= NULL;
	pClear			= NULL;
	bumpMap1		= NULL;
	bumpMap2		= NULL;
	reflectionMap	= NULL;
	diffMap			= NULL;

	pMat			= NULL;
	pBump			= NULL;
	pLight			= NULL;
}


NvSurface* AppVar::BuildQuad(uint inSize,const Vec2 &  pos)
{
	// The box geometry
	NvSurface*	boxSurf 	= NvSurface::Create( 4, NvSurface::CO_LOC|NvSurface::CO_TEX );
	NV_ASSERT( boxSurf );

	Vec2  xy0 				= Vec2( (pos.x)-(inSize/2), (pos.y)-(inSize/2) );
	Vec2  xy1 				= Vec2( (pos.x)+(inSize/2), (pos.y)+(inSize/2) );
	
	boxSurf->EnableDBF	();
	boxSurf->Begin		();
	boxSurf->TexCoord	(Vec2(0,0));					// v0 ----- v1								
	boxSurf->Vertex		(Vec3(xy0.x,xy0.y,0.f) );		// |      / |
	boxSurf->TexCoord	(Vec2(1,0));					// |     /  |
	boxSurf->Vertex		(Vec3(xy1.x,xy0.y,0.f) );		// |    /   |
	boxSurf->TexCoord	(Vec2(0,1));					// |   /    |
	boxSurf->Vertex		(Vec3(xy0.x,xy1.y,0.f) );		// |  /     |
	boxSurf->TexCoord	(Vec2(1,1));					// | /      |
	boxSurf->Vertex		(Vec3(xy1.x,xy1.y,0.f) );		// v2 ----- v3
	boxSurf->End		();
	return boxSurf;
}



uint AppVar::Load()
{
	uint32	map0ID	= AppTools::FindUIDByName( BUMPMAP1_NAME );
	uint32	map1ID	= AppTools::FindUIDByName( BUMPMAP2_NAME );
	uint32  map2ID	= AppTools::FindUIDByName( RFLCTMAP_NAME );
	uint32	map3ID	= AppTools::FindUIDByName( MATERIAL_NAME );
	
	RscPrefetchQ rscQ;
		rscQ.AddRsc( map0ID );
		rscQ.AddRsc( map1ID );
		rscQ.AddRsc( map2ID );
		rscQ.AddRsc( map3ID );
	RscManager::AddPrefetchQ(&rscQ);
	
	// load Data
	if(!AppTools::PreparePrefetch(&rscQ))
	{
		NV_WARNING("Cannot create Mesh");
		Shut();
		core::Exit();
		return 0;
	}
	
	bumpMap1		= NvBitmap::Create( map0ID );
	bumpMap2		= NvBitmap::Create( map1ID );
	reflectionMap	= NvBitmap::Create( map2ID );
	diffMap			= NvBitmap::Create( map3ID );
	
	NV_ASSERT( bumpMap1		 );
	NV_ASSERT( bumpMap2		 );
	NV_ASSERT( reflectionMap );
	NV_ASSERT( diffMap		 );
	


	// The NvWaterShader
	pWater = NvWaterShader::Create();
	NV_ASSERT( pWater );
	pWater->Enable( NvShader::EN_CLIPPING );
	pWater->SetMap( NvWaterShader::MI_BUMP1		, bumpMap1		);
	pWater->SetMap( NvWaterShader::MI_BUMP2		, bumpMap2		);
	pWater->SetMap( NvWaterShader::MI_DIFFUSE	, diffMap		);
	pWater->SetMap( NvWaterShader::MI_REFLECTION, reflectionMap );
	
	
	// The NvFrameClear object
	pClear	= NvFrameClear::Create();
	NV_ASSERT( pClear );
	pClear->EnableColor( TRUE );
	pClear->EnableDepth( TRUE );
	pClear->SetColor(0x5555AAFF);

	
	NvSurface* 	quad1 = BuildQuad(100 , Vec2(50.0f , 50.0f) );
	NvSurface* 	quad2 = BuildQuad(100 , Vec2(155.0f, 50.0f) );
	NvSurface* 	quad3 = BuildQuad(100 , Vec2(260.0f, 50.0f) );
	
	pMat	= NvGeomShader::Create( quad1 );
	pBump	= NvGeomShader::Create( quad2 );
	pLight	= NvGeomShader::Create( quad3 );

	SafeRelease( quad1	);
	SafeRelease( quad2	);
	SafeRelease( quad3	);


	DpySource source1( diffMap		 );
	DpySource source2( bumpMap1		 );
	DpySource source3( reflectionMap );
	
	uint32	defaultQuadMode = DpyState::BM_NONE|DpyState::CM_TWOSIDED|DpyState::TM_LINEAR|DpyState::TF_DECAL;

	// Mat
	pMat->SetDrawStart( 0 );
	pMat->SetDrawSize( 4 );	
	pMat->Enable( NvShader::EN_THROUGHMODE );
	pMat->Disable( NvShader::EN_RD_DEPTH );
	DpyState * dpyS1 = pMat->GetDisplayState(0);
		dpyS1->SetMode( DpyState::CM_TWOSIDED );
		dpyS1->SetSource(source1);
		dpyS1->SetAlphaPass(0.0f);
		dpyS1->SetMode(defaultQuadMode);
	
	
	// Bump
	pBump->SetDrawStart( 0 );
	pBump->SetDrawSize( 4 );	
	pBump->Enable( NvShader::EN_THROUGHMODE );
	pBump->Disable( NvShader::EN_RD_DEPTH );
	DpyState * dpyS2 = pBump->GetDisplayState(0);
		dpyS2->SetMode( DpyState::CM_TWOSIDED );
		dpyS2->SetSource(source2);
		dpyS2->SetAlphaPass(0.0f);
		dpyS2->SetMode(defaultQuadMode);
	
	// Light
	pLight->SetDrawStart( 0 );
	pLight->SetDrawSize( 4 );	
	pLight->Enable( NvShader::EN_THROUGHMODE );
	pLight->Disable( NvShader::EN_RD_DEPTH );
	DpyState * dpyS3 = pLight->GetDisplayState(0);
		dpyS3->SetMode( DpyState::CM_TWOSIDED );
		dpyS3->SetSource(source3);
		dpyS3->SetAlphaPass(0.0f);
		dpyS3->SetMode(defaultQuadMode);
	
	
	return 1;
}


void AppVar::Shut()
{
	SafeRelease( pMat	);
	SafeRelease( pBump	);
	SafeRelease( pLight	);
	
	SafeRelease( pWater			);
	SafeRelease( bumpMap1		);
	SafeRelease( bumpMap2		);
	SafeRelease( reflectionMap	);
	SafeRelease( diffMap		);
	SafeRelease( pClear			);
}



