/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include "appvar.h"
#include "appctrl.h"





namespace
{
	AppVar appvar;
}


void InitNeova();
void ShutNeova();

void GameLoop();



void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif

	if (!core::Init()) {
		NV_WARNING("Cannot init core");
		return;
	}
	
	if (!RscManager::Init()) {
		core::Shut();
		NV_WARNING("Cannot init RscManager");
		return;
	}
	
	if (!RscManager::OpenBigFile(BF_PATH)) {
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot open BigFile");
		return;
	}
	
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED)) {
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot init DpyManager");
		return;
	}
}


void ShutNeova()
{
	DpyManager::Shut();
	RscManager::Shut();
	
	core::Shut();
}



void GameLoop()
{

	clock::Time initTime;
	clock::GetTime( &initTime );
	float deltaTime, elapsedTime, lElapsedTime = 0;

	float   planeAngle 	= 0.0f;//-0.403262f;
	Vec3	planeTrans	( 0.0f, 0.0f, 0.0f );
	Quat	planeRot  	(planeAngle,Vec3(1.0f,0.0f,0.0f));
	float	camAngle1	= 0.0f;
	float	camAngle2	= 0.0f;
	Vec3	transBump1(0.0f,0.0f,0.0f);
	Vec3	transBump2(0.0f,0.0f,0.0f);
	Matrix  diffMatrix		= Matrix::UNIT;
	Matrix  bump1Matrix		= Matrix::UNIT;
	Matrix  bump2Matrix		= Matrix::UNIT;

	Vec2	waterSize(100.0f,100.0f);
	
	
	
	Vec4 viewport( 0, 0, float(DpyManager::GetRasterWidth(DPYR_IN_FRAME)), float(DpyManager::GetRasterHeight(DPYR_IN_FRAME))	);
	Vec2 clipRange(0.1f, 1000.f);
	float fov = Pi/4;
	Matrix projMat;
	MatrixPerspectiveRH( &projMat, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );

	Matrix unitMat = Matrix::UNIT;

	
	// draw data
	for (;;)
	{
		// core update
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;		
		
		// Timing
		{
			clock::Time currTime;			
			clock::GetTime( &currTime );
			
			elapsedTime = currTime 	  - initTime;
			deltaTime 	= elapsedTime - lElapsedTime;
			
			lElapsedTime = elapsedTime;
		}
		
		
		float bump_tx = 0.0012f;
		float bump_ty = -0.0004f;
		// Controller
		{
			AppCtrl::AppInputs input;
			if( !AppCtrl::GetController( &input ) )
				break ;

			// camera angle
			if( input.dir1.x!=0.0f )		camAngle1+= input.dir1.x * deltaTime;
			if( input.dir1.y!=0.0f )		camAngle2-= input.dir1.y * deltaTime;
			
			// Bump
			if( input.dir2.x!=0.0f )		bump_tx = input.dir2.x * deltaTime /4.0f;
			if( input.dir2.y!=0.0f )		bump_ty = input.dir2.y * deltaTime /4.0f;
			
			// waterSize
			if( input.fltButton4!=0.0f ) 	waterSize.x	+= input.fltButton4 * deltaTime * 10.0f;
			if( input.fltButton5!=0.0f )	waterSize.x	-= input.fltButton5 * deltaTime * 10.0f;
			
			waterSize.x = Max(waterSize.x,1.0f);
			waterSize.y = waterSize.x;
			appvar.pWater->SetSize(waterSize.x,waterSize.y);
			appvar.pWater->SetTextureArea(100.0f,100.0f);
			
			planeTrans.x = -waterSize.x / 2.0f;
			planeTrans.y = -waterSize.y / 2.0f;
			
						
			
		}
		
		// Compute WorldMat
		Matrix	planeMat;
		Quat planeRot( planeAngle, Vec3(1.0f,0.0f,0.0f) );
		MatrixBuildTR( &planeMat, &planeTrans, &planeRot , NULL );
		
		transBump1.x += bump_tx;
		transBump2.y += bump_ty;
		MatrixTranslation( &bump1Matrix, &transBump1 );
		MatrixTranslation( &bump2Matrix, &transBump2 );
		appvar.pWater->SetMatrix( NvWaterShader::MI_BUMP1	, &bump1Matrix	);
		appvar.pWater->SetMatrix( NvWaterShader::MI_BUMP2	, &bump2Matrix	);
		appvar.pWater->SetMatrix( NvWaterShader::MI_DIFFUSE	, &diffMatrix	);
		
		
		Matrix warpRotation;
		MatrixRotation( &warpRotation, &Vec3(0.0f,0.0f,1.0f), -camAngle1 );
		appvar.pWater->SetMatrix( NvWaterShader::MI_REFLECTION, &warpRotation );
		
			
		// Compute ViewMat
		Matrix invViewMat;
		Vec3 camTrans(0.0f,0.0f,-200.0f);
		Quat camRot = Quat(camAngle1,Vec3(0.0f,0.0f,1.0f)) * 
					  Quat(camAngle2,Vec3(1.0f,0.0f,0.0f)) ;
		
		MatrixBuildTR(&invViewMat,&camTrans,&camRot,NULL);


		//Begin frame
		DpyManager::BeginFrame();
			DpyManager::SetSession(0);
					DpyManager::SetTarget	( DpyTarget(DPYR_IN_FRAME)						);	// Target
					DpyManager::SetView		( &invViewMat, &projMat, &clipRange, &viewport	);	// Set ViewProj matrix
					DpyManager::Draw		( appvar.pClear									);	// clear buffer

			DpyManager::SetSession(1);
					DpyManager::SetWorldTR	( &planeMat										);	// Set World matrix
					DpyManager::Draw		( appvar.pWater									);	// Draw water

			//Draw textures on quad.
			DpyManager::SetSession(2);
				DpyManager::SetWorldTR		( &unitMat										);	// Set World matrix
				DpyManager::Draw			( appvar.pMat									);
				DpyManager::Draw			( appvar.pLight									);
				DpyManager::Draw			( appvar.pBump									);
		
		//End and flush frame
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_16"


void	GameMain	(		)
{
	InitNeova();
	
	appvar.Init();
	if ( !appvar.Load() )
	{
		appvar.Shut();
		return;
	}
	
	
	GameLoop();
		
		
	// Shut
	appvar.Shut();
	ShutNeova();
}

