/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#include <Nova.h>

//Include the resources manager's header
#include <NvRscManager.h>

using namespace nv;




//The resources big file
#if   defined(_PSP)
#define BF_PATH "BIGFILE_PSP"
#elif defined(_PS2)
#define BF_PATH "BIGFILE_PS2"
#elif defined(_WIN32)
#define BF_PATH "BIGFILE_DX"
#elif defined(_NGC)
#define BF_PATH "BIGFILE_NGC"
#else
#define BF_PATH "BIGFILE"
#endif



void DoLoading	(		)
{
	//Load the resource with ID 1 from the big file
	RscPrefetchQ rscQ;					//The prefetch queue used to load the resource
	rscQ.AddRsc(1);						//Add the resource with ID 1 to the queue
	RscManager::AddPrefetchQ(&rscQ);	//Add the queue to the resources manager
	
	for (;;)
	{
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
		
		//Update the resources manager.
		//On each loop, the resources manager can load data
		//from the big file during a maximum of 1 ms. You can
		//set this value to any strictly positive integer.
		RscManager::Update(1);

		//Print out the loading progression
		if (rscQ.IsReady())
		{
			//If the prefetch queue is fulfilled
			if( rscQ.GetState() == RscPrefetchQ::COMPLETED )
				libc::Printf("Loading completed\n");
			else
				libc::Printf("Loading failed\n");

			RscManager::Unload(&rscQ);
			break;
		}
		else
		{
			//Get the progression of the currently processed
			//queue and keep the level value only if the currently
			//processed queue is rscQ.
			float level = 0;
			if (RscManager::GetProgress(&level) != &rscQ)
				level = 0;
			DebugPrintf("Loading... %.2f %%\n", level*100.f);
		}
	}
}


#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_02"

void GameMain()
{
	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		return;
	}
	
	//Initialize the resources manager
	if (!RscManager::Init())
	{
		core::Shut();
		NV_WARNING("Cannot init RscManager");
		return;
	}
	
	//Open the resources big file
	if (!RscManager::OpenBigFile(BF_PATH))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot open BigFile");
		return;
	}

	DoLoading();

	//Shut the resources manager
	RscManager::Shut();
	
	core::Shut();
}
