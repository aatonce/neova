/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





void AppVar::Init()
{
	cls	 = NULL;
	skel = NULL;
	
	cls	 = NvFrameClear::Create();
	cls->SetColor( 0x30304000 );
	
	
	
	mesh_prelit_UID			= FindUIDByName( "*l_infant*prelit*NvMesh*"			);
	mesh_tolit_UID			= FindUIDByName( "*l_infant*tolit*NvMesh*"			);
	uint32	skl_UID			= FindUIDByName( "*l_infant*NvSkeleton*"			);
			skla_UID[0]		= FindUIDByName( "*l_infant_attente1*NvSkelAnim*"	);
			skla_UID[1]		= FindUIDByName( "*l_infant_attente2*NvSkelAnim*"	);
			skla_UID[2]		= FindUIDByName( "*l_infant_marche1*NvSkelAnim*"	);
			skla_UID[3]		= FindUIDByName( "*l_infant_marche2*NvSkelAnim*"	);
			skla_UID[4]		= FindUIDByName( "*l_infant_tir1*NvSkelAnim*"		);

	UInt32A uidA;
		uidA.push_back( mesh_tolit_UID	);
		uidA.push_back( mesh_prelit_UID	);
		uidA.push_back( skl_UID			);
		uidA.push_back( skla_UID[0]		);
		uidA.push_back( skla_UID[1]		);
		uidA.push_back( skla_UID[2]		);
		uidA.push_back( skla_UID[3]		);
		uidA.push_back( skla_UID[4]		);
	SyncPrefetch( uidA );

	
	skel = NvSkeleton::Create( skl_UID );
	NV_ASSERT( skel );
}




uint AppVar::Load()
{
	for( uint i=0; i<NB_ANIM; i++ )
	{
		skla[i]	= NvSkelAnim::Create( skla_UID[i] );
		NV_ASSERT( skla[i] );

		mesh_tolit [i] = NvMesh::Create( mesh_tolit_UID  );
		mesh_prelit[i] = NvMesh::Create( mesh_prelit_UID );
		NV_ASSERT( mesh_tolit [i] );
		NV_ASSERT( mesh_prelit[i] );

		shd_tolit [i] = NvGeomShader::Create	( mesh_tolit [i] );
		shd_prelit[i] = NvGeomShader::Create	( mesh_prelit[i] );
		shd_toon  [i] = NvCartoonShader::Create	( mesh_tolit [i] );
		NV_ASSERT( shd_tolit [i] );
		NV_ASSERT( shd_prelit[i] );
		NV_ASSERT( shd_toon  [i] );

		// resize & init bones with pose mode !
		skel->InitState ( &skt[i].bone_states  );		
		skel->InitTarget( &skt[i].bone_targets );
		skt[i].t = 0;

		// map skeleton (bone set) animation
		skla[i]->Map( skel );

		// map mesh with bone's matrix array
		mesh_tolit [i]->MapSkeleton (	skel->GetBoneCpt(),
										skel->GetBoneNameA(),
										skt[i].bone_targets.blendA.data() );
		mesh_prelit[i]->MapSkeleton (	skel->GetBoneCpt(),
										skel->GetBoneNameA(),
										skt[i].bone_targets.blendA.data() );

		mesh_tolit [i]->Enable	( NvMesh::EN_NRM_SKINNING );
		mesh_prelit[i]->Enable	( NvMesh::EN_NRM_SKINNING );
		mesh_tolit [i]->Update	();
		mesh_prelit[i]->Update	();
	}
	
	return 1;
}



void AppVar::ChangeStateTexture()
{
	static bool	tex_enabled = TRUE;
	tex_enabled = !tex_enabled;
	
	for( uint i=0; i<NB_ANIM; i++ )
	{
		if( tex_enabled )
		{
			shd_prelit[i]->Enable ( NvShader::EN_TEXTURING );
			shd_tolit [i]->Enable ( NvShader::EN_TEXTURING );
			shd_toon  [i]->Enable ( NvShader::EN_TEXTURING );
		}
		else
		{
			shd_prelit[i]->Disable( NvShader::EN_TEXTURING );
			shd_tolit [i]->Disable( NvShader::EN_TEXTURING );
			shd_toon  [i]->Disable( NvShader::EN_TEXTURING );
		}
	}
}



void AppVar::Shut()
{
	SafeRelease( cls  );
	SafeRelease( skel );
	
	for( uint i=0; i<NB_ANIM; i++ )
	{
		SafeRelease( skla[i]		);
		SafeRelease( mesh_prelit[i] );
		SafeRelease( mesh_tolit[i]  );
		SafeRelease( shd_prelit[i]	);
		SafeRelease( shd_tolit[i] 	);
		SafeRelease( shd_toon[i]  	);
	}

	poseA.free();
}





void AppVar::CheckForCulling( float _camD )
{
	uint32	shd_en  = shd_prelit[0]->GetEnabled();
			shd_en &= ~( NvShader::EN_CULLING | NvShader::EN_CLIPPING );
			
	if( _camD > 0.6f )		shd_en |= 0;							// fast
	else					shd_en |= NvShader::EN_CULLING;			// culling
	
	for( uint i=0; i<NB_ANIM; i++ )
	{
		shd_tolit [i]->SetEnabled( shd_en );
		shd_prelit[i]->SetEnabled( shd_en );
	}
}


void AppVar::UpdateSkinning( float _deltaT )
{
	for( uint i=0; i<NB_ANIM; i++ )
	{
		// animation looping
		float endTime = skla[i]->GetEndTime();
		skt[i].t += _deltaT;
		while( skt[i].t > endTime )
			skt[i].t -= endTime;
			
		// update local bone's animations
		skla[i]->ComputeState( skt[i].t, skt[i].bone_states.trsA.data() );
		
		// convert local bone's animations (states) to world & blend matrices (targets)
		skel->ComputeTarget( &skt[i].bone_targets, &skt[i].bone_states );
		
		// update the mesh skinning process
		mesh_tolit [i]->Update();
		mesh_prelit[i]->Update();
	}
}


void AppVar::UpdateMatrix(float _camAlpha, float _camBeta, float _camD)
{
	Matrix camM;
	
	float	camFov	 = Pi * 0.25f;
	
	// cam location
	float r  = Vec2Norm( &bbox ) * _camD;
	float ca, sa, cb, sb;
	Sincos( _camAlpha, &sa, &ca );
	Sincos( _camBeta,  &sb, &cb );
	Vec3 loc( sa*cb*r, sb*r, ca*cb*r );

	// cam axys
	Vec3 cam_x, cam_y, cam_z;
	Vec3Normalize( &cam_z, &loc );
	cam_x = Vec3::UNIT_Y ^ cam_z;
	Vec3Normalize( &cam_x, &cam_x );
	cam_y = cam_z ^ cam_x;
	Vec3Normalize( &cam_y, &cam_y );

	MatrixIdentity( &camM );
	MatrixSetAxis( &camM, &cam_x, &cam_y, &cam_z );
	MatrixSetTR( &camM, &loc, NULL, NULL, &camM );

	MatrixFastInverse( &viewMat, &camM );
	
// Vec2 camClip( 0.5f, 5000.f );
	MatrixPerspectiveRH( &projMat, camFov, DpyManager::GetVirtualAspect(), 0.5f, 5000.0f );
}



void AppVar::Draw( uint _whatIsDisplay, bool _showperf )
{
	Vec2 camClip( 0.5f, 5000.f );
	
	DpyManager::BeginFrame();
	DpyManager::SetView( &viewMat, &projMat, &camClip );

	// CLS
	DpyManager::SetSession( 0 );
	DpyManager::Draw( cls );

	// Draw poses
	DpyManager::SetSession( 4 );
	int nbPose = poseA.size();
	for( int i=0; i<nbPose; i++ )
	{
		Matrix* wtr    = &poseA[i].worldTR;
		int     animNo = poseA[i].animNo;
		NV_ASSERT( animNo>=0 && animNo<NB_ANIM );
		
		DpyManager::SetWorldTR( wtr );
		if		( _whatIsDisplay == 0 )		DpyManager::Draw( shd_tolit	 [animNo] );
		else if	( _whatIsDisplay == 1 )		DpyManager::Draw( shd_prelit [animNo] );
		else								DpyManager::Draw( shd_toon	 [animNo] );
	}

	DpyManager::EndFrame();
	DpyManager::FlushFrame( TRUE, _showperf );

	if( _showperf )
	{
		char msg[ 128 ];
		Sprintf( msg, "POSE[%d]  GPU-FPS[%d]  CPU-FPS[%d]  TEX[%dKB]  #tri[%dK]  #loc[%dK]  #vtx[%dK]  #prim[%dK]",
			poseA.size(),
			int( DpyManager::GetPerformance( DpyManager::PERF_GPU_FPS	) 			)		,
			int( DpyManager::GetPerformance( DpyManager::PERF_CPU_FPS	) 			)		,
			int( DpyManager::GetPerformance( DpyManager::PERF_TEX_BSIZE	) 			) >> 10,
			int( DpyManager::GetPerformance( DpyManager::PERF_TRI_CPT	) * 0.001f 	)		,
			int( DpyManager::GetPerformance( DpyManager::PERF_LOC_CPT	) * 0.001f 	)		,
			int( DpyManager::GetPerformance( DpyManager::PERF_VTX_CPT	) * 0.001f 	)		,
			int( DpyManager::GetPerformance( DpyManager::PERF_PRIM_CPT	) * 0.001f 	)		);
		Printf( "%s\n", msg );
	}
}




void AppVar::BuildInstance( uint _nb )
{
	bbox = Vec2( _nb*2, _nb*2 );
	float cx = bbox.x * 0.5f;
	float cy = bbox.y * 0.5f;

	int nb = _nb * _nb;
	poseA.resize( nb );
	for( int i = 0 ; i < nb ; i++ )
	{
		int		colNo = i % _nb;
		int		rowNo = i / _nb;
		float	locx  = 1.f + colNo*2 - cx;
		float	locz  = 1.f + rowNo*2 - cy;
		Vec3 loc( locx, 0.f, locz );
		MatrixBuildTR( &poseA[i].worldTR, &loc, NULL, NULL );
		poseA[i].animNo  = libc::Rand() % NB_ANIM;
		NV_ASSERT( poseA[i].animNo >= 0 );
		NV_ASSERT( poseA[i].animNo <  NB_ANIM );
	}
}





