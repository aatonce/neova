/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#if defined(_WIN32)
#include <kernel\pc\nvwindows.h>
#include <kernel\pc\NvHrdw.h>
#endif


#include <Nova.h>
#include <NvUID.h>
#include <NvRscManager.h>
#include <NvDpyManager.h>
#include <NvSndManager.h>
#include <NvMesh.h>
#include <NvSkeleton.h>
#include <NvSkelAnim.h>
#include <NvGeomShader.h>
#include <NvCartoonShader.h>
#include <NvFrameClear.h>
#include <NvBitmap.h>
#include <NvSurface.h>



#if   defined(_PSP)
#define BF_PATH "BIGFILE_PSP"
#elif defined(_PS2)
#define BF_PATH "BIGFILE_PS2"
#elif defined(_WIN32)
#define BF_PATH "BIGFILE_DX"
#elif defined(_NGC)
#define BF_PATH "BIGFILE_NGC"
#else
#define BF_PATH "BIGFILE"
#endif



using namespace nv;


uint	PreparePrefetch		( RscPrefetchQ* 	_inPrefetchQ							);
uint32	FindUIDByName		( pcstr				inPatternFn								);
void	SyncPrefetch		( UInt32A	&		inUIDA									);
void	ShowNoPadPlugged	( uint 				inCpt		,	bool inReleaseData		);

