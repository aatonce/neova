/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"
#include "appctrl.h"





void InitNeova();
void ShutNeova();
uint UpdateNeova();

void GameLoop();





void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif


	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		core::Exit();
	}
	
	if (!RscManager::Init())
	{
		NV_WARNING("Cannot init RscManager");
		core::Shut();
		core::Exit();
	}
	
	if (!RscManager::OpenBigFile(BF_PATH))
	{
		NV_WARNING("Cannot open BigFile");
		RscManager::Shut();
		core::Shut();
		core::Exit();
	}
	
	//Initialize the display manager
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		NV_WARNING("Cannot init DpyManager");
		RscManager::Shut();
		core::Shut();
		core::Exit();
	}

	//Initialize the display manager
	if (!SndManager::Init( ))
	{
		NV_WARNING("Cannot init DpyManager");
		RscManager::Shut();
		core::Shut();
		core::Exit();
	}
}



void ShutNeova()
{
	SndManager::Shut();
	DpyManager::Shut();
	RscManager::CloseBigFile();
	RscManager::Shut();
	nv::core::Shut();
}


uint UpdateNeova()
{
	uint upd_flags = core::Update();
	RscManager::Update( 1.0f );		// 1 ms
	SndManager::Update();
	
	return upd_flags;
}






void GameLoop()
{
	AppVar appvar;

	appvar.Init();
	appvar.Load();

	int nb_pose = 10;
	appvar.BuildInstance( nb_pose );


	float	camAlpha = 0.0f;
	float	camBeta  = 0.0f;
	float	camD	 = 1.0f;

	bool	showPerf = FALSE;
	int		showMode = 0;		// 0:tolit 1:prelit 2:toon

	clock::Time initTime;
	clock::GetTime( &initTime );
	
	float deltaTime, elapsedTime, lElapsedTime = 0;

	for( ;; )
	{
		uint upd_flags = UpdateNeova();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;

		// Timing
		{
			clock::Time currTime;			
			clock::GetTime( &currTime );
			
			elapsedTime = currTime 	  - initTime;
			deltaTime 	= elapsedTime - lElapsedTime;
			
			lElapsedTime = elapsedTime;
		}

		// Controller
		{
			AppCtrl::AppInputs input;
			if( !AppCtrl::GetController( &input ) )
				return ;

			if( input.button0_pressed )	{ showMode = (++showMode) % 3; 										}	// (tolit || prelit || toon) Display
			if( input.button1_pressed )	{ nb_pose++;					appvar.BuildInstance( ++nb_pose );	}	// Increase instance
			if( input.button2_pressed )	{ nb_pose=Max( 1, nb_pose-1 );	appvar.BuildInstance( nb_pose 	);	}	// Decrease instance
			if( input.button3_pressed )	{ appvar.ChangeStateTexture();										}	// (Enable || Disable) Texture 
			
			showPerf = input.oneButton_pressed;

			
			camAlpha += input.dir2.x * deltaTime;
			while( camAlpha> TwoPi )	camAlpha -= TwoPi;
			while( camAlpha<-TwoPi )	camAlpha += TwoPi;
			
			camBeta = Clamp( camBeta - ( input.dir2.y*deltaTime ), Pi*0.025f, Pi*0.3f );
			camD	= Clamp( camD	 + ( input.dir1.y*deltaTime ), 0.1f	 , 1.0f    );
		}
		
		
		// Update culling, skinning, matrix  and draw scene
		{
			appvar.CheckForCulling( camD 		);
			appvar.UpdateSkinning ( deltaTime   );
		
			appvar.UpdateMatrix( camAlpha, camBeta, camD );
			appvar.Draw( showMode, showPerf );
		}
	}

	appvar.Shut();
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_07"

void GameMain()
{
	// Init
	InitNeova();

	// GameLoop
	GameLoop();

	// Shut
	ShutNeova();
}
