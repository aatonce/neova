/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _APPVAR_H_
#define _APPVAR_H_


#include "appdef.h"

#define		NB_ANIM		5




struct Pose
{
	Matrix				worldTR;		// Used for create a 
	int					animNo;
};
typedef vector<Pose>	PoseA;



struct SkelTarget
{
	float				t;				// local anim time
	NvSkeleton::State	bone_states;

	ALIGNED128( NvSkeleton::Target, bone_targets );
};




struct AppVar
{
	uint32	skla_UID[NB_ANIM];
	uint32	mesh_prelit_UID;
	uint32	mesh_tolit_UID;
	
	NvFrameClear*		cls;
	NvSkeleton*			skel;
	
	NvSkelAnim*			skla		[NB_ANIM];
	NvMesh*				mesh_prelit	[NB_ANIM];
	NvMesh*				mesh_tolit	[NB_ANIM];
	NvGeomShader*		shd_prelit	[NB_ANIM];
	NvGeomShader*		shd_tolit	[NB_ANIM];
	NvCartoonShader*	shd_toon	[NB_ANIM];
	
	ALIGNED128( SkelTarget, skt[NB_ANIM] );
	
	
	
	Matrix viewMat, projMat;
	
	
	PoseA poseA;			// Mesh instancing
	Vec2  bbox;
	
	
	void Init();
	void Shut();
	uint Load();
	
	
	void ChangeStateTexture	(													);
	void BuildInstance		( uint _nb 											);	// Update poseA and bbox
	
	void CheckForCulling	( float _camD 										);
	void UpdateSkinning		( float _deltaT 									);
	void UpdateMatrix		( float _camAlpha, 		float _camBeta, float _camD );
	void Draw				( uint _whatIsDisplay,	bool _showperf 				);
};


#endif // _APPVAR_H_

