/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#include <NvDpyManager.h>
#include <NvFrameClear.h>
#include <NvSurface.h>
#include <NvGeomShader.h>
#include <NvImage.h>

#include "HelpDisplay.h"

//----------------------------------------------------------------------------------------------------------------

NvSurface*		surf	=	NULL	;
NvBitmap*		bmp		=	NULL	;
NvGeomShader *	shader	=	NULL	;
NvFrameClear *	clear	=	NULL	;


bool
display::Init(NvBitmap * inBmp)
{
	if (surf || bmp || shader) return FALSE;
	if (! inBmp) return FALSE;
	inBmp->AddRef();
	bmp = inBmp;

	float bmpW = float(bmp->GetWidth());
	float bmpH = float(bmp->GetHeight());

	surf	= NvSurface::Create(4 , NvSurface::CO_LOC | NvSurface::CO_TEX );
	shader	= NvGeomShader::Create(surf);
	clear		= NvFrameClear::Create();
	NV_ASSERT( surf );
	NV_ASSERT( shader );
	NV_ASSERT( clear );

	surf->EnableDBF();

	Vec2   tex;
	Vec3   pos;

	surf->Begin();
	pos=Vec3(0.0f,0.0f,0.0f);
	tex=Vec2(0.0f,0.0f);
	surf->TexCoord(tex);
	surf->Vertex(pos);
	pos=Vec3(float(bmpW),0.0f,0.0f);
	tex=Vec2(1.0f,0.0f);
	surf->TexCoord(tex);
	surf->Vertex(pos);
	pos=Vec3(0.0f,float(bmpH),0.0f);
	tex=Vec2(0.0f,1.0f);
	surf->TexCoord(tex);
	surf->Vertex(pos);
	pos=Vec3(float(bmpW),float(bmpH),0.0f);
	tex=Vec2(1.0f,1.0f);
	surf->TexCoord(tex);
	surf->Vertex(pos);
	surf->End();

	shader->Enable		( NvGeomShader::EN_THROUGHMODE);
	shader->Disable		( NvShader::EN_RD_DEPTH );
	shader->SetDrawStart( 0 );
	shader->SetDrawSize	( 4 );
	DpyState * dpys = shader->GetDisplayState(0);
	DpySource srcBmp	(bmp);
	dpys->SetSource		(srcBmp);
	dpys->Enable		(DpyState::EN_TEXTURING);
	dpys->SetMode		(DpyState::CM_TWOSIDED );
	dpys->SetMode		(DpyState::TM_NEAREST );
	dpys->SetAlphaPass	(0.7f);

	clear->EnableColor	(TRUE);
	clear->SetColor		(0xF7941D00);
	return TRUE;
}

void
display::Shut()
{
	SafeRelease(surf);
	SafeRelease(bmp);
	SafeRelease(shader);
	SafeRelease(clear);
}

void
display::Draw()
{
	if (! surf || ! bmp || ! shader) return ;

	float bmpW = float(bmp->GetWidth());
	float bmpH = float(bmp->GetHeight());

	Matrix recTransfo;
	uint rW = DpyManager::GetRasterWidth (DPYR_IN_FRAME);
	uint rH = DpyManager::GetRasterHeight(DPYR_IN_FRAME);
	Vec3 pos(float((rW-bmpW)/2),float((rH-bmpH)/2),0.0f);
	MatrixBuildTR( &recTransfo, &pos, NULL, NULL );

	Vec4 viewport( 0, 0, float(rW), float(rH) );
	Matrix	invWorldTransfo,	projTransfo;
	Vec2	clipRange(1, 100);
	float	fov = Pi/4;
	MatrixIdentity(&invWorldTransfo);
	MatrixPerspectiveRH( &projTransfo, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );
	
	DpyManager::BeginFrame	(							);
	DpyManager::SetSession	(	0						);
	DpyManager::SetTarget	(	DPYR_IN_FRAME);
	DpyManager::SetView		(	&invWorldTransfo		, 
								&projTransfo			, 
								&clipRange				, 
								&viewport				);
	DpyManager::Draw		(	clear					);
	DpyManager::SetSession	(	1						);			
	DpyManager::SetWorldTR	(	&recTransfo				);
	DpyManager::Draw		(	shader					);
	DpyManager::EndFrame	(							);
	DpyManager::FlushFrame	(							);
}




