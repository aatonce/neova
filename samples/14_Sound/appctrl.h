/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef _APPCTRL_H_
#define _APPCTRL_H_




#include "appdef.h"







	
namespace AppCtrl
{
	struct AppInputs
	{
		Vec2  dir1, dir2;
		
		bool button0_pressed,
			 button1_pressed,
			 button2_pressed,
			 button3_pressed,
			 button9_pressed,
			 dir3L_pressed,
			 dir3R_pressed,
			 dir3U_pressed,
			 dir3D_pressed;
			 
		float fltButton4,
			  fltButton5,
			  fltButton6,
			  fltButton7;
	};

	
	// Get the first status and return fill AppInputs
	bool	GetController	( AppInputs* _outAppInput 		);
	
	
	// Private function
	int		WaitController	(								);
	int		FindController	(								);
}



#endif // _APPCTRL_H_
