/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/





#include "appvar.h"
#include "HelpDisplay.h"
#include "appctrl.h"






namespace
{
	AppVar appvar;
}


bool MyStreamCallBack( int inSoundId, float &outBegin, float &outDuration );

void InitNeova();
void ShutNeova();

void GameLoop();









void InitNeova()
{
//	core::SetParameter( core::PR_CONSOLE_ONLY, TRUE );

	#if defined(FROM_CD)
	core::SetParameter( core::PR_MEDIA_TYPE, "cdrom" );
	#elif defined(FROM_DVD)
	core::SetParameter( core::PR_MEDIA_TYPE, "dvdrom" );
	#endif

	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_DX_D3DADAPTER, uint32(1) );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif

	#if !defined(_ATMON) && (defined(FROM_CD) || defined(FROM_DVD))
	core::SetParameter( core::PR_FILE_PREFIX, "cdrom0:\\" );
	core::SetParameter( core::PR_FILE_SUFFIX, ";1" );
	#endif

	if( !core::Init() )
	{
		NV_WARNING("Cannot init core");
		core::Exit();
		return;
	}

	#if defined(_ATMON) && (defined(FROM_CD) || defined(FROM_DVD))
	core::SetParameter( core::PR_FILE_PREFIX, "cdrom0:\\" );
	core::SetParameter( core::PR_FILE_SUFFIX, ";1" );
	#endif

	if( !RscManager::Init() )
	{
		NV_WARNING("Cannot init RscManager");
		core::Shut();
		core::Exit();
		return;
	}

	if (!RscManager::OpenBigFile(BF_PATH))
	{
		NV_WARNING("Cannot open BigFile");
		RscManager::Shut();
		core::Shut();
		core::Exit();
		return;
	}

	if( !SndManager::Init() )
	{
		NV_WARNING("Cannot init SndManager");
		RscManager::Shut();
		core::Shut();
		core::Exit();
		return;
	}

	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		NV_WARNING("Cannot init DpyManager");
		RscManager::Shut();
		core::Shut();
		core::Exit();
		return;
	}
}



void ShutNeova()
{
	SndManager::Shut();
	DpyManager::Shut();
	RscManager::Shut();

	core::Shut();
}


bool MyStreamCallBack( int inSoundId, float &outBegin, float &outDuration )
{
	if( inSoundId==appvar.curBgmId && appvar.curSegmentDuration)
	{
		outBegin		= appvar.curSegmentBegin;
		outDuration		= appvar.curSegmentDuration;
		Printf( "stream-CB: BGM %d: (%f,%f) return TRUE\n", inSoundId, outBegin, outDuration );
		return TRUE;
	}
	else
	{
		Printf( "stream-CB: %d: return TRUE\n", inSoundId );
	}
	return TRUE;
}

void GameLoop	(			)
{
	display::Init(appvar.displayHelp);
	SndManager::SetHandler (&MyStreamCallBack);

	int	curMarker = 0;
	uint nbMarker = appvar.stream1->GetMarkerCpt();
	curMarker = nbMarker - 1;

	// lock all NvSound
	int sd1Id = SndManager::SdLock	( SndManager::SD_ANY, appvar.snd1		);
	int sd2Id = SndManager::SdLock	( SndManager::SD_ANY, appvar.snd2		);
	if ( sd1Id < 0 || sd2Id < 0 )
	{
		appvar.Shut();
		return ;
	}
	// waiting for sound locking.
	while (!SndManager::SdIsLocked(sd1Id) || !SndManager::SdIsLocked(sd2Id) )
	{
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;

		SndManager::Update();
		if ( (!SndManager::SdIsLocked(sd1Id) && !SndManager::SdIsLocking(sd1Id)) ||
			 (!SndManager::SdIsLocked(sd2Id) && !SndManager::SdIsLocking(sd2Id))	)
			return ;
	}

	SndManager::Update();

	float	sndPan		= 0.f;
	float	bgmVol		= 0.5f;
	bool	bgmInPause	= FALSE;
	int		bgmId = appvar.curBgmId = SndManager::SdAlloc();

	// Sound state is required only if you want to manage
	// the end-of-playing state of a sound !
	// 0 : idle
	// 1 : started by a ChPlay() call
	// 2 : listening
	int		sndState[2];
	sndState[0] = 0;
	sndState[1] = 0;

	
	clock::Time initTime;
	clock::GetTime( &initTime );
	float deltaTime, elapsedTime, lElapsedTime = 0;

	// draw data
	for (;;)
	{
		// core update
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
		
		
		// update sound manager.
		SndManager::Update();
		
		// Timing
		{
			clock::Time currTime;			
			clock::GetTime( &currTime );
			
			elapsedTime = currTime 	  - initTime;
			deltaTime 	= elapsedTime - lElapsedTime;
			
			lElapsedTime = elapsedTime;
		}

		
		// Controller
		{
			AppCtrl::AppInputs input;
			if( !AppCtrl::GetController( &input ) )
				break ;
			
			
			sndPan+=( input.dir1.x * deltaTime );


			// play sound1 if it is not already playing.
			if( sndState[0] )
			{
				bool playing = SndManager::ChIsPlaying( 1 );
				if( !playing )
					sndState[0] = 0;	// stopped
			}
			if( input.button0_pressed && sndState[0]==0  )
			{
				SndManager::ChPlay( 1, sd1Id,1.f,sndPan, 1.f );
				SndManager::ChWaitPlaying( 1 );
				sndState[0] = 1;		// started, by not listening yet !
			}
			
			// play sound2 if it is not already playing.
			if( sndState[1] )
			{
				bool playing = SndManager::ChIsPlaying( 2 );
				if( !playing )
					sndState[1] = 0;	// stopped
			}
			if( input.button1_pressed && sndState[1]==0 )
			{
				SndManager::ChPlay( 2, sd2Id,1.f,sndPan, 1.f );
				SndManager::ChWaitPlaying( 2 );
				sndState[1] = 1;	// started, by not listening yet !
			}

			if( input.button3_pressed )		{ appvar.InitTestDolbyDigital(); }		// test multi channel.
			

			
			if( !SndManager::BgmIsPlaying() )
			{
				SndManager::SdState bmgStt = SndManager::SdGetState( bgmId );
				if( bmgStt==SndManager::SD_LOCKED )
				{
					// start background music
					SndManager::BgmPlay(bgmId,bgmVol);
					SndManager::BgmWaitPlaying();
					Printf( "BGM is locked. Now start to play ...\n" );
				}
				else if( bmgStt<=SndManager::SD_UNLOCKED )
				{
					static int waitcpt = 0;
					waitcpt++;
					if( waitcpt==500 )
					{
						Printf( "BGM is unlocked. Now start to lock ...\n" );
						waitcpt = 0;
					}
					if (curMarker >= 0)
					{
						float beginning = appvar.stream1->GetMarkerTime(curMarker);
						float end		= (curMarker != nbMarker-1)?appvar.stream1->GetMarkerTime(curMarker+1):appvar.stream1->GetDuration();
						float duration  = end - beginning;
						SndManager::SdLock( bgmId, appvar.stream1, beginning, duration );
						appvar.curBgmId = bgmId;
						appvar.curSegmentBegin		= beginning;
						appvar.curSegmentDuration  = duration ;
					}
					else
					{
						SndManager::SdLock( bgmId, appvar.stream1 );
						appvar.curSegmentDuration = 0;
						appvar.curSegmentBegin = 0;
					}
				}
				else
				{
					static int lockcpt = 0;
					lockcpt++;
					if( lockcpt==500 )
					{
						Printf( "BGM is in state %d ...\n", int(bmgStt) );
						lockcpt = 0;
					}
				}
			}


			// set Background music volume
			if( input.dir3U_pressed )
			{
				bgmVol += 1.0f * deltaTime;
				bgmVol  = Clamp( bgmVol , 0.0f, 1.0f );
				SndManager::BgmSetVolume( bgmVol );
			}
			if( input.dir3D_pressed )
			{
				bgmVol -= 1.0f* deltaTime;
				bgmVol  = Clamp( bgmVol , 0.0f, 1.0f );
				SndManager::BgmSetVolume( bgmVol );
			}

			if( input.dir3R_pressed )
			{
				uint nbMarker = appvar.stream1->GetMarkerCpt();
				Printf( "BGM has %d markers.\n", nbMarker );
				if( nbMarker )
				{
					// change segment with the next sound marker.
					curMarker++;
					curMarker %= nbMarker;
					appvar.curSegmentBegin = appvar.stream1->GetMarkerTime(curMarker);

					float end;
					if(curMarker != nbMarker-1)	end = appvar.stream1->GetMarkerTime(curMarker+1);
					else						end = appvar.stream1->GetDuration();

					appvar.curSegmentDuration = end - appvar.curSegmentBegin;

					SndManager::BgmBreak();
				}
			}
			
			if( input.dir3L_pressed )
			{
				if( !bgmInPause )
				{
					SndManager::BgmPause();		// stop bgm and keep the current position 
					bgmInPause = TRUE;			// its state is "Playing" and not "Idle"
				}
				else
				{
					// resume bgm
					SndManager::BgmResume();
					bgmInPause = FALSE;
				}
			}
		}
		
		appvar.UpdateTestDolbyDigital();
		display::Draw();
	}
}



#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_14"


void	GameMain	(		)
{
	// Init
	InitNeova();
	
	appvar.Init();
	if( !appvar.Load() )
	{
		appvar.Shut();
		ShutNeova();
		return;
	}

	// GameLoop
	GameLoop();
	
	

	// Shut
	appvar.Shut();
	ShutNeova();
}

