/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"

#define SOUND1			"*selected*"
#define SOUND2			"*changed*"
#define STREAM1			"*bgm*wav*"
//#define STREAM1			"*ambiance*wav*"
#define HELP			"*Help-14_Sound*tif*"


const char* speakersRscName[] =
{
	"fcenter",
	"fright",
	"sright",
	"rright",
	"rcenter", 
	"rleft",
	"sleft",
	"fleft"
};
							



/* ---------------------------------		AppVar			--------------------------------------------- */
void AppVar::Init()
{
	snd1				= NULL;
	snd2				= NULL;
	stream1				= NULL;
	for( uint i=0; i<8; i++)
	{
		dpl2SndTest[i]		= NULL;
		dpl2spkSnd[i]	    = -1;
	}

	curBgmId			= -1;
	curSegmentBegin		= 0;
	curSegmentDuration	= 0; 

	displayHelp	= NULL;
}





void AppVar::InitTestDolbyDigital()
{
	if ( dpl2spkSnd[0] >= 0)
		return;

	// Switch to Dolby ProLogic 2 ?
	if( !SndManager::ProbeOutputMode(SndManager::OM_DPL2) )
		return;
	SndManager::SetOutputMode( SndManager::OM_DPL2 );

	for (uint i = 0 ; i < 8 ; ++i )
		dpl2spkSnd[i] = SndManager::SdLock( SndManager::SD_ANY, dpl2SndTest[i] );

	while( TRUE ) {
		uint ret = core::Update();
		if (ret & (core::UPD_EXIT_ASKED|core::UPD_EXITED))
			return ;
		SndManager::Update();
		bool again = FALSE;
		for( int i = 0 ; i < 8 ; i++ ) {
			if( dpl2spkSnd[i] < 0 )
				continue;
			if( SndManager::SdIsLocked(dpl2spkSnd[i]) )
				continue;
			if (SndManager::SdGetState(dpl2spkSnd[i]) != SndManager::SD_LOCKING)
				dpl2spkSnd[i] = -1;

			again = TRUE;
		}
		if( !again )
			break;
	}
}

void AppVar::UpdateTestDolbyDigital()
{
	static int curSound = -1;
	static int snd_ch	= -1;

	if( snd_ch!=-1 )
	{
		NV_ASSERT( dpl2spkSnd[curSound]>=0 );
		if( SndManager::ChIsPlaying(snd_ch) )
			return ;	// a sound is played

		SndManager::SdFree( dpl2spkSnd[curSound] );
		dpl2spkSnd[curSound] = -1;
		curSound			 = -1;
		snd_ch				 = -1;
	}
	NV_ASSERT( curSound==-1 );

	uint i;
	for( i=0; i<8; ++i )
	{
		if( dpl2spkSnd[i]<0 )
			continue;

		if( !SndManager::SdIsLocked(dpl2spkSnd[i]) )
			continue;

		curSound = i;
		break;
	}
	if (curSound < 0)
		return;

	// set panAngle for this channel to define sound orientation.
	float panAngle = (2 * math::Pi) * float(i) / 8.f;
	snd_ch = SndManager::ChPlay( SndManager::CH_ANY, dpl2spkSnd[curSound], 1.f, panAngle );
	
	SndManager::ChWaitPlaying( snd_ch );
}

uint AppVar::Load()
{
	// loading ressource
	uint32	sound1ID		= AppTools::FindUIDByName( SOUND1	);
	uint32	sound2ID		= AppTools::FindUIDByName( SOUND2	);
	uint32	stream1ID		= AppTools::FindUIDByName( STREAM1	);
	uint32	bmpHelpID		= AppTools::FindUIDByName( HELP		);
	uint32	speakerUID[8]	;

	RscPrefetchQ rscQ;
		rscQ.AddRsc(sound1ID);
		rscQ.AddRsc(sound2ID);
		rscQ.AddRsc(stream1ID);
		rscQ.AddRsc(bmpHelpID);

	for( int i=0; i<8; i++ )
	{
		char tmp[64];
		Sprintf( tmp, "*%s*", speakersRscName[i] );
		uint32 spkUID = AppTools::FindUIDByName( tmp );
		if( !spkUID )
		{
			speakerUID[i] = 0;
			Printf( "spk-snd %s not found !\n", tmp );			
		}
		else
		{
			speakerUID[i] = spkUID;
			rscQ.AddRsc(spkUID);
		}		
	}

	RscManager::AddPrefetchQ(&rscQ);
	
	if(!AppTools::PreparePrefetch(&rscQ))
	{
		NV_WARNING("Cannot create Sound");
		return 0;
	}

	snd1		= NvSound::Create(sound1ID);
	snd2		= NvSound::Create(sound2ID);
	stream1		= NvSound::Create(stream1ID);
	displayHelp = NvBitmap::Create(bmpHelpID);

	for( uint i=0; i<8; i++ )
	{
		dpl2SndTest[i] = NvSound::Create( speakerUID[i] );
		if( !dpl2SndTest[i] || dpl2SndTest[i]->IsStreamed() )
		{
			Printf( "Error on spk sound %s !\n", speakersRscName[i] );
			return 0;
		}
	}

	if( snd1 && snd2 && stream1 && displayHelp )
		return 1;
	else
		return 0;
}


void AppVar::Shut()
{
	SafeRelease(snd1);
	SafeRelease(snd2);
	SafeRelease(stream1);
	SafeRelease(displayHelp);
	for (uint i = 0 ; i < 8 ; ++i ) 
		SafeRelease(dpl2SndTest[i]);
}




