/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"




void AppVar::Init()
{
	pClear	= NULL;
	boxSurf	= NULL;
	boxShd	= NULL;
	
	// The clear object
	pClear   = NvFrameClear::Create();
	NV_ASSERT( pClear );
	pClear->SetColor(0x5555AAFF);

	// The box geometry
	boxSurf = NvSurface::Create( 4, NvSurface::CO_LOC|NvSurface::CO_COLOR );
	NV_ASSERT( boxSurf );

	// The box drawing shader
	boxShd  = NvGeomShader::Create( boxSurf );
	NV_ASSERT( boxShd  );
	boxShd->SetDrawStart( 0 );
	boxShd->SetDrawSize( 4 );

	// Activate the 2D mode here !
	boxShd->Enable ( NvShader::EN_THROUGHMODE 	);
	boxShd->Disable( NvShader::EN_RD_DEPTH 		);
	boxShd->GetDisplayState(0)->SetMode( DpyState::CM_TWOSIDED );
}



void AppVar::Shut()
{
	SafeRelease( boxSurf );
	SafeRelease( boxShd  );
	SafeRelease( pClear  );
}



void AppVar::UpdateBox( float _deltaT )
{
	int   Width   = DpyManager::GetRasterWidth ( DPYR_IN_FRAME );
	int   Height  = DpyManager::GetRasterHeight( DPYR_IN_FRAME );
	
	float sizeX = float(  Width/4.0f ) * Abs( Cos(_deltaT) );
	float sizeY = float( Height/4.0f );
	
	Vec2  vMin = Vec2( (Width/2.0f) - sizeX, Height/2.0f - sizeY );
	Vec2  vMax = Vec2( (Width/2.0f) + sizeX, Height/2.0f + sizeY );
	
	Vec3 	positionArray[4];											// v0 ----- v1
		positionArray[0] = Vec3(vMin.x, vMin.y, 0.0f);					// |     /  |
		positionArray[1] = Vec3(vMax.x, vMin.y, 0.0f);					// |   /    |
		positionArray[2] = Vec3(vMin.x, vMax.y, 0.0f);					// | /      |
		positionArray[3] = Vec3(vMax.x, vMax.y, 0.0f);					// v2 ----- v3
	
	
	Psm		boxColorPSM = NvSurface::GetNativeColorPSM();
	uint32	boxColor    = GetPSM( boxColorPSM, 255, 0, 0, 255 );
	
	
	// Set Vertex position and color to the boxSurf
	boxSurf->EnableDBF();
	boxSurf->Begin();
	for( uint i=0; i<4; i++)
	{
		boxSurf->Color ( boxColor, boxColorPSM );			
		boxSurf->Vertex( positionArray[i] );
	}
	boxSurf->End();

	
	// Update box Position
	float cos2T = Abs( Cos(_deltaT*2.0f) );
	
	boxPosition = Vec3( 0.0f								,
						-cos2T * float(Height/4.0f-16.0f)	,
						0.0f 								);
}

void AppVar::Draw2D()
{
	// Update the box 2D transfo
	Matrix worldMat;
	MatrixBuildTR( &worldMat, &boxPosition, NULL, NULL );
	
	
	// Draw Frame
	DpyManager::BeginFrame();
	DpyManager::SetTarget( DpyTarget(DPYR_IN_FRAME) );

	DpyManager::Draw( pClear );						// Clear
	DpyManager::SetWorldTR( &worldMat );			// Set world matrix
	DpyManager::Draw( boxShd );						// Set world matrix

	DpyManager::EndFrame();
	DpyManager::FlushFrame();
}