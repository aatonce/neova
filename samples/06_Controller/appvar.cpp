/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"





void AppVar::Init()
{
	meshID = 0;
	
	rscQ 		= NULL;
	pRscMesh 	= NULL;
	pDispMesh 	= NULL;
	pClear 		= NULL;
	
	meshID = RscManager::FindRscByType(NvMesh::TYPE);
	Printf( "meshId: %x\n", meshID );
	
	rscQ = NvGameNew( RscPrefetchQ );
	rscQ->AddRsc(meshID);
	RscManager::AddPrefetchQ(rscQ);
	
	pClear = NvFrameClear::Create();
	pClear->EnableColor( TRUE );
	pClear->EnableDepth( TRUE );
	pClear->SetColor(0x5555AAFF);
}




uint AppVar::Load()
{
	// Load Resource
	if(!PreparePrefetch(rscQ))
	{
		Shut();
		return 0;
	}
	
	pRscMesh = NvMesh::Create(meshID);
	pDispMesh = NvGeomShader::Create(pRscMesh);
	
	if (!pRscMesh || !pDispMesh)
	{
		NV_WARNING("Cannot create Mesh");
		core::Exit();
		
		return 0;
	}
	
	
	// Create Box 2D to show wii cursor |  Loock sample 09_Draw2D
	boxSurf = NvSurface::Create( 4, NvSurface::CO_LOC|NvSurface::CO_COLOR );
	NV_ASSERT( boxSurf );
	boxShd  = NvGeomShader::Create( boxSurf );
	NV_ASSERT( boxShd  );
	boxShd->SetDrawStart( 0 );
	boxShd->SetDrawSize( 4 );
	boxShd->Enable ( NvShader::EN_THROUGHMODE );
	boxShd->Disable( NvShader::EN_RD_DEPTH 	  );
	boxShd->GetDisplayState(0)->SetMode( DpyState::CM_TWOSIDED );
	Psm		boxColorPSM = NvSurface::GetNativeColorPSM();
	uint32	boxColor    = GetPSM( boxColorPSM, 255, 0, 0, 255 );
	
	boxSurf->EnableDBF();
	boxSurf->Begin();
		boxSurf->Color( boxColor, boxColorPSM );		// v0 ----- v1
		boxSurf->Vertex( Vec3(-3.0f, 3.0f, 0.0f) );		// |      / |
		boxSurf->Color( boxColor, boxColorPSM );		// |     /  |
		boxSurf->Vertex( Vec3( 3.0f, 3.0f, 0.0f) );		// |    /   |
		boxSurf->Color( boxColor, boxColorPSM );		// |   /    |
		boxSurf->Vertex( Vec3(-3.0f,-3.0f, 0.0f) );		// |  /     |
		boxSurf->Color( boxColor, boxColorPSM );		// | /      |
		boxSurf->Vertex( Vec3( 3.0f,-3.0f, 0.0f) );		// v2 ----- v3
	boxSurf->End();
	
	return 1;
}



void AppVar::Shut()
{
	SafeRelease( pDispMesh	);
	SafeRelease( pRscMesh	);
	SafeRelease( pClear		);
	
	SafeRelease( boxSurf 	);
	SafeRelease( boxShd  	);
	
	
	NvGameDelete(rscQ);
	rscQ = NULL;
}



void AppVar::DrawCursor(Vec2 _pos)
{
	Matrix boxTransfo;
	_pos = (_pos+Vec2(1.0f, 1.0f)) / 2.0f;				// Rescale position cursor : projection in screen coord
	
	Vec3   boxPos( 	_pos.x * (float)DpyManager::GetRasterWidth ( DPYR_IN_FRAME ),
					_pos.y * (float)DpyManager::GetRasterHeight( DPYR_IN_FRAME ),
					0.0f 												);
	MatrixBuildTR( &boxTransfo, &boxPos, NULL, NULL );
		
	DpyManager::SetWorldTR( &boxTransfo );
	DpyManager::Draw( boxShd );
}

void AppVar::DrawDefCursor(Vec2 _pos)
{
	Matrix boxTransfo;
	
	Vec3   boxPos( 	_pos.x, _pos.y, 0.0f );
	MatrixBuildTR( &boxTransfo, &boxPos, NULL, NULL );
		
	DpyManager::SetWorldTR( &boxTransfo );
	DpyManager::Draw( boxShd );
}


void AppVar::Draw(Matrix* pTransfo)
{
	DpyManager::SetTarget(DpyTarget(DPYR_IN_FRAME));
	Vec4 viewport( 0, 0, DpyManager::GetRasterWidth(DPYR_IN_FRAME), DpyManager::GetRasterHeight(DPYR_IN_FRAME)	);
	Matrix invWorldTransfo;
	MatrixIdentity(&invWorldTransfo);
	float fov = Pi/4;
	Vec2 clipRange(0.1f, 100);
	Matrix projTransfo;
	MatrixPerspectiveRH( &projTransfo, fov, DpyManager::GetVirtualAspect(), clipRange.x, clipRange.y );
	DpyManager::SetView( &invWorldTransfo, &projTransfo, &clipRange, &viewport);

	DpyManager::SetSession(0);
	DpyManager::Draw(pClear);
	
	DpyManager::SetSession(1);
	if (pTransfo)
		DpyManager::SetWorldTR(pTransfo);
	DpyManager::Draw(pDispMesh);
}



