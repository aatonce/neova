/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _APPVAR_H_
#define _APPVAR_H_


#include "appdef.h"



struct AppVar
{
	uint32 			meshID;
	RscPrefetchQ* 	rscQ;
	
	NvMesh* 		pRscMesh;
	NvGeomShader* 	pDispMesh;
	NvFrameClear* 	pClear;
	
	
	NvGeomShader*	boxShd;
	NvSurface*		boxSurf;
	
	void Init();
	void Shut();
	
	uint Load();
	
	void DrawCursor(Vec2 _pos);
	void DrawDefCursor(Vec2 _pos);
	
	void Draw(Matrix* pTransfo);
};


#endif // _APPVAR_H_

