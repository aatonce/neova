/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appvar.h"
#include "appctrl.h"




namespace
{
	AppVar appvar;
}


void InitNeova();
void ShutNeova();

void GameLoop();





void InitNeova()
{
	#if defined(_DX) && defined(_DEBUG)
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(800) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(600) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(800) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(600) );
	#elif defined(_DX)
	core::SetParameter( core::PR_DX_FULLSCREEN, TRUE );
	core::SetParameter( core::PR_FRONT_FRAME_W, uint32(1280) );
	core::SetParameter( core::PR_FRONT_FRAME_H, uint32(1024) );
	core::SetParameter( core::PR_BACK_FRAME_W,  uint32(1280) );
	core::SetParameter( core::PR_BACK_FRAME_H,  uint32(1024) );
	#endif


	if (!core::Init())
	{
		NV_WARNING("Cannot init core");
		core::Exit();
	}
	
	if (!RscManager::Init())
	{
		core::Shut();
		NV_WARNING("Cannot init RscManager");
		core::Exit();
	}
	
	if (!RscManager::OpenBigFile(BF_PATH))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot open BigFile");
		core::Exit();
	}
	
	//Initialize the display manager
	if (!DpyManager::Init(DpyManager::CS_RIGHT_HANDED))
	{
		RscManager::Shut();
		core::Shut();
		NV_WARNING("Cannot init DpyManager");
		core::Exit();
	}
}

void ShutNeova()
{
	AppCtrl::Shut();
	DpyManager::Shut();
	RscManager::Shut();
	core::Shut();
}



void GameLoop()
{
	// Acquire Controllers
	AppCtrl::Acquire();



	// Mesh transfo
	Matrix worldMat;
	Vec3 initLoc(0.0f, 0.0f, -30.0f);
	MatrixBuildTR(&worldMat, &initLoc, NULL, NULL );
	
	Vec2A cursorsPos;		// Position cursor wii
	Vec2A cursorsDefPos;	// State buton

	clock::Time lastTime, now;
	clock::GetTime( &lastTime );
	for (;;)
	{
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
		

		// Get time since last frame
		clock::GetTime(&now);
		float dt = now - lastTime;
		lastTime = now;
		



		// Update Controllers ->	- check if pads are connected again
		//							- update data
		AppCtrl::Update();
		
		cursorsPos.clear();
		cursorsDefPos.clear();
		
		for(uint i=0; i<(uint)AppCtrl::GetNbStoredControllers(); i++)
		{
			AppCtrl::AppInputs* currInput;
			currInput = AppCtrl::GetController(i);
			NV_ASSERT(currInput);
			
			float thrx = - currInput->dir1.x 	* dt;
			float thrz = - currInput->dir1.y 	* dt;
			
			if(currInput->has_ext)
			{
				thrz = (currInput->ext_ldist - currInput->ext_dist) * dt;
				cursorsPos.push_back(currInput->ext_position);
			}
				
			
			Vec3& w_right = *worldMat.right ();
			//Vec3& w_up    = *worldMat.up	();
			Vec3& w_front = *worldMat.front ();				
			Vec3 d = (w_right*thrx)+
					 (w_front*thrz) ;
			
			Vec3Add( worldMat.location(), worldMat.location(), &d );
			
			if( currInput->button1_pressed )
				MatrixBuildTR( &worldMat, &initLoc, NULL, NULL );
			
			// Compute mesh orientation according to the controler state
			float pitch =  currInput->dir2.x 	* dt;
			float yaw   = -currInput->dir2.y 	* dt;

			// Add rotation offset to the mesh worldMat
			if( yaw || pitch )
			{
				Quat q;
				QuatEuler(&q, yaw, pitch, 0);
				QuatNormalize(&q, &q);
				Matrix m;
				MatrixRotation(&m, &q);
				worldMat = m * worldMat;
			}
			
			if( currInput->button1_pressed 	)	cursorsDefPos.push_back( Vec2( 10.0f+(10.0f*0.0f), 10.0f+(10.0f*i) ) );
			if( currInput->button2_pressed 	)	cursorsDefPos.push_back( Vec2( 10.0f+(10.0f*1.0f), 10.0f+(10.0f*i) ) );
			if( currInput->home_pressed 	)	cursorsDefPos.push_back( Vec2( 10.0f+(10.0f*2.0f), 10.0f+(10.0f*i) ) );
			if( currInput->plus_pressed 	)	cursorsDefPos.push_back( Vec2( 10.0f+(10.0f*3.0f), 10.0f+(10.0f*i) ) );
			if( currInput->moins_pressed	)	cursorsDefPos.push_back( Vec2( 10.0f+(10.0f*4.0f), 10.0f+(10.0f*i) ) );
			if( currInput->A_pressed 		)	cursorsDefPos.push_back( Vec2( 10.0f+(10.0f*5.0f), 10.0f+(10.0f*i) ) );
			if( currInput->B_pressed		)	cursorsDefPos.push_back( Vec2( 10.0f+(10.0f*6.0f), 10.0f+(10.0f*i) ) );
		}
				
		
		// Draw
		DpyManager::BeginFrame();
			appvar.Draw(&worldMat);
			
			for(uint i=0; i<cursorsPos.size(); i++)
				appvar.DrawCursor(cursorsPos[i]);				// DrawCursor
			
			for(uint i=0; i<cursorsDefPos.size(); i++)
				appvar.DrawDefCursor(cursorsDefPos[i]);			// Draw State Buton
				
		DpyManager::EndFrame();
		DpyManager::FlushFrame();
	}

	cursorsPos.clear();
	cursorsDefPos.clear();
}





#undef	NVAPP_PSP_MODNAME
#define	NVAPP_PSP_MODNAME	"NvSample_06"

void GameMain()
{
	// Init
	InitNeova();
	
	appvar.Init();
	
	if( !appvar.Load() )
		return;
	

	// GameLoop
	GameLoop();
	
	

	// Shut
	appvar.Shut();
	ShutNeova();
}
