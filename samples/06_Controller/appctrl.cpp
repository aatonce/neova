/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#include "appctrl.h"




vector<AppCtrl::AppInputs> inputA;




void AppCtrl::AppInputs::Init()
{
	button1_pressed = false;
	button2_pressed = false;

	has_ext			= false;
	home_pressed	= false;
	plus_pressed	= false;
	moins_pressed	= false;
	A_pressed		= false;
	B_pressed		= false;
}



void AppCtrl::Acquire()
{
	ctrl::Close();
	ctrl::Open();
	inputA.clear();
	
	Printf( "Waiting for some controllers ...\n" );
	for(uint i=0 ;;++i)
	{
		uint upd_flags = core::Update();
		uint upd_exit  = core::UPD_EXIT_ASKED | core::UPD_EXITED;
		if( upd_flags & upd_exit )
			break;
		
		
		int cptCtrl = CheckAllControllers();
		if( cptCtrl > 0 )
		{
			Printf( "%d Controllers acquired !\n", cptCtrl );
			break;
		}
		
		ShowNoPadPlugged(i,FALSE);
	}
	ShowNoPadPlugged(0,TRUE);
}




int AppCtrl::CheckAllControllers()
{
	inputA.clear();
	
	int cptCtrl = 0;
	for( int i=0; i<int(ctrl::GetNbMax()); i++ )
	{
		ctrl::Status* pStat = ctrl::GetStatus(i);
		// Check the controller type
		if( pStat && (pStat->type==ctrl::TP_PADDLE || pStat->type==ctrl::TP_COMBI))
		{
			AppInputs newInput;
			newInput.Init();
			newInput.ctrl_no = i;
			inputA.push_back(newInput);
			
			cptCtrl++;
		}
	}
	
	return cptCtrl;
}


int AppCtrl::GetNbConnectControllers()
{
	int cptCtrl = 0;
	for( int i=0; i<int(ctrl::GetNbMax()); i++ )
	{
		ctrl::Status* pStat = ctrl::GetStatus(i);
		if( pStat && (pStat->type==ctrl::TP_PADDLE || pStat->type==ctrl::TP_COMBI))
			cptCtrl++;
	}
	return cptCtrl;
}



int AppCtrl::GetNbStoredControllers()
{
	return inputA.size();
}


AppCtrl::AppInputs* AppCtrl::GetController(uint _ind)
{
	if(_ind>=inputA.size())
		return NULL;
	else
		return &inputA[_ind];
}


void AppCtrl::Update()
{
	if(GetNbConnectControllers()!=GetNbStoredControllers())	
	{
		if(!CheckAllControllers())
			Acquire();
	}
	
	bool ctrlDisconnect = false;
	for(uint i=0;i<inputA.size();i++)
	{
		inputA[i].has_ext = false;
	#if defined(_NGC)
		int wiiM_no = ctrl::CtrlNoToChan( inputA[i].ctrl_no );
		if (wiiM_no>=0)
		{
			ctrl::KStatus wiim_status;
			
			// Activate wiiMote and get KStatus
			if(!ctrl::GetKStatus(wiiM_no, &wiim_status, 1))
			{
				ctrlDisconnect = true;
				break;											// Wii mote disconnect
			}
			
			ctrl::EnableDPD(wiiM_no);
			inputA[i].has_ext = true;
					
			inputA[i].button1_pressed	= (wiim_status.hold & ctrl::BUTTON_1);
			inputA[i].button2_pressed	= (wiim_status.hold & ctrl::BUTTON_2);

			inputA[i].dir1 				= Vec2(	wiim_status.pos.x,
												wiim_status.pos.y	);

			// dir2 -> wii multidir
			float dx = 0.0f, dy = 0.0f;
			if		(wiim_status.hold & ctrl::BUTTON_LEFT	)	{ dx = -1.0f; }
			else if	(wiim_status.hold & ctrl::BUTTON_RIGHT	)	{ dx =  1.0f; }
			if		(wiim_status.hold & ctrl::BUTTON_DOWN	)	{ dy = -1.0f; }
			else if	(wiim_status.hold & ctrl::BUTTON_UP		)	{ dy =  1.0f; }
			inputA[i].dir2 = Vec2( dx, dy );	
			
			
			// Ext
			inputA[i].ext_position  	= wiim_status.pos;
			inputA[i].ext_ldist			= inputA[i].ext_dist;
			inputA[i].ext_dist	 		= wiim_status.dist;	
			
			inputA[i].home_pressed		= (wiim_status.hold & ctrl::BUTTON_HOME);
			inputA[i].plus_pressed		= (wiim_status.hold & ctrl::BUTTON_PLUS);
			inputA[i].moins_pressed		= (wiim_status.hold & ctrl::BUTTON_MINUS);
			inputA[i].A_pressed			= (wiim_status.hold & ctrl::BUTTON_A);
			inputA[i].B_pressed			= (wiim_status.hold & ctrl::BUTTON_B);	
		}
	#endif
	
		if(!inputA[i].has_ext)
		{
			ctrl::Status* ctrl_status;
			inputA[i].has_ext = false;
			
			ctrl_status = ctrl::GetStatus( inputA[i].ctrl_no );
			if(!ctrl_status)
			{
				ctrlDisconnect = true;
				break;											// pad disconnect
			}

			
			inputA[i].button1_pressed	= ctrl_status->Filtered( ctrl::BT_BUTTON0 );
			inputA[i].button2_pressed	= ctrl_status->Filtered( ctrl::BT_BUTTON1 );
			
			inputA[i].dir1 = Vec2( 	ctrl_status->Filtered( ctrl::BT_DIR1X ),
									ctrl_status->Filtered( ctrl::BT_DIR1Y )	);
			inputA[i].dir2 = Vec2( 	ctrl_status->Filtered( ctrl::BT_DIR2X ),
									ctrl_status->Filtered( ctrl::BT_DIR2Y )	);
		}
	}
	
	if(ctrlDisconnect)
	{
		inputA.clear();											// Destroy	
		AppCtrl::Acquire();										// Must rebuild inputA				
	}
}



void AppCtrl::Shut()
{
	inputA.free();
}