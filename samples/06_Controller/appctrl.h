/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef _APPCTRL_H_
#define _APPCTRL_H_



#include "appdef.h"


#if defined(_NGC)
	#include <Kernel/NGC/NvkCore_Ctrl[NGC].h>
#endif



namespace AppCtrl
{
	struct AppInputs
	{
		int		ctrl_no;
		
		bool	button1_pressed;
		bool	button2_pressed;
		Vec2	dir1;
		Vec2	dir2;

		// platform specific extensions

		bool	has_ext;		// ext_?? inputs are valid ?
		Vec2	ext_position;	// available only if has_ext is TRUE !
		float	ext_dist;		// available only if has_ext is TRUE !
		float	ext_ldist;		// available only if has_ext is TRUE !
		bool	home_pressed;
		bool	plus_pressed;
		bool	moins_pressed;
		bool	A_pressed;
		bool	B_pressed;
		
		
		void Init();
	};


	void Update();
	void Shut();

	void Acquire();
	int  CheckAllControllers();
	
	AppInputs* GetController(uint _ind);
	
	
	int  GetNbConnectControllers();
	int  GetNbStoredControllers();
}



#endif // _APPCTRL_H_
