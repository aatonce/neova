/**

@page nvstudio_bootfile Quark Boot File

This file is executed by the Quark application at launch. Search order is as follows:
- At the place indicated by the <i>ATONCETECH_QUARK_BOOT</i> environment variable ;
- File named <i>Quark.boot</i> in the Quark application directory ;
- File named <i>Quark.boot</i> in the current directory.

This boot file will initialize some Quark options and set several parameters of the underlying GeoStudio framework.

Here is an example of <i>Quark.boot</i>  file:
@code
//
// This is the Quark boot file.


// Default renderer
std/setDefRenderer "GeoRendererDX";


// Current repository
vdb/setRepositoryPath "d:\\vdb_repository";


// Add copy-compilers for .raw files and for somes HW platforms
vdb/addCopyCompiler "PC"	"raw";
vdb/addCopyCompiler "PS2"	"raw";
vdb/addCopyCompiler "PSP"	"raw";
vdb/addCopyCompiler "PC"	"lua";
vdb/addCopyCompiler "PS2"	"lua";
vdb/addCopyCompiler "PSP"	"lua";
vdb/addCopyCompiler "PC"	"luac";
vdb/addCopyCompiler "PS2"	"luac";
vdb/addCopyCompiler "PSP"	"luac";


// Manually add some type mappings to prevent the systematic
// recompiling of first resources images and sounds
vdb/addTypeMapping "tga"	"NvBitmap";
vdb/addTypeMapping "tif"	"NvBitmap";
vdb/addTypeMapping "tiff"	"NvBitmap";
vdb/addTypeMapping "png"	"NvBitmap";
vdb/addTypeMapping "psd"	"NvBitmap";
vdb/addTypeMapping "jpg"	"NvBitmap";
vdb/addTypeMapping "jpeg"	"NvBitmap";
vdb/addTypeMapping "bmp"	"NvBitmap";
vdb/addTypeMapping "wav"	"NvSound";
vdb/addTypeMapping "vag"	"NvSound";
vdb/addTypeMapping "mp3"	"NvSound";
vdb/addTypeMapping "mod"	"NvSound";
vdb/addTypeMapping "it"		"NvSound";
vdb/addTypeMapping "xm"		"NvSound";
vdb/addTypeMapping "s3m"	"NvSound";


// Setup NAFS service
nafs/start;
nafs/share "public" "d:\\nafs\\public";
nafs/share "vdb"    "d:\\nafs\\vdb" "dummypass";


// Set Quark options
quark/setAlign     "right";
quark/setSize      80;
quark/setFontSize  12;
quark/setParameter "PC"	 "viewer_addr" "127.0.0.1";
quark/setParameter "PSP" "viewer_addr" "192.168.1.202";
quark/setParameter "PS2" "viewer_addr" "192.168.0.152";
quark/setParameter "config_file" "../Quark.cfg";
quark/setParameter "start_version" "PSP";
quark/setParameter "start_config"  "Test1";

@endcode

Commands specific to Quark are:
- <i>quark/setAlign</i>: sets the alignment and position of the 3D cubes in the interface.
  Possible values are <i>"right"</i>, <i>"left"</i>, <i>"top"</i> and <i>"bottom"</i>.
- <i>quark/setSize</i>: sets the height/width in pixels of the 3D cubes in the interface.
- <i>quark/setFontSize</i>: size in points of the font used for displaying titles and other texts in the Quark interface.
- <i>quark/setParameter</i> : defines the value of a parameter. This command's format is:
	@code
	quark/setParameter [version-name] parameter-name parameter-value;
	@endcode
	The version-name attribute is optional. It is only used to qualify the HW version to which the parameter must be associated.
	The <i>viewer_addr</i> parameter set the netwokr address of the distant viewer for a given HW version.
	The <i>config_file</i> parameter specifies a file containing the compilation configurations to be loaded.
	The <i>start_version</i> parameter sets the initial HW version.
	The <i>start_config</i> parameter sets the name of the initial compilation configuration.

Any command from the GeoStudio framework can be added to this boot file.

*/


