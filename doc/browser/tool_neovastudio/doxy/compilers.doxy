/**

@page nvstudio_compilers Resources cross-compilation manager

@image html cube_compilers.gif "Fig 1. Resources cross-compilation manager."



@section nvstudio_compilers_sec1 Resource compilation 
This manager (fig. 1) will cross-compile resources for Neova CoreEngine.
An NvMesh resources, for example, represents an generic and multiplatform description of a 3D mesh.
This ASCII resource, in VIFF format, can be edited and directly previewed by the various Neova Studio production help tools.
However, this resource format is not instantly exploitable by an HW version of the Neova CoreEngine runtime.
For that, the resource must first be <i>compiled</i> for a target platform.@n
The compilation step will adapt and optimize the resource's data for a given target platform. During this transformation,
additional data can also be generated, such as collision information for example. A compiled resource has a compressed binary format
that cannot be edited or reverse engineered.@n
The compilation porcess of a resource is however highly customizable through the use of parameters. These compilation parameters are included in the original resource 
itself, in a VIFF vector that is specific to each resource type.
@sa @ref nvstudio_viffexplorer



@section nvstudio_compilers_sec2 Resource compilation cache
The resource compilation manager keeps an independant cache of compiled resources for each target platform.
For a given HW version (X), this cache is in a folder named <i>vdb_X</i>, located in the same parent directory as the current repository.
Using this cache will allow the compilation manager to reduce compilation times of a resource group in the case where few 
original resources have been modified since the last compilation of the group.@n
The cache folder of the current version can be explored by left double clicking on the cube.



@section nvstudio_compilers_sec3 Mouse interactions
When the mouse is located on the compilation manager, it rotates in 3D and gets the contectual focus of Neova Studio. 
Possible interactions are illustrated on the following figure (fig. 2) :
@n@n
@image html mouse_compilers.gif "Fig 2. Mouse interactions."



@section nvstudio_compilers_sec4 Contextual menus
A Contextual menu opens with a right click on the rotating cube.

<table class="menuDesc" align="center"><tr><td class="menuDescLeft">
Select version
</td><td class="menuDescRight">
Selects another current version of the comilation target HW.

</td></tr><tr><td class="menuDescLeft">
Setup version
</td><td class="menuDescRight">
Opens a submenu for current version parameters.
<table border="0"><tr><td class="menuDescLeft">
Clear cache
</td><td class="menuDescRight">
Complete suppresion of the compilation cache for the current HW version.
This operation will erase the full cache folder recursively. All resource compilation previously made for the current version will
be lost and have to be done again.
</td></tr><tr><td class="menuDescLeft">
Build cache BIGFILE
</td><td class="menuDescRight">
Construit un BIGFILE � partir de toutes les ressources du cache de compilation pour la version HW courante.
</td></tr><tr><td class="menuDescLeft">
Share cache BIGFILE
</td><td class="menuDescRight">
Shares on the network the BIGFILE from the compilation for the current HW version.
The network sharing is done through the internal NAFS file server of GeoStudio.
The NAFS file server must be activated.
@sa @ref nvstudio_repository_sec3
</td></tr><tr><td class="menuDescLeft">
View cache BIGFILE
</td><td class="menuDescRight">
Shows the BIGFILE from the current HW version compilation cache. This BIGFILE is built by the <i>Build cache BIGFILE</i> entry in this same submenu.
Visualisation is launched on the remote viewer defined for the current HW version.
A remote viewer can be defined for every HW version supported by the compilation manager.
To allow acces to the BIGFILE by network, the folder containing the BIGFILE is shared
with automatic management by the internal NAFS file server of GeoStudio.
The NAFS file server must be activated.                   
@sa @ref nvstudio_repository_sec3
</td></tr><tr><td class="menuDescLeft">
Explore cache
</td><td class="menuDescRight">
Opens a Windows file explorer on the compilation cache folder for the current HW version.
</td></tr></table>

</td></tr><tr><td class="menuDescLeft">
Select configuration
</td><td class="menuDescRight">
Selects a nex configuration for the current compilation.
@sa @ref nvstudio_cfgfile

</td></tr><tr><td class="menuDescLeft">
About configuration
</td><td class="menuDescRight">
Shows information about the current compilation configuration.

</td></tr><tr><td class="menuDescLeft">
Setup configurations
</td><td class="menuDescRight">
Opens a submenu for setting configuration parameters.
<table border="0"><tr><td class="menuDescLeft">
Edit configurations
</td><td class="menuDescRight">
Opens in editing mode the current compilation configuration definition file.
Any modification will only be applied after this file has been reloaded.
</td></tr><tr><td class="menuDescLeft">
Reload configurations
</td><td class="menuDescRight">
(Re)loads the current compilation configuration definition file.
</td></tr><tr><td class="menuDescLeft">
Open configurations
</td><td class="menuDescRight">
Opens a file selector to choose a new compilation configuration definition file.
</td></tr></table>
@sa @ref nvstudio_cfgfile

</td></tr><tr><td class="menuDescLeft">
Update BIGFILE
</td><td class="menuDescRight">
Updatess the current target BIGFILE, as stated by the current compilation configuration.
The compilation cache for the current HW version is first updated.
Resources that are not compiled or that have been modified since their previous compilation will be (re)compiled.
Updating the BIGFILE will incremently add (re)compiled rzsources. This update is faster that a full 
rebuild of the BIGFILE but will increase its KB size.

</td></tr><tr><td class="menuDescLeft">
(Re)Build BIGFILE
</td><td class="menuDescRight">
Full rebuild of the current target BIGFILE, as stated in the current compilation configuration.
The compilation cache fot the current HW version is first updated.

</td></tr><tr><td class="menuDescLeft">
Reduce BIGFILE
</td><td class="menuDescRight">
Reduces the size of the current target BIGFILE, as stated in the current compilation configuration.
This reduction will delete unreferenced data from the BIGFILE, such as those that result of the incremental update operations.

</td></tr><tr><td class="menuDescLeft">
Share BIGFILE
</td><td class="menuDescRight">
Shares on the network the resources BIGFILE associated to the current compilation configuration.
This network sharing is handled directly by the internal NAFS file system of the GeoStudio framework.
The NAFS file server must be activated.
@sa @ref nvstudio_repository_sec3

</td></tr><tr><td class="menuDescLeft">
View BIGFILE
</td><td class="menuDescRight">
Shows the current target BIGFILE resources ont the remote viewer defined for the current compilation configuration.
A remote viewer can be defined for every HW version supported by the compilation manager.
To allow acces to the BIGFILE by network, the folder containing the BIGFILE is shared         
with automatic management by the internal NAFS file server of GeoStudio.                      
The NAFS file server must be activated.                                                       
@sa @ref nvstudio_repository_sec3

</td></tr><tr><td class="menuDescLeft">
Explore BIGFILE
</td><td class="menuDescRight">
Opens a Windows file explorer on the folder containing the current BIGFILE.

</td></tr><tr><td class="menuDescLeft">
View NAFS
</td><td class="menuDescRight">
Shows the data of a NAFS network share on the remote viewer defined for the current HW version.
@sa @ref nvstudio_repository_sec3

</td></tr><tr><td class="menuDescLeft">
Viewer
</td><td class="menuDescRight">
Opens a submenu for setiing the parameters of the viewer associated to the current HW version.
<table border="0"><tr><td class="menuDescLeft">
Connect/Disconnect
</td><td class="menuDescRight">
Connects to/Disconnects from the remote viewer.
</td></tr><tr><td class="menuDescLeft">
Reset
</td><td class="menuDescRight">
Resets the remote viewer.
</td></tr><tr><td class="menuDescLeft">
Edit settings
</td><td class="menuDescRight">
Opne the interface for setting the parameters of the remote viewer. This interface allows the user to set the address and network port 
of the remote viewer.
It also has an input zone for commands that will be extecuted by the remote viewer.
Finally, this interface allows the user to remotely see the real graphical display for the platform of the remote viewer.
</td></tr></table>

</td></tr></table>

*/


