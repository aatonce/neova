/**

@mainpage Neova Studio Application

<table border="0"><tr>
	<td class="pageRef" width="100px">Package</td>
	<td class="pageRef">Neova Studio</td>
</tr><tr>
	<td class="pageRef">Requires</td>
	<td class="pageRef">GeoStudio Framework</td>
</tr><tr>
	<td class="pageRef" width="100px">platform</td>
	<td class="pageRef">Microsoft Windows 2000/XP/Vista</td>
</tr></table>


@section nvstudio_sec1 Presentation

The Neova Studio applcation is a data manager for Neova CoreEngine.
Neova Studio allows the generation and edition of Neova resources as well as their compilation for a a target platform.
Its interface is simple to use and can be directly integrated on the desktop of the workstation.
Most actions are made through operations such as "drag & drop" from the Windows file explorer
or directly through a right of left click on interface elements.@n
Neova Studio is not a complete data production solution but rather an additional independent toolset with 
several services to help validate and prototype data for Neova CoreEngine.
@n@n@n@n
@image html geo_scn2.png "Fig 1. Neova Studio functional diagram."



@section nvstudio_sec2 Installation

Neova Studio is in fact a GUI showing the various features of the GeoStudio framework
for use in Neova CoreEngine. The application requires the installation of the GeoStudio framework.
Parameters are set in Neova Studio in two main files:
- A Quark.boot boot file, for parameters regarding some Quark and its underlying GeoStudio framework options;
- A .cfg configuration file, to define Neova and other resources compilation configuration.

See the rest of this text for more information on these files' syntax.


@section nvstudio_sec3 Graphical User Interface for Neova Studio

When launching Neova Studio, several cubes are displayed on the desktop(fig. 3).
They represent the main entry points of the application.
Another control point is located in the notification zone of the Windows taskbar (fig. 2).
It gives more advanced controls on the application itself.

<table border="0" width="100%"><tr><td width="50%">
@image html icontray.gif "Fig 2. Neova Studio notification icon."
</td><td width="50%">
@image html cubes.gif "Fig 3. Neova Studio's integrated GUI."
</td></tr></table>

- @ref nvstudio_icontray : Notification icon.
- @ref nvstudio_repository : Resource manager.
- @ref nvstudio_viffexplorer : VIFF format resource explorer.
- @ref nvstudio_spaceviewer : 3D files and resources viewer.
- @ref nvstudio_compilers : BIGFILE genration and cross-compilation manager.
- @ref nvstudio_shell : Commande terminal of the GeoStudio framework.
- @ref nvstudio_bootfile : Quark boot file.
- @ref nvstudio_cfgfile : Resource compilation configuration definition file.


@section nvstudio_sec4 Neova Studio use case

In this example, we will build a BIGFILE with graphical resources from a 3DSMax scene.
This is a typical Neova Studio use case, which is made of the 5 following steps:

@subsection nvstudio_sec4_1 1- 3DSMax data export
The scene is created in the 3DSMax DCC editor. Through the use of the export plugin that comes with the Neova Studio package, the full scene
is saved in a source file of the G3D type. By using tyhe default values of the export filters, the G3D files contains all the data
of the original scene. It can be seen as an equivalent of the .max file, with the added interest of having an open format that can be directly used 
by a third party application like Neova Studio.
@sa <a href="../browser_tool_DCC/index.htm">Plugins for DCC applications</a>

@subsection nvstudio_sec4_2 2- View the exported G3D scene
A G3D file contains all the information of a 3D scene. This file format is directly handled by the 3D viewer of Neova Studio.
To validate the conformity of the G3D file exported data with the original scene, the G3D fiel can be dragged on the cube 
corresponding to the @ref nvstudio_spaceviewer of Neova Sutdio. The <i>SpaceViewer</i> has several types of inetractors allowing the user
to visually explore the 3D scene with a replay of the various animations.
@sa @ref nvstudio_spaceviewer

@subsection nvstudio_sec4_3 3- Extracting data for Neova CoreEngine
In this important step, we extract from the G3D source file a set of resources for Neova CoreEngine. The G3D file is dragged on the cube
corresponding to the @ref nvstudio_repository. The import interface then opens on the screen (@ref nvstudio_repository_sec4) and presents the list
of Neova and Nexus data that can be extracted. The extraction consists in importing these base resource files in the current 
repository folder. These resources can be saved anywhere in the current repository folder but cannot be save outside this
root import directory.@n
The import interface allows the selection of a subset of the resources and the renaming or substitution of the resources.
The resources imported in the repository represent the <i>runtime</i> database of the application. However, these resources
have a generic format independant from the target platform and are completely editable.@n
The Neova graphical resources of the NvMesh or NvBitmap types can be directly previewed by the <i>SpaceViewer</i> by dragging them on the 
corresponding cube.
@sa @ref nvstudio_repository @n
	@ref nvstudio_spaceviewer

@subsection nvstudio_sec4_4 4- BIGFILE Generation
The imported Neova resources can not be directly used by the Neova CoreEngine runtime. They must be compiled for a given target platform.
This compilation step is made by the @ref nvstudio_compilers. It is a cross-compilation of the resources that allows the adaptation and
optimisation of the generic data into dedicated binary formats for a target platforme. These are directly supported very efficiently
bay the Neova CoreEngine runtime version for this hardware platform.@n
A BIGFILE is an optimised archive of the compiled resources for a given paltform. It is defined by a compilation configuration 
simply describing what resources make up the BIGFILE, independently from the target platform. A compilation configuration thus defines
the contents of a BIGFILE and a given configuration can be used to generate version of the same BIGFILE for several tagert platforms. 
It is also possible to differenciate the various HW version of a BIGFILE by different <i>basenames</i>.@n
The compilation manager can manage several configurations. To select the current configuration, use the mouse wheel when the mouse 
is over the compilation manager cube.
@sa @ref nvstudio_compilers @n
	@ref nvstudio_cfgfile

@subsection nvstudio_sec4_5 5- BIGFILE visualisation
Neova Studio can give a quick preview of a BIGFILE's resources.
A native viewer is available for each HW platform supported by Neova CoreEngine.
The native viewer must first have been lauchend on the remote target platform.
The network parameters to access the viewer must then be defined in the compilation manager for this platform.
These network paramaters can be manually input by the user (@ref nvstudio_compilers_sec4) or through a Quark boot file (@ref nvstudio_bootfile).@n
The <i>View BIGFILE</i> item of the contextual menu of the compilation manager launches the preview command for the current BIGFILE.
@sa @ref nvstudio_compilers @n
	@ref nvstudio_bootfile

*/


