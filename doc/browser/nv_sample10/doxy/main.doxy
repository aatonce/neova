/**

@mainpage (nv10) Procedural texture

<table border="0"><tr>
	<td class="pageRef" width="100px">platform</td>
	<td class="pageRef">all</td>
</tr><tr>
	<td class="pageRef" width="100px">sample</td>
	<td class="pageRef"><a href="files.htm">source code</a></td>
</tr></table>

The NvImage class can create procedural images with or without palettes.
@n
An NvImage gives access to an associated NvBitmap resource, which allows the use of procedural images as textures.
<br>
<br>
Initialisation or update of data is a two steps process.
One must first create an access on the rectangular region that will be modified by calling the 
NvImage::CreateAccess(Region &inRegion) function. This access stays valid until a destruction request. 
Then, obtaining a pointer corresponding to the access gives access to the data. 
it is possible to get a pointer on the pixels as well as on the color palette. Modifications are 
effective after the access has been validated thanks to the NvImage::ValidateClutAccess() function 
for palette data change and NvImage::ValidatePixelAccess(Region &inSubRegion) for a portion of the image.  

 Palette validation affects the whole palette of the image. Pixel validation 
 affects only the portion of image contained in the intersection of the region of the access
 and the validated region.




@image html snap.png


*/

