Plugins for Discreet 3DSMax 4 to 8 and Softimage|XSI 5 will export all or some subpart of an 
editing scene data set. Ascii G3D format is used. These plugins are available in the
NeovaStudio package.