
import sys, os, string


def find_files( topPath ) :
	fl = []
	for root, dirs, files in os.walk( topPath ) :
		for file in files :
			f = os.path.join( root, file )
			if str(f).endswith( '.doxy' ) :
				fl.append( f )
	return fl;


def line_filter( pattern, lines ) :
	out_lines = []
	for l in lines :
		if l.find(pattern) < 0 :
			out_lines.append( l )
	return out_lines


def word_filter( pattern, lines ) :
	out_lines = []
	for l in lines :
		words = l.split()
		lwords = []
		for w in words :
			if w.find(pattern) < 0 :
				lwords.append( w )
		if len(lwords) > 0 :
			wl = string.join(lwords)
			out_lines.append( wl+'\n' )
	return out_lines


def seq_filter( pattern0, pattern1, lines ) :
	out_lines = []
	inseq = False
	for l in lines :
		words = l.split()
		lwords = []

		for w in words :
			seq0 = w.find( pattern0 )
			seq1 = w.find( pattern1 )
			if not inseq and seq0>=0 :
				if seq0 > 0 :
					lwords.append( w )
				if seq1 < seq0 :
					inseq = True
			elif inseq and seq1>=0 :
				if seq1 > 0 :
					lwords.append( w )
				if seq0 < seq1 :
					inseq = False
			elif not inseq :
				lwords.append( w )

		if len(lwords) > 0 :
			wl = string.join(lwords)
			out_lines.append( wl+'\n' )
	return out_lines


def word_count( lines ) :
	wc = 0
	for l in lines :
		wc += len( l.split() )
	return wc



src_files  = []
src_files += find_files( 'headers' )
src_files += find_files( 'pages' )

print len(src_files), 'files.'



# collapse all
src_lines = []
for f in src_files :
	slines = file(f,'r').readlines()
	for l in slines :
		src_lines.append( l )

print len(src_lines), 'raw lines.'



# filtering

file('before_wc_filtering.doxy','w').writelines( src_lines )
print "Before: ", word_count(src_lines), "words."

src_lines = line_filter( '@file', src_lines )
src_lines = line_filter( '@namespace', src_lines )
src_lines = line_filter( '@struc', src_lines )
src_lines = line_filter( '@enum', src_lines )
src_lines = line_filter( '@sa', src_lines )
src_lines = line_filter( '@var', src_lines )
src_lines = line_filter( '@def', src_lines )
src_lines = line_filter( '@copydoc', src_lines )
src_lines = line_filter( '@typedef', src_lines )
src_lines = line_filter( '@union', src_lines )
print "Line filtering: ", word_count(src_lines), "words."

src_lines = seq_filter( '@fn', ')', src_lines )
src_lines = seq_filter( '@code', '@endcode', src_lines )

src_lines = seq_filter( '<td',		'>', src_lines )
src_lines = seq_filter( '<tr',		'>', src_lines )
src_lines = seq_filter( '<li',		'>', src_lines )
src_lines = seq_filter( '<table',	'>', src_lines )
src_lines = seq_filter( '<ul',		'>', src_lines )
src_lines = seq_filter( '<table',	'>', src_lines )
src_lines = seq_filter( '<a',		'>', src_lines )
print "Sequences filtering: ", word_count(src_lines), "words."

src_lines = word_filter( '/**', src_lines )
src_lines = word_filter( '*/', src_lines )
src_lines = word_filter( '@brief', src_lines )
src_lines = word_filter( '@param', src_lines )
src_lines = word_filter( '@return', src_lines )
src_lines = word_filter( '@retval', src_lines )
src_lines = word_filter( '@section', src_lines )
src_lines = word_filter( '@page', src_lines )
src_lines = word_filter( '@attention', src_lines )
src_lines = word_filter( '@li', src_lines )
src_lines = word_filter( '@n', src_lines )
src_lines = word_filter( '@warning', src_lines )
src_lines = word_filter( '@ref', src_lines )
src_lines = word_filter( '@remarks', src_lines )
src_lines = word_filter( '@remark', src_lines )
src_lines = word_filter( '@section', src_lines )
src_lines = word_filter( '@subsection', src_lines )
src_lines = word_filter( '@subsubsection', src_lines )
src_lines = word_filter( '@mainpage', src_lines )
src_lines = word_filter( '@anchor', src_lines )
src_lines = word_filter( '\image', src_lines )
src_lines = word_filter( '@image', src_lines )
src_lines = word_filter( 'href=', src_lines )
src_lines = word_filter( '<div', src_lines )
src_lines = word_filter( '<br>', src_lines )
src_lines = word_filter( '<b>', src_lines )
src_lines = word_filter( '<i>', src_lines )
src_lines = word_filter( '<ol>', src_lines )

src_lines = word_filter( 'Nv', src_lines )
src_lines = word_filter( '::', src_lines )
src_lines = word_filter( 'pvoid', src_lines )
src_lines = word_filter( 'uint', src_lines )
src_lines = word_filter( 'uint32', src_lines )

src_lines = word_filter( '<code', src_lines )

src_lines = word_filter( '/', src_lines )
print "Word filtering: ", word_count(src_lines), "words."


print "After: ", word_count(src_lines), "words."
file('after_wc_filtering.doxy','w').writelines( src_lines )


