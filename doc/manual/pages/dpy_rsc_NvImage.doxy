//DONE
/**

@page dpy_rsc_NvImage The NvImage graphical resource

<table border="0"><tr>
	<td class="pageRef" width="100px">interface</td>
	<td class="pageRef">NvImage</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvImage.h</td>
</tr><tr>
	<td class="pageRef">platform</td>
	<td class="pageRef">any</td>
</tr></table>

This class represents 3-channel (RGB) or 4-channel (RGBA) images. Images are mostly used as sources for geometry <i>texturing</i>,
via the DpySource structure.

@section dpy_rsc_NvImage_sec1 Availability
Like NvBitmap resources, NvImage resources represent textures for geometry mapping.
However, a NvBitmap resource is static and precompiled, and requires prefetching.
On the contrary, a NvImage image is immediately available and the user can create it on the fly.

@section dpy_rsc_NvImage_sec2 Using images
Images are created by the NvImage::Create() function.
The W and H dimensions of the image are given by the first two arguments passed to the function.
The third parameter sets the image format, true-color 32-bit or colormapped (using a CLUT).
The last parameter is used for a validation or error return code on the operation.
The range of these parameters depends on the platform and may change between two systems.
In case of image creation failure, please refer to the error return parameter to determine the reason,
as well as to Neova's documentation on the platform.
\remarks Images do not support mipmapping!

@section dpy_rsc_NvImage_sec3 Accessing the texels of an image
Images do not define texels by default, they must be initizalized by the programmer.
Accessing the data of images is done through steps described in the following subsections, illustrated by figure 1.

@subsection dpy_rsc_NvImage_sec3_1 Creating the access buffer
Before accessing the data in any way, the user must explicitly create the texel acces buffer.
This query is done via the NvImage::CreateAccess() function.
In order to reduce memory load, this buffer is associated to a region of the image.
It should be noted that depending on the platform, creating this buffer may require a small
but significant amount of CPU time.
An access buffer remains valid until it is explicitly released.
Repetitive creation/release queries on a given access buffer should thus be considered with extra care in each frame.

@subsection dpy_rsc_NvImage_sec3_2 R/W data access
Accessing the data is done via the NvImage::GetPixelAccess() and
NvImage::GetClutAccess() methods.
These methods return a R/W pointer respectively on the pixels and colors of the CLUT, as well as various format and alignment data.
The pointer is NULL if the access buffer has not previously been created by the NvImage::CreateAccess() function.
The returned address can be used by the application for random R/W access to the data.<br>
However, the following points must be taken into acount:
- The access buffer remains valid through several display frames.
	However, an access pointer may not remain valid from a frame to the other.
	This depends on platform-specific implementation possibilities.
	The current value of an access pointer should then be systematically requested before each read or write operation.
- The pointer gives direct access to a zone of the access buffer. Initializing the buffer must be done by the user.
	As a consequence, reading data in an uninitialized zone returns undetermined data.
- The data format varies depending on the platform. The NvImage::GetPixelAccess() and NvImage::GetClutAccess() functions
	return the storing format of the component via the <code>outPsm</code> Psm out parameter.
	This is the native format of the platform, that corresponds to the fastest use.
	Format conversions must be handled by the user, possibly via the GetPSM() and ConvertPSM() functions.
- Access buffer modifications (i.e. writes) must be signaled by calling the 
	NvImage::ValidatePixelAccess() and NvImage::ValidateClutAccess() methods.

@subsection dpy_rsc_NvImage_sec3_3 Releasing the access buffer
The NvImage::ReleaseAccess() method releases the access buffer associated to the image region data.

\image html "nova_dpyimageaccess.png" "Fig. 1: Accessing image data."

@section dpy_rsc_NvImage_sec4 Example of use
The following functions builds a chess-board image and returns a NvBitmap interface:

@code
NvBitmap* CreateChessImage()
{
	bool usingCLUT = TRUE;

	// 32x32 image
	NvImage::CrStatus crstatus;
	NvImage* imgP = NvImage::Create( 32, 32, usingCLUT, &crstatus );

	// failed ?
	if( !imgP ) {
		if( usingCLUT && crstatus == NvImage::CS_CLUT_UNSUPPORTED ) {
			// Try with no CLUT
			usingCLUT = FALSE;
			imgP = NvImage::Create( 32, 32, usingCLUT );
		}
		if( !imgP )
			return NULL;
	}

	// Creating access buffer for the whole image
	NvImage::Region reg;
	reg.x = 0;	reg.y = 0;
	reg.w = 32;	reg.h = 32;
	if( !imgP->CreateAccess(reg) ) {
		SafeRelease( imgP );
		return NULL;
	}

	// Writing chess-board
	{
		// Getting texel access
		uint lstride;
		Psm  psm;
		uint32* bits = (uint32*) imgP->GetPixelAccess( lstride, psm );
		if( !bits ) {
			SafeRelease( imgP );
			return NULL;
		}
		for( uint tx = 0 ; tx < 32 ; tx++ ) {
			for( uint ty = 0 ; ty < 32 ; ty++ ) {
				if( usingCLUT ) {
					uint8 col = ((tx>>3)+(ty>>3))&1 ? 0U : 1U;
					((uint8*)bits)[tx+ty*32] = col;
				} else {
					uint32 col = ((tx>>3)+(ty>>3))&1 ? 0U : ~0U;
					bits[tx+ty*32] = col;
				}
			}
		}
		// Validating all texels
		imgP->ValidatePixelAccess( reg );
	}

	if( usingCLUT ) {
		// Initializing color 0 & 1
		Psm psm;
		uint32* colP = imgP->GetClutAccess( psm );
		colP[0] = GetPSM( psm, 0, 0, 0, 0 );
		colP[1] = GetPSM( psm, 255, 255, 255, 255 );
		imgP->ValidateClutAccess();
	}

	// Releasing access
	imgP->ReleaseAccess();
	return imgP->GetBitmap();
}
@endcode



*/


