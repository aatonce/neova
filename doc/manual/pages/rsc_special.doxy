//DONE
/**

@page rsc_special Utility resources

Neova's resource management is based on using a @ref rsc_data "BIGFILE" and on @ref rsc_streaming "prefetching" mechanisms.
This management is simple and efficient, but may lack flexibility during prototyping or for simple user data input.
In order to be more flexible, Neova offers two utility resources classes:
NvUID and NvRawData.

@section rsc_special_sec1 The NvUID utility class

<table border="0"><tr>
	<td class="pageRef" width="100px">interface</td>
	<td class="pageRef">NvUID</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvUID.h</td>
</tr></table>

In common file systems, a file is located through a string access path (e.g. "c:\data\world.map").
This kind of access is made via the particular NvUID resource.
This resource is unique inside a given BIGFILE. It contains the UIDs of all resources in the BIGFILE, associated to their respective access paths as they are known when compiling and creating the BIGFILE.
The resource provides two methods:
- NvUID::GetRscInfo() returns the edition access path for the resource indexed by i in the BIGFILE, associated to its UID;
- NvUID::FindUIDByName() searches for the UIDs that match a given access path pattern.

It should be noted that this resource requires substantial memory use.

@section rsc_special_sec2 The NvRawData utility class

<table border="0"><tr>
	<td class="pageRef" width="100px">interface</td>
	<td class="pageRef">NvRawData</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvRawData.h</td>
</tr></table>

Neova's streaming system can be extended very simply to any user resources by using this resource class.
User data may be either binary or character data.
By integrating these types of data inside Neova management, their management can be done by instance. Their prefetching can also be performed asynchronously and be multiplexed with other resources. Ultimately, this allows to obtain a single, homogeneous runtime database, formed of a single BIGFILE.
The NvRawData::Register() static functions add a new resource manager (@ref rsc_factory "Factory"), for a given type or file extension.
User data of the given type will be supported by this manager.
The user resource is then handled like a normal resource via a NvRawData object.
The following code illustrates this use:

@code
uint32 userDataType = nv::crc::Get("NvRawData");

// Finding user rsc by type in the bigfile
uint32 uidRsc = RscManager::FindRscByType( userDataType );
NV_ASSERT( uidRsc );

// Registering rawData factory
NvRawData::Register( userDataType );

// Loading it
MyPrefetch( uidRsc );

// Creating it
NvRawData* uidRscP = NvRawData::Create( uidRsc );
NV_ASSERT( uidRscP );

pvoid userDataPtr   = uidRscP->GetDataPtr();
uint  userDataBSize = uidRscP->GetDataBSize();

SafeRelease( uidRscP );
@endcode
*/


