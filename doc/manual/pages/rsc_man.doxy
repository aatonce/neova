//DONE
/**

@page rsc_man The RscManager resource manager

<table border="0"><tr>
	<td class="pageRef" width="100px">namespace</td>
	<td class="pageRef">RscManager</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvRscManager.h</td>
</tr></table>

The resource manager handles BIGFILE management and resource prefetching.
The declarations and functions of the manager are grouped into the RscManager C++ namespace.

@section rsc_man_sec1 Init/Update/Shut
As any main Neova component, the resource manager has initialization, update and release functions.
The RscManager::Init() function must be called before using the manager. The RscManager::Update() function
passes the lead to the manager for a maximal given time. This function must be called regularly in order
to ensure prefetching progress.
Lastly, the RscManager::Shut() shuts down the manager.

@section rsc_man_sec2 BIGFILE management
The RscManager handles BIGFILE management. Only a single BIGFILE can be used at a time.
The manager is limited to a single current BIGFILE because it uses the nv::file Core service,
which only allows to access a single file at a time.
It should be noted that when the RscManager resource manager is open on a BIGFILE, the nv::file service is also
open on the BIGFILE data file. The application may then send its own read queries, that will be multiplexed with these of the manager.
However, it must not directly close the nv::file service.

The following code is the skeleton of an application that uses a BIGFILE:

@code
#include <Nova.h>
#include <RscManager.h>
using namespace nv;

void GameMain ( )
{
    if( !core::Init() )
        return;
    if( !RscManager::Init() )
        return;

    bool res;
    res = RscManager::OpenBigFile( "WORLD01" );
    NV_ASSERT( res );

    for( ;; ) {
        // main loop ...

        // Updating the Core
        bool cnt = core::Update();
        if( !cnt )  break;

        // Updating the manager
        RscManager::Update( 0.5f );
    }

    RscManager::CloseBigFile();

    RscManager::Shut();
    core::Shut();
}
@endcode

The manager also offers various functions to get information on the opened BIGFILE and its resources,
such as the RscManager::GetRscCpt() functions that returns the number of these resources,
RscManager::GetRscUID() that returns the UID of the resource indexed by i,
RscManager::GetRscInfo() that returns information on a UID-identified resource and
RscManager::FindRscByType() that allows to find all resources of a given TYPE in the BIGFILE.

@section _rsc_man_sec3 Using prefetching
Prefetching is a resource preloading process. By predicting the application resource needs, this mechanism
allows to suppress the important lag phase induced by store device access.
The prefetching steps are the following:
<ol>
<li>Stacking the UIDs of the resources to be prefetched in a RscPrefetchQ structure;
<li>Adding the RscPrefetchQ list to the manager;
<li>Prefetch completion is given by the status of the RscPrefetchQ list.
	An active wait can be performed by polling on this status,
	with Core and manager update in each loop;
</ol>

The following code illustrates the use of prefetching:

@code
#include <Nova.h>
#include <RscManager.h>
using namespace nv;

void GameMain ( )
{
    if( !core::Init() )
        return;
    if( !RscManager::Init() )
        return;

    bool res;
    res = RscManager::OpenBigFile( "WORLD01" );
    NV_ASSERT( res );

    // Looking for the ROOT resource
    uint32 rootUID = RscManager::FindRscByType( 0x658056A9 );
    NV_ASSERT( rootUID != INVALID_RSC_UID );

    // Prefetch list
    RscPrefetchQ prefQ;
    prefQ.AddRsc( rootUID );

    // Adding the list to the manager
    RscManager::AddPrefetchQ( &prefQ );

    // Active wait
    while( !prefQ.IsReady() ) {
        core::Update();
        RscManager::Update( 1.0f );
        
        // Progress output
        RscPrefetchQP curPrefQ;
        float         progress;
        curPrefQ = GetProgress( &progress );
        if( curPrefQ != &prefQ )
            progress = 0.0f;
        // setup progressbar
    }

    // Get a generic interface to the ROOT rsc
    NvResourceP rootP;
    rootP = RscManager::CreateInstance( rootUID );
    NV_ASSERT( rootP );

    RscManager::CloseBigFile();
    RscManager::Shut();
    core::Shut();
}
@endcode

@section _rsc_man_sec4 The various states of a prefetch query
The RscPrefetchQ structure represents a prefetch query for a resource set, identified by a UID list.
The status of a query is given by the RscPrefetchQ::GetState() method.
Its range of value is (fig. 1):

\image html "nova_rscqop.png" "Fig. 1: Various states of a RscPrefetchQ prefetch query."

-	<b>(not initialised)</b>:<br>
	Like many other Nove classes, the <code>RscPrefetchQ()</code> structure
	does not have a constructor. The creation state of an object is undefined.
-	<b>PENDING</b>:<br>
	Prefetching queries are handled by the manager through the <code>AddPrefetchQ()</code> function.
	They are inserted into an internal FIFO and are processed sequentially.
-	<b>ABORTED</b>:<br>
	A <b>PENDING</b> prefetching query may be cancelled by the
	RscManager::AbortPrefetchQ() manager function.
	The internal UID list is not affected by this operation.
	The treatment of a list by the manager is an atomic operation.
	If the prefetch list to be deleted is already being processed or has already been,
	it cannot be deleted anymore and the RscManager::AbortPrefetchQ() function returns an error.
-	<b>COMPLETED</b>:<br>
	The query has been successfully performed and prefetching is done. Requested resources are immediately available.
	After prefetching, the UID list of the RscPrefetchQ() structure contains the UIDs of the resources
	that have <b>actually</b> been prefetched by the manager, after dependency resolving.
	The result list then adds the UIDs corresponding to the recursive dependencies to the initial list.
	However, it does not contain the resources that may already have been available at prefetching time.<br>
	<br>
	Let us take for example the BIGFILE of figure 2 and the UID set {10,15} of currently available resources.
	Prefetching the initial list {4,15} will result, after dependency resolving, into the list {4,15,17,16}.
	The UID 15 resource being already available, the final UID list of resource to be prefetched is reduced to
	the set {4,17,16}.
	\image html "nova_rscdep.png" "Fig. 2: Resource dependencies."
-	<b>IDLE</b>:<br>
	The manager offers the RscManager::Unload() function. This function explicitly unloads a resource set.
	Only <b>unused resources</b> will be deleted and will stop being available (see the resource state cycle).
	After this operation, the list of the RscPrefetchQ structure contains the UIDS of the resources currently being used.<br>
	This function cannot be applied to a PENDING query.


@section rsc_man_sec5 Adding a new resource factory
The RscManager::RegisterFactory() manager function allows the application to easily add its own resource factories.
These new factories allow Neova's manager to spport very efficiently types dedicated to the application.
Factories can be overloaded and factory registration for a given type replace the previous one.


@section rsc_man_sec6 Generic resource creation
Through the RscManager::CreateInstance() manager function, the application can obtain a <i>generic</i> NvResource interface on a resource.
This interface may then be type-casted appropriately to the type of the resource, given by the NvResource::GetRscType() method.


@section rsc_man_sec7 System parameters
The following system parameters are checked by the RscManager each time a BIGFILE is opened in order to configure prefetching: 
<ul>
<li>uint32 nv::core::PR_RSCMAN_CACHEBSIZE:<br>
	Byte size of the read cache used by the manager to load several more or less adjacent resources in a single system access (HDD, CD, ...).
	Optimal load is reached with a significant cache size and adjacent data on the storing device.
	Default value is 64kB.
<li>uint32 nv::core::PR_RSCMAN_READ_BSTART_ALIGN:<br>
	Read offset alignment in byte. Default value is 2048B, which corresponds to alignment on sector start.
<li>uint32 nv::core::PR_RSCMAN_READ_BSIZE_ALIGN:<br>
	Read size alignment in byte. Default value is 512B.
<li>nv::core::PR_RSCMAN_LOG_MAXBSIZE de type uint32:<br>
	Maximal byte distance between two adjacent resources in the BIGFILE to belong to the same read cache (i.e. if the distance betwen two adjacent resources is greater than this value, these two resources will be sequentially loaded). Default value is 4kB.
</ul>

It should be noted that depending on the platform, some of these parameters may be restricted in value or be ignored by the manager.
Please refer to the documentation on your Neova version for more information.


*/


