//DONE
/**

@page dpy_shading The shader model

<table border="0"><tr>
	<td class="pageRef" width="100px">interface</td>
	<td class="pageRef">NvShader</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvShader.h</td>
</tr><tr>
	<td class="pageRef">platform</td>
	<td class="pageRef">any</td>
</tr></table>

@section dpy_shading_sec1 Presenting the model

The NvShader graphical object is the base element for geometry display in the Neova CoreEngine.
A shader represents a central rasterization process, composed of:
- an input ( a graphical resource);
- parameters that are specific to the shader;
- global parameters that are managed by the DpyManager manager (i.e. display contexts);
- an output (target frame-buffers).

Figure 1. illustrates this model. Specific and global parameters are dealt with in the following pages (@ref dpy_ctxt and @ref dpy_state). Various rasterization processes are supported by the various shader classes, which derive from the NvShader base class.
This class is presented in the @ref dpy_obj_NvShader section. 

\image html "nova_shading.png" "Fig. 1: Shader model.."

@section dpy_shading_sec2 Simple example

In this example, a NvGeomShader shader is used to display a NvMesh resource (precompiled geometry).

\image html "nova_shading_ex1.png" "Fig. 2: Simple example."


@section dpy_shading_sec3 Example of composition for complex display.

In this example, the goal is to display a geometry in a TV-like style. Mirror reflection effects are simulated by doing an additive second mapping pass. Three shaders are necessary for this. The first two shaders display the same geometry in off-screen mode (i.e. in a texture), following distinct processes. The third shader then rasterizes the result in the final working frame, starting from a NvSurface procedural geometry.@n
The @ref dpy_sort section explains how to order shader display.

\image html "nova_shading_ex2.png" "Fig. 3: Example of composition for complex display."

*/


