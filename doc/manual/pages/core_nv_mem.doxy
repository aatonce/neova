//DONE
/**

@page core_nv_mem Memory management service

<table border="0"><tr>
	<td class="pageRef" width="100px">namespace</td>
	<td class="pageRef">nv::mem</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvCore_Mem.h</td>
</tr><tr>
	<td class="pageRef">platform</td>
	<td class="pageRef">any</td>
</tr></table>

This service manages the main memory. It adds many functionalities to standard memory management services of C and C++ environments.

@section core_nv_mem_sec1 Statistics, Log and Leaks
This service gives access at all times to various parameters that describe current memory state.
Aside from this, each memory allocation is identified and added to an internal history.
This history allows later analysis of application memory management in order to refine its memory use strategy,
or to identify memory leaks quickly.


@section core_nv_mem_sec2 nv::mem::AllocFlags allocation flags
Each allocation query is associated to a nv::mem::AllocFlags flag combination.
These flags are used by the main memory manager to efficiently identify the source of the query, as well as the nature,
the purpose and the persistence factor of the allocation query.
By taking these flags into account in its allocation strategy, the memory manager can optimize performance in term of
allocation/reuse speed, limitation of management space and prevention of memory fragmentation problems.


@section core_nv_mem_sec3 User memory manager
The nv::core::PR_MEM_ALLOCATOR system parameter can be used to set a new user memory manager to replace Neova CoreEngine's.
The application then disposes of total, global control on memory management.
The Neova CoreEngine has several specific subordinate memory allocators.
By basing itself on these implementations, a used memory manager can be quickly put into use.


@section core_nv_mem_sec4 Principles of use.
Several utility functions are available in the header file of this service in order to simplify memory management
and to make it comply with the C/C++ standard.
This functions can be found at various integration levels in the CoreEngine:<br>
<ul>
<li><code>Nv[M|C|Re]alloc</code>/<code>free</code></li>
	are 100% compatible with the corresponding stdlib functions;
<li>The various forms of the <code>new</code> and <code>delete</code> operators are overloaded;
<li><code>[Engine|Game][M|C|Re]alloc</code>/<code>[Engine|Game]Free</code>
	perform allocations in specific zones, namely GAME and ENGINE.
</ul>

In practice, the principles of use are the following:
<ul>
<li>The application must use the NvMalloc() function (and similar ones) to perform its memory allocations;
<li>The Neova runtime performs all its allocations exclusively through the EngineMalloc() function (and similar ones);
<li>As in the above, application extensions introduced in the engine must use the EngineMalloc() function (and similar ones)
to perform its memory allocations ;
<li>Depending on its needs, the application may pick a memory pool through the EngineMalloc() or GameMalloc() functions (and similar ones),
or through the nv::mem::Alloc() function which is more general.
</ul>

*/


