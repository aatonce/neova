//DONE
/**

@page rsc_nobf Management without BIGFILE

@section rsc_nobf_sec1 Objective
Neova's resource management by BIGFILE may sometimes be complicated or too restrictive in its use.
This is why using a BIGFILE is not an obligatory step in Neova: the used may develop its own resource loading management and use it in Neova.@n
The following sections explain all steps necessary to do this.

@section rsc_nobf_sec2 Prerequisites
A RscManager resource manager uses BigFiles asynchronously and substantially optimizes the loading process of a resource set.
However, the code of this manager is completely generic (hardware-independent) and it handles data loading in collaboration
with the various resource factories (see @ref rsc_factory).
The following steps are required:
- Loading compiled data from a storage device;
- Passing loaded data to the associated resource factories.

@section rsc_nobf_sec3 Steps
Loading the data can be made efficiently and asynchronously by the nv::file Neova service.
Despite this, any other means of data access can be chosen by the user.
Once the data loaded in memory, the user must register it in the RscFactory factory that matches the type of the resource.
The various factories can be reached via the RscManager manager, using the RscManager::GetFactory() function. In order to do this,
the resource manager must have been previously initialized.

The following code illustrates these steps:@n
@warning The following code is a minimalist example, and does not support data unzipping. The next section shows a complete code example.

@code
//
// Let LoadData() be our loading function

bool LoadData	 (	pvoid&	outDataPtr,
					uint&	outDataBSize,
					uint32&	outUID,
					uint32& outType 	);

//
// Data pre-loading function

bool PreloadData ( ... )
{
	// Loading the resource as a whole
	pvoid	myDataPtr;
	uint	myDataBSize;
	uint32	myDataUID;
	uint32	myDataType;
	LoadData( myDataPtr, myDataBSize, myDataUID, myDataType );

	// Getting the associated factory
	RscFactory* fac = RscManager::GetFactory( myDataType );
	if( !fac ) {
		NV_WARNING( "The factory must be registered first !" );
		return FALSE;
	}

	// Already pre-loaded?
	if( fac->IsAvailableRsc(myDataUID) )
		return TRUE;

	// Memory allocation request
	// > Warning, only a subset of the data may be necessary!
	// > The rest may for example be streamed later.
	pvoid  facDataPtr;
	uint32 facDataBSize = fac->GetToPrefetchRscBSize( myDataBSize );
	facDataPtr = fac->AllocMemRsc( facDataBSize );
	if( !facDataPtr )
		return FALSE;

	// Copying the data into the buffer of the factory
	Memcpy( facDataPtr, myDataPtr, facDataBSize );

	// Releasing the load buffer
	Free( myDataPtr );

	// Checking content (format and version)
	// This test is also performed by the PrefetchRsc() method of the factory
	if( !fac->CheckRsc(facDataPtr,facDataBSize) ) {
		NV_WARNING( "Invalid data !" );
		fac->FreeMemRsc( facDataPtr, facDataBSize );
		return FALSE;
	}

	// Passing the lead to the factory
	fac->PrefetchRsc( myDataUID, facDataPtr, facDataBSize );

	return TRUE;
}


// Initializing the manager
// > Neova resource factories are registered.
RscManager::Init();

PreloadData ( ... );
@endcode


@section rsc_nobf_sec4 Data compression
Compiled data may be compressed. The nv::libc::IsPacked() and nv::libc::Unpack() functions can be used to uncompress resource data before
passing it to the factory.@n
A resource factory class may use only a subpart of the data. Resources associated to such factory classes will mainly be streamed
and as such must never be compressed in this way.

The following code is a modified version of the preceding LoadData() function. It supports data unzipping:
@code
//
// Resource pre-loading function,
// with unzipping support.

bool PreloadData ( ... )
{
	// Loading the resource as a whole
	pvoid	myDataPtr;
	uint	myDataBSize;
	uint32	myDataUID;
	uint32	myDataType;
	LoadData( myDataPtr, myDataBSize, myDataUID, myDataType );

	// Getting the associated factory
	RscFactory* fac = RscManager::GetFactory( myDataType );
	if( !fac ) {
		NV_WARNING( "The factory must be registered first !" );
		return FALSE;
	}

	// Already pre-loaded?
	if( fac->IsAvailableRsc(myDataUID) )
		return TRUE;

	// Packed data ?
	uint unpackBSize;
	bool packed = nv::libc::GetUnpackBSize( myDataPtr, myDataBSize, unpackBSize );

	// Memory allocation request
	// > Warning, only a subset of the data may be necessary!
	// > The rest may for example be streamed later.
	pvoid  facDataPtr;
	uint32 facDataBSize = packed ? unpackBSize : fac->GetToPrefetchRscBSize( myDataBSize );
	facDataPtr = fac->AllocMemRsc( facDataBSize );
	if( !facDataPtr )
		return FALSE;

	// Copying data
	if( packed ) {
		// If compressed, the data cannot correspond to a subpart currently requested!
		// The data is unpacked directly into the buffer of the factory
		NV_ASSERT( facDataBSize == unpackBSize );
		NV_ASSERT( unpackBSize > myDataBSize );
		nv::libc::Unpack(	myDataPtr,
							myDataBSize,
							facDataPtr );
	} else {
		// Uncompressed data may be a currently requested subpart!
		// The data is copied into the buffer of the factory
		Memcpy( facDataPtr, myDataPtr, facDataBSize );
	}

	// Releasing the load buffer
	Free( myDataPtr );

	// Checking content (format and version)
	// This test is also performed by the PrefetchRsc() method of the factory
	if( !fac->CheckRsc(facDataPtr,facDataBSize) ) {
		NV_WARNING( "Invalid data !" );
		fac->FreeMemRsc( facDataPtr, facDataBSize );
		return FALSE;
	}

	// Passing the lead to the factory
	fac->PrefetchRsc( myDataUID, facDataPtr, facDataBSize );

	return TRUE;
}
@endcode

@section rsc_nobf_sec5 Assigning UIDs
In the preceding example, it has been assumed that the LoadData() function returns the UID associated to the resource.
This UID is needed to uniquely identify the resource. It can be extracted from any user databade, or dynamically generated by load-time numbering.
This is for the application to manage. UIDs may be generated by hashing the filename of the resource. The resulting UID must though correspond to a <b>NUMBERING</b> starting from 0 and the full 32-bit range must not be used, so as to allow this UID to be used as an index.

@section rsc_nobf_sec6 Using Quark-compiled resources
The Quark application is a part of the Neova CoreEngine package. This application performs data compilation for a target platform.
Filenames of Quark-compiled data match the following pattern: "UID TYPE [optional-path]".
- UID is a string of 8 hexadecimal digits that represent the UID of the resource;
- TYPE is a string of 8 hexadecimal digits that represent the TYPE of the resource.

A resource may depend on other resources. These dependencies are identified by a UID list associated to the resource.
With Quark, the dependencies of a resource are located in an auxiliary file whose filename matches the following pattern: 
"UID TYPE [optional-path].DEP". This file contains a binary vector of 32-bit words. Each 32-bit entry represents a UID.

@section rsc_nobf_sec7 Compatibility with BIGFILE use
User resource management as presented above is fully compatibly with previous, later or simultaneous use of a Neova BIGFILE.
The only rule to follow in order to do this is to remain coherent and to share UIDs and TYPEs for all manipulated resources.


*/


