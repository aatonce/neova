//DONE
/**

@page dpy_obj_NvPoseShader The NvPoseShader graphical object

<table border="0"><tr>
	<td class="pageRef" width="100px">interface</td>
	<td class="pageRef">NvPoseShader</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvPoseShader.h</td>
</tr><tr>
	<td class="pageRef">platform</td>
	<td class="pageRef">any</td>
</tr></table>

This shader handles multiple display of a NvMesh or NvSurface graphical resource.
The multiple display is configured by a pose list implemented by a vector of combined components.
The shader is created by the NvPoseShader::Create() static function.

@section dpy_obj_NvPoseShader_sec1 Particularities
Although this shader is used mostly in the same way as the NvGeomShader shader,
it does have the following additional functionalities:
- Multiple display of the associated geometry: The user gives a list of pose matrices.
	On some platforms, this multiple display is much more efficient than multiple display of the shader itself,
	through the DpyManager::Draw() function, and with different positionning contexts;
- Support of various pose display modes, which is particularly suited to certain geometry classes. The
	pose display mode is set by the NvPoseShader::SetMode() method;
- This shader dynamically computes an opacity value for each pose of the geometry: when doing
	alpha-blending, this opacity value is used to smoothen the visual of the output or brutal display pose exit.
	The latter scenario may occur for instance with frustum-limited pose display.


@section dpy_obj_NvPoseShader_sec2 Use
The following code illustrates the use of the NvPoseShader:
@code
	// Posable ?
	NvShader* shaderP = NULL;
	uint poseListComponents = NvPoseShader::C_LOCATION | NvPoseShader::C_ROTATION | NvPoseShader::C_SCALE | NvPoseShader::C_RGBA32;
	shaderP = NvPoseShader::Create( meshP, poseListComponents, 64 );
	if( shaderP ) {
		NvPoseShader* poseShader;
		poseShader = (NvPoseShader*) shaderP;
		poseShader->SetMode( NvPoseShader::M_LOCKED );
//		poseShader->SetMode( NvPoseShader::M_ORIENTED );
		poseShader->CreateAccess();
		Vec3*   tA;
		Quat*   rA;
		uint32* cA;
		Vec3*	sA;
		uint  tS, rS, cS, sS;
		tA = (Vec3*)   poseShader->GetAccess( NvPoseShader::C_LOCATION, tS );
		rA = (Quat*)   poseShader->GetAccess( NvPoseShader::C_ROTATION, rS );
		sA = (Vec3*)   poseShader->GetAccess( NvPoseShader::C_SCALE,	sS );
		cA = (uint32*) poseShader->GetAccess( NvPoseShader::C_RGBA32,   cS );
		NV_ASSERT( tA );
		NV_ASSERT( rA );
		NV_ASSERT( sA );
		NV_ASSERT( cA );
		for( int i = 0 ; i < poseShader->GetListSize() ; i++ ) {
			float l = 16.0f;
			float b = float(i) / 63.0f;
			float a = TwoPi * b;
			*tA = Vec3( Cosf(a)*l, 0, Sinf(a)*l );		AS_UINT32( tA ) += tS;
			*rA = Quat( a, Vec3(0,0,1) );				AS_UINT32( rA ) += rS;
			*sA = Vec3( b, b, b );						AS_UINT32( sA ) += sS;
			*cA = 0xFFFFFF00;							AS_UINT32( cA ) += cS;
		}
		poseShader->ValidateAccess( ~0U, ~0U );
		poseShader->ReleaseAccess();
		poseShader->SetListDrawSize( poseShader->GetListSize() );
		// Enable alpha fading !
		poseShader->SetFading( 10, 50 );
		poseShader->GetDisplayState(0)->Disable( DpyState::EN_WR_DEPTH );
		poseShader->GetDisplayState(0)->SetMode( DpyState::BM_FILTER | DpyState::BS_FRAGMENT );
		poseShader->GetDisplayState(0)->SetAlphaPass( 0.02f );
	}
	else {
		shaderP = NvGeomShader::Create( meshP );
	}
@endcode
*/

