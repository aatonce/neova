//DONE
/**

@page dpy_obj_NvShadowingShader The NvShadowingShader graphical object

<table border="0"><tr>
	<td class="pageRef" width="100px">interface</td>
	<td class="pageRef">NvShadowingShader</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvShadowingShader.h</td>
</tr><tr>
	<td class="pageRef">platform</td>
	<td class="pageRef">any</td>
</tr></table>

Rendering the shadowing of a geometry is done by a NvShadowingShader shader, which uses volume shadowing with a stencil buffer.
The shader is created by the NvShadowingShader::Create() function.
Only NvMesh resources are supported.

@section dpy_obj_NvShadowingShader_sec1 Functioning
This shader allows real-time, accurate rendering of the shadowing of a geometry, or the geometry itself as well as the rest of the scene.
This is done by volume shadowing.
A temporary stencil buffer is use to mask shadow zones in the current frame.
The shader mainly deals with shadow volume rendering in the stencil buffer and can also
handle a few other operations on the stencil buffer.
These operations correspond to the NvShadowingShader::Flags flags and are activated by the NvShadowingShader::SetFlags()
method of the shader.@n
The shader works as follows:
- Performing pre-operations;
- Rendering the shadow volumes in the stencil buffer;
- Performing post-operations.

The main operations correspond to the NvShadowingShader::F_CLEAR_STENCIL
and NvShadowingShader::F_DRAW_SHADOWS flags. The NvShadowingShader::F_CLEAR_STENCIL flag
is used to reset the stencil buffer that is shared among the shaders, for example to start a new shadowing session.
The flag NvShadowingShader::F_DRAW_SHADOWS flag is used to apply the current mask of the shared stencil buffer on the rendering frame which corresponds to the back buffer being constructed.@n
It should be noted that the stencil buffer is shared among the shaders. These operations can then be optimized:
Only the first shadowing shader will perform NvShadowingShader::F_CLEAR_STENCIL and only the last shadowing shader will perform
 NvShadowingShader::F_DRAW_SHADOWS. The example below illustrates this principle.
 
@note Temporary creation of the stencil buffer requires a variable amount of video memory, that will be shared among all shadowing shaders.
		The application must make sure to have a sufficient amount of vram available at shader creation time.

@section dpy_obj_NvShadowingShader_sec2 Temporary stencil buffer
It should also be noted that depending on the platform, the vram occupied by the temporary stencil buffer
may be used by other rendering shaders classes. However, the persistence of the stencil buffer is only guaranteed
during consecutive display of a NvShadowingShader sequence. This means that if the application wants such persistence
between two displays of shadowing shaders, namely to optimize NvShadowingShader::F_CLEAR_STENCIL and NvShadowingShader::F_DRAW_SHADOWS
operations, it must ensure that no other shading class will be displayed in-between.
In order to do this, a specific session may be created and/or shader sort deactivated.

@section dpy_obj_NvShadowingShader_sec3 Prepairing NvMesh resources
In order to be used as a shadowing source by a NvShadowingShader shader, a NvMesh resource must contain additional data.
The generation of this specific data must be specified at resource compilation time.
This depends on the resource compilation process and must be taken into account by the data production stream of the application.

@section dpy_obj_NvShadowingShader_sec4 Use
The following code illustrates the use of this shader:
@code
NvShadowingShader* shadow1 = NvShadowingShader::Create( nvMesh1 );
NvShadowingShader* shadow2 = NvShadowingShader::Create( nvMesh2 );

shadow1->Disable( NvShader::EN_SORTING );
shadow2->Disable( NvShader::EN_SORTING );
shadow1->Setup( sunLocation, 50.f0 );
shadow2->Setup( sunLocation, 100.f0 );

// shadow1 clears the stencil buffer.
shadow1->SetFlags( NvShadowingShader::F_CAP_ALL | NvShadowingShader::F_CLEAR_STENCIL );

// shadow2 accumulates in stencil buffer
// and draws the shadowed areas in the back frame buffer.
shadow2->SetFlags( NvShadowingShader::F_CAP_ALL | NvShadowingShader::F_DRAW_SHADOWS );
shadow2->SetColor( 0x44444400 );

DpyManager::SetSession( 12 );
DpyManager::Draw( shadow1 );
DpyManager::Draw( shadow2 );

@endcode

*/



