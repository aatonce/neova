//DONE
/**

@page dpy_ctxt Display contexts

A graphical context regroups several generic parameters that define the display environment of a graphical object.
Each object has an associated graphical context.<br>
On top of the specific display parameters of an object (that can be reached through its interface), the application can modify the object graphical context in a more global way before its display.

@section dpy_ctxt_sec1 Description
A graphical context is composed of the various following data categories:
- @ref dpy_ctxt_session "Session Number";
- @ref dpy_ctxt_target "Rendering Target";
- @ref dpy_ctxt_view "Visibility Information";
- @ref dpy_ctxt_light "Lighting sources";
- @ref dpy_ctxt_wtr "Space position".

@section dpy_ctxt_sec2 Principle
The following figure (fig. 1) illustrates context use between the various actors: the application, the DpyManager display manager and the graphical objects.

\image html "nova_dpyctxt.png" "Fig. 1: Display context management."

The DpyManager manager keeps a <b>current context</b> that is modified by the application through the various <code>Set[Session|Light|Target|View|WorldTR]()</code> functions.
When the application issues the display order for an object by the DpyManager::Draw() function,
the current context gets associated to the object as its graphical display context.

@code
Vec4 redAmbient( 1, 0, 0, 1 );
Vec4 greenAmbient( 0, 1, 0, 1 );

//
// Object 0

DpyManager::SetSession( 1 );
DpyManager::SetWorldTR( &objectM[0] );
DpyManager::SetLight( 0, &redAmbient );
// The complete current context is assigned to the object
// Session: 1
// WorldTR: objectM[0]
// Light0 : L
DpyManager::Draw( objectP[0] );

//
// Object 1

DpyManager::SetWorldTR( &objectM[1] );

// The complete current context is assigned to the object
// Session: 1
// WorldTR: objectM[1]
// Light0 : L
DpyManager::Draw( objectP[1] );

//
// Object 2

DpyManager::SetLight( 0, &greenAmbient );

// The complete current context is assigned to the object
// Session: 1
// WorldTR: objectM[1]
// Light0 : L'
DpyManager::Draw( objectP[1] );
@endcode

@section dpy_ctxt_sec3 Graphical object multicontext
Most graphical objects can be display several times in the same frame,
through successive calls to the DpyManager::Draw() function.
Each display query done through this function is associated to the current context of the DpyManager manager.
As a result, a graphical object has one or more graphical contexts, represented by a display multicontext.

Multicontext display of a graphical object is defined by the following points:
- Multiple displays of the same object (i.e. in the same frame) are performed atomically, without possibility for interlacing with the display of another object. When such interlacing is required, several objects will then be instanciated and used together;
- These display use the <b>session associated to the first display of the object</b>.
	The session numbers of following contexts are ignored;
- Some graphical objects limit context number to 1 (i.e. the object can only be displayed once per frame).
 The validation status of a display query can be checked by the return value of the DpyManager::Draw() function.

The following figure illustrates this principle:

\image html "nova_dpymultictxt.png" "Fig. 2: Multicontext display."

*/


