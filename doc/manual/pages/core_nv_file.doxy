//DONE
/**

@page core_nv_file File access service

<table border="0"><tr>
	<td class="pageRef" width="100px">namespace</td>
	<td class="pageRef">nv::file</td>
</tr><tr>
	<td class="pageRef">header</td>
	<td class="pageRef">NvCore_File.h</td>
</tr><tr>
	<td class="pageRef">platform</td>
	<td class="pageRef">any</td>
</tr></table>

Files contained in the database of the application can be accessed through this service.
Only a single file can be opened at a time. It then becomes the current target for all read requests.


@section core_nv_file_sec1 Read requests
A read request is defined by a structure:
ReadRequest = { bufferPtr, bsize, boffset[4], priority }.
Its attributes are: the destination memory buffer, the byte length of the data block to be read, up to 4 offsets
corresponding to the position of various copies of the same block in the current file, as well as a priority level between 0 and 15.<br>
Several read requests can occur simultaneously. Access is random and asynchronous.
Depending on the platform, several updates of the Core (nv::core::Update()) are required before load completion.
The completion status of a read request is determined by polling on its state throught the ReadRequest::GetState() method.

The following skeleton code illustrates the use of this service:

@code
#include <Nova.h>
using namespace nv;

static char buffer[ 2048 ];

void GameMain ( )
{
    // ...

    if( !core::Init() )
        return;

    // Opening the file
    #ifdef WIN32
    core::SetParameter( PR_FILE_PREFIX, "E:\\TEMPDATA\\" );
    core::SetParameter( PR_FILE_SUFFIX, ""				 );
    core::SetParameter( PR_MEDIA_TYPE,  "dvdrom"				 );
    #endif
    file::Open( "L01\\LEMONSEQ.DAT" );

    #ifdef _DEBUG
    Printf( "file %s opened.\n", file::GetFullname("L01\\LEMONSEQ.DAT");
    #endif

    // Creating the request
    file::ReadRequest rr;
    rr.Setup( buffer, 2048, 16 );
    file::AddReadRequest( &rr );
    NV_ASSERT( rr.GetState()==file::ReadRequest::PENDING );

    // Main loop
    for( ;; )
    {
        core::Update();

        if( rr.IsReady() ) {
            if( rr.GetState()!=file::ReadRequest::COMPLETED )
            {
                // request failed or aborted !
            }
            else
            {
                // data available in buffer
            }
        }

        if( ... )
            break;
    }

    // ...

    file::Close();
    core::Shut();
}
@endcode


@section core_nv_file_sec2 System parameters
The nv::core::PR_FILE_PREFIX and nv::core::PR_FILE_SUFFIX system parameters
are used by this service to complete the filename at opening time:
<code>fullname = filePrefix + inFilename + fileSuffix</code>.<br>
Their values depend entirely on the target platform and are initialized by Neova
to access a default storage device.<br>
For information about the format and possible values of these parameters,
please consult the documentation of your Neova version.



@section core_nv_file_sec3 Extending the file access system
Neova CoreEngine's kernel supports adding new data access drivers.
The system is based on abstract drivers that can be implemented so as to access any type of data in any particular way.
By default, the native driver that gives access to CDVD, host and devkit data is the only active driver.
The CoreEngine also includes a driver to access data hosted on a network. This driver is based on the NAFS file sharing protocol of the GeoStudio framework.
Neova CoreEngine's kernel API is not the subject of this document. However, as an example, the following code shows how to activate the NAFS client driver which enables transparent access to network-shared data:

@code
#include <Nova.h>
#include <Kernel/Common/NvkCore_File.h>


#define ENABLE_NAFS
#define NAFS_AS_DEFAULT_FS
#define NAFS_SERVER_IP		"192.168.4.123"


void GameMain()
{
	// Initializing the Neova CoreEngine
	if( !nv::core::Init() )
		return;

	// Adding nafs support
	#ifdef ENABLE_NAFS
	{
		nv::file::AbstractFS* nafs = nv::file::CreateNetworkAbstractFS();
		if( nafs ) {
			nafs->Init();
			if( nv::file::RegisterAbstractFS(nafs,"nafs") )
				DebugPrintf( "NAFS supported.\n" );
			else
				DebugPrintf( "NAFS failed.\n" );
		}

		// Is Nafs the default file system ?
		#ifdef NAFS_AS_DEFAULT_FS
		core::SetParameter( core::PR_FILE_PREFIX, "nafs:"NAFS_SERVER_IP",,,," );
		core::SetParameter( core::PR_FILE_SUFFIX, "" );
		#endif
	}
	#endif

	//
	// ...
	//
}

@endcode

*/


