//DONE
/** @file NvRscManager.h
	@brief Resource manager.
*/

/** @namespace RscManager
	@brief Resource manager.
*/


/** @var INVALID_RSC_UID
	Invalid resource identifier value.
*/
/** @var INVALID_RSC_TYPE
	Invalid resource type value.
*/


/** @struct NvResource
	@brief Neova resource base class.
*/
/** @fn uint32 NvResource::GetRscType	(		)
	@return Resource type.
*/
/** @fn uint32 NvResource::GetRscUID	(		)
	@return Resource UID.
*/


/** @struct RscPrefetchQ
	@brief Prefetch queue.

	A prefetch queue contains a resource list to be loaded in memory for later instanciation.
	Resources are defined by their UID (Unique IDentifier taking the form of a 32-bit integer).
	Prefetching the queue is handled by the resource manager through the RscManager::AddPrefetchQ function.@n
	Once the prefetching operation performed, the queue contains the UID set of the resource that have actually been loaded,
	after dependency resolving. This list may be much larger than the initial one.@n
	Prefetched resources can either be immediately instanciated or be unloaded depending on the prefetching strategy of the application.
	@sa RscManager::AddPrefetchQ(), RscManager::AbortPrefetchQ()
*/
/** @enum RscPrefetchQ::State
	Queue state. The operation supported by a queue are regrouped in the interface of the RscManager resource manager.
*/
/** @var RscPrefetchQ::IDLE
	The queue is not being processed.
*/
/** @var RscPrefetchQ::PENDING
	The queue is being processed by the RscManaer manager.
*/
/** @var RscPrefetchQ::COMPLETED
	The queue has been successfully prefetched. Resources are available in memory and can be immediately instanciated.
*/
/** @var RscPrefetchQ::ABORTED
	The prefetching process for the queue has been aborted through a call to the RscManager::AbortPrefetchQ() function.
*/
/** @var RscPrefetchQ::FAILED
	The prefetching process for the queue has failed.
*/
/** @fn RscPrefetchQ::RscPrefetchQ
	Initializes the queue.
*/
/** @fn void RscPrefetchQ::AddRsc	(	uint32		inUID		)
	Adds a resource to be prefetched to the queue.
	@param[in] inUID UID of the resource to be prefetched.
*/
/** @fn uint RscPrefetchQ::GetSize	(	)
	@return size of the queue, i.e. number of resources to be prefetched.
*/
/** @fn uint32 RscPrefetchQ::operator [](	int		inNo	)
	Access operator.
	@return UID of the resource inNo.
*/
/** @fn void RscPrefetchQ::Clear	(		)
	Clears the prefetch queue.
*/
/** @fn bool RscPrefetchQ::IsReady	(	)
	@retval TRUE if the queue is not being processed.
*/
/** @fn RscPrefetchQ::State RscPrefetchQ::GetState	(	)
	@return Current queue state.
*/
/** @var RscPrefetchQ::stt
	Private.
*/
/** @var RscPrefetchQ::next
	Private.
*/
/** @var RscPrefetchQ::rscUIDA
	Private.
*/




/** @fn bool RscManager::Init	(		)
	Initializes the resource manager.
	@retval TRUE if the manager is ready.
*/
/** @fn bool RscManager::Shut	(		)
	Releases the manager.
*/
/** @fn bool RscManager::RegisterFactory	(	RscFactory*		inFactory	)
	Registers a new resource factory.
	@param[in] inFactory Resource factory.
*/
/** @fn RscFactory* RscManager::GetFactory	(	uint32		inType		)
	@param[in] inType Type of the factory, i.e. of the associated resource.
	@return Factory associated the specified resource type.
*/


/** @fn void RscManager::Update	(	float		inMaxMsTime		)
	Updates the resource manager for a certain amount of CPU time.
	This update must come along with an update of Neova's kernel through the nv::core::Update() function.
	@param[in] inMaxMsTime Maximal CPU time to be used for the update, in milliseconds.
*/

/** @fn bool RscManager::OpenBigFile	(	pcstr		inPath		)
	Opens a bigfile. Only a single bigfile can be opened at a time.
	@param[in] inPath Path to the bigfile.
	@retval TRUE upon success.
*/
/** @fn bool RscManager::CloseBigFile	(		)
	Closes the current bigfile.
*/
/** @fn pcstr RscManager::GetBigFile	(		)
	@return Path to the bigfile.
*/

/** @fn bool RscManager::AddPrefetchQ	(	RscPrefetchQ*	inPrefetchQ	)
	Registers a new prefetch queue to be processed.
	@param[in] inPrefetchQ Prefetch queue.
	@retval TRUE if the queue has been registered.
*/
/** @fn bool RscManager::AbortPrefetchQ	(	RscPrefetchQ*	inPrefetchQ	)
	Deletes a prefetch queue from the list to be processed.
	If the specified queue is being processed, the prefetching process cannot be interrupted anymore.
	@param[in] inPrefetchQ Prefetch queue.
	@retval TRUE if the prefetching of the queue has been cancelled.
*/
/** @fn RscPrefetchQ* RscManager::GetProgress(	float*	outProgress	)
	Returns information on the queue currently being prefetched.
	@param[out] outProgress Progress coefficient defined in [0,1].
	@return Pointer to the queue currently being prefetched.
	@retval NULL if there is no active prefetching process.
*/
/** @fn uint RscManager::Unload( RscPrefetchQ*	inPrefetchQ	)
	Unloads a queue of resources that have been prefetched but not necessary instanciated.
	Resources among this list that have been instanciated can't be unloaded and will be ignored.
	@param[in] inPrefetchQ Resource queue to be unloaded.
	@return Number of unloaded resources.
*/	
/** @fn uint RscManager::Unload( UInt32A&	inUIDs	)
	Unloads a set of resources that have been prefetched but not necessary instanciated.
	Resources among this list that have been instanciated can't be unloaded and will be ignored.
	@param[in] inUIDs Array of resource uids.
	@return Number of unloaded resources.
*/	
/** @fn uint RscManager::Unload( uint32 inUID	)
	Unloads a resource that have been prefetched but not necessary instanciated.
	@param[in] inUID uid of the resource to unload.
	@retval 1 if the resource has been unloaded.
	@retval 0 if the resource is instancied and can't be unloaded.
*/	
/** @fn uint RscManager::UnloadAll( )
	Unloads all prefetched but not instanciated resources in memory.
	@return Number of unloaded resources.
*/	

/** @struct RscManager::RscInfo
	@brief Information on a resource located in the current bigfile.
*/
/** @var uint32 RscManager::RscInfo::uid
	Resource UID.
*/
/** @var uint32 RscManager::RscInfo::type;
	Resource type. This is a 32-bit integer CRC.
	
/** @var uint32 RscManager::RscInfo::bsize
	Resource byte size.
*/
/** @var uint32 RscManager::RscInfo::boffset[4]
	4 possible locations of the resource in the bigfile.
	If the 4 locations share the same value, only a single copy of the resource data exists;
	3 identical locations mean that 2 copies of the resource data exist in the bigfile, etc.
*/
/** @var uint32 RscManager::RscInfo::depCpt
	Number of internal dependencies of the resource.
*/

/** @fn uint32 RscManager::GetRscCpt(	)
	@return Number of resources in the current bigfile.
*/
/** @fn uint32 RscManager::GetRscUID(	uint	inNo	)
	@param[in] inNo Index defined in[ 0, RscManager::GetRscCpt() [.
	@return UID of the inNo resource.
*/
/** @fn bool RscManager::GetRscInfo	(	uint32			inUID,
										RscInfo*		outInfo		)
	Returns information on a UID-identified resource.
	@param[in] inUID Resource UID.
	@param[out] outInfo Resource information.
	@retval TRUE if information has been found.
*/

/** @fn uint32 RscManager::FindRscByType(	uint32			inType,
											uint32			inPreviousUID = 0	)
	Searches for the next resource of the given type.
	@param[in] inType Type of the resource to be found.
	@param[in] inPreviousUID Reference UID. If set at 0, the search will start from the first resource of the bigfile,
				else it will start from the first resource associated to this UID.
	@retval 0 No result.
*/

/** @fn NvResource* RscManager::CreateInstance	(	uint32	inUID	)
	Creates an instance of the resource.
	@param[in] inUID Resource UID.
	@return Pointer to the resource instance.
	@retval NULL Ressource unavailable, it must be prefetched.
*/

/** @fn bool RscManager::IsAvailableRsc	(	uint32	inUID	)
	@param[in] inUID Resource UID.
	@retval TRUE if the resource is available in memory.
*/

/** @fn uint RscManager::GetAvailableRsc(	)
	@return Number of resources available in memory.
*/


/** @struct RscManager::RscLog
	@brief Information on the loading time of a resource.
*/
/** @var uint32 RscManager::RscLog::uid
	UID of the loaded resource.
*/
/** @var float RscManager::RscLog::t
	Load time of the resource, in seconds.
	@sa nv::clock
*/

/** @fn bool RscManager::GetRscLog(	nv::vector<RscLog> & outLog )
	Returns the list of resources that have been loaded since the last call to this function,
	and resets the internal list.@n
	The nv::core::PR_RSCMAN_LOG_MAXBSIZE system parameter defines the maximal memory footprint
	of this internal list, in bytes.@n
	This service is disabled in Neova's "master" versions.
	@param[out] outLog List of loaded resources.
	@return The list overflow status. FALSE if the list is complete and valid.
			TRUE if the list has reached the limit defined by the nv::core::PR_RSCMAN_LOG_MAXBSIZE system parameter.
*/



