//DONE
/** @file NvCore_File.h
	@brief File system access.
*/

/** @namespace nv::file
	@brief File system access.

	This service gives access to the files of the application database.
	Only a single file can be opened at a time by the nv::file::Open() function, and becomes the current target.
	Loading requests are performed through the nv::file::ReadRequest structure.
	To ensure load progress, the application must periodically update Neova's core by a call to the nv::core::Update() function.
	
	@attention The RscManager resource manager is a generic, HW-independent service that is based entirely on this file access service.
	Opening a bigfile in the RscManager sets it as the current file for this service and the application can,
	according to its needs, simultaneously access the bigfile data through the RscManager and/or by a direct read request.
	However, closing the bigfile will also close the current file.est en fait un service g�n�rique, HW non-d�pendant,
	reposant enti�rement sur ce service d'acc�s aux fichiers.
*/

/** @fn bool nv::file::Open( pcstr )
	Opends the current file. Only a single file can be opened at a time, and becomes the current target.
	@param[in] inFilename Character string identifying a filesystem file. This filename is augmented by the nv::core::PR_FILE_PREFIX and
				nv::core::PR_FILE_SUFFIX system parameters.
				File designation varies from a platform to the other.
				Please refer to the documentation specific to each platform for more information.
	@retval TRUE if the file has been successfully opened.
	@sa nv::core::PR_FILE_PREFIX, nv::core::PR_FILE_SUFFIX
*/

/** @fn bool nv::file::Close( )
	Closes the current file.
	@retval TRUE if the file has successfully been closed.
*/

/** @fn uint32 nv::file::GetSize( )
	Information on current file size.
	@return Byte size of the current file.
*/

/** @fn pcstr nv::file::GetFullname( pcstr )
	Completes a file designation with system augmentation.
	@return Augmented file designation.
	@sa nv::file::Open()
*/

/** @fn uint16 nv::file::GetPriorityMask( )
	Returns the channel activity mask. There are 16 channels, from 0 (lowest) to 15 (highest).
	The bit i of this mask enables (b[i]=1) or disables (b[i]=0) the data channel i.
	@return Channel activity mask.
	@sa nv::file::SetPriorityMask(), nv::file::AddReadRequest()
*/

/** @fn void nv::file::SetPriorityMask( uint16 )
	Sets the channel activity mask. There are 16 channels, from 0 (lowest) to 15 (highest).
	The bit i of this mask enables (b[i]=1) or disables (b[i]=0) the data channel i.
	@param[in] inMask Channel activity mask.
	@sa nv::file::GetPriorityMask(), nv::file::AddReadRequest()
*/

/** @fn bool nv::file::AddReadRequest( ReadRequest* inRequest, uint8 inPriorityLevel = 8 )
	Registers a read request for the current file. The operation parameters are given by the ReadRequest structure.
	After being registered, the request state becomes ReadRequest::PENDING.
	The read operation is associated to a priority level, from 0 (lowest) to 15 (highest).
	An operation of priority P will be performed if:
	@li The channel associated to P is activated (see nv::file::SetPriorityMask());
	@li No other operation has a priority > P;
	@li The operation if the first in queue for channel P.

	Read operations are asynchronous and they require several updates of Neova's core through the nv::core::Update() function.
	The completion of a read request is indicated by the current state of the request.
	@param[in,out] inRequest Read request.
	@param[in] inPriorityLevel Priority level in [0(lowest), 15(highest)].
	@retval TRUE if the request has been successfuly registered.
	@sa nv::file::GetPriorityMask(), nv::file::AbortReadRequest()
*/

/** @fn bool nv::file::AbortReadRequest( ReadRequest* inRequest )
	Cancels the registration of a read request for the current file.
	A read request can be cancelled up to the time when it becomes effective.
	From this moment on, it cannot be cancelled anymore anf this function returns FALSE.
	After successful cancellation of a request, its state changes to ReadRequest::ABORTED.
	@param[in,out] inRequest Read request.
	@retval TRUE if the request has been successfully cancelled.
	@sa nv::file::AddReadRequest()
*/

/**	@fn uint nv::file::GetRequestQueueSize	(	)
	@return Number of requests in the pending queue.
	@sa nv::file::AddReadRequest(), nv::file::AbortReadRequest()
*/

/** @struct nv::file::ReadRequest
	@brief Read request parameters.

	This structure represents a read requests for data in the current file.
	An instance of this class is not initialized by default.
	Its initialization and parameter definition are done by the nv::file::ReadRequest::Setup() method.
	The parameters correspond to the destination memory buffer, the size of the data to be read,
	and up to 4 alternative locations of the content to be read. The location to be used will be determined
	at reading time	for more performance.
*/

/** @var nv::file::ReadRequest::bSize
	Private
*/
/** @var nv::file::ReadRequest::bOffset
	Private
*/
/** @var nv::file::ReadRequest::bufferPtr
	Private
*/
/** @var nv::file::ReadRequest::sysFlags
	Private
*/
/** @var nv::file::ReadRequest::stt
	Private
*/
/** @var nv::file::ReadRequest::next
	Private
*/

/** @fn bool nv::file::ReadRequest::Setup( pvoid, uint32, uint32, uint32, uint32, uint32 )
	Initializes a read request and defines its parameters.
	Up to 4 locations for the same content are supported in order to optimize data read performance on the storing device.
	The file structure is handled by the application. When using a bigfile, the RscManager resource manager logs loading operations
	in order to optimize load times.
	@param[in] inBufferPtr Pointer to the data load buffer.
				The address of this buffer must be aligned on 16 bytes!
	@param[in] inBSize Byte size of the data block to be loaded. This size must be aligned on 16 bytes.
	@param[in] inBOffset0 Source data block location, in bytes, relatively to the start of the file.
	@param[in] inBOffset1 Optional location of the first copy of the data.
	@param[in] inBOffset2 Optional location of the second copy of the data.
	@param[in] inBOffset3 Optional location of the third copy of the data.
	@retval TRUE if the request parameters are valid.
	@sa RscManager::GetRscLog(), nv::file::AddReadRequest()
*/

/** @fn bool nv::file::ReadRequest::IsReady( )
	@retval TRUE if the read request has been processed.
*/

/** @fn nv::file::ReadRequest::State nv::file::ReadRequest::GetState( )
	@return Read request current state.
*/

/** @var nv::file::ReadRequest::IDLE
	The read request has been initialized.
*/
/** @var nv::file::ReadRequest::PENDING
	The read request is pending processing, or being processed.
*/
/** @var nv::file::ReadRequest::COMPLETED
	The read request has been successfully performed and its result is available in memory.
*/
/** @var nv::file::ReadRequest::ABORTED
	The read request has been cancelled.
*/
/** @var nv::file::ReadRequest::FAILED
	The read request has failed.
*/


/** @fn bool nv::file::DumpToFile	(	pcstr			inFilename,
										pvoid			inBuffer,
										uint			inBSize		)
	Saves data on disk. This operation is only available in development versions of the Neova CoreEngine (<MASTER).
	@param[in] inFilename Character string identifying a filesystem file. This filename is augmented by the nv::core::PR_FILE_PREFIX and
				nv::core::PR_FILE_SUFFIX system parameters.
				File designation varies from a platform to the other.
				Please refer to the documentation specific to each platform for more information.
	@param[in] inBuffer Pointer to the data to be saved.
	@param[in] inBSize Data byte size.
	@retval TRUE upon success.
	@sa nv::file::AppendToFile(), nv::file::DumpFromFile()
*/


/** @fn bool nv::file::DumpFromFile	(	pcstr			inFilename,
										pvoid &			outBuffer,
										uint  &			outBSize		)
	Loads an entire file in memory. This operation is only available in development versions of the Neova CoreEngine (<MASTER).
	This function allocated the memory block necessary for data storing.
	The application is responsible for releasing this memory block after use.
	@param[in] inFilename Character string identifying a filesystem file. This filename is augmented by the nv::core::PR_FILE_PREFIX and
				nv::core::PR_FILE_SUFFIX system parameters.
				File designation varies from a platform to the other.
				Please refer to the documentation specific to each platform for more information.
	@param[in] outBuffer Pointer to the memory block to be loaded.
	@param[in] outBSize Byte size of the data (i.e. of the memory block).
	@retval TRUE upon success.
	@sa nv::file::AppendToFile(), nv::file::DumpToFile()
*/


/** @fn bool nv::file::AppendToFile	(	pcstr			inFilename,
										pvoid			inBuffer,
										uint			inBSize		)
	This function does the same thing as the nv::file::DumpToFile() function aside from the fact that the data is appended to the file.
	@retval TRUE upon success.
	@sa nv::file::DumpToFile(), nv::file::DumpFromFile()
*/


