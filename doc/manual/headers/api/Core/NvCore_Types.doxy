//DONE
/** @file NvCore_Types.h
	@brief Type definitions.

	The base types are defined in the nv::types namespace and are then introduced at global level.
	The _NvCore_Types_NOT_GLOBAL_ symbol can be define to prevent exporting Neova CoreEngine's base types to the global level.

	In case of conflict, these these types can be globally redefined by:
	@code
	//
	// Main inclusion header.
	//
	
	// Put your own type definitions here.
	typedef signed char			int8;
	typedef unsigned char		uint8;
	typedef signed short		int16;
	typedef unsigned short		uint16;
	typedef signed int			int32;
	typedef unsigned int		uint32;
	// ...
	
	#define _NvCore_Types_NOT_GLOBAL_
	#include <Nova.h>
	// ...
	@endcode

	Neova's base types are always available. The following code shows a partial redefinition if the application cannot redefine
	for example the uint64 or uint128 types for the platform:
	@code
	//
	// Main inclusion header
	//

	#define _NvCore_Types_NOT_GLOBAL
	#include <NvCore_Types.h>					// predefinition of base types

	// Final type definitions are here
	typedef signed char			int8;
	typedef unsigned char		uint8;
	typedef signed short		int16;
	typedef unsigned short		uint16;
	typedef signed int			int32;
	typedef unsigned int		uint32;
	// ...
	typedef nv::types::uint64		uint64;		// System uint64 definition
	typedef nv::types::uint128		uint128;	// System uint128 definitions

	#include <Nova.h>							// Introduces all headers,
												// aside from NvCore_Types.h that has already been parsed.
	// ...
	@endcode

	The NV_COMPILE_TIME_CHECK_TYPES() macro tests (at compilation time only) the compatiblity of type definitions,
	between compiled Neova libraries and global redefined types. This macro is inserted at the beginning of application code,
	for example in the GameMain() function.
	
	@warning The automatic documentation system we use predents some bugs with handling typdefs with identical names,
	even if they are defined in distincts namespaces. This happens with the ::int8 and nv::types::int8 types.
	This bug currently prevents distinct documentation on these distinct types.
	Please refer to the NvCore_Types.h source file for better visibility.
*/


/** @namespace nv::types
	@brief Base types

	This namespace contains the various base types.
*/
