
import sys, os, operator

inf  = 'keats.txt'
outf = '../src/Core/Common/nv_key.h'
keyx = 0x7C

outk = []
inl  = file(inf,'r').readlines()
for l in inl :
	for c in l :
		outk.append( operator.xor( ord(c),keyx) )

outl = []
outl.append( '\n' )
outl.append( '\n' )


outl.append( '// TEXT:\n' )
for l in inl :
	outl.append( '// %s\n' % l )
outl.append( '\n' )
outl.append( '\n' )


outl.append( '// KEY:\n' )
l = '// '
for c in outk :
	l += '%02X' % c
outl.append( l+'\n' )
outl.append( '\n' )
outl.append( '\n' )

l = ''
n = len( outk );
for c in outk :
	if n>1 :	l += '0x%02X, ' % c
	else:		l += '0x%02X'   % c
	n = n-1

outl.append( l+'\n' )
outl.append( '\n' )
outl.append( '\n' )


file(outf,'w').writelines( outl )


