// VideoConvertView.h : interface of the CVideoConvertView class
//


#pragma once
#include "afxwin.h"
#include "PicBox.h"
#include "afxcmn.h"


#define EXPORT_FRAME_RATIO_RANGE 10

class CVideoConvertDoc;
class CVideoConvertView : public CFormView
{
	friend class CVideoConvertDoc;
protected: // create from serialization only
	CVideoConvertView();
	DECLARE_DYNCREATE(CVideoConvertView)

public:
	enum{ IDD = IDD_VIDEO_CONVERT_FORM };

// Attributes
public:
	CVideoConvertDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CVideoConvertView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void ClearAll();

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	CString m_strConvertStatus;
	CString m_strCompFactor;
	CString m_strFrameRate;
	CProgressCtrl m_ConvertProgress;
	CSliderCtrl m_ConvertCompression;
	CSliderCtrl m_ConvertFrameRate;
	SPicBox m_PreviewBox;
	CButton m_exportJpeg;
	CButton m_exportWav;
	CButton m_exportSwizzle;
	CComboBox m_exportPlatform;
	CComboBox  m_rgbCombo;

	afx_msg void OnStnClickedCompressionFactor();
	afx_msg void OnExportJPG();
	afx_msg void OnExportWav();
	afx_msg void OnCbnSelchangeComboPlatform();
};

#ifndef _DEBUG  // debug version in VideoConvertView.cpp
inline CVideoConvertDoc* CVideoConvertView::GetDocument() const
   { return reinterpret_cast<CVideoConvertDoc*>(m_pDocument); }
#endif

