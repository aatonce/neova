// VideoConvert.h : main header file for the VideoAna application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

#include "dlgset.h"


// CVideoConvertApp:
// See VideoConvert.cpp for the implementation of this class
//

class CVideoConvertApp : public CWinApp
{
public:
	CVideoConvertApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
public:
	virtual int ExitInstance();
};

extern CVideoConvertApp theApp;