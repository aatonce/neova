// VideoConvert Version 3.0\n\nVideo analyzing framework demonstration program.

// VideoConvertView.cpp : implementation of the CVideoConvertView class
//

#include "stdafx.h"
#include "VideoConvert.h"

#include "VideoConvertDoc.h"
#include "VideoConvertView.h"
#include "stdio.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CVideoConvertView

IMPLEMENT_DYNCREATE(CVideoConvertView, CFormView)

BEGIN_MESSAGE_MAP(CVideoConvertView, CFormView)
	ON_WM_TIMER()
	ON_STN_CLICKED(IDC_COMPRESSION_FACTOR, &CVideoConvertView::OnStnClickedCompressionFactor)
	ON_BN_CLICKED(IDC_EXPORT_JPG, &CVideoConvertView::OnExportJPG)
	ON_BN_CLICKED(IDC_EXPORT_WAV, &CVideoConvertView::OnExportWav)
	ON_CBN_SELCHANGE(IDC_COMBO_PLATFORM, &CVideoConvertView::OnCbnSelchangeComboPlatform)
END_MESSAGE_MAP()

// CVideoConvertView construction/destruction

CVideoConvertView::CVideoConvertView()
	: CFormView(CVideoConvertView::IDD)
	, m_strConvertStatus(_T("Open a video file and push \"start\"."))
	, m_strCompFactor(_T("Quality: 95%"))
	, m_strFrameRate(_T("Export all frames"))

{
	// TODO: add construction code here

}


CVideoConvertView::~CVideoConvertView()
{
}

void CVideoConvertView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PREVIEWBOX, m_PreviewBox);
	DDX_Text(pDX, IDC_CONVERT_STATUS, m_strConvertStatus);
	DDX_Text(pDX, IDC_COMPRESSION_FACTOR, m_strCompFactor);
	DDX_Text(pDX, IDC_FRAMERATE_TEXT, m_strFrameRate);
	DDX_Control(pDX, IDC_CONVERT_PROGRESS, m_ConvertProgress);
	DDX_Control(pDX, IDC_CONVERT_SLIDER, m_ConvertCompression);
	DDX_Control(pDX, IDC_FRAMERATE_SLIDER, m_ConvertFrameRate);
	DDX_Control(pDX, IDC_EXPORT_JPG, m_exportJpeg);
	DDX_Control(pDX, IDC_EXPORT_WAV, m_exportWav);
	DDX_Control(pDX, IDC_SWIZZLE, m_exportSwizzle);
	DDX_Control(pDX, IDC_RGB_COMBO, m_rgbCombo);
	DDX_Control(pDX, IDC_COMBO_PLATFORM, m_exportPlatform);
	
}

BOOL CVideoConvertView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CVideoConvertView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//CDialogSettings ds(m_hWnd);
	//ds.Load("Software\\CodeProject\\DialogAppExample");   

	m_ConvertCompression.SetPos(95);

	m_ConvertFrameRate.SetRange(1,10);
	m_ConvertFrameRate.SetPos(10);

	m_exportWav.SetCheck(TRUE);

	m_strCompFactor.Format("Quality: %d %%", m_ConvertCompression.GetPos());
	m_strFrameRate.Format("Export all frames");

	if (!m_rgbCombo.GetCount())
	{
		m_rgbCombo.AddString("RGB");
		m_rgbCombo.AddString("BGR");
		m_rgbCombo.SetCurSel(0);
	}

	if (!m_exportPlatform.GetCount())
	{
		m_exportPlatform.AddString("PC");
		m_exportPlatform.AddString("Wii");
		m_exportPlatform.SetCurSel(1);
		m_exportSwizzle.SetCheck(TRUE);
	}

}


// CVideoConvertView diagnostics

#ifdef _DEBUG
void CVideoConvertView::AssertValid() const
{
	CFormView::AssertValid();
}

void CVideoConvertView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CVideoConvertDoc* CVideoConvertView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CVideoConvertDoc)));
	return (CVideoConvertDoc*)m_pDocument;
}
#endif //_DEBUG


// CVideoConvertView message handlers

LRESULT CVideoConvertView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your specialized code here and/or call the base class
	switch(message)
	{
	case WM_USER_PREVIEW_FRAME:
		{
			// Preview image
			m_PreviewBox.SetDibBits((LPBYTE)wParam, (int)lParam);
			m_PreviewBox.PaintDIB();

			return 0;
			break;
		}

	case WM_USER_UPDATE_PROGRESS:
		{
			// Convert status
			m_ConvertProgress.SetPos((int)wParam);
			m_strConvertStatus.Format("Reading... Frame: %d/%d", wParam, GetDocument()->m_nTotalFrames);
			UpdateData(FALSE);
			return 0;
			break;
		}

	case WM_HSCROLL:
		{
			m_strCompFactor.Format("Quality: %d%%", m_ConvertCompression.GetPos());
			if (m_ConvertFrameRate.GetPos()==10)
			{
				m_strFrameRate.Format("Export all frames", m_ConvertFrameRate.GetPos());
			}
			else
			{
				m_strFrameRate.Format("Export %d/10 frame(s)", m_ConvertFrameRate.GetPos());
			}
			UpdateData(FALSE);
			return 0;
			break;
		}

	case WM_APP_GRAPHNOTIFY:
		{
			GetDocument()->OnGraphNotify();
			return 0;
		}

	default:
		return CFormView::WindowProc(message, wParam, lParam);
	    break;
	}

}

void CVideoConvertView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if(ID_TIMER_EVENT_UPDATE == nIDEvent)
		SetEvent(GetDocument()->m_hUpdateEvent);

	CFormView::OnTimer(nIDEvent);
}

void CVideoConvertView::ClearAll()
{
	m_ConvertProgress.SetPos(0);
	m_PreviewBox.SetDibBits(NULL, 0);

	m_strConvertStatus = "Open a video file and push \"start\".";

	UpdateData(FALSE);
}


void CVideoConvertView::OnStnClickedCompressionFactor()
{
	// TODO: Add your control notification handler code here
}


void CVideoConvertView::OnExportJPG()
{
	//CString l_strCheckBoxVal;
	//int l_ChkBox = m_exportJpeg.GetCheck();
	//l_strCheckBoxVal.Format("Check Box Value : %d",l_ChkBox);
	//MessageBox(l_strCheckBoxVal);	
}

void CVideoConvertView::OnExportWav()
{
	// TODO: Add your control notification handler code here
}

void CVideoConvertView::OnCbnSelchangeComboPlatform()
{
	// TODO: Add your control notification handler code here

	int platforfmId = m_exportPlatform.GetCurSel();

	switch (platforfmId)
	{
		// PC
		case 0:
		m_exportSwizzle.SetCheck(FALSE);
		m_rgbCombo.SetCurSel(1);
		break;

		// WII
		case 1:
		m_exportSwizzle.SetCheck(TRUE);
		m_rgbCombo.SetCurSel(0);
		break;
	}

}
