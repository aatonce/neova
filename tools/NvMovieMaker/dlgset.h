#ifndef __DLGSET_H__
#define __DLGSET_H__

/*
 *  CDialogSettings class
 *
 *  umdev - multi-ide
 *  http://www.uemake.com
 *
 *  you may freely use or modify this code
 *  -------------------------------------------------------------
 *  This code is published with hope that will be useful for you.
 *  All what you need to use this class in your application is to
 *  include this header file into your source file and to the
 *  initialization routine add Load() function and to the destroy
 *  routine to add Save() function.
 */

#include <stdlib.h>
#include <windowsx.h>
#include <commctrl.h>

class CDialogSettings {
public:
  CDialogSettings(HWND hWindow) {
    this->hWindow = hWindow;
  }
  /*
    This function find child controls and store state of these windows in
    the registry.
    lpszRegPath - path into the registry where you want to store data
  */
  BOOL Save(LPCSTR lpszRegPath) {
    lparam p;
    p.bSave = TRUE;
    p.hWindow = hWindow;
    p.lpszRegPath = lpszRegPath;
    if (hWindow && IsWindow(hWindow)) {
      return EnumChildWindows(hWindow, &ep, reinterpret_cast<LPARAM>(&p));
    } else {
      return FALSE;
    }
  }
  /*
    This function restore state of child controls
    lpszRegPath - path into the registry where are data saved
  */
  BOOL Load(LPCSTR lpszRegPath) {
    lparam p;
    p.bSave = FALSE;
    p.hWindow = hWindow;
    p.lpszRegPath = lpszRegPath;
    if (hWindow && IsWindow(hWindow)) {
      return EnumChildWindows(hWindow, &ep, reinterpret_cast<LPARAM>(&p));
    } else {
      return FALSE;
    }
  }
  struct lparam {
    HWND hWindow;				// handle of dialog window
    LPCSTR lpszRegPath;	// path to the registry
    BOOL bSave;					// flag if we need data save to or load from the registry
  };
private:
  /*
    simple class to allocate memory. I am using this construction very often,
    because it is an assurance that memory blocks will be also deallocated :)
  */
  class CMem {
  public:
    CMem(DWORD dwSize) {
      p = new char[dwSize];
      s = dwSize;
    }
    ~CMem() {
      if (p) {
        delete[] p;
      }
    }
    char* p;
    DWORD s;
  };
  /*
    enumerate procedure that will store/restore data to/from registry.
    function handles most common controls that we are using to get
    input from users.
  */
  static BOOL __stdcall ep(HWND hWindow, LPARAM lParam) {
    static char buffer[32];
    lparam* p = reinterpret_cast<lparam*>(lParam);
    /*
      condition	(GetParent(hWindow)==p->hWindow) is used here, because
      we want to work only with dialog controls, not with child windows
      of these controls. work with child windows of  controls may cause
      problems (for example IPAddress has  a  four  child edit controls
      with same ID. We don't need to work with these windows directly.
    */
    if (GetParent(hWindow)==p->hWindow && GetDlgCtrlID(hWindow)!=65535) {
      if (GetClassName(hWindow, buffer, sizeof(buffer))) {
        if (!strcmp(buffer, "Edit") ||
            !strcmp(buffer, "RICHEDIT") ||
            !strcmp(buffer, "RichEdit20A")) {
          if (p->bSave) {
            /* from the edit controls we need a text */
            DWORD dwSize = Edit_GetTextLength(hWindow);
            CMem  buf(dwSize+1);
            if (GetWindowText(hWindow, buf.p, buf.s)) {
              if (!SetKey(HKEY_CURRENT_USER, p->lpszRegPath,
                i2s(GetDlgCtrlID(hWindow)),
                buf.p, buf.s, REG_SZ)) {
                return FALSE;
              }
            }
          } else {
            LPCSTR lpId  = i2s(GetDlgCtrlID(hWindow));
            DWORD dwSize = GetKeySz(HKEY_CURRENT_USER, p->lpszRegPath, lpId, REG_SZ);
            if (dwSize) {
              CMem buf(dwSize);
              if (GetKey(HKEY_CURRENT_USER, p->lpszRegPath, lpId, buf.p, buf.s, REG_SZ)) {
                SetWindowText(hWindow, buf.p);
              }
            }
          }
        } else if (!strcmp(buffer, "Button")) {
          /* from checkboxed and radio buttons we need a check state */
          if (p->bSave) {
            DWORD nCheckState = Button_GetCheck(hWindow);
            if (!SetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &nCheckState, sizeof(nCheckState), REG_DWORD)) {
              return FALSE;
            }
          } else {
            DWORD nCheckState=0;
            if (GetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &nCheckState, sizeof(nCheckState), REG_DWORD)) {
              Button_SetCheck(hWindow, nCheckState);
            }
          }
        } else if (!strcmp(buffer, "ListBox")) {
          /* in the listoboxes is interesting current position */
          if (p->bSave) {
            DWORD nCurSel = ListBox_GetCurSel(hWindow);
            if (!SetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &nCurSel, sizeof(nCurSel), REG_DWORD)) {
              return FALSE;
            }
          } else {
            DWORD nCurSel;
            if (GetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &nCurSel, sizeof(nCurSel), REG_DWORD)) {
              ListBox_SetCurSel(hWindow, nCurSel);
            }
          }
        } else if (!strcmp(buffer, "ComboBox")) {
          /* from combo boxes we need to get a current position,
             but also text from cb's edit control */
          if (p->bSave) {
            DWORD nCurSel = ComboBox_GetCurSel(hWindow);
            DWORD dwSize  = ComboBox_GetTextLength(hWindow);
            CMem  buf(dwSize+sizeof(nCurSel)+1);
            *(reinterpret_cast<LPDWORD>(buf.p)) = nCurSel;
            if (!GetWindowText(hWindow, buf.p+sizeof(DWORD), buf.s)) {
              *(buf.p+sizeof(DWORD)) = 0;
            }
            if (!SetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              buf.p, buf.s, REG_BINARY)) {
              return FALSE;
            }
          } else {
            LPCSTR lpId  = i2s(GetDlgCtrlID(hWindow));
            DWORD dwSize = GetKeySz(HKEY_CURRENT_USER, p->lpszRegPath, lpId, REG_BINARY);
            if (dwSize) {
              CMem buf(dwSize);
              if (GetKey(HKEY_CURRENT_USER, p->lpszRegPath, lpId, buf.p, buf.s, REG_BINARY)) {
                ComboBox_SetCurSel(hWindow, *(reinterpret_cast<LPDWORD>(buf.p)));
                ComboBox_SetText(hWindow, buf.p+sizeof(DWORD));
              }
            }
          }
        } else if (!strcmp(buffer, "SysIPAddress32")) {
          /* from this control we need only one dword - ip address */
          if (p->bSave) {
            DWORD dwIp = 0;
            SendMessage(hWindow, IPM_GETADDRESS, 0,
            reinterpret_cast<LPARAM>(&dwIp));
            if (!SetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &dwIp, sizeof(dwIp), REG_DWORD)) {
              return FALSE;
            }
          } else {
            DWORD dwIp;
            if (GetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &dwIp, sizeof(dwIp), REG_DWORD)) {
              SendMessage(hWindow, IPM_SETADDRESS,
              0, dwIp);
            }
          }
        } else if (!strcmp(buffer, HOTKEY_CLASS)) {
          if (p->bSave) {
            DWORD dwHKey = SendMessage(hWindow, HKM_GETHOTKEY, 0, 0L);
            if (!SetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &dwHKey, sizeof(dwHKey), REG_DWORD)) {
              return FALSE;
            }
          } else {
            DWORD dwHKey;
            if (GetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &dwHKey, sizeof(dwHKey), REG_DWORD)) {
              SendMessage(hWindow, HKM_SETHOTKEY,
              dwHKey, 0L);
            }
          }
        } else if (!strcmp(buffer, "SysDateTimePick32")) {
          /* data from this control we will save as binary data
             because systemtime struct is not a standard registry
             date-type */
          SYSTEMTIME pST;
          if (p->bSave) {
            memset(&pST, sizeof(pST), 0);
            DateTime_GetSystemtime(hWindow, &pST);
            if (!SetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &pST, sizeof(pST), REG_BINARY)) {
              return FALSE;
            }
          } else {
            if (GetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &pST, sizeof(pST), REG_BINARY)) {
              DateTime_SetSystemtime(hWindow, GDT_VALID, &pST);
            }
          }
        } else if (!strcmp(buffer, "SysTabControl32")) {
          /* in the tab control is interesting active tab */
          if (p->bSave) {
            DWORD nCurSel = SendMessage(hWindow, TCM_GETCURSEL, 0, 0L);
            if (!SetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &nCurSel, sizeof(nCurSel), REG_DWORD)) {
              return FALSE;
            }
          } else {
            DWORD nCurSel;
            if (GetKey(HKEY_CURRENT_USER, p->lpszRegPath,
              i2s(GetDlgCtrlID(hWindow)),
              &nCurSel, sizeof(nCurSel), REG_DWORD)) {
              SendMessage(hWindow, TCM_SETCURSEL, nCurSel, 0L);
            }
          }
        }
      }
    }
    return TRUE;
  }
  /*
    function that read data from the registry. hKeyRoot is a root key as HKEY_CURRENT_USER, _CLASSES_ROOT, ...
    lpRegPath is path to the registry, next parameters are easy to understand ...
  */
  static BOOL GetKey(HKEY hKeyRoot, LPCSTR lpRegPath, LPCSTR lpKey, LPVOID lpData, DWORD dwSize, DWORD dwType = REG_BINARY) {
    HKEY  hKey;
    if (RegOpenKeyEx(hKeyRoot, lpRegPath, 0, KEY_READ, &hKey)!=ERROR_SUCCESS) return FALSE;
    if (RegQueryValueEx(hKey, lpKey, NULL,
      reinterpret_cast<LPDWORD>(&dwType),
      reinterpret_cast<LPBYTE>(lpData),
      reinterpret_cast<LPDWORD>(&dwSize))!=ERROR_SUCCESS) {
      RegCloseKey(hKey);
      return FALSE;
    }
    RegCloseKey(hKey);
    return TRUE;
  }
  /*
    in some cases we need to know size of data that we want to read from registry. if we use function
    to get data from the registry, but we set buffer size to 0, function return size of the key
    that we want to get data from
  */
  static DWORD GetKeySz(HKEY hKeyRoot, LPCSTR lpRegPath, LPCSTR lpKey, DWORD dwType = REG_BINARY) {
    HKEY  hKey;
    if (RegOpenKeyEx(hKeyRoot, lpRegPath, 0, KEY_READ, &hKey)!=ERROR_SUCCESS) return 0;
    DWORD dwSize = 0;
    switch (RegQueryValueEx(hKey, lpKey, NULL,
      reinterpret_cast<LPDWORD>(&dwType),
      reinterpret_cast<LPBYTE>(&dwSize),
      reinterpret_cast<LPDWORD>(&dwSize))) {
    case ERROR_MORE_DATA:
      RegCloseKey(hKey);
      return dwSize;
    default:
      RegCloseKey(hKey);
      return 0;
    }
  }
  /*
    function that saves data to the registry. hKeyRoot is a root key as HKEY_CURRENT_USER, _CLASSES_ROOT, ...
    lpRegPath is path to the registry, next parameters are easy to understand ...
  */
  static BOOL SetKey(HKEY hKeyRoot, LPCSTR lpRegPath, LPCSTR lpKey, LPVOID lpData, DWORD dwSize, DWORD dwType = REG_BINARY) {
    DWORD dwDisposition;
    HKEY  hKey;
    if (RegOpenKeyEx(hKeyRoot, lpRegPath, 0, KEY_WRITE, &hKey)!=ERROR_SUCCESS) {
      if (RegCreateKeyEx(hKeyRoot, lpRegPath, 0, NULL, REG_OPTION_NON_VOLATILE,
        KEY_WRITE, NULL, &hKey, &dwDisposition)!=ERROR_SUCCESS) {
        return FALSE;
      }
    }
    if (RegSetValueEx(hKey, lpKey, 0, dwType, reinterpret_cast<BYTE*>(lpData), dwSize)!=ERROR_SUCCESS) {
      RegCloseKey(hKey);
      return FALSE;
    }
    RegCloseKey(hKey);
    return TRUE;
  }
  /*
    simple function to convert unsigned int to string.
    this function is used to generate key names from
    the ID's of the dialog controls.
  */
  static LPCSTR i2s(UINT nNumber) {
    static char szn[34];
    return _itoa(nNumber, szn, 16);
  }
  HWND hWindow;
};

#endif // __DLGSET_H__
