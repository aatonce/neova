// VideoConvert Version 3.0\n\nVideo analyzing framework demonstration program.

// VideoConvertDoc.h : interface of the CVideoConvertDoc class
//


#pragma once

#include <atlbase.h>
#include <dshow.h>
#include <qedit.h>
#include "stdio.h"

// Some code in "vector" generate this warning and is bothering :-(
#pragma warning(disable : 4995)
#include "vector"
#pragma warning(default : 4995)

#define FRAME_DIFF_BUFFER_SIZE 50

typedef signed   char                    int8;
typedef unsigned char                    uint8;
typedef signed   short                   int16;
typedef unsigned short                   uint16;
typedef signed   int                     int32;
typedef unsigned int                     uint32;
typedef signed __int64                   int64;
typedef unsigned __int64                 uint64;
#define     MK_UINT64( HI32, LO32 )                  ((uint64(HI32)<<32)|uint64(LO32))
#define     NV_MOVIE_CVER     MK_UINT64( '____', 'MV01' )


#define sEndian(X) ((0xff & (X>>8)) | ((0xff & X) << 8))
#define lEndian(X) (((X >> 24) & 0xff) | ((X >> 8) & 0xff00) | ((X & 0xff00) << 8) | ((X & 0xff) << 24))
typedef std::vector<int16>			Int16A;


class CVideoConvertDoc;

// -------------------------------------------------

// SVideoProcessAdapter is only for providing a callback for ISampleGrabber.
class SVideoProcessAdapter : public ISampleGrabberCB
{
public:
	SVideoProcessAdapter(CVideoConvertDoc *pVideoConvertDoc):m_pVideoConvertDoc(pVideoConvertDoc){}
public:
	virtual ~SVideoProcessAdapter(void){}

public:
	STDMETHOD(BufferCB)(double SampleTime, BYTE *pBuffer, long nBufferLen);

	STDMETHOD(SampleCB)(double, IMediaSample *){return S_OK;}

	STDMETHOD( QueryInterface )( REFIID iid, LPVOID *ppv )
	{
		if( iid == IID_ISampleGrabberCB || iid == IID_IUnknown )
		{ 
			*ppv = (void *) static_cast<ISampleGrabberCB*>( this );
			return NOERROR;
		} 
		return E_NOINTERFACE;
	}

	STDMETHOD_( ULONG, AddRef )(){return 2;}
	STDMETHOD_( ULONG, Release)(){return 1;}

protected:
	CVideoConvertDoc *m_pVideoConvertDoc;
};

// -------------------------------------------------

class SAudioProcessAdapter : public ISampleGrabberCB
{
public:
	SAudioProcessAdapter(CVideoConvertDoc *pVideoConvertDoc):m_pVideoConvertDoc(pVideoConvertDoc){}
public:
	virtual ~SAudioProcessAdapter(void){}

public:
	STDMETHOD(BufferCB)(double SampleTime, BYTE *pBuffer, long nBufferLen);

	STDMETHOD(SampleCB)(double, IMediaSample *);

	STDMETHOD( QueryInterface )( REFIID iid, LPVOID *ppv )
	{
		if( iid == IID_ISampleGrabberCB || iid == IID_IUnknown )
		{ 
			*ppv = (void *) static_cast<ISampleGrabberCB*>( this );
			return NOERROR;
		} 
		return E_NOINTERFACE;
	}

	STDMETHOD_( ULONG, AddRef )(){return 2;}
	STDMETHOD_( ULONG, Release)(){return 1;}


protected:
	CVideoConvertDoc *m_pVideoConvertDoc;
};

// -------------------------------------------------


union Sample
{
	struct {
		int16	l;				// left channel
		int16	r;				// right channel
	};
	uint32		lr;
	int16		s[2];
};

typedef std::vector<Sample>		Samples;


struct Marker
{
	uint32		id;
	uint16		t;
	CString		label;
	CString		note;
};
typedef std::vector<Marker>		Markers;


// PCM
// 16bits signed stereo interleaved <left,right>

struct Pcm
{
	uint16			freq;			//   0 => invalid
	float			loopPoint;		// < 0 => no loop point
	Samples			samples;
	Markers			markers;

	bool IsMono	(			)
	{
		for( unsigned long i=0 ; i<samples.size() ; i++ ) {
			if( samples[i].l != samples[i].r )
				return FALSE;
		}
		return TRUE;
	}

	void GetLeft	(	Int16A&			outSamples		)
	{
		outSamples.resize( samples.size() );
		for( unsigned long i=0 ; i<samples.size() ; i++ )
			outSamples[i] = samples[i].l;
	}



};

struct RIFFHeader {					// 3 fields IFF header !
	uint32		chunkID;			// 'RIFF'
	uint32		chunkSize;
	uint32		formType;			// 'WAVE'
} ;

struct FormatChunk {				
	uint32		chunkID;			// 'fmt '
	uint32		chunkSize;
	uint16		waveFmtTag;			// WAVE_FileEncodingTags mask
	uint16		channel;			// <Channels> 1 = mono, 2 = stereo
	uint32		samplesPerSec;		// <Sample rate>  Samples per second: e.g., 44100
	uint32		bytesPerSec;		// <bytes/second> sample rate * block align
	uint16		blockSize;			// <block align>  channels * bits/sample / 8
	uint16		bitsPerSample;		// <bits/sample> 8 or 16
};

struct DataChunk {
	uint32		chunkID;			// 'data'
	uint32		chunkSize;
	//	uchar		waveformData[];
};



// --------------------------------------------------

class CVideoConvertView;



class CVideoConvertDoc : public CDocument
{
	friend class CVideoConvertView;

	// SVideoProcessAdapter is only for providing a callback for DirectShow.
	// All data are actually stored and processed in CVideoConvertDoc,
	// so make SVideoProcessAdapter a friend for convenience
	friend class SVideoProcessAdapter;
	friend class SAudioProcessAdapter;

protected: // create from serialization only
	CVideoConvertDoc();
	DECLARE_DYNCREATE(CVideoConvertDoc)

// Attributes
public:
	enum GraphStutas
	{
		GRAPH_NONE,
		GRAPH_STOPPED,
		GRAPH_RUNNING,
		GRAPH_PAUSED,
		GRAPH_PAUSEPENDING,	// Pause action has fired but not finished yet
		GRAPH_STOPPENDING	// Stop action has fired but not finished yet
	};

protected:
	CVideoConvertView *m_pVideoConvertView;
	HANDLE m_hUpdateEvent;

	// DirectShow objects needed to extract frames
	IGraphBuilder *m_pGraph;
	IGraphBuilder *m_pGraphSound;
	IBaseFilter *m_pGrabberFilter;
	IBaseFilter *m_pGrabberFilterSound;
	ISampleGrabber *m_pGrabber;
	ISampleGrabber *m_pGrabberSound;
	IBaseFilter *m_pSrcFilter;
	IBaseFilter *m_pSrcFilterSound;
	IBaseFilter *m_pNullRenderer;
	IMediaControl *m_pControl;
	IMediaEventEx *m_pEvent;
	SVideoProcessAdapter m_VideoProcessAdapter;
	SAudioProcessAdapter m_AudioProcessAdapter;

	// The processing status
	GraphStutas m_GraphStatus;

	// Data for processing frames
	REFERENCE_TIME m_lDuration;
	REFERENCE_TIME m_lTimeperFrame;
	REFERENCE_TIME m_nTotalFrames;
	unsigned int m_nCurFrame;	// Frame no that is currently processing
	unsigned int m_nExportedFrame;	// Frame currently exported
	BITMAPINFOHEADER m_Bih;	// info header of frames of the video
	BITMAPFILEHEADER m_Bfh;	// File header of frames of the video

	// --- Header NV Movies

	struct _nv_header
	{
		uint64   ver;
		uint32  flags;
		uint16  width;
		uint16  height;
		uint32  fcount;
		uint32	fmaxbsize;        // byte-size de la plus grosse jpeg (pour optimiser le streaming)
		float   frate;
		float	duration;
	};

	// --- Frame NV Movies

	struct _nv_frame
	{
		float       time;             // temps t de pr�sentation de l�image
		uint32		boffset;          // byte-offset de la jpeg par rapport au d�but du fichier archive
		//uint32		bsize;            // byte-size de la jpeg compl�te
	};

	struct _nv_header nv_header;
	CArray <_nv_frame, _nv_frame&> nv_frames;

	CString		inputfile;
	CString		inputpath;
	FILE*		tmpfile;
	FILE*		outfile;
	FILE*		wavfile;

	Pcm			SndOut;

	DWORD		Soudfreq;
	WORD		SoundnbChannel;
	WORD		SoundbitsPerChannel;
	WORD		SoundbitsPerSample;

// Operations
public:

	bool WavWriteFile	(Pcm& inPcm);

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CVideoConvertDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	// This function is called when a frame is extracted by DirectShow.
	// Note that the function will be called from another thread, 
	// so pay special attention to cooperations among threads!
	HRESULT ProcessVideoFrame(double SampleTime, BYTE *pBuffer, long nBufferLen);

	//HRESULT ProcessAudioFrame(double SampleTime, BYTE *pBuffer, long nBufferLen);
	HRESULT ProcessAudioFrame(double SampleTime, IMediaSample *pSample);

	//// Wait until analyzing ends or pauses
	//static UINT __cdecl WaitProc(CVideoConvertDoc * pThis);

	// Helpers building the graph
	HRESULT ConnectFilters(IGraphBuilder *pGraph, IBaseFilter *pFirst, IBaseFilter *pSecond);
	HRESULT GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin);

	//BOOL Bitmap2JPG(HGDIOBJ hBitmap, LPCTSTR pszFileJPG, int nQualite);
	BOOL Bitmap2JPG(HBITMAP hBitmap, LPCTSTR pszFileJPG, int nQualite);

	// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:

	CString GetFileOnly(LPCTSTR Path);
	CString GetFolderOnly(LPCTSTR Path);
	CString RenameFileExt(LPCTSTR Path, LPCTSTR Ext);

	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	afx_msg void OnUpdateConvertStart(CCmdUI *pCmdUI);
	afx_msg void OnUpdateConvertPause(CCmdUI *pCmdUI);
	afx_msg void OnUpdateConvertStop(CCmdUI *pCmdUI);
	afx_msg void OnConvertStart();
	afx_msg void OnConvertPause();
	afx_msg void OnConvertStop();
	afx_msg void OnConvertFinished();
	void OnGraphNotify();
};


