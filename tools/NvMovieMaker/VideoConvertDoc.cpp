// VideoConvert Version 3.0\n\nVideo analyzing framework demonstration program.

// VideoConvertDoc.cpp : implementation of the CVideoConvertDoc class
//

#include "stdafx.h"
#include "VideoConvert.h"

#include "VideoConvertDoc.h"
#include "VideoConvertView.h"

#define HAVE_BOOLEAN

#include "Jpegfile.h"

// -----


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define SAFE_RELEASE(x) { if (x) x->Release(); x = NULL; }

STDMETHODIMP SVideoProcessAdapter::BufferCB(double SampleTime, BYTE *pBuffer, long nBufferLen)
{
	return m_pVideoConvertDoc->ProcessVideoFrame(SampleTime, pBuffer, nBufferLen);
}

STDMETHODIMP SAudioProcessAdapter::BufferCB(double SampleTime, BYTE *pBuffer, long nBufferLen)
{
	return S_OK;
}

STDMETHODIMP SAudioProcessAdapter::SampleCB( double SampleTime, IMediaSample *pSample )
{
	return m_pVideoConvertDoc->ProcessAudioFrame(SampleTime, pSample);
}


// CVideoConvertDoc

IMPLEMENT_DYNCREATE(CVideoConvertDoc, CDocument)

BEGIN_MESSAGE_MAP(CVideoConvertDoc, CDocument)
	ON_UPDATE_COMMAND_UI(ID_CONVERT_START, &CVideoConvertDoc::OnUpdateConvertStart)
	ON_UPDATE_COMMAND_UI(ID_CONVERT_PAUSE, &CVideoConvertDoc::OnUpdateConvertPause)
	ON_UPDATE_COMMAND_UI(ID_CONVERT_STOP, &CVideoConvertDoc::OnUpdateConvertStop)
	ON_COMMAND(ID_CONVERT_START, &CVideoConvertDoc::OnConvertStart)
	ON_COMMAND(ID_CONVERT_PAUSE, &CVideoConvertDoc::OnConvertPause)
	ON_COMMAND(ID_CONVERT_STOP, &CVideoConvertDoc::OnConvertStop)
	ON_COMMAND(ID_CONVERT_FINISHED, &CVideoConvertDoc::OnConvertFinished)
END_MESSAGE_MAP()


// CVideoConvertDoc construction/destruction

CVideoConvertDoc::CVideoConvertDoc()
: m_pGraph(NULL)
, m_pGrabberFilter(NULL)
, m_pGrabber(NULL)
, m_pGraphSound(NULL)
, m_pGrabberFilterSound(NULL)
, m_pGrabberSound(NULL)
, m_pControl(NULL)
, m_pEvent(NULL)
, m_GraphStatus(GRAPH_NONE)
, m_pSrcFilter(NULL)
, m_pNullRenderer(NULL)
, m_VideoProcessAdapter(this)
, m_AudioProcessAdapter(this)
, m_nCurFrame(0)
,m_nExportedFrame(0)
{
	memset(&m_Bfh, 0, sizeof(m_Bfh));
	m_Bfh.bfType = 0x4d42;
	m_Bfh.bfOffBits = sizeof(m_Bfh) + sizeof(BITMAPINFOHEADER);
	m_hUpdateEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	memset(&m_Bih, 0, sizeof(m_Bih));

	tmpfile=NULL;			/* target file */
}

CVideoConvertDoc::~CVideoConvertDoc()
{
	//ClearAll();
}

BOOL CVideoConvertDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CVideoConvertDoc serialization

void CVideoConvertDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CVideoConvertDoc diagnostics

#ifdef _DEBUG
void CVideoConvertDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CVideoConvertDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


CString CVideoConvertDoc::GetFileOnly(LPCTSTR Path)
{
	// Strip off the path and return just the filename part
	CString temp = (LPCTSTR) Path; // Force CString to make a copy
	::PathStripPath(temp.GetBuffer(0));
	temp.ReleaseBuffer(-1);
	return temp;
}

CString CVideoConvertDoc::GetFolderOnly(LPCTSTR Path)
{
	// Strip off the file name so we can direct the file scanning dialog to
	// back to the same directory as before.
	CString temp = (LPCTSTR) Path; // Force CString to make a copy
	::PathRemoveFileSpec(temp.GetBuffer(0));
	::PathAddBackslash(temp.GetBuffer(_MAX_PATH));
	temp.ReleaseBuffer(-1);
	return temp;
}

CString CVideoConvertDoc::RenameFileExt(LPCTSTR Path, LPCTSTR Ext)
{
	CString cs = Path;
	::PathRenameExtension(cs.GetBuffer(_MAX_PATH), Ext);
	return cs;
}


// CVideoConvertDoc commands

BOOL CVideoConvertDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	inputfile = GetFileOnly(lpszPathName);
	inputpath = GetFolderOnly(lpszPathName);

	m_GraphStatus = GRAPH_STOPPED;

	POSITION ViewPos = GetFirstViewPosition();
	m_pVideoConvertView = (CVideoConvertView *)GetNextView(ViewPos);
	m_pVideoConvertView->m_PreviewBox.SetDibBits(NULL, 0);

	m_pVideoConvertView->RedrawWindow();
	m_pVideoConvertView->ClearAll();

	return TRUE;
}

void CVideoConvertDoc::OnUpdateConvertStart(CCmdUI *pCmdUI)
{
	if(GRAPH_STOPPED == m_GraphStatus)
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}

void CVideoConvertDoc::OnUpdateConvertPause(CCmdUI *pCmdUI)
{
	if(GRAPH_RUNNING == m_GraphStatus || GRAPH_PAUSED == m_GraphStatus)
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}

void CVideoConvertDoc::OnUpdateConvertStop(CCmdUI *pCmdUI)
{
	if(GRAPH_RUNNING == m_GraphStatus || GRAPH_PAUSED == m_GraphStatus)
		pCmdUI->Enable();
	else
		pCmdUI->Enable(FALSE);
}

void CVideoConvertDoc::OnConvertStart()
{
	ASSERT(GRAPH_STOPPED == m_GraphStatus);

	// Create the graph builder
	HRESULT hr = ::CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER,
		IID_IGraphBuilder, (void**)(&m_pGraph));
	if (FAILED(hr))
	{
		AfxMessageBox("Failed creating DirectShow objects!");
		return;
	}

	// Create the Sample Grabber
	ASSERT(m_pGrabber == NULL);
	hr = CoCreateInstance(CLSID_SampleGrabber, NULL, CLSCTX_INPROC_SERVER,
		IID_IBaseFilter, (void **)(&m_pGrabberFilter));
	hr = m_pGrabberFilter->QueryInterface(IID_ISampleGrabber,
		(void **)(&m_pGrabber));
	hr = m_pGraph->AddFilter(m_pGrabberFilter, L"SampleGrabber");

	// Set the media type
	AM_MEDIA_TYPE mt;
	ZeroMemory(&mt, sizeof(AM_MEDIA_TYPE));
	mt.formattype = FORMAT_VideoInfo; 
	mt.majortype = MEDIATYPE_Video;
	mt.subtype = MEDIASUBTYPE_RGB24;	// only accept 24-bit bitmaps
	hr = m_pGrabber->SetMediaType(&mt);

	// Create the src filter
	wchar_t strFilename[MAX_PATH];
	MultiByteToWideChar(CP_ACP, 0, m_strPathName, -1, strFilename, MAX_PATH);
	hr = m_pGraph->AddSourceFilter(strFilename, L"Source", &m_pSrcFilter);
	if(FAILED(hr))
	{
		AfxMessageBox("Unsupported media type!");
		return;
	}

	// Connect the src and grabber
	hr = ConnectFilters(m_pGraph, m_pSrcFilter, m_pGrabberFilter);
	if(FAILED(hr))
	{
		SAFE_RELEASE(m_pSrcFilter);
		SAFE_RELEASE(m_pGrabber);
		SAFE_RELEASE(m_pGrabberFilter);
		SAFE_RELEASE(m_pGraph);
		AfxMessageBox("Unsupported media type!");
		return;
	}

	// --- Sound grabber start ---

	if (m_pVideoConvertView->m_exportWav.GetCheck())
	{

		// Create the graph builder
		/*hr = ::CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER,
			IID_IGraphBuilder, (void**)(&m_pGraphSound));
		if (FAILED(hr))
		{
			AfxMessageBox("Failed creating DirectShow objects!");
			return;
		}*/

		// Create the Sound Sample Grabber
		ASSERT(m_pGrabberSound == NULL);
		hr = CoCreateInstance(CLSID_SampleGrabber, NULL, CLSCTX_INPROC_SERVER,
			IID_IBaseFilter, (void **)(&m_pGrabberFilterSound));
		hr = m_pGrabberFilterSound->QueryInterface(IID_ISampleGrabber,
			(void **)(&m_pGrabberSound));
		hr = m_pGraph->AddFilter(m_pGrabberFilterSound, L"SampleGrabber");

		// Set the media type
		AM_MEDIA_TYPE mtSound;
		ZeroMemory(&mtSound, sizeof(AM_MEDIA_TYPE));
		mtSound.majortype = MEDIATYPE_Audio;
		mtSound.formattype = FORMAT_WaveFormatEx;
		mtSound.subtype = MEDIASUBTYPE_PCM;	
		hr = m_pGrabberSound->SetMediaType(&mtSound);
		if( FAILED(hr) ) {
			AfxMessageBox("Unsupported Sound media type!");
			return ;
		}


		// Create the src filter
		wchar_t strFilenameSound[MAX_PATH];
		MultiByteToWideChar(CP_ACP, 0, m_strPathName, -1, strFilenameSound, MAX_PATH);
		hr = m_pGraph->AddSourceFilter(strFilenameSound, L"Source", &m_pSrcFilterSound);
		if(FAILED(hr))
		{
			AfxMessageBox("Unsupported media type!");
			return;
		}

		// Connect the src and grabber
		hr = ConnectFilters(m_pGraph, m_pSrcFilterSound, m_pGrabberFilterSound);
		if(FAILED(hr))
		{
			SAFE_RELEASE(m_pSrcFilterSound);
			SAFE_RELEASE(m_pGrabberSound);
			SAFE_RELEASE(m_pGrabberFilterSound);
			SAFE_RELEASE(m_pGraphSound);
			AfxMessageBox("Unsupported Sound media type!");
			return;
		}

		// check output wav format
		ZeroMemory(&mtSound, sizeof(AM_MEDIA_TYPE));
		hr = m_pGrabberSound->GetConnectedMediaType( &mtSound );
		if( FAILED(hr) || mtSound.formattype!=FORMAT_WaveFormatEx ) {
			AfxMessageBox("Unsupported Sound media type!");
			return ;
		}

		ASSERT( mtSound.cbFormat >= sizeof(WAVEFORMATEX) );
		WAVEFORMATEX* fmt = (WAVEFORMATEX*) mtSound.pbFormat;
		ASSERT( fmt->wFormatTag == WAVE_FORMAT_PCM );
		SndOut.freq				=	Soudfreq				= fmt->nSamplesPerSec;
		SoundnbChannel			= fmt->nChannels;
		SoundbitsPerChannel		= fmt->wBitsPerSample;
		SoundbitsPerSample		= fmt->nChannels * fmt->wBitsPerSample;
		SndOut.loopPoint		= -1.f;
		SndOut.samples.clear();
		SndOut.markers.clear();

		// Set modes
		m_pGrabberSound->SetBufferSamples(TRUE);	// Buffer seems to be no use in callback mode
		m_pGrabberSound->SetCallback(&m_AudioProcessAdapter, 0);
	}

	// --- Sound grabber end ---


	// Create the NULL renderer and connect
	m_pNullRenderer = NULL;
	hr = CoCreateInstance(CLSID_NullRenderer, NULL, CLSCTX_INPROC_SERVER,
		IID_IBaseFilter, (void **)(&m_pNullRenderer));
	hr = m_pGraph->AddFilter(m_pNullRenderer, L"NullRenderer");
	hr = ConnectFilters(m_pGraph, m_pGrabberFilter, m_pNullRenderer);

	m_nCurFrame = 0;
	m_nExportedFrame = 0;

	// Set modes
	m_pGrabber->SetBufferSamples(FALSE);	// Buffer seems to be no use in callback mode
	m_pGrabber->SetCallback(&m_VideoProcessAdapter, 1);

	// Necessary interfaces for controlling
	m_pGraph->QueryInterface(IID_IMediaControl, (void **)(&m_pControl));
	m_pGraph->QueryInterface(IID_IMediaEventEx, (void **)(&m_pEvent));

	m_pEvent->SetNotifyWindow((OAHWND)m_pVideoConvertView->m_hWnd, WM_APP_GRAPHNOTIFY, 0);

	// Turn off the sync clock for max speed
	IMediaFilter *pMediaFilter = NULL;
	m_pGraph->QueryInterface(IID_IMediaFilter, reinterpret_cast<void**>(&pMediaFilter));
	pMediaFilter->SetSyncSource(NULL);
	SAFE_RELEASE(pMediaFilter);

	// Retrieve the actual media type
	ZeroMemory(&mt, sizeof(mt));
	hr = m_pGrabber->GetConnectedMediaType(&mt);
	VIDEOINFOHEADER *pVih;
	if (mt.formattype == FORMAT_VideoInfo) 
		pVih = reinterpret_cast<VIDEOINFOHEADER*>(mt.pbFormat);
	else 
	{
		SAFE_RELEASE(m_pControl);
		SAFE_RELEASE(m_pEvent);
		SAFE_RELEASE(m_pSrcFilter);
		SAFE_RELEASE(m_pNullRenderer);
		SAFE_RELEASE(m_pGrabber);
		SAFE_RELEASE(m_pGrabberFilter);
		SAFE_RELEASE(m_pGraph);
		AfxMessageBox("No video stream found!");
		return; // Something went wrong, perhaps not appropriate media type
	}

	// Save the video info header
	memcpy(&m_Bih, &pVih->bmiHeader, sizeof(m_Bih));
	m_Bfh.bfSize = sizeof(m_Bfh) + sizeof(BITMAPINFOHEADER) + m_Bih.biSizeImage;
	m_pVideoConvertView->m_PreviewBox.SetBih(&m_Bih);
	m_lTimeperFrame = pVih->AvgTimePerFrame;

	// Free the media type
	if (mt.cbFormat != 0)
	{
		CoTaskMemFree((PVOID)mt.pbFormat);
		// Strictly unnecessary but tidier
		mt.cbFormat = 0;
		mt.pbFormat = NULL;
	}
	if (mt.pUnk != NULL)
	{
		// Unnecessary because pUnk should not be used, but safest.
		mt.pUnk->Release();
		mt.pUnk = NULL;
	}

	// Get video info
	IMediaSeeking *pSeeking = NULL;
	m_pGraph->QueryInterface(IID_IMediaSeeking, (void **)(&pSeeking));
	pSeeking->GetDuration(&m_lDuration);
	if(FAILED(pSeeking->SetTimeFormat(&TIME_FORMAT_FRAME)))
		m_nTotalFrames = m_lDuration / m_lTimeperFrame;
	else
		pSeeking->GetDuration(&m_nTotalFrames);
	SAFE_RELEASE(pSeeking);

	m_pVideoConvertView->ClearAll();
	m_pVideoConvertView->m_ConvertProgress.SetRange32(0, (int)m_nTotalFrames);

	// Setup the view
	m_pVideoConvertView->SetTimer(ID_TIMER_EVENT_UPDATE, 500, NULL);
	SetEvent(m_hUpdateEvent);

	m_GraphStatus = GRAPH_RUNNING;
	m_pControl->Run(); // Run the graph to start the analyzing process!
	
	// --- Init NVMovie header

	nv_header.ver = NV_MOVIE_CVER;
	nv_header.flags = 0;
	nv_header.fmaxbsize	= 0;

	nv_frames.RemoveAll();
	
	// --- Open the tmp NVMovie to append 

	CString strNvMovieFilename = inputpath + RenameFileExt(inputfile,".tmp");
	if ((tmpfile = fopen(strNvMovieFilename, "wb+")) == NULL) {
		char buf[250];
		sprintf(buf, "Can't create %s\n", strNvMovieFilename);
		AfxMessageBox(buf);
		return;
	}

}

void CVideoConvertDoc::OnConvertPause()
{
	ASSERT(GRAPH_RUNNING == m_GraphStatus || GRAPH_PAUSED == m_GraphStatus);
	if(GRAPH_RUNNING == m_GraphStatus)
	{
		if(S_FALSE == m_pControl->Pause())
		{
			OAFilterState oState;
			m_pControl->GetState(INFINITE, &oState);
		}
		m_GraphStatus = GRAPH_PAUSED;
	}
	else
	{
		m_pControl->Run();
		m_GraphStatus = GRAPH_RUNNING;
	}

}

void CVideoConvertDoc::OnConvertStop()
{
	ASSERT(GRAPH_RUNNING == m_GraphStatus || GRAPH_PAUSED == m_GraphStatus);

	if(GRAPH_RUNNING == m_GraphStatus && S_FALSE == m_pControl->Pause())
	{
		OAFilterState oState;
		m_pControl->GetState(INFINITE, &oState);	// Wait until pause finished
		TRACE("Wait to paused\n");
	}
	m_pControl->Stop();
	m_GraphStatus = GRAPH_STOPPED;

	OnConvertFinished();

	SAFE_RELEASE(m_pControl);
	SAFE_RELEASE(m_pEvent);
	SAFE_RELEASE(m_pSrcFilter);
	SAFE_RELEASE(m_pNullRenderer);
	SAFE_RELEASE(m_pGrabber);
	SAFE_RELEASE(m_pGrabberFilter);

	SAFE_RELEASE(m_pSrcFilterSound);
	SAFE_RELEASE(m_pGrabberSound);
	SAFE_RELEASE(m_pGrabberFilterSound);
	SAFE_RELEASE(m_pGraphSound);

	SAFE_RELEASE(m_pGraph);

}


// ----------------------

bool CVideoConvertDoc::WavWriteFile	(Pcm& inPcm)
{

	if (inPcm.samples.size() == 0)
	{
		char buf[250];
		sprintf(buf, "No sound!");
		AfxMessageBox(buf);
		return false;
	}

	CString strWavFilename = inputpath + RenameFileExt(inputfile,".wav");
	if ((wavfile = fopen(strWavFilename, "wb")) == NULL) {
		char buf[250];
		sprintf(buf, "Can't create %s\n", strWavFilename);
		AfxMessageBox(buf);
		return FALSE;
	}

	// Header
	struct WavHeader
	{
		RIFFHeader		riff;
		FormatChunk		fmt;
		DataChunk		data;
	} hd;

	bool mono = inPcm.IsMono();
	
	uint32 dataSize = inPcm.samples.size() * (mono?2:4);

	
	hd.riff.chunkID = lEndian('RIFF');
	hd.riff.chunkSize = sizeof(RIFFHeader) + dataSize - 8;
	hd.riff.formType = lEndian('WAVE');
	hd.fmt.chunkID = lEndian('fmt ');
	hd.fmt.chunkSize = 24 - 8;
	hd.fmt.waveFmtTag = WAVE_FORMAT_PCM;
	hd.fmt.channel = mono ? 1 : 2;
	hd.fmt.samplesPerSec = inPcm.freq;
	hd.fmt.bytesPerSec = inPcm.freq * (mono?2:4);
	hd.fmt.blockSize = (mono?2:4);
	hd.fmt.bitsPerSample = 16;
	hd.data.chunkID = lEndian('data');
	hd.data.chunkSize = dataSize;


	fwrite(&hd,sizeof(hd),1,wavfile);

	if( mono ) 
	{
		Int16A	sa;
		inPcm.GetLeft( sa );
		fwrite(&sa[0],dataSize,1,wavfile);
	} 
	else 
	{
		fwrite(&inPcm.samples[0],dataSize,1,wavfile);
	}

	fclose(wavfile);


	return TRUE;
}



void CVideoConvertDoc::OnConvertFinished()
{
	// TODO: Add your command handler code here
	ASSERT(GRAPH_STOPPED == m_GraphStatus || GRAPH_RUNNING == m_GraphStatus);

	// --- Fill nv_movie Header

	nv_header.width = (uint16) m_Bih.biWidth;
	nv_header.height = (uint16) m_Bih.biHeight;
	nv_header.fcount = m_nExportedFrame;
	nv_header.frate = nv_header.fcount/nv_header.duration;


	m_pVideoConvertView->KillTimer(ID_TIMER_EVENT_UPDATE);
	m_GraphStatus = GRAPH_STOPPED;

	// Free DirectShow resources when finished analyzing
	SAFE_RELEASE(m_pControl);
	SAFE_RELEASE(m_pEvent);
	SAFE_RELEASE(m_pSrcFilter);
	SAFE_RELEASE(m_pGraph);
	SAFE_RELEASE(m_pNullRenderer);
	SAFE_RELEASE(m_pGrabber);
	SAFE_RELEASE(m_pGrabberFilter);

	// 

	if (tmpfile==NULL)
	{
		AfxMessageBox("failed to saved!");
		return;
	}

	CString strNvMovieFilename = inputpath + RenameFileExt(inputfile,".NvM");
	if ((outfile = fopen(strNvMovieFilename, "wb")) == NULL) {
		char buf[250];
		sprintf(buf, "Can't create %s\n", strNvMovieFilename);
		AfxMessageBox(buf);
		return;
	}

	fwrite(&nv_header,sizeof(nv_header),1,outfile);

	for (unsigned int i=0; i<m_nExportedFrame;i++)
	{
		struct _nv_frame thisframe;
		thisframe = nv_frames.GetAt(i);
		thisframe.boffset += (uint32) (sizeof(_nv_header) + (sizeof(_nv_frame)*nv_frames.GetCount()));
		fwrite(&thisframe,sizeof(_nv_frame),1,outfile);
	}

	size_t  numr;
	size_t  numw;
	size_t  posStart = 0;
	char buffer[100];
	rewind (tmpfile);
	while(feof(tmpfile)==0){	
		if((numr=fread(buffer,1,100,tmpfile))!=100){
			if(ferror(tmpfile)!=0){
				fprintf(stderr,"read file error.\n");
				exit(1);
			}
			else if(feof(tmpfile)!=0);
		}
		if((numw=fwrite(buffer,1,numr,outfile))!=numr){
			fprintf(stderr,"write file error.\n");
			exit(1);
		}
	}	

	fclose (tmpfile);
	fclose (outfile);

	if (m_pVideoConvertView->m_exportWav.GetCheck())
	{
		WavWriteFile	(SndOut);
	}

	CString strtmpFilename = inputpath + RenameFileExt(inputfile,".tmp");
	DeleteFile(strtmpFilename);

	CString strMsg;
	strMsg.Format("%d frames exported.", m_nExportedFrame);
	AfxMessageBox(strMsg);
}

// Helper functions:
HRESULT CVideoConvertDoc::GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin)
{
	IEnumPins  *pEnum;
	IPin       *pPin;
	pFilter->EnumPins(&pEnum);
	while(pEnum->Next(1, &pPin, 0) == S_OK)
	{
		PIN_DIRECTION PinDirThis;
		pPin->QueryDirection(&PinDirThis);
		if (PinDir == PinDirThis)
		{
			pEnum->Release();
			*ppPin = pPin;
			return S_OK;
		}
		pPin->Release();
	}
	pEnum->Release();
	return E_FAIL;  
}

HRESULT CVideoConvertDoc::ConnectFilters(IGraphBuilder *pGraph, IBaseFilter *pFirst, IBaseFilter *pSecond)
{
	IPin *pOut = NULL, *pIn = NULL;
	HRESULT hr = GetPin(pSecond, PINDIR_INPUT, &pIn);
	if (FAILED(hr)) return hr;

	// The previous filter may have multiple outputs, so try each one!
	IEnumPins  *pEnum;
	pFirst->EnumPins(&pEnum);
	while(pEnum->Next(1, &pOut, 0) == S_OK)
	{
		PIN_DIRECTION PinDirThis;
		pOut->QueryDirection(&PinDirThis);
		if (PINDIR_OUTPUT == PinDirThis)
		{
			hr = pGraph->Connect(pOut, pIn);
			if(!FAILED(hr))
			{
				break;
			}
		}
		SAFE_RELEASE(pOut);
	}
	SAFE_RELEASE(pOut);
	SAFE_RELEASE(pEnum);
	SAFE_RELEASE(pIn);
	return hr;
}

HRESULT CVideoConvertDoc::ProcessAudioFrame(double SampleTime, IMediaSample *pSample)
{

	BYTE* data;
	LONG bsize = pSample->GetActualDataLength();
	HRESULT hr = pSample->GetPointer( &data );
	if( FAILED(hr) )
		return hr;

	uint32 bytesPerSample = (SoundbitsPerChannel * SoundnbChannel) / 8;
	ASSERT( (bsize%bytesPerSample) == 0 );

	Sample s;

	uint8* p = (uint8*) data;
	uint8* p_end = p + bsize;
	while( p < p_end ) {
		if( SoundbitsPerChannel==8 ) {
			if( SoundnbChannel==1 ) {
				s.l = (int16(p[0])-128) << 8;
				s.r = (int16(p[0])-128) << 8;
			} else {
				s.l = (int16(p[0])-128) << 8;
				s.r = (int16(p[1])-128) << 8;
			}
		} else {
			int16* p16 = (int16*) p;
			if( SoundnbChannel==1 ) {
				s.l = p16[0];
				s.r = p16[0];
			} else {
				s.l = p16[0];
				s.r = p16[1];
			}
		}
		SndOut.samples.push_back( s );
		p += bytesPerSample;
	}

	return S_OK;

}


HRESULT CVideoConvertDoc::ProcessVideoFrame(double SampleTime, BYTE *pBuffer, long nBufferLen)
{
	// Keep in mind that code here is executed within another thread,
	// so do consider the data access problem among threads

	// Here just do nothing but send a preview image and update progress view
	// Comment the following "if" line if you want to see each frame :)
	//if(WAIT_OBJECT_0 == WaitForSingleObject(m_hUpdateEvent, 0) || m_nCurFrame == m_nTotalFrames - 1)
	{
		m_pVideoConvertView->SendMessage(WM_USER_PREVIEW_FRAME, (WPARAM)pBuffer, nBufferLen);
		m_pVideoConvertView->SendMessage(WM_USER_UPDATE_PROGRESS, (WPARAM)m_nCurFrame + 1);
	}

	nv_header.duration = (float) SampleTime;

	//// The following code demonstrates how to save a snapshot to JPEG file for every wanted frame

	if(0 == m_nCurFrame % (EXPORT_FRAME_RATIO_RANGE-m_pVideoConvertView->m_ConvertFrameRate.GetPos()+1))
	{
		fpos_t oldSize;
		fpos_t newSize;

		fgetpos( tmpfile, &oldSize );

		BOOL color = true;
		BOOL ok = JpegFile::VertFlipBuf(pBuffer, m_Bih.biWidth*3, m_Bih.biHeight);
		
		if (m_pVideoConvertView->m_rgbCombo.GetCurSel())
		{
			BOOL ok2 = JpegFile::BGRFromRGB(pBuffer, m_Bih.biWidth, m_Bih.biHeight);
		}

		if (m_pVideoConvertView->m_exportSwizzle.GetCheck())
		{
			BOOL ok4 = JpegFile::SwizzleForWii(pBuffer, m_Bih.biWidth, m_Bih.biHeight);
		}

		BOOL ok3 = JpegFile::AppendRGBToJpegFile(tmpfile, 
			pBuffer,
			m_Bih.biWidth,
			m_Bih.biHeight,
			color, 
			m_pVideoConvertView->m_ConvertCompression.GetPos());			// quality value 1-100.

		fgetpos( tmpfile, &newSize );

		unsigned long sizeNewPic = (unsigned long) (newSize - oldSize);
		if (nv_header.fmaxbsize<sizeNewPic)
			nv_header.fmaxbsize = sizeNewPic;

		struct _nv_frame this_frame;
		this_frame.boffset = (uint32) oldSize;
		this_frame.time = (float) SampleTime;
		nv_frames.Add(this_frame);

		// -- export the Jpeg file

		if (m_pVideoConvertView->m_exportJpeg.GetCheck())
		{
			CString strJPGFile;
			strJPGFile.Format(inputfile+"_%05d.jpg", m_nCurFrame );
			CString strJPGFilename = inputpath + RenameFileExt(strJPGFile,".jpg");
			BOOL ok4 = JpegFile::RGBToJpegFile(strJPGFilename, 
				pBuffer,
				m_Bih.biWidth,
				m_Bih.biHeight,
				color, 
				m_pVideoConvertView->m_ConvertCompression.GetPos());			// quality value 1-100.
		}

		m_nExportedFrame++;
	}

	//// The following code demonstrates how to get rgb values of a specified pixel
	//// You can write a loop to examine all pixels
	//// Keep in mind the pixel data is stored from bottom to top in pBuffer
	//int x = 0;
	//int y = 0;
	//int nLineBytes = (m_Bih.biWidth * 24 + 31) / 32 * 4;	// # of bytes per line
	//BYTE *pLine = pBuffer + (m_Bih.biHeight - y - 1) * nLineBytes;
	//BYTE *pPixel = pLine + 3 * x;
	//BYTE B = *pPixel;
	//BYTE G = *(pPixel + 1);
	//BYTE R = *(pPixel + 2);

	m_nCurFrame++;	// m_nCurFrame indicates which frame is being processed
	return S_OK;
}

void CVideoConvertDoc::OnGraphNotify()
{
	if(!m_pEvent)
		return;

	long lEventCode;
	LONG_PTR lParam1;
	LONG_PTR lParam2;

	while(S_OK == m_pEvent->GetEvent(&lEventCode, &lParam1, &lParam2, 0))
	{
		TRACE("%d\n", lEventCode);
		//if(EC_PAUSED == lEventCode && GRAPH_RUNNING == m_GraphStatus)
		//{
		//}
		if(EC_COMPLETE == lEventCode)	// All data has been rendered.
		{
			if(S_FALSE == m_pControl->Pause())
			{
				OAFilterState oState;
				m_pControl->GetState(INFINITE, &oState);
			}
			m_pControl->Stop();
			//AfxMessageBox("Graph complete");
			AfxGetMainWnd()->PostMessage(WM_COMMAND, ID_CONVERT_FINISHED);
		}

		m_pEvent->FreeEventParams(lEventCode, lParam1, lParam2);
	}
	
}

