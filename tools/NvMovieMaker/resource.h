//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VideoConvert.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_VIDEO_CONVERT_FORM          101
#define IDR_MAINFRAME                   128
#define IDR_VideoConvertTYPE            129
#define IDC_PREVIEWBOX                  1000
#define IDC_CONVERT_PROGRESS            1001
#define IDC_CONVERT_STATUS              1002
#define IDC_SLIDER1                     1012
#define IDC_CONVERT_SLIDER              1012
#define IDC_COMPRESSION_FACTOR          1013
#define IDC_EXPORT_JPG                  1015
#define IDC_RGB_EXPORT                  1016
#define IDC_RGB_COMBO                   1016
#define IDC_SLIDER2                     1017
#define IDC_FRAMERATE_SLIDER            1018
#define IDC_FRAMERATE_TEXT              1019
#define IDC_EXPORT_WAV                  1020
#define IDC_COMBO_PLATFORM              1022
#define IDC_CHECK2                      1023
#define IDC_SWIZZLE                     1023
#define ID_CONVERT_START                32772
#define ID_CONVERT_PAUSE                32773
#define ID_CONVERT_STOP                 32774
#define ID_CONVERT_FINISHED             32775
#define ID_TOOLS_OPTIONS                32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
