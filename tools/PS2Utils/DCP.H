#ifndef	__DCP_h__
#define	__DCP_h__
/* SCEI CONFIDENTIAL
 "PlayStation 2" Artist Tool Sound Previewer Release 1.0
 */
/*
 *	Copyright (C) 2000 Sony Computer Entertainment Inc.
 *	All Right Reserved
 */

/*	DebugStation Control Protocol */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef	macintosh
#include	<Types.h>
#else
typedef unsigned char	UInt8;
typedef unsigned short	UInt16;
typedef unsigned int	UInt32;
#endif	/*macintosh*/

#ifdef	macintosh
#pragma	options	align=mac68k
#endif	/*macintosh*/

typedef struct {
	UInt16	code;		/* Action Code */
	UInt16	id;			/* Resource Identifier */
	UInt32	length;		/* Packet Length (exclude DCP_HDR) */
} DCP_HDR;

#define	DCP_PORT_MONITOR		9000
#define	DCP_PORT_SOUNDCTL		9010

#ifdef	macintosh
#pragma	options	align=reset
#endif	/*macintosh*/


#ifdef __cplusplus
}	/* extern "C" */
#endif

#endif	/*__DCP_h__*/
