/* SCEI CONFIDENTIAL
 "PlayStation 2" Artist Tool Client Release 1.0
 */
/*
 *	Copyright (C) 2000 Sony Computer Entertainment Inc.
 *	All Right Reserved
 */
/* 
 * WinSock version Coded by Jeb, Silicon Dreams Studio Ltd July 2000	
*/

#include <process.h>
#include "direct.h"
#include "stdio.h"
#include "stdlib.h"
#include "sys/types.h"
#include "windows.h"
#include "wtypes.h"
#include "windef.h"
#include "Winsock.h"
#include "dcp_mcp.h"


typedef struct
{
	UInt16 code;
	UInt16 reserved;
	UInt32 length;
} hellopack;

typedef struct
{
	UInt16 code;
	UInt16 reserved;
	UInt32 length;
	UInt32 ident;
} hellorpack;

typedef struct
{
	UInt16 code;
	UInt16 reserved;
	UInt32 length;
	UInt16 type;
	UInt16 argc;
	UInt32 size;
	char filename[400];

}exepack;

typedef struct
{
	UInt16 code;
	UInt16 resv1;
	UInt32 length;
	UInt32 result;
	UInt16 resv2;
} exeret;

typedef struct
{
	UInt16 unknown1;
	UInt16 unknown2;
	UInt16 unknown3;
}	exehdret;

typedef struct
{
	char *IPAddr;
	char *FileName;
} exeargs;

typedef struct
{
	UInt16 code;
	UInt16 resv1;
	UInt32 length;
	UInt16 result;
	UInt16 resv2;
} resetreply;

int makesocket (char *target_hostip);
void silenthello(int s);
void commandexec(void *Args);
void commandreset(void *Args);
void commandkill(void *Args);
void MCP_Init(char *IPAddr);
