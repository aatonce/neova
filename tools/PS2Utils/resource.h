//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Ps2util.rc
//
#define IDC_MYICON                      2
#define IDD_PS2UTIL_DIALOG              102
#define IDD_ABOUTBOX                    103
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDS_HELLO                       106
#define IDI_PS2UTIL                     107
#define IDI_SMALL                       108
#define IDC_PS2UTIL                     109
#define IDR_MAINFRAME                   128
#define IDD_INFOS                       132
#define IDD_IP                          133
#define IDM_KILL                        135
#define IDL_INFOS                       1008
#define IDC_IP                          1011
#define IDC_EDIT                        1015
#define IDM_OPEN                        32771
#define IDM_RESET                       32772
#define IDM_IP                          32773
#define IDM_DEBUG                       32775
#define IDM_LOG                         32776
#define IDM_CLEAR                       32777
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
