// main.c
#include <stdarg.h>
#include <eekernel.h>
#include <sifcmd.h>
#include <sifdev.h>
#include <sifrpc.h>
#include <eeregs.h>
#include <libgraph.h>
#include <libcdvd.h>
#include <libdma.h>
#include <libpkt.h>
#include <sifdev.h>
#include <libdev.h>
#include <libpad.h>
#include <libmc.h>
#include <string.h>
#include "atwinmon.h"

int main(int argc, char *argv[])
{
	int returnvalue;

	returnvalue=atwinmon(argc,argv);

	return returnvalue;
}

