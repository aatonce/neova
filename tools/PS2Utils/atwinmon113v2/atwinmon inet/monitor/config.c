/* SCEI CONFIDENTIAL
"PlayStation 2" Artist Tool Monitor Release 1.3
 */
/*
 *                      Artist Tool / Monitor
 *
 *              ---- Monitor Configuration File ----
 * 
 *         Copyright (C) 2001 Sony Computer Entertainment Inc.
 *                        All Rights Reserved.
 *
 *                          Name : config.c
 *
 *            Version   Date           Design      Log
 *  --------------------------------------------------------------------
 *            1.00      Oct,31,2000    kaol
 *                      Mar., 2001     kaol	   media_type is added
 *            1.20c     May,  2001     kaol	   if_type is added
 */

/* O P T I O N :       I O P   R E P L A C E M E N T   M O D U L E
 * ---------------------------------------------------------------- */

#include <sifdev.h>
#include <eetypes.h>
unsigned char *iop_image = IOP_IMAGE_FILE;


/* O P T I O N :                                     N E T W O R K
 * ---------------------------------------------------------------- */

#define ON  1
#define OFF 0

int dhcp_enable = OFF;
char *monitor_ip_addr = "192.168.0.152";
char *monitor_netmask = "255.255.255.0";
char *monitor_gateway = "192.168.0.1";
char *monitor_dns     = "192.168.0.1";


/* O P T I O N :                               V I D E O   M O D E   
 * ---------------------------------------------------------------- */

#define FORMAT_NTSC 0
#define FORMAT_PAL 1

int video_format = FORMAT_NTSC;


/* O P T I O N :                               M E D I A   T Y P E
 * ---------------------------------------------------------------- */

#include <libcdvd.h>
int media_type = SCECdCD; /* or SCECdDVD */

/* O P T I O N :       N E T W O R K   I N T E R F A C E   T Y P E
 * ---------------------------------------------------------------- */

#define IF_USB_ETHERNET_ADAPTOR 0
#define IF_NETWORK_ADAPTOR      1

int if_type = IF_NETWORK_ADAPTOR; /* or IF_NETWORK_ADAPTOR */

/* O N L Y  `if_type == IF_USB_ETHERNET_ADAPTOR':
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * 
 * #### But, the variables below MUST be declared at all time, ####
 * #### not depending on the value of `if_type'.               ####
 *
 * if_usb_module: ... replacement module of AN986.IRX
 * - - - - - - - - - - - - - - - - - - - - - - - - - -
 *	.IRX file name that supports USB-Ethernet adaptor, with uppercase,
 *	or NULL, if using AN986.IRX or IF_NETWORK_ADAPTOR
 *
 * if_usb_module_option:
 * - - - - - - - - - - - - - - - - - - - - - - - - - -
 *	option string for `if_usb_support_module'
 *
 * if_usb_module_option_length:
 * - - - - - - - - - - - - - - - - - - - - - - - - - -
 *	the length of `if_usb_support_module_option';
 *	the value is 10 if option is "-foo\0-bar\0", for example.
 */

unsigned char *if_usb_module               = NULL;
unsigned char *if_usb_module_option        = NULL;
unsigned int   if_usb_module_option_length = 0;

/* Atwinmon options */

int exception_handle = ON;
int usemultitap = ON;

/* ----------------------------------------------------------------
 *	End on File
 * ---------------------------------------------------------------- */
/* This file ends here, DON'T ADD STUFF AFTER THIS */
