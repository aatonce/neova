@echo off
echo copying IOP MODULES
copy %SCE%\iop\modules\sio2man.irx target
copy %SCE%\iop\modules\mtapman.irx target
copy %SCE%\iop\modules\padman.irx target
copy %SCE%\iop\modules\mcman.irx target
copy %SCE%\iop\modules\mcserv.irx target
copy %SCE%\iop\modules\usbd.irx target
copy %SCE%\iop\modules\inet.irx target
copy %SCE%\iop\modules\netcnf.irx target
copy %SCE%\iop\modules\inetctl.irx target
copy %SCE%\iop\modules\dev9.irx target
copy %SCE%\iop\modules\smap.irx target
echo eraseing IOP IMAGE
erase target\*.img
copy %SCE%\iop\modules\*.img target


