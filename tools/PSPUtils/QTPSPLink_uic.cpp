/****************************************************************************
** Form implementation generated from reading ui file 'd:\work\prog\Nova\tools\PSPUtils\QTPSPLink.ui'
**
** Created: ven. 9. juin 18:22:54 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "QTPSPLink_uic.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qframe.h>
#include <qlabel.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qgroupbox.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qtextedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a QTPSPLink as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
QTPSPLink::QTPSPLink( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "QTPSPLink" );
    QTPSPLinkLayout = new QVBoxLayout( this, 6, 6, "QTPSPLinkLayout"); 

    ConfigFrame = new QFrame( this, "ConfigFrame" );
    ConfigFrame->setFrameShape( QFrame::StyledPanel );
    ConfigFrame->setFrameShadow( QFrame::Raised );
    ConfigFrameLayout = new QHBoxLayout( ConfigFrame, 6, 6, "ConfigFrameLayout"); 

    elfLabel = new QLabel( ConfigFrame, "elfLabel" );
    ConfigFrameLayout->addWidget( elfLabel );

    ELFButton = new QPushButton( ConfigFrame, "ELFButton" );
    ELFButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, ELFButton->sizePolicy().hasHeightForWidth() ) );
    ELFButton->setMaximumSize( QSize( 20, 32767 ) );
    ELFButton->setAutoDefault( FALSE );
    ConfigFrameLayout->addWidget( ELFButton );

    bfLabel = new QLabel( ConfigFrame, "bfLabel" );
    ConfigFrameLayout->addWidget( bfLabel );

    BFButton = new QPushButton( ConfigFrame, "BFButton" );
    BFButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, BFButton->sizePolicy().hasHeightForWidth() ) );
    BFButton->setMaximumSize( QSize( 20, 32767 ) );
    BFButton->setAutoDefault( FALSE );
    ConfigFrameLayout->addWidget( BFButton );

    syncELFBox = new QCheckBox( ConfigFrame, "syncELFBox" );
    syncELFBox->setChecked( TRUE );
    ConfigFrameLayout->addWidget( syncELFBox );

    syncBFBox = new QCheckBox( ConfigFrame, "syncBFBox" );
    syncBFBox->setChecked( TRUE );
    ConfigFrameLayout->addWidget( syncBFBox );

    Use_Paddle = new QCheckBox( ConfigFrame, "Use_Paddle" );
    Use_Paddle->setEnabled( TRUE );
    Use_Paddle->setChecked( TRUE );
    ConfigFrameLayout->addWidget( Use_Paddle );
    QSpacerItem* spacer = new QSpacerItem( 190, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    ConfigFrameLayout->addItem( spacer );

    ipLabel = new QLabel( ConfigFrame, "ipLabel" );
    ConfigFrameLayout->addWidget( ipLabel );

    ipEdit = new QLineEdit( ConfigFrame, "ipEdit" );
    ipEdit->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)0, 0, 0, ipEdit->sizePolicy().hasHeightForWidth() ) );
    QFont ipEdit_font(  ipEdit->font() );
    ipEdit_font.setPointSize( 10 );
    ipEdit->setFont( ipEdit_font ); 
    ipEdit->setFocusPolicy( QLineEdit::ClickFocus );
    ConfigFrameLayout->addWidget( ipEdit );
    QTPSPLinkLayout->addWidget( ConfigFrame );

    logBox = new QGroupBox( this, "logBox" );
    logBox->setColumnLayout(0, Qt::Vertical );
    logBox->layout()->setSpacing( 6 );
    logBox->layout()->setMargin( 11 );
    logBoxLayout = new QGridLayout( logBox->layout() );
    logBoxLayout->setAlignment( Qt::AlignTop );

    tabWidget2 = new QTabWidget( logBox, "tabWidget2" );

    tab = new QWidget( tabWidget2, "tab" );
    tabLayout = new QVBoxLayout( tab, 4, 6, "tabLayout"); 

    outputText = new QTextEdit( tab, "outputText" );
    QFont outputText_font(  outputText->font() );
    outputText_font.setFamily( "Courier New" );
    outputText->setFont( outputText_font ); 
    outputText->setTextFormat( QTextEdit::PlainText );
    outputText->setWordWrap( QTextEdit::WidgetWidth );
    outputText->setReadOnly( TRUE );
    tabLayout->addWidget( outputText );

    inputText = new QLineEdit( tab, "inputText" );
    tabLayout->addWidget( inputText );
    tabWidget2->insertTab( tab, QString("") );

    tab_2 = new QWidget( tabWidget2, "tab_2" );
    tabLayout_2 = new QGridLayout( tab_2, 1, 1, 4, 6, "tabLayout_2"); 

    messageText = new QTextEdit( tab_2, "messageText" );
    QFont messageText_font(  messageText->font() );
    messageText_font.setFamily( "Courier New" );
    messageText->setFont( messageText_font ); 
    messageText->setTextFormat( QTextEdit::PlainText );
    messageText->setWordWrap( QTextEdit::WidgetWidth );
    messageText->setReadOnly( TRUE );

    tabLayout_2->addWidget( messageText, 0, 0 );
    tabWidget2->insertTab( tab_2, QString("") );

    logBoxLayout->addWidget( tabWidget2, 0, 0 );
    QTPSPLinkLayout->addWidget( logBox );

    ActionBox = new QGroupBox( this, "ActionBox" );
    ActionBox->setColumnLayout(0, Qt::Vertical );
    ActionBox->layout()->setSpacing( 6 );
    ActionBox->layout()->setMargin( 6 );
    ActionBoxLayout = new QHBoxLayout( ActionBox->layout() );
    ActionBoxLayout->setAlignment( Qt::AlignTop );

    ConnectButton = new QPushButton( ActionBox, "ConnectButton" );
    ConnectButton->setAutoDefault( FALSE );
    ActionBoxLayout->addWidget( ConnectButton );

    DisconnectButton = new QPushButton( ActionBox, "DisconnectButton" );
    DisconnectButton->setAutoDefault( FALSE );
    ActionBoxLayout->addWidget( DisconnectButton );

    ExecButton = new QPushButton( ActionBox, "ExecButton" );
    ExecButton->setAutoDefault( FALSE );
    ActionBoxLayout->addWidget( ExecButton );

    KillButton = new QPushButton( ActionBox, "KillButton" );
    KillButton->setAutoDefault( FALSE );
    ActionBoxLayout->addWidget( KillButton );

    ResetButton = new QPushButton( ActionBox, "ResetButton" );
    ResetButton->setAutoDefault( FALSE );
    ActionBoxLayout->addWidget( ResetButton );

    RestartButton = new QPushButton( ActionBox, "RestartButton" );
    RestartButton->setAutoDefault( FALSE );
    ActionBoxLayout->addWidget( RestartButton );

    ModListButton = new QPushButton( ActionBox, "ModListButton" );
    ModListButton->setAutoDefault( FALSE );
    ActionBoxLayout->addWidget( ModListButton );

    SyncDataButton = new QPushButton( ActionBox, "SyncDataButton" );
    SyncDataButton->setAutoDefault( FALSE );
    ActionBoxLayout->addWidget( SyncDataButton );
    QSpacerItem* spacer_2 = new QSpacerItem( 100, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    ActionBoxLayout->addItem( spacer_2 );
    QTPSPLinkLayout->addWidget( ActionBox );
    languageChange();
    resize( QSize(946, 818).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
QTPSPLink::~QTPSPLink()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void QTPSPLink::languageChange()
{
    setCaption( tr( "PSP Link v0.1" ) );
    elfLabel->setText( tr( "ELF" ) );
    QToolTip::add( elfLabel, tr( "Choose the ELF file to launch" ) );
    ELFButton->setText( tr( "..." ) );
    QToolTip::add( ELFButton, tr( "Specify the ELF filename" ) );
    bfLabel->setText( tr( "BIGFILE" ) );
    QToolTip::add( bfLabel, tr( "Choose the ELF file to launch" ) );
    BFButton->setText( tr( "..." ) );
    QToolTip::add( BFButton, tr( "Specify the BIGFILE filename" ) );
    syncELFBox->setText( tr( "Sync ELF" ) );
    QToolTip::add( syncELFBox, tr( "Enable syncing ELF on memory stick." ) );
    syncBFBox->setText( tr( "Sync BIGFILE" ) );
    QToolTip::add( syncBFBox, tr( "Enable syncing BIGFILE on memory stick." ) );
    Use_Paddle->setText( tr( "Use Paddle" ) );
    ipLabel->setText( tr( "IP" ) );
    QToolTip::add( ipLabel, tr( "Set the IP address of the PSP" ) );
    ipEdit->setInputMask( tr( "000.000.000.000;_" ) );
    QToolTip::add( ipEdit, tr( "PSP's IP address" ) );
    logBox->setTitle( tr( "Outputs" ) );
    outputText->setText( QString::null );
    tabWidget2->changeTab( tab, tr( "Console" ) );
    messageText->setText( QString::null );
    tabWidget2->changeTab( tab_2, tr( "Messages" ) );
    ActionBox->setTitle( tr( "Actions" ) );
    ConnectButton->setText( tr( "Connect" ) );
    QToolTip::add( ConnectButton, tr( "Connect to the IP specified (port 23)" ) );
    DisconnectButton->setText( tr( "Disconnect" ) );
    QToolTip::add( DisconnectButton, tr( "Close the connection" ) );
    ExecButton->setText( tr( "Exec" ) );
    QToolTip::add( ExecButton, tr( "Launch the elf file on the memory stick" ) );
    KillButton->setText( tr( "Kill" ) );
    QToolTip::add( KillButton, tr( "Kill the last process started" ) );
    ResetButton->setText( tr( "Reset" ) );
    QToolTip::add( ResetButton, tr( "Reset the shell on the PSP (and close the connection)" ) );
    RestartButton->setText( tr( "Restart" ) );
    QToolTip::add( RestartButton, tr( "Copy the ELF to the memory stick and launch it" ) );
    ModListButton->setText( tr( "ModList" ) );
    QToolTip::add( ModListButton, tr( "Send the 'modlist' command in order to see all module launched" ) );
    SyncDataButton->setText( tr( "SyncData" ) );
    QToolTip::add( SyncDataButton, tr( "Sync data on memory stick." ) );
}

