/****************************************************************************
** PSPLink meta object code from reading C++ file 'PSPLink.h'
**
** Created: ven. 9. juin 18:33:33 2006
**      by: The Qt MOC ($Id: $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "PSPLink.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.2.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *PSPLink::className() const
{
    return "PSPLink";
}

QMetaObject *PSPLink::metaObj = 0;
static QMetaObjectCleanUp cleanUp_PSPLink( "PSPLink", &PSPLink::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString PSPLink::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "PSPLink", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString PSPLink::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "PSPLink", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* PSPLink::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QTPSPLink::staticMetaObject();
    static const QUMethod slot_0 = {"connectToHost", 0, 0 };
    static const QUMethod slot_1 = {"closeConnection", 0, 0 };
    static const QUParameter param_slot_2[] = {
	{ "msg", &static_QUType_QString, 0, QUParameter::In }
    };
    static const QUMethod slot_2 = {"sendToServer", 1, param_slot_2 };
    static const QUMethod slot_3 = {"socketReadyRead", 0, 0 };
    static const QUMethod slot_4 = {"socketPReadyRead", 0, 0 };
    static const QUMethod slot_5 = {"socketConnected", 0, 0 };
    static const QUMethod slot_6 = {"socketConnectionClosed", 0, 0 };
    static const QUMethod slot_7 = {"socketClosed", 0, 0 };
    static const QUParameter param_slot_8[] = {
	{ "e", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_8 = {"socketError", 1, param_slot_8 };
    static const QUMethod slot_9 = {"socketPConnected", 0, 0 };
    static const QUMethod slot_10 = {"socketPConnectionClosed", 0, 0 };
    static const QUMethod slot_11 = {"socketPClosed", 0, 0 };
    static const QUParameter param_slot_12[] = {
	{ "e", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_12 = {"socketPError", 1, param_slot_12 };
    static const QUMethod slot_13 = {"byebye", 0, 0 };
    static const QUMethod slot_14 = {"userCommand", 0, 0 };
    static const QUMethod slot_15 = {"slotOpenELF", 0, 0 };
    static const QUMethod slot_16 = {"slotOpenBIGFILE", 0, 0 };
    static const QUMethod slot_17 = {"execELF", 0, 0 };
    static const QUMethod slot_18 = {"killELF", 0, 0 };
    static const QUMethod slot_19 = {"resetPSP", 0, 0 };
    static const QUMethod slot_20 = {"Restart", 0, 0 };
    static const QUMethod slot_21 = {"ModList", 0, 0 };
    static const QUMethod slot_22 = {"SyncDataMS", 0, 0 };
    static const QUMethod slot_23 = {"AcquirePaddle", 0, 0 };
    static const QUMethod slot_24 = {"ConnectPaddle", 0, 0 };
    static const QUParameter param_slot_25[] = {
	{ "on", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_25 = {"SlotUsePaddle", 1, param_slot_25 };
    static const QMetaData slot_tbl[] = {
	{ "connectToHost()", &slot_0, QMetaData::Public },
	{ "closeConnection()", &slot_1, QMetaData::Public },
	{ "sendToServer(QString)", &slot_2, QMetaData::Public },
	{ "socketReadyRead()", &slot_3, QMetaData::Public },
	{ "socketPReadyRead()", &slot_4, QMetaData::Public },
	{ "socketConnected()", &slot_5, QMetaData::Public },
	{ "socketConnectionClosed()", &slot_6, QMetaData::Public },
	{ "socketClosed()", &slot_7, QMetaData::Public },
	{ "socketError(int)", &slot_8, QMetaData::Public },
	{ "socketPConnected()", &slot_9, QMetaData::Public },
	{ "socketPConnectionClosed()", &slot_10, QMetaData::Public },
	{ "socketPClosed()", &slot_11, QMetaData::Public },
	{ "socketPError(int)", &slot_12, QMetaData::Public },
	{ "byebye()", &slot_13, QMetaData::Public },
	{ "userCommand()", &slot_14, QMetaData::Public },
	{ "slotOpenELF()", &slot_15, QMetaData::Public },
	{ "slotOpenBIGFILE()", &slot_16, QMetaData::Public },
	{ "execELF()", &slot_17, QMetaData::Public },
	{ "killELF()", &slot_18, QMetaData::Public },
	{ "resetPSP()", &slot_19, QMetaData::Public },
	{ "Restart()", &slot_20, QMetaData::Public },
	{ "ModList()", &slot_21, QMetaData::Public },
	{ "SyncDataMS()", &slot_22, QMetaData::Public },
	{ "AcquirePaddle()", &slot_23, QMetaData::Public },
	{ "ConnectPaddle()", &slot_24, QMetaData::Public },
	{ "SlotUsePaddle(bool)", &slot_25, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"PSPLink", parentObject,
	slot_tbl, 26,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_PSPLink.setMetaObject( metaObj );
    return metaObj;
}

void* PSPLink::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "PSPLink" ) )
	return this;
    return QTPSPLink::qt_cast( clname );
}

bool PSPLink::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: connectToHost(); break;
    case 1: closeConnection(); break;
    case 2: sendToServer((QString)static_QUType_QString.get(_o+1)); break;
    case 3: socketReadyRead(); break;
    case 4: socketPReadyRead(); break;
    case 5: socketConnected(); break;
    case 6: socketConnectionClosed(); break;
    case 7: socketClosed(); break;
    case 8: socketError((int)static_QUType_int.get(_o+1)); break;
    case 9: socketPConnected(); break;
    case 10: socketPConnectionClosed(); break;
    case 11: socketPClosed(); break;
    case 12: socketPError((int)static_QUType_int.get(_o+1)); break;
    case 13: byebye(); break;
    case 14: userCommand(); break;
    case 15: slotOpenELF(); break;
    case 16: slotOpenBIGFILE(); break;
    case 17: execELF(); break;
    case 18: killELF(); break;
    case 19: resetPSP(); break;
    case 20: Restart(); break;
    case 21: ModList(); break;
    case 22: SyncDataMS(); break;
    case 23: AcquirePaddle(); break;
    case 24: ConnectPaddle(); break;
    case 25: SlotUsePaddle((bool)static_QUType_bool.get(_o+1)); break;
    default:
	return QTPSPLink::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool PSPLink::qt_emit( int _id, QUObject* _o )
{
    return QTPSPLink::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool PSPLink::qt_property( int id, int f, QVariant* v)
{
    return QTPSPLink::qt_property( id, f, v);
}

bool PSPLink::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
