
#include <windows.h>


#include "PSPLink.h"
#include <qvariant.h>
#include <qlineedit.h>
#include <qtextedit.h>
#include <qsocket.h>
#include <qapplication.h>
#include <qtextstream.h>
#include <qfiledialog.h>
#include <qfile.h>
#include <qdir.h> 
#include <qmessagebox.h> 
#include <qcheckbox.h> 
#include <qstring.h> 
#include <qobject.h>
#include <qpushbutton.h> 
#include <qtimer.h>

#include <stdlib.h>
#include <string.h>

#include "Paddle.h"

//#define PORT				23		// 0.9d
#define PORT				10000	// 0.9g
#define PADDLE_PORT			5555	// 

#define TIME_CHECK_PADDLE				100
#define TIME_CHECK_CONNECT_FOR_PADDLE	100

namespace
{

	QString PSPELFNAME = "psp001.prx";

}


PSPLink::PSPLink(	QWidget* parent,
					const char* name) : QTPSPLink(parent,name)
{
	// set slots
	connect( ConnectButton, SIGNAL(clicked()), this, SLOT(connectToHost()) );
	connect( DisconnectButton, SIGNAL(clicked()), this, SLOT(closeConnection()) );
	connect( ResetButton, SIGNAL(clicked()), this, SLOT(resetPSP()) );
	connect( ELFButton, SIGNAL( clicked() ), this, SLOT( slotOpenELF() ) );
	connect( BFButton, SIGNAL( clicked() ), this, SLOT( slotOpenBIGFILE() ) );
	connect( ExecButton, SIGNAL( clicked() ), this, SLOT( execELF() ) );
	connect( KillButton, SIGNAL( clicked() ), this, SLOT( killELF() ) );
	connect( ModListButton, SIGNAL( clicked() ), this, SLOT( ModList() ) );
	connect( RestartButton, SIGNAL(clicked()), this, SLOT(Restart()) );
	connect( SyncDataButton, SIGNAL(clicked()), this, SLOT(SyncDataMS()) );

	connect( inputText, SIGNAL(returnPressed()), this, SLOT(userCommand()) );

	// create the socket and connect various of its signals
	socket = new QSocket( this );
	connect( socket, SIGNAL(connected()), this, SLOT(socketConnected()) );
	connect( socket, SIGNAL(connectionClosed()), this, SLOT(socketConnectionClosed()) );
	connect( socket, SIGNAL(readyRead()), this, SLOT(socketReadyRead()) );
	connect( socket, SIGNAL(error(int)), this, SLOT(socketError(int)) );

	// create the socket to use PC's paddle with PSP
	socketPaddle = new QSocket( this );
	connect( socketPaddle, SIGNAL(connected()), this, SLOT(socketPConnected()) );
	connect( socketPaddle, SIGNAL(connectionClosed()), this, SLOT(socketPConnectionClosed()) );
	connect( socketPaddle, SIGNAL(readyRead()), this, SLOT(socketPReadyRead()) );
	connect( socketPaddle, SIGNAL(error(int)), this, SLOT(socketPError(int)) );

	connect( this, SIGNAL(aboutToQuit()), this, SLOT(byebye()) )  ; 

	elfFilename	= "";
	elfPath		= "";
	bfFilename	= "";
	bfPath		= "";
	moduleID	= "";

	inputText->setFocus() ;
	openIniFile();
	connectToHost();


	ctrl::Init();
	ctrl::Open(winId());
	if (ctrl::GetNbPaddle() == 0 ) 		
		ctrl::Close();

	Use_Paddle->setDisabled(true);		

	AcquirePTimer = new QTimer(this,"AcquirePaddleTimer");
	connect( AcquirePTimer, SIGNAL(timeout()), this, SLOT(AcquirePaddle()) );
	AcquirePTimer->start(TIME_CHECK_PADDLE,false);
	connectedPaddle = false;

	TryToConnectPaddleTimer =new QTimer(this,"TryToConnectPaddleTimer");
	connect( TryToConnectPaddleTimer, SIGNAL(timeout()), this, SLOT(ConnectPaddle()) );
	connect( Use_Paddle, SIGNAL(toggled(bool)), this, SLOT(SlotUsePaddle(bool)) );
}

void 
PSPLink::AcquirePaddle()
{
	if (ctrl::GetNbPaddle() == 0 ) 		
		ctrl::Open(winId());	

	ctrl::Update();
	if (ctrl::GetStatus(0) != NULL ) {		
		Use_Paddle->setDisabled(false);
		if (Use_Paddle->isChecked() && !TryToConnectPaddleTimer->isActive()){
			TryToConnectPaddleTimer->start(TIME_CHECK_CONNECT_FOR_PADDLE,false);
		}
	}
	else {
		Use_Paddle->setDisabled(true);		
		TryToConnectPaddleTimer->stop();		
		if (socketPaddle->state() != QSocket::Idle ) {
			socketPaddle->close();
		}
	}

	if (ctrl::GetNbPaddle() == 0 ) 		
		ctrl::Close();
}

void 
PSPLink::ConnectPaddle()
{
	if (socketPaddle->state() == QSocket::Idle ) 
		socketPaddle->connectToHost(ipEdit->text() , PADDLE_PORT);
}

void 
PSPLink::SlotUsePaddle( bool on ){
	if (on){
		if(!TryToConnectPaddleTimer->isActive() ) {
			OutputMessage( tr("Paddle : Try to connect"));
			TryToConnectPaddleTimer->start(TIME_CHECK_CONNECT_FOR_PADDLE,false);
		}
	}
	else {
		if (socketPaddle->state() != QSocket::Idle ) {
			socketPaddle->close();
		}
		OutputMessage( tr("Paddle : Stop"));
		TryToConnectPaddleTimer->stop();
	}
}

void
PSPLink::ClearMessage()
{
	messageText->clear();
}


void
PSPLink::OutputMessage( QString	inQS	)
{
	QTime t = QTime::currentTime();
	messageText->append( tr("[%1] %2\n").arg(t.toString()).arg(inQS) );
}


void
PSPLink::ClearConsole()
{
	outputText->clear();
}


void
PSPLink::OutputConsole( QString	inQS	)
{
	outputText->append( inQS );
}


bool
PSPLink::AreFileSynced(	QString		inSourceFile,
						QString		inDestFile	)
{
	QFileInfo sf( inSourceFile );
	QFileInfo df( inDestFile );
	if( !sf.isFile() )
		return TRUE;
	if( !df.isFile() )
		return FALSE;
	QDateTime sd = sf.lastModified();
	QDateTime dd = df.lastModified();
//	OutputMessage( tr("source file %1 %2").arg(inSourceFile).arg(sd.toString()) );
//	OutputMessage( tr("dest   file %1 %2").arg(inDestFile).arg(dd.toString()) );
	return ( sd <= dd );
}


void PSPLink::userCommand()
{
	sendToServer(inputText->text());
	inputText->clear();
}


void PSPLink::connectToHost()
{
	// connect to the server
	OutputMessage( tr("Trying to connect to the server") );
	saveIniFile();
	socket->connectToHost( ipEdit->text(), PORT );
}


void PSPLink::closeConnection()
{
	socket->close();
	if ( socket->state() == QSocket::Closing ) {
		// We have a delayed close.
		connect( socket, SIGNAL(delayedCloseFinished()), SLOT(socketClosed()) );
	} else {
		// The socket is closed.
		socketClosed();
	}
}


void PSPLink::sendToServer(QString msg)
{
	OutputMessage( tr("Sending cmd <%1>").arg(msg) );
	// write to the server
	QTextStream os(socket);
	os << msg << "\n";
}


void PSPLink::socketReadyRead()
{
	// read from the server
	while ( socket->canReadLine() ) {
		QString line = socket->readLine();
		OutputConsole( line );
		checkRead(line);
	}
}

void PSPLink::socketPReadyRead(){
	while ( socketPaddle->canReadLine() ) {
		QString line = socketPaddle->readLine();
		if (line.compare(QString("update"))){
			ctrl::Update();
			ctrl::Status * ptrS = ctrl::GetStatus(0);
			if (ptrS)
				socketPaddle->writeBlock((const char *) ptrS->button , sizeof(ptrS->button));
		}
	}
}

void PSPLink::socketConnected()
{
	OutputMessage( tr("Connected to server, port %1").arg(PORT) );
	connected = true;
}


void PSPLink::socketConnectionClosed()
{
	OutputMessage( tr("Connection closed by the server") );
	connected = false;
}


void PSPLink::socketClosed()
{
	OutputMessage( tr("Connection closed") );
	connected = false;
}


void PSPLink::socketError( int e )
{
	OutputMessage( tr("Error number %1 occurred").arg(e) );
	connected = false;
}



void PSPLink::socketPConnected()
{
	OutputMessage( tr("Paddle : Connected to server, port %1").arg(PADDLE_PORT) );
	connectedPaddle = true;
	TryToConnectPaddleTimer->stop();
}


void PSPLink::socketPConnectionClosed()
{
	OutputMessage( tr("Paddle : Connection closed by the server") );
	connectedPaddle = false;
	TryToConnectPaddleTimer->start(TIME_CHECK_CONNECT_FOR_PADDLE,false);
}


void PSPLink::socketPClosed()
{
	OutputMessage( tr("Paddle : Connection closed") );
	connectedPaddle = false;
	TryToConnectPaddleTimer->start(TIME_CHECK_CONNECT_FOR_PADDLE,false);
}


void PSPLink::socketPError( int e )
{
	if (e!=0)
		OutputMessage( tr("Paddle : Error number %1 occurred").arg(e) );	
}

void PSPLink::byebye()
{
	closeConnection();
	saveIniFile();
	ctrl::Close();
	ctrl::Shut();
	QApplication::exit( 0 );
}


void PSPLink::checkRead(QString line)
{
	if ( line.find("Load/Start") != -1 ) {
		moduleID = line.mid(line.find("UID:")+5,10);
		OutputMessage( tr("ModuleID is <%1>.").arg(moduleID) );
	}
}


void PSPLink::slotOpenELF()
{
	QString tempPath = "C:\\";
	if ( !bfPath.isEmpty() )
		tempPath = elfPath; 
	QString s = QFileDialog::getOpenFileName(
                    tempPath,
                    "PSP module file (*.elf *.prx)",
                    this,
                    "open file dialog",
                    "Choose the module to launch" );
  if ( ! s.isEmpty() ) {
  		elfPath = s.left(s.findRev("/"));
  		QString filename = s.right(s.length() - ((s.findRev("/"))+1));
		elfFilename = filename;
		OutputMessage( tr("ELF is <%1>, path=<%2>").arg(elfFilename).arg(elfPath) );
		saveIniFile();
  }
}


void PSPLink::slotOpenBIGFILE()
{
	QString tempPath = "C:\\";
	if ( !bfPath.isEmpty() )
		tempPath = bfPath; 
	QString s = QFileDialog::getOpenFileName(
                    tempPath,
                    "NEOVA bigfile file (*.DAT *.TOC)",
                    this,
                    "open file dialog",
                    "Choose the bigfile to use" );
  if ( ! s.isEmpty() )
  {
  		bfPath = s.left(s.findRev("/"));
  		QString filename = s.right(s.length() - ((s.findRev("/"))+1));
		bfFilename = filename.left( filename.length() - 4 );	// remove ".EXT"
		OutputMessage( tr("BIGFILE is <%1>, path=<%2>").arg(bfFilename).arg(bfPath) );
		saveIniFile();
  }
}


void PSPLink::execELF()
{
	if ( ! connected )
	{
		OutputMessage( tr("Not connected to server !") );
		return;
	}
	if ( ! elfFilename.isEmpty() )
	{
		ClearConsole();
		sendToServer ("ldstart ms0:/" + PSPELFNAME );
	}
}


void PSPLink::resetPSP()
{
	if ( ! connected )
	{
		OutputMessage( tr("Not connected to server") );
		return;
	}
	sendToServer ("reset");
}


void PSPLink::killELF()
{
	if ( ! connected )
	{
		OutputMessage( tr("Not connected to server") );
		return;
	}
	if ( ! moduleID.isEmpty() )
		sendToServer ("kill " + moduleID );
}


void PSPLink::openIniFile()
{
	elfPath = "C:\\";
	bfPath  = "C:\\";

	QFile file("psplink.ini");
	if( !file.open(IO_ReadOnly) ) {
		OutputMessage( tr("Init file error !") );
		return;
	}

	QTextStream ts( &file );
    QString rLine;

	// IP
	rLine = ts.readLine();
	if( rLine.isEmpty() ) {
		OutputMessage( tr("Init file error !") );
		file.close();
		return;
	}
	ipEdit->setText(rLine);

	rLine = ts.readLine();
	if( rLine.isEmpty() ) {
		OutputMessage( tr("Init file error !") );
		file.close();
		return;
	}
	elfPath = rLine.left(rLine.findRev("/"));
	elfFilename = rLine.right(rLine.length() - ((rLine.findRev("/"))+1));

	rLine = ts.readLine();
	if( rLine.isEmpty() ) {
		OutputMessage( tr("Init file error !") );
		file.close();
		return;
	}
	bfPath = rLine.left(rLine.findRev("/"));
	bfFilename = rLine.right(rLine.length() - ((rLine.findRev("/"))+1));

	file.close();
}


void PSPLink::saveIniFile()
{
	QFile file("psplink.ini");
	if ( file.open( IO_WriteOnly ) ) {
        QTextStream stream( &file );
		stream << ipEdit->text() + "\n" ; 
		stream << elfPath + "/" + elfFilename + "\n";
		stream << bfPath + "/" + bfFilename + "\n";
		stream << "\n";
		file.close();
	}
}


void PSPLink::ModList()
{
	if ( ! connected )
	{
		OutputMessage( tr("Not connected to server") );
		return;
	}
	sendToServer ("modlist");
}



void PSPLink::Restart()
{
	// check elf
	if ( elfFilename.isEmpty() )
	{
		QMessageBox::critical(	0, 
								"Restart Error",
								QString("Choose an ELF file first")
								);
		return;
	}

	// try to kill elf ( if module still executing )
//	killELF();

	// Sync data on MS
	SyncDataMS();

	// execute
	OutputMessage( tr("Executing ....") );
	execELF();
}


QChar PSPLink::findMS()
{
	// try to find the psp memory stick
	// from drive A to Z
	for ( char i = 'a' ; i < 'z' ; i++ )
	{
		QString drivePath = tr("%1:\\").arg(i);
		const char* dp = drivePath.ascii();
		uint driveType = GetDriveType( drivePath.ascii() );
		if( driveType != DRIVE_REMOVABLE )
			continue;
		QFileInfo fileinfo;
		fileinfo.setFile( tr("%1:\\PSP\\GAME").arg(i) );
		if( fileinfo.exists() )
			return QChar(i);
	}
	return NULL;
}


void PSPLink::SyncDataMS()
{
	// check psp unit drive
	QChar MSDrive = findMS();
	if( !MSDrive )
	{
		QMessageBox::critical(	0, 
								"Restart Error",
								QString("PSP unit not found: Check USB !")
								);
		return;
	}

	// Sync ELF
	if( syncELFBox->isOn() )
	{
		QString source = elfPath + "/" + elfFilename;
		QString dest = QString(MSDrive) + ":\\" + PSPELFNAME;
		if( !AreFileSynced(source,dest) )
		{
			OutputMessage( tr("Copying ELF from <%1> to <%2> ...").arg(source).arg(dest) );
			if ( !CopyFile(source.local8Bit(),dest.local8Bit(),FALSE ) )
			{
				QMessageBox::critical(	0, 
										"Restart Error",
										QString("Copy failed")
										);
				return;
			}
		}
	}

	// Sync BIGFILE
	if( syncBFBox->isOn() )
	{
		QString sourceDAT = bfPath + "/" + bfFilename + ".DAT";
		QString destDAT   = QString(MSDrive) + ":\\BIGFILE.DAT";
		if( !AreFileSynced(sourceDAT,destDAT) )
		{
			OutputMessage( tr("Copying BF from <%1> to <%2> ...").arg(sourceDAT).arg(destDAT) );
			if ( !CopyFile(sourceDAT.local8Bit(),destDAT.local8Bit(),FALSE ) ) {
				QMessageBox::critical(	0, 
										"Restart Error",
										QString("Copy failed")
										);
				return;
			}
		}

		QString sourceTOC = bfPath + "/" + bfFilename + ".TOC";
		QString destTOC   = QString(MSDrive) + ":\\BIGFILE.TOC";
		if( !AreFileSynced(sourceTOC,destTOC) )
		{
			OutputMessage( tr("Copying BF from <%1> to <%2> ...").arg(sourceTOC).arg(destTOC) );
			if ( !CopyFile(sourceTOC.local8Bit(),destTOC.local8Bit(),FALSE ) ) {
				QMessageBox::critical(	0, 
										"Restart Error",
										QString("Copy failed")
										);
				return;
			}
		}
	}

	OutputMessage( tr("Data synced.") );
}


