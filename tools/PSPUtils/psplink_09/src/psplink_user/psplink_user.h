/*
 * PSPLINK
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPLINK root for details.
 *
 * psplink_user.h -Header for PSPLINK user module.
 *
 * Copyright (c) 2005 James F <tyranid@gmail.com>
 *
 * $HeadURL: svn://svn.pspdev.org/psp/trunk/psplink/psplink_user/psplink_user.h $
 * $Id: psplink_user.h 1785 2006-02-05 11:05:44Z tyranid $
 */
#ifndef __PSPLINK_USER_H__
#define __PSPLINK_USER_H__

typedef int (*GdbHandler)(PspDebugRegBlock *regs);

void psplinkUserRegisterGdbHandler(GdbHandler gdbhandler);

#endif
