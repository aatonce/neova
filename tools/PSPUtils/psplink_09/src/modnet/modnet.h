/*
 * PSPLINK
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPLINK root for details.
 *
 * modnet.h - Main code for MODNET user module.
 *
 * Copyright (c) 2005 James F <tyranid@gmail.com>
 * Some parts (c) 2005 PSPPet
 *
 * $HeadURL: svn://svn.pspdev.org/psp/trunk/psplink/modnet/modnet.h $
 * $Id: modnet.h 1667 2006-01-09 19:00:06Z tyranid $
 */
#ifndef __MODNET_H__
#define __MODNET_H__

int modNetIsInit(void);
const char* modNetGetIpAddress(void);

#endif
