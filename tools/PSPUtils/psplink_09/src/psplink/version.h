/*
 * PSPLINK
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPLINK root for details.
 *
 * version.h - PSPLink version string
 *
 * Copyright (c) 2005 James F <tyranid@gmail.com>
 *
 * $HeadURL: svn://svn.pspdev.org/psp/trunk/psplink/psplink/version.h $
 * $Id: version.h 1834 2006-03-12 15:24:32Z tyranid $
 */

#ifndef __VERSION_H__
#define __VERSION_H__

#define PSPLINK_VERSION "0.9e"

#endif
