/*
 * PSPLINK
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPLINK root for details.
 *
 * sio.h - PSPLINK kernel module sio code
 *
 * Copyright (c) 2005 James F <tyranid@gmail.com>
 *
 * $HeadURL: svn://svn.pspdev.org/psp/trunk/psplink/psplink/sio.h $
 * $Id: sio.h 1666 2006-01-09 17:42:42Z tyranid $
 */

#ifndef __SIO_H__
#define __SIO_H__

void sioInit(int baudrate);
int sioReadChar(void);
int sioReadCharWithTimeout(void);

#endif
