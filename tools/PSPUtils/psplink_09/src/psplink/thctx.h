/*
 * PSPLINK
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPLINK root for details.
 *
 * thctx.h - Thread context library code for psplink
 *
 * Copyright (c) 2005 James F <tyranid@gmail.com>
 *
 * $HeadURL: svn://svn.pspdev.org/psp/trunk/psplink/psplink/thctx.h $
 * $Id: thctx.h 1775 2006-01-31 18:26:15Z tyranid $
 */
#ifndef __THCTX_H__
#define __THCTX_H__

int threadFindContext(SceUID uid);

#endif
