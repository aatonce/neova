/*
 * PSPLINK
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPLINK root for details.
 *
 * shell.h - PSPLINK kernel module shell code
 *
 * Copyright (c) 2005 James F <tyranid@gmail.com>
 * Copyright (c) 2005 Julian T <lovely@crm114.net>
 *
 * $HeadURL: svn://svn.pspdev.org/psp/trunk/psplink/psplink/shell.h $
 * $Id: shell.h 1816 2006-02-26 14:09:15Z tyranid $
 */

/* Return values for the commands */
#define CMD_EXITSHELL 	1
#define CMD_OK		  	0
#define CMD_ERROR		-1

/* Parse a command string */
int shellParse(char *command);
void shellStart(void);
int shellInit(const char *cliprompt, const char *path, const char *init_dir);
