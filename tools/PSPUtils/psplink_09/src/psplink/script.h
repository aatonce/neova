/*
 * PSPLINK
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPLINK root for details.
 *
 * script.h - PSPLINK kernel module shell script code
 *
 * Copyright (c) 2005 James F <tyranid@gmail.com>
 *
 * $HeadURL: svn://svn.pspdev.org/psp/trunk/psplink/psplink/script.h $
 * $Id: script.h 1766 2006-01-29 00:03:02Z tyranid $
 */

#ifndef __SCRIPT_H__
#define __SCRIPT_H__

int scriptRun(const char *filename, int argc, char **argv, const char *lastmod, int print);

#endif
