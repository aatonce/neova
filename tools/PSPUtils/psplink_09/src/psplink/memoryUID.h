/*
 * PSPLINK
 * -----------------------------------------------------------------------
 * Licensed under the BSD license, see LICENSE in PSPLINK root for details.
 *
 * memoryUID.h - Header file for UID dumper.
 *
 * Copyright (c) 2005 John_K
 *
 * $HeadURL: svn://svn.pspdev.org/psp/trunk/psplink/psplink/memoryUID.h $
 * $Id: memoryUID.h 1775 2006-01-31 18:26:15Z tyranid $
 */
#ifndef __MEMORYUID_H__
#define __MEMORYUID_H__

#define UIDLIST_START_1_0 0x8800d030
#define UIDLIST_START_1_5 0x8800fc98

struct _uidList {
    struct _uidList *parent;
    struct _uidList *nextChild;
    struct _uidList *unknown;   //(0x8)
    u32 UID;
    char *name;
    short unknown2;
    short attribute;
    struct _uidList *nextEntry;
};
typedef struct _uidList uidList;

uidList* findObjectByUID(SceUID uid);
u32 findUIDByName(const char *name);
void printUIDList(const char *name);

#endif
