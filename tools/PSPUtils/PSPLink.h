#ifndef PSPLINK_H
#define PSPLINK_H

#include <qvariant.h>
#include <qdialog.h>
#include <qsocket.h>
#include <qevent.h>

#include <windows.h>
#include "QTPSPLink_uic.h" 


class QTimer;

class PSPLink : public QTPSPLink
{
	Q_OBJECT

public:
	PSPLink(	QWidget* parent = 0,
				const char* name = 0);

public slots:

	void connectToHost();
    void closeConnection();

    void sendToServer(QString msg);
    void socketReadyRead();
    void socketPReadyRead();

    void socketConnected();
    void socketConnectionClosed();
    void socketClosed();
    void socketError( int e );

	void socketPConnected();
    void socketPConnectionClosed();
    void socketPClosed();
    void socketPError( int e );


	void byebye();
	void userCommand();

	void slotOpenELF();
	void slotOpenBIGFILE();
	void execELF();
	void killELF();
	void resetPSP();
	void Restart();
	void ModList();
	void SyncDataMS();

	void AcquirePaddle();
	void ConnectPaddle();
	void SlotUsePaddle( bool on);

private:
	void		initLayout();
	void		checkRead(QString line);
	void		openIniFile();
	void		saveIniFile();
	QChar		findMS();
	bool		AreFileSynced( QString inSourceFile, QString inDestFile );

	void		OutputMessage( QString	inQS	);
	void		ClearMessage();
	void		OutputConsole( QString	inQS	);
	void		ClearConsole();

	QSocket*	socket;
	QString		elfFilename;
	QString		elfPath;
	QString		bfFilename;
	QString		bfPath;
	QString		moduleID;
	bool 		connected;

	QSocket*	socketPaddle;
	bool 		connectedPaddle;
	QTimer		* AcquirePTimer;
	QTimer		* TryToConnectPaddleTimer;
};


#endif 