/****************************************************************************
** Form interface generated from reading ui file 'd:\work\prog\Nova\tools\PSPUtils\QTPSPLink.ui'
**
** Created: ven. 9. juin 18:22:54 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef QTPSPLINK_H
#define QTPSPLINK_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QFrame;
class QLabel;
class QPushButton;
class QCheckBox;
class QLineEdit;
class QGroupBox;
class QTabWidget;
class QWidget;
class QTextEdit;

class QTPSPLink : public QDialog
{
    Q_OBJECT

public:
    QTPSPLink( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~QTPSPLink();

    QFrame* ConfigFrame;
    QLabel* elfLabel;
    QPushButton* ELFButton;
    QLabel* bfLabel;
    QPushButton* BFButton;
    QCheckBox* syncELFBox;
    QCheckBox* syncBFBox;
    QCheckBox* Use_Paddle;
    QLabel* ipLabel;
    QLineEdit* ipEdit;
    QGroupBox* logBox;
    QTabWidget* tabWidget2;
    QWidget* tab;
    QTextEdit* outputText;
    QLineEdit* inputText;
    QWidget* tab_2;
    QTextEdit* messageText;
    QGroupBox* ActionBox;
    QPushButton* ConnectButton;
    QPushButton* DisconnectButton;
    QPushButton* ExecButton;
    QPushButton* KillButton;
    QPushButton* ResetButton;
    QPushButton* RestartButton;
    QPushButton* ModListButton;
    QPushButton* SyncDataButton;

protected:
    QVBoxLayout* QTPSPLinkLayout;
    QHBoxLayout* ConfigFrameLayout;
    QGridLayout* logBoxLayout;
    QVBoxLayout* tabLayout;
    QGridLayout* tabLayout_2;
    QHBoxLayout* ActionBoxLayout;

protected slots:
    virtual void languageChange();

};

#endif // QTPSPLINK_H
