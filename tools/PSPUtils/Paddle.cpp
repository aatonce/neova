/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2005 AtOnce Technologies
** Copyright (C) 2001-2005 Gerald Gainant, All rights reserved.
**
** This file is part of the Nova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.nova-engine.com or email info@nova-engine.com for
** information about Nova CoreEngine and AtOnce Technologies others products License Agreements.
**
** Contact info@nova-engine.com if any conditions of this licensing are
** not clear to you.
**
** Revision: $Id: NvCore_Ctrl[PC].cpp 476 2006-05-10 19:59:17Z magicg $
**
*****************************************************************LIC-HDR*/
#include "Paddle.h"

#include <dinput.h>
#include <ctype.h>

#include <assert.h>
#include <stdlib.h>
#include <vector>

// DI
#define	NV_DINPUT_CREATE			DirectInput8Create
#define	NV_DINPUT_REFIID			IID_IDirectInput8
#define	NV_DINPUT					LPDIRECTINPUT8
#define	NV_DINPUTDEVICE				LPDIRECTINPUTDEVICE8
#define	NV_DIDEVCAPS				DIDEVCAPS

#define Clamp(a,b,c) a

using namespace std;

namespace
{

	struct DevInfos
	{
		NV_DINPUTDEVICE		dev;
		NV_DIDEVCAPS		caps;
		DIDEVICEINSTANCE	inst;

		void Init( NV_DINPUTDEVICE inDev )
		{
			HRESULT hr;

			dev = inDev;

			caps.dwSize = sizeof(caps);
			hr = dev->GetCapabilities( &caps );
			assert( SUCCEEDED(hr) );

			inst.dwSize = sizeof(inst);
			hr = dev->GetDeviceInfo( &inst );
			assert( SUCCEEDED(hr) );
		}
		void Shut(  )
		{
			if( dev )
				dev->Release();
			dev = NULL;
		}
	};

	NV_DINPUT						pDI;
	unsigned int					devStatusMask;
	ctrl::Status					devStatusA[32];	
	vector<DevInfos>				devJoyA;



	BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance,
										 VOID* pContext )
	{
		HRESULT hr;
		NV_DINPUTDEVICE devJoy;
		hr = pDI->CreateDevice( pdidInstance->guidInstance, &devJoy, NULL );
		if( SUCCEEDED(hr) && devJoy ) {
			DevInfos dinf;
			dinf.dev = devJoy;
			devJoyA.push_back( dinf );
		}
		return DIENUM_CONTINUE;
	}
}


bool
ctrl::Init()
{
    HRESULT hr;
	hr = NV_DINPUT_CREATE(	GetModuleHandle(NULL),
							DIRECTINPUT_VERSION, 
							NV_DINPUT_REFIID,
							(VOID**)&pDI,
							NULL	);
	if( !SUCCEEDED(hr) || !pDI ) {
		return FALSE;
	}

	devStatusMask	= 0;
	return TRUE;
}


void
ctrl::Update()
{
	devStatusMask = 0;
	if (!devJoyA.size()) return ;

	// Update Joystick
	for( unsigned int i = 0 ; i < devJoyA.size() ; i++ )
	{
		if( !devJoyA[i].dev )
			continue;
	    // Poll the device to read the current state
		HRESULT hr = devJoyA[i].dev->Poll();
		if( FAILED(hr) ) {
			hr = devJoyA[i].dev->Acquire();
			while( hr == DIERR_INPUTLOST )
				hr = devJoyA[i].dev->Acquire();
			// hr may be DIERR_OTHERAPPHASPRIO or other errors.  This
			// may occur when the app is minimized or in the process of 
			// switching, so just try again later 
			/*if (hr != E_ACCESSDENIED)*/
				continue;
		}
		{
			memset(&devStatusA[i],1,sizeof(ctrl::Status));
			devStatusA[i].type = ctrl::TP_PADDLE;
			DevInfos* dev = (DevInfos*) (&devJoyA[i]);
			HRESULT hr;

			DIJOYSTATE2	dijs2;
			hr = dev->dev->GetDeviceState( sizeof(DIJOYSTATE2), &dijs2 );
			if( FAILED(hr) )
				continue;

			devStatusA[i].type	= ctrl::TP_PADDLE;

			// SuperJoyBox3 "MP-8888 USB Joypad"
			if( dev->inst.guidProduct.Data1 == 0x88880925 )
			{
				devStatusA[i].button[ ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
				DWORD pov = dijs2.rgdwPOV[0];
				if( pov != -1 ) {
					if( pov >= 31000 || pov < 5000 )
						devStatusA[i].button[ ctrl::BT_DIR3up ] = 1.0f;
					if( pov >= 4500 && pov < 14000 )
						devStatusA[i].button[ ctrl::BT_DIR3right ] = 1.0f;
					if( pov >= 13500 && pov < 23000 )
						devStatusA[i].button[ ctrl::BT_DIR3down ] = 1.0f;
					if( pov >= 22500 && pov < 32000 )
						devStatusA[i].button[ ctrl::BT_DIR3left ] = 1.0f;
				}
				for( unsigned int j = ctrl::BT_BUTTON0 ; j < ctrl::BT_MAX ; j++ )
					if( dijs2.rgbButtons[j-ctrl::BT_BUTTON0] & 0x80 )
						devStatusA[i].button[j] = 1.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON8 ] = dijs2.rgbButtons[9] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON9 ] = dijs2.rgbButtons[8] & 0x80 ? 1.0f : 0.0f;
			}

			// EMS SW2 "4 axis 16 button manette de jeu" (0x30b43) ?
			else if( dev->inst.guidProduct.Data1 == 0x30b43 )
			{
				devStatusA[i].button[ ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
				for( unsigned int j = ctrl::BT_BUTTON0 ; j < ctrl::BT_MAX ; j++ )
					if( dijs2.rgbButtons[j-ctrl::BT_BUTTON0] & 0x80 )
						devStatusA[i].button[j] = 1.0f;
				devStatusA[i].button[ ctrl::BT_DIR3up ]    = dijs2.rgbButtons[12] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_DIR3right ] = dijs2.rgbButtons[13] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_DIR3down ]  = dijs2.rgbButtons[14] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_DIR3left ]  = dijs2.rgbButtons[15] & 0x80 ? 1.0f : 0.0f;
			}

			// CCL USB SMART JOY DELUXE "4 axis 16 button manette de jeu" ?
			else if( dev->inst.guidProduct.Data1 == 0x6676666 )
			{
				devStatusA[i].button[ ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
				for( unsigned int j = ctrl::BT_BUTTON0 ; j < ctrl::BT_MAX ; j++ )
					if( dijs2.rgbButtons[j-ctrl::BT_BUTTON0] & 0x80 )
						devStatusA[i].button[j] = 1.0f;
				devStatusA[i].button[ ctrl::BT_DIR3up ]    = dijs2.rgbButtons[12] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_DIR3right ] = dijs2.rgbButtons[13] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_DIR3down ]  = dijs2.rgbButtons[14] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_DIR3left ]  = dijs2.rgbButtons[15] & 0x80 ? 1.0f : 0.0f;
			}

			//usb koo Interactive (PS2Paddle)
			else if (  dev->inst.guidProduct.Data1 == 0x10060e8f)
			{
				devStatusA[i].button[ ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
				DWORD pov = dijs2.rgdwPOV[0];
				if( pov != -1 ) {
					if( pov >= 31000 || pov < 5000 )
						devStatusA[i].button[ ctrl::BT_DIR3up ] = 1.0f;
					if( pov >= 4500 && pov < 14000 )
						devStatusA[i].button[ ctrl::BT_DIR3right ] = 1.0f;
					if( pov >= 13500 && pov < 23000 )
						devStatusA[i].button[ ctrl::BT_DIR3down ] = 1.0f;
					if( pov >= 22500 && pov < 32000 )
						devStatusA[i].button[ ctrl::BT_DIR3left ] = 1.0f;
				}
				for( unsigned int j = ctrl::BT_BUTTON0 ; j < ctrl::BT_MAX ; j++ )
					if( dijs2.rgbButtons[j-ctrl::BT_BUTTON0] & 0x80 )
						devStatusA[i].button[j] = 1.0f;

				devStatusA[i].button[ ctrl::BT_BUTTON8] = dijs2.rgbButtons[8] & 0x80 ? 1.0f : 0.0f;		// select
				devStatusA[i].button[ ctrl::BT_BUTTON9] = dijs2.rgbButtons[11] & 0x80 ? 1.0f : 0.0f;	// start
				devStatusA[i].button[ ctrl::BT_BUTTON10] = dijs2.rgbButtons[9] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON11] = dijs2.rgbButtons[10] & 0x80 ? 1.0f : 0.0f;
			}

			// USB XBox 360 controller
			// Driver Microsoft version:6.0.5221.0 date:15/09/2005
			// (http://download.microsoft.com/download/A/C/8/AC8CA87C-4F5B-49D6-9E0A-C0BBA2E96663/XBOX360Eng.exe)
			else if(  dev->inst.guidProduct.Data1 == 0x028e045e )
			{
				devStatusA[i].button[ ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lRx-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRy-32768)/32767.0F, -1.0f, +1.0f );
				DWORD pov = dijs2.rgdwPOV[0];
				if( pov != -1 ) {
					if( pov >= 31000 || pov < 5000 )
						devStatusA[i].button[ ctrl::BT_DIR3up ] = 1.0f;
					if( pov >= 4500 && pov < 14000 )
						devStatusA[i].button[ ctrl::BT_DIR3right ] = 1.0f;
					if( pov >= 13500 && pov < 23000 )
						devStatusA[i].button[ ctrl::BT_DIR3down ] = 1.0f;
					if( pov >= 22500 && pov < 32000 )
						devStatusA[i].button[ ctrl::BT_DIR3left ] = 1.0f;
				}
				devStatusA[i].button[ ctrl::BT_BUTTON0]  = dijs2.rgbButtons[3] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON1]  = dijs2.rgbButtons[1] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON2]  = dijs2.rgbButtons[0] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON3]  = dijs2.rgbButtons[2] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON4]  = Clamp( float(dijs2.lZ-32768)/32767.0F, 0.f, 1.f );
				devStatusA[i].button[ ctrl::BT_BUTTON5]  = Clamp( float(32768-dijs2.lZ)/32767.0F, 0.f, 1.f );
				devStatusA[i].button[ ctrl::BT_BUTTON6]  = dijs2.rgbButtons[4] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON7]  = dijs2.rgbButtons[5] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON8]  = dijs2.rgbButtons[6] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON9]  = dijs2.rgbButtons[7] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON10] = dijs2.rgbButtons[8] & 0x80 ? 1.0f : 0.0f;
				devStatusA[i].button[ ctrl::BT_BUTTON11] = dijs2.rgbButtons[9] & 0x80 ? 1.0f : 0.0f;
			}

			// Default
			else
			{
				devStatusA[i].button[ ctrl::BT_DIR1X ]	= Clamp( float(dijs2.lX-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR1Y ]	= Clamp( float(dijs2.lY-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2X ]	= Clamp( float(dijs2.lZ-32768)/32767.0F, -1.0f, +1.0f );
				devStatusA[i].button[ ctrl::BT_DIR2Y ]	= Clamp( float(dijs2.lRz-32768)/32767.0F, -1.0f, +1.0f );
				for( unsigned int j = ctrl::BT_BUTTON0 ; j < ctrl::BT_MAX ; j++ )
					if( dijs2.rgbButtons[j-ctrl::BT_BUTTON0] & 0x80 )
						devStatusA[i].button[j] = 1.0f;
			}		
		}
		devStatusMask |= 1<<(i);
	}
}


void
ctrl::Shut()
{
	if( pDI ) {
		ctrl::Close();
		pDI->Release();
		devJoyA.clear();
	}
}


void
ctrl::Open(	HANDLE hwnd	)
{
	HRESULT hr;

	// Look for several joysticks
	{
		devJoyA.clear();
		hr = pDI->EnumDevices(	DI8DEVCLASS_GAMECTRL, 
								EnumJoysticksCallback,
								NULL, DIEDFL_ATTACHEDONLY );
		if( !SUCCEEDED(hr) ) {
			devJoyA.clear();
		}
		else {
			// Up to 16 paddles !
			if( devJoyA.size() > 16 ) {
				for( unsigned int i = 16 ; i < devJoyA.size() ; i++ )
					devJoyA[i].Shut();
				devJoyA.resize( 16 );
			}

			for( unsigned int i = 0 ; i < devJoyA.size() ; i++ )
			{
				// Set the default data format
				assert( devJoyA[i].dev );
				hr = devJoyA[i].dev->SetDataFormat( &c_dfDIJoystick2 );
				if( !SUCCEEDED(hr) ) {
					devJoyA[i].Shut();
				}
				else {
					// Set the cooperative level
					unsigned int coopLevel;

					coopLevel = DISCL_EXCLUSIVE | DISCL_FOREGROUND;
					hr = devJoyA[i].dev->SetCooperativeLevel( (HWND)hwnd, coopLevel );
					if( !SUCCEEDED(hr) )
						devJoyA[i].Shut();
					else
						devJoyA[i].Init( devJoyA[i].dev );
				}
			}
		}
	}

	devStatusMask	= 0;
}

unsigned int 
ctrl::GetNbPaddle()
{
	return devJoyA.size();
}


ctrl::Status*
ctrl::GetStatus(	unsigned int	inCtrlNo		)
{
	if( devStatusMask & (1<<inCtrlNo) )
		return & devStatusA[inCtrlNo];
	else
		return NULL;
}


void
ctrl::Close()
{
	for( unsigned int i = 0 ; i < devJoyA.size() ; i++ )
		devJoyA[i].Shut();
	devJoyA.clear();
	devStatusMask = 0;
}

float
ctrl::Status::Filtered	(		Button			inButton,
									float			inAbsThr	)
{
	assert( inAbsThr >= 0.f );
	float v = Clamp( button[int(inButton)], -1.f, +1.f );
	if( v > inAbsThr )
		return (v-inAbsThr)/(1.f-inAbsThr);
	else if( v < -inAbsThr )
		return (v+inAbsThr)/(1.f-inAbsThr);
	else
		return 0.f;
}



