/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2005 AtOnce Technologies
** Copyright (C) 2001-2005 Gerald Gainant, All rights reserved.
**
** This file is part of the Nova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.nova-engine.com or email info@nova-engine.com for
** information about Nova CoreEngine and AtOnce Technologies others products License Agreements.
**
** Contact info@nova-engine.com if any conditions of this licensing are
** not clear to you.
**
** Revision: $Id: NvCore_Ctrl.h 476 2006-05-10 19:59:17Z magicg $
**
*****************************************************************LIC-HDR*/


#ifndef	_Paddle_H_
#define	_Paddle_H_

#include <windows.h>



namespace ctrl
{
	enum Type {
		TP_PADDLE,				// paddle
		TP_MOUSE,				// mouse
		TP_KEYBOARD,			// keyboard
		TP_MOUSE_KEYBOARD,		// mouse + keyboard
		TP_STERRINGWHEEL,		// sterring wheel
		TP_GUN					// gun
	};

	enum Button {				// Sony Playstation 2		Sony PSP
		BT_DIR1X = 0,			// Analog stick0 DirX		Analog stick DirX
		BT_DIR1Y,				// Analog stick0 DirY		Analog stick DirY
		BT_DIR2X,				// Analog stick1 DirX		Analog stick DirX
		BT_DIR2Y,				// Analog stick1 DirY		Analog stick DirY
		BT_DIR3right,			// Dir3 right				Digital right
		BT_DIR3left,			// Dir3 left				Digital left
		BT_DIR3up,				// Dir3 up					Digital up
		BT_DIR3down,			// Dir3 down				Digital down
		BT_BUTTON0,				// TRIANGLE					TRIANGLE
		BT_BUTTON1,				// CIRCLE					CIRCLE
		BT_BUTTON2,				// CROSS					CROSS
		BT_BUTTON3,				// SQUARE					SQUARE
		BT_BUTTON4,				// L2						L1
		BT_BUTTON5,				// R2						R1
		BT_BUTTON6,				// L1						L1
		BT_BUTTON7,				// R1						R1
		BT_BUTTON8,				// Select					Select
		BT_BUTTON9,				// Start					Start
		BT_BUTTON10,			// L3
		BT_BUTTON11,			// R3
		BT_BUTTON12,			//
		BT_BUTTON13,			//
		BT_BUTTON14,			//							INTERCEPTED
		BT_BUTTON15,			//							POWER/HOLD
		BT_MAX
	};

	struct Status {
		Type			type;
		bool			acquired;
		float			button[BT_MAX];
		unsigned int	flags[4];
		float			Filtered		(		Button			inButton,
												float			inAbsThr	= 0.3f	);		// >= 0 !
	};

	bool				Init			(											);
	void				Update			(											);
	void				Shut			(											);
	void				Open			(		HANDLE					hwnd		);
	Status*				GetStatus		(		unsigned int			inCtrlNo	);
	void				Close			(											);
	unsigned int		GetNbPaddle		(											);
}


#endif	// _Paddle_H_


