/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2005 AtOnce Technologies
** Copyright (C) 2001-2005 Gerald Gainant, All rights reserved.
**
** This file is part of the Nova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.nova-engine.com or email info@nova-engine.com for
** information about Nova CoreEngine and AtOnce Technologies others products License Agreements.
**
** Contact info@nova-engine.com if any conditions of this licensing are
** not clear to you.
**
** Revision: $Id: devcons.h 561 2006-06-09 19:35:44Z magicg $
**
*****************************************************************LIC-HDR*/


#ifndef _DEVCONS_H_
#define _DEVCONS_H_



namespace devcons
{
	// Init/Shut the console
	void			Init		(							);
	void			Shut		(							);

	// Output
	void			Printf		(	const char* inFmt, ...	);
	void			Output		(	const char* inBuffer	);
}



#endif // _DEVCONS_H_


