/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2005 AtOnce Technologies
** Copyright (C) 2001-2005 Gerald Gainant, All rights reserved.
**
** This file is part of the Nova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.nova-engine.com or email info@nova-engine.com for
** information about Nova CoreEngine and AtOnce Technologies others products License Agreements.
**
** Contact info@nova-engine.com if any conditions of this licensing are
** not clear to you.
**
** Revision: $Id: devcons.cpp 561 2006-06-09 19:35:44Z magicg $
**
*****************************************************************LIC-HDR*/


#include <kernel.h>
#include <kerneltypes.h>
#include <kernelutils.h>
#include <psperror.h>
#include <powersvc.h>
#include <scratchpad.h>
#include <devkit_version.h>
#include <displaysvc.h>
#include <stdio.h>
#include <psptypes.h>
#include <psperror.h>
#include <geman.h>
#include <thread.h>
#include <stdarg.h>
#include "devcons.h"




#define	VSYNC_DELAY		5
#define	BACK_COLOR		0x00000000
#define	FRONT_COLOR		0x004080FF


namespace
{
	const unsigned int fonttex[] =
	{
		0x00000000, 0x00000000, /* ' ' */
		0x10101010, 0x00100000, /* '!' */
		0x00282828, 0x00000000, /* '"' */
		0x287c2828, 0x0028287c, /* '#' */
		0x38147810, 0x00103c50, /* '$' */
		0x10204c0c, 0x00606408, /* '%' */
		0x08141408, 0x00582454, /* '&' */
		0x00102040, 0x00000000, /* ''' */
		0x10102040, 0x00402010, /* '(' */
		0x10100804, 0x00040810, /* ')' */
		0x10385410, 0x00105438, /* '*' */
		0x7c101000, 0x00001010, /* '+' */
		0x00000000, 0x08101000, /* ',' */
		0x7c000000, 0x00000000, /* '-' */
		0x00000000, 0x00181800, /* '.' */
		0x10204000, 0x00000408, /* '/' */
		0x54644438, 0x0038444c, /* '0' */
		0x10141810, 0x007c1010, /* '1' */
		0x20404438, 0x007c0418, /* '2' */
		0x30404438, 0x00384440, /* '3' */
		0x24283020, 0x0020207c, /* '4' */
		0x403c047c, 0x00384440, /* '5' */
		0x3c040830, 0x00384444, /* '6' */
		0x1020447c, 0x00101010, /* '7' */
		0x38444438, 0x00384444, /* '8' */
		0x78444438, 0x00182040, /* '9' */
		0x00100000, 0x00001000, /* ':' */
		0x00100000, 0x08101000, /* ';' */
		0x0c183060, 0x00603018, /* '<' */
		0x007c0000, 0x0000007c, /* '=' */
		0x6030180c, 0x000c1830, /* '>' */
		0x20404438, 0x00100010, /* '?' */
		0x54744438, 0x00380474, /* '@' */
		0x44442810, 0x0044447c, /* 'A' */
		0x3c48483c, 0x003c4848, /* 'B' */
		0x04044830, 0x00304804, /* 'C' */
		0x4848281c, 0x001c2848, /* 'D' */
		0x3c04047c, 0x007c0404, /* 'E' */
		0x3c04047c, 0x00040404, /* 'F' */
		0x74044438, 0x00384444, /* 'G' */
		0x7c444444, 0x00444444, /* 'H' */
		0x10101038, 0x00381010, /* 'I' */
		0x20202070, 0x00182420, /* 'J' */
		0x0c142444, 0x00442414, /* 'K' */
		0x04040404, 0x007c0404, /* 'L' */
		0x54546c44, 0x00444444, /* 'M' */
		0x544c4c44, 0x00446464, /* 'N' */
		0x44444438, 0x00384444, /* 'O' */
		0x3c44443c, 0x00040404, /* 'P' */
		0x44444438, 0x00582454, /* 'Q' */
		0x3c44443c, 0x00442414, /* 'R' */
		0x38044438, 0x00384440, /* 'S' */
		0x1010107c, 0x00101010, /* 'T' */
		0x44444444, 0x00384444, /* 'U' */
		0x44444444, 0x00102828, /* 'V' */
		0x54444444, 0x00446c54, /* 'W' */
		0x10284444, 0x00444428, /* 'X' */
		0x38444444, 0x00101010, /* 'Y' */
		0x1020407c, 0x007c0408, /* 'Z' */
		0x10101070, 0x00701010, /* '[' */
		0x10080400, 0x00004020, /* '\' */
		0x1010101c, 0x001c1010, /* ']' */
		0x00442810, 0x00000000, /* '^' */
		0x00000000, 0x007c0000, /* '_' */
		0x00000000, 0x00000000, /* ' ' */
		0x40380000, 0x00784478, /* 'a' */
		0x4c340404, 0x00344c44, /* 'b' */
		0x44380000, 0x00384404, /* 'c' */
		0x64584040, 0x00586444, /* 'd' */
		0x44380000, 0x0038047c, /* 'e' */
		0x7c105020, 0x00101010, /* 'f' */
		0x64580000, 0x38405864, /* 'g' */
		0x4c340404, 0x00444444, /* 'h' */
		0x10180010, 0x00381010, /* 'i' */
		0x10180010, 0x0c121010, /* 'j' */
		0x14240404, 0x0024140c, /* 'k' */
		0x10101018, 0x00381010, /* 'l' */
		0x542c0000, 0x00545454, /* 'm' */
		0x4c340000, 0x00444444, /* 'n' */
		0x44380000, 0x00384444, /* 'o' */
		0x4c340000, 0x0404344c, /* 'p' */
		0x64580000, 0x40405864, /* 'q' */
		0x4c340000, 0x00040404, /* 'r' */
		0x04780000, 0x003c403c, /* 's' */
		0x083c0808, 0x00304808, /* 't' */
		0x24240000, 0x00582424, /* 'u' */
		0x44440000, 0x00102844, /* 'v' */
		0x54440000, 0x00285454, /* 'w' */
		0x28440000, 0x00442810, /* 'x' */
		0x44440000, 0x38405864, /* 'y' */
		0x207c0000, 0x007c0810, /* 'z' */
		0x04080830, 0x00300808, /* '{' */
		0x10101010, 0x00101010, /* '|' */
		0x2010100c, 0x000c1010, /* '}' */
		0x0000007c, 0x00000000, /* '~' */
		0x00000000, 0x00000000, /* ' ' */
	};


	template <typename T>
	inline T UncachedPointer( T inPtr )
	{
		return (T)sceKernelMakeUncachedAddr( (void*)inPtr );
	}


	void* GetVRamTop()
	{
		int intr = sceKernelCpuSuspendIntr();
		void* top = UncachedPointer( (void*)sceGeEdramGetAddr() );
		sceKernelCpuResumeIntr( intr );
		return top;
	}


	void _ClearAll()
	{
		uint* fp = (uint*) GetVRamTop();
		for( uint i = 0 ; i < 512*272 ; i++ )
			*fp++ = BACK_COLOR;
	}


	void _ScrollUp()
	{
		uint* dp = (uint*) GetVRamTop();
		uint* sp = dp + 512*8;
		for( uint i = 0 ; i < 512*(272-8) ; i++ )
			*dp++ = *sp++;
		for( uint i = 0 ; i < 512*8 ; i++ )
			*dp++ = BACK_COLOR;
	}


	void _OutputChar( int cx, int cy, char c )
	{
		if( c < ' ' )	return;
		if( c > '~' )	return;
		c -= ' ';
		uint* fp = (uint*) GetVRamTop();
		uint mask=0, data=0;
		for( uint y = 0 ; y < 8 ; y++ ) {
			if( y == 0 ) {
				data = fonttex[c*2];
				mask = 1;
			} else if( y == 4 ) {
				data = fonttex[c*2+1];
				mask = 1;
			}
			for( uint x = 0 ; x < 8 ; x++ ) {
				uint* px = fp + (cy*8+y)*512 + (cx*8+x);
				*px = (data&mask) ? FRONT_COLOR : BACK_COLOR;
				mask = mask << 1;
			}
		}
	}


	bool			console = false;
	int				cx, cy;

}



void
devcons::Init	(		)
{
	if( console )
		return;

	sceDisplaySetMode( SCE_DISPLAY_MODE_LCD, 480, 272 );
	sceDisplaySetFrameBuf( (void*)sceGeEdramGetAddr(), 512, SCE_DISPLAY_PIXEL_RGBA8888, SCE_DISPLAY_UPDATETIMING_NEXTVSYNC );

	_ClearAll();
	cx = cy = 0;
	console = true;
}


void
devcons::Shut	(		)
{
	if( !console )
		return;

	console = false;
}


void
devcons::Printf	(	const char* inFmt, ...	)
{
	if( !console || !inFmt )
		return;

	char str[256];
	va_list	arg;
	va_start( arg, inFmt );
	vsprintf( str, inFmt, arg );
	va_end( arg );

	char* pc = (char*) str;
	while( *pc ) {
		if( *pc=='\n' ) {
			cx = 0;
			cy++;
			pc++;
			continue;
		}
		if( cx==480/8 ) {
			cx = 0;
			cy++;
		}
		while( cy >= 272/8 ) {
			_ScrollUp();
			cy--;
		}
		_OutputChar( cx, cy, *pc );
		cx++;
		pc++;
	}

	printf( str );
	#if defined(_ATMON)
	// delay with psplink output !
	sceKernelDelayThread( 10 );
	#endif

	uint vsyncDelay = VSYNC_DELAY;
	while( vsyncDelay-- )
		sceDisplayWaitVblankStart();
}


void
devcons::Output	(	const char* inBuffer	)
{
	if( !console || !inBuffer )
		return;

	char* pc = (char*) inBuffer;
	while( *pc ) {
		if( *pc=='\n' ) {
			cx = 0;
			cy++;
			pc++;
			continue;
		}
		if( cx==480/8 ) {
			cx = 0;
			cy++;
		}
		while( cy >= 272/8 ) {
			_ScrollUp();
			cy--;
		}
		_OutputChar( cx, cy, *pc );
		cx++;
		pc++;
	}

	printf( inBuffer );
	#if defined(_ATMON)
	// delay with psplink output !
	sceKernelDelayThread( 10 );
	#endif
}





