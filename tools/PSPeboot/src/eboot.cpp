/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2005 AtOnce Technologies
** Copyright (C) 2001-2005 Gerald Gainant, All rights reserved.
**
** This file is part of the Nova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.nova-engine.com or email info@nova-engine.com for
** information about Nova CoreEngine and AtOnce Technologies others products License Agreements.
**
** Contact info@nova-engine.com if any conditions of this licensing are
** not clear to you.
**
** Revision: $Id: eboot.cpp 561 2006-06-09 19:35:44Z magicg $
**
*****************************************************************LIC-HDR*/


#include <stdio.h>
#include <psptypes.h>
#include <psperror.h>
#include <kernel.h>
#include <thread.h>
#include <displaysvc.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <stdarg.h>
#include "devcons.h"



#if defined(__MWERKS__)
extern "C" {
void mwInit(void);
void mwExit(void);
}
#endif


SCE_MODULE_INFO( NvEBOOT,
				 (SCE_MODULE_ATTR_EXCLUSIVE_LOAD|SCE_MODULE_ATTR_EXCLUSIVE_START),
				 1, 1 );

namespace
{

	char logText[128*64];
	char* logCur;

	void InitLog (	)
	{
		logCur = logText;
		*logCur = 0;
	}

	void AppendLog( const char* inFmt, ... )
	{
		va_list	arg;
		va_start( arg, inFmt );
		vsprintf( logCur, inFmt, arg );
		va_end( arg );
		logCur += strlen( logCur );
	}

	void OutputLog( )
	{
		devcons::Init();
		devcons::Output( logText );
		devcons::Shut();
	}



	char appPath[128];

	void BuildAppPath	(	char*	inArg0	)
	{
		char* p0 = strrchr( inArg0, '/' );
		char* p1 = strrchr( inArg0, '\\' );
		char* p  = (p0 && p0>p1) ? p0 : p1;
		memset( appPath, 0, sizeof(appPath) );
		if( p )
			memcpy( appPath, inArg0, p-inArg0+1 );
	}

	char* GetAppPath (	)
	{
		return appPath;
	}

	char* FindAppFile ( char* inFn )
	{
		static char appf[128];
		int fd = sceIoOpen( inFn, SCE_O_RDONLY , 0 );
		if( fd>=0 || !appPath[0] ) {
			sceIoClose( fd );
			strcpy( appf, inFn );
		} else {
			strcpy( appf, appPath );
			strcat( appf, inFn );
		}
		return appf;
	}

	SceUID load_module( const char* filename, int mode )	// 0:NULL 1:kernel 2:user
	{
		SceKernelLMOption lmMode;
		lmMode.size = sizeof(lmMode);
		lmMode.mpidtext = mode==1 ? SCE_KERNEL_PRIMARY_KERNEL_PARTITION : SCE_KERNEL_PRIMARY_USER_PARTITION;
		lmMode.mpiddata = mode==1 ? SCE_KERNEL_PRIMARY_KERNEL_PARTITION : SCE_KERNEL_PRIMARY_USER_PARTITION;
		lmMode.position = SCE_KERNEL_LMWO_POS_Low;
		lmMode.access   = SCE_KERNEL_LMWO_ACCESS_Noseek;
		SceUID mid = sceKernelLoadModule( filename, 0, mode?&lmMode:NULL );
		if( mid < 0 )
			return mid;
		int ret = sceKernelStartModule( mid, 0, NULL, NULL, NULL );
		if( ret < 0 )
			return ret;
		return mid;
	}

	int unload_module( SceUID mid )
	{
		int ret = sceKernelStopModule( mid, 0, NULL, NULL, NULL );
		if( ret < 0 )
			return ret;
		ret = sceKernelUnloadModule( mid );
		if( ret < 0 )
			return ret;
		return 0;
	}

	bool load_file( char* inFn, void*& outBuffer, uint& outBSize )
	{
		if( !inFn )
			return false;
		int fd = sceIoOpen( inFn, SCE_O_RDONLY , 0 );
		if( fd < 0 )
			return false;
		uint bsize = sceIoLseek( fd, 0, SCE_SEEK_END ) ;
		sceIoLseek( fd, 0, SCE_SEEK_SET );
		if( bsize == 0 )
			return false;
		void* data = malloc( bsize+1 );
		if( !data ) {
			sceIoClose( fd );
			return false;
		}
		uint readen = sceIoRead( fd, data, bsize );
		if( readen != bsize ) {
			free( data );
			sceIoClose( fd );
			return false;
		}
		sceIoClose( fd );
		((char*)data)[bsize] = 0;
		outBuffer = data;
		outBSize  = bsize;
		return true;
	}

	int get_string( char*& ioPtr, char* outS, bool* outNL = NULL )
	{
		if( outNL )
			*outNL = false;
		while( *ioPtr && *ioPtr<=32 ) {
			if( (*ioPtr=='\n' || *ioPtr=='\r') && outNL )
				*outNL = true;
			ioPtr++;
		}
		if( *ioPtr == 0 )
			return 0;
		int len = 0;
		while( *ioPtr>32 ) {
			*outS++ = *ioPtr++;
			len++;
		}
		*outS = 0;
		return len;
	}

	bool cmd_lsmod( char*& iniCur, int mode )		// 0:NULL 1:kernel 2:user
	{
		char modfile[80];
		if( !get_string(iniCur,modfile) ) {
			AppendLog( "ERROR: Command argument is missing !\n" );
			return false;
		}
		char* appModfile = FindAppFile( modfile );
		if( !appModfile || strlen(appModfile)==0 ) {
			AppendLog( "ERROR: Invalid module <%s> !\n", modfile );
			return false;
		}
		SceUID mid = load_module( appModfile, mode );
		if( mid > 0 ) {
			AppendLog( "Module <%s> loaded.\n", modfile );
			return true;
		}
		if( mid == SCE_KERNEL_ERROR_EXCLUSIVE_LOAD ) {
			AppendLog( "Module <%s> already loaded (%08xh).\n", modfile, mid );
			return true;
		} else {
			AppendLog( "ERROR: Module <%s> failed (%08xh) !\n", modfile, mid );
			return true;
		}
	}

}




int main( int argc, char ** argv )
{
	#if defined(_ATMON)
	// delay with psplink output !
	sceKernelDelayThread( 10 );
	sceKernelDelayThread( 10 );
	#endif

#if defined(__MWERKS__)
	mwInit();
#endif

	InitLog();
	BuildAppPath( argv[0] );

	AppendLog( " |\n" );
	AppendLog( " | AtOnce Technologies\n" );
	AppendLog( " | Bootstrap for Sony PSP\n" );
	AppendLog( " |\n" );
	AppendLog( "\n" );
	for( int i = 0 ; i < argc ; i++ )
		AppendLog( "[%d/%d] <%s>\n", i+1, argc, argv[i] );
	AppendLog( "Application path: <%s>\n", GetAppPath() );

	bool  errorFound = false;
	void* iniData;
	uint  iniBSize;
	char* initFile = FindAppFile( "eboot.ini" );
	if( !load_file(initFile,iniData,iniBSize) )
	{
		AppendLog( "ERROR: Failed to load .ini file <%s> !\n", initFile );
		errorFound = true;
	}
	else
	{
		AppendLog( "Loaded .ini file <%s> !\n", initFile );
		char* iniCur = (char*) iniData;
		for( ;; ) {
			char cmd[80];
			if( !get_string(iniCur,cmd) )
				break;

			bool cmdSuccess = false;
			if( stricmp(cmd,"lsmod")==0 )
				cmdSuccess = cmd_lsmod( iniCur, 0 );
			else if( stricmp(cmd,"lskmod")==0 )
				cmdSuccess = cmd_lsmod( iniCur, 1 );
			else if( stricmp(cmd,"lsumod")==0 )
				cmdSuccess = cmd_lsmod( iniCur, 2 );
			else
				AppendLog( "ERROR: Invalid cmd <%> !\n", cmd );

			if( !cmdSuccess ) {
				errorFound = true;
				break;
			}
		}
	}

	// error ? => display log
	if( errorFound )
		OutputLog();

#if defined(__MWERKS__)
	mwExit();
#endif

	return 0;
}


