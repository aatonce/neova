Notes au sujet de l'utilisation de VCL.
01 / 12 /2004
G�rald Gainant
VCL 1.4 beta3
---------------------------------------


- Output messages
	Message lpgen[N], N indique le nombre de cycle de la boucle en cours d'optimisation.
	La limitation (N min) est donn�e par le nombre d'instructions basses (ibne, iadd, ...),
	le nombre d'instructions hautes (mula, madd, ...) et les cycles de stalls P & Q.
	Le nombre de cycles N,M est donn� en r�sultat (N>=M). N est le nombre de cycles obtenu, M le nombre minimum de cycles.
	Si N=M, la boucle est optimale.


- Boucle traitant 1 seul vertex s'optimise mal, avec souvent N=M+2.
	Il vaut mieux traiter deux sommets par boucle, avec une recopie du code et un renommage des variables.
	La directive .vcl_loop_copy du processeur VUPP d'AtOnce Tech. permet cela :

			loop:
				--LoopCS 2,0
				lq			xyzk@(ptr)
				mulax		acc,   M0, xyzk@
				madday		acc,   M1, xyzk@
				maddaz		acc,   M2, xyzk@
				maddw.xyz	pxyq@, M3, vf00
				sqi			pxyq@(ptr++)
				.vcl_loop_copy
				ibne		ptr, ptr_end, loop

		Will output :

			loop:
				--LoopCS 2,0
				lq			xyzk_c0(ptr)
				mulax		acc,   M0, xyzk_c0
				madday		acc,   M1, xyzk_c0
				maddaz		acc,   M2, xyzk_c0
				maddw.xyz	pxyq_c0,M3, vf00
				sqi			pxyq_c0,(ptr++)
	
				lq			xyzk_c1(ptr)
				mulax		acc,   M0, xyzk_c1
				madday		acc,   M1, xyzk_c1
				maddaz		acc,   M2, xyzk_c1
				maddw.xyz	pxyq_c1,M3, vf00
				sqi			pxyq_c1,(ptr++)
				ibne		ptr, ptr_end, loop


- Directive --LoopCS n, m
	n = nombre minimum d'it�ration de la boucle.
			Le nombre de sommets calcul�s au minimum est n0 = n * Nombre-de-sommet-calcul�-dans-la-boucle.
			n0 sommets seront calcul�s au minimum, donc attention au test de sortie de la boucle (ibne ptr,ptrEnd,loop) !
	m = nombre d'it�ration pouvant �tre effectu�e en avance.
			Pr�voir m * Nombre-de-sommet-calcul�-dans-la-boucle en fin de buffer de sortie pour pr�venir d'un �crasement m�moire.

	Ex: on consid�re une boucle de calcul de 2 sommets.
		--LoopCS 1, 0
			Calcul de 1*2 sommets au minimum. VCL g�n�re 2 �pilogues de code VU pour ins�rer les conditions avant la boucle principale.
		--LoopCS 2, 0
			Calcul de 2*2 sommets au minimum. VCL g�n�re 1 seul �pilogue de code VU (-40% de code VU environ).
		--LoopCS 3, 0
			Calcul de 3*2 sommets au minimum. R�sultat souvent �gal � n=2 avec 2 sommets par boucle.

	Choix de n :
			n (1..3) ne change pas l'optimisation de la boucle principale, mais rajoute du code VU pour les tests de sortie (�pilogues).
			ATTENTION!! : n impose qu'un nombre minimum de calcul seront effectu�s avant de rencontrer le premier test.
			Par exemple, si n=2 et que la boucle calcul 2 sommets, au total 4 sommets seront calcul�s avant le premier test
			de sortie. Donc attention � la condition de test si elle est du type ibne ptr, ptrEnd, loop.
			Dans ce cas, le calcul de ptrEnd doit prendre en compte :
				1/ Arrondi du nombre de calcul � 2 sommets par boucle (multiple de 2)
				2/ Arrondi du nombre de calcul � au moins 4 sommets pour valider le premier test.

	Choix de m :
			m (0..3) permet � VCL d'optimiser plus ou moins le code de la boucle. Un padding m�moire en fin de buffer
			de sortie doit �tre pris en compte. Le meilleur moyen pour fixer m est de tester en commen�ant par 0 et de voir
			le r�sultat de l'optimisation de la boucle. Si {lpgen[N] LoopName N,M} avec N=M, alors la boucle est optimale.

	Par d�fault :
		1/ Calculer au moins 2 sommets par boucle (.vcl_loop_copy)
		2/ Utiliser --LoopCS 2, 0
		3/ Jeter un oeil au code .vsm g�n�r�

	Probl�me connu :
		Avec n>1, un minimum de sommets seront toujours calcul�s avant le test de sortie. Si le nombre de sommets
		� calculer est inf�rieur � ce nombre minimum calcul�, cela peut avoir des effets sur les variables
		d'�tat communes � plusieurs sommets, comme un historique ADC par exemple.
		Cependant, cela se produit plut�t en fin de *surface*, donc pas vraiment g�nant.


- D�correlation des entr�es / calcul / sorties
		Un sommet poss�de 3 �tats de traitement :
			1/ Sa lecture (lq)
			2/ Son calcul
			3/ Son �criture (sq)
		VCL est efficace quand des variables diff�rentes sont utilis�es pour chacune de ces �tapes.
		Par exemple :
			1/ Sa lecture   (lq   r_xyz)
			2/ Son calcul   (madd p_xyz)
			3/ Son �criture (sq   s_xyz)
		De toute fa�on, VCL optimise le jeu des registres utilis�s.
		Donc � tester sans soucis, cela peut faire gagner quelques cycles.


