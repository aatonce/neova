/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_LibC_H_
#define	_NvCore_LibC_H_




namespace nv { namespace libc
{
	enum FnmatchFlags
	{
		FNM_NOESCAPE	= 0x01,		// Disable backslash escaping.
		FNM_PATHNAME	= 0x02,		// Slash must be matched by slash.
		FNM_PERIOD		= 0x04,		// Period must be matched by period.
		FNM_LEADING_DIR	= 0x08,		// Ignore /<tail> after Imatch.
		FNM_CASEFOLD	= 0x10,		// Case insensitive search.
		FNM_IGNORECASE	= FNM_CASEFOLD,
		FNM_FILE_NAME	= FNM_PATHNAME|FNM_NOESCAPE|FNM_IGNORECASE
	};

	void*				Memset			(	void* inPtr, int inValue, uint32 inSize				);
	void*				Memcpy			(	void* inDst, void* inSrc, uint32 inSize				);
	void*				Memncpy			(	void* inDst, void* inSrc, uint32 inSize, uint inCpt	);
	void*				Memmove			(	void* inDst, void* inSrc, uint32 inSize				);
	int					Memcmp			(	void* inP0,  void* inP1,  uint   inSize				);
	int					Memcmp			(	void* inPtr, int inValue, uint32 inSize				);

	bool				IsPacked		(	void* inSrc, uint inLen								);
	bool				GetUnpackBSize	(	void* inSrc, uint inLen, uint& outLen				);
	bool				Unpack			(	void* inSrc, uint inLen, void* inDst				);		// inDst is at least GetUnpackBSize() bytes !
	bool				Pack			(	void* inSrc, uint inLen, void* inDst, uint& outLen	);		// inDst is at least inSrcBSize bytes !

	int					Toupper			(	int   c												);
	int					Tolower			(	int	  c												);

	void*				Zero			(	void* inDst, uint32 inSize							);
	bool				CheckZero		(	void* inDst, uint32 inSize							);
	template <typename inC> inline
	void*				Zero			(	inC & inV											) { return Zero(&inV,sizeof(inC)); }
	template <typename inC> inline
	bool				CheckZero		(	inC & inV											) { return CheckZero(&inV,sizeof(inC)); }

	pstr				Strcpy			(	pstr  inDest,  pcstr inSrc							);
	pstr				Strncpy			(	pstr  inDest,  pcstr inSrc, uint count				);
	pstr				Strcat			(	pstr  inDest,  pcstr inSrc							);
	uint				Strlen			(	pcstr inString										);
	int					Strcmp			(	pcstr string1, pcstr string2						);
	int					Strncmp			(	pcstr string1, pcstr string2, uint count			);
	int					Strnicmp		(	pcstr string1, pcstr string2, uint count			);
	int					Stricmp			(	pcstr string1, pcstr string2						);
	pstr				Strchr			(	pcstr inString, int inC								);
	pstr				Strrchr			(	pcstr inString, int inC								);
	uint				Strcspn			(	pcstr inString, pcstr inCharSet						);
	uint				Strspn			(	pcstr inString, pcstr inCharSet						);
	pcstr				Strpbrk			(	pcstr inString, pcstr inCharSet 					);
	pstr				Strupr			(	pstr  inString 										);
	pstr				Strlwr			(	pstr  inString 										);
	float				Strtof			(	pcstr inString,  pstr* endPtr						);
	long				Strtol			(	pcstr inString,  pstr* endPtr, int base				);
	ulong				Strtoul			(	pcstr inString,  pstr* endPtr, int base				);

	bool				Fnmatch			(	pcstr inPattern, pcstr inString, uint inFlags		);		// FnmatchFlags

	void				Printf			(	pcstr inFmt, ...									);
	int					Sprintf			( 	pstr  inDst,   pcstr inFmt, ...						);
	pcstr				Tsprintf		( 	pcstr inFmt, ...									);
	void				FlushPrintf		(														);
	void				DumpData		(	uint32 inUID, void* inPtr, uint inSize				);

	bool				RandRestore		(	pcstr		inRandState								);		// 5 uint32 : "uint32 uint32 uint32 uint32 uint32" 
	bool				RandSave		(	pstr		outRandStatea							);		// an allocated buffer with 256 chars.
	uint32				Rand			(	uint32		inM0=0, uint inM1=0xFFFFFF				);		// in [M0,M1]
	float				Randf			(														);		// in [0,+1]
	float				Randsf			(														);		// in [-1,+1]
} }



#ifndef	_NvCore_LibC_NO_GLOBAL_
using namespace nv::libc;
#endif	// _NvCore_LibC_NO_GLOBAL_


#ifdef		_NVCOMP_ENABLE_DBG
#define		DebugPrintf		Printf
#else
inline void	NULL_Printf( pcstr, ... )		{	}
#define		DebugPrintf		NULL_Printf
#endif



#endif	// _NvCore_LibC_H_


