/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_CRC_H_
#define	_NvCore_CRC_H_




namespace nv { namespace crc
{

	//
	// Binary -> CRC

	uint32			Get		(	pvoid		inBuffer,
								uint32		inBSize		);

	//
	// String Path -> CRC
	//
	// Path is normalised to allow hashed file-path to be compare :
	// 1. lowercase -> uppercase
	// 2. '\' -> '/'
	// 3. multiple '/' are reduce to one '/'

	uint32			Get		(	pcstr		inPath		);

} }



#define	CRC( _STRING )		( ::nv::crc::Get(_STRING) )



#endif	// _NvCore_CRC_H_



