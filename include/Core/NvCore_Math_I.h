/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _NvCore_Math_H_




//--------------------------
// Math Fct
//--------------------------

namespace nv { namespace math
{

	template < typename T >
	inline T Min ( const T & inA, const T & inB )
	{
		return ( inA < inB ? inA : inB );
	}

	template < typename T >
	inline T Max ( const T & inA, const T & inB )
	{
		return ( inA > inB ? inA : inB );
	}

	template < typename T >
	inline T Clamp( const T & inV, const T & inMin, const T & inMax )
	{
		return Min( Max( inV, inMin ) , inMax );
	}




	//--------------------------
	// 2D Vector
	//--------------------------

	inline
	Vec2::Vec2(	const float * inFP )
	{
		x = inFP[0];
		y = inFP[1];
	}

	inline
	Vec2::Vec2(	const Vec2 & inV )
	{
		Vec2Copy( this, &inV );
	}

	inline
	Vec2::Vec2(	const Vec3 & inV )
	{
		x = inV.x;
		y = inV.y;
	}

	inline
	Vec2::Vec2(	const Vec4 & inV )
	{
		x = inV.x;
		y = inV.y;
	}

	inline
	Vec2::Vec2(	const Quat & inQ )
	{
		x = inQ.x;
		y = inQ.y;
	}

	inline
	Vec2::Vec2(	float inX, float inY )
	{
		x = inX;
		y = inY;
	}

	inline
	Vec2&
	Vec2::Set	(	float inX,	float inY		)
	{
		x = inX;
		y = inY;
		return *this;
	}

	inline
	Vec2 &
	Vec2::operator = (	const Vec2 & inV )
	{
		Vec2Copy( this, &inV );
		return *this;
	}

	inline
	Vec2 &
	Vec2::operator += (	const Vec2 & inV )
	{
		Vec2Add( this, this, &inV );
		return *this;
	}

	inline
	Vec2 &
	Vec2::operator -= (	const Vec2 & inV )
	{
		Vec2Sub( this, this, &inV );
		return *this;
	}

	inline
	Vec2 &
	Vec2::operator *= (	float inF )
	{
		Vec2Scale( this, this, inF );
		return *this;
	}

	inline
	Vec2 &
	Vec2::operator /= (	float inF )
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		Vec2Scale( this, this, ooF );
		return *this;
	}

	inline
	Vec2
	Vec2::operator + (	) const
	{
		return *this;
	}

	inline
	Vec2
	Vec2::operator - (	) const
	{
		return Vec2	( -x, -y );
	}

	inline
	Vec2
	Vec2::operator + (	const Vec2 & inV ) const
	{
		return Vec2	( x+inV.x, y+inV.y );
	}

	inline
	Vec2
	Vec2::operator - (	const Vec2 & inV ) const
	{
		return Vec2	( x-inV.x, y-inV.y );
	}

	inline
	float
	Vec2::operator * (	const Vec2 & inV ) const
	{
		return Vec2Dot( this, (const Vec2*)&inV );
	}

	inline
	float
	Vec2::operator ^ (	const Vec2 & inV ) const
	{
		return Vec2Cross( this, (const Vec2*)&inV );
	}

	inline
	Vec2
	Vec2::operator * (	float inF ) const
	{
		return Vec2	( x*inF, y*inF );
	}

	inline
	Vec2
	Vec2::operator / (	float inF ) const
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		return Vec2( x*ooF, y*ooF );
	}

	inline
	Vec2
	operator * ( float inF, const Vec2 & inV	)
	{
		return Vec2	( inV.x*inF, inV.y*inF );
	}

	inline
	bool
	Vec2::operator == (	const Vec2 & inV ) const
	{
		return ( (x==inV.x) && (y==inV.y) );
	}

	inline
	bool
	Vec2::operator != (	const Vec2 & inV ) const
	{
		return ( (x!=inV.x) || (y!=inV.y) );
	}




	//--------------------------
	// 3D Vector
	//--------------------------

	inline
	Vec3::Vec3(	const float * inFP )
	{
		x = inFP[0];
		y = inFP[1];
		z = inFP[2];
	}

	inline
	Vec3::Vec3(	const Vec3 & inV )
	{
		Vec3Copy( this, &inV );
	}

	inline
	Vec3::Vec3(	const Vec2 & inV )
	{
		x = inV.x;
		y = inV.y;
		z = 0.0f;
	}

	inline
	Vec3::Vec3(	const Vec4 & inV )
	{
		x = inV.x;
		y = inV.y;
		z = inV.z;
	}

	inline
	Vec3::Vec3(	const Quat & inQ )
	{
		x = inQ.x;
		y = inQ.y;
		z = inQ.z;
	}

	inline
	Vec3::Vec3(	float inX, float inY, float inZ )
	{
		x = inX;
		y = inY;
		z = inZ;
	}

	inline
	Vec3&
	Vec3::Set	(	float inX,	float inY, float inZ	)
	{
		x = inX;
		y = inY;
		z = inZ;
		return *this;
	}

	inline
	Vec3 &
	Vec3::operator = (	const Vec3 & inV )
	{
		Vec3Copy( this, &inV );
		return *this;
	}

	inline
	Vec3 &
	Vec3::operator += (	const Vec3 & inV )
	{
		Vec3Add( this, this, &inV );
		return *this;
	}

	inline
	Vec3 &
	Vec3::operator -= (	const Vec3 & inV )
	{
		Vec3Sub( this, this, &inV );
		return *this;
	}

	inline
	Vec3 &
	Vec3::operator *=	(	const Matrix & inM	)
	{
		Vec3Apply( this, this, &inM );
		return *this;
	}

	inline
	Vec3 &
	Vec3::operator *=	(	const Quat & inQ	)
	{
		Vec3Apply( this, this, &inQ );
		return *this;
	}

	inline
	Vec3 &
	Vec3::operator *= (	float inF )
	{
		Vec3Scale( this, this, inF );
		return *this;
	}

	inline
	Vec3 &
	Vec3::operator /= (	float inF )
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		Vec3Scale( this, this, ooF );
		return *this;
	}

	inline
	Vec3
	Vec3::operator + (	) const
	{
		return *this;
	}

	inline
	Vec3
	Vec3::operator - (	) const
	{
		return Vec3	( -x, -y, -z );
	}

	inline
	Vec3
	Vec3::operator + (	const Vec3 & inV ) const
	{
		Vec3 v;
		Vec3Add( &v, this, &inV );
		return v;
	}

	inline
	Vec3
	Vec3::operator - (	const Vec3 & inV ) const
	{
		Vec3 v;
		Vec3Sub( &v, this, &inV );
		return v;
	}

	inline
	Vec3
	Vec3::operator ^ (	const Vec3 & inV ) const
	{
		Vec3 v;
		Vec3Cross( &v, this, &inV );
		return v;
	}

	inline
	float
	Vec3::operator * (	const Vec3 & inV ) const
	{
		return Vec3Dot( this, &inV );
	}

	inline
	Vec3
	Vec3::operator * (	const Matrix & inM	) const
	{
		Vec3 v;
		Vec3Apply( &v, this, &inM );
		return v;
	}

	inline
	Vec3
	Vec3::operator * (	const Quat & inQ ) const
	{
		Vec3 v;
		Vec3Apply( &v, this, &inQ );
		return v;
	}

	inline
	Vec3
	Vec3::operator * (	float inF ) const
	{
		Vec3 v;
		Vec3Scale( &v, this, inF );
		return v;
	}

	inline
	Vec3
	Vec3::operator / (	float inF ) const
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		return Vec3( x*ooF, y*ooF, z*ooF );
	}

	inline
	Vec3
	operator * ( float inF, const Vec3 & inV	)
	{
		Vec3 v;
		Vec3Scale( &v, &inV, inF );
		return v;
	}

	inline
	bool
	Vec3::operator == (	const Vec3 & inV ) const
	{
		return ( x==inV.x && y==inV.y && z==inV.z );
	}

	inline
	bool
	Vec3::operator != (	const Vec3 & inV ) const
	{
		return ( x!=inV.x || y!=inV.y || z!=inV.z );
	}




	//--------------------------
	// 4D Vector
	//--------------------------

	inline
	Vec4::Vec4(	const float * inFP )
	{
		x = inFP[0];
		y = inFP[1];
		z = inFP[2];
		w = inFP[3];
	}

	inline
	Vec4::Vec4(	const Vec4 & inV )
	{
		Vec4Copy( this, &inV );
	}

	inline
	Vec4::Vec4(	const Vec3 & inV )
	{
		x = inV.x;
		y = inV.y;
		z = inV.z;
		w = 1.0f;
	}

	inline
	Vec4::Vec4(	const Vec2 & inV )
	{
		x = inV.x;
		y = inV.y;
		z = 0.0f;
		w = 1.0f;
	}

	inline
	Vec4::Vec4(	const Quat & inQ )
	{
		x = inQ.x;
		y = inQ.y;
		z = inQ.z;
		w = inQ.w;
	}

	inline
	Vec4::Vec4(	float inX, float inY, float inZ, float inW )
	{
		x = inX;
		y = inY;
		z = inZ;
		w = inW;
	}

	inline
	Vec4&
	Vec4::Set	(	float inX,	float inY, float inZ, float inW	)
	{
		x = inX;
		y = inY;
		z = inZ;
		w = inW;
		return *this;
	}

	inline
	Vec4 &
	Vec4::operator = (	const Vec4 & inV )
	{
		Vec4Copy( this, &inV );
		return *this;
	}

	inline
	Vec4 &
	Vec4::operator += (	const Vec4 & inV )
	{
		Vec4Add( this, this, &inV );
		return *this;
	}

	inline
	Vec4 &
	Vec4::operator -= (	const Vec4 & inV )
	{
		Vec4Sub( this, this, &inV );
		return *this;
	}

	inline
	Vec4 &
	Vec4::operator *=	(	const Matrix & inM	)
	{
		Vec4Apply( this, this, &inM );
		return *this;
	}

	inline
	Vec4 &
	Vec4::operator *=	(	const Quat & inQ	)
	{
		Vec4Apply( this, this, &inQ );
		return *this;
	}

	inline
	Vec4 &
	Vec4::operator *= (	float inF )
	{
		Vec4Scale( this, this, inF );
		return *this;
	}

	inline
	Vec4 &
	Vec4::operator /= (	float inF )
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		Vec4Scale( this, this, ooF );
		return *this;
	}

	inline
	Vec4
	Vec4::operator + (	) const
	{
		return *this;
	}

	inline
	Vec4
	Vec4::operator - (	) const
	{
		return Vec4	( -x, -y, -z, -w );
	}

	inline
	Vec4
	Vec4::operator + (	const Vec4 & inV ) const
	{
		return Vec4	( x+inV.x, y+inV.y, z+inV.z, w+inV.w );
	}

	inline
	Vec4
	Vec4::operator - (	const Vec4 & inV ) const
	{
		return Vec4	( x-inV.x, y-inV.y, z-inV.z, w-inV.w );
	}

	inline
	float
	Vec4::operator * (	const Vec4 & inV ) const
	{
		return Vec4Dot( this, &inV );
	}

	inline
	Vec4
	Vec4::operator * (	const Matrix & inM	) const
	{
		Vec4 v;
		Vec4Apply( &v, this, &inM );
		return v;
	}

	inline
	Vec4
	Vec4::operator * (	const Quat & inQ ) const
	{
		Vec4 v;
		Vec4Apply( &v, this, &inQ );
		return v;
	}

	inline
	Vec4
	Vec4::operator * (	float inF ) const
	{
		return Vec4	( x*inF, y*inF, z*inF, w*inF );
	}

	inline
	Vec4
	Vec4::operator / (	float inF ) const
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		return Vec4( x*ooF, y*ooF, z*ooF, w*ooF );
	}

	inline
	Vec4
	operator * ( float inF, const Vec4 & inV	)
	{
		return Vec4( inV.x*inF, inV.y*inF, inV.z*inF, inV.w*inF	);
	}

	inline
	bool
	Vec4::operator == (	const Vec4 & inV ) const
	{
		return ( (x==inV.x) && (y==inV.y) && (z==inV.z) && (w==inV.w) );
	}

	inline
	bool
	Vec4::operator != (	const Vec4 & inV ) const
	{
		return ( (x!=inV.x) || (y!=inV.y) || (z!=inV.z) || (w!=inV.w) );
	}

	inline
	uint
	Vec4::Clip	(		)	const
	{
		return Vec4Clip( this );
	}



	//--------------------------
	// 4D Matrix
	//--------------------------

	inline
	Matrix::Matrix(	const float * inFP )
	{
		m11=inFP[0];  m12=inFP[1];  m13=inFP[2];  m14=inFP[3];
		m21=inFP[4];  m22=inFP[5];  m23=inFP[6];  m24=inFP[7];
		m31=inFP[8];  m32=inFP[9];  m33=inFP[10]; m34=inFP[11];
		m41=inFP[12]; m42=inFP[13]; m43=inFP[14]; m44=inFP[15];
	}

	inline
	Matrix::Matrix	(	const Matrix &	inM )
	{
		MatrixCopy( this, &inM );
	}

	inline
	Matrix::Matrix	(	float in11, float in12, float in13, float in14,
						float in21, float in22, float in23, float in24,
						float in31, float in32, float in33, float in34,
						float in41, float in42, float in43, float in44	)
	{
		m11=in11; m12=in12; m13=in13; m14=in14;
		m21=in21; m22=in22; m23=in23; m24=in24;
		m31=in31; m32=in32; m33=in33; m34=in34;
		m41=in41; m42=in42; m43=in43; m44=in44;
	}

	inline
	Matrix::Matrix ( const Quat & inQ )
	{
		MatrixRotation( this, &inQ );
	}

	inline
	Matrix::Matrix ( const Vec3 & inV )
	{
		MatrixIdentity( this );
		m41 = inV.x;
		m42 = inV.y;
		m43 = inV.z;
	}

	inline
	float&
	Matrix::operator ()	(	int Row, int Col		)
	{
		return m[Row][Col];
	}

	inline
	float
	Matrix::operator () (	int Row, int Col		) const
	{
		return m[Row][Col];
	}

	inline
	Matrix&
	Matrix::operator =	(	const Matrix & inM )
	{
		MatrixCopy( this, &inM );
		return *this;
	}

	inline
	Matrix&
	Matrix::operator *=	(	const Matrix & inM )
	{
		MatrixMul( this, this, &inM );
		return *this;
	}

	inline
	Matrix&
	Matrix::operator +=	(	const Matrix & inM )
	{
		MatrixAdd( this, this, &inM );
		return *this;
	}

	inline
	Matrix&
	Matrix::operator -=	(	const Matrix & inM )
	{
		MatrixSub( this, this, &inM );
		return *this;
	}

	inline
	Matrix&
	Matrix::operator *=	(	float inF )
	{
		MatrixScalar( this, this, inF );
		return *this;
	}

	inline
	Matrix&
	Matrix::operator /=	(	float inF	)
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		MatrixScalar( this, this, ooF );
		return *this;
	}

	inline
	Matrix
	Matrix::operator +	(	) const
	{
		return *this;
	}

	inline
	Matrix
	Matrix::operator -	(	) const
	{
		Matrix m;
		MatrixScalar( &m, this, -1.f );
		return m;		
	}

	inline
	Matrix
	Matrix::operator *	(	const Matrix & inM	) const
	{
		Matrix m;
		MatrixMul( &m, this, &inM );
		return m;
	}

	inline
	Matrix
	Matrix::operator +	(	const Matrix & inM ) const
	{
		Matrix m;
		MatrixAdd( &m, this, &inM );
		return m;
	}

	inline
	Matrix
	Matrix::operator -	(	const Matrix & inM	) const
	{
		Matrix m;
		MatrixSub( &m, this, &inM );
		return m;
	}

	inline
	Matrix
	Matrix::operator *	(	float inF ) const
	{
		Matrix m;
		MatrixScalar( &m, this, inF );
		return m;		
	}

	inline
	Matrix
	Matrix::operator /	(	float inF ) const
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		Matrix m;
		MatrixScalar( &m, this, ooF );
		return m;		
	}

	inline
	Matrix
	operator * ( float inF, const Matrix & inM	)
	{
		Matrix m;
		MatrixScalar( &m, &inM, inF );
		return m;		
	}

	inline
	bool
	Matrix::operator == (	const Matrix & inM	) const
	{
		return Memcmp( (pvoid)this, (pvoid)&inM, sizeof(Matrix)) == 0;
	}

	inline
	bool
	Matrix::operator != (	const Matrix & inM	) const
	{
		return Memcmp( (pvoid)this, (pvoid)&inM, sizeof(Matrix)) != 0;
	}

	inline
	Vec3*
	Matrix::right		(						) const
	{
		return (Vec3*)&m11;
	}

	inline
	Vec3*
	Matrix::up			(						) const
	{
		return (Vec3*)&m21;
	}

	inline
	Vec3*
	Matrix::front		(						) const
	{
		return (Vec3*)&m31;
	}

	inline
	Vec3*
	Matrix::location	(						) const
	{
		return (Vec3*)&m41;
	}



	//--------------------------
	// Quaternion
	//--------------------------

	inline
	Quat::Quat(	const float * inFP	)
	{
		x = inFP[0];
		y = inFP[1];
		z = inFP[2];
		w = inFP[3];
	}

	inline
	Quat::Quat(	const Quat & inQ	)
	{
		QuatCopy( this, &inQ );
	}

	inline
	Quat::Quat	(	float inX, float inY, float inZ, float inW		)
	{
		x = inX;
		y = inY;
		z = inZ;
		w = inW;
	}

	inline
	Quat::Quat(	float theta, const Vec3 & inU	)
	{
		QuatFromAxisAngle( this, &inU, theta );
	}

	inline
	Quat::Quat(	const Vec2 & inV	)
	{
		x = inV.x;
		y = inV.y;
		z = 0.0f;
		w = 0.0f;
	}

	inline
	Quat::Quat(	const Vec3 & inV	)
	{
		x = inV.x;
		y = inV.y;
		z = inV.z;
		w = 0.0f;
	}

	inline
	Quat::Quat( const Vec4 & inV )
	{
		x = inV.x;
		y = inV.y;
		z = inV.z;
		w = inV.w;
	}

	inline
	Quat::Quat (	const Matrix & inM )
	{
		QuatFromMatrix( this, &inM );
	}

	inline
	Quat&
	Quat::Set	(	float x, float y, float z, float w		)
	{
		x = x;
		y = y;
		z = z;
		w = w;
		return *this;
	}

	inline
	Quat&
	Quat::operator =	(	const Quat & inQ	)
	{
		QuatCopy( this, &inQ );
		return *this;
	}

	inline
	Quat&
	Quat::operator *=	(	const Quat & inQ	)
	{
		QuatMul( this, this, &inQ );
		return *this;
	}

	inline
	Quat&
	Quat::operator +=	(	const Quat & inQ	)
	{
		x += inQ.x;
		y += inQ.y;
		z += inQ.z;
		w += inQ.w;
		return *this;
	}

	inline
	Quat&
	Quat::operator -=	(	const Quat & inQ	)
	{
		x -= inQ.x;
		y -= inQ.y;
		z -= inQ.z;
		w -= inQ.w;
		return *this;
	}

	inline
	Quat&
	Quat::operator *=	(	float inF	)
	{
		x *= inF;
		y *= inF;
		z *= inF;
		w *= inF;
		return *this;
	}

	inline
	Quat&
	Quat::operator /=	(	float inF	)
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		x *= ooF;
		y *= ooF;
		z *= ooF;
		w *= ooF;
		return *this;
	}

	inline
	Quat
	Quat::operator +	(	) const
	{
		return *this;
	}

	inline
	Quat
	Quat::operator -	(	) const
	{
		return Quat( -x, -y, -z ,-w );
	}

	inline
	Quat
	Quat::operator *	(	const Quat & inQ	) const
	{
		Quat q;
		QuatMul( &q, this, &inQ );
		return q;
	}

	inline
	Quat
	Quat::operator +	(	const Quat & inQ	) const
	{
		return Quat( x+inQ.x, y+inQ.y, z+inQ.z, w+inQ.w );
	}

	inline
	Quat
	Quat::operator -	(	const Quat & inQ	) const
	{
		return Quat( x-inQ.x, y-inQ.y, z-inQ.z, w-inQ.w );
	}

	inline
	Quat
	Quat::operator *	(	float inF	) const
	{
		return Quat( x*inF, y*inF, z*inF, w*inF );
	}

	inline
	Quat
	Quat::operator /	(	float inF 	) const
	{
		NV_ASSERT( inF != 0.0f );
		float ooF = 1.0f / inF;
		return Quat( x*ooF, y*ooF, z*ooF, w*ooF );
	}

	inline
	Quat
	operator * ( float inF, const Quat & inQ )
	{
		return Quat( inF*inQ.x, inF*inQ.y, inF*inQ.z, inF*inQ.w );
	}

	inline
	bool
	Quat::operator == (	const Quat & inQ	) const
	{
		return ( x==inQ.x && y==inQ.y && inQ.z==z && inQ.w==w );
	}

	inline
	bool
	Quat::operator != (	const Quat & inQ	) const
	{
		return ( x!=inQ.x || y!=inQ.y || inQ.z!=z || inQ.w!=w );
	}



	//--------------------------
	// 3D Box
	//--------------------------


	inline
	Box3::Box3(	const Box3 & inB )
	{
		Box3Copy( this, &inB );
	}


	inline
	Box3::Box3(	const Sph3 & inS )
	{
		center		= inS.center;
		rotation	= Quat::UNIT;
		float r		= inS.radius * 2.0f;
		length		= Vec3( r, r, r );
	}


	inline
	Box3 &
	Box3::operator = (	const Box3 & inB	)
	{
		Box3Copy( this, &inB );
		return *this;
	}


	inline
	Box3 &
	Box3::operator += (	const Box3 & inB	)
	{
		Box3Concat( this, this, &inB );
		return *this;
	}


	inline
	Box3
	Box3::operator +	(	const Box3 &	inB	) const
	{
		Box3 b;
		Box3Concat( &b, this, &inB );
		return b;
	}


	inline
	Box3&
	Box3::operator *=	(	const Matrix &	inM		)
	{
		Box3Apply( this, this, &inM );
		return *this;
	}


	inline
	Box3
	Box3::operator *	(	const Matrix &	inM		) const
	{
		Box3 b;
		Box3Apply( &b, this, &inM );
		return b;
	}


	inline
	bool
	Box3::operator == (	const Box3 &	inB		) const
	{
		return		( inB.center	== center	)
				&&	( inB.rotation	== rotation )
				&&	( inB.length	== length	);
	}


	inline
	bool
	Box3::operator != (	const Box3 & inB	) const
	{
		return		( inB.center	!= center	)
				||	( inB.rotation	!= rotation )
				||	( inB.length	!= length	);
	}




	//--------------------------
	// 3D Sphere
	//--------------------------

	inline
	Sph3::Sph3(	const Sph3 & inS )
	{
		Sph3Copy( this, &inS );
	}


	inline
	Sph3::Sph3(	const Box3 & inB )
	{
		center	= inB.center;
		radius	= inB.length.x;
		radius  = inB.length.y > radius ? inB.length.y : radius;
		radius  = inB.length.z > radius ? inB.length.z : radius;
		radius *= 0.5f;
	}


	inline
	Sph3 &
	Sph3::operator = (	const Sph3 & inS	)
	{
		Sph3Copy( this, &inS );
		return *this;
	}


	inline
	Sph3 &
	Sph3::operator += (	const Sph3 & inS	)
	{
		Sph3Concat( this, this, &inS );
		return *this;
	}


	inline
	Sph3
	Sph3::operator +	(	const Sph3 &	inS	) const
	{
		Sph3 s;
		Sph3Concat( &s, this, &inS );
		return s;
	}


	inline
	Sph3&
	Sph3::operator *=	(	const Matrix &	inM		)
	{
		Sph3Apply( this, this, &inM );
		return *this;
	}


	inline
	Sph3
	Sph3::operator *	(	const Matrix &	inM		) const
	{
		Sph3 s;
		Sph3Apply( &s, this, &inM );
		return s;
	}


	inline
	bool
	Sph3::operator ==	(	const Sph3 &	inS		) const
	{
		return		( inS.center	== center	)
				&&	( inS.radius	== radius	);
	}


	inline
	bool
	Sph3::operator !=	(	const Sph3 &	inS		) const
	{
		return		( inS.center	!= center	)
				||	( inS.radius	!= radius	);
	}



	//--------------------------
	// 3D Frustum
	//--------------------------

	inline
	Frustum3::Frustum3(	const Frustum3 & inF )
	{
		Frustum3Copy( this, &inF );
	}

	inline
	Frustum3::Frustum3(	const Box3 & inB )
	{
		Frustum3Build( this, &inB );
	}

	inline
	Frustum3::Frustum3(	const Sph3 & inS )
	{
		Frustum3Build( this, &inS );
	}

	inline
	Frustum3::Frustum3(	const Matrix & inM )
	{
		Frustum3Build( this, &inM );
	}

	inline
	bool
	Frustum3::IsInside	(	const Box3 & inB ) const
	{
		float hw = inB.length.x * 0.5f;
		float hh = inB.length.y * 0.5f;
		float hd = inB.length.z * 0.5f;
		Vec3 l;
		uint clipAnd = 0x3F;
		l = Vec3( -hw, -hh, -hd ) * inB.rotation + inB.center;
		clipAnd &= Frustum3Clip( this, &l );
		l = Vec3( +hw, -hh, -hd ) * inB.rotation + inB.center;
		clipAnd &= Frustum3Clip( this, &l );
		l = Vec3( +hw, -hh, +hd ) * inB.rotation + inB.center;
		clipAnd &= Frustum3Clip( this, &l );
		l = Vec3( -hw, -hh, +hd ) * inB.rotation + inB.center;
		clipAnd &= Frustum3Clip( this, &l );
		l = Vec3( -hw, +hh, -hd ) * inB.rotation + inB.center;
		clipAnd &= Frustum3Clip( this, &l );
		l = Vec3( +hw, +hh, -hd ) * inB.rotation + inB.center;
		clipAnd &= Frustum3Clip( this, &l );
		l = Vec3( +hw, +hh, +hd ) * inB.rotation + inB.center;
		clipAnd &= Frustum3Clip( this, &l );
		l = Vec3( -hw, +hh, +hd ) * inB.rotation + inB.center;
		clipAnd &= Frustum3Clip( this, &l );
		// All outside ?
		return clipAnd ? FALSE : TRUE;
	}

	inline
	bool
	Frustum3::IsInside	(	const Sph3 & inS ) const
	{
		return Frustum3Inside( this, &inS.center, inS.radius );
	}

	inline
	bool
	Frustum3::IsInside	(	const Vec3 & inV ) const
	{
		return Frustum3Inside( this, &inV );
	}

	inline
	bool
	Frustum3::IsInside	(	const Vec4 & inV ) const
	{
		return Frustum3Inside( this, &inV );
	}

} }


#endif // _NvCore_Math_H_


