/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_H_
#define	_NvCore_H_




namespace nv { namespace core
{

	//
	// Core Parameters

	enum Parameter
	{
		PR_SYS_OS				= 0,	// [pcstr]		OS name
		PR_SYS_CMDLINE,					// [pcstr]		Application command line
		PR_SYS_CWD,						// [pcstr]		Application current working directory (a boot time)
		PR_EVAL_KEY,					// [pcstr]

		// DMM
		PR_MEM_ALLOCATOR,				// [uint32]		Custom Dynamic Memory Managers allocator (mem::Allocator*)
		PR_MEM_BALIGN,					// [uint32]		Default memory allocation alignment in bytes
		PR_MEM_RESET_VALUE,				// [uint32]		Reset value for unallocated memory. Default is 0xCD
		PR_MEM_LOG_MAXBSIZE,			// [uint32]		Max size in bytes used to bufferize all the memory operations
		PR_MEM_LOG_FILENAME,			// [pcstr]		Filename used to backup the buffer of all the memory operations
		PR_MEM_APP_ADDR,				// [uint32]		Application start address
		PR_MEM_APP_BSIZE,				// [uint32]		Application all sections size in bytes
		PR_MEM_APP_HEAP_ADDR,			// [uint32]		Application native heap address
		PR_MEM_APP_HEAP_BSIZE,			// [uint32]		Application native heap size in bytes
		PR_MEM_APP_STACK_ADDR,			// [uint32]		Application stack top address
		PR_MEM_APP_STACK_BSIZE,			// [uint32]		Application stack size in bytes

		// I/O
		PR_FILE_PREFIX,					// [pcstr]		File prefix
		PR_FILE_SUFFIX,					// [pcstr]		File suffix
		PR_MEDIA_TYPE,					// [pcstr]		"cdrom" or "dvdrom" or "" or NULL
		PR_MEDIA_STATUS_ERROR,			// [uint32]		If > 0, the given status code is used to simulate a file error
		PR_MEDIA_STATUS_FILE,			// [pcstr]		File used to test media status (file::CheckStatus)

		// RscManager
		PR_RSCMAN_CACHEBSIZE,			// [uint32]		Sliding-window cache sise in bytes
		PR_RSCMAN_JUMP_MAXBSIZE,		// [uint32]		Max size in bytes allowed for holes in reading-cache
		PR_RSCMAN_READ_BSTART_ALIGN,	// [uint32]		Sliding-window start alignement in bytes
		PR_RSCMAN_READ_BSIZE_ALIGN,		// [uint32]		Sliding-window size alignement in bytes
		PR_RSCMAN_LOG_MAXBSIZE,			// [uint32]		Max size in bytes allowed for the UID log
		PR_RSCMAN_RRPRIORITY,			// [uint32]		ReadRequest priority for resource data
		PR_RSCMAN_RETRY,				// [bool]		ReadRequest retry on load error (can't be breaked !)

		// DpyManager
		PR_FRONT_FRAME_W,				// [uint32]		Front-frame width
		PR_FRONT_FRAME_H,				// [uint32]		Front-frame height
		PR_BACK_FRAME_W,				// [uint32]		Back-frame width
		PR_BACK_FRAME_H,				// [uint32]		Back-frame height
		PR_OFF_FRAME_W,					// [uint32]		Off-frame width (C32Z32)
		PR_OFF_FRAME_H,					// [uint32]		Off-frame height (C32Z32)
		PR_CHECK_MIPMAPPING_MODE,		// [uint32]		Mode to check mipmapping
		PR_CHECK_MIPMAPPING_LODS,		// [uint32]		LOD to paint for identification
		PR_RESET_ALLVRAM,				// [uint32]		reset all vram at DpyManager init time ?
		PR_RESET_ALLVRAM_RGBA,			// [uint32]		reset all vram color (see PR_RESET_ALLVRAM)
		PR_ENABLE_GPU_DRAWING,			// [bool]		enable the real drawing process
		PR_CONSOLE_ONLY,				// [bool]		output console only
		PR_BGCOLOR_RW,					// [bool]		Enable BGCOLOR rw access
		PR_VSYNC_DRAW,					// [bool]		Enable vertical blank synchronization
		PR_ASYNC_DRAW,					// [bool]		Enable asynchronous drawing
		PR_ASYNC_CHECK,					// [bool]		Enable checking of asynchronous drawing data
		PR_ASYNC_BREAK,					// [bool]		Break before asynchronous drawing
		PR_ASYNC_LOG,					// [bool]		Output informations on asynchronous drawing
		PR_HIDE_CURSOR,					// [bool]		Hide the system cursor

		// SndManager
		PR_SNDMAN_STREAM_CACHEMST,		// [uint32]		Time in millisecond of data cache for the following streaming sound

		// ProfManager
		PR_PROFMAN_DATA_MAXBSIZE,		// [uint32]		Maximum size of the profiler data heap in bytes

		// Microsoft Windows
		PR_DX_D3D,						// [uint32]		D3D object
		PR_DX_D3DDEV,					// [uint32]		D3D device object
		PR_DX_D3DADAPTER,				// [uint32]		D3DADAPTER_DEFAULT = 0
		PR_DX_RUNNING_MODE,				// [uint32]		The DX running level correspond to Hrdw::RunningLevel.
		PR_DX_FULLSCREEN,				// [bool]		Fullscreen else windowed
		PR_DX_ENABLE_RESIZING,			// [bool]		Perform auto-resizing of DX surfaces
		PR_DI_COOPLEVEL,				// [uint32]		DirectInput cooperative level flags (if 0, defaults are used)
		PR_DS_BUFFERDESC_FLAGS,			// [uint32]		The capabilities of the Direct Sound buffer.
		PR_W32_SCREENSAVER,				// [bool]		Enabled if TRUE else disabled
		PR_W32_HINSTANCE,				// [uint32]		Windows application HINSTANCE
		PR_W32_HWND,					// [uint32]		Target window handle
		PR_W32_HWND_DEFPOS,				// [uint32]		Target window default position on screen ((x<<16)|(y<<0)), centered if 0xffffffff
		PR_W32_HWND_FOCUS,				// [uint32]		Focus window handle (if 0 PR_W32_HWND is used)
		PR_W32_HWND_DEFPROC,			// [uint32]		Neova default window proc
		PR_W32_HWND_MANAGED,			// [bool]		Enable message managment (Peek/Dispatch) if TRUE else disabled
		PR_W32_HWND_TITLE,				// [pcstr]		Target window title
		PR_W32_HWND_SMALLICON,			// [uint32]		Small icon rsc HICON
		PR_W32_HWND_BIGICON,			// [uint32]		Big icon rsc HICON
		PR_W32_HWND_CONSTRAINTCURSOR,	// [bool]		Constraint the cursor in the HWND client rect. PR_DX_FULLSCREEN must be setting on.

		// Sony Playstation 2
		PR_EE_DMAC_HEAPS_BSIZE,			// [uint32]		DMAC heaps size in bytes
		PR_EE_DMAC_HEAPS_UNCACHE,		// [bool]		Select fast uncached access mode for DMAC heaps
		PR_EE_DMAC_BREAK_ON_PKT,		// [uint32]		If not NULL, break on this dma packet
		PR_EE_VMODE,					// [uint32]		gs video/display flags
		PR_EE_BACKSTACK_BSIZE,			// [uint32]		Stack backtracing forward size for MIPS in bytes
		PR_IOP_IMGFILE,					// [pcstr]		IOP image filename
		PR_IOP_IRXZIP,					// [pcstr]		Filename of the zip-archive containing the IRX modules to load
		PR_IOP_IRXLIST,					// [uint32]		NULL terminated list of the IRX module pathnames to load, as char** { {path,args}, ..., NULL }
		PR_IOP_RPC_THREADPRI,			// [uint32]		Priority of the RPC thread, on IOP side
		PR_IOP_UPD_THREADPRI,			// [uint32]		Priority of the main update thread, on IOP side
		PR_IOP_UPD_TIMERFREQ,			// [uint32]		Timer frequency of the main update thread, on IOP side
		PR_IOP_NET_THREADPRI,			// [uint32]		Priority of the network update thread, on IOP side
		PR_IOP_STREAM_CACHEBSIZE,		// [uint32]		Size in bytes of the shared buffer used to manage all the data streams, on IOP side
		PR_IOP_EELOAD_REQMAXBSIZE,		// [uint32]		Max size in bytes allowed for an EELOAD stream request, on IOP side
		PR_IOP_EELOAD_TRYCOUNTER,		// [uint32]		Max number of try before EELOAD stream request is forced against VOICE stream request, on IOP side
		PR_IOP_VOICE_REQMAXBSIZE,		// [uint32]		Max size in bytes allowed for a VOICE stream request, on IOP side
		PR_IOP_CMDBUF_FILLING,			// [uint32]		Current filling % (in [0,100]) of the commands sending buffer
		PR_IOP_CMDBUF_OVERFLOW,			// [uint32]		This counter is incremented with each overflow prevention of the commands sending buffer
		PR_IPU_COLUMNMODE_MOVIE,		// [bool]		Movie is encoded in column mode (faster to upload) ?

		// Sony Playstation Portable
		PR_ALG_DMAC_HEAPS_BSIZE,		// [uint32]		DMAC heaps size in bytes
		PR_ALG_DMAC_HEAPS_UNCACHE,		// [bool]		Select fast uncached access mode for DMAC heaps
		PR_ALG_DMAC_BREAK_ON_PKT,		// [uint32]		If not NULL, break on this dma packet
		PR_ALG_VMODE,					// [uint32]		ge video/display flags
		PR_ALG_BACKSTACK_BSIZE,			// [uint32]		Stack backtracing forward size for MIPS in bytes
		PR_ALG_VSYNC_INTR_IDX,			// [uint32]		The vsync-intr index used by the CoreEngine (0-15) (default is 0)
		PR_ALG_TH_PRIORITY,				// [uint32]		The thread priority (lowest=111, highest=16)

		// Nintendo GameCube / Revolution
		PR_NGC_ARENA_MAXHEAPS,			// [uint32]		Maximum arena heaps (given to OSInitAlloc() function)
		PR_NGC_HEAP_HANDLE,				// [uint32]		OSHeapHandle of the main heap
		PR_NGC_MEM2_HANDLE,				// [uint32]		MEMHeapHandle of the Mem2 heap (Wii only)
		PR_NGC_ARAM_MAXALLOC,			// [uint32]		Maximum allocations in aram.
		PR_NGC_XFB_BSIZE,				// [uint32]		Size in bytes of the XBF reserved arena
		PR_NGC_XFB_BASE,				// [uint32]		Low address of the XBF reserved arena
		PR_NGC_GX_FIFO_BSIZE,			// [uint32]		Size in bytes of the GX FIFO reserved arena
		PR_NGC_GX_FIFO_BASE,			// [uint32]		Low address of the GX FIFO reserved arena
		PR_NGC_VMODE,					// [uint32]		NGC video/display flags.
		PR_NGC_CUSTOM_RENDERMODEOBJ,	// [uint32]		NGC custom GXRenderModeObj pointer, used if PR_NGC_VMODE is defined as gx::VM_CUSTOM.
		PR_NGC_DMAC_HEAPS_BSIZE,		// [uint32]		DMAC heaps size in bytes
		PR_NGC_DMAC_HEAPS_UNCACHE,		// [bool]		Select fast uncached access mode for DMAC heaps
		PR_NGC_PAD_ANALOGMODE,			// [uint32]		Analog mode of all NGC controlers.
		PR_NGC_DVDETH,					// [bool]		Enable BBA dvd-eth emulator
		PR_NGC_DVDETH_SERVER,			// [pcstr]		BBA dvd-eth server addr and port "serverIP:port"
		PR_NGC_DVDETH_CLIENT,			// [pcstr]		BBA dvd-eth client addrs "localIP,netmaskIP,gatewayIP"
		PR_NGC_DVDETH_FSFMAXBSIZE,		// [uint32]		BBA dvd-eth FST max size in bytes
		PR_NGC_STREAM_LENGTH,			// [uint32]		Sound's buffer Time in Ms for streaming mode. (default : 2245)
		PR_NGC_LOAD_TRYCOUNTER,			// [uint32]		Max number of try before general file resquest is forced against sound stream request. (default : 6)
		PR_NGC_SOUND_MEM_BUFFERBSIZE,	// [uint32] 	2 Ram buffers is used to load sound data (streamed or not) to Aram ! (default : 1024*64)
		PR_NGC_FX_MEM2,					// [bool]		FX buffers are allocated in Mem2 (default TRUE) (Revolution only)
		PR_NGC_POWER_PRESSED,			// [bool]		Power button has been pressed
		PR_NGC_RESET_PRESSED,			// [bool]		Reset button has been pressed

		// LAST
		PR_LAST
	};


	bool		SetParameter	(	nv::core::Parameter		inParam,
									bool					inValue				);
	bool		SetParameter	(	nv::core::Parameter		inParam,
									uint32					inValue				);
	bool		SetParameter	(	nv::core::Parameter		inParam,
									pcstr					inValue				);

	bool		GetParameter	(	nv::core::Parameter		inParam,
									bool*					outValue = NULL		);
	bool		GetParameter	(	nv::core::Parameter		inParam,
									uint32*					outValue = NULL		);
	bool		GetParameter	(	nv::core::Parameter		inParam,
									pcstr*					outValue = NULL		);


	//
	// Init / Shut

	bool		Init			(												);
	void		Shut			(												);
	void		Exit			(												);
	bool		ExitAsked		(												);


	//
	// Update the Nova Core

	enum UpdateFlags
	{
		UPD_EXITED					=	(1<<0),
		UPD_EXIT_ASKED				=	(1<<1),
		UPD_LOST_FOCUS				=	(1<<2),
		UPD_SET_FOCUS				=	(1<<3),
		UPD_LOST_VISIBILITY			=	(1<<4),
		UPD_SET_VISIBILITY			=	(1<<5),
		UPD_LOST_DEVICE				= 	(1<<6),
		UPD_DATA_REBUILDING_NEEDED	= 	(1<<7),
	};

	uint		Update			(												);


	//
	// System

	enum SystemID
	{
		UnixOS				= 0,
		MacOS,
		IPhone,
		SymbianOS,
		MSWindows,
		MSXBox,
		MSXBox360,
		NintendoGC,
		NintendoDS,
		NintendoWii,
		SCEPlaystationPortable,
		SCEPlaystation2,
		SCEPlaystation3
	};

	SystemID	GetSystemID			(											);


	//
	// Release informations

	uint		GetReleaseNumber	(											);
	uint		GetReleaseRevision	(											);
	uint		GetReleaseVersion	(											);
	pcstr		GetDebugLevel		(											);


	//
	// System configuration

	#undef	LANG_JAPANESE
	#undef	LANG_ENGLISH
	#undef	LANG_FRENCH
	#undef	LANG_SPANISH
	#undef	LANG_GERMAN
	#undef	LANG_ITALIAN
	#undef	LANG_DUTCH
	#undef	LANG_PORTUGUESE
	#undef	LANG_RUSSIAN
	#undef	LANG_HANGUL
	#undef	LANG_CHINESE_T
	#undef	LANG_CHINESE_S

	enum Aspect					// Aspect ratio setting for display screen
	{
		ASPECT_43,				// 4:3
		ASPECT_FULL,			// Full-screen
		ASPECT_169				// 16:9
	};

	enum VideoMode
	{	
		VM_Other,
		VM_EURGB60,
		VM_Progressive,
	};

	enum SoundMode 
	{
		SM_Mono		=	0,
		SM_Stereo	= 	1,
		SM_Surround	=	2,
	};
	
	enum DateNotation			// Date format
	{
		DATE_YYYYMMDD,			// Year/Month/Day
		DATE_MMDDYYYY,			// Month/Day/Year
		DATE_DDMMYYYY			// Day/Month/Year
	};

	enum Language				// Language setting
	{
		LANG_JAPANESE,			// Japanese
		LANG_ENGLISH,			// English
		LANG_FRENCH,			// French
		LANG_SPANISH,			// Spanish
		LANG_GERMAN,			// German
		LANG_ITALIAN,			// Italian
		LANG_DUTCH,				// Dutch
		LANG_PORTUGUESE,		// Portuguese
		LANG_RUSSIAN,			// Russian
		LANG_HANGUL,			//
		LANG_CHINESE_T,			// Chinese
		LANG_CHINESE_S			// Chinese
	};

	enum SummerTime				// Daylight savings setting
	{
		SUMT_ON,				// Daylight savings
		SUMT_OFF				// No daylight savings
	};

	enum TimeNotation 			// Time format
	{
		TIME_24H,				// 24-hour format
		TIME_12H				// 12-hour (AM/PM) format
	};

	Aspect			GetAspect			(									);	
	VideoMode		GetVideoMode		(									);
	SoundMode		GetSoundMode		(									);
	DateNotation	GetDateNotation		(									);
	Language		GetLanguage			(									);
	SummerTime		GetSummerTime		(									);
	TimeNotation	GetTimeNotation		(									);

} }


#endif	// _NvCore_H_




