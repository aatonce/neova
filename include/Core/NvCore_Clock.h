/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_Clock_H_
#define	_NvCore_Clock_H_



namespace nv { namespace clock
{

	struct Time
	{
		float			ipart;				// Integer part of time
		float			fpart;				// Fractionnal part of time

		// Get dt & absolute time
		inline	float	operator -		( Time & inTRef )	{ return (ipart-inTRef.ipart)+(fpart-inTRef.fpart); }
		inline			operator float	(				)	{ return ipart+fpart;								}
		inline			operator int	(				)	{ return (int)ipart;								}
	};


	void		GetTime			(		Time *				outTime		);

} }



#endif	//	_NvCore_Clock_H_


