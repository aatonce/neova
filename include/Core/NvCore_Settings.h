/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _NvCore_Settings_H_
#define _NvCore_Settings_H_



// I'm using Neova !
#define	USING_NEOVA



// Targeted HW
#if !defined(_DX) && !defined(_EE) && !defined(_PSP) && !defined(_NGC) && !defined(_PS3)
	#if defined(_WIN32)
		#define _DX
	#elif defined(__ee__) || defined(__R5900__)
		#define _EE
	#elif defined(__psp__) || defined(_MIPS_ARCH_ALLEGREX) || (defined(__R3000__) && defined(__SCE__))
		#define _PSP
	#elif defined(GEKKO) || defined(HW2)
		#define _NGC
	#elif defined(SN_TARGET_PS3)
		#define _PS3
	#else
		#error "Unknown platform !"
	#endif
#endif


// Engine part
#if !defined(_NVENGINE) && !defined(_NVGAME)
	#define _NVGAME
#elif defined(_NVENGINE)
	#undef  _NVGAME
#elif defined(_NVGAME)
	#undef	_NVENGINE
#endif


// Define compilation version switchs
#undef	_NVCOMP_FAST_CODE				// optimised code ?
#undef	_NVCOMP_DEVKIT					// supporting devkit ?
#undef	_NVCOMP_ENABLE_DBG				// enables debug code/outputs/assert ?
#undef	_NVCOMP_ENABLE_CONSOLE			// enables console outputs ?
#undef	_NVCOMP_ENABLE_PROFILING		// enables profiling ?

//											fast-code | dbg-code | devkit | profiling | console
//										    ----------|----------|--------|-----------|--------
#if defined(_FINAL)						//	    O     |          |        |           |
	#define	_NVCOMP_FAST_CODE
#elif defined(_MASTER)					//	    O     |          |        |           |    O
	#define _NVCOMP_FAST_CODE
	#define _NVCOMP_ENABLE_CONSOLE
#elif defined(_RELEASE)					//	    O     |          |        |     O     |    O
	#define _NVCOMP_FAST_CODE
	#define _NVCOMP_ENABLE_PROFILING
	#define _NVCOMP_ENABLE_CONSOLE
	#define _NVCOMP_DEVKIT
#elif defined(_DEBUG_OPT)				//	    O     |     O    |   O    |     O     |    O
	#define _NVCOMP_FAST_CODE
	#define _NVCOMP_ENABLE_PROFILING
	#define _NVCOMP_ENABLE_DBG
	#define _NVCOMP_ENABLE_CONSOLE
	#define _NVCOMP_DEVKIT
#elif defined(_DEBUG)					//	          |     O    |   O    |     O     |    O
	#define _NVCOMP_ENABLE_PROFILING
	#define _NVCOMP_ENABLE_DBG
	#define _NVCOMP_ENABLE_CONSOLE
	#define _NVCOMP_DEVKIT
#endif



#endif // _NvCore_Settings_H_


