/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _NvCore_Types_H_
#define _NvCore_Types_H_



namespace nv { namespace types
{

	// Sized types

	typedef signed   char					int8;
	typedef unsigned char					uint8;
	typedef signed   short					int16;
	typedef unsigned short					uint16;
	typedef signed   int					int32;
	typedef unsigned int					uint32;

	#if defined(__MWERKS__) && (defined(_EE) || defined(_PSP))
		typedef signed long long			int64;
		typedef unsigned long long			uint64;
		typedef	__int128					int128;
		typedef	unsigned __int128			uint128;
	#elif defined(__GNUC__)
		typedef signed long long			int64;
		typedef unsigned long long			uint64;
		typedef	int							int128 __attribute__ ((mode(TI)));
		typedef	unsigned int				uint128 __attribute__ ((mode(TI)));
	#elif defined(_MSC_VER) || defined(_WIN32)
		typedef signed __int64				int64;
		typedef unsigned __int64			uint64;
		typedef struct { int64 hi,low; }	int128;
		typedef struct { uint64 hi,low; }	uint128;
	#else
		typedef signed long long			int64;
		typedef unsigned long long			uint64;
		typedef struct { int64 hi,low; }	int128;
		typedef struct { uint64 hi,low; }	uint128;
	#endif

	// Native types

	typedef unsigned char					uchar;
	typedef unsigned short					ushort;
	typedef unsigned int					uint;
	typedef unsigned long					ulong;
	typedef	void *							pvoid;
	typedef	const char *					pcstr;
	typedef char *							pstr;
	typedef unsigned char					byte;

} }


#ifndef	_NvCore_Types_NOT_GLOBAL_
typedef nv::types::int8						int8;
typedef nv::types::int16					int16;
typedef nv::types::int32					int32;
typedef nv::types::int64					int64;
typedef nv::types::int128					int128;
typedef nv::types::uint8					uint8;
typedef nv::types::uint16					uint16;
typedef nv::types::uint32					uint32;
typedef nv::types::uint64					uint64;
typedef nv::types::uint128					uint128;
typedef nv::types::uchar					uchar;
typedef nv::types::ushort					ushort;
typedef nv::types::uint						uint;
typedef nv::types::ulong					ulong;
typedef nv::types::pvoid					pvoid;
typedef nv::types::pcstr					pcstr;
typedef nv::types::pstr						pstr;
typedef nv::types::byte						byte;
#endif	// _NvCore_Types_NOT_GLOBAL_



//
// some macros

#define AS_UINT8( P )						*( (uint8*)		&(P) )
#define AS_INT8( P )						*( (int8*)		&(P) )
#define AS_UINT16( P )						*( (uint16*)	&(P) )
#define AS_INT16( P )						*( (int16*)		&(P) )
#define AS_UINT32( P )						*( (uint32*)	&(P) )
#define AS_INT32( P )						*( (int32*)		&(P) )
#define AS_UINT64( P )						*( (uint64*)	&(P) )
#define AS_INT64( P )						*( (int64*)		&(P) )
#define AS_UINT128( P )						*( (uint128*)	&(P) )
#define AS_FLOAT( P )						*( (float*)		&(P) )
#define AS_STRING( P )						#P
#define	MK_UINT64( HI32, LO32 )				((uint64(HI32)<<32)|uint64(LO32))


// Alignement

#if defined(__GNUC__) || defined(__MWERKS__) || defined(__GCC__)
	#define	ALIGNED_X( inType, inVar, inBA )		inType inVar __attribute__((aligned(inBA)))
#elif defined(_MSC_VER) || defined(_WIN32)
	#define	ALIGNED_X( inType, inVar, inBA )		__declspec(align(inBA)) inType inVar
#else
	#define	ALIGNED_X( inType, inVar, inBA )
#endif

#undef	NVHW_DCACHE_BALIGN		// D$ line byte size
#undef	NVHW_IO_BALIGN			// multi-purpose i/o alignement byte size
#if defined(_EE) || defined(_PSP)
	#define NVHW_DCACHE_BALIGN				(64)
	#define	NVHW_IO_BALIGN					(64)
	#define	NVHW_FILE_BALIGN				(16)
	#define	NVHW_DMAC_BALIGN				(16)
#elif defined(_NGC)
	#define NVHW_DCACHE_BALIGN				(32)
	#define	NVHW_IO_BALIGN					(32)
	#define	NVHW_FILE_BALIGN				(32)
	#define	NVHW_DMAC_BALIGN				(32)
#else
	#define NVHW_DCACHE_BALIGN				(16)
	#define	NVHW_IO_BALIGN					(16)
	#define	NVHW_FILE_BALIGN				(16)
	#define	NVHW_DMAC_BALIGN				(16)
#endif

#define	ALIGNED32( inType, inVar )			ALIGNED_X( inType, inVar, 4 )
#define	ALIGNED64( inType, inVar )			ALIGNED_X( inType, inVar, 8 )
#define	ALIGNED128( inType, inVar )			ALIGNED_X( inType, inVar, 16 )	// 1QW
#define	ALIGNED512( inType, inVar )			ALIGNED_X( inType, inVar, 64 )	// 4QW
#define	ALIGNED1024( inType, inVar )		ALIGNED_X( inType, inVar, 128 )	// 8QW
#define	ALIGNED_DCACHE( inType, inVar )		ALIGNED_X( inType, inVar, NVHW_DCACHE_BALIGN )
#define	ALIGNED_IO( inType, inVar )			ALIGNED_X( inType, inVar, NVHW_IO_BALIGN )


#endif	// _NvCore_Types_H_



#undef										CBool
typedef char								CBool;

#undef										FALSE
#define FALSE								false
#undef										CFALSE
#define	CFALSE								0

#undef										TRUE
#define TRUE								true
#undef										CTRUE
#define	CTRUE								1

#undef										NULL
#define NULL								(0)


