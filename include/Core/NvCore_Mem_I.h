/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _NvCore_Mem_H_




namespace nv {

	template <typename T> inline
	T* ConstructInPlace	(	T*		inPtr		)
	{
		NV_ASSERT( inPtr );
		if( !inPtr )
			return NULL;
		#ifdef _NVENGINE
		#undef new
		#endif
		return new(inPtr) T;	// placement without memory allocation !
		#ifdef _NVENGINE
		#define	new $illegal new operator in engine part$
		#endif
	}

	template <typename T> inline
	void* DestructInPlace		(	T*		inPtr		)
	{
		NV_ASSERT( inPtr );
		if( inPtr )
			inPtr->~T();
		return inPtr;
	}

}





#endif // _NvCore_Mem_H_



