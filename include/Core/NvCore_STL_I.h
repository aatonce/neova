/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _NvCore_STL_H_


//
// iterator

namespace nv
{
#define	NV_IT_DECL	template < typename T > inline
#define	NV_IT_DEF	base_iterator< T >

	NV_IT_DECL
	NV_IT_DEF::base_iterator(	T *		inPtr	)
	{
		ptr = inPtr;
	}

	NV_IT_DECL
	NV_IT_DEF
	NV_IT_DEF::operator + (	int		inN	) const
	{
		return base_iterator( ptr + inN );
	}

	NV_IT_DECL
	NV_IT_DEF
	NV_IT_DEF::operator - (	int		inN	) const
	{
		return base_iterator( ptr + inN );
	}

	NV_IT_DECL
	void
	NV_IT_DEF::operator += (	int		inN	)
	{
		ptr += inN;
	}

	NV_IT_DECL
	void
	NV_IT_DEF::operator -= (	int		inN	)
	{
		ptr -= inN;
	}

	NV_IT_DECL
	void
	NV_IT_DEF::operator ++ (				)
	{
		ptr++;
	}

	NV_IT_DECL
	void
	NV_IT_DEF::operator --	(			)
	{
		ptr--;
	}

	NV_IT_DECL
	void
	NV_IT_DEF::operator ++ (	int		)
	{
		ptr++;
	}

	NV_IT_DECL
	void
	NV_IT_DEF::operator --	(	int		)
	{
		ptr--;
	}

	NV_IT_DECL
	typename NV_IT_DEF::size_type
	NV_IT_DEF::operator -	(	const NV_IT_DEF & inRef ) const
	{
		NV_ASSERT( inRef.ptr >= ptr );
		return (inRef.ptr - ptr);
	}

	NV_IT_DECL
	NV_IT_DEF &
	NV_IT_DEF::operator =	(	const NV_IT_DEF & inRef )
	{
		ptr = inRef.ptr;
		return *this;
	}

	NV_IT_DECL
	bool
	NV_IT_DEF::operator ==	(	const NV_IT_DEF & inRef ) const
	{
		return ( ptr == inRef.ptr );
	}

	NV_IT_DECL
	bool
	NV_IT_DEF::operator !=	(	const NV_IT_DEF & inRef ) const
	{
		return ( ptr != inRef.ptr );
	}

	NV_IT_DECL
	bool
	NV_IT_DEF::operator <	(	const NV_IT_DEF &	inRef ) const
	{
		return ( ptr < inRef.ptr );
	}

	NV_IT_DECL
	bool
	NV_IT_DEF::operator >	(	const NV_IT_DEF &	inRef ) const
	{
		return ( ptr > inRef.ptr );
	}

	NV_IT_DECL
	bool
	NV_IT_DEF::operator <=	(	const NV_IT_DEF &	inRef ) const
	{
		return ( ptr <= inRef.ptr );
	}

	NV_IT_DECL
	bool
	NV_IT_DEF::operator >=	(	const NV_IT_DEF &	inRef ) const
	{
		return ( ptr >= inRef.ptr );
	}

	NV_IT_DECL
	typename NV_IT_DEF::pointer
	NV_IT_DEF::operator ->	(		) const
	{
		return ptr;
	}

	NV_IT_DECL
	typename NV_IT_DEF::reference
	NV_IT_DEF::operator *	(		) const
	{
		return *ptr;
	}

	NV_IT_DECL
	typename NV_IT_DEF::pointer
	NV_IT_DEF::operator &	(		) const
	{
		return ptr;
	}

#undef	NV_IT_DECL
#undef	NV_IT_DEF
}



//
// vectors

namespace nv
{
#define	NV_VEC_DECL		template < typename T > inline
#define	NV_SVEC_DEF		sysvector< T >
#define	NV_UVEC_DEF		vector< T >
#define	NV_GVEC_DEF		gvector< T >
#define	NV_EVEC_DEF		evector< T >


	// System vector

	NV_VEC_DECL
	void
	NV_SVEC_DEF::Init	(	uint	inAllocFlags	)
	{
		_init( sizeof(T), inAllocFlags );
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::Shut	(		)
	{
		_free();
	}

	NV_VEC_DECL
	NV_SVEC_DEF &
	NV_SVEC_DEF::operator =	(	const NV_SVEC_DEF &	inRef )
	{
		copy( inRef );
		return *this;
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::reference
	NV_SVEC_DEF::operator []	(	size_type	inPos	)
	{
		return at( inPos );
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::reference
	NV_SVEC_DEF::at		(	size_type	inPos	)
	{
		uint8* ptr = vfirst + (inPos * vtbsize);
		NV_ASSERT( vfirst );
		NV_ASSERT( ptr>=vfirst );
		NV_ASSERT( ptr<vlast );
		return *((T*)ptr);
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::pointer
	NV_SVEC_DEF::data	(						) const
	{
		return (T*)vfirst;
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::iterator
	NV_SVEC_DEF::begin	(		) const
	{
		return iterator( (T*)vfirst );
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::iterator
	NV_SVEC_DEF::end		(		) const
	{
		return iterator( (T*)vlast );
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::size_type
	NV_SVEC_DEF::size	(		) const
	{
		return uint32(vlast-vfirst)/vtbsize;
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::size_type
	NV_SVEC_DEF::max_size	(		) const
	{
		return _max_size();
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::size_type
	NV_SVEC_DEF::capacity	(		) const
	{
		return uint32(vend-vfirst)/vtbsize;
	}

	NV_VEC_DECL
	bool
	NV_SVEC_DEF::empty		(		) const
	{
		return (vfirst == vlast);
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::reserve		(	size_type	inSize	)
	{
		_reserve( inSize );
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::reference
	NV_SVEC_DEF::front		(			) const
	{
		NV_ASSERT( vfirst != vlast );
		return *((T*)vfirst);
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::reference
	NV_SVEC_DEF::back		(			) const
	{
		NV_ASSERT( vfirst != vlast );
		return *((T*)(vlast-vtbsize));
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::push_back	(	const T &	inT		)
	{
		_push_back( (uint8*)&inT );
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::reference
	NV_SVEC_DEF::alloc_back	(	size_type	inSize	)
	{
		return *((T*)_alloc_back(inSize));
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::pop_back	(						)
	{
		_pop_back();
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::swap		(	NV_SVEC_DEF &	inRef	)
	{
		_swap( &inRef );
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::iterator
	NV_SVEC_DEF::insert		(	const iterator		inPos,
								const T &			inT	)
	{
		return iterator( (T*)_insert((uint8*)&inPos,(uint8*)&inT) );
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::insert		(	const iterator		inPos,
								const iterator		inFirst,
								const iterator		inEnd	)
	{
		_insert( (uint8*)&inPos, (uint8*)&inFirst, (uint8*)&inEnd );
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::insert		(	const iterator		inPos,
								size_type			inSize,
								const T &			inT		)
	{
		_insert( (uint8*)&inPos, inSize, (uint8*)&inT );
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::iterator
	NV_SVEC_DEF::erase		(	const iterator		inPos	)
	{
		return iterator( (T*)_erase((uint8*)&inPos) );
	}

	NV_VEC_DECL
	typename NV_SVEC_DEF::iterator
	NV_SVEC_DEF::erase		(	const iterator		inFirst,
								const iterator		inEnd	)
	{
		return iterator( (T*)_erase((uint8*)&inFirst,(uint8*)&inEnd) );
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::clear		(				)
	{
		_resize( 0, NULL );
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::copy		(	const NV_SVEC_DEF &	inRef	)
	{
		_free();
		_init( vmflags, (base_vector*)&inRef );
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::copy		(	const iterator		inFirst,
								const iterator		inEnd	)
	{
		_resize( 0, NULL );
		_insert( vfirst, (uint8*)&inFirst, (uint8*)&inEnd );
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::resize		(	const size_type		inSize,
								const T	&			inT		)
	{
		_resize( inSize, (uint8*)&inT );
	}

	NV_VEC_DECL
	void
	NV_SVEC_DEF::free		(				)
	{
		_free();
	}


	// User generic vector

	NV_VEC_DECL
	NV_UVEC_DEF::vector	(		)
	{
		sysvector<T>::Init( nv::mem::AFC_DEFAULT_VECTOR );
	}

	NV_VEC_DECL
	NV_UVEC_DEF::~vector	(		)
	{
		sysvector<T>::Shut();
	}

	NV_VEC_DECL
	NV_UVEC_DEF::vector	(	size_type	inSize		)
	{
		_init( sizeof(T), nv::mem::AFC_DEFAULT_VECTOR, inSize );
	}

	NV_VEC_DECL
	NV_UVEC_DEF::vector	(	size_type	inSize, const T & inT	)
	{
		_init( sizeof(T), nv::mem::AFC_DEFAULT_VECTOR, inSize, (pvoid)&inT );
	}

	NV_VEC_DECL
	NV_UVEC_DEF::vector	(	const NV_SVEC_DEF & inRef		)
	{
		_init( nv::mem::AFC_DEFAULT_VECTOR, &inRef );
	}

	NV_VEC_DECL
	NV_UVEC_DEF::vector	(	const iterator		inFirst,
							const iterator		inLast	)
	{
		_init( sizeof(T), nv::mem::AFC_DEFAULT_VECTOR, &inFirst, &inLast );
	}

	NV_VEC_DECL
	NV_UVEC_DEF &
	NV_UVEC_DEF::operator =	(	const NV_SVEC_DEF &	inRef )
	{
		copy( inRef );
		return *this;
	}

	NV_VEC_DECL
	typename NV_UVEC_DEF::reference
	NV_UVEC_DEF::operator []	(	size_type	inPos	)
	{
		return at( inPos );
	}


	// Game vector

	NV_VEC_DECL
	NV_GVEC_DEF::gvector	(		)
	{
		sysvector<T>::Init( nv::mem::AFC_GAME_VECTOR );
	}

	NV_VEC_DECL
	NV_GVEC_DEF::~gvector	(		)
	{
		sysvector<T>::Shut();
	}

	NV_VEC_DECL
	NV_GVEC_DEF::gvector	(	size_type	inSize		)
	{
		_init( sizeof(T), nv::mem::AFC_GAME_VECTOR, inSize );
	}

	NV_VEC_DECL
	NV_GVEC_DEF::gvector	(	size_type	inSize, const T & inT	)
	{
		_init( sizeof(T), nv::mem::AFC_GAME_VECTOR, inSize, (pvoid)&inT );
	}

	NV_VEC_DECL
	NV_GVEC_DEF::gvector	(	const NV_SVEC_DEF & inRef		)
	{
		_init( nv::mem::AFC_GAME_VECTOR, &inRef );
	}

	NV_VEC_DECL
	NV_GVEC_DEF::gvector	(	const iterator		inFirst,
							const iterator		inLast	)
	{
		_init( sizeof(T), nv::mem::AFC_GAME_VECTOR, &inFirst, &inLast );
	}

	NV_VEC_DECL
	NV_GVEC_DEF &
	NV_GVEC_DEF::operator =	(	const NV_SVEC_DEF &	inRef )
	{
		copy( inRef );
		return *this;
	}

	NV_VEC_DECL
	typename NV_GVEC_DEF::reference
	NV_GVEC_DEF::operator []	(	size_type	inPos	)
	{
		return at( inPos );
	}


	// Engine vector

	NV_VEC_DECL
	NV_EVEC_DEF::evector	(		)
	{
		sysvector<T>::Init( nv::mem::AFC_ENGINE_VECTOR );
	}

	NV_VEC_DECL
	NV_EVEC_DEF::~evector	(		)
	{
		sysvector<T>::Shut();
	}

	NV_VEC_DECL
	NV_EVEC_DEF::evector	(	size_type	inSize		)
	{
		_init( sizeof(T), nv::mem::AFC_ENGINE_VECTOR, inSize );
	}

	NV_VEC_DECL
	NV_EVEC_DEF::evector	(	size_type	inSize, const T & inT	)
	{
		_init( sizeof(T), nv::mem::AFC_ENGINE_VECTOR, inSize, (pvoid)&inT );
	}

	NV_VEC_DECL
	NV_EVEC_DEF::evector	(	const NV_SVEC_DEF & inRef		)
	{
		_init( nv::mem::AFC_ENGINE_VECTOR, &inRef );
	}

	NV_VEC_DECL
	NV_EVEC_DEF::evector	(	const iterator		inFirst,
								const iterator		inLast	)
	{
		_init( sizeof(T), nv::mem::AFC_ENGINE_VECTOR, &inFirst, &inLast );
	}

	NV_VEC_DECL
	NV_EVEC_DEF &
	NV_EVEC_DEF::operator =	(	const NV_SVEC_DEF &	inRef )
	{
		copy( inRef );
		return *this;
	}

	NV_VEC_DECL
	typename NV_EVEC_DEF::reference
	NV_EVEC_DEF::operator []	(	size_type	inPos	)
	{
		return at( inPos );
	}


#undef	NV_GVEC_DEF
#undef	NV_EVEC_DEF
#undef	NV_UVEC_DEF
#undef	NV_SVEC_DEF
#undef	NV_VEC_DECL
}




//
// trees

namespace nv
{
#define	NV_TREE_DECL	template < typename Key, typename Compare > inline
#define	NV_TREE_DEF		tree<Key,Compare>
#define	NV_TREE_NODE	tree<Key,Compare>::Node
#define	NV_PTREE_DECL	template < typename Key, typename Compare > inline
#define	NV_PTREE_DEF	ptree<Key,Compare>
#define	NV_PTREE_NODE	ptree<Key,Compare>::Node


	// node

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_NODE::parent	(		)
	{
		return (Node*)_parent;
	}

	NV_TREE_DECL
	void
	NV_TREE_NODE::parent	(	Node* n	)
	{
		_parent = n;
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_NODE::left	(		)
	{
		return (Node*)_left;
	}

	NV_TREE_DECL
	void
	NV_TREE_NODE::left	( Node* n	)
	{
		_left = n;
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_NODE::right	(		)
	{
		return (Node*)_right;
	}

	NV_TREE_DECL
	void
	NV_TREE_NODE::right	(	Node* n	)
	{
		_right = n;
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_NODE::next	(		)
	{
		return (Node*)_next;
	}

	NV_TREE_DECL
	void
	NV_TREE_NODE::next	(	Node*	n	)
	{
		_next = n;
	}
	
	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_NODE::prev	(		)
	{
		return (Node*)_prev;
	}

	NV_TREE_DECL
	void
	NV_TREE_NODE::prev	(	Node*	n	)
	{
		_prev = n;
	}


	// tree

	NV_TREE_DECL
	void
	NV_TREE_DEF::Init	(		)
	{
		_init();
	}

	NV_TREE_DECL
	void
	NV_TREE_DEF::Shut	(		)
	{
		_shut();
	}

	NV_TREE_DECL
	void
	NV_TREE_DEF::clear()
	{
		_clear();
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_DEF::insert(	Node*	node	)
	{
		return (Node*)_insert( node );
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_DEF::insertUnique(	Node*	node	)
	{
		return (Node*)_insertUnique( node );
	}

	NV_TREE_DECL
	void
	NV_TREE_DEF::erase(	Node*	node	)
	{
		_erase( node );
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_DEF::find( const Key & key )
	{
		Node n;	n.key = key;
		return (Node*)_find( &n );
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_DEF::first()
	{
		return (Node*)_first;
	}
	
	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_DEF::last()
	{
		return (Node*)_last;
	}
	
	NV_TREE_DECL
	uint
	NV_TREE_DEF::size()
	{
		return _size;
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_DEF::nextUnique( Node* node )
	{
		return (Node*)_nextUnique( node );
	}

	NV_TREE_DECL
	typename NV_TREE_NODE*
	NV_TREE_DEF::prevUnique( Node* node )
	{
		return (Node*)_prevUnique( node );
	}

	NV_TREE_DECL
	int
	NV_TREE_DEF::_compareNode( const _Node* _n0, const _Node* _n1 ) const
	{
		const Key& k0 = ((const Node*)_n0)->key;
		const Key& k1 = ((const Node*)_n1)->key;
		if( compareKey(k0,k1) )	return -1;
		if( compareKey(k1,k0) )	return +1;
		return 0;
	}


	// ptree

	NV_PTREE_DECL
	void
	NV_PTREE_DEF::Init	(	)
	{
		NV_TREE_DEF::Init();
		poolList = NULL;
		freeList = NULL;
	}

	NV_PTREE_DECL
	void
	NV_PTREE_DEF::Shut	(	)
	{
		freemem();
		NV_TREE_DEF::Shut();
	}

	NV_PTREE_DECL
	void
	NV_PTREE_DEF::clear (		)
	{
		Node *c=tree<Key,Compare>::first(), *n;
		while( c ) {
			n = c->next();
			freeNode( c );
			c = n;
		}
		NV_TREE_DEF::clear();
	}

	NV_PTREE_DECL
	void
	NV_PTREE_DEF::freemem	(	)
	{
		clear();
		Node *p=poolList, *np;
		while( p ) {
			np = p->next();
			freePool( p->parent() );
			p = np;
		}
		poolList = NULL;
		freeList = NULL;
	}

	NV_PTREE_DECL
	typename NV_PTREE_NODE*
	NV_PTREE_DEF::insert	(	const Key&	inKey	)
	{
		Node* n = allocNode();
		if( !n )	return NULL;
		n->key = inKey;
		if( NV_TREE_DEF::insert(n) )	return n;
		freeNode( n );
		return NULL;
	}

	NV_PTREE_DECL
	typename NV_PTREE_NODE*
	NV_PTREE_DEF::insertUnique	(	const Key&	inKey	)
	{
		Node* n = allocNode();
		if( !n )	return NULL;
		n->key = inKey;
		if( NV_TREE_DEF::insertUnique(n) )	return n;
		freeNode( n );
		return NULL;
	}

	NV_PTREE_DECL
	void
	NV_PTREE_DEF::erase ( const Key&	inKey	)
	{
		erase( find(inKey) );
	}

	NV_PTREE_DECL
	void
	NV_PTREE_DEF::erase ( Node* n	)
	{
		NV_TREE_DEF::erase( n );
		freeNode( n );
	}

	NV_PTREE_DECL
	typename NV_PTREE_NODE*
	NV_PTREE_DEF::allocNode	(		)
	{
		if( !freeList ) {
			uint32 newpoolBS = 0;
			void* newpool = allocPool( newpoolBS, sizeof(Node) );
			if( !newpool || !newpoolBS )	return NULL;
			uint ncpt = newpoolBS / sizeof(Node);
			if( ncpt < 2 ) {
				freePool( newpool );
				return NULL;
			}
			// add pool (first node is reserved)
			Node* poolN = (Node*) newpool;
			poolN->parent( (Node*)newpool );
			poolN->next( poolList );
			poolList = poolN;
			// add free nodes
			Node* freeN = poolN+1;
			for( uint i = 0 ; i < ncpt-2 ; i++ )
				freeN[i].next( &freeN[i+1] );
			freeN[ncpt-2].next( NULL );
			freeList = freeN;
		}
		Node* n = freeList;
		freeList = n->next();
		return n;
	}

	NV_PTREE_DECL
	void
	NV_PTREE_DEF::freeNode	(	Node*	n	)
	{
		if( n ) {
			n->next( freeList );
			freeList = n;
		}
	}



#undef	NV_PTREE_NODE
#undef	NV_PTREE_DEF
#undef	NV_PTREE_DECL
#undef	NV_TREE_NODE
#undef	NV_TREE_DEF
#undef	NV_TREE_DECL
}


#endif // _NvCore_STL_H_



