/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _NvCore_Def_H_





template < typename T > inline
void	SafeRelease	(	T * &	ioITF	)
{
	if( ioITF ) {
		ioITF->Release();
		ioITF = NULL;
	}
}


template < typename T > inline
bool	SafeGetRef		(	T * &	ioITF,	T *	inRefITF	)
{
	if( ioITF == inRefITF )
		return FALSE;
	if( inRefITF )
		inRefITF->AddRef();
	if( ioITF )
		ioITF->Release();
	ioITF = inRefITF;
	return TRUE;
}


template < typename T > inline
bool	SafeGetValidRef	(	T * &	ioITF,	T *	inRefITF	)
{
	if( !inRefITF || ioITF==inRefITF )
		return FALSE;
	inRefITF->AddRef();
	if( ioITF )
		ioITF->Release();
	ioITF = inRefITF;
	return TRUE;
}


template < typename T > inline
void	SafeFree	(	T * &	ioPTR	)
{
	if( ioPTR ) {
		NvFree( (pvoid)ioPTR );
		ioPTR = NULL;
	}
}

template < typename T > inline
void	SafeDelete	(	T * &	ioPTR	)
{
#ifdef _NVENGINE
	nv::report::Message( nv::report::ERROR, "Illegal delete operator in engine part !" );
#else
	if( ioPTR ) {
		delete ioPTR;
		ioPTR = NULL;
	}
#endif
}

template <typename tC> inline
void Swap( tC & ioC1, tC & ioC2 )
{
	tC tmp = ioC1;
	ioC1 = ioC2;
	ioC2 = tmp;
}


template <typename tC> inline
tC InvertByteOrder( tC inC )
{
	union	{
		uchar	c[ sizeof(tC) ];
		tC		t;
	};
	t = inC;
	uchar* lo = c;
	uchar* hi = c + sizeof(tC) - 1;
	while( lo < hi )
		Swap( *lo++, *hi-- );
	return t;
}

#ifdef _EE
inline uint32 InvertByteOrder( uint32 inC )
{
	register uint32 outC;
	asm volatile(
	"	.set	noreorder\n"
	"	pextlb	%0, $0, %1\n"
	"	prevh	%0, %0\n"
	"	ppacb	%0, $0, %0\n"
	"	.set	reorder\n"
	: "=r"(outC) : "r"(inC) );
	return outC;
}

inline int32 InvertByteOrder( int32 inC )
{
	register int32 outC;
	asm volatile(
	"	.set	noreorder\n"
	"	pextlb	%0, $0, %1\n"
	"	prevh	%0, %0\n"
	"	ppacb	%0, $0, %0\n"
	"	.set	reorder\n"
	: "=r"(outC) : "r"(inC) );
	return outC;
}

inline uint64 InvertByteOrder( uint64 inC )
{
	register uint64 outC;
	register uint64 tmp;
	asm volatile(
	"	.set	noreorder\n"
	"	pextlb	%0, $0, %2\n"
	"	prevh	%0, %0\n"
	"	pcpyud	%1, %0, $0\n"
	"	pcpyld	%0, %0, %1\n"
	"	ppacb	%0, $0, %0\n"
	"	.set	reorder\n"
	: "=&r"(outC),"=&r"(tmp) : "r"(inC) );
	return outC;
}

inline int64 InvertByteOrder( int64 inC )
{
	register int64 outC;
	register int64 tmp;
	asm volatile(
	"	.set	noreorder\n"
	"	pextlb	%0, $0, %2\n"
	"	prevh	%0, %0\n"
	"	pcpyud	%1, %0, $0\n"
	"	pcpyld	%0, %0, %1\n"
	"	ppacb	%0, $0, %0\n"
	"	.set	reorder\n"
	: "=&r"(outC),"=&r"(tmp) : "r"(inC) );
	return outC;
}
#endif // _EE


template <typename tC> inline
tC LSBToNative( tC inC )
{
	#ifdef IS_LSB
	return inC;
	#else
	return InvertByteOrder( inC );
	#endif
}

template <typename tC> inline
tC MSBToNative( tC inC )
{
	#ifdef IS_MSB
	return inC;
	#else
	return InvertByteOrder( inC );
	#endif
}

template <typename tC> inline
tC NativeToLSB( tC inC )
{
	#ifdef IS_LSB
	return inC;
	#else
	return InvertByteOrder( inC );
	#endif
}

template <typename tC> inline
tC NativeToMSB( tC inC )
{
	#ifdef IS_MSB
	return inC;
	#else
	return InvertByteOrder( inC );
	#endif
}

template <>	inline
Psm	LSBToNative		(	Psm		inC		)
{
	#ifdef IS_LSB
	return inC;
	#else
	if( inC >= PSM_CLUT8 )	return inC;
	return Psm( uint(inC)^1 );
	#endif
}

template <>	inline
Psm	MSBToNative		(	Psm		inC		)
{
	#ifdef IS_MSB
	return inC;
	#else
	if( inC >= PSM_CLUT8 )	return inC;
	return Psm( uint(inC)^1 );
	#endif
}

template <>	inline
Psm	NativeToLSB		(	Psm		inC		)
{
	#ifdef IS_LSB
	return inC;
	#else
	if( inC >= PSM_CLUT8 )	return inC;
	return Psm( uint(inC)^1 );
	#endif
}

template <>	inline
Psm	NativeToMSB		(	Psm		inC		)
{
	#ifdef IS_MSB
	return inC;
	#else
	if( inC >= PSM_CLUT8 )	return inC;
	return Psm( uint(inC)^1 );
	#endif
}

template < typename tC > inline
tC	ConvertLSB	(	tC&		inC		)
{
	return ( inC = LSBToNative(inC) );
}

template < typename tC > inline
tC	ConvertMSB	(	tC&		inC		)
{
	return ( inC = MSBToNative(inC) );
}

inline uint Ftoui( float v )
{
	if( v < 0.0f )			return 0;
	if( v < 4194304.0f )	return uint(int(v));
	return uint((int(v*0.5f))<<1);
}

inline int32 FTOI_19_4( float v )
{
	union { float asFp; int32 asI; };
	asFp = v + 524288.0f /*float($80000)*/;
	return asI & 0x7FFFFF;
}

inline int32 FTOI_23_0( float v )
{
	union { float asFp; int32 asI; };
	asFp = v + 8388608.0f /*float($800000)*/;
	return asI & 0x7FFFFF;
}



template < typename T > inline
T RoundX( T inV, uint inA )
{
	return (inA ? ((T)((uint32(inV)+(inA-1))&(~(inA-1)))) : inV );
}

template < typename T > inline T	Round2( T inV )				{ return RoundX( inV, 2U  );  }
template < typename T > inline T	Round4( T inV )				{ return RoundX( inV, 4U  );  }
template < typename T > inline T	Round8( T inV )				{ return RoundX( inV, 8U  );  }
template < typename T > inline T	Round16( T inV )			{ return RoundX( inV, 16U );  }
template < typename T > inline T	Round32( T inV )			{ return RoundX( inV, 32U );  }
template < typename T > inline T	Round64( T inV )			{ return RoundX( inV, 64U );  }
template < typename T > inline T	Round128( T inV )			{ return RoundX( inV, 128U ); }
template < typename T > inline T	Round256( T inV )			{ return RoundX( inV, 256U ); }
template < typename T > inline T	Round512( T inV )			{ return RoundX( inV, 512U ); }
template < typename T > inline T	Round1024( T inV )			{ return RoundX( inV, 1024U); }


inline bool IsPow2( uint32 inV )
{
	return ((inV&(inV-1)) == 0);
}

inline bool IsPow2( uint64 inV )
{
	return ((inV&(inV-1)) == 0);
}

inline uint GetFloorPow2( uint32 inV )
{
	return GetGreatestBitNo( inV );
}

inline uint GetCeilPow2( uint32 inV )
{
	uint bhi = GetFloorPow2( inV );
	if( (1U<<bhi) < inV )
		bhi++;
	return bhi;
}

inline uint32 GetLowestPowOf2( uint32 v )
{
	return v & (v^(v-1));
}

inline uint64 GetLowestPowOf2( uint64 v )
{
	return v & (v^(v-1));
}

inline uint32 GetGreatestPowOf2( uint32 v )
{
	union { float asFp; int32 asI; };
	if( v==0 )		return 1;
	if( v>>31 )	{
		return 0x80000000;
	} else {
		asFp = float( int32(v) );
		return uint32(1<<((asI>>23)-127));
	}
}

inline uint64 GetGreatestPowOf2( uint64 v )
{
	uint32 lo = uint32( v & 0xFFFFFFFF );
	uint32 hi = uint32( v >> 32 );
	if( hi )
		return uint64(GetGreatestPowOf2(hi)) << 32;
	else
		return uint64(GetGreatestPowOf2(lo));
}

inline uint GetLowestBitNo( uint32  v )
{
	union { float asFp; int32 asI; };
	if( v == 0x80000000 ) {
		return 31;
	} else {
		asFp = float( (int32)GetLowestPowOf2(v) );
		return uint32((asI>>23)-127);
	}
}

inline uint GetLowestBitNo( uint64  v )
{
	uint32 lo = uint32( v & 0xFFFFFFFF );
	uint32 hi = uint32( v >> 32 );
	if( lo )
		return GetLowestBitNo( lo );
	else
		return GetLowestBitNo( hi ) + 32;
}

inline uint GetGreatestBitNo( uint32  v )
{
	union { float asFp; int32 asI; };
	if( v>>31 ) {
		return 31;
	} else {
		asFp = float( int32(v) );
		return uint32((asI>>23)-127);
	}
}

inline uint GetGreatestBitNo( uint64  v )
{
	uint32 lo = uint32( v & 0xFFFFFFFF );
	uint32 hi = uint32( v >> 32 );
	if( hi )
		return GetGreatestBitNo( hi ) + 32;
	else
		return GetGreatestBitNo( lo );
}


inline uint GetBitCount( uint32 a )
{
	// From graphics gems II
	a = (a&0x55555555) + ((a>>1) &(0x55555555));	// a: 16  2-bit tallies
	a = (a&0x33333333) + ((a>>2) &(0x33333333));	// a:  8  4-bit tallies
	a = (a&0x07070707) + ((a>>4) &(0x07070707));	// a:  4  8-bit tallies
	a = (a&0x000F000F) + ((a>>8) &(0x000F000F));	// a:  2 16-bit tallies
	a = (a&0x0000001F) + ((a>>16)&(0x0000001F));	// a:  1 32-bit tally
	return a;
}


inline uint GetBitCount( uint64 a )
{
	return GetBitCount(uint32(a)) + GetBitCount(uint32(a>>32));
}


inline uint32 GetBitRangeMask	(	uint	inFromNo,	uint	inToNo	)
{
	NV_ASSERT( inFromNo < 32 );
	NV_ASSERT( inToNo   < 32 );

	uint32 m;

	if( inFromNo == inToNo )
	{
		m = (1U << inFromNo);
	}
	else if( inToNo > inFromNo )
	{
		m = ~0U;
		m = ( m >> (inFromNo)  ) << (inFromNo);
		m = ( m << (31-inToNo) ) >> (31-inToNo);
		#ifdef _NVCOMP_ENABLE_DBG
		for( uint i = 0; i < 32 ; i++ ) {
			uint32 bi = (m>>i) & 1;
			if( i>=inFromNo && i<=inToNo )	{ NV_ASSERT( bi );	}
			else							{ NV_ASSERT( !bi ); }
		}
		#endif
	}
	else	// if( inFromNo > inToNo )
	{ 
		m = ~0U;
		m = ( m >> (inToNo+1)    ) << (inToNo+1);
		m = ( m << (32-inFromNo) ) >> (32-inFromNo);
		m = ~m;
		#ifdef _NVCOMP_ENABLE_DBG
		for( uint i = 0; i < 32 ; i++ ) {
			uint32 bi = (m>>i) & 1;
			if( i>=inFromNo || i<=inToNo )	{ NV_ASSERT( bi );	}
			else							{ NV_ASSERT( !bi ); }
		}
		#endif
	}

	return m;
}


inline uint	GetBitRangeCount	(	uint32	inV,	uint	inFromNo	)
{
	if( inFromNo > 31 )
		return 0;
	uint32 im = ~( inV >> inFromNo );
	return ( im ? GetLowestBitNo(im) : 32 );
}


inline void cpy64to32(	uint32*	dst32/*[2]*/,
						uint64*	src64/*[1]*/	)
{
	NV_ASSERT_A32( dst32 );
	NV_ASSERT_A64( src64 );
#ifdef _EE
	register uint64 tmp;
	asm volatile (
	"	.set	noreorder\n"
	"	ld		%0, 0(%2)\n"
	"	sw		%0, 0(%1)\n"
	"	dsrl32	%0, %0, 0\n"
	"	sw		%0, 4(%1)\n"
	"	.set	reorder\n"
	: "=&r"(tmp) : "r"(dst32),"r"(src64) );
#else
	dst32[0] = ((uint32*)src64)[0];
	dst32[1] = ((uint32*)src64)[1];
#endif
}

inline void cpy64HIto32(	uint32*	dst32/*[1]*/,
							uint64*	src64/*[1]*/	)
{
	NV_ASSERT_A32( dst32 );
	NV_ASSERT_A32( src64 );
#ifdef _EE
	register uint64 tmp;
	asm volatile (
	"	.set	noreorder\n"
	"	lw		%0, 4(%2)\n"
	"	sw		%0, 0(%1)\n"
	"	.set	reorder\n"
	: "=&r"(tmp) : "r"(dst32),"r"(src64) );
#else
	dst32[0] = ((uint32*)src64)[1];
#endif
}

inline void cpy64LOto32(	uint32*	dst32/*[1]*/,
							uint64*	src64/*[1]*/	)
{
	NV_ASSERT_A32( dst32 );
	NV_ASSERT_A32( src64 );
#ifdef _EE
	register uint64 tmp;
	asm volatile (
	"	.set	noreorder\n"
	"	lw		%0, 0(%2)\n"
	"	sw		%0, 0(%1)\n"
	"	.set	reorder\n"
	: "=&r"(tmp) : "r"(dst32),"r"(src64) );
#else
	dst32[0] = ((uint32*)src64)[0];
#endif
}


#endif // _NvCore_Def_H_


