/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_Jpeg_H_
#define	_NvCore_Jpeg_H_



namespace nv { namespace jpeg
{

	enum DecodePSM
	{
		OPSM_888X,		// RGB 32
		OPSM_5650,		// RGB 16
	};


	struct ImageDesc
	{
		int		width;
		int		height;
		int		numco;		// number of components
		int		bpp;		// bytes per pixel
		int		bpsl;		// bytes per scanline
	};


	uint		GetReserve			(										);
	uint		GetReserveFilling	(										);
	bool		SetReserve			(	uint		inBSize					);
	void		FreeReserve			(										);

	int			DecodeBegin			(	ImageDesc&	outDesc,
										byte*		inJpegAddr,
										uint		inJpegBSize,
										DecodePSM	inDecPSM = OPSM_888X	);

	bool		DecodeLine			(	byte*&		outPixels				);

	void		DecodeEnd			(										);

} }



#endif	//	_NvCore_Jpeg_H_



