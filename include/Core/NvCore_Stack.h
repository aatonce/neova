/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_Stack_H_
#define	_NvCore_Stack_H_



//
// WARNING for MIPS processors :
//
// For mips processor, stack-backtracing process uses several heuristics based
// on the "SYSTEM V ABI Mips processor" rules.
// The convenient alloca() memory allocation doesn't respect theses rules and
// you must be awared that theses heuristics *will fatally failed*
// in a function calling alloca() and undeterminated results can be expected !
//
//
// All the functions of this namespace have no implementation in MASTER mode
// (i.e. returns FALSE or NULL)
//



namespace nv { namespace stack
{

	struct Frame {
		pvoid			ptr;		// pointer
		uint			bsize;		// size in bytes
		pvoid			retAddr;	// return address
		pvoid			locAddr;	// local address
	};


	// Stack backtracing

	bool				GetFrame			(	Frame&		outF				);
	bool				GetBackFrame		(	Frame&		outBackF,
												Frame&		inF					);


	// Local informations

	pvoid				GetReturnAddr		(									);
	pvoid				GetLocalAddr		(									);


	// Output

	uint				OutputCallstack		(	Frame&		inFromF,
												pcstr		inLabel		= NULL	);

} }




#endif	// _NvCore_Stack_H_


