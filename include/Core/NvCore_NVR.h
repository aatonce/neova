/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_NVR_H_
#define	_NvCore_NVR_H_




namespace nv { namespace nvram
{

	enum Result
	{
		NVR_STATUS_READY		=	0,
		NVR_STATUS_UNFORMATTED	=	-1,
		NVR_STATUS_NOTFOUND		=	-2,
		NVR_STATUS_INVALID		=	-3,

		NVR_SUCCESS				=	0,
		NVR_NOTFOUND			=	-4,
		NVR_ISFULL				=	-5,
		NVR_MAXFILES			=	-6,
		NVR_MAXBLOCKS			=	-7,
		NVR_PROTECTED			=	-8,
		NVR_ALREADYEXIST		=	-9,
		NVR_ERROR				=	-10,
		NVR_CORRUPT				= 	-11,
		NVR_BUSY				= 	-12,
		NVR_ALLOCATION_FAILED	=   -13,
		NVR_SYSMAXFILES			=	-14,
		NVR_SYSMAXBLOCKS		=	-15,
		NVR_AUTHENTICATION	=	-16,
		NVR_ECC_CRIT				=	-17,
	};
	


	bool			SetProductCode		(	pcstr			inProductCode			);
	pcstr			GetProductCode		(											);
	uint			GetNbMax			(											);

	bool			IsReady				(	Result*			outLastResult	= NULL	);

	bool			GetStatus			(	uint			inNVRNo,
											uint32*			outFullBSize	= NULL,
											uint32*			outFreeBSize	= NULL,
											bool*			outSwitched		= NULL	);

	bool			Format				(	uint			inNVRNo					);

	bool			GetFirstRecord		(	uint			inNVRNo,
											pstr			outName			= NULL,
											uint32*			outBSize		= NULL	);

	bool			GetNextRecord		(	pstr			outName			= NULL,
											uint32*			outBSize		= NULL	);

	bool			CreateRecord		(	uint			inNVRNo,
											pcstr			inName,
											uint32			inBSize,
											pvoid			inBuffer		= NULL	);

	bool			WriteRecord			(	uint			inNVRNo,
											pcstr			inName,
											pvoid			inBuffer,
											uint32			inBSize,
											uint32			inBOffset		= 0		);

	bool			ReadRecord			(	uint			inNVRNo,
											pcstr			inName,
											pvoid			inBuffer,
											uint32			inBSize,
											uint32			inBOffset		= 0		);

	bool			DeleteRecord		(	uint			inNVRNo,
											pcstr			inName					);

} }




#endif	// _NvCore_NVR_H_



