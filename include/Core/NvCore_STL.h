/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_STL_H_
#define	_NvCore_STL_H_




namespace nv
{

	//
	// base iterator

	template < typename T >
	struct base_iterator
	{
		typedef	T		value_type;
		typedef	uint	size_type;
		typedef T*		pointer;
		typedef T&		reference;

						base_iterator	(												) {}
						base_iterator	(	T * inPtr									);
		base_iterator	operator +		(	int											) const;
		base_iterator	operator -		(	int											) const;
		void	 		operator +=		(	int											);
		void	 		operator -=		(	int											);
		void	 		operator ++		(												);
		void	 		operator --		(												);
		void	 		operator ++		(	int											);
		void	 		operator --		(	int											);
		size_type 		operator -		(	const base_iterator &						) const;
		base_iterator&	operator =		(	const base_iterator &						);
		bool		 	operator ==		(	const base_iterator &						) const;
		bool		  	operator !=		(	const base_iterator &						) const;
		bool		 	operator <		(	const base_iterator &						) const;
		bool		 	operator >		(	const base_iterator &						) const;
		bool		 	operator <=		(	const base_iterator &						) const;
		bool		 	operator >=		(	const base_iterator &						) const;
		pointer			operator ->		(												) const;
		reference		operator *		(												) const;
		pointer			operator &		(												) const;

	private:
		pointer			ptr;
	};


	//
	// base vector

	struct base_vector
	{
		void			_init			(	uint, uint									);
		void			_init			(	uint, uint, uint							);
		void			_init			(	uint, uint, uint, uint8*					);
		void			_init			(	uint, base_vector*							);
		void			_init			(	uint, uint, uint8*, uint8*					);
		void			_free			(												);
		uint32			_max_size		(												) const;
		void			_reserve		(	uint										);
		void			_mem_reserve	(	uint32										);
		void			_push_back		(	uint8*										);
		uint8*			_alloc_back		(	uint										);
		void			_pop_back		(												);
		void			_swap			(	base_vector *								);
		uint8*			_mem_insert		(	uint8*, uint32								);
		uint8*			_insert			(	uint8*, uint8*								);
		void			_insert			(	uint8*, uint8*, uint8*						);
		void			_insert			(	uint8*, uint, uint8*						);
		uint8*			_mem_erase		(	uint8*, uint32								);
		uint8*			_erase			(	uint8*										);
		uint8*			_erase			(	uint8*, uint8*								);
		void			_resize			(	uint, uint8*								);
	protected:
		uint8*			vfirst;
		uint8*			vlast;
		uint8*			vend;
		uint16			vtbsize;
		uint16			vmflags;
	};


	//
	// base tree (Red-Black)
	// Double linked nodes for fast traversal.

	struct base_tree
	{
		enum _Color {
			NC_RED,
			NC_BLACK,
			NC_DUP
		};

		struct _Node {
			_Color		_color;
			_Node*		_parent;
			_Node*		_left;
			_Node*		_right;
			_Node*		_next;
			_Node*		_prev;
		};

		void			_init			(												);
		void			_shut			(												);
		_Node*			_insert			(	_Node*										);
		_Node*			_insertUnique	(	_Node*										);
		void			_erase			(	_Node*										);
		_Node*			_find			(	_Node*										);
		void			_clear			(												);
		_Node*			_nextUnique		(	_Node*										);
		_Node*			_prevUnique		(	_Node*										);
	protected:
		_Node			_nil;
		_Node*			_root;
		uint			_size;
		_Node*			_first;
		_Node*			_last;
		void			_rotateRight	(	_Node*										);
		void			_rotateLeft		(	_Node*										);
		void			_fixInsert		(	_Node*										);
		void			_fixErase		(	_Node*										);
		_Node*			_doInsert		(	_Node*	,	bool							);
		void			_doErase		(	_Node*										);
		void			_dllErase		(	_Node*										);
		void			_dllInsertBefore(	_Node*	,	_Node*							);
		void			_dllAddAfter	(	_Node*	,	_Node*							);
		_Node*			_treeNext		(	_Node*										);
		_Node*			_treePrev		(	_Node*										);
		_Node*			_findMin		(	_Node*										);
		_Node*			_findMax		(	_Node*										);
		void			_initNode		(	_Node*										);
		void			_replaceTreeNode(	_Node*	,	_Node*							);
		virtual int		_compareNode	(	const _Node*, const _Node*					) const = 0;	// <0 (lt), >0 (gt), ==0 (eq)
	};


	//
	// Init/Shut vector
	// no item's constructor/destructor calls !
	// This class of vector uses manual initialisation/destruction.
	// The nv::mem::AllocFlags can be selected in the Init() function.

	template <	typename		T	>
	struct sysvector : public base_vector
	{
		typedef T					value_type;
		typedef	uint				size_type;
		typedef T*					pointer;
		typedef T&					reference;
		typedef base_iterator<T>	iterator;

		void			Init			(	uint inAllocFlags = nv::mem::AFC_DEFAULT_VECTOR	);
		void			Shut			(													);

		sysvector&		operator =		(	const sysvector<T> &							);
		reference		operator []		(	size_type										);
		reference		at				(	size_type										);
		pointer			data			(													) const;	// (size() ? &[0] : NULL)
		iterator		begin			(													) const;
		iterator		end				(													) const;
		size_type		size			(													) const;
		size_type		max_size		(													) const;
		size_type		capacity		(													) const;
		bool			empty			(													) const;
		void			reserve			(	size_type										);
		reference		front			(													) const;
		reference		back			(													) const;
		reference		alloc_back		(	size_type										);
		void			push_back		(	const T &										);
		void			pop_back		(													);
		void			swap			(	sysvector<T> &									);
		iterator		insert			(	const iterator, const T &						);
		void			insert			(	const iterator, const iterator, const iterator	);			// [first,last)
		void			insert			(	const iterator, size_type, const T &			);
		iterator		erase			(	const iterator									);
		iterator		erase			(	const iterator, const iterator					);			// [first,last)
		void			clear			(													);
		void			copy			(	const sysvector<T> &							);
		void			copy			(	const iterator, const iterator					);
		void			resize			(	size_type, const T & t = T()					);
		void			free			(													);			// reserve(0)
	};


	//
	// Generic user vector
	// see STL homepage (http://www.sgi.com/tech/stl/index.html)
	// no item's constructor/destructor calls !
	// This class of vector uses the nv::mem::AFC_DEFAULT_VECTOR flags & automatic initialisation/destruction.

	template <	typename		T	>
	struct vector : public sysvector<T>
	{
		typedef typename sysvector<T>::value_type	value_type;
		typedef	typename sysvector<T>::size_type	size_type;
		typedef typename sysvector<T>::pointer		pointer;
		typedef typename sysvector<T>::reference	reference;
		typedef typename sysvector<T>::iterator		iterator;

						vector			(													);
					   ~vector			(													);
						vector			(	size_type										);
						vector			(	size_type, const T &							);
						vector			(	const sysvector<T> &							);
						vector			(	const iterator, const iterator					);			// [first,last)
		vector  &		operator =		(	const sysvector<T> &							);
		reference		operator []		(	size_type										);
	};


	//
	// Engine user vector
	// no item's constructor/destructor calls !
	// This class of vector uses the nv::mem::AFC_ENGINE_VECTOR flags & automatic initialisation/destruction.

	template <	typename		T	>
	struct evector : public sysvector<T>
	{
		typedef typename sysvector<T>::value_type	value_type;
		typedef	typename sysvector<T>::size_type	size_type;
		typedef typename sysvector<T>::pointer		pointer;
		typedef typename sysvector<T>::reference	reference;
		typedef typename sysvector<T>::iterator		iterator;

						evector			(													);
					   ~evector			(													);
						evector			(	size_type										);
						evector			(	size_type, const T &							);
						evector			(	const sysvector<T> &							);
						evector			(	const iterator, const iterator					);			// [first,last)
		evector  &		operator =		(	const sysvector<T> &							);
		reference		operator []		(	size_type										);
	};


	//
	// Game user vector
	// no item's constructor/destructor calls !
	// This class of vector uses the nv::mem::AFC_GAME_VECTOR flags & automatic initialisation/destruction.

	template <	typename		T	>
	struct gvector : public sysvector<T>
	{
		typedef typename sysvector<T>::value_type	value_type;
		typedef	typename sysvector<T>::size_type	size_type;
		typedef typename sysvector<T>::pointer		pointer;
		typedef typename sysvector<T>::reference	reference;
		typedef typename sysvector<T>::iterator		iterator;

						gvector			(													);
					   ~gvector			(													);
						gvector			(	size_type										);
						gvector			(	size_type, const T &							);
						gvector			(	const sysvector<T> &							);
						gvector			(	const iterator, const iterator					);			// [first,last)
		gvector  &		operator =		(	const sysvector<T> &							);
		reference		operator []		(	size_type										);
	};


	//
	// Generic tree Red-Black
	// Node are only managed. The user has the responsability of nodes allocation/destruction.
	// Compare object are similar to STL's Strict Weak Order object.
	//
	//		struct MyCmp
	//		{
	//			bool operator () ( const Key& k0, const Key& k1 ) const
	//			{
	//				// ...
	//			}
	//		}

	template < typename Key, typename Compare >
	struct tree : public base_tree
	{
		struct Node : public _Node {
			Key			key;
			Node*		parent			(			);		// get
			void		parent			( Node*		);		// set
			Node*		left			(			);		// get
			void		left			( Node*		);		// set
			Node*		right			(			);		// get
			void		right			( Node*		);		// set
			Node*		next			(			);		// get
			void		next			( Node*		);		// set
			Node*		prev			(			);		// get
			void		prev			( Node*		);		// set
		};

		void			Init			(													);
		void			Shut			(													);

		virtual Node*	insert			(	Node*			inNode							);
		virtual Node*	insertUnique	(	Node*			inNode							);
		virtual void	erase			(	Node*			inNode							);
		virtual Node*	find			(	const Key&		inKey							);
		virtual void	clear			(													);
		virtual Node*	first			(													);
		virtual Node*	last			(													);
		virtual uint	size			(													);
		virtual Node*	nextUnique		(	Node*			inNode							);
		virtual Node*	prevUnique		(	Node*			inNode							);
	protected:
		Compare			compareKey;
		virtual int		_compareNode	(	const _Node*	inNode0,
											const _Node*	inNode1							) const;
	};


	//
	// Pooled tree
	// This class manages a pool of nodes.

	template < typename Key, typename Compare >
	struct ptree : public tree<Key,Compare>
	{
		typedef typename tree<Key,Compare>::Node	Node;

		void			Init			(													);
		void			Shut			(													);

		virtual Node*	insert			(	const Key&		inKey							);
		virtual Node*	insertUnique	(	const Key&		inKey							);
		virtual void	erase			(	Node*			inNode							);
		virtual void	erase			(	const Key&		inKey							);
		virtual void	clear			(													);
		virtual void	freemem			(													);
	protected:
		Node*	poolList;
		Node*	freeList;
		virtual	Node*	allocNode		(													);
		virtual	void	freeNode		(	Node*			inNode							);
		virtual void*	allocPool		(	uint32&			outBSize,	uint	inNodeBSize	) = 0;
		virtual void	freePool		(	void*			inPointer						) = 0;
	};

}




#include "NvCore_STL_I.h"

#endif // _NvCore_STL_H_


