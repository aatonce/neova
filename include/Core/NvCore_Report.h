/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_Report_H_
#define	_NvCore_Report_H_



//
// Reporting basic/once/break

#undef	NV_REPORT
#undef	NV_REPORT_ONCE
#undef	NV_REPORT_BREAK

#if defined( _NVCOMP_ENABLE_CONSOLE )
	#ifdef  _NVCOMP_ENABLE_DBG
		#define NV_REPORT(inTYPE,inMSG)		nv::report::Output( inTYPE, inMSG, __FILE__, __FUNCTION__, __LINE__ )
	#else
		#define NV_REPORT(inTYPE,inMSG)		nv::report::Output( inTYPE, inMSG )
	#endif

	#define	NV_REPORT_ONCE(inTYPE,inMSG)										\
			{	static bool skip = FALSE; if(!skip) {							\
					NV_REPORT( inTYPE, inMSG );									\
					skip = TRUE;												\
			} }

	#define NV_REPORT_BREAK(inTYPE,inMSG)										\
			{	static bool skip = FALSE; if(!skip) {							\
					NV_REPORT( inTYPE, inMSG );									\
					nv::report::Action act = nv::report::AskUserAction();		\
					if( act == nv::report::A_DONTCARE )		skip = TRUE;		\
					else if( act == nv::report::A_EXIT )	nv::core::Exit();	\
					else if( act == nv::report::A_BREAK )	NV_BREAK();			\
			} }
#else
	#define NV_REPORT(inTYPE,inMSG)			{}
	#define	NV_REPORT_ONCE(inTYPE,inMSG)	{}
	#define NV_REPORT_BREAK(inTYPE,inMSG)	{ NV_BREAK(); }
#endif



//
// Reporting MESSAGE/WARNING/ERROR

#undef	NV_MESSAGE
#undef	NV_WARNING
#undef	NV_MESSAGE_ONCE
#undef	NV_WARNING_ONCE
#undef	NV_ERROR
#define	NV_MESSAGE(inMSG)					NV_REPORT( nv::report::T_MESSAGE, inMSG )
#define NV_WARNING(inMSG)					NV_REPORT( nv::report::T_WARNING, inMSG )
#define NV_WARNING_BREAK(inMSG)				NV_REPORT_BREAK( nv::report::T_WARNING, inMSG )
#define	NV_MESSAGE_ONCE(inMSG)				NV_REPORT_ONCE( nv::report::T_MESSAGE, inMSG )
#define NV_WARNING_ONCE(inMSG)				NV_REPORT_ONCE( nv::report::T_WARNING, inMSG )
#define NV_ERROR(inMSG)						NV_REPORT_BREAK( nv::report::T_ERROR, inMSG )



//
// DEBUG assertion & reporting

#undef	NV_BREAK
#undef	NV_BREAK_IF
#undef	NV_ASSERT
#undef	NV_ASSERT_IF
#undef	NV_ASSERTC
#undef	NV_ASSERT_ALIGNED
#undef	NV_ASSERT_A32
#undef	NV_ASSERT_A64
#undef	NV_ASSERT_A128
#undef	NV_ASSERT_NA128
#undef	NV_ASSERT_A256
#undef	NV_ASSERT_NA256
#undef	NV_ASSERT_A512
#undef	NV_ASSERT_RESULT
#undef	NV_ASSERT_RETURN_MTH
#undef	NV_ASSERT_RETURN_CAL
#undef	NV_DEBUG_MESSAGE
#undef	NV_DEBUG_WARNING

#if defined( _NVCOMP_ENABLE_DBG )
	#if defined(_EE) && defined(__GNUC__)
		#define	NV_BREAK()					{ asm volatile ("break"); }
	#elif defined(_EE) && defined(__MWERKS__)
		#define	NV_BREAK()					{ asm volatile ("breakc 0"); }
	#elif defined(_PSP) && defined(__GNUC__)
		#define	NV_BREAK()					{ asm volatile ("break"); }
	#elif defined(_PSP) && defined(__MWERKS__)
		#define	NV_BREAK()					{ asm volatile ("breakc 0"); }
	#elif defined(_NGC) && defined(__MWERKS__)
		#define NV_BREAK()					{ asm volatile { mfmsr r0; ori r11,r0,0x400; mtmsr r11; mtmsr r0; } }
	#elif defined(_MSC_VER) || defined(_WIN32)
		#define	NV_BREAK()					{ _asm int 3 }
//		#define	NV_BREAK()					DebugBreak()
//		void DebugBreak(void);
	#else
		#define	NV_BREAK()					{ *((uint*)NULL) = 0xDEADF00D; }	// default is bus error !
	#endif
	#define	NV_BREAK_IF(inC)				{ if(inC)    { NV_BREAK(); }		}
	#define NV_ASSERT(inC)					{ if(!(inC)) { NV_ERROR( "Assert has failed on "#inC ); } }
	#define NV_ASSERT_IF(inC0,inC1)			{ if(inC0)	 { NV_ASSERT(inC1); } }
	#define NV_ASSERTC(inC, inMSG)			{ if(!(inC)) { NV_ERROR(inMSG); } }
	#define NV_ASSERT_ALIGNED(inC,inA)		NV_ASSERTC( (inC)&&((uint32(inC)&((inA)-1))==0), "Data must be aligned on a "#inA" bytes boundary and not NULL !" )
	#define NV_ASSERT_A32(inC)				NV_ASSERT_ALIGNED(inC,4)
	#define NV_ASSERT_A64(inC)				NV_ASSERT_ALIGNED(inC,8)
	#define NV_ASSERT_A128(inC)				NV_ASSERT_ALIGNED(inC,16)
	#define NV_ASSERT_NA128(inC)			NV_ASSERTC( ((uint32(inC)&0xF)==0), "Data must be aligned on a 16 bytes boundary !" )
	#define NV_ASSERT_A256(inC)				NV_ASSERT_ALIGNED(inC,32)
	#define NV_ASSERT_NA256(inC)			NV_ASSERTC( ((uint32(inC)&0x1F)==0), "Data must be aligned on a 32 bytes boundary !" )
	#define NV_ASSERT_A512(inC)				NV_ASSERT_ALIGNED(inC,64)
	#define NV_ASSERT_RESULT(inC)			NV_ASSERT( inC )
	#define	NV_ASSERT_RETURN_MTH(inRes,inC)	{ if(!(inC)) { NV_ASSERT(inC); return inRes; } }
	#define	NV_ASSERT_RETURN_CAL(inC)		{ if(!(inC)) { NV_ASSERT(inC); return; } }
	#define	NV_DEBUG_MESSAGE(inMSG)			NV_MESSAGE( inMSG )
	#define	NV_DEBUG_WARNING(inMSG)			NV_WARNING( inMSG )
#else
	#define	NV_BREAK()						{}
	#define	NV_BREAK_IF(inC)				{}
	#define	NV_ASSERT(inC)					{}
	#define	NV_ASSERT_IF(inC0,inC1)			{}
	#define	NV_ASSERTC(inC,inMSG)			{}
	#define NV_ASSERT_ALIGNED(inC,inA)		{}
	#define NV_ASSERT_A32(inC)				{}
	#define NV_ASSERT_A64(inC)				{}
	#define NV_ASSERT_A128(inC)				{}
	#define NV_ASSERT_NA128(inC)			{}
	#define NV_ASSERT_A256(inC)				{}
	#define NV_ASSERT_NA256(inC)			{}
	#define NV_ASSERT_A512(inC)				{}
	#define NV_ASSERT_RESULT(inC)			{ (inC); }
	#define	NV_ASSERT_RETURN_MTH(inRes,inC)	{ if(!(inC)) { return inRes; } }
	#define	NV_ASSERT_RETURN_CAL(inC)		{ if(!(inC)) { return; } }
	#define	NV_DEBUG_MESSAGE(inMSG)			{}
	#define	NV_DEBUG_WARNING(inMSG)			{}
#endif



//
// Compile time assert

#undef	NV_COMPILE_TIME_ASSERT
#undef	NV_COMPILE_TIME_ASSERT_ISPOD
#undef	NV_COMPILE_TIME_ASSERT_ISRVALUE
#define	NV_COMPILE_TIME_ASSERT(inC)				{ typedef int	__compile_time_assert_fail[1-2*!(inC)]; }
#define	NV_COMPILE_TIME_ASSERT_ISPOD( inC )		{ typedef union __compile_time_assert_ispod { inC t; };	}	// non-POD type not allowed in union (C++ standard)
#define	NV_COMPILE_TIME_ASSERT_ISRVALUE(inV)	{ typedef int	__compile_time_assert_isrvalue[inV];	}




namespace nv { namespace report
{

	enum Type {
		T_MESSAGE	= 0,
		T_WARNING,
		T_ERROR
	};

	enum Action {
		A_DONTCARE	= 0,
		A_CONTINUE,
		A_BREAK,
		A_EXIT
	};

	void		Output			(	Type		inType,
									pcstr		inMessage,
									pcstr		inFilename	= NULL,
									pcstr		inFctName	= NULL,
									int			inLineNo	= -1		);

	Action		AskUserAction	(										);

} }


#endif	// _NvCore_Report_H_


