/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_Startup_H_
#define	_NvCore_Startup_H_




#ifndef NVAPP_GAME_STARTUP
#define	NVAPP_GAME_STARTUP				GameStartup
#endif




//
// Win32

#if defined(_WIN32) || defined(_MSC_VER)

	struct HINSTANCE__;
	#define	NVCORE_STARTUP_TARGS			void*, char*, uint32
	#define NVAPP_STARTUP					int __stdcall WinMain( HINSTANCE__* hInstance, HINSTANCE__*, char* strCmdLine, int )
	#define NVAPP_CORE_STARTUP() \
		{ \
			uint32 base_sp; \
			__asm { mov base_sp, EBP }; \
			nv::core::Startup__( hInstance, strCmdLine, base_sp ); \
		}

	#define	NVAPP_STARTUP_INIT				{}
	#define	NVAPP_STARTUP_SHUT				{}
	#define	NVAPP_DECL						NVAPP_NEWDELETE

#endif // WIN32




//
// SCE Playstation

#if defined(_EE) || defined(_PSP)

	#define	NVCORE_STARTUP_TARGS			int, char**, uint32
	#define NVAPP_STARTUP					int main( int argc, char ** argv )
	#define NVAPP_CORE_STARTUP() \
		{ \
			uint32 base_sp; \
			asm volatile( "sw $29, 0(%0); sync.p" :: "r"(&base_sp) ); \
			nv::core::Startup__( argc, argv, base_sp ); \
		}

	#if defined(__MWERKS__)
		#define	NVAPP_STARTUP_INIT			{ mwInit(); }
		#define	NVAPP_STARTUP_SHUT			{ mwExit(); }
		#define NVAPP_MW_DECL \
			extern "C" { \
				void mwInit(void); \
				void mwExit(void); \
			}
	#else
		#define	NVAPP_STARTUP_INIT
		#define	NVAPP_STARTUP_SHUT
		#define NVAPP_MW_DECL
	#endif

	#define	NVAPP_DECL \
		NVAPP_MW_DECL \
		NVAPP_NEWDELETE

#endif // EE or PSP




//
// SCE Playstation Portable

#if defined(_PSP)

	#include <kernel.h>
	#define NVAPP_PSP_MODNAME		"NeovaApplication"
	#define NVAPP_PSP_MODATTR		(SCE_MODULE_ATTR_EXCLUSIVE_LOAD|SCE_MODULE_ATTR_EXCLUSIVE_START)
	#define NVAPP_PSP_MODMAJOR		(1)
	#define NVAPP_PSP_MODMINOR		(1)
	#define NVAPP_PSP_HEAPKBSIZE	(20<<10)
	#define NVAPP_PSP_STACKKBSIZE	(64)
	#define NVAPP_PSP_PRIORITY		(SCE_KERNEL_MODULE_INIT_PRIORITY)
	#define NVAPP_PSP_ATTRIBUTE		(SCE_KERNEL_TH_UMODE | SCE_KERNEL_TH_USE_VFPU)

	#if defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus)
		#define NVAPP_PSP_MODINFO( name, attribute, major, minor ) \
			extern int _gp[]; \
			SCE_LIB_TABEL_ADDRESS_DEFINE(); \
			extern SceModuleInfo __psp_moduleinfo SCE_MODULE_INFO_SECTION = { \
				attribute, { minor, major }, name, 0, static_cast<void*>(_gp), \
				static_cast<const void*>(__begin_of_section_lib_ent+1), \
				static_cast<const void*>(__end_of_section_lib_ent), \
				static_cast<const void*>(__begin_of_section_lib_stub+1), \
				static_cast<const void*>(__end_of_section_lib_stub) }; \
			SCE_LIB_EXPORT_VAR_OTHER_NAME( module_info, __psp_moduleinfo, )
	#else
		#define NVAPP_PSP_MODINFO( name, attribute, major, minor ) \
			extern int _gp[]; \
			SCE_LIB_TABEL_ADDRESS_DEFINE(); \
			SceModuleInfo __psp_moduleinfo SCE_MODULE_INFO_SECTION = { \
				attribute, { minor, major }, name, 0, (void*)_gp, \
				(const void*)(__begin_of_section_lib_ent+1), \
				(const void*)(__end_of_section_lib_ent), \
				(const void*)(__begin_of_section_lib_stub+1), \
				(const void*)(__end_of_section_lib_stub) }; \
			SCE_LIB_EXPORT_VAR_OTHER_NAME( module_info, __psp_moduleinfo, )
	#endif

	#define	NVAPP_PSP_MODVARS \
		NVAPP_PSP_MODINFO( NVAPP_PSP_MODNAME, NVAPP_PSP_MODATTR, NVAPP_PSP_MODMAJOR, NVAPP_PSP_MODMINOR ); \
		extern "C" { \
			SceInt32 sce_newlib_heap_kb_size  = NVAPP_PSP_HEAPKBSIZE; \
			SceInt32 sce_newlib_stack_kb_size = NVAPP_PSP_STACKKBSIZE; \
			SceInt32 sce_newlib_priority      = NVAPP_PSP_PRIORITY; \
			unsigned int sce_newlib_attribute = NVAPP_PSP_ATTRIBUTE; \
		}

	#if defined(__MWERKS__)
		#define NVAPP_MW_DECL_PSP \
			extern "C" { \
				void _init(void); \
				void _fini(void); \
				void _init(void){} \
				void _fini(void){} \
			}
	#else
		#define NVAPP_MW_DECL_PSP {}
	#endif

	#undef NVAPP_DECL
	#define	NVAPP_DECL \
		NVAPP_PSP_MODVARS \
		NVAPP_MW_DECL \
		NVAPP_MW_DECL_PSP \
		NVAPP_NEWDELETE

#endif // PSP




//
// Nintendo GameCube & Revolution

#if defined(_NGC)

	#define	NVCORE_STARTUP_TARGS			int, char**
	#define NVAPP_STARTUP					int main( int argc, char ** argv )
	#define NVAPP_CORE_STARTUP() \
		{ \
			nv::core::Startup__( argc, argv ); \
		}

	#define	NVAPP_STARTUP_INIT			{ OSInit(); }
	#define	NVAPP_STARTUP_SHUT			{}

	#define	NVAPP_DECL \
		extern "C" { \
			void OSInit(void); \
		} \
		NVAPP_NEWDELETE

#endif // NGC



#if defined(_PS3)

#define	NVCORE_STARTUP_TARGS			int, char**
#define NVAPP_STARTUP					int main( int argc, char ** argv )
#define NVAPP_CORE_STARTUP() \
		{ \
			nv::core::Startup__( argc, argv ); \
		}

#define	NVAPP_STARTUP_INIT			{ }
#define	NVAPP_STARTUP_SHUT			{ }

#define	NVAPP_DECL \
		NVAPP_NEWDELETE

#endif // _PS3




//
// The CoreEngine startup function.
// Must be called first, before *ANY* other function and especially before the functions :
// - nv::core::SetParameter()
// - nv::core::GetParameter()
// - nv::core::Init()
//
// This startup function is normally called by the GameMain() macro.

namespace nv { namespace core
{
	bool		Startup__		(	NVCORE_STARTUP_TARGS						);
} }




//
// Game abstract entry point
// Usage :
//
// void GameMain()
// {
//		...
// }
//
// will introduce in-place the platform specific entry-point.

#ifndef GameMain
#define	GameMain()						\
										\
	NVAPP_GAME_STARTUP();				\
										\
	NVAPP_DECL							\
										\
	NVAPP_STARTUP						\
	{									\
		NV_COMPILE_TIME_CHECK_TYPES();	\
		NVAPP_STARTUP_INIT				\
		NVAPP_CORE_STARTUP();			\
		NVAPP_GAME_STARTUP();			\
		nv::core::Shut();				\
		NVAPP_STARTUP_SHUT				\
		nv::core::Exit();				\
		return 0;						\
	}									\
										\
	void NVAPP_GAME_STARTUP()

#endif // GameMain




#endif	// _NvCore_Startup_H_



