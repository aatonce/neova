/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_Mem_H_
#define	_NvCore_Mem_H_



namespace nv { namespace mem
{
	enum AllocFlags
	{
		// Part
		AF_PART_UNDEF		= (0<<0),
		AF_PART_GAME		= (1<<0),
		AF_PART_ENGINE		= (2<<0),
		AF_PART_RSC			= (3<<0),
		AF_PART_MASK		= (3<<0),
		// Type
		AF_TYPE_UNDEF		= (0<<2),
		AF_TYPE_VECTOR		= (1<<2),
		AF_TYPE_OBJECT		= (2<<2),
		AF_TYPE_INSTANCE	= (3<<2),
		AF_TYPE_DATA		= (4<<2),
		AF_TYPE_MASK		= (7<<2),
		// Data
		AF_DATA_UNDEF		= (0<<5),
		AF_DATA_ANIM		= (1<<5),
		AF_DATA_MESH		= (2<<5),
		AF_DATA_SURFACE		= (3<<5),
		AF_DATA_BITMAP		= (4<<5),
		AF_DATA_IMAGE		= (5<<5),
		AF_DATA_MOVIE		= (6<<5),
		AF_DATA_SOUND		= (7<<5),
		AF_DATA_MUSIC		= (8<<5),
		AF_DATA_MASK		= (15<<5),
		//
		AF_STATIC			= (1<<9),
		AF_RESIZABLE		= (1<<10),
		//
		// Some combos ...
		//
		AFC_ENGINE_DEFAULT	= (AF_PART_ENGINE),
		AFC_ENGINE_OBJECT	= (AFC_ENGINE_DEFAULT|AF_TYPE_OBJECT),
		AFC_ENGINE_STATIC	= (AFC_ENGINE_DEFAULT|AF_STATIC),
		AFC_ENGINE_VECTOR	= (AFC_ENGINE_DEFAULT|AF_TYPE_VECTOR|AF_RESIZABLE),
		AFC_GAME_DEFAULT	= (AF_PART_GAME),
		AFC_GAME_OBJECT		= (AFC_GAME_DEFAULT|AF_TYPE_OBJECT),
		AFC_GAME_STATIC		= (AFC_GAME_DEFAULT|AF_STATIC),
		AFC_GAME_VECTOR		= (AFC_GAME_DEFAULT|AF_TYPE_VECTOR|AF_RESIZABLE),
		AFC_RSC_DEFAULT		= (AF_PART_RSC),
	#ifdef _NVENGINE
		AFC_DEFAULT			= (AFC_ENGINE_DEFAULT),
	#else
		AFC_DEFAULT			= (AFC_GAME_DEFAULT),
	#endif
		AFC_DEFAULT_OBJECT	= (AFC_DEFAULT|AF_TYPE_OBJECT),
		AFC_DEFAULT_VECTOR	= (AFC_DEFAULT|AF_TYPE_VECTOR|AF_RESIZABLE)
	};

	enum RangeStatus
	{
		RS_NOT_AVAILABLE	= 0,			// no information ...
		RS_OUTSIDE,							// the range doesn't belong to the heap !
		RS_OVERLAP,							// the range overlaps several blocks
		RS_FREE,							// the range is integrally in a free block
		RS_ALLOCATED						// the range is integrally in an allocated block
	};

	//
	// Global Dynamic Memory Managment

	void*			AllocInPlace			(	void*			inPointer,
												uint32			inBSize				);
	void*			Alloc					(	uint32			inBSize,
												uint			inFlags				);
	void*			AllocA					(	uint32			inBSize,
												uint			inBAlignment,
												uint			inFlags				);
	void*			Realloc					(	void*			inPointer,
												uint32			inBSize,
												uint			inFlags				);
	void			Free					(	void*			inPointer			);

	void			EnableProfiling			(										);
	void			DisableProfiling		(										);
	bool			IsProfiling				(										);
	uint32			GetTotBSize				(										);
	uint32			GetAllocCpt				(										);
	uint32			GetAllocTotBSize		(										);
	uint32			GetAllocTotBSizePeak	(										);
	uint32			GetAllocNb				(										);
	uint32			GetAllocNbPeak			(										);
	RangeStatus		CheckRange				(	void*			inPointer,
												uint32			inBSize				);
} }





namespace nv
{

	//
	// AFC_ENGINE_DEFAULT allocation / recycling

	void*		EngineMalloc		(	uint32				inBSize			);
	void*		EngineMallocA		(	uint32				inBSize,
										uint				inBAlignment	);
	void*		EngineCalloc		(	uint				inN,
										uint32				inBSize			);
	void*		EngineRealloc		(	void*				inPtr,
										uint32				inBSize			);
	void		EngineFree			(	void*				inPtr			);


	//
	// AFC_GAME_DEFAULT allocation / recycling

	void*		GameMalloc			(	uint32				inBSize			);
	void*		GameMallocA			(	uint32				inBSize,
										uint				inBAlignment	);
	void*		GameCalloc			(	uint				inN,
										uint32				inBSize			);
	void*		GameRealloc			(	void*				inPtr,
										uint32				inBSize			);
	void		GameFree			(	void*				inPtr			);


	//
	// AFC_RSC_DEFAULT allocation / recycling

	void*		RscMalloc			(	uint32				inBSize			);
	void*		RscMallocA			(	uint32				inBSize,
										uint				inBAlignment	);
	void*		RscCalloc			(	uint				inN,
										uint32				inBSize			);
	void*		RscRealloc			(	void*				inPtr,
										uint32				inBSize			);
	void		RscFree				(	void*				inPtr			);


	//
	// In-place T() & ~T()

	template <typename T> inline
	T*			ConstructInPlace	(	T*					inPtr			);

	template <typename T> inline
	void*		DestructInPlace		(	T*					inPtr			);

}



//
// ENGINE/GAME default selector

#ifdef _NVENGINE
	#define		NvMalloc				nv::EngineMalloc
	#define		NvMallocA				nv::EngineMallocA
	#define		NvFree					nv::EngineFree
	#define		NvCalloc				nv::EngineCalloc
	#define		NvRealloc				nv::EngineRealloc
#else
	#define		NvMalloc				nv::GameMalloc
	#define		NvMallocA				nv::GameMallocA
	#define		NvFree					nv::GameFree
	#define		NvCalloc				nv::GameCalloc
	#define		NvRealloc				nv::GameRealloc
#endif



//
// Engine|Game New & Delete without operators new,delete
// nv::DestructInPlace<T>(ptr) directly calls the object destructor.
// Consequently, your object *must have* a desctructor, even empty ({}).

#define			NvEngineNew(T)			nv::ConstructInPlace<T>((T*)nv::mem::Alloc ( sizeof(T),           nv::mem::AFC_ENGINE_OBJECT ))
#define			NvEngineNewA(T,A)		nv::ConstructInPlace<T>((T*)nv::mem::AllocA( sizeof(T),        A, nv::mem::AFC_ENGINE_OBJECT ))
#define			NvEngineNewS(T,SPLY)	nv::ConstructInPlace<T>((T*)nv::mem::Alloc ( sizeof(T)+(SPLY),    nv::mem::AFC_ENGINE_OBJECT ))
#define			NvEngineNewAS(T,A,SPLY)	nv::ConstructInPlace<T>((T*)nv::mem::AllocA( sizeof(T)+(SPLY), A, nv::mem::AFC_ENGINE_OBJECT ))
template<typename T> inline
void			NvEngineDelete(T* ptr)	{ nv::mem::Free( (pvoid)nv::DestructInPlace<T>(ptr) ); }


#define			NvGameNew(T)			nv::ConstructInPlace<T>((T*)nv::mem::Alloc ( sizeof(T),           nv::mem::AFC_GAME_OBJECT ))
#define			NvGameNewA(T,A)			nv::ConstructInPlace<T>((T*)nv::mem::AllocA( sizeof(T),        A, nv::mem::AFC_GAME_OBJECT ))
#define			NvGameNewS(T,SPLY)		nv::ConstructInPlace<T>((T*)nv::mem::Alloc ( sizeof(T)+(SPLY),	  nv::mem::AFC_GAME_OBJECT ))
#define			NvGameNewAS(T,A,SPLY)	nv::ConstructInPlace<T>((T*)nv::mem::AllocA( sizeof(T)+(SPLY), A, nv::mem::AFC_GAME_OBJECT ))
template<typename T> inline
void			NvGameDelete(T* ptr)	{ nv::mem::Free( (pvoid)nv::DestructInPlace<T>(ptr) ); }



//
// Overloads memory operators
// The _NOVA_NO_NEWDELETE_ symbol can be defined to prevent the definition of memory operators new / delete

#ifdef _NGC
#define	NV_NEWDELETE_SIZET		unsigned long
#else
#define	NV_NEWDELETE_SIZET		unsigned int
#endif

#ifdef _NVENGINE
	void*			operator new		( NV_NEWDELETE_SIZET, void* ptr );
	#undef			new
	#define			new					$illegal new operator in engine part$
	#undef			delete
	#define			delete				$illegal delete operator in engine part$
#elif !defined(NVAPP_NEWDELETE)
	extern	void*	operator new		( NV_NEWDELETE_SIZET, void* ptr );
	extern	void*	operator new		( NV_NEWDELETE_SIZET bsize );
	extern	void*	operator new []		( NV_NEWDELETE_SIZET bsize );
	extern	void	operator delete		( void* ptr );
	extern	void	operator delete []	( void* ptr );
	#ifndef _NOVA_NO_NEWDELETE_
		#define	NVAPP_NEWDELETE																													\
			void*	operator new		( NV_NEWDELETE_SIZET, void* ptr )	{ return ptr; }														\
			void*	operator new		( NV_NEWDELETE_SIZET bsize )		{	return nv::mem::Alloc( bsize, nv::mem::AFC_GAME_OBJECT ); }		\
			void*	operator new []		( NV_NEWDELETE_SIZET bsize )		{	return nv::mem::Alloc( bsize, nv::mem::AFC_GAME_OBJECT ); }		\
			void	operator delete		( void* ptr )						{	nv::mem::Free( ptr ); }											\
			void	operator delete []	( void* ptr )						{	nv::mem::Free( ptr ); }
	#else 
		#define	NVAPP_NEWDELETE
	#endif
#endif 	



// inlined part
#include "NvCore_Mem_I.h"


#endif	// _NvCore_Mem_H_



