/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_Math_H_
#define	_NvCore_Math_H_




#ifndef AUTODOC_PARSING
namespace nv { namespace math
{
#endif


	extern const float	Pi;				// PI
	extern const float	HalfPi;			// PI/2
	extern const float	TwoPi;			// 2PI
	extern const float	OOPi;			// 1/PI
	extern const float	FpEpsilon;		// smallest such that 1.0+FpEpsilon != 1.0
	extern const float	FpMin;			// min positive value
	extern const float	FpMax;			// max value
	extern const float	DegToRad;		// Deg -> Rad
	extern const float	RadToDeg;		// Rad -> Deg



	//--------------------------
	//
	// Functions
	//
	//--------------------------

	float	Asin	(	float x								);
	float	Asinf	(	float x								);
	float	Acos	(	float x								);
	float	Acosf	(	float x								);
	float	Atan	(	float x								);
	float	Atanf	(	float x								);
	float	Atan2	(	float x, float y					);
	float	Atan2f	(	float x, float y					);
	float	Sin		(	float a								);
	float	Sinf	(	float a								);
	float	Cos		(	float a								);
	float	Cosf	(	float a								);
	void	Sincos	(	float a, float* outS, float* outC	);
	void	Sincosf	(	float a, float* outS, float* outC	);
	float	Tan		(	float a								);
	float	Tanf	(	float a								); 
	float	Exp		(	float x								);
	float	Expf	(	float x								);
	float	Log		(	float x								);
	float	Logf	(	float x								);
	float	Log2	(	float x								);
	float	Log2f	(	float x								);
	float	Log10	(	float x								);
	float	Log10f	(	float x								);
	float	Pow		(	float x, float y					);
	float	Powf	(	float x, float y					);
	float   Isqrt	(	float x								);
	float   Isqrtf	(	float x								);
	float   Sqrt	(	float x								);
	float   Sqrtf	(	float x								);
	int		Ftoi	(	float x								);
	float	Itof	(	int	  x								);
	float	Abs		(	float x								);
	float	Fabs	(	float x								);
	float	Fabsf	(	float x								);
	float	Absf	(	float x								);
	float	Fmod	(	float x, float y					);
	float	Ceil	(	float x								);
	float	Floor	(	float x								);
	float	Round	(	float x								);

	template <typename T>	T	Min		( const T & inA, const T & inB );
	template <typename T>	T	Max		( const T & inA, const T & inB );
	template <typename T>	T	Clamp	( const T & inV, const T & inMin, const T & inMax );



	struct	Vec2;
	struct	Vec3;
	struct	Vec4;
	struct	Quat;
	struct	Matrix;
	struct	Box3;
	struct	Sph3;



	//--------------------------
	//
	// 2D Vector ( x , y )
	//
	//--------------------------

	struct Vec2
	{
					Vec2	(										) {}
					Vec2	(	const float *						);
					Vec2	(	const Vec2 &						);
					Vec2	(	float inX, float inY				);
		explicit	Vec2	(	const Vec3 &						);		// Vec2(x,y)
		explicit	Vec2	(	const Vec4 &						);		// Vec2(x,y)
		explicit	Vec2	(	const Quat &						);		// Vec2(x,y)

		// assignment operators
		Vec2&		Set		(	float inX, float inY				);
		Vec2&	operator  =	(	const Vec2 &						);
		Vec2&	operator +=	(	const Vec2 &						);
		Vec2&	operator -=	(	const Vec2 &						);
		Vec2&	operator *=	(	float								);
		Vec2&	operator /=	(	float								);

		// unary operators
		Vec2	operator +	(										) const;
		Vec2	operator -	(										) const;

		// binary operators
		Vec2	operator +	(	const Vec2 &						) const;
		Vec2	operator -	(	const Vec2 &						) const;
		float	operator ^	(	const Vec2 &						) const;
		float	operator *	(	const Vec2 &						) const;
		Vec2	operator *	(	float								) const;
		Vec2	operator /	(	float								) const;

		friend Vec2 operator * ( float, const Vec2 &				);

		// test operators
		bool	operator == (	const Vec2 &						) const;
		bool	operator != (	const Vec2 &						) const;

		float	x, y;

		static	const Vec2	ZERO;			// {0,0}
		static	const Vec2	ONE;			// {1,1}
		static	const Vec2	UNIT_X;			// {1,0}
		static	const Vec2	UNIT_Y;			// {0,1}
	};

	Vec2*	Vec2Copy			(       Vec2* outV, const Vec2* inV									);
	// Vec2Zero					= Vec2(0,0)
	Vec2*	Vec2Zero			(       Vec2* outV													);
	// Vec2Norm					= ||V||
	float	Vec2Norm			( const Vec2* inV													);
	// Vec2Norm2				= ||V||^2
	float	Vec2Norm2			( const Vec2* inV													);
	// Vec2Normalize			= V/||V||
	Vec2*	Vec2Normalize		(       Vec2* outV, const Vec2* inV									);
	// Vec2Cross				= ( Vec3(V0,0) x Vec3(V1,0) ).z
	float	Vec2Cross			( const Vec2* inV0, const Vec2* inV1								);
	// Vec2Dot					= V0.V1
	float	Vec2Dot				( const Vec2* inV0, const Vec2* inV1								);
	// Vec2Add					= V0+V1
	Vec2*	Vec2Add				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				);
	// Vec2Sub					= V0-V1
	Vec2*	Vec2Sub				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				);
	// Vec2Mul					= Vec2( x0*x1, y0*y1 )
	Vec2*	Vec2Mul				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				);
	// Vec2Min					= Vec2( min(x0,x1), min(y0,y1) )
	Vec2*	Vec2Min				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				);
	// Vec2Max					= Vec2( max(x0,x1), max(y0,y1) )
	Vec2*	Vec2Max				(       Vec2* outV, const Vec2* inV0, const Vec2* inV1				);
	// Vec2Scale				= Vec2( x*f, y*f )
	Vec2*	Vec2Scale			(       Vec2* outV, const Vec2* inV,  float inF						);
	// Vec2Apply				= Vec2( Vec4(V,0,1) * M )
	Vec2*	Vec2Apply			(       Vec2* outV, const Vec2* inV,  const Matrix* inM				);
	// Vec2ApplyVector			= Vec3( Vec4(V,0,0) * M )			(rotation only)
	Vec2*	Vec2ApplyVector		(       Vec2* outV, const Vec2* inV,  const Matrix* inM				);
	// Vec2Lerp					= V0 + F*(V1-V0) : Linear interpolation from V0 to V1
	Vec2*	Vec2Lerp			(       Vec2* outV, const Vec2* inV0, const Vec2* inV1, float inF	);





	//--------------------------
	//
	// 3D Vector ( x , y , z )
	//
	//--------------------------

	struct Vec3
	{
					Vec3	(										) {}
					Vec3	(	const float *						);
					Vec3	(	const Vec3 &						);
					Vec3	(	float inX, float inY, float inZ		);
		explicit	Vec3	(	const Vec2 &						);		// Vec3(x,y,0)
		explicit	Vec3	(	const Vec4 &						);		// Vec3(x,y,z)
		explicit	Vec3	(	const Quat &						);		// Vec3(x,y,z)

		// assignment operators
		Vec3&		Set		(	float inX, float inY, float inZ		);
		Vec3&	operator  =	(	const Vec3 &						);
		Vec3&	operator +=	(	const Vec3 &						);
		Vec3&	operator -=	(	const Vec3 &						);
		Vec3&	operator *=	(	const Matrix &						);
		Vec3&	operator *=	(	const Quat &						);
		Vec3&	operator *=	(	float								);
		Vec3&	operator /=	(	float								);

		// unary operators
		Vec3	operator +	(										) const;
		Vec3	operator -	(										) const;

		// binary operators
		Vec3	operator +	(	const Vec3 &						) const;
		Vec3	operator -	(	const Vec3 &						) const;
		Vec3	operator ^	(	const Vec3 &						) const;
		float	operator *	(	const Vec3 &						) const;
		Vec3	operator *	(	const Matrix &						) const;
		Vec3	operator *	(	const Quat &						) const;
		Vec3	operator *	(	float								) const;
		Vec3	operator /	(	float								) const;

		friend Vec3 operator * ( float, const Vec3 &				);

		// test operators
		bool	operator == (	const Vec3 &						) const;
		bool	operator != (	const Vec3 &						) const;

		float	x, y, z;

		static	const Vec3	ZERO;			// {0,0,0}
		static	const Vec3	ONE;			// {1,1,1}
		static	const Vec3	UNIT_X;			// {1,0,0}
		static	const Vec3	UNIT_Y;			// {0,1,0}
		static	const Vec3	UNIT_Z;			// {0,0,1}
	};


	Vec3*	Vec3Copy			(       Vec3* outV, const Vec3* inV									);
	// Vec3Zero					= Vec3(0,0,0)
	Vec3*	Vec3Zero			(       Vec3* outV													);
	// Vec3Norm					= ||V||
	float	Vec3Norm			( const Vec3* inV													);
	// Vec3Norm2				= ||V||^2
	float	Vec3Norm2			( const Vec3* inV													);
	// Vec3Normalize			= V/||V||
	Vec3*	Vec3Normalize		(       Vec3* outV, const Vec3* inV									);
	// Vec3CrossProduct			= V0xV1
	Vec3*	Vec3Cross			(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				);
	// Vec3DotProduct			= V0.V1
	float	Vec3Dot				( const Vec3* inV0, const Vec3* inV1								);
	// Vec3Add					= V0+V1
	Vec3*	Vec3Add				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				);
	// Vec3Sub					= V0-V1
	Vec3*	Vec3Sub				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				);
	// Vec3Mul					= Vec3( x0*x1, y0*y1, z0*z1 )
	Vec3*	Vec3Mul				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				);
	// Vec3Min					= Vec3( min(x0,x1), min(y0,y1), min(z0,z1) )
	Vec3*	Vec3Min				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				);
	// Vec3Max					= Vec3( max(x0,x1), max(y0,y1), max(z0,z1) )
	Vec3*	Vec3Max				(       Vec3* outV, const Vec3* inV0, const Vec3* inV1				);
	// Vec3Scale				= Vec3( x*f, y*f, z*f )
	Vec3*	Vec3Scale			(       Vec3* outV, const Vec3* inV,  float inF						);
	// Vec3Lerp					= V0 + F*(V1-V0) : Linear interpolation from V0 to V1
	Vec3*	Vec3Lerp			(       Vec3* outV, const Vec3* inV0, const Vec3* inV1, float inF	);
	// Vec3Apply				= Vec3( Vec4(V,1) * M )
	Vec3*	Vec3Apply			(       Vec3* outV, const Vec3* inV,  const Matrix* inM				);
	// Vec3Apply				= Vec3( Vec4(V,1) * M ) * F
	Vec3*	Vec3Apply			(       Vec3* outV, const Vec3* inV,  const Matrix* inM, float inF	);
	// Vec3Apply				= Vec3( Q * Quat(0,V) * Q^-1 )	(expects unit quaternion)
	Vec3*	Vec3Apply			(       Vec3* outV, const Vec3* inV,  const Quat* inQ				);
	// Vec3ApplyVector			= Vec3( Vec4(V,0) * M )			(rotation only)
	Vec3*	Vec3ApplyVector		(       Vec3* outV, const Vec3* inV,  const Matrix* inM				);
	// Vec3ApplyVector			= Vec3( Vec4(V,0) * M ) * F		(rotation only)
	Vec3*	Vec3ApplyVector		(       Vec3* outV, const Vec3* inV,  const Matrix* inM, float inF	);
	// Vec3Blend			   += Vec3( Vec4(V,1) * M )
	Vec3*	Vec3Blend			(       Vec3* outV, const Vec3* inV,  const Matrix* inM				);
	// Vec3Blend			   += Vec3( Vec4(V,1) * M ) * F
	Vec3*	Vec3Blend			(       Vec3* outV, const Vec3* inV,  const Matrix* inM, float inF	);
	// Vec3BlendVector		   += Vec3( Vec4(V,0) * M )			(rotation only)
	Vec3*	Vec3BlendVector		(       Vec3* outV, const Vec3* inV,  const Matrix* inM				);
	// Vec3BlendVector		   += Vec3( Vec4(V,0) * M ) * F		(rotation only)
	Vec3*	Vec3BlendVector		(       Vec3* outV, const Vec3* inV,  const Matrix* inM, float inF	);
	void	Vec3GetBaseFromBX	(		Vec3* outBX, Vec3* outBY, Vec3* outBZ, const Vec3* inBX		);
	void	Vec3GetBaseFromBY	(		Vec3* outBX, Vec3* outBY, Vec3* outBZ, const Vec3* inBY		);
	void	Vec3GetBaseFromBZ	(		Vec3* outBX, Vec3* outBY, Vec3* outBZ, const Vec3* inBZ		);






	//--------------------------
	//
	// 4D Vector ( x , y , z , w )
	//
	//--------------------------

	struct Vec4
	{
					Vec4	(										) {}
					Vec4	(	const float *						);
					Vec4	(	const Vec4 &						);
					Vec4	(	float x, float y, float z, float w	);
		explicit	Vec4	(	const Vec3 &						);		// Vec4(x,y,z,1)
		explicit	Vec4	(	const Vec2 &						);		// Vec4(x,y,0,1)
		explicit	Vec4	(	const Quat &						);		// Vec4(x,y,z,w)

		// assignment operators
		Vec4&		Set		(	float x, float y, float z, float w	);
		Vec4&	operator  =	(	const Vec4 &						);
		Vec4&	operator +=	(	const Vec4 &						);
		Vec4&	operator -=	(	const Vec4 &						);
		Vec4&	operator *=	(	const Matrix &						);
		Vec4&	operator *=	(	const Quat &						);
		Vec4&	operator *=	(	float								);
		Vec4&	operator /=	(	float								);

		// unary operators
		Vec4	operator +	(										) const;
		Vec4	operator -	(										) const;

		// binary operators
		Vec4	operator +	(	const Vec4 &						) const;
		Vec4	operator -	(	const Vec4 &						) const;
		float	operator *	(	const Vec4 &						) const;
		Vec4	operator *	(	const Matrix &						) const;
		Vec4	operator *	(	const Quat &						) const;
		Vec4	operator *	(	float								) const;
		Vec4	operator /	(	float								) const;

		friend Vec4 operator * ( float, const Vec4 &				);

		// test operators
		bool	operator == (	const Vec4 &						) const;
		bool	operator != (	const Vec4 &						) const;
		uint		Clip	(										) const;	// {-z,+z,-y,+y,-x,+x}

		float	x, y, z, w;

		static	const Vec4	ZERO;		// {0,0,0,0}
		static	const Vec4	ONE;		// {1,1,1,1}
		static	const Vec4	UNIT_X;		// {1,0,0,0}
		static	const Vec4	UNIT_Y;		// {0,1,0,0}
		static	const Vec4	UNIT_Z;		// {0,0,1,0}
		static	const Vec4	UNIT_W;		// {0,0,0,1}
	};


	Vec4*	Vec4Copy			(       Vec4* outV, const Vec4* inV									);
	// Vec4Zero					= Vec3(0,0,0,0)
	Vec4*	Vec4Zero			(       Vec4* outV													);
	// Vec4Norm					= ||V||
	float	Vec4Norm			( const Vec4* inV													);
	// Vec4Norm2				= ||V||^2
	float	Vec4Norm2			( const Vec4* inV													);
	// Vec4Normalize			= V/||V||
	Vec4*	Vec4Normalize		(       Vec4* outV, const Vec4* inV									);
	// Vec4Cross				= x(V0,V1,V2)
	Vec4*	Vec4Cross			(       Vec4* outV, const Vec4* inV0, const Vec4* inV1, const Vec4* inV2	);
	// Vec4Dot					= V0.V1
	float	Vec4Dot				( const Vec4* inV0, const Vec4* inV1								);
	// Vec4Add					= V0+V1
	Vec4*	Vec4Add				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				);
	// Vec4Sub					= V0-V1
	Vec4*	Vec4Sub				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				);
	// Vec4Mul					= Vec4( x0*x1, y0*y1, z0*z1, w0*w1 )
	Vec4*	Vec4Mul				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				);
	// Vec4Min					= Vec4( min(x0,x1), min(y0,y1), min(z0,z1), min(w0,w1) )
	Vec4*	Vec4Min				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				);
	// Vec4Max					= Vec4( max(x0,x1), max(y0,y1), max(z0,z1), max(w0,w1) )
	Vec4*	Vec4Max				(       Vec4* outV, const Vec4* inV0, const Vec4* inV1				);
	// Vec4Scale				= Vec4( x*f, y*f, z*f, w*f )
	Vec4*	Vec4Scale			(       Vec4* outV, const Vec4* inV,  float inF						);
	// Vec4Lerp					= V0 + F*(V1-V0) : Linear interpolation from V0 to V1
	Vec4*	Vec4Lerp			(       Vec4* outV, const Vec4* inV0, const Vec4* inV1, float inF	);
	// Vec4Apply				= V * M
	Vec4*	Vec4Apply			(       Vec4* outV, const Vec4* inV,  const Matrix* inM				);
	// Vec4Apply				= (V * M) * F
	Vec4*	Vec4Apply			(       Vec4* outV, const Vec4* inV,  const Matrix* inM, float inF	);
	// Vec4Apply				= Q * V * Q^-1
	Vec4*	Vec4Apply			(       Vec4* outV, const Vec4* inV,  const Quat* inQ				);
	// Vec4ApplyVector			= (V{.w=0} * M)				(rotation only, outputed w=1)
	Vec4*	Vec4ApplyVector		(       Vec4* outV, const Vec4* inV,  const Matrix* inM				);
	// Vec4ApplyVector			= (V{.w=0} * M) * F			(rotation only, outputed w=1)
	Vec4*	Vec4ApplyVector		(       Vec4* outV, const Vec4* inV,  const Matrix* inM, float inF	);
	// Vec4Blend			   += (V * M)
	Vec4*	Vec4Blend			(       Vec4* outV, const Vec4* inV,  const Matrix* inM				);
	// Vec4Blend			   += (V * M) * F
	Vec4*	Vec4Blend			(       Vec4* outV, const Vec4* inV,  const Matrix* inM, float inF	);
	// Vec4BlendVector		   += (V{.w=0} * M)				(rotation only, outputed w=1)
	Vec4*	Vec4BlendVector		(       Vec4* outV, const Vec4* inV,  const Matrix* inM				);
	// Vec4BlendVector		   += (V{.w=0} * M) * F			(rotation only, outputed w=1)
	Vec4*	Vec4BlendVector		(       Vec4* outV, const Vec4* inV,  const Matrix* inM, float inF	);
	// Vec4Clip					: returns bitfield {-z,+z,-y,+y,-x,+x}
	//							  -x if set to 1 when .x < -|w|
	//							  +x if set to 1 when .x > +|w|
	uint	Vec4Clip			(		const Vec4* inV												);




	//--------------------------
	//
	// Quaternion ( s , V )
	//
	//	rotation around U of theta radians.
	//  ( s , V ) = ( cos(theta/2) ,    sin(theta/2)*U    )
	//			  = (     q.w      , Vec3(q.x,q.y,q.z)   )
	//
	// left <- right composition order
	// i.e.		P -> R0 -> R1 -> R2 -> P'
	//		=>	P' = (Q2*Q1*Q0) * (0,P) * (Q2*Q1*Q0)^-1
	//
	//--------------------------

	struct Quat
	{
					Quat	(													) {}
					Quat	(	const float *									);
					Quat	(	const Quat &									);
					Quat	(	float x, float y, float z, float w				);
					Quat	(	float theta, const Vec3 & inU					);	// angle, axis
		explicit	Quat	(	const Vec2 &									);	// Quat(x,y,0,0)
		explicit	Quat	(	const Vec3 &									);	// Quat(x,y,z,0)
		explicit	Quat	(	const Vec4 &									);	// Quat(x,y,z,w)
		explicit	Quat	(	const Matrix &									);  // Rotation only

		// assignment operators
		Quat&		Set		(	float x, float y, float z, float w				);
		Quat&	operator  =	(	const Quat &									);
		Quat&	operator *=	(	const Quat &									);
		Quat&	operator +=	(	const Quat &									);
		Quat&	operator -=	(	const Quat &									);
		Quat&	operator *=	(	float											);
		Quat&	operator /=	(	float											);

		// unary operators
		Quat	operator +	(													) const;
		Quat	operator -	(													) const;

		// binary operators
		Quat	operator *	(	const Quat &									) const;
		Quat	operator +	(	const Quat &									) const;
		Quat	operator -	(	const Quat &									) const;
		Quat	operator *	(	float											) const;
		Quat	operator /	(	float											) const;

		friend Quat operator * ( float, const Quat &							);

		// test operators
		bool	operator == (	const Quat &									) const;
		bool	operator != (	const Quat &									) const;

		float	x, y, z, w;

		static	const Quat	ZERO;		// {0,0,0,0}
		static	const Quat	ONE;		// {1,1,1,1}
		static	const Quat	UNIT;		// {0,0,0,1}
	};


	Quat*	QuatCopy			(       Quat* outQ, const Quat* inQ									);
	// QuatZero					= Quat(0,0,0,0)
	Quat*	QuatZero			(       Quat* outQ													);
	// QuatIdentity				= Id (1,0) = Quat(0,0,0,1)
	Quat*	QuatIdentity		(       Quat* outQ													);
	// QuatIsIdentity			= Q==Id ?
	bool	QuatIsIdentity		( const Quat* inQ													);
	// QuatNorm					= ||Q||
	float	QuatNorm			( const Quat* inQ													);
	// QuatNorm2				= ||Q||^2
	float	QuatNorm2			( const Quat* inQ													);
	// QuatNormalize			= Q/||Q||
	Quat*	QuatNormalize		(       Quat* outQ, const Quat* inQ									);
	// QuatDot					= Q0.Q1
	float	QuatDot				( const Quat* inQ0, const Quat* inQ1								);
	// QuatConjugate			= Q' = Quat(-x,-y,-z,+w)	(i.e. -theta rotation <=> ( cos(-theta/2), sin(-theta/2)*U ) = ( cos(theta/2), -sin(theta/2)*U ) = ( s , -V ) )
	Quat*	QuatConjugate		(       Quat* outQ, const Quat* inQ									);
	// QuatFastInverse			= Q'	(expects unit quaternion)
	Quat*	QuatFastInverse		(       Quat* outQ, const Quat* inQ									);
	// QuatInverse				= Q'/||Q||
	Quat*	QuatInverse			(       Quat* outQ, const Quat* inQ									);
	// QuatToAxisAngle			: get axis U and angle theta
	void	QuatToAxisAngle		(       Vec3* outV, float * outAngle, const Quat* inQ				);
	// QuatFromAxisAngle		: set axis U and angle theta
	Quat*	QuatFromAxisAngle	(       Quat* outQ, const Vec3* inV, float inAngle					);
	// QuatFromMatrix			: Q <- MatrixRotation(M)
	Quat*	QuatFromMatrix		(       Quat* outQ, const Matrix* inM								);
	// QuatEuler				: Yaw around the Y axis, pitch/X, roll/Z
	Quat*	QuatEuler			(		Quat* outQ, float inEx, float inEy, float inEz				);
	// QuatMul					= Q0*Q1
	Quat*	QuatMul				(		Quat* outQ, const Quat* inQ0, const Quat* inQ1				);
	// QuatLn					: ln(q)=(0,theta/2*U)	(expects unit quaternion)
	Quat*	QuatLn				(		Quat* outQ, const Quat* inQ									);
	// QuatExp					: exp(ln(q))=q	(expects pure quaternion (w=0). w is ignored in calculation)
	Quat*	QuatExp				(		Quat* outQ, const Quat* inQ									);
	// QuatSlerp				: spherical linear interpolation from Q0(inT=0) to Q1(inT=1)	(expects unit quaternions)
	Quat*	QuatSlerp			(		Quat* outQ, const Quat* inQ0, const Quat* inQ1, float inT	);
	// QuatPow					: scale angle = ( cos(a*s), sin(a*s)*U )
	Quat*	QuatPow				(		Quat* outQ, const Quat* inQ, float inScale					);
	#define	NV_ASSERT_UNIT_QUAT(inQP)	NV_ASSERT( Fabsf(QuatNorm2((inQP))-1.0f) < 0.001f )







	//--------------------------
	//
	// 4D Matrix
	//
	// Matrix use american form (transposed of european) !
	//
	//	[ RX.x RX.y RX.z 0 ]
	//	[ RY.x RY.y RY.z 0 ]
	//	[ RZ.x RZ.y RZ.z 0 ]
	//	[ TX   TY   TZ   1 ]
	//
	// left -> right composition order
	//
	// i.e.		P -> M0 -> M1 -> M2 -> P'
	//		=>	P' = P * ( M0 * M1 * M2 )
	//
	//--------------------------

	struct Matrix
	{
					Matrix	(													) {}
					Matrix	(	const float *									);
					Matrix	(	const Matrix &									);
					Matrix	(	float in11, float in12, float in13, float in14,
								float in21, float in22, float in23, float in24,
								float in31, float in32, float in33, float in34,
								float in41, float in42, float in43, float in44	);
		explicit	Matrix	(	const Quat &									);	// pure rotation matrix
		explicit	Matrix	(	const Vec3 &									);	// pure translation matrix

		// access grants
		float&	operator ()	(	int Row, int Col								);
		float	operator () (	int Row, int Col								) const;

		// assignment operators
		Matrix&	operator  =	(	const Matrix &									);
		Matrix&	operator *=	(	const Matrix &									);
		Matrix&	operator +=	(	const Matrix &									);
		Matrix&	operator -=	(	const Matrix &									);
		Matrix&	operator *=	(	float											);
		Matrix&	operator /=	(	float											);

		// unary operators
		Matrix	operator +	(													) const;
		Matrix	operator -	(													) const;

		// binary operators
		Matrix	operator *	(	const Matrix &									) const;
		Matrix	operator +	(	const Matrix &									) const;
		Matrix	operator -	(	const Matrix &									) const;
		Matrix	operator *	(	float											) const;
		Matrix	operator /	(	float											) const;

		friend Matrix operator * ( float, const Matrix &						);

		// test operators
		bool	operator == (	const Matrix &									) const;
		bool	operator != (	const Matrix &									) const;

		// axis
		Vec3*	right		(													) const;
		Vec3*	up			(													) const;
		Vec3*	front		(													) const;
		Vec3*	location	(													) const;

		union {
			struct {
				float		m11, m12, m13, m14;
				float		m21, m22, m23, m24;
				float		m31, m32, m33, m34;
				float 		m41, m42, m43, m44;
			};
			float m[4][4];	// [row][col]
		};

		static	const Matrix	ZERO;
		static	const Matrix	UNIT;
	};

	Matrix*	MatrixCopy			(       Matrix* outM, const Matrix* inM											);
	// MatrixZero				= 0
	Matrix*	MatrixZero			(       Matrix* outM															);
	// MatrixIdentity			= Id
	Matrix*	MatrixIdentity		(       Matrix* outM															);
	// MatrixIsIdentity			= M==Id ?
	bool	MatrixIsIdentity	( const Matrix*	inM																);
	// MatrixDeterminant		= 4x4 determinant
	float	MatrixDeterminant	( const Matrix* inM																);
	// MatrixAdd				= M0+M1
	Matrix*	MatrixAdd			(       Matrix* outM, const Matrix* inM0, const Matrix* inM1					);
	// MatrixSub				= M0-M1
	Matrix*	MatrixSub			(       Matrix* outM, const Matrix* inM0, const Matrix* inM1					);
	// MatrixMul				= M0*M1
	Matrix*	MatrixMul			(       Matrix* outM, const Matrix* inM0, const Matrix* inM1					);
	// MatrixScalar				= M*f
	Matrix*	MatrixScalar		(       Matrix* outM, const Matrix* inM, float inF								);
	// MatrixTranspose			= M^t
	Matrix*	MatrixTranspose		(       Matrix* outM, const Matrix* inM											);
	// MatrixInverse			= M^-1
	Matrix*	MatrixInverse		(		Matrix* outM, float * outDeterminant, const Matrix* inM					);
	// MatrixFastInverse		= M^-1	(expects M is a regular affine matrix with uniform scale)
	Matrix*	MatrixFastInverse	(		Matrix* outM, const Matrix* inM											);
	// MatrixNormalize			: Matrix Normalisation (normalizes the rotation axis)
	Matrix*	MatrixNormalize		(		Matrix* outM, const Matrix* inM											);
	// MatrixOrthoNormalize		: Matrix Orthonormalisation (orthogonizes & normalizes the rotation axis)
	Matrix*	MatrixOrthoNormalize(		Matrix* outM, const Matrix* inM											);
	// MatrixTranslate			= M * MatrixTranslation(T)
	Matrix*	MatrixTranslate		(		Matrix* outM, const Matrix* inM, float inTX, float inTY, float inTZ		);
	Matrix*	MatrixTranslate		(		Matrix* outM, const Matrix* inM, const Vec3* inV						);
	Matrix*	MatrixTranslate		(		Matrix* outM, const Matrix* inM, const Matrix* inT						);
	// MatrixPreTranslate		= MatrixTranslation(T) * M
	Matrix*	MatrixPreTranslate	(		Matrix* outM, const Matrix* inM, float inTX, float inTY, float inTZ		);
	Matrix*	MatrixPreTranslate	(		Matrix* outM, const Matrix* inM, const Vec3* inV						);
	Matrix*	MatrixPreTranslate	(		Matrix* outM, const Matrix* inM, const Matrix* inT						);
	// MatrixRotate				= M * MatrixRotation(R)
	Matrix*	MatrixRotate		(		Matrix* outM, const Matrix* inM, const Quat* inQ						);
	Matrix*	MatrixRotate		(		Matrix* outM, const Matrix* inM, const Vec3* inAxis, float inAngle		);
	Matrix*	MatrixRotate		(		Matrix* outM, const Matrix* inM, const Matrix* inR						);
	// MatrixPreRotate			= MatrixRotation(R) * M
	Matrix*	MatrixPreRotate		(		Matrix* outM, const Matrix* inM, const Quat* inQ						);
	Matrix*	MatrixPreRotate		(		Matrix* outM, const Matrix* inM, const Vec3* inAxis, float inAngle		);
	Matrix*	MatrixPreRotate		(		Matrix* outM, const Matrix* inM, const Matrix* inR						);
	// MatrixRotate[X|Y|Z]		= M * MatrixRotation[X|Y|Z]()
	Matrix*	MatrixRotateX		(		Matrix* outM, const Matrix* inM, float inAngle							);
	Matrix*	MatrixRotateY		(		Matrix* outM, const Matrix* inM, float inAngle							);
	Matrix*	MatrixRotateZ		(		Matrix* outM, const Matrix* inM, float inAngle							);
	// MatrixPreRotate[X|Y|Z]	= MatrixRotation[X|Y|Z]() * M
	Matrix*	MatrixPreRotateX	(		Matrix* outM, const Matrix* inM, float inAngle							);
	Matrix*	MatrixPreRotateY	(		Matrix* outM, const Matrix* inM, float inAngle							);
	Matrix*	MatrixPreRotateZ	(		Matrix* outM, const Matrix* inM, float inAngle							);
	// MatrixScale				= M * MatrixScaling(S)
	Matrix*	MatrixScale			(		Matrix* outM, const Matrix* inM, float inSX, float inSY, float inSZ		);
	Matrix*	MatrixScale			(		Matrix* outM, const Matrix* inM, const Vec3* inV						);
	Matrix*	MatrixScale			(		Matrix* outM, const Matrix* inM, const Matrix* inS						);
	// MatrixPreScale			= MatrixScaling(S) * M
	Matrix*	MatrixPreScale		(		Matrix* outM, const Matrix* inM, float inSX, float inSY, float inSZ		);
	Matrix*	MatrixPreScale		(		Matrix* outM, const Matrix* inM, const Vec3* inV						);
	Matrix*	MatrixPreScale		(		Matrix* outM, const Matrix* inM, const Matrix* inS						);
	// MatrixTranslation		: Builds a pure translation matrix
	Matrix*	MatrixTranslation	(		Matrix* outM, float inTX, float inTY, float inTZ						);
	Matrix*	MatrixTranslation	(		Matrix* outM, const Vec3* inV											);
	Matrix*	MatrixTranslation	(		Matrix* outM, const Matrix* inM	 										);
	// MatrixRotation[X|Y|Z]	: Builds a pure rotation matrix around [X|Y|Z] of angle radians
	Matrix*	MatrixRotationX		(		Matrix* outM, float inAngle												);
	Matrix*	MatrixRotationY		(		Matrix* outM, float inAngle												);
	Matrix*	MatrixRotationZ		(		Matrix* outM, float inAngle												);
	// MatrixRotation			: Builds a pure rotation matrix
	Matrix*	MatrixRotation		(		Matrix* outM, const Vec3* inAxis, float inAngle							);
	Matrix*	MatrixRotation		(		Matrix* outM, const Quat* inQ											);
	Matrix*	MatrixRotation		(		Matrix* outM, const Matrix* inM	 										);
	// MatrixScaling			: Builds a pure scaling matrix
	Matrix*	MatrixScaling		(		Matrix* outM, float inSX, float inSY, float inSZ						);
	Matrix*	MatrixScaling		(		Matrix* outM, const Vec3* inV											);
	Matrix*	MatrixScaling		(		Matrix* outM, const Matrix* inM	 										);
	// MatrixBuildTR			: Builds a matrix from identity
	Matrix*	MatrixBuildTR		(		Matrix*	outM, const Vec3* inT, const Quat* inR, const Vec3* inS			);
	// MatrixSetTR				: Set parts of the matrix
	Matrix*	MatrixSetTR			(		Matrix*	outM, const Vec3* inT, const Quat* inR, const Vec3* inS, const Matrix* inM	);
	// MatrixGetTR				: Get rotation and/or translation parts of the matrix
	void	MatrixGetTR			(		Vec3*	outT, Quat* outR, Vec3*	outS, const Matrix*	inM					);
	// MatrixGetAxis			: Get rotation axis
	void	MatrixGetAxis		(		Vec3*	outRight, Vec3* outUp, Vec3* outFront, const Matrix* inM		);
	// MatrixSetAxis			: Set rotation axis
	Matrix*	MatrixSetAxis		(		Matrix*	outM, const Vec3* inRight, const Vec3* inUp, const Vec3* inFront);
	// MatrixOrthoRH			: Builds a right-handed orthogonal projection matrix.
	Matrix*	MatrixOrthoRH		(		Matrix*	outM, float inW, float inH, float inZNear, float inZFar			);
	// MatrixOrthoLH			: Builds a left-handed orthogonal projection matrix.
	Matrix*	MatrixOrthoLH		(		Matrix*	outM, float inW, float inH, float inZNear, float inZFar			);
	// MatrixPerspectiveRH		: Builds a right-handed perspective projection matrix based on a field of view.
	Matrix*	MatrixPerspectiveRH	(		Matrix*	outM, float inFovx, float inAspect, float inZNear, float inZFar	);
	// MatrixPerspectiveLH		: Builds a left-handed perspective projection matrix based on a field of view.
	Matrix*	MatrixPerspectiveLH	(		Matrix*	outM, float inFovx, float inAspect, float inZNear, float inZFar	);
	// MatrixIsProjRH			: Checks if the projection matrix is right-handled ?
	bool	MatrixIsProjRH		(		Matrix*	inM																);
	// MatrixIsProjLH			: Checks if the projection matrix is left-handled ?
	bool	MatrixIsProjLH		(		Matrix*	inM																);




	//--------------------------
	//
	// T-R[-S] Array -> Matrix Array
	//
	// inUsePIdx = FALSE :
	//	M[i] = convert_to_matrix(TR[i])
	//
	// inUsePIdx = TRUE  :
	//	use the parent index.
	//	- (i==0) : M[0] = convert_to_matrix(TR[0])
	//	- (i >0) : M[i] = convert_to_matrix(TR[i]) * M[ TR[i].parentIdx ]
	//			   note 1 : asserting (TR[i].parentIdx <= i) !!!
	//			   note 2 : no-parent => parentIdx = -1
	//
	//--------------------------

	struct TR {		// 2QW
		Vec3	t;			// location
		int32	parentIdx;	// [optionnal parent index, none = -1]
		Quat	r;			// rotation
	};

	struct TRS {	// 3QW
		Vec3	t;			// location
		int32	parentIdx;	// [optionnal parent index, none = -1]
		Vec3	s;			// scale
		float	pad0;
		Quat	r;			// rotation
	};

	void	MatrixArraySetTR	(		Matrix*	outMArray, const TR*   inTRArray,   uint inCount );
	void	MatrixArraySetTR	(		Matrix*	outMArray, const TRS*  inTRSArray,  uint inCount );		// scale is ignored !
	void	MatrixArraySetTRS	(		Matrix*	outMArray, const TRS*  inTRSArray,  uint inCount );		// scale is applied !
	void	MatrixArrayMul		(		Matrix*	outMArray, Matrix* inM0Array, Matrix* inM1Array, uint inCount );




	//--------------------------
	//
	// 3D Box ( center , rotation , length )
	//
	//--------------------------

	struct Box3
	{
					Box3	(													) {}
					Box3	(	const Box3 &									);
		explicit	Box3	(	const Sph3 &									);

		// concat operators
		Box3&	operator  =	(	const Box3 &									);
		Box3&	operator +=	(	const Box3 &									);
		Box3	operator +	(	const Box3 &									) const;

		// TR
		Box3&	operator *=	(	const Matrix &									);
		Box3	operator *	(	const Matrix &									) const;

		// test operators
		bool	operator == (	const Box3 &									) const;
		bool	operator != (	const Box3 &									) const;

		Vec3	center;
		Quat	rotation;
		Vec3	length;		//	< X-width>=0, Y-height>=0, Z-depth>=0 > in the local box space
	};

	Box3*	Box3Copy			(       Box3* outB, const Box3* inB									);
	// Box3Concat				: Bounding box of b0 & b1
	Box3*	Box3Concat			(       Box3* outB, const Box3* inB0, const Box3* inB1				);
	// Box3AxysAlign			: Computes a no-rotation-bounding-box of b
	Box3*	Box3AxysAlign		(       Box3* outB, const Box3* inB									);
	// Box3Apply				: Apply a TR on the box
	Box3*	Box3Apply			(       Box3* outB, const Box3* inB,  const Matrix* inM				);





	//--------------------------
	//
	// 3D Sphere ( center , radius )
	//
	//--------------------------

	struct Sph3
	{
					Sph3	(													) {}
					Sph3	(	const Sph3 &									);
		explicit	Sph3	(	const Box3 &									);

		// concat operators
		Sph3&	operator  =	(	const Sph3 &									);
		Sph3&	operator +=	(	const Sph3 &									);
		Sph3	operator +	(	const Sph3 &									) const;

		// TR
		Sph3&	operator *=	(	const Matrix &									);
		Sph3	operator *	(	const Matrix &									) const;

		// test operators
		bool	operator == (	const Sph3 &									) const;
		bool	operator != (	const Sph3 &									) const;

		Vec3	center;
		float	radius;		// >= 0
	};

	Sph3*	Sph3Copy			(       Sph3* outS, const Sph3* inB									);
	// Sph3Concat				: Bounding sphere of s0 & s1
	Sph3*	Sph3Concat			(       Sph3* outS, const Sph3* inS0, const Sph3* inS1				);
	// Sph3Apply				: Apply a TR on the sphere
	Sph3*	Sph3Apply			(       Sph3* outS, const Sph3* inS,  const Matrix* inM				);




	//--------------------------
	//
	// 3D Frustum (6 planes, inside oriented)
	//
	//--------------------------

	struct Frustum3
	{
					Frustum3	(													) {}
					Frustum3	(	const Frustum3 &								);
		explicit	Frustum3	(	const Box3 &									);
		explicit	Frustum3	(	const Sph3 &									);
		explicit	Frustum3	(	const Matrix &									);

		// some tests
		bool		IsInside	(	const Box3 &									) const;
		bool		IsInside	(	const Sph3 &									) const;
		bool		IsInside	(	const Vec3 &									) const;
		bool		IsInside	(	const Vec4 &									) const;		// w is ignored

		Vec4		plane[6];	// left/right/bottom/top/near/far
	};

	Frustum3*	Frustum3Copy	(       Frustum3* outF, const Frustum3* inF							);
	// Frustum3Build			: Build from a box
	Frustum3*	Frustum3Build	(       Frustum3* outF, const Box3* inB								);
	// Frustum3Build			: Build from a sphere
	Frustum3*	Frustum3Build	(       Frustum3* outF, const Sph3* inS								);
	// Frustum3Build			: Build from a projection matrix
	Frustum3*	Frustum3Build	(       Frustum3* outF, const Matrix* inM							);
	// Frustum3Apply			: Apply a TR on the frustum
	Frustum3*	Frustum3Apply	(       Frustum3* outF, const Frustum3* inF,  const Matrix* inM		);
	// FrustumClip				: returns bitfield {-z,+z,-y,+y,-x,+x}
	//							  -x if set to 1 when .x < -|w|
	//							  +x if set to 1 when .x > +|w|
	uint		Frustum3Clip	(		const Frustum3* inF, const Vec3* inV, float inRadius = 0.f	);
	uint		Frustum3Clip	(		const Frustum3* inF, const Vec4* inV, float inRadius = 0.f	);
	// FrustumInside			: returns TRUE is inside
	bool		Frustum3Inside	(		const Frustum3* inF, const Vec3* inV, float inRadius = 0.f	);
	bool		Frustum3Inside	(		const Frustum3* inF, const Vec4* inV, float inRadius = 0.f	);




	//--------------------------
	// Curves
	//
	// ComputeHermiteBasis		: Compute Hermite coefficients for u in [0,1]
	// ComputeBezierBasis		: Compute Bezier  coefficients for u in [0,1]
	// ComputeSplineInterp		: Spline interpolation from basics coefficients
	//							  Hermite : <K0,K1,K2,K3> = <P0,P1,T0,T1>
	//							  Bezier  : <K0,K1,K2,K3> = <P0,P1,P2,P3>
	//--------------------------

	void	ComputeHermiteBasis			(	float *		outB/*[4]*/, float inU						);

	void	ComputeBezierBasis			(	float *		outB/*[4]*/, float inU						);

	void	ComputeSplineInterp			(	float &		outP,
											float *		inB/*[4]*/,
											float 		K0, float 	K1, float	K2,	float	K3		);

	void	ComputeSplineInterp			(	Vec2 &		outP,
											float *		inB/*[4]*/,
											Vec2 &		K0, Vec2 &	K1, Vec2 &	K2,	Vec2 &	K3		);

	void	ComputeSplineInterp			(	Vec3 &		outP,
											float *		inB/*[4]*/,
											Vec3 &		K0, Vec3 &	K1, Vec3 &	K2,	Vec3 &	K3		);

	float	ComputeHermiteSmoothStep	(	float x0, float x1, float v								);

	float	GaussianUnit				(	float		inDeviation,
											float		inDistance									);
	float	Gaussian					(	float		inDeviation,
											float		inDistance									);
	float 	GaussianDeviation			(	float		inRadius									);
	int		GaussianWidth				(	float		inDeviation									);
	int		GaussianNormSequence		(	float*		outG/*[ GaussianWidth() ]*/,					// Output of (GaussianWidth()) samples
											float		inDeviation,
											bool		inNormalize									);	// Normalization ?

#ifndef AUTODOC_PARSING
} }
#endif



#ifndef _NvCore_Math_NOT_GLOBAL_
using namespace nv::math;
#endif // _NvCore_Math_NOT_GLOBAL_



#include "NvCore_Math_I.h"


#endif	// _NvCore_Math_H_




