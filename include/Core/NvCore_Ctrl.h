/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_Ctrl_H_
#define	_NvCore_Ctrl_H_



namespace nv { namespace ctrl
{
	enum Type
	{
		TP_PADDLE	= 0,			// paddle
		TP_MOUSE,					// mouse
		TP_KEYBOARD,				// keyboard
		TP_STERRINGWHEEL,			// sterring wheel
		TP_DVDREMOTE,				// DVD remote
		TP_GUN,						// gun
		TP_TOMTOM,					// (like NGC Tarukonga)
		TP_COMBI,					// (like wiiremote)
		TP_MAX
	};

	enum Button
	{								// Sony Playstation2&3		Sony PSP				XBox360					Nintendo GameCube
									// -------------------		--------				-------					-----------------
		BT_DIR1X	= 0,			// Analog stick0 DirX		Analog stick DirX		Analog stick0 DirX		Analog stick DirX
		BT_DIR1Y,					// Analog stick0 DirY		Analog stick DirY		Analog stick0 DirY		Analog stick DirY
		BT_DIR2X,					// Analog stick1 DirX		Analog stick DirX		Analog stick1 DirX		Analog substick C DirX
		BT_DIR2Y,					// Analog stick1 DirY		Analog stick DirY		Analog stick1 DirY		Analog substick C DirY
		BT_DIR3right,				// Dir3 right				Digital right			Digital right			Digital right
		BT_DIR3left,				// Dir3 left				Digital left			Digital left			Digital left
		BT_DIR3up,					// Dir3 up					Digital up				Digital up				Digital up
		BT_DIR3down,				// Dir3 down				Digital down			Digital down			Digital down
		BT_BUTTON0,					// TRIANGLE					TRIANGLE				Y						Y
		BT_BUTTON1,					// CIRCLE					CIRCLE					B						X
		BT_BUTTON2,					// CROSS					CROSS					A						A
		BT_BUTTON3,					// SQUARE					SQUARE					X						B
		BT_BUTTON4,					// L2						L						LT						L
		BT_BUTTON5,					// R2						R						RT						R
		BT_BUTTON6,					// L1						CROSS+L					LB						A+L
		BT_BUTTON7,					// R1						CROSS+R					RB						A+R
		BT_BUTTON8,					// Select					Select					Back					Z
		BT_BUTTON9,					// Start					Start					Start					Start/Pause
		BT_BUTTON10,				// L3												Analog stick0 down		L trigger
		BT_BUTTON11,				// R3												Analog stick1 down		R trigger
		BT_BUTTON12,				//
		BT_BUTTON13,				//
		BT_BUTTON14,				//							INTERCEPTED
		BT_BUTTON15,				//							POWER/HOLD
		BT_MAX
	};

	struct Status
	{
		Type			type;
		float			button[BT_MAX];
		float			Filtered		(		Button			inButton,
												float			inDeadZoneLo = -1.f,		// in [0,1], <0 for default value
												float			inDeadZoneHi = -1.f	);		// in [0,1], <0 for default value
	};


	void				Open			(											);
	void				Close			(											);

	uint				GetNbMax		(											);
	Status*				GetStatus		(		uint			inCtrlNo			);

	bool				SetActuators	(		uint			inCtrlNo,
												float			inLowSpeed,					// in [0,1]
												float			inHighSpeed			);		// in [0,1]

	bool				GetDefDeadZone	(		Type			inType,
												Button			inButton,
												float*			outLo		= NULL,
												float*			outHi		= NULL	);
	bool				SetDefDeadZone	(		Type			inType,
												Button			inButton,
												float			inLo,
												float			inHi				);
} }


#endif	// _NvCore_Ctrl_H_


