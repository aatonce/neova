/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCore_File_H_
#define	_NvCore_File_H_




namespace nv { namespace file
{

	//
	// Read Request

	struct ReadRequest
	{
		enum State
		{
			IDLE		= 0,			// Not enqueued.
			PENDING		= 1,			// Enqueued ...
			COMPLETED	= 2,			// Data are available.
			ABORTED		= -1,			// The request has been aborted !
			FAILED		= -2			// The request failed !
		};

								ReadRequest	(									)	{ stt = IDLE;				}

		bool					Setup		(	pvoid		inBufferPtr,		// must be 16-bytes aligned !
												uint32		inBSize,			// must be 16-bytes aligned !
												uint32		inBOffset0,			// seek offset in bytes
												uint32		inBOffset1 = ~0U,	// (up to four locations for the same data)
												uint32		inBOffset2 = ~0U,
												uint32		inBOffset3 = ~0U	);

		bool					IsIdle		(									)	{ return (stt==IDLE);		}
		bool					IsPending	(									)	{ return (stt==PENDING);	}
		bool					IsReady		(									)	{ return (stt!=PENDING);	}
		bool					IsCompleted	(									)	{ return (stt==COMPLETED);	}
		bool					IsState		(	State		inState				)	{ return (stt==inState);	}
		State					GetState	(									)	{ return stt;				}

		// reserved ...
		uint32					bSize;
		uint32					bOffset[4];
		pvoid					bufferPtr;
		uint					sysFlags;
		volatile State			stt;
		ReadRequest* volatile	next;
	};



	enum Status
	{
		FS_OK				= 0,
		FS_BUSY				= 1,
		FS_WRONG_DISK		= 2,
		FS_NO_DISK			= 3,
		FS_NO_DRIVE			= 4,
		FS_FATAL			= 5,
		FS_RETRY			= 6,
		FS_NOT_AVAILABLE	= 7,
		FS_TIMEOUT			= 8,
	};



	//
	// Access

	Status 						GetStatus			(													);
	Status 						CheckStatus			(													);

	bool						Open				(	pcstr			inFilename						);
	uint32						GetSize				(													);
	
	bool						AddReadRequest		(	ReadRequest*	inRequest,
														uint8			inPriorityLevel	= 8				);	// in [0(lowest), 15(highest)]
	bool						AbortReadRequest	(	ReadRequest*	inRequest						);
	uint						GetRequestQueueSize	(													);
	uint						GetRequestCompltdCpt(													);

	uint16						GetPriorityMask		(													);
	void						SetPriorityMask		(	uint16			inMask			= 0				);

	bool						Close				(													);


	// System-decorated fullname from generic filename
	// Available before core initialization.
	pcstr						GetFullname			(	pcstr			inFilename						);


	// Direct file access (not supported in MASTER & FINAL !)
	bool						DumpFromFile		(	pcstr			inFilename,
														pvoid&			outBuffer,
														uint&			outBSize						);
	bool						DumpToFile			(	pcstr			inFilename,
														pvoid			inBuffer,
														uint			inBSize							);
	bool						AppendToFile		(	pcstr			inFilename,
														pvoid			inBuffer,
														uint			inBSize							);

} }


#endif	// _NvCore_File_H_



