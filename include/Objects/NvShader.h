/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvShader_H_
#define	_NvShader_H_


#include <NvDpyManager.h>
interface NvResource;


interface NvShader : public NvDpyObject
{
	NVITF_DECL( Shader )

	enum EnableFlags
	{
		EN_WR_COLOR			= (1<<0),			// Write in target color-buffer ?
		EN_WR_DEPTH			= (1<<1),			// Write in target depth-buffer ?
		EN_RD_DEPTH			= (1<<2),			// Read from target depth-buffer ?
		EN_RD_STENCIL		= (1<<3),			// Read from target stencil-buffer ?
		EN_TEXTURING		= (1<<4),			// Texturing ?
		EN_SORTING			= (1<<5),			// Enable sorting ?
		EN_CULLING			= (1<<6),			// Enable face culling ?
		EN_CLIPPING			= (1<<7),			// Enable face clipping ?
		EN_THROUGHMODE		= (1<<8),			// 2D drawing ?
		EN_DEFAULT			= ~(EN_RD_STENCIL|EN_THROUGHMODE|EN_CLIPPING)
	};


	virtual	NvInterface*	GetBase				(									) = 0;
	virtual void			AddRef				(									) = 0;
	virtual	uint			GetRefCpt			(									) = 0;
	virtual	void			Release				(									) = 0;
	virtual	NvResource*		GetResource			(									) { return NULL;  }

	// Enable flags

	virtual void			Enable				(	uint32			inEnableFlags	) = 0;		// mask of EnableFlags
	virtual void			Disable				(	uint32			inEnableFlags	) = 0;		// mask of EnableFlags
	virtual void			SetEnabled			(	uint32			inEnableFlags	) = 0;		// mask of EnableFlags
	virtual uint32			GetEnabled			(									) = 0;		// mask of EnableFlags

	// Geometry surface properties

	virtual bool			HideSurface			(	uint			inSurfaceIndex	) { return FALSE; }
	virtual bool			ShowSurface			(	uint			inSurfaceIndex	) { return FALSE; }
	virtual bool			HideAllSurfaces		(									) { return FALSE; }
	virtual bool			ShowAllSurfaces		(									) { return FALSE; }
	virtual DpyState*		GetDisplayState		(	uint			inSurfaceIndex	) { return NULL;  }
	virtual	bool			SetAmbient			(	uint			inSurfaceIndex,
													Vec4*			inColor			) { return FALSE; }
	virtual	bool			SetDiffuse			(	uint			inSurfaceIndex,
													Vec4*			inColor			) { return FALSE; }

	// NvSurface display range

	virtual	uint			GetDrawStride		(									) { return 0;	  }
	virtual	bool			SetDrawStart		(	uint			inOffset		) { return FALSE; }
	virtual	bool			SetDrawSize			(	uint			inSize			) { return FALSE; }
};



#endif // _NvShader_H_



