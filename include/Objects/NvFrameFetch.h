/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you. 
**
*****************************************************************LIC-HDR*/


#ifndef	_NvFrameFetch_H_
#define	_NvFrameFetch_H_


#include <NvShader.h>


interface NvFrameFetch : public NvShader
{
	NVITF_DECL( FrameFetch )


	static	NvFrameFetch*	Create			(	uint			inWitdh					,
												uint			inHeight				);

	NvInterface*			GetBase			(											);
	void					AddRef			(											);
	uint					GetRefCpt		(											);
	void					Release			(											);

	// Enable flags
	void					Enable			(	uint32			inEnableFlags			){}
	void					Disable			(	uint32			inEnableFlags			){}
	void					SetEnabled		(	uint32			inEnableFlags			){}
	uint32					GetEnabled		(											){return 0;}

	NvBitmap  *				GetFrameData	(											);	
};



#endif // _NvFrameFetch_H_



