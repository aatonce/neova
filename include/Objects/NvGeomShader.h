/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvGeomShader_H_
#define	_NvGeomShader_H_


#include <NvShader.h>
interface NvResource;


interface NvGeomShader : public NvShader
{
	NVITF_DECL( GeomShader )

	static	NvGeomShader*	Create			(	NvResource*		inResource				);

	NvInterface*			GetBase			(											);
	void					AddRef			(											);
	uint					GetRefCpt		(											);
	void					Release			(											);
	NvResource*				GetResource		(											);

	// Enable flags

	void					Enable			(	uint32			inEnableFlags			);
	void					Disable			(	uint32			inEnableFlags			);
	void					SetEnabled		(	uint32			inEnableFlags			);
	uint32					GetEnabled		(											);

	// Basic properties

	bool					HideSurface		(	uint			inSurfaceIndex			);
	bool					ShowSurface		(	uint			inSurfaceIndex			);
	bool					HideAllSurfaces	(											);
	bool					ShowAllSurfaces	(											);
	DpyState*				GetDisplayState	(	uint			inSurfaceIndex			);
	bool					SetAmbient		(	uint			inSurfaceIndex,
												Vec4*			inColor					);
	bool					SetDiffuse		(	uint			inSurfaceIndex,
												Vec4*			inColor					);
	uint					GetDrawStride	(											);
	bool					SetDrawStart	(	uint			inOffset				);
	bool					SetDrawSize		(	uint			inSize					);
};



#endif // _NvGeomShader_H_



