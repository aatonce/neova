/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvPoseShader_H_
#define	_NvPoseShader_H_



#include <NvShader.h>
interface NvResource;



interface NvPoseShader : public NvShader
{
	NVITF_DECL( PoseShader )


	enum Mode {									// Representation mode
		M_LOCKED			= 0,				// XYZ-Locked
		M_ORIENTED			= 1,				// Camera full XYZ-oriented
		M_ORIENTED_XZ		= 2,				// Camera XZ-oriented, Y-locked
		M_ORIENTED_XY		= 3,				// Camera XY-oriented, Z-locked
		M_ORIENTED_YZ		= 4					// Camera YZ-oriented, X-locked
	};

	enum Component {							// Pose's components
		C_LOCATION			= (1<<0),			// Vec3	  location
		C_ROTATION			= (1<<1),			// Quat	  rotation
		C_SCALE				= (1<<2),			// Vec3	  scale
		C_RGBA32			= (1<<3),			// uint32 RGBA
		C_DISPLACEMENT		= (1<<4)			// Vec3	  displacement
	};


	static NvPoseShader*	Create				(	NvResource*		inResource,
													uint			inComponents,				// Mask of C_... flags
													uint			inSize				);		// > 0

	NvInterface*			GetBase				(										);
	void					AddRef				(										);
	uint					GetRefCpt			(										);
	void					Release				(										);
	NvResource*				GetResource			(										);

	// Enable flags

	void					Enable				(	uint32			inEnableFlags		);
	void					Disable				(	uint32			inEnableFlags		);
	void					SetEnabled			(	uint32			inEnableFlags		);
	uint32					GetEnabled			(										);

	// Basic properties

	bool					HideSurface			(	uint			inSurfaceIndex		);
	bool					ShowSurface			(	uint			inSurfaceIndex		);
	bool					HideAllSurfaces		(										);
	bool					ShowAllSurfaces		(										);
	DpyState*				GetDisplayState		(	uint			inSurfaceIndex		);
	uint					GetDrawStride		(										);
	bool					SetDrawStart		(	uint			inOffset			);
	bool					SetDrawSize			(	uint			inSize				);

	// Parameters

	void					SetMode				(	Mode			inMode				);
	void					SetFading			(	float			inInnerDistance,			// inInnerDistance > 0 !
													float			inOuterDistance		);		// inOuterDistance > inInnerDistance !

	// Pose list managment

	uint					GetPoseComponents	(										);
	uint					GetListSize			(										);
	void					SetListDrawSize		(	uint			inSize				);
	uint					GetListDrawSize		(										);

	bool					CreateAccess		(										);
	pvoid					GetAccess			(	Component		inComponent,
													uint&			outStride,
													Psm*			outPSM		= NULL	);
	void					ValidateAccess		(	uint			inComponents,
													uint			inSizeFrom0			);
	void					ReleaseAccess		(										);
};



#endif // _NvPoseShader_H_



