/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvShadowingShader_H_
#define	_NvShadowingShader_H_



#include <NvShader.h>
interface NvResource;


interface NvShadowingShader : public NvShader
{
	NVITF_DECL( ShadowingShader )


	enum Type {
		T_DIRECTIONAL,							// Cast directional shadowing volume
		T_POINT									// Cast perspective shadowing volume
	};


	enum Flags {
		F_CAP_SIDES				= (1<<0),		// Compute volume sides cap
		F_CAP_FRONT				= (1<<1),		// Compute volume front cap
		F_CAP_BACK				= (1<<2),		// Compute volume back cap
		F_CAP_ALL				= (7<<0),		// Compute volume full closure
		F_CLEAR_STENCIL			= (1<<3),		// Reset the stencil buffer
		F_BACK_STENCIL			= (1<<4),		// Backup the shadowing stencil buffer in the back buffer stencil channel
		F_DRAW_SHADOWS			= (1<<5),		// Draw shadows in the back buffer
		F_DBG_SHOW_VOLUMES		= (1<<6),		// Draw shadow volumes in the back buffer
		F_DEFAULT				= (F_CAP_SIDES)
	};


	static NvShadowingShader*	Create			(	NvResource*		inResource				);

	NvInterface*				GetBase			(											);
	void						AddRef			(											);
	uint						GetRefCpt		(											);
	void						Release			(											);
	NvResource*					GetResource		(											);

	// Enable flags

	void						Enable			(	uint32			inEnableFlags			);
	void						Disable			(	uint32			inEnableFlags			);
	void						SetEnabled		(	uint32			inEnableFlags			);
	uint32						GetEnabled		(											);

	// Management

	bool						Setup			(	Type			inType,
													const Vec3&		inSource,						// location or direction
													float			inOffsetLength  = 0.f,			// >= 0
													float			inExtrudeLength = 500.f	);		// > 0
	void						SetColor		(	uint32			inRGBA					);		// A is not used
	void						SetFlags		(	uint			inFlags					);		// F_... mask
	uint						GetFlags		(											);		// F_... mask
};



#endif // _NvShadowingShader_H_



