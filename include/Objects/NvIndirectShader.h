/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you. 
**
*****************************************************************LIC-HDR*/


#ifndef	_NvIndirectShader_H_
#define	_NvIndirectShader_H_


#include <NvShader.h>
interface NvResource;


interface NvIndirectShader : public NvShader
{
	NVITF_DECL( IndirectShader )

	enum MapId
	{
		MI_Bump1		= 0,
		MI_Bump2		= 1,
		MI_Reflection	= 2,
	};
	
	enum FilterType
	{
		FT_Near		= 0,
		FT_Linear		= 1,
	};
	
	static	NvIndirectShader*	Create			(											);

	NvInterface*				GetBase			(											);
	void						AddRef			(											);
	uint						GetRefCpt		(											);
	void						Release			(											);
	NvResource*					GetResource		(											);

	// Enable flags

	void						Enable			(	uint32			inEnableFlags			);
	void						Disable			(	uint32			inEnableFlags			);
	void						SetEnabled		(	uint32			inEnableFlags			);
	uint32						GetEnabled		(											);

	// Basic properties
	DpyState*					GetDisplayState	(	uint			inSurfaceIndex			);

	void						SetSize			(	float			inWidth					,
													float			inHeight				);
	
	bool						SetMap			(	MapId			inId					,
													NvBitmap * 		inMap					,
													FilterType		inFt	=	FT_Near	);
													
	bool						SetCoord		(	MapId			inId					,
													Vec2			inCoord[4]				);													
	
	bool						SetBumpScale	(	float			bumpX					, // [0,1]
													float			bumpY					);// [0,1]

	
};



#endif // _NvIndirectShader_H_



