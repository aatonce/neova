/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvCartoonShader_H_
#define	_NvCartoonShader_H_


#include <NvShader.h>
#include <NvBitmap.h>

interface NvResource;


interface NvCartoonShader : public NvShader
{
	NVITF_DECL( CartoonShader )

	static NvCartoonShader*	Create					(	NvResource*		inResource				);
	NvInterface*			GetBase					(											);
	void					AddRef					(											);
	uint					GetRefCpt				(											);
	void					Release					(											);
	NvResource*				GetResource				(											);

	// Enable flags

	void					Enable					(	uint32			inEnableFlags			);
	void					Disable					(	uint32			inEnableFlags			);
	void					SetEnabled				(	uint32			inEnableFlags			);
	uint32					GetEnabled				(											);

	// Basic properties

	bool					HideSurface				(	uint			inSurfaceIndex			);
	bool					ShowSurface				(	uint			inSurfaceIndex			);
	bool					HideAllSurfaces			(											);
	bool					ShowAllSurfaces			(											);
	DpyState*				GetDisplayState			(	uint			inSurfaceIndex			);
	bool					SetAmbient				(	uint			inSurfaceIndex,
														Vec4*			inColor					);
	bool					SetDiffuse				(	uint			inSurfaceIndex,
														Vec4*			inColor					);
	uint					GetDrawStride			(											);
	bool					SetDrawStart			(	uint			inOffset				);
	bool					SetDrawSize				(	uint			inSize					);

	// Toon rendering parameters

	enum {
		DEF_SHADLEVEL		= 40,	// %
		DEF_INKRAMP_START	= 15,	// %
		DEF_INKRAMP_END		= 20,	// %
		DEF_OUTL_THICKNESS	= 2,	// in pixel unit
		DEF_OUTL_QUALITY	= 0
	};

	void					SetShadingLevel			(	float			inLevel					);		// in [0,1]

	void					SetInking				(	bool			inOnOff					);
	void					SetInkingRamp			(	float			inStartLevel,					// in [0,1]
														float			inEndLevel				);		// in [0,1]

	void					SetOutlining			(	bool			inOnOff					);
	void					SetOutliningThickness	(	float			inThickness				);		// >= 0
	void					SetOutliningQuality		(	float			inQuality				);		// in [0,1] 0:fastest, 1:accuratest
};



#endif // _NvCartoonShader_H_



