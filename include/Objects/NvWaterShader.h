/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you. 
**
*****************************************************************LIC-HDR*/


#ifndef	_NvWaterShader_H_
#define	_NvWaterShader_H_


#include <NvShader.h>
interface NvResource;


interface NvWaterShader : public NvShader
{
	NVITF_DECL( WaterShader )

	enum MapID
	{
		MI_BUMP1,
		MI_BUMP2,
		MI_DIFFUSE,
		MI_REFLECTION,
	};
	static	NvWaterShader*	Create			(											);

	NvInterface*			GetBase			(											);
	void					AddRef			(											);
	uint					GetRefCpt		(											);
	void					Release			(											);
	NvResource*				GetResource		(											);

	// Enable flags

	void					Enable			(	uint32			inEnableFlags			);
	void					Disable			(	uint32			inEnableFlags			);
	void					SetEnabled		(	uint32			inEnableFlags			);
	uint32					GetEnabled		(											);

	// Basic properties
	DpyState*				GetDisplayState	(	uint			inSurfaceIndex			);
	
	bool					SetMap			(	MapID			inId					,
												NvBitmap * 		inMap					);
												
	bool					SetMatrix		(	MapID			inId					,
												Matrix * 		inMatrix				);												
												
	void					SetSize			(	float			inWidth					,
												float			inHeight				);
												
	void					SetTextureArea	(	float			inWidth					,
												float			inHeight				);
												
};



#endif // _NvWaterShader_H_



