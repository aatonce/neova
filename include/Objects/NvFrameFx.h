/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvFrameFx_H_
#define	_NvFrameFx_H_


#include <NvDpyManager.h>
interface NvkFrameFx;



interface NvFrameFx : public NvDpyObject
{
	NVITF_DECL( FrameFx )

	enum Fx
	{
		FX_DFOG		= 0,	// Depth based fog
		FX_DOF,				// Depth of view
		FX_BLOOM,			// Bloom
		FX_BLUR,			// Simple blur
		FX_OUTLINE,			// Borders in black for Cartoon Rendering
	};

	static NvFrameFx*		Create				(								);
	NvInterface*			GetBase				(								);
	void					AddRef				(								);
	uint					GetRefCpt			(								);
	void					Release				(								);
	NvkFrameFx*				GetKInterface		(								);

	bool					GenerateFx			(	uint			inFxFlags	);	// FX_... flags mask

	bool					SetDFogColor		(	uint32			inRGBA		);
													
	bool					SetDFogDepth		(	uint8			inDepthIdx	,	// In [0,255]
													uint8			inDepth		);	// In [0,255]									
									
	bool					SetDOFSharpness		(	uint8			inDepthIdx	,	// In [0,255]
													uint8			inSharpness	);	// In [0,255]

	bool					SetBlurRadius		(	float			inRadius	);	// In pixel unit

	bool					SetBloom			(	uint32			inRGBA,
													float			inRadius	);	// In pixel unit
};



#endif	// _NvFrameFx_H_


