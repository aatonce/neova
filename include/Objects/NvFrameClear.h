/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvFrameClear_H_
#define	_NvFrameClear_H_


#include <NvDpyManager.h>



interface NvFrameClear : public NvDpyObject
{
	NVITF_DECL( FrameClear )

	static	NvFrameClear*	Create			(							);

	NvInterface*			GetBase			(							);
	void					AddRef			(							);
	uint					GetRefCpt		(							);
	void					Release			(							);

	void					EnableDepth		(	bool		inOnOff		);
	void					EnableColor		(	bool		inOnOff		);
	void					SetColor		(	uint32		inRGBA		);
};



#endif	// _NvFrameClear_H_



