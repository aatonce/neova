/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvUserShader_H_
#define	_NvUserShader_H_


#include <NvDpyManager.h>



interface NvUserShader : public NvDpyObject
{
	NVITF_DECL( UserShader )


	struct UserCB
	{
		virtual void	OnRef				(	NvUserShader*	inShader			) = 0;
		virtual void	OnRelease			(	NvUserShader*	inShader			) = 0;
		virtual void	OnDraw				(	NvUserShader*	inShader,
												uint32			inFrameNo,
												int				inContextChainHead,
												int				inContextChainCpt	) = 0;
	};


	static	NvUserShader*	Create			(	UserCB*			inCB				);

	NvInterface*			GetBase			(										);
	void					AddRef			(										);
	uint					GetRefCpt		(										);
	void					Release			(										);

};



#endif	// _NvUserShader_H_



