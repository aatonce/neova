/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvSound_H_
#define	_NvSound_H_


#include <Nova.h>
#include <NvRscManager.h>
interface NvkSound;



interface NvSound : public NvResource
{
	NVITF_DECL( Sound )

	static	const uint32	TYPE;
	static	NvSound*		Create					(	uint32		inUID				);

	NvInterface*			GetBase					(									);
	void					AddRef					(									);
	uint					GetRefCpt				(									);
	void					Release					(									);
	uint32					GetRscType				(									);
	uint32					GetRscUID				(									);
	NvkSound*				GetKInterface			(									);

	float					GetDuration				(									);
	bool					IsStreamed				(									);
	bool					IsLooped				(									);
	bool					IsMusic					(									);

	uint					GetMarkerCpt			(									);
	uint32					GetMarkerNameCRC		(	uint		inMarkerNo			);
	float					GetMarkerTime			(	uint		inMarkerNo			);
	int						FindMarker				(	uint32		inMarkerName		);
};



#endif // _NvSound_H_



