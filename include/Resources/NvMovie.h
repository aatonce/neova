/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef	_NvMovie_H_
#define	_NvMovie_H_




#include <NvBitmap.h>
interface NvkMovie;




interface NvMovie : public NvResource
{
	NVITF_DECL( Movie )

	enum State
	{
		MS_UNLOCKED = 0,
		MS_UNLOCKING,
		MS_LOCKING,
		MS_LOCKED,
		MS_DECODING,
		MS_SEEKING,
	};


	static const uint32		TYPE;
	static NvMovie*			Create				(	uint32		inUID			);

	NvInterface*			GetBase				(								);
	void					AddRef				(								);
	uint					GetRefCpt			(								);
	void					Release				(								);
	uint32					GetRscType			(								);
	uint32					GetRscUID			(								);
	NvkMovie*				GetKInterface		(								);

	uint					GetWidth			(								);		// pixels
	uint					GetHeight			(								);		// pixels
	uint					CountFrames			(								);
	float					GetFrameRate		(								);		// 0: not available
	float					GetFrameTime		(	int			inFrameNo		);
	float					GetDuration			(								);

	State					GetState			(								);
	bool					IsBusy				(								);

	bool					Lock				(	int			inFrameNo=0		);
	bool					Unlock				(								);
	bool					IsLocked			(								);

	int						GetPosition			(								);		// in frame unit

	bool					SkipFrame			(	int			inFrameCount=1	);
	bool					DecodeFrame			(								);

	NvBitmap*				GetBitmap			(								);

	void					Update				(								);
};




#endif // _NvMovie_H_



