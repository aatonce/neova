/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvSurface_H_
#define	_NvSurface_H_


#include <Nova.h>
#include <NvRscManager.h>
interface NvkSurface;



interface NvSurface : public NvResource
{
	NVITF_DECL( Surface )


	//
	// Vertex Components

	enum Component {
		CO_LOC			=		(1<<0),		// Location vector
		CO_TEX			=		(1<<1),		// Texture coordinate
		CO_COLOR		=		(1<<2),		// Primary color
		CO_NORMAL		=		(1<<3),		// Normal vector
		CO_KICK			=		(1<<4)		// Kicking mask
	};


	//
	// Packing mode

	enum PackMode {
		PM_NONE			=		0,
		PM_SAFE			=		1,
		PM_MAX			=		2,
		PM_FAST			=		PM_NONE,
		PM_STATIC		=		PM_MAX,
		PM_DEFAULT		=		PM_NONE
	};


	//
	// Present mode

	enum PresentMode {
		SM_SWAP			=		0,
		SM_COPY			=		1,
		SM_DEFAULT		=		SM_SWAP
	};


	static	const uint32		TYPE;
	static	NvSurface*			Create				(	uint			inSize,
														uint			inComponents,				// CO_* bit-mask of Components
														PackMode		inPackMode = PM_DEFAULT		);

	NvInterface*				GetBase				(												);
	void						AddRef				(												);
	uint						GetRefCpt			(												);
	void						Release				(												);
	uint32						GetRscType			(												);
	uint32						GetRscUID			(												);
	NvkSurface*					GetKInterface		(												);

	uint						GetComponents		(												);
	bool						HasComponent		(	Component		inComponent					);
	PackMode					GetPackingMode		(												);
	uint						GetMaxSize			(												);
	static Psm					GetNativeColorPSM	(												);

	bool						Resize				(	uint			inSize						);
	void						EnableDBF			(	PresentMode		inPresentMode= SM_DEFAULT,
														bool			inCreateNow  = FALSE		);
	void						DisableDBF			(	bool			inReleaseNow = FALSE		);
	bool						IsEnabledDBF		(												);
	bool						HasDBF				(												);

	bool						Begin				(	uint			inStartOffset = 0			);
	void						TexCoord			(	const Vec2&		inST						);
	void						Color				(	uint32			inColor,
														Psm				inPSM						);
	void						Normal				(	const Vec3&		inNXYZ						);
	void						Vertex				(	const Vec3&		inXYZ,
														bool			inKick = TRUE				);	// Kick enables(=TRUE) or disables(=FALSE) the triangle drawing.
	void						End					(												);
};




#endif // _NvSurface_H_



