/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvImage_H_
#define	_NvImage_H_


#include <NvBitmap.h>
interface NvkImage;




interface NvImage : public NvResource
{
	NVITF_DECL( Image )

	enum CrStatus
	{
		CS_SUCCESS				=  0,
		CS_UNSUPPORTED			= -1,			// Immediate bitmap is not supported in any case.
		CS_POW2_ONLY			= -2,			// Dimensions have to be power of 2.
		CS_SQUARE_ONLY			= -3,			// Bitmap have to be square.
		CS_WIDTH_GREATER		= -4,			// Bitmap must be width > height.
		CS_CLUT_UNSUPPORTED		= -5,			// CLUT is not supported.
		CS_CLUT_ONLY			= -6,			// Only CLUTed bitmap is supported.
		CS_TOO_MANY				= -7,			// No more immediate bitmap can be created.
		CS_TOO_HIGH				= -8,			// Too high resolution cannot be supported.
		CS_TOO_LOW				= -9,			// Too low resolution cannot be supported.
		CS_NOMORE_MEMORY		= -10,			// No more memory.
		CS_ERROR				= -11,			// other error
	};

	struct Region
	{
		uint	x, y;							// origin
		uint	w, h;							// dimensions
	};


	static const uint32		TYPE;
	static NvImage*			Create				(	uint		inWidth,
													uint		inHeight,
													bool		inUsingCLUT = FALSE,
													CrStatus*	outStatus	= NULL		);

	NvInterface*			GetBase				(										);
	void					AddRef				(										);
	uint					GetRefCpt			(										);
	void					Release				(										);
	uint32					GetRscType			(										);
	uint32					GetRscUID			(										);
	NvkImage*				GetKInterface		(										);

	NvBitmap*				GetBitmap			(										);

	uint32					GetWidth			(										);
	uint32					GetHeight			(										);
	NvBitmap::AlphaStatus	GetAlphaStatus		(										);

	bool					CreateAccess		(	Region&		inRegion				);
	pvoid					GetPixelAccess		(	uint&		outLineStride,
													Psm&		outPSM					);
	uint32*					GetClutAccess		(	Psm&		outPSM					);
	void					ValidatePixelAccess	(	Region&		inSubRegion				);
	void					ValidateClutAccess	(										);
	void					ReleaseAccess		(										);
};





#endif // _NvImage_H_


