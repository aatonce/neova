/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvMesh_H_
#define	_NvMesh_H_


#include <Nova.h>
#include <NvRscManager.h>
interface NvMorphTarget;
interface NvkMesh;



interface NvMesh : public NvResource
{
	NVITF_DECL( Mesh )

	enum EnableFlags {
		EN_LOC_SKINNING		= (1<<0),				// Compute skinning of locations
		EN_NRM_SKINNING		= (1<<1),				// Compute skinning of normals
		EN_SHD_SKINNING		= (1<<2),				// Compute skinning of shadowing data
		EN_MORPHING			= (1<<3),				// Compute morphing
		EN_DEFAULT			= (~EN_NRM_SKINNING)	// (default)
	};

	static	const uint32	TYPE;
	static	NvMesh*			Create				(	uint32			inUID				);

	NvInterface*			GetBase				(										);
	void					AddRef				(										);
	uint					GetRefCpt			(										);
	void					Release				(										);
	uint32					GetRscType			(										);
	uint32					GetRscUID			(										);
	NvkMesh*				GetKInterface		(										);

	Box3*					GetBBox				(										);
	uint32					GetNameCRC			(										);
	uint					GetSurfaceCpt		(										);
	uint32					GetSurfaceNameCRC	(	uint			inSurfaceNo			);
	uint32					GetSurfaceBitmapUID	(	uint			inSurfaceNo			);
	void					RefBitmaps			(										);
	void					UnrefBitmaps		(										);

	bool					IsSkinnable			(										);
	bool					IsNrmSkinnable		(										);
	bool					IsMorphable			(										);
	bool					IsLightable			(										);
	bool					IsShadowing			(										);
	bool					IsFullOpaque		(										);

	// Enable flags

	void					Enable				(	uint32			inEnableFlags		);		// EnableFlags mask
	void					Disable				(	uint32			inEnableFlags		);		// EnableFlags mask
	void					SetEnabled			(	uint32			inEnableFlags		);		// EnableFlags mask
	uint32					GetEnabled			(										);		// EnableFlags mask

	// Skinning

	bool					MapSkeleton			(	uint			inBoneCount,
													uint32*			inBoneNameA,
													Matrix*			inBlend16A			);		// Must be 16bytes aligned !
	void					UnmapSkeleton		(										);

	// Morphing

	uint					GetMorphSlotCpt		(										);
	void					SetMorphSlotCpt		(	uint			inSize				);
	NvMorphTarget*			GetMorphSlotTarget	(	uint			inSlotNo			);
	void					SetMorphSlotTarget	(	uint			inSlotNo,
													NvMorphTarget*	inTarget			);
	void					SetMorphSlotWeight	(	uint			inSlotNo,
													float			inWeight			);

	// Processing (according to enabled flags !)

	void					Update				(										);

};


#endif // _NvMesh_H_


