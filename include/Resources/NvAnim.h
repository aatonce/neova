/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvAnim_H_
#define	_NvAnim_H_



#include <Nova.h>
#include <NvRscManager.h>



interface NvAnim : public NvResource
{
	NVITF_DECL( Anim )


	enum Ctrl
	{
		CTRL_LOC	= (1<<0),
		CTRL_ROT	= (1<<1),
		CTRL_SCL	= (1<<2),
		CTRL_FLOAT	= (1<<3),
		CTRL_RGB	= (1<<4),
	};

	struct Note
	{
		float		time;
		int			channel;
		pcstr		text;
	};


	static	const uint32	TYPE;
	static	NvAnim*			Create				(	uint32		inUID				);


	NvInterface*			GetBase				(									);
	void					AddRef				(									);
	uint					GetRefCpt			(									);
	void					Release				(									);
	uint32					GetRscType			(									);
	uint32					GetRscUID			(									);

	uint					GetCtrlMask			(									);	// CTRL_...
	float					GetBeginTime		(									);
	float					GetEndTime			(									);
	float					GetDuration			(									);

	Box3*					GetBBox				(									);

	bool					GetLocValue			(	float		inT,
													Vec3*		outLoc		= NULL,
													Vec3*		outVelocity	= NULL,
													Vec3*		outSpeedUp	= NULL	);

	bool					GetRotValue			(	float		inT,
													Quat*		outRot		= NULL,
													Quat*		outVelocity	= NULL,
													Quat*		outSpeedUp	= NULL	);

	bool					GetSclValue			(	float		inT,
													Vec3*		outScl		= NULL,
													Vec3*		outVelocity	= NULL,
													Vec3*		outSpeedUp	= NULL	);

	bool					GetFloatValue		(	float		inT,
													float*		outF		= NULL,
													float*		outVelocity	= NULL,
													float*		outSpeedUp	= NULL	);

	bool					GetRGBValue			(	float		inT,
													Vec3*		outRGB		= NULL,
													Vec3*		outVelocity	= NULL,
													Vec3*		outSpeedUp	= NULL	);

	void					GetTRValue			(	float		inT,
													Matrix*		outTR,
													Vec3*		inDefLoc	= NULL,
													Quat*		inDefRot	= NULL,
													Vec3*		inDefScl	= NULL	);

	void					GetTRValue			(	float		inT,
													Matrix*		outTR,
													Matrix*		inDefTR				);


	// Note tracks

	uint					CountNotes			(									);
	Note*					EnumNotes			(									);

};




#endif // _NvAnim_H_


