/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvSkeleton_H_
#define	_NvSkeleton_H_


#include <Nova.h>
#include <NvRscManager.h>



interface NvSkeleton : public NvResource
{
	NVITF_DECL( Skeleton )


	// Target
	// trsA   : Final skeleton bone's matrix array, in root space
	// blendA : blendA[i] = toBoneTR0[i] * trsA[i]

	struct Target {
		MatrixA				trsA;		// 16 bytes aligned !
		MatrixA				blendA;		// 16 bytes aligned !
	};


	// State
	// Final skeleton bone's TRS array, in parent space

	struct State {
		TRSA				trsA;		// 16 bytes aligned !
	};


	static	const uint32	TYPE;
	static	NvSkeleton*		Create					(	uint32			inUID				);

	NvInterface*			GetBase					(										);
	void					AddRef					(										);
	uint					GetRefCpt				(										);
	void					Release					(										);
	uint32					GetRscType				(										);
	uint32					GetRscUID				(										);

	Box3*					GetBBox					(										);
	uint					GetBoneCpt				(										);

	// Bone hierarchical informations (none => -1)
	int16*					GetBoneParentIdxA		(										);		// count = GetBoneCpt();
	int16*					GetBoneSiblingIdxA		(										);		// count = GetBoneCpt();
	int16*					GetBoneChildIdxA		(										);		// count = GetBoneCpt();

	// Bone name CRC

	uint32					GetBoneName				(	uint			inBoneNo			);
	uint32*					GetBoneNameA			(										);		// count = GetBoneCpt();
	int						FindBoneByName			(	uint32			inBoneName			);

	// Bone note

	pcstr*					GetBoneNoteA			(										);		// count = GetBoneCpt();

	// Bone initial TRS

	TRS*					GetBoneInitialTRSA		(										);		// count = GetBoneCpt();

	// To Bone initial TR (relative to root)

	Matrix*					GetToBoneInitialTRA		(										);		// count = GetBoneCpt();

	// Animation processing

	void					InitState				(	State*			inState				);
	void					InitTarget				(	Target*			inTarget			);

	bool					ComputeTarget			(	Target*			outTarget,
														State*			inState				);

};




#endif	// _NvSkeleton_H_


