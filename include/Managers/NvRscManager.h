/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvRscManager_H_
#define	_NvRscManager_H_


#include <Nova.h>
interface RscPrefetchQ;
interface RscFactory;






//
// The base resource instance

interface NvResource : public NvInterface
{
	virtual	uint32		GetRscType			(				) = 0;
	virtual	uint32		GetRscUID			(				) = 0;
};





//
// Resource UID & TYPE invalid value

enum
{
	INVALID_RSC_UID		= 0,
	INVALID_RSC_TYPE	= 0
};
#define	ISVALID_RSC_UID ( inUID )	( (inUID ) != INVALID_RSC_UID  )
#define	ISVALID_RSC_TYPE( inTYPE )	( (inTYPE) != INVALID_RSC_TYPE )








//
// The resource Manager


namespace RscManager
{

	bool			Init				(													);
	bool			Shut				(													);

	bool			RegisterFactory		(	RscFactory*				inFactory				);
	RscFactory*		GetFactory			(	uint32					inType					);
	uint			CountFactory		(													);
	RscFactory*		EnumFactory			(	uint					inIndex					);


	//
	// Update the manager
	// 'inMaxMsTime' represents the maximum time slice used by the manager (>0).

	void			Update				(	float					inMaxMsTime				);


	//
	// BigFile open / close / get-path

	bool			OpenBigFile			(	pcstr					inPath					);
	bool			CloseBigFile		(													);
	pcstr			GetBigFile			(													);


	//
	// Prefetching

	bool			AddPrefetchQ		(	RscPrefetchQ*			inPrefetchQ				);
	bool			AbortPrefetchQ		(	RscPrefetchQ*			inPrefetchQ				);
	RscPrefetchQ*	GetProgress			(	float*					outProgress	= NULL		);		// in [0,1]


	//
	// Resource infos from the current BigFile
	// Returns INVALID_RSC_UID or FALSE if the call has failed.

	struct RscInfo
	{
		uint32		uid;
		uint32		type;
		uint32		bsize;
		uint32		boffset[4];
		uint32		depCpt;
	};

	uint32			GetRscCpt			(													);
	uint32			GetRscUID			(	uint					inNo					);
	bool			GetRscInfo			(	uint32					inUID,
											RscInfo*				outInfo	= NULL			);


	//
	// Find Resource UID by type
	// Returns INVALID_RSC_UID if no result is available.

	uint32			FindRscByType		(	uint32					inType,							// Type CRC
											uint32					inPreviousUID	= 0		);


	//
	// Generic Resource instance creation

	NvResource*		CreateInstance		(	uint32					inUID					);
	bool			IsAvailableRsc		(	uint32					inUID					);


	//
	// Unloading

	uint			Unload				(	uint32					inUID					);
	uint			Unload				(	UInt32A&				inUIDs					);
	uint			Unload				(	RscPrefetchQ*			inPrefetchQ				);
	uint			UnloadAll			(													);


	//
	// Atomic bigfile prefetch

	enum AtomStatus
	{
		AS_Retry = 0,
		AS_Abort,
	};

	struct AtomHook
	{
		virtual AtomStatus		onError			(	nv::file::Status	inError			) = 0;
		virtual void			onProgress		(	float				inProgress		) = 0;
	};

	bool						AtomOpenBigFile	(	AtomHook*			inHook,
													pcstr				inBigfile		);

	bool						AtomPrefetch	(	AtomHook*			inHook,
													RscPrefetchQ*		inPrefetchQ		);



	//
	// Log

	struct RscLog
	{
		uint32		uid;
		float		t;		// time in s
	};

	bool			GetRscLog			(	nv::vector<RscLog>&		outLog					);

}






//
// The Prefetch Resource Queue

struct RscPrefetchQ
{
	enum State
	{
		IDLE		= 0,			// Not enqueued.
		PENDING		= 1,			// Enqueued ...
		COMPLETED	= 2,			// Prefetch is completed.
		ABORTED		= -1,			// The prefetch has been aborted !
		FAILED		= -2			// The prefetch failed !
	};

					RscPrefetchQ		(							)	{ stt = IDLE;				}
	void			AddRsc				(	uint32		inUID		)	{ rscUIDA.push_back(inUID);	}
	uint			GetSize				(							)	{ return rscUIDA.size();	}
	uint32			operator []			(	int			inNo		)	{ return rscUIDA[inNo];		}
	void			Clear				(							)	{ rscUIDA.clear();			}

	bool			IsReady				(							)	{ return (stt!=PENDING);	}
	bool			IsCompleted			(							)	{ return (stt==COMPLETED);	}
	State			GetState			(							)	{ return stt;				}

	// reserved ...
	volatile State			stt;
	RscPrefetchQ* volatile	next;
	UInt32A					rscUIDA;
};




#endif	// _NvRscManager_H_





