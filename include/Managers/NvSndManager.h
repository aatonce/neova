/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvSndManager_H_
#define	_NvSndManager_H_


#include <Nova.h>
interface NvSound;




namespace SndManager
{
	enum OutputMode
	{										// output mode
		OM_MONO			= 0,				// 1.1
		OM_MONOPAN,							// 1.1 (mono using panning minimization)
		OM_STEREO,							// 2.1
		OM_DPL2								// 4.1 (Dolby Pro-Logic II)
	};

	enum SdState
	{
		SD_UNUSED		= 0,				// unused (invalid id!)
		SD_UNLOCKED		= 1,				// unlocked
		SD_LOCKING		= 2,				// locking
		SD_LOCKED		= 3,				// locked
		SD_UNLOCKING	= 4					// unlocking
	};

	enum ChState
	{
		CH_IDLE			= 0,
		CH_PLAYING		= 1
	};

	enum
	{
		SD_ANY			= -1000,			// SdLock(), SdAlloc()
		CH_ANY			= -1000,			// ChPlay()
	};

	typedef bool		(*StreamHandler)	(	int					inSoundId,					// streamed sound-id
												float&				outFromTime,				// next segment start time
												float&				outDuration				);	// next segment duration



	bool				Init				(												);
	void				Update				(												);
	StreamHandler		SetHandler			(	StreamHandler		inHandler				);
	bool				Shut				(												);


	//
	// Output managment

	bool				ProbeOutputMode		(	OutputMode			inOMode					);
	void				SetOutputMode		(	OutputMode			inOMode					);
	OutputMode			GetOutputMode		(												);
	void				SetMasterVolume		(	float				inVol					);	// in [0,1]


	//
	// Sound management

	int					SdGetNbMax			(												);
	int					SdAny				(												);
	SdState				SdGetState			(	int					inSoundId				);
	int					SdLock				(	int					inSoundId,					// valid sound or SD_ANY
												NvSound*			inSndObj,
												float				inFromTime	= 0			,	// (for streamed sound only !)
												float				inDuration	= 0			);	// (for streamed sound only !)
	bool				SdUnlock			(	int					inSoundId				);
	bool				SdBreak				(	int					inSoundId				);	// (for streamed sound only !)
	void				SdFree				(	int					inSoundId				);
	void				SdFreeAll			(												);
	int					SdAlloc				(	int					inSoundId  = SD_ANY		);
	bool				SdIsLocking			(	int					inSoundId				);
	bool				SdIsLocked			(	int					inSoundId				);
	bool				SdIsUnused			(	int					inSoundId				);


	//
	// Channel management

	int					ChGetNb				(												);
	int					ChAny				(												);
	ChState				ChGetState			(	int					inChannel				);
	int					ChPlay				(	int					inChannel,					// valid channel or CH_ANY
												int					inSoundId,
												float				inVol			= 1.f,		// volume in [0,1]
												float				inPanAngle		= 0.f,		// panning angle in [-Pi,Pi]
												float				inPitchFactor	= 1.f	);
	bool				ChStop				(	int					inChannel				);
	void				ChStopAll			(												);
	bool				ChPause				(	int					inChannel				);
	bool				ChResume			(	int					inChannel				);
	bool				ChSetVolume			(	int					inChannel,
												float				inVol					);	// volume in [0,1]
	bool				ChSetPanning		(	int					inChannel,
												float				inAngle					);	// panning angle in [-Pi,Pi]
	bool				ChSetPitchFactor	(	int					inChannel,
												float				inFactor				);
	bool				ChIsPlaying			(	int					inChannel				);
	bool				ChWaitPlaying		(	int					inChannel,
												float				inTimeout		= 1.f	);	// > 0, in second


	//
	// Background music management

	bool				BgmPlay				(	int					inSoundId,
												float				inVol			= 1.f	);	// volume in [0,1]
	bool				BgmStop				(												);
	bool				BgmPause			(												);
	bool				BgmResume			(												);
	bool				BgmBreak			(												);
	bool				BgmSetVolume		(	float				inVol					);	// volume in [0,1]
	bool				BgmIsPlaying		(												);
	bool				BgmWaitPlaying		(	float				inTimeout		= 1.f	);	// > 0, in second


	//
	// Tools

	float				MbToLinearVolume	(	float				inMillibel				);	// [-10000,0] -> [0,1]
	float				LinearToMbVolume	(	float				inLinear				);	// [0,1] -> [-10000,0]
}



#include "NvSndManager_I.h"



#endif	// _NvSndManager_H_


