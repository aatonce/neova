/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifdef _NvDpyManager_H_



//
// Display Target

inline DpyTarget::DpyTarget			(								)
{
	Raster = DPYR_NULL;
}

inline DpyTarget::DpyTarget			(	DpyRaster	inRaster		)
{
	Raster = inRaster;
}

inline bool DpyTarget::operator ==	(	const DpyTarget	&	inRef	)	const
{
	return ( Raster == inRef.Raster );
}

inline bool DpyTarget::operator !=	(	const DpyTarget	&	inRef	)	const
{
	return ( Raster != inRef.Raster );
}

inline void DpyTarget::operator =	(	const DpyTarget	&	inRef	)
{
	Raster = inRef.Raster;
}

inline void DpyTarget::Set			(	DpyRaster	inRaster		)
{
	Raster = inRaster;
}



//
// Display Source

inline DpySource::DpySource			(								)
{
	Raster = DPYR_NULL;
	Bitmap = NULL;
}

inline DpySource::DpySource			(	DpyRaster	inRaster		)
{
	Raster = inRaster;
	Bitmap = NULL;
}

inline DpySource::DpySource			(	NvBitmap*	inBitmap		)
{
	Bitmap = inBitmap;
	Raster = DPYR_NULL;
}

inline bool DpySource::operator ==	(	const DpySource	&	inRef	)	const
{
	return (Bitmap==inRef.Bitmap) && (Raster==inRef.Raster);
}

inline bool DpySource::operator !=	(	const DpySource	&	inRef	)	const
{
	return (Bitmap!=inRef.Bitmap) || (Raster!=inRef.Raster);
}

inline void DpySource::operator =	(	const DpySource	&	inRef	)
{
	Raster = inRef.Raster;
	Bitmap = inRef.Bitmap;
}

inline void DpySource::Set			(	DpyRaster	inRaster		)
{
	Raster = inRaster;
	Bitmap = NULL;
}

inline void DpySource::Set			(	NvBitmap*	inBitmap		)
{
	Bitmap = inBitmap;
	Raster = DPYR_NULL;
}



#endif // _NvDpyManager_H_


