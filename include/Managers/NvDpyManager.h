/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvDpyManager_H_
#define	_NvDpyManager_H_


#include <Nova.h>
interface NvBitmap;




//
// Display Raster
//
// - NULL raster (drawing disabled)
// - IN-FRAME (back buffer)
// - OFF-FRAMES (auxiliaries buffers)

enum DpyRaster
{
	DPYR_NULL				= 0,
	DPYR_IN_FRAME			= 1,
	DPYR_OFF_FRAME_C16		= 2,
	DPYR_OFF_FRAME_C32H		= 3,
	DPYR_OFF_FRAME_C32V		= 4,
	DPYR_OFF_FRAME_C16Z16H	= 5,
	DPYR_OFF_FRAME_C16Z16V	= 6,
	DPYR_OFF_FRAME_C32Z32	= 7
};




//
// Display target
//
// A rendering destination raster

interface DpyTarget
{
	inline				DpyTarget			(									);
	inline				DpyTarget			(		  DpyRaster		inRaster	);
	inline	void		operator	=		(	const DpyTarget&	inRef		);
	inline	bool		operator	==		(	const DpyTarget&	inRef		)	const;
	inline	bool		operator	!=		(	const DpyTarget&	inRef		)	const;
	inline	void		Set					(		  DpyRaster		inRaster	);
	DpyRaster			Raster;
};





//
// Display source
//
// A rendering source
// - A raster
// - A bitmap

interface DpySource
{
	inline				DpySource			(									);
	inline				DpySource			(		  DpyRaster		inRaster	);
	inline				DpySource			(		  NvBitmap*		inBitmap	);
	inline	void		operator	=		(	const DpySource&	inRef		);
	inline	bool		operator	==		(	const DpySource&	inRef		)	const;
	inline	bool		operator	!=		(	const DpySource&	inRef		)	const;
	inline	void		Set					(		  DpyRaster		inRaster	);
	inline	void		Set					(		  NvBitmap*		inBitmap	);
	DpyRaster			Raster;
	NvBitmap*			Bitmap;
};





//
// Display state
//
// Generic GPU parameters manager

interface DpyState
{
	enum EnableFlags
	{
		EN_WR_COLOR				= (1<<0),			// Write in target color-buffer ?
		EN_WR_DEPTH				= (1<<1),			// Write in target depth-buffer ?
		EN_RD_DEPTH				= (1<<2),			// Read from target depth-buffer ?
		EN_RD_STENCIL			= (1<<3),			// Read from target stencil-buffer ?
		EN_TEXTURING			= (1<<4),			// Texturing ?
		EN_ALL_MASK				= (0x1F),
		EN_DEFAULT				= ~(EN_RD_STENCIL),
		EN_SHADER_MASK			= (EN_ALL_MASK)		// (NvShader -> DpyState mask)
	};

	enum ModeFlags
	{
		// Texturing function
		TF_DECAL				= (1<<0),
		TF_MODULATE				= (2<<0),			// (default)
		TF_HIGHLIGHT			= (3<<0),
		TF_HIGHLIGHT2			= (4<<0),
		TF_MASK					= (7<<0),
		// Texturing magnification sampler
		TM_NEAREST				= (1<<3),
		TM_LINEAR				= (2<<3),			// (default)
		TM_MASK					= (3<<3),
		// Texturing reduction sampler
		TR_NEAREST_MIP0			= (1<<5),			// (no mipmaping)
		TR_LINEAR_MIP0			= (2<<5),			// (no mipmaping)
		TR_NEAREST_NEAREST		= (3<<5),
		TR_NEAREST_LINEAR		= (4<<5),
		TR_LINEAR_NEAREST		= (5<<5),			// (default)
		TR_LINEAR_LINEAR		= (6<<5),
		TR_MASK					= (7<<5),
		// Texturing wrapping mode
		TW_REPEAT_REPEAT		= (1<<8),			// (default)
		TW_REPEAT_CLAMP			= (2<<8),
		TW_CLAMP_REPEAT			= (3<<8),
		TW_CLAMP_CLAMP			= (4<<8),
		TW_MASK					= (7<<8),
		// Culling mode
		CM_FRONTSIDED			= (1<<11),			// (default)
		CM_BACKSIDED			= (2<<11),
		CM_TWOSIDED				= (3<<11),
		CM_MASK					= (3<<11),
		// Blending mode
		BM_SELECT				= (1<<13),			// Select NONE/FILTER according to the source alpha status (default)
		BM_NONE					= (2<<13),			// DST = SRC
		BM_FILTER				= (3<<13),			// DST = a*SRC + (1-a)*DST
		BM_ADD					= (4<<13),			// DST = DST   + a*SRC
		BM_SUB					= (5<<13),			// DST = DST   - a*SRC
		BM_MODULATE				= (6<<13),			// DST = a*SRC
		BM_FILTER2				= (7<<13),			// DST = a*DST + (1-a)*SRC
		BM_ADD2					= (8<<13),			// DST = SRC   + a*DST
		BM_SUB2					= (9<<13),			// DST = SRC   - a*DST
		BM_MODULATE2			= (10<<13),			// DST = a*DST
		BM_DUMMY				= (11<<13),			// DST = DST
		BM_ZERO					= (12<<13),			// DST = 0
		BM_MUL					= (13<<13),			// DST = DST*SRC
		BM_MASK					= (15<<13),
		// Blending source
		BS_FRAGMENT				= (1<<17),			// From fragment (default)
		BS_BUFFER				= (2<<17),			// From target buffer
		BS_CTE					= (3<<17),			// Fix
		BS_MASK					= (3<<17),

		MODE_DEFAULT			= TF_MODULATE		// (default)
								| TM_LINEAR
								| TR_LINEAR_NEAREST
								| TW_REPEAT_REPEAT
								| CM_FRONTSIDED
								| BM_SELECT
								| BS_FRAGMENT
	};

	// Enable flags
	virtual void			Enable			(	uint				inEnableFlags	) = 0;	// mask of EnableFlags
	virtual void			Disable			(	uint				inEnableFlags	) = 0;	// mask of EnableFlags
	virtual void			SetEnabled		(	uint				inEnableFlags	) = 0;	// mask of EnableFlags
	virtual uint			GetEnabled		(										) = 0;	// mask of EnableFlags

	// Mode flags
	virtual	void			SetMode			(	uint				inModeFlags		) = 0;	// Mask of ModeFlags values
	virtual	uint			GetMode			(										) = 0;	// Mask of ModeFlags values

	// State constants
	virtual	bool			SetSource		(	DpySource			inSource		) = 0;
	virtual	void			GetSource		(	DpySource&			outSource		) = 0;
	virtual	void			SetSourceOffset	(	const Vec2&			inOffsetXY		) = 0;
	virtual	void			GetSourceOffset	(	Vec2&				outOffsetXY		) = 0;	// in [0,1]^2
	virtual	bool			SetAlphaPass	(	float				inAlphaPass		) = 0;	// in [0,1]
	virtual	float			GetAlphaPass	(										) = 0;	// in [0,1]
	virtual	bool			SetBlendSrcCte	(	float				inCte			) = 0;	// in [0,1]
	virtual	float			GetBlendSrcCte	(										) = 0;	// in [0,1]
	virtual void			SetWRColorMask	(	uint32				inRGBAMask		) = 0;
	virtual uint32			GetWRColorMask	(										) = 0;
};





//
// Display object

interface NvDpyObject : public NvInterface
{
	//
};





//
// Display manager

namespace DpyManager
{
	enum CoordinateSystem
	{
		CS_RIGHT_HANDED	= 0,
		CS_LEFT_HANDED
	};

	enum Performance
	{
		PERF_GPU_FPS,
		PERF_CPU_FPS,
		PERF_DRAW_CPT,
		PERF_TRI_CPT,
		PERF_LOC_CPT,
		PERF_VTX_CPT,
		PERF_PRIM_CPT,
		PERF_TEX_BSIZE
	};


	// Init / Shut

	bool				Init				(	DpyManager::CoordinateSystem	inCS		);
	bool				Shut				(												);


	// Display managment

	CoordinateSystem	GetCoordinateSystem	(												);
	uint				GetVSyncRate		(												);
	uint				GetLatency			(												);
	uint				GetViewingLatency	(												);
	uint				GetPhysicalWidth	(												);
	uint				GetPhysicalHeight	(												);
	float				GetPhysicalAspect	(												);
	uint				GetVirtualWidth		(												);
	uint				GetVirtualHeight	(												);
	float				GetVirtualAspect	(												);
	uint				GetRasterWidth		(	DpyRaster			inRaster				);
	uint				GetRasterHeight		(	DpyRaster			inRaster				);
	bool				TranslateCRTC		(	int					inX0,
												int					inY0					);


	// Frame managment

	bool				BeginFrame			(												);
	bool				EndFrame			(												);
	bool				FlushFrame			(	bool				inPresent = TRUE,
												bool				inGetPerf = FALSE		);
	void				SyncFrame			(												);
	void				SetFrameBreakBefore	(	NvDpyObject*		inDpyObj  = NULL		);
	void				SetFrameBreakAfter	(	NvDpyObject*		inDpyObj  = NULL		);
	bool				IsInFrame			(	NvDpyObject*		inDpyObj				);
	bool				IsDrawing			(	NvDpyObject*		inDpyObj				);
	bool				IsDrawing			(	uint32				inFrameNo				);
	bool				SaveFrame			(	pcstr				inFilename				);
	bool				ReadFrame			(	pvoid				outBuffer				);
	bool				WriteFrame			(	pvoid				inBuffer				);
	uint				GetFrameNo			(												);
	Psm					GetFramePSM			(												);
	float				GetPerformance		(	Performance			inPerf					);


	// Session context

	void				SetSession			(	uint8				inSessionNo				);	// in [0,15]

	// Target context

	void				SetTarget			(	DpyTarget			inTarget				);

	// View context

	void				SetView				(	Matrix*				inViewTR	 = NULL,		// camera-matrix^-1
												Matrix*				inProjTR	 = NULL,		// projection-matrix
												Vec2*				inClipRange	 = NULL,		// ( nearZ, farZ )
												Vec4*				inViewport	 = NULL		);	// ( x, y, w, h ) in pixel

	// Lighting context

	void				SetLight			(	uint				inLightNo,					// in [0,2]
												Vec4*				inColor		 = NULL,		// in [0,1]^4
												Vec3*				inDirection	 = NULL		);

	// Transform context

	void				SetWorldTR			(	Matrix*				inWorldTR				);

	// Draw order
	
	bool				Draw				(	NvDpyObject*		inDpyObj				);
	
	
	// Draw centered texture immediately
	void				DrawImmediate		(	void* 				inTex					,		// RGBA8 bitmap
												uint				inWidth					,
												uint				inHeight				,
												int					inOffsetX	= 0			,		// default => centered
												int					inOffsetY	= 0			);		// default => centered
	
}



#include <NvDpyManager_I.h>


#endif	// _NvDpyManager_H_


