/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _NvSndManager_H_


inline
bool
SndManager::SdIsLocking		(	int			inSoundId		)
{
	return SdGetState(inSoundId) == SD_LOCKING;
}


inline
bool
SndManager::SdIsLocked		(	int			inSoundId		)
{
	return SdGetState(inSoundId) == SD_LOCKED;
}


inline
bool
SndManager::SdIsUnused		(	int			inSoundId		)
{
	return SdGetState(inSoundId) == SD_UNUSED;
}


inline
bool
SndManager::ChIsPlaying		(	int			inChannel		)
{
	return ChGetState(inChannel) == CH_PLAYING;
}


inline
bool
SndManager::ChWaitPlaying	(	int					inChannel,
								float				inTimeout		)
{
	if( inChannel<0 || inChannel>=ChGetNb() )
		return FALSE;
	nv::clock::Time t0, t1;
	nv::clock::GetTime( &t0 );
	for( ;; ) {
		nv::clock::GetTime( &t1 );
		if( (t1-t0) > inTimeout )		return FALSE;
		if( ChIsPlaying(inChannel) )	return TRUE;
		nv::core::Update();
		SndManager::Update();
	}
}


inline
bool
SndManager::BgmWaitPlaying	(	float				inTimeout		)
{
	nv::clock::Time t0, t1;
	nv::clock::GetTime( &t0 );
	for( ;; ) {
		nv::clock::GetTime( &t1 );
		if( (t1-t0) > inTimeout )		return FALSE;
		if( BgmIsPlaying() )			return TRUE;
		nv::core::Update();
		SndManager::Update();
	}
}


#endif // _NvSndManager_H_



