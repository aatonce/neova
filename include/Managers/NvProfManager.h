/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvProfManager_H_
#define	_NvProfManager_H_


#include <Nova.h>



namespace ProfManager
{

	bool			Init				(	uint			inPort	= 18255			);
	void			Shut				(											);

	bool			IsRunning			(											);
	bool			IsConnected			(											);
	bool			IsRecording			(											);
	void			Update				(											);


	//
	// Profiling node

	const uint32	USER_PUID = 0x10000;

	bool			Setup				(	uint32			inPUID,
											pvoid			inData,
											uint			inBSize					);
	bool			Append				(	uint32			inPUID,
											pvoid			inData,
											uint			inBSize					);
	bool			Append				(	pvoid			inData,
											uint			inBSize					);

}





inline void
PROF_SETUP( uint32 inPUID, pvoid inData, uint inBSize ) {
	#ifndef _MASTER
	ProfManager::Setup( inPUID, inData, inBSize );
	#endif
}

template < typename T > inline void
PROF_SETUP( uint32 inPUID, T & inData ) {
	#ifndef _MASTER
	ProfManager::Setup( inPUID, &inData, sizeof(T) );
	#endif
}

inline void
PROF_APPEND( uint32 inPUID, pvoid inData, uint inBSize ) {
	#ifndef _MASTER
	ProfManager::Append( inPUID, inData, inBSize );
	#endif
}

template < typename T > inline void
PROF_APPEND( uint32 inPUID, const T & inData ) {
	#ifndef _MASTER
	ProfManager::Append( inPUID, &inData, sizeof(T) );
	#endif
}

inline void
PROF_APPEND( pvoid inData, uint inBSize ) {
	#ifndef _MASTER
	ProfManager::Append( inData, inBSize );
	#endif
}

template < typename T > inline void
PROF_APPEND( const T & inData ) {
	#ifndef _MASTER
	ProfManager::Append( &inData, sizeof(T) );
	#endif
}





#endif	// _NvProfManager_H_



