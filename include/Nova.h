/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#ifndef _NOVA_H_
#define _NOVA_H_



#include <NvCore_Settings.h>
#include <NvCore_Types.h>
#include <NvCore.h>
#include <NvCore_Report.h>
#include <NvCore_LibC.h>
#include <NvCore_Mem.h>
#include <NvCore_STL.h>
#include <NvCore_Math.h>
#include <NvCore_Def.h>
#include <NvCore_CRC.h>
#include <NvCore_Clock.h>
#include <NvCore_Ctrl.h>
#include <NvCore_NVR.h>
#include <NvCore_File.h>
#include <NvCore_Net.h>
#include <NvCore_Jpeg.h>
#include <NvCore_Stack.h>
#include <NvCore_Startup.h>


#endif	// _NOVA_H_



