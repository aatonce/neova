/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkSound_H_
#define	_NvkSound_H_


#include "NvSndManager[PC].h"
#include <NvSound.h>


interface NvkSound : public NvResource
{
	NVKITF_DECL( Sound )

	NvInterface *			GetBase				(							);
	void					AddRef				(							);
	uint					GetRefCpt			(							);
	void					Release				(							);
	uint32					GetRscType			(							);
	uint32					GetRscUID			(							);
	NvSound*				GetInterface		(							);

	float					GetDuration			(							);
	uint32					GetSampleLength		(							);
	bool					IsStreamed			(							);
	bool					IsLooped			(							);
	bool					IsMusic				(							);
	uint					GetMode				(							);
	uint					GetFreq				(							);
	void					GetBFData			(	uint32	&	outBOffset,
													uint32	&	outBSize	);
};



#endif // _NvkSound_H_


