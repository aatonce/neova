/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkSurface_H_
#define	_NvkSurface_H_


#include "NvDpyManager[PC].h"
#include <NvSurface.h>
#include <Kernel/PC/NvHrdw.h>

interface NvkSurface : public NvInterface
{
	NVKITF_DECL( Surface )

	enum Buffer {
		BF_NULL		= 0,
		BF_BACK		= 1,
		BF_FRONT	= 2
	};

	enum EnableFlag{
		EN_PRESENT		= (1<<0),
		EN_BEGIN_END	= (1<<1),
		EN_GENERATE_IDX	= (1<<2),
		EN_ALL			= 0x7,
		EN_DEFAULT		= EN_ALL
	};

	NvInterface*				GetBase					(												);
	void						AddRef					(												);
	uint						GetRefCpt				(												);
	void						Release					(												);
	uint32						GetRscType				(												);
	uint32						GetRscUID				(												);
	NvSurface*					GetInterface			(												);

	Hrdw::DeclIndex				GetVSDecl				(												);
	uint						GetComponents			(												);
	uint						GetComponentSize		(	NvSurface::Component	inCo				);
	bool						HasComponent			(	NvSurface::Component	inComponent			);
	NvSurface::PackMode			GetPackingMode			(												);
	uint						GetMaxSize				(												);
	uint						GetVertexStride			(												);
	static Psm					GetNativeColorPSM		(												);

	bool						Reset					(	uint					inMaxSize			);
	void						EnableDBF				(	NvSurface::PresentMode	inPresentMode,
															bool					inCreateNow = FALSE	);
	void						DisableDBF				(	bool					inReleaseNow= FALSE	);
	bool						IsEnabledDBF			(												);
	bool						HasDBF					(												);

	void						Enable					(	EnableFlag				inFlag				);
	void						Disable					(	EnableFlag				inFlag				);
	bool						IsEnabled				(	EnableFlag				inFlag				);

	void						DestroyBuffer			(	Buffer					inBuffer			);
	void						AllocBuffer				(	Buffer					inBuffer			);	
	Hrdw::VertexBuffer			GetVtxBufferBase		(	Buffer					inBuffer			);
	Hrdw::IndexBuffer			GetIdxBufferBase		(	Buffer					inBuffer			);
	uint16*						GetIdcBufferBase		(	Buffer					inBuffer			);
	void						GenerateIdxFromKick		(	Buffer					inBuffer			);
	bool						PresentBuffer			(												);

	void						UpdateBeforeDraw		(												);
	bool						IsDrawing				(												);

	uint						GetDrawingSize			(	Buffer					inBuffer,
															uint					inVertexOffset,
															uint					inVertexSize		);
	uint						GetDrawingOffset		(   Buffer					inBuffer,
															uint					inVertexOffset		);
	uint						GetVertexSize			(	Buffer					inBuffer			);
};



#endif // _NvkSurface_H_


