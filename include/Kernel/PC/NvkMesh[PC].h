/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkMesh_H_
#define	_NvkMesh_H_


#include "NvDpyManager[PC].h"
#include <NvMesh.h>
#include <Kernel/PC/NvHrdw.h>

interface NvkMesh : public NvResource
{
	NVKITF_DECL( Mesh )

	struct Shading {
		uint32				nameCRC;					// 0 is unnamed
		uint32				emissive;					// ABGR in [0,255]
		uint32				ambient;					// ABGR in [0,255]
		uint32				diffuse;					// ABGR in [0,255]
		uint32				specular;					// ABGR in [0,255]
		float				glossiness;
		float				specLevel;
		uint32				stateEnFlags;				// DpyState::EnableFlags
		uint32				stateMdFlags;				// DpyState::ModeFlags
		float				stateBlendCte;				// DpyState blend-source-cte
		float				stateAlphaPass;				// DpyState alpha-pass-threshold
		uint32				bitmapUID;
		float				mappingRatio;				// For L&K auto-computation
		uint32				isFullOpaque;				// 0/1
	};

	struct ShadowingVtx {
		Vec3				location;
		Vec3				normal;
		uint32				boneCRC;					// none if 0
	};

	struct ShadowingFace {								// 2 x V3_16
		int16				vtx[3];						// face vertex indices (always >= 0)
		int16				adj[3];						// face adjacency (always >= 0, same faceNo for none)
	};

	struct BunchData {
		uint16				surfNo;						// surface idx
		uint16				bunchNo;					// bunch idx
		
		uint32				locIdxOffset;				// loc index array		//uint16 * 
		uint32				nrmIdxOffset;				// normal index array	//uint16 *

		uint32				ibSize;						// index buffer size
		uint32				vbSize;						// vertex buffer size	== NbVertex
		uint32				indexOffset;				// index Offset for RenderPrimitive in the directX buffer
		uint32				vertexDataOffset;			// VTX, NRM, TEX, COL Offset for RenderPrimitive in the directX buffer

		uint16				nextByBunch;				// next in bunch order ('bunchByBunch' list) (last.next=first)
		uint16				nextBySurf;					// next in surf order ('bunchBySurf' list) (last.next=first)
	};

	struct BunchList {
		uint16				size;						// list size (>=0)
		uint16				startIdx;					// list start idx in bunchDataA
	};

	struct Bunch {
		uint32				faceCpt;					// # face
		uint32				locCpt;						// # locations
		uint32				nrmCpt;				

		uint32				vtxCpt;						// # welded vertex
		uint16				surfCpt;					// # surface for the bunch
	};

	struct BunchHrdwData{
		Hrdw::IndexBuffer		ib;					// NULL if unused
		Hrdw::VertexBuffer		vbLoc;				// Always != NULL
		Hrdw::VertexBuffer		vbNrm;				// NULL if unused
		Hrdw::VertexBuffer		vbTex;				// NULL if unused
		Hrdw::VertexBuffer		vbCol;				// NULL if unused

		Hrdw::VertexBuffer		vbLocDB;			// Double Buffer for skinning/morphing , NULL if no skinning/morphing
		Hrdw::VertexBuffer		vbNrmDB;			// Double Buffer for skinning/morphing , NULL if no skinning/morphing
	};

	NvInterface	*				GetBase				(									);
	void						AddRef				(									);
	uint						GetRefCpt			(									);
	void						Release				(									);
	uint32						GetRscType			(									);
	uint32						GetRscUID			(									);
	NvMesh*						GetInterface		(									);

	Box3*						GetBBox				(									);
	uint32						GetNameCRC			(									);
	bool						IsSkinnable			(									);
	bool						IsNrmSkinnable		(									);
	bool						IsMorphable			(									);
	bool						IsLightable			(									);
	bool						IsShadowing			(									);
	bool						IsFullOpaque		(									);

	uint						GetSurfaceCpt		(									);
	bool						GetSurfaceShading	(	Shading&		outShading,
														uint			inSurfaceNo		);
	void						RefBitmaps			(									);
	void						UnrefBitmaps		(									);

	uint						GetBunchCpt			(									);
	uint						GetBunchDataCpt		(									);
	Bunch*						GetBunchA			(									);
	Box3*						GetBunchBBoxA		(									);
	BunchData*					GetBunchDataA		(									);
	BunchList*					GetBunchByBunchA	(									);
	BunchList*					GetBunchBySurfA		(									);
	BunchHrdwData *				GetHrdwData			(									);

	bool						GetShadowing		(	uint&				outVtxCpt,
														uint&				outFaceCpt,
														ShadowingVtx*&		outVtxA,
														ShadowingFace*&		outFaceA	);

	void						UpdateBeforeDraw	(									);
};



#endif // _NvkMesh_H_



