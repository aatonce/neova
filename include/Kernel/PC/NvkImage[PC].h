/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef	_NvkImage_H_
#define	_NvkImage_H_


#include "NvHrdw.h"
#include <NvImage.h>
#include <Kernel/PC/NvHrdw.h>




interface NvkImage : public NvResource
{
	NVKITF_DECL( Image )

	NvInterface *			GetBase				(								);
	void					AddRef				(								);
	uint					GetRefCpt			(								);
	void					Release				(								);
	uint32					GetRscType			(								);
	uint32					GetRscUID			(								);
	NvImage*				GetInterface		(								);

	NvBitmap*				GetBitmap			(								);

	uint32					GetWidth			(								);
	uint32					GetHeight			(								);
	NvBitmap::AlphaStatus	GetAlphaStatus		(								);

	Hrdw::Texture			GetTexture			(								);
	int						GetTextureClutIdx	(								);

	void					UpdateBeforeDraw	(								);

	int						UpdateJpeg			(	byte*		inJpegAddr,
													uint		inJpegBSize		);
};




#endif // _NvkImage_H_


