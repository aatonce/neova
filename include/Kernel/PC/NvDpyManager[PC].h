/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvDpyManager_PC_H_
#define	_NvDpyManager_PC_H_


#include "NvHrdw.h"
#include <NvDpyManager.h>
#include <Kernel/Common/NvDpyContext.h>
interface NvkBitmap;




namespace DpyManager
{
	extern NvDpyContext*		context;
	extern int					hwraster[DPYR_OFF_FRAME_C32Z32+1];
	extern Matrix				clipMatrix;

	bool						IsFullscreen		(										);

	// Windowed mode only !
	int							CreateTargetHwnd	(	HWND				inHwnd			);
	bool						ActiveTargetHwnd	(	HWND				inHwnd			);
	bool						ResizeTargetHwnd	(	HWND				inHwnd			);
	void						CheckTargetHwnd		(										);
	bool						GetTargetHwndInfo	(	HWND				inHwnd,
														int*				outHwRasterId,
														int*				outHwTargetId	);

	uint						AllocDevPalette		(										);
	void						ReleaseDevPalette	(	uint				inIdx			);

	// Garbager for safe release of frame-dependant objects
	void						AddToGarbager		(	NvInterface*		inITF			);
	void						FlushGarbager		(										);

	
}




#endif	// _NvDpyManager_PC_H_


