/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#ifndef _NvHrdw_H_
#define _NvHrdw_H_



#define DIRECTINPUT_VERSION     0x0800


#if defined(_NVENGINE) && defined(new)
	#undef		new
	#undef		delete
	#include	"NvWindows.h"
	#include	<dinput.h>
	#include	<dsound.h>
	#include	<d3d9.h>
	#include	<d3dx9.h>
	#undef		new
	#undef		delete
	#define		new					$illegal new operator in engine part$
	#define		delete				$illegal delete operator in engine part$
#else
	#include	"NvWindows.h"
	#include	<dinput.h>
	#include	<dsound.h>
	#include	<d3d9.h>
	#include	<d3dx9.h>
#endif

#include <Nova.h>

#include <NvCore_Types.h>
#include <NvDpyManager.h>


// D3D
#define	NV_D3D_CREATE				Direct3DCreate9
#define	NV_D3D						LPDIRECT3D9
#define	NV_D3DDEVICE				PDIRECT3DDEVICE9
#define	NV_D3DCAPS					D3DCAPS9
#define	NV_D3DPRESENT_PARAMETERS	D3DPRESENT_PARAMETERS
#define	NV_D3DDISPLAY_MODE			D3DDISPLAYMODE
#define	NV_D3DDEVTYPE				D3DDEVTYPE
#define NV_D3DVIEWPORT				D3DVIEWPORT9
#define	NV_D3DVERTEXELEMENT			D3DVERTEXELEMENT9
#define	NV_D3DTEXTURE				PDIRECT3DTEXTURE9
#define	NV_D3DSURFACE				PDIRECT3DSURFACE9
#define	NV_D3DSURFACEDESC			D3DSURFACE_DESC
#define	NV_D3DSWAPCHAIN				PDIRECT3DSWAPCHAIN9
#define	NV_D3DVERTEXBUFFER			PDIRECT3DVERTEXBUFFER9
#define	NV_D3DINDEXBUFFER			PDIRECT3DINDEXBUFFER9
#define	NV_D3DVERTEXDECL			LPDIRECT3DVERTEXDECLARATION9
#define	NV_D3DVERTEXSHADER			PDIRECT3DVERTEXSHADER9
#define NV_D3DFORMAT				D3DFORMAT
#define NV_D3DRESOURCETYPE			D3DRESOURCETYPE
#define NV_D3DMULTISAMPLE_TYPE		D3DMULTISAMPLE_TYPE


// DS
#define		NV_DS_CREATE		DirectSoundCreate8 //(LPCGUID lpcGuidDevice,LPDIRECTSOUND8 * ppDS8,LPUNKNOWN pUnkOuter);
#define		NV_DSOUND			LPDIRECTSOUND8
#define		NV_DSCAPS			DSCAPS
#define		NV_DSBUFFERDESC		DSBUFFERDESC
#define		NV_DSSOUNDBUFFER	LPDIRECTSOUNDBUFFER
#define		NV_DS3DSOUNDBUFFER	LPDIRECTSOUND3DBUFFER8
#define		NV_IDS3DSOUNDBUFFER	IID_IDirectSound3DBuffer8


// DI
#define	NV_DINPUT_CREATE			DirectInput8Create
#define	NV_DINPUT_REFIID			IID_IDirectInput8
#define	NV_DINPUT					LPDIRECTINPUT8
#define	NV_DINPUTDEVICE				LPDIRECTINPUTDEVICE8
#define	NV_DIDEVCAPS				DIDEVCAPS


#define HRDW_COLOR_ARGB(a,r,g,b) (((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff)))
#define HRDW_COLOR_RGBA(r,g,b,a) HRDW_COLOR_ARGB(a,r,g,b)
#define	HRDW_VS_VERSION(M,m)	 ((M)*100+(m))

namespace Hrdw
{	
//-----------------------------------------------------------------------------------------
//								Special Data
//-----------------------------------------------------------------------------------------	
	enum SD_Shader{
		SD_NoVertexShader = 0,
		SD_NoPixelShader  = 1,
	};
	enum SD_Stream{
		SD_DummyStream = 1,
	};
	
//-----------------------------------------------------------------------------------------
//								Type 
//-----------------------------------------------------------------------------------------	
	typedef uint16	IndexBuffer;
	typedef uint16	VertexBuffer;
	
	typedef uint16  ShaderProgram;
	
	typedef uint16	DeclIndex;

	typedef uint16 	Texture;			
//-----------------------------------------------------------------------------------------
//								Enumeration 
//-----------------------------------------------------------------------------------------	

	enum TextureCapabilities{
		TEX_SQUAREONLY = 0,
		TEX_POW2
	};
	
	enum DriverCapabilities{
		DRV_AUTOGENMIPMAP = 0
	};
	
	enum ClearTarget {
		NV_CLEAR_STENCIL = 1, 
		NV_CLEAR_TARGET  = 2, 
		NV_CLEAR_ZBUFFER = 4
	};
	
	enum DeviceState {
		NV_DEVICE_NULL,
		NV_DEVICE_LOST,
		NV_DEVICE_OK,
		NV_DEVICE_NEED_RESET,
	};
//----------------------------------------------------------------------------------------
	enum {
		VS_NRM_STREAM		= (1<<0),
		VS_TEX_STREAM		= (1<<1),
		VS_COL_STREAM		= (1<<2),
	};
//----------------------------------------------------------------------------------------	
	/*enum Pool{
		NV_DEFAULT 			= 0	,
		NV_MANAGED			= 1	,
		NV_SYSTEM			= 2	,
		NV_DYNAMIC_DEFAULT	= 3	,
	};*/

	enum Usage {
		NV_STATIC			= 0,
		NV_DYNAMIC			= 1,
	};

	enum LockFlags{
		NV_DISCARD 			= 1,
		NV_NO_DIRTY_UPDATE 	= 2,
		NV_NOSYSLOCK 		= 4,
		NV_READONLY 		= 8,
		NV_NOOVERWRITE 		= 16
	};
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//							Stream Enumeration
/*	enum StreamUsage{
		NV_DONOTCLIP 	= 1,
		NV_DYNAMIC 		= 2,
		NV_NPATCHES  	= 4,
		NV_POINTS   	= 8,
		NV_RTPATCHES 	=16,
		NV_WRITEONLY 	=32	
	};	
*/
	enum IndexFormat{
		NV_INDEX16 		= 0,
		NV_INDEX32 				
	};	 	
	enum PrimitiveType{
	    NV_POINTLIST 		=0,
	    NV_LINELIST 		,
	    NV_LINESTRIP 		,
	    NV_TRIANGLELIST	 	,
	    NV_TRIANGLESTRIP 	,
	    NV_TRIANGLEFAN 
	};	
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//							Shader Enumeration
	enum ShaderType{
		NV_VERTEXSHADER = 1,
		NV_PIXELSHADER  = 2,
	  //NV_GEOMSHADER	= 4, //DirectX10 !!
		NV_ASM			= 8,
		NV_CG			= 16,
		NV_HLSL			= 32,
		NV_UNDEF		= 64
	};

	enum ShaderProfile{
		NV_VS_1_1 = 0,
		NV_VS_2,
		NV_VS_2_a,
		NV_VS_3,
		NV_PS_1_1,
		NV_PS_1_4,
		NV_PS_2,
		NV_PS_2_a,
		NV_PS_2_b,
		NV_PS_3,
		NV_NONE
	};
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//							Declaration Enumeration
	enum ElementType {
		NV_FLOAT1 = 0,
	    NV_FLOAT2 ,
	    NV_FLOAT3 ,
	    NV_FLOAT4 ,
	    NV_COLORT ,
	    NV_UBYTE4 ,
	    NV_SHORT2 ,
	    NV_SHORT4 ,
	    NV_UBYTE4N ,
	    NV_SHORT2N ,
	    NV_SHORT4N ,
	    NV_USHORT2N ,
	    NV_USHORT4N ,
	    NV_UDEC3 ,
	    NV_DEC3N ,
	    NV_FLOAT16_2 ,
	    NV_FLOAT16_4 ,
	    NV_UNUSED 
	};
	 
	enum ElementUsage{
		NV_POSITION = 0,
	    NV_BLENDWEIGHT ,
	    NV_BLENDINDICES,
	    NV_NORMAL ,
	    NV_PSIZE ,
	    NV_TEXCOORD ,
	    NV_TANGENT,
	    NV_BINORMAL ,
	    NV_TESSFACTOR,
	    NV_POSITIONT,
	    NV_COLOR ,
	    NV_FOG ,
	    NV_DEPTH ,
	    NV_SAMPLE,
	};
										    
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//							Texture Enumeration
/*
	enum TextureUsage{
		NV_TEX_DYNAMIC 		= 1,
		NV_TEX_WRITEONLY 	= 2	
	};	
*/
	enum MipmapLevel {
		NV_MipMapMax 		= 0,
		NV_MipMapLevel1  	= 1,
		NV_MipMapLevel2   	= 2,
		NV_MipMapLevel4   	= 4,
		NV_MipMapLevel8  	= 8,		
		NV_MipMapLevel16  	= 16				
	};

	enum TextureFormat {
		NV_A8R8G8B8			= 0,
		NV_A8B8G8R8			,
		NV_P8 				,		
		NV_A16B16G16R16F 	,
		NV_A8 				,
		NV_A8P8 			,
		NV_R5G6B5
	};
	
	#define NO_TEXTURE 0
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//							State Enumeration
	//Render State
	enum RenderState
	{
	    NV_ZENABLE					=0,
	    NV_FILLMODE					,
	    NV_SHADEMODE				,
	    NV_ZWRITEENABLE				,
	    NV_ALPHATESTENABLE			,
	    NV_LASTPIXEL				,
	    NV_SRCBLEND					,
	    NV_DESTBLEND				,
	    NV_CULLMODE					,
	    NV_ZFUNC					,
	    NV_ALPHAREF					,
	    NV_ALPHAFUNC				,
	    NV_DITHERENABLE				,
	    NV_ALPHABLENDENABLE			,
	    NV_FOGENABLE				,
	    NV_SPECULARENABLE			,
	    NV_FOGCOLOR					,
	    NV_FOGTABLEMODE				,
	    NV_FOGSTART					,
	    NV_FOGEN					,
	    NV_FOGDENSITY				,
	    NV_RANGEFOGENABLE			,
	    NV_STENCILENABLE			,
	    NV_STENCILFAIL				,
	    NV_STENCILZFAIL				,
	    NV_STENCILPASS				,
	    NV_STENCILFUNC				,
	    NV_STENCILREF				,
	    NV_STENCILMASK				,
	    NV_STENCILWRITEMASK			,
	    NV_TEXTUREFACTOR			,
	    NV_WRAP0					,
	    NV_WRAP1					,
	    NV_WRAP2					,
	    NV_WRAP3					,
	    NV_WRAP4					,
	    NV_WRAP5					,
	    NV_WRAP6					,
	    NV_WRAP7					,
	    NV_CLIPPING					,
	    NV_LIGHTING					,
	    NV_AMBIENT					,
	    NV_FOGVERTEXMODE			,
	    NV_COLORVERTEX				,
	    NV_LOCALVIEWER				,
	    NV_NORMALIZENORMALS			,
	    NV_DIFFUSEMATERIALSOURCE	,
	    NV_SPECULARMATERIALSOURCE	,
	    NV_AMBIENTMATERIALSOURCE	,
	    NV_EMISSIVEMATERIALSOURCE	,
	    NV_VERTEXBLEND				,
	    NV_CLIPPLANEENABLE			,
	    NV_POINTSIZE				,
	    NV_POINTSIZE_MIN			,
	    NV_POINTSPRITEENABLE		,
	    NV_POINTSCALEENABLE			,
	    NV_POINTSCALE_A				,
	    NV_POINTSCALE_B				,
	    NV_POINTSCALE_C				,
	    NV_MULTISAMPLEANTIALIAS		,
	    NV_MULTISAMPLEMASK			,
	    NV_PATCHEDGESTYLE			,
	    NV_DEBUGMONITORTOKEN		,
	    NV_POINTSIZE_MAX			,
	    NV_INDEXEDVERTEXBLENDENABLE	,
	    NV_COLORWRITEENABLE			,
	    NV_TWEENFACTOR				,
	    NV_BLENDOP					,
	    NV_POSITIONDEGREE			,
	    NV_NORMALDEGREE				,
	    NV_SCISSORTESTENABLE		,
	    NV_SLOPESCALEDEPTHBIAS		,
	    NV_ANTIALIASEDLINEENABLE	,
	    NV_MINTESSELLATIONLEVEL		,
	    NV_MAXTESSELLATIONLEVEL		,
	    NV_ADAPTIVETESS_X			,
	    NV_ADAPTIVETESS_Y			,
	    NV_ADAPTIVETESS_Z			,
	    NV_ADAPTIVETESS_W			,
	    NV_ENABLEADAPTIVETESSELLATION,
	    NV_TWOSIDEDSTENCILMODE		,
	    NV_CCW_STENCILFAIL			,
	    NV_CCW_STENCILZFAIL			,
	    NV_CCW_STENCILPASS			,
	    NV_CCW_STENCILFUNC			,
	    NV_COLORWRITEENABLE1		,
	    NV_COLORWRITEENABLE2		,
	    NV_COLORWRITEENABLE3		,
	    NV_BLENDFACTOR				,
	    NV_SRGBWRITEENABLE			,
	    NV_DEPTHBIAS				,
	    NV_WRAP8					,
	    NV_WRAP9					,
	    NV_WRAP10					,
	    NV_WRAP11					,
	    NV_WRAP12					,
	    NV_WRAP13					,
	    NV_WRAP14					,
	    NV_WRAP15					,
	    NV_SEPARATEALPHABLENDENABLE	,
	    NV_SRCBLENDALPHA			,
	    NV_DESTBLENDALPHA			,
	    NV_BLENDOPALPHA				
	} ;
	
	enum ZBufferType
	{
	    NV_ZB_FALSE	=0,
	    NV_ZB_TRUE	,
	    NV_ZB_USEW	
	};
	
	enum FillMode
	{
	    NV_FILL_POINT		=0,
	    NV_FILL_WIREFRAME	,
	    NV_FILL_SOLID		
	};
	
	enum ShadeMode
	{
	    NV_SHADE_FLAT		= 0,
	    NV_SHADE_GOURAUD	,
	    NV_SHADE_PHONG		
	};

	enum Cull
	{
	    NV_CULL_NONE	=0,
	    NV_CULL_CW		,
	    NV_CULL_CCW		
	};
							    
	enum Blend
	{
	    NV_BLEND_ZERO			=0,
	    NV_BLEND_ONE			,
	    NV_BLEND_SRCCOLOR		,
	    NV_BLEND_INVSRCCOLOR	,
	    NV_BLEND_SRCALPHA		,
	    NV_BLEND_INVSRCALPHA	,
	    NV_BLEND_DESTALPHA		,
	    NV_BLEND_INVDESTALPHA	,
	    NV_BLEND_DESTCOLOR		,
	    NV_BLEND_INVDESTCOLOR	,
	    NV_BLEND_SRCALPHASAT	,
	    NV_BLEND_BOTHSRCALPHA	,
	    NV_BLEND_BOTHINVSRCALPHA,
	    NV_BLEND_BLENDFACTOR	,
	    NV_BLEND_INVBLENDFACTOR	
	};
	
	enum CMPFunc
	{
	    NV_CMP_NEVER		=0,
	    NV_CMP_LESS			,
	    NV_CMP_EQUAL		,
	    NV_CMP_LESSEQUAL	,
	    NV_CMP_GREATER		,
	    NV_CMP_NOTEQUAL		,
	    NV_CMP_GREATEREQUAL	,
	    NV_CMP_ALWAYS		
	};
	
	enum FogMode
	{
	    NV_FOG_NONE= 0,
	    NV_FOG_EXP,
	    NV_FOG_EXP2,
	    NV_FOG_LINEAR
	};
	
	enum StencilOP
	{
	    NV_STENCILOP_KEEP = 0,
	    NV_STENCILOP_ZERO,
	    NV_STENCILOP_REPLACE,
	    NV_STENCILOP_INCRSAT,
	    NV_STENCILOP_DECRSAT,
	    NV_STENCILOP_INVERT,
	    NV_STENCILOP_INCR,
	    NV_STENCILOP_DECR
	};
	
	enum MaterialColorSource
	{
	    NV_MCS_MATERIAL = 0,
	    NV_MCS_COLOR1,
	    NV_MCS_COLOR2,
	};
	    										
	enum VertexBlendFlags
	{
	    NV_VBF_DISABLE = 0,
	    NV_VBF_1WEIGHTS,
	    NV_VBF_2WEIGHTS,
	    NV_VBF_3WEIGHTS,
	    NV_VBF_TWEENING,
	    NV_VBF_0WEIGHTS,
	};
	
	enum PatchedGEStyle
	{
	    NV_PATCHEDGE_DISCRETE = 0,
	    NV_PATCHEDGE_CONTINUOUS
	};
	
	enum DebugMonitorTokens
	{
	    NV_DMT_ENABLE = 0,
	    NV_DMT_DISABLE
	};
	
	enum BlendOP
	{
	    NV_BLENDOP_ADD = 0,
	    NV_BLENDOP_SUBTRACT,
	    NV_BLENDOP_REVSUBTRACT,
	    NV_BLENDOP_MIN,
	    NV_BLENDOP_MAX
	};
	
	enum DegreeType
	{
	    NV_DEGREE_LINEAR = 0,
	    NV_DEGREE_QUADRATIC,
	    NV_DEGREE_CUBIC,
	    NV_DEGREE_QUINTIC
	};	
	//texture State
	enum TextureStageStateType
	{
	    NV_COLOROP = 0,
	    NV_COLORARG1,
	    NV_COLORARG2,
	    NV_ALPHAOP,
	    NV_ALPHAARG1,
	    NV_ALPHAARG2,
	    NV_BUMPENVMAT00,
	    NV_BUMPENVMAT01,
	    NV_BUMPENVMAT10,
	    NV_BUMPENVMAT11,
	    NV_TEXCOORDINDEX ,
	    NV_BUMPENVLSCALE ,
	    NV_BUMPENVLOFFSET,
	    NV_TEXTURETRANSFORMFLAGS,
	    NV_COLORARG0,
	    NV_ALPHAARG0,
	    NV_RESULTARG,
	    NV_CONSTANT,
	} ;
	
	enum TextureOP
	{
	    NV_TOP_DISABLE = 0,
	    NV_TOP_SELECTARG1,
	    NV_TOP_SELECTARG2,
	    NV_TOP_MODULATE,
	    NV_TOP_MODULATE2X,
	    NV_TOP_MODULATE4X,
	    NV_TOP_ADD,
	    NV_TOP_ADDSIGNED,
	    NV_TOP_ADDSIGNED2X,
	    NV_TOP_SUBTRACT,
	    NV_TOP_ADDSMOOTH,
	    NV_TOP_BLENDDIFFUSEALPHA,
	    NV_TOP_BLENDTEXTUREALPHA,
	    NV_TOP_BLENDFACTORALPHA,
	    NV_TOP_BLENDTEXTUREALPHAPM,
	    NV_TOP_BLENDCURRENTALPHA,
	    NV_TOP_PREMODULATE,
	    NV_TOP_MODULATEALPHA_ADDCOLOR,
	    NV_TOP_MODULATECOLOR_ADDALPHA,
	    NV_TOP_MODULATEINVALPHA_ADDCOLOR,
	    NV_TOP_MODULATEINVCOLOR_ADDALPHA,
	    NV_TOP_BUMPENVMAP,
	    NV_TOP_BUMPENVMAPLUMINANCE,
	    NV_TOP_DOTPRODUCT3,
	    NV_TOP_MULTIPLYADD,
	    NV_TOP_LERP
	};

	enum TextureArgument { // combine flags with Midifier Flags
		NV_TA_CONSTANT  		=   1,
		NV_TA_CURRENT   		=   2,
		NV_TA_DIFFUSE   		=   4 ,
		NV_TA_SELECTMASK		=   8 ,
		NV_TA_SPECULAR  		=  16,
		NV_TA_TEMP 				=  32,
		NV_TA_TEXTURE   		=  64,
		NV_TA_TFACTOR   		= 128,
		// Modifier flags
		NV_TA_ALPHAREPLICATE	= 256 ,
		NV_TA_COMPLEMENT 		= 512
	};	

	enum TextureCoordinate {	
		NV_TSS_TCI_PASSTHRU = 0,
		NV_TSS_TCI_CAMERASPACENORMAL ,
		NV_TSS_TCI_CAMERASPACEPOSITION ,
		NV_TSS_TCI_CAMERASPACEREFLECTIONVECTOR ,
		NV_TSS_TCI_SPHEREMAP 
	};		
	
	enum TextureTransformation 
	{
	    NV_TTFF_DISABLE = 0,
	    NV_TTFF_COUNT1,
	    NV_TTFF_COUNT2,
	    NV_TTFF_COUNT3,
	    NV_TTFF_COUNT4,
	    NV_TTFF_PROJECTED
	};
	//Sampler State
	
	enum SamplerStageType
	{
	    NV_ADDRESSU,
	    NV_ADDRESSV,
	    NV_ADDRESSW,
	    NV_BORDERCOLOR,
	    NV_MAGFILTER,
	    NV_MINFILTER,
	    NV_MIPFILTER,
	    NV_MIPMAPLODBIAS,
	    NV_MAXMIPLEVEL,
	    NV_MAXANISOTROPY,
	    NV_SRGBTEXTURE,
	    NV_ELEMENTINDEX,
	    NV_DMAPOFFSET
	};
	
	enum TextureAddress
	{
	    NV_TADDRESS_WRAP,
	    NV_TADDRESS_MIRROR,
	    NV_TADDRESS_CLAMP,
	    NV_TADDRESS_BORDER,
	    NV_TADDRESS_MIRRORONCE
	};
	
	enum TextureFilter
	{
	    NV_TEXF_NONE,
	    NV_TEXF_POINT,
	    NV_TEXF_LINEAR,
	    NV_TEXF_ANISOTROPIC,
	    NV_TEXF_PYRAMIDALQUAD,
	    NV_TEXF_GAUSSIANQUAD
	} ;	

	enum WriteColor {
		NV_COLORWRITEENABLE_ALPHA = 1, 
		NV_COLORWRITEENABLE_BLUE  = 2, 
		NV_COLORWRITEENABLE_GREEN = 4,
		NV_COLORWRITEENABLE_RED	  = 8,
	};

/***********************************************************************************************/
//					Hardware capabilities / required

	enum RunningMode
	{
		RM_Unknown	=  0,
		RM_None		=  1,
		RM_TNL		=  2,
		RM_VtxNL	=  3,
		RM_Limited	=  4,
		RM_Full		=  5,
	};

	enum
	{
		REQ_HwStreams				= 4,		// Be aware that #streams is fixed to 16 in software-vtx-proc !
		REQ_HwPixelShaderVersion	= 20,		// Major*10 + Minor
		REQ_SimultaneousTextures	= 2,
		REQ_SimultaneousRT			= 2,
		REQ_HwVertexShaderVersion	= 20,		// Major*10 + Minor
	};

//-----------------------------------------------------------------------------------------
//								functions declaration
//-----------------------------------------------------------------------------------------	


	uint				GetNbScreen				(											);
	bool				ProbeFullscreen			(	uint			inAdapter,
													uint			inWidth,
													uint			inHeight				);
	bool				FindBestFullscreen		(	uint			inAdapter,
													uint			inWidth,
													uint			inHeight,
													uint&			outBestWidth,
													uint&			outBestHeight			);

	RunningMode			GetBestRunningMode		(	uint			inAdapter				);
	PCSTR				GetRunningModeName		(	RunningMode		inRM					);

	bool 				Init					(	uint			inAdapter,
													HWND			inFocusWindow,
													bool			inFullscreen,
													uint			inFullscreenWidth,
													uint			inFullscreenHeight,
													bool			inSoftVProcessing		);
	bool 				Shut					(											);

	bool				ResetDevice				(											);


	bool				IsFullscreen			(											);
	uint				GetFullscreenWidth		(											);
	uint				GetFullscreenHeight		(											);

	uint				GetVSyncRate			(										);
	int					GetMaxTextureWidth		(										);
    int 				GetMaxTextureHeight		(										);
    bool 				GetTextureCapability	(	TextureCapabilities inTexCaps		);
    bool 				GetDriverCapability		(	DriverCapabilities  inDrvCaps		);


	// Rasters

	int					CreateRaster			(	uint			inW,									
													uint			inH,
													uint			inColorBPP,
													uint			inDepthBPP		= 0,
													bool			inWithDephTex	= FALSE		);
	bool				FreeRaster				(	int				inId				);
	uint				GetRasterHeight			(	int				inId				);
	uint				GetRasterWidth			(	int				inId				);
	void				GetRasterFormat			(	int				inId				,
													uint *			oColorBPP			,
													uint *			oDepthBPP			,
													uint *			oStencilBPP			);
	bool				ResizeRaster			(	int				inId,
													uint			inWidth,
													uint			inHeight			);
	bool				ReadRaster				(	pvoid			outBuffer,
													int				inId				);
	bool				WriteRaster				(	pvoid			inBuffer,
													int				inId				);


	// Windowed target

	int					CreateTarget			(	HWND			inHwnd,
													uint			inWidth,
													uint			inHeight			);
	bool				FreeTarget				(	int				inId				);
	bool				ResizeTarget			(	int				inId,
													uint			inWidth,
													uint			inHeight			);
	uint				GetTargetHeight			(	int				inId				);
	uint				GetTargetWidth			(	int				inId				);

	bool				BeginScene				(										);
	bool				EndScene				(										);    
	void				ResetStates				(										);	

	bool				CopyBackToFront			(										);

	bool				SetSource				(	int					inId,				
													uint				inSamplerNum,
													bool				inUseDepth	= FALSE	);

	bool 				SetRaster				(	int					inRasterId,
													bool				inSetRTZ	= TRUE ,		// set a Render target to write Z
													bool				inSetOnlyZ	= FALSE	);		// Set only ZRT => index 0

	void				ClearRaster				(	uint				flags,
													uint32				argb, 
													float				z				);

	bool				SetTarget				(	int					inId			);

	void 				SetViewPort				(	int					X, 
													int 				Y, 
													int 				Width,
													int					Height, 
													float				MinZ,
													float				MaxZ			);

	bool				CheckDeviceFormatForMipMap	(TextureFormat		CheckFormat		);
	bool				CheckDeviceFormatForDynamic	(TextureFormat		CheckFormat		);											    	

													
	void				UpdateDeviceState		(										);
	DeviceState			GetDeviceState			(										);
	

/***********************************************************************************************/
//					Streams
	
	VertexBuffer 		CreateVertexBuffer		( uint 					inSize,
												  Usage		 			inUsage = NV_STATIC	);
											 
	IndexBuffer			CreateIndexBuffer		( uint 					inSize,
												  IndexFormat			inFormat,
												  Usage		 			inUsage = NV_STATIC		);
												 
	void 				ReleaseVertexBuffer     ( VertexBuffer 			inVB 					);
	void 				ReleaseIndexBuffer 		( IndexBuffer 			inIB					);
	 													 
	void * 				LockVertexBuffer		( VertexBuffer 			inVB,
												  uint 					inOffset,
												  uint 					inSize,
												  int					inLockFlag/*LockFlags*/	);
												 
	void 				UnlockVertexBuffer		( VertexBuffer 			inVB					);
		
	void *				LockIndexBuffer			( IndexBuffer 			inIB,
												  uint 					inOffset,
												  uint 					inSize,
												  int					inLockFlag/*LockFlags*/	);
												 
	void 				UnlockIndexBuffer		( IndexBuffer 			inIB					);
	

	bool 				SetVertexBuffer			( VertexBuffer 			inVB,
												  uint					inStreamNumber,
												  uint					inOffet,
												  uint					inStride				);
												 
	bool				SetIndexBuffer			( IndexBuffer 			inIB);

	
	void 				DrawPrimitive			( PrimitiveType 		inType,
												  uint			 		inNbPrimitiveToRender,
												  uint					inIndexOffset,	/* The fisrt index		*/
												  uint					inNbVertexUsed,	/*	Nb Vertex in the continuous zone adressed by the index [minIndex,maxIndex]*/
												  uint					inVertexOffset = 0	/* Addind to each index */ );
												 
	void 				DrawPrimitive			( PrimitiveType 		inType,
												  uint			 		inNbPrimitiveToRender,
												  uint					inVertexOffset		);

	bool				IBValidData				(	IndexBuffer			inIB				);
	bool				VBValidData				(	VertexBuffer		inVB				);
	void				IBSetValidData			(	IndexBuffer			inIB				); //	XSetValidData(ind) => ( XValidData(ind) = TRUE )
	void				VBSetValidData			(	VertexBuffer		inVB				);

/***********************************************************************************************/
//					Shader
	

	ShaderProgram		CreateShaderProgramFromFile		(uint				inType,
														 char * 			inStrPath,
														 ShaderProfile	 	inVersion,														 
														 char * 			inEntryPointName			= "main",
														 const char *		inDefines					= NULL, // Syntax : name1=def1 ; name2=def2 ...
														 bool				inCreateReservedRegister	= false);

	ShaderProgram		CreateShaderProgram				(uint				inType,
														 char * 			inStr,
														 uint 				inSize,
														 ShaderProfile 		inVersion,														 
														 char * 			inEntryPointName			= "main",
														 const char *		inDefines					= NULL, // Syntax : name1=def1 ; name2=def2 ...
														 bool				inCreateReservedRegister	= false);

	void				ReleaseShader					(ShaderProgram 		inVS					);

	bool 				SetShaderProgram				(ShaderProgram 		inVS					);

	bool 				SetVertexShaderConstantB		(uint				inRegister,
												 	 	 bool * 			inData,
													 	 uint 				inNbVect				);	

	bool 				SetVertexShaderConstantF		(uint				inRegister,
													  	 float * 			inData,
													 	 uint 				inNbVect 				);	

	bool 				SetVertexShaderConstantI		(uint				inRegister,
													 	 int * 				inData,
													  	 uint 				inNbVect 				);

	bool 				SetPixelShaderConstantB			(uint				inRegister,
												 	 	 bool * 			inData,
													 	 uint 				inNbVect				);	

	bool 				SetPixelShaderConstantF			(uint				inRegister,
													  	 float * 			inData,
													 	 uint 				inNbVect 				);	

	bool 				SetPixelShaderConstantI			(uint				inRegister,
													 	 int * 				inData,
													  	 uint 				inNbVect 				);

	void 				InternalUpdateVertexMapping		(uint16				inFirstRegister	,													  	
														 uint16 			inSize,
														 void * 			inData					);

	void 				InternalUpdatePixelMapping		(uint16				inFirstRegister	,													  	
														 uint16 			inSize,
														 void * 			inData					);


	template <typename T>
	void 				FullUpdateVertexMapping			(T * 				inMapping				);

	template <typename T>
	void 				UpdateVertexMapping				(T * 				inMapping,
														 void	*			inFirstVar,				 // the beginning of the updated data
														 uint16				inSize					); // update inSize Vector
														 
	template <typename T>
	void 				UpdateVertexMapping				(T * 				inMapping,
														 void	*			inFirstVar				);  // the beginning of the updated data														 
	
	template <typename T>
	void 				FullUpdatePixelMapping			(T * 				inMapping				);

	template <typename T>
	void 				UpdatePixelMapping				(T * 				inMapping,
														 void	*			inFirstVar,				// the beginning of the updated data
														 uint16				inSize					); // update inSize Vector
														 
	template <typename T>
	void 				UpdatePixelMapping				(T * 				inMapping,
														 void	*			inFirstVar				);  // the beginning of the updated data
														 


									 				  	 									 				 
	bool				InternalCheckMapping			(ShaderProgram inVS,
														 uint16			inRegister,
							 							 uint16			inSize,
														 char * 		inName						);

	#define Hrdw_CheckMappingByName( inShaderDecl, inType, inMember, inVarName )	\
				Hrdw::InternalCheckMapping (inShaderDecl,MOFFSET(inType,inMember) / 16, MSIZE(inType,inMember)/ 4,inVarName)

	#define Hrdw_CheckMapping( inShaderDecl, inType, inMember)				\
				Hrdw_CheckMappingByName( inShaderDecl, inType, inMember, ##inMember )

/***********************************************************************************************/
//					Declaration

	
	
	void 				BeginVertexDeclaration			(										);

	void 				AddToVertexDeclaration			(uint8				Stream,
														 uint8				Offset,
														 ElementType		Type,			
														 ElementUsage		Usage,
														 uint8				Index = 0		);

	DeclIndex		 	FinalizeVertexDeclaration		(										);
	
	bool 				SetVertexDeclaration			(DeclIndex 		inDecl			);


/***********************************************************************************************/
//					Texture

	Texture 				CreateTexture		(uint 				inWidth,
												 uint				inHeight,
												 MipmapLevel		inLevel,			// inLevel != NVMipMapLevel1 => AUTOGENMIPMAPP 
												 TextureFormat 		inFormat,
												 Usage		 		inUsage = NV_STATIC		);		// Tex_LockFlags
												 
	void 					ReleaseTexture		(Texture 			inTex					);
	
	void 					SetTexture			(Texture 			inTex,
												 uint				inSampler				);
	
	void *					LockTexture			(Texture 			inTex,				// Level 1 is lock
												 uint				inLockFlag, 		/*LockFlags*/
												 int &				outPitch,
												 uint 				inLeft = 0,
												 uint				inTop = 0,
												 uint 				inRight = 0,
												 uint				inBottom = 0			);
	
	void 					UnlockTexture		(Texture 			inTex,
												 bool 				genMipmapNow = false	);

	int 					GetMipmapLevel		(Texture 			inTex					);
	
	void 					SetPaletteEntries	(uint				inPaletteNum,
												 uint32 *			inEntry					);
												 
	void 					SetCurrentPalette	(uint				inPaletteNum			); 
	
	bool					TexValidData		(Texture 			inTex					);
	void					TexSetValidData		(Texture 			inTex					); //	TexSetValidData(ind) => ( TexValidData(ind) = TRUE )

/***********************************************************************************************/
//					RenderState	
	
	void 					SetRenderState		(RenderState 			inRS ,
												 uint 					inValue					);
	
	void			 		SetTextureStageState(int 					inStage,
												 TextureStageStateType 	inType,
												 uint					inValue					);
												 

	void			 		SetSamplerState		(int 					inSampler,
												 SamplerStageType	 	inType,
												 uint					inValue					);


}// namespace Hrdw


template <typename T>
void 				
Hrdw::FullUpdateVertexMapping (T * inMapping	)
{
	InternalUpdateVertexMapping(0, sizeof(T)/16,inMapping);
}

template <typename T>
void
Hrdw::UpdateVertexMapping	(T * 			inMapping,
							void	*		inFirstVar	)
{
	uint boffset = uint(inFirstVar) - uint(inMapping) ;
	InternalUpdateVertexMapping(   boffset >> 4, 
								  (sizeof(T) - boffset) >> 4 ,
								  (void*)(int(inFirstVar)- ( boffset&0xF)));
}
														 
template <typename T>
void 				
Hrdw::UpdateVertexMapping		(T * 			inMapping,
								void	*		inFirstVar,	
								uint16			inSize	)
{
	uint boffset = uint(inFirstVar) - uint(inMapping) ;
	InternalUpdateVertexMapping(   boffset >> 4, 
								  inSize ,
								  (void*)(int(inFirstVar)- ( boffset&0xF)));
}



template <typename T>
void
Hrdw::FullUpdatePixelMapping	(T * 		inMapping		)
{
	InternalUpdatePixelMapping(0, sizeof(T)/16,inMapping);	
}

template <typename T>
void
Hrdw::UpdatePixelMapping	(T * 			inMapping,
							 void	*		inFirstVar,		  
							 uint16			inSize			)
{
	uint boffset = uint(inFirstVar) - uint(inMapping) ;
	InternalUpdatePixelMapping(   boffset >> 4, 
								  inSize ,
								 (void*)(int(inFirstVar)- ( boffset&0xF)));
}
							 
template <typename T>
void
Hrdw::UpdatePixelMapping	(T * 			inMapping,
							 void	*		inFirstVar		)
{
	uint boffset = uint(inFirstVar) - uint(inMapping) ;
	InternalUpdatePixelMapping(   boffset >> 4, 
								  (sizeof(T) - boffset) >> 4 ,
								 (void*)(int(inFirstVar)- ( boffset&0xF)));
}
/*
template <typename T>
bool 
Hrdw::CheckVertexMapping(HLSLVertexShader inVS,
						 T * inMapping, 
						 void * inField,
						 uint16	inSize,
						 char * inHLSLParamName)
{
	return InternalCheckVertexMapping (inVS,  (uint(inField) - uint(inMapping)) / 16,inSize / 4,inHLSLParamName);	
}*/
/*
#define Hrdw_CheckMapping( inShaderDecl, inType, inMember, inVarName )	\
			...
#define Hrdw_CheckMapping( inShaderDecl, inType, inMember)				\
			Hrdw_CheckMapping( inShaderDecl, inType, inMember, ##inMember )

struct Map0 {
	Matrix	wtr;
	Vec4	l0;
	Vec4	l1;
};

Hrdw_CheckMapping( mys, Map0, wtr, "wtr" );

Hrdw_CheckMapping( mys, Map0, wtr )	Hrdw_CheckMapping( mys, Map0, wtr, ##wtr )
*/
/*
template <typename T>
bool
Hrdw::CheckPixelMapping	(HLSLPixelShader inPS,
						 T * 			inMapping, 
						 void * 		inField,
						 uint16	inSize,
						 char * 		inHLSLParamName	)
{
	return InternalCheckPixelMapping (inPS,(uint(inField) - uint(inMapping)) / 16,inSize / 4,inHLSLParamName);		
}
*/


#endif //_NvHrdw_H_