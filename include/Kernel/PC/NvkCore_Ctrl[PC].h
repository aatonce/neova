/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _WIN32

#ifndef	_NvkCore_Ctrl_H_
#define	_NvkCore_Ctrl_H_


namespace nv { namespace ctrl
{

	struct DevInfo
	{
		NV_DINPUTDEVICE		dev;
		NV_DIDEVCAPS		caps;
		DIDEVICEINSTANCE	inst;

		enum ProductID
		{
			SuperJoyBox3	= 0x88880925,		// SuperJoyBox3 "MP-8888 USB Joypad"
			EMS_SW2			= 0x30b43,			// EMS SW2 "4 axis 16 button manette de jeu"
			SmartJoyDeluxe	= 0x6676666,		// CCL USB SMART JOY DELUXE "4 axis 16 button manette de jeu"
			KooInteractive	= 0x10060e8f,		// USB koo Interactive (PS2Paddle)
			PS3Sixaxis		= 0x0268054c,		// USB sixaxis driver (PS3 controler)
			XBox360ctrl		= 0x028e045e,		// USB XBox 360 controller (Driver Microsoft version:6.0.5221.0 date:15/09/2005)
		};

		union
		{	
			struct{
				TCHAR			di_kb[ 256 ];		// kb data
				TCHAR			di_ascii[ 256 ];	// ascii translation data
			};
			DIMOUSESTATE2	di_ms;
			DIJOYSTATE2		di_joy;
		};

		void			Init				(		NV_DINPUTDEVICE	inDev				);
		void			Shut				(											);
		ulong			GetProductID		(											);
		bool			IsProductID			(		ulong			inProductData1		);
	};


	struct Mapper
	{
		virtual	void	Init				(											) = 0;
		virtual	bool	SetStatus			(		Status*			outStatus,
													uint			inCtrlNo,
													Type			inType,
													DevInfo*		inData				) = 0;
		virtual	void	Shut				(											) = 0;
	};


	struct Keyboard
	{
		uint32			keys [ 256/32 ];
		uint32			ascii[ 256/32 ];
	};


	struct Mouse
	{
		bool			has_xy;
		bool			has_dxy;
		int				x,	y;
		int				dx, dy;
		int				wheel[4];
		uint			buttons;
	};


	bool				SetMapper			(		Mapper*			inMapper	= NULL	);

	bool				GetKeyboard			(		uint			inCtrlNo,
													Keyboard*		outK = NULL			);

	bool				GetMouse			(		uint			inCtrlNo,
													Mouse*			outM = NULL			);

	DevInfo*			GetDevInfo			(		uint			inCtrlNo			);

} }


#endif //_NvkCore_Ctrl_H_

#endif // _WIN32


