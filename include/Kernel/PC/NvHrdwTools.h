/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#ifndef _NvHrdw_TOOLS_H_
#define _NvHrdw_TOOLS_H_

#include "NvHrdw.h"

namespace Hrdw { namespace tools {

	struct VsCopyMappingTex {
		float   offsetTexCoord[2];
		float   pad[2];
	};	

	struct VsCopyMapping {
		Vec4	color;
	};	

	void ReleaseData		(					); 

	void SetVertexBuffer	(	bool inWithTex	);
	void DrawSprite			(					);
	void SetShader			(	bool inWithTex	, 
								bool inVertexS	, 
								bool inPixelS	);

}}

#endif //_NvHrdw_TOOLS_H_