/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvDpyManager_PSP_H_
#define	_NvDpyManager_PSP_H_


#include "ALG/ALG.h"
#include <NvDpyManager.h>
#include <Kernel/Common/NvDpyContext.h>
interface NvkBitmap;
interface NvDpyObject_PSP;




namespace DpyManager
{
	extern CoordinateSystem		coordSystem;
	extern NvDpyContext*		context;

	// rasters
	struct Raster {
		uint					width;				// in pixels
		uint					height;				// in pixels
		uint32					rgbaAddr;
		uint32					depthAddr;
		// as destination
		uint32					fbp;				// CMD_FBP
		uint32					fbw;				// CMD_FBW
		uint32					fpf;				// CMD_FPF
		uint32					offsetxy[2];		// CMD_OFFSETX/Y
		uint32					zbp;				// CMD_ZBP
		uint32					zbw;				// CMD_ZBW
		uint32					zmsk;				// CMD_ZMSK
		// as source
		Vec2					tratio;				// ( with/2^TW, height/2^TH )
		uint32					tbp0;				// CMD_TBP0
		uint32					tbw0;				// CMD_TBW0
		uint32					tsize0;				// CMD_TSIZE0
		uint32					tpf;				// CMD_TPF
		uint32					tmode;				// CMD_TMODE

		Vec4					scale0[2];			// 0(2D) / 1(3D)
		Vec4					trans0[2];			// 0(2D) / 1(3D)

		inline
		void					GetViewport		(	int				inMode,		// 0(2D) / 1(3D)
													const Vec4&		inVP,
													Vec4&			outScale,
													Vec4&			outTrans	)
		{
			NV_ASSERT( inMode==0 || inMode==1 );
			outScale = scale0[inMode];
			outTrans = trans0[inMode];
			if( inMode==0 ) {
				outTrans.x += inVP.x;
				outTrans.y += inVP.y;
			} else {
				outScale.x *= (inVP.z * 0.5f);
				outScale.y *= (inVP.w * 0.5f);
				outTrans.x += (inVP.x + inVP.z * 0.5f);
				outTrans.y += (inVP.y + inVP.w * 0.5f);
			}
		}
	};

	extern Raster				raster[DPYR_OFF_FRAME_C32Z32+1];
	extern float				rasterMaxWidth;
	extern float				rasterMaxHeight;

	// Projection
	const  float				zbuffMax			=	65535.f;
	extern float				projNormalisedXY;
	extern float				ooProjNormalisedXY;
	extern Matrix				clipMatrix;

	// visible Z range is -Z [0 -> -inf]
	float						GetZProjNormalised	(	float				inZ,
														Matrix*				inProjTR			);	// returns in [-1;+1]
	uint16						GetZProjBuffValue	(	float				inZ,
														Matrix*				inProjTR			);

	// Setup GE commands
	// - raster FBP/FBW/FPF/OFFSETXY/ZBP/ZBW/ZMSK
	// - viewport SCISSOR
	// - screen SXYZ/TXYZ
	// - Transformation WORLD/VIEW/PROJ
	// Output #commands
	uint						SetupDrawing		(	dmac::Cursor*		DC,
														DpyRaster			inRaster,
														NvDpyContext::View*	inView,
														Matrix*				inWorldTR,
														bool				inThroughMode,
														bool				inEnableCalls		);

	// Native access
	bool						IsInFrame			(	NvDpyObject_PSP*	inDpyObj			);
	bool						IsDrawing			(	NvDpyObject_PSP*	inDpyObj			);
	void						SetFrameBreakBefore	(	NvDpyObject_PSP*	inDpyObj			);
	void						SetFrameBreakAfter	(	NvDpyObject_PSP*	inDpyObj			);
	bool						Draw				(	NvDpyObject_PSP*	inDpyObj			);


	// Garbager for safe release of frame-dependant objects
	void						AddToGarbager		(	NvInterface*		inITF				);
	void						FlushGarbager		(											);
}



#endif	// _NvDpyManager_PSP_H_


