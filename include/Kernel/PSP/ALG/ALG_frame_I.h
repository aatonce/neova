/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _ALG_FRAME_H_



//
// Packet's GeContext

inline void frame::GeContext::SetUser		(	uint32*		inMADR			)
{
	NV_ASSERT_A32( inMADR );
	cadr = inMADR;
}


inline void frame::GeContext::RestoreDefault	(				)
{
	cadr = &cmd;
	NV_ASSERT_A32( cadr );
}




//
//	Packet

inline void frame::Packet::SetSeparator		(	bool	inEnable	)
{
	if( inEnable )	flags |= FLG_SEPARATOR;
	else			flags &= ~(FLG_SEPARATOR);
}


inline void frame::Packet::SetNeedUpdate	(	bool	inEnable	)
{
	if( inEnable )	flags |= FLG_UPD_REQUESTED;
	else			flags &= ~(FLG_UPD_REQUESTED);
}


inline bool frame::Packet::IsDrawing		(						)
{
	return ( flags & STT_DRAWING ) != 0;
}




#endif // _ALG_FRAME_H_


