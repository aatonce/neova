/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _ALG_H_
#define _ALG_H_


#include <Nova.h>
#include "ALG_defs.h"
#include "ALG_dmac.h"
#include "ALG_ge.h"
#include "ALG_tram.h"
#include "ALG_frame.h"
#include "ALG_devcons.h"
#include "ALG_audio.h"


#if defined(__MWERKS__)
#undef SCE_GE_EXTRACT_ADDR24
#undef SCE_GE_EXTRACT_BASE8
#undef SCE_GE_EXTRACT_BASE24
#undef SCE_GE_EXTRACT_FLOAT24
#define SCE_GE_EXTRACT_ADDR24(_adr32)		(((SceUInt32)(_adr32)) & 0x00FFFFFFU)
#define SCE_GE_EXTRACT_BASE8(_adr32)		((((SceUInt32)(_adr32))>>24) & 0x0FU)
#define SCE_GE_EXTRACT_BASE24(_adr32)		((((SceUInt32)(_adr32))>>8) & 0x000FFFFFU)
#define SCE_GE_EXTRACT_FLOAT24(_float32)	((((const ScePspUnion32 *)&(_float32))->ui)>>8)
#endif // __MWERKS__


#endif // _ALG_H_


