/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _ALG_GE_H_



inline
uint
ge::GetVertexBSize	(	uint32		inVType		)	// ge vtype cmd
{
	VertexMap vm;
	vm.Init( inVType );
	return vm.bsize;
}


inline
bool
ge::VertexMap::HasComponent(	Component	inC		)
{
	NV_ASSERT( inC <= XYZ );			// valid component
	return (cbs[inC] != 0);
}


inline
uint
ge::VertexMap::GetIndexType	(			)
{
	return GET_VTYPE_IT( vtype );
}


inline
uint8*
ge::VertexMap::GetComponentPointer(		uint8* inVBase,	Component inC )
{
	if( !HasComponent(inC) )	return NULL;
	return inVBase + cbo[inC];
}


inline
uint32
ge::VertexBuffer::GetBSize	(			)
{
	return ( size * vmap.bsize );
}


inline
uint8*
ge::VertexBuffer::At	(	uint		inVIndex	)
{
	NV_ASSERT( inVIndex < size );
	return (uint8*)( base + inVIndex*vmap.bsize );
}


inline
bool
ge::VertexBuffer::HasComponent	(	VertexMap::Component	inC		)
{
	return vmap.HasComponent( inC );
}


inline
uint
ge::VertexBuffer::GetNbWeight	(		)
{
	if( vmap.cbs[VertexMap::WEIGHT] == 0 )
		return 0;
	return ((vmap.vtype>>14)&7) + 1;
}


inline
void
ge::VertexBuffer::SetBuffer	(	pvoid	inBuffer	)
{
	base = uint32(inBuffer);
}


#endif // _ALG_GE_H_


