/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/

#ifndef _ALG_GE_TOOLS_H_
#define _ALG_GE_TOOLS_H_

namespace ge { namespace tools {

enum CopyFlags{
	CF_FilteringNearest = 0x1,	
	CF_FilteringLinear	= 0x2,	// default
	CF_UseAlpha			= 0x4,		
	CF_TSync			= 0x8,
	CF_TFlush			= 0x10,
};
	
struct MipmapGC {
	float	weight;		// in [0,1]
	int		blend;		// in [0,128]
	bool	quality;	// TRUE is best quality is required !
};

//
// Gaussian convolution based on mipmaps linear interpolation

struct Kernel33 {					// 3x3 convolution kernel
	union {
		struct {
			float	k00, k01, k02;	// (-1,-1) ( 0,-1) ( 1,-1)
			float	k10, k11, k12;	// (-1, 0) ( 0, 0) ( 1, 0)
			float	k20, k21, k22;	// (-1, 1) ( 0, 1) ( 1, 1)
		};
		float		k[3][3];		// [y][x]
	};
	void	Reset						(																								);
	void	Normalize					(																								);
	void	RotateCW					(																								);
	void	RotateCCW					(																								);
};

uint		GetPSMBitsSize				(	int 				inPsm																	);

uint 		GetBufferByteSize 			( 	int 				inPsm																	, 
											uint 				inSizeX 																, 
											uint 				inSizeY																	);

int 		Compute_MipmapedGaussConvol	(	MipmapGC*			outMipGC/*[16]*/														,
											int					inSizeX																	,
											int					inSizeY																	,
											float				inRadius																,
											float				inQuality																);
											
void 		InitContextCmds				(	dmac::Cursor * 		inDC																	);

void 		RestoreContextCmds			(	dmac::Cursor * 		inDC																	);

void 		LoadClut					(	dmac::Cursor * 		inDC																	,
											uint 				inClutAddr																, 
											uint8 				inPsm																	, 
											uint8 				inZShift																, 
											uint8 				inMask																	); 

	
void 		CopyZToAlpha				(	dmac::Cursor *		outDC 																	,
											uint				inFromMAddr																,
											uint				inToVAddr																,
											uint				inSizeX																	,
											uint				inSizeY																	,				
											uint32**			outGeFBPCmd		= NULL													);
											
void 		CopyBuffer					(	dmac::Cursor*		inDC 																	,
											uint				inFromMAddr																,
											uint				inToVAddr																,
											uint				inSrcSizeX																,
											uint				inSrcSizeY																,						
											uint				inDstSizeX																,
											uint				inDstSizeY																,						
											Vec4&				inRec																	,
											Vec4&				inTex																	,
											uint16				inZ			= 0															,
											uint				inFlafs		= CF_FilteringLinear | CF_UseAlpha | CF_TSync | CF_TFlush	,
											uint32**			outGeFBPCmd	= NULL														,
											uint32**			outGeTBPCmd	= NULL														);

void 		SimpleCopyBuffer			(	dmac::Cursor*		inDC 																	,
											uint				inFromMAddr																,
											uint				inToVAddr																,
											uint				inSizeX																	,
											uint				inSizeY																	,						
											uint16				inZ			= 0															,
											uint				inFlafs		= CF_FilteringLinear | CF_UseAlpha | CF_TSync | CF_TFlush	,
											uint32**			outGeFBPCmd	= NULL														,
											uint32**			outGeTBPCmd	= NULL														);
											
void 		CopyZAsIdxTex				(	dmac::Cursor*		inDC 																	,
											uint				inFromMAddr																,
											uint				inToVAddr																,
											uint				inSizeX																	,
											uint				inSizeY																	,						
											uint16				inZ			= 0															,
											uint				inFlafs		= CF_FilteringNearest | CF_UseAlpha | CF_TSync | CF_TFlush	,
											uint32**			outGeFBPCmd	= NULL														);
																						
void 		FillBuffer					(	dmac::Cursor*		inDC 																	,
											uint				inToVAddr																,
											uint				inDstSizeX																,
											Vec4&				inRec																	,
											uint16				inZ																		,
											uint32				inColorABGR																,
											uint32**			outGeFBPCmd	= NULL														);
											
bool		PreparePass					(	dmac::Cursor*		inDC 																	,
											uint				inFromMAddr																,
											uint				inToVAddr																,
											uint				inSrcSizeX																,
											uint				inSrcSizeY																,
											uint				inDstSizeX																,
											uint				inDstSizeY																,
											uint32**			outGeTBPCmd	= NULL														);
											
bool		UnPreparePass				(	dmac::Cursor*		inDC 																	,
											uint				inFromMAddr																,
											uint				inToVAddr																,
											uint				inSrcSizeX																,
											uint				inSrcSizeY																,
											uint				inDstSizeX																,
											uint				inDstSizeY																,								
											uint16				inZVal		= 0															,
											int					inCopyFlags	= CF_FilteringLinear | CF_TSync 							,
											uint32**			outGeFBPCmd	= NULL														);
											
void		DiffConvolvePass			(	dmac::Cursor*		inDC																	,
											uint				inFromMAddr																,
											uint				inToVAddr																,
											uint				inTmpVaddr																,
											uint				inSizeX																	,
											uint				inSizeY																	,									
											const Kernel33&		inKernel																,
											uint16				inZ			= 0															);
											
void 		GaussianBlur				(	dmac::Cursor*		inDC																	,
											uint				inBlurVAddr																,
											uint				inTmpVAddr																,
											uint				inSizeX																	,
											uint				inSizeY																	,
											float				inRadius	= 2.f														,
											float				inQuality	= 0.f														);
											
void 		GlowBlur					(	dmac::Cursor*		inDC																	,
											uint				inBlurVAddr																,
											uint				inTmpVAddr																,
											uint				inSizeX																	,
											uint				inSizeY																	,
											float				inRadius	= 2.f														,
											float				inQuality	= 0.f														);
											
}}

#endif // _ALG_GE_TOOLS_H_


