/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _ALG_DMAC_H_
#define _ALG_DMAC_H_



namespace dmac
{

	//
	// Data cursor

	struct Cursor {
		uint8*			start;
		uint32			bsize;

		union {
			pvoid		vd;
			uint8*		i8;
			uint16*		i16;
			uint32*		i32;
			float*		f32;
			uint64*		i64;
			uint128*	i128;
		};

		void			Memcpy			(	pvoid	inPtr,			uint inBSize				);
		pvoid			Dup				(	pvoid	inStart	= NULL, uint inBAlignement = 64		);		// D$ alignment
		uint32			GetUsedBSize	(														);
		uint32			GetLeftBSize	(														);
		void			Align32			(														);
		void			Align64			(														);
		void			Align128		(														);
		void			Align128_32		(														);
		void			Align8QW		(														);
		void			Align8QW_1		(														);
	};


	//
	// Init/Reset the DMAC

	void			Init				(												);
	void			Reset				(												);
	void			Shut				(												);


	//
	// GE channel

	void			Start				(	void*		inMADR							);		// toGE
	bool			IsBusy				(												);
	float			GetPerfTime			(												);
	bool			Sync				(												);		// FALSE => stalling !
	void			Stop				(												);
	void			Pause				(												);
	void			Continue			(												);
	volatile uint16	GetMark				(												);
	volatile uint16	GetMarkCpt			(												);
	bool			WaitMark			(	uint16		inMark							);		// FALSE => stalling !
	bool			WaitNextMark		(												);		// FALSE => stalling !
	void			InsertMark_CH		(	Cursor*		inDC,	uint16	inMark			);


	//
	// Debugging

	bool			IsValidMemRange		(	void*		inPtr,
											uint		inBSize							);

	bool			CheckData			(	void*		inPtr,
											uint		inBSize,
											bool		inVerbose						);
	bool			CheckPacket			(	void*		inMADR,
											bool		inVerbose						);
	bool			CheckChain			(	void*		inFromMADR,
											void*		inToMADR,								// NULL => to END CMD
											bool		inVerbose						);

	bool			CheckUsingAdr		(	void*		inFromMADR,
											void*		inToMADR,
											void*		inTestMADR,
											bool 		inVerbose						);

	//
	// Double DMA Heap
	// Fast uncached pointers (FAST_UNCACHED_MEM) with no need to flush the data cache !

	void			SwapHeap			(												);
	Cursor*			GetFrontHeapCursor	(												);
	Cursor*			GetBackHeapCursor	(												);
}



#ifdef _NVCOMP_ENABLE_DBG

	#define NV_ASSERT_DC( CURSOR_PTR )																					\
		{																												\
			NV_ASSERTC( CURSOR_PTR, "NULL Cursor !" );																	\
			NV_ASSERT_A64( (CURSOR_PTR)->start )																		\
			NV_ASSERT_A32( (CURSOR_PTR)->i8 )																			\
			NV_ASSERTC( (CURSOR_PTR)->i8>=(CURSOR_PTR)->start, "Cursor underflow !" );									\
			NV_ASSERTC( uint32(((CURSOR_PTR)->i8)-((CURSOR_PTR)->start))<((CURSOR_PTR)->bsize), "Cursor overflow !" );	\
		}

#else

	#define NV_ASSERT_DC( CURSOR_PTR ) {}

#endif


#include "ALG_dmac_I.h"

#endif // _ALG_DMAC_H_


