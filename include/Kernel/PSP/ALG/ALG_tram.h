/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _ALG_TRAM_H_
#define _ALG_TRAM_H_



namespace tram
{

	struct TexDesc;
	struct TexReg;


	//
	//	Texture Descriptor
	//	Texel components RGBA in [0,255]

	struct	TexDesc
	{
		enum {								// Flags
			F_TRANSPOSED	= (1<<0),		// Texels transposition ?
		};

		struct Trx {	
			uint8* 		bitmapData;			// BITMAP DATA PTR
			uint32		bitmapBSize;		// BITMAP DATA BYTE SIZE
			uint32		TBW;
		};

		struct GeCmd {
			uint32		cbp;				// Clut Instructions
			uint32		cbw	;
			uint32		clut;
			uint32		cload;
			uint32		tbp[4];				// fix Texture Pointer 0..4
			uint32		tbw[4];
			uint32		tsize[4];
			uint32		tpf;				// Texture Format
			uint32		tmode;				// Storage Mode, MultipleClut, TextureLevel (0..7)
		};

		uint8			flags;				// flags (reserved)
		uint8			psm;				// in [ SCE_GS_PSMT4, SCE_GS_PSMT8, SCE_GS_PSMCT32 ]
		uint16			mipmapCpt;			// in [1,6]
		uint16			L2_W;				// W=1<<L2_W
		uint16			L2_H;				// H=1<<L2_H
		uint32			resBSize;			// descriptor size in bytes (for duplication)
		Trx				trx[5];				// trx[0]=CLUT, trx[1]=mipmap0, ...
		GeCmd			geCmd;
	};


	//
	// Registered Texture Handle

	struct TexReg {
		TexDesc*		desc;
		uint32			allBmpBSize;
		int				next_free;			// reserved ...
	};




	//
	// Init/Reset

	void		Init				(										);
	void		Reset				(										);
	void		Shut				(										);


	//
	// TexDesc Register / Release / Build / Translate

	int			RegisterDesc		(	TexDesc*		inDesc				);
	int			FindDesc			(	TexDesc*		inDesc				);
	TexReg*		GetRegistered		(	int				inTexId				);
	void		ReleaseDesc			(	int				inTexId				);
	TexReg*		GetRegistrationA	(	uint*			outSize		 = NULL,
										int*			outFirstFree = NULL	);

	bool		BuildDesc			(	TexDesc*		outDesc,
										uint			inPSM,					// SCE_GE_TPF_IDTEX8/SCE_GE_TPF_8888.
										uint			inL2W,					// Width pow of 2
										uint			inL2H,					// Height pow of 2
										pvoid			inCLUTPtr	= NULL,		// for SCE_GS_PSMT8
										pvoid			inTexelPtr	= NULL	);	// Block height pow of 2
										
	void		DMATranslateDesc	(	TexDesc*		ioDesc,
										uint32			inAddrOffset		);

	//
	// Test the texture mipmaping usage
	// Mode :
	// 0 => Paint by LOD's index
	// 1 => Paint by LOD's sup_pow2( MAX(w,h) )
	// 2 => Paint by LOD's sup_pow2( MIN(w,h) )

	bool		TestMipmaping		(	TexDesc*		inDesc,
										uint			inMode,
										uint			inLODMask			);
}


#include <Kernel/PSP/ALG/ALG_tram_I.h>


#endif // _ALG_TRAM_H_



