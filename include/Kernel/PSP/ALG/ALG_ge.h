/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _ALG_GE_H_
#define _ALG_GE_H_



namespace ge
{
	//
	// video mode options :
	// - VM_SHOWBACK flag could be used for debug convenience.
	// - VM_VESA1A/VM_VGA flags could be used with PSP hardware tool only.
	// - VM_SWAPPING indicates that the present op. will be performed by the buffers addrs swap.
	//   Default is a present operation performed by copying the whole back buffer to the front buffer since the vblank.
	//   Swapping is GPU cheaper than copying, but it is also most constraining such that front & back will have same
	//   dimensions & psm, involving a greater consumption of vram and disabling color/alpha modulation.

	enum VMode {						// vmode parameter
		VM_SWAPPING		=	(1<<0),		// force present front/back buffers swapping
		VM_PSM16		=	(1<<1),		// front frame psm is 16BPP / default is 32BPP
		VM_LCD			=	(1<<2),		// LCD output mode
		VM_VESA1A		=	(1<<3),		// VESA1A (VGA) output mode  
		VM_VGA			=	(1<<4),		// Pseudo-VGA output mode  
		VM_SHOWBACK		=	(1<<5)		// debug show back buffer
	};


	struct Context {
		uint32			cmode;			// CMD_CMODE
		uint32			vtype;			// CMD_VTYPE
		uint32			region[2];		// CMD_REGION1/REGION2
		uint32			scissor[2];		// CMD_SCISSOR1/SCISSOR2
		uint32			offsetxy[2];	// CMD_OFFSETX/Y
		uint32			projd[16];		// CMD_PROJD0-15
		uint32			sxyz[3];		// CMD_SX/SY/SZ
		uint32			txyz[3];		// CMD_TX/TY/TZ
		uint32			fbp;			// CMD_FBP
		uint32			fbw;			// CMD_FBW
		uint32			fpf;			// CMD_FPF
		uint32			zbp;			// CMD_ZBP
		uint32			zbw;			// CMD_ZBW
		uint32			minmaxz[2];		// CMD_MINZ/MAXZ
		uint32			cull;			// CMD_CULL
		uint32			shade;			// CMD_SHADE
		uint32			cref;			// CMD_CREF
		uint32			ctest;			// CMD_CTEST
		uint32			cmsk;			// CMD_CMASK
		uint32			atest;			// CMD_ATEST
		uint32			stest;			// CMD_STEST
		uint32			ztest;			// CMD_ZTEST
		uint32			zmsk;			// CMD_ZMSK
		uint32			sop;			// CMD_SOP
		uint32			lop;			// CMD_LOP
		uint32			blend;			// CMD_BLEND
		uint32			fix[2];			// CMD_FIXA/FIXB
		uint32			dith[4];		// CMD_DITH0-3
		uint32			pmsk[2];		// CMD_PMSK1/PMSK2
		uint32			lte;			// CMD_LTE
		uint32			cle;			// CMD_CLE
		uint32			bce;			// CMD_BCE
		uint32			tme;			// CMD_TME
		uint32			fge;			// CMD_FGE
		uint32			dte;			// CMD_DTE
		uint32			abe;			// CMD_ABE
		uint32			ate;			// CMD_ATE
		uint32			zte;			// CMD_ZTE
		uint32			ste;			// CMD_STE
		uint32			aae;			// CMD_AAE
		uint32			pce;			// CMD_PCE
		uint32			cte;			// CMD_CTE
		uint32			loe;			// CMD_LOE
		uint32			nrev;			// CMD_NREV
		uint32			pface;			// CMD_PFACE
		uint32			suv[2];			// CMD_SU/SV
		uint32			tuv[2];			// CMD_TU/TV
		uint32			tlevel;			// CMD_TLEVEL
		uint32			tfunc;			// CMD_TFUNC
		uint32			tmap;			// CMD_TMAP
		uint32			tmode;			// CMD_TMODE
		uint32			tfilter;		// CMD_TFILTER
		uint32			twrap;			// CMD_TLEVEL
		uint32			tslope;			// CMD_TSLOPE
		uint32			tec;			// CMD_TEC
		uint32			fog[2];			// CMD_FOG1/FOG2
		uint32			fc;				// CMD_FC
	};

	const uint64		CMODE			=	uint64(1)<<0;
	const uint64		VTYPE			=	uint64(1)<<1;
	const uint64		REGION			=	uint64(1)<<2;
	const uint64		SCISSOR			=	uint64(1)<<3;
	const uint64		OFFSETXY		=	uint64(1)<<4;
	const uint64		PROJD			=	uint64(1)<<5;
	const uint64		SXYZ			=	uint64(1)<<6;
	const uint64		TXYZ			=	uint64(1)<<7;
	const uint64		FBP				=	uint64(1)<<8;
	const uint64		FBW				=	uint64(1)<<9;
	const uint64		FPF				=	uint64(1)<<10;
	const uint64		ZBP				=	uint64(1)<<11;
	const uint64		ZBW				=	uint64(1)<<12;
	const uint64		MINMAXZ			=	uint64(1)<<13;
	const uint64		CULL			=	uint64(1)<<14;
	const uint64		SHADE			=	uint64(1)<<15;
	const uint64		CREF			=	uint64(1)<<16;
	const uint64		CTEST			=	uint64(1)<<17;
	const uint64		CMSK			=	uint64(1)<<18;
	const uint64		ATEST			=	uint64(1)<<19;
	const uint64		STEST			=	uint64(1)<<20;
	const uint64		ZTEST			=	uint64(1)<<21;
	const uint64		ZMSK			=	uint64(1)<<22;
	const uint64		SOP				=	uint64(1)<<23;
	const uint64		LOP				=	uint64(1)<<24;
	const uint64		BLEND			=	uint64(1)<<25;
	const uint64		FIX				=	uint64(1)<<26;
	const uint64		DITH			=	uint64(1)<<27;
	const uint64		PMSK			=	uint64(1)<<28;
	const uint64		LTE				=	uint64(1)<<29;
	const uint64		CLE				=	uint64(1)<<30;
	const uint64		BCE				=	uint64(1)<<31;
	const uint64		TME				=	uint64(1)<<32;
	const uint64		FGE				=	uint64(1)<<33;
	const uint64		DTE				=	uint64(1)<<34;
	const uint64		ABE				=	uint64(1)<<35;
	const uint64		ATE				=	uint64(1)<<36;
	const uint64		ZTE				=	uint64(1)<<37;
	const uint64		STE				=	uint64(1)<<38;
	const uint64		AAE				=	uint64(1)<<39;
	const uint64		PCE				=	uint64(1)<<40;
	const uint64		CTE				=	uint64(1)<<41;
	const uint64		LOE				=	uint64(1)<<42;
	const uint64		NREV			=	uint64(1)<<43;
	const uint64		PFACE			=	uint64(1)<<44;
	const uint64		SUV				=	uint64(1)<<45;
	const uint64		TUV				=	uint64(1)<<46;
	const uint64		TLEVEL			=	uint64(1)<<47;
	const uint64		TFUNC			=	uint64(1)<<48;
	const uint64		TMAP			=	uint64(1)<<49;
	const uint64		TMODE			=	uint64(1)<<50;
	const uint64		TFILTER			=	uint64(1)<<51;
	const uint64		TWRAP			=	uint64(1)<<52;
	const uint64		TSLOPE			=	uint64(1)<<53;
	const uint64		TEC				=	uint64(1)<<54;
	const uint64		FOG				=	uint64(1)<<55;
	const uint64		FC				=	uint64(1)<<56;
	const uint64		ALL_REGS		=	(uint64(1)<<57)-1UL;


	//
	// Init/Reset GE & CRTC & FRAME

	void				Init				(	bool			inEnableCRTC		);
	void				Reset				(	bool			inEnableCRTC		);
	void				Shut				(										);


	//
	// Sync with GE

	bool				IsBusy				(										);
	void				Sync				(										);
	bool				IsPaused			(										);
	void				Pause				(										);
	void				Resume				(										);


	//
	// VRam management

	pvoid				GetVRamTop			(										);		// uncached access !
	uint32				GetVRamBSize		(										);
	pvoid				GetVRamPointer		(	uint32			inLocalAddr = 0		);		// uncached access !
	pvoid				GetVRamAddr			(	uint32			inLocalAddr = 0		);		// cached access !

	void				TransferToVRam_CH	(	dmac::Cursor*	inDC				);
	void				TransferFromVRam_CH	(	dmac::Cursor*	inDC				);


	//
	// Flush registers of the context

	void				FlushContext_CH		(	dmac::Cursor*	inDC,
												const Context*	inCtxt,
												uint64			inRegistersMask,
												uint32**		outFBPCmdAddr=NULL	);


	//
	// Vertex management

	uint32				PackSFloat			(	uint			inType,					// SCE_GE_VTYPE_CHAR/SHORT/FLOAT
												float			inV,
												float			inS	= 1.f,				// scale
												float			inT	= 0.f			);	// translation
	uint32				PackUFloat			(	uint			inType,					// SCE_GE_VTYPE_UCHAR/USHORT/FLOAT
												float			inV,
												float			inS = 1.f,				// scale
												float			inT = 0.f			);	// translation
	uint32				PackColor			(	uint			inType,					// SCE_GE_VTYPE_CT5650/CT5551/CT4444/CT8888
												uint32			inRGBA				);

	uint				GetVTypeBSize		(	uint			inType				);	// SCE_GE_VTYPE_xxx
	uint				GetVertexBSize		(	uint32			inVType				);	// ge vtype cmd

	struct	VertexMap
	{
		// components order: weight -> st -> color -> nxyz -> xyz
		enum Component {
			WEIGHT	= 0,
			ST		= 1,
			COLOR	= 2,
			NXYZ	= 3,
			XYZ		= 4,
		};
		uint32			vtype;
		uint8			cbs[5];				// component byte-size
		uint8			cba[5];				// component byte-alignement
		uint8			cbo[5];				// component byte-offset
		uint8			padbs;				// final padding byte-size
		uint8			bsize;				// full byte-size

		void			Init				(	uint32			inVType					);	// ge vtype cmd
		bool			HasComponent		(	Component		inC						);
		uint			GetComponentType	(	Component		inC						);
		uint			GetIndexType		(											);
		uint8*			GetComponentPointer	(	uint8*			inBase, Component inC	);
	};

	struct	VertexBuffer
	{
		uint32			base;				// buffer base addr
		uint			size;				// # vertex
		VertexMap		vmap;
		float			locS;
		Vec3			locT;
		float			texS;
		Vec2			texT;

		void			Init				(	uint32				inVType,
												pvoid				inBuffer,
												uint				inSize,
												float				inLocS	= 1.f,
												const Vec3&			inLocT	= Vec3::ZERO,
												float				inTexS	= 1.f,
												const Vec2&			inTexT	= Vec2::ZERO);
		uint32			GetBSize			(											);
		uint8*			At					(	uint					inVIndex		);
		bool			HasComponent		(	VertexMap::Component	inC				);
		uint			GetNbWeight			(											);
		void			SetBuffer			(	pvoid					inBuffer		);
		void*			SetValue			(	uint					inVIndex,				// vertex index
												uint					inUIndex,				// component unit index
												VertexMap::Component	inC,
												uint32					inV				);
		void*			SetWeight			(	uint					inVIndex,
												uint					inWIndex,
												float					inW				);
		void*			SetTex				(	uint					inVIndex,
												const Vec2&				inST			);
		void*			SetColor			(	uint					inVIndex,
												uint32					inRGBA			);
		void*			SetNormal			(	uint					inVIndex,
												const Vec3&				inNXYZ			);
		void*			SetLocation			(	uint					inVIndex,
												const Vec3&				inXYZ			);
	};

}


#include "ALG_ge_crtc.h"
#include "ALG_ge_frame.h"
#include "ALG_ge_I.h"
#include "ALG_ge_tools.h"


#endif // _ALG_GE_H_


