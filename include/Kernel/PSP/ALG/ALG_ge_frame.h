/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _ALG_GE_FRAME_H_
#define _ALG_GE_FRAME_H_


namespace ge { namespace frame {


	//
	//	Frame's constants

	extern uint32			PhysicalSizeX;		// front physical width
	extern uint32			PhysicalSizeY;		// front physical height
	extern uint32			VirtualSizeX;		// front virtual width
	extern uint32			VirtualSizeY;		// front virtual height
	extern uint32			SizeX;				// back width
	extern uint32			SizeY;				// back height
	extern uint32			SizeX2;				// 2^SizeX2 >= SizeX
	extern uint32			SizeY2;				// 2^SizeY2 >= SizeY
	extern uint32			OffsetX;			// back screen offset-x (0.12.4)
	extern uint32			OffsetY;			// back screen offset-y (0.12.4)
	extern uint32			FrontAddr;			// front addr
	extern uint32			FrontWidth;			// front width aligment
	extern uint32			FrontBSize;			// front byte size
	extern uint32			FrontPSM;			// front draw psm (SCE_DISPLAY_PIXEL_RGBx/SCE_GE_FPF_x)
	extern uint32			BackAddr;			// back addr
	extern uint32			BackWidth;			// back width aligment
	extern uint32			BackBSize;			// back byte size
	extern uint32			BackPSM;			// back psm (always SCE_DISPLAY_PIXEL_RGBA8888/SCE_GE_FPF_8888)
	extern uint32			DepthAddr;			// depth addr
	extern uint32			DepthWidth;			// depth width aligment
	extern uint32			DepthBSize;			// depth byte size
	extern uint32			VRamTopAddr;		// top base addr
	extern uint32			VRamLowerAddr;		// free vram lower addr
	extern uint32			VRamUpperAddr;		// free vram upper addr
	extern Context			DefContext;			// Default frame ctxt


	// Front/Back swapping

	bool					IsSwapMode			(										);		// VM_SWAPPING activated ?
	bool					IsSwapCompliant		(										);		// front & back have same dims & psm ?
	void					Swap				(										);
	bool					IsOddFrame			(										);
	bool					IsEvenFrame			(										);


	//	Putback a bitmap in the back-buffer

	void					PutBack				(	pvoid				inData			);
	void					PutBack_CH			(	dmac::Cursor*		inDC,
													pvoid				inData			);


	//	Get back-buffer & depth-buffer
	void					GetBack				(	pvoid				inBuffer		);

	void					GetDepth			(	pvoid				inBuffer		);



	// Frame operations with DMA modifiers

	struct ClearMod {
		uint32*				_cm;
		uint32*				_sciss;
		uint32*				_color;
		void				Translate			(	uint32				inD				);
		void				SetColor			(	uint32				inABGR			);
		void				SetRect				(	int					inX,
													int					inY,
													int					inW,
													int					inH				);
		void				EnableColor			(	bool				inOnOff			);
		void				EnableDepth			(	bool				inOnOff			);
	};

	// Present Back -> Front
	void					Present_CH			(	dmac::Cursor*		inDC			);

	// Clear Back & Depth
	void					Clear_CH			(	dmac::Cursor*		inDC,
													ClearMod&			outMod			);

} }


#endif // _ALG_GS_FRAME_H_


