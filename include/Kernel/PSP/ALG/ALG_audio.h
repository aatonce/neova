/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifndef _ALG_AUDIO_H_
#define _ALG_AUDIO_H_



namespace audio
{
	enum {
		NB_SOUND_MAX	= 128,
		NB_CHANNEL		= 32
	};

	enum SdState {
		SD_UNUSED		= 0,			// unused
		SD_UNLOCKED		= 1,			// unlocked
		SD_LOCKING		= 2,			// locking
		SD_LOCKED		= 3,			// locked
		SD_UNLOCKING	= 4				// unlocking
	};

	enum ChState {
		CH_IDLE			= 0,
		CH_PLAYING		= 1
	};

	enum MixSwitch {
		MX_DRY			= (1<<0),
		MX_WET			= (1<<1),
	};

	enum FxType {
		FX_TYPE_ROOM	= 0,			// Room Reverb
		FX_TYPE_STUDIOA	= 1,			// Studio Reverb A
		FX_TYPE_STUDIOB	= 2,			// Studio Reverb B
		FX_TYPE_STUDIOC	= 3,			// Studio Reverb C
		FX_TYPE_HALL	= 4,			// Hall Reverb
		FX_TYPE_SPACE	= 5,			// Space echo
		FX_TYPE_ECHO	= 6,			// Echo
		FX_TYPE_DELAY	= 7,			// Delay
		FX_TYPE_PIPE	= 8,			// Pipe Echo
	};


	bool			Init				(										);
	void			Update				(										);
	void			Shut				(										);


	//
	// Synthesizer

	void			EnableMix			(	MixSwitch		inMix				);
	void			DisableMix			(	MixSwitch		inMix				);
	void			SetMasterVolume		(	float			inVol				);		// in [0,1]
	void			SetEffectVolume		(	float			inVol				);		// in [0,1]
	void			SetEffectType		(	FxType			inFxType			);
	void			SetEffectParam		(	uint			inDT,						// delay time
											uint			inFB				);		// feedback level


	//
	// Sound management

	SdState			SdGetState			(	int				inSdId				);
	uint			SdGetMode			(	int				inSdId				);		// must be locked !
	uint			SdGetBaseFreq		(	int				inSdId				);		// must be locked !
	float			SdGetDuration		(	int				inSdId				);		// must be locked !
	bool			SdLock				(	int				inSdId,
											uint			inMode,						// 0:mono-SFX 1:mono-STREAM 2:MUSIC
											uint			inDataFreq,					// in Hz
											uint32			inDataBOffset,
											uint32			inDataBSize,
											float			inDuration,
											float			inLockFromTime = 0,
											float			inLockDuration = 0	);
	bool			SdUnlock			(	int				inSdId				);
	void			SdFree				(	int				inSdId				);
	int				SdAlloc				(	int				inSdId				);
	int				SdAny				(										);


	//
	// Channel management

	ChState			ChGetState			(	int				inChannel			);
	int				ChGetSoundId		(	int				inChannel			);
	bool			ChPlay				(	int				inChannel,
											int				inSdId,
											uint			inPitch	= 0x1000,
											int				inVolL	= 0x1000,
											int				inVolR	= 0x1000	);
	bool			ChStop				(	int				inChannel			);
	bool			ChSetVolume			(	int				inChannel,
											int				inVolL,
											int				inVolR				);
	bool			ChSetPitch			(	int				inChannel,
											uint			inPitch				);
	int				ChAny				(										);


	// Background music managment

}


#endif // _ALG_AUDIO_H_




