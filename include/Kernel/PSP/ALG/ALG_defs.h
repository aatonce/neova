/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_ALG_DEFS_H_
#define	_ALG_DEFS_H_


// SCE Headers
#include <kernel.h>
#include <kerneltypes.h>
#include <kernelutils.h>
#include <psperror.h>
#include <powersvc.h>
#include <scratchpad.h>
#include <devkit_version.h>
#include <displaysvc.h>
#include <geman.h>
#include <gecmd.h>
#include <libwave.h>



#define HCNT_PER_V		286		// H-count number per 1V



#define	IMPORT_TEXT( _SYMBOL )		extern u_int _SYMBOL [] __attribute__((section(".text")))
#define	IMPORT_DATA( _SYMBOL )		extern u_int _SYMBOL [] __attribute__((section(".data")))


/*
                +----GraphicsEngine(Max166MHz)---+   +-----------------+
                | +--------------+    +--------+ |   |    GE eDRAM     |
                | |GraphicsEngine|====| eDRAM  |=|===|        2MB      |
                | |     Core     |@256|  Ctrl  | |   +-----------------+
  +-----------+ | +------+-------+ x2 +---+----+ | +------+       to LCD
  |Scrach Pad | |        |                |@128  | |      +------------>
  |    SRAM   | |   +----+---+       +----+---+  | |      |       to DDR
  |           | |   |  Bus   |M     S|  Bus   |  | | DMAC |      +----->
  | Max 166MHz| |   | Matrix +-------+ Matrix |  | |      | +----+-----+
  |    16KB   | |    ----+---+       +----+---+  | |      | |  Memory  |
  |           | |        |                |      | |      | |Controller|
  +-----+-----+ +--------+----------------+------+ +--+---+ +----+-----+
        |@128            |@128            |@128       |@128      |@128  
        |S               |M               |S          | M        |S     
  ======+==========+=====+================+===========+=====+====+======
                   |M           SystemBus Max166MHz @128    |M          
                   |@128                                    |@128       
   +---------------+----------------------------+   +-------+----------+
   |  +------------+-------------------+        |   | +-----+--------+ |
   |  |          Bus I/F               |        |   | | Write Buffer | |
   |  |      Max 166MHz @128/@32       |        |   | |              | |
   |  +--+------+----------+--------+--+        |   | | Max 166MHz   | |
   |     |@128  |@32       |@32     |@128       |   | |    @128      | |
   |  +--+---+  |      +---+---+ +--+----+      |   | |   8 qword    | |
   |  |Icache|  |Un-   |UnCache| |Dcache |      |   | +--------------+ |
   |  | 16KB |  |Cache |   WB  | |  16KB |      |   |(uncached sv.q,wb)|
   |  |333MHz|  |read  | 16word| | 333MHz|      |   |                  |
   |  +--+---+  |path  +---+---+ ++----+-+      |   |                  |
   |     |@32   |          |@32   |@32 |@128/@32|   |                  |
   |     |      |          |      |    +--------+---+(cached lv/sv/    |
   |  +--+------+----------+------+-------+     |   |          sv.q,wb)|
   |  |     ALLEGREX Core,FPU             +=====+===+(uncached lv/sv/  |
   |  +-----------------------------------+  @32|   |          mfv/mtv)|
   +--------------ALLEGREX (Max333MHz)----------+   +-VFPU (Max333MHz)-+
*/



// D$ alignment (64 bytes) buffer to prevent DMA/D$ conflit
inline void* AllocDMABuffer( uint32 inBSize )
{
	uint32 uncBSize = Round64( inBSize );
	void*  uncPtr   = NvMallocA( uncBSize, 64 );
	return uncPtr;
}

// DMA memory bit mask
#define DMA_MEM								(0x1FFFFFFF)
#define	DMA_ADDR( inADDR )					(uint32(inADDR)&DMA_MEM)

// SPR Area
#define	SPR_START							(0x00010000)
#define	SPR_HALF							(0x00012000)
#define SPR_END								(0x00014000)
#define	SPR_MEM								(0x00013FFF)
#define	SPR_ADDR( inADDR )					(uint32(inADDR)&SPR_MEM)

// Unchached memory bit mask
#define	UNCACHED_MEM						(0x40000000)

template <typename T>
inline T CachedPointer( T inPtr )
{
	return (T)sceKernelMakeCachedAddr ( (void*)inPtr );
}

template <typename T>
inline T UncachedPointer( T inPtr )
{
	return (T)sceKernelMakeUncachedAddr( (void*)inPtr );
}

template <typename T>
inline bool IsCachedPointer( T inPtr )
{
	return sceKernelIsCachedAddr( (void*)inPtr ) != 0;
}


//
// Pipelines sync

inline void SyncMemory()
{
	asm volatile ("sync; nop");
}

inline void SyncPipeline()
{
	asm volatile ("sync; nop");
}

inline void SyncRW()
{
	asm volatile ("sync; nop");
}

inline void SafeSyncDCache( pvoid begin, pvoid end )
{
	NV_ASSERT_ALIGNED( begin, 64 );	// check 64bytes boundaries
	NV_ASSERT_ALIGNED( end, 64 );
	sceKernelDcacheWritebackRange ( (char*)begin, uint32(end)-uint32(begin) );
}

inline void FlushDCache	(	)
{
	sceKernelDcacheWritebackAll();
}



//
// Cache Prefetching

inline void PrefetchDCache( pvoid start, uint size )
{
	//sceKernelDcacheFillRange( inAddr, inBSize );	
	unsigned int uiStart, uiEnd;
	unsigned int ui;

#if defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus)
	uiStart = reinterpret_cast<unsigned int>(start);
#else	/* defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus) */
	uiStart = (unsigned int)start;
#endif	/* defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus) */

	/*J 指定されたアドレスが非キャッシュ領域であるかどうかチェック */
	/*E check whether the specified address is un-cached region    */
	if (__builtin_allegrex_ext(uiStart, 30, 1)!=0) {
		return;
	}

	uiEnd = uiStart + size;
	for (ui=uiStart; ui<uiEnd; ui+=SCE_ALLEGREX_DCACHE_BLOCK_SIZE) {
#if defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus)
		__builtin_allegrex_cache(SCE_ALLEGREX_CACHEOP_FILL_D, reinterpret_cast<void *>(ui));
#else	/* defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus) */
		__builtin_allegrex_cache(SCE_ALLEGREX_CACHEOP_FILL_D, (void *)ui);
#endif	/* defined(_LANGUAGE_C_PLUS_PLUS)||defined(__cplusplus)||defined(c_plusplus) */
	}

}

inline void PrefetchICache( pvoid inAddr, uint inBSize )
{
	register uint32 tmp0, tmp1, tmp2;
	asm volatile (
	"	.set noreorder\n"
	"	add		%0, $0, %3\n"
	"	add		%1, %4, %3\n"
	"0:	sync\n"
	"	cache	0x0e, 0(%0)\n"
	"	sync\n"
	"	addiu	%0, %0, 64\n"
	"	nop\n"
	"	slt		%2, %0, %1\n"
	"	nop\n"
	"	bgtz	%2, 0b\n"
	"	nop\n"
	"	.set reorder\n"
	  : "=&r"(tmp0), "=&r"(tmp1), "=&r"(tmp2)
	  : "r"(inAddr), "r"(inBSize) );
}

inline void InvalidateICache( pvoid inAddr, uint inBSize )
{
	sceKernelIcacheInvalidateRange( inAddr, inBSize );
}


//
// Interrupt

#define	DI		sceKernelCpuSuspendIntr
#define	EI		sceKernelCpuResumeIntr
#define	SEI		sceKernelCpuResumeIntrWithSync



//
// Format

inline uint32 ExtFp32( float inF )
{
	return AS_UINT32(inF);
}

inline uint32 ExtFp24( float inF )
{
	return AS_UINT32(inF) >> 8;
}

inline uint32 ExtFp16( float inF )
{
	return AS_UINT32(inF) >> 16;
}

// positions are bits to right/left-shift
// positions & size must be integral values (r-values) !
#define		ExtBits( v, pos, size )							__builtin_allegrex_ext( v, pos, size )
#define		InsBits( vdst, vsrc, posdst, size )				__builtin_allegrex_ins( vdst, vsrc, posdst, size )
#define		CopyBits( vdst, posdst, vsrc, possrc, size )	InsBits( vdst, ExtBits(vsrc,possrc,size), posdst, size )
#define		RevBits( v, pos, size )							CopyBits( v, pos, ~v, pos, size )



//
// GE cmds

#ifndef	SCE_GE_SET_SIGNAL_SIGNAL_WAIT
#define SCE_GE_SIGNAL_SIGNAL_WAIT					0x01
#define SCE_GE_SIGNAL_SIGNAL_NOWAIT					0x02
#define SCE_GE_SIGNAL_SIGNAL_PAUSE					0x03
#define SCE_GE_SET_SIGNAL_SIGNAL_WAIT(_id)			((SCE_GE_CMD_SIGNAL<<24) | (SCE_GE_SIGNAL_SIGNAL_WAIT<<16) | ((_id) & 0xFFFF))
#define SCE_GE_SET_SIGNAL_SIGNAL_NOWAIT(_id)		((SCE_GE_CMD_SIGNAL<<24) | (SCE_GE_SIGNAL_SIGNAL_NOWAIT<<16) | ((_id) & 0xFFFF))
#define SCE_GE_SET_SIGNAL_SIGNAL_PAUSE(_id)			((SCE_GE_CMD_SIGNAL<<24) | (SCE_GE_SIGNAL_SIGNAL_PAUSE<<16) | ((_id) & 0xFFFF))
#endif

#define	SCE_GE_SET_VTYPE_ZERO()						SCE_GE_SET_VTYPE(0,0,0,0,0,0,0,0,0)
#define	GET_VTYPE_TT( inVTYPE )						ExtBits( inVTYPE, 0, 2 )
#define	GET_VTYPE_CT( inVTYPE )						ExtBits( inVTYPE, 2, 3 )
#define	GET_VTYPE_NT( inVTYPE )						ExtBits( inVTYPE, 5, 2 )
#define	GET_VTYPE_VT( inVTYPE )						ExtBits( inVTYPE, 7, 2 )
#define	GET_VTYPE_WT( inVTYPE )						ExtBits( inVTYPE, 9, 2 )
#define	GET_VTYPE_IT( inVTYPE )						ExtBits( inVTYPE, 11, 2 )
#define	GET_VTYPE_WC( inVTYPE )						ExtBits( inVTYPE, 14, 3 )
#define	GET_VTYPE_MC( inVTYPE )						ExtBits( inVTYPE, 18, 3 )
#define	GET_VTYPE_TRU( inVTYPE )					ExtBits( inVTYPE, 23, 1 )

#define	GET_PRIM_COUNT( inPRIM )					ExtBits( inPRIM, 0, 16 )
#define	GET_PRIM_PRIM( inPRIM )						ExtBits( inPRIM, 16, 3 )



inline void BuildGeMatrix( uint32* outGeM, float* inS, uint inSize, uint inCmd )
{
	inCmd &= 0xFF;
	register uint32* sm     = (uint32*) inS;
	register uint32* sm_end = sm + inSize;
	while( sm != sm_end )
		*outGeM++ = InsBits( ExtBits(*sm++,8,24), inCmd, 24, 8 );
}

inline void BuildGeMatrix16( uint32* outGeM, const Matrix* inM, uint inCmd )
{
	inCmd &= 0xFF;
	register uint32* sm  = (uint32*) inM->m;
	outGeM[0]  = InsBits( ExtBits(sm[0],8,24),  inCmd, 24, 8 );
	outGeM[1]  = InsBits( ExtBits(sm[1],8,24),  inCmd, 24, 8 );
	outGeM[2]  = InsBits( ExtBits(sm[2],8,24),  inCmd, 24, 8 );
	outGeM[3]  = InsBits( ExtBits(sm[3],8,24),  inCmd, 24, 8 );
	outGeM[4]  = InsBits( ExtBits(sm[4],8,24),  inCmd, 24, 8 );
	outGeM[5]  = InsBits( ExtBits(sm[5],8,24),  inCmd, 24, 8 );
	outGeM[6]  = InsBits( ExtBits(sm[6],8,24),  inCmd, 24, 8 );
	outGeM[7]  = InsBits( ExtBits(sm[7],8,24),  inCmd, 24, 8 );
	outGeM[8]  = InsBits( ExtBits(sm[8],8,24),  inCmd, 24, 8 );
	outGeM[9]  = InsBits( ExtBits(sm[9],8,24),  inCmd, 24, 8 );
	outGeM[10] = InsBits( ExtBits(sm[10],8,24), inCmd, 24, 8 );
	outGeM[11] = InsBits( ExtBits(sm[11],8,24), inCmd, 24, 8 );
	outGeM[12] = InsBits( ExtBits(sm[12],8,24), inCmd, 24, 8 );
	outGeM[13] = InsBits( ExtBits(sm[13],8,24), inCmd, 24, 8 );
	outGeM[14] = InsBits( ExtBits(sm[14],8,24), inCmd, 24, 8 );
	outGeM[15] = InsBits( ExtBits(sm[15],8,24), inCmd, 24, 8 );
}

inline void BuildGeMatrix12( uint32* outGeM, const Matrix* inM, uint inCmd )
{
	inCmd &= 0xFF;
	register uint32* sm = (uint32*) inM->m;
	outGeM[0]  = InsBits( ExtBits(sm[0],8,24),  inCmd, 24, 8 );
	outGeM[1]  = InsBits( ExtBits(sm[1],8,24),  inCmd, 24, 8 );
	outGeM[2]  = InsBits( ExtBits(sm[2],8,24),  inCmd, 24, 8 );
	outGeM[3]  = InsBits( ExtBits(sm[4],8,24),  inCmd, 24, 8 );
	outGeM[4]  = InsBits( ExtBits(sm[5],8,24),  inCmd, 24, 8 );
	outGeM[5]  = InsBits( ExtBits(sm[6],8,24),  inCmd, 24, 8 );
	outGeM[6]  = InsBits( ExtBits(sm[8],8,24),  inCmd, 24, 8 );
	outGeM[7]  = InsBits( ExtBits(sm[9],8,24),  inCmd, 24, 8 );
	outGeM[8]  = InsBits( ExtBits(sm[10],8,24), inCmd, 24, 8 );
	outGeM[9]  = InsBits( ExtBits(sm[12],8,24), inCmd, 24, 8 );
	outGeM[10] = InsBits( ExtBits(sm[13],8,24), inCmd, 24, 8 );
	outGeM[11] = InsBits( ExtBits(sm[14],8,24), inCmd, 24, 8 );
}




//
// PSP system clock
// System time is in units of microseconds.

inline uint32 GetCycleCpt()
{
	return sceKernelGetSystemTimeLow();
}

inline uint32 GetTimeCycleCpt( float inTime )
{
	const float freq = 1000000.0F;
	return uint32(freq * inTime);
}

inline float GetCycleCptTime( uint32 inCycCpt )
{
	const float per1 = 1.0F / 1000000.0F;
	const float per2 = 2.0F * float(0x40000000L) / 1000000.0F;
	if( inCycCpt&0x80000000UL ) {
		inCycCpt ^= 0x80000000UL;
		return float(int(inCycCpt)) * per1 + per2;
	} else {
		return float(int(inCycCpt)) * per1;
	}
}

inline void WaitCycleCpt( uint32 inCycCpt )
{
	// Pas mieux !
	while( inCycCpt-- ) {}
}








#endif	// _ALG_DEFS_H_



