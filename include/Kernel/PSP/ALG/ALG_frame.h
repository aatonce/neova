/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _ALG_FRAME_H_
#define _ALG_FRAME_H_



namespace frame {


	//
	// Frame's Performer
	interface Packet;

	struct Agent : public NvInterface {
		virtual	NvInterface*	GetBase			(							) {		return this;	}
		virtual	void			GeneratePacket	(	dmac::Cursor*	inDC,
													Packet*			inPkt	) {}	// called if packet.cadr == NULL !
		virtual	void			UpdatePacket	(	Packet*			inPkt	) {}	// called if packet.SetNeedUpdate() is TRUE !
	};



	//
	// Packet's GE Context
	// Asserts ATE=ABE=ZTE=STE=CTE=LOE=1

	struct GeCallList {
		// viewing							// GE cmd				Init value (- is CMD_NOP)	Modified
		uint32			atest;				// CMD_ATEST			-							Y
		uint32			blend;				// CMD_BLEND			-							Y
		uint32			fix[2];				// CMD_FIXA/FIXB		-							Y
		uint32			pmsk[2];			// CMD_PMSK1/PMSK2		-							Y
		uint32			ztest;				// CMD_ZTEST			-							Y
		uint32			zmsk;				// CMD_ZMSK				-							Y
		uint32			stest;				// CMD_STEST			-							Y
		uint32			bce;				// CMD_BCE				-							Y
		uint32			cull;				// CMD_CULL				-							Y
		// texturing
		uint32			tme;				// CMD_TME				-							Y
		uint32			tflush_or_ret;		// CMD_TFLUSH/RET		CMD_RET						Y (CMD_TFLUSH or CMD_RET according to tme)
		uint32			tfunc;				// CMD_TFUNC			-							Y (according to tme)
		uint32			tfilter;			// CMD_TFILTER			-							Y (according to tme)
		uint32			tlevel;				// CMD_TLEVEL			-							Y (according to tme)
		uint32			twrap;				// CMD_TWRAP			-							Y (according to tme)
		uint32			tmap;				// CMD_TMAP				-							Y (according to tme)
		uint32			suv[2];				// CMD_SU/SV			-							Y (according to tme & ST packing)
		uint32			tuv[2];				// CMD_TU/TV			-							Y (according to tme & ST packing)
		// texture setup
		uint32			cbp;				// CMD_CBP				-							Y (according to tme)
		uint32			cbw;				// CMD_CBW				-							Y (according to tme)
		uint32			clut;				// CMD_CLUT				-							Y (according to tme)
		uint32			cload;				// CMD_CLOAD			-							Y (according to tme)
		uint32			tbp[4];				// CMD_TBP0-3			-							Y (according to tme)
		uint32			tbw[4];				// CMD_TBW0-3			-							Y (according to tme)
		uint32			tsize[4];			// CMD_TSIZE0-3			-							Y (according to tme)
		uint32			tpf;				// CMD_TPF				-							Y (according to tme)
		uint32			tmode;				// CMD_TMODE			-							Y (according to tme)
		// return
		uint32			ret;				// CMD_RET				CMD_RET						*NO*
	};

	struct GeContext {
		GeCallList		cmd;
		pvoid			cadr;				// Called-list addr
		int16			texId;				// tram:: texture id (-1 = none)
		uint8			texLODMask;			// tex LOD mask

		void			Init				(								);
		void			SetUser				(	uint32*		inMADR			);
		void			RestoreDefault		(								);
	};


	//
	// Frame's Packet

	struct Packet {
		uint32			hkey;				// sort hash-key
		uint64			enterContextMask;	// enter context mask
		uint64			leaveContextMask;	// leave context mask
		Agent*	 		owner;				// Agent owner of the packet
		pvoid			cadr;				// Called-list addr (generated when NULL)
		GeContext*		ctxtA;				// GE contexts
		uint16			ctxtCpt;			// #contexts

		void			Init				(	Agent*		inOwner	= NULL	);
		void			BuildBasicHKey		(								);
		void			SetSeparator		(	bool		inEnable		);		// separator  ?
		void			SetNeedUpdate		(	bool		inEnable		);		// owner->UpdatePacket() ?
		bool			IsDrawing			(								);
		bool			IsTextured			(								);

		// private:
		enum {
			FLG_SEPARATOR		= (1<<0),	// => { Sort(); Push(); Sort(); }
			FLG_UPD_REQUESTED	= (1<<1),	// update event requested on packet
			STT_DRAWING			= (1<<2),	// Is drawing ?
			STT_QUEUING			= (1<<3)	// Is queuing ?
		};
		uint16			flags;				// reserved flags
	};


	//
	// Init/Reset

	void		Init					(													);
	void		Reset					(													);
	void		Shut					(													);


	//
	// Frame's Queue Building

	void		Begin					(	bool		inASync	= TRUE						);		// Asynchronous ?
	void		Push					(	Packet*		inPkt								);
	void		Sort					(	/* according to Packet.hkey */					);
	void		End						(													);


	//
	// Frame Sync & Flush

	void		WaitDMA					(													);
	void		Sync					(													);
	void		Flush					(	bool		inVSync		= TRUE,							// Waits V-blank ?
											bool		inShowFront	= FALSE					);


	//
	// Statistics

	enum Performance {
		PERF_BEGIN_END		= 0,
		PERF_BEGIN_BEGIN,
		PERF_SYNC_UPDATE,
		PERF_SYNC_UPDATE_IDLE,
		PERF_PREPARE_FLUSH,
		PERF_VSYNC_IDLE,
		PERF_FLUSH,
		PERF_DMA,
		PERF_WAIT_DMA,
		PERF_TRXD_TEXBSIZE
	};

	float		GetPerformance			(	Performance	inPerf								);
}


#include "ALG_frame_I.h"


#endif // _ALG_FRAME_H_


