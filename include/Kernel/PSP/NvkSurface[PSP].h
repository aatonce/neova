/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkSurface_H_
#define	_NvkSurface_H_


#include "NvDpyManager[PSP].h"
#include <NvSurface.h>


interface NvkSurface : public NvResource
{
	NVKITF_DECL( Surface )

	enum Buffer {
		BF_BACK			= 0,
		BF_FRONT		= 1
	};

	enum SubBuffer {
		SBF_VTX			= 0,			// Vertex buffer
		SBF_KICK		= 1,			// Kick buffer: array of uint32, one bit per vertex
		SBF_IDX			= 2,			// Index buffer: array of uint16, one or more per vertex, up to maxSize*2
		SBF_ILT			= 3				// Vertex's start index lookup table: array of uint16, one per vertex
	};

	enum {
		EN_PRESENT		= (1<<0),		// Enable the present process.
		EN_BEGIN_END	= (1<<1),		// Enable the Begin()/End() sequence from the user interface.
		EN_GENERATE_IDX	= (1<<2),		// Enable the GenerateIdxFromKick() call in the present process.
		EN_ALL			= 0x7,
		EN_DEFAULT		= EN_ALL
	};

	NvInterface*				GetBase				(												);
	void						AddRef				(												);
	uint						GetRefCpt			(												);
	void						Release				(												);
	uint32						GetRscType			(												);
	uint32						GetRscUID			(												);
	NvSurface*					GetInterface		(												);

	uint						GetComponents		(												);
	bool						HasComponent		(	NvSurface::Component	inComponent			);
	NvSurface::PackMode			GetPackingMode		(												);
	uint						GetMaxSize			(												);
	static Psm					GetNativeColorPSM	(												);

	bool						Resize				(	uint					inMaxSize			);
	void						EnableDBF			(	NvSurface::PresentMode	inPresentMode,
														bool					inCreateNow			);
	void						DisableDBF			(	bool					inReleaseNow		);
	bool						IsEnabledDBF		(												);
	bool						HasDBF				(												);

	void						Enable				(	uint					inEnableFlags		);	// bit-mask of EN_*
	void						Disable				(	uint					inEnableFlags		);	// bit-mask of EN_*
	bool						IsEnabled			(	uint					inEnableFlags		);	// bit-mask of EN_*

	bool						PresentBuffer		(												);
	uint8*						AllocBuffer			(												);
	void						FreeBuffer			(	uint8*					inBuffer			);
	uint32						GetBufferVTYPE		(												);
	uint						GetBufferBSize		(												);
	uint8*						GetBufferBase		(	Buffer					inBuffer			);
	uint8*						GetSubBufferBase	(	Buffer					inBuffer,
														SubBuffer				inSubBuffer			);

	void						GenerateIdxFromKick	(	Buffer					inBuffer			);	// Generate IDX & ILT sub-suffers from the kick buffer

	uint8*						GetComponentBase	(	Buffer					inBuffer,
														NvSurface::Component	inComponent,
														uint*					outStride	= NULL,
														uint*					outPsm		= NULL	);	// SCE_GE_VTYPE_*

	bool						GetBufferDrawing	(	Buffer					inBuffer,
														uint					inOffset,
														uint					inSize,
														uint32*					outGeCmds			);	// 0:BASE 1:VADR 2:IADR 3:VTYPE 4:PRIM

	void						UpdateBeforeDraw	(												);
	bool						IsDrawing			(												);

	// Utilities
	static
	NvSurface::Component		CoFromNative		(	ge::VertexMap::Component	inComponent		);

	static
	ge::VertexMap::Component	CoToNative			(	NvSurface::Component	 	inComponent		);
};



#endif // _NvkSurface_H_


