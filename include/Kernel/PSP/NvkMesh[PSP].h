/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkMesh_H_
#define	_NvkMesh_H_


#include "NvDpyManager[PSP].h"
#include <NvMesh.h>


interface NvkMesh : public NvResource
{
	NVKITF_DECL( Mesh )

	struct Shading {
		uint32				nameCRC;			// 0 is unnamed
		uint32				emissive;			// ABGR in [0,255]
		uint32				ambient;			// ABGR in [0,255]
		uint32				diffuse;			// ABGR in [0,255]
		uint32				specular;			// ABGR in [0,255]
		float				glossiness;
		float				specLevel;
		uint32				stateEnFlags;		// DpyState::EnableFlags
		uint32				stateMdFlags;		// DpyState::ModeFlags
		float				stateBlendCte;		// DpyState blend-source-cte
		float				stateAlphaPass;		// DpyState alpha-pass-threshold
		uint32				bitmapUID;
		float				mappingRatio;		// For L&K auto-computation
		float				mipOFFL;			// DpyState OFFL
		uint32				isFullOpaque;		// 0/1
	};

	struct BunchBBox {
		Box3				bbox;				// OBB
		uint32				envelop[8*3];		// Max bsize is Vec3 x 8
	};

	struct BunchData {
		uint16				surfNo;				// surface idx
		uint16				bunchNo;			// bunch idx
		uint32				vadrBOffset;		// bunch vertex-buffer byte-offset, relative to the global dmaSection
		uint32				iadrBOffset;		// bunch index-buffer byte-offset, relative to the global dmaSection
		uint32				prim;				// ge PRIM register
		uint16				nextByBunch;		// link in a 'bunchByBunch' list, i.e. next bunch part with a different surf (itself=last)
		uint16				nextBySurf;			// link in a 'bunchBySurf' list, i.e next bunch part with the same surf (itself=last)
	};

	struct BunchList {
		uint16				size;				// list size (>=0)
		uint16				startIdx;			// list start idx in bunchDataA
	};

	struct Bunch {
		uint32				faceCpt;			// # face
		uint32				locCpt;				// # locations
		uint32				vtxCpt;				// # welded vertex
		uint32				primCpt;			// # stripped vertex
		uint16				boneIdx[3];			// bones weighting the bunch vertices
		uint16				surfCpt;			// # surface for the bunch
	};

	struct BunchBlend {
		uint32*				boneBlend[3];
	};

	struct ShadowingVtx {
		Vec3				location;
		uint32				boneCRC;			// none if 0
	};

	struct ShadowingFace {
		int16				vtx[3];				// face vertex indices (always >= 0)
		int16				adj[3];				// face adjacency (same faceNo for none)
	};

	NvInterface*			GetBase				(											);
	void					AddRef				(											);
	uint					GetRefCpt			(											);
	void					Release				(											);
	uint32					GetRscType			(											);
	uint32					GetRscUID			(											);
	NvMesh*					GetInterface		(											);

	Box3*					GetBBox				(											);
	uint32					GetNameCRC			(											);
	bool					IsSkinnable			(											);
	bool					IsNrmSkinnable		(											);
	bool					IsMorphable			(											);
	bool					IsLightable			(											);
	bool					IsShadowing			(											);
	bool					IsFullOpaque		(											);

	void					GetUnpacking		(	float*				outLocUS = NULL,
													Vec3*				outLocT	 = NULL,
													float*				outTexUS = NULL,
													Vec2*				outTexT	 = NULL		);

	uint					GetSurfaceCpt		(											);
	Shading*				GetSurfaceShadingA	(											);

	uint					GetBunchCpt			(											);
	uint					GetBunchDataCpt		(											);
	uint32					GetBunchDataBase	(											);
	uint32					GetBunchDataVTYPE	(											);
	Bunch*					GetBunchA			(											);
	BunchBBox*				GetBunchBBoxA		(											);
	BunchData*				GetBunchDataA		(											);
	BunchBlend*				GetBunchBlendA		(											);
	BunchList*				GetBunchByBunchA	(											);
	BunchList*				GetBunchBySurfA		(											);

	void					RefBitmaps			(											);
	void					UnrefBitmaps		(											);

	bool					GetShadowing		(	uint&				outVtxCpt,
													uint&				outFaceCpt,
													ShadowingVtx*&		outVtxA,
													ShadowingFace*&		outFaceA			);

	void					UpdateBeforeDraw	(											);
	bool					IsDrawing			(											);
};



#endif // _NvkMesh_H_



