/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvDpyState_PSP_H_
#define	_NvDpyState_PSP_H_


#include "NvDpyManager[PSP].h"
#include "NvkBitmap[PSP].h"




interface DpyState_PSP : public DpyState
{
	enum CapabilityFlags {
		CF_HAS_RGB		= (1<<0),
		CF_HAS_RGBA		= (1<<1),
		CF_HAS_NORMAL	= (1<<2),
		CF_HAS_TEX0		= (1<<3),
		CF_HAS_TEX1		= (1<<4),
		CF_HAS_TEX2		= (1<<5),
		CF_HAS_TEX3		= (1<<6)
	};


	// Parameters
	uint				mode;
	DpySource			src;
	Vec2				srcT;
	Vec2				srcUnpackT;
	float				srcUnpackS;
	float				alphaPass;
	float				blendCte;
	uint32				wrColorMask;
	uint8				mipOFFL;
	uint8				caps;
	uint8				enableOp;
	uint8				enableOpMask;
	bool				changed;


	// Init/Shut
	void				Init				(											);
	void				Shut				(											);

	// Enable flags
	void				Enable				(	uint				inEnableFlags		);		// mask of EnableFlags
	void				Disable				(	uint				inEnableFlags		);		// mask of EnableFlags
	void				SetEnabled			(	uint				inEnableFlags		);		// mask of EnableFlags
	uint				GetEnabled			(											);		// mask of EnableFlags
	void				SetEnabledMask		(	uint				inEnableFlags		);		// mask of EnableFlags
	uint				GetEnabledMask		(											);		// mask of EnableFlags
	uint				GetGlobalEnabled	(											);		// is (enableOp & enableOpMask)

	// Caps
	void				SetCapabilities		(	uint				inCapabilities		);		// mask of CapabilityFlags
	uint				GetCapabilities		(											);		// mask of CapabilityFlags

	// Mode flags
	uint				UpdateMode			(	uint				inMode,
												uint				inMask,
												uint				inFlags				);
	void				SetMode				(	uint				inModeFlags			);
	uint				GetMode				(											);

	// State constants
	bool				SetSource			(	DpySource			inSource			);
	void				GetSource			(	DpySource&			outSource			);
	void				SetSourceOffset		(	const Vec2&			inOffset			);
	void				SetSourceUnpack		(	float				inScale,
												const Vec2&			inOffset			);
	void				GetSourceOffset		(	Vec2&				outOffsetXY			);		// in [0,1]^2
	bool				SetAlphaPass		(	float				inAlphaPass			);		// in [0,1]
	float				GetAlphaPass		(											);		// in [0,1]
	bool				SetBlendSrcCte		(	float				inCte				);		// in [0,1]
	float				GetBlendSrcCte		(											);		// in [0,1]
	void				SetWRColorMask		(	uint32				inRGBAMask			);
	uint32				GetWRColorMask		(											);

	// Check
	bool				HasChanged			(											);
	bool				HasValidSource		(											);
	bool				IsTexturing			(											);

	// PSP specific
	void				SetMipmapping		(	float				inOffset			);

	NvkBitmap*			SetTexturing		(	uint32				inBitmapUID,
												float				inMappingOffset,
												float				inMappingRatio		);

	void				Activate			(	frame::GeContext*	ioContext			);
};



#endif	// _NvDpyState_PSP_H_



