/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_GEK_DEFS_H_
#define	_GEK_DEFS_H_

#include <dolphin.h>
#include <dolphin/gd.h>
#include <dolphin/gx.h>


// Color Convertion : uint32 to GXColor;
union GekColor {
	uint32	c;
	GXColor	gxc;
};

template <typename T>
inline T* PhysicalPointer( T* inPtr )
{
	return (T*)( uint32(inPtr) & 0x3FFFFFFFU );
}

template <typename T>
inline T* CachedPointer( T* inPtr )
{
	uint32 paddr = uint32(inPtr) & 0x3FFFFFFFU;
	return (T*)( paddr | OS_BASE_CACHED );
}

template <typename T>
inline T* UncachedPointer( T* inPtr )
{
	uint32 paddr = uint32(inPtr) & 0x3FFFFFFFU;
	return (T*)( paddr | OS_BASE_UNCACHED );
}



//
// Pipelines sync

inline void SyncMemory()
{
	PPCSync (  );
}

inline void SyncPipeline()
{
	PPCSync (  );
}

inline void SyncRW()
{
	PPCSync (  );
}

inline void SyncDCache( void* begin, void* end )
{
	DCStoreRange( begin, uint32(end)-uint32(begin) );
}

inline void SyncDCache( void* begin, uint32 inBSize )
{
	DCStoreRange( begin, inBSize );
}



//
// EFB Direct

#define	EFB_COLOR_ADDR		0x08000000
#define	EFB_DEPTH_ADDR		0x08400000

inline uint32* efb_ARGBAddr( u16 x, u16 y )
{
	return (uint32*)( (uint32)OSPhysicalToUncached(EFB_COLOR_ADDR) + (y<<12) + (x<<2) );
}

inline uint32* efb_DepthAddr( u16 x, u16 y )
{
	return (uint32*)( (uint32)OSPhysicalToUncached(EFB_DEPTH_ADDR) + (y<<12) + (x<<2) );
}

inline void efb_PeekARGB( u16 x, u16 y, uint32* color	)
{
	*color = *efb_ARGBAddr(x,y);
}

inline void efb_PokeARGB( u16 x, u16 y, uint32 color )
{
	*efb_ARGBAddr(x,y) = color;
}





#endif	// _GEK_DEFS_H_



