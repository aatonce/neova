/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _GEK_DMAC_H_



inline
void
dmac::Cursor::Memcpy( void * inPtr, uint inBSize )
{
	NV_ASSERT_DC( this );
	NV_ASSERT( inPtr );
	nv::libc::Memcpy( vd, inPtr, inBSize );
	i8 += inBSize;
}


inline
void*
dmac::Cursor::Dup(	void *	inStart, uint inBAlignement	)
{
	NV_ASSERT_DC( this );
	uint8* dup_start = inStart ? (uint8*)inStart : start;
	NV_ASSERT( (dup_start>=start) && (dup_start<(start+bsize)) );
	uint memSize = uint32(i8) - uint32(dup_start);
	void * dup_copy = nv::EngineMallocA( memSize, inBAlignement );
	nv::libc::Memcpy( CachedPointer(dup_copy), CachedPointer(dup_start), memSize );
	return dup_copy;
}


inline
uint32
dmac::Cursor::GetLeftBSize()
{
	NV_ASSERT_DC( this );
	if( !start )			return 0;
	NV_ASSERT( i8 >= start );
	if( i8 < start )		return 0;
	uint32 offset = uint32(i8) - uint32(start);
	NV_ASSERT( offset <= bsize );
	return ( offset >= bsize ? 0 : bsize-offset );
}


inline
uint32
dmac::Cursor::GetUsedBSize()
{
	if( !start )			return 0;
	NV_ASSERT( i8 >= start );
	if( i8 < start )		return 0;
	uint32 offset = uint32(i8) - uint32(start);
	NV_ASSERT( offset <= bsize );
	return ( offset >= bsize ? bsize : offset );
}


inline
void
dmac::Cursor::Align32()
{
	uint32 r = RoundX( uint32(i8), 4 );
	i8 += ( r - uint32(i8) );
}


inline
void
dmac::Cursor::Align64()
{
	uint32 r = RoundX( uint32(i8), 8 );
	i8 += ( r - uint32(i8) );
}


inline
void
dmac::Cursor::Align128()
{
	uint32 r = RoundX( uint32(i8), 16 );
	i8 += ( r - uint32(i8) );
}


inline
void
dmac::Cursor::Align256()
{
	uint32 r = RoundX( uint32(i8), 32 );
	i8 += ( r - uint32(i8) );
}


inline
void
dmac::Cursor::Align512()
{
	uint32 r = RoundX( uint32(i8), 64 );
	i8 += ( r - uint32(i8) );
}


inline
void
dmac::Cursor::Align1QW()
{
	Align128();
}


inline
void
dmac::Cursor::Align2QW()
{
	Align256();
}


inline
void
dmac::Cursor::Align4QW()
{
	Align512();
}


#endif // _GEK_DMAC_H_


