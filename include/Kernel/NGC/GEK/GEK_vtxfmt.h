/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifndef _GEK_VTXFMT_H_
#define _GEK_VTXFMT_H_



namespace vtxfmt
{
	enum Fmt {
		FMT_NONE		= 0,
		FMT_S8			= 1,
		FMT_S16			= 2,
		FMT_F32			= 3,
		FMT_RGBA32		= FMT_F32
	};

	enum Component {
		CO_LOC_S16		= (FMT_S16<<0),			// short [3]
		CO_LOC_F32		= (FMT_F32<<0),			// float [3]
		CO_TEX_S16		= (FMT_S16<<2),			// short [2]
		CO_TEX_F32		= (FMT_F32<<2),			// float [2]
		CO_COLOR		= (FMT_RGBA32<<4),		// uint32
		CO_NORMAL_S8	= (FMT_S8<<6),			// char  [3]
		CO_NORMAL_F32	= (FMT_F32<<6),			// float [3]
		
		CO_LOC_MASK		= (3<<0),
		CO_TEX_MASK		= (3<<2),
		CO_COL_MASK		= (3<<4),
		CO_NRM_MASK		= (3<<6),						
	};

	void		Init			(										);
	void 		Shut			(										);
	void 		Reset			(										);

	GXVtxFmt 	Probe			(	uint			inComponents		);		// mask of CO_ flags
	GXVtxFmt 	Activate		(	uint			inComponents,				// mask of CO_ flags
									int				inLocFrac = 0		);		// loc attr frac

	uint		GetStride		(	Component		inComponent			);

	GXCompType	GetType			(	Component		inComponent			);
}



#endif //_GEK_VTXFMT_H_


