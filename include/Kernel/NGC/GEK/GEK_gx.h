/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _GEK_GX_H_
#define _GEK_GX_H_



namespace gx
{

	enum VMode
	{
		VM_SELECT				= 0,
		VM_NTSC_240_DS			= 1,
		VM_NTSC_240_INT			= 2,
		VM_NTSC_480_INT_DF		= 3,
		VM_NTSC_480_INT			= 4,
		VM_NTSC_480_PROG		= 5,
		VM_NTSC_480_PROG_SOFT	= 6,
		VM_MPAL_240_DS			= 7,
		VM_MPAL_240_INT			= 8,
		VM_MPAL_480_INT_DF		= 9,
		VM_MPAL_480_INT			= 10,
		VM_PAL_264_DS			= 11,
		VM_PAL_264_INT			= 12,
		VM_PAL_528_INT_DF		= 13,
		VM_PAL_528_INT			= 14,
		VM_EURGB60_240_DS		= 15,
		VM_EURGB60_240_INT		= 16,
		VM_EURGB60_480_INT_DF	= 17,
		VM_EURGB60_480_INT		= 18,
		VM_EURGB60_480_PROG		= 21,				
		VM_PAL_574_INT			= 19,
		VM_PAL_OVERSCAN_INT1	= 20,
		VM_PAL_OVERSCAN_INT2	= 21,
		
		VM_CUSTOM				= 23,
	};

	enum CopyFormat  
	{
		//------ Z Format ------
		CF_Z8 		= 0, // The 8 more significant bits
		CF_Z8M		= 1,
		CF_Z8L		= 2,
		CF_Z16L		= 3,
		CF_Z16 		= 4, // The 8 more significant bits		
		CF_Z24X8 	= 5, // 
		//---- Color Format ----
		CF_RGB565	= 6,  
		CF_RGB5A3	= 7, 	
		CF_RGBA8	= 8,
		CF_R8		= 9,
		//----------------------
		CF_MAX		= 10,
	};
	
	void				Init				(												);
	void				Shut				(												);

	void				ResetRegisters		(												);
	void				ResetCRTC			(	bool						inEnableCRTC	);
	void				Reset				(	bool						inEnableCRTC	);

	// Video mode

	void				Setup				(												);
	void				Setup				(	VMode						inMode			);
	void				Setup				(	const GXRenderModeObj&		inMode			);
	GXRenderModeObj*	GetVMode			(												);


	// EFB

	int					GetWidth			(												);
	int					GetHeight			(												);


	// XFB

	void*				GetXFBBase			(												);
	uint				GetXFBLineStride	(												);
	int					GetXFBWidth			(												);
	int					GetXFBHeight		(												);
	int					GetVirtualWidth		(												);
	int					GetVirtualHeight	(												);

	uint32				GetVSyncRate		(												);
	volatile uint32		GetVSyncCpt			(												);


	// Display

	void				EnableCRTC			(												);
	void				DisableCRTC			(												);
	void				TranslateCRTC		(	int							inX0,
												int							inY0			);
	bool				IsEvenFrame			(												);
	void				VSync				(												);


	// Drawing

	void				Present				(	bool						inDoClear		);
	void				Flush				(												);
	void				Sync				(												);
	void				ClearBack			(	uint32						inClrColor		);
	void				CopyTex				( 	uint8 *						ioBuffer		,
												CopyFormat					inFormat		,
												uint						inWidth		= 0	,
												uint						inHeight	= 0	,
												uint						inX			= 0	,
												uint						inY			= 0 );
								

	// Direct EFB

	void				ClearBack_Direct	(	const Vec4&					inXYWH,
												uint32						inRGBA			);

	void				CopyBack_Direct		(	const Vec2&					inToXY,
												const Vec2&					inFromXY,
												const Vec2&					inWH			);

	void				PutBack24_Direct	(	const Vec4&					inXYWH,
												uint8*						inData,
												uint						inStride		);

	void				PutBack32_Direct	(	const Vec4&					inXYWH,
												uint8*						inData,
												uint						inStride		);

	void				GetBack_Direct		(	const Vec4&					inXYWH,
												uint8*						inBuffer,
												uint						inStride		);

	void				GetDepth_Direct		(	const Vec4&					inXYWH,
												uint8*						inBuffer,
												uint						inStride		);

	void				EncodeXFBLine		(	uint32*						inFromRGBA,
												uint8*						inToLineAddr,
												uint						inSize			);

	// Get Frame Buffer

	bool				CopyEFBToMem		( 	void * 						ioDest			, 
												uint 						inLeft 			, 
												uint 						inTop 			, 
												uint 						inWidth			, 
												uint 						inHeight 		, 
												GXTexFmt	 				inFmt			);

}



#endif // _GEK_GX_H_


