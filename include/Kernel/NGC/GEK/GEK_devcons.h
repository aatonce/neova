/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifndef _NGC_DEVCONS_H_
#define _NGC_DEVCONS_H_


namespace devcons
{	

	struct RawBitmap
	{
		byte*		pixels;		// raw format RGB[A]
		uint		bsize;
		uint		width;
		uint		height;
	};



	void			Init		(									);
	void			Shut		(									);

	void			GetChar		(	uint32*			outChar,		// u32 [8*8*inYScale]
									uchar			inC,
									uint32			inForeCol,
									uint32			inBackCol,
									uint			inYScale=1		);

	void 			Clear		(									);
	void			ScrollUp	(									);
	void			Sync		(									);

	void			OutputChar	(	uint				cx,
									uint				cy,
									char				c			);

	void			OutputRaw	(	uint				px,
									uint				py,
									const RawBitmap&	raw			);

	void			Locate		(	uint				cx,
									uint				cy			);

	void			Printf		(	pcstr				inText		);


	void 			SafeClear		(									);
	void			SafeSync		(									);
	void			SafeOutputRaw	(	uint				px,
										uint				py,
										const RawBitmap&	raw			);
	
}


#endif //  _NGC_DEVCONS_H



