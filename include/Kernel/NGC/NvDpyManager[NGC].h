/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvDpyManager_NGC_H_
#define	_NvDpyManager_NGC_H_


#include "GEK/GEK.h"
#include <NvDpyManager.h>
#include <Kernel/Common/NvDpyContext.h>
interface NvShader;
interface NvkBitmap;
interface NvDpyObject_NGC;




namespace DpyManager
{
	extern const float LIGHT_SCALE_VALUE;
	
	extern CoordinateSystem		coordSystem;
	extern NvDpyContext*		context;

	// Projection
	extern Matrix				clipMatrix;


	// Common Shader Fct
	void						SetupDrawing		(	DpyRaster			inRaster,
														NvDpyContext::View*	inView,
														Matrix*				inWorldTR,
														bool				inThroughMode		);

	void						SetupRaster 		(	DpyRaster			inRaster			);


	// Native access
	bool						IsInFrame			(	NvDpyObject_NGC*	inDpyObj			);
	bool						IsDrawing			(	NvDpyObject_NGC*	inDpyObj			);
	void						SetFrameBreakBefore	(	NvDpyObject_NGC*	inDpyObj			);
	void						SetFrameBreakAfter	(	NvDpyObject_NGC*	inDpyObj			);
	bool						Draw				(	NvDpyObject_NGC*	inDpyObj			);

	bool						GetPerformances		(	NvShader*			inShader,
														uint32&				outTriCpt,				// # triangles
														uint32&				outLocCpt,				// # original locations
														uint32&				outVtxWeldCpt,			// # welded vertex {components}
														uint32&				outVtxProcCpt		);	// # processed vertex

	// Garbager for safe release of frame-dependant objects
	void						AddToGarbager		(	NvInterface*		inITF				);
	void						FlushGarbager		(											);
	
	
	// Off Frame Raster 
	bool				CopyEFBToTex		( 	DpyRaster					inSaveAsRaster	);

	void				GetRasterTex		(	DpyRaster					inSaveAsRaster	,
												GXTexFmt	&				outFmt			,
												uint		&				outWidth		,
												uint		&				outHeight		,
												void		**				outData			);
}



#endif	// _NvDpyManager_NGC_H_


