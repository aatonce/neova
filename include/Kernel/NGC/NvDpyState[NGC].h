/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvDpyState_NGC_H_
#define	_NvDpyState_NGC_H_


#include <Kernel/NGC/GEK/GEK.h>
#include "NvDpyManager[NGC].h"
#include "NvkBitmap[NGC].h"


interface DpyState_NGC : public DpyState
{
	enum GDObjParameter {
		GDDL_Size = 192	,	// bsize
	};
	
	enum CapabilityFlags {
		CF_HAS_RGB		= (1<<0),
		CF_HAS_RGBA		= (1<<1),
		CF_HAS_NORMAL	= (1<<2),
		CF_HAS_TEX0		= (1<<3),
		CF_HAS_TEX1		= (1<<4),
		CF_HAS_TEX2		= (1<<5),
		CF_HAS_TEX3		= (1<<6)
	};
	enum AnisotropicFilterLevel 
	{
		AFL_A1,
		AFL_A2,
		AFL_A4,
	};

	// Parameters
	uint					mode;
	DpySource				src;
	Vec2					srcXY;
	NvkBitmap*				srcBmp;
	float					alphaPass;
	float					blendCte;
	uint32					wrColorMask;
	uint32					defColor;
	uint8					caps;
	uint8					enabled;
	bool					changed;
	float					lod_bias;
	bool					bias_clamp;
	bool					do_edge_lod;
	AnisotropicFilterLevel	aFilter;
		
	GDLObj 				gdDL;
	uchar				dlBuffer[GDDL_Size] ATTRIBUTE_ALIGN(32) ;

	// Init/Shut
	void				Init					(												);
	void				Shut					(												);

	// Copy
	void				Copy					(	const DpyState_NGC&		inRef				);

	// Enable flags
	void				Enable					(	uint					inEnableFlags		);		// mask of EnableFlags
	void				Disable					(	uint					inEnableFlags		);		// mask of EnableFlags
	void				SetEnabled				(	uint					inEnableFlags		);		// mask of EnableFlags
	uint				GetEnabled				(												);		// mask of EnableFlags

	// Caps
	void				SetCapabilities			(	uint					inCapabilities		);		// mask of CapabilityFlags
	uint				GetCapabilities			(												);		// mask of CapabilityFlags

	// Mode flags
	void				SetMode					(	uint					inModeFlags			);
	uint				GetMode					(												);

	// State constants
	bool				SetSource				(	DpySource				inSource			);
	void				GetSource				(	DpySource&				outSource			);
	void				SetSourceOffset			(	const Vec2&				inOffsetXY			);
	void				GetSourceOffset			(	Vec2&					outOffsetXY			);		// in [0,1]^2
	bool				SetAlphaPass			(	float					inAlphaPass			);		// in [0,1]
	float				GetAlphaPass			(												);		// in [0,1]
	bool				SetBlendSrcCte			(	float					inCte				);		// in [0,1]
	float				GetBlendSrcCte			(												);		// in [0,1]
	void				SetWRColorMask			(	uint32					inRGBAMask			);
	uint32				GetWRColorMask			(												);
	void				SetDefaultColor			(	uint32					inRGBAColor			);
	
	// Check
	bool				HasValidSource			(												);
	bool				IsTexturing				(												);

	NvkBitmap*			SetTexturing			(	uint32					inBitmapUID,
													float					inMappingOffset,
													float					inMappingRatio		);

	//NGC Specifics
	void				SetMipmapping			(	float					inBias				,
													bool					inBiasClamp			,	// Prevents over biasing the LOD when the polygon is perpendicular to the view direction
													bool					onEdgeLoad			,	// if true is set, LOD is calculated using adjacent texels. If true is not set, diagonal texels are used.
													AnisotropicFilterLevel	inAnisoFilterLevel	);	

	void				ForceUpdate				(												);
	void				Activate				(												);
	void				ActivateCartoonLighting	(	bool					inIsInkable			);
	
	void				ActivateLightmap		(	DpySource &				inLightmapSrc, 
													uint 					inLightmapMode		);
	
	static void			ActivateOutlining		(	uint32					inCullMode			);
	
	static bool 		FixTexture				(	DpySource & 			inSrc				, 
													uint 					inMode				,
													float					inBias				,
													bool					inBiasClamp			,
													bool					onEdgeLoad			,
													AnisotropicFilterLevel	inAnisoFilterLevel	,
													uint					inTexId 	=	0	);
};



#endif	// _NvDpyState_NGC_H_



