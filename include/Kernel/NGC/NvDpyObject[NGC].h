/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvDpyObject_NGC_H_
#define	_NvDpyObject_NGC_H_



#include "NvDpyManager[NGC].h"



interface NvDpyObject_NGC : public NvInterface
{
	struct DpyCtxt {
		uint32				drawFrameNo;		// #frame
		int16				chainHead;			// in contextChainA
		int16				chainCpt;			// chain length
	};
	
	enum PrimitiveType
	{
		PT_TriangleStrip 	= 0x0,
		PT_Line 			= 0x1,
		PT_LineStrip 		= 0x2,
		PT_Point		 	= 0x3,
	};

	DpyCtxt					dpyctxt;			// display context
	uint					refCpt;				// instance ref counter

			void			InitDpyObject		(											);
			bool			ShutDpyObject		(											);
	virtual	NvInterface*	GetBase				(											) { return this;										}
	virtual	void			AddRef				(											) { refCpt++;											}
	virtual	uint			GetRefCpt			(											) { return refCpt;										}
	virtual	void			Release				(											) { NV_ASSERT_RESULT( ShutDpyObject() );				}
	virtual bool			IsDrawing			(											) { return DpyManager::IsDrawing(dpyctxt.drawFrameNo);	}
	virtual	bool			Draw				(											) = 0;

	// Only available on GeomShader (with NvSurface)
	virtual bool			SetPrimitiveType	(	PrimitiveType 	inPt					) { return FALSE;										}
	
	
	virtual	bool			GetPerformances		(	uint32&			outTriCpt,										// # triangles
													uint32&			outLocCpt,										// # original locations
													uint32&			outVtxWeldCpt,									// # welded vertex {components}
													uint32&			outVtxProcCpt			) { return FALSE; }		// # processed vertex
};




#endif	// _NvDpyObject_NGC_H_


