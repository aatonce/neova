/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkMesh_H_
#define	_NvkMesh_H_

#include <NvMesh.h>


interface NvkMesh : public NvResource
{
	NVKITF_DECL( Mesh )


	struct Shading {
		uint32				nameCRC;					// 0 is unnamed
		uint32				emissive;					// ABGR in [0,255]
		uint32				ambient;					// ABGR in [0,255]
		uint32				diffuse;					// ABGR in [0,255]
		uint32				specular;					// ABGR in [0,255]
		float				glossiness;
		float				specLevel;
		uint32				stateEnFlags;				// DpyState::EnableFlags
		uint32				stateMdFlags;				// DpyState::ModeFlags
		float				stateBlendCte;				// DpyState blend-source-cte
		float				stateAlphaPass;				// DpyState alpha-pass-threshold
		uint32				bitmapUID;
		uint32				isFullOpaque;				// 0/1
	};


	struct Bunch {
		Box3				bbox;						// bbox
		uint32				locCpt;						// # bunch locations
		uint32				triCpt;						// # bunch faces
		uint32				vtxCpt;						// # bunch welded vertices
		uint32				primCpt;					// # bunch proc vertices
		void*				dlistAddr;					// display-list addr
		uint32				dlistBSize;					// display-list bsize
	};


	struct Vtx {
		uint				vtxFmt;						// mask of vtxfmt CO_... flags
		int					locFrac;					// loc packing frac
		void*				compAddr[4];				// loc/tex/col/nrm addr
		uint32				compBStride[4];				// loc/tex/col/nrm stride bsize
	};


	NvInterface*			GetBase				(										);
	void					AddRef				(										);
	uint					GetRefCpt			(										);
	void					Release				(										);
	uint32					GetRscType			(										);
	uint32					GetRscUID			(										);
	NvMesh*					GetInterface		(										);

	Box3*					GetBBox				(										);
	uint32					GetNameCRC			(										);
	bool					IsSkinnable			(										);
	bool					IsNrmSkinnable		(										);
	bool					IsMorphable			(										);
	bool					IsLightable			(										);
	bool					IsShadowing			(										);
	bool					IsFullOpaque		(										);

	uint					GetSurfaceCpt		(										);
	Shading*				GetSurfaceShadingA	(										);
	Bunch*					GetBunchA			(										);
	Vtx*					GetVtx				(										);

	void					RefBitmaps			(										);
	void					UnrefBitmaps		(										);

	void					UpdateBeforeDraw	(										);
	bool					IsDrawing			(										);
};



#endif // _NvkMesh_H_



