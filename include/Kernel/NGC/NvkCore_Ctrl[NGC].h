/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkCore_Ctrl_H_
#define	_NvkCore_Ctrl_H_

#ifdef RVL


#include <Nova.h>
struct NvSound;


namespace nv { namespace ctrl
{

	enum
	{
		BUTTON_LEFT   			= 0x0001,
		BUTTON_RIGHT   			= 0x0002,
		BUTTON_DOWN    			= 0x0004,
		BUTTON_UP      			= 0x0008,
		BUTTON_A       			= 0x0800,
		BUTTON_B       			= 0x0400,
		BUTTON_HOME    			= 0x8000,	

		BUTTON_PLUS    			= 0x0010,
		BUTTON_MINUS   			= 0x1000,
		BUTTON_1       			= 0x0200,
		BUTTON_2       			= 0x0100,
		BUTTON_Z       			= 0x2000,
		BUTTON_C       			= 0x4000,

		CL_BUTTON_UP       		= 0x0001,
		CL_BUTTON_LEFT     		= 0x0002,
		CL_TRIGGER_ZR      		= 0x0004,
		CL_BUTTON_X        		= 0x0008,
		CL_BUTTON_A        		= 0x0010,
		CL_BUTTON_Y        		= 0x0020,
		CL_BUTTON_B        		= 0x0040,
		CL_TRIGGER_ZL      		= 0x0080,
		CL_TRIGGER_R       		= 0x0200,
		CL_BUTTON_PLUS     		= 0x0400,
		CL_BUTTON_HOME     		= 0x0800,
		CL_BUTTON_MINUS    		= 0x1000,
		CL_TRIGGER_L       		= 0x2000,
		CL_BUTTON_DOWN    	 	= 0x4000,
		CL_BUTTON_RIGHT    		= 0x8000,

		MAX_READ_BUFS  			= 16,
		
		DEV_CORE            	= 0,
		DEV_FREESTYLE       	= 1,
		DEV_CLASSIC         	= 2,
		DEV_UNKNOWN        		= 255,
		DEV_NOT_FOUND       	= 253,
		DEV_NOT_SUPPORTED   	= 252,
		DEV_FUTURE          	= 251,
		
		FMT_CORE       			= 0,
		FMT_CORE_ACC           	= 1,
		FMT_CORE_ACC_DPD       	= 2,
		FMT_FREESTYLE          	= 3,
		FMT_FREESTYLE_ACC      	= 4,
		FMT_FREESTYLE_ACC_DPD  	= 5,
		FMT_CLASSIC            	= 6,
		FMT_CLASSIC_ACC        	= 7,
		FMT_CLASSIC_ACC_DPD    	= 8,
		FMT_CORE_ACC_DPD_FULL  	= 9,
		
		ERR_NONE          		=  0,
		ERR_NO_CONTROLLER 		= -1,
		ERR_BUSY          		= -2,
		ERR_TRANSFER     		= -3,
		ERR_INVALID     		= -4,
		ERR_NOPERM				= -5,
		ERR_BROKEN				= -6,
		ERR_CORRUPTED      		= -7,
		
	};

	struct KStatus
	{
	    uint32				hold		;
	    uint32				trig		;
	    uint32				release 	;

	    nv::math::Vec3		acc 		;
	    float				accValue 	;
	    float				accSpeed 	;

	    nv::math::Vec2		pos 		;
	    nv::math::Vec2		vec;
	    float				speed 		;

	    nv::math::Vec2		horizon 	;
	    nv::math::Vec2		horiVec;
	    float				horiSpeed 	;

	    float				dist 		;
	    float				distVec		;
	    float				distSpeed 	;

	    nv::math::Vec2		accVertical ;

	    uint8				devType 	;
	    int8				err			;
	    int8				dpdValidFg 	;
	    uint8				dataFormat 	;

	    struct Freestyle
		{
			nv::math::Vec2 stick ;
			nv::math::Vec3 acc ;
			float accValue ;
	        float accSpeed ;
	    } fs ;

	    struct Classic
		{
	        uint32			hold;
	        uint32			trig;
	        uint32			release;
	    
	        nv::math::Vec2	lStick;
	        nv::math::Vec2	rStick;
	        
	        float			lTrigger;
	        float			rTrigger;
	    } cl ;
	};


	uint32 	GetKStatus				(		int32 						chan		, 		// Can be call more than one per frame.
											KStatus		*				outStatus	,		// Return the number of KStatus store in outStatus.
											uint32						nbStatus	);		// nbStatus : number of KStatus that can be store in outStatus.
	
	bool	IsConnected				(		int32						chan		);
	
	void 	SetPosParam				( 		int32 						chan		, 
											float 						play_radius	, 
											float 						sensitivity );
		
	void 	SetHoriParam			( 		int32 						chan		, 
											float 						play_radius	, 
											float 						sensitivity );
										
	void 	SetDistParam			( 		int32 						chan		, 
											float 						play_radius	, 
											float 						sensitivity );
										
	void 	SetAccParam				( 		int32 						chan		, 
											float 						play_radius	, 
											float 						sensitivity );
										
	void 	SetBtnRepeat			( 		int32 						chan		, 
											float 						delay_sec	, 
											float 						pulse_sec 	);
	
	void 	SetObjInterval			( 		float 						interval 	);
	
	void 	SetSensorHeight			( 		int32 						chan		, 
											float 						level 		);
	
	int32 	CalibrateDPD			( 		int32 						chan 		);
		
	void 	SetFSStickClamp 		( 		int8 						min			, 
											int8 						max 		);
	
	void 	EnableDPD				( 		int32 						chan 		);
	
	void 	DisableDPD				( 		int32 						chan 		);
		
	void 	EnableAimingMode		( 		int32 						chan 		);
		
	void 	DisableAimingMode		( 		int32 						chan 		);
	
	void 	GetProjectionPos		( 		nv::math::Vec2 *			dst			, 
											const nv::math::Vec2 *		src			, 
											const nv::math::Vec4 *		projRect	,  // x = left , y = top , z = right , w = bottom
											float 						viRatio 	);
	
	void 	EnableStickCrossClamp	( 												);
	
	void 	DisableStickCrossClamp	( 												);
	
	int		CtrlNoToChan			(		uint 						inCtrlNo	);


	
	enum	SlotState
	{
		ST_Unlocked = 0,
		ST_Locking 	= 1,
		ST_Locked 	= 2,
		ST_Unlocking= 3,
	};
	
	enum	SpeakerState
	{
		SS_Play 	= 0,
		SS_Mute 	= 1,
		SS_Stop 	= 2,
	};
	
	uint			SpeakerGetMaxSlot		(												);

	bool			SpeakerLockSlot			(		uint						inSlot		,
													NvSound	*					inSnd		);
												
	bool			SpeakerUnlockSlot		(		uint						inSlot		);
	
	SlotState		SpeakerGetSlotState		(		uint						inSlot		);
	
	
	bool			SpeakerPlay				(		uint						inChan		,
													uint						inSlot		);
												
	bool			SpeakerStop				(		uint						inChan		);
	
	bool			SpeakerMute				(		uint						inChan		,
													bool						inMute		);
	
	SpeakerState	SpeakerGetWMState		(		uint						inChan		);
	
}}

#endif // RVL
#endif //_NvkCore_Ctrl_H_