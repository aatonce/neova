/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvDpyManager_PS2_H_
#define	_NvDpyManager_PS2_H_


#include "EE/EE.h"
#include <NvDpyManager.h>
#include <Kernel/Common/NvDpyContext.h>
interface NvShader;
interface NvkBitmap;
interface NvDpyObject_PS2;




namespace DpyManager
{
	extern CoordinateSystem		coordSystem;
	extern NvDpyContext*		context;

	// rasters
	struct Raster {
		uint					width;				// in pixels
		uint					height;				// in pixels
		uint					rgbaBAddr;			// in blocks
		uint					depthBAddr;			// in blocks
		uint64					rgbaFrameReg;		// sceGsFrame
		uint64					rgbaXYOffsetReg;	// sceGsXyoffset
		uint64					rgbaScissor;		// sceGsScissor
		uint64					rgbaTex0Reg;		// sceGsTex0
		uint64					rgbaTex1Reg;		// sceGsTex1
		uint64					depthFrameReg;		// sceGsFrame
		uint64					depthZBufReg;		// sceGsZbuf
		Vec4					scale0[2];			// 0(2D) / 1(3D)
		Vec4					trans0[2];			// 0(2D) / 1(3D)

		inline
		void					GetViewport		(	int				inMode,		// 0(2D) / 1(3D)
													const Vec4&		inVP,
													Vec4&			outScale,
													Vec4&			outTrans	)
		{
			NV_ASSERT( inMode==0 || inMode==1 );
			outScale = scale0[inMode];
			outTrans = trans0[inMode];
			if( inMode==0 ) {
				outTrans.x += inVP.x;
				outTrans.y += inVP.y;
			} else {
				outScale.x *= (inVP.z * 0.5f);
				outScale.y *= (inVP.w * 0.5f);
				outTrans.x += (inVP.x + inVP.z * 0.5f);
				outTrans.y += (inVP.y + inVP.w * 0.5f);
			}
		}
	};

	extern Raster				raster[DPYR_OFF_FRAME_C32Z32+1];
	extern float				rasterMaxWidth;
	extern float				rasterMaxHeight;

	// Projection
	const  float				zbuffMax			=	float( 1<<(23-4) ) - 1.0F;					// (23 (bits if mantissa with FTOI4 fast packing) - 4 (bits lost with XYZF2 packing) !
	extern float				projNormalisedXY;
	extern float				ooProjNormalisedXY;
	extern Matrix				clipMatrix;


	// VRam mapping
	enum VRamBuffer {
		VRM_FRONT	= 0,		// front buffer
		VRM_FRAME	= 1,		// frame buffer (i.e. back buffer)
		VRM_DEPTH	= 2,		// depth buffer as PSMCT32
		VRM_OFF		= 3,		// Off-frame as DPYR_OFF_FRAME_C32H
		VRM_TEX		= 4			// Texture buffer as PSMCT32
	};

	void						GetVRamMapping		(	VRamBuffer			inBuffer,
														uint&				outBAddr,
														uint&				outPSM,
														uint&				outSizeX,
														uint&				outSizeY			);

	float						GetZProjNormalised	(	float				inZ,
														Matrix*				inProjTR			);	// returns in [-1;+1]
	uint32						GetZProjBuffValue	(	float				inZ,
														Matrix*				inProjTR			);

	// Native access
	bool						IsInFrame			(	NvDpyObject_PS2*	inDpyObj			);
	bool						IsDrawing			(	NvDpyObject_PS2*	inDpyObj			);
	void						SetFrameBreakBefore	(	NvDpyObject_PS2*	inDpyObj			);
	void						SetFrameBreakAfter	(	NvDpyObject_PS2*	inDpyObj			);
	bool						Draw				(	NvDpyObject_PS2*	inDpyObj			);

	// Shader native access
	bool						SetMipmapping		(	NvShader*			inShader,
														uint				inSurfaceIndex,
														int					inL,
														float				inK					);

	bool						GetPerformances		(	NvShader*			inShader,
														uint32&				outTriCpt,				// # triangles
														uint32&				outLocCpt,				// # original locations
														uint32&				outVtxWeldCpt,			// # welded vertex {components}
														uint32&				outVtxProcCpt		);	// # processed vertex

	// Garbager for safe release of frame-dependant objects
	void						AddToGarbager		(	NvInterface*		inITF				);
	void						FlushGarbager		(											);
}



#endif	// _NvDpyManager_PS2_H_


