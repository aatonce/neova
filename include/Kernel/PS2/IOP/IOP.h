/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _IOP_H_
#define _IOP_H_



namespace iop
{
	enum TargetRam {
		LOAD_EE_RAM		= 0,
		LOAD_IOP_RAM	= 1,
		LOAD_SPU2_RAM	= 2,
	};

	enum {
		SD_NB_SOUND_MAX	= (256),		// Limite max sur le nombre d'allocation en SRAM (SdAlloc)
		SD_NB_CHANNEL	= (46),			// 46 channels available. channels 46/47 are reserved for BGM playback !
		SD_BGM_CHANNEL	= (46),
		SD_BGM_LCHANNEL	= (SD_BGM_CHANNEL),
		SD_BGM_RCHANNEL	= (47)
	};

	enum SdState {
		SD_UNUSED		= 0,			// unused (invalid id!)
		SD_UNLOCKED		= 1,			// unlocked
		SD_LOCKING		= 2,			// locking
		SD_LOCKED		= 3,			// locked
		SD_UNLOCKING	= 4,			// unlocking
	};

	typedef bool	(*StreamHandler)	(	int				inSdId,
											float&			outFromTime,			// next segment start time
											float&			outDuration			);	// next segment duration



	//
	// Init/Reset/Shut.

	// [SYNCHRONOUS]
	bool			Init				(	int				inMediaMode			);	// SCECdDVD or SCECdCD
	void			Reset				(										);
	void			Update				(										);
	StreamHandler	SetHandler			(	StreamHandler	inHandler			);
	void			Shut				(										);



	//
	// file Open/Close

	// [SYNCHRONOUS]
	bool			OpenFile			(	pcstr			inFilename,
											uint32 *		outBSize			);
	bool			CloseFile			(										);



	//
	// Send buffered commands and asynchonously receives the IOP status

	bool			Flush				(										);		// Returns FALSE if a current flush is in progress (in wait of IOP status)
	bool			ForceFlush			(										);		// SyncFlush() + Flush()

	// Check & Wait flush completion.

	bool			IsFlushReady		(										);
	void			SyncFlush			(										);
	uint			GetFlushBCapacity	(										);		// Commands sending buffer capacity in bytes
	uint			GetFlushBSize		(										);		// Commands sending buffer current filling size in bytes
	int				GetFlushFilling		(										);		// Commands sending buffer current filling rate in [0%, 100%]



	//
	// load data to EE, IOP or SPU2 RAM.
	// Several offsets of the same data block can be given for an optimised seeking strategy.

	// [ASYNCHRONOUS i.e. need some Flush() to get the status changed.]
	bool			LoadData			(	TargetRam		inTargetRam,
											pvoid			inBufferPtr,
											uint32			inBSize,					// 16 bytes aligned
											uint32			inBOffset0,
											uint32			inBOffset1	  = -1,
											uint32			inBOffset2	  = -1,
											uint32			inBOffset3	  = -1	);
	bool			IsLoadReady			(										);
	bool			IsLoadSuccess		(										);



	//
	// SPU2
	// SdAlloc > SdLock > SdStart > SdStop > [SdUnlock >] SdFree

	// [SYNCHRONOUS]
	bool			AllocSPU2Mem		(	uint32			inBSize,
											void*&			outAddr				);
	void			FreeSPU2Mem			(	pvoid			inPtr				);

	// [SYNCHRONOUS]
	bool			SdIsValid			(	int				inSdId				);
	int				SdAny				(										);		// returned <0 lockId are invalid !
	int				SdAlloc				(	int				inSdId	  			);		// returned <0 lockId are invalid !
	bool			SdFree				(	int				inSdId				);		// [unlock ,] free the lockId
	SdState			SdGetState			(	int				inSdId				);
	uint			SdGetMode			(	int				inSdId				);		// must be locked !
	uint			SdGetBaseFreq		(	int				inSdId				);		// must be locked !
	float			SdGetDuration		(	int				inSdId				);		// must be locked !

	// [ASYNCHRONOUS i.e. need some Flush() to be fixed.]
	bool			SdLock				(	int				inSdId,
											uint			inMode,						// 0:mono-SFX 1:mono-STREAM 2:MUSIC
											uint			inDataFreq,					// in Hz
											uint32			inDataBOffset,
											uint32			inDataBSize,
											float			inDuration,
											float			inLockFromTime = 0,			// for streamed only !
											float			inLockDuration = 0	);		// for streamed only !
	bool			SdUnlock			(	int				inSdId				);

	// LibSD Wrapper
	// SD_VP_ : ( reg, channel,	value )
	// SD_P_  : ( reg, core,	value )
	// SD_S_  : ( reg, channel,	0/1	  )
	// SD_A_  : ( reg, core,	value )
	// SD_C_  : ( reg, -,		value )
	// SD_VA_ : ( reg, channel,	value )
	// [ASYNCHRONOUS i.e. need some Flush() to be fixed.]
	bool			SdSet				(	uint16			inRegister,
											uint16			inChannelOrCore,
											uint			inValue				);
	bool			SdStart				(	int				inSdId,
											uint			inChannel,
											uint			inPitch,
											uint			inVolL,
											uint			inVolR				);
	bool			SdStop				(	int				inSdId,
											uint			inChannel			);
	bool			SdSetVolume			(	int				inSdId,
											uint			inChannel,
											uint			inVolL,
											uint			inVolR				);
	bool			SdSetPitch			(	int				inSdId,
											uint			inChannel,
											uint			inPitch				);
	bool			SdBreak				(	int				inSdId				);
	bool			SdSetSegment		(	int				inSdId,
											float			inFromTime	= 0,
											float			inDuration	= 0		);
	bool			SdNoSegment			(	int				inSdId				);

	// [SYNCHRONOUS, but updated with Flush() return context]
	uint64			SdGetENDX			(										);
	uint64			SdGetENVX			(										);
	bool			SdIsPlaying			(	int				inChannel			);
	uint64			SdGetLastDown		(										);



	//
	// IOP mem manager

	// [SYNCHRONOUS, updated by Flush() return context]
	uint32			GetIOPFreeMemSize	(										);
	uint32			GetIOPFreeMaxSize	(										);
	// [ASYNCHRONOUS i.e. need some Flush() to get the status changed.]
	bool			AllocIOPMem			(	uint32			inBSize				);
	void			FreeIOPMem			(	pvoid			inPtr				);
	bool			GetAllocIOPMemPtr	(	pvoid*			outPtr				);
}



#endif // _IOP_H_

