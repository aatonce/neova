/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkMesh_H_
#define	_NvkMesh_H_


#include "NvDpyManager[PS2].h"
#include <NvMesh.h>


interface NvkMesh : public NvResource
{
	NVKITF_DECL( Mesh )

	struct Shading {
		uint32				nameCRC;			// 0 is unnamed
		uint32				emissive;			// ABGR in [0,255]
		uint32				ambient;			// ABGR in [0,255]
		uint32				diffuse;			// ABGR in [0,255]
		uint32				specular;			// ABGR in [0,255]
		float				glossiness;
		float				specLevel;
		uint32				stateEnFlags;		// DpyState::EnableFlags
		uint32				stateMdFlags;		// DpyState::ModeFlags
		float				stateBlendCte;		// DpyState blend-source-cte
		float				stateAlphaPass;		// DpyState alpha-pass-threshold
		uint32				bitmapUID;
		float				mappingRatio;		// For L&K auto-computation
		float				mipK;				// DpyState K
		uint8				mipL;				// DpyState L
		uint8				isFullOpaque;		// 0/1
		uint8				hasUV;				// 0/1
		uint8				hasColor;			// 0/1
	};

	struct BunchData {
		uint16				surfNo;				// surface idx
		uint16				bunchNo;			// bunch idx
		uint32				packetCpt;			// # unpack-packet
		uint32				unpackQSize;		// UNPACK QSize
		uint32				dmaBOffset;			// byte-offset of the surface VI1-DMA uploaders, relative to the global dmaSection
		uint32				dmaQSize;			// q-size of the surface VI1-DMA uploaders
		uint16				nextByBunch;		// next bunch-data in bunch order ('bunchByBunch' list) (last.next=first)
		uint16				nextBySurf;			// next bunch-data in surf order ('bunchBySurf' list) (last.next=first)
	};

	struct BunchList {
		uint16				size;				// list size (>=0)
		uint16				startIdx;			// list start idx in bunchDataA
	};

	struct Bunch {
		uint32				faceCpt;			// # face
		uint32				locCpt;				// # locations
		uint32				vtxCpt;				// # welded vertex
		uint32				primCpt;			// # stripped vertex
		uint32				surfCpt;			// # surface for the bunch
	};

	struct ShadowingVtx {
		Vec3				location;
		uint32				boneCRC;			// none if 0
	};

	struct ShadowingFace {
		int16				vtx[3];				// face vertex indices (always >= 0)
		int16				adj[3];				// face adjacency (same faceNo for none)
	};

	NvInterface*			GetBase				(										);
	void					AddRef				(										);
	uint					GetRefCpt			(										);
	void					Release				(										);
	uint32					GetRscType			(										);
	uint32					GetRscUID			(										);
	NvMesh*					GetInterface		(										);

	Box3*					GetBBox				(										);
	uint32					GetNameCRC			(										);
	bool					IsSkinnable			(										);
	bool					IsNrmSkinnable		(										);
	bool					IsMorphable			(										);
	bool					IsLightable			(										);
	bool					IsShadowing			(										);
	bool					IsFullOpaque		(										);
	int32					GetPackIPartLen		(										);

	uint					GetSurfaceCpt		(										);
	Shading*				GetSurfaceShadingA	(										);

	uint					GetBunchCpt			(										);
	uint					GetBunchDataCpt		(										);
	uint32					GetBunchDataBase	(										);
	Bunch*					GetBunchA			(										);
	Box3*					GetBunchBBoxA		(										);
	BunchData*				GetBunchDataA		(										);
	BunchList*				GetBunchByBunchA	(										);
	BunchList*				GetBunchBySurfA		(										);

	void					RefBitmaps			(										);
	void					UnrefBitmaps		(										);

	bool					GetShadowing		(	uint&				outVtxCpt,
													uint&				outFaceCpt,
													ShadowingVtx*&		outVtxA,
													ShadowingFace*&		outFaceA		);

	void					UpdateBeforeDraw	(										);
	bool					IsDrawing			(										);
};



#endif // _NvkMesh_H_



