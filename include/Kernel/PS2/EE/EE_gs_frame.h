/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_GS_FRAME_H_
#define _EE_GS_FRAME_H_


namespace gs { namespace frame {


	//
	//	Frame's constants

	extern uint32			PhysicalSizeX;		// front physical width
	extern uint32			PhysicalSizeY;		// front physical height
	extern uint32			VirtualSizeX;		// front virtual width
	extern uint32			VirtualSizeY;		// front virtual height
	extern uint32			SizeX;				// back width
	extern uint32			SizeY;				// back height
	extern uint32			SizeX2;				// 2^SizeX2 >= SizeX
	extern uint32			SizeY2;				// 2^SizeY2 >= SizeY
	extern uint32			FrontBAddr;			// front block addr
	extern uint64			FrontFrameReg;		// sceGsFrame
	extern uint32			BackBAddr;			// back block addr
	extern uint64			BackFrameReg;		// sceGsFrame
	extern uint32			DepthBAddr;			// depth block addr
	extern uint64			DepthFrameReg;		// sceGsFrame
	extern uint64			DepthZBufReg;		// sceGsZbuf
	extern uint32			VRamLowerBAddr;		// free vram lower block addr
	extern uint32			VRamUpperBAddr;		// free vram upper block addr
	extern Context			DefContext;			// Default frame ctxt


	//	Put a bitmap in the back buffer (4.39%60Hz)
	void		PutBack24_CH1		(	dmac::Cursor*		inDC,
										uint128*			inData		);
	void		PutBack32_CH1		(	dmac::Cursor*		inDC,
										uint128*			inData		);


	//	Get VRam BackBuffer & DepthBuffer
	void		GetBack_CH1			(	uint128*			inBuffer	);

	void		GetDepth_CH1		(	uint128*			inBuffer	);


	// Frame operations with DMA modifiers

	struct PresentMod {
		uint32*				bgr;
		uint8*				opacity;
		uint16*				uv0;
		void				Translate		(	uint32		inD			);
		void				SetColor		(	uint32		inBGR		);
		void				SetOpacity		(	float		inOpacity	);		// in [0,1]
		void				SetOddFrame		(							);
		void				SetEvenFrame	(							);
	};

	struct ClearMod {
		uint32*				z;
		uint32*				abgr;
		sceGsFrame*			gsFrame;
		sceGsScissor*		gsScissor;
		sceGsZbuf*			gsZBuf;
		void				Translate		(	uint32		inD			);
		void				SetRect			(	int			inX,
												int			inY,
												int			inW,
												int			inH			);
		void				SetColor		(	uint32		inABGR		);
		void				SetZ			(	uint32		inZ			);
		void				SetFBMSK		(	uint32		inABGR		);
		void				SetZMSK			(	uint		inZ_0_1		);
	};

	// Present Back -> Front
	void		Present_CH1			(	dmac::Cursor*		inDC,
										PresentMod&			outMod			);

	// Clear Back & Depth
	void		Clear_CH1			(	dmac::Cursor*		inDC,
										ClearMod&			outMod			);

} }


#endif // _EE_GS_FRAME_H_


