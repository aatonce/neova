/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_TRAM_H_
#define _EE_TRAM_H_



namespace tram
{

	struct TexDesc;
	struct TexReg;
	struct TexTRX;


	//
	//	Texture Descriptor
	//	Texel components RGB in [0,255], A in [0,128]

	struct TexDesc
	{
		enum {									// Flags
			F_TRANSPOSED	= (1<<0),			// Texels transposition ?
			F_DMA_CALLED	= (1<<1),			// Trx[].dmaData must be DMA'CALLed ? (Default is DMA'REFed)
		};

		struct Trx {
			uint128*	dmaData;				// DMA DATA PTR
			uint32		dmaQSize;				// DMA DATA QSIZE
			uint8* 		bitmapData;				// BITMAP DATA PTR
			uint32		bitmapBSize;			// BITMAP DATA BYTE SIZE
			uint32		TBW;
			uint32		BITBLTBUF[2];
		};

		uint8			flags;					// flags
		uint8			psm;					// in [ SCE_GS_PSMT4, SCE_GS_PSMT8, SCE_GS_PSMCT24, SCE_GS_PSMCT32 ]
		uint16			mipmapCpt;				// in [1,6]
		uint16			L2_W;					// W=1<<L2_W
		uint16			L2_H;					// H=1<<L2_H
		uint16			trxBSize[8];			// block size of each trx (0 => none)
		uint16			trxAllBSize;			// block size for all trx
		uint32			resBSize;				// descriptor size in bytes (for duplication)
		Trx				trx[8];					// trx[0]=CLUT, trx[1]=mipmap0, ...
	};




	//
	// Texture Transfer Uploader
	// PATH3 with TTE=0 (dmatags are not transfered !)

	struct TexTRX
	{
		bool					Init			(	TexDesc*		inDesc				);

		void					Update			(	uint			inBAddr,
													uint8			inLODMask			);

		void					EnableEOP		(										);
		void					DisableEOP		(										);
		static void				EnableEOP		(	pvoid			inTexTRXLink		);
		static void				DisableEOP		(	pvoid			inTexTRXLink		);

		// GS REGS (2x32bits)
		sceGsTex0				TEX0;
		sceGsTex1				TEX1;
		sceGsMiptbp1			MIP1;
		sceGsMiptbp2			MIP2;

		// Current state
		uint8					allLodMask;			// TRX all LOD-mask
		uint8					lodMask;			// TRX LOD-mask
		uint16					baddr;				// VRAM addr in gs-block unit

		// DMA start-TADR & link-TADR
		ALIGNED128( uint32,		tadr[16*8+8] );
		uint32* 				link;
		TexTRX*					next;				// next managed
	};




	//
	// Registered Texture Handle

	struct TexReg {
		TexDesc*		desc;
		uint32			allBmpBSize;
		TexTRX*			firstTRX;
		TexTRX*			lastTRX;
		int				next_free;			// reserved ...
	};




	//
	// Init/Reset

	void		Init				(	uint32			inPStart			);	//	PEnd = 512
	void		Reset				(										);
	void		Shut				(										);


	//
	// VRam Mapping

	uint		GetPStart			(										);
	uint		GetPSize			(										);


	//
	// TexDesc Register / Release / Build / Translate

	int			RegisterDesc		(	TexDesc*		inDesc				);
	int			FindDesc			(	TexDesc*		inDesc				);
	TexReg*		GetRegistered		(	int				inTexId				);
	void		ReleaseDesc			(	int				inTexId				);
	TexReg*		GetRegistrationA	(	uint*			outSize		 = NULL,
										int*			outFirstFree = NULL	);

	bool		BuildDesc			(	TexDesc*		outDesc,
										dmac::Cursor*	inDC,
										uint			inPSM,					// SCE_GS_PSMT8/24/32. SCE_GS_PSMCT24 is GS-transfered as SCE_GS_PSMCT32 !!
										uint			inL2W,					// Width pow of 2
										uint			inL2H,					// Height pow of 2
										pvoid			inCLUTPtr	= NULL,		// for SCE_GS_PSMT8
										pvoid			inTexelPtr	= NULL,
										uint			inBlockL2W	= 0,		// Block width pow of 2
										uint			inBlockL2H	= 0		);	// Block height pow of 2
	void		DMATranslateDesc	(	TexDesc*		ioDesc,
										uint32			inAddrOffset		);


	//
	// TexTRX Allocate / Free

	TexTRX*		AllocateTRX			(										);
	void		ReleaseTRX			(	TexTRX*			inTRX				);
	TexTRX*		AllocateManagedTRX	(	int				inTexId				);


	//
	// Test the texture mipmaping usage
	// Mode :
	// 0 => Paint by LOD's index
	// 1 => Paint by LOD's sup_pow2( MAX(w,h) )
	// 2 => Paint by LOD's sup_pow2( MIN(w,h) )

	bool		TestMipmaping		(	TexDesc*		inDesc,
										uint			inMode,
										uint			inLODMask			);
}


#include <Kernel/PS2/EE/EE_tram_I.h>


#endif // _EE_TRAM_H_



