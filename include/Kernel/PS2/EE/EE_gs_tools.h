/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_GS_TOOLS_H_
#define _EE_GS_TOOLS_H_



/*
 Bilinear sampling
 -----------------

 +(0.5,0.5) is the center of texels.

 iu0 =  int(u-0.5)     iu1 = iu0+1
 iv0 =  int(v-0.5)     iv1 = iv0+1
 a   = frac(u-0.5)
 b   = frac(v-0.5)

 (iu0,iv0)    (iu1,iv0)
 (1-a)(1-b)   (a)(1-b)
      A          B
        \      /
          \  /
            O
          /  \
        /      \
      C          D
 (1-a)(b)      (a)(b)
 (iu0,iv1)   (iu1,iv1)

 ex:
 (u,v)=(0.5,0.5) -> (a,b)=(0,0)     -> O = A		   -> point
 (u,v)=(1  ,0.5) -> (a,b)=(0.5,0)   -> O = (A+B)/2     -> interpolation horizontale
 (u,v)=(0.5,1  ) -> (a,b)=(0,0.5)   -> O = (A+C)/2     -> interpolation verticale
 (u,v)=(1  ,1  ) -> (a,b)=(0.5,0.5) -> O = (A+B+C+D)/4 -> double interpolation h & v


 Nearest sampling
 ----------------

 center of pixels (0.5,0.5) must be used for a 1:1 copy.
*/

#define	_UV(V)				( int32((V)*16.0f) )				// 10.4
#define	_UV_NI(V)			( int32(V)*16+8    )				// 10.4 no interpolation at center of texel with (a,b)=(0,0)
#define	_UV_I(V)			( int32(V)*16+16   )				// 10.4 interpolation between two centers of texels with (a,b)=(0.5,0.5)
#define	_XY(V)				( int32((V)*16.0f) )				// 12.4



namespace gs { namespace tools {


	enum PassFlags {
		PF_ABE			= (1<<0),		// PRIM.ABE=1
		PF_MODULATE		= (1<<1),		// TEX0.TFX='modulate'
		PF_NEAREST		= (1<<2),		// Force TEX1.MMAG=TEX1.MMIN='nearest' sampling
		PF_NOSCISSOR	= (1<<3),		// SCISSOR reg. is not written
		PF_NOCLAMP		= (1<<4),		// CLAMP reg. is not written
		PF_PSM24		= (1<<5)		// using PSM24 instead of default PSM32 (enabling TEXA.AEM switch)
	};

	struct Kernel33 {					// 3x3 convolution kernel
		union {
			struct {
				float	k00, k01, k02;	// (-1,-1) ( 0,-1) ( 1,-1)
				float	k10, k11, k12;	// (-1, 0) ( 0, 0) ( 1, 0)
				float	k20, k21, k22;	// (-1, 1) ( 0, 1) ( 1, 1)
			};
			float		k[3][3];		// [y][x]
		};
		void	Reset					();
		void	Normalize				();
		void	RotateCW				();
		void	RotateCCW				();
	};


	//	Get VRam Block/Page size

	int			GetPSM_BPP				(	uint					inPSM				);
	int			GetPSM_BWidth2			(	uint					inPSM				);
	int			GetPSM_BHeight2			(	uint					inPSM				);
	int			GetPSM_PWidth2			(	uint					inPSM				);
	int			GetPSM_PHeight2			(	uint					inPSM				);

	int			GetBlockSize			(	uint					inPSM,
											uint					inSizeX,
											uint					inSizeY				);

	int			GetPageSize				(	uint					inPSM,
											uint					inSizeX,
											uint					inSizeY				);

	// PSM32/16 block size, aligned on page
	int			GetPSM32_ABSize			(	uint					inSizeX,
											uint					inSizeY				);
	int			GetPSM16_ABSize			(	uint					inSizeX,
											uint					inSizeY				);


	//	Mapping UV bilinear offset
	void		GetDDABilinearOffset	(	float&					outOffset0,
											float&					outOffset1,
											int						inFromSize,
											int						inToSize			);


	// Open / Continue / Close
	bool		Gs_Open_CH1				(	dmac::Cursor*			inDC,
											bool					inEnableInit = TRUE	);
	uint		Gs_QSize_CH1			(	dmac::Cursor*			inDC				);
	bool		Gs_Continue_CH1			(	dmac::Cursor*			inDC				);
	bool		Gs_Close_CH1			(	dmac::Cursor*			inDC				);


	//
	// Basics operations

	// Basic draw sprite (only XYZ2 [& UV] regs are written)
	void		Gs_DrawSpritePass_CH1	(	dmac::Cursor*			inDC,
											Vec4*					inToRegion,				// { x0, y0, x1, y1 }
											Vec4*					inFromRegion = NULL,	// { u0, v0, u1, v1 }
											uint32					inZ			 = 0	);

	// Bitmap upload transmission (EERAM -> VRAM)
	// Beware that this process is not PATH3 compliant !
	void		Gs_TrxPass_CH1			(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											uint					inPSM,
											int						inToBAddr,
											uint128*				inData				);

	// Bitmap download transmission (VRAM -> EERAM)
	void		Gs_GrabPass_CH1			(	int						inSizeX,
											int						inSizeY,
											uint					inPSM,
											int						inFromBAddr,
											uint128*				inBuffer			);


	//
	// Operations without texturing

	// Apply (PSM32)
	void		Gs_ApplyPass_CH1		(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Apply (PSMZ32)
	void		Gs_ApplyDepthPass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Invert (PSM32, using ALPHA_2)
	// The ALPHA component can't be inverted -> keeped
	void		Gs_InvertPass_CH1		(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Invert depth (PSMZ32, using ALPHA_2)
	// The ALPHA component can't be inverted -> keeped
	void		Gs_InvertDepthPass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);


	//
	// Operations with texturing

	// Generic Blit (default sampling mode is linear)
	void		Gs_BlitPass_CH1			(	dmac::Cursor*			inDC,
											int						inFromSizeX,
											int						inFromSizeY,
											Vec4*					inFromRegion,			// { x, y, w, h }
											int						inFromBAddr,
											int						inToSizeX,
											int						inToSizeY,
											Vec4*					inToRegion,				// { x, y, w, h }
											int						inToBAddr,
											int						inClutBAddr = -1,		// Source is PSMT8H
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Copy with translation
	void		Gs_TranslatePass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inToOffsetX,
											int						inToOffsetY,
											int						inFromBAddr,
											int						inToBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Copy
	void		Gs_CopyPass_CH1			(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inFromBAddr,
											int						inToBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Resize
	void		Gs_ResizePass_CH1		(	dmac::Cursor*			inDC,
											int						inFromSizeX,
											int						inFromSizeY,
											int						inFromBAddr,
											int						inToSizeX,
											int						inToSizeY,
											int						inToBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Resize down x2
	void		Gs_Downx2Pass_CH1		(	dmac::Cursor*			inDC,
											int						inFromSizeX,
											int						inFromSizeY,
											int						inFromBAddr,
											int						inToBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Resize up x2
	void		Gs_Upx2Pass_CH1			(	dmac::Cursor*			inDC,
											int						inFromSizeX,
											int						inFromSizeY,
											int						inFromBAddr,
											int						inToBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Prepare (reducing & fill safeband with U&V border clamping)
	bool		Gs_PreparePass_CH1		(	dmac::Cursor*			inDC,
											int						inFromSizeX,
											int						inFromSizeY,
											int						inFromBAddr,
											int						inToSizeX,
											int						inToSizeY,
											int						inToBAddr,
											float					inFactor    = 0.5f,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Unprepare
	bool		Gs_UnpreparePass_CH1	(	dmac::Cursor*			inDC,
											int						inFromSizeX,
											int						inFromSizeY,
											int						inFromBAddr,
											int						inToSizeX,
											int						inToSizeY,
											int						inToBAddr,
											float					inFactor    = 0.5f,		// Same factor used for Gs_PreparePass_CH1() !!
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// General convolution (using ALPHA_2)
	void		Gs_ConvolvePass_CH1		(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inFromBAddr,
											int						inToBAddr,
											const Kernel33&			inKernel,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Differential convolution (using ALPHA_2)
	void		Gs_DiffConvolvePass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inFromBAddr,
											int						inToBAddr,
											const Kernel33&			inKernel,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Convolution for separable kernel (using ALPHA_2)
	void		Gs_SepConvolvePass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											int						inTmpBAddr,
											const Kernel33&			inKernel,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Gaussian blur (using ALPHA_2)
	void		Gs_GaussianBlurPass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											int						inTmpBAddr,
											float					inRadius,
											float					inQuality	= 0,		// in [0,1]
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Glowing blur (using ALPHA_2)
	void		Gs_GlowBlurPass_CH1		(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											int						inTmpBAddr,
											float					inRadius,
											float					inQuality	= 0,		// in [0,1]
											Kernel33*				inKernel	= NULL,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Focal blur (using ALPHA_2)
	void		Gs_FocalBlurPass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											int						inTmpBAddr,
											float					inRadius,
											float					inQuality	= 0,		// in [0,1]
											Kernel33*				inKernel	= NULL,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Depth[15:8] -> Alpha
	void		Gs_DepthToAlphaPass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inFromBAddr,
											int						inToBAddr			);

	// Color -> Depth
	void		Gs_RGBAToDepthPass_CH1	(	dmac::Cursor*			inDC,
											int						inSizeX,
											int						inSizeY,
											int						inFromBAddr,
											int						inToBAddr,
											uint					inFlags		= 0,		// PassFlags
											uint32					inFBMSK		= 0,
											uint32					inZ			= 0		);

	// Stripes for testing
	void		Gs_GenerateHStripes_CH1	(	dmac::Cursor*			inDC,
											uint					inStep,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											uint32					inFBMSK		= 0		);

	void		Gs_GenerateVStripes_CH1	(	dmac::Cursor*			inDC,
											uint					inStep,
											int						inSizeX,
											int						inSizeY,
											int						inBAddr,
											uint32					inFBMSK		= 0		);

} }


#endif // _EE_GS_TOOLS_H_


