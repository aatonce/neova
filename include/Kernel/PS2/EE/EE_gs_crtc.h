/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_GS_CRTC_H_
#define _EE_GS_CRTC_H_


namespace gs { namespace crtc {

	//
	// Frame Cpt

	extern volatile uint32	vsyncCpt;
	extern			uint32	vsyncRate;		// 50/60


	//
	// Enable / Disable

	void			Enable		(									);
	void			Disable		(									);


	//
	// Translate

	void			Translate	(	int		inX0,	int		inY0		);


	//
	// even frame ?

	bool			IsEvenFrame	(									);


	//
	// VSync

	void			VSync		(									);


	//
	// BG Color RW

	uint			SetBGColor	(	uint32	inRGB					);		// returns previous
	uint			GetBGColor	(									);

} }


#endif // _EE_GS_CRTC_H_


