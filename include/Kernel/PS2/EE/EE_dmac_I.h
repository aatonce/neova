/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _EE_DMAC_H_


inline
uint32
dmac::GetEnableI	(	uint32		inChannelsMask		)
{
	register uint32 CM = inChannelsMask << 16;
	return ((*D_STAT) & CM) ^ CM;
}


inline
uint32
dmac::GetDisableI	(	uint32		inChannelsMask		)
{
	register uint32 CM = inChannelsMask << 16;
	return ((*D_STAT) & CM);
}


inline
uint32 dmac::SyncLoop	(	)
{
	register uint32	cyc = GetCycleCpt();
#if defined(__MWERKS__)
	asm volatile (
	"	.set noreorder\n"
	"	sync.l\n"
	"	sync.p\n"
	"l_:\n"
	"	bc0t x_\n"
	"	nop\n"
	"	bc0t x_\n"
	"	nop\n"
	"	bc0t x_\n"
	"	nop\n"
	"	bc0f l_\n"
	"	nop\n"
	"x_:\n"
	"	.set reorder\n" );
#else	// GCC
	asm volatile (
	"	.set noreorder\n"
	"	sync.l\n"
	"	sync.p\n"
	"0:	bc0t 1f\n"
	"	nop\n"
	"	bc0t 1f\n"
	"	nop\n"
	"	bc0t 1f\n"
	"	nop\n"
	"	bc0f 0b\n"
	"	nop\n"
	"1:\n"
	"	.set reorder\n" );
#endif
	return GetCycleCpt() - cyc;
}





inline
void
dmac::Start_CH0		(	void * inTADR	)
{
	NV_ASSERT( (GetBusyMask()&CH0_VIF0_M) == 0 );
	NV_ASSERT_A128( inTADR );

	// Source chain logical mode
	*VIF0_ERR = 0xF;					// prevent VIF0_ERR bugs (see Restrictions.pdf)
	*D_STAT  = D_STAT_CIS0_M;
	*D0_QWC  = 0;
	*D0_TADR = DMA_ADDR(inTADR);
	SyncMemory();
	*D0_CHCR =	(1<<D_CHCR_DIR_O)		// from Memory
			|	(1<<D_CHCR_MOD_O)		// CHAIN
			|	(1<<D_CHCR_TTE_O)		// TTE
			|	(1<<D_CHCR_STR_O);		// START
}


inline
void
dmac::Start_CH1	(	void * inTADR, bool inIT )
{
	NV_ASSERT( (GetBusyMask()&CH1_VIF1_M) == 0 );
	NV_ASSERT_A128( inTADR );

	// Source chain logical mode
	*VIF1_ERR = 0xF;					// prevent VIF1_ERR bugs (see Restrictions.pdf)
	*D_STAT  = D_STAT_CIS1_M | ( inIT ? GetEnableI(CH1_VIF1_M) : GetDisableI(CH1_VIF1_M) );
	*D1_QWC  = 0;
	*D1_TADR = DMA_ADDR(inTADR);
	SyncMemory();
	*D1_CHCR =	(1<<D_CHCR_DIR_O)		// from Memory
			|	(1<<D_CHCR_MOD_O)		// CHAIN
			|	(1<<D_CHCR_TTE_O)		// TTE
			|	(1<<D_CHCR_STR_O);		// START
}


inline
void
dmac::Start_CH2		(	void * inTADR, bool inIT )
{
	NV_ASSERT( (GetBusyMask()&CH2_GIF_M) == 0 );
	NV_ASSERT_A128( inTADR );

	// Source chain logical mode
	*D_STAT  = D_STAT_CIS2_M | ( inIT ? GetEnableI(CH2_GIF_M) : GetDisableI(CH2_GIF_M) );
	*D2_QWC  = 0;
	*D2_TADR = DMA_ADDR(inTADR);
	SyncMemory();
	*D2_CHCR =	(1<<D_CHCR_DIR_O)		// from Memory
			|	(1<<D_CHCR_MOD_O)		// CHAIN
			|	(1<<D_CHCR_STR_O);		// START
}


inline
void
dmac::Start_CH8		(	void*		inSADR		)
{
	NV_ASSERT( (GetBusyMask()&CH8_FROM_SPR_M) == 0 );
	NV_ASSERT_NA128( inSADR );						// inSADR in SPR local are, valid in [0x0000 to 0x3FF0]
	NV_ASSERT( uint32(inSADR) <= 0x3FF0 );

	*D_STAT  = D_STAT_CIS8_M;
	*D8_QWC  = 0;
	*D8_SADR = uint32(inSADR);
	SyncMemory();
	*D8_CHCR =	(1<<D_CHCR_MOD_O)		// CHAIN
			|	(1<<D_CHCR_STR_O);		// START
}


inline
void
dmac::Start_CH9		(	void*		inTADR,		void*		inSADR		)
{
	NV_ASSERT( (GetBusyMask()&CH9_TO_SPR_M) == 0 );
	NV_ASSERT_A128( inTADR );
	NV_ASSERT_NA128( inSADR );						// inSADR in SPR local are, valid in [0x0000 to 0x3FF0]
	NV_ASSERT( uint32(inSADR) <= 0x3FF0 );

	*D_STAT  = D_STAT_CIS9_M;
	*D9_QWC  = 0;
	*D9_TADR = DMA_ADDR(inTADR);
	*D9_SADR = uint32(inSADR);
	SyncMemory();
	*D9_CHCR =	(1<<D_CHCR_MOD_O)		// CHAIN
			|	(1<<D_CHCR_TTE_O)		// TTE (dmatag is tranfer to spr. usefull for reverting with ch8 !)
			|	(1<<D_CHCR_STR_O);		// START
}


inline
uint32
dmac::SyncFromIPU()
{
	if( (*D3_CHCR)&D_CHCR_STR_M ) {
		*D_PCR = CH3_FROM_IPU_M;
		return SyncLoop();
	}
	return 0;
}


inline
uint32
dmac::SyncToIPU()
{
	if( (*D4_CHCR)&D_CHCR_STR_M ) {
		*D_PCR = CH4_TO_IPU_M;
		return SyncLoop();
	}
	return 0;
}


inline
uint32
dmac::SyncIPU()
{
	return SyncFromIPU() + SyncToIPU();
}


inline
void
dmac::FeedToIPU		(	void *		inMemSrc,
						uint32		inQSize,
						bool		inIT		)
{
	NV_ASSERT( !IsBusy(CH4_TO_IPU_M) );
	NV_ASSERT_A128( inMemSrc );
	NV_ASSERT( inQSize <= 65534 );

	*D_STAT = D_STAT_CIS4_M | ( inIT ? GetEnableI(CH4_TO_IPU_M) : GetDisableI(CH4_TO_IPU_M) );
	*D4_QWC  = inQSize;
	*D4_MADR = DMA_ADDR( inMemSrc );
	SyncMemory();
	*D4_CHCR = (1<<D_CHCR_DIR_O) | (1<<D_CHCR_STR_O);
}


inline
void
dmac::FeedFromIPU	(	void *		inMemDst,
						uint32		inQSize,
						bool		inIT		)
{
	NV_ASSERT( !IsBusy(CH3_FROM_IPU_M) );
	NV_ASSERT_A128( inMemDst );
	NV_ASSERT( inQSize <= 65534 );

	*D_STAT = D_STAT_CIS3_M | ( inIT ? GetEnableI(CH3_FROM_IPU_M) : GetDisableI(CH3_FROM_IPU_M) );
	*D3_QWC  = inQSize;
	*D3_MADR = DMA_ADDR( inMemDst );
	SyncMemory();
	*D3_CHCR = (1<<D_CHCR_STR_O);
}



inline
uint32
dmac::SyncFromSPR()
{
	if( (*D8_CHCR)&D_CHCR_STR_M ) {
		*D_PCR = CH8_FROM_SPR_M;
		return SyncLoop();
	}
	return 0;
}


inline
uint32
dmac::SyncToSPR()
{
	if( (*D9_CHCR)&D_CHCR_STR_M ) {
		*D_PCR = CH9_TO_SPR_M;
		return SyncLoop();
	}
	return 0;
}


inline
uint32
dmac::SyncSPR()
{
	return SyncFromSPR() + SyncToSPR();
}


inline
void
dmac::CopyToSPR(	void *		inSprDst,
					void *		inMemSrc,
					uint32		inQSize )
{
	NV_ASSERT( !IsBusy(CH9_TO_SPR_M) );
	NV_ASSERT_A128( inMemSrc );
	NV_ASSERT_A128( inSprDst );
	NV_ASSERT( uint32(inSprDst)>=SPR_START );
	NV_ASSERT( uint32(inSprDst)<=SPR_END   );
	NV_ASSERT( inQSize > 0 );

	*D_STAT  = D_STAT_CIS9_M;
	*D9_QWC  = inQSize;
	*D9_MADR = DMA_ADDR( inMemSrc );
	*D9_SADR = uint32( inSprDst ) - SPR_START;
	SyncMemory();
	*D9_CHCR = (1<<D_CHCR_DIR_O) | (1<<D_CHCR_STR_O);
}


inline
void
dmac::CopyFromSPR(	void *		inMemDst,
					void *		inSprSrc,
					uint32		inQSize )
{
	NV_ASSERT( !IsBusy(CH8_FROM_SPR_M) );
	NV_ASSERT_A128( inMemDst );
	NV_ASSERT_A128( inSprSrc );
	NV_ASSERT( uint32(inSprSrc)>=SPR_START );
	NV_ASSERT( uint32(inSprSrc)<=SPR_END   );
	NV_ASSERT( inQSize > 0 );

	*D_STAT  = D_STAT_CIS8_M;
	*D8_QWC  = inQSize;
	*D8_MADR = DMA_ADDR( inMemDst );
	*D8_SADR = uint32( inSprSrc ) - SPR_START;
	SyncMemory();
	*D8_CHCR = (1<<D_CHCR_DIR_O) | (1<<D_CHCR_STR_O);
}


inline
uint
dmac::Pause		(			)
{
	uint enr = *D_ENABLER;
	*D_ENABLEW = enr | (1<<16);
	uint dummy0 = *D_PCR;	// uncached read/w
	uint dummy1 = *D_PCR;	// uncached read/w
	*D_PCR		= dummy0;
	*D_PCR		= dummy1;
	*D_CTRL = (((*D_CTRL)>>1)<<1);	// .DMAE = 0
	return enr;
}


inline
void
dmac::Restore	(	uint	enr		)
{
	*D_CTRL = (*D_CTRL)|1;	// .DMAE = 1
	*D_ENABLEW = enr;
}


#endif // _EE_DMAC_H_


