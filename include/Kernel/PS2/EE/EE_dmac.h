/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_DMAC_H_
#define _EE_DMAC_H_



namespace dmac
{

	//
	// Data cursor

	struct Cursor {
		uint8*			start;
		uint32			bsize;
		uint32*			tag;

		union {
			pvoid		vd;
			uint8*		i8;
			uint16*		i16;
			uint32*		i32;
			float*		f32;
			uint64*		i64;
			uint128*	i128;
		};

		void			Memcpy			(	pvoid	inPtr,			uint inBSize				);
		pvoid			Dup				(	pvoid	inStart	= NULL, uint inBAlignement = 64		);		// D$ alignment
		uint32			GetUsedBSize	(														);
		uint32			GetLeftBSize	(														);
		void			Align32			(														);
		void			Align64			(														);
		void			Align128		(														);
		void			Align128_32		(														);
		void			Align8QW		(														);
		void			Align8QW_1		(														);

		void			SetTag			(														);
		void			NullTag			(														);
		uint32*			GetTag			(														);
		uint			GetTagQSize		(														);
		void			SetTagQSize		(														);
	};


	//
	// Users channels Mask (CH5-7 are reserved by SCE)

	enum {
		CH0_VIF0_M		=	(1<<0),			// TTE=0
		CH1_VIF1_M		=	(1<<1),			// TTE=1
		CH2_GIF_M		=	(1<<2),			// TTE=0
		CH3_FROM_IPU_M	=	(1<<3),
		CH4_TO_IPU_M	=	(1<<4),
		CH8_FROM_SPR_M	=	(1<<8),
		CH9_TO_SPR_M	=	(1<<9),
		CH_ALL_M		=	0x31F
	};


	//
	// Init/Reset the DMAC

	void			Init				(												);
	void			Reset				(												);
	void			Shut				(												);
	void			EnableCycStealing	(												);
	void			DisableCycStealing	(												);


	//
	// Get the channel interrupt ON/OFF STAT mask

	uint32			GetEnableI			(	uint32		inChannelsMask					);
	uint32			GetDisableI			(	uint32		inChannelsMask					);


	//
	// Returns the channels busy mask

	volatile uint32	GetBusyMask			(												);
	volatile bool	IsBusy				(	uint32		inChannelsMask					);


	//
	// Waiting for *ALL masked* DMA channels to terminate

	void			Sync				(	uint32		inChannelsMask					);
	uint32			SyncTimeout			(	uint32		inChannelsMask					);
	uint32			SyncLoop			(												);		// returns #cycle idle


	//
	// Safely stop some channels
	// Safely pause/restart all channels

	void			Stop				(	uint32		inChannelsMask					);
	uint			Pause				(												);		// Interrupts must be disabled !
	void			Restore				(	uint		inENABLER						);		// Interrupts must be disabled !


	//
	// Start a channel (chain logical mode)

	void			Start_CH0			(	void*		inTADR							);		// toVIF0  (src chain, TTE=1)
	void			Start_CH1			(	void*		inTADR,	bool	inIT = FALSE	);		// toVIF1  (src chain, TTE=1)
	void			Start_CH2			(	void*		inTADR,	bool	inIT = FALSE	);		// toGIF   (src chain, TTE=0)
	void			Start_CH8			(	void*		inSADR							);		// fromSPR (dst chain, TTE=0)
	void			Start_CH9			(	void*		inTADR,	void*		inSADR		);		// toSPR   (src chain, TTE=1)


	//
	// IPU dma feeding (CH3/CH4)

	uint32			SyncFromIPU			(												);
	uint32			SyncToIPU			(												);
	uint32			SyncIPU				(												);

	void			FeedToIPU			(	void *		inMemSrc,								// qword aligned !
											uint32		inQSize,
											bool		inIT	= FALSE					);

	void			FeedFromIPU			(	void *		inMemDst,								// qword aligned !
											uint32		inQSize,
											bool		inIT	= FALSE					);


	//
	// SPR dma copy (CH8/CH9)

	uint32			SyncFromSPR			(												);
	uint32			SyncToSPR			(												);
	uint32			SyncSPR				(												);

	void			CopyToSPR			(	void *		inSprDst,								// qword aligned !
											void *		inMemSrc,								// qword aligned !
											uint32		inQSize							);

	void			CopyFromSPR			(	void *		inMemDst,								// qword aligned !
											void *		inSprSrc,								// qword aligned !
											uint32		inQSize							);


	//
	// Debugging

	bool			IsValidMemRange		(	void*		inPtr,
											uint		inBSize							);

	void			OutputRegisters		(	uint32		inChannelsMask					);

	bool			CheckData_CH1		(	void*		inPtr,
											uint		inBSize,
											bool		inVerbose						);
	bool			CheckPacket_CH1		(	void*		inTADR,
											bool		inVerbose						);
	bool			CheckChain_CH1		(	void*		inFromTADR,
											void*		inToTADR,								// NULL => to END TAG
											bool		inVerbose						);


	//
	// Double DMA Heap
	// Fast uncached pointers (FAST_UNCACHED_MEM) with no need to flush the data cache !

	void			SwapHeap			(												);
	Cursor*			GetFrontHeapCursor	(												);
	Cursor*			GetBackHeapCursor	(												);
}


#include "EE_dmac_I.h"



#ifdef _NVCOMP_ENABLE_DBG

	#define NV_ASSERT_DC( CURSOR_PTR )																					\
		{																												\
			NV_ASSERTC( CURSOR_PTR, "NULL Cursor !" );																	\
			NV_ASSERT_A128( (CURSOR_PTR)->start )																		\
			NV_ASSERT_A128( (CURSOR_PTR)->i8 )																			\
			NV_ASSERTC( (CURSOR_PTR)->i8>=(CURSOR_PTR)->start, "Cursor underflow !" );									\
			NV_ASSERTC( uint32(((CURSOR_PTR)->i8)-((CURSOR_PTR)->start))<((CURSOR_PTR)->bsize), "Cursor overflow !" );	\
		}

#else

	#define NV_ASSERT_DC( CURSOR_PTR ) {}

#endif



#endif // _EE_DMAC_H_


