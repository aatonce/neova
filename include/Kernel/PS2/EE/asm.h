/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


		.set	noreorder
		.set	nomacro
		.set	noat
#ifdef __mips16
		.set	nomips16	/* This file contains 32 bit assembly code. */
#endif



/*

>> N32 and Native 64-Bit Interface Register Conventions
Note that "Caller-saved" means only that the caller may not assume
that the value in the register is preserved across the call.

Register Name		Software Name		 Use		 				Saver
-------------		-------------		 ---						-----
$0 					zero				 Hardware zero
$1 or $at			at					 Assembler temporary		Caller-saved
$2..$3				v0..v1				 Function results 			Caller-saved
$4..$7				a0..a3				 Subprogram arguments		Caller-saved
$8..$15				t0..t7				 Temporaries 				Caller-saved
$16..$23			s0..s7				 Saved 						Callee-saved
$24..$25			t8..t9				 Temporaries				Caller-saved
$26..$27			kt0..kt1			 Reserved for kernel 	
$28 or $gp			gp					 Global pointer 			Callee-saved
$29 or $sp			sp					 Stack pointer 				Callee-saved
$30 or $fp			fp or s8			 Frame pointer (if needed)  Callee-saved
$31					ra					 Return address				Caller-saved

output registers : v0 [v1]
input registers  : a0..a3 [t0..t3]
tmp   registers  : t0..t9

*/

#ifndef NO_MIPS_REGNAME
#define	zero		$0
#define	zr			$0
#define	at			$1
#define	v0			$2
#define	v1			$3
#define	a0			$4
#define	a1			$5
#define	a2			$6
#define	a3			$7
#define	t0			$8
#define	t1			$9
#define	t2			$10
#define	t3			$11
#define	t4			$12
#define	t5			$13
#define	t6			$14
#define	t7			$15
#define	s0			$16
#define	s1			$17
#define	s2			$18
#define	s3			$19
#define	s4			$20
#define	s5			$21
#define	s6			$22
#define	s7			$23
#define	t8			$24
#define	t9			$25
#define	gp			$28
#define	sp			$29
#define	fp			$30
#define	s8			$30
#define	ra			$31
#endif

#ifndef NO_VU_REGNAME
// for COP2 instructions
#define	vi00		$vi00
#define	vi01		$vi01
#define	vi02		$vi02
#define	vi03		$vi03
#define	vi04		$vi04
#define	vi05		$vi05
#define	vi06		$vi06
#define	vi07		$vi07
#define	vi08		$vi08
#define	vi09		$vi09
#define	vi10		$vi10
#define	vi11		$vi11
#define	vi12		$vi12
#define	vi13		$vi13
#define	vi14		$vi14
#define	vi15		$vi15
#endif


/*

 6 MIPS Physical pipes :
	I0, I1	: 64bits integer arithmetic
	LS		: load/store pipe
	BR		: branch pipe to execute a branch instruction
	C1		: COP1 (FPU) instruction
	C2		: COP2 (VPU0) instruction
	
 Instruction routing, or what physical pipes are used for instructions of each category
 ( EE Core User's Manual - 1.3.5 ) :
 	*		: instruction which require more than one execution pipeline
 			  ex: COP1 Move is executed in both LS and C1 pipelines
 	O		: can be this pipeline
 			  ex: the ALU instructions are executed in either the I0 or I1 pipe.


 | Categories    |   Instructions       | I0 | I1 | LS | BR | C1 | C2 |
 |---------------|----------------------|----|----|----|----|----|----o
 | Load/Store    | Load/Store,          |    |    |    |    |    |    |
 |               | QW Load/Store,       |    |    | O  |    |    |    |
 |               | Prefetch,            |    |    |    |    |    |    |
 |               | Cache                |    |    |    |    |    |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | SYNC          | Synchronization      |    | O  |    |    |    |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | COP0          | COP0 Move,           |    |    |    |    |    |    |
 |               | COP0 Operation       |    |    | O  |    |    |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | COP1 Move     | COP1 Move,           |    |    |    |    |    |    |
 |               | COP1 Load/Store      |    |    | *  |    | *  |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | COP2 Move     | COP2 Move            |    |    |    |    |    |    |
 |               | COP2 Load/Store      |    |    | *  |    |    | *  |
 |---------------|----------------------|----|----|----|----|----|----|
 | COP1 Operate  | COP1 Operation       | *  |    |    |    | *  |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | COP2 Operate  | COP2 Operation       | *  |    |    |    |    | *  |	(-> error in manual !)
 |---------------|----------------------|----|----|----|----|----|----|
 | ALU           | Arithmetic, Shift,   |    |    |    |    |    |    |
 |               | Logical,             | O <.> O |    |    |    |    |
 |               | Trap, SYSCAL, BREAK  |    |    |    |    |    |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | MAC0          | Multiply,            |    |    |    |    |    |    |
 |               | Mul-acc for HI/LO,   | O  |    |    |    |    |    |
 |               | Move to/from HI/LO   |    |    |    |    |    |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | MAC1          | Multiply,            |    |    |    |    |    |    |
 |               | Mul-acc for HI1/LO1, |    |  O |    |    |    |    |
 |               | Move to/from HI1/LO1 |    |    |    |    |    |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | Branch        | Branch and jump      |    |    |    | O  |    |    |
 |---------------|----------------------|----|----|----|----|----|----|
 | Wide Operate  | QW MMI instructions  | *  |  * |    |    |    |    |
 ----------------|----------------------|----|----|----|----|----|----|


 Alignement:
	Instructions are always fetched in pairs at an 8 byte alignment but execution pairing can occur at any alignment.
	If you designed a perfect 4 cycle loop (4 pairs, 8 instructions) and left it at a non 8 byte aligned address
	the fetcher would need to fetch 5 times instead of 4 making it a 5 cycle loop instead of 4 eventhough you have 4 perfect pairs.
	In general you make things much easier on yourself if you write a .p2align 3 immediately above your loop (over the label of your loop)
	which will make it 8 byte aligned. 

 Loading:
	Another thing you might want to know is that loading from memory is same speed as the scratchpad if it's a hit,
	it's a 1/2 instruction meaning you can do another bus access in the next cycle but you should not use the register until the next cycle after that.
	Example: 
		a1: lw $t0, 0(%0)			# 1. cycle  
		a2: nop  
		b1: lw $t1, 4(%0)			# 2. cycle  
		b2: nop  
		c1: add $t2, $t0, $t0		# 3. cycle  
		c2: nop  
	So $t0 is ready for use in the 3. cycle assuming it's a hit or fetched from scratchpad. 
	If you get a miss the stall will be longer then 30 cycles (use DMA for intensive data reading).

 The scratchpad RAM:
	The scratchpad is devided into 4 banks each 4 kb, if you access from a different bank then last read/store you did,
	then there will be a penalty of 1 cycle and you will not be able to fill the stall with another instruction.
	You will be stalled unconditionally for 1 cycle.
	So don't switch banks too often. Another weird thing, you also get a 1 cycle loss when switching sign of offset regardless of the base register.
	Assumed in this sample that %0 and %1 point to somewhere on the scratchpad :
		a1: lw $t0, 4(%0)			# 1. cycle  
		a2: nop  
		b1: lw $t1, -4(%1)			# 2. cycle  
		b2: nop  
		c1: nop						# 4. cuz of stall  
		c2: nop  
	Again we get an unconditional stall, no chance to fill the gap with other code. 
	So make sure you fiddle with the base pointers before entering a loop so all your base registers (%0, %1 in this case)
	will provide you with either negative offsets all the way through the loop or possitive all the way.
	This is ONLY! an issue when dealing with the scratchpad !!

*/


		// NOP on the I0/I1 physical pipeline
		.macro	I_NOP
				nop
		//	or
		//		addu		zr, zr, zr
		.endm

		// NOP on the BR physical pipeline
		.macro	BR_NOP
				b			*+8
		//	or
		//		.int		0x01000010
		.endm

		// NOP on the LS physical pipeline
		.macro	LS_NOP
				mfc0		zr, $9
		.endm


		// VU0 continue TPC -> CMSAR0
		// (safe with instruction pairing alignment)
		.macro	VU0_CNT		REG
				cfc2.i		\REG, $vi26	// waits vu0 completion (E=1)
				I_NOP
				ctc2		\REG, $vi27
				I_NOP
				vnop					// two vnops in order to adjust timing
				vnop
				vcallmsr	$vi27
				I_NOP
		.endm


		// Reset/Stop MIPS performances counters
		// (safe with instruction pairing alignment)
		.macro	ResetPerfCounters
				mtpc		zr, 0
				mtpc		zr,	1
				nop
				sync.p
		.endm
		.macro	StopPerfCounters
				mtps		zr, 0
				sync.p
		.endm
		.macro	GetCycleCpt	REG
				mfc0        \REG, $9
				I_NOP
		.endm


		// (not safe with instruction pairing alignment !)
		.macro	LoadWord	REG, LABEL
				lui			\REG, %hi(\LABEL)
				addiu		\REG, %lo(\LABEL)
				lw			\REG, 0(\REG)
		.endm
		.macro	LoadImm		REG, IMM
				lui			\REG, %hi(\IMM)
				addiu		\REG, %lo(\IMM)
		.endm				




