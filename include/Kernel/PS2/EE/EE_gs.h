/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_GS_H_
#define _EE_GS_H_



namespace gs
{

	enum VMode {						// vmode parameter
		VM_PAL			=	(1<<0),		// PAL(50 FPS) / NTSC(60 FPS)
		VM_PSM16		=	(1<<1),		// front frame in 16BPP / default is 32BPP
		VM_FIELD		=	(1<<2),		// Interlace mode : FIELD (+0,+2,+4,... ; +1,+3,+5,...) or FRAME (+0,+1,+2,+3,...)
		VM_AFLICKER		=	(1<<3),		// anti-flicker using the two crtcs vertical blending
		VM_SHOWBACK		=	(1<<4)		// debug show back buffer
	};


	struct Context {
		// Regs in each context
		uint64			texflush;		// sceGsTexflush
		uint64			xyoffset;		// sceGsXyoffset
		uint64			tex2;			// sceGsTex2
		uint64			tex1;			// sceGsTex1
		uint64			tex0;			// sceGsTex0
		uint64			clamp;			// sceGsClamp
		uint64			mip1;			// sceGsMiptbp1
		uint64			mip2;			// sceGsMiptbp2
		uint64			scissor;		// sceGsScissor
		uint64			alpha;			// sceGsAlpha
		uint64			test;			// sceGsTest
		uint64			fba;			// sceGsFba
		uint64			frame;			// sceGsFrame
		uint64			zbuf;			// sceGsZbuf

		// Regs shared by contexts
		uint64			texa;			// sceGsTexa
		uint64			fogcol;			// sceGsFogcol
		uint64			dthe;			// sceGsDthe
		uint64			dimx;			// sceGsDimx
		uint64			pabe;			// sceGsPabe
		uint64			colclamp;		// sceGsColclamp
		uint64			prim;			// sceGsPrim
		uint64			prmode;			// sceGsPrmode
		uint64			prmodecont;		// sceGsPrmodecont
	};

	enum {
		// Regs in each context
		TEXFLUSH			=	0x00000001,
		XYOFFSET			= 	0x00000002,
		TEX2				=	0x00000004,
		TEX1				=	0x00000008,
		TEX0				=	0x00000010,
		CLAMP				=	0x00000020,
		MIPTBP1				=	0x00000040,
		MIPTBP2				= 	0x00000080,
		SCISSOR				= 	0x00000100,
		ALPHA				= 	0x00000200,
		TEST				=	0x00000400,
		FBA					=	0x00000800,
		FRAME				= 	0x00001000,
		ZBUF				=	0x00002000,

		CONTEXT_SPECIFIC	=	0x00003FFF,

		// Regs shared by contexts
		TEXA				=	0x00004000,
		FOGCOL				= 	0x00008000,
		DTHE				=	0x00010000,
		DIMX				=	0x00020000,
		PABE				=	0x00040000,
		COLCLAMP			= 	0x00080000,
		PRIM				=	0x00100000,
		PRMODE				=	0x00200000,
		PRMODECONT			=	0x00400000,

		CONTEXT_SHARED		=	0x007FC000,

		ALL_REGS			=	CONTEXT_SPECIFIC | CONTEXT_SHARED
	};

	enum {
		CONTEXT1			=	0x01,
		CONTEXT2			=	0x02
	};


	//
	//	Init/Reset GS & CRTC & FRAME
	//	Be Careful : GS reset changes the VU state

	void			Init			(	bool			inEnableCRTC		);
	void			Reset			(	bool			inEnableCRTC		);
	void			Shut			(										);


	//
	//	Sync with GIF

	bool			IsBusy			(										);
	void			Sync			(										);
	bool			IsPaused		(										);
	void			Pause			(										);
	void			Resume			(										);


	//
	//	Flush all the registers of the context

	void			FlushContext_CH1(	dmac::Cursor*	inDC,
										const Context*	inCtxt,
										uint32			inRegistersMask,
										uint32			inContextsMask		);

	//	PATH3 managment on CH1

	void			SetupPATH3_CH1	(	int				inM3R,						// 0/1
										int				inM3P,						// 0/1
										int				inIMT				);		// 0/1

	void			InsertPATH3_CH1	(	dmac::Cursor*	inDC,
										pvoid&			ioLinkCH1,
										bool			inSync,						// FLUSHA ?
										bool			inContinue			);		// Open;Close PATH3 ?

}


#include "EE_gs_crtc.h"
#include "EE_gs_tools.h"
#include "EE_gs_dmac.h"
#include "EE_gs_frame.h"



#endif // _EE_GS_H_


