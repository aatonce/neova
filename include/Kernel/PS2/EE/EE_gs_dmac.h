/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_GS_DMAC_H_
#define _EE_GS_DMAC_H_


namespace gs {

	void		SetADReg				(	dmac::Cursor*		inDC,
											uint				inReg,
											uint64				inValue		);


	//
	// A+D regs shared by contexts

	void		Set_BITBLTBUF			(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 );

	void		Set_Clamp_COLCLAMP		(	dmac::Cursor*		inDC		);
	void		Set_Wrap_COLCLAMP		(	dmac::Cursor*		inDC		);

	void		Set_DIMX				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7, uint inF8, uint inF9, uint inF10, uint inF11, uint inF12, uint inF13, uint inF14, uint inF15 );

	void		Enable_DTHE				(	dmac::Cursor*		inDC		);
	void		Disable_DTHE			(	dmac::Cursor*		inDC		);

	void		Write_FINISH			(	dmac::Cursor*		inDC		);

	void		Set_FOG					(	dmac::Cursor*		inDC,
											uint				inF0		);

	void		Set_FOGCOL				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2 );

	void		Set_LABEL				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1 );

	void		Enable_PABE				(	dmac::Cursor*		inDC		);
	void		Disable_PABE			(	dmac::Cursor*		inDC		);

	void		Set_PRIM				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7, uint inF8 );

	void		Set_PRMODE				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7 );

	void		Enable_PRMODE			(	dmac::Cursor*		inDC		);
	void		Disable_PRMODE			(	dmac::Cursor*		inDC		);

	void		Set_RGBAQ				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4=0 );
	void		Set_RGBAQ				(	dmac::Cursor*		inDC,
											uint				inABGR, uint inQ=0 );

	void		Set_SCANMSK				(	dmac::Cursor*		inDC,
											uint				inF0		);

	void		Set_SIGNAL				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1	);

	void		Set_ST					(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1	);

	void		Set_TEXA				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2	);

	void		Set_TEXCLUT				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2	);

	void		Write_TEXFLUSH			(	dmac::Cursor*		inDC		);

	void		Set_TRXDIR				(	dmac::Cursor*		inDC,
											uint				inF0		);

	void		Set_TRXPOS				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4 );

	void		Set_TRXREG				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1 );

	void		Set_UV					(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1 );							// 10.4 raw coordinates
	void		Set_UVi					(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1 );							// 10.0 coordinates -> Set_UV( u<<4, v<<4 )
	void		Set_UVic				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1 );							// 10.0 centered coordinates -> Set_UV( (u<<4)+8, (v<<4)+8 )
	void		Set_UVf					(	dmac::Cursor*		inDC,
											float				inF0, float inF1 );							// float coordinates -> Set_UV( u*16.0f, v*16.0f )

	void		Set_XYZ2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2 );				// 10.4 XY raw coordinates
	void		Set_XYZ2i				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2 );				// 10.0 XY coordinates -> Set_XYZ2( x<<4, y<<4, z )
	void		Set_XYZ2f				(	dmac::Cursor*		inDC,
											float				inF0, float inF1, uint inF2 );				// float coordinates -> Set_XYZ2( x*16.0f, y*16.0f, z )

	void		Set_XYZF2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3 );	// 10.4 XY raw coordinates
	void		Set_XYZF2i				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3 );	// 10.0 XY coordinates -> Set_XYZF2( x<<4, y<<4, f, z )
	void		Set_XYZF2f				(	dmac::Cursor*		inDC,
											float				inF0, float inF1, uint inF2, uint inF3 );	// float coordinates -> Set_XYZF2( x*16.0f, y*16.0f, f, z )

	void		Set_XYZ3				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2 );				// 10.4 XY raw coordinates
	void		Set_XYZ3i				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2 );				// 10.0 XY coordinates -> Set_XYZ2( x<<4, y<<4, z )
	void		Set_XYZ3f				(	dmac::Cursor*		inDC,
											float				inF0, float inF1, uint inF2 );				// float coordinates -> Set_XYZ2( x*16.0f, y*16.0f, z )

	void		Set_XYZF3				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3 );	// 10.4 XY raw coordinates
	void		Set_XYZF3i				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3 );	// 10.0 XY coordinates -> Set_XYZF2( x<<4, y<<4, f, z )
	void		Set_XYZF3f				(	dmac::Cursor*		inDC,
											float				inF0, float inF1, uint inF2, uint inF3 );	// float coordinates -> Set_XYZF2( x*16.0f, y*16.0f, f, z )


	//
	// A+D regs in context 2

	void		Set_ALPHA_2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4 );

	void		Set_CLAMP_2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 );
	void		Set_Clamp_CLAMP_2		(	dmac::Cursor*		inDC		);
	void		Set_Repeat_CLAMP_2		(	dmac::Cursor*		inDC		);

	void		Enable_FBA_2			(	dmac::Cursor*		inDC		);
	void		Disable_FBA_2			(	dmac::Cursor*		inDC		);

	void		Set_FRAME_2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3 );
	void		SetFrame_2				(	dmac::Cursor*		inDC,
											uint				inFBP, uint inSizeX, uint inPSM, uint inFBMSK=0 );

	void		Set_MIPTBP1_2			(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 );
	void		Set_MIPTBP2_2			(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 );
	void		Set_MIPTBP_2			(	dmac::Cursor*		inDC,
											uint				inSizeX, uint inTBP1, uint inTBP2, uint inTBP3, uint inTBP4, uint inTBP5, uint inTBP6 );

	void		Set_SCISSOR_2			(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3 );

	void		Set_TEST_2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7 );

	void		Set_TEX0_2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7=0, uint inF8=0, uint inF9=0, uint inF10=0, uint inF11=0 );

	void		Set_TEX1_2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6 );
	void		Nearest_TEX1_2			(	dmac::Cursor*		inDC		);
	void		Linear_TEX1_2			(	dmac::Cursor*		inDC		);

	void		Set_TEX2_2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 );

	void		SetTexture_2			(	dmac::Cursor*		inDC,
											uint				inTBP, uint inSizeX, uint inSizeY, uint inPSM, uint inTFX, uint inMMAG_MMIN, int inCBP = -1 );

	void		Set_XYOFFSET_2			(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1 );
	void		Set_fXYOFFSET_2			(	dmac::Cursor*		inDC,
											float				inF0, float inF1 );

	void		Set_ZBUF_2				(	dmac::Cursor*		inDC,
											uint				inF0, uint inF1, uint inF2 );
}


#include "EE_gs_dmac_I.h"


#endif // _EE_GS_DMAC_H_


