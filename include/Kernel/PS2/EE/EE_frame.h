/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_FRAME_H_
#define _EE_FRAME_H_



namespace frame {


	//
	// Frame's Performer
	interface Packet;
	interface GsContext;

	struct Agent : public NvInterface {
		virtual	NvInterface*	GetBase			(								) {		return this;	}
		virtual	void			UpdatePacket	(	Packet*			inPkt		) {}
		virtual	void			UpdateGsContext	(	GsContext*		inGsCtxt	) {}
	};



	//
	// GS A+D Context
	// A GS context owner can be set to access GS registers after the frame manager update.

	struct GsContext {
		// General context
		uint32			giftag[2];		// PACKED GifTag [ + PRIM to reset the primitive context !! ]
		uint32			__0x0e;			// A+D
		uint32			frame[2];		// sceGsFrame
		uint32			__0x4c;
		uint32			xyoffset[2];	// sceGsXyoffset
		uint32			__0x18;
		uint32			scissor[2];		// sceGsScissor
		uint32			__0x40;
		uint32			zbuf[2];		// sceGsZbuf
		uint32			__0x4e;
		uint32			test[2];		// sceGsTest
		uint32			__0x47;
		uint32			alpha[2];		// sceGsAlpha
		uint32			__0x42;
		uint32			colclamp[2];	// sceGsColclamp
		uint32			__0x46;
		// Texturing context
		uint32			texflush[2];	// sceGsTexflush
		uint32			__0x3f;
		uint32			tex1[2];		// sceGsTex1 (before tex0!)
		uint32			__0x14;
		uint32			tex0[2];		// sceGsTex0
		uint32			__0x06;
		uint32			mip1[2];		// sceGsMiptbp1
		uint32			__0x34;
		uint32			mip2[2];		// sceGsMiptbp2
		uint32			__0x36;
		uint32			clamp[2];		// sceGsClamp
		uint32			__0x08;
		// Other GS settings
		Vec2			stOffset;
		// Some flags
		Agent*	 		owner;			// Agent owner of the context, called after GsContext is finally updated by the frame manager !
		uint32			owner_data[3];

		void			Init		(	uint			inPRIM=0	);	// Init without texturing context
		uint			GetPRIM		(								);
		void			SetPRIM		(	uint			inPRIM		);
		void			DisableTex	(								);
		void			EnableTex	(								);
		void			ClearTCC	(								);	// tex0.TCC = 0

		// Apply'ing is :
		// REG		|	unchanged FIELD		|	overwritten FIELDS
		// ---------|-----------------------|----------------------
		//    tex0	|	TFX					|	all others
		//    tex1	|	MMAG MMIN L K		|	all others
		// miptbp1	|	none				|	all
		// miptbp2	|	none				|	all
		void			ApplyTexTRX	(	tram::TexTRX*	inTRX	);
	};


	//
	// Texturing Context

	struct TexContext {
		GsContext*		gsContext;			// GS A+D context, partially overwritten by the frame manager
		int16			id;					// tram:: texture id (-1 = none)
		uint8			LODMask;			// tex LOD mask
		TexContext*		next;				// next context ...
	};


	//
	// Frame's Packet
	//
	// Packet managment flexibility enables you to :
	// - sort independant packet using a user 32bits hash-key
	// - delimit the sort range by introducting packets defined as separator packets
	// - manage multi-pass drawing using sub packets
	//
	// Caution:
	// - link[2] is reserved for the frame manager !!

	struct Packet {
		Agent*	 		owner;				// Agent owner of the packet

		uint32			hkey;				// sort hash-key
		uint32*			tadr;				// start dmac TADR
		uint32*			link;				// link  dmac TADR (Caution: link[2] is reserved for the frame manager !!)
		int				mcId;				// VU1 only microcode id (cf vu:: namespace)
		TexContext*		texList;			// tex context list
		Packet*			subList;			// sub packet for another VU1 microcode drawing pass

		void			Init				(	Agent*		inOwner = NULL	);
		void			SetSeparator		(	bool		inEnable		);		// separator  ?
		void			SetPATH3Compliant	(	bool		inTrueFalse		);		// PATH3 safe ?
		void			SetResetMCCache		(	bool		inEnable		);		// mc reset   ?
		void			SetNeedUpdate		(	bool		inEnable		);		// owner->UpdatePacket() ?
		void			Translate			(	uint32		inBOffset		);		// tadr/link
		bool			IsDrawing			(								);
		bool			IsTextured			(								);
		void			Append				(	TexContext*	inTexContext	);
		void			Append				(	Packet*		inSubPkt		);
		void			BuildBasicHKey		(								);

		// private ...
		enum {
			FLG_SEPARATOR		= (1<<0),	// => { Sort(); Push(); Sort(); }
			FLG_DISABLE_PATH3	= (1<<1),	// packet is not safe with path3 concurrent trxing !
			FLG_RESET_MCCACHE	= (1<<2),	// vu:: VU1 mc cache is reset before
			FLG_UPD_REQUESTED	= (1<<3),	// update event requested on packet
			STT_UPD_PENDING		= (1<<4),	// Update wait end of drawing process
			STT_DRAWING			= (1<<5)	// Is drawing
		};
		uint			flags;
	};


	//
	// Init/Reset

	void		Init					(													);
	void		Reset					(													);
	void		Shut					(													);


	//
	// Frame's Queue Building

	void		Begin					(													);
	void		Push					(	Packet*		inPkt								);
	void		Sort					(	/* according to Packet::hkey */					);
	void		End						(													);


	//
	// Frame Sync & Flush

	void		WaitDMA					(													);
	void		Sync					(													);
	void		Flush					(	bool		inASync	= TRUE,								// Asynchronous ?
											bool		inVSync = TRUE						);		// Waits V-Blank ?


	//
	// Statistics

	uint		GetQSize				(													);
	Packet*		GetQEntry				(	uint		inNo	/* in [0,GetQSize()-1] */	);

	pvoid		GetAsyncFirstCH1		(													);
	pvoid		GetAsyncFirstCH2		(													);

	enum Performance {
		PERF_BEGIN_END		= 0,
		PERF_BEGIN_BEGIN,
		PERF_SYNC_UPDATE,
		PERF_SYNC_UPDATE_IDLE,
		PERF_PREPARE_FLUSH,
		PERF_VSYNC_IDLE,
		PERF_FLUSH,
		PERF_DMA,
		PERF_WAIT_DMA,
		PERF_TRXD_TEXBSIZE
	};

	float		GetPerformance			(	Performance	inPerf								);
}


#include "EE_frame_I.h"


#endif // _EE_FRAME_H_


