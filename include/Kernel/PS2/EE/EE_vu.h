/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _EE_VU_H_
#define _EE_VU_H_



namespace vu
{

	//
	//	Init/Reset

	void			Init			(											);
	void			Reset			(											);
	void			Shut			(											);


	//
	//	Control

	uint32			GetVPU_STAT		(											);
	uint16			GetVPU_TPC		(	int					inVUNo				);	// 0/1
	bool			IsVIFBusy		(	int					inVUNo				);	// 0/1
	bool			IsVPUBusy		(	int					inVUNo				);	// 0/1
	void			SyncVIF			(	int					inVUNo				);	// 0/1
	void			SyncVPU			(	int					inVUNo				);	// 0/1
	uint16			GetVU1_IR		(	int					inIRNo				);	// in [0,15]
	Vec4			GetVU1_FR		(	int					inIFNo				);	// in [0,31]
	bool			ContinueVU1		(	uint16				inAddr				);
	uint16			StepVU1			(	uint32				inCycDelay = 0		);
	void			ForceBreak		(	int					inVUNo				);	// 0/1
	void			Break_PATH12	(											);	// stop DMAC-CH1/2 - VU0/1


	//
	// Register a VU micro-code.
	// Returns an integer micro-code ID (>=0) or -1 if an error occurs.

	int				RegisterMC		(	int					inVUNo,					// 0/1
										uint*				inMCStart,
										uint*				inMCEnd				);


	//
	//	VU0/1 MC-Cache
	//
	//	ResetMCCache	: Reset the cache. Following Request or Setup restart from 0.
	//					  The corresponding vu-mem content is undefined.
	//
	//	UploadMC		: request a MC to be uploaded in the MC-Cache ([MPG])
	//					  returns FALSE if no upload is needed (MC already in MC-CACHE)
	//
	//	RequestMC		: request a MC to be uploaded & activated in the MC-Cache ([MPG+]MSCAL)
	//					  returns the dmac *link* pointer
	//					  VIF BASE/OFFSET are not fixed !
	//
	//	GetMCExecIAddr	: returns current MC execution address
	//					  returns -1 if MC isn't present in the vu-mem

	void			ResetMCCache	(	int					inVUNo				);	// 0/1

	bool			UploadMC		(	dmac::Cursor*		inDC,
										int					inMCNo				);

	uint32*			RequestMC		(	dmac::Cursor*		inDC,
										int					inMCNo				);

	int				GetMCExecIAddr	(	int					inMCNo				);

}


#endif // _EE_VU_H_



