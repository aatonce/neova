/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef _EE_IPU_H_
#define _EE_IPU_H_




namespace ipu
{

	struct Callback {
		// inStreamNo: 0 (input stream) / 1 (output stream)
		virtual void		OnStreamEnded	(	int		inStreamId		) = 0;
	};


	struct Stream {
		uint32		ch4_tadr;			// toIPU
		uint32		ch4_madr;			// toIPU
		uint		ch4_qwc;			// toIPU
		uint32		ch3_madr;			// fromIPU
		uint32		ch3_qwc;			// fromIPU
		uint		in_fifo_qwc;		// IPU in_FIFO
		uint		bp;					// IPU_BP.BP
	};



	void			Init				(													);
	void			Reset				(													);
	void			Shut				(													);

	Callback*		GetCallback			(													);
	void			SetCallback			(	Callback*			inCallback					);

	void			Sync				(													);
	bool			IsBusy				(													);
	bool			IsError				(													);

	// frame management

	bool			Has_InputStream		(													);
	bool			Has_OutputStream	(													);
	void			GetStreams			(	Stream&				outStream					);

	bool			BeginFrame_Input	(	uint				inBP,								// in [0,127]
											void*				inPtr_0,
											uint				inQSize_0,
											void*				inPtr_1			= NULL,
											uint				inQSize_1		= 0			);

	bool			BeginFrame_Output	(	void*				inPtr,
											uint				inQSize						);

	void			EndFrame			(	Stream&				outStream					);
	void			ResetFrame			(													);
}




#endif // _EE_IPU_H_


