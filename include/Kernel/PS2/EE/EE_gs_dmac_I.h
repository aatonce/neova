/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _EE_GS_DMAC_H_


inline
void
gs::SetADReg	(	dmac::Cursor*		inDC,
					uint				inReg,
					uint64				inValue		)
{
	NV_ASSERT_DC( inDC );
	*(inDC->i64++) = inValue;
	*(inDC->i64++) = inReg;
}


//
// A+D regs shared by contexts

inline
void
gs::Set_BITBLTBUF		(	dmac::Cursor*		inDC,
							uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 )
{
	SetADReg( inDC, SCE_GS_BITBLTBUF, SCE_GS_SET_BITBLTBUF(inF0,inF1,inF2,inF3,inF4,inF5) );
}

inline
void
gs::Set_Clamp_COLCLAMP		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_COLCLAMP, 1 );
}

inline
void
gs::Set_Wrap_COLCLAMP		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_COLCLAMP, 0 );
}

inline
void
gs::Set_DIMX				(	dmac::Cursor*		inDC,
								uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7, uint inF8, uint inF9, uint inF10, uint inF11, uint inF12, uint inF13, uint inF14, uint inF15 )
{
	SetADReg( inDC, SCE_GS_DIMX, SCE_GS_SET_DIMX(inF0,inF1,inF2,inF3,inF4,inF5,inF6,inF7,inF8,inF9,inF10,inF11,inF12,inF13,inF14,inF15) );
}

inline
void
gs::Enable_DTHE			(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_DTHE, 1 );
}

inline
void
gs::Disable_DTHE			(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_DTHE, 0 );
}

inline
void
gs::Write_FINISH			(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_FINISH, 0 );
}

inline
void
gs::Set_FOG			(	dmac::Cursor*		inDC,
								uint				inF0		)
{
	SetADReg( inDC, SCE_GS_FOG, SCE_GS_SET_FOG(inF0) );
}

inline
void
gs::Set_FOGCOL		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2 )
{
	SetADReg( inDC, SCE_GS_FOGCOL, SCE_GS_SET_FOGCOL(inF0,inF1,inF2) );
}

inline
void
gs::Set_LABEL			(	dmac::Cursor*		inDC,
							uint				inF0, uint inF1 )
{
	SetADReg( inDC, SCE_GS_LABEL, SCE_GS_SET_LABEL(inF0,inF1) );
}

inline
void
gs::Enable_PABE		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_PABE, SCE_GS_SET_PABE(1) );
}

inline
void
gs::Disable_PABE		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_PABE, SCE_GS_SET_PABE(0) );
}

inline
void
gs::Set_PRIM		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7, uint inF8 )
{
	SetADReg( inDC, SCE_GS_PRIM, SCE_GS_SET_PRIM(inF0,inF1,inF2,inF3,inF4,inF5,inF6,inF7,inF8) );
}

inline
void
gs::Set_PRMODE		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7 )
{
	SetADReg( inDC, SCE_GS_PRMODE, SCE_GS_SET_PRMODE(inF0,inF1,inF2,inF3,inF4,inF5,inF6,inF7) );
}

inline
void
gs::Enable_PRMODE			(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_PRMODECONT, SCE_GS_SET_PRMODECONT(0) );
}

inline
void
gs::Disable_PRMODE		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_PRMODECONT, SCE_GS_SET_PRMODECONT(1) );
}

inline
void
gs::Set_RGBAQ		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4 )
{
	SetADReg( inDC, SCE_GS_RGBAQ, SCE_GS_SET_RGBAQ(inF0,inF1,inF2,inF3,inF4) );
}

inline
void
gs::Set_RGBAQ		(	dmac::Cursor*		inDC,
						uint				inABGR, uint inQ )
{
	SetADReg( inDC, SCE_GS_RGBAQ, (((u_long)inQ)<<32) | (u_long)inABGR );
}

inline
void
gs::Set_SCANMSK		(	dmac::Cursor*		inDC,
						uint				inF0		)
{
	SetADReg( inDC, SCE_GS_SCANMSK, SCE_GS_SET_SCANMSK(inF0) );
}

inline
void
gs::Set_SIGNAL		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1	)
{
	SetADReg( inDC, SCE_GS_SIGNAL, SCE_GS_SET_SIGNAL(inF0,inF1) );
}

inline
void
gs::Set_ST	(	dmac::Cursor*		inDC,
				uint				inF0, uint inF1	)
{
	SetADReg( inDC, SCE_GS_ST, SCE_GS_SET_ST(inF0,inF1) );
}

inline
void
gs::Set_TEXA		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2	)
{
	SetADReg( inDC, SCE_GS_TEXA, SCE_GS_SET_TEXA(inF0,inF1,inF2) );
}

inline
void
gs::Set_TEXCLUT		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2	)
{
	SetADReg( inDC, SCE_GS_TEXCLUT, SCE_GS_SET_TEXCLUT(inF0,inF1,inF2) );
}

inline
void
gs::Write_TEXFLUSH		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_TEXFLUSH, 0 );
}

inline
void
gs::Set_TRXDIR	(	dmac::Cursor*		inDC,
					uint				inF0		)
{
	SetADReg( inDC, SCE_GS_TRXDIR, SCE_GS_SET_TRXDIR(inF0) );
}

inline
void
gs::Set_TRXPOS		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4 )
{
	SetADReg( inDC, SCE_GS_TRXPOS, SCE_GS_SET_TRXPOS(inF0,inF1,inF2,inF3,inF4) );
}

inline
void
gs::Set_TRXREG		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1 )
{
	SetADReg( inDC, SCE_GS_TRXREG, SCE_GS_SET_TRXREG(inF0,inF1) );
}

inline
void
gs::Set_UV	(	dmac::Cursor*		inDC,
				uint				inF0, uint inF1 )
{
	SetADReg( inDC, SCE_GS_UV, SCE_GS_SET_UV(inF0,inF1) );
}

inline
void
gs::Set_UVi	(	dmac::Cursor*		inDC,
				uint				inF0, uint inF1 )
{
	SetADReg( inDC, SCE_GS_UV, SCE_GS_SET_UV(inF0<<4,inF1<<4) );
}

inline
void
gs::Set_UVic(	dmac::Cursor*		inDC,
				uint				inF0, uint inF1 )
{
	SetADReg( inDC, SCE_GS_UV, SCE_GS_SET_UV((inF0<<4)+8,(inF1<<4)+8) );
}

inline
void
gs::Set_UVf	(	dmac::Cursor*		inDC,
				float				inF0, float inF1 )
{
	SetADReg( inDC, SCE_GS_UV, SCE_GS_SET_UV(inF0*16.0f,inF1*16.0f) );
}

inline
void
gs::Set_XYZ2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2 )
{
	SetADReg( inDC, SCE_GS_XYZ2, SCE_GS_SET_XYZ2(inF0,inF1,inF2) );
}

inline
void
gs::Set_XYZ2i		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2 )
{
	SetADReg( inDC, SCE_GS_XYZ2, SCE_GS_SET_XYZ2(inF0<<4,inF1<<4,inF2) );
}

inline
void
gs::Set_XYZ2f		(	dmac::Cursor*		inDC,
						float				inF0, float inF1, uint inF2 )
{
	SetADReg( inDC, SCE_GS_XYZ2, SCE_GS_SET_XYZ2(inF0*16.0f,inF1*16.0f,inF2) );
}

inline
void
gs::Set_XYZF2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3 )
{
	SetADReg( inDC, SCE_GS_XYZF2, SCE_GS_SET_XYZF2(inF0,inF1,inF2,inF3) );
}

inline
void
gs::Set_XYZF2i		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3 )
{
	SetADReg( inDC, SCE_GS_XYZF2, SCE_GS_SET_XYZF2(inF0<<4,inF1<<4,inF2,inF3) );
}

inline
void
gs::Set_XYZF2f		(	dmac::Cursor*		inDC,
						float				inF0, float inF1, uint inF2, uint inF3 )
{
	SetADReg( inDC, SCE_GS_XYZF2, SCE_GS_SET_XYZF2(inF0*16.0f,inF1*16.0f,inF2,inF3) );
}

inline
void
gs::Set_XYZ3		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2 )
{
	SetADReg( inDC, SCE_GS_XYZ3, SCE_GS_SET_XYZ3(inF0,inF1,inF2) );
}

inline
void
gs::Set_XYZ3i		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2 )
{
	SetADReg( inDC, SCE_GS_XYZ3, SCE_GS_SET_XYZ3(inF0<<4,inF1<<4,inF2) );
}

inline
void
gs::Set_XYZ3f		(	dmac::Cursor*		inDC,
						float				inF0, float inF1, uint inF2 )
{
	SetADReg( inDC, SCE_GS_XYZ3, SCE_GS_SET_XYZ3(inF0*16.0f,inF1*16.0f,inF2) );
}

inline
void
gs::Set_XYZF3		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3 )
{
	SetADReg( inDC, SCE_GS_XYZF3, SCE_GS_SET_XYZF3(inF0,inF1,inF2,inF3) );
}

inline
void
gs::Set_XYZF3i		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3 )
{
	SetADReg( inDC, SCE_GS_XYZF3, SCE_GS_SET_XYZF3(inF0<<4,inF1<<4,inF2,inF3) );
}

inline
void
gs::Set_XYZF3f		(	dmac::Cursor*		inDC,
						float				inF0, float inF1, uint inF2, uint inF3 )
{
	SetADReg( inDC, SCE_GS_XYZF3, SCE_GS_SET_XYZF3(inF0*16.0f,inF1*16.0f,inF2,inF3) );
}


//
// A+D regs in context 2

inline
void
gs::Set_ALPHA_2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4 )
{
	SetADReg( inDC, SCE_GS_ALPHA_2, SCE_GS_SET_ALPHA_2(inF0,inF1,inF2,inF3,inF4) );
}

inline
void
gs::Set_CLAMP_2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 )
{
	SetADReg( inDC, SCE_GS_CLAMP_2, SCE_GS_SET_CLAMP_2(inF0,inF1,inF2,inF3,inF4,inF5) );
}

inline
void
gs::Set_Clamp_CLAMP_2	(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_CLAMP_2, SCE_GS_SET_CLAMP_2(1,1,0,0,0,0) );
}

inline
void
gs::Set_Repeat_CLAMP_2		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_CLAMP_2, 0 );
}

inline
void
gs::Enable_FBA_2		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_FBA_2, SCE_GS_SET_FBA_2(1) );
}

inline
void
gs::Disable_FBA_2		(	dmac::Cursor*		inDC		)
{
	SetADReg( inDC, SCE_GS_FBA_2, SCE_GS_SET_FBA_2(0) );
}

inline
void
gs::Set_FRAME_2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3 )
{
	SetADReg( inDC, SCE_GS_FRAME_2, SCE_GS_SET_FRAME_2(inF0,inF1,inF2,inF3) );
}

inline
void
gs::SetFrame_2		(	dmac::Cursor*		inDC,
						uint				inFBP, uint inSizeX, uint inPSM, uint inFBMSK )
{
	Set_FRAME_2( inDC, inFBP, (inSizeX+63)>>6, inPSM, inFBMSK );
}

inline
void
gs::Set_MIPTBP1_2	(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 )
{
	SetADReg( inDC, SCE_GS_MIPTBP1_2, SCE_GS_SET_MIPTBP1_2(inF0,inF1,inF2,inF3,inF4,inF5) );
}

inline
void
gs::Set_MIPTBP2_2		(	dmac::Cursor*		inDC,
							uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 )
{
	SetADReg( inDC, SCE_GS_MIPTBP2_2, SCE_GS_SET_MIPTBP2_2(inF0,inF1,inF2,inF3,inF4,inF5) );
}

inline
void
gs::Set_MIPTBP_2		(	dmac::Cursor*		inDC,
							uint				inSizeX, uint inTBP1, uint inTBP2, uint inTBP3, uint inTBP4, uint inTBP5, uint inTBP6 )
{
	Set_MIPTBP1_2( inDC, inTBP1, ((inSizeX>>1)+63)>>6, inTBP2, ((inSizeX>>2)+63)>>6, inTBP3, ((inSizeX>>3)+63)>>6 );
	Set_MIPTBP2_2( inDC, inTBP4, ((inSizeX>>4)+63)>>6, inTBP5, ((inSizeX>>5)+63)>>6, inTBP6, ((inSizeX>>6)+63)>>6 );
}

inline
void
gs::Set_SCISSOR_2		(	dmac::Cursor*		inDC,
							uint				inF0, uint inF1, uint inF2, uint inF3 )
{
	SetADReg( inDC, SCE_GS_SCISSOR_2, SCE_GS_SET_SCISSOR_2(inF0,inF1,inF2,inF3) );
}

inline
void
gs::Set_TEST_2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7 )
{
	SetADReg( inDC, SCE_GS_TEST_2, SCE_GS_SET_TEST_2(inF0,inF1,inF2,inF3,inF4,inF5,inF6,inF7) );
}

inline
void
gs::Set_TEX0_2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6, uint inF7, uint inF8, uint inF9, uint inF10, uint inF11 )
{
	SetADReg( inDC, SCE_GS_TEX0_2, SCE_GS_SET_TEX0_2(inF0,inF1,inF2,inF3,inF4,inF5,inF6,inF7,inF8,inF9,inF10,inF11) );
}

inline
void
gs::Set_TEX1_2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5, uint inF6 )
{
	SetADReg( inDC, SCE_GS_TEX1_2, SCE_GS_SET_TEX1_2(inF0,inF1,inF2,inF3,inF4,inF5,inF6) );
}

inline
void
gs::Nearest_TEX1_2		(	dmac::Cursor*		inDC		)
{
	Set_TEX1_2( inDC, 0, 0, 0, 0, 0, 0, 0 );
}

inline
void
gs::Linear_TEX1_2		(	dmac::Cursor*		inDC		)
{
	Set_TEX1_2( inDC, 0, 0, 1, 1, 0, 0, 0 );
}

inline
void
gs::Set_TEX2_2		(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1, uint inF2, uint inF3, uint inF4, uint inF5 )
{
	SetADReg( inDC, SCE_GS_TEX2_2, SCE_GS_SET_TEX2_2(inF0,inF1,inF2,inF3,inF4,inF5) );
}

inline
void
gs::SetTexture_2	(	dmac::Cursor*		inDC,
						uint				inTBP, uint inSizeX, uint inSizeY, uint inPSM, uint inTFX, uint inMMAG_MMIN, int inCBP )
{
	Write_TEXFLUSH( inDC );
	if( inMMAG_MMIN )	Linear_TEX1_2( inDC );
	else				Nearest_TEX1_2( inDC );
	uint wP2 = GetCeilPow2( inSizeX );
	uint hP2 = GetCeilPow2( inSizeY );
	NV_ASSERT( wP2 <= 10 );
	NV_ASSERT( hP2 <= 10 );
	Set_TEX0_2(	inDC,
				inTBP,
				(inSizeX+63)>>6,
				inPSM,
				wP2, hP2,
				1,
				inTFX,
				(inCBP>=0)?inCBP:0, 0, 0, 0, (inCBP>=0)?1:0 );
}

inline
void
gs::Set_XYOFFSET_2	(	dmac::Cursor*		inDC,
						uint				inF0, uint inF1 )
{
	SetADReg( inDC, SCE_GS_XYOFFSET_2, SCE_GS_SET_XYOFFSET_2(inF0,inF1) );
}

inline
void
gs::Set_fXYOFFSET_2	(	dmac::Cursor*		inDC,
						float				inF0, float inF1 )
{
	Set_XYOFFSET_2( inDC, int(inF0*16.0f), int(inF1*16.0f) );
}

inline
void
gs::Set_ZBUF_2	(	dmac::Cursor*		inDC,
					uint				inF0, uint inF1, uint inF2 )
{
	SetADReg( inDC, SCE_GS_ZBUF_2, SCE_GS_SET_ZBUF_2(inF0,inF1,inF2) );
}


#endif // _EE_GS_DMAC_H_


