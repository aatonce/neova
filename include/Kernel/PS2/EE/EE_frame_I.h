/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _EE_FRAME_H_



//
// GsContext

inline uint frame::GsContext::GetPRIM	(								)
{
	return (giftag[1]>>15)&0x7FF;
}

inline void frame::GsContext::SetPRIM	(	uint			inPRIM		)
{
	giftag[1] = (giftag[1]&(~(0x7FF<<15)))|((inPRIM&0x7FF)<<15);
}

inline void frame::GsContext::DisableTex	(								)
{
	__0x3f=__0x14=__0x06=__0x34=__0x36=__0x08=SCE_GS_NOP;
}

inline void frame::GsContext::EnableTex	(								)
{
	__0x3f=0x3f; __0x14=0x14; __0x06=0x6; __0x34=0x34; __0x36=0x36; __0x08=0x8;
}

inline void frame::GsContext::ClearTCC	(								)
{
	tex0[1] &= ~(1<<2);	// tex0.TCC = 0
}




//
//	Packet

inline void frame::Packet::SetSeparator		(	bool	inEnable	)
{
	if( inEnable )	flags |= FLG_SEPARATOR;
	else			flags &= ~(FLG_SEPARATOR);
}


inline void frame::Packet::SetPATH3Compliant	(	bool		inTrueFalse	)
{
	if( inTrueFalse )	flags &= ~(FLG_DISABLE_PATH3);
	else				flags |= FLG_DISABLE_PATH3;
}


inline void frame::Packet::SetResetMCCache	(	bool	inEnable	)
{
	if( inEnable )	flags |= FLG_RESET_MCCACHE;
	else			flags &= ~(FLG_RESET_MCCACHE);
}


inline void frame::Packet::SetNeedUpdate	(	bool	inEnable	)
{
	if( inEnable )	flags |= FLG_UPD_REQUESTED;
	else			flags &= ~(FLG_UPD_REQUESTED);
}


inline bool frame::Packet::IsDrawing		(						)
{
	return ( flags & STT_DRAWING ) != 0;
}



#endif // _EE_FRAME_H_


