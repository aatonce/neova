/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_EE_DEFS_H_
#define	_EE_DEFS_H_


// SCE Headers
#include <eekernel.h>
#include <eestruct.h>
#include <eeregs.h>
#include <libgraph.h>
#include <libdma.h>
#include <libdev.h>
#include <libsdr.h>
#include <sifdev.h>



#define	IMPORT_TEXT( _SYMBOL )				extern u_int _SYMBOL [] __attribute__((section(".text")))
#define	IMPORT_DATA( _SYMBOL )				extern u_int _SYMBOL [] __attribute__((section(".data")))
#define	IMPORT_RO_DATA( _SYMBOL )			extern u_int _SYMBOL [] __attribute__((section(".rodata")))
#define	IMPORT_VU_DATA( _SYMBOL )			extern u_int _SYMBOL [] __attribute__((section(".vudata")))
#define	IMPORT_VU_TEXT( _SYMBOL )			extern u_int _SYMBOL [] __attribute__((section(".vutext")))
#define	IMPORT_VU_CODE( _START, _END )		IMPORT_VU_TEXT( _START ); IMPORT_VU_TEXT( _END );
#define	IMPORT_VU_CODE_NAME( _NAME )		IMPORT_VU_CODE( _NAME##_CodeStart, _NAME##_CodeEnd )


// D$ alignment (64 bytes) buffer to prevent DMA/D$ conflit
inline void* AllocDMABuffer( uint32 inBSize )
{
	uint32 uncBSize = Round64( inBSize );
	void*  uncPtr   = NvMallocA( uncBSize, 64 );
	return uncPtr;
}

// DMA memory bit mask
#define DMA_SPR								(0x80000000)
#define DMA_MEM								(0x0FFFFFFF)
#define	DMA_ADDR( inADDR )					(uint32(inADDR)&DMA_MEM)

// SPR Area
#define	SPR_START							(0x70000000)
#define	SPR_HALF							(0x70002000)
#define SPR_END								(0x70004000)
#define	SPR_MEM								(0x70003FFF)
#define	SPR_ADDR( inADDR )					(uint32(inADDR)&SPR_MEM)

// Unchached memory bit mask
#define	UNCACHED_MEM						(0x20000000)
#define	FAST_UNCACHED_MEM					(0x30000000)

template <typename T>
inline T CachedPointer( T inPtr )
{
	return (T)( DMA_ADDR(inPtr) );
}

template <typename T>
inline T UncachedPointer( T inPtr )
{
	return (T)( DMA_ADDR(inPtr) | UNCACHED_MEM );
}

template <typename T>
inline T FastUncachedPointer( T inPtr )
{
	return (T)( DMA_ADDR(inPtr) | FAST_UNCACHED_MEM );
}

inline void sceResetEE	(	)
{
	sceDevVif0Reset();		// VIF0 including VIF0-FIFO
	sceDevVif1Reset();		// VIF1 including VIF0-FIFO
	sceDevVu0Reset();
	sceDevVu1Reset();
	sceGsResetPath();
	sceDmaReset(1);
}



#define SCE_VIF1_SET_UNPACK_R( vuaddr, num, cmd, irq )		( SCE_VIF1_SET_UNPACK( vuaddr, num, cmd, irq ) | (1<<15) )
#define SCE_VIF1_SET_UNPACK_U( vuaddr, num, cmd, irq )		( SCE_VIF1_SET_UNPACK( vuaddr, num, cmd, irq ) | (1<<14) )
#define SCE_VIF1_SET_UNPACK_M( vuaddr, num, cmd, irq )		( SCE_VIF1_SET_UNPACK( vuaddr, num, cmd, irq ) | (1<<28) )
#define SCE_VIF1_SET_UNPACK_RU( vuaddr, num, cmd, irq )		( SCE_VIF1_SET_UNPACK( vuaddr, num, cmd, irq ) | (1<<15) | (1<<14) )
#define SCE_VIF1_SET_UNPACK_MR( vuaddr, num, cmd, irq )		( SCE_VIF1_SET_UNPACK( vuaddr, num, cmd, irq ) | (1<<28) | (1<<15) )
#define SCE_VIF1_SET_UNPACK_MU( vuaddr, num, cmd, irq )		( SCE_VIF1_SET_UNPACK( vuaddr, num, cmd, irq ) | (1<<14) | (1<<28) )
#define SCE_VIF1_SET_UNPACK_MRU( vuaddr, num, cmd, irq )	( SCE_VIF1_SET_UNPACK( vuaddr, num, cmd, irq ) | (1<<28) | (1<<14) | (1<<15) )

#define UNPACK_S_32		0
#define UNPACK_S_16		1
#define UNPACK_S_8		2
#define UNPACK_V2_32	4
#define UNPACK_V2_16	5
#define UNPACK_V2_8		6
#define UNPACK_V3_32	8
#define UNPACK_V3_16	9
#define UNPACK_V3_8		10
#define UNPACK_V4_32	12
#define UNPACK_V4_16	13
#define UNPACK_V4_8		14
#define UNPACK_V4_5		15

#define	SCE_VIF1_SET_OPEN_PATH3( irq )							(0 | ((u_int)0x06 << 24) | ((u_int)(irq) << 31))
#define	SCE_VIF1_SET_CLOSE_PATH3( irq )							(0xFFFF | ((u_int)0x06 << 24) | ((u_int)(irq) << 31))

#define SCE_GIF_SET_PACKED_TAG( nloop, eop, pre, prim, nreg )	SCE_GIF_SET_TAG(nloop, eop, pre, prim, SCE_GIF_PACKED, nreg)
#define SCE_GIF_SET_REGLIST_TAG( nloop, eop, nreg )				SCE_GIF_SET_TAG(nloop, eop, 0, 0, SCE_GIF_REGLIST, nreg)
#define SCE_GIF_SET_IMAGE_TAG( nloop, eop )						SCE_GIF_SET_TAG(nloop, eop, 0, 0, SCE_GIF_IMAGE, 0)

#define REG0( reg )		(uint64(reg) << 0)
#define REG1( reg )		(uint64(reg) << 4)
#define REG2( reg )		(uint64(reg) << 8)
#define REG3( reg )		(uint64(reg) << 12)
#define REG4( reg )		(uint64(reg) << 16)
#define REG5( reg )		(uint64(reg) << 20)
#define REG6( reg )		(uint64(reg) << 24)
#define REG7( reg )		(uint64(reg) << 28)
#define REG8( reg )		(uint64(reg) << 32)
#define REG9( reg )		(uint64(reg) << 36)
#define REG10( reg )	(uint64(reg) << 40)
#define REG11( reg )	(uint64(reg) << 44)
#define REG12( reg )	(uint64(reg) << 48)
#define REG13( reg )	(uint64(reg) << 52)
#define REG14( reg )	(uint64(reg) << 56)
#define REG15( reg )	(uint64(reg) << 60)

#define SCE_GS_A_D		0x0e
#undef	SCE_GS_NOP
#define SCE_GS_NOP		0x0f

#define DMA_TAG_REFE	uint32(0 << 28)
#define DMA_TAG_CNT		uint32(1 << 28)
#define DMA_TAG_NEXT	uint32(2 << 28)
#define DMA_TAG_REF		uint32(3 << 28)
#define DMA_TAG_REFS	uint32(4 << 28)
#define DMA_TAG_CALL	uint32(5 << 28)
#define DMA_TAG_RET		uint32(6 << 28)
#define DMA_TAG_END		uint32(7 << 28)
#define	DMA_TAG_MASK	uint32(7 << 28)

#define	SET_DMATAG_RET(  TADR	 )		{	((uint16*)TADR)[1] = (DMA_TAG_RET>>16);		}
#define	SET_DMATAG_END(  TADR	 )		{	((uint16*)TADR)[1] = (DMA_TAG_END>>16);		}
#define	SET_DMATAG_NEXT( TADR, ADDR )	{	((uint16*)TADR)[1] = (DMA_TAG_NEXT>>16);	\
											((uint32*)TADR)[1] = DMA_ADDR(ADDR);		}


//
// Pipelines sync

inline void SyncMemory()
{
	// sync loads/stores, invalid UCAB ro-cache, write-back WBB wo-cache
	asm volatile ("sync.l");
}

inline void SyncPipeline()
{
	asm volatile ("sync.p");
}

inline void SyncRW()
{
	asm volatile ("sync.l;sync.p");
}

inline void SafeSyncDCache( pvoid begin, pvoid end )
{
	NV_ASSERT_ALIGNED( begin, 64 );	// check 64bytes boundaries
	NV_ASSERT_ALIGNED( end, 64 );
	SyncDCache( begin, end );
}


//
// Cache Prefetching

inline void PrefetchDCache( pvoid inAddr, uint inBSize )
{
	register uint32 tmp0, tmp1, tmp2;
	asm volatile (
	"	.set noreorder\n"
	"	di\n"
	"	add		%0, $0, %3\n"
	"	add		%1, %4, %3\n"
	"0:	sync.l\n"
	"	pref	0,0(%0)\n"
	"	sync.l\n"
	"	addiu	%0, %0, 64\n"
	"	nop\n"
	"	slt		%2, %0, %1\n"
	"	nop\n"
	"	bgtz	%2, 0b\n"
	"	nop\n"
	"	ei\n"
	"	.set reorder\n"
	: "=&r"(tmp0), "=&r"(tmp1), "=&r"(tmp2)
	  : "r"(inAddr), "r"(inBSize) );
}

inline void PrefetchICache( pvoid inAddr, uint inBSize )
{
	register uint32 tmp0, tmp1, tmp2;
	asm volatile (
	"	.set noreorder\n"
	"	di\n"
	"	add		%0, $0, %3\n"
	"	add		%1, %4, %3\n"
	"0:	sync.l\n"
	"	cache	0x0e, 0(%0)\n"
	"	sync.l\n"
	"	addiu	%0, %0, 64\n"
	"	nop\n"
	"	slt		%2, %0, %1\n"
	"	nop\n"
	"	bgtz	%2, 0b\n"
	"	nop\n"
	"	ei\n"
	"	.set reorder\n"
	  : "=&r"(tmp0), "=&r"(tmp1), "=&r"(tmp2)
	  : "r"(inAddr), "r"(inBSize) );
}

inline void InvalidateICache( pvoid inAddr, uint inBSize )
{
	register uint32 tmp0, tmp1, tmp2;
	asm volatile (
	"	.set noreorder\n"
	"	di\n"
	"	add		%0, $0, %3\n"
	"	add		%1, %4, %3\n"
	"0:	sync.l\n"
	"	cache	0x0b, 0(%0)\n"
	"	sync.l\n"
	"	addiu	%0, %0, 64\n"
	"	nop\n"
	"	slt		%2, %0, %1\n"
	"	nop\n"
	"	bgtz	%2, 0b\n"
	"	nop\n"
	"	ei\n"
	"	.set reorder\n"
	  : "=&r"(tmp0), "=&r"(tmp1), "=&r"(tmp2)
	  : "r"(inAddr), "r"(inBSize) );
}




//
// EE Clock Counter (periode de 14.56s)
// Reset du COP0[$9] interdit !

inline uint32 GetCycleCpt()
{
	register uint32 cpr0;
	asm volatile ("nop; mfc0 %0, $9; nop": "=r"(cpr0));
	return cpr0;
}

inline uint32 GetTimeCycleCpt( float inTime )
{
	const float freq = 294912000.0F;
	return uint32(freq * inTime);
}

inline float GetCycleCptTime( uint32 inCycCpt )
{
	const float per1 = 1.0F / 294912000.0F;
	const float per2 = 2.0F * float(0x40000000L) / 294912000.0F;
	if( inCycCpt&0x80000000UL ) {
		inCycCpt ^= 0x80000000UL;
		return float(int(inCycCpt)) * per1 + per2;
	} else {
		return float(int(inCycCpt)) * per1;
	}
}

inline void WaitCycleCpt( uint32 inCycCpt )
{
	// active wait without BUS access (DMAC at full speed !)
	register uint32 tmp0, tmp1;
	asm volatile (
	"	.set	noreorder\n"
	"	mfc0	%0, $9\n"
	"0:	mfc0	%1, $9\n"
	"	nop\n"
	"	subu	%1, %1, %0\n"
	"	sltu	%1, %1, %2\n"		// n cycles delay
	"	nop\n"
	"	nop\n"
	"	bgtz	%1, 0b\n"
	"	nop\n"
	"	.set	reorder\n"
	  : "=&r"(tmp0), "=&r"(tmp1)
	  : "r"(inCycCpt)	);
}


//
// EE performance counter
// cf "EE Core User's Manual" COP0 reg. PCCR (reg25)
// 			E0							E1
//	0		(reserved)					Issues the low order branch
//	1		Processor cycle				Processor cycle
//	2		Issues a single instr.		Issues a double instr.
//	3		Issues branch				Branch prediction miss
//	6		I$ miss						D$ miss
//	7		-							WBB single request unusable
//	8		Non-blocking load			WBB burst request unusable
//	9		WBB single request			WBB burst request almost full
//	10		WBB burst request			WBB burst request full
//	11		CPU addr. bus busy			CPU data bus busy
//	12		Completes instr.			Completes instr.
//	13		-							-
//	14		Completes the COP2 instr.	Completes the COP2 instr.
//  15		Completes loads				Completes stores
//	16		No event					No event
//	17-31	(reserved)					(reserved)

inline void StopPerfCounters( )
{
	asm volatile ("mtps $0,0; sync.p");
}

inline void ResetPerfCounters( )
{
	asm volatile ("mtpc $0,0; mtpc $0,1; sync.p");
}

inline void SetPerfCounterEvents( uint inE0, uint inE1 )
{
	uint pccr = 0x80004010 | (inE0<<5) | (inE1<<15);
	asm volatile (
	"	.set noreorder\n"
	"	mtps %0, 0\n"
	"	mtpc $0, 0\n"		// reset
	"	mtpc $0, 1\n"		// reset
	"	sync.p\n"
	"	.set	reorder\n"
	:: "r"(pccr) );
}

inline uint32 GetPerfCounter0( )
{
	register uint32 pcr0;
	asm volatile ("mfpc %0,0": "=r"(pcr0));
	return pcr0;
}

inline uint32 GetPerfCounter1( )
{
	register uint32 pcr1;
	asm volatile ("mfpc %0,1": "=r"(pcr1));
	return pcr1;
}




#endif	// _EE_DEFS_H_



