/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkSurface_H_
#define	_NvkSurface_H_


#include <NvSurface.h>
#include "NvDpyManager[PS2].h"


interface NvkSurface : public NvResource
{
	NVKITF_DECL( Surface )

	enum Buffer {
		BF_BACK			= 0,
		BF_FRONT		= 1
	};

	enum {
		EN_PRESENT		= (1<<0),		// Enable the present process.
		EN_BEGIN_END	= (1<<1),		// Enable the Begin()/End() sequence from the user interface.
		EN_ALL			= 0x3,
		EN_DEFAULT		= EN_ALL
	};

	enum {								// .w component of location as uint32
		ADC_OFF			= 0x49000000,	// Enable kicking
		ADC_ON			= 0x49008001	// Disable kicking
	};

	NvInterface*				GetBase				(													);
	void						AddRef				(													);
	uint						GetRefCpt			(													);
	void						Release				(													);
	uint32						GetRscType			(													);
	uint32						GetRscUID			(													);
	NvSurface*					GetInterface		(													);

	uint						GetComponents		(													);
	bool						HasComponent		(	NvSurface::Component	inComponent				);
	NvSurface::PackMode			GetPackingMode		(													);
	uint						GetMaxSize			(													);
	static Psm					GetNativeColorPSM	(													);

	void						EnableDBF			(	NvSurface::PresentMode	inPresentMode,
														bool					inCreateNow				);
	void						DisableDBF			(	bool					inReleaseNow			);
	bool						IsEnabledDBF		(													);
	bool						HasDBF				(													);

	bool						Resize				(	uint					inMaxSize				);
	void						Enable				(	uint					inEnableFlags			);	// bit-mask of EN_*
	void						Disable				(	uint					inEnableFlags			);	// bit-mask of EN_*
	bool						IsEnabled			(	uint					inEnableFlags			);	// bit-mask of EN_*

	bool						PresentBuffer		(													);
	uint8*						AllocBuffer			(													);
	void						FreeBuffer			(	uint8*					inBuffer				);
	uint						GetBufferBSize		(													);
	uint8*						GetBufferBase		(	Buffer					inBuffer				);

	uint8*						GetComponentBase	(	Buffer					inBuffer,
														NvSurface::Component	inComponent,
														uint*					outStride	= NULL,
														Psm*					outPsm		= NULL		);

	void						UpdateBeforeDraw	(													);
	bool						IsDrawing			(													);
};



#endif // _NvkSurface_H_


