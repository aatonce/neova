/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifdef _EE

#ifndef	_NvkCore_Ctrl_H_
#define	_NvkCore_Ctrl_H_


namespace nv { namespace ctrl
{
	//
	// id table :
	// 0 (ERROR)
	// 2 (NeGi-CON)
	// 3 (GunCON(Konami))
	// 4 (STANDARD)
	// 5 (JOYSTICK),
	// 6 (GunCON(NAMCO))
	// 7 (ANALOG DUALSHOCK/DUALSHOCK 2)
	// 0x100 (TSURI-CON)
	// 0x300 (JOG-CON)

	int		GetCtrlTermId		(	uint 		inCtrlNo	);

	bool	IsCtrlAnalog		(	uint		inCtrlNo	);
	bool	IsCtrlPressMode		(	uint		inCtrlNo	);
	bool	HasCtrlActuators	(	uint		inCtrlNo	);
}}


#endif //_NvkCore_Ctrl_H_

#endif // _EE


