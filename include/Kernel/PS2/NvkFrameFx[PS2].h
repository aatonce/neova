/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkFrameFx_H_
#define	_NvkFrameFx_H_


#include "NvDpyManager[PS2].h"
#include <NvFrameFx.h>


interface NvkFrameFx : public NvDpyObject
{
	NVKITF_DECL( FrameFx )

	enum SystemMap {
		SM_INFRAME	= 0,
		SM_OFFFRAME,
		SM_DEPTHFRAME
	};

	enum Combine {
		CB_BLEND	= (1<<0),		// blending
		CB_VIEWPORT	= (1<<1),		// viewporting
		CB_ATEST	= (1<<2),		// alpha testing (fragment & buffer)
		CB_ZTEST	= (1<<3),		// depth testing
		CB_ZWRITE	= (1<<4),		// depth writing
		CB_MODULATE	= (1<<5),		// mapping modulation
		CB_COLWRAP	= (1<<6),		// color wrapping around
		CB_NEAREST	= (1<<7),		// NEAREST texel sampling
		CB_AEM1		= (1<<8),		// texture.a = (texture.rgb=0) ? 0 : 255
		CB_ALL		= 0xFFFF,
		CB_DEFAULT	= 0
	};

	enum InputColor {
		IC_FRAGMENT = 0,
		IC_BUFFER,
		IC_ZERO
	};

	enum InputAlpha {
		IA_FRAGMENT = 0,
		IA_BUFFER,
		IA_FIX
	};

	enum FragmentATEST {
		FA_ALWAYS	 = 0,
		FA_LESS,
		FA_LEQUAL,
		FA_EQUAL,
		FA_GEQUAL,
		FA_GREATER,
		FA_NOTEQUAL
	};

	enum BufferATEST {
		BA_ALWAYS	 = 0,
		BA_ZERO,
		BA_NONZERO,
	};

	enum DepthTEST {
		DT_ALWAYS	 = 0,
		DT_GEQUAL,
		DT_GREATER,
	};

	struct Kernel33 {					// 3x3 convolution kernel
		union {
			struct {
				float	k00, k01, k02;	// (-1,-1) ( 0,-1) ( 1,-1)
				float	k10, k11, k12;	// (-1, 0) ( 0, 0) ( 1, 0)
				float	k20, k21, k22;	// (-1, 1) ( 0, 1) ( 1, 1)
			};
			float		k[3][3];		// [y][x]
		};
		void	Reset					();
		void	Normalize				();
		void	RotateCW				();
		void	RotateCCW				();
	};


	NvInterface*			GetBase				(								);
	void					AddRef				(								);
	uint					GetRefCpt			(								);
	void					Release				(								);
	NvFrameFx*				GetInterface		(								);


	//
	// Fx managment

	bool					Begin				(												);
	void					End					(												);

	// VRam maps
	int						GetSystemMap		(	SystemMap			inSysMap				);
	int						AllocMap			(	int					inSizeX,
													int					inSizeY					);
	int						GetMapWidth			(	int					inMapId					);
	int						GetMapHeight		(	int					inMapId					);
	void					FreeMap				(	int					inMapId					);

	// Combine states
	void					Enable				(	uint				inCombineFlags			);	// Combine
	void					Disable				(	uint				inCombineFlags			);	// Combine
	void					Setup				(	uint				inCombineFlags			);	// Combine
	void					SetBlend			(	InputColor			inA,						// Blend is (A-B)*C+D
													InputColor			inB,
													InputAlpha			inC,
													InputColor			inD,
													float				inFIX	= 0.0f			);	// in [0,2]
	void					SetViewport			(	const Vec4&			inViewport				);	// ( x, y, w, h ) in pixels
	void					SetDrawViewport		(												);
	void					SetRGBAMask			(	uint32				inRGBA					);	// b[i]=0 -> written
	void					SetRGBAValue		(	uint32				inRGBA					);	// in [0,255]^3
	void					SetAlphaTest		(	FragmentATEST		inFragment,					// in [0,2]
													float				inFragmentRef,
													BufferATEST			inBuffer				);
	void					SetDepthValue		(	float				inZ,
													const Matrix&		inProjTR				);	// Projection matrix
	void					SetDepthTest		(	int					inMapId,
													DepthTEST			inTest					);


	//
	// Fx passes

	bool					ApplyPass			(	int					inFromToMapId			);
	bool					ApplyDepthPass		(	int					inFromToMapId			);

	bool					InvertPass			(	int					inFromToMapId			);
	bool					InvertDepthPass		(	int					inFromToMapId			);

	bool					NormalizePass		(	int					inFromToMapId,
													float				inFactor,					// in [0,2]
													uint32				inAddRGBA		= 0,
													uint32				inSubRGBA		= 0		);
	bool					NormalizeDepthPass	(	int					inFromToMapId,
													float				inFactor,					// in [0,2]
													uint32				inAddRGBA		= 0,
													uint32				inSubRGBA		= 0		);

	bool					BlitPass			(	int					inFromMapId,
													Vec4*				inFromRegion,				// {x,y,w,h}
													int					inToMapId,
													Vec4*				inToRegion,					// {x,y,w,h}
													int					inClutId		= -1	);	// -1 for none

	bool					TranslatePass		(	int					inFromMapId,
													int					inToMapId,
													int					inToOffsetX		= 0,
													int					inToOffsetY		= 0		);

	bool					CopyPass			(	int					inFromMapId,
													int					inToMapId				);

	bool					ResizePass			(	int					inFromMapId,
													int					inToMapId				);

	int						Downx2Pass			(	int					inFromMapId				);
	int						Upx2Pass			(	int					inFromMapId				);

	bool					PreparePass			(	int					inFromMapId,
													int					inToMapId,
													float				inFactor		= 0.5f	);
	bool					UnpreparePass		(	int					inFromMapId,
													int					inToMapId,
													float				inFactor		= 0.5f	);	// Same factor used for PreparePass() !

	bool					ConvolvePass		(	int					inFromMapId,
													int					inToMapId,
													const Kernel33&		inKernel				);
	bool					SepConvolvePass		(	int					inFromToMapId,
													const Kernel33&		inKernel				);
	bool					DiffConvolvePass	(	int					inFromMapId,
													int					inToMapId,
													const Kernel33&		inKernel				);

	bool					GaussianBlurPass	(	int					inFromToMapId,
													float				inRadius		= 2.0f,
													float				inQuality		= 0		);	// in [0,1]
 	bool					GlowBlurPass		(	int					inFromToMapId,
													const Kernel33&		inKernel,
													float				inRadius		= 2.0f,
													float				inQuality		= 0		);	// in [0,1]
	bool					FocalBlurPass		(	int					inFromToMapId,
													const Kernel33&		inKernel,
													float				inRadius		= 2.0f,
													float				inQuality		= 0		);	// in [0,1]

	bool					DepthToAlphaPass	(	int					inFromMapId,
													int					inToMapId				);

	bool					RGBAToDepthPass		(	int					inFromMapId,
													int					inToMapId				);

	bool					TrxPass				(	int					inToMapId,
													uint128*			inData					);	// uint32 [W*H], 16 bytes aligned


	//
	// Clut

	bool					AllocClut			(	int					inNbClut				);
	bool					SetClutColor		(	int					inClutId,
													uint8				inIndex,					// in [0,255]
													uint32				inRGBA					);	// in [0,255]^4

	//
	// Testing

	bool					GenerateHStripes	(	int					inToMapId,
													int					inStep			= 4		);

	bool					GenerateVStripes	(	int					inToMapId,
													int					inStep			= 4		);
};



#endif // _NvkFrameFx_H_



