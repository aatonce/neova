/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef _VU1_DEFS_I_H_
#define	_VU1_DEFS_I_H_



// XYZ pack/unpack

inline
uint32
vu1::GetXYZUnpackI	(	int		inPackIPartLen	)
{
	if( inPackIPartLen < 0 )	return 0U;
	return (uint32(inPackIPartLen+135)<<23) | 0x8000U;
}


inline
float
vu1::GetXYZUnpackF	(	int		inPackIPartLen	)
{
//	float  packRange	= float( 1<<(inPackIPartLen) );
//	float  packRow	  	= float( 1<<(23-(15-(inPackIPartLen))) );
//	return (packRow + packRange);
	union {	uint32 iv; float  fv; };
	iv = GetXYZUnpackI( inPackIPartLen );
	return fv;
}


inline
int
vu1::GetXYZPackIPartLen	(	uint32	inXYZUnpack		)
{
	if( inXYZUnpack == 0 )		return -1;
	NV_ASSERT( (inXYZUnpack>>23) >= 135 );
	return (inXYZUnpack>>23)-135U;
}


inline
int
vu1::GetXYZPackIPartLen	(	float	inXYZUnpack		)
{
	union {	uint32 iv; float  fv; };
	fv = inXYZUnpack;
	return GetXYZPackIPartLen( iv );
}



#endif // _VU1_DEFS_I_H_

