/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



/*

	Do not change anything here.
	Simply include this file after the CLIP_PROCESS( my_xgkickPtr ) call.

*/



#ifndef VU1_CLIP4D_LOOP_H
#define VU1_CLIP4D_LOOP_H




//
// restore the two last clipped vertices ...
// clip_state : < -, PRIM-strip, buffer0, buffer1 >

#define		CLIP_LOAD_CONTEXT()											\
				mfir.x			clip_state, vi00;						\
				lq.yzw			clip_state, CLIP_BUFFER0+0(vi00);		\
				lq				loc_0, CLIP_BUFFER0+1(vi00);			\
				lq				stq_0, CLIP_BUFFER0+2(vi00);			\
				lq				col_0, CLIP_BUFFER0+3(vi00);			\
				lq				loc_1, CLIP_BUFFER0+4(vi00);			\
				lq				stq_1, CLIP_BUFFER0+5(vi00);			\
				lq				col_1, CLIP_BUFFER0+6(vi00);


//
// save the two last clipped vertices ...

#define		CLIP_SAVE_CONTEXT()											\
				iaddiu			clip_cnt, vi00, 1;						\
				isw.x			clip_cnt,   CLIP_BUFFER0+0(vi00);		\
				sq.yzw			clip_state, CLIP_BUFFER0+0(vi00);		\
				sq				loc_0, CLIP_BUFFER0+1(vi00);			\
				sq				stq_0, CLIP_BUFFER0+2(vi00);			\
				sq				col_0, CLIP_BUFFER0+3(vi00);			\
				sq				loc_1, CLIP_BUFFER0+4(vi00);			\
				sq				stq_1, CLIP_BUFFER0+5(vi00);			\
				sq				col_1, CLIP_BUFFER0+6(vi00);


//
// Build a FAN giftag { REGS=0x412, NREG=3, PRIM=fan, PRE=1 }
// Using STRIP giftag .y field of clip_state
// addi.y 0x2C000000 => set the fan bit 15 to 1.
// outputing the PRIM register resets the strip drawing history !

#define		CLIP_BUILD_FAN_GIFTAG( outGIFTAG, inNLOOP )					\
				mfir.x			outGIFTAG, inNLOOP;						\
				loi				0x2C000000;								\
				addi.y			outGIFTAG,	clip_state, i;				\
				loi				0x00000412;								\
				maxi.zw			outGIFTAG,	vf00, i;


//
// Build a STRIP giftag { REGS=0x412, NREG=3, PRIM=strip, PRE=1 }
// Using STRIP giftag .y field of clip_state
// outputing the PRIM register resets the strip drawing history !

#define		CLIP_BUILD_STRIP_GIFTAG( outGIFTAG, inNLOOP )				\
				mfir.x			outGIFTAG, inNLOOP;						\
				move.y			outGIFTAG, clip_state;					\
				loi				0x00000412;								\
				maxi.zw			outGIFTAG,	vf00, i;





_Clip_BEGIN:
		// load the clipping context
		CLIP_LOAD_CONTEXT()

		// clipping is needed ?
		ibne			needToClip, vi00, _Clip_ctxt
		// no need to clip ... keep the context safe with the last two inputed vertices !
		// note that if nloop < 2, then it is the last part of the surface and this invalid clip context is no used later ...
		isubiu			nb, nloop, 2
		CLIP_FORWARD_VERTICE( nb )
		CLIP_NEXT_VERTICE( loc_0, col_0, stq_0, vi00 )
		CLIP_NEXT_VERTICE( loc_1, col_1, stq_1, vi00 )
		b				_Clip_exit

		// context reseted ?
_Clip_ctxt:
		ilw.x			clip_cnt, CLIP_BUFFER0+0(vi00)
		ibne			clip_cnt, vi00, _Clip_enter
		// build context with the first two inputed vertices ...
		CLIP_NEXT_VERTICE( loc_0, col_0, stq_0, vi00 )
		CLIP_NEXT_VERTICE( loc_1, col_1, stq_1, vi00 )
		iaddiu			outPtr, outPtr, 6
_Clip_enter:

		// --
		// -- start of clipping loop
		// --

_Clip_loop:
		CLIP_NEXT_VERTICE( loc_2, col_2, stq_2, adc_s )
		ilw.w			adc_k,	2(outPtr)
		iaddiu			outPtr, outPtr, 3
		ibgez			adc_k,	_Clip_skip						// skip if total inside (output ADC=0, i.e. >= 0)
		ibltz			adc_s,	_Clip_skip						// skip if strip-swapped (strip ADC=1, i.e. <  0)

		// skip if total out on +X or -X or ...
		iaddiu			cfMask,   vi00, 0x3F
		iaddiu			andClipF, vi00, 0x3F
		clipw			loc_0, loc_0;	fcget cf;	iand andClipF, andClipF, cf
		clipw			loc_1, loc_1;	fcget cf;	iand andClipF, andClipF, cf
		clipw			loc_2, loc_2;	fcget cf;	iand andClipF, andClipF, cf
		iand			andClipF, andClipF, cfMask
		ibgtz			andClipF, _Clip_skip

#ifdef	CLIP_DEBUG_RGB
		//	DEBUG input vertices are R G B colored
		loi				128
		maxi			col_0,  vf00, i
		maxi			col_1,  vf00, i
		maxi			col_2,  vf00, i
		mulx.yz			col_0, vf00, vf00
		mulx.xz			col_1, vf00, vf00
		mulx.xy			col_2, vf00, vf00
#endif

		// setup clip field mask
		mr32			clip_field0, vf00						// { 0, 0,+1, 0 }
		mr32			clip_field1, vf00
		subw.z			clip_field1, vf00, vf00					// { 0, 0,-1, 0 }

		// save triangle vertices in clipping data area
		mtir			clip_buff, clip_state[z]
		iaddiu			clip_in,  clip_buff, 1
		iaddiu			clip_end, clip_buff, 10
		sq				stq_0, 0(clip_in)
		sq				col_0, 1(clip_in)
		sq				loc_0, 2(clip_in)
		sq				stq_1, 3(clip_in)
		sq				col_1, 4(clip_in)
		sq				loc_1, 5(clip_in)
		sq				stq_2, 6(clip_in)
		sq				col_2, 7(clip_in)
		sq				loc_2, 8(clip_in)

_Clip_pass0:

		// compute wec for all input vertices
		iaddiu			Sw, vi00, (1<<4)
_Clip_wec_loop:
//		--LoopCS 1, 0 : Do not uncomment this ! VCL fails with fmand unrolling ...
		lq				t_loc, 2(clip_in)
		mul.xyz			t_loc, t_loc, clip_field0				// < 0 or +x or -x, 0 or +y or -y, 0 or +z or -z, w >
		addax			acc,   t_loc, t_loc
		madday			acc,   vf00,  t_loc
		maddz			t_wec, vf00,  t_loc						// t_wec = w+x / w-x / w+y / w-y / w+z / w-z
		fmand			s_wec, Sw								// sign of t_wec
		isw.z			s_wec, 0(clip_in)
		sq.w			t_wec, 0(clip_in)
		iaddiu			clip_in, clip_in, 3
		ibne			clip_in, clip_end, _Clip_wec_loop

		iaddiu			clip_in,  clip_buff, 1
		iaddiu			clip_out, clip_buff, 1
		iaddiu			andClipF, vi00, 0x3F					// init andClip & orClip masks
		iaddiu			orClipF,  vi00, 0

		ilw.z			t_swec0, 0(clip_in)						// sign of wec0
		lq				t_stq0,  0(clip_in)
		lq				t_col0,  1(clip_in)
		lq				t_loc0,  2(clip_in)
		sq				t_stq0,  0(clip_end)
		sq				t_col0,  1(clip_end)
		sq				t_loc0,  2(clip_end)

_Clip_pass1:
		ilw.z			t_swec1, 3(clip_in)						// sign of wec1
		lq				t_stq1,  3(clip_in)
		lq				t_col1,  4(clip_in)
		lq				t_loc1,  5(clip_in)

		// output if v0 is valid, i.e. wec0 > 0
		ibgtz			t_swec0, _Clip_pass2
		sq				t_stq0, 0(clip_out)
		sq				t_col0, 1(clip_out)
		sq				t_loc0, 2(clip_out)
		iaddiu			clip_out, clip_out, 3
		clipw			t_loc0, t_loc0
		fcget			cf
		iand			andClipF, andClipF, cf
		ior				orClipF,  orClipF,  cf
_Clip_pass2:

		// intersect if sign(wec0) != sign(wec1)
		ibeq			t_swec0, t_swec1, _Clip_pass3
		sub.w			dwec, t_stq0, t_stq1					// wec0 - wec1
		div				q, t_stq0[w], dwec[w]					// q in [0,1]
		sub				t_dloc, t_loc1, t_loc0
		sub				t_dcol, t_col1, t_col0
		sub				t_dstq, t_stq1, t_stq0
		mulaw			acc, t_loc0, vf00;	madd	t_loc, t_dloc, q
		mulaw			acc, t_col0, vf00;	madd	t_col, t_dcol, q
		mulaw			acc, t_stq0, vf00;	madd	t_stq, t_dstq, q
		sq				t_stq, 0(clip_out)
		sq				t_col, 1(clip_out)
		sq				t_loc, 2(clip_out)
		iaddiu			clip_out, clip_out, 3
		clipw			t_loc, t_loc
		fcget			cf
		iand			andClipF, andClipF, cf
		ior				orClipF,  orClipF,  cf
_Clip_pass3:

		move			t_stq0,	t_stq1
		move			t_col0,	t_col1
		move			t_loc0,	t_loc1
		iadd			t_swec0, t_swec1, vi00

		iaddiu			clip_in, clip_in, 3
		ibne			clip_in, clip_end, _Clip_pass1
		iaddiu			clip_end, clip_out,  0
		iaddiu			clip_in,  clip_buff, 1
		ibeq			clip_in, clip_end, _Clip_skip				// no more vertices ?

		// checks resulting clipping flags
		iaddiu			cf, vi00, 0x3F
		iand			andClipF, andClipF, cf
		iand			orClipF,  orClipF,  cf
		ibgtz			andClipF, _Clip_skip						// all outside ?
		ibeq			orClipF, vi00, _Clip_proj					// all inside ?

		// loop on the next plane ...
		addax			acc, clip_field1, vf00						// acc = clip_field1 & set Zw
		iaddiu			Zw, vi00, (1<<0)
		fmand			Zw, Zw
		mr32			clip_field1, clip_field0
		maddx			clip_field0, vf00, vf00						// clip_field0 = acc
		ibgtz			Zw, _Clip_pass0								// while clip_field.w == 0 !

_Clip_proj:
		iaddiu			nloop, vi00, 0x4000
		iadd			nloop, nloop, nloop
_Clip_proj_loop:
		--LoopCS 3, 3
		lq				t_stq, 0(clip_in)
		lq				t_col, 1(clip_in)
		lq				t_loc, 2(clip_in)
		ftoi0			t_col, t_col
		div				q, vf00[w], t_loc[w]
		mul				t_stq, t_stq, q
		add.z			t_stq, vf00,  q
		mul				t_loc, t_loc, q
		mula			acc, TOPROJS, t_loc
		maddw			t_loc, TOPROJT, vf00
		sq				t_stq, 0(clip_in)
		sq				t_col, 1(clip_in)
		sq				t_loc, 2(clip_in)
		iaddiu			clip_in, clip_in, 3
		iaddiu			nloop, nloop, 1
		ibne			clip_in, clip_end, _Clip_proj_loop

		// clipped fan output
		CLIP_BUILD_FAN_GIFTAG( GIFTAG_fan, nloop )
		sq				GIFTAG_fan,	0(clip_buff)
		xgkick			clip_buff

		// swap clipping buffers ([z] <=> [w])
		mr32.z			clip_state, clip_state
		mfir.w			clip_state, clip_buff
		iaddiu			clip_out_one, vi00, 1
		mfir.x			clip_state, clip_out_one					// set clip-out-one [x] != 0

_Clip_skip:
		move			loc_0, loc_1
		move			stq_0, stq_1
		move			col_0, col_1
		move			loc_1, loc_2
		move			stq_1, stq_2
		move			col_1, col_2
		ibne			outPtr, outPtrEnd, _Clip_loop

		// --
		// -- end of clipping loop
		// --

		// Restore clip flags state
		clipw			loc_0, loc_0
		clipw			loc_1, loc_1

		// Restore GS-strip-primitive history if at least one clipped-output is done (write to the PRIM reg).
		// The two last vertices are outputed from the culled output buffer !
		// If at least one clipped triangle have been outputed, at least one fan-xgkick has been done
		// and it's now safe to write in the culled output buffer.
		mtir			clip_out_one, clip_state[x]
		ibeq			clip_out_one, vi00, _Clip_exit				// isset clip-out-one [x] ?
		isubiu			v_output, outPtrEnd, 7
		iaddiu			nloop, vi00, 0x4001
		iadd			nloop, nloop, nloop
		CLIP_BUILD_STRIP_GIFTAG( GIFTAG_strip, nloop )
		sq				GIFTAG_strip, 0(v_output)
		xgkick			v_output									// kick the two last vertices

_Clip_exit:
		// If at least one clipped triangle have been outputed, a previous xgkick has been done
		// to restore the GS-strip-primitive history and it's now safe to write to context to the
		// clipping output buffer.
		CLIP_SAVE_CONTEXT()
_Clip_END:




#endif // VU1_CLIP4D_LOOP_H



