/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _VU1_MAPPING_H_
#define	_VU1_MAPPING_H_


//
// Fixed common mapping

// D$ area (last addr must be < 256 for 8bits index !)
#define	VU1_DCACHE_QA				(0)
#define	VU1_DCACHE_ENTRY_QS			(2)
#define	VU1_DCACHE_ENTRY_NB			(64)
#define	VU1_DCACHE_QS				(VU1_DCACHE_ENTRY_NB*VU1_DCACHE_ENTRY_QS)

// Clip working buffers
#define	VU1_CLIP_CTE_QS				(2)		// clipping constants
#define	VU1_CLIP_REG_QS				(4)		// clipping status context
#define	VU1_CLIP_BUFF_QS			(32)	// {GIFTAG + up to 9 outputed vertices}
#define	VU1_CLIP_FULL_QS			(VU1_CLIP_CTE_QS+VU1_CLIP_REG_QS+(VU1_CLIP_BUFF_QS*2))

// Context area (up to 32QW)
#define	VU1_CONTEXT_QS				(32)

// Direct area : (A+D)GIFTAG + 15 GS REGS
#define	VU1_DIRECT_QS				(16)


//
// Some constants

#define VU1_DMA_STRIDE				(4)			// DMA_TAG_REF'able stride for Vec[2|3|4] and uint32 !
#define	VU1_UNPACK_ST				(8224.0)
#define	VU1_UNPACK_NXYZ				(65537.0)



//
// Some predefined full mapping

// With D$ area basic mapping (used by tolit mesh precompiler)
#define VU1_WITH_DCACHE_DCACHE_QA	(VU1_DCACHE_QA)
#define VU1_WITH_DCACHE_CLIP_QA		(VU1_WITH_DCACHE_DCACHE_QA+VU1_DCACHE_QS)
#define	VU1_WITH_DCACHE_CTXT_QA		(VU1_WITH_DCACHE_CLIP_QA+VU1_CLIP_FULL_QS)
#define	VU1_WITH_DCACHE_DIRECT_QA	(VU1_WITH_DCACHE_CTXT_QA+VU1_CONTEXT_QS)
#define VU1_WITH_DCACHE_BASE_QA		(VU1_WITH_DCACHE_DIRECT_QA+VU1_DIRECT_QS)
#define	VU1_WITH_DCACHE_REMAIN_QS	(1024-VU1_WITH_DCACHE_BASE_QA)
#define VU1_WITH_DCACHE_HB_QS		(VU1_WITH_DCACHE_REMAIN_QS/2)
#define VU1_WITH_DCACHE_OFFSET_QS	(VU1_WITH_DCACHE_HB_QS)
#define VU1_WITH_DCACHE_UNPACK_QS	((VU1_WITH_DCACHE_HB_QS/2)-1-1-2)		// HEADER(1QW) & multiple of 2 loopCS-overflow (dummy toCache(1QW)+fromCache(2QW))
#define	VU1_WITH_DCACHE_OUPUT_QS	((VU1_WITH_DCACHE_HB_QS/2)-1-3)			// GIFTAG & multiple of 2 loopCS-overflow (dummy one GS output (3QW))

// Without D$ area basic mapping (used by prelit mesh precompiler)
#define VU1_NO_DCACHE_CLIP_QA		(0)
#define	VU1_NO_DCACHE_CTXT_QA		(VU1_NO_DCACHE_CLIP_QA+VU1_CLIP_FULL_QS)
#define	VU1_NO_DCACHE_DIRECT_QA		(VU1_NO_DCACHE_CTXT_QA+VU1_CONTEXT_QS)
#define	VU1_NO_DCACHE_BASE_QA		(VU1_NO_DCACHE_DIRECT_QA+VU1_DIRECT_QS)
#define	VU1_NO_DCACHE_REMAIN_QS		(1024-VU1_NO_DCACHE_BASE_QA)
#define	VU1_NO_DCACHE_HB_QS			(VU1_NO_DCACHE_REMAIN_QS/2)
#define	VU1_NO_DCACHE_OFFSET_QS		(VU1_NO_DCACHE_HB_QS)
#define	VU1_NO_DCACHE_UNPACK_QS		((VU1_NO_DCACHE_HB_QS/2)-1-3)			// HEADER(1QW) & multiple de 2 loopCS 1,0 overflow (dummy one GS input (3QW))
#define	VU1_NO_DCACHE_OUPUT_QS		((VU1_NO_DCACHE_HB_QS/2)-1-3)			// GIFTAG(1QW) & multiple de 2 loopCS 1,0 overflow (dummy one GS output (3QW))



//
// Customisable mapping for VU micro-codes

#if defined(_VUCPP) && !defined(_NO_MAPPING_CTE)

#ifdef _USING_DCACHE
	#define	DCACHE				(VU1_WITH_DCACHE_DCACHE_QA)
	#define CLIP_CTE			(VU1_WITH_DCACHE_CLIP_QA)
	#define	CTXT				(VU1_WITH_DCACHE_CTXT_QA)
	#define	DIRECT				(VU1_WITH_DCACHE_DIRECT_QA)
	#define	BASE				(VU1_WITH_DCACHE_BASE_QA)
	#define	OFFSET_QS			(VU1_WITH_DCACHE_OFFSET_QS)
#else
	#define	DCACHE				(VU1_NO_DCACHE_DCACHE_QA)
	#define CLIP_CTE			(VU1_NO_DCACHE_CLIP_QA)
	#define	CTXT				(VU1_NO_DCACHE_CTXT_QA)
	#define	DIRECT				(VU1_NO_DCACHE_DIRECT_QA)
	#define	BASE				(VU1_NO_DCACHE_BASE_QA)
	#define	OFFSET_QS			(VU1_NO_DCACHE_OFFSET_QS)
#endif

#define	CLIP_REG				(CLIP_CTE+VU1_CLIP_CTE_QS)
#define	CLIP_BUFFER0			(CLIP_REG+VU1_CLIP_REG_QS)
#define	CLIP_BUFFER1			(CLIP_BUFFER0+VU1_CLIP_BUFF_QS)
//#define CLIP_DEBUG_RGB

#define	CTXT_TOPROJ_S			(CTXT+0)
#define	CTXT_TOPROJ_T			(CTXT+1)
#define	CTXT_TOPROJ_M			(CTXT+2)
#define	CTXT_TOVIEW_M			(CTXT+6)
#define	CTXT_PROJ_M				(CTXT+10)
#define	CTXT_PARAM				(CTXT+14)
#define	CTXT_VP					(CTXT+15)

#ifdef _USING_LIGHT
	#define	CTXT_LIGHT_M		(CTXT+17)
	#define	CTXT_LIGHT_C		(CTXT+20)
	#define	CTXT_LAST			(CTXT+17+6)
#else
	#define	CTXT_LAST			(CTXT+17)
#endif

#define	OUTPUT_OFFSET			(OFFSET_QS/2)

#endif


#endif	// _VU1_MAPPING_H_



