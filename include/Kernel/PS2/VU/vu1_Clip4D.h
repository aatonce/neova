/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



/*

	You must define the following symbols :

	- CLIP_REG
		Q-addr of a 4 QW backup area (see CLIP_BACKUP_4 & CLIP_RESTORE_4)

	- CLIP_BUFFER0
		The first clipping buffer of at least 32 QW

	- CLIP_BUFFER1
		The second clipping buffer of at least 32 QW

	- CLIP_NEXT_VERTICE( outLoc, outCol, outTex, outStripADC )
		The vcl code which load the next vertice.
		An example is :
			#define		CLIP_NEXT_VERTICE( outLoc, outCol, outTex, outStripADC )	\
							lq				fcache,	0(unpackPtr);					\
							mtir			procQA,	fcache[z];						\
							lq				outLoc,	0(procQA);						\
							lq				outCol,	1(procQA);						\
							itof0			outCol,	outCol;							\
							subx			outTex,	fcache,	CTE;					\
							add.xy			outTex,	outTex,	STOFFSET;				\
							mr32.z			outTex,	vf00;							\
							ilw.w			outStripADC, 0(unpackPtr);				\
							iaddiu			unpackPtr, unpackPtr, 1;

	- CLIP_FORWARD_VERTICE( inNb )
		The vcl code which jump over inNb vertice(s).
		An example is, with 1Q per vertice :
			#define		CLIP_FORWARD_VERTICE( inNb )								\
							iadd			unpackPtr, unpackPtr, inNb;

	Optionnaly, you can define the CLIP_DEBUG_RGB debug symbols to overwrite
	the vertex colors with Red, Green and Blue colors.

	Finally, call CLIP_PROCESS( xgkickPtr, needToClip? ) to process the 4D clipping on your xgkicked buffer.

*/



#ifndef VU1_CLIP4D_H
#define VU1_CLIP4D_H


#include "vu1_Tools.h"




//
// Reset the clipping context
// Use it when you start a new strip.

#define		CLIP_RESET()												\
				iaddiu			clip_buff0, vi00, CLIP_BUFFER0;			\
				iaddiu			clip_buff1, vi00, CLIP_BUFFER1;			\
				isw.x			vi00,		CLIP_BUFFER0+0(vi00);		\
				isw.z			clip_buff0, CLIP_BUFFER0+0(vi00);		\
				isw.w			clip_buff1, CLIP_BUFFER0+0(vi00);		\
				RESET_CLIP_HISTORY();


//
// Set the PRIM used for clipping outputs

// asserts the .y field is { NREG=3, PRIM=strip, PRE=1 }
#define		CLIP_SET_PRIM_NREG3( inGIFTAG )								\
				sq.y			inGIFTAG, CLIP_BUFFER0+0(vi00);			// only the PRIM field

// asserts the .y field is { NREG=1, PRIM=strip, PRE=1 }
// change NREG 1->3
#define		CLIP_SET_PRIM_NREG1( inGIFTAG )								\
				loi				0x5F800000;								\
				muli.y			inGIFTAG, inGIFTAG, i;					\
				CLIP_SET_PRIM_NREG3( inGIFTAG );



//
// Init the clipping process

#define		CLIP_BACKUP_4( inVar0, inVar1, inVar2, inVar3 )				\
				sq				inVar0,		CLIP_REG+0(vi00);			\
				sq				inVar1,		CLIP_REG+1(vi00);			\
				sq				inVar2,		CLIP_REG+2(vi00);			\
				sq				inVar3,		CLIP_REG+3(vi00);

#define		CLIP_RESTORE_4( inVar0, inVar1, inVar2, inVar3 )			\
				lq				inVar0,		CLIP_REG+0(vi00);			\
				lq				inVar1,		CLIP_REG+1(vi00);			\
				lq				inVar2,		CLIP_REG+2(vi00);			\
				lq				inVar3,		CLIP_REG+3(vi00);

#define		CLIP_PROCESS( inXGKICKPtr, inNeedToClip )					\
				iadd			needToClip,	vi00, inNeedToClip;			\
				ilw.x			nloop,		0(inXGKICKPtr);				\
				iaddiu			tmp,		vi00,  0x7FFF;				\
				iand			nloop,		nloop, tmp;					\
				iaddiu			outPtr,		inXGKICKPtr,	1;			\
				iadd			outPtrEnd,	outPtr,		nloop;			\
				iadd			outPtrEnd,	outPtrEnd,	nloop;			\
				iadd			outPtrEnd,	outPtrEnd,	nloop;



#define		CLIP_OUTPUT_PTR		outPtr
#define		CLIP_OUTPUT_ENDPTR	outPtrEnd



#endif // VU1_CLIP4D_H



