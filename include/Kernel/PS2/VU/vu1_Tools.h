/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/




#ifndef VU1_TOOLS_H
#define VU1_TOOLS_H



//
// Build {1,1,1,0} registers

#define	BUILD_ONES()											\
			add.xyz			ONE_F, vf00, vf00[w];				\
			mr32.w			ONE_F, vf00;						\
			iaddiu			ONE_I, vi00, 1;



//
// Build ADC contants for lq fast culling.
// CTEs are 0 & 0xFFFFFFFF

#define	BUILD_ADC_CONSTANTS( inMemOffset )						\
			mfir			adc_pass,	vi00;					\
			sq				adc_pass,	(inMemOffset)+0(vi00);	\
			isubiu			vi01,		vi00,	1;				\
			mfir			adc_failed,	vi01;					\
			sq				adc_failed,	(inMemOffset)+1(vi00);


//
// Reset the clip history
// The last PATH1 direct must write the PRIM reg to reset the primitive !!!

#define	RESET_CLIP_HISTORY()									\
			fcset			0;


//
// Build a continue-GIFTAG { REGS=0x412, NREG=3, PRE=0 }, without PRIM

#define	BUILD_CNT_GIFTAG( outGIFTAG, inNLOOP )					\
			mfir.x			outGIFTAG,	inNLOOP;				\
			loi				0x30000000;							\
			maxi.y			outGIFTAG,	vf00, i;				\
			loi				0x00000412;							\
			maxi.zw			outGIFTAG,	vf00, i;

#define	XGKICK_CNT( inVIAddr, inNLOOP )							\
			BUILD_CNT_GIFTAG( tmp_GIFTAG, inNLOOP );			\
			sq				tmp_GIFTAG,	0(inVIAddr);			\
			xgkick			inVIAddr;



//
// jump if bit is not set to 1

#define	jump_if_unset( inVI, inBitNo, inLabel )					\
			iaddiu			tmp, vi00,	(1<<inBitNo);			\
			iand			tmp, tmp,	inVI;					\
			ibeq			tmp, vi00, inLabel;



//
// jump if bit is set to 1

#define	jump_if_set( inVI, inBitNo, inLabel )					\
			iaddiu			tmp, vi00,	(1<<inBitNo);			\
			iand			tmp, tmp,	inVI;					\
			ibne			tmp, vi00, inLabel;


//
// jump if ==

#define	jump_if_equ( inVI, inImm, inLabel )						\
			iaddiu			tmp, vi00,	inImm;					\
			ibeq			tmp, inVI, inLabel;



//
// jump if !=

#define	jump_if_neq( inVI, inImm, inLabel )						\
			iaddiu			tmp, vi00,	inImm;					\
			ibne			tmp, inVI, inLabel;



//
// loi if ==

#define	loi_if_equ( inVI, inImm, inI_if_true, inI_if_false, inLabel )	\
			loi				inI_if_true;								\
			iaddiu			tmp, vi00,	inImm;							\
			ibeq			tmp, inVI, inLabel;							\
			loi				inI_if_false;								\
		inLabel:



//
// loi if !=

#define	loi_if_neq( inVI, inImm, inI_if_true, inI_if_false, inLabel )	\
			loi				inI_if_true;								\
			iaddiu			tmp, vi00,	inImm;							\
			ibne			tmp, inVI, inLabel;							\
			loi				inI_if_false;								\
		inLabel:



//
// Quat -> matrix 3x3

#define	quat_to_matrix( outM, inQ )								\
		mula.xyz		acc, inQ, inQ;							\
		loi				1.414213562;							\
		muli			vclsmlftemp, inQ, I;					\
		madd.xyz		vcl_2qq, inQ, inQ;						\
		addw.xyz		Vector111, vf00, vf00;					\
		opmula.xyz		ACC, vclsmlftemp, vclsmlftemp;			\
		msubw.xyz		vclsmlftemp2, vclsmlftemp, vclsmlftemp;	\
		maddw.xyz		vclsmlftemp3, vclsmlftemp, vclsmlftemp;	\
		addaw.xyz		acc, vf00, vf00;						\
		msubax.yz		acc, Vector111, vcl_2qq;				\
		msuby.z			outM##2, Vector111, vcl_2qq;			\
		msubay.x		acc, Vector111, vcl_2qq;				\
		msubz.y			outM##1, Vector111, vcl_2qq;			\
		mr32.y			outM##0, vclsmlftemp2;					\
		msubz.x			outM##0, Vector111, vcl_2qq;			\
		mr32.x			outM##2, vclsmlftemp2;					\
		addy.z			outM##0, vf00, vclsmlftemp3;			\
		mr32.w			vclsmlftemp, vclsmlftemp2;				\
		mr32.z			outM##1, vclsmlftemp;					\
		addx.y			outM##2, vf00, vclsmlftemp3;			\
		mr32.y			vclsmlftemp3, vclsmlftemp3;				\
		mr32.x			outM##1, vclsmlftemp3;


//
// Cross product V0 x V1

#define	cross_product( outV, inV0, inV1 )						\
		opmula.xyz		ACC, inV0, inV1;						\
		opmsub.xyz		outV, inV1, inV0;


//
// XYZ normalize

#define	normalize_xyz( outV, inV )								\
		erleng			p, inV;									\
		mfp.xyz			tmp_rnorm, p;							\
		mul.xyz			outV, inV, tmp_rnorm;


//
// unit_x/y/z

#define	unit_x( outV )											\
		addw.x			outV, vf00, vf00;						\
		move.yzw		outV, vf00;
#define	unit_y( outV )											\
		addw.y			outV, vf00, vf00;						\
		move.xzw		outV, vf00;
#define	unit_z( outV )											\
		addw.z			outV, vf00, vf00;						\
		move.xyw		outV, vf00;


//
// Matrix 4x4 mul M = M0 x M1

#define	matrix_mul_44( outM, inM0, inM1 )						\
		mulax			acc,		inM1##0, inM0##0;			\
		madday			acc,		inM1##1, inM0##0;			\
		maddaz			acc,		inM1##2, inM0##0;			\
		maddw			outM##0,	inM1##3, inM0##0;			\
		mulax			acc,		inM1##0, inM0##1;			\
		madday			acc,		inM1##1, inM0##1;			\
		maddaz			acc,		inM1##2, inM0##1;			\
		maddw			outM##1,	inM1##3, inM0##1;			\
		mulax			acc,		inM1##0, inM0##2;			\
		madday			acc,		inM1##1, inM0##2;			\
		maddaz			acc,		inM1##2, inM0##2;			\
		maddw			outM##2,	inM1##3, inM0##2;			\
		mulax			acc,		inM1##0, inM0##3;			\
		madday			acc,		inM1##1, inM0##3;			\
		maddaz			acc,		inM1##2, inM0##3;			\
		maddw			outM##3,	inM1##3, inM0##3;


//
// Matrix 3x3 mul M = M0 x M1

#define	matrix_mul_33( outM, inM0, inM1 )						\
		mulax.xyz		acc,		inM1##0, inM0##0;			\
		madday.xyz		acc,		inM1##1, inM0##0;			\
		maddz.xyz		outM##0,	inM1##2, inM0##0;			\
		mulax.xyz		acc,		inM1##0, inM0##1;			\
		madday.xyz		acc,		inM1##1, inM0##1;			\
		maddz.xyz		outM##1,	inM1##2, inM0##1;			\
		mulax.xyz		acc,		inM1##0, inM0##2;			\
		madday.xyz		acc,		inM1##1, inM0##2;			\
		maddz.xyz		outM##2,	inM1##2, inM0##2;


#endif // VU1_TOOLS_H




