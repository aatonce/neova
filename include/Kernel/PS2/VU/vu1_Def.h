/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _VU1_DEFS_H_
#define	_VU1_DEFS_H_


#include "vu1_Mapping.h"
interface NvkSurface;



namespace vu1
{

	//
	// Clipping mode

	enum ClipMode {
		CM_NONE			= 0,
		CM_CULLING		= 1,
		CM_CLIPPING		= 2
	};


	//
	// XYZ pack/unpack

	uint32				GetXYZUnpackI			(	int		inPackIPartLen	);
	float				GetXYZUnpackF			(	int		inPackIPartLen	);
	int					GetXYZPackIPartLen		(	uint32	inXYZUnpack		);
	int					GetXYZPackIPartLen		(	float	inXYZUnpack		);
	int					FindXYZPackIPartLen		(	float	inRadius		);


	//
	// Basic contexts

	struct ViewContext {
		Vec4			toprojScale;		//   1  QW : <sx,sy,sz,0>
		Vec4			toprojTrans;		//   1  QW : <tx,ty,tz,0>
		Matrix			toprojMatrix;		//   4  QW : worldTR * viewTR * projTR
		Matrix			toviewMatrix;		//   4  QW : worldTR * viewTR
		Matrix			projMatrix;			//   4  QW :                    projTR
		uint32			pad1[3];			//   ^
		float			xyzUnpack;			//   1  QW	: <-, -, -, xyzUnpack>
		uint64			vpFrameReg;			//   1  DW : viewport sceGsFrame
		uint64			vpXYOffsetReg;		//   1  DW : viewport sceGsXyoffset
		uint64			vpScissorReg;		//   1  DW : viewport sceGsScissor
		uint64			vpZBufReg;			//   1  DW : viewport sceGsZbuf
	};										// = 17 QW

	struct ToLitFilters {
		Vec4			colors[3];			//   3  QW : AmbientColor, DL1Color, DL2Color
	};										// = 3  QW

	struct ToLitContext {
		Vec4			dotMatrix[3];		//   3  QW : output <c0,c1,-,->
		ToLitFilters	filters;			//   3  QW
	};										// = 6  QW

	struct ToLitViewContext {
		ViewContext		view;				//  17  QW
		ToLitContext	tl;					//   6  QW
	};										// = 23 QW

	enum {
		BCL_THROUGHMODE	= (1<<0),			// 2D view mode
		BCL_TOPROJ		= (1<<1),			// build context toproj*
		BCL_TOVIEW		= (1<<2),			// build context toviewMatrix
		BCL_PROJ		= (1<<3),			// build context projMatrix
		BCL_VP			= (1<<4),			// build context vp*
		BCL_LIGHT_M		= (1<<5),			// build context tl.dotMatrix (assert ToLitViewContext !)
		BCL_LIGHT_F		= (1<<6),			// build context tl.filters (assert ToLitViewContext !)
		BCL_MIN			= (BCL_TOPROJ|BCL_VP),
		BCL_MIN_2D		= (BCL_MIN|BCL_THROUGHMODE),
		BCL_TL_MIN		= (BCL_MIN|BCL_LIGHT_M),
		BCL_TL_MIN_2D	= (BCL_MIN_2D|BCL_LIGHT_M),
		BCL_ALL			= (0xFF)
	};

	// Returns array of ViewContext *or* ToLitViewContext (if BCL_LIGHT_M is given)
	void*				BuildViewContextList	(	dmac::Cursor*			DC,
													uint					inSizeof,
													int						inChainIdx,
													uint					inFlags			= BCL_MIN,		// mask of BCL_* flags
													float					inXYZUnpack		= 0.f	);

	// Returns array of ToLitFilters
	void*				BuildLightFiltersList	(	dmac::Cursor*			DC,
													uint					inSizeof,
													int						inChainIdx,
													uint					inNbFilters		= 0,
													Vec4*					inAmbientFilter	= NULL,
													Vec4*					inDiffuseFilter	= NULL,
													uint					inFilterStride	= 0		);		// default is sizeof(ToLitFilters)



	//
	// basic mesh

	namespace mesh
	{
		struct Context {
			ToLitViewContext	tl_view;
		};

		int			Probe_MC			(	ClipMode				inClipMode,
											bool					inToLit					);

		int			Begin_CH1			(	dmac::Cursor*			DC,
											ClipMode				inClipMode,
											bool					inToLit					);

		void		SetContext_CH1		(	dmac::Cursor*			DC,
											Context*				inContext		= NULL,		// dma ref'ed
											ToLitFilters*			inTLFilters		= NULL,		// dma ref'ed
											Vec4*					inToProjTans	= NULL	);	// dma embedded

		void		SetDirect_CH1		(	dmac::Cursor*			DC,
											frame::GsContext*		inGsRegs				);

		void		SetData_CH1			(	dmac::Cursor*			DC,
											uint32					inAddr,
											uint					inQSize,
											uint					inNbPacket,
											uint					inITOPFlags		= 0		);

		void		End_CH1				(	dmac::Cursor*			DC						);
	}


	//
	// basic surface

	namespace surface
	{
		struct Context {
			ToLitViewContext	tl_view;
		};

		int			Probe_MC			(	ClipMode				inClipMode,
											bool					inToLit					);

		bool		BuildData_CH1		(	uint32&					outUploaderAddr,
											dmac::Cursor*			DC,
											uint					inStart,
											uint					inSize,
											Vec4*					inLocA,							// .x,.y,.z,.w=ADC
											Vec2*					inTexA,
											uint32*					inColA,
											Vec3*					inNrmA					);

		bool		BuildData_CH1		(	uint32&					outUploaderAddr,
											dmac::Cursor*			DC,
											uint					inStart,
											uint					inSize,
											NvkSurface*				inSurf					);

		int			Begin_CH1			(	dmac::Cursor*			DC,
											ClipMode				inClipMode,
											bool					inToLit					);

		void		SetContext_CH1		(	dmac::Cursor*			DC,
											Context*				inContext		= NULL,			// dma ref'ed
											ToLitFilters*			inTLFilters		= NULL	);		// dma ref'ed [3]

		void		SetDirect_CH1		(	dmac::Cursor*			DC,
											frame::GsContext*		inGsRegs				);

		void		CallData_CH1		(	dmac::Cursor*			DC,
											uint32					inUploaderAddr,
											uint					inITOPFlags		= 0		);

		void		End_CH1				(	dmac::Cursor*			DC						);
	}


	//
	// poser (prelit mesh only !)

	namespace poser
	{
		enum DataType {
			DT_PRELIT_MESH	= 0,
			DT_SURFACE		= 1
		};

		struct ContextPose {
			uint32			mode;				// |	: mode
			float			fadingA;			// |    : .y = fading a coeff
			float			fadingB;			// |    : .z = fading b coeff
			float			pad;				// 1 QW
		};

		struct Context {
			ViewContext		view;
			ContextPose		pose;
		};

		uint		GetDataMaxQS		(													);

		int			Probe_MC			(	ClipMode				inClipMode,
											DataType				inDataType				);

		Context*	BuildContextList	(	dmac::Cursor*			DC,
											int						inChainIdx,
											uint					inMode,
											float					inFadInnerD,
											float					inFadOuterD,
											float					inXYZUnpack		= 0.f	);

		bool		BuildPoseList_CH1	(	uint32&					outPoseListAddr,
											dmac::Cursor*			DC,
											uint					inStart,
											uint					inSize,
											Vec3*					inLocA,
											Quat*					inRotA,
											Vec3*					inSclA,
											uint32*					inColA,
											Vec3*					inDisplacA				);

		int			Begin_CH1			(	dmac::Cursor*			DC,
											ClipMode				inClipMode,
											DataType				inDataType				);

		void		SetContext_CH1		(	dmac::Cursor*			DC,
											Context*				inContext				);		// dma ref'ed

		void		SetDirect_CH1		(	dmac::Cursor*			DC,
											frame::GsContext*		inGsRegs				);

		void		SetData_CH1			(	dmac::Cursor*			DC,
											uint32					inAddr,
											uint					inQSize					);

		void		CallPoseList_CH1	(	dmac::Cursor*			DC,
											uint32					inPoseListAddr,
											uint					inITOPFlags		= 0		);

		void		End_CH1				(	dmac::Cursor*			DC						);
	}


	//
	// cartoon rendering (tolit mesh only !)

	namespace cartoon
	{
		enum {
			// Extended ITOP flags
			DO_DARK_SHADING		= (1<<2),
			DO_RADIANT_SHADING	= (1<<3),
			DO_INKING			= (1<<4),
			DO_OUTLINING		= (DO_INKING)
		};

		struct ContextToon {
			uint32				shade0_test0;		// |    : sceGsTest
			float				shade0_c[3];		// 1 QW : < ambient-coeff (1), diffuse-coeff (0),   255 >
			uint32				shade1_test0;		// |    : sceGsTest
			float				shade1_c[3];		// 1 QW : < ambient-coeff (1), diffuse-coeff (1),   255 >
			float				ink_ramp_a;			// |	: inking ramp coeff a
			float				ink_ramp_b;			// |	: inking ramp coeff b
			float				pad[2];				// 1 QW
		};

		struct Context {
			ToLitViewContext	tl_view;
			ContextToon			toon;
		};

		int			Probe_MC			(	ClipMode				inClipMode				);

		int			Begin_CH1			(	dmac::Cursor*			DC,
											ClipMode				inClipMode				);

		void		SetContext_CH1		(	dmac::Cursor*			DC,
											Context*				inContext		= NULL,			// dma ref'ed
											ToLitViewContext*		inContextBase	= NULL,
											ContextToon*			inContextToon	= NULL,
											ToLitFilters*			inTLFilters		= NULL,			// dma ref'ed
											Vec4*					inToProjTans	= NULL	);		// dma embedded

		void		SetDirect_CH1		(	dmac::Cursor*			DC,
											frame::GsContext*		inGsRegs				);

		void		SetData_CH1			(	dmac::Cursor*			DC,
											uint32					inAddr,
											uint					inQSize,
											uint					inNbPacket,
											uint					inITOPFlags		= 0		);

		void		End_CH1				(	dmac::Cursor*			DC						);
	}


	//
	// mapping rendering

	namespace mapping
	{
		enum DataType {
			DT_PRELIT_MESH	= 0,
			DT_TOLIT_MESH	= 1,
			DT_SURFACE		= 2
		};

		struct ContextMap {
			Vec4			sourceSelect;		// 1 QW : < loc (0/1), tex (0/1), normal (0/1), - >
			Vec4			colorFilter;		// 1 QW : < r, g, b, a > in [0,1]
			Matrix			sourceMatrix;		// 4 QW
		};

		struct Context {
			ViewContext		view;
			ContextMap		map;
		};

		int			Probe_MC			(	ClipMode				inClipMode,
											DataType				inDataType				);

		int			Begin_CH1			(	dmac::Cursor*			DC,
											ClipMode				inClipMode,
											DataType				inDataType				);

		void		SetContext_CH1		(	dmac::Cursor*			DC,
											Context*				inContext		= NULL,			// dma ref'ed
											ViewContext*			inContextBase	= NULL,			// dma ref'ed
											ContextMap*				inContextMap	= NULL	);		// dma ref'ed

		void		SetDirect_CH1		(	dmac::Cursor*			DC,
											frame::GsContext*		inGsRegs				);

		void		SetData_CH1			(	dmac::Cursor*			DC,
											uint32					inAddr,
											uint					inQSize,
											uint					inNbPacket,
											uint					inITOPFlags		= 0		);

		void		End_CH1				(	dmac::Cursor*			DC						);
	}
}


#include "vu1_Def_I.h"


#endif	// _VU1_DEFS_H_



