/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef _NvDpyContext_H_
#define _NvDpyContext_H_


#include <NvDpyManager.h>



struct NvDpyContext
{

	struct Light {
		Vec4			color		[3];	// { r, g, b, a }
		Vec3			direction	[2];	// { dx, dy, dz }
	};

	struct View {
		Matrix			viewTR;				// camera-matrix^-1
		Matrix			projTR;				// projection-matrix
		Vec2			clipRange;			// ( nearZ, farZ )
		Vec4			viewport;			// { x, y, w, h }
	};

	struct Context {
		uint8			raster;				// DpyRaster
		int16			viewIdx;			// in viewA
		int16			lightIdx;			// in lightA
		int16			wtrIdx;				// in wtrA
	};

	struct ContextChain {
		int16			contextIdx;			// in contextA
		int16			next;				// in contextChainA
	};


	typedef nv::vector<Light> 				LightA;
	typedef nv::vector<View> 				ViewA;
	typedef nv::vector<Matrix> 				MatrixA;
	typedef nv::vector<Context> 			ContextA;
	typedef nv::vector<ContextChain>		ContextChainA;


	LightA				lightA;
	ViewA				viewA;
	MatrixA				wtrA;
	ContextA			contextA;
	ContextChainA		contextChainA;


	bool 				Init				(												);
	bool 				Shut				(												);
	bool				Reset				(												);

	// Target context
	void				SetTarget			(	DpyTarget			inTarget				);

	// View context
	void				SetView				(	Matrix*				inViewTR	 = NULL,		// camera-matrix^-1
												Matrix*				inProjTR	 = NULL,		// projection-matrix
												Vec2*				inClipRange	 = NULL,		// ( nearZ, farZ )
												Vec4*				inViewport	 = NULL		);	// ( x, y, w, h ) in pixel

	// Lighting context
	void				SetLight			(	uint				inLightNo,					// in [0,2]
												Vec4*				inColor		 = NULL,		// in [0,1]^4
												Vec3*				inDirection	 = NULL		);

	// Transform context
	void				SetWorldTR			(	Matrix*				inWorldTR				);

	// Push & chain context
	int					Push				(	int					inNext 		 = -1		);


private :	

	enum {
		CTXT_LIGHT_CHANGED	= (1<<0),
		CTXT_VIEW_CHANGED	= (1<<1),
		CTXT_WTR_CHANGED	= (1<<2),
		CTXT_TARGET_CHANGED	= (1<<3)
	};

	Light				curLight;
	View				curView;
	Matrix				curWTR;
	Context				curContext;
	uint				curState;	
};


#include "NvDpyContext_I.h"



#endif //_NvDpyContext_H_
