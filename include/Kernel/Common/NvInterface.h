/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifndef _NvInterface_H_
#define _NvInterface_H_


// User Interface methods/calls

#define	NVITF_BASECLASS( inC )															Nv##inC##Base
#define	NVITF_BASEPTR( inC, inPtr )														((NVITF_BASECLASS(inC)*)(((char*)inPtr)-((char*)MOFFSET(NVITF_BASECLASS(inC),itf))))
#define	NVITF_CHECKOFFSET( inC, inPtr )													NV_ASSERT( &(NVITF_BASEPTR(inC,inPtr)->itf) == (inPtr) )
#define	NVITF_MTH0( inC, inTR, inMTH )													inTR Nv##inC :: inMTH ( )	{ NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH( ); }
#define	NVITF_MTH1( inC, inTR, inMTH, inT0 )											inTR Nv##inC :: inMTH ( inT0 P0 )	{ NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH( P0 ); }
#define	NVITF_MTH2( inC, inTR, inMTH, inT0, inT1 )										inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1 ) { NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH( P0, P1 ); }
#define	NVITF_MTH3( inC, inTR, inMTH, inT0, inT1, inT2 )								inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2 ) { NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2); }
#define	NVITF_MTH4( inC, inTR, inMTH, inT0, inT1, inT2, inT3 )							inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3 ) { NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3); }
#define	NVITF_MTH5( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4 )					inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4 )	{ NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4); }
#define	NVITF_MTH6( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5 )				inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5 )	{ NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5); }
#define	NVITF_MTH7( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5, inT6 )		inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5, inT6 P6 )	{ NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5,P6); }
#define	NVITF_MTH8( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5, inT6, inT7 )	inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5, inT6 P6, inT7 P7 ) { NVITF_CHECKOFFSET(inC,this); return NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5,P6,P7); }
#define	NVITF_CAL0( inC, inTR, inMTH )													inTR Nv##inC :: inMTH ( )	{ NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH( ); }
#define	NVITF_CAL1( inC, inTR, inMTH, inT0 )											inTR Nv##inC :: inMTH ( inT0 P0 )	{ NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH( P0 ); }
#define	NVITF_CAL2( inC, inTR, inMTH, inT0, inT1 )										inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1 ) { NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH( P0, P1 ); }
#define	NVITF_CAL3( inC, inTR, inMTH, inT0, inT1, inT2 )								inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2 ) { NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2); }
#define	NVITF_CAL4( inC, inTR, inMTH, inT0, inT1, inT2, inT3 )							inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3 ) { NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3); }
#define	NVITF_CAL5( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4 )					inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4 )	{ NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4); }
#define	NVITF_CAL6( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5 )				inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5 )	{ NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5); }
#define	NVITF_CAL7( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5, inT7 )		inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5, inT6 P6 )	{ NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5,P6); }
#define	NVITF_CAL8( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5, inT7, inT8 )	inTR Nv##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5, inT6 P6, inT7 P7 ) { NVITF_CHECKOFFSET(inC,this); NVITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5,P6,P7); }



// Kernel Interface methods/calls

#define	NVKITF_BASECLASS( inC )															Nv##inC##Base
#define	NVKITF_BASEPTR( inC, inPtr )													((NVKITF_BASECLASS(inC)*)(((char*)inPtr)-((char*)MOFFSET(NVKITF_BASECLASS(inC),kitf))))
#define	NVKITF_CHECKOFFSET( inC, inPtr )												NV_ASSERT( &(NVKITF_BASEPTR(inC,inPtr)->kitf) == (inPtr) )
#define	NVKITF_MTH0( inC, inTR, inMTH )													inTR Nvk##inC :: inMTH ( )	{ NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH( ); }
#define	NVKITF_MTH1( inC, inTR, inMTH, inT0 )											inTR Nvk##inC :: inMTH ( inT0 P0 )	{ NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH( P0 ); }
#define	NVKITF_MTH2( inC, inTR, inMTH, inT0, inT1 )										inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1 ) { NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH( P0, P1 ); }
#define	NVKITF_MTH3( inC, inTR, inMTH, inT0, inT1, inT2 )								inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2 ) { NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2); }
#define	NVKITF_MTH4( inC, inTR, inMTH, inT0, inT1, inT2, inT3 )							inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3 ) { NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3); }
#define	NVKITF_MTH5( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4 )					inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4 )	{ NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4); }
#define	NVKITF_MTH6( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5 )				inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5 )	{ NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5); }
#define	NVKITF_MTH7( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5, inT6 )		inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5, inT6 P6 ) { NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5,P6); }
#define	NVKITF_MTH8( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5, inT6, inT7 )	inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5, inT6 P6, inT7 P7 ) { NVKITF_CHECKOFFSET(inC,this); return NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5,P6,P7); }
#define	NVKITF_CAL0( inC, inTR, inMTH )													inTR Nvk##inC :: inMTH ( )	{ NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH( ); }
#define	NVKITF_CAL1( inC, inTR, inMTH, inT0 )											inTR Nvk##inC :: inMTH ( inT0 P0 )	{ NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH( P0 ); }
#define	NVKITF_CAL2( inC, inTR, inMTH, inT0, inT1 )										inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1 ) { NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH( P0, P1 ); }
#define	NVKITF_CAL3( inC, inTR, inMTH, inT0, inT1, inT2 )								inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2 ) { NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2); }
#define	NVKITF_CAL4( inC, inTR, inMTH, inT0, inT1, inT2, inT3 )							inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3 ) { NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3); }
#define	NVKITF_CAL5( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4 )					inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4 )	{ NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4); }
#define	NVKITF_CAL6( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5 )				inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5 )	{ NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5); }
#define	NVKITF_CAL7( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5, inT6 )		inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5, inT6 P6 ) { NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5,P6); }
#define	NVKITF_CAL8( inC, inTR, inMTH, inT0, inT1, inT2, inT3, inT4, inT5, inT6, inT7 )	inTR Nvk##inC :: inMTH ( inT0 P0, inT1 P1, inT2 P2, inT3 P3, inT4 P4, inT5 P5, inT6 P6, inT7 P7 ) { NVKITF_CHECKOFFSET(inC,this); NVKITF_BASEPTR(inC,this)->inMTH(P0,P1,P2,P3,P4,P5,P6,P7); }



#endif	// _NvInterface_H_



