/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkCore_Mem_H_
#define	_NvkCore_Mem_H_


namespace nv { namespace mem
{

	//
	// Neova native memory heaps

	enum MemId {
		MEM_MAIN		= 0,
		MEM_NGC_ARAM	= 1,
		MEM_NGC_MEM2	= 2
	};

	bool								NativeInit			(											);
	void								NativeShut			(											);
	bool								NativeIsReady		(											);
	bool								NativeIsExists		(	MemId			inMemId					);

	void*								NativeMalloc		(	uint32			inBSize,
																uint			inBAlign,
																MemId			inMemId		= MEM_MAIN	);

	void*								NativeRealloc		(	void*			inPointer,
																uint32			inBSize,
																MemId			inMemId		= MEM_MAIN	);

	void								NativeFree			(	void*			inPointer,
																MemId			inMemId		= MEM_MAIN	);

	uint32								NativeUsedBSize		(	MemId			inMemId		= MEM_MAIN	);
	uint32								NativeFreeBSize		(	MemId			inMemId		= MEM_MAIN	);

	template<typename T> inline	T*		NativeNewS			(	uint			inSply,
																MemId			mid			= MEM_MAIN	)	{ return nv::ConstructInPlace<T>((T*)nv::mem::NativeMalloc(sizeof(T)+(inSply),16,mid)) ;}
	template<typename T> inline	T*		NativeNew			(	MemId			mid			= MEM_MAIN	)	{ return nv::ConstructInPlace<T>((T*)nv::mem::NativeMalloc(sizeof(T),16,mid)); }
	template<typename T> inline	void	NativeDelete		(	T*				ptr,
																MemId			mid			= MEM_MAIN	)	{ nv::mem::NativeFree((void*)nv::DestructInPlace<T>(ptr),mid); }




	//
	// Neova allocators

	interface Allocator : public NvInterface
	{
		virtual	NvInterface*	GetBase						(										) = 0;
		virtual void			AddRef						(										) = 0;
		virtual	uint			GetRefCpt					(										) = 0;
		virtual	void			Release						(										) = 0;

		virtual bool			Reset						(										) = 0;
		virtual void			Update						(										) = 0;

		virtual void*			AllocInPlace				(	void*			inPointer,
																uint32			inBSize				) = 0;
		virtual void*			Alloc						(	uint32			inBSize,
																uint			inFlags				) = 0;
		virtual void*			AllocA						(	uint32			inBSize,
																uint			inBAlignment,
																uint			inFlags				) = 0;
		virtual void*			Realloc						(	void*			inPointer,
																uint32			inBSize,
																uint			inFlags				) = 0;
		virtual void			Free						(	void*			inPointer			) = 0;

		virtual	void			EnableProfiling				(										) = 0;
		virtual	void			DisableProfiling			(										) = 0;
		virtual	bool			IsProfiling					(										) = 0;
		virtual uint32			GetTotBSize					(										) = 0;
		virtual uint32			GetAllocCpt					(										) = 0;
		virtual uint32			GetAllocTotBSize			(										) = 0;
		virtual uint32			GetAllocTotBSizePeak		(										) = 0;
		virtual uint32			GetAllocNb					(										) = 0;
		virtual uint32			GetAllocNbPeak				(										) = 0;
		virtual	RangeStatus		CheckRange					(	void*			inPointer,
																uint32			inBSize				) = 0;
	};


	// The default & global CoreEngine memory allocator
	// This allocator must be released by its Release() method.
	Allocator*					CreateCoreEngineDefAllocator(	bool			inCanProfile	= TRUE		);


	// An native allocator implementation
	// This allocator is based on Native[Malloc|Realloc|Free]() functions
	// Its goal is only to log memory events
	// This allocator must be released by its Release() method.
	Allocator*					CreateNativeAllocator		(	MemId			inMemId			= MEM_MAIN,
																bool			inCanProfile	= TRUE		);


	// An inner allocator implementation
	// This allocator must be released by its Release() method.
	Allocator*					CreateInnerAllocator		(	void*			inHeapBase,
																uint32			inHeapBSize,
																bool			inCanProfile	= TRUE		);


	// An outer allocator implementation
	// This allocator must be released by its Release() method.
	Allocator*					CreateOuterAllocator		(	void*			inHeapBase,
																uint32			inHeapBSize,
																uint			inMaxAlloc,
																bool			inCanProfile	= TRUE		);


	// A fast pool oriented fixed size allocator implementation
	// This allocator must be released by its Release() method.
	Allocator*					CreatePoolAllocator			(	void*			inHeapBase,
																uint32			inHeapBSize,
																uint			inItemBSize,
																bool			inCanProfile	= TRUE		);


	//
	// Log

	bool						LogOpen						(												);
	void						LogClose					(												);
	int							LogGetFillingRate			(												);		// in [0,100]
	bool						LogAppendAllocOp			(	void*			inPointer,
																uint32			inBSize,
																uint			inFlags,
																pcstr			inComment		= NULL		);
	bool						LogAppendFreeOp				(	void*			inPointer,
																pcstr			inComment		= NULL		);
	bool						LogAppendMessage			(	pcstr			inMessage					);
	bool						LogFlushCache				(												);
} }


#endif	// _NvkCore_Mem_H_




