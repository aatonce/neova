/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/



#ifdef  _NvDpyContext_H_
 



inline
void				
NvDpyContext::SetTarget		(	DpyTarget			inTarget				)
{
	NV_ASSERT( inTarget.Raster >= DPYR_NULL );
	NV_ASSERT( inTarget.Raster <= DPYR_OFF_FRAME_C32Z32 );
	curContext.raster = inTarget.Raster;
	curState |= CTXT_TARGET_CHANGED;	
}

inline
void				
NvDpyContext::SetView	(	Matrix*				inViewTR	 ,		
							Matrix*				inProjTR	 ,	
							Vec2*				inClipRange	 ,	
							Vec4*				inViewport	 		)
{
	if( inViewTR ) {
		MatrixCopy( &curView.viewTR, inViewTR );
		curState |= CTXT_VIEW_CHANGED;
	}
	if( inProjTR ) {
		MatrixCopy( &curView.projTR, inProjTR );
		curState |= CTXT_VIEW_CHANGED;
	}
	if( inClipRange ) {
		Vec2Copy( &curView.clipRange, inClipRange );
		curState |= CTXT_VIEW_CHANGED;
	}
	if( inViewport ) {
		Vec4Copy( &curView.viewport, inViewport );
		curState |= CTXT_VIEW_CHANGED;
	}	
}

inline
void				
NvDpyContext::SetLight		(	uint				inLightNo,				
								Vec4*				inColor,	
								Vec3*				inDirection	 	)
{
	if( inLightNo > 2 )
		return;
	if( inColor ) {
		Vec4Copy( &curLight.color[ inLightNo ], inColor );
		curState |= CTXT_LIGHT_CHANGED;
	}
	if( inDirection && inLightNo>0 ) {
		Vec3Copy( &curLight.direction[ inLightNo-1 ], inDirection );
		curState |= CTXT_LIGHT_CHANGED;
	}	
}

inline
void				
NvDpyContext::SetWorldTR	(	Matrix*				inWorldTR				)
{
	if( inWorldTR ) {
		MatrixCopy( &curWTR, inWorldTR );
		curState |= CTXT_WTR_CHANGED;
	}
}


inline
int
NvDpyContext::Push ( 	int		inLinkCtxtNo )
{
	// NULL target -> no context pushed !
	if( curContext.raster == DPYR_NULL )
		return -1;

	// Setup context
	if( curState ) {
		if( curState & CTXT_LIGHT_CHANGED ) {
			lightA.push_back( curLight );
			curContext.lightIdx = lightA.size() - 1;
		}
		if( curState & CTXT_WTR_CHANGED ) {
			wtrA.push_back( curWTR );
			curContext.wtrIdx = wtrA.size() - 1;
		}
		if( curState & CTXT_VIEW_CHANGED ) {
			viewA.push_back( curView );
			curContext.viewIdx = viewA.size() - 1;
		}
		contextA.push_back( curContext );
		curState = 0;
	}

	// Context chain
	NV_ASSERTC( contextChainA.size() <= 32000, "DpyManager::Draw() stack overflow (>32000) !" );
	int16 chainIdx = contextChainA.size();
	contextChainA.alloc_back( 1 );
	contextChainA[ chainIdx ].contextIdx = contextA.size() - 1;
	contextChainA[ chainIdx ].next		 = inLinkCtxtNo;

	return chainIdx;
}



#endif //	_NvDpyContext_H_



