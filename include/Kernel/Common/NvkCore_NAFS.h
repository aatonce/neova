/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkCore_NAFS_H_
#define	_NvkCore_NAFS_H_


namespace nv { namespace nafs
{

	enum
	{
		LIMIT_SHLEN	= 64,			// Max. len for share name and password
		DEF_PORT	= 4816			// Default service port
	};
	extern pcstr	DEF_SHNAME;		// Default share name


	//
	// Share info

	struct ShareInfo
	{
		char		addr[LIMIT_SHLEN];
		uint16		port;
		char		name[LIMIT_SHLEN];
		char		passwd[LIMIT_SHLEN];

		void		Init			(												);

		bool		Init			(	pcstr				inAddr,
										uint16				inPort					);

		bool		Init			(	pcstr				inFullname,						// host,[port],shname,[shpasswd],path
										char*				outPath			= NULL,
										uint				inPathMaxLen	= 0		);
	};


	//
	// Command

	struct Command
	{
		void		Init			(										);
		void		Shut			(										);
		bool		Connect			(	pcstr				inAddr,
										uint16				inPort=DEF_PORT	);
		bool		Connect			(	const ShareInfo&	inShInfo		);
		void		Disconnect		(										);
		bool		IsConnected		(										);

		uint		GetCapacity		(										);
		void		SetCapacity		(	uint				inBSize			);
		uint8*		GetBuffer		(										);

		bool		ExecBuffer		(	uint				inBSize			);
		bool		ExecBufferWait	(	uint				inBSize			);
		bool		Exec			(	pcstr inFmt, ...					);
		bool		ExecWait		(	pcstr inFmt, ...					);

		void		Update			(										);
		bool		IsReady			(										);
		char*		GetResult		(										);

		// Basic synchronous commands
		bool		GetFilesize		(	const ShareInfo&	inShInfo,
										pcstr				inFilename,
										uint32&				outBSize		);
		bool		ReadFile		(	const ShareInfo&	inShInfo,
										pcstr				inFilename,
										pvoid				inBuffer,
										uint32				inBSize,
										uint32				inBOffset		);
		bool		WriteFile		(	const ShareInfo&	inShInfo,
										pcstr				inFilename,
										pvoid				inBuffer,
										uint32				inBSize,
										uint32				inBOffset		);
		bool		DumpFromFile	(	const ShareInfo&	inShInfo,
										pcstr				inFilename,
										pvoid&				outBuffer,
										uint32&				outBSize		);
		bool		DumpToFile		(	const ShareInfo&	inShInfo,
										pcstr				inFilename,
										pvoid				inBuffer,
										uint32				inBSize			);
		bool		AppendToFile	(	const ShareInfo&	inShInfo,
										pcstr				inFilename,
										pvoid				inBuffer,
										uint32				inBSize			);

	private:
		UInt8A		buffer;
		int			sockId;
		uint		tosend_bsize;
		uint		tosend_pos;
		uint		torcv_bsize;
		uint		torcv_pos;
	};


	//
	// Tools

	uint8			DecodeHexByte	(	uint8*				inHB		);
	void			EncodeHexByte	(	uint8*				outHB,
										uint8				inB			);

} }


#endif	// _NvkCore_NAFS_H_



