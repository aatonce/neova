/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvkCore_File_H_
#define	_NvkCore_File_H_


namespace nv { namespace file
{
	// Abstract file-system

	interface AbstractFS
	{
		virtual	bool		Init				(											) = 0;
		virtual	void		Shut				(											) = 0;

		virtual Status		GetStatus			(											) = 0;

		virtual	bool		IsOpened			(											) = 0;
		virtual	bool		Open				(	pcstr			inFilename,
													uint32&			outBSize				) = 0;
		virtual	bool		Close				(											) = 0;

		virtual	bool		Read				(	uint			inSysFlags,
													pvoid			inBufferPtr,
													uint32			inBSize,
													uint32			inBOffset0,
													uint32			inBOffset1,
													uint32			inBOffset2,
													uint32			inBOffset3				) = 0;
		virtual	bool		IsReady				(											) = 0;
		virtual	void		Sync				(											) = 0;
		virtual	bool		IsSuccess			(											) = 0;

		// Tools

		virtual	bool		DumpFromFile		(	pcstr			inFilename,
													pvoid&			outBuffer,
													uint&			outBSize				) = 0;
		virtual	bool		DumpToFile			(	pcstr			inFilename,
													pvoid			inBuffer,
													uint			inBSize					) = 0;
		virtual bool		AppendToFile		(	pcstr			inFilename,
													pvoid			inBuffer,
													uint			inBSize					) = 0;

		// Sub devices

		virtual uint		GetNbSubDevice		(											) = 0;
		virtual pcstr		GetSubDeviceName	(	uint			inSubdevNo				) = 0;
		virtual pcstr		GetSubDevicePrefix	(	uint			inSubdevNo				) = 0;
		virtual pcstr		GetSubDeviceSuffix	(	uint			inSubdevNo				) = 0;
	};


	//
	// Default abstract file-systems

	AbstractFS*		GetSystemIOAbstractFS		(											);
	AbstractFS*		CreateNetworkAbstractFS		(											);


	//
	// Registering

	bool			RegisterAbstractFS			(	AbstractFS*		inAbstractFS,
													pcstr			inDevice				);	// 8+

	int				GetNbAbstractFS				(											);
	int				FindAbstractFS				(	pcstr			inDevice				);
	AbstractFS*		GetAbstractFS				(	int				inIndex					);
	pcstr			GetAbstractFSDevice			(	int				inIndex					);
	bool			EnableAbstractFS			(	int				inIndex					);
	bool			DisableAbstractFS			(	int				inIndex					);
	bool			IsEnabledAbstractFS			(	int				inIndex					);


	//
	// Fullname hooking

	typedef pcstr	(*FullnameHook)				(	pcstr			inFullname				);
	FullnameHook	SetFullnameHook				(	FullnameHook	inHook					);

} }


#endif	// _NvkCore_File_H_



