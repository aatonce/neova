/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvRscFactory_SHR_H_
#define	_NvRscFactory_SHR_H_


#include "NvRscFactory.h"
using namespace nv;


struct RscFactory_SHR : public RscFactory
{
	virtual					~RscFactory_SHR			() {}


	//
	// Instance Sharing Mode

	enum SHRM
	{
							// memory usage for N create() ................... | #instance | #DATA
		SHRM_FULL,			// One instance for all creations.			       |     1     |    1
		SHRM_DATA_SHR,		// Data are shared.								   |     N     |    1
		SHRM_DATA_DUP,		// Data are duplicated from another instance.      |     N     |    N
		SHRM_DATA_DUP0,		// Data are duplicated from an original copy.      |     N     |   N+1
		SHRM_CUSTOM,		// Customized data sharing mode                    |     -     |    -
	};


	//
	// Resource instance base

	struct Instance : public NvInterface
	{
		struct Inst
		{
			pvoid			dataptr;					// instance data pointer
			uint32			databsize;					// instance data size in bytes
			uint32			uid;						// instance UID
			uint			refCpt;						// instance ref counter
			Instance*		prev;						// previous instance
			Instance*		next;						// next instance
		} inst;
		NvInterface*		GetBase						(								)	{ return this; }
	};


	//
	// Resource descriptor

	struct Desc
	{
		pvoid				ptr;						// original rsc data pointer
		uint32				bsize;						// original rsc data size in bytes
		uint32				uid;						// rsc UID
		SHRM				shrm;						// sharing mode
		Instance*			head;						// instance list
	};

	nv::vector<Desc>		descA;


	virtual	Desc*			AddDesc						(	uint32		inUID,
															uint32*		outIndex = NULL );
	virtual	Desc*			FindDesc					(	uint32		inUID,
															uint32*		outIndex = NULL	);

	virtual	void			RegisterInit				(								)	{}
	virtual	void			RegisterShut				(								)	{}

	virtual	uint32			GetType						(								) = 0;

	virtual	pvoid			AllocMemRsc					(	uint32		inBSize			);
	virtual	void			FreeMemRsc					(	pvoid		inPtr,
															uint32		inBSize			);

	virtual bool			EnumAvailableRsc			(	uint		inIdx,
															uint*		outUID,
															uint*		outNbInstance,
															uint*		outNbReference,
															uint*		outMemUBSize	);

	virtual	bool			IsAvailableRsc				(	uint32		inUID			);

	virtual uint32			GetToPrefetchRscBSize		(	uint32		inUID,
															uint32		inBSize			);
	virtual	bool			PrefetchRsc					(	uint32		inUID,
															pvoid		inPtr,
															uint32		inBSize			);
	virtual	bool			UnloadRsc					(	uint32		inUID			);
	virtual	void			TranslateRsc				(	pvoid		inRscData,
															uint32		inBOffset		)	{}
	virtual	SHRM			GetRscSHRMode				(	pvoid		inRscData		) = 0;

	virtual	NvResource*		GetInstanceITF				(	Instance*	inInst			) = 0;
	virtual	NvResource*		CreateInstance				(	uint32		inUID			);
	virtual	void			FreeInstance				(	NvResource*	inRsc			);

	virtual	void			InitInstanceObject			(	Instance*	inInst,
															Desc*		inDesc,
															pvoid		inRscData,
															uint32		inRscBSize		);
	virtual	Instance*		CreateInstanceObject		(	Desc*		inDesc,
															pvoid		inRscData,					// NULL for SHRM_CUSTOM mode
															uint32		inRscBSize		) = 0;		// 0    for SHRM_CUSTOM mode
	virtual	void			ReleaseInstanceObject		(	Desc*		inDesc,
															Instance*	inInst			) = 0;
};



#endif // _NvRscFactory_SHR_H_




