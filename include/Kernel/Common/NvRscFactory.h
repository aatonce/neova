/*LIC-HDR********************************************************************
**
** Copyright (C) 2001-2006 AtOnce Technologies
**
** This file is part of the Neova CoreEngine Runtime.
**
** This file and the associated product may be used and distributed
** under the terms of a current License as defined by AtOnce Technologies
** and appearing in the file LICENSE.TXT included in the packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See http://www.atonce-technologies.com or email info@atonce-technologies.com
** for informations about Neova CoreEngine and AtOnce Technologies others products
** License Agreements.
**
** Contact info@atonce-technologies.com if any conditions of this license
** are not clear to you.
**
*****************************************************************LIC-HDR*/


#ifndef	_NvRscFactory_H_
#define	_NvRscFactory_H_


#include <Nova.h>
#include <NvRscManager.h>



struct RscFactory
{
	virtual	void			RegisterInit			(								)	= 0;
	virtual	void			RegisterShut			(								)	= 0;

	virtual	uint32			GetType					(								)	= 0;

	virtual bool			EnumAvailableRsc		(	uint		inIdx,
														uint*		outUID,
														uint*		outNbInstance,
														uint*		outNbReference,
														uint*		outMemUBSize	)	= 0;

	virtual	bool			IsAvailableRsc			(	uint32		inUID			)	= 0;

	virtual	pvoid			AllocMemRsc				(	uint32		inBSize			)	= 0;
	virtual	void			FreeMemRsc				(	pvoid		inPtr,
														uint32		inBSize			)	= 0;

	virtual uint32			GetToPrefetchRscBSize	(	uint32		inUID,
														uint32		inBSize			)	= 0;
	virtual	bool			PrefetchRsc				(	uint32		inUID,
														pvoid		inPtr,
														uint32		inBSize			)	= 0;

	virtual bool			CheckRsc				(	pvoid		inPtr,
														uint32		inBSize			)	{ return TRUE; }

	virtual	bool			UnloadRsc				(	uint32		inUID			)	= 0;

	virtual	NvResource*		CreateInstance			(	uint32		inUID			)	= 0;
	virtual void			ReleaseInstance			(	NvResource*	inRsc			)	{ SafeRelease(inRsc); }
	virtual	void			FreeInstance			(	NvResource*	inRsc			)	= 0;

	// Data Relocation
	static	void			TranslateData			(	pvoid		inDataPtr,
														uint32		inBOffset,
														uint32*		inRelocationTab	);
	static	void			TranslatePointer		(	pvoid		inPointerAddr,
														uint32		inBOffset		);
};




#endif	// _NvRscFactory_H_


